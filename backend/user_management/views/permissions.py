from operator import or_, and_, itemgetter
from urllib import parse
import urllib.parse
from functools import reduce
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import permissions, status
from django.db import IntegrityError
from django.db.models import Q
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from backend.api_permissions.role_based_api_permissions import (
    HasSystemAdminPermission,
    HasRdOperationsPermission,
)
from user_management.user_management_models.user_models import User
from user_management.user_management_models.custom_user_permissions_model import (
    CustomUserPermissions,
)
from user_management.user_management_models.custom_permissions_model import (
    CustomPermissions,
)
from user_management.user_management_models.permission_request_model import (
    PermissionRequest,
)
from user_management.views.utils import (
    is_undefined,
    CustomPagination,
    get_user_info_from_jwt_token,
)
from user_management.serializers.permissions_serializers import (
    GetPermissionsSerializer,
    GetPendingPermissionsSerializer,
    GetActivePermissionsSerializer,
    GetUsersBasedOnSpecifiedPermissionSerializer,
)
from user_management.static.permission import Permission
from cms.views.utils import get_needed_parameters


# getting existing permissions
class GetPermissions(APIView):
    def get(self, request):
        return Response(get_existing_permissions(request))

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


def get_existing_permissions(request):
    custom_permissions = CustomPermissions.objects.all()
    serialized = GetPermissionsSerializer(custom_permissions, many=True)
    return serialized.data


# checking if the specified user is a TA
class GetUserPermissions(APIView):
    def get(self, request):
        user_info = get_user_info_from_jwt_token(request)
        user = User.objects.get(username=user_info["username"])
        # get permissions of the specified user
        custom_user_permissions = CustomUserPermissions.objects.filter(user=user)
        serialized = GetActivePermissionsSerializer(custom_user_permissions, many=True)
        return Response(serialized.data)


# sending permission request
class SendPermissionRequest(APIView):
    def post(self, request):
        return send_permission_request(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


def send_permission_request(request):
    user_info = get_user_info_from_jwt_token(request)
    success, parameters = get_needed_parameters(
        [
            "goc_email",
            "pri_or_military_nbr",
            "supervisor",
            "supervisor_email",
            "rationale",
            "permission_requested",
        ],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    (
        goc_email,
        pri_or_military_nbr,
        supervisor,
        supervisor_email,
        rationale,
        permission_requested,
    ) = itemgetter(
        "goc_email",
        "pri_or_military_nbr",
        "supervisor",
        "supervisor_email",
        "rationale",
        "permission_requested",
    )(
        parameters
    )

    try:
        # check if the requested permission is already associated to the username
        try:
            current_user_permissions = CustomUserPermissions.objects.get(
                user_id=user_info["username"], permission_id=permission_requested
            ).permission_id
            if current_user_permissions > 0:
                return Response(
                    {"error": "this permission is already associated to this username"},
                    status=status.HTTP_400_BAD_REQUEST,
                )
        except:
            pass
        response = PermissionRequest.objects.create(
            username=User.objects.get(username=user_info["username"]),
            goc_email=goc_email,
            pri_or_military_nbr=pri_or_military_nbr,
            supervisor=supervisor,
            supervisor_email=supervisor_email,
            rationale=rationale,
            permission_requested=CustomPermissions.objects.get(
                permission_id=permission_requested
            ),
        )
        response.save()

        return Response(status=status.HTTP_200_OK)
    except CustomPermissions.DoesNotExist:
        return Response(
            {"error": "this permission does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    except IntegrityError:
        return Response(
            {"error": "this permission has already been requested"},
            status=status.HTTP_400_BAD_REQUEST,
        )


# getting all pending permissions (permissions that have been requested, but not approved yet)
class GetPendingPermissions(APIView):
    permission_classes = (
        permissions.IsAuthenticated
        & (HasSystemAdminPermission | HasRdOperationsPermission),
    )

    def get(self, request):
        return get_pending_permissions(request)


def get_pending_permissions(request):
    success, parameters = get_needed_parameters(["page", "page_size"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    page, page_size = itemgetter("page", "page_size")(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # user information
    user_info = get_user_info_from_jwt_token(request)

    # getting all needed pending permission
    permission_ids_to_display = get_permission_ids_to_display(user_info["username"])

    # Get all permission requests that the user can see
    pending_permissions = PermissionRequest.objects.filter(
        permission_requested_id__in=permission_ids_to_display
    ).exclude(username=user_info["username"])

    # only getting data for current selected page
    new_pending_permissions = pending_permissions[
        (current_page - 1) * page_size : current_page * page_size
    ]

    # ordering pending permissions by request date
    # getting serialized pending permissions data
    serialized_pending_permissions_data = GetPendingPermissionsSerializer(
        new_pending_permissions, many=True
    ).data
    # getting sorted serialized data by request date (ascending)
    ordered_pending_permissions = sorted(
        serialized_pending_permissions_data,
        key=lambda k: k["request_date"].lower(),
        reverse=False,
    )
    # initializing user permissions ids array (the user_permission_id order here represents the order)
    user_permission_ids_array = []
    # looping in ordered pending permissions
    for i in ordered_pending_permissions:
        # inserting user permissions ids (ordered respectively by last name) in an array
        user_permission_ids_array.insert(
            len(user_permission_ids_array), i["permission_request_id"]
        )

    # sorting pending permissions queryset based on ordered (by last name) user permission ids
    new_pending_permissions = list(
        PermissionRequest.objects.filter(
            permission_request_id__in=user_permission_ids_array
        ).exclude(username=user_info["username"])
    )
    new_pending_permissions.sort(
        key=lambda t: user_permission_ids_array.index(t.permission_request_id)
    )

    # serializing the data
    serialized = GetPendingPermissionsSerializer(new_pending_permissions, many=True)
    # retuning final data using pagination library
    return paginator.get_paginated_response(
        serialized.data, pending_permissions.count(), current_page, page_size
    )


# getting found pending permissions (triggered by a search)
class GetFoundPendingPermissions(APIView):
    permission_classes = (
        permissions.IsAuthenticated
        & (HasSystemAdminPermission | HasRdOperationsPermission),
    )

    def get(self, request):
        return get_found_pending_permissions(request)


def get_found_pending_permissions(request):
    success, parameters = get_needed_parameters(
        [
            "keyword",
            "current_language",
            "page",
            "page_size",
        ],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    page, page_size, current_language, keyword = itemgetter(
        "page",
        "page_size",
        "current_language",
        "keyword",
    )(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # user information
    user_info = get_user_info_from_jwt_token(request)

    # getting all needed pending permissions
    permission_ids_to_display = get_permission_ids_to_display(user_info["username"])

    # if blank search
    if keyword == " ":
        found_pending_permissions = PermissionRequest.objects.filter(
            permission_requested_id__in=permission_ids_to_display
        ).exclude(username=user_info["username"])
    # regular search
    else:
        # decode keyword to get special characters
        keyword = parse.unquote(keyword)
        # search while interface is in English
        if current_language == "en":
            # getting all permission ids based on keyword that is matching with en_name
            permission_ids = CustomPermissions.objects.filter(
                Q(en_name__icontains=keyword)
            )
        # search while interface is in French
        else:
            # getting all permission ids based on keyword that is matching with fr_name
            permission_ids = CustomPermissions.objects.filter(
                Q(fr_name__icontains=keyword)
            )

        # since there is a comma between last name and first name in pending permissions table, if the user put
        # a comma in one of the search keyword words, remove it
        keyword_without_comma = keyword.replace(",", "")

        # splitting keyword string
        split_keyword = keyword_without_comma.split()
        # if keyword contains more than one word
        if len(split_keyword) > 1:
            # getting all user ids based on keyword that is matching with first_name and/or last_name and/or username
            user_ids = User.objects.filter(
                reduce(
                    and_,
                    [
                        Q(first_name__icontains=splitted_keyword)
                        | Q(last_name__icontains=splitted_keyword)
                        | Q(email__icontains=splitted_keyword)
                        for splitted_keyword in split_keyword
                    ],
                )
            )
        # keyword is empty or contains only one word
        else:
            # getting all user ids based on keyword that is matching with first_name and/or last_name and/or username
            user_ids = User.objects.filter(
                Q(first_name__icontains=keyword)
                | Q(last_name__icontains=keyword)
                | Q(email__icontains=keyword)
            )

        # getting permissions requested for matching request dates and provided keyword
        permissions_requested_based_on_request_date = PermissionRequest.objects.filter(
            Q(request_date__contains=keyword)
        )

        # if at least one result has been found based on the keyword provided
        if (
            permission_ids.exists()
            or user_ids.exists()
            or permissions_requested_based_on_request_date.exists()
        ):
            found_pending_permissions_based_on_users = PermissionRequest.objects.none()
            found_pending_permissions_based_on_permissions = (
                PermissionRequest.objects.none()
            )
            found_pending_permissions_based_on_request_date = (
                PermissionRequest.objects.none()
            )

            # if there are matches in names
            if user_ids.exists():
                # getting all pending permissions where there are matches with provided keyword and names
                found_pending_permissions_based_on_users = (
                    PermissionRequest.objects.filter(
                        reduce(or_, [Q(username_id=user.username) for user in user_ids])
                    ).exclude(username=user_info["username"])
                )

            # if there are matches in permissions
            if permission_ids.exists():
                # getting all pending permissions where there are matches with provided keyword and permissions
                found_pending_permissions_based_on_permissions = (
                    PermissionRequest.objects.filter(
                        reduce(
                            or_,
                            [
                                Q(permission_requested_id=permission.permission_id)
                                for permission in permission_ids
                            ],
                        )
                    ).exclude(username=user_info["username"])
                )

            # if there are matches in request date
            if permissions_requested_based_on_request_date.exists():
                # getting all pending permissions where there are matches with provided keyword and request dates
                found_pending_permissions_based_on_request_date = PermissionRequest.objects.filter(
                    reduce(
                        or_,
                        [
                            Q(
                                permission_request_id=permission_requested.permission_request_id
                            )
                            for permission_requested in permissions_requested_based_on_request_date
                        ],
                    )
                ).exclude(
                    username=user_info["username"]
                )

            # creating combined queryset
            temp_found_pending_permissions = (
                found_pending_permissions_based_on_users.union(
                    found_pending_permissions_based_on_permissions,
                    found_pending_permissions_based_on_request_date,
                )
            )
            found_pending_permissions = []
            # looping in temp_found_pending_permissions in order to only get the respective permissions
            for found_pending_permission in temp_found_pending_permissions:
                # if current permission ID is part of the permission_ids_to_display array
                if (
                    found_pending_permission.permission_requested_id
                    in permission_ids_to_display
                ):
                    found_pending_permissions.append(found_pending_permission)

        # there are no results found based on the keyword provided
        else:
            return Response({"no results found"}, status=status.HTTP_200_OK)

    # creating a new array based on combined queryset (not possible to limit a combined queryset)
    # ref: https://django.readthedocs.io/en/stable/ref/models/querysets.html#union
    found_pending_permissions_array = []

    for pending_permission in found_pending_permissions:
        found_pending_permissions_array.append(pending_permission)

    # only getting data for current selected page
    new_found_pending_permissions = found_pending_permissions_array[
        (current_page - 1) * page_size : current_page * page_size
    ]

    # ordering found pending permissions by last_name
    # getting serialized pending permissions data
    serialized_found_pending_permissions_data = GetPendingPermissionsSerializer(
        new_found_pending_permissions, many=True
    ).data
    # getting sorted serialized data by request date (ascending)
    ordered_found_pending_permissions = sorted(
        serialized_found_pending_permissions_data,
        key=lambda k: k["request_date"].lower(),
        reverse=False,
    )
    # initializing user permissions ids array (the user_permission_id order here represents the order)
    user_permission_ids_array = []
    # looping in ordered pending permissions
    for i in ordered_found_pending_permissions:
        # inserting user permissions ids (ordered respectively by request date) in an array
        user_permission_ids_array.insert(
            len(user_permission_ids_array), i["permission_request_id"]
        )

    # sorting pending permissions queryset based on ordered (by request date) user permission ids
    new_found_pending_permissions = list(
        PermissionRequest.objects.filter(
            permission_request_id__in=user_permission_ids_array
        ).exclude(username=user_info["username"])
    )
    new_found_pending_permissions.sort(
        key=lambda t: user_permission_ids_array.index(t.permission_request_id)
    )

    # serializing the data
    serialized = GetPendingPermissionsSerializer(
        new_found_pending_permissions, many=True
    )
    # retuning final data using pagination library
    return paginator.get_paginated_response(
        serialized.data, len(found_pending_permissions), current_page, page_size
    )


# getting user pending permissions (permissions that have been requested, but not approved yet)
class GetUserPendingPermissions(APIView):
    def get(self, request):
        return get_user_pending_permissions(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


def get_user_pending_permissions(request):
    user_info = get_user_info_from_jwt_token(request)
    # return only pending permissions of the specified user
    response = PermissionRequest.objects.filter(username=user_info["username"])

    serializer = GetPendingPermissionsSerializer(response, many=True)
    return Response(serializer.data)


# getting available permissions (for permission requests)
# available permissions: permissions that are not in user' permissions and/or in user' pending permissions
class GetAvailablePermissionsForRequest(APIView):
    def get(self, request):
        return get_available_permissions(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


def get_available_permissions(request):
    user_info = get_user_info_from_jwt_token(request)
    # initializing available_permissions array
    available_permissions = []

    # getting all existing permissions, user' permissions and user' pending permissions
    existing_permissions = CustomPermissions.objects.all()
    current_user_permissions = CustomUserPermissions.objects.filter(
        Q(user_id=user_info["username"])
    )
    current_user_pending_permissions = PermissionRequest.objects.filter(
        Q(username=user_info["username"])
    )

    # checking what permission should be avaliable, meaning permissions that are not part of user' permissions and user' pending permissions
    # looping in existing permissions
    for existing_permissions in existing_permissions:
        # initializing push_permission flag
        push_permission = True
        # looping in user' permissions
        for user_permissions in current_user_permissions:
            # if user has the specified permission
            if existing_permissions.permission_id == user_permissions.permission_id:
                # don't push this permission to available permissions
                push_permission = False
        # looping in user' pending permissions
        for pending_user_permissions in current_user_pending_permissions:
            # if user has the specified pending permission
            if (
                existing_permissions.permission_id
                == pending_user_permissions.permission_requested_id
            ):
                # don't push this permission to available permissions
                push_permission = False

        # if no push_permission flag has been updated to False, that means the specified permission is available for permission request
        if push_permission:
            available_permissions.append(existing_permissions.permission_id)
    # looking for permission ids that are in available_permissions array
    response = CustomPermissions.objects.filter(permission_id__in=available_permissions)
    # using GetPermissionsSerializer to get all the needed data
    serializer = GetPermissionsSerializer(response, many=True)
    return Response(serializer.data)


# granting specified permission to specified user
class GrantPermission(APIView):
    permission_classes = (
        permissions.IsAuthenticated
        & (HasSystemAdminPermission | HasRdOperationsPermission),
    )

    def post(self, request):
        return grant_permission(request)


def grant_permission(request):
    success, parameters = get_needed_parameters(["username", "permission_id"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    username, permission_id = itemgetter("username", "permission_id")(parameters)

    try:
        # creating new custom permissions object
        permission_request = PermissionRequest.objects.get(
            username=username, permission_requested_id=permission_id
        )
        response = CustomUserPermissions.objects.create(
            permission_id=CustomPermissions.objects.get(
                permission_id=permission_id
            ).permission_id,
            user_id=User.objects.get(username=username).username,
            goc_email=permission_request.goc_email,
            pri_or_military_nbr=permission_request.pri_or_military_nbr,
            supervisor=permission_request.supervisor,
            supervisor_email=permission_request.supervisor_email,
            rationale=permission_request.rationale,
        )
        # saving new object
        response.save()

        # deleting associated permission request
        pending_permission_id = PermissionRequest.objects.get(
            username=username, permission_requested_id=permission_id
        )
        # deleting permission object
        pending_permission_id.delete()
        return Response(status=status.HTTP_200_OK)
    except CustomPermissions.DoesNotExist:
        return Response(
            {"error": "the specified permission does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    except User.DoesNotExist:
        return Response(
            {"error": "the specified username does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    except PermissionRequest.DoesNotExist:
        return Response(
            {"error": "there is no pending permission associated to this request"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    except IntegrityError:
        return Response(
            {"error": "this permission is already associated to this username"},
            status=status.HTTP_400_BAD_REQUEST,
        )


# denying requested permission
class DenyPermission(APIView):
    permission_classes = (
        permissions.IsAuthenticated
        & (HasSystemAdminPermission | HasRdOperationsPermission),
    )

    def post(self, request):
        return deny_permission(request)


def deny_permission(request):
    success, parameters = get_needed_parameters(["permission_request_id"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    permission_request_id = itemgetter("permission_request_id")(parameters)

    try:
        # getting specified permission
        permission = PermissionRequest.objects.get(
            permission_request_id=permission_request_id
        )
        # deleting the permission from permission request table
        permission.delete()

        return Response(status=status.HTTP_200_OK)
    except PermissionRequest.DoesNotExist:
        return Response(
            {"error": "the specified permission request ID does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )


# getting active permissions
class GetActivePermissions(APIView):
    permission_classes = (
        permissions.IsAuthenticated
        & (HasSystemAdminPermission | HasRdOperationsPermission),
    )

    def get(self, request):
        return get_active_permissions(request)


def get_active_permissions(request):
    success, parameters = get_needed_parameters(["page", "page_size"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    page, page_size = itemgetter("page", "page_size")(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()
    # getting all active permissions

    user_info = get_user_info_from_jwt_token(request)

    # getting all needed active permissions
    permission_ids_to_display = get_permission_ids_to_display(user_info["username"])

    active_permissions = CustomUserPermissions.objects.filter(
        permission_id__in=permission_ids_to_display
    )

    # only getting data for current selected page
    new_active_permissions = active_permissions[
        (current_page - 1) * page_size : current_page * page_size
    ]

    # ordering active permissions by last_name
    # getting serialized active permissions data
    serialized_active_permissions_data = GetActivePermissionsSerializer(
        new_active_permissions, many=True
    ).data
    # getting sorted serialized data by last_name (ascending)
    ordered_active_permissions = sorted(
        serialized_active_permissions_data,
        key=lambda k: k["last_name"].lower(),
        reverse=False,
    )
    # initializing user permissions ids array (the user_permission_id order here represents the order)
    user_permission_ids_array = []
    # looping in ordered active permissions
    for i in ordered_active_permissions:
        # inserting user permissions ids (ordered respectively by last name) in an array
        user_permission_ids_array.insert(
            len(user_permission_ids_array), i["user_permission_id"]
        )

    # sorting active permissions queryset based on ordered (by last name) user permission ids
    new_active_permissions = list(
        CustomUserPermissions.objects.filter(
            user_permission_id__in=user_permission_ids_array
        )
    )
    new_active_permissions.sort(
        key=lambda t: user_permission_ids_array.index(t.user_permission_id)
    )

    # serializing the data
    serialized = GetActivePermissionsSerializer(new_active_permissions, many=True)
    # retuning final data using pagination library
    return paginator.get_paginated_response(
        serialized.data, active_permissions.count(), current_page, page_size
    )


# getting found active permissions (triggered by a search)
class GetFoundActivePermissions(APIView):
    permission_classes = (
        permissions.IsAuthenticated
        & (HasSystemAdminPermission | HasRdOperationsPermission),
    )

    def get(self, request):
        return get_found_active_permissions(request)


def get_found_active_permissions(request):
    success, parameters = get_needed_parameters(
        [
            "keyword",
            "current_language",
            "page",
            "page_size",
        ],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    page, page_size, current_language, keyword = itemgetter(
        "page",
        "page_size",
        "current_language",
        "keyword",
    )(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    user_info = get_user_info_from_jwt_token(request)

    # getting all needed active permissions
    permission_ids_to_display = get_permission_ids_to_display(user_info["username"])

    # if blank search
    if keyword == " ":
        found_active_permissions = CustomUserPermissions.objects.filter(
            permission_id__in=permission_ids_to_display
        )
    # regular search
    else:
        # search while interface is in English
        if current_language == "en":
            # getting all permission ids based on keyword that is matching with en_name
            permission_ids = CustomPermissions.objects.filter(
                Q(en_name__icontains=keyword)
            )
        # search while interface is in French
        else:
            # getting all permission ids based on keyword that is matching with fr_name
            permission_ids = CustomPermissions.objects.filter(
                Q(fr_name__icontains=keyword)
            )

        # since there is a comma between last name and first name in active permissions table, if the user put
        # a comma in one of the search keyword words, remove it
        keyword_without_comma = keyword.replace(",", "")

        # splitting keyword string
        split_keyword = keyword_without_comma.split()
        # if keyword contains more than one word
        if len(split_keyword) > 1:
            # getting all user ids based on keyword that is matching with first_name and/or last_name and/or username
            user_ids = User.objects.filter(
                reduce(
                    and_,
                    [
                        Q(first_name__icontains=splitted_keyword)
                        | Q(last_name__icontains=splitted_keyword)
                        | Q(email__icontains=splitted_keyword)
                        for splitted_keyword in split_keyword
                    ],
                )
            )
        # keyword is empty or contains only one word
        else:
            # getting all user ids based on keyword that is matching with first_name and/or last_name and/or username
            user_ids = User.objects.filter(
                Q(first_name__icontains=keyword)
                | Q(last_name__icontains=keyword)
                | Q(email__icontains=keyword)
            )

        # if at least one result has been found based on the keyword provided
        if permission_ids.exists() or user_ids.exists():
            # if there are matches in both (permissions and names)
            if permission_ids.exists() and user_ids.exists():
                # getting all active permissions where there are matches with provided keyword and names
                found_active_permissions_names = CustomUserPermissions.objects.filter(
                    reduce(or_, [Q(user_id=user.username) for user in user_ids])
                )
                # getting all active permissions where there are matches with provided keyword and permissions
                found_active_permissions_permissions = (
                    CustomUserPermissions.objects.filter(
                        reduce(
                            or_,
                            [
                                Q(permission_id=permission.permission_id)
                                for permission in permission_ids
                            ],
                        )
                    )
                )
                temp_found_active_permissions = found_active_permissions_names.union(
                    found_active_permissions_permissions
                )
                found_active_permissions = []
                # looping in temp_found_active_permissions in order to only get the respective permissions
                for found_active_permission in temp_found_active_permissions:
                    # if current permission ID is part of the permission_ids_to_display array
                    if (
                        found_active_permission.permission_id
                        in permission_ids_to_display
                    ):
                        found_active_permissions.append(found_active_permission)

            # if there are matches only in names
            elif user_ids.exists():
                # getting all active permissions where there are matches with provided keyword and names
                temp_found_active_permissions = CustomUserPermissions.objects.filter(
                    reduce(or_, [Q(user_id=user.username) for user in user_ids])
                )
                found_active_permissions = []
                # looping in temp_found_active_permissions in order to only get the respective permissions
                for found_active_permission in temp_found_active_permissions:
                    # if current permission ID is part of the permission_ids_to_display array
                    if (
                        found_active_permission.permission_id
                        in permission_ids_to_display
                    ):
                        found_active_permissions.append(found_active_permission)

            # if there are matches only in permissions
            elif permission_ids.exists():
                # getting all active permissions where there are matches with provided keyword and permissions
                temp_found_active_permissions = CustomUserPermissions.objects.filter(
                    reduce(
                        or_,
                        [
                            Q(permission_id=permission.permission_id)
                            for permission in permission_ids
                        ],
                    )
                )
                found_active_permissions = []
                # looping in temp_found_active_permissions in order to only get the respective permissions
                for found_active_permission in temp_found_active_permissions:
                    # if current permission ID is part of the permission_ids_to_display array
                    if (
                        found_active_permission.permission_id
                        in permission_ids_to_display
                    ):
                        found_active_permissions.append(found_active_permission)

        # there are no results found based on the keyword provided
        else:
            return Response({"no results found"}, status=status.HTTP_200_OK)

    # creating a new array based on combined queryset (not possible to limit a combined queryset)
    # ref: https://django.readthedocs.io/en/stable/ref/models/querysets.html#union
    found_active_permissions_array = []

    for active_permission in found_active_permissions:
        found_active_permissions_array.append(active_permission)

    # only getting data for current selected page
    new_found_active_permissions = found_active_permissions_array[
        (current_page - 1) * page_size : current_page * page_size
    ]

    # ordering found active permissions by last_name
    # getting serialized active permissions data
    serialized_found_active_permissions_data = GetActivePermissionsSerializer(
        new_found_active_permissions, many=True
    ).data
    # getting sorted serialized data by last_name (ascending)
    ordered_found_active_permissions = sorted(
        serialized_found_active_permissions_data,
        key=lambda k: k["last_name"].lower(),
        reverse=False,
    )
    # initializing user permissions ids array (the user_permission_id order here represents the order)
    user_permission_ids_array = []
    # looping in ordered active permissions
    for i in ordered_found_active_permissions:
        # inserting user permissions ids (ordered respectively by last name) in an array
        user_permission_ids_array.insert(
            len(user_permission_ids_array), i["user_permission_id"]
        )

    # sorting active permissions queryset based on ordered (by last name) user permission ids
    new_found_active_permissions = list(
        CustomUserPermissions.objects.filter(
            user_permission_id__in=user_permission_ids_array
        )
    )
    new_found_active_permissions.sort(
        key=lambda t: user_permission_ids_array.index(t.user_permission_id)
    )

    # serializing the data
    serialized = GetActivePermissionsSerializer(new_found_active_permissions, many=True)
    # retuning final data using pagination library
    return paginator.get_paginated_response(
        serialized.data, len(found_active_permissions), current_page, page_size
    )


# updating specified active permission
class UpdateActivePermissionData(APIView):
    permission_classes = (
        permissions.IsAuthenticated
        & (HasSystemAdminPermission | HasRdOperationsPermission),
    )

    def post(self, request):
        return update_active_permission_data(request)


def update_active_permission_data(request):
    success, parameters = get_needed_parameters(
        [
            "user_permission_id",
            "goc_email",
            "supervisor",
            "supervisor_email",
            "reason_for_modification",
        ],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    (
        user_permission_id,
        goc_email,
        supervisor,
        supervisor_email,
        reason_for_modification,
    ) = itemgetter(
        "user_permission_id",
        "goc_email",
        "supervisor",
        "supervisor_email",
        "reason_for_modification",
    )(
        parameters
    )

    try:
        # getting specified user permission
        user_permission = CustomUserPermissions.objects.get(
            user_permission_id=user_permission_id
        )

        # validating goc email (in case a manual update is done directly in the database)
        try:
            validate_email(goc_email)
        except ValidationError:
            return Response(
                {"error": "The goc email is invalid"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        # validating supervisor email (in case a manual update is done directly in the database)
        try:
            validate_email(supervisor_email)
        except ValidationError:
            return Response(
                {"error": "The supervisor email is invalid"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        # updating needed fields
        user_permission.goc_email = goc_email
        user_permission.supervisor = supervisor
        user_permission.supervisor_email = supervisor_email
        user_permission.reason_for_modif_or_del = reason_for_modification

        # saving new data
        user_permission.save()

        return Response(status=status.HTTP_200_OK)
    except CustomUserPermissions.DoesNotExist:
        return Response(
            {"error": "the specified user permission does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )


# deleting specified user permission
class DeleteActivePermission(APIView):
    permission_classes = (
        permissions.IsAuthenticated
        & (HasSystemAdminPermission | HasRdOperationsPermission),
    )

    def post(self, request):
        return delete_active_permission(request)


def delete_active_permission(request):
    success, parameters = get_needed_parameters(
        ["user_permission_id", "reason_for_deletion"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    user_permission_id, reason_for_deletion = itemgetter(
        "user_permission_id", "reason_for_deletion"
    )(parameters)

    try:
        # getting specified user permission
        user_permission = CustomUserPermissions.objects.get(
            user_permission_id=user_permission_id
        )

        # updating reason_for_modif_or_del field
        user_permission.reason_for_modif_or_del = reason_for_deletion
        user_permission.save()

        # deleting specified user permission
        user_permission.delete()
        return Response(status=status.HTTP_200_OK)
    except CustomUserPermissions.DoesNotExist:
        return Response(
            {"error": "the specified user permission does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )


# getting all users that have specified permission
class GetUsersBasedOnSpecifiedPermission(APIView):
    permission_classes = (
        permissions.IsAuthenticated
        & (HasSystemAdminPermission | HasRdOperationsPermission),
    )

    def get(self, request):
        return get_users_based_on_specified_permission(request)


def get_users_based_on_specified_permission(request):
    permission_codename = request.query_params.get("permission_codename", None)
    # making sure that we have the needed parameters
    if is_undefined(permission_codename):
        return Response(
            {"error": "no 'permission_codename' parameter"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    try:
        permission_id = CustomPermissions.objects.get(
            codename=permission_codename
        ).permission_id
    except CustomPermissions.DoesNotExist:
        return Response(
            {"error": "the specified permission codename does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    users = CustomUserPermissions.objects.filter(permission_id=permission_id)
    serializer = GetUsersBasedOnSpecifiedPermissionSerializer(users, many=True)

    return Response(serializer.data)


# getting selected user permissions
class GetSelectedUserPermissions(APIView):
    def get(self, request):
        return get_selected_user_existing_permissions(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


def get_selected_user_existing_permissions(request):
    username = request.query_params.get("username", None)

    # username is defined
    if username is not None:
        # decoding username that might contain special characters
        username = urllib.parse.unquote(username)

    # get permissions of the specified user
    custom_user_permissions = CustomUserPermissions.objects.filter(user=username)
    serialized = GetActivePermissionsSerializer(custom_user_permissions, many=True)
    return Response(serialized.data)


# getting selected pending permissions
class GetSelectedUserPendingPermissions(APIView):
    def get(self, request):
        return get_selected_user_pending_permissions(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


def get_selected_user_pending_permissions(request):
    username = request.query_params.get("username", None)

    # username is defined
    if username is not None:
        # decoding username that might contain special characters
        username = urllib.parse.unquote(username)

    # return only pending permissions of the specified user
    response = PermissionRequest.objects.filter(username=username)
    serializer = GetPendingPermissionsSerializer(response, many=True)
    return Response(serializer.data)


def get_permission_ids_to_display(username):
    permission_ids_to_display = []

    is_staff = User.objects.get(username=username).is_staff

    if is_staff:
        for permission_id in CustomPermissions.objects.all().values_list(
            "permission_id", flat=True
        ):
            permission_ids_to_display.append(permission_id)

    else:
        # Is the user a System Administrator?
        permission_id = CustomPermissions.objects.get(
            codename=Permission.SYSTEM_ADMINISTRATOR
        ).permission_id

        is_etta = CustomUserPermissions.objects.filter(
            user=username, permission=permission_id
        )

        if is_etta:
            for permission_id in CustomPermissions.objects.filter(
                codename__in=(
                    Permission.SYSTEM_ADMINISTRATOR,
                    Permission.SCORER,
                    Permission.TEST_ADMINISTRATOR,
                    Permission.AAE,
                )
            ).values_list("permission_id", flat=True):
                permission_ids_to_display.append(permission_id)

        # Is the user a RD Operations?
        permission_id = CustomPermissions.objects.get(
            codename=Permission.RD_OPERATIONS
        ).permission_id

        is_rd_operations = CustomUserPermissions.objects.filter(
            user=username, permission=permission_id
        )

        if is_rd_operations:
            for permission_id in CustomPermissions.objects.filter(
                codename__in=(
                    Permission.RD_OPERATIONS,
                    Permission.PPC_ADMINISTRATOR,
                    Permission.TEST_BUILDER,
                    Permission.TEST_DEVELOPER,
                )
            ).values_list("permission_id", flat=True):
                permission_ids_to_display.append(permission_id)

    return permission_ids_to_display
