import pytz
import datetime
import urllib.parse
from rest_framework import status
from rest_framework.response import Response
from user_management.user_management_models.ee_info_options import EEInfoOptions
from user_management.static.extended_profile import (
    ExtendedProfileEEInfo,
    VisibleMinorityId,
    DisabilityId,
)
from user_management.user_management_models.ta_extended_profile_models import (
    TaExtendedProfile,
)
from backend.views.utils import get_user_info_from_jwt_token
from user_management.user_management_models.user_models import User
from user_management.user_management_models.user_extended_profile import (
    UserExtendedProfile,
)
from user_management.user_management_models.user_extended_profile_aboriginal import (
    UserExtendedProfileAboriginal,
)
from user_management.user_management_models.user_extended_profile_visible_minority import (
    UserExtendedProfileVisibleMinority,
)
from user_management.user_management_models.user_extended_profile_disability import (
    UserExtendedProfileDisability,
)
from user_management.serializers.user_extended_profile_serializer import (
    UserExtendedProfileSerializer,
    SelectedUserExtendedProfileSerializer,
    TaExtendedProfileSerializer,
)


def save_extended_profile(username, parameters, optional_parameters):
    user = User.objects.get(username=username)
    extended_profile = UserExtendedProfile.objects.filter(user_id=user.id).first()
    if not extended_profile:
        extended_profile = UserExtendedProfile(user_id=user.id)

    # Optional Info
    extended_profile.current_employer_id = (
        optional_parameters["current_employer_id"] or 0
    )
    extended_profile.organization_id = optional_parameters["organization_id"] or 0
    extended_profile.group_id = optional_parameters["group_id"] or 0
    extended_profile.subgroup_id = optional_parameters["subclassification_id"] or 0
    extended_profile.level_id = optional_parameters["level_id"] or 0
    extended_profile.residence_id = optional_parameters["residence_id"] or 0
    extended_profile.education_id = optional_parameters["education_id"] or 0
    extended_profile.gender_id = optional_parameters["gender_id"] or 0

    # EE Info
    extended_profile.identify_as_woman = EEInfoOptions.objects.get(
        opt_id=int(parameters["identify_as_woman"])
    )
    extended_profile.aboriginal = EEInfoOptions.objects.get(
        opt_id=int(parameters["aboriginal"])
    )
    extended_profile.visible_minority = EEInfoOptions.objects.get(
        opt_id=int(parameters["visible_minority"])
    )
    extended_profile.disability = EEInfoOptions.objects.get(
        opt_id=int(parameters["disability"])
    )

    extended_profile.save()

    # deleting existing aboriginal, visible minority and disability data
    UserExtendedProfileAboriginal.objects.filter(
        user_extended_profile_id=extended_profile.id
    ).delete()
    UserExtendedProfileVisibleMinority.objects.filter(
        user_extended_profile_id=extended_profile.id
    ).delete()
    UserExtendedProfileDisability.objects.filter(
        user_extended_profile_id=extended_profile.id
    ).delete()

    # aboriginal is set to YES
    if extended_profile.aboriginal.opt_id == ExtendedProfileEEInfo.YES:
        # if aboriginal_ids not NULL
        if optional_parameters["aboriginal_ids"] != None:
            # formatting aboriginal_ids
            aboriginal_ids = str(optional_parameters["aboriginal_ids"])
            aboriginal_ids = aboriginal_ids.replace("[", "")
            aboriginal_ids = aboriginal_ids.replace("]", "")
            aboriginal_ids = aboriginal_ids.split(",")
            # looping in aboriginal_ids
            for aboriginal_id in aboriginal_ids:
                # creating needed aboriginal entries
                UserExtendedProfileAboriginal.objects.create(
                    aboriginal_id=aboriginal_id,
                    user_extended_profile_id=extended_profile.id,
                )

    # visible_minority is set to YES
    if extended_profile.visible_minority.opt_id == ExtendedProfileEEInfo.YES:
        # if visible_minority_ids not NULL
        if optional_parameters["visible_minority_ids"] != None:
            # formatting visible_minority_ids
            visible_minority_ids = str(optional_parameters["visible_minority_ids"])
            visible_minority_ids = visible_minority_ids.replace("[", "")
            visible_minority_ids = visible_minority_ids.replace("]", "")
            visible_minority_ids = visible_minority_ids.split(",")
            # looping in visible_minority_ids
            for visible_minority_id in visible_minority_ids:
                # if other option
                if int(visible_minority_id) == VisibleMinorityId.OTHER:
                    # creating needed visible minority entries
                    UserExtendedProfileVisibleMinority.objects.create(
                        visible_minority_id=visible_minority_id,
                        user_extended_profile_id=extended_profile.id,
                        other_specification=optional_parameters[
                            "visible_minority_other_specification_content"
                        ]
                        or "",
                    )
                else:
                    # creating needed visible minority entries
                    UserExtendedProfileVisibleMinority.objects.create(
                        visible_minority_id=visible_minority_id,
                        user_extended_profile_id=extended_profile.id,
                        other_specification=None,
                    )

    # disability is set to YES
    if extended_profile.disability.opt_id == ExtendedProfileEEInfo.YES:
        # if disability_ids not NULL
        if optional_parameters["disability_ids"] != None:
            # formatting disability_ids
            disability_ids = str(optional_parameters["disability_ids"])
            disability_ids = disability_ids.replace("[", "")
            disability_ids = disability_ids.replace("]", "")
            disability_ids = disability_ids.split(",")
            # looping in disability_ids
            for disability_id in disability_ids:
                # if other option
                if int(disability_id) == DisabilityId.OTHER:
                    # creating needed disability entries
                    UserExtendedProfileDisability.objects.create(
                        disability_id=disability_id,
                        user_extended_profile_id=extended_profile.id,
                        other_specification=optional_parameters[
                            "disability_other_specification_content"
                        ]
                        or "",
                    )
                else:
                    # creating needed disability entries
                    UserExtendedProfileDisability.objects.create(
                        disability_id=disability_id,
                        user_extended_profile_id=extended_profile.id,
                        other_specification=None,
                    )

    # setting is_profile_complete flag with timestamp
    user.is_profile_complete = True
    user.last_profile_update = datetime.datetime.now(pytz.utc)
    user.save()

    return Response(status.HTTP_200_OK)


def get_extended_profile(username):
    user = User.objects.get(username=username)

    extended_profile = UserExtendedProfile.objects.filter(user_id=user.id).first()

    if not extended_profile:
        return Response(
            {"message": "no extended profile for the requested user"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    return Response(
        UserExtendedProfileSerializer(extended_profile).data, status=status.HTTP_200_OK
    )


def get_selected_user_extended_profile(request):
    username = request.query_params.get("username", None)

    # username is defined
    if username is not None:
        # decoding username that might contain special characters
        username = urllib.parse.unquote(username)

    user = User.objects.get(username=username)

    extended_profile = UserExtendedProfile.objects.filter(user_id=user.id).first()

    if not extended_profile:
        return Response(
            {"message": "no extended profile for the requested user"},
            status=status.HTTP_200_OK,
        )
    return Response(
        SelectedUserExtendedProfileSerializer(extended_profile).data,
        status=status.HTTP_200_OK,
    )


def get_ta_extended_profile(request):
    # getting user info
    user_info = get_user_info_from_jwt_token(request)

    try:
        # getting ta extended profile data
        ta_extended_profile_data = TaExtendedProfile.objects.get(
            username_id=user_info["username"]
        )

        serialized_data = TaExtendedProfileSerializer(
            ta_extended_profile_data, many=False
        ).data

        return Response(serialized_data)

    except TaExtendedProfile.DoesNotExist:
        return Response([])
