from rest_framework.views import APIView
from rest_framework import permissions, status
from rest_framework.response import Response
from user_management.views.user_extended_profile import (
    save_extended_profile,
    get_extended_profile,
    get_selected_user_extended_profile,
    get_ta_extended_profile,
)
from cms.views.utils import get_optional_parameters, get_needed_parameters
from backend.views.utils import get_user_info_from_jwt_token
from backend.api_permissions.role_based_api_permissions import (
    HasSystemAdminPermission,
    HasTestAdminPermission,
)


class GetExtendedProfile(APIView):
    def get(self, request):
        user_info = get_user_info_from_jwt_token(request)

        return get_extended_profile(user_info["username"])

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class SetExtendedProfile(APIView):
    def post(self, request):
        success, parameters = get_needed_parameters(
            ["identify_as_woman", "aboriginal", "visible_minority", "disability"],
            request,
        )
        if not success:
            return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

        optional_parameters = get_optional_parameters(
            [
                "current_employer_id",
                "organization_id",
                "group_id",
                "subclassification_id",
                "level_id",
                "residence_id",
                "education_id",
                "gender_id",
                # needs to be provided as an array
                "aboriginal_ids",
                # needs to be provided as an array
                "visible_minority_ids",
                "visible_minority_other_specification_content",
                # needs to be provided as an array
                "disability_ids",
                "disability_other_specification_content",
            ],
            request,
        )

        user_info = get_user_info_from_jwt_token(request)

        return save_extended_profile(
            user_info["username"], parameters, optional_parameters
        )

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class GetSelectedUserExtendedProfile(APIView):
    def get(self, request):
        return get_selected_user_extended_profile(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


class GetTaExtendedProfile(APIView):
    def get(self, request):
        return get_ta_extended_profile(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestAdminPermission()]
