from datetime import datetime, timedelta
import pytz
from django.utils import timezone
from django.contrib.auth.password_validation import (
    validate_password,
    get_password_validators,
)
from django.conf import settings
from django.core.mail import send_mail
from django.core.exceptions import ValidationError
from django.dispatch import receiver
from django_rest_passwordreset.signals import (
    reset_password_token_created,
    pre_password_reset,
    post_password_reset,
)
from django_rest_passwordreset.models import (
    ResetPasswordToken,
    clear_expired,
    get_password_reset_token_expiry_time,
    get_password_reset_lookup_field,
)
from django_rest_passwordreset.serializers import (
    EmailSerializer,
    PasswordTokenSerializer,
    ResetTokenSerializer,
)
from rest_framework import exceptions
from rest_framework.response import Response
from rest_framework import status
from rest_framework.generics import GenericAPIView
from user_management.user_management_models.user_models import User
from user_management.user_management_models.user_password_reset_tracking_model import (
    UserPasswordResetTracking,
)
from user_management.static.password_reset import PasswordResetConfigurations
from text_resources_backend.text_resources import TextResources

HTTP_USER_AGENT_HEADER = getattr(
    settings, "DJANGO_REST_PASSWORDRESET_HTTP_USER_AGENT_HEADER", "HTTP_USER_AGENT"
)
HTTP_IP_ADDRESS_HEADER = getattr(
    settings, "DJANGO_REST_PASSWORDRESET_IP_ADDRESS_HEADER", "REMOTE_ADDR"
)


# this is the exact same view as in django_rest_passwordreset.views.py
class CustomResetPasswordValidateToken(GenericAPIView):
    """
    An Api View which provides a method to verify that a token is valid
    """

    throttle_classes = ()
    permission_classes = ()
    serializer_class = ResetTokenSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        token = serializer.validated_data["token"]

        # get token validation time
        password_reset_token_validation_time = get_password_reset_token_expiry_time()

        # find token
        reset_password_token = ResetPasswordToken.objects.filter(key=token).first()

        if reset_password_token is None:
            return Response({"status": "notfound"}, status=status.HTTP_404_NOT_FOUND)

        # check expiry date
        expiry_date = reset_password_token.created_at + timedelta(
            hours=password_reset_token_validation_time
        )

        if timezone.now() > expiry_date:
            # delete expired token
            reset_password_token.delete()
            return Response({"status": "expired"}, status=status.HTTP_404_NOT_FOUND)

        return Response({"status": "OK"})


# this is the exact same view as in django_rest_passwordreset.views.py
class CustomResetPasswordConfirm(GenericAPIView):
    """
    An Api View which provides a method to reset a password based on a unique token
    """

    throttle_classes = ()
    permission_classes = ()
    serializer_class = PasswordTokenSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        password = serializer.validated_data["password"]
        token = serializer.validated_data["token"]

        # get token validation time
        password_reset_token_validation_time = get_password_reset_token_expiry_time()

        # find token
        reset_password_token = ResetPasswordToken.objects.filter(key=token).first()

        if reset_password_token is None:
            return Response({"status": "notfound"}, status=status.HTTP_404_NOT_FOUND)

        # check expiry date
        expiry_date = reset_password_token.created_at + timedelta(
            hours=password_reset_token_validation_time
        )

        if timezone.now() > expiry_date:
            # delete expired token
            reset_password_token.delete()
            return Response({"status": "expired"}, status=status.HTTP_404_NOT_FOUND)

        # change users password (if we got to this code it means that the user is_active)
        if reset_password_token.user.eligible_for_reset():
            pre_password_reset.send(
                sender=self.__class__, user=reset_password_token.user
            )
            try:
                # validate the password against existing validators
                validate_password(
                    password,
                    user=reset_password_token.user,
                    password_validators=get_password_validators(
                        settings.AUTH_PASSWORD_VALIDATORS
                    ),
                )
            except ValidationError as e:
                # raise a validation error for the serializer
                raise exceptions.ValidationError({"password": e.messages})

            reset_password_token.user.set_password(password)
            reset_password_token.user.save()
            post_password_reset.send(
                sender=self.__class__, user=reset_password_token.user
            )

            user = User.objects.get(email=reset_password_token.user)
            # if the user is locked, unlock the user and reset login_attempts
            # to ensure that they can login
            if user.locked:
                user.locked = False
                user.locked_until = None
                user.login_attempts = 0
                user.save()

        # Delete all password reset tokens for this user
        ResetPasswordToken.objects.filter(user=reset_password_token.user).delete()

        return Response({"status": "OK"})


# this is the exact same view as in django_rest_passwordreset.views.py (with some minor changes to accommodate the send email requirements)
class CustomResetPasswordRequestToken(GenericAPIView):
    """
    An Api View which provides a method to request a password reset token based on an e-mail address

    Sends a signal reset_password_token_created when a reset token was created
    """

    throttle_classes = ()
    permission_classes = ()
    serializer_class = EmailSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        email = serializer.validated_data["email"]

        # before we continue, delete all existing expired tokens
        password_reset_token_validation_time = get_password_reset_token_expiry_time()

        # datetime.now minus expiry hours
        now_minus_expiry_time = timezone.now() - timedelta(
            hours=password_reset_token_validation_time
        )

        # delete all tokens where created_at < now - 24 hours
        clear_expired(now_minus_expiry_time)

        # find a user by email address (case insensitive search)
        users = User.objects.filter(
            **{"{}__iexact".format(get_password_reset_lookup_field()): email}
        )

        active_user_found = False

        # iterate over all users and check if there is any user that is active
        # also check whether the password can be changed (is useable), as there could be users that are not allowed
        # to change their password (e.g., LDAP user)
        for user in users:
            if user.eligible_for_reset():
                active_user_found = True

        # No active user found, raise a validation error
        # but not if DJANGO_REST_PASSWORDRESET_NO_INFORMATION_LEAKAGE == True
        if not active_user_found and not getattr(
            settings, "DJANGO_REST_PASSWORDRESET_NO_INFORMATION_LEAKAGE", False
        ):
            raise exceptions.ValidationError(
                {
                    "email": [
                        (
                            "There is no active user associated with this e-mail address or the password can not be changed"
                        )
                    ]
                }
            )
        # last but not least: iterate over all users that are active and can change their password
        # and create a Reset Password Token and send a signal with the created token
        for user in users:
            if user.eligible_for_reset():
                # define the token as none for now
                token = None

                # check if the user already has a token
                if user.password_reset_tokens.all().count() > 0:
                    # yes, already has a token, re-use this token
                    token = user.password_reset_tokens.all()[0]
                else:
                    # no token exists, generate a new token
                    token = ResetPasswordToken.objects.create(
                        user=user,
                        user_agent=request.META.get(HTTP_USER_AGENT_HEADER, ""),
                        ip_address=request.META.get(HTTP_IP_ADDRESS_HEADER, ""),
                    )
                send_email = send_email_requirements_respected(token)
                if send_email:
                    # send a signal that the password token was created
                    # let whoever receives this signal handle sending the email for the password reset
                    reset_password_token_created.send(
                        sender=self.__class__,
                        instance=self,
                        reset_password_token=token,
                        current_language=request.data["current_language"],
                    )
                else:
                    return Response(
                        TextResources.lockedEmailSendFunctionality,
                        status=status.HTTP_400_BAD_REQUEST,
                    )
        # done
        return Response({"status": "OK"})


def send_email_requirements_respected(reset_password_token):
    # initializing password_reset_request_attempts
    password_reset_request_attempts = 0
    # getting user password reset tracking info (if it exists)
    existing_user_password_reset_tracking = UserPasswordResetTracking.objects.filter(
        user_id=User.objects.get(username=reset_password_token.user.username).id
    ).last()

    # if it exsits
    if existing_user_password_reset_tracking:
        # if the request attempts has reached the maximum amount
        if (
            existing_user_password_reset_tracking.request_attempts
            >= PasswordResetConfigurations.MAX_AMOUNT_OF_EMAIL_REQUESTS
        ):
            # if the lock out time is passed
            if datetime.now(
                pytz.utc
            ) > existing_user_password_reset_tracking.last_request_date + timedelta(
                seconds=PasswordResetConfigurations.LOCK_OUT_TIME
            ):
                # updating user password reset tracking data
                existing_user_password_reset_tracking.request_attempts = 1
                existing_user_password_reset_tracking.last_request_date = datetime.now()
                existing_user_password_reset_tracking.save()
                return True

        # the request attempts has not reached the maximum amount yet
        else:
            # updating user password reset tracking data
            existing_user_password_reset_tracking.request_attempts = (
                existing_user_password_reset_tracking.request_attempts + 1
            )
            existing_user_password_reset_tracking.last_request_date = datetime.now()
            existing_user_password_reset_tracking.save()
            return True

    else:
        # creating new row in user_password_reset_tracking table
        user_password_reset_tracking = UserPasswordResetTracking.objects.create(
            user_id=User.objects.get(username=reset_password_token.user.username).id,
            request_attempts=password_reset_request_attempts + 1,
            last_request_date=datetime.now(),
        )
        user_password_reset_tracking.save()
        return True

    return False


@receiver(reset_password_token_created)
def password_reset_token_created(
    sender, instance, reset_password_token, current_language, *args, **kwargs
):
    site_protocol = settings.SITE_PROTOCOL
    site_domain = settings.SITE_DOMAIN

    context = {
        "email": reset_password_token.user.email,
        "reset_password_url": "{}{}/oec-cat/reset-password?token={}".format(
            site_protocol, site_domain, reset_password_token.key
        ),
    }

    email_subject = "Outil d’évaluation des candidats : Réinitialisation du mot de passe / Candidate Assessment Tool : Password Reset"

    email_en_part = """
        Hello,
        \n
        You are receiving this message because a request was made to reset the password for account {email} in the Candidate Assessment Tool.
        \n
        To reset your password, please follow the instructions below:
        \n
        1. Click on the following link: {url}
        2. Choose your preferred language.
        3. Enter the new password twice.
        4. Click “Reset Password.”
        5. Click “OK.”
        6. Log in with the new password.
        \n
        If you received this in error, you can safely ignore it.
        \n
        Replies to this system-generated email will not be answered. If you experience technical difficulties, use the contact information provided in the test invitation email to get help.
        \n
        Thank you.
        The Candidate Assessment Tool Support Team
        """.format(
        email=context["email"],
        url=context["reset_password_url"],
    )

    email_fr_part = """
        Bonjour,
        \n
        Ce message donne suite à votre demande de réinitialisation du mot de passe pour le compte {email} dans l’Outil d’évaluation des candidats.
        \n
        Veuillez suivre les étapes suivantes pour réinitialiser votre mot de passe :
        \n
        1. Cliquez sur le lien {url}
        2. Cliquez sur la langue de votre choix.
        3. Inscrivez le nouveau mot de passe 2 fois.
        4. Cliquez sur « Réinitialiser le mot de passe ».
        5. Cliquez sur « OK ».
        6. Ouvrez une session à l’aide du nouveau mot de passe.
        \n
        Si vous avez reçu ce message par erreur, vous pouvez l’ignorer en toute sécurité.
        \n
        Les réponses à ce courriel généré par le système ne seront pas traitées. Si vous avez des difficultés techniques, utilisez les coordonnées fournies dans le courriel d’invitation au test pour obtenir de l’aide.
        \n
        Merci,
        L’équipe de soutien de l’Outil d’évaluation des candidats
        """.format(
        email=context["email"],
        url=context["reset_password_url"],
    )

    email_content = """
        (The English follows)
        \n
        {email_fr_part}
        \n
        \n
        {email_en_part}
        """.format(
        email_fr_part=email_fr_part, email_en_part=email_en_part
    )

    send_mail(
        # title:
        email_subject,
        # message:
        email_content,
        # from (will be overridden by the real email in Dev/Test/Prod):
        settings.EMAIL_HOST_USER,
        # to:
        [context["email"]],
    )


# called right after a successful password reset
@receiver(post_password_reset)
def post_password_reset_actions(user, *args, **kwargs):
    # getting user id
    user_id = User.objects.get(username=user).id

    # resetting request attempts in user password reset tracking table
    user_password_reset_tracking = UserPasswordResetTracking.objects.get(
        user_id=user_id
    )
    user_password_reset_tracking.request_attempts = 0
    user_password_reset_tracking.save()

    # updating last_password_change in user table
    user = User.objects.get(id=user_id)
    user.last_password_change = datetime.now()
    user.save()
