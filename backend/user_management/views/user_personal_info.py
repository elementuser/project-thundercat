import datetime
import urllib.parse
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django_rest_passwordreset.views import ResetPasswordToken
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import permissions, status
from cms.views.utils import get_needed_parameters, get_optional_parameters
from user_management.serializers.user_profile_serializer import (
    UserProfileSerializer,
    UserProfileChangeRequestSerializer,
)
from user_management.views.utils import get_user_info_from_jwt_token
from user_management.user_management_models.user_models import User
from user_management.user_management_models.user_profile_change_request import (
    UserProfileChangeRequest,
)
from backend.api_permissions.role_based_api_permissions import HasSystemAdminPermission


def update_user_personal_info(request):
    success, parameters = get_needed_parameters(["username"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

    optional_parameters = get_optional_parameters(
        ["secondaryEmail", "pri", "military_service_nbr", "psrsAppID"], request
    )
    # secondary email validation (useful only for manual entry in the DB, not for the Personal Info page, since there is frontend validation first)
    try:
        # if secondary_email string is not empty, validate it
        if optional_parameters["secondaryEmail"] != "":
            validate_email(optional_parameters["secondaryEmail"])
    except ValidationError:
        return Response(
            {"error": "secondary email is invalid"}, status=status.HTTP_400_BAD_REQUEST
        )
    # getting current user
    user_info = get_user_info_from_jwt_token(request)

    if user_info["username"] != parameters["username"]:
        return Response(
            {
                "error": "you do not have permission to access someone else's personal information."
            },
            status=status.HTTP_400_BAD_REQUEST,
        )
    current_user = User.objects.filter(username=parameters["username"])

    # updating user's personal info
    current_user.update(
        secondary_email=optional_parameters["secondaryEmail"],
        pri=optional_parameters["pri"],
        military_service_nbr=optional_parameters["military_service_nbr"],
        psrs_applicant_id=optional_parameters["psrsAppID"],
    )
    return Response(
        UserProfileSerializer(current_user[0], many=False).data,
        status=status.HTTP_200_OK,
    )


class UpdateUserPersonalInfo(APIView):
    def post(self, request):
        return update_user_personal_info(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


def update_last_password_change_time(request):
    user_info = get_user_info_from_jwt_token(request)
    # getting current user
    current_user = User.objects.filter(username=user_info["username"])
    # updating user's last password change time
    current_user.update(last_password_change=datetime.date.today())
    return status.HTTP_200_OK


def send_user_profile_change_request(request):
    success, parameters = get_needed_parameters(
        [
            "username",
            "current_first_name",
            "current_last_name",
            "current_birth_date",
            "new_first_name",
            "new_last_name",
            "new_birth_date",
            "comments",
        ],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

    # getting user_id
    user_id = User.objects.get(username=parameters["username"]).id

    # making sure that there is no existing request at the moment (if so, delete them)
    existing_profile_change_requests = UserProfileChangeRequest.objects.filter(
        user_id=user_id
    )
    if existing_profile_change_requests:
        for existing_profile_change_request in existing_profile_change_requests:
            existing_profile_change_request.delete()

    # checking if new DOB has been provided
    new_birth_date = None
    if parameters["new_birth_date"] != "null-null-null":
        # setting new_birth_date with provided parameter
        new_birth_date = parameters["new_birth_date"]

    # creating new user profile change request
    UserProfileChangeRequest.objects.create(
        user_id=user_id,
        current_first_name=parameters["current_first_name"],
        current_last_name=parameters["current_last_name"],
        current_birth_date=parameters["current_birth_date"],
        new_first_name=parameters["new_first_name"],
        new_last_name=parameters["new_last_name"],
        new_birth_date=new_birth_date,
        comments=parameters["comments"],
        reason_for_deny="",
    )

    return Response(status=status.HTTP_200_OK)


class SendUserProfileChangeRequest(APIView):
    def post(self, request):
        return send_user_profile_change_request(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


def get_user_profile_change_request(request):
    success, parameters = get_needed_parameters(
        [
            "username",
        ],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

    try:
        # getting user_id
        user_id = User.objects.get(username=parameters["username"]).id
    except User.DoesNotExist:
        return Response(
            {"error": "Provided username does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    # checking if there is an existing request
    existing_profile_change_requests = UserProfileChangeRequest.objects.filter(
        user_id=user_id
    )

    # existing profile change request
    if existing_profile_change_requests:
        serialized_data = UserProfileChangeRequestSerializer(
            existing_profile_change_requests.last(), many=False
        ).data
        return Response(serialized_data)

    # no profile change request
    else:
        return Response(
            {
                "info": "{0} has no existing user profile change request".format(
                    parameters["username"]
                )
            },
            status=status.HTTP_200_OK,
        )


class GetUserProfileChangeRequest(APIView):
    def get(self, request):
        return get_user_profile_change_request(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


def delete_user_profile_change_request(request):
    success, parameters = get_needed_parameters(
        [
            "username",
        ],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

    try:
        # getting user_id
        user_id = User.objects.get(username=parameters["username"]).id
    except User.DoesNotExist:
        return Response(
            {"error": "Provided username does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    # get existing request(s) and delete needed data
    existing_profile_change_requests = UserProfileChangeRequest.objects.filter(
        user_id=user_id
    )
    for existing_profile_change_request in existing_profile_change_requests:
        existing_profile_change_request.delete()

    return Response(status=status.HTTP_200_OK)


class DeleteUserProfileChangeRequest(APIView):
    def post(self, request):
        return delete_user_profile_change_request(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class UpdateUserLastPasswordChangeTime(APIView):
    def post(self, request):
        return Response(update_last_password_change_time(request))

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class ChangeUserPassword(APIView):
    def post(self, request):
        success, parameters = get_needed_parameters(
            ["new_password", "confirm_password", "username"], request
        )
        if not success:
            return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

        return change_password(parameters)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


def change_password(parameters):
    if parameters["new_password"] != parameters["confirm_password"]:
        return Response(
            {"error": "passwords don't match"}, status=status.HTTP_400_BAD_REQUEST
        )

    user = User.objects.filter(username=parameters["username"]).first()
    if not user:
        return Response(
            {"error": "user doesn't exist"}, status=status.HTTP_400_BAD_REQUEST
        )

    user.set_password(parameters["new_password"])
    user.save()
    return Response(status=status.HTTP_200_OK)


# check if a specified user account (active) exsits
def email_exists(parameters):
    # getting user
    # decoding email that might contain special characters
    user = User.objects.filter(
        email=urllib.parse.unquote(parameters["email"]), is_active=1
    )
    # if it does not exist
    if not user:
        # return false
        return Response(False, status=status.HTTP_200_OK)
    # if it exists
    else:
        # return true
        return Response(True, status=status.HTTP_200_OK)


class DoesThisEmailExist(APIView):
    def get(self, request):
        success, parameters = get_needed_parameters(["email"], request)
        if not success:
            return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

        return email_exists(parameters)

    def get_permissions(self):
        return [permissions.IsAuthenticatedOrReadOnly()]


# check if a specified user account has an active reset password session
def has_active_password_reset_session(parameters):
    # getting password reset row
    password_reset_rorw = ResetPasswordToken.objects.filter(
        key=parameters["password_reset_token"]
    )
    # if it does not exist
    if not password_reset_rorw:
        # return false
        return Response(False, status=status.HTTP_200_OK)
    # if it exists
    else:
        # return true
        return Response(True, status=status.HTTP_200_OK)


class GetSelectedUserPersonalInfo(APIView):
    def get(self, request):
        return get_selected_user_personal_info(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


def get_selected_user_personal_info(request):
    username = request.query_params.get("username", None)

    # username is defined
    if username is not None:
        # decoding username that might contain special characters
        username = urllib.parse.unquote(username)

    # getting user
    user = User.objects.filter(username=username)
    # returning data
    data = UserProfileSerializer(user, many=True)
    return Response(data.data)
