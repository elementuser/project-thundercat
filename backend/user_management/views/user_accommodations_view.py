from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import permissions
from cms.views.utils import get_user_info_from_jwt_token
from user_management.views.user_accommodations import (
    get_user_accommodations,
    save_user_accommodations,
)


class GetUserAccommodations(APIView):
    def get(self, request):
        user_info = get_user_info_from_jwt_token(request)
        return Response(get_user_accommodations(user_info))

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class SaveUserAccommodations(APIView):
    def post(self, request):
        user_info = get_user_info_from_jwt_token(request)

        return save_user_accommodations(user_info, request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]
