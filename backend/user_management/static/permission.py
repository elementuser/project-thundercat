# This file is a collection of the valid permissions for user permissions.

# ==================== ** IMPORTANT ** ====================
# if you update this file, you also need to update:
#   - Frontend file called Constants.jsx (...\project-thundercat\frontend\src\components\profile\Constants.jsx)
#   - Backend Unit Test file called util_permissions.py (...\project-thundercat\backend\tests\Utils\util_permissions.py)


# unique codename of respective permissions
class Permission:
    TEST_ADMINISTRATOR = "is_test_administrator"
    SYSTEM_ADMINISTRATOR = "is_etta"
    PPC_ADMINISTRATOR = "is_ppc"
    SCORER = "is_scorer"
    TEST_BUILDER = "is_tb"
    AAE = "is_aae"
    RD_OPERATIONS = "is_rd_operations"
    TEST_DEVELOPER = "is_td"
