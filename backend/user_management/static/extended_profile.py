# ==================== ** IMPORTANT ** ====================
# if you update this file, you also need to update the frontend file called Constants.jsx (...\project-thundercat\frontend\src\components\profile\Constants.jsx)


class ExtendedProfileEEInfo:
    YES = 1
    NO = 2
    PREFER_NOT_TO_SAY = 3


class VisibleMinorityId:
    OTHER = 22


class DisabilityId:
    OTHER = 6
