class PasswordResetConfigurations:
    MAX_AMOUNT_OF_EMAIL_REQUESTS = 3
    # time in seconds (5 minutes)
    LOCK_OUT_TIME = 300
