from rest_framework import serializers
from user_management.user_management_models.user_profile_change_request import (
    UserProfileChangeRequest,
)
from user_management.user_management_models.user_models import User


class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            "first_name",
            "last_name",
            "birth_date",
            "email",
            "secondary_email",
            "pri",
            "military_service_nbr",
            "last_password_change",
            "is_staff",
            "psrs_applicant_id",
            "last_login",
            "date_joined",
        ]


class UserProfileChangeRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfileChangeRequest
        fields = "__all__"
