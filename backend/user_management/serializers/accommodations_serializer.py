from rest_framework import serializers
from user_management.user_management_models.accommodations import Accommodations


class AccommodationsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Accommodations
        fields = ["font_size", "font_family", "spacing"]
