from rest_framework import serializers
from user_management.user_management_models.custom_permissions_model import (
    CustomPermissions,
)
from user_management.user_management_models.custom_user_permissions_model import (
    CustomUserPermissions,
)
from user_management.user_management_models.permission_request_model import (
    PermissionRequest,
)
from user_management.user_management_models.user_models import User


# Serializers define the API representation
class GetPermissionsSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomPermissions
        fields = "__all__"


class GetPendingPermissionsSerializer(serializers.ModelSerializer):
    codename = serializers.SerializerMethodField()
    en_name = serializers.SerializerMethodField()
    fr_name = serializers.SerializerMethodField()
    en_description = serializers.SerializerMethodField()
    fr_description = serializers.SerializerMethodField()
    first_name = serializers.SerializerMethodField()
    last_name = serializers.SerializerMethodField()
    email = serializers.SerializerMethodField()

    # getting custom permissions codename
    def get_codename(self, request):
        codename = CustomPermissions.objects.get(
            permission_id=request.permission_requested_id
        ).codename
        return codename

    # getting custom permissions en_name
    def get_en_name(self, request):
        en_name = CustomPermissions.objects.get(
            permission_id=request.permission_requested_id
        ).en_name
        return en_name

    # getting custom permissions fr_name
    def get_fr_name(self, request):
        fr_name = CustomPermissions.objects.get(
            permission_id=request.permission_requested_id
        ).fr_name
        return fr_name

    # getting custom permissions en_description
    def get_en_description(self, request):
        en_description = CustomPermissions.objects.get(
            permission_id=request.permission_requested_id
        ).en_description
        return en_description

    # getting custom permissions fr_description
    def get_fr_description(self, request):
        fr_description = CustomPermissions.objects.get(
            permission_id=request.permission_requested_id
        ).fr_description
        return fr_description

    # getting user first_name
    def get_first_name(self, request):
        first_name = User.objects.get(username=request.username_id).first_name
        return first_name

    # getting user last_name
    def get_last_name(self, request):
        last_name = User.objects.get(username=request.username_id).last_name
        return last_name

    # getting user email
    def get_email(self, request):
        email = User.objects.get(username=request.username_id).email
        return email

    class Meta:
        model = PermissionRequest
        fields = "__all__"


class GetActivePermissionsSerializer(serializers.ModelSerializer):
    codename = serializers.SerializerMethodField()
    en_name = serializers.SerializerMethodField()
    fr_name = serializers.SerializerMethodField()
    en_description = serializers.SerializerMethodField()
    fr_description = serializers.SerializerMethodField()
    first_name = serializers.SerializerMethodField()
    last_name = serializers.SerializerMethodField()
    email = serializers.SerializerMethodField()
    goc_email = serializers.SerializerMethodField()
    pri_or_military_nbr = serializers.SerializerMethodField()
    supervisor = serializers.SerializerMethodField()
    supervisor_email = serializers.SerializerMethodField()
    rationale = serializers.SerializerMethodField()

    # getting custom permissions codename
    def get_codename(self, request):
        codename = CustomPermissions.objects.get(
            permission_id=request.permission_id
        ).codename
        return codename

    # getting custom permissions en_name
    def get_en_name(self, request):
        en_name = CustomPermissions.objects.get(
            permission_id=request.permission_id
        ).en_name
        return en_name

    # getting custom permissions fr_name
    def get_fr_name(self, request):
        fr_name = CustomPermissions.objects.get(
            permission_id=request.permission_id
        ).fr_name
        return fr_name

    # getting custom permissions en_description
    def get_en_description(self, request):
        en_description = CustomPermissions.objects.get(
            permission_id=request.permission_id
        ).en_description
        return en_description

    # getting custom permissions fr_description
    def get_fr_description(self, request):
        fr_description = CustomPermissions.objects.get(
            permission_id=request.permission_id
        ).fr_description
        return fr_description

    # getting user first_name
    def get_first_name(self, request):
        first_name = User.objects.get(username=request.user_id).first_name
        return first_name

    # getting user last_name
    def get_last_name(self, request):
        last_name = User.objects.get(username=request.user_id).last_name
        return last_name

    # getting user email
    def get_email(self, request):
        email = User.objects.get(username=request.user_id).email
        return email

    # getting user goc_email
    def get_goc_email(self, request):
        goc_email = CustomUserPermissions.objects.get(
            user_permission_id=request.user_permission_id
        ).goc_email
        return goc_email

    # getting user pri_or_military_nbr
    def get_pri_or_military_nbr(self, request):
        pri_or_military_nbr = CustomUserPermissions.objects.get(
            user_permission_id=request.user_permission_id
        ).pri_or_military_nbr
        return pri_or_military_nbr

    # getting user supervisor
    def get_supervisor(self, request):
        supervisor = CustomUserPermissions.objects.get(
            user_permission_id=request.user_permission_id
        ).supervisor
        return supervisor

    # getting user supervisor_email
    def get_supervisor_email(self, request):
        supervisor_email = CustomUserPermissions.objects.get(
            user_permission_id=request.user_permission_id
        ).supervisor_email
        return supervisor_email

    # getting user rationale
    def get_rationale(self, request):
        rationale = CustomUserPermissions.objects.get(
            user_permission_id=request.user_permission_id
        ).rationale
        return rationale

    class Meta:
        model = CustomUserPermissions
        fields = "__all__"


class GetUsersBasedOnSpecifiedPermissionSerializer(serializers.ModelSerializer):
    first_name = serializers.SerializerMethodField()
    last_name = serializers.SerializerMethodField()
    email = serializers.SerializerMethodField()

    # getting user first_name
    def get_first_name(self, request):
        first_name = User.objects.get(username=request.user_id).first_name
        return first_name

    # getting user last_name
    def get_last_name(self, request):
        last_name = User.objects.get(username=request.user_id).last_name
        return last_name

    # getting user email
    def get_email(self, request):
        email = User.objects.get(username=request.user_id).email
        return email

    class Meta:
        model = CustomUserPermissions
        fields = "__all__"
