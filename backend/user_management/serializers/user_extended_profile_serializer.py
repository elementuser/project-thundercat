from rest_framework import serializers
from django.db.models import F
from user_management.user_management_models.user_extended_profile_aboriginal import (
    UserExtendedProfileAboriginal,
)
from user_management.user_management_models.user_extended_profile_visible_minority import (
    UserExtendedProfileVisibleMinority,
)
from user_management.user_management_models.user_extended_profile_disability import (
    UserExtendedProfileDisability,
)
from user_management.user_management_models.ta_extended_profile_models import (
    TaExtendedProfile,
)
from user_management.user_management_models.user_extended_profile import (
    UserExtendedProfile,
)
from backend.ref_table_views.cat_employer_sts_vw import CatRefEmployerStsVW
from backend.ref_table_views.cat_ref_departments_vw import CatRefDepartmentsVW
from backend.ref_table_views.cat_education import CatEducation
from backend.ref_table_views.cat_ref_classification_vw import CatRefClassificationVW
from backend.ref_table_views.cat_ref_gender_vw import CatRefGenderVW
from backend.ref_table_views.cat_ref_province_vw import CatRefProvinceVW
from backend.ref_table_views.cat_aboriginal_vw import CatRefAboriginalVW
from backend.ref_table_views.cat_visible_minority_vw import CatRefVisibleMinorityVW
from backend.ref_table_views.cat_disability_vw import CatRefDisabilityVW


def get_aboriginal_data(abrg_id):
    try:
        aboriginal_data = CatRefAboriginalVW.objects.get(abrg_id=abrg_id)
    except:
        aboriginal_data = {
            "abrg_id": 0,
            "edesc": "",
            "fdesc": "",
        }
    return aboriginal_data


class UserExtendedProfileAboriginalSerializer(serializers.ModelSerializer):
    abrg_id = serializers.SerializerMethodField()
    edesc = serializers.SerializerMethodField()
    fdesc = serializers.SerializerMethodField()

    def get_abrg_id(self, request):
        try:
            abrg_id = get_aboriginal_data(abrg_id=request.aboriginal_id).abrg_id
        except AttributeError:
            abrg_id = get_aboriginal_data(abrg_id=request.aboriginal_id)["abrg_id"]
        return abrg_id

    def get_edesc(self, request):
        try:
            edesc = get_aboriginal_data(abrg_id=request.aboriginal_id).edesc
        except AttributeError:
            edesc = get_aboriginal_data(abrg_id=request.aboriginal_id)["edesc"]
        return edesc

    def get_fdesc(self, request):
        try:
            fdesc = get_aboriginal_data(abrg_id=request.aboriginal_id).fdesc
        except AttributeError:
            fdesc = get_aboriginal_data(abrg_id=request.aboriginal_id)["fdesc"]
        return fdesc

    class Meta:
        model = CatRefAboriginalVW
        fields = ["abrg_id", "edesc", "fdesc"]


def get_visible_minority_data(vismin_id):
    try:
        visible_minority_data = CatRefVisibleMinorityVW.objects.get(vismin_id=vismin_id)
    except:
        visible_minority_data = {
            "vismin_id": 0,
            "edesc": "",
            "fdesc": "",
            "full_edesc": "",
            "full_fdesc": "",
            "other_specification": None,
        }
    return visible_minority_data


class UserExtendedProfileVisibleMinoritySerializer(serializers.ModelSerializer):
    vismin_id = serializers.SerializerMethodField()
    edesc = serializers.SerializerMethodField()
    fdesc = serializers.SerializerMethodField()
    full_edesc = serializers.SerializerMethodField()
    full_fdesc = serializers.SerializerMethodField()
    other_specification = serializers.SerializerMethodField()

    def get_vismin_id(self, request):
        try:
            vismin_id = get_visible_minority_data(request.visible_minority_id).vismin_id
        except AttributeError:
            vismin_id = get_visible_minority_data(request.visible_minority_id)[
                "vismin_id"
            ]
        return vismin_id

    def get_edesc(self, request):
        try:
            edesc = get_visible_minority_data(request.visible_minority_id).edesc
        except AttributeError:
            edesc = get_visible_minority_data(request.visible_minority_id)["edesc"]
        return edesc

    def get_fdesc(self, request):
        try:
            fdesc = get_visible_minority_data(request.visible_minority_id).fdesc
        except AttributeError:
            fdesc = get_visible_minority_data(request.visible_minority_id)["fdesc"]
        return fdesc

    def get_full_edesc(self, request):
        try:
            full_edesc = get_visible_minority_data(
                request.visible_minority_id
            ).full_edesc
        except AttributeError:
            full_edesc = get_visible_minority_data(request.visible_minority_id)[
                "full_edesc"
            ]
        return full_edesc

    def get_full_fdesc(self, request):
        try:
            full_fdesc = get_visible_minority_data(
                request.visible_minority_id
            ).full_fdesc
        except AttributeError:
            full_fdesc = get_visible_minority_data(request.visible_minority_id)[
                "full_fdesc"
            ]
        return full_fdesc

    def get_other_specification(self, request):
        other_specification = UserExtendedProfileVisibleMinority.objects.get(
            id=request.id,
        ).other_specification
        return other_specification

    class Meta:
        model = CatRefVisibleMinorityVW
        fields = [
            "vismin_id",
            "edesc",
            "fdesc",
            "full_edesc",
            "full_fdesc",
            "other_specification",
        ]


def get_disability_data(dsbl_id):
    try:
        disability_data = CatRefDisabilityVW.objects.get(dsbl_id=dsbl_id)
    except:
        disability_data = {
            "dsbl_id": 0,
            "edesc": "",
            "fdesc": "",
            "full_edesc": "",
            "full_fdesc": "",
            "other_specification": None,
        }
    return disability_data


class UserExtendedProfileDisabilitySerializer(serializers.ModelSerializer):
    dsbl_id = serializers.SerializerMethodField()
    edesc = serializers.SerializerMethodField()
    fdesc = serializers.SerializerMethodField()
    full_edesc = serializers.SerializerMethodField()
    full_fdesc = serializers.SerializerMethodField()
    other_specification = serializers.SerializerMethodField()

    def get_dsbl_id(self, request):
        try:
            dsbl_id = get_disability_data(request.disability_id).dsbl_id
        except AttributeError:
            dsbl_id = get_disability_data(request.disability_id)["dsbl_id"]
        return dsbl_id

    def get_edesc(self, request):
        try:
            edesc = get_disability_data(request.disability_id).edesc
        except AttributeError:
            edesc = get_disability_data(request.disability_id)["edesc"]
        return edesc

    def get_fdesc(self, request):
        try:
            fdesc = get_disability_data(request.disability_id).fdesc
        except AttributeError:
            fdesc = get_disability_data(request.disability_id)["fdesc"]
        return fdesc

    def get_full_edesc(self, request):
        try:
            full_edesc = get_disability_data(request.disability_id).full_edesc
        except AttributeError:
            full_edesc = get_disability_data(request.disability_id)["full_edesc"]
        return full_edesc

    def get_full_fdesc(self, request):
        try:
            full_fdesc = get_disability_data(request.disability_id).full_fdesc
        except AttributeError:
            full_fdesc = get_disability_data(request.disability_id)["full_fdesc"]
        return full_fdesc

    def get_other_specification(self, request):
        other_specification = UserExtendedProfileDisability.objects.get(
            id=request.id
        ).other_specification
        return other_specification

    class Meta:
        model = CatRefDisabilityVW
        fields = [
            "dsbl_id",
            "edesc",
            "fdesc",
            "full_edesc",
            "full_fdesc",
            "other_specification",
        ]


class UserExtendedProfileSerializer(serializers.ModelSerializer):
    aboriginal_data = serializers.SerializerMethodField()
    visible_minority_data = serializers.SerializerMethodField()
    disability_data = serializers.SerializerMethodField()

    def get_aboriginal_data(self, request):
        aboriginal_data = UserExtendedProfileAboriginal.objects.filter(
            user_extended_profile_id=request.id
        )

        serialized_data = UserExtendedProfileAboriginalSerializer(
            aboriginal_data, many=True
        ).data
        return serialized_data

    def get_visible_minority_data(self, request):
        visible_minority_data = UserExtendedProfileVisibleMinority.objects.filter(
            user_extended_profile_id=request.id
        )

        serialized_data = UserExtendedProfileVisibleMinoritySerializer(
            visible_minority_data, many=True
        ).data
        return serialized_data

    def get_disability_data(self, request):
        disability_data = UserExtendedProfileDisability.objects.filter(
            user_extended_profile_id=request.id
        )

        serialized_data = UserExtendedProfileDisabilitySerializer(
            disability_data, many=True
        ).data
        return serialized_data

    class Meta:
        model = UserExtendedProfile
        fields = "__all__"


class SelectedUserExtendedProfileSerializer(serializers.ModelSerializer):
    employer_en_name = serializers.SerializerMethodField()
    employer_fr_name = serializers.SerializerMethodField()
    organization_en_name = serializers.SerializerMethodField()
    organization_fr_name = serializers.SerializerMethodField()
    group = serializers.SerializerMethodField()
    sub_group = serializers.SerializerMethodField()
    level = serializers.SerializerMethodField()
    residence_en_name = serializers.SerializerMethodField()
    residence_fr_name = serializers.SerializerMethodField()
    education_en_name = serializers.SerializerMethodField()
    education_fr_name = serializers.SerializerMethodField()
    gender_en_name = serializers.SerializerMethodField()
    gender_fr_name = serializers.SerializerMethodField()

    # getting employer en name
    def get_employer_en_name(self, request):
        if request.current_employer_id:
            employer_en_name = CatRefEmployerStsVW.objects.get(
                empsts_id=request.current_employer_id, active_flg=1
            ).edesc
            return employer_en_name

    # getting employer en name
    def get_employer_fr_name(self, request):
        if request.current_employer_id:
            employer_fr_name = CatRefEmployerStsVW.objects.get(
                empsts_id=request.current_employer_id, active_flg=1
            ).fdesc
            return employer_fr_name

    # getting organization en name
    def get_organization_en_name(self, request):
        if request.organization_id:
            organization_en_name = CatRefDepartmentsVW.objects.get(
                dept_id=request.organization_id, active_flg=1
            ).edesc
            return organization_en_name

    # getting organization fr name
    def get_organization_fr_name(self, request):
        if request.organization_id:
            organization_fr_name = CatRefDepartmentsVW.objects.get(
                dept_id=request.organization_id, active_flg=1
            ).fdesc
            return organization_fr_name

    # getting group
    def get_group(self, request):
        if request.group_id:
            group = CatRefClassificationVW.objects.get(
                classif_id=request.group_id, active_flg=1
            ).class_grp_cd
            return group

    # getting sub-group
    def get_sub_group(self, request):
        if request.group_id:
            sub_group = CatRefClassificationVW.objects.get(
                classif_id=request.group_id, active_flg=1
            ).class_sbgrp_cd
            return sub_group

    # getting level
    def get_level(self, request):
        if request.level_id:
            level = CatRefClassificationVW.objects.get(
                classif_id=request.level_id, active_flg=1
            ).class_lvl_cd
            return level

    # getting residence en name
    def get_residence_en_name(self, request):
        if request.residence_id:
            residence_en_name = CatRefProvinceVW.objects.get(
                prov_id=request.residence_id, active_flg=1
            ).edesc
            return residence_en_name

    # getting residence fr name
    def get_residence_fr_name(self, request):
        if request.residence_id:
            residence_fr_name = CatRefProvinceVW.objects.get(
                prov_id=request.residence_id, active_flg=1
            ).fdesc
            return residence_fr_name

    # getting education en name
    def get_education_en_name(self, request):
        if request.education_id:
            education_en_name = CatEducation.objects.get(
                education_id=request.education_id, active_flg=1
            ).edesc
            return education_en_name

    # getting education fr name
    def get_education_fr_name(self, request):
        if request.education_id:
            education_fr_name = CatEducation.objects.get(
                education_id=request.education_id, active_flg=1
            ).fdesc
            return education_fr_name

    # getting gender en name
    def get_gender_en_name(self, request):
        if request.gender_id:
            gender_en_name = CatRefGenderVW.objects.get(
                gnd_id=request.gender_id, active_flg=1
            ).edesc
            return gender_en_name

    # getting gender fr name
    def get_gender_fr_name(self, request):
        if request.gender_id:
            gender_fr_name = CatRefGenderVW.objects.get(
                gnd_id=request.gender_id, active_flg=1
            ).fdesc
            return gender_fr_name

    class Meta:
        model = UserExtendedProfile
        fields = "__all__"


class TaExtendedProfileSerializer(serializers.ModelSerializer):
    department_abrv_en = serializers.SerializerMethodField()
    department_abrv_fr = serializers.SerializerMethodField()
    department_name_en = serializers.SerializerMethodField()
    department_name_fr = serializers.SerializerMethodField()

    # getting en abrv
    def get_department_abrv_en(self, request):
        department_abrv_en = CatRefDepartmentsVW.objects.get(
            dept_id=request.department_id
        ).eabrv
        return department_abrv_en

    # getting fr abrv
    def get_department_abrv_fr(self, request):
        department_abrv_fr = CatRefDepartmentsVW.objects.get(
            dept_id=request.department_id
        ).fabrv
        return department_abrv_fr

    # getting en desc
    def get_department_name_en(self, request):
        department_name_en = CatRefDepartmentsVW.objects.get(
            dept_id=request.department_id
        ).edesc
        return department_name_en

    # getting fr desc
    def get_department_name_fr(self, request):
        department_name_fr = CatRefDepartmentsVW.objects.get(
            dept_id=request.department_id
        ).edesc
        return department_name_fr

    class Meta:
        model = TaExtendedProfile
        fields = "__all__"
