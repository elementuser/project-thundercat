from django.db import models
from simple_history.models import HistoricalRecords
from user_management.user_management_models.user_models import User
from user_management.user_management_models.custom_permissions_model import (
    CustomPermissions,
)


##################################################################################
# CUSTOM PERMISSIONS MODEL
##################################################################################


class PermissionRequest(models.Model):
    permission_request_id = models.AutoField(primary_key=True)
    goc_email = models.CharField(max_length=254, blank=False, null=False)
    pri_or_military_nbr = models.CharField(max_length=10, null=False, blank=False)
    supervisor = models.CharField(max_length=180, blank=False, null=False)
    supervisor_email = models.CharField(max_length=254, blank=False, null=False)
    rationale = models.CharField(max_length=300, blank=False, null=False)
    permission_requested = models.ForeignKey(
        CustomPermissions,
        to_field="permission_id",
        on_delete=models.DO_NOTHING,
        null=False,
    )
    request_date = models.DateTimeField(auto_now_add=True, blank=False, null=False)
    username = models.ForeignKey(
        User, to_field="username", on_delete=models.DO_NOTHING, null=False
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical model
    history = HistoricalRecords()

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Permission Request"
        constraints = [
            models.UniqueConstraint(
                name="one_username_per_permission_requested_instance",
                fields=["username", "permission_requested"],
            )
        ]
