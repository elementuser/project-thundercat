from django.db import models
from simple_history.models import HistoricalRecords
from user_management.user_management_models.user_extended_profile import (
    UserExtendedProfile,
)


# ** IMPORTANT **
# If any changes are made to this model, please make sure to notify CAMM team (CAT/CAMM Relation Logic)
class UserExtendedProfileVisibleMinority(models.Model):
    other_specification = models.CharField(max_length=200, null=True, blank=True)
    visible_minority_id = models.IntegerField()
    user_extended_profile = models.ForeignKey(
        UserExtendedProfile, on_delete=models.DO_NOTHING
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords(use_base_model_db=False)

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "User Extended Profile (Visible Minority)"
