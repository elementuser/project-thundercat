from django.db import models
from simple_history.models import HistoricalRecords
from user_management.user_management_models.user_models import User


class UserPasswordResetTracking(models.Model):
    request_attempts = models.IntegerField(default=0)
    last_request_date = models.DateTimeField(auto_now_add=True, blank=False)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "{0}".format(self.user.username)
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "User Password Reset Tracking"
