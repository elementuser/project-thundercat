from django.contrib import admin
from user_management.user_management_models.custom_permissions_model import (
    CustomPermissions,
)
from user_management.user_management_models.custom_user_permissions_model import (
    CustomUserPermissions,
)
from user_management.user_management_models.user_extended_profile import (
    UserExtendedProfile,
)
from user_management.user_management_models.accommodations import Accommodations

admin.site.register(CustomPermissions)
admin.site.register(CustomUserPermissions)
admin.site.register(Accommodations)
admin.site.register(UserExtendedProfile)
