from django.contrib.auth.models import UserManager, AbstractUser
from django.db import models
from django.db.models import Q
from simple_history.models import HistoricalRecords

##################################################################################
# USER MODELS
##################################################################################

# number of login attempts allowed
NUM_OF_ATTEMPTS = 5


class LOCK_OUT_TIME:
    hours = 4
    minutes = 0


# customize user manager by allowing the user to log in by email or username


class CustomUserManager(UserManager):
    def get_by_natural_key(self, username):
        return self.get(
            Q(**{self.model.USERNAME_FIELD: username})
            | Q(**{self.model.EMAIL_FIELD: username})
        )


# ** IMPORTANT **
# If any changes are made to this model, please make sure to notify CAMM team (CAT/CAMM Relation Logic)
class User(AbstractUser):
    objects = CustomUserManager()
    secondary_email = models.EmailField(max_length=254, blank=True, null=True)
    first_name = models.CharField(max_length=30, null=False, blank=False)
    last_name = models.CharField(max_length=150, null=False, blank=False)
    birth_date = models.DateField(blank=False, null=False)
    pri = models.CharField(max_length=10, null=True, blank=True)
    military_service_nbr = models.CharField(max_length=7, null=True, blank=True)
    last_password_change = models.DateField(blank=True, null=True)
    psrs_applicant_id = models.CharField(max_length=8, null=True, blank=True)
    last_login = models.DateTimeField(blank=True, null=True)
    last_login_attempt = models.DateTimeField(blank=True, null=True)
    login_attempts = models.IntegerField(default=0)
    locked = models.BooleanField(default=False)
    locked_until = models.DateTimeField(blank=True, null=True)
    is_profile_complete = models.BooleanField(default=False)
    last_profile_update = models.DateTimeField(blank=True, null=True)
    REQUIRED_FIELDS = [
        "first_name",
        "last_name",
        "birth_date",
        "email",
        "secondary_email",
        "pri",
        "military_service_nbr",
        "last_password_change",
        "is_staff",
        "psrs_applicant_id",
        "last_login",
        "is_profile_complete",
    ]

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    class Meta:
        # Adding indexes to make search faster
        indexes = [
            models.Index(fields=["first_name", "last_name", "email", "birth_date"])
        ]
