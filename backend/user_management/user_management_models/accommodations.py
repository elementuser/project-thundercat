from django.db import models
from simple_history.models import HistoricalRecords
from user_management.user_management_models.user_models import User


class Accommodations(models.Model):
    user = models.OneToOneField(User, primary_key=True, on_delete=models.CASCADE)
    font_family = models.CharField(
        default="Nunito Sans", max_length=75, blank=True, null=True
    )
    font_size = models.CharField(default="16px", max_length=10, blank=True, null=True)
    spacing = models.BooleanField(default="False")

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "{0}".format(self.user.username)
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Accommodations"
