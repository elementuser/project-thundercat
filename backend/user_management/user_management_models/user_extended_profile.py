from django.db import models
from simple_history.models import HistoricalRecords
from user_management.user_management_models.user_models import User
from user_management.static.extended_profile import ExtendedProfileEEInfo
from user_management.user_management_models.ee_info_options import EEInfoOptions


# ** IMPORTANT **
# If any changes are made to this model, please make sure to notify CAMM team (CAT/CAMM Relation Logic)
class UserExtendedProfile(models.Model):
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    current_employer_id = models.IntegerField()
    organization_id = models.IntegerField()
    group_id = models.IntegerField()
    subgroup_id = models.IntegerField(default=0)
    level_id = models.IntegerField()
    residence_id = models.IntegerField()
    education_id = models.IntegerField()
    gender_id = models.IntegerField()
    identify_as_woman = models.ForeignKey(
        EEInfoOptions,
        to_field="opt_id",
        related_name="identify_as_woman",
        on_delete=models.DO_NOTHING,
        default=ExtendedProfileEEInfo.PREFER_NOT_TO_SAY,
    )
    aboriginal = models.ForeignKey(
        EEInfoOptions,
        to_field="opt_id",
        related_name="aboriginal",
        on_delete=models.DO_NOTHING,
        default=ExtendedProfileEEInfo.PREFER_NOT_TO_SAY,
    )
    visible_minority = models.ForeignKey(
        EEInfoOptions,
        to_field="opt_id",
        related_name="visible_minority",
        on_delete=models.DO_NOTHING,
        default=ExtendedProfileEEInfo.PREFER_NOT_TO_SAY,
    )
    disability = models.ForeignKey(
        EEInfoOptions,
        to_field="opt_id",
        related_name="disability",
        on_delete=models.DO_NOTHING,
        default=ExtendedProfileEEInfo.PREFER_NOT_TO_SAY,
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "({0}) current_employer_id: {1}, organization_id: {2}, group_id: {3}, subgroup_id: {4}, level_id: {5}, residence_id: {6}, education_id: {7}, gender_id: {8}".format(
            self.user.id,
            self.current_employer_id,
            self.organization_id,
            self.group_id,
            self.subgroup_id,
            self.level_id,
            self.residence_id,
            self.education_id,
            self.gender_id,
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "User Extended Profile"
