# Generated by Django 3.2.13 on 2022-07-07 15:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user_management_models', '0040_auto_20220706_1455'),
    ]

    operations = [
        migrations.AddField(
            model_name='historicaluserextendedprofile',
            name='aboriginal',
            field=models.CharField(default='PREFER NOT TO SAY', max_length=25),
        ),
        migrations.AddField(
            model_name='historicaluserextendedprofile',
            name='disability',
            field=models.CharField(default='PREFER NOT TO SAY', max_length=25),
        ),
        migrations.AddField(
            model_name='historicaluserextendedprofile',
            name='identify_as_woman',
            field=models.CharField(default='PREFER NOT TO SAY', max_length=25),
        ),
        migrations.AddField(
            model_name='historicaluserextendedprofile',
            name='visible_minority',
            field=models.CharField(default='PREFER NOT TO SAY', max_length=25),
        ),
        migrations.AddField(
            model_name='userextendedprofile',
            name='aboriginal',
            field=models.CharField(default='PREFER NOT TO SAY', max_length=25),
        ),
        migrations.AddField(
            model_name='userextendedprofile',
            name='disability',
            field=models.CharField(default='PREFER NOT TO SAY', max_length=25),
        ),
        migrations.AddField(
            model_name='userextendedprofile',
            name='identify_as_woman',
            field=models.CharField(default='PREFER NOT TO SAY', max_length=25),
        ),
        migrations.AddField(
            model_name='userextendedprofile',
            name='visible_minority',
            field=models.CharField(default='PREFER NOT TO SAY', max_length=25),
        ),
    ]
