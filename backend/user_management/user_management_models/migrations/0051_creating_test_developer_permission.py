# Generated by Django 2.1.7 on 20190530 14:07
# Edited by Francis Normand to create AAE permission

from django.db import migrations


def create_test_developer_permissions(apps, schema_editor):
    # get models
    custom_permissions = apps.get_model("user_management_models", "CustomPermissions")
    content_type = apps.get_model("contenttypes", "ContentType")
    # get db alias
    db_alias = schema_editor.connection.alias

    try:
        # get custom permissions content type model
        custom_permissions_content_type_id = (
            content_type.objects.using(db_alias)
            .filter(model="custompermissions")
            .last()
            .id
        )

        # creating TA permission
        accommodations_specialized_permission = custom_permissions(
            en_name="Test Developer",
            fr_name="Développeur de test",
            en_description="Responsible for managing test content, test assembly, data extractions, and statistical analyses.",
            fr_description="Responsable de la gestion du contenu de test, des règles d’assemblage des tests, des extractions de données et des analyses statistiques.",
            codename="is_td",
            content_type_id=custom_permissions_content_type_id,
        )
        accommodations_specialized_permission.save()

    # should only happen while testing (backend tests)
    except Exception as e:
        print("Exception (should only be displayed while testing): ", e)


def rollback_changes(apps, schema_editor):
    # get models
    custom_permissions = apps.get_model("user_management_models", "CustomPermissions")
    # get db alias
    db_alias = schema_editor.connection.alias

    # get TA permission object
    accommodations_specialized_permission = (
        custom_permissions.objects.using(db_alias).filter(codename="is_td").last()
    )
    if accommodations_specialized_permission:
        accommodations_specialized_permission.delete()


class Migration(migrations.Migration):

    dependencies = [
        (
            "user_management_models",
            "0050_update_permission_name_and_description_of_test_builder",
        )
    ]

    operations = [
        migrations.RunPython(create_test_developer_permissions, rollback_changes)
    ]
