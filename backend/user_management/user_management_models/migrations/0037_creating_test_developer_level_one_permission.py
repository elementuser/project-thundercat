# Generated by Django 2.1.7 on 20190530 14:07

from django.db import migrations


def create_test_developer_level_1_permissions(apps, schema_editor):
    # get models
    custom_permissions = apps.get_model("user_management_models", "CustomPermissions")
    content_type = apps.get_model("contenttypes", "ContentType")
    # get db alias
    db_alias = schema_editor.connection.alias

    try:
        # get custom permissions content type model
        custom_permissions_content_type_id = (
            content_type.objects.using(db_alias)
            .filter(model="custompermissions")
            .last()
            .id
        )

        # creating Test Develop Level 1 permission
        td_1_permission = custom_permissions(
            en_name="Test Developer Level 1",
            fr_name="FR Test Developer Level 1",
            en_description="EN Description Here",
            fr_description="FR Description Here",
            codename="is_td_1",
            content_type_id=custom_permissions_content_type_id,
        )
        td_1_permission.save()

    # should only happen while testing (backend tests)
    except Exception as e:
        print("Exception (should only be displayed while testing): ", e)


def rollback_changes(apps, schema_editor):
    # get models
    custom_permissions = apps.get_model("user_management_models", "CustomPermissions")
    # get db alias
    db_alias = schema_editor.connection.alias

    # get test developer level 1 (td_1) permission object
    td_1_permission = (
        custom_permissions.objects.using(db_alias).filter(codename="is_td_1").last()
    )
    if td_1_permission:
        td_1_permission.delete()


class Migration(migrations.Migration):

    dependencies = [("user_management_models", "0036_auto_20211201_1331")]

    operations = [migrations.RunPython(create_test_developer_level_1_permissions, rollback_changes)]
