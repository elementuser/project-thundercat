from user_management.user_management_models.custom_permissions_model import (
    CustomPermissions,
)
from user_management.user_management_models.user_models import User
from user_management.user_management_models.user_extended_profile import (
    UserExtendedProfile,
)
from user_management.user_management_models.user_extended_profile_aboriginal import (
    UserExtendedProfileAboriginal,
)
from user_management.user_management_models.user_extended_profile_visible_minority import (
    UserExtendedProfileVisibleMinority,
)
from user_management.user_management_models.user_extended_profile_disability import (
    UserExtendedProfileDisability,
)
from user_management.user_management_models.permission_request_model import (
    PermissionRequest,
)
from user_management.user_management_models.accommodations import Accommodations
from user_management.views.password_reset import (
    password_reset_token_created,
    post_password_reset_actions,
)
from user_management.user_management_models.user_password_reset_tracking_model import (
    UserPasswordResetTracking,
)
from user_management.user_management_models.ta_extended_profile_models import (
    TaExtendedProfile,
)
from user_management.user_management_models.ee_info_options import (
    EEInfoOptions,
)
from user_management.user_management_models.user_profile_change_request import (
    UserProfileChangeRequest,
)

# These imports help to auto discover the models
