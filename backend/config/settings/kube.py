from .base import *


DEBUG = False

DISCOVERY_URL = os.environ.get("DISCOVERY_URL", None)
if not DISCOVERY_URL:
    print('Environment variable "DISCOVERY_URL" is invalid or unprovided\n')

OAUTH_PROVIDER_USERNAME = os.environ.get("OAUTH_PROVIDER_USERNAME", None)
if not OAUTH_PROVIDER_USERNAME:
    print('Environment variable "OAUTH_PROVIDER_USERNAME" is invalid or unprovided\n')

OAUTH_PROVIDER_PASSWORD = os.environ.get("OAUTH_PROVIDER_PASSWORD", None)
if not OAUTH_PROVIDER_PASSWORD:
    print('Environment variable "OAUTH_PROVIDER_PASSWORD" is invalid or unprovided\n')

DATABASES = {
    "default": {
        "ENGINE": "mssql",
        "NAME": os.environ.get("DATABASE_NAME", ""),
        "USER": os.environ.get("DATABASE_USER", ""),
        "PASSWORD": os.environ.get("DATABASE_PASSWORD", ""),
        "HOST": os.environ.get("DATABASE_HOST", ""),
        "PORT": os.environ.get("DATABASE_PORT", ""),
        "OPTIONS": {"driver": "ODBC Driver 17 for SQL Server"},  # odbc driver installed
    }
}

STATIC_URL = "/static_backend/"
STATIC_ROOT = os.path.join(BASE_DIR, "static")

ALLOWED_HOSTS = ["*"]

MEDIA_URL = "/media/"
MEDIA_ROOT = os.path.join(BASE_DIR, "media/")
