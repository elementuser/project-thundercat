from .base import *
import json
from datetime import timedelta
from celery.schedules import crontab
from backend.celery import tasks

file = os.environ.get("DATABASE_FILE", None)
if not file:
    print('Environment variable "DATABASE_FILE" is invalid or unprovided\n')

# Opening JSON file
f = open(file)

# returns JSON object as
# a dictionary
file_data = json.load(f)

# Closing file
f.close()

SECRET_KEY = file_data.get("SECRET_KEY", None)
if not SECRET_KEY:
    print('Environment variable "SECRET_KEY" is invalid or unprovided\n')

DISCOVERY_URL = file_data.get("DISCOVERY_URL", None)
if not DISCOVERY_URL:
    print('Environment variable "DISCOVERY_URL" is invalid or unprovided\n')

OAUTH_PROVIDER_USERNAME = file_data.get("OAUTH_PROVIDER_USERNAME", None)
if not OAUTH_PROVIDER_USERNAME:
    print('Environment variable "OAUTH_PROVIDER_USERNAME" is invalid or unprovided\n')

OAUTH_PROVIDER_PASSWORD = file_data.get("OAUTH_PROVIDER_PASSWORD", None)
if not OAUTH_PROVIDER_PASSWORD:
    print('Environment variable "OAUTH_PROVIDER_PASSWORD" is invalid or unprovided\n')

REDIS_HOST = file_data.get("REDIS_HOST", None)
if not REDIS_HOST:
    print('Environment variable "REDIS_HOST" is invalid or unprovided\n')

REDIS_PORT = file_data.get("REDIS_PORT", None)
if not REDIS_PORT:
    print('Environment variable "REDIS_PORT" is invalid or unprovided\n')

REDIS_DB = file_data.get("REDIS_DB", None)
if not REDIS_DB:
    print('Environment variable "REDIS_DB" is invalid or unprovided\n')

DEBUG_FLAG = file_data.get("DEBUG_FLAG", None)
if not DEBUG_FLAG:
    DEBUG = False
else:
    if DEBUG_FLAG == "True":
        DEBUG = True
    else:
        DEBUG = False

# celery
CELERY_BROKER_URL = "redis://localhost:6379/0"
CELERY_RESULT_BACKEND = "redis://localhost:6379/0"
CELERY_ACCEPT_CONTENT = ["json"]
CELERY_TASK_SERIALIZER = "json"
CELERY_TIMEZONE = "UTC"

DATABASES = {
    "default": {
        "ENGINE": "mssql",
        "NAME": file_data.get("DATABASE_NAME", ""),
        "USER": file_data.get("DATABASE_USER", ""),
        "PASSWORD": file_data.get("DATABASE_PASSWORD", ""),
        "HOST": file_data.get("DATABASE_HOST", ""),
        "PORT": file_data.get("DATABASE_PORT", ""),
        # odbc driver installed
        "OPTIONS": {"driver": "ODBC Driver 17 for SQL Server"},
    }
}

STATIC_URL = "/static_backend/"
STATIC_ROOT = os.path.join(BASE_DIR, "static")

ALLOWED_HOSTS = ["*"]

MEDIA_URL = "/media/"
MEDIA_ROOT = os.path.join(BASE_DIR, "media/")

# specific channel layer configuration for legacy
# LEGACY_SETTINGS
CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "channels_redis.core.RedisChannelLayer",
        "CONFIG": {"hosts": [("127.0.0.1", 6379)]},
    }
}

# TODO (fnormand): setup this setting once django v3.1 and higher will be deployed
# validity period (in seconds)
# PASSWORD_RESET_TIMEOUT = 5
# time in hours
DJANGO_REST_MULTITOKENAUTH_RESET_TOKEN_EXPIRY_TIME = 1

EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
EMAIL_HOST = file_data.get("EMAIL_HOST", None)
if not EMAIL_HOST:
    print('Environment variable "EMAIL_HOST" is invalid or unprovided\n')
EMAIL_PORT = file_data.get("EMAIL_PORT", None)
if not EMAIL_PORT:
    print('Environment variable "EMAIL_PORT" is invalid or unprovided\n')
EMAIL_HOST_USER = file_data.get("EMAIL_HOST_USER", None)
if not EMAIL_HOST_USER:
    print('Environment variable "EMAIL_HOST_USER" is invalid or unprovided\n')
EMAIL_HOST_PASSWORD = file_data.get("EMAIL_HOST_PASSWORD", None)
if not EMAIL_HOST_PASSWORD:
    EMAIL_HOST_PASSWORD = ""
EMAIL_USE_TLS = True
EMAIL_USE_SSL = False

# useful for reset password email link
SITE_PROTOCOL = file_data.get("SITE_PROTOCOL", None)
if not SITE_PROTOCOL:
    print('Environment variable "SITE_PROTOCOL" is invalid or unprovided\n')
SITE_DOMAIN = file_data.get("SITE_DOMAIN", None)
if not SITE_DOMAIN:
    print('Environment variable "SITE_DOMAIN" is invalid or unprovided\n')

CELERY_BEAT_SCHEDULE = {
    # ========== ASSIGNED TESTS TASKS ==========
    # running this one every night at 11PM (3:00AM UTC)
    "run_every_night_at_11": {
        "task": "backend.celery.tasks.run_every_night_at_11",
        # UTC time (3:00AM)
        "schedule": crontab(hour=3, minute=0),
    },
    # running this one every night at 11:15PM (3:15AM UTC)
    "run_every_night_at_11_15": {
        "task": "backend.celery.tasks.run_every_night_at_11_15",
        # UTC time (3:15AM)
        "schedule": crontab(hour=3, minute=15),
    },
    # running this one every 60 minutes
    "run_every_60_minutes": {
        "task": "backend.celery.tasks.run_every_60_minutes",
        "schedule": timedelta(hours=1),
    },
    # running this one every 15 minutes
    "run_every_15_minutes": {
        "task": "backend.celery.tasks.run_every_15_minutes",
        "schedule": timedelta(minutes=15),
    },
    # ========== ASSIGNED TESTS TASKS (END) ==========
    # ========== TEST PERMISSIONS TASKS ==========
    # running this one every morning at 1:00AM (5:00AM UTC)
    "run_every_morning_at_1": {
        "task": "backend.celery.tasks.run_every_morning_at_1",
        # UTC time (5:00AM)
        "schedule": crontab(hour=5, minute=0),
    },
    # ========== TEST PERMISSIONS TASKS (END) ==========
    # ========== UIT TASKS ==========
    # running this one every morning at 00:01AM (4:01AM UTC)
    "run_every_morning_at_00_01": {
        "task": "backend.celery.tasks.run_every_morning_at_00_01",
        # UTC time (4:01AM)
        "schedule": crontab(hour=4, minute=1),
    },
    # ========== UIT TASKS (END) ==========
    # ========== USER UPDATE TASKS ==========
    # running this one every morning at 00:30AM (4:30AM UTC)
    "run_every_morning_at_00_30": {
        "task": "backend.celery.tasks.run_every_morning_at_00_30",
        # UTC time (4:30AM)
        "schedule": crontab(hour=4, minute=30),
    },
    # ========== USER UPDATE TASKS (END) ==========
    # ========== DEPROVISION TASKS ==========
    # running this one every night at 11:30PM (3:30AM UTC)
    "run_every_evening_at_11_30": {
        "task": "backend.celery.tasks.run_every_evening_at_11_30",
        # UTC time (3:30AM)
        "schedule": crontab(hour=3, minute=30),
    },
    # running this one every morning at 00:45AM (4:45AM UTC)
    "run_every_morning_at_00_45": {
        "task": "backend.celery.tasks.run_every_morning_at_00_45",
        # UTC time (4:45AM)
        "schedule": crontab(hour=4, minute=45),
    },
    # ========== DEPROVISION TASKS (END) ==========
}
