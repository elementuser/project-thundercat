from .base import *
import os
from datetime import timedelta
from backend.celery import tasks

DEBUG = True

# Database
# https://docs.djangoproject.com/en/2.1/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "mssql",
        "NAME": "master",
        "USER": "SA",
        "PASSWORD": "someSecurePassword10!",
        "HOST": "db",  # set in docker-compose.yml
        "PORT": 1433,  # ms sql port,
        # odbc driver installed
        "OPTIONS": {"driver": "ODBC Driver 17 for SQL Server"},
    }
}

# INSTALLED_APPS.append("debug_toolbar")

# MIDDLEWARE.append("debug_toolbar.middleware.DebugToolbarMiddleware")

STATIC_URL = "/static/"
STATIC_ROOT = os.path.join(BASE_DIR, "static/")

MEDIA_URL = "/media/"
MEDIA_ROOT = os.path.join(BASE_DIR, "media/")
IS_LOCAL = True

SECRET_KEY = "9t7j9n-@yf!8(xtohmf3k85f&qdateph(j7-_0pn-8iwu$l)xj"

# celery
CELERY_BROKER_URL = "redis://project-thundercat-redis-1:6379/0"
CELERY_RESULT_BACKEND = "redis://project-thundercat-redis-1:6379/0"
CELERY_ACCEPT_CONTENT = ["json"]
CELERY_TASK_SERIALIZER = "json"
CELERY_TIMEZONE = "UTC"

# Redis
REDIS_HOST = "redis"
REDIS_PORT = 6379
REDIS_DB = 0

# TODO (fnormand): setup this setting once django v3.1 and higher will be deployed
# validity period (in seconds)
# PASSWORD_RESET_TIMEOUT = 5
# time in hours
DJANGO_REST_MULTITOKENAUTH_RESET_TOKEN_EXPIRY_TIME = 1

# testing locally with a real email (gmail)
# EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
# EMAIL_HOST = "smtp.gmail.com"
# EMAIL_PORT = "587"
# EMAIL_HOST_USER = "<your_gmail>"
# EMAIL_HOST_PASSWORD = "<your_gmail_password>"
# EMAIL_USE_TLS = True
# EMAIL_USE_SSL = False

# console email (comment if testing with code above)
EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"
EMAIL_HOST_USER = "noreply@somehost.local"

SITE_PROTOCOL = "http://"
SITE_DOMAIN = "localhost:81"

CELERY_BEAT_SCHEDULE = {
    # ========== ASSIGNED TESTS TASKS ==========
    # running this one every minute locally for testing purposes
    "run_every_night_at_11": {
        "task": "backend.celery.tasks.run_every_night_at_11",
        "schedule": timedelta(minutes=1),
    },
    # running this one every minute locally for testing purposes
    "run_every_night_at_11_15": {
        "task": "backend.celery.tasks.run_every_night_at_11_15",
        "schedule": timedelta(minutes=1),
    },
    # running this one every minute locally for testing purposes
    "run_every_60_minutes": {
        "task": "backend.celery.tasks.run_every_60_minutes",
        "schedule": timedelta(minutes=1),
    },
    # running this one every minute locally for testing purposes
    "run_every_15_minutes": {
        "task": "backend.celery.tasks.run_every_15_minutes",
        "schedule": timedelta(minutes=1),
    },
    # ========== ASSIGNED TESTS TASKS (END) ==========
    # ========== TEST PERMISSIONS TASKS ==========
    # running this one every minute locally for testing purposes
    "run_every_morning_at_1": {
        "task": "backend.celery.tasks.run_every_morning_at_1",
        "schedule": timedelta(minutes=1),
    },
    # ========== TEST PERMISSIONS TASKS (END) ==========
    # ========== UIT TASKS ==========
    # running this one every minute locally for testing purposes
    "run_every_morning_at_00_01": {
        "task": "backend.celery.tasks.run_every_morning_at_00_01",
        "schedule": timedelta(minutes=1),
    },
    # ========== UIT TASKS (END) ==========
    # ========== USER UPDATE TASKS ==========
    "run_every_morning_at_00_30": {
        "task": "backend.celery.tasks.run_every_morning_at_00_30",
        "schedule": timedelta(minutes=1),
    },
    # ========== USER UPDATE TASKS (END) ==========
    # ========== DEPROVISION TASKS ==========
    # running this one every minute locally for testing purposes
    "run_every_evening_at_11_30": {
        "task": "backend.celery.tasks.run_every_evening_at_11_30",
        "schedule": timedelta(minutes=1),
    },
    #
    # Commented so it doesn't affect accounts that we don't use often locally
    #
    # "run_every_morning_at_00_45": {
    #     "task": "backend.celery.tasks.run_every_morning_at_00_45",
    #     "schedule": timedelta(minutes=1),
    # },
    # ========== DEPROVISION TASKS (END) ==========
}
