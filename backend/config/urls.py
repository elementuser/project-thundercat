from django.urls import re_path
from django.conf.urls.static import static
from django.conf import settings
from django.urls import path, include
from django.contrib import admin
from rest_framework import routers
from rest_framework_simplejwt.views import TokenRefreshView, TokenVerifyView
from rest_framework_swagger.views import get_swagger_view
from backend.views.testStatus import update_test_status
from backend.views import (
    views,
    database_check_view,
    assigned_tests_view,
    room_check_in_view,
    notepad_view,
    # update_email_answer_view,
    # test_scorer_answer_view,
    uit_processes,
    utils,
)
from backend.views import (
    create_simple_jwt_view,
    test_access_code_view,
    # test_scorer_assignment_view,
    tics_view,
    uit_assigned_tests_view,
    # psrs_generate_test_access_code_simulation,
    mark_for_review_view,
    uit_scoring_view,
    test_administration_view,
    system_administration_view,
    reports_data_view,
    my_tests_view,
    extended_profile_options_view,
    update_multiple_choice_answers_modify_date_view,
    retrieve_all_users_view,
    system_alert_view,
    criticality_view,
    celery_triggers_view,
    language_view,
)
from cms.views import (
    retrieve_test_information_view,
    test_permissions,
    test_section_data_view,
    # get_file_view,
    public_tests,
    get_test_definitions_view,
    # scorer_test_section_view,
    test_builder_view,
    item_bank_view,
    test_break_bank_view,
)
from user_management.views import (
    permissions,
    user_personal_info,
    user_accommodations_view,
    user_extended_profile_view,
    password_reset,
)

schema_view = get_swagger_view(title="ThunderCAT APIs")

router = routers.DefaultRouter()
router.register(r"oec-cat/api/database-check", database_check_view.DatabaseViewSet)

urlpatterns = [
    re_path(r"^$", schema_view),
    re_path(r"^oec-cat/api/admin/", admin.site.urls),
    re_path(r"^oec-cat/api/auth/", include("djoser.urls")),
    re_path(r"^oec-cat/api/auth/", include("djoser.urls.authtoken")),
    path(r"oec-cat/api/backend-status", views.index, name="index"),
    path("", include(router.urls)),
    re_path(
        r"^oec-cat/api/auth/",
        include("rest_framework.urls", namespace="rest_framework"),
    ),
    re_path(
        r"^oec-cat/api/auth/jwt/create_token/",
        create_simple_jwt_view.CreateJWTtoken.as_view(),
        name="token_obtain_pair",
    ),
    re_path(
        r"^oec-cat/api/auth/jwt/refresh_token/",
        TokenRefreshView.as_view(),
        name="token_refresh",
    ),
    re_path(
        r"^oec-cat/api/auth/jwt/verify_token/",
        TokenVerifyView.as_view(),
        name="token_verify",
    ),
    re_path(
        r"^oec-cat/api/password_reset/validate_token",
        password_reset.CustomResetPasswordValidateToken.as_view(),
    ),
    re_path(
        r"^oec-cat/api/password_reset/confirm",
        password_reset.CustomResetPasswordConfirm.as_view(),
    ),
    re_path(
        r"^oec-cat/api/password_reset",
        password_reset.CustomResetPasswordRequestToken.as_view(),
    ),
    re_path(
        r"^oec-cat/api/update-user-personal-info",
        user_personal_info.UpdateUserPersonalInfo.as_view(),
    ),
    re_path(
        r"^oec-cat/api/send-user-profile-change-request",
        user_personal_info.SendUserProfileChangeRequest.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-user-profile-change-request",
        user_personal_info.GetUserProfileChangeRequest.as_view(),
    ),
    re_path(
        r"^oec-cat/api/delete-user-profile-change-request",
        user_personal_info.DeleteUserProfileChangeRequest.as_view(),
    ),
    re_path(
        r"^oec-cat/api/does-this-email-exist",
        user_personal_info.DoesThisEmailExist.as_view(),
    ),
    # API disabled until feature is approved
    # re_path(
    #     r"^oec-cat/api/change-user-password",
    #     user_personal_info.ChangeUserPassword.as_view(),
    # ),
    re_path(
        r"^oec-cat/api/update-user-last-password-change-time",
        user_personal_info.UpdateUserLastPasswordChangeTime.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-active-non-public-tests",
        retrieve_test_information_view.GetActiveNonPublicTests.as_view(),
    ),
    re_path(
        r"^oec-cat/api/assigned-tests", assigned_tests_view.AssignedTestsSet.as_view()
    ),
    re_path(
        r"^oec-cat/api/update-test-time",
        test_administration_view.UpdateTestTime.as_view(),
    ),
    re_path(
        r"^oec-cat/api/update-break-bank",
        test_administration_view.UpdateBreakBank.as_view(),
    ),
    re_path(
        r"^oec-cat/api/approve-candidate",
        test_administration_view.ApproveCandidate.as_view(),
    ),
    re_path(
        r"^oec-cat/api/lock-candidate-test",
        test_administration_view.LockCandidateTest.as_view(),
    ),
    re_path(
        r"^oec-cat/api/lock-all-candidates-test",
        test_administration_view.LockAllCandidatesTest.as_view(),
    ),
    re_path(
        r"^oec-cat/api/unlock-candidate-test",
        test_administration_view.UnlockCandidateTest.as_view(),
    ),
    re_path(
        r"^oec-cat/api/unlock-all-candidates-test",
        test_administration_view.UnlockAllCandidatesTest.as_view(),
    ),
    re_path(
        r"^oec-cat/api/un-assign-candidate",
        test_administration_view.UnAssignCandidate.as_view(),
    ),
    re_path(r"^oec-cat/api/quit-test", update_test_status.QuitTest.as_view()),
    re_path(
        r"^oec-cat/api/update-test-status",
        update_test_status.UpdateTestStatus.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-previous-test-status",
        update_test_status.GetPreviousTestStatus.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-current-test-status",
        update_test_status.GetCurrentTestStatus.as_view(),
    ),
    re_path(
        r"^oec-cat/api/mark-for-review",
        mark_for_review_view.MarkForReviewView.as_view(),
    ),
    re_path(
        r"^oec-cat/api/update-multiple-choice-answers-modify-date",
        update_multiple_choice_answers_modify_date_view.UpdateMultipleChoiceAnswersModifyDate.as_view(),
    ),
    re_path(r"^oec-cat/api/get-permissions", permissions.GetPermissions.as_view()),
    re_path(
        r"^oec-cat/api/send-permission-request",
        permissions.SendPermissionRequest.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-pending-permissions",
        permissions.GetPendingPermissions.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-found-pending-permissions",
        permissions.GetFoundPendingPermissions.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-user-pending-permissions",
        permissions.GetUserPendingPermissions.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-user-accommodations",
        user_accommodations_view.GetUserAccommodations.as_view(),
    ),
    re_path(
        r"^oec-cat/api/save-user-accommodations",
        user_accommodations_view.SaveUserAccommodations.as_view(),
    ),
    re_path(r"^oec-cat/api/grant-permission", permissions.GrantPermission.as_view()),
    re_path(r"^oec-cat/api/deny-permission", permissions.DenyPermission.as_view()),
    re_path(
        r"^oec-cat/api/get-active-permissions",
        permissions.GetActivePermissions.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-found-active-permissions",
        permissions.GetFoundActivePermissions.as_view(),
    ),
    re_path(
        r"^oec-cat/api/update-active-permission",
        permissions.UpdateActivePermissionData.as_view(),
    ),
    re_path(
        r"^oec-cat/api/delete-active-permission",
        permissions.DeleteActivePermission.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-user-permissions", permissions.GetUserPermissions.as_view()
    ),
    re_path(
        r"^oec-cat/api/get-selected-user-permissions",
        permissions.GetSelectedUserPermissions.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-selected-user-pending-permissions",
        permissions.GetSelectedUserPendingPermissions.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-available-permissions",
        permissions.GetAvailablePermissionsForRequest.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-users-based-on-specified-permission",
        permissions.GetUsersBasedOnSpecifiedPermission.as_view(),
    ),
    re_path(
        r"^oec-cat/api/grant-test-permission",
        test_permissions.GrantTestPermission.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-test-permissions",
        test_permissions.GetTestPermissions.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-ta-orderless-test-options",
        test_permissions.GetTaOrderlessTestPermissions.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-ta-orderless-uit-test-options",
        test_permissions.GetTaOrderlessUitTestPermissions.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-selected-user-test-permissions",
        test_permissions.GetSelectedUserTestPermissions.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-unassigned-orderless-test-administrators",
        test_permissions.GetOrderlessTestAdministrators.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-all-orderless-test-administrators",
        test_permissions.GetAllOrderlessTestAdministrators.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-found-orderless-test-administrators",
        test_permissions.GetFoundOrderlessTestAdministrators.as_view(),
    ),
    re_path(
        r"^oec-cat/api/add-orderless-test-administrators",
        test_permissions.AddOrderlessTestAdministrators.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-orderless-test-accesses",
        test_permissions.GetOrderlessTestAccesses.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-found-orderless-test-accesses",
        test_permissions.GetFoundOrderlessTestAccesses.as_view(),
    ),
    re_path(
        r"^oec-cat/api/add-delete-orderless-test-access",
        test_permissions.AddDeleteOrderlessTestAccess.as_view(),
    ),
    re_path(
        r"^oec-cat/api/delete-orderless-test-administrator",
        test_permissions.DeleteOrderlessTestAdministrator.as_view(),
    ),
    re_path(
        r"^oec-cat/api/update-orderless-test-administrator-department",
        test_permissions.UpdateOrderlessTestAdministratorDepartment.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-test-permission-financial-data",
        test_permissions.GetTestPermissionFinancialData.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-all-active-test-permissions",
        test_permissions.GetAllActiveTestPermissions.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-found-active-test-permissions",
        test_permissions.GetFoundActiveTestPermissions.as_view(),
    ),
    re_path(
        r"^oec-cat/api/delete-test-permission",
        test_permissions.DeleteTestPermission.as_view(),
    ),
    re_path(
        r"^oec-cat/api/update-test-permission",
        test_permissions.UpdateTestPermission.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-new-test-access-code",
        test_access_code_view.GetNewTestAccessCode.as_view(),
    ),
    re_path(
        r"^oec-cat/api/delete-test-access-code",
        test_access_code_view.DeleteTestAccessCode.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-active-test-access-codes",
        test_access_code_view.GetActiveTestAccessCodes.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-all-active-test-access-codes",
        test_access_code_view.GetAllActiveTestAccessCodes.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-found-active-test-access-codes",
        test_access_code_view.GetAllFoundActiveTestAccessCodes.as_view(),
    ),
    re_path(r"^oec-cat/api/room-check-in", room_check_in_view.RoomCheckIn.as_view()),
    re_path(
        r"^oec-cat/api/get-test-administrator-assigned-candidates",
        test_administration_view.GetTestAdministratorAssignedCandidates.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-all-active-tests",
        system_administration_view.GetAllActiveTests.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-found-active-tests",
        system_administration_view.GetAllFoundActiveTests.as_view(),
    ),
    re_path(
        r"^oec-cat/api/as-etta-invalidate-candidate",
        system_administration_view.invalidateCandidateAsETTA.as_view(),
    ),
    re_path(
        r"^oec-cat/api/as-etta-validate-candidate",
        system_administration_view.validateCandidateAsETTA.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-reason-for-invalidating-the-test",
        system_administration_view.GetReasonForInvalidatingTheTest.as_view(),
    ),
    re_path(r"^oec-cat/api/update-notepad", notepad_view.SaveNotepad.as_view()),
    re_path(r"^oec-cat/api/get-notepad", notepad_view.GetNotepad.as_view()),
    # removed for launch of SLE Acc because they are not needed and are not compelete.
    # TODO write postman tests for these
    # re_path(r"^oec-cat/api/save-score", test_scorer_answer_view.SaveScore.as_view()),
    # re_path(r"^oec-cat/api/get-scores", test_scorer_answer_view.GetScores.as_view()),
    re_path(
        r"^oec-cat/api/get-extended-profile-dropdown-options",
        extended_profile_options_view.GetExtendedProfileOptions.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-extended-profile",
        user_extended_profile_view.GetExtendedProfile.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-selected-user-extended-profile",
        user_extended_profile_view.GetSelectedUserExtendedProfile.as_view(),
    ),
    re_path(
        r"^oec-cat/api/set-extended-profile",
        user_extended_profile_view.SetExtendedProfile.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-ta-extended-profile",
        user_extended_profile_view.GetTaExtendedProfile.as_view(),
    ),
    # CMS
    re_path(
        r"^oec-cat/api/get-test-definition-data",
        get_test_definitions_view.GetTestDefinitionData.as_view(),
    ),
    re_path(
        r"^oec-cat/api/archive-test-definition-test-code",
        get_test_definitions_view.ArchiveTestDefinitionTestCode.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-test-definition-versions-collected",
        get_test_definitions_view.GetTestDefinitionsVersionsCollectedView.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-test-section",
        test_section_data_view.TestSectionData.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-questions-list",
        test_section_data_view.GetQuestionsList.as_view(),
    ),
    re_path(
        r"^oec-cat/api/validate-timeout-state",
        test_section_data_view.ValidateTimeoutState.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-current-test-section",
        test_section_data_view.TestSectionDataById.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-updated-time-after-lock-pause-actions",
        test_section_data_view.GetUpdatedTimeAfterLockPause.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-test-extract", test_builder_view.ExtractTestView.as_view()
    ),
    re_path(
        r"^oec-cat/api/test-definition", test_builder_view.TestDefinitionView.as_view()
    ),
    re_path(
        r"^oec-cat/api/upload-test", test_builder_view.TestBuilderUploaderView.as_view()
    ),
    re_path(
        r"^oec-cat/api/try-test-section", test_builder_view.TryTestSectionView.as_view()
    ),
    re_path(
        r"^oec-cat/api/get-server-time",
        utils.GetServerTime.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-scoring-method-types",
        test_builder_view.TestBuilderGetScoringMethodTypes.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-break-bank-remaining-time",
        test_break_bank_view.GetBreakBankRemainingTime.as_view(),
    ),
    re_path(
        r"^oec-cat/api/trigger-break-bank-action",
        test_break_bank_view.TriggerBreakBankAction.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-specified-test-scoring-method-data",
        test_builder_view.GetSpecifiedTestScoringMethodData.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-test-definition-latest-version",
        test_builder_view.GetTestDefinitionLatestVersion.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-test-data",
        retrieve_test_information_view.GetTestData.as_view(),
    ),
    # Commented out because not needed for SLE Acc launch
    # TODO write postman tests
    # re_path(
    #     r"^oec-cat/api/get_scorer_unassigned_tests",
    #     test_scorer_assignment_view.GetScorerUnassignedTests.as_view(),
    # ),
    # re_path(
    #     r"^oec-cat/api/get_scorer_assigned_tests",
    #     test_scorer_assignment_view.GetScorerAssignedTests.as_view(),
    # ),
    # re_path(
    #     r"^oec-cat/api/assign_test_to_scorer",
    #     test_scorer_assignment_view.AssignTestToScorer.as_view(),
    # ),
    re_path(r"^oec-cat/api/get-tics-data", tics_view.GetTicsData.as_view()),
    re_path(r"^oec-cat/api/public-tests", public_tests.PublicTests.as_view()),
    # OIT
    re_path(
        r"^oec-cat/api/uit-seen-question",
        uit_assigned_tests_view.UitSeenQuestion.as_view(),
    ),
    re_path(
        r"^oec-cat/api/uit-save-answer", uit_assigned_tests_view.UitSaveAnswer.as_view()
    ),
    re_path(
        r"^oec-cat/api/get-test-answers",
        uit_assigned_tests_view.GetTestAnswers.as_view(),
    ),
    re_path(
        r"^oec-cat/api/update-candidate-answer-modify-date",
        uit_assigned_tests_view.UpdateCandidateAnswerModifyDate.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-all-item-banks", item_bank_view.GetAllItemBanks.as_view()
    ),
    re_path(
        r"^oec-cat/api/get-found-item-banks", item_bank_view.GetFoundItemBanks.as_view()
    ),
    re_path(
        r"^oec-cat/api/get-item-banks-for-test-builder",
        item_bank_view.GetItemBanksForTestBuilder.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-item-bank-bundles-for-test-builder",
        item_bank_view.GetItemBankBundlesForTestBuilder.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-selected-item-bank-permissions",
        item_bank_view.GetSelectedItemBankDataAsRdOperations.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-selected-item-bank-data",
        item_bank_view.GetSelectedItemBankData.as_view(),
    ),
    re_path(
        r"^oec-cat/api/create-new-item-bank", item_bank_view.CreateNewItemBank.as_view()
    ),
    re_path(
        r"^oec-cat/api/update-item-bank-data-as-rd-operations",
        item_bank_view.UpdateItemBankDataAsRdOperations.as_view(),
    ),
    re_path(
        r"^oec-cat/api/add-test-developer-item-bank-permissions",
        item_bank_view.AddTestDeveloper.as_view(),
    ),
    re_path(
        r"^oec-cat/api/delete-test-developer-item-bank-permissions",
        item_bank_view.DeleteTestDeveloper.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-item-bank-access-types",
        item_bank_view.GetItemBankAccessTypes.as_view(),
    ),
    re_path(
        r"^oec-cat/api/update-item-bank-access-types",
        item_bank_view.UpdateItemBankAccessType.as_view(),
    ),
    re_path(
        r"^oec-cat/api/update-item-bank-data",
        item_bank_view.UpdateItemBankData.as_view(),
    ),
    re_path(r"^oec-cat/api/get-all-items", item_bank_view.GetAllItems.as_view()),
    re_path(r"^oec-cat/api/get-found-items", item_bank_view.GetFoundItems.as_view()),
    re_path(r"^oec-cat/api/get-item-data", item_bank_view.GetItemData.as_view()),
    re_path(
        r"^oec-cat/api/get-selected-item-version-data",
        item_bank_view.GetSelectedItemVersionData.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-response-formats",
        item_bank_view.GetResponseFormats.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-development-statuses",
        item_bank_view.GetDevelopmentStatuses.as_view(),
    ),
    re_path(r"^oec-cat/api/create-new-item", item_bank_view.CreateNewItem.as_view()),
    re_path(r"^oec-cat/api/update-item-data", item_bank_view.UpdateItemData.as_view()),
    re_path(
        r"^oec-cat/api/delete-item-draft", item_bank_view.DeleteItemDraft.as_view()
    ),
    re_path(
        r"^oec-cat/api/create-item-comment",
        item_bank_view.CreateItemComment.as_view(),
    ),
    re_path(
        r"^oec-cat/api/delete-item-comment",
        item_bank_view.DeleteItemComment.as_view(),
    ),
    re_path(r"^oec-cat/api/get-all-bundles", item_bank_view.GetAllBundles.as_view()),
    re_path(
        r"^oec-cat/api/get-specific-bundle", item_bank_view.GetSpecificBundle.as_view()
    ),
    re_path(
        r"^oec-cat/api/get-found-bundles", item_bank_view.GetFoundBundles.as_view()
    ),
    re_path(
        r"^oec-cat/api/create-new-bundle", item_bank_view.CreateNewBundle.as_view()
    ),
    re_path(r"^oec-cat/api/upload-bundle", item_bank_view.UploadBundle.as_view()),
    re_path(
        r"^oec-cat/api/get-items-ready-to-be-added-to-bundle",
        item_bank_view.GetItemsReadyToBeAddedToBundle.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-items-found-ready-to-be-added-to-bundle",
        item_bank_view.GetFoundItemsReadyToBeAddedToBundle.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-bundles-ready-to-be-added-to-bundle",
        item_bank_view.GetBundlesReadyToBeAddedToBundle.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-all-found-bundles-ready-to-be-added-to-bundle",
        item_bank_view.GetFoundBundlesReadyToBeAddedToBundle.as_view(),
    ),
    re_path(
        r"^oec-cat/api/preview-generated-items-list-from-bundle",
        item_bank_view.PreviewItemsListFromBundle.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-generated-items-list-from-bundle",
        item_bank_view.GetItemsListFromBundle.as_view(),
    ),
    # Scoring
    re_path(
        r"^oec-cat/api/re-score-submitted-test",
        uit_scoring_view.ReScoreSubmittedTest.as_view(),
    ),
    # My Tests
    re_path(
        r"^oec-cat/api/assigned-scored-tests",
        my_tests_view.AssignedScoredTests.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-all-users", retrieve_all_users_view.GetAllUsers.as_view()
    ),
    re_path(
        r"^oec-cat/api/get-found-users",
        retrieve_all_users_view.GetAllFoundUsers.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-selected-user-personal-info/",
        user_personal_info.GetSelectedUserPersonalInfo.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-all-tests-for-selected-user/",
        my_tests_view.GetAllTestsForSelectedUser.as_view(),
    ),
    # New eMIB style
    # TODO uncomment for eMIB
    # re_path(
    #     r"^oec-cat/api/update-email-answer",
    #     update_email_answer_view.UpdateEmailAnswer.as_view(),
    # ),
    # protected files
    # re_path(r"^oec-cat/api/get-file", get_file_view.GetFile.as_view()),
    # scorer
    # re_path(
    #     r"^oec-cat/api/get-scorer-test-section",
    #     scorer_test_section_view.ScorerTestSection.as_view(),
    # ),
    # reports
    re_path(
        r"^oec-cat/api/get-ta-assigned-test-order-numbers",
        reports_data_view.GetTaAssignedTestOrderNumbers.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-all-existing-test-order-numbers",
        reports_data_view.GetAllExistingTestOrderNumbers.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-tests-based-on-test-order-number",
        reports_data_view.GetTestsBasedOnTestOrderNumber.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-candidates-based-on-selected-test",
        reports_data_view.GetCandidatesBasedOnSelectedTest.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-candidate-tests",
        reports_data_view.GetCandidateTests.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-results-report-data",
        reports_data_view.GetResultsReportData.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-financial-report-data",
        reports_data_view.GetFinancialReportData.as_view(),
    ),
    re_path(
        r"^oec-cat/api/reports-get-test-definition-data",
        reports_data_view.GetTestDefinitionData.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-test-content-report-data",
        reports_data_view.GetTestContentReportData.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-test-taker-report-data",
        reports_data_view.GetTestTakerReportData.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-candidate-actions-report-data",
        reports_data_view.GetCandidateActionsReportData.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-all-active-uit-processes-for-TA",
        uit_processes.GetAllActiveUitProcessesForTa.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-all-completed-uit-processes-for-TA",
        uit_processes.GetAllCompletedUitProcessesForTa.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-found-completed-uit-processes-for-TA",
        uit_processes.GetFoundCompletedUitProcessesForTa.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-found-active-uit-processes-for-TA",
        uit_processes.GetFoundActiveUitProcessesForTa.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-selected-completed-uit-processes-details-for-TA",
        uit_processes.GetSelectedCompletedUitProcessesDetailsForTa.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-reasons-for-testing",
        uit_processes.GetReasonsForTesting.as_view(),
    ),
    re_path(
        r"^oec-cat/api/send-uit-invitations", uit_processes.SendUitInvitations.as_view()
    ),
    re_path(
        r"^oec-cat/api/uit-invitation-end-date-validation",
        uit_processes.UitInvitationEndDateValidation.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-selected-uit-active-processes-details",
        uit_processes.GetSelectedUITActiveProcessesDetails.as_view(),
    ),
    re_path(
        r"^oec-cat/api/update-selected-uit-active-process-validity-end-date",
        uit_processes.UpdateSelectedUITActiveProcessValidityEndDate.as_view(),
    ),
    re_path(
        r"^oec-cat/api/deactivate-single-uit-test",
        uit_processes.DeactivateSingleUitTest.as_view(),
    ),
    re_path(
        r"^oec-cat/api/deactivate-uit-process",
        uit_processes.DeactivateUitProcess.as_view(),
    ),
    re_path(
        r"^oec-cat/api/create-system-alert",
        system_alert_view.CreateSystemAlert.as_view(),
    ),
    re_path(
        r"^oec-cat/api/modify-system-alert",
        system_alert_view.ModifySystemAlert.as_view(),
    ),
    re_path(
        r"^oec-cat/api/archive-system-alert",
        system_alert_view.ArchiveSystemAlert.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-active-system-alerts-to-be-displayed-on-home-page",
        system_alert_view.GetActiveSystemAlertsToDisplayOnHomePage.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-system-alert-data",
        system_alert_view.GetSystemAlertData.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-all-active-system-alerts",
        system_alert_view.GetAllActiveSystemAlerts.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-all-archived-system-alerts",
        system_alert_view.GetAllArchivedSystemAlerts.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-found-archived-system-alerts",
        system_alert_view.GetFoundArchivedSystemAlerts.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-criticality-options",
        criticality_view.GetCriticalityOptions.as_view(),
    ),
    re_path(
        r"^oec-cat/api/celery/update_non_submitted_active_assigned_tests",
        celery_triggers_view.UpdateNonSubmittedActiveAssignedTests.as_view(),
    ),
    re_path(
        r"^oec-cat/api/celery/unpause_assigned_tests",
        celery_triggers_view.UnpauseAssignedTests.as_view(),
    ),
    re_path(
        r"^oec-cat/api/celery/invalidate_locked_assigned_tests",
        celery_triggers_view.InvalidateLockedAssignedTests.as_view(),
    ),
    re_path(
        r"^oec-cat/api/celery/unassign_ready_and_pre_test_assigned_tests",
        celery_triggers_view.UnassignReadyAndPreTestAssignedTests.as_view(),
    ),
    re_path(
        r"^oec-cat/api/celery/remove_expired_test_permissions",
        celery_triggers_view.RemoveExpiredTestPermissions.as_view(),
    ),
    re_path(
        r"^oec-cat/api/celery/handle_expired_uit_processes",
        celery_triggers_view.HandleExpiredUitProcesses.as_view(),
    ),
    re_path(
        r"^oec-cat/api/celery/handle_send_uit_invite_emails",
        celery_triggers_view.HandleSendUitInviteEmails.as_view(),
    ),
    re_path(
        r"^oec-cat/api/celery/handle_update_uit_validity_end_date_emails",
        celery_triggers_view.HandleUpdateUitValidityEndDateEmails.as_view(),
    ),
    re_path(
        r"^oec-cat/api/celery/handle_delete_uit_invite_emails",
        celery_triggers_view.HandleDeleteUitInviteEmails.as_view(),
    ),
    re_path(
        r"^oec-cat/api/celery/deprovision_after_inactivity",
        celery_triggers_view.DeprovisionAfterInactivity.as_view(),
    ),
    re_path(
        r"^oec-cat/api/celery/deprovision_admin_after_inactivity_thirty_days",
        celery_triggers_view.DeprovisionAdminAfterInactivityThirtyDays.as_view(),
    ),
    re_path(
        r"^oec-cat/api/celery/reset_completed_profile_flag",
        celery_triggers_view.ResetCompletedProfileFlag.as_view(),
    ),
    re_path(
        r"^oec-cat/api/get-language-data",
        language_view.GetLanguageData.as_view(),
    ),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
