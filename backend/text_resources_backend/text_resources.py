from user_management.user_management_models.user_models import LOCK_OUT_TIME
from user_management.static.password_reset import PasswordResetConfigurations


class TextResources:
    invalidLogin = {
        "en": "Email address and/or password is invalid",
        "fr": "Adresse courriel et/ou mot de passe invalide",
    }
    lockedAccount = {
        "en": "User account has been locked for {0} hours".format(LOCK_OUT_TIME.hours),
        "fr": "Le compte de l'utilisateur a été verrouillé pour {0} heures".format(
            LOCK_OUT_TIME.hours
        ),
    }
    lockedEmailSendFunctionality = {
        "en": "You can no longer send emails related to this account for the next {0} minutes".format(
            int(round(PasswordResetConfigurations.LOCK_OUT_TIME / 60))
        ),
        "fr": "Vous ne pourrez pas envoyer de courriels en lien avec ce compte au cours des {0} prochaines minutes".format(
            int(round(PasswordResetConfigurations.LOCK_OUT_TIME / 60))
        ),
    }
    cannotOverrideExistingTestDefinitions_1 = {
        "en": "Matching test definition with the same version and test code.",
        "fr": "FR Matching test definition with the same version and test code.",
    }
    cannotOverrideExistingTestDefinitions_2 = {
        "en": "Cannot override existing test definitions.",
        "fr": "FR Cannot override existing test definitions.",
    }
    cannotOverrideExistingTestDefinitions_3 = {
        "en": "If you wish to do this, contact the IT team.",
        "fr": "FR If you wish to do this, contact the IT team.",
    }
    addressBookContactError_1 = {
        "en": "Could not find an address book contact.",
        "fr": "FR Could not find an address book contact.",
    }
    addressBookContactError_2 = {
        "en": "Did you delete a contact from the address book and forget to remove them from Tree Descriptions or emails questions?",
        "fr": "FR Did you delete a contact from the address book and forget to remove them from Tree Descriptions or emails questions?",
    }
    emailQuestionError_1 = {
        "en": "Could not create email question.",
        "fr": "FR Could not create email question.",
    }
    emailQuestionError_2 = {
        "en": "Make sure that you have fully completed all details on the component.",
        "fr": "FR Make sure that you have fully completed all details on the component.",
    }
    uitTestAlreadyStarted = {
        "en": "You cannot unassign/deactivate this test since the candidate has already started it.",
        "fr": "Vous ne pouvez pas désassigner/désactiver ce test puisque le candidat l'a déjà commencé.",
    }
    weekday = {
        "monday": {"en": "Monday", "fr": "Lundi"},
        "tuesday": {"en": "Tuesday", "fr": "Mardi"},
        "wednesday": {"en": "Wednesday", "fr": "Mercredi"},
        "thursday": {"en": "Thursday", "fr": "Jeudi"},
        "friday": {"en": "Friday", "fr": "Vendredi"},
        "saturday": {"en": "Saturday", "fr": "Samedi"},
        "sunday": {"en": "Sunday", "fr": "Dimanche"},
    }
    month = {
        "january": {"en": "January", "fr": "Janvier"},
        "february": {"en": "February", "fr": "Février"},
        "march": {"en": "March", "fr": "Mars"},
        "april": {"en": "April", "fr": "Avril"},
        "may": {"en": "May", "fr": "Mai"},
        "june": {"en": "June", "fr": "Juin"},
        "july": {"en": "July", "fr": "Juillet"},
        "august": {"en": "August", "fr": "Août"},
        "september": {"en": "September", "fr": "Septembre"},
        "october": {"en": "October", "fr": "Octobre"},
        "november": {"en": "November", "fr": "Novembre"},
        "december": {"en": "December", "fr": "Décembre"},
    }

    commons = {
        "active": {"en": "Active", "fr": "Actif"},
        "inactive": {"en": "Inactive", "fr": "Inactif"},
    }
