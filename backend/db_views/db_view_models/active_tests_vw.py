from django.db import models


class ActiveTestsVW(models.Model):
    assigned_test_id = models.IntegerField(
        db_column="ASSIGNED_TEST_ID", primary_key=True
    )
    candidate_first_name = models.EmailField(
        db_column="CANDIDATE_FIRST_NAME", max_length=30
    )
    candidate_last_name = models.EmailField(
        db_column="CANDIDATE_LAST_NAME", max_length=150
    )
    candidate_username = models.EmailField(
        db_column="CANDIDATE_USERNAME", max_length=254
    )
    candidate_email = models.EmailField(db_column="CANDIDATE_EMAIL", max_length=254)
    ta_id = models.EmailField(db_column="TA_ID", max_length=254)
    ta_first_name = models.EmailField(db_column="TA_FIRST_NAME", max_length=30)
    ta_last_name = models.EmailField(db_column="TA_LAST_NAME", max_length=150)
    test_status = models.IntegerField(db_column="TEST_STATUS")
    test_order_number = models.CharField(db_column="TEST_ORDER_NUMBER", max_length=12)
    reference_number = models.CharField(db_column="REFERENCE_NUMBER", max_length=25)
    time_limit = models.IntegerField(db_column="TIME_LIMIT")
    test_id = models.IntegerField(db_column="TEST_ID")
    test_code = models.CharField(db_column="TEST_CODE", max_length=25)

    class Meta:
        managed = False
        db_table = "active_tests_vw"
