from django.db import models


class ItemOptionDraftsVW(models.Model):
    item_option_id = models.IntegerField(db_column="ITEM_OPTION_ID", primary_key=True)
    system_id = models.CharField(db_column="SYSTEM_ID", max_length=15)
    option_type = models.IntegerField(db_column="OPTION_TYPE")
    item_option_modify_date = models.DateTimeField(db_column="ITEM_OPTION_MODIFY_DATE")
    option_order = models.IntegerField(db_column="OPTION_ORDER")
    exclude_from_shuffle = models.BooleanField(
        db_column="EXCLUDE_FROM_SHUFFLE", default=0
    )
    historical_option_id = models.CharField(
        db_column="HISTORICAL_OPTION_ID", max_length=50, default=""
    )
    rationale = models.TextField(db_column="RATIONALE", default="")
    score = models.CharField(max_length=25)
    item_id = models.IntegerField(db_column="ITEM_ID")
    username_id = models.EmailField(db_column="USERNAME_ID", max_length=254)
    item_option_text_id = models.IntegerField(db_column="ITEM_OPTION_TEXT_ID")
    text = models.TextField()
    item_option_text_modify_date = models.DateTimeField(
        db_column="ITEM_OPTION_TEXT_MODIFY_DATE"
    )
    language_id = models.IntegerField(db_column="LANGUAGE_ID")

    class Meta:
        managed = False
        db_table = "item_option_drafts_vw"
