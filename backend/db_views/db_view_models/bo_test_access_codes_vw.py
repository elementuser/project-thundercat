from django.db import models


class BoTestAccessCodesVW(models.Model):
    test_access_code = models.CharField(
        db_column="TEST_ACCESS_CODE", primary_key=True, max_length=10
    )
    test_order_number = models.CharField(db_column="TEST_ORDER_NUMBER", max_length=12)
    reference_number = models.CharField(db_column="REFERENCE_NUMBER", max_length=25)
    created_date = models.DateField(db_column="CREATED_DATE")
    ta_username = models.EmailField(db_column="TA_USERNAME", max_length=254)
    ta_email = models.EmailField(db_column="TA_EMAIL", max_length=254)
    ta_first_name = models.EmailField(db_column="TA_FIRST_NAME", max_length=30)
    ta_last_name = models.EmailField(db_column="TA_LAST_NAME", max_length=150)
    ta_goc_email = models.EmailField(db_column="TA_GOC_EMAIL", max_length=254)
    test_id = models.IntegerField(db_column="TEST_ID")
    en_test_name = models.CharField(db_column="EN_TEST_NAME", max_length=175)
    fr_test_name = models.CharField(db_column="FR_TEST_NAME", max_length=175)

    class Meta:
        managed = False
        db_table = "bo_test_access_codes_vw"
