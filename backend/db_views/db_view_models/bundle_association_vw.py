from django.db import models


class BundleAssociationVW(models.Model):
    bundle_bundles_association_id = models.IntegerField(
        db_column="BUNDLE_BUNDLES_ASSOCIATION_ID", primary_key=True
    )
    bundle_items_association_id = models.IntegerField(
        db_column="BUNDLE_ITEMS_ASSOCIATION_ID"
    )
    bundle_source_id = models.IntegerField(db_column="BUNDLE_SOURCE_ID")
    system_id_link = models.CharField(db_column="SYSTEM_ID_LINK", max_length=15)
    version = models.IntegerField(db_column="VERSION")
    bundle_link_id = models.IntegerField(db_column="BUNDLE_LINK_ID")
    display_order = models.IntegerField(db_column="DISPLAY_ORDER")

    class Meta:
        managed = False
        db_table = "bundle_association_vw"
