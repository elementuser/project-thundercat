from django.db import models


class ItemContentDraftsVW(models.Model):
    item_content_id = models.IntegerField(db_column="ITEM_CONTENT_ID", primary_key=True)
    system_id = models.CharField(db_column="SYSTEM_ID", max_length=15)
    content_type = models.IntegerField(db_column="CONTENT_TYPE")
    item_content_modify_date = models.DateTimeField(
        db_column="ITEM_CONTENT_MODIFY_DATE"
    )
    content_order = models.IntegerField(db_column="CONTENT_ORDER")
    item_id = models.IntegerField(db_column="ITEM_ID")
    username_id = models.EmailField(db_column="USERNAME_ID", max_length=254)
    item_content_text_id = models.IntegerField(db_column="ITEM_CONTENT_TEXT_ID")
    text = models.TextField()
    item_content_text_modify_date = models.DateTimeField(
        db_column="ITEM_CONTENT_TEXT_MODIFY_DATE"
    )
    language_id = models.IntegerField(db_column="LANGUAGE_ID")

    class Meta:
        managed = False
        db_table = "item_content_drafts_vw"
