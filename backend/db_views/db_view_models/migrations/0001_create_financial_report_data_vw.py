from django.db import connection
from django.db import migrations
from django.conf import settings


def create_financial_report_data_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER FUNCTION [dbo].[status2str] 
( 
	@lang_code varchar(2),
	@status_id numeric
)
RETURNS varchar(100)
AS 
BEGIN
	DECLARE @Result		varchar(100)
    IF (@lang_code = 'fr')
		BEGIN
			SELECT @Result	= CASE 
			WHEN @status_id = 11 THEN 'ATTRIBUÉ'
			WHEN @status_id = 12 THEN 'PRÊT'
			WHEN @status_id = 13 THEN 'ACTIF'
			WHEN @status_id = 14 THEN 'VERROUILLÉ'
			WHEN @status_id = 15 THEN 'EN PAUSE'			
			WHEN @status_id = 19 THEN 'ABANDONNÉ'
			WHEN @status_id = 20 THEN 'SOUMIS'
			WHEN @status_id = 21 THEN 'NON ATTRIBUÉ'
			WHEN @status_id = 22 THEN 'PRÉ_TEST'
			ELSE CAST(@status_id AS varchar(4))
			END
		END
	ELSE
		BEGIN
			SELECT @Result	= CASE 
			WHEN @status_id = 11 THEN 'ASSIGNED'
			WHEN @status_id = 12 THEN 'READY'
			WHEN @status_id = 13 THEN 'ACTIVE'
			WHEN @status_id = 14 THEN 'LOCKED'
			WHEN @status_id = 15 THEN 'PAUSED'			
			WHEN @status_id = 19 THEN 'QUIT'
			WHEN @status_id = 20 THEN 'SUBMITTED'
			WHEN @status_id = 21 THEN 'UNASSIGNED'
			WHEN @status_id = 22 THEN 'PRE_TEST'
			ELSE CAST(@status_id AS varchar(4))
			END
		END
	RETURN @Result
END"""
        )

        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[financial_report_vw]
                    AS	

                    SELECT TOP 100 percent
                    	IIF (u.id is null, v.last_name, u.last_name) as "candidate_last_name",
                        IIF (u.id is null, v.first_name, u.first_name) as "candidate_first_name",
                        IIF (u.id is null, v.pri_or_military_nbr, u.pri_or_military_nbr) as "candidate_pri",
                        a.ta_id as "ta_email",
                        de.edesc as "ta_org_en",
                        de.fdesc as "ta_org_fr",
                        IIF (dp.edesc is null, 'Public Service Commission', dp.edesc) as "requesting_dep_en",
                        IIF (dp.fdesc is null, 'Commission de la Fonction Publique', dp.fdesc) as "requesting_dep_fr",	
                        a.test_order_number as "order_no",	
                        p.staffing_process_number as "assessment_process", 
                        o.reference_number as "reference_number",          
                        p.is_org as "org_code",
                        p.is_org as "fis_org_code",
                        p.is_ref as "fis_ref_code",
                        p.billing_contact as "billing_contact_name",
                        dbo.status2str('fr', a.status) AS "test_status_fr",
	                    dbo.status2str('en', a.status) AS "test_status_en",     
                        a.is_invalid as "is_invalid",
                        cast(a.submit_date as date) as "submit_date",    
                        d.test_code as "test_code",
                        d.fr_name as "test_description_fr",
                        d.en_name as "test_description_en",
                        a.orderless_financial_data_id,
                        a.status as "test_status",		
                        a.test_id as "test_id",
                        a.id as "assigned_test_id"
                    FROM	
                        {db_name}..custom_models_assignedtest a
                    OUTER APPLY (SELECT TOP 1 * FROM {db_name}..user_management_models_historicaluser u1 WHERE u1.username = a.username_id AND u1.history_date < a.submit_date ORDER BY u1.history_date DESC) u
                    JOIN {db_name}..user_management_models_user v on v.username = a.username_id
                    LEFT JOIN {db_name}..user_management_models_taextendedprofile e ON e.username_id = a.ta_id
                    LEFT JOIN {db_name}..OLTF_DEPARTEMENTS_VW de ON de.DEPT_ID = e.department_id
                    OUTER APPLY (SELECT TOP 1 * FROM {db_name}..cms_models_historicaltestpermissions h WHERE h.test_order_number = a.test_order_number AND h.username_id = a.ta_id AND h.history_date < a.submit_date ORDER BY h.history_date DESC) p
                    LEFT JOIN {db_name}..OLTF_DEPARTEMENTS_VW dp ON cast(dp.DEPT_ID as nvarchar) = p.department_ministry_code
                    JOIN {db_name}..cms_models_testdefinition d ON d.id = a.test_id
                    LEFT JOIN {db_name}..custom_models_orderlessfinancialdata o ON o.id = a.orderless_financial_data_id
                    WHERE
                        --a.submit_date BETWEEN '2021-01-01' AND DATEADD(DAY, 1, '2021-02-01')
                        --AND 
                        a.status IN (19, 20)

                    ORDER BY
                        a.test_order_number, a.test_id, a.status, a.id DESC""".format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute("DROP FUNCTION IF EXISTS [dbo].[status2str]")
        cursor.execute("DROP VIEW IF EXISTS [dbo].[financial_report_vw]")


class Migration(migrations.Migration):
    dependencies = [
        (
            "custom_models",
            "0046_assignedtest_must_be_a_unique_status_username_tac_test_combination",
        ),
        ("ref_table_views", "0017_creating_more_data_in_oltf_department_vw"),
    ]
    operations = [
        migrations.RunPython(create_financial_report_data_vw, rollback_changes)
    ]
