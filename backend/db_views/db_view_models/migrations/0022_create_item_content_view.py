from django.db import connection
from django.db import migrations
from django.conf import settings


def create_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[item_content_vw] AS
				SELECT
					ic.id as 'item_content_id',
					ic.content_type,
					ic.content_order,
					ic.modify_date as 'item_content_modify_date',
					ic.item_id,
					ict.id as 'item_content_text_id',
					ict.text as 'text',
					ict.modify_date as 'item_content_text_modify_date',
					ict.language_id
				FROM {db_name}..cms_models_itemcontent ic
				LEFT JOIN {db_name}..cms_models_itemcontenttext ict on ict.item_content_id = ic.id
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute("DROP VIEW IF EXISTS [dbo].[item_content_vw]")


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0021_itemdraftsvw",
        ),
    ]
    operations = [migrations.RunPython(create_vw, rollback_changes)]
