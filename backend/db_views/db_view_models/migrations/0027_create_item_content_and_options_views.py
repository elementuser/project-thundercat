from django.db import connection
from django.db import migrations
from django.conf import settings


def create_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[item_content_drafts_vw] AS
				SELECT 
                    c.id as 'item_content_id',
                    c.system_id,
                    c.content_type,
                    c.modify_date as 'item_content_modify_date',
                    c.content_order,
                    c.item_id,
                    c.username_id,
                    ct.id as 'item_content_text_id',
                    ct.text,
                    ct.modify_date as 'item_content_text_modify_date',
                    ct.language_id
                FROM {db_name}..cms_models_itemcontentdrafts c
                JOIN {db_name}..cms_models_itemcontenttextdrafts ct on ct.item_content_id = c.id
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )

        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[item_option_drafts_vw] AS
				SELECT 
                    o.id as 'item_option_id',
                    o.system_id,
                    o.option_type,
                    o.modify_date as 'item_option_modify_date',
                    o.option_order,
                    o.score,
                    o.item_id,
                    o.username_id,
                    ot.id as 'item_option_text_id',
                    ot.text,
                    ot.modify_date as 'item_option_text_modify_date',
                    ot.language_id
                FROM {db_name}..cms_models_itemoptiondrafts o
                JOIN {db_name}..cms_models_itemoptiontextdrafts ot on ot.item_option_id = o.id
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute("DROP VIEW IF EXISTS [dbo].[item_content_drafts_vw]")
        cursor.execute("DROP VIEW IF EXISTS [dbo].[item_option_drafts_vw]")


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0026_update_item_drafts_view",
        ),
    ]
    operations = [migrations.RunPython(create_vw, rollback_changes)]
