from django.db import connection
from django.db import migrations
from django.conf import settings


def create_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[item_bank_attribute_values_vw] AS
                SELECT 
                    av.id as 'item_bank_attribute_value_id', 
                    av.attribute_value_order,
                    av.item_bank_attribute_id,
                    av.attribute_value_type,
                    avt.id as 'item_bank_attribute_values_text_id',
                    avt.text
                FROM {db_name}..cms_models_itembankattributevalues av 
                JOIN {db_name}..cms_models_itembankattributevaluestext avt ON avt.item_bank_attribute_values_id = av.id
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute("DROP VIEW IF EXISTS [dbo].[item_bank_attribute_values_vw]")


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0038_update_test_result_vw",
        ),
    ]
    operations = [migrations.RunPython(create_vw, rollback_changes)]
