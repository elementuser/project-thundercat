from django.db import connection
from django.db import migrations
from django.conf import settings


def create_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[bundle_association_vw] AS
                SELECT 
                    NULL as 'bundle_bundles_association_id',
                    bia.id as 'bundle_items_association_id', 
                    bia.bundle_source_id,
                    bia.system_id_link as 'system_id_link',
                    bia.version as 'version',
                    NULL as 'bundle_link_id',
                    bia.display_order
                FROM {db_name}..cms_models_bundleitemsassociation bia
                UNION
                SELECT 
                    bba.id as 'bundle_bundles_association_id',
                    NULL as 'bundle_items_association_id',
                    bba.bundle_source_id,
                    NULL as 'system_id_link',
                    NULL as 'version',
                    bba.bundle_link_id as 'bundle_link_id',
                    bba.display_order 
                FROM {db_name}..cms_models_bundlebundlesassociation bba
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute("DROP VIEW IF EXISTS [dbo].[bundle_association_vw]")


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0042_auto_20230523_1101",
        ),
    ]
    operations = [migrations.RunPython(create_vw, rollback_changes)]
