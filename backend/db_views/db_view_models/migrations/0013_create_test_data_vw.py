from django.db import connection
from django.db import migrations
from django.conf import settings


def create_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[test_data_vw] AS
				SELECT 
	at.id as 'assigned_test_id',
	at.status as 'test_status',
	IIF (hta.id is not NULL, hta.username, ta.username) as 'ta_username',
	IIF (hta.id is not NULL, hta.email, ta.email) as 'ta_email',
	IIF (hu.id is not NULL, hu.username, u.username) as 'candidate_username',
	IIF (hu.id is not NULL, hu.email, u.email) as 'candidate_email',
	IIF (hu.id is not NULL, hu.first_name, u.first_name) as 'candidate_first_name',
	IIF (hu.id is not NULL, hu.last_name, u.last_name) as 'candidate_last_name',
	td.id as 'test_id',
	td.en_name as 'en_test_name',
	td.fr_name as 'fr_test_name',
	td.version as 'test_version',
	IIF (htp.id is not NULL, htp.test_order_number, htp2.test_order_number) as 'test_order_number',
	IIF (htp.id is not NULL, htp.staffing_process_number, htp2.staffing_process_number) as 'staffing_process_number',
	htp3.ta_usernames as 'allowed_ta_usernames',
	ofd.reference_number as 'reference_number'
FROM {db_name}..custom_models_assignedtest at
OUTER APPLY (SELECT TOP 1 * FROM {db_name}..user_management_models_historicaluser u1 WHERE u1.username = at.ta_id AND u1.history_date < at.submit_date ORDER BY u1.history_date DESC) hta
JOIN {db_name}..user_management_models_user ta on ta.username = at.ta_id
OUTER APPLY (SELECT TOP 1 * FROM {db_name}..user_management_models_historicaluser u2 WHERE u2.username = at.username_id AND u2.history_date < at.submit_date ORDER BY u2.history_date DESC) hu
JOIN {db_name}..user_management_models_user u on u.username = at.username_id
JOIN {db_name}..cms_models_testdefinition td on td.id = at.test_id
OUTER APPLY (SELECT TOP 1 * FROM {db_name}..cms_models_historicaltestpermissions htp1 WHERE htp1.test_order_number = at.test_order_number AND htp1.history_date < at.submit_date ORDER BY htp1.history_date DESC) htp
OUTER APPLY (SELECT TOP 1 * FROM {db_name}..cms_models_historicaltestpermissions htp1 WHERE htp1.test_order_number = at.test_order_number ORDER BY htp1.history_date DESC) htp2
OUTER APPLY (SELECT string_agg(ta_username_list.username_id, ',') within group (ORDER BY ta_username_list.username_id) as 'ta_usernames' FROM (SELECT DISTINCT username_id FROM {db_name}..cms_models_historicaltestpermissions htp WHERE htp.test_order_number = at.test_order_number) ta_username_list) htp3
LEFT JOIN {db_name}..custom_models_orderlessfinancialdata ofd on ofd.id = at.orderless_financial_data_id
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute("DROP VIEW IF EXISTS [dbo].[test_data_vw]")


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0012_testorderandreferencenumbersvw",
        ),
    ]
    operations = [migrations.RunPython(create_vw, rollback_changes)]
