from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vws(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """ALTER VIEW [dbo].[item_option_vw] AS
				SELECT
                    io.id as 'item_option_id',
                    io.option_type,
                    io.option_order,
					io.exclude_from_shuffle,
                    io.modify_date as 'item_option_modify_date',
                    io.item_id,
                    io.score,
                    iot.id as 'item_option_text_id',
                    iot.text as 'text',
                    iot.modify_date as 'item_option_text_modify_date',
                    iot.language_id
                FROM {db_name}..cms_models_itemoption io
                LEFT JOIN {db_name}..cms_models_itemoptiontext iot on iot.item_option_id = io.id
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )

        cursor.execute(
            """ALTER VIEW [dbo].[item_option_drafts_vw] AS
				SELECT 
                    o.id as 'item_option_id',
                    o.system_id,
                    o.option_type,
                    o.modify_date as 'item_option_modify_date',
                    o.option_order,
					o.exclude_from_shuffle,
                    o.score,
                    o.item_id,
                    o.username_id,
                    ot.id as 'item_option_text_id',
                    ot.text,
                    ot.modify_date as 'item_option_text_modify_date',
                    ot.language_id
                FROM {db_name}..cms_models_itemoptiondrafts o
                JOIN {db_name}..cms_models_itemoptiontextdrafts ot on ot.item_option_id = o.id
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """ALTER VIEW [dbo].[item_option_vw] AS
				SELECT
                    io.id as 'item_option_id',
                    io.option_type,
                    io.option_order,
                    io.modify_date as 'item_option_modify_date',
                    io.item_id,
                    io.score,
                    iot.id as 'item_option_text_id',
                    iot.text as 'text',
                    iot.modify_date as 'item_option_text_modify_date',
                    iot.language_id
                FROM {db_name}..cms_models_itemoption io
                LEFT JOIN {db_name}..cms_models_itemoptiontext iot on iot.item_option_id = io.id
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )

        cursor.execute(
            """ALTER VIEW [dbo].[item_option_drafts_vw] AS
				SELECT 
                    o.id as 'item_option_id',
                    o.system_id,
                    o.option_type,
                    o.modify_date as 'item_option_modify_date',
                    o.option_order,
                    o.score,
                    o.item_id,
                    o.username_id,
                    ot.id as 'item_option_text_id',
                    ot.text,
                    ot.modify_date as 'item_option_text_modify_date',
                    ot.language_id
                FROM {db_name}..cms_models_itemoptiondrafts o
                JOIN {db_name}..cms_models_itemoptiontextdrafts ot on ot.item_option_id = o.id
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0031_auto_20230421_0943",
        ),
    ]
    operations = [migrations.RunPython(update_vws, rollback_changes)]
