from django.db import connection
from django.db import migrations
from django.conf import settings


def create_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[user_look_up_vw] AS
				SELECT
                    u.id as 'user_id',
                    u.first_name,
                    u.last_name,
                    u.birth_date as 'date_of_birth',
                    u.email,
                    u.username
                FROM {db_name}..user_management_models_user u
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute("DROP VIEW IF EXISTS [dbo].[user_look_up_vw]")


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0006_activetestsvw",
        ),
    ]
    operations = [migrations.RunPython(create_vw, rollback_changes)]
