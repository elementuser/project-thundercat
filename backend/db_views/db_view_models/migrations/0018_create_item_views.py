from django.db import connection
from django.db import migrations
from django.conf import settings


def create_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[item_latest_versions_data_vw] AS
				SELECT
					i.id as 'item_id',
					i.system_id,
					i.version,
					v.number_of_versions as 'number_of_versions',
					i.item_bank_id,
					i.modify_date,
					i.last_modified_by_username_id,
					--s.id as 'active_status_id',
					--s.codename as 'active_status_codename',
					--s.en_name as 'active_status_name_en',
					--s.fr_name as 'active_status_name_fr',
					ds.id as 'development_status_id',
					ds.codename as 'development_status_codename',
					ds.en_name as 'development_status_name_en',
					ds.fr_name as 'development_status_name_fr',
					rf.id as 'response_format_id',
					rf.codename as 'response_format_codename',
					rf.en_name as 'response_format_name_en',
					rf.fr_name as 'response_format_name_fr',
					IIF (hi.id is not NULL, hi.historical_id, NULL) as 'historical_id'
				FROM {db_name}..cms_models_items i
				--JOIN {db_name}..cms_models_itemactivestatuses s on s.id = i.active_status_id
				JOIN {db_name}..cms_models_itemdevelopmentstatuses ds on ds.id = i.development_status_id
				JOIN {db_name}..cms_models_itemresponseformats rf on rf.id = i.response_format_id
				LEFT JOIN {db_name}..cms_models_itemhistoricalids hi on hi.system_id = i.system_id
				OUTER APPLY (SELECT MAX(version) as 'number_of_versions' FROM {db_name}..cms_models_items it WHERE it.system_id = i.system_id) v
				WHERE i.id in (SELECT MAX(it.id) FROM {db_name}..cms_models_items it GROUP BY it.system_id)
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )

        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[all_items_data_vw] AS
				SELECT
					i.id as 'item_id',
					i.system_id,
					i.version,
					v.number_of_versions as 'number_of_versions',
					i.item_bank_id,
					i.modify_date,
					i.last_modified_by_username_id,
					--s.id as 'active_status_id',
					--s.codename as 'active_status_codename',
					--s.en_name as 'active_status_name_en',
					--s.fr_name as 'active_status_name_fr',
					ds.id as 'development_status_id',
					ds.codename as 'development_status_codename',
					ds.en_name as 'development_status_name_en',
					ds.fr_name as 'development_status_name_fr',
					rf.id as 'response_format_id',
					rf.codename as 'response_format_codename',
					rf.en_name as 'response_format_name_en',
					rf.fr_name as 'response_format_name_fr',
					IIF (hi.id is not NULL, hi.historical_id, NULL) as 'historical_id'
				FROM {db_name}..cms_models_items i
				--JOIN {db_name}..cms_models_itemactivestatuses s on s.id = i.active_status_id
				JOIN {db_name}..cms_models_itemdevelopmentstatuses ds on ds.id = i.development_status_id
				JOIN {db_name}..cms_models_itemresponseformats rf on rf.id = i.response_format_id
				LEFT JOIN {db_name}..cms_models_itemhistoricalids hi on hi.system_id = i.system_id
				OUTER APPLY (SELECT MAX(version) as 'number_of_versions' FROM {db_name}..cms_models_items it WHERE it.system_id = i.system_id) v
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute("DROP VIEW IF EXISTS [dbo].[item_latest_versions_data_vw]")
        cursor.execute("DROP VIEW IF EXISTS [dbo].[all_items_data_vw]")


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0017_update_test_data_vw",
        ),
    ]
    operations = [migrations.RunPython(create_vw, rollback_changes)]
