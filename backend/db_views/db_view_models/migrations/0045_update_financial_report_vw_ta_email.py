from django.db import connection
from django.db import migrations
from django.conf import settings


def update_financial_report_data_vw(apps, schema_editor):
    with connection.cursor() as cursor:

        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[financial_report_vw]
                    AS	

                    SELECT TOP 100 percent
                        IIF (u.id is null, v.last_name, u.last_name) as "candidate_last_name",
                        IIF (u.id is null, v.first_name, u.first_name) as "candidate_first_name",
                        IIF (u.id is null, v.pri_or_military_nbr, u.pri_or_military_nbr) as "candidate_pri",
                        ta.goc_email as "ta_email",
                        de.edesc as "ta_org_en",
                        de.fdesc as "ta_org_fr",
                        IIF (dp.edesc is null and a.orderless_financial_data_id is not null, odp.edesc, IIF(dp.edesc is null, 'Public Service Commission', dp.edesc)) as "requesting_dep_en",
                        IIF (dp.fdesc is null and a.orderless_financial_data_id is not null, odp.fdesc, IIF(dp.fdesc is null, 'Public Service Commission', dp.fdesc)) as "requesting_dep_fr",
                        a.test_order_number as "order_no",	
                        p.staffing_process_number as "assessment_process", 
                        o.reference_number as "reference_number",    
                        CASE 
                            WHEN a.orderless_financial_data_id is not null THEN o.department_ministry_id
                            WHEN ISNUMERIC(p.department_ministry_code) = 1 THEN (p.department_ministry_code) ELSE (104)
                        END as "org_code",
                        IIF (a.orderless_financial_data_id is not null, o.fis_organisation_code , p.is_org) as "fis_org_code",
                        IIF (a.orderless_financial_data_id is not null, o.fis_reference_code, p.is_ref) as "fis_ref_code",
                        IIF (a.orderless_financial_data_id is not null, o.billing_contact_name, p.billing_contact) as "billing_contact_name",
                        dbo.status2str('fr', a.status) AS "test_status_fr",
                        dbo.status2str('en', a.status) AS "test_status_en",     
                        a.is_invalid as "is_invalid",
                        cast(a.submit_date as date) as "submit_date",    
                        d.test_code as "test_code",
                        d.fr_name as "test_description_fr",
                        d.en_name as "test_description_en",
                        a.orderless_financial_data_id,
                        a.status as "test_status",		
                        a.test_id as "test_id",
                        a.id as "assigned_test_id"
                    FROM	
                        {db_name}..custom_models_assignedtest a
                        OUTER APPLY (SELECT TOP 1 id, last_name, first_name, pri_or_military_nbr FROM {db_name}..user_management_models_historicaluser u1 WHERE u1.username = a.username_id AND u1.history_date < a.submit_date ORDER BY u1.history_date DESC) u
                        OUTER APPLY (SELECT TOP 1 * FROM {db_name}..user_management_models_historicalpermissionrequest pr WHERE pr.username_id = a.ta_id and pr.history_type = '+' and pr.permission_requested_id = 1 and pr.history_date < a.submit_date order by history_date desc) ta
                        JOIN {db_name}..user_management_models_user v on v.username = a.username_id
                        LEFT JOIN {db_name}..user_management_models_taextendedprofile e ON e.username_id = a.ta_id
                        LEFT JOIN {db_name}..OLTF_DEPARTEMENTS_VW de ON de.DEPT_ID = e.department_id
                        OUTER APPLY (SELECT TOP 1 staffing_process_number, department_ministry_code, is_org, is_ref, billing_contact FROM {db_name}..cms_models_historicaltestpermissions h WHERE h.test_order_number = a.test_order_number AND h.username_id = a.ta_id AND h.history_date < a.submit_date AND h.history_type = '+' ORDER BY h.history_date DESC) p
                        LEFT JOIN {db_name}..OLTF_DEPARTEMENTS_VW dp ON cast(dp.DEPT_ID as nvarchar) = p.department_ministry_code
                        JOIN {db_name}..cms_models_testdefinition d ON d.id = a.test_id
                        LEFT JOIN {db_name}..custom_models_orderlessfinancialdata o ON o.id = a.orderless_financial_data_id
                        LEFT JOIN {db_name}..OLTF_DEPARTEMENTS_VW odp ON cast(odp.DEPT_ID as nvarchar) = o.department_ministry_id

                    WHERE
                        a.status IN (19, 20)

                    ORDER BY
                        a.test_order_number, a.test_id, a.status, a.id DESC""".format(
                                    db_name=settings.DATABASES["default"]["NAME"]
                                )
                            )


def rollback_changes(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[financial_report_vw]
                    AS	

                    SELECT TOP 100 percent
                        IIF (u.id is null, v.last_name, u.last_name) as "candidate_last_name",
                        IIF (u.id is null, v.first_name, u.first_name) as "candidate_first_name",
                        IIF (u.id is null, v.pri_or_military_nbr, u.pri_or_military_nbr) as "candidate_pri",
                        a.ta_id as "ta_email",
                        de.edesc as "ta_org_en",
                        de.fdesc as "ta_org_fr",
                        IIF (dp.edesc is null and a.orderless_financial_data_id is not null, odp.edesc, IIF(dp.edesc is null, 'Public Service Commission', dp.edesc)) as "requesting_dep_en",
                        IIF (dp.fdesc is null and a.orderless_financial_data_id is not null, odp.fdesc, IIF(dp.fdesc is null, 'Public Service Commission', dp.fdesc)) as "requesting_dep_fr",
                        a.test_order_number as "order_no",	
                        p.staffing_process_number as "assessment_process", 
                        o.reference_number as "reference_number",    
                        CASE 
                            WHEN a.orderless_financial_data_id is not null THEN o.department_ministry_id
                            WHEN ISNUMERIC(p.department_ministry_code) = 1 THEN (p.department_ministry_code) ELSE (104)
                        END as "org_code",
                        IIF (a.orderless_financial_data_id is not null, o.fis_organisation_code , p.is_org) as "fis_org_code",
                        IIF (a.orderless_financial_data_id is not null, o.fis_reference_code, p.is_ref) as "fis_ref_code",
                        IIF (a.orderless_financial_data_id is not null, o.billing_contact_name, p.billing_contact) as "billing_contact_name",
                        dbo.status2str('fr', a.status) AS "test_status_fr",
                        dbo.status2str('en', a.status) AS "test_status_en",     
                        a.is_invalid as "is_invalid",
                        cast(a.submit_date as date) as "submit_date",    
                        d.test_code as "test_code",
                        d.fr_name as "test_description_fr",
                        d.en_name as "test_description_en",
                        a.orderless_financial_data_id,
                        a.status as "test_status",		
                        a.test_id as "test_id",
                        a.id as "assigned_test_id"
                    FROM	
                        {db_name}..custom_models_assignedtest a
                        OUTER APPLY (SELECT TOP 1 id, last_name, first_name, pri_or_military_nbr FROM {db_name}..user_management_models_historicaluser u1 WHERE u1.username = a.username_id AND u1.history_date < a.submit_date ORDER BY u1.history_date DESC) u
                        JOIN {db_name}..user_management_models_user v on v.username = a.username_id
                        LEFT JOIN {db_name}..user_management_models_taextendedprofile e ON e.username_id = a.ta_id
                        LEFT JOIN {db_name}..OLTF_DEPARTEMENTS_VW de ON de.DEPT_ID = e.department_id
                        OUTER APPLY (SELECT TOP 1 staffing_process_number, department_ministry_code, is_org, is_ref, billing_contact FROM {db_name}..cms_models_historicaltestpermissions h WHERE h.test_order_number = a.test_order_number AND h.username_id = a.ta_id AND h.history_date < a.submit_date AND h.history_type = '+' ORDER BY h.history_date DESC) p
                        LEFT JOIN {db_name}..OLTF_DEPARTEMENTS_VW dp ON cast(dp.DEPT_ID as nvarchar) = p.department_ministry_code
                        JOIN {db_name}..cms_models_testdefinition d ON d.id = a.test_id
                        LEFT JOIN {db_name}..custom_models_orderlessfinancialdata o ON o.id = a.orderless_financial_data_id
                        LEFT JOIN {db_name}..OLTF_DEPARTEMENTS_VW odp ON cast(odp.DEPT_ID as nvarchar) = o.department_ministry_id

                    WHERE
                        a.status IN (19, 20)

                    ORDER BY
                        a.test_order_number, a.test_id, a.status, a.id DESC""".format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0044_bundleassociationvw",
        ),
    ]
    operations = [
        migrations.RunPython(update_financial_report_data_vw, rollback_changes)
    ]
