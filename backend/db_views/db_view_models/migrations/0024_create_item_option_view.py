from django.db import connection
from django.db import migrations
from django.conf import settings


def create_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[item_option_vw] AS
				SELECT
                    io.id as 'item_option_id',
                    io.option_type,
                    io.option_order,
                    io.modify_date as 'item_option_modify_date',
                    io.item_id,
                    io.score,
                    iot.id as 'item_option_text_id',
                    iot.text as 'text',
                    iot.modify_date as 'item_option_text_modify_date',
                    iot.language_id
                FROM {db_name}..cms_models_itemoption io
                LEFT JOIN {db_name}..cms_models_itemoptiontext iot on iot.item_option_id = io.id
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute("DROP VIEW IF EXISTS [dbo].[item_option_vw]")


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0023_itemcontentvw",
        ),
    ]
    operations = [migrations.RunPython(create_vw, rollback_changes)]
