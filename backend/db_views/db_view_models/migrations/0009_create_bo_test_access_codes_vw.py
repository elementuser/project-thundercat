from django.db import connection
from django.db import migrations
from django.conf import settings


def create_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[bo_test_access_codes_vw] AS
				SELECT
	tac.test_access_code,
	tac.test_order_number,
	ofd.reference_number,
	tac.created_date,
	ta.username as 'ta_username',
	ta.email as 'ta_email',
	ta.first_name as 'ta_first_name',
	ta.last_name as 'ta_last_name',
    up.goc_email as 'ta_goc_email',
	td.id as 'test_id',
	td.en_name as 'en_test_name',
	td.fr_name as 'fr_test_name'
  FROM {db_name}..custom_models_testaccesscode tac
  OUTER APPLY (SELECT TOP 1 * FROM {db_name}..user_management_models_historicaluser u1 WHERE u1.username = tac.ta_username_id ORDER BY u1.history_date DESC) ta
  JOIN {db_name}..cms_models_testdefinition td on td.id = tac.test_id
  LEFT JOIN {db_name}..custom_models_orderlessfinancialdata ofd on ofd.id = tac.orderless_financial_data_id
  OUTER APPLY (SELECT TOP 1 * FROM {db_name}..user_management_models_historicalcustomuserpermissions up1 WHERE up1.user_id = tac.ta_username_id AND up1.history_date < tac.created_date ORDER BY up1.history_date DESC) up

  UNION

  SELECT 
	utac.test_access_code,
	utac.test_order_number,
	ofd.reference_number,
	utac.created_date,
	ta.username as 'ta_username',
	ta.email as 'ta_email',
	ta.first_name as 'ta_first_name',
	ta.last_name as 'ta_last_name',
    up.goc_email as 'ta_goc_email',
	td.id as 'test_id',
	td.en_name as 'en_test_name',
	td.fr_name as 'fr_test_name'
  FROM {db_name}..custom_models_unsupervisedtestaccesscode utac
  OUTER APPLY (SELECT TOP 1 * FROM {db_name}..user_management_models_historicaluser u1 WHERE u1.username = utac.ta_username_id ORDER BY u1.history_date DESC) ta
  JOIN {db_name}..cms_models_testdefinition td on td.id = utac.test_id
  LEFT JOIN {db_name}..custom_models_orderlessfinancialdata ofd on ofd.id = utac.orderless_financial_data_id
  OUTER APPLY (SELECT TOP 1 * FROM {db_name}..user_management_models_historicalcustomuserpermissions up1 WHERE up1.user_id = utac.ta_username_id AND up1.history_date < utac.created_date ORDER BY up1.history_date DESC) up
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute("DROP VIEW IF EXISTS [dbo].[bo_test_access_codes_vw]")


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0008_userlookupvw",
        ),
    ]
    operations = [migrations.RunPython(create_vw, rollback_changes)]
