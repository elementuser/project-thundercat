from django.db import connection
from django.db import migrations
from django.conf import settings


def create_test_result_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[test_result_vw]
AS
	-- Person ID							[user_management_models_user] id 
	-- Username								[user_management_models_user] email
	-- Name									
		-- firstname						[user_management_models_user] lastname, 
		-- lastname							[user_management_models_user] firstname
	-- PRI									[user_management_models_user] pri_or_military_nbr
	-- Order No.								[custom_models_assignedtest] test_order_number
	-- Assessment Process / Reference Number	[cms_models_testpermissions] staffing_process_number 
	-- Test										[cms_models_testdefinition] test_code
	-- Test Description (FR)					[cms_models_testdefinition] fr_name 
	-- Test Description (EN)					[cms_models_testdefinition] en_name
	-- Test Start Date						[custom_models_assignedtest] start_date
	-- Score								[custom_models_assignedtest] total_score
	-- Level								[custom_models_assignedtest] en_converted_score / fr_converted_score (depends on is_invalid)
	-- Test Status							[custom_models_assignedtest] status (number converted to description)

	SELECT
		assigned_test_id,
		user_id, 
        user_firstname AS 'candidate_first_name',
        user_lastname AS 'candidate_last_name',
        username,
		uit_candidate_email,
		candidate_email,
		candidate_pri_or_military_nbr,
        ta_id,
		reference_number,
		test_order_number,
		process_number,
		history_id,
		td_test_code,
		td_fr_name,
		td_en_name,
		submit_date,
		test_score,
		CASE 
			WHEN test_is_invalid = 1 THEN 'Invalide'
			ELSE
				CASE 
					WHEN test_score IS NULL THEN '-' 
					WHEN test_fr_converted_score IS NULL THEN 'Conversion invalide du résultat'
				ELSE
					test_fr_converted_score
				END
		END AS 'level_fr',
		CASE 
			WHEN test_is_invalid = 1 THEN 'Invalid'
			ELSE	
				CASE
					WHEN test_score IS NULL THEN '-' 
					WHEN test_en_converted_score IS NULL THEN 'Invalid Score Conversion'
				ELSE
					test_en_converted_score
				END
		END AS 'level_en',
		dbo.status2str('fr', test_status) AS test_status_fr,
		dbo.status2str('en', test_status) AS test_status_en,
		test_status,
		test_id

	FROM
	(
		SELECT 
			at.id AS 'assigned_test_id',
			at.test_id,
            at.ta_id AS 'ta_id',
			ofd.reference_number AS 'reference_number',
			at.test_order_number AS 'test_order_number',
			at.submit_date AS 'submit_date',
			at.total_score AS 'test_score',
			at.en_converted_score AS 'test_en_converted_score',
			at.fr_converted_score AS 'test_fr_converted_score',
			at.status AS 'test_status',
			at.is_invalid AS 'test_is_invalid',			

			IIF(u.id is not null, u.id, v.id) AS 'user_id', 
            IIF(u.id is not null, u.username, v.username) AS 'username',
			t.candidate_email AS 'uit_candidate_email',
			IIF(u.id is not null, u.email, v.email) AS 'candidate_email', 
			IIF(u.id is not null, u.first_name, v.first_name) AS 'user_firstname', 
			IIF(u.id is not null, u.last_name, v.last_name) AS 'user_lastname', 	
			IIF(u.id is not null, u.pri_or_military_nbr, v.pri_or_military_nbr) AS 'candidate_pri_or_military_nbr',
	
			p.staffing_process_number AS 'process_number',
			p.history_id AS 'history_id',
	
			td.test_code AS 'td_test_code',
			td.fr_name AS 'td_fr_name',
			td.en_name AS 'td_en_name'	

		FROM {db_name}..custom_models_assignedtest at
		OUTER APPLY (SELECT TOP 1 * FROM {db_name}..user_management_models_historicaluser u1 WHERE u1.username = at.username_id AND u1.history_date < at.submit_date ORDER BY u1.history_date DESC) u
		JOIN {db_name}..user_management_models_user v on v.username = at.username_id
		OUTER APPLY (SELECT TOP 1 * FROM {db_name}..cms_models_historicaltestpermissions p1 WHERE p1.test_order_number = at.test_order_number AND p1.username_id = at.ta_id AND p1.history_date < at.submit_date ORDER BY p1.history_date DESC) p
		JOIN {db_name}..cms_models_testdefinition td ON td.id = at.test_id
		LEFT JOIN {db_name}..custom_models_orderlessfinancialdata ofd on at.orderless_financial_data_id = ofd.id
		OUTER APPLY (SELECT TOP 1 * FROM {db_name}..custom_models_historicalunsupervisedtestaccesscode t1 WHERE t1.uit_invite_id = at.uit_invite_id ORDER BY t1.history_date DESC) t
	) r""".format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute("DROP VIEW IF EXISTS [dbo].[test_result_vw]")


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0002_financialreportvw",
        ),
    ]
    operations = [migrations.RunPython(create_test_result_vw, rollback_changes)]
