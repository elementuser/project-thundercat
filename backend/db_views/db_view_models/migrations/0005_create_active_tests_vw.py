from django.db import connection
from django.db import migrations
from django.conf import settings


def create_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[active_tests_vw] AS
				SELECT
					at.id as 'assigned_test_id',
					u.first_name as 'candidate_first_name',
					u.last_name as 'candidate_last_name',
                    u.username as 'candidate_username',
					u.email as 'candidate_email',
					ta.email as 'ta_id',
					ta.first_name as 'ta_first_name',
					ta.last_name as 'ta_last_name',
					at.status as 'test_status',
					tp.test_order_number,
					ofd.reference_number,
					ats.time_limit,
                    td.id as 'test_id',
                    td.test_code
				FROM {db_name}..custom_models_assignedtest at
				OUTER APPLY (SELECT TOP 1 * FROM {db_name}..user_management_models_historicaluser u1 WHERE u1.username = at.username_id ORDER BY u1.history_date DESC) u
                OUTER APPLY (SELECT TOP 1 * FROM {db_name}..user_management_models_historicaluser u1 WHERE u1.username = at.ta_id ORDER BY u1.history_date DESC) ta
				OUTER APPLY (SELECT TOP 1 * FROM {db_name}..cms_models_historicaltestpermissions tp1 WHERE tp1.test_order_number = at.test_order_number AND tp1.username_id = at.ta_id ORDER BY tp1.history_date DESC) tp
				LEFT JOIN {db_name}..custom_models_orderlessfinancialdata ofd on ofd.id = at.orderless_financial_data_id
				OUTER APPLY (SELECT SUM(ats1.test_section_time) as 'time_limit' FROM {db_name}..custom_models_assignedtestsection ats1 WHERE ats1.assigned_test_id = at.id) ats
                LEFT JOIN {db_name}..cms_models_testdefinition td on td.id = at.test_id
				WHERE at.status in (11, 12, 13, 14, 15, 22)
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute("DROP VIEW IF EXISTS [dbo].[active_tests_vw]")


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0004_testresultreportvw",
        ),
    ]
    operations = [migrations.RunPython(create_vw, rollback_changes)]
