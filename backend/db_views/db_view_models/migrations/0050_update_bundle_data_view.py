from django.db import connection
from django.db import migrations
from django.conf import settings


def create_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[bundle_data_vw] AS
                SELECT * FROM
                    (SELECT
                        b.id,
                        b.name,
                        b.version_text,
                        b.shuffle_items,
                        b.shuffle_bundles,
                        b.shuffle_between_bundles,
                        b.active,
                        b.item_bank_id,
                        bundle_items_data.item_count
                    FROM {db_name}..cms_models_bundles b
                    OUTER APPLY (SELECT count(id) as 'item_count' 
                                FROM {db_name}..cms_models_bundleitemsassociation bia 
                                WHERE bia.bundle_source_id= b.id
                                ) bundle_items_data
                    ) a

                    OUTER APPLY (SELECT TOP 1 * FROM (
                                                SELECT count(bbra.id) as 'bundle_count' 
                                                    FROM {db_name}..cms_models_bundlebundlesruleassociations bbra 
                                                    JOIN {db_name}..cms_models_bundlerule br on br.bundle_id = a.id
                                                    JOIN {db_name}..cms_models_bundlebundlesrule bbr on bbr.bundle_rule_id = br.id
                                                    WHERE bbra.bundle_bundles_rule_id = bbr.id
                                                UNION 
                                                SELECT count(bundle_source_id) as 'bundle_count' 
                                                    FROM {db_name}..cms_models_bundlebundlesassociation bba 
                                                    WHERE bba.bundle_source_id= a.id
                                                ) bundle_rule_data
                                ORDER BY bundle_count DESC
                                ) bundle_count
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[bundle_data_vw] AS
                SELECT * FROM
                    (SELECT
                        b.id,
                        b.name,
                        b.version_text,
                        b.shuffle_items,
                        b.shuffle_bundles,
                        b.active,
                        b.item_bank_id,
                        bundle_items_data.item_count
                    FROM {db_name}..cms_models_bundles b
                    OUTER APPLY (SELECT count(id) as 'item_count' 
                                FROM {db_name}..cms_models_bundleitemsassociation bia 
                                WHERE bia.bundle_source_id= b.id
                                ) bundle_items_data
                    ) a

                    OUTER APPLY (SELECT TOP 1 * FROM (
                                                SELECT count(bbra.id) as 'bundle_count' 
                                                    FROM {db_name}..cms_models_bundlebundlesruleassociations bbra 
                                                    JOIN {db_name}..cms_models_bundlerule br on br.bundle_id = a.id
                                                    JOIN {db_name}..cms_models_bundlebundlesrule bbr on bbr.bundle_rule_id = br.id
                                                    WHERE bbra.bundle_bundles_rule_id = bbr.id
                                                UNION 
                                                SELECT count(bundle_source_id) as 'bundle_count' 
                                                    FROM {db_name}..cms_models_bundlebundlesassociation bba 
                                                    WHERE bba.bundle_source_id= a.id
                                                ) bundle_rule_data
                                ORDER BY bundle_count DESC
                                ) bundle_count
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0049_bundlebundlesruledatavw",
        ),
    ]
    operations = [migrations.RunPython(create_vw, rollback_changes)]
