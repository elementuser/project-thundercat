from django.db import connection
from django.db import migrations
from django.conf import settings


def create_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[bundle_bundles_rule_data_vw] AS
                SELECT 
                    br.id,
                    br.bundle_id,
                    bbr.id as 'bundle_bundles_rule_id',
                    bbr.number_of_bundles,
                    bbr.shuffle_bundles,
                    bbr.keep_items_together,
                    bbr.display_order as 'bundle_bundles_rule_display_order',
                    bbra.id as 'bundle_bundles_rule_associations_id',
                    bbra.bundle_id as 'bundle_bundles_rule_associations_bundle_id',
                    bbra.display_order
                FROM {db_name}..cms_models_bundlerule br
                JOIN {db_name}..cms_models_bundlebundlesrule bbr on bbr.bundle_rule_id = br.id
                JOIN {db_name}..cms_models_bundlebundlesruleassociations bbra on bbra.bundle_bundles_rule_id = bbr.id
                WHERE br.rule_type_id = (SELECT id from {db_name}..cms_models_bundleruletype WHERE codename = 'bundle_bundles_rule')
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute("DROP VIEW IF EXISTS [dbo].[bundle_bundles_rule_data_vw]")


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0047_bundledatavw",
        ),
    ]
    operations = [migrations.RunPython(create_vw, rollback_changes)]
