from django.db import connection
from django.db import migrations
from django.conf import settings


def create_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[test_order_and_reference_numbers_vw] AS
				SELECT
	test_order_number,
	reference_number,
	ta_username,
	IIF(u.id is not null, u.email, v.email) AS 'ta_email', 
	staffing_process_number
FROM (

	SELECT DISTINCT 
		htp.test_order_number as 'test_order_number', 
		NULL as 'reference_number',
		htp.username_id as 'ta_username', 
		htp.staffing_process_number as 'staffing_process_number'
	FROM {db_name}..cms_models_historicaltestpermissions htp

	UNION

	SELECT DISTINCT
		NULL as 'test_order_number',
		ofd.reference_number as 'reference_number',
		CASE
			WHEN htac.ta_username_id is not NULL THEN htac.ta_username_id
			WHEN hutac.ta_username_id is not NULL THEN hutac.ta_username_id
			WHEN at.orderless_financial_data_id is not NULL THEN at.ta_id
			WHEN ui.ta_username_id is not NULL THEN ui.ta_username_id
		ELSE
			NULL
		END as 'ta_username',
		NULL as 'staffing_process_number'
	FROM {db_name}..custom_models_orderlessfinancialdata ofd
	OUTER APPLY (SELECT TOP 1 * FROM {db_name}..custom_models_historicaltestaccesscode htac1 WHERE htac1.orderless_financial_data_id = ofd.id AND htac1.history_type = '+' AND htac1.orderless_financial_data_id is not NULL ORDER BY htac1.history_date DESC) htac
	OUTER APPLY (SELECT TOP 1 * FROM {db_name}..custom_models_historicalunsupervisedtestaccesscode hutac1 WHERE hutac1.orderless_financial_data_id = ofd.id AND hutac1.history_type = '+' AND hutac1.orderless_financial_data_id is not NULL ORDER BY hutac1.history_date DESC) hutac
	LEFT JOIN {db_name}..custom_models_assignedtest at on at.orderless_financial_data_id = ofd.id
	LEFT JOIN {db_name}..custom_models_uitinvites ui on ui.id = ofd.uit_invite_id
) r
OUTER APPLY (SELECT TOP 1 * FROM {db_name}..user_management_models_historicaluser u1 WHERE u1.username = r.ta_username ORDER BY u1.history_date DESC) u
JOIN {db_name}..user_management_models_user v on v.username = r.ta_username
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            "DROP VIEW IF EXISTS [dbo].[test_order_and_reference_numbers_vw]"
        )


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0010_botestaccesscodesvw",
        ),
    ]
    operations = [migrations.RunPython(create_vw, rollback_changes)]
