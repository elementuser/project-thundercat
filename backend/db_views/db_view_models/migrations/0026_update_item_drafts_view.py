from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """ALTER VIEW [dbo].[item_drafts_vw] AS
				SELECT
					id.item_id as 'item_id',
					id.system_id,
					id.item_bank_id,
					id.modify_date,
					id.username_id,
					--s.id as 'active_status_id',
					--s.codename as 'active_status_codename',
					--s.en_name as 'active_status_name_en',
					--s.fr_name as 'active_status_name_fr',
					ds.id as 'development_status_id',
					ds.codename as 'development_status_codename',
					ds.en_name as 'development_status_name_en',
					ds.fr_name as 'development_status_name_fr',
					rf.id as 'response_format_id',
					rf.codename as 'response_format_codename',
					rf.en_name as 'response_format_name_en',
					rf.fr_name as 'response_format_name_fr',
					IIF (hi.id is not NULL, hi.historical_id, NULL) as 'historical_id'
				FROM {db_name}..cms_models_itemdrafts id
				--JOIN {db_name}..cms_models_itemactivestatuses s on s.id = id.active_status_id
				JOIN {db_name}..cms_models_itemdevelopmentstatuses ds on ds.id = id.development_status_id
				JOIN {db_name}..cms_models_itemresponseformats rf on rf.id = id.response_format_id
				LEFT JOIN {db_name}..cms_models_itemhistoricalids hi on hi.system_id = id.system_id
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """ALTER VIEW [dbo].[item_drafts_vw] AS
				SELECT
					id.id as 'item_id',
					id.system_id,
					id.item_bank_id,
					id.modify_date,
					id.username_id,
					--s.id as 'active_status_id',
					--s.codename as 'active_status_codename',
					--s.en_name as 'active_status_name_en',
					--s.fr_name as 'active_status_name_fr',
					ds.id as 'development_status_id',
					ds.codename as 'development_status_codename',
					ds.en_name as 'development_status_name_en',
					ds.fr_name as 'development_status_name_fr',
					rf.id as 'response_format_id',
					rf.codename as 'response_format_codename',
					rf.en_name as 'response_format_name_en',
					rf.fr_name as 'response_format_name_fr',
					IIF (hi.id is not NULL, hi.historical_id, NULL) as 'historical_id'
				FROM {db_name}..cms_models_itemdrafts id
				--JOIN {db_name}..cms_models_itemactivestatuses s on s.id = id.active_status_id
				JOIN {db_name}..cms_models_itemdevelopmentstatuses ds on ds.id = id.development_status_id
				JOIN {db_name}..cms_models_itemresponseformats rf on rf.id = id.response_format_id
				LEFT JOIN {db_name}..cms_models_itemhistoricalids hi on hi.system_id = id.system_id
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0025_itemoptionvw",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
