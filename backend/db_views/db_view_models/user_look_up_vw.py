from django.db import models


class UserLookUpVW(models.Model):
    user_id = models.IntegerField(db_column="USER_ID", primary_key=True)
    first_name = models.EmailField(db_column="FIRST_NAME", max_length=30)
    last_name = models.EmailField(db_column="LAST_NAME", max_length=150)
    date_of_birth = models.DateField(db_column="DATE_OF_BIRTH")
    email = models.EmailField(db_column="EMAIL", max_length=254)
    username = models.EmailField(db_column="USERNAME", max_length=254)

    class Meta:
        managed = False
        db_table = "user_look_up_vw"
