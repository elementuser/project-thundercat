from django.db import models


class TestDataVW(models.Model):
    assigned_test_id = models.IntegerField(
        db_column="ASSIGNED_TEST_ID", primary_key=True
    )
    test_status = models.IntegerField(db_column="TEST_STATUS")
    ta_username = models.EmailField(db_column="TA_USERNAME", max_length=254)
    ta_email = models.EmailField(db_column="TA_EMAIL", max_length=254)
    candidate_username = models.EmailField(
        db_column="CANDIDATE_USERNAME", max_length=254
    )
    candidate_email = models.EmailField(db_column="CANDIDATE_EMAIL", max_length=254)
    candidate_first_name = models.EmailField(
        db_column="CANDIDATE_FIRST_NAME", max_length=30
    )
    candidate_last_name = models.EmailField(
        db_column="CANDIDATE_LAST_NAME", max_length=150
    )
    test_id = models.IntegerField(db_column="TEST_ID")
    en_test_name = models.CharField(db_column="EN_TEST_NAME", max_length=175)
    fr_test_name = models.CharField(db_column="FR_TEST_NAME", max_length=175)
    test_version = models.IntegerField(db_column="TEST_VERSION")
    test_order_number = models.CharField(db_column="TEST_ORDER_NUMBER", max_length=12)
    staffing_process_number = models.CharField(
        db_column="STAFFING_PROCESS_NUMBER",
        max_length=50,
    )
    allowed_ta_usernames = models.TextField(db_column="ALLOWED_TA_USERNAMES")
    reference_number = models.CharField(db_column="REFERENCE_NUMBER", max_length=25)

    class Meta:
        managed = False
        db_table = "test_data_vw"
