from django.db import models


class FinancialReportVW(models.Model):
    assigned_test_id = models.IntegerField(
        db_column="ASSIGNED_TEST_ID", primary_key=True
    )
    candidate_last_name = models.CharField(
        db_column="CANDIDATE_LAST_NAME", max_length=150, null=False, blank=False
    )
    candidate_first_name = models.CharField(
        db_column="CANDIDATE_FIRST_NAME", max_length=30, null=False, blank=False
    )
    candidate_pri = models.CharField(
        db_column="CANDIDATE_PRI", max_length=10, null=True, blank=True
    )
    ta_email = models.EmailField(
        db_column="TA_EMAIL", max_length=254, blank=False, null=False
    )
    ta_org_en = models.CharField(db_column="TA_ORG_EN", max_length=200, null=False)
    ta_org_fr = models.CharField(db_column="TA_ORG_FR", max_length=200, null=False)
    requesting_dep_en = models.CharField(
        db_column="REQUESTING_DEP_EN", max_length=200, null=False
    )
    requesting_dep_fr = models.CharField(
        db_column="REQUESTING_DEP_FR", max_length=200, null=False
    )
    order_no = models.CharField(
        db_column="ORDER_NO", max_length=12, blank=False, null=True
    )
    assessment_process = models.CharField(
        db_column="ASSESSMENT_PROCESS",
        max_length=50,
        blank=False,
        null=False,
        default="default",
    )
    org_code = models.CharField(
        db_column="ORG_CODE", max_length=16, null=False, default="104"
    )
    fis_org_code = models.CharField(
        db_column="FIS_ORG_CODE",
        max_length=16,
        blank=False,
        null=False,
        default="default",
    )
    fis_ref_code = models.CharField(
        db_column="FIS_REF_CODE",
        max_length=20,
        blank=False,
        null=False,
        default="default",
    )
    billing_contact_name = models.CharField(
        db_column="BILLING_CONTACT_NAME",
        max_length=180,
        blank=False,
        null=False,
        default="default",
    )
    is_invalid = models.BooleanField(db_column="IS_INVALID", default=False)
    submit_date = models.DateField(db_column="SUBMIT_DATE", blank=True, null=True)
    test_code = models.CharField(db_column="TEST_CODE", max_length=25)
    test_description_en = models.CharField(
        db_column="TEST_DESCRIPTION_EN", max_length=175
    )
    test_description_fr = models.CharField(
        db_column="TEST_DESCRIPTION_FR", max_length=175
    )
    orderless_financial_data_id = models.IntegerField(
        db_column="ORDERLESS_FINANCIAL_DATA_ID"
    )
    test_status = models.IntegerField(db_column="TEST_STATUS", blank=True, null=False)
    test_id = models.IntegerField(db_column="TEST_ID")
    reference_number = models.CharField(
        db_column="REFERENCE_NUMBER", max_length=25, null=False, blank=False
    )

    class Meta:
        managed = False
        db_table = "financial_report_vw"
