from django.db import models


class ItemDraftsVW(models.Model):
    item_id = models.IntegerField(db_column="ITEM_ID", primary_key=True)
    system_id = models.CharField(db_column="SYSTEM_ID", max_length=15)
    item_bank_id = models.IntegerField(db_column="ITEM_BANK_ID")
    modify_date = models.DateTimeField(db_column="MODIFY_DATE")
    username_id = models.EmailField(db_column="USERNAME_ID", max_length=254)
    active_status = models.BooleanField(db_column="ACTIVE_STATUS", default=0)
    shuffle_options = models.BooleanField(db_column="SHUFFLE_OPTIONS", default=0)
    development_status_id = models.IntegerField(db_column="DEVELOPMENT_STATUS_ID")
    development_status_codename = models.CharField(
        db_column="DEVELOPMENT_STATUS_CODENAME", max_length=50
    )
    development_status_name_en = models.CharField(
        db_column="DEVELOPMENT_STATUS_NAME_EN", max_length=25
    )
    development_status_name_fr = models.CharField(
        db_column="DEVELOPMENT_STATUS_NAME_FR", max_length=25
    )
    response_format_id = models.IntegerField(db_column="RESPONSE_FORMAT_ID")
    response_format_codename = models.CharField(
        db_column="RESPONSE_FORMAT_CODENAME", max_length=50
    )
    response_format_name_en = models.CharField(
        db_column="RESPONSE_FORMAT_NAME_EN", max_length=25
    )
    response_format_name_fr = models.CharField(
        db_column="RESPONSE_FORMAT_NAME_FR", max_length=25
    )
    historical_id = models.CharField(db_column="HISTORICAL_ID", max_length=50)

    class Meta:
        managed = False
        db_table = "item_drafts_vw"
