from django.db import models


class TestOrderAndReferenceNumbersVW(models.Model):
    ta_username = models.EmailField(
        db_column="TA_USERNAME", max_length=254, primary_key=True
    )
    ta_email = models.EmailField(db_column="TA_EMAIL", max_length=254)
    test_order_number = models.CharField(db_column="TEST_ORDER_NUMBER", max_length=12)
    staffing_process_number = models.CharField(
        db_column="STAFFING_PROCESS_NUMBER",
        max_length=50,
    )
    reference_number = models.CharField(db_column="REFERENCE_NUMBER", max_length=25)

    class Meta:
        managed = False
        db_table = "test_order_and_reference_numbers_vw"
