from rest_framework import serializers
from django.db.models import Count
from cms.cms_models.bundles import Bundles
from cms.cms_models.bundle_bundles_rule import BundleBundlesRule
from cms.cms_models.bundle_items_basic_rule import BundleItemsBasicRule
from cms.cms_models.bundle_rule import BundleRule
from cms.static.bundles_utils import BundleRuleTypeStatic
from cms.cms_models.bundle_rule_type import BundleRuleType
from cms.serializers.item_bank_serializer import (
    BundleAssociationSerializer,
    BundlesSimpleSerializer,
)
from db_views.db_view_models.bundle_association_vw import BundleAssociationVW
from db_views.db_view_models.bundle_data_vw import BundleDataVW
from db_views.db_view_models.bundle_bundles_rule_data_vw import BundleBundlesRuleDataVW


class BundleDataViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = BundleDataVW
        fields = "__all__"


class BundleDetailedDataViewSerializer(serializers.ModelSerializer):
    bundle_associations = serializers.SerializerMethodField()
    bundle_items_basic_rule_data = serializers.SerializerMethodField()
    bundle_bundles_rule_data = serializers.SerializerMethodField()

    def get_bundle_associations(self, request):
        bundle_associations = BundleAssociationVW.objects.filter(
            bundle_source_id=request.id
        ).order_by("display_order")

        return BundleAssociationSerializer(bundle_associations, many=True).data

    def get_bundle_items_basic_rule_data(self, request):
        bundle_items_basic_rule_type_id = BundleRuleType.objects.get(
            codename=BundleRuleTypeStatic.BUNDLE_ITEMS_BASIC
        ).id
        bundle_items_basic_rule = BundleRule.objects.filter(
            bundle_id=request.id, rule_type_id=bundle_items_basic_rule_type_id
        )

        # bundle_items_basic_rule exists
        if bundle_items_basic_rule:
            bundle_items_basic_rule_data = BundleItemsBasicRule.objects.filter(
                bundle_rule_id=bundle_items_basic_rule.last().id
            )
            return bundle_items_basic_rule_data.last().number_of_items

        # bundle_items_basic_rule does not exsit
        else:
            return ""

    def get_bundle_bundles_rule_data(self, request):
        bundle_bundles_rule_data = []
        grouped_bundle_bundles_rules = []

        # getting related_bundle_bundles_rules
        related_bundle_bundles_rules = BundleBundlesRuleDataVW.objects.filter(
            bundle_id=request.id
        ).order_by("bundle_bundles_rule_id")

        # looping in related_bundle_bundles_rule_data
        for related_bundle_bundles_rule in related_bundle_bundles_rules:
            # bundle_bundles_rule_id of current iteration does not exist in grouped_bundle_bundles_rules
            if not any(
                x["bundle_bundles_rule_id"]
                == related_bundle_bundles_rule.bundle_bundles_rule_id
                for x in grouped_bundle_bundles_rules
            ):
                # populating grouped_bundle_bundles_rules
                grouped_bundle_bundles_rules.append(
                    {
                        "id": related_bundle_bundles_rule.id,
                        "bundle_bundles_rule_id": related_bundle_bundles_rule.bundle_bundles_rule_id,
                        "number_of_bundles": related_bundle_bundles_rule.number_of_bundles,
                        "shuffle_bundles": related_bundle_bundles_rule.shuffle_bundles,
                        "keep_items_together": related_bundle_bundles_rule.keep_items_together,
                        "bundle_bundles_rule_display_order": related_bundle_bundles_rule.bundle_bundles_rule_display_order,
                    }
                )

        # grouped_bundle_bundles_rules exists
        if grouped_bundle_bundles_rules:
            # looping in grouped_bundle_bundles_rules
            for grouped_bundle_bundles_rule in grouped_bundle_bundles_rules:
                # initializing bundle_bundles_rule_associations
                bundle_bundles_rule_associations = []
                # getting rule data of current iteration
                rule_data_of_current_iteration = [
                    obj
                    for obj in related_bundle_bundles_rules
                    if obj.bundle_bundles_rule_id
                    == grouped_bundle_bundles_rule["bundle_bundles_rule_id"]
                ]
                # looping in rule_data_of_current_iteration
                for data in rule_data_of_current_iteration:
                    # populating bundle_bundles_rule_associations
                    bundle_bundles_rule_associations.append(
                        BundleBundlesRuleDataViewSerializer(data, many=False).data
                    )

                # populating bundle_bundles_rule_data
                bundle_bundles_rule_data.append(
                    {
                        "id": grouped_bundle_bundles_rule["bundle_bundles_rule_id"],
                        "bundle_bundles_rule_associations": bundle_bundles_rule_associations,
                        "number_of_bundles": grouped_bundle_bundles_rule[
                            "number_of_bundles"
                        ],
                        "shuffle_bundles": grouped_bundle_bundles_rule[
                            "shuffle_bundles"
                        ],
                        "keep_items_together": grouped_bundle_bundles_rule[
                            "keep_items_together"
                        ],
                        "display_order": grouped_bundle_bundles_rule[
                            "bundle_bundles_rule_display_order"
                        ],
                        "bundle_rule": grouped_bundle_bundles_rule["id"],
                    }
                )

        return bundle_bundles_rule_data

    class Meta:
        model = BundleDataVW
        fields = "__all__"


class BundleBundlesRuleDataViewSerializer(serializers.ModelSerializer):
    # that additional information is needed to have the exact same format as the one in the frontend (redux states)
    bundle_data = serializers.SerializerMethodField()
    bundle_bundles_association_id = serializers.SerializerMethodField()
    bundle_items_association_id = serializers.SerializerMethodField()
    bundle_link_id = serializers.SerializerMethodField()
    bundle_source_id = serializers.SerializerMethodField()
    item_data = serializers.SerializerMethodField()
    system_id_link = serializers.SerializerMethodField()
    version = serializers.SerializerMethodField()

    def get_bundle_data(self, request):
        bundle_data = Bundles.objects.get(
            id=request.bundle_bundles_rule_associations_bundle_id
        )
        return BundlesSimpleSerializer(bundle_data, many=False).data

    def get_bundle_bundles_association_id(self, request):
        return None

    def get_bundle_items_association_id(self, request):
        return None

    def get_bundle_link_id(self, request):
        return Bundles.objects.get(
            id=request.bundle_bundles_rule_associations_bundle_id
        ).id

    def get_bundle_source_id(self, request):
        return request.bundle_id

    def get_item_data(self, request):
        return None

    def get_system_id_link(self, request):
        return None

    def get_version(self, request):
        return None

    class Meta:
        model = BundleBundlesRuleDataVW
        fields = "__all__"
