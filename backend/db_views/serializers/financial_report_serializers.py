from db_views.db_view_models.financial_report_vw import FinancialReportVW
from rest_framework import serializers


class FinancialReportDataViewSerializer(serializers.ModelSerializer):
    department_description_en = serializers.SerializerMethodField()
    department_description_fr = serializers.SerializerMethodField()
    requesting_department_en = serializers.SerializerMethodField()
    requesting_department_fr = serializers.SerializerMethodField()

    # Doing this to escape commas
    def get_department_description_en(self, request):
        department_description_en = '"{0}"'.format(request.ta_org_en)
        return department_description_en

    # Doing this to escape commas
    def get_department_description_fr(self, request):
        department_description_fr = '"{0}"'.format(request.ta_org_fr)
        return department_description_fr

    # Doing this to escape commas
    def get_requesting_department_en(self, request):
        requesting_department_en = '"{0}"'.format(request.requesting_dep_en)
        return requesting_department_en

    # Doing this to escape commas
    def get_requesting_department_fr(self, request):
        requesting_department_fr = '"{0}"'.format(request.requesting_dep_fr)
        return requesting_department_fr

    class Meta:
        model = FinancialReportVW
        fields = "__all__"
