from db_views.db_view_models.active_tests_vw import ActiveTestsVW
from rest_framework import serializers


class ActiveTestsViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = ActiveTestsVW
        fields = "__all__"
