from db_views.db_view_models.user_look_up_vw import UserLookUpVW
from rest_framework import serializers


class UserLookUpViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserLookUpVW
        fields = "__all__"
