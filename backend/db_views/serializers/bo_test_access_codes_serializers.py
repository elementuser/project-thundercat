from db_views.db_view_models.bo_test_access_codes_vw import BoTestAccessCodesVW
from rest_framework import serializers


class BoTestAccessCodesViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = BoTestAccessCodesVW
        fields = "__all__"
