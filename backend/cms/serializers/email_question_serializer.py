from rest_framework import serializers
from cms.cms_models.email_question import EmailQuestion
from cms.cms_models.address_book_contact import AddressBookContact
from cms.serializers.address_book_contact_serializer import AddressBookContactSerializer
from cms.serializers.email_question_details_serializer import (
    EmailQuestionDetailsSerializer,
)
from cms.cms_models.email_question_details import EmailQuestionDetails


class EmailQuestionSerializer(serializers.ModelSerializer):
    to_field = AddressBookContactSerializer(read_only=True, many=True)
    from_field = serializers.SerializerMethodField()
    cc_field = AddressBookContactSerializer(read_only=True, many=True)
    details = serializers.SerializerMethodField()

    class Meta:
        model = EmailQuestion
        fields = "__all__"

    def to_representation(self, obj):
        """Move fields from Email Questino Details to user representation."""
        representation = super().to_representation(obj)
        details_representation = representation.pop("details")
        for key in details_representation:
            representation[key] = details_representation[key]

        return representation

    def get_details(self, request):
        language = self.context.get("language")

        email_details = EmailQuestionDetails.objects.get(
            email_question=request.id, language=language
        )
        return EmailQuestionDetailsSerializer(email_details, many=False).data

    def get_from_field(self, request):
        language = self.context.get("language")

        contact = AddressBookContact.objects.get(id=request.from_field.id)
        return AddressBookContactSerializer(
            contact, context={"language": language}
        ).data


class NoLanguageDataEmailQuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = EmailQuestion
        fields = "__all__"
