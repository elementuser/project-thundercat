from rest_framework import serializers
from cms.cms_models.address_book_contact import AddressBookContact
from cms.cms_models.address_book_contact_details import AddressBookContactDetails
from cms.serializers.address_book_contact_details_serializer import (
    AddressBookContactDetailsSerializer,
)


class TreeDescriptionAddressBookContactSerializer(serializers.ModelSerializer):

    team_information_tree_child = serializers.SerializerMethodField()
    text = serializers.SerializerMethodField()

    class Meta:
        model = AddressBookContact
        fields = "__all__"

    def get_team_information_tree_child(self, request):
        language = self.context.get("language")
        data = AddressBookContact.objects.filter(parent=request.id)
        if not data:
            return
        return TreeDescriptionAddressBookContactSerializer(
            data, many=True, context={"language": language}
        ).data

    def get_text(self, request):
        language = self.context.get("language")
        data = AddressBookContact.objects.get(id=request.id)
        details = AddressBookContactSerializer(
            data, context={"language": language}
        ).data

        return details


class AddressBookContactSerializer(serializers.ModelSerializer):
    details = serializers.SerializerMethodField()

    class Meta:
        model = AddressBookContact
        fields = "__all__"

    def to_representation(self, obj):
        """Move fields from ContactDetails to user representation."""
        representation = super().to_representation(obj)
        details_representation = representation.pop("details")
        for key in details_representation:
            if key != "id":
                representation[key] = details_representation[key]
        representation["label"] = "{0} ({1})".format(
            obj.name, details_representation["title"]
        )
        representation["value"] = obj.id
        return representation

    def get_details(self, request):
        language = self.context.get("language")
        data = AddressBookContactDetails.objects.get(
            contact=request.id, language=language
        )
        return AddressBookContactDetailsSerializer(
            data, context={"language": language}
        ).data
