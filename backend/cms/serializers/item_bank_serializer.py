from rest_framework import serializers
from random import shuffle
from ast import literal_eval
from itertools import groupby
from cms.cms_models.item_attribute_value_drafts import ItemAttributeValueDrafts
from cms.static.item_bank_utils import DRAFT_OPTION
from cms.cms_models.item_comments import ItemComments
from cms.static.item_bank_utils import AttributeValueType
from cms.cms_models.item_bank import ItemBank
from cms.cms_models.item_attribute_value import ItemAttributeValue
from cms.cms_models.item_bank_attributes import ItemBankAttributes
from cms.cms_models.item_bank_permissions import ItemBankPermissions
from cms.cms_models.item_bank_access_types import ItemBankAccessTypes
from cms.cms_models.item_response_formats import ItemResponseFormats
from cms.cms_models.item_development_statuses import ItemDevelopmentStatuses
from cms.cms_models.item_bank_attribute_values import ItemBankAttributeValues
from cms.cms_models.item_bank_attributes_text import ItemBankAttributesText
from cms.cms_models.item_bank_attribute_values_text import (
    ItemBankAttributeValuesText,
)
from cms.cms_models.bundles import Bundles

from db_views.db_view_models.item_latest_versions_data_vw import (
    ItemLatestVersionsDataVW,
)
from db_views.db_view_models.item_bank_attribute_values_vw import (
    ItemBankAttributeValuesVW,
)
from db_views.db_view_models.item_option_drafts_vw import ItemOptionDraftsVW
from db_views.db_view_models.all_items_data_vw import AllItemsDataVW
from db_views.db_view_models.item_drafts_vw import ItemDraftsVW
from db_views.db_view_models.item_content_vw import ItemContentVW
from db_views.db_view_models.item_option_vw import ItemOptionVW
from db_views.db_view_models.item_content_drafts_vw import ItemContentDraftsVW
from db_views.db_view_models.bundle_association_vw import BundleAssociationVW

from user_management.user_management_models.user_models import User
from backend.custom_models.language import Language
from backend.ref_table_views.cat_ref_departments_vw import CatRefDepartmentsVW
from backend.custom_models.language import Language
from backend.serializers.language_serializer import LanguageSerializer
from backend.serializers.cat_ref_departments_serializer import (
    CatRefDepartmentsSerializer,
)
from backend.static.psc_department import PscDepartment
from backend.custom_models.assigned_answer_choices import AssignedAnswerChoices
from cms.static.item_bank_utils import ItemContentType, ItemOptionType


# mirror of DRAGGABLE_ITEM_TYPE in ""..\frontend\src\components\commons\CustomDraggable\CustomDraggable.jsx"
class DraggableItemType:
    TEXT_AREA_COMPONENT = "TextAreaComponent"
    TEXT_OPTION_COMPONENT = "TestOptionComponent"


# this is shuffling the item options
# this is the mirror logic of the shuffle item options logic in the frontend (...\project-thundercat\frontend\src\helpers\itemBankItemPreview.js)
def shuffle_item_options(provided_item_options):
    # initializing needed variables
    final_options_array = []
    array_to_be_shuffled = []

    # looping in custom_inner_group
    for option_data in provided_item_options:
        # option of current iteration is excluded from shuffle
        if option_data["exclude_from_shuffle"]:
            # populating final_options_array with object value
            final_options_array.append(option_data)
        else:
            # populating final_options_array with null value
            final_options_array.append(None)
            # populating array_to_be_shuffled with object value
            array_to_be_shuffled.append(option_data)

    # shuffling array_to_be_shuffled array
    shuffle(array_to_be_shuffled)

    # combining final_options_array and array_to_be_shuffled together
    for index, final_option in enumerate(final_options_array):
        # value of current iteration is None
        if final_option == None:
            # overriding respective value in final_options_array
            final_options_array[index] = array_to_be_shuffled[0]
            # removing first element of array_to_be_shuffled
            array_to_be_shuffled.pop(0)

    return final_options_array


class ItemBankSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = ItemBank
        fields = "__all__"


class ItemBankSerializer(serializers.ModelSerializer):
    department_data = serializers.SerializerMethodField()
    action_buttons_disabled = serializers.SerializerMethodField()
    language_data = serializers.SerializerMethodField()

    def get_department_data(self, request):
        try:
            department_data = CatRefDepartmentsVW.objects.get(
                dept_id=request.department_id
            )
            serialized_data = CatRefDepartmentsSerializer(
                department_data, many=False
            ).data
            return serialized_data
        except:
            return {
                "dept_id": PscDepartment.DEPT_ID,
                "eabrv": PscDepartment.EABRV,
                "fabrv": PscDepartment.FABRV,
                "edesc": PscDepartment.EDESC,
                "fdesc": PscDepartment.FDESC,
            }

    def get_action_buttons_disabled(self, request):
        allowed_item_bank_ids = self.context.get("allowed_item_bank_ids", None)
        if allowed_item_bank_ids is not None:
            if request.id in allowed_item_bank_ids:
                return False
            return True
        else:
            return True

    def get_language_data(self, request):
        language_data = None
        if request.language_id is not None:
            language_data = LanguageSerializer(
                Language.objects.get(language_id=request.language_id), many=False
            ).data
        return language_data

    class Meta:
        model = ItemBank
        fields = "__all__"


class ItemBankAttributeValuesSerializer(serializers.ModelSerializer):
    order = serializers.IntegerField(source="attribute_value_order")
    value = serializers.SerializerMethodField()

    class Meta:
        model = ItemBankAttributeValues
        fields = [
            "id",
            "order",
            "item_bank_attribute_id",
            "attribute_value_type",
            "value",
        ]

    def get_value(self, obj):
        # get the correct value depending on the type of value
        if obj.attribute_value_type == AttributeValueType.TEXT:
            try:
                value_obj = ItemBankAttributeValuesText.objects.get(
                    item_bank_attribute_values_id=obj.id
                )
                return ItemBankAttributeValuesTextSerializer(value_obj).data
            except ItemBankAttributeValuesText.DoesNotExist:
                return {}

        return {}


class ItemBankAttributeValuesTextSerializer(serializers.ModelSerializer):
    class Meta:
        model = ItemBankAttributeValuesText
        fields = ["text"]


class ItemBankAttributeTextSerializer(serializers.ModelSerializer):
    class Meta:
        model = ItemBankAttributesText
        fields = ["id", "text", "modify_date", "item_bank_attribute_id", "language_id"]


class ItemBankAttributesSerializer(serializers.ModelSerializer):
    item_bank_attribute_text = serializers.SerializerMethodField()
    item_bank_attribute_values = serializers.SerializerMethodField()
    order = serializers.IntegerField(source="attribute_order")

    def get_item_bank_attribute_text(self, request):
        item_bank_attribute_text = ItemBankAttributesText.objects.filter(
            item_bank_attribute_id=request.id
        ).order_by("language_id")

        serialized_data = ItemBankAttributeTextSerializer(
            item_bank_attribute_text, many=True
        ).data

        # getting language data
        language_data = Language.objects.all()

        # initializing final_obj
        final_obj = {}

        # grouping text by language
        for obj, inner_group in groupby(serialized_data, lambda x: x["language_id"]):
            for language in language_data:
                # matching language
                if obj == language.language_id:
                    # populating obj
                    final_obj["{0}".format(language.ISO_Code_1)] = list(inner_group)

        # returning final_obj
        return final_obj

    def get_item_bank_attribute_values(self, request):
        item_bank_attribute_values = ItemBankAttributeValues.objects.filter(
            item_bank_attribute_id=request.id
        ).order_by("attribute_value_order")
        serialized_data = ItemBankAttributeValuesSerializer(
            item_bank_attribute_values, many=True
        ).data
        return serialized_data

    class Meta:
        model = ItemBankAttributes
        fields = [
            "id",
            "order",
            "item_bank_id",
            "item_bank_attribute_text",
            "item_bank_attribute_values",
        ]


class SelectedItemBankDataSerializer(serializers.ModelSerializer):
    item_bank_definition = serializers.SerializerMethodField()
    item_bank_attributes = serializers.SerializerMethodField()
    item_bank_items = serializers.SerializerMethodField()

    def get_item_bank_definition(self, request):
        language_data = request.language_id
        if language_data is not None:
            language_data = LanguageSerializer(
                Language.objects.get(language_id=request.language_id), many=False
            ).data

        item_bank_definition_obj = {
            "id": request.id,
            "custom_item_bank_id": request.custom_item_bank_id,
            "language_id": request.language_id,
            "language_data": language_data,
            "en_name": request.en_name,
            "fr_name": request.fr_name,
        }
        return [item_bank_definition_obj]

    def get_item_bank_attributes(self, request):
        item_bank_attributes = ItemBankAttributes.objects.filter(
            item_bank_id=request.id
        ).order_by("attribute_order")
        serialized_data = ItemBankAttributesSerializer(
            item_bank_attributes, many=True
        ).data
        return serialized_data

    def get_item_bank_items(self, request):
        # getting needed items
        latest_versions_items = ItemLatestVersionsDataVW.objects.filter(
            item_bank_id=request.id
        )

        # ordering items by custom System ID (ascending)
        ordered_items = sorted(
            latest_versions_items,
            # converting second part of string (incrementing value) to int, so the ordering is respected
            key=lambda k: int(k.system_id.split("_")[1]),
            reverse=False,
        )

        # initializing needed variables
        all_items_including_drafts = ItemLatestVersionsDataViewSerializer(
            ordered_items, many=True
        ).data

        related_item_drafts = []

        # making sure we have at least one item (to avoid errors)
        if all_items_including_drafts:
            # getting all related item drafts
            related_item_drafts = ItemDraftsVW.objects.filter(
                # using the first element to get the item bank ID
                item_bank_id=all_items_including_drafts[0]["item_bank_id"],
                username_id=self.context.get("username"),
            )

        # looping in all_items_including_drafts
        for index, item_data in enumerate(all_items_including_drafts):
            # trying to get the index of the current related item draft (if it exists)
            try:
                index_of_current_related_item_draft = [
                    x.system_id for x in related_item_drafts
                ].index(item_data["system_id"])

                context = {}
                context["contains_draft"] = True
                # updating the respective object with the item draft data
                all_items_including_drafts[index] = ItemDraftsViewSerializer(
                    related_item_drafts[index_of_current_related_item_draft],
                    many=False,
                    context=context,
                ).data
            except:
                pass

        return all_items_including_drafts

    class Meta:
        model = ItemBank
        fields = [
            "item_bank_definition",
            "item_bank_attributes",
            "item_bank_items",
        ]


class ItemBankAttributeValuesViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = ItemBankAttributeValuesVW
        fields = "__all__"


class ItemBankAccessTypesSerializer(serializers.ModelSerializer):
    class Meta:
        model = ItemBankAccessTypes
        fields = "__all__"


class ItemBankAccessesSerializer(serializers.ModelSerializer):
    test_developer_data = serializers.SerializerMethodField()
    item_bank_access_type_data = serializers.SerializerMethodField()

    def get_item_bank_access_type_data(self, request):
        item_bank_access_type_data = ItemBankAccessTypes.objects.get(
            id=request.item_bank_access_type_id
        )
        serialized_data = ItemBankAccessTypesSerializer(
            item_bank_access_type_data, many=False
        ).data
        return serialized_data

    def get_test_developer_data(self, request):
        test_developer_data = User.objects.get(username=request.username_id)
        return {
            "first_name": test_developer_data.first_name,
            "last_name": test_developer_data.last_name,
            "email": test_developer_data.email,
        }

    class Meta:
        model = ItemBankPermissions
        fields = "__all__"


class SelectedRdOperationsItemBankDataSerializer(serializers.ModelSerializer):
    item_bank_permissions = serializers.SerializerMethodField()
    department_data = serializers.SerializerMethodField()

    def get_item_bank_permissions(self, request):
        item_bank_permissions = ItemBankPermissions.objects.filter(
            item_bank_id=request.id
        )
        serialized_data = ItemBankAccessesSerializer(
            item_bank_permissions, many=True
        ).data
        return serialized_data

    def get_department_data(self, request):
        department_data = CatRefDepartmentsVW.objects.filter(
            dept_id=request.department_id
        )
        # department found
        if department_data:
            serialized_data = CatRefDepartmentsSerializer(
                department_data[0], many=False
            ).data
            return serialized_data
        else:
            return {
                "dept_id": PscDepartment.DEPT_ID,
                "eabrv": PscDepartment.EABRV,
                "fabrv": PscDepartment.FABRV,
                "edesc": PscDepartment.EDESC,
                "fdesc": PscDepartment.FDESC,
            }

    class Meta:
        model = ItemBank
        fields = "__all__"


class ItemCommentsSerializer(serializers.ModelSerializer):
    user_first_name = serializers.SerializerMethodField()
    user_last_name = serializers.SerializerMethodField()

    def get_user_first_name(self, request):
        user_first_name = User.objects.get(username=request.username_id).first_name
        return user_first_name

    def get_user_last_name(self, request):
        user_last_name = User.objects.get(username=request.username_id).last_name
        return user_last_name

    class Meta:
        model = ItemComments
        fields = "__all__"


class ItemContentViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = ItemContentVW
        fields = "__all__"


class ItemOptionDraftsViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = ItemOptionDraftsVW
        fields = "__all__"


class ItemContentDraftsViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = ItemContentDraftsVW
        fields = "__all__"


class ItemOptionViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = ItemOptionVW
        fields = "__all__"


class ItemOptionTestTakerReportSerializer(serializers.ModelSerializer):
    # making sure we have the same terms as questions from the Test Builder
    # ======================================================
    id = serializers.SerializerMethodField()
    scoring_value = serializers.SerializerMethodField()
    ppc_answer_id = serializers.SerializerMethodField()
    order = serializers.SerializerMethodField()
    # ======================================================
    # presentation order
    assigned_answer_choice_order = serializers.SerializerMethodField()

    def get_id(self, request):
        return request.item_option_id

    def get_scoring_value(self, request):
        return request.score

    def get_ppc_answer_id(self, request):
        return request.historical_option_id

    def get_order(self, request):
        return request.option_order

    def get_assigned_answer_choice_order(self, request):
        # if context["assigned_test_id"] is defined
        if self.context.get("assigned_test_id", None):
            try:
                # getting assigned answer choices
                assigned_answer_choices = literal_eval(
                    AssignedAnswerChoices.objects.get(
                        assigned_test_id=self.context["assigned_test_id"],
                        item_id=request.item_id,
                    ).answer_choices
                )
                # getting order
                assigned_answer_choice_order = (
                    assigned_answer_choices.index(request.item_option_id) + 1
                )
                return assigned_answer_choice_order
            except:
                return None
        return None

    class Meta:
        model = ItemOptionVW
        fields = "__all__"


class AllItemsDataViewSerializer(serializers.ModelSerializer):
    available_versions = serializers.SerializerMethodField()
    item_content = serializers.SerializerMethodField()
    item_options = serializers.SerializerMethodField()
    item_comments = serializers.SerializerMethodField()
    item_attribute_values = serializers.SerializerMethodField()

    def get_available_versions(self, request):
        contains_draft = self.context.get("contains_draft", None)
        available_versions = AllItemsDataVW.objects.filter(
            system_id=request.system_id
        ).values("item_id", "version")

        # if contains draft
        if contains_draft:
            available_versions = list(available_versions)
            available_versions.append(DRAFT_OPTION)

        return available_versions

    def get_item_content(self, request):
        # getting item content and ordering by language ID
        item_content = ItemContentVW.objects.filter(item_id=request.item_id).order_by(
            "language_id"
        )

        # getting serialized data
        serialized_data = ItemContentViewSerializer(item_content, many=True).data

        updated_serialized_data = []

        # looping in serialized_data
        for data in serialized_data:
            # adding attributes needed in the frontend logic
            obj = data
            obj["content_type"] = data["content_type"]
            obj["item_type"] = DraggableItemType.TEXT_AREA_COMPONENT
            updated_serialized_data.append(obj)

        # getting language data
        language_data = Language.objects.all()

        # initializing final_obj
        final_obj = {}

        # grouping text by language
        for obj, inner_group in groupby(
            updated_serialized_data, lambda x: x["language_id"]
        ):
            for language in language_data:
                # matching language
                if obj == language.language_id:
                    # initializing needed variables
                    custom_inner_group = list(inner_group)
                    markdown_array = []
                    screen_reader_array = []

                    for data in custom_inner_group:
                        # MARKDOWN
                        if data["content_type"] == ItemContentType.MARKDOWN:
                            # populating markdown_array
                            markdown_array.append(data)
                        # SCREEN READER
                        elif data["content_type"] == ItemContentType.SCREEN_READER:
                            # populating screen_reader_array
                            screen_reader_array.append(data)
                        # overwriting id of current iteration
                        # note that if you update this ID, you'll need to update it in the frontend as well (needed for draggable logic)
                        # file ref: ...\frontend\src\components\testBuilder\itemBank\items\ContentTab.jsx
                        # MARKDOWN
                        if data["content_type"] == ItemContentType.MARKDOWN:
                            data["id"] = "item-stem-{0}-{1}".format(
                                language.ISO_Code_1, (len(markdown_array))
                            )
                        # SCREEN READER
                        elif data["content_type"] == ItemContentType.SCREEN_READER:
                            data["id"] = "item-stem-screen-reader-{0}-{1}".format(
                                language.ISO_Code_1, (len(screen_reader_array))
                            )

                    # sorting by content type / content_order
                    custom_inner_group.sort(
                        key=lambda x: (x["content_type"], x["content_order"]),
                        reverse=False,
                    )

                    # populating obj
                    final_obj["{0}".format(language.ISO_Code_1)] = custom_inner_group

        # returning final_obj
        return final_obj

    def get_item_options(self, request):
        # getting item options and ordering by language ID
        item_options = ItemOptionVW.objects.filter(item_id=request.item_id).order_by(
            "language_id"
        )

        # getting serialized data
        serialized_data = ItemOptionViewSerializer(item_options, many=True).data

        updated_serialized_data = []

        # looping in serialized_data
        for data in serialized_data:
            # adding attributes needed in the frontend logic
            obj = data
            obj["option_type"] = data["option_type"]
            obj["item_type"] = DraggableItemType.TEXT_OPTION_COMPONENT
            updated_serialized_data.append(obj)

        # getting language data
        language_data = Language.objects.all()

        # initializing final_obj
        final_obj = {}

        # grouping text by language
        for obj, inner_group in groupby(
            updated_serialized_data, lambda x: x["language_id"]
        ):
            for language in language_data:
                # matching language
                if obj == language.language_id:
                    # initializing needed variables
                    custom_inner_group = list(inner_group)
                    multiple_choice_array = []
                    multiple_choice_screen_reader_array = []

                    for data in custom_inner_group:
                        # MULTIPLE CHOICE
                        if data["option_type"] == ItemOptionType.MULTIPLE_CHOICE:
                            # populating multiple_choice_array
                            multiple_choice_array.append(data)
                        # MULTIPLE CHOICE SCREEN READER
                        elif (
                            data["option_type"]
                            == ItemOptionType.MULTIPLE_CHOICE_SCREEN_READER
                        ):
                            # populating multiple_choice_screen_reader_array
                            multiple_choice_screen_reader_array.append(data)
                        # overwriting id of current iteration
                        # note that if you update this ID, you'll need to update it in the frontend as well (needed for draggable logic)
                        # file ref: ...\frontend\src\components\testBuilder\itemBank\items\ContentTab.jsx
                        # MULTIPLE CHOICE
                        if data["option_type"] == ItemContentType.MARKDOWN:
                            data["id"] = "item-option-{0}-{1}".format(
                                language.ISO_Code_1, (len(multiple_choice_array))
                            )
                        # MULTIPLE CHOICE SCREEN READER
                        elif data["option_type"] == ItemContentType.SCREEN_READER:
                            data["id"] = "item-option-screen-reader-{0}-{1}".format(
                                language.ISO_Code_1,
                                (len(multiple_choice_screen_reader_array)),
                            )

                    # sorting by option type / option_order
                    custom_inner_group.sort(
                        key=lambda x: (x["option_type"], x["option_order"]),
                        reverse=False,
                    )

                    # shuffle options is enables
                    if request.shuffle_options:
                        final_options_array = shuffle_item_options(custom_inner_group)

                        # overriding custom_inner_group with final_options_array
                        custom_inner_group = final_options_array

                    # populating obj
                    final_obj["{0}".format(language.ISO_Code_1)] = custom_inner_group

        # returning final_obj
        return final_obj

    def get_item_comments(self, request):
        item_comments = ItemComments.objects.filter(
            system_id=request.system_id
        ).order_by("-modify_date")
        return ItemCommentsSerializer(item_comments, many=True).data

    def get_item_attribute_values(self, request):
        # List of attributes available for the current Item Bank
        item_bank_attributes = ItemBankAttributes.objects.filter(
            item_bank_id=request.item_bank_id
        ).order_by("attribute_order")

        # List of item_attribute_values that we return for the current item
        item_attribute_values_final = []

        for item_bank_attribute in item_bank_attributes:
            item_bank_attribute_values = ItemBankAttributeValuesVW.objects.filter(
                item_bank_attribute_id=item_bank_attribute.id
            )
            item_attribute_value_array = []

            for item_bank_attribute_value in item_bank_attribute_values:
                # Test for Item Attribute Value
                item_attribute_value = ItemAttributeValue.objects.filter(
                    item_bank_attribute_value=item_bank_attribute_value.item_bank_attribute_value_id,
                    item_id=request.item_id,
                )
                if item_attribute_value:
                    item_attribute_value_array = [item_bank_attribute_value]
                    break
            if item_attribute_value_array:
                item_attribute_values_final.append(
                    ItemBankAttributeValuesViewSerializer(
                        item_attribute_value_array[0], many=False
                    ).data
                )
            else:
                item_attribute_values_final.append(
                    {
                        "item_bank_attribute_value_id": "N/A",
                        "text": "N/A",
                        "attribute_text_en": item_bank_attribute_values[
                            0
                        ].attribute_text_en,
                        "attribute_text_fr": item_bank_attribute_values[
                            0
                        ].attribute_text_fr,
                    }
                )

        return item_attribute_values_final

    class Meta:
        model = AllItemsDataVW
        fields = "__all__"


class ItemLatestVersionsDataViewSerializer(serializers.ModelSerializer):
    available_versions = serializers.SerializerMethodField()
    item_content = serializers.SerializerMethodField()
    item_options = serializers.SerializerMethodField()
    item_comments = serializers.SerializerMethodField()
    item_attribute_values = serializers.SerializerMethodField()

    def get_available_versions(self, request):
        contains_draft = self.context.get("contains_draft", None)
        available_versions = AllItemsDataVW.objects.filter(
            system_id=request.system_id
        ).values("item_id", "version")

        # if contains draft
        if contains_draft:
            available_versions = list(available_versions)
            available_versions.append(DRAFT_OPTION)

        return available_versions

    def get_item_content(self, request):
        # getting item content and ordering by language ID
        item_content = ItemContentVW.objects.filter(item_id=request.item_id).order_by(
            "language_id"
        )

        # getting serialized data
        serialized_data = ItemContentViewSerializer(item_content, many=True).data

        updated_serialized_data = []

        # looping in serialized_data
        for data in serialized_data:
            # adding attributes needed in the frontend logic
            obj = data
            obj["content_type"] = data["content_type"]
            obj["item_type"] = DraggableItemType.TEXT_AREA_COMPONENT
            updated_serialized_data.append(obj)

        # getting language data
        language_data = Language.objects.all()

        # initializing final_obj
        final_obj = {}

        # grouping text by language
        for obj, inner_group in groupby(
            updated_serialized_data, lambda x: x["language_id"]
        ):
            for language in language_data:
                # matching language
                if obj == language.language_id:
                    # initializing needed variables
                    custom_inner_group = list(inner_group)
                    markdown_array = []
                    screen_reader_array = []

                    for data in custom_inner_group:
                        # MARKDOWN
                        if data["content_type"] == ItemContentType.MARKDOWN:
                            # populating markdown_array
                            markdown_array.append(data)
                        # SCREEN READER
                        elif data["content_type"] == ItemContentType.SCREEN_READER:
                            # populating screen_reader_array
                            screen_reader_array.append(data)
                        # overwriting id of current iteration
                        # note that if you update this ID, you'll need to update it in the frontend as well (needed for draggable logic)
                        # file ref: ...\frontend\src\components\testBuilder\itemBank\items\ContentTab.jsx
                        # MARKDOWN
                        if data["content_type"] == ItemContentType.MARKDOWN:
                            data["id"] = "item-stem-{0}-{1}".format(
                                language.ISO_Code_1, (len(markdown_array))
                            )
                        # SCREEN READER
                        elif data["content_type"] == ItemContentType.SCREEN_READER:
                            data["id"] = "item-stem-screen-reader-{0}-{1}".format(
                                language.ISO_Code_1, (len(screen_reader_array))
                            )

                    # sorting by content type / content_order
                    custom_inner_group.sort(
                        key=lambda x: (x["content_type"], x["content_order"]),
                        reverse=False,
                    )

                    # populating obj
                    final_obj["{0}".format(language.ISO_Code_1)] = custom_inner_group

        # returning final_obj
        return final_obj

    def get_item_options(self, request):
        # getting item options and ordering by language ID
        item_options = ItemOptionVW.objects.filter(item_id=request.item_id).order_by(
            "language_id"
        )

        # getting serialized data
        serialized_data = ItemOptionViewSerializer(item_options, many=True).data

        updated_serialized_data = []

        # looping in serialized_data
        for data in serialized_data:
            # adding attributes needed in the frontend logic
            obj = data
            obj["option_type"] = data["option_type"]
            obj["item_type"] = DraggableItemType.TEXT_OPTION_COMPONENT
            updated_serialized_data.append(obj)

        # getting language data
        language_data = Language.objects.all()

        # initializing final_obj
        final_obj = {}

        # grouping text by language
        for obj, inner_group in groupby(
            updated_serialized_data, lambda x: x["language_id"]
        ):
            for language in language_data:
                # matching language
                if obj == language.language_id:
                    # initializing needed variables
                    custom_inner_group = list(inner_group)
                    multiple_choice_array = []
                    multiple_choice_screen_reader_array = []

                    for data in custom_inner_group:
                        # MULTIPLE CHOICE
                        if data["option_type"] == ItemOptionType.MULTIPLE_CHOICE:
                            # populating multiple_choice_array
                            multiple_choice_array.append(data)
                        # MULTIPLE CHOICE SCREEN READER
                        elif (
                            data["option_type"]
                            == ItemOptionType.MULTIPLE_CHOICE_SCREEN_READER
                        ):
                            # populating multiple_choice_screen_reader_array
                            multiple_choice_screen_reader_array.append(data)
                        # overwriting id of current iteration
                        # note that if you update this ID, you'll need to update it in the frontend as well (needed for draggable logic)
                        # file ref: ...\frontend\src\components\testBuilder\itemBank\items\ContentTab.jsx
                        # MULTIPLE CHOICE
                        if data["option_type"] == ItemContentType.MARKDOWN:
                            data["id"] = "item-option-{0}-{1}".format(
                                language.ISO_Code_1, (len(multiple_choice_array))
                            )
                        # MULTIPLE CHOICE SCREEN READER
                        elif data["option_type"] == ItemContentType.SCREEN_READER:
                            data["id"] = "item-option-screen-reader-{0}-{1}".format(
                                language.ISO_Code_1,
                                (len(multiple_choice_screen_reader_array)),
                            )

                    # sorting by option type / option_order
                    custom_inner_group.sort(
                        key=lambda x: (x["option_type"], x["option_order"]),
                        reverse=False,
                    )

                    # shuffle options is enables
                    if request.shuffle_options:
                        final_options_array = shuffle_item_options(custom_inner_group)

                        # overriding custom_inner_group with final_options_array
                        custom_inner_group = final_options_array

                    # populating obj
                    final_obj["{0}".format(language.ISO_Code_1)] = custom_inner_group

        # returning final_obj
        return final_obj

    def get_item_comments(self, request):
        item_comments = ItemComments.objects.filter(
            system_id=request.system_id
        ).order_by("-modify_date")
        return ItemCommentsSerializer(item_comments, many=True).data

    def get_item_attribute_values(self, request):
        # List of attributes available for the current Item Bank
        item_bank_attributes = ItemBankAttributes.objects.filter(
            item_bank=request.item_bank_id
        ).order_by("attribute_order")

        # List of item_attribute_values that we return for the current item
        item_attribute_values_final = []

        for item_bank_attribute in item_bank_attributes:
            item_bank_attribute_values = ItemBankAttributeValuesVW.objects.filter(
                item_bank_attribute_id=item_bank_attribute.id
            )
            item_attribute_value_array = []

            for item_bank_attribute_value in item_bank_attribute_values:
                # Test for Item Attribute Value
                item_attribute_value = ItemAttributeValue.objects.filter(
                    item_bank_attribute_value=item_bank_attribute_value.item_bank_attribute_value_id,
                    item_id=request.item_id,
                )
                if item_attribute_value:
                    item_attribute_value_array = [item_bank_attribute_value]
                    break
            if item_attribute_value_array:
                item_attribute_values_final.append(
                    ItemBankAttributeValuesViewSerializer(
                        item_attribute_value_array[0], many=False
                    ).data
                )
            else:
                item_attribute_values_final.append(
                    {
                        "item_bank_attribute_value_id": "N/A",
                        "text": "N/A",
                        "attribute_text_en": item_bank_attribute_values[
                            0
                        ].attribute_text_en,
                        "attribute_text_fr": item_bank_attribute_values[
                            0
                        ].attribute_text_fr,
                    }
                )

        return item_attribute_values_final

    class Meta:
        model = ItemLatestVersionsDataVW
        fields = "__all__"


class ItemLatestVersionsDataViewSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = ItemLatestVersionsDataVW
        fields = "__all__"


class ItemDraftsViewSerializer(serializers.ModelSerializer):
    available_versions = serializers.SerializerMethodField()
    item_content = serializers.SerializerMethodField()
    item_options = serializers.SerializerMethodField()
    item_comments = serializers.SerializerMethodField()
    item_attribute_values = serializers.SerializerMethodField()

    def get_available_versions(self, request):
        contains_draft = self.context.get("contains_draft", None)
        available_versions = AllItemsDataVW.objects.filter(
            system_id=request.system_id
        ).values("item_id", "version")

        # if contains draft
        if contains_draft:
            available_versions = list(available_versions)
            available_versions.append(DRAFT_OPTION)

        return available_versions

    def get_item_content(self, request):
        # getting item content and ordering by language ID
        item_content = ItemContentDraftsVW.objects.filter(
            item_id=request.item_id, username_id=request.username_id
        ).order_by("language_id")

        # getting serialized data
        serialized_data = ItemContentDraftsViewSerializer(item_content, many=True).data

        updated_serialized_data = []

        # looping in serialized_data
        for data in serialized_data:
            # adding attributes needed in the frontend logic
            obj = data
            obj["content_type"] = data["content_type"]
            obj["item_type"] = DraggableItemType.TEXT_AREA_COMPONENT
            updated_serialized_data.append(obj)

        # getting language data
        language_data = Language.objects.all()

        # initializing final_obj
        final_obj = {}

        # grouping text by language
        for obj, inner_group in groupby(
            updated_serialized_data, lambda x: x["language_id"]
        ):
            for language in language_data:
                # matching language
                if obj == language.language_id:
                    # initializing needed variables
                    custom_inner_group = list(inner_group)
                    markdown_array = []
                    screen_reader_array = []

                    for data in custom_inner_group:
                        # MARKDOWN
                        if data["content_type"] == ItemContentType.MARKDOWN:
                            # populating markdown_array
                            markdown_array.append(data)
                        # SCREEN READER
                        elif data["content_type"] == ItemContentType.SCREEN_READER:
                            # populating screen_reader_array
                            screen_reader_array.append(data)
                        # overwriting id of current iteration
                        # note that if you update this ID, you'll need to update it in the frontend as well (needed for draggable logic)
                        # file ref: ...\frontend\src\components\testBuilder\itemBank\items\ContentTab.jsx
                        # MARKDOWN
                        if data["content_type"] == ItemContentType.MARKDOWN:
                            data["id"] = "item-stem-{0}-{1}".format(
                                language.ISO_Code_1, (len(markdown_array))
                            )
                        # SCREEN READER
                        elif data["content_type"] == ItemContentType.SCREEN_READER:
                            data["id"] = "item-stem-screen-reader-{0}-{1}".format(
                                language.ISO_Code_1, (len(screen_reader_array))
                            )

                    # sorting by content type / content_order
                    custom_inner_group.sort(
                        key=lambda x: (x["content_type"], x["content_order"]),
                        reverse=False,
                    )

                    # populating obj
                    final_obj["{0}".format(language.ISO_Code_1)] = custom_inner_group

        # returning final_obj
        return final_obj

    def get_item_options(self, request):
        # getting item options and ordering by language ID
        item_options = ItemOptionDraftsVW.objects.filter(
            item_id=request.item_id, username_id=request.username_id
        ).order_by("language_id")

        # getting serialized data
        serialized_data = ItemOptionDraftsViewSerializer(item_options, many=True).data

        updated_serialized_data = []

        # looping in serialized_data
        for data in serialized_data:
            # adding attributes needed in the frontend logic
            obj = data
            obj["option_type"] = data["option_type"]
            obj["item_type"] = DraggableItemType.TEXT_OPTION_COMPONENT
            updated_serialized_data.append(obj)

        # getting language data
        language_data = Language.objects.all()

        # initializing final_obj
        final_obj = {}

        # grouping text by language
        for obj, inner_group in groupby(
            updated_serialized_data, lambda x: x["language_id"]
        ):
            for language in language_data:
                # matching language
                if obj == language.language_id:
                    # initializing needed variables
                    custom_inner_group = list(inner_group)
                    multiple_choice_array = []
                    multiple_choice_screen_reader_array = []

                    for data in custom_inner_group:
                        # MULTIPLE CHOICE
                        if data["option_type"] == ItemOptionType.MULTIPLE_CHOICE:
                            # populating multiple_choice_array
                            multiple_choice_array.append(data)
                        # MULTIPLE CHOICE SCREEN READER
                        elif (
                            data["option_type"]
                            == ItemOptionType.MULTIPLE_CHOICE_SCREEN_READER
                        ):
                            # populating multiple_choice_screen_reader_array
                            multiple_choice_screen_reader_array.append(data)
                        # overwriting id of current iteration
                        # note that if you update this ID, you'll need to update it in the frontend as well (needed for draggable logic)
                        # file ref: ...\frontend\src\components\testBuilder\itemBank\items\ContentTab.jsx
                        # MULTIPLE CHOICE
                        if data["option_type"] == ItemContentType.MARKDOWN:
                            data["id"] = "item-option-{0}-{1}".format(
                                language.ISO_Code_1, (len(multiple_choice_array))
                            )
                        # MULTIPLE CHOICE SCREEN READER
                        elif data["option_type"] == ItemContentType.SCREEN_READER:
                            data["id"] = "item-option-screen-reader-{0}-{1}".format(
                                language.ISO_Code_1,
                                (len(multiple_choice_screen_reader_array)),
                            )

                    # sorting by option type / option_order
                    custom_inner_group.sort(
                        key=lambda x: (x["option_type"], x["option_order"]),
                        reverse=False,
                    )

                    # shuffle options is enables
                    if request.shuffle_options:
                        final_options_array = shuffle_item_options(custom_inner_group)

                        # overriding custom_inner_group with final_options_array
                        custom_inner_group = final_options_array

                    # populating obj
                    final_obj["{0}".format(language.ISO_Code_1)] = custom_inner_group

        # returning final_obj
        return final_obj

    def get_item_comments(self, request):
        item_comments = ItemComments.objects.filter(
            system_id=request.system_id
        ).order_by("-modify_date")
        return ItemCommentsSerializer(item_comments, many=True).data

    def get_item_attribute_values(self, request):
        # List of attributes available for the current Item Bank
        item_bank_attributes = ItemBankAttributes.objects.filter(
            item_bank=request.item_bank_id
        ).order_by("attribute_order")

        # List of item_attribute_values that we return for the current item
        item_attribute_values_final = []

        for item_bank_attribute in item_bank_attributes:
            item_bank_attribute_values = ItemBankAttributeValuesVW.objects.filter(
                item_bank_attribute_id=item_bank_attribute.id
            )
            item_attribute_value_array = []

            for item_bank_attribute_value in item_bank_attribute_values:
                item_attribute_value_draft = ItemAttributeValueDrafts.objects.filter(
                    item_bank_attribute_value=item_bank_attribute_value.item_bank_attribute_value_id,
                    item_id=request.item_id,
                )
                if item_attribute_value_draft:
                    item_attribute_value_array = [item_bank_attribute_value]
                    break
            if item_attribute_value_array:
                item_attribute_values_final.append(
                    ItemBankAttributeValuesViewSerializer(
                        item_attribute_value_array[0], many=False
                    ).data
                )
            else:
                item_attribute_values_final.append(
                    {
                        "item_bank_attribute_value_id": "N/A",
                        "text": "N/A",
                        "attribute_text_en": item_bank_attribute_values[
                            0
                        ].attribute_text_en,
                        "attribute_text_fr": item_bank_attribute_values[
                            0
                        ].attribute_text_fr,
                    }
                )
        return item_attribute_values_final

    class Meta:
        model = ItemDraftsVW
        fields = "__all__"


class ItemContentSerializer(serializers.ModelSerializer):
    # id name is needed in the frontend (this model does not have an ID, but an item_id)
    id = serializers.SerializerMethodField()
    item_content = serializers.SerializerMethodField()
    item_options = serializers.SerializerMethodField()
    item_bank_language_id = serializers.SerializerMethodField()

    def get_id(self, request):
        return request.item_id

    def get_item_content(self, request):
        # getting item content and ordering by language ID
        item_content = ItemContentVW.objects.filter(item_id=request.item_id).order_by(
            "language_id"
        )

        # getting serialized data
        serialized_data = ItemContentViewSerializer(item_content, many=True).data

        updated_serialized_data = []

        # looping in serialized_data
        for data in serialized_data:
            # adding attributes needed in the frontend logic
            obj = data
            obj["content_type"] = data["content_type"]
            obj["item_type"] = DraggableItemType.TEXT_AREA_COMPONENT
            updated_serialized_data.append(obj)

        # getting language data
        language_data = Language.objects.all()

        # initializing final_obj
        final_obj = {}

        # grouping text by language
        for obj, inner_group in groupby(
            updated_serialized_data, lambda x: x["language_id"]
        ):
            for language in language_data:
                # matching language
                if obj == language.language_id:
                    # initializing needed variables
                    custom_inner_group = list(inner_group)
                    markdown_array = []
                    screen_reader_array = []

                    for data in custom_inner_group:
                        # MARKDOWN
                        if data["content_type"] == ItemContentType.MARKDOWN:
                            # populating markdown_array
                            markdown_array.append(data)
                        # SCREEN READER
                        elif data["content_type"] == ItemContentType.SCREEN_READER:
                            # populating screen_reader_array
                            screen_reader_array.append(data)
                        # overwriting id of current iteration
                        # note that if you update this ID, you'll need to update it in the frontend as well (needed for draggable logic)
                        # file ref: ...\frontend\src\components\testBuilder\itemBank\items\ContentTab.jsx
                        # MARKDOWN
                        if data["content_type"] == ItemContentType.MARKDOWN:
                            data["id"] = "item-stem-{0}-{1}".format(
                                language.ISO_Code_1, (len(markdown_array))
                            )
                        # SCREEN READER
                        elif data["content_type"] == ItemContentType.SCREEN_READER:
                            data["id"] = "item-stem-screen-reader-{0}-{1}".format(
                                language.ISO_Code_1, (len(screen_reader_array))
                            )

                    # sorting by content type / content_order
                    custom_inner_group.sort(
                        key=lambda x: (x["content_type"], x["content_order"]),
                        reverse=False,
                    )

                    # populating obj
                    final_obj["{0}".format(language.ISO_Code_1)] = custom_inner_group

        # returning final_obj
        return final_obj

    def get_item_options(self, request):
        assigned_test_id = self.context.get("assigned_test_id", None)

        # initializing needed variables
        item_options = []
        assigned_answer_choices = []
        item_options_built_from_assigned_answer_choices = False

        # assigned_test_id is defined (real test)
        if assigned_test_id is not None:
            # checking if there is existing assigned answer choices
            assigned_answer_choices = AssignedAnswerChoices.objects.filter(
                assigned_test_id=assigned_test_id, item_id=request.item_id
            )

            # we have some assigned_answer_choices
            if assigned_answer_choices:
                # setting item_options_built_from_assigned_answer_choices to True
                item_options_built_from_assigned_answer_choices = True
                # getting answer choices from AssignedAnswerChoices
                answer_choices_string = assigned_answer_choices.first().answer_choices
                # converting answer_choices_string (string) in array
                answer_choices_array = literal_eval(answer_choices_string)
                # building the items list based on the found assigned_questions_list
                respective_item_options = ItemOptionVW.objects.filter(
                    item_option_id__in=answer_choices_array
                )
                # making sure that the order of the respective_item_options is respected
                for answer_choice_from_list_array in answer_choices_array:
                    for item_option_from_db in respective_item_options:
                        if (
                            item_option_from_db.item_option_id
                            == answer_choice_from_list_array
                        ):
                            item_options.append(item_option_from_db)

                # sorting item option by language_id
                item_options.sort(
                    key=lambda x: x.language_id,
                    reverse=False,
                )

        # item_options still empty at this point
        if not item_options:
            # getting item options and ordering by language ID
            item_options = ItemOptionVW.objects.filter(
                item_id=request.item_id
            ).order_by("language_id")

        # getting serialized data
        serialized_data = ItemOptionViewSerializer(item_options, many=True).data

        updated_serialized_data = []

        # looping in serialized_data
        for data in serialized_data:
            # adding attributes needed in the frontend logic
            obj = data
            obj["option_type"] = data["option_type"]
            obj["item_type"] = DraggableItemType.TEXT_OPTION_COMPONENT
            updated_serialized_data.append(obj)

        # getting language data
        language_data = Language.objects.all()

        # initializing final_obj
        final_obj = {}

        # grouping text by language
        for obj, inner_group in groupby(
            updated_serialized_data, lambda x: x["language_id"]
        ):
            for language in language_data:
                # matching language
                if obj == language.language_id:
                    # initializing needed variables
                    custom_inner_group = list(inner_group)
                    multiple_choice_array = []
                    multiple_choice_screen_reader_array = []

                    for data in custom_inner_group:
                        # MULTIPLE CHOICE
                        if data["option_type"] == ItemOptionType.MULTIPLE_CHOICE:
                            # populating multiple_choice_array
                            multiple_choice_array.append(data)
                        # MULTIPLE CHOICE SCREEN READER
                        elif (
                            data["option_type"]
                            == ItemOptionType.MULTIPLE_CHOICE_SCREEN_READER
                        ):
                            # populating multiple_choice_screen_reader_array
                            multiple_choice_screen_reader_array.append(data)
                        # overwriting id of current iteration
                        # note that if you update this ID, you'll need to update it in the frontend as well (needed for draggable logic)
                        # file ref: ...\frontend\src\components\testBuilder\itemBank\items\ContentTab.jsx
                        # MULTIPLE CHOICE
                        if data["option_type"] == ItemContentType.MARKDOWN:
                            data["id"] = "item-option-{0}-{1}".format(
                                language.ISO_Code_1, (len(multiple_choice_array))
                            )
                        # MULTIPLE CHOICE SCREEN READER
                        elif data["option_type"] == ItemContentType.SCREEN_READER:
                            data["id"] = "item-option-screen-reader-{0}-{1}".format(
                                language.ISO_Code_1,
                                (len(multiple_choice_screen_reader_array)),
                            )

                    # item options not generated from assigned answer choices (item_options_built_from_assigned_answer_choices is False)
                    if not item_options_built_from_assigned_answer_choices:
                        # sorting by option type / option_order
                        custom_inner_group.sort(
                            key=lambda x: (x["option_type"], x["option_order"]),
                            reverse=False,
                        )

                        # shuffle options is enables
                        if request.shuffle_options:
                            final_options_array = shuffle_item_options(
                                custom_inner_group
                            )

                            # overriding custom_inner_group with final_options_array
                            custom_inner_group = final_options_array

                        # assigned_test_id is defined (real test) + assigned_answer_choices is empty (no assigned answer choices created yet)
                        if assigned_test_id is not None and not assigned_answer_choices:
                            # creating item_option_ids array
                            item_option_ids = []
                            for option_data in custom_inner_group:
                                item_option_ids.append(option_data["item_option_id"])
                            # adding a try catch here to handle duplicate entries
                            # the unique constraint will be triggered if a duplicate entry tries to be added
                            try:
                                AssignedAnswerChoices.objects.create(
                                    assigned_test_id=assigned_test_id,
                                    question_id=None,
                                    item_id=request.item_id,
                                    answer_choices=item_option_ids,
                                )
                            except:
                                pass

                    # populating obj
                    final_obj["{0}".format(language.ISO_Code_1)] = custom_inner_group

        # returning final_obj
        return final_obj

    def get_item_bank_language_id(self, request):
        item_bank_language_id = ItemBank.objects.get(
            id=request.item_bank_id
        ).language_id
        return item_bank_language_id

    class Meta:
        model = ItemLatestVersionsDataVW
        fields = [
            "id",
            "item_id",
            "system_id",
            "version",
            "item_bank_id",
            "active_status",
            "shuffle_options",
            "development_status_id",
            "response_format_id",
            "historical_id",
            "item_content",
            "item_options",
            "item_bank_language_id",
        ]


class ResponseFormatsSerializer(serializers.ModelSerializer):
    class Meta:
        model = ItemResponseFormats
        fields = "__all__"


class DevelopmentStatusesSerializer(serializers.ModelSerializer):
    class Meta:
        model = ItemDevelopmentStatuses
        fields = "__all__"


class BundlesSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bundles
        fields = "__all__"


class BundleAssociationSerializer(serializers.ModelSerializer):
    item_data = serializers.SerializerMethodField()
    bundle_data = serializers.SerializerMethodField()

    def get_item_data(self, request):
        item_data = ItemLatestVersionsDataVW.objects.filter(
            system_id=request.system_id_link
        ).first()
        if item_data:
            return ItemLatestVersionsDataViewSimpleSerializer(
                item_data, many=False
            ).data
        else:
            return None

    def get_bundle_data(self, request):
        bundle_data = Bundles.objects.filter(id=request.bundle_link_id).first()
        if bundle_data:
            return BundlesSimpleSerializer(bundle_data, many=False).data
        else:
            return None

    class Meta:
        model = BundleAssociationVW
        fields = "__all__"
