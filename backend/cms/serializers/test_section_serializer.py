from datetime import datetime, timezone
from rest_framework import serializers
from cms.cms_models.test_section import TestSection
from cms.cms_models.test_section_component import TestSectionComponent
from cms.cms_models.test_section_reducer import TestSectionReducer
from cms.serializers.test_section_reducer_serializer import TestSectionReducerSerializer
from cms.serializers.test_section_component_serializer import (
    TestSectionComponentSerializer,
    InboxScorerSerializer,
)
from cms.cms_models.next_section_button_type_proceed import NextSectionButtonTypeProceed
from cms.serializers.next_section_button_type_proceed_serializer import (
    NextSectionButtonTypeProceedSerializer,
)
from cms.cms_models.next_section_button_type_popup import NextSectionButtonTypePopup
from cms.serializers.next_section_button_type_popup_serializer import (
    NextSectionButtonTypePopupSerializer,
)
from cms.cms_models.competency_type import CompetencyType
from cms.serializers.competency_types_serializer import CompetencyTypeSerializer

from cms.static.next_section_button_type import NextSectionButtonType
from cms.static.test_section_component_type import TestSectionComponentType
from backend.views.utils import is_undefined
from backend.static.languages import Languages
from backend.custom_models.assigned_test import AssignedTest
from backend.custom_models.accommodation_request import AccommodationRequest
from backend.custom_models.break_bank import BreakBank
from backend.custom_models.additional_time import AdditionalTime
from backend.custom_models.assigned_test_section import AssignedTestSection


class TestSectionSerializer(serializers.ModelSerializer):
    redux = serializers.SerializerMethodField()
    default_time = serializers.SerializerMethodField()
    localize = serializers.SerializerMethodField()
    is_public = serializers.SerializerMethodField()
    count_up = serializers.SerializerMethodField()
    scorer = serializers.SerializerMethodField()
    current_time_utc = serializers.SerializerMethodField()

    class Meta:
        model = TestSection
        fields = "__all__"

    def get_is_public(self, request):
        return request.test_definition.is_public

    def get_count_up(self, request):
        return request.test_definition.count_up

    def get_redux(self, request):

        redux = TestSectionReducer.objects.filter(test_section=request.id)
        data = TestSectionReducerSerializer(redux, many=True).data
        return data

    def get_default_time(self, request):
        # if there is a context for current test section time limit
        current_test_section_time = self.context.get("current_test_section_time", None)
        if current_test_section_time:
            return current_test_section_time
        # there is no context
        else:
            default_time = request.default_time
            if is_undefined(default_time):
                return
            if default_time < 1:
                default_time = 0
                return default_time
            return default_time

    def get_localize(self, request):
        return {
            self.context["language"]: self.get_localize_models(
                request, self.context["language"]
            )
        }

    def get_localize_models(self, request, language):
        context = self.context
        data = {}
        button_data = {}

        # get correct language title
        data["title"] = (
            request.en_title if language == Languages.EN else request.fr_title
        )

        section_type = request.section_type
        data["type"] = section_type

        # Next Section button serialization
        data["next_section_button"] = self.get_next_section_button_data(
            request, button_data, language
        )

        # Get the test section component serialization
        components = TestSectionComponent.objects.filter(
            test_section=request.id
        ).order_by("order")

        data["components"] = TestSectionComponentSerializer(
            components, many=True, context=context
        ).data

        return data

    def get_next_section_button_data(self, request, button_data, language):
        if request.next_section_button_type == NextSectionButtonType.PROCEED:
            button = NextSectionButtonTypeProceed.objects.get(
                test_section=request.id, language=language
            )
            button_serial = NextSectionButtonTypeProceedSerializer(
                button, many=False, context={"language": language}
            ).data

            button_data["button_text"] = button_serial["button_text"]

        if request.next_section_button_type == NextSectionButtonType.POPUP:
            button = NextSectionButtonTypePopup.objects.get(
                test_section=request.id, language=language
            )
            button_data = NextSectionButtonTypePopupSerializer(
                button, many=False, context={"language": language}
            ).data
            # check context for next test section time limit
            next_test_section_time = self.context.get("next_test_section_time", None)
            if next_test_section_time:
                button_data["next_section_time_limit"] = next_test_section_time

                # ==================== ACCOMMODATIONS ====================
                # initializing additional_time and break_bank
                button_data["additional_time"] = None
                button_data["break_bank"] = None

                # assigned_test_id context is defined
                if self.context.get("assigned_test_id") is not None:
                    # getting assigned test data
                    assigned_test_data = AssignedTest.objects.get(
                        id=self.context.get("assigned_test_id")
                    )
                    # accommodation request ID defined
                    if assigned_test_data.accommodation_request_id is not None:
                        # getting accommodation request data
                        accommodation_request_data = AccommodationRequest.objects.get(
                            id=assigned_test_data.accommodation_request_id
                        )
                        # ==================== BREAK BANK ====================
                        # break bank has been setup
                        if accommodation_request_data.break_bank_id is not None:
                            # getting related break bank time
                            break_bank_time_in_minutes = (
                                BreakBank.objects.get(
                                    id=accommodation_request_data.break_bank_id
                                ).break_time
                                / 60
                            )
                            button_data["break_bank"] = break_bank_time_in_minutes
                        else:
                            button_data["break_bank"] = None
                        # ==================== BREAK BANK (END) ====================

                        # ==================== ADDITIONAL TIME ====================
                        # getting related additional time data
                        additional_time_data = AdditionalTime.objects.filter(
                            accommodation_request_id=accommodation_request_data.id,
                            test_section_id=self.context.get("next_test_section_id"),
                        )
                        # additional time has been setup for this current test section
                        if additional_time_data:
                            # calculating additional time (additional time value - default time)
                            default_test_section_time = int(
                                TestSection.objects.get(
                                    id=self.context.get("next_test_section_id")
                                ).default_time
                            )
                            calculated_additional_time = (
                                additional_time_data.last().test_section_time
                                - default_test_section_time
                            )
                            button_data["additional_time"] = calculated_additional_time
                        else:
                            button_data["additional_time"] = None
                        # ==================== ADDITIONAL TIME (END) ====================
                    else:
                        button_data["additional_time"] = None
                        button_data["break_bank"] = None

                # ==================== ACCOMMODATIONS (END) ====================

        button_data["button_type"] = request.next_section_button_type
        return button_data

    def get_scorer(self, request):
        if not self.context.get("scorer", None):
            return None

        scorer_information = {}
        # get competencies
        competencies = CompetencyType.objects.filter(
            test_definition=request.test_definition
        )
        scorer_information["competency_types"] = CompetencyTypeSerializer(
            competencies, many=True
        ).data

        # get questions
        components = TestSectionComponent.objects.filter(
            test_section=request.id, component_type=TestSectionComponentType.INBOX
        )
        questions = []
        for component in components:
            questions.extend(
                InboxScorerSerializer(component, context=self.context).data["questions"]
            )
        scorer_information["questions"] = questions
        return scorer_information

    def get_current_time_utc(self, request):
        return datetime.now(timezone.utc)


class TestSectionTitleDescSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestSection
        fields = "__all__"


class AssignedTestSectionSerializer(serializers.ModelSerializer):
    test_section = serializers.SerializerMethodField()
    minimum_score = serializers.SerializerMethodField()

    def get_test_section(self, assigned_test_section):
        test_section = TestSection.objects.get(id=assigned_test_section.test_section_id)
        return TestSectionTitleDescSerializer(test_section).data

    def get_minimum_score(self, assigned_test_section):
        test_section = TestSection.objects.get(id=assigned_test_section.test_section_id)
        return test_section.minimum_score

    class Meta:
        model = AssignedTestSection
        fields = "__all__"
