from rest_framework import serializers
from cms.cms_models.competency_type import CompetencyType


class CompetencyTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = CompetencyType
        fields = "__all__"
