from rest_framework import serializers
from cms.cms_models.bundles import Bundles
from cms.cms_models.item_bank import ItemBank
from cms.cms_models.item_bank_rule import ItemBankRule
from cms.cms_models.email_question import EmailQuestion
from cms.cms_models.page_section_type_tree_description import (
    PageSectionTypeTreeDescription,
)
from cms.cms_models.address_book_contact import AddressBookContact
from cms.cms_models.address_book_contact_details import AddressBookContactDetails

from cms.cms_models.test_definition import TestDefinition
from cms.cms_models.test_section import TestSection
from cms.cms_models.test_section_component import TestSectionComponent
from cms.cms_models.section_component_page import SectionComponentPage
from cms.cms_models.page_section import PageSection
from cms.cms_models.page_section_type_markdown import PageSectionTypeMarkdown
from cms.cms_models.page_section_type_sample_email import PageSectionTypeSampleEmail
from cms.cms_models.page_section_type_sample_email_response import (
    PageSectionTypeSampleEmailResponse,
)
from cms.cms_models.page_section_type_sample_task_response import (
    PageSectionTypeSampleTaskResponse,
)
from cms.cms_models.page_section_type_image_zoom import PageSectionTypeImageZoom

from cms.cms_models.next_section_button_type_popup import NextSectionButtonTypePopup
from cms.cms_models.next_section_button_type_proceed import NextSectionButtonTypeProceed

from cms.cms_models.situation_example_rating import SituationExampleRating
from cms.cms_models.situation_example_rating_details import (
    SituationExampleRatingDetails,
)

from cms.cms_models.test_section_reducer import TestSectionReducer
from cms.cms_models.new_question import NewQuestion
from cms.cms_models.multiple_choice_question import MultipleChoiceQuestion
from cms.cms_models.question_section import QuestionSection
from cms.cms_models.question_section_type_markdown import QuestionSectionTypeMarkdown
from cms.cms_models.answer import Answer
from cms.cms_models.email_question_details import EmailQuestionDetails
from cms.cms_models.answer_details import AnswerDetails
from cms.cms_models.question_block_type import QuestionBlockType
from cms.cms_models.question_list_rule import QuestionListRule
from cms.cms_models.competency_type import CompetencyType
from cms.cms_models.scoring_method_types import ScoringMethodTypes
from cms.cms_models.scoring_methods import ScoringMethods
from cms.cms_models.scoring_threshold import ScoringThreshold
from cms.cms_models.scoring_pass_fail import ScoringPassFail
from cms.views.utils import SCORING_METHOD_TYPE

from cms.static.next_section_button_type import NextSectionButtonType
from cms.static.page_section_type import PageSectionType
from cms.static.question_section_type import QuestionSectionType
from cms.cms_models.question_situation import QuestionSituation


class TestDefinitionExtractSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestDefinition
        fields = "__all__"


class FlatTestDefinitionExtractSerializer(serializers.ModelSerializer):
    test_definition = serializers.SerializerMethodField()
    next_section_buttons = serializers.SerializerMethodField()
    data = serializers.SerializerMethodField()

    class Meta:
        model = TestDefinition
        fields = ["data", "test_definition", "next_section_buttons"]

    def to_representation(self, obj):
        """Move fields from serializerMethodField to main representation."""
        representation = super().to_representation(obj)
        details_representation = representation.pop("data")
        for key in details_representation:
            representation[key] = details_representation[key]

        return representation

    def get_test_definition(self, request):
        objs = TestDefinition.objects.filter(id=request.id)
        return TestDefinitionExtractSerializer(objs, many=True).data

    def get_next_section_buttons(self, request):
        next_section_buttons = []
        # use this to sequence the id of different types of objects so we don't have
        # conflicts in the test builder. This simplifies the code on the front end
        next_button_id = 1
        objs = TestSection.objects.filter(test_definition=request.id)
        for obj in objs:
            button_type = obj.next_section_button_type

            if button_type == NextSectionButtonType.PROCEED:
                objs = NextSectionButtonTypeProceed.objects.filter(test_section=obj.id)
                for button in NextSectionButtonTypeProceedExtractSerializer(
                    objs, many=True
                ).data:
                    button["next_section_button_type"] = button_type
                    button["id"] = next_button_id
                    next_button_id = next_button_id + 1
                    next_section_buttons.append(button)

            elif button_type == NextSectionButtonType.POPUP:
                objs = NextSectionButtonTypePopup.objects.filter(test_section=obj.id)
                for button in NextSectionButtonTypePopupExtractSerializer(
                    objs, many=True
                ).data:
                    button["next_section_button_type"] = button_type
                    button["id"] = next_button_id
                    next_button_id = next_button_id + 1
                    next_section_buttons.append(button)

        return next_section_buttons

    # this function loops through all children and adds them to a main map
    # the to_representation brings all these objects to the root object.
    def get_data(self, request):
        data = {}
        data["test_sections"] = []
        data["test_section_components"] = []
        data["questions"] = []
        data["question_sections"] = []
        data["question_section_definitions"] = []
        data["emails"] = []
        data["email_details"] = []
        data["answers"] = []
        data["answer_details"] = []
        data["contact_details"] = []
        data["address_book"] = []
        data["multiple_choice_question_details"] = []
        data["section_component_pages"] = []
        data["page_sections"] = []
        data["page_section_definitions"] = []
        data["question_block_types"] = []
        data["competency_types"] = []
        data["question_list_rules"] = []
        data["item_bank_rules"] = []
        data["question_situations"] = []
        data["situation_example_ratings"] = []
        data["situation_example_rating_details"] = []
        data["scoring_method_type"] = ""
        data["scoring_threshold"] = []
        data["scoring_pass_fail_minimum_score"] = ""
        data["final_scoring_threshold_conversions"] = []
        data["selectedTestDefinitionId"] = "null"

        # page_section_definition_id_array = []
        page_sections_id_array = []
        section_component_page_id_array = []
        questions_id_array = []
        answers_id_array = []
        answer_details_id_array = []
        emails_id_array = []
        email_details_id_array = []
        q_section_id_array = []
        q_s_definitions_id_array = []
        mc_obj_id_array = []
        contact_detail_id_array = []
        address_book_id_array = []
        component_id_array = []
        rule_id_array = []

        # use this to sequence the id of different types of objects so we don't have
        # conflicts in the test builder. This simplifies the code on the front end
        page_section_definition_id = 1
        question_section_definition_id = 1

        # get all related test sections
        ts_objs = TestSection.objects.filter(test_definition=request.id)
        data["test_sections"].extend(
            TestSectionExtractSerializer(ts_objs, many=True).data
        )

        block_objs = QuestionBlockType.objects.filter(test_definition=request.id)
        data["question_block_types"].extend(
            QuestionBlockTypeExtractSerializer(block_objs, many=True).data
        )

        comp_objs = CompetencyType.objects.filter(test_definition=request.id)
        data["competency_types"].extend(
            CompetencyTypeExtractSerializer(comp_objs, many=True).data
        )

        # test sections (overview, instructions)
        for ts_obj in ts_objs:
            tsc_objs = TestSectionComponent.objects.filter(test_section=ts_obj.id)
            # test section components (inbox, info content)
            for tsc_obj in tsc_objs:
                # dont duplicate objs in the serializer
                if tsc_obj.id not in component_id_array:
                    data["test_section_components"].append(
                        TestSectionComponentExtractSerializer(tsc_obj, many=False).data
                    )
                    # add the serialized object to the parent
                    component_id_array.append(tsc_obj.id)

                    scp_objs = SectionComponentPage.objects.filter(
                        test_section_component=tsc_obj.id
                    )
                    # section component pages (side tabs)
                    for scp_obj in scp_objs:
                        # dont duplicate objs in the serializer
                        if scp_obj.id not in section_component_page_id_array:
                            data["section_component_pages"].append(
                                SectionComponentPageExtractSerializer(
                                    scp_obj, many=False
                                ).data
                            )
                            # add the serialized object to the parent
                            section_component_page_id_array.append(scp_obj.id)

                            ps_objs = PageSection.objects.filter(
                                section_component_page=scp_obj.id
                            )
                            # page sections
                            for ps_obj in ps_objs:
                                # dont duplicate objs in the serializer
                                if ps_obj.id not in page_sections_id_array:
                                    data["page_sections"].append(
                                        PageSectionExtractSerializer(
                                            ps_obj, many=False
                                        ).data
                                    )
                                    page_sections_id_array.append(ps_obj.id)
                                    ps_defs = get_page_section_definiton(ps_obj)
                                    # page section definitions (markdown, sample email, etc)
                                    for ps_def in ps_defs:
                                        definition = ps_def
                                        definition[
                                            "page_section_type"
                                        ] = ps_obj.page_section_type
                                        definition["id"] = page_section_definition_id
                                        page_section_definition_id += 1
                                        data["page_section_definitions"].append(
                                            definition
                                        )

                    # get question list rules
                    question_rule_objs = QuestionListRule.objects.filter(
                        test_section_component=tsc_obj.id
                    )
                    for rule in question_rule_objs:
                        if rule.id in rule_id_array:
                            continue
                        data["question_list_rules"].append(
                            QuestionListRuleExtractSerializer(rule, many=False).data
                        )
                        rule_id_array.append(rule.id)

                    # get item bank rules
                    item_bank_rules_context = {}
                    item_bank_rules_context["current_language"] = self.context.get(
                        "current_language"
                    )
                    item_bank_rules = ItemBankRule.objects.filter(
                        test_section_component=tsc_obj.id
                    ).order_by("order")
                    for rule in item_bank_rules:
                        if rule.id in rule_id_array:
                            continue
                        data["item_bank_rules"].append(
                            ItemBankRuleExtractSerializer(
                                rule, many=False, context=item_bank_rules_context
                            ).data
                        )
                        rule_id_array.append(rule.id)

                    # questions
                    question_objs = NewQuestion.objects.filter(
                        test_section_component=tsc_obj.id
                    )

                    for question in question_objs:
                        if question.id in questions_id_array:
                            continue
                        data["questions"].append(
                            NewQuestionExtractSerializer(question, many=False).data
                        )
                        questions_id_array.append(question.id)

                        answer_objs = Answer.objects.filter(question=question.id)
                        # answer objects (score value)
                        for answer_obj in answer_objs:
                            if answer_obj.id not in answers_id_array:
                                data["answers"].append(
                                    AnswerExtractSerializer(answer_obj, many=False).data
                                )
                                answers_id_array.append(answer_obj.id)

                                answer_details_objs = AnswerDetails.objects.filter(
                                    answer=answer_obj.id
                                )
                                # text displayed on answer (languages)
                                for answer_details_obj in answer_details_objs:
                                    if answer_details_obj.id in answer_details_id_array:
                                        continue
                                    data["answer_details"].append(
                                        AnswerDetailsExtractSerializer(
                                            answer_details_obj, many=False
                                        ).data
                                    )
                                    answer_details_id_array.append(
                                        answer_details_obj.id
                                    )

                        # multiple chocie obj details
                        mc_objs = MultipleChoiceQuestion.objects.filter(
                            question=question.id
                        )
                        for mc_obj in mc_objs:
                            if mc_obj.id not in mc_obj_id_array:
                                data["multiple_choice_question_details"].append(
                                    MultipleChoiceQuestionExtractSerializer(
                                        mc_obj, many=False
                                    ).data
                                )
                                mc_obj_id_array.append(mc_obj.id)

                        # tetx displayed n questions
                        q_section_objs = QuestionSection.objects.filter(
                            question=question.id
                        )
                        for q_section_obj in q_section_objs:
                            if q_section_obj.id not in q_section_id_array:
                                data["question_sections"].append(
                                    QuestionSectionExtractSerializer(
                                        q_section_obj, many=False
                                    ).data
                                )
                                q_section_id_array.append(q_section_obj.id)

                                q_s_definition_objs = get_question_section_definitions(
                                    q_section_obj
                                )

                                for q_s_d_obj in q_s_definition_objs:
                                    definition = q_s_d_obj
                                    definition[
                                        "question_section_type"
                                    ] = q_section_obj.question_section_type
                                    # renumbering IDs to avoid conflicts in test builder
                                    # this works because only new question sections definitions
                                    # can be created and not edited
                                    definition["id"] = question_section_definition_id
                                    question_section_definition_id = (
                                        question_section_definition_id + 1
                                    )
                                    data["question_section_definitions"].append(
                                        definition
                                    )
                                    q_s_definitions_id_array.append(q_s_d_obj["id"])

                        email_objs = EmailQuestion.objects.filter(question=question.id)
                        # email questions
                        for email_obj in email_objs:
                            if email_obj.id not in emails_id_array:
                                data["emails"].append(
                                    EmailQuestionExtractSerializer(
                                        email_obj, many=False
                                    ).data
                                )
                                emails_id_array.append(email_obj.id)

                                email_details_objs = (
                                    EmailQuestionDetails.objects.filter(
                                        email_question=email_obj.id
                                    )
                                )
                                for detail_obj in email_details_objs:
                                    if detail_obj.id not in email_details_id_array:
                                        data["email_details"].append(
                                            EmailQuestionDetailsExtractSerializer(
                                                detail_obj, many=False
                                            ).data
                                        )
                                        email_details_id_array.append(detail_obj.id)

                        # question situations
                        situations = QuestionSituation.objects.filter(question=question)
                        for situation in situations:
                            data["question_situations"].append(
                                QuestionSituationExtractSerializer(situation).data
                            )

                        ratings = SituationExampleRating.objects.filter(
                            question=question
                        )
                        for rating in ratings:
                            data["situation_example_ratings"].append(
                                SituationExampleRatingExtractSerializer(rating).data
                            )

                            details = SituationExampleRatingDetails.objects.filter(
                                example_rating=rating
                            )
                            data["situation_example_rating_details"].extend(
                                SituationExampleRatingDetailsExtractSerializer(
                                    details, many=True
                                ).data
                            )

            contact_details = AddressBookContact.objects.filter(
                test_section__id=ts_obj.id
            )
            # address book contacts
            for contact in contact_details:
                objs = AddressBookContactDetails.objects.filter(contact=contact.id)
                for obj in objs:
                    if obj.id not in contact_detail_id_array:
                        data["contact_details"].append(
                            AddressBookContactDetailsExtractSerializer(
                                obj, many=False
                            ).data
                        )
                        contact_detail_id_array.append(obj.id)
            # address book contact details (title)
            contacts = AddressBookContact.objects.filter(test_section__id=ts_obj.id)
            for obj in contacts:
                if obj.id not in address_book_id_array:
                    data["address_book"].append(
                        AddressBookContactExtractSerializer(obj, many=False).data
                    )
                    address_book_id_array.append(obj.id)
            # scoring method data
            try:
                # getting scoring method
                scoring_method = ScoringMethodSerializer(
                    ScoringMethods.objects.get(test_id=request.id), many=False
                ).data
                # getting scoring method type
                scoring_method_type = ScoringMethodTypesSerializer(
                    ScoringMethodTypes.objects.get(id=scoring_method["method_type"]),
                    many=False,
                ).data
                # getting scoring method label based on provided language
                current_language = self.context.get("current_language")
                label = ""
                # provided language is en
                if current_language == "en":
                    label = scoring_method_type["en_name"]
                # provided language is fr
                else:
                    label = scoring_method_type["fr_name"]
                # saving scoring method type data
                data["scoring_method_type"] = {
                    "label": label,
                    "value": scoring_method_type["method_type_codename"],
                }
                # scoring method type is THRESHOLD
                if (
                    scoring_method_type["method_type_codename"]
                    == SCORING_METHOD_TYPE.THRESHOLD
                ):
                    # initializing scoring_threshold_array
                    scoring_threshold_array = []
                    # looping in scoring threshold items
                    for scoring_threshold in ScoringThreshold.objects.filter(
                        scoring_method_id=scoring_method["id"]
                    ):
                        # pushing data to scoring_threshold_array
                        scoring_threshold_array.append(
                            {
                                "id": scoring_threshold.id,
                                "en_conversion_value": scoring_threshold.en_conversion_value,
                                "fr_conversion_value": scoring_threshold.fr_conversion_value,
                                # minimum and maximum score need to be converted in string (frontend is handling them as string)
                                "minimum_score": str(scoring_threshold.minimum_score),
                                "maximum_score": str(scoring_threshold.maximum_score),
                            }
                        )
                    # saving scoring_threshold and final_scoring_threshold_conversions data
                    data["scoring_threshold"] = scoring_threshold_array
                    data[
                        "final_scoring_threshold_conversions"
                    ] = scoring_threshold_array
                # scoring method type is PASS_FAIL
                elif (
                    scoring_method_type["method_type_codename"]
                    == SCORING_METHOD_TYPE.PASS_FAIL
                ):
                    # saving scoring_pass_fail_minimum_score data
                    # needs to be converted in string (frontend is handling them as string)
                    data["scoring_pass_fail_minimum_score"] = str(
                        ScoringPassFailSerializer(
                            ScoringPassFail.objects.get(
                                scoring_method_id=scoring_method["id"]
                            ),
                            many=False,
                        ).data["minimum_score"]
                    )
            except ScoringMethods.DoesNotExist:
                pass

            data["selectedTestDefinitionId"] = request.id
        return data


class TestSectionExtractSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestSection
        fields = "__all__"


class TestSectionReducerExtractSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestSectionReducer
        fields = "__all__"


class TestSectionComponentExtractSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestSectionComponent
        fields = "__all__"


class SectionComponentPageExtractSerializer(serializers.ModelSerializer):
    class Meta:
        model = SectionComponentPage
        fields = "__all__"


class PageSectionExtractSerializer(serializers.ModelSerializer):
    # sections = serializers.SerializerMethodField()

    class Meta:
        model = PageSection
        fields = "__all__"


class ScoringMethodSerializer(serializers.ModelSerializer):
    class Meta:
        model = ScoringMethods
        fields = "__all__"


class ScoringMethodTypesSerializer(serializers.ModelSerializer):
    class Meta:
        model = ScoringMethodTypes
        fields = "__all__"


class ScoringPassFailSerializer(serializers.ModelSerializer):
    class Meta:
        model = ScoringPassFail
        fields = "__all__"


def get_page_section_definiton(request):
    section_type = request.page_section_type

    if section_type == PageSectionType.MARKDOWN:
        objs = PageSectionTypeMarkdown.objects.filter(page_section=request.id)
        return PageSectionTypeMarkdownExtractSerializer(objs, many=True).data

    elif section_type == PageSectionType.ZOOM_IMAGE:
        objs = PageSectionTypeImageZoom.objects.filter(page_section=request.id)
        return PageSectionTypeImageZoomExtractSerializer(objs, many=True).data

    elif section_type == PageSectionType.SAMPLE_EMAIL:
        objs = PageSectionTypeSampleEmail.objects.filter(page_section=request.id)
        return PageSectionTypeSampleEmailExtractSerializer(objs, many=True).data

    elif section_type == PageSectionType.SAMPLE_EMAIL_RESPONSE:
        objs = PageSectionTypeSampleEmailResponse.objects.filter(
            page_section=request.id
        )
        return PageSectionTypeSampleEmailResponseExtractSerializer(objs, many=True).data

    elif section_type == PageSectionType.SAMPLE_TASK_RESPONSE:
        objs = PageSectionTypeSampleTaskResponse.objects.filter(page_section=request.id)
        return PageSectionTypeSampleTaskResponseExtractSerializer(objs, many=True).data

    elif section_type == PageSectionType.TREE_DESCRIPTION:
        objs = PageSectionTypeTreeDescription.objects.filter(page_section=request.id)
        return PageSectionTypeTreeDescriptionExtractSerializer(objs, many=True).data


def get_question_section_definitions(request):
    section_type = request.question_section_type

    if section_type == QuestionSectionType.MARKDOWN:
        objs = QuestionSectionTypeMarkdown.objects.filter(question_section=request.id)
        return QuestionSectionTypeMarkdownExtractSerializer(objs, many=True).data


# Address Book -> inbox style
class AddressBookContactExtractSerializer(serializers.ModelSerializer):
    class Meta:
        model = AddressBookContact
        fields = "__all__"


class AddressBookContactDetailsExtractSerializer(serializers.ModelSerializer):
    class Meta:
        model = AddressBookContactDetails
        fields = "__all__"


# Questions
class NewQuestionExtractSerializer(serializers.ModelSerializer):
    class Meta:
        model = NewQuestion
        fields = "__all__"


class QuestionSectionExtractSerializer(serializers.ModelSerializer):
    class Meta:
        model = QuestionSection
        fields = "__all__"


class EmailQuestionExtractSerializer(serializers.ModelSerializer):
    class Meta:
        model = EmailQuestion
        fields = "__all__"


# Page Sections
class PageSectionTypeMarkdownExtractSerializer(serializers.ModelSerializer):
    class Meta:
        model = PageSectionTypeMarkdown
        fields = "__all__"


class PageSectionTypeSampleEmailExtractSerializer(serializers.ModelSerializer):
    class Meta:
        model = PageSectionTypeSampleEmail
        fields = "__all__"


class PageSectionTypeSampleEmailResponseExtractSerializer(serializers.ModelSerializer):
    class Meta:
        model = PageSectionTypeSampleEmailResponse
        fields = "__all__"


class PageSectionTypeSampleTaskResponseExtractSerializer(serializers.ModelSerializer):
    class Meta:
        model = PageSectionTypeSampleTaskResponse
        fields = "__all__"


class PageSectionTypeImageZoomExtractSerializer(serializers.ModelSerializer):
    class Meta:
        model = PageSectionTypeImageZoom
        fields = "__all__"


class PageSectionTypeTreeDescriptionExtractSerializer(serializers.ModelSerializer):
    class Meta:
        model = PageSectionTypeTreeDescription
        fields = "__all__"


# Next Section buttons
class NextSectionButtonTypePopupExtractSerializer(serializers.ModelSerializer):
    class Meta:
        model = NextSectionButtonTypePopup
        fields = "__all__"


class NextSectionButtonTypeProceedExtractSerializer(serializers.ModelSerializer):
    class Meta:
        model = NextSectionButtonTypeProceed
        fields = "__all__"


# Questions
class MultipleChoiceQuestionExtractSerializer(serializers.ModelSerializer):
    class Meta:
        model = MultipleChoiceQuestion
        fields = "__all__"


class AnswerExtractSerializer(serializers.ModelSerializer):
    class Meta:
        model = Answer
        fields = "__all__"


class AnswerDetailsExtractSerializer(serializers.ModelSerializer):
    class Meta:
        model = AnswerDetails
        fields = "__all__"


class QuestionSectionTypeMarkdownExtractSerializer(serializers.ModelSerializer):
    class Meta:
        model = QuestionSectionTypeMarkdown
        fields = "__all__"


class EmailQuestionDetailsExtractSerializer(serializers.ModelSerializer):
    class Meta:
        model = EmailQuestionDetails
        fields = "__all__"


class QuestionBlockTypeExtractSerializer(serializers.ModelSerializer):
    class Meta:
        model = QuestionBlockType
        fields = "__all__"


class QuestionListRuleExtractSerializer(serializers.ModelSerializer):
    class Meta:
        model = QuestionListRule
        fields = "__all__"


class ItemBankRuleExtractSerializer(serializers.ModelSerializer):
    item_bank = serializers.SerializerMethodField()
    item_bank_bundle = serializers.SerializerMethodField()

    def get_item_bank(self, request):
        # getting current_language from context
        current_language = self.context.get("current_language", None)
        item_bank = ItemBank.objects.get(id=request.item_bank_id)
        item_bank_name = item_bank.en_name
        # if current_language is "fr"
        if current_language == "fr":
            item_bank_name = item_bank.fr_name
        return {
            "label": "{0} ({1})".format(item_bank.custom_item_bank_id, item_bank_name),
            "value": item_bank.id,
            "language_id": item_bank.language_id,
        }

    def get_item_bank_bundle(self, request):
        item_bank_bundle = Bundles.objects.get(id=request.item_bank_bundle_id)
        return {"label": item_bank_bundle.name, "value": item_bank_bundle.id}

    class Meta:
        model = ItemBankRule
        fields = [
            "id",
            "item_bank",
            "item_bank_bundle",
            "order",
            "test_section_component",
        ]


class CompetencyTypeExtractSerializer(serializers.ModelSerializer):
    class Meta:
        model = CompetencyType
        fields = "__all__"


class QuestionSituationExtractSerializer(serializers.ModelSerializer):
    class Meta:
        model = QuestionSituation
        fields = "__all__"


class SituationExampleRatingExtractSerializer(serializers.ModelSerializer):
    class Meta:
        model = SituationExampleRating
        fields = "__all__"


class SituationExampleRatingDetailsExtractSerializer(serializers.ModelSerializer):
    class Meta:
        model = SituationExampleRatingDetails
        fields = "__all__"
