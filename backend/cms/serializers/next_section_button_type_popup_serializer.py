from rest_framework import serializers
from cms.cms_models.next_section_button_type_popup import NextSectionButtonTypePopup


class NextSectionButtonTypePopupSerializer(serializers.ModelSerializer):
    class Meta:
        model = NextSectionButtonTypePopup
        fields = ["content", "title", "button_text", "confirm_proceed"]
