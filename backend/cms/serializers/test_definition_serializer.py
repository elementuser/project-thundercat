from rest_framework import serializers
from cms.cms_models.test_definition import TestDefinition


class TestDefinitionSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestDefinition
        fields = "__all__"


class TestDefinitionDataSerializer(serializers.ModelSerializer):
    details = serializers.SerializerMethodField()
    action_buttons_disabled = serializers.SerializerMethodField()

    class Meta:
        model = TestDefinition
        fields = ["details", "action_buttons_disabled"]

    def get_details(self, request):
        data = {}
        data["id"] = request.id
        data["test_code"] = request.test_code
        data["version"] = request.version
        data["en_name"] = request.en_name
        data["fr_name"] = request.fr_name
        data["is_uit"] = request.is_uit
        data["is_public"] = request.is_public
        data["count_up"] = request.count_up
        data["active"] = request.active
        data["parent_code"] = request.parent_code
        data["retest_period"] = request.retest_period
        data["archived"] = request.archived
        data["version_notes"] = request.version_notes
        return [data]

    def get_action_buttons_disabled(self, request):
        allowed_to_view_tests_not_using_any_item_bank = self.context.get(
            "allowed_to_view_tests_not_using_any_item_bank", None
        )
        if allowed_to_view_tests_not_using_any_item_bank is not None:
            # not allowed ==> disabled
            return not allowed_to_view_tests_not_using_any_item_bank
        else:
            return True


class TestDefinitionVersionsCollectedSerializer(serializers.ModelSerializer):
    details = serializers.SerializerMethodField()

    class Meta:
        model = TestDefinition
        fields = ["details", "parent_code"]

    def get_details(self, request):
        # get all the versions
        versions = TestDefinition.objects.filter(
            parent_code=request["parent_code"],
            is_public=False,
            active=True,
            archived=False,
        ).order_by()
        return TestDefinitionSerializer(versions, many=True).data


class TestDefinitionTestCodesCollectedSerializer(serializers.ModelSerializer):
    details = serializers.SerializerMethodField()
    action_buttons_disabled = serializers.SerializerMethodField()

    class Meta:
        model = TestDefinition
        fields = ["details", "parent_code", "test_code", "action_buttons_disabled"]

    def get_details(self, request):
        # get all the versions
        test_codes = TestDefinition.objects.filter(
            parent_code=request["parent_code"], test_code=request["test_code"]
        ).order_by()
        return TestDefinitionSerializer(test_codes, many=True).data

    def get_action_buttons_disabled(self, request):
        allowed_to_view_tests_not_using_any_item_bank = self.context.get(
            "allowed_to_view_tests_not_using_any_item_bank", None
        )
        if allowed_to_view_tests_not_using_any_item_bank is not None:
            # not allowed ==> disabled
            return not allowed_to_view_tests_not_using_any_item_bank
        else:
            return True
