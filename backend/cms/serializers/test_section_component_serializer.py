from random import shuffle, randint
from rest_framework import serializers
from rest_framework import status
from rest_framework.response import Response
from backend.custom_models.assigned_test import AssignedTest
from backend.custom_models.assigned_questions_list import AssignedQuestionsList
from cms.cms_models.test_section import TestSection
from cms.cms_models.test_section_component import TestSectionComponent
from cms.cms_models.section_component_page import SectionComponentPage
from cms.serializers.section_component_page_serializer import (
    SectionComponentPageSerializer,
)
from cms.cms_models.question_list_rule import QuestionListRule
from cms.cms_models.new_question import NewQuestion
from cms.cms_models.viewed_questions import ViewedQuestions
from cms.serializers.question_serializer import (
    QuestionSerializer,
    NoLanguageDataQuestionSerializer,
)
from cms.cms_models.address_book_contact import AddressBookContact
from cms.serializers.address_book_contact_serializer import AddressBookContactSerializer
from cms.static.test_section_component_type import TestSectionComponentType
from cms.views.retrieve_item_bank_data import (
    get_item_bank_items_list,
)
from user_management.user_management_models.user_models import User


def get_number_of_questions_with_current_priority(questions, priority, indexes_array):
    nb_of_questions = 0
    # looping in questions
    for index, question in enumerate(questions):
        if index not in indexes_array:
            if question["priority"] == priority:
                nb_of_questions += 1

    return nb_of_questions


def add_dependencies(
    local_testing,
    questions,
    question,
    question_id_array,
    questions_from_test_definition,
):
    # if this testlet has already been added
    if question.id in question_id_array:
        return {
            "questions_list": questions,
            "updated_question_id_array": question_id_array,
        }

    # testing from the test builder
    if local_testing:
        question_dependencies = []
        for dependency in question.dependencies:
            for ques in questions_from_test_definition:
                if ques.id == dependency:
                    question_dependencies.append(ques)

    # testing from a real test (sample and/or real tests)
    else:
        question_dependencies = question.dependencies.all()

    # initializing dependents
    dependents = []
    # adding current questiont to dependents array
    dependents.append(question)
    # looping in question dependencies
    for dependent_question in question_dependencies:
        dependents.append(dependent_question)

    # sort by dependent order
    dependents.sort(key=lambda q: q.dependent_order)

    # add dependent question is the order they were given
    for question in dependents:
        # making sure that the question is not already there
        if question.id in question_id_array:
            continue
        questions.append(question)
        question_id_array.append(question.id)

    return {"questions_list": questions, "updated_question_id_array": question_id_array}


# converting object to dict
def obj_dic(dictionary):
    top = type("new", (object,), dictionary)
    seqs = tuple, list, set, frozenset
    for i, j in dictionary.items():
        if isinstance(j, dict):
            setattr(top, i, obj_dic(j))
        elif isinstance(j, seqs):
            setattr(
                top, i, type(j)(obj_dic(sj) if isinstance(sj, dict) else sj for sj in j)
            )
        else:
            setattr(top, i, j)
    return top


def get_question_list(
    local_testing,
    assigned_test_id,
    test_section_component_id,
    shuffle_all,
    shuffle_question_blocks,
    test_definition,
):
    # initializing all needed variables
    assigned_questions_list = []
    questions = []
    question_id_array = []
    questions_to_add = []
    question_ids = []
    questions_in_rules = []
    dependencies_data = []
    questions_from_test_definition = []
    uses_item_exposure = False

    # testing from a real test (with an assigned test ID)
    if not local_testing and assigned_test_id:
        # getting test section based on provided test_section_component_id
        test_section = TestSection.objects.get(
            testsectioncomponent=test_section_component_id
        )
        # setting uses_item_exposure flag
        if test_section.item_exposure == 1:
            uses_item_exposure = True
        # check if a questions list already exists in assignedquestionslist table
        assigned_questions_list = AssignedQuestionsList.objects.filter(
            assigned_test_id=assigned_test_id, test_section_id=test_section.id
        ).first()

        # if an assigned_questions_list already exists
        if assigned_questions_list:
            # assigning questions_list value to it
            assigned_questions_list = assigned_questions_list.questions_list

            # converting string assigned_questions_list to array/list
            # ====================================================================
            # removing '[' and ']' chars, so we have a list separated by ','
            assigned_questions_list = assigned_questions_list.replace("[", "")
            assigned_questions_list = assigned_questions_list.replace("]", "")
            # splitting values to get a list of question IDs
            assigned_questions_list = list(assigned_questions_list.split(", "))
            # converting string values to int values
            assigned_questions_list = [
                int(numeric_string) for numeric_string in assigned_questions_list
            ]
            # ====================================================================

            # looping in assigned_questions_list array
            for question_id in assigned_questions_list:
                # adding current question data to questions_to_add array
                questions_to_add.append(NewQuestion.objects.get(id=question_id))
            # returning questions data
            return questions_to_add

    # converting test_definition obj to dict
    test_definition_obj = obj_dic(test_definition)

    # testing from the test builder
    if local_testing:
        # getting related questions based on test_section_component_id from the test_definition_obj
        for question in test_definition_obj.new_test.questions:
            if question.test_section_component == test_section_component_id:
                questions_from_test_definition.append(question)
        # sorting questions_from_test_definition array by order
        questions_from_test_definition.sort(key=lambda x: x.order, reverse=False)
        # initializing final_question_list_rules
        final_question_list_rules = []
        # looping in question_list_rules
        for question_list_rule in test_definition_obj.new_test.question_list_rules:
            # if test_section_component_id = current question list_rule's test section component ID
            if question_list_rule.test_section_component == test_section_component_id:
                # adding question list rule to final_question_list_rules array
                final_question_list_rules.append(question_list_rule)
        # getting rules from the provided json
        rules = final_question_list_rules

    # testing from a real test (sample and/or real tests)
    else:
        # getting rules based on the test_section_component_id
        rules = QuestionListRule.objects.filter(
            test_section_component=test_section_component_id
        ).order_by("order")

    # looping in rules
    for rule in rules:
        # initializing needed variables
        questions_list = []
        question_ids = []
        indexes_array = []
        questions_list_completed = False
        # testing from the test builder
        if local_testing:
            # getting question block type of current rule
            block_type = rule.question_block_type[0]

            # number of questions = 0
            if rule.number_of_questions == 0:
                # getting all questions related to the provided block_type and populating question_ids array
                for question in questions_from_test_definition:
                    if question.question_block_type == block_type:
                        # populating questions_list array with all question IDs related to the current block type
                        questions_list.append(question.id)

            # number of questions > 0
            else:
                # getting all questions related to the provided block_type and populating question_ids array
                for question in questions_from_test_definition:
                    if question.question_block_type == block_type:
                        question_ids.append(question.id)
                # creating questions list based on provided number of questions considering all dependencies
                while not questions_list_completed:
                    # looping until looping variable is False
                    looping = True
                    while looping:
                        # we have too many question IDs
                        if len(indexes_array) > rule.number_of_questions:
                            # reinitializing indexes_array
                            indexes_array = []
                            continue
                        # generating random index
                        random_index = randint(0, len(question_ids) - 1)
                        # making sure that random index is not already in indexes_array
                        if random_index in indexes_array:
                            continue
                        else:
                            # getting question data from the question ID based on the provided random index
                            question_id = question_ids[random_index]
                            current_question = questions_from_test_definition[
                                [
                                    question.id
                                    for question in questions_from_test_definition
                                ].index(question_id)
                            ]

                            # if current question has dependencies
                            if current_question.dependencies:
                                # looping in dependencies
                                for dependency in current_question.dependencies:
                                    dependency_index = question_ids.index(dependency)

                                    # if dependency_index not in indexes_array:
                                    indexes_array.append(dependency_index)

                            # adding random_index to indexes_array
                            indexes_array.append(random_index)

                            # we have the right amount of questions
                            if len(indexes_array) == rule.number_of_questions:
                                # stopping the loop
                                looping = False
                                break

                    # sorting array
                    indexes_array.sort()
                    # only getting question ids based on indexes_array
                    questions_list = list(map(question_ids.__getitem__, indexes_array))

                    # number of questions matches the length of questions_list
                    if rule.number_of_questions == len(questions_list):
                        # stopping the loop (we have a valid questions list)
                        questions_list_completed = True
                    else:
                        # reinitializing questions_list and indexes_array
                        questions_list = []
                        indexes_array = []

        # testing from a real test (sample and/or real tests)
        else:
            # getting question block type of current rule
            block_type = rule.question_block_type.all()[0]
            # number of questions = 0
            if rule.number_of_questions == 0:
                # getting all questions IDs related to the current block_type
                question_ids = NewQuestion.objects.filter(
                    test_section_component=test_section_component_id,
                    question_block_type=block_type.id,
                ).values("id")
                # updating questions_list array
                questions_list = question_ids

            # number of questions > 0
            else:
                # uses_item_exposure is enabled
                if uses_item_exposure:
                    question_ids = []
                    # getting all questions IDs related to the current block_type (except questions with undefined ppc_question_id)
                    temp_questions = NewQuestion.objects.filter(
                        test_section_component=test_section_component_id,
                        question_block_type=block_type.id,
                    ).exclude(ppc_question_id__exact="")
                    # getting viewed questions of current user
                    user_id = User.objects.get(
                        username=AssignedTest.objects.get(
                            id=assigned_test_id
                        ).username_id
                    ).id
                    viewed_questions = (
                        ViewedQuestions.objects.filter(user_id=user_id)
                        .values("ppc_question_id", "count")
                        .order_by("count")
                    )
                    # looping in temp_questions
                    for question in temp_questions:
                        # question is in viewed_questions
                        index = next(
                            (
                                i
                                for i, obj in enumerate(viewed_questions)
                                if obj["ppc_question_id"] == question.ppc_question_id
                            ),
                            -1,
                        )
                        if index >= 0:
                            question_ids.append(
                                {
                                    "id": question.id,
                                    "priority": viewed_questions[index]["count"],
                                }
                            )
                        # question is not in viewed_questions
                        else:
                            question_ids.append({"id": question.id, "priority": 0})
                    # ordering question_ids by priority
                    question_ids.sort(key=lambda x: x["priority"], reverse=False)
                # uses_item_exposure is disabled
                else:
                    question_ids = NewQuestion.objects.filter(
                        test_section_component=test_section_component_id,
                        question_block_type=block_type.id,
                    ).values("id")

                questions_list_completed = False
                # creating questions list based on provided number of questions considering all dependencies
                while not questions_list_completed:
                    # generating random index
                    looping = True
                    # uses_item_exposure is enabled
                    if uses_item_exposure:
                        # initializing current_priority (priority of the first question_ids = lowest number)
                        current_priority = question_ids[0]["priority"]
                        # initializing nb_of_questions_with_current_priority
                        nb_of_questions_with_current_priority = 0
                    while looping:
                        # we have too many question IDs
                        if len(indexes_array) > rule.number_of_questions:
                            # reinitializing indexes_array
                            indexes_array = []
                            nb_of_questions_with_current_priority = 0
                            continue
                        # uses_item_exposure is enabled
                        if uses_item_exposure:
                            # getting amount of questions where priority is set to the current one
                            nb_of_questions_with_current_priority = (
                                get_number_of_questions_with_current_priority(
                                    question_ids, current_priority, indexes_array
                                )
                            )
                            # no more questions with current priority
                            if nb_of_questions_with_current_priority == 0:
                                current_priority += 1
                                # getting amount of questions where priority is set to the current one
                                nb_of_questions_with_current_priority = (
                                    get_number_of_questions_with_current_priority(
                                        question_ids, current_priority, indexes_array
                                    )
                                )
                            looping_in_indexes = True
                            while looping_in_indexes:
                                # generating random index
                                random_index = randint(0, len(question_ids) - 1)
                                # question of generated index has the same priority as the current one
                                if (
                                    question_ids[random_index]["priority"]
                                    == current_priority
                                ):
                                    looping_in_indexes = False
                        # uses_item_exposure is disabled
                        else:
                            # generating random index
                            random_index = randint(0, len(question_ids) - 1)
                        # making sure that random index is not already in indexes_array
                        if random_index in indexes_array:
                            continue
                        else:
                            # getting question data from the question ID based on the provided random index
                            current_question = NewQuestion.objects.get(
                                id=question_ids[random_index]["id"]
                            )

                            # if current question has dependencies
                            if current_question.dependencies.all():
                                # looping in dependencies
                                for dependency in current_question.dependencies.all():
                                    # if type of question_ids is list (meaning that question_ids is no longer a queryset ==> uses_item_exposure enabled)
                                    if type(question_ids == list):
                                        temp_question_ids = []
                                        # looping in question_ids
                                        for question_id in question_ids:
                                            temp_question_ids.append(question_id["id"])
                                        dependency_index = list(
                                            temp_question_ids
                                        ).index(dependency.id)
                                    else:
                                        dependency_index = list(
                                            question_ids.values_list("id", flat=True)
                                        ).index(dependency.id)

                                    # if dependency_index not in indexes_array:
                                    indexes_array.append(dependency_index)

                            indexes_array.append(random_index)
                            # looping = False
                            if len(indexes_array) == int(rule.number_of_questions):
                                looping = False
                                break
                    # sorting array
                    indexes_array.sort()
                    # only getting question ids based on indexes_array
                    questions_list = list(map(question_ids.__getitem__, indexes_array))

                    # number of questions matches the length of questions_list
                    if rule.number_of_questions == len(questions_list):
                        # stopping the loop (we have a valid questions list)
                        questions_list_completed = True
                    else:
                        # reinitializing questions_list and indexes_array
                        questions_list = []
                        indexes_array = []

        # if rule includes shuffling questions
        if rule.shuffle:
            # type of questions_list is not list
            if type(questions_list) != list:
                # convert to list
                questions_list = list(questions_list)
            shuffle(questions_list)

        # populating questions_in_rules
        questions_in_rules.append(questions_list)

        # adding all questions of current questions_list in questions_to_add array
        for question in questions_list:
            questions_to_add.append(question)

    # if there are no rules, just return every question
    if not rules:
        # initializing needed variables
        questions_to_add = []

        # testing from the test builder
        if local_testing:
            # populating questions_to_add based on questions_from_test_definition
            for question in questions_from_test_definition:
                questions_to_add.append(question)

        # testing from a real test (sample and/or real tests)
        else:
            questions_to_add = NewQuestion.objects.filter(
                test_section_component=test_section_component_id
            ).order_by("order")

        # if the test section component config to shuffle all questions
        if shuffle_all:
            # type of questions_list is not list
            if type(questions_to_add) != list:
                # convert to list
                questions_to_add = list(questions_to_add)
            shuffle(questions_to_add)

    # there is at least one rule
    else:
        # if the test section component config to shuffle all questions
        if shuffle_all:
            # initializing temp_questions_to_add
            temp_questions_to_add = []
            # execute loop until the length of questions_to_add array is the same as the lenght of temp_questions_to_add array
            while len(questions_to_add) != len(temp_questions_to_add):
                # getting random index based on the amount of rules
                random_index = randint(0, len(questions_in_rules) - 1)
                # looping in questions of specific rule (based on the random index)
                for question in questions_in_rules[random_index]:
                    # making sure that the question is not already in the temp_questions_to_add array
                    if question not in temp_questions_to_add:
                        # add question to temp_questions_to_add
                        temp_questions_to_add.append(question)
                        break
                    # question already exists in temp_questions_to_add array
                    else:
                        continue
            # updating questions_to_add
            questions_to_add = temp_questions_to_add
        # if the test section component config to shuffle question blocks
        elif shuffle_question_blocks:
            # initializing temp_questions_to_add
            temp_questions_to_add = []
            # shuffling questions_in_rules
            shuffle(questions_in_rules)
            # building the new questions_to_add array based on the questions_in_rules array
            for question_block in questions_in_rules:
                for question in question_block:
                    temp_questions_to_add.append(question)
            # updating questions_to_add
            questions_to_add = temp_questions_to_add

        # initializing temp_array
        temp_array = []
        # creating a simple array with index numbers based on the amount of IDs in questions_to_add
        for index in range(len(questions_to_add)):
            temp_array.append(index)
        # testing from the test builder
        if local_testing:
            # looping in questions_from_test_definition in order to replace the question IDs with question data
            for question in questions_from_test_definition:
                # current question ID exists in questions_to_add
                if question.id in questions_to_add:
                    # getting index of current question ID in questions_to_add array
                    index = questions_to_add.index(question.id)
                    # updating the right index of the temp_array array based on the question data
                    temp_array[index] = question

        # testing from a real test (sample and/or real tests)
        else:
            for question_id in questions_to_add:
                # getting question data
                question = NewQuestion.objects.get(id=question_id["id"])
                # getting index of current question ID in questions_to_add array
                index = [x["id"] for x in questions_to_add].index(question.id)
                # updating the right index ot the temp_array array based on the question data
                temp_array[index] = question

        # updating questions_to_add array
        questions_to_add = temp_array

    # handling dependencies and dependent orders
    for question in questions_to_add:
        # add dependent questions in order/sequence
        dependencies_data = add_dependencies(
            local_testing,
            questions,
            question,
            question_id_array,
            questions_from_test_definition,
        )
        # updating question_id_array and questions
        question_id_array = dependencies_data["updated_question_id_array"]
        questions = dependencies_data["questions_list"]

    # testing from the test builder
    if local_testing:
        # formatting json_response based on questions
        json_response = []
        for question in questions:
            answers = []
            details = {}
            example_rating = []
            sections = []
            situation = {}

            all_answers = test_definition_obj.new_test.answers
            all_answers_details = test_definition_obj.new_test.answer_details

            # populating answers array
            for answer in all_answers:
                if answer.question == question.id:
                    for answer_detail in all_answers_details:
                        if answer_detail.answer == answer.id:
                            answers.append(
                                {
                                    "answer": answer_detail.id,
                                    "content": answer_detail.content,
                                    "id": answer.id,
                                    "language": answer_detail.language,
                                    "order": answer.order,
                                    "question": answer.question,
                                }
                            )

            # shuffle answer choices is enabled
            if question.shuffle_answer_choices:
                shuffle(answers)

            all_multiple_choice_question_details = (
                test_definition_obj.new_test.multiple_choice_question_details
            )

            for (
                multiple_choice_question_details
            ) in all_multiple_choice_question_details:
                if multiple_choice_question_details.question == question.id:
                    details = {
                        "difficulty": multiple_choice_question_details.question_difficulty_type
                    }

            all_question_sections = test_definition_obj.new_test.question_sections
            all_question_section_definitions = (
                test_definition_obj.new_test.question_section_definitions
            )

            # populating answers array
            for question_section in all_question_sections:
                if question_section.question == question.id:
                    for question_section_definition in all_question_section_definitions:
                        if (
                            question_section_definition.question_section
                            == question_section.id
                        ):
                            sections.append(
                                {
                                    "id": question_section.id,
                                    "order": question_section.order,
                                    "question": question_section.question,
                                    "question_section_content": {
                                        "content": question_section_definition.content,
                                        "language": question_section_definition.language,
                                    },
                                    "question_section_type": question_section_definition.question_section_type,
                                }
                            )

            # making sure that the question_block_type exists
            try:
                question_block_type = question.question_block_type
            except AttributeError:
                # if it does not exist, update question_block_type value to None
                question_block_type = None

            json_response.append(
                {
                    "id": question.id,
                    "question_type": question.question_type,
                    "pilot": question.pilot,
                    "question_block_type": question_block_type,
                    "order": question.order,
                    "dependent_order": question.dependent_order,
                    "shuffle_answer_choices": question.shuffle_answer_choices,
                    "ppc_question_id": question.ppc_question_id,
                    "answers": answers,
                    "details": details,
                    # TODO EMIB/INBOX
                    "email": {
                        "competency_types": [],
                        "email_id": None,
                        "question": None,
                    },
                    # TODO EMIB/INBOX
                    "example_rating": example_rating,
                    "sections": sections,
                    # TODO EMIB/INBOX
                    "situation": situation,
                }
            )

        # updating questions_list based on provided json response
        dependencies_data["questions_list"] = json_response

        # returning questions_list
        return Response(dependencies_data["questions_list"], status=status.HTTP_200_OK)

    # testing from a real test (sample and/or real tests)
    else:
        # testing from a real test (with an assigned test ID)
        if assigned_test_id:
            # if there is no assigned_questions_list created yet
            if not assigned_questions_list:
                # looping in questions_list and adding question ID to assigned_questions_list array
                assigned_questions_list = []
                for question in dependencies_data["questions_list"]:
                    # populating assigned_questions_list array
                    assigned_questions_list.append(question.id)
                # creating new row in assignedquestionslist table
                AssignedQuestionsList.objects.create(
                    assigned_test_id=assigned_test_id,
                    test_section_id=test_section.id,
                    questions_list=assigned_questions_list,
                    from_item_bank=False,
                )

        # returning questions_list
        return dependencies_data["questions_list"]


class TestSectionComponentSerializer(serializers.ModelSerializer):
    test_section_component = serializers.SerializerMethodField()
    title = serializers.SerializerMethodField()
    component_type = serializers.SerializerMethodField()

    class Meta:
        model = TestSectionComponent
        fields = [
            "order",
            "id",
            "title",
            "component_type",
            "test_section_component",
            "language",
            "shuffle_question_blocks",
        ]

    def get_title(self, request):
        return {"en": request.en_title, "fr": request.fr_title}

    def get_component_type(self, request):
        return request.component_type

    def get_test_section_component(self, request):
        context = self.context
        # getting assigned_test_id from context if it exists (otherwise None)
        assigned_test_id = self.context.get("assigned_test_id", None)
        if request.language:
            context["language"] = request.language

        context["assigned_test_id"] = assigned_test_id

        component_type = request.component_type
        # factory pattern for test definition model serializing
        if (
            component_type == TestSectionComponentType.SINGLE_PAGE
            or component_type == TestSectionComponentType.SIDE_NAVIGATION
        ):
            # get single component
            pages = SectionComponentPage.objects.filter(
                test_section_component=request.id
            ).order_by("order")

            return SectionComponentPageSerializer(
                pages, many=True, context=context
            ).data

        elif component_type == TestSectionComponentType.INBOX:
            data = {}
            # get question list using Question List Rules
            questions = get_question_list(
                False,
                assigned_test_id,
                request.id,
                request.shuffle_all_questions,
                request.shuffle_question_blocks,
                {},
            )

            data["questions"] = QuestionSerializer(
                questions, many=True, context=context
            ).data
            address_book = AddressBookContact.objects.filter(
                test_section__in=request.test_section.all()
            )
            data["address_book"] = AddressBookContactSerializer(
                address_book, many=True, context=context
            ).data
            return data

        elif component_type == TestSectionComponentType.QUESTION_LIST:
            data = {}
            # get question list using Question List Rules
            questions = get_question_list(
                False,
                assigned_test_id,
                request.id,
                request.shuffle_all_questions,
                request.shuffle_question_blocks,
                {},
            )

            data["questions"] = QuestionSerializer(
                questions, many=True, context=context
            ).data

            return data

        elif component_type == TestSectionComponentType.ITEM_BANK:
            # initializing needed variables
            data = {}

            # updating data object
            data["questions"] = get_item_bank_items_list(
                False, request.id, assigned_test_id
            )

            return data


class InboxScorerSerializer(serializers.ModelSerializer):
    questions = serializers.SerializerMethodField()

    class Meta:
        model = TestSectionComponent
        fields = ["questions"]

    def get_questions(self, request):
        # right now this returns all questions. Later it might be worth returning
        # only questions in the assigned test
        assigned_test_id = self.context["assigned_test_id"]
        questions = NewQuestion.objects.filter(test_section_component=request)

        return NoLanguageDataQuestionSerializer(questions, many=True).data
