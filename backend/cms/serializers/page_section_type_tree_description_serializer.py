from rest_framework import serializers
from cms.cms_models.page_section_type_tree_description import (
    PageSectionTypeTreeDescription,
)
from cms.cms_models.address_book_contact import AddressBookContact
from cms.serializers.address_book_contact_serializer import (
    TreeDescriptionAddressBookContactSerializer,
)


class PageSectionTypeTreeDescriptionSerializer(serializers.ModelSerializer):
    address_book = serializers.SerializerMethodField()

    class Meta:
        model = PageSectionTypeTreeDescription
        fields = ["address_book"]

    def get_address_book(self, request):
        language = self.context.get("language")
        obj = AddressBookContact.objects.filter(id=request.address_book_contact.id)
        return TreeDescriptionAddressBookContactSerializer(
            obj, many=True, context={"language": language}
        ).data
