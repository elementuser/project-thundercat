from rest_framework import serializers
from cms.cms_models.page_section_type_image_zoom import PageSectionTypeImageZoom


class PageSectionTypeImageZoomSerializer(serializers.ModelSerializer):
    class Meta:
        model = PageSectionTypeImageZoom
        fields = "__all__"
