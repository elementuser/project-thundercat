from rest_framework import serializers
from cms.cms_models.situation_example_rating_details import (
    SituationExampleRatingDetails,
)


class SituationExampleRatingDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = SituationExampleRatingDetails
        fields = "__all__"
