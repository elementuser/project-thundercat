from django.db import models
from simple_history.models import HistoricalRecords
from user_management.user_management_models.user_models import User
from user_management.user_management_models.ta_extended_profile_models import (
    TaExtendedProfile,
)


##################################################################################
# ORDERLESS TEST PERMISSIONS MODEL
##################################################################################

MAX_CODE_LEN = 25


class OrderlessTestPermissions(models.Model):
    ta_extended_profile = models.ForeignKey(
        TaExtendedProfile, on_delete=models.DO_NOTHING, null=False
    )
    parent_code = models.CharField(max_length=MAX_CODE_LEN, blank=False, null=False)
    test_code = models.CharField(max_length=MAX_CODE_LEN, blank=False, null=False)
    date_assigned = models.DateTimeField(auto_now_add=True, blank=False, null=False)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "{0}: {1} - {2}".format(
            self.ta_extended_profile, self.parent_code, self.test_code
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Orderless Test Permissions"
        unique_together = (("ta_extended_profile", "test_code", "parent_code"),)
