from django.db import models
from simple_history.models import HistoricalRecords
from cms.cms_models.new_question import NewQuestion
from cms.cms_models.competency_type import CompetencyType


MAX_CHAR_LEN = 100


class SituationExampleRating(models.Model):
    score = models.FloatField()
    question = models.ForeignKey(NewQuestion, on_delete=models.CASCADE)
    competency_type = models.ForeignKey(CompetencyType, on_delete=models.CASCADE)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "id: {0} score: {1}".format(self.id, self.score)
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Situation Example Rating"
