from django.db import models
from simple_history.models import HistoricalRecords
from cms.cms_models.test_definition import TestDefinition


MAX_CHAR_LEN = 150
MAX_DESC_LEN = 250

# Test Model (addional data for an item of type test)
# Stores the test_name pk (a unique string to define the test), item id (parent item id),
# is_public (is a sample test anyone can try out or not), default_time (the default time limit in minutes),
# and type (string specifying the type of test)

# NOTE: Test uses a String pk because migrations/rollbacks can result in different ids for tests
# on a dev machine or a deployed environment. The string key will always be the same, no matter the environment


# ** IMPORTANT **
# If any changes are made to this model, please make sure to notify CAMM team (CAT/CAMM Relation Logic)
class TestSection(models.Model):
    test_definition = models.ForeignKey(TestDefinition, on_delete=models.DO_NOTHING)
    order = models.IntegerField()
    section_type = models.IntegerField()
    default_time = models.IntegerField(null=True, blank=True)
    en_title = models.CharField(max_length=MAX_CHAR_LEN)
    fr_title = models.CharField(max_length=MAX_CHAR_LEN)
    # can only be null on finish/quit sections
    next_section_button_type = models.IntegerField()
    scoring_type = models.IntegerField()
    minimum_score = models.IntegerField(default=0, blank=True, null=True)
    uses_notepad = models.BooleanField(default=False, blank=True, null=True)
    uses_calculator = models.BooleanField(default=False, blank=False, null=False)
    block_cheating = models.BooleanField(default=True, blank=True, null=True)
    default_tab = models.IntegerField(blank=True, null=True)
    item_exposure = models.BooleanField(default=False, blank=False, null=False)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "id: {3}, {0} for {1} version {2}".format(
            self.en_title,
            self.test_definition.test_code,
            self.test_definition.version,
            self.id,
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Test Section"
        ordering = ["test_definition", "id"]
