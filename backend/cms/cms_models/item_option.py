from django.db import models
from simple_history.models import HistoricalRecords
from cms.cms_models.items import Items


class ItemOption(models.Model):
    option_type = models.IntegerField(blank=False, null=False)
    modify_date = models.DateTimeField(auto_now=True)
    score = models.CharField(blank=True, null=True, max_length=25)
    option_order = models.IntegerField(blank=False, null=False)
    exclude_from_shuffle = models.BooleanField(default=0)
    historical_option_id = models.CharField(
        max_length=50, blank=True, null=True, default=""
    )
    rationale = models.TextField(default="")
    item = models.ForeignKey(Items, to_field="id", on_delete=models.DO_NOTHING)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Item Option"
