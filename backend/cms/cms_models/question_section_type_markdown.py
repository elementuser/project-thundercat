from django.db import models
from simple_history.models import HistoricalRecords
from cms.cms_models.question_section import QuestionSection
from backend.custom_models.language import Language


class QuestionSectionTypeMarkdown(models.Model):
    question_section = models.ForeignKey(QuestionSection, on_delete=models.DO_NOTHING)
    language = models.ForeignKey(
        Language, on_delete=models.DO_NOTHING, to_field="ISO_Code_1"
    )
    content = models.TextField()

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate history table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console

    def __str__(self):
        ret = "question id: {0}, section id: {3} [{1}] {2}".format(
            self.question_section.question.id,
            self.language.ISO_Code_1,
            self.content[:20],
            self.question_section.id,
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Question Section Type Markdown"
