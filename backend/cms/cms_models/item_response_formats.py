from django.db import models
from simple_history.models import HistoricalRecords


class ItemResponseFormats(models.Model):
    en_name = models.CharField(max_length=25, blank=False, null=False)
    fr_name = models.CharField(max_length=25, blank=False, null=False)
    codename = models.CharField(max_length=50, null=False, blank=False)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "codename: {0}, name: {1}".format(self.codename, self.en_name)
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Item Response Formats"
        constraints = [
            models.UniqueConstraint(
                name="must_be_a_unique_item_response_format_codename",
                fields=["codename"],
            )
        ]
