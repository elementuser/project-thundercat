from django.db import models
from django.db.models import Q
from simple_history.models import HistoricalRecords
from cms.cms_models.item_bank import ItemBank


class Bundles(models.Model):
    name = models.CharField(max_length=50, blank=False, null=False)
    version_text = models.CharField(max_length=50, blank=True, null=True)
    shuffle_items = models.BooleanField(default=0)
    shuffle_bundles = models.BooleanField(default=0)
    shuffle_between_bundles = models.BooleanField(default=0)
    active = models.BooleanField(default=0)
    item_bank = models.ForeignKey(ItemBank, to_field="id", on_delete=models.DO_NOTHING)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "id: {0}, name: {1}, active: {2}".format(self.id, self.name, self.active)
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Bundles"
        constraints = [
            models.UniqueConstraint(
                name="bundle_name_must_be_unique_in_item_bank",
                fields=["name", "item_bank"],
            ),
            models.CheckConstraint(
                name="shuffle_bundles_and_shuffle_between_bundles_are_mutually_exclusive",
                check=Q(shuffle_bundles=True, shuffle_between_bundles=False)
                | Q(shuffle_bundles=False, shuffle_between_bundles=True)
                | Q(shuffle_bundles=False, shuffle_between_bundles=False),
            ),
        ]
