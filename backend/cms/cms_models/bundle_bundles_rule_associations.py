from django.db import models
from simple_history.models import HistoricalRecords
from cms.cms_models.bundle_bundles_rule import BundleBundlesRule
from cms.cms_models.bundles import Bundles


class BundleBundlesRuleAssociations(models.Model):
    display_order = models.IntegerField(null=False, blank=False)
    bundle = models.ForeignKey(Bundles, to_field="id", on_delete=models.DO_NOTHING)
    bundle_bundles_rule = models.ForeignKey(
        BundleBundlesRule, to_field="id", on_delete=models.DO_NOTHING, default=1
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "id: {0}, display order: {1}, bundle ID: {2} bundle rule ID: {3}".format(
            self.id, self.display_order, self.bundle, self.bundle_bundles_rule
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Bundle Bundles Rule Associations"
