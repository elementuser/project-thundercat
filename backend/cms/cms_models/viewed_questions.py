from django.db import models
from simple_history.models import HistoricalRecords
from user_management.user_management_models.user_models import User


class ViewedQuestions(models.Model):
    # cannot use a ForeignKey for this field, since the ppc_question_id is not a primary key
    ppc_question_id = models.CharField(max_length=25, default="")
    count = models.IntegerField(null=False, default=0)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING, null=False, blank=False)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "id: {0}, PPC Question ID: {1}, Count: {2}".format(
            self.id,
            self.ppc_question_id,
            self.count,
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Question"
