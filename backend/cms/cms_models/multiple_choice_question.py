from django.db import models
from simple_history.models import HistoricalRecords
from cms.cms_models.new_question import NewQuestion


MAX_CHAR_LEN = 100


class MultipleChoiceQuestion(models.Model):
    question = models.ForeignKey(NewQuestion, on_delete=models.DO_NOTHING)
    question_difficulty_type = models.IntegerField()
    # provide user frendly names in Django Admin Console

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    def __str__(self):
        ret = "id: {2} ({0}) difficulty: {1}".format(
            self.question.test_section_component.test_section.all().values(
                "test_definition_id", "en_title"
            ),
            self.question_difficulty_type,
            self.question.id,
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Multiple Choice Question"
