from django.db import models
from simple_history.models import HistoricalRecords
from cms.cms_models.item_option_drafts import ItemOptionDrafts
from backend.custom_models.language import Language


class ItemOptionTextDrafts(models.Model):
    text = models.TextField(blank=True, null=True)
    modify_date = models.DateTimeField(auto_now=True)
    language = models.ForeignKey(
        Language, to_field="language_id", on_delete=models.DO_NOTHING
    )
    item_option = models.ForeignKey(
        ItemOptionDrafts, to_field="id", on_delete=models.DO_NOTHING
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Item Option Text Drafts"
