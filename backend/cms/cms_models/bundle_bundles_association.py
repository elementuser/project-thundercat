from django.db import models
from simple_history.models import HistoricalRecords
from cms.cms_models.bundles import Bundles


class BundleBundlesAssociation(models.Model):
    bundle_source = models.ForeignKey(
        Bundles,
        to_field="id",
        on_delete=models.DO_NOTHING,
        related_name="bundle_source",
    )
    bundle_link = models.ForeignKey(
        Bundles, to_field="id", on_delete=models.DO_NOTHING, related_name="bundle_link"
    )
    display_order = models.IntegerField()

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Bundles Bundles Association"
