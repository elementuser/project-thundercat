from django.db import models
from simple_history.models import HistoricalRecords
from cms.cms_models.item_bank_attribute_values import ItemBankAttributeValues


class ItemBankAttributeValuesText(models.Model):
    item_bank_attribute_values = models.ForeignKey(
        ItemBankAttributeValues, to_field="id", on_delete=models.DO_NOTHING
    )
    text = models.TextField(blank=True, null=True)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "id: {0}, item attribute value id: {1}, text: {2}".format(
            self.id, self.item_bank_attribute_values_id, self.text
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Item Bank Attribute Values Text"
