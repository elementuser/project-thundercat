from django.db import models
from simple_history.models import HistoricalRecords
from cms.cms_models.items import Items
from cms.cms_models.item_development_statuses import ItemDevelopmentStatuses
from cms.cms_models.item_response_formats import ItemResponseFormats
from cms.cms_models.item_bank import ItemBank
from user_management.user_management_models.user_models import User


class ItemDrafts(models.Model):
    system_id = models.CharField(max_length=15, blank=False, null=False)
    historical_id = models.CharField(max_length=50, blank=True, null=False, default="")
    modify_date = models.DateTimeField(auto_now=True)
    item = models.ForeignKey(Items, to_field="id", on_delete=models.DO_NOTHING)
    item_bank = models.ForeignKey(
        ItemBank, to_field="id", on_delete=models.DO_NOTHING, default=1
    )
    username = models.ForeignKey(User, to_field="username", on_delete=models.DO_NOTHING)
    response_format = models.ForeignKey(
        ItemResponseFormats,
        to_field="id",
        on_delete=models.DO_NOTHING,
        blank=True,
        default="",
    )
    development_status = models.ForeignKey(
        ItemDevelopmentStatuses,
        to_field="id",
        on_delete=models.DO_NOTHING,
        blank=True,
        default="",
    )
    active_status = models.BooleanField(default=0)
    shuffle_options = models.BooleanField(default=0)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Item Drafts"
