# Generated by Django 2.2.3 on 2020-11-06 13:15

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('custom_models', '0006_assignedtest_test_order_number'),
        ('cms_models', '0008_questionsectiontypemarkdownend'),
    ]

    operations = [
        migrations.AddField(
            model_name='testsectioncomponent',
            name='language',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='custom_models.Language', to_field='ISO_Code_1'),
        ),
    ]
