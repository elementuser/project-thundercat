# Generated by Django 3.0.14 on 2021-07-20 17:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cms_models', '0024_historicalscoringmethods_historicalscoringmethodtypes_historicalscoringpassfail_historicalscoringthr'),
    ]

    operations = [
        migrations.AddField(
            model_name='historicaltestdefinition',
            name='archived',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='testdefinition',
            name='archived',
            field=models.BooleanField(default=False),
        ),
    ]
