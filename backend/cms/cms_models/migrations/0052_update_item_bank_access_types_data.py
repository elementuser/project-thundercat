from django.db import migrations
from cms.static.item_bank_access_types import ItemBankAccessType


def update_item_bank_access_types_data(apps, schema_editor):
    # get models
    item_bank_accesss_types = apps.get_model("cms_models", "itembankaccesstypes")
    # get db alias
    db_alias = schema_editor.connection.alias

    # updating item bank access types data
    access_type_1 = item_bank_accesss_types.objects.using(db_alias).get(
        codename=ItemBankAccessType.NONE
    )
    access_type_1.en_description = "None"
    access_type_1.fr_description = "Aucun"
    access_type_1.priority = 99
    access_type_1.save()

    access_type_2 = item_bank_accesss_types.objects.using(db_alias).get(
        codename=ItemBankAccessType.OWNER
    )
    access_type_2.en_description = "Owner"
    access_type_2.fr_description = "Propriétaire"
    access_type_2.priority = 1
    access_type_2.save()

    access_type_3 = item_bank_accesss_types.objects.using(db_alias).get(
        codename=ItemBankAccessType.CONTRIBUTOR
    )
    access_type_3.en_description = "Contributor"
    access_type_3.fr_description = "Contributeur"
    access_type_3.priority = 2
    access_type_3.save()


def rollback_changes(apps, schema_editor):
    # get models
    item_bank_accesss_types = apps.get_model("cms_models", "itembankaccesstypes")
    # get db alias
    db_alias = schema_editor.connection.alias

    # updating to previous data
    access_type_1 = item_bank_accesss_types.objects.using(db_alias).get(
        codename=ItemBankAccessType.NONE
    )
    access_type_1.en_description = "None (TEMP)"
    access_type_1.fr_description = "FR None (TEMP)"
    access_type_1.priority = 1
    access_type_1.save()

    access_type_2 = item_bank_accesss_types.objects.using(db_alias).get(
        codename=ItemBankAccessType.OWNER
    )
    access_type_2.en_description = "Owner (TEMP)"
    access_type_2.fr_description = "FR Owner (TEMP)"
    access_type_2.priority = 1
    access_type_2.save()

    access_type_3 = item_bank_accesss_types.objects.using(db_alias).get(
        codename=ItemBankAccessType.CONTRIBUTOR
    )
    access_type_3.en_description = "Contributor (TEMP)"
    access_type_3.fr_description = "FR Contributor (TEMP)"
    access_type_3.priority = 1
    access_type_3.save()


class Migration(migrations.Migration):
    dependencies = [("cms_models", "0051_auto_20221208_1418")]
    operations = [
        migrations.RunPython(update_item_bank_access_types_data, rollback_changes)
    ]
