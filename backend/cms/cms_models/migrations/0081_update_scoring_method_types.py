from django.db import migrations


def update_scoring_method_types(apps, schema_editor):
    # get models
    scoring_method_types = apps.get_model("cms_models", "scoringmethodtypes")
    # get db alias
    db_alias = schema_editor.connection.alias

    method_type_1 = scoring_method_types.objects.using(db_alias).get(method_type_codename="THRESHOLD")
    method_type_1.en_name = "Score Bands"
    method_type_1.fr_name = "Bandes de scores"
    method_type_1.save()

    method_type_2 = scoring_method_types.objects.using(db_alias).get(method_type_codename="PASS_FAIL")
    method_type_2.en_name = "Pass/Fail"
    method_type_2.fr_name = "Succès/Échec"
    method_type_2.save()

    method_type_3 = scoring_method_types.objects.using(db_alias).get(method_type_codename="RAW")
    method_type_3.en_name = "Raw Score"
    method_type_3.fr_name = "Score brut"
    method_type_3.save()

    method_type_4 = scoring_method_types.objects.using(db_alias).get(method_type_codename="PERCENTAGE")
    method_type_4.en_name = "Percentage"
    method_type_4.fr_name = "Pourcentage"
    method_type_4.save()

    method_type_5 = scoring_method_types.objects.using(db_alias).get(method_type_codename="NONE")
    method_type_5.en_name = "None"
    method_type_5.fr_name = "Aucun"
    method_type_5.save()

def rollback_changes(apps, schema_editor):
    # get models
    scoring_method_types = apps.get_model("cms_models", "scoringmethodtypes")
    # get db alias
    db_alias = schema_editor.connection.alias

    method_type_1 = scoring_method_types.objects.using(db_alias).get(method_type_codename="THRESHOLD")
    method_type_1.en_name = "Threshold"
    method_type_1.fr_name = "FR Threshold"
    method_type_1.save()

    method_type_2 = scoring_method_types.objects.using(db_alias).get(method_type_codename="PASS_FAIL")
    method_type_2.en_name = "Pass/Fail"
    method_type_2.fr_name = "FR Pass/Fail"
    method_type_2.save()

    method_type_3 = scoring_method_types.objects.using(db_alias).get(method_type_codename="RAW")
    method_type_3.en_name = "Raw"
    method_type_3.fr_name = "FR Raw"
    method_type_3.save()

    method_type_4 = scoring_method_types.objects.using(db_alias).get(method_type_codename="PERCENTAGE")
    method_type_4.en_name = "Percentage"
    method_type_4.fr_name = "FR Percentage"
    method_type_4.save()

    method_type_5 = scoring_method_types.objects.using(db_alias).get(method_type_codename="NONE")
    method_type_5.en_name = "None"
    method_type_5.fr_name = "FR None"
    method_type_5.save()


class Migration(migrations.Migration):
    dependencies = [("cms_models", "0080_alter_historicaladdressbookcontact_options_and_more")]
    operations = [
        migrations.RunPython(
            update_scoring_method_types, rollback_changes
        )
    ]
