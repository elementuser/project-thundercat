# Generated by Django 3.2.18 on 2023-04-27 15:03

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import simple_history.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('cms_models', '0069_auto_20230425_0827'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='historicalitembankattributevalues',
            name='value_identifier',
        ),
        migrations.RemoveField(
            model_name='itembankattributevalues',
            name='value_identifier',
        ),
        migrations.AddField(
            model_name='historicalitembankattributevalues',
            name='attribute_value_type',
            field=models.IntegerField(default=1),
        ),
        migrations.AddField(
            model_name='itembankattributevalues',
            name='attribute_value_type',
            field=models.IntegerField(default=1),
        ),
        migrations.CreateModel(
            name='ItemBankAttributeValuesText',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.TextField(blank=True, null=True)),
                ('item_bank_attribute_values', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='cms_models.itembankattributevalues')),
            ],
            options={
                'verbose_name_plural': 'Item Bank Attribute Values Text',
            },
        ),
        migrations.CreateModel(
            name='ItemAttributeValue',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('item', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='cms_models.items')),
                ('item_bank_attribute_value', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='cms_models.itembankattributevalues')),
            ],
            options={
                'verbose_name_plural': 'Item Attribute Values',
            },
        ),
        migrations.CreateModel(
            name='HistoricalItemBankAttributeValuesText',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('text', models.TextField(blank=True, null=True)),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
                ('history_user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('item_bank_attribute_values', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='cms_models.itembankattributevalues')),
            ],
            options={
                'verbose_name': 'historical item bank attribute values text',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
        migrations.CreateModel(
            name='HistoricalItemAttributeValue',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
                ('history_user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('item', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='cms_models.items')),
                ('item_bank_attribute_value', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='cms_models.itembankattributevalues')),
            ],
            options={
                'verbose_name': 'historical item attribute value',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
    ]
