from django.db import migrations
from cms.static.items_utils import ItemDevelopmentStatus


OLD_PILOT_NAME_EN = "PILOT"
NEW_PILOT_NAME_EN = "Experimental"
OLD_PILOT_NAME_FR = "FR PILOT"
NEW_PILOT_NAME_FR = "Expérimental"

OLD_REGULAR_NAME_EN = "REGULAR"
NEW_REGULAR_NAME_EN = "Regular"
OLD_REGULAR_NAME_FR = "FR REGULAR"
NEW_REGULAR_NAME_FR = "Régulier"

OLD_RETIRED_NAME_EN = "RETIRED"
NEW_RETIRED_NAME_EN = "Archived"
OLD_RETIRED_NAME_FR = "FR RETIRED"
NEW_RETIRED_NAME_FR = "Archivé"

OLD_REVIEW_NEEDED_CODENAME = "is_review_needed"
NEW_REVIEW_NEEDED_CODENAME = "is_draft"
OLD_REVIEW_NEEDED_NAME_EN = "REVIEW_NEEDED"
NEW_REVIEW_NEEDED_NAME_EN = "Draft"
OLD_REVIEW_NEEDED_NAME_FR = "FR REVIEW_NEEDED"
NEW_REVIEW_NEEDED_NAME_FR = "Ébauche"


def update_item_development_statuses(apps, schema_editor):
    # get models
    item_development_status = apps.get_model("cms_models", "itemdevelopmentstatuses")
    # get db alias
    db_alias = schema_editor.connection.alias

    # updating respective item development statuses
    development_status_1 = item_development_status.objects.using(db_alias).get(
        codename=ItemDevelopmentStatus.PILOT
    )
    development_status_1.en_name = NEW_PILOT_NAME_EN
    development_status_1.fr_name = NEW_PILOT_NAME_FR
    development_status_1.save()

    development_status_2 = item_development_status.objects.using(db_alias).get(
        codename=ItemDevelopmentStatus.REGULAR
    )
    development_status_2.en_name = NEW_REGULAR_NAME_EN
    development_status_2.fr_name = NEW_REGULAR_NAME_FR
    development_status_2.save()

    development_status_3 = item_development_status.objects.using(db_alias).get(
        codename=ItemDevelopmentStatus.RETIRED
    )
    development_status_3.en_name = NEW_RETIRED_NAME_EN
    development_status_3.fr_name = NEW_RETIRED_NAME_FR
    development_status_3.save()

    development_status_4 = item_development_status.objects.using(db_alias).get(
        codename=OLD_REVIEW_NEEDED_CODENAME
    )
    development_status_4.codename = NEW_REVIEW_NEEDED_CODENAME
    development_status_4.en_name = NEW_REVIEW_NEEDED_NAME_EN
    development_status_4.fr_name = NEW_REVIEW_NEEDED_NAME_FR
    development_status_4.save()


def rollback_changes(apps, schema_editor):
    # get models
    item_development_status = apps.get_model("cms_models", "itemdevelopmentstatuses")
    # get db alias
    db_alias = schema_editor.connection.alias

    # updating respective item development statuses
    development_status_1 = item_development_status.objects.using(db_alias).get(
        codename=ItemDevelopmentStatus.PILOT
    )
    development_status_1.en_name = OLD_PILOT_NAME_EN
    development_status_1.fr_name = OLD_PILOT_NAME_FR
    development_status_1.save()

    development_status_2 = item_development_status.objects.using(db_alias).get(
        codename=ItemDevelopmentStatus.REGULAR
    )
    development_status_2.en_name = OLD_REGULAR_NAME_EN
    development_status_2.fr_name = OLD_REGULAR_NAME_FR
    development_status_2.save()

    development_status_3 = item_development_status.objects.using(db_alias).get(
        codename=ItemDevelopmentStatus.RETIRED
    )
    development_status_3.en_name = OLD_RETIRED_NAME_EN
    development_status_3.fr_name = OLD_RETIRED_NAME_FR
    development_status_3.save()

    development_status_4 = item_development_status.objects.using(db_alias).get(
        codename=NEW_REVIEW_NEEDED_CODENAME
    )
    development_status_4.codename = OLD_REVIEW_NEEDED_CODENAME
    development_status_4.en_name = OLD_REVIEW_NEEDED_NAME_EN
    development_status_4.fr_name = OLD_REVIEW_NEEDED_NAME_FR
    development_status_4.save()


class Migration(migrations.Migration):
    dependencies = [("cms_models", "0059_updating_item_response_formats")]
    operations = [
        migrations.RunPython(update_item_development_statuses, rollback_changes)
    ]
