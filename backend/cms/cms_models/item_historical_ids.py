from django.db import models
from simple_history.models import HistoricalRecords
from cms.cms_models.item_bank import ItemBank


class ItemHistoricalIds(models.Model):
    historical_id = models.CharField(max_length=50, blank=False, null=False)
    system_id = models.CharField(max_length=15, blank=False, null=False, default="TEMP")
    item_bank = models.ForeignKey(
        ItemBank, to_field="id", on_delete=models.DO_NOTHING, default=1
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "id: {0}, system_id: {1}, historical_id: {2}, item_bank_id: {3}".format(
            self.id, self.system_id, self.historical_id, self.item_bank
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Item Historical IDs"
        constraints = [
            models.UniqueConstraint(
                name="must_be_a_unique_item_historical_id_and_item_bank_combination",
                fields=["historical_id", "item_bank"],
            )
        ]
