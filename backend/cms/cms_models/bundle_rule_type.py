from django.db import models
from simple_history.models import HistoricalRecords


class BundleRuleType(models.Model):
    codename = models.CharField(max_length=50, default="temp", null=False, blank=False)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "id: {0}, codename: {1}".format(self.id, self.codename)
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Bundle Rule Type"
