from django.db import models
from simple_history.models import HistoricalRecords
from cms.cms_models.item_bank import ItemBank
from cms.cms_models.item_bank_access_types import ItemBankAccessTypes
from user_management.user_management_models.user_models import User


class ItemBankPermissions(models.Model):
    username = models.ForeignKey(User, to_field="username", on_delete=models.DO_NOTHING)
    item_bank = models.ForeignKey(ItemBank, to_field="id", on_delete=models.DO_NOTHING)
    item_bank_access_type = models.ForeignKey(
        ItemBankAccessTypes, to_field="id", on_delete=models.DO_NOTHING
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "id: {0}, item bank id: {1}, item bank access type id: {2}".format(
            self.id, self.item_bank_id, self.item_bank_access_type_id
        )
        return ret
