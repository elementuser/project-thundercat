from django.db import models
from simple_history.models import HistoricalRecords
from backend.static.psc_department import PscDepartment

from backend.custom_models.language import Language


class ItemBank(models.Model):
    custom_item_bank_id = models.CharField(max_length=25, default="")
    en_name = models.CharField(max_length=150, blank=True)
    fr_name = models.CharField(max_length=150, blank=True)
    comments = models.CharField(max_length=200, blank=True, default="")
    department_id = models.IntegerField(
        null=False, blank=False, default=PscDepartment.DEPT_ID
    )
    language = models.ForeignKey(
        Language,
        to_field="language_id",
        on_delete=models.DO_NOTHING,
        null=True,
        blank=False,
        default=1,
    )
    modify_date = models.DateTimeField(auto_now=True)
    archived = models.BooleanField(default=False)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "id: {0}, custom item bank id: {1}, name: {2}".format(
            self.id, self.custom_item_bank_id, self.en_name
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Item Bank"
        constraints = [
            models.UniqueConstraint(
                name="must_be_a_unique_custom_item_bank_id",
                fields=["custom_item_bank_id"],
            )
        ]
