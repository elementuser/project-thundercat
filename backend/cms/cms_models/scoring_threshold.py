from django.db import models
from simple_history.models import HistoricalRecords
from cms.cms_models.scoring_methods import ScoringMethods


MAX_CHAR_LEN = 50


class ScoringThreshold(models.Model):
    minimum_score = models.IntegerField()
    maximum_score = models.IntegerField()
    en_conversion_value = models.TextField(max_length=MAX_CHAR_LEN)
    fr_conversion_value = models.TextField(max_length=MAX_CHAR_LEN)
    scoring_method = models.ForeignKey(ScoringMethods, on_delete=models.DO_NOTHING)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate history table
    history = HistoricalRecords()
    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "id: {0} scoring method id: {1} [from {2} to {3} = {4}]".format(
            self.id,
            self.scoring_method,
            self.minimum_score,
            self.maximum_score,
            self.en_conversion_value,
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Scoring Threshold"
