from django.db import models
from simple_history.models import HistoricalRecords
from user_management.user_management_models.user_models import User
from cms.cms_models.item_bank_attribute_values import ItemBankAttributeValues
from cms.cms_models.items import Items


class ItemAttributeValueDrafts(models.Model):
    item = models.ForeignKey(Items, to_field="id", on_delete=models.DO_NOTHING)
    item_bank_attribute_value = models.ForeignKey(
        ItemBankAttributeValues, to_field="id", on_delete=models.DO_NOTHING
    )
    system_id = models.CharField(max_length=15, blank=False, null=False)
    username = models.ForeignKey(User, to_field="username", on_delete=models.DO_NOTHING)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "item_id: {0}, item_bank_attribute_value_id: {1}, system_id: {2}, username: {3}".format(
            self.item, self.item_bank_attribute_value, self.system_id, self.username
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Item Attribute Values Drafts"
