from django.db import models
from simple_history.models import HistoricalRecords
from cms.cms_models.bundles import Bundles
from cms.cms_models.item_bank import ItemBank
from cms.cms_models.test_section_component import TestSectionComponent


# this is a rule for a question list (using the Item Bank type)


class ItemBankRule(models.Model):
    order = models.IntegerField(null=True, blank=True)
    test_section_component = models.ForeignKey(
        TestSectionComponent, on_delete=models.DO_NOTHING
    )
    item_bank = models.ForeignKey(ItemBank, on_delete=models.DO_NOTHING)
    item_bank_bundle = models.ForeignKey(Bundles, on_delete=models.DO_NOTHING)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate history table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "id: {0} -> {1}  ({2})".format(
            self.id, self.item_bank, self.item_bank_bundle
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Item Bank Rule"
