from cms.cms_models.test_permissions_model import TestPermissions
from cms.cms_models.orderless_test_permissions_model import OrderlessTestPermissions
from cms.cms_models.page_section import PageSection
from cms.cms_models.page_section_type_markdown import PageSectionTypeMarkdown
from cms.cms_models.reducer_control import ReducerControl
from cms.cms_models.test_definition import TestDefinition
from cms.cms_models.test_section_reducer import TestSectionReducer
from cms.cms_models.test_section import TestSection
from cms.cms_models.page_section_type_image_zoom import PageSectionTypeImageZoom
from cms.cms_models.test_section_component import TestSectionComponent
from cms.cms_models.section_component_page import SectionComponentPage
from cms.cms_models.page_section_type_sample_email import PageSectionTypeSampleEmail
from cms.cms_models.page_section_type_sample_email_response import (
    PageSectionTypeSampleEmailResponse,
)
from cms.cms_models.page_section_type_sample_task_response import (
    PageSectionTypeSampleTaskResponse,
)
from cms.cms_models.file_resource_details import FileResourceDetails
from cms.cms_models.next_section_button_type_proceed import NextSectionButtonTypeProceed


from cms.cms_models.multiple_choice_question import MultipleChoiceQuestion
from cms.cms_models.new_question import NewQuestion
from cms.cms_models.question_block_type import QuestionBlockType
from cms.cms_models.question_section_type_markdown import QuestionSectionTypeMarkdown
from cms.cms_models.question_section import QuestionSection
from cms.cms_models.answer import Answer
from cms.cms_models.answer_details import AnswerDetails
from cms.cms_models.question_list_rule import QuestionListRule

# email stuff
from cms.cms_models.address_book_contact import AddressBookContact
from cms.cms_models.address_book_contact_details import AddressBookContactDetails
from cms.cms_models.page_section_type_tree_description import (
    PageSectionTypeTreeDescription,
)
from cms.cms_models.email_question import EmailQuestion
from cms.cms_models.email_question_details import EmailQuestionDetails
from cms.cms_models.next_section_button_type_popup import NextSectionButtonTypePopup
from cms.cms_models.competency_type import CompetencyType
from cms.cms_models.question_situation import QuestionSituation
from cms.cms_models.situation_example_rating import SituationExampleRating
from cms.cms_models.situation_example_rating_details import (
    SituationExampleRatingDetails,
)
from cms.cms_models.scoring_method_types import ScoringMethodTypes
from cms.cms_models.scoring_methods import ScoringMethods
from cms.cms_models.scoring_threshold import ScoringThreshold
from cms.cms_models.scoring_pass_fail import ScoringPassFail
from cms.cms_models.item_bank import ItemBank
from cms.cms_models.item_bank_attributes import ItemBankAttributes
from cms.cms_models.item_bank_attribute_values import ItemBankAttributeValues
from cms.cms_models.item_bank_permissions import ItemBankPermissions
from cms.cms_models.item_bank_access_types import ItemBankAccessTypes
from cms.cms_models.viewed_questions import ViewedQuestions
from cms.cms_models.items import Items
from cms.cms_models.item_comments import ItemComments
from cms.cms_models.item_development_statuses import ItemDevelopmentStatuses
from cms.cms_models.item_historical_ids import ItemHistoricalIds
from cms.cms_models.item_response_formats import ItemResponseFormats
from cms.cms_models.item_drafts import ItemDrafts
from cms.cms_models.item_content import ItemContent
from cms.cms_models.item_content_text import ItemContentText
from cms.cms_models.item_option import ItemOption
from cms.cms_models.item_option_text import ItemOptionText
from cms.cms_models.item_content_drafts import ItemContentDrafts
from cms.cms_models.item_content_text_drafts import ItemContentTextDrafts
from cms.cms_models.item_option_drafts import ItemOptionDrafts
from cms.cms_models.item_option_text_drafts import ItemOptionTextDrafts
from cms.cms_models.item_bank_attributes_text import ItemBankAttributesText
from cms.cms_models.item_attribute_value import ItemAttributeValue
from cms.cms_models.item_bank_attribute_values_text import ItemBankAttributeValuesText
from cms.cms_models.item_attribute_value_drafts import ItemAttributeValueDrafts
from cms.cms_models.bundles import Bundles
from cms.cms_models.bundle_items_association import BundleItemsAssociation
from cms.cms_models.bundle_bundles_association import BundleBundlesAssociation
from cms.cms_models.bundle_rule import BundleRule
from cms.cms_models.bundle_rule_type import BundleRuleType
from cms.cms_models.bundle_items_basic_rule import BundleItemsBasicRule
from cms.cms_models.bundle_bundles_rule import BundleBundlesRule
from cms.cms_models.bundle_bundles_rule_associations import (
    BundleBundlesRuleAssociations,
)
from cms.cms_models.item_bank_rule import ItemBankRule

# These imports help to auto discover the models
