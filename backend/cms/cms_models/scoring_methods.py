from django.db import models
from simple_history.models import HistoricalRecords
from cms.cms_models.scoring_method_types import ScoringMethodTypes
from cms.cms_models.test_definition import TestDefinition


MAX_CHAR_LEN = 100


# ** IMPORTANT **
# If any changes are made to this model, please make sure to notify CAMM team (CAT/CAMM Relation Logic)
class ScoringMethods(models.Model):
    method_type = models.ForeignKey(ScoringMethodTypes, on_delete=models.DO_NOTHING)
    test = models.ForeignKey(TestDefinition, on_delete=models.DO_NOTHING)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate history table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "id: {0} method type: {1} test_id: {2}".format(
            self.id, self.method_type, self.test
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Scoring Methods"
