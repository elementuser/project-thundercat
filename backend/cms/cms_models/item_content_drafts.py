from django.db import models
from simple_history.models import HistoricalRecords
from cms.cms_models.items import Items
from user_management.user_management_models.user_models import User


class ItemContentDrafts(models.Model):
    system_id = models.CharField(max_length=15, blank=False, null=False)
    content_type = models.IntegerField(blank=False, null=False)
    modify_date = models.DateTimeField(auto_now=True)
    content_order = models.IntegerField(blank=False, null=False)
    item = models.ForeignKey(Items, to_field="id", on_delete=models.DO_NOTHING)
    username = models.ForeignKey(User, to_field="username", on_delete=models.DO_NOTHING)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Item Content Drafts"
