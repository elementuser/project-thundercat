from django.db import models
from simple_history.models import HistoricalRecords
from cms.cms_models.items import Items
from user_management.user_management_models.user_models import User


class ItemComments(models.Model):
    comment = models.CharField(max_length=255, blank=False, null=False)
    modify_date = models.DateTimeField(auto_now=True)
    system_id = models.CharField(max_length=15, blank=False, null=False, default="")
    version = models.IntegerField(blank=False, null=False, default=1)
    username = models.ForeignKey(User, to_field="username", on_delete=models.DO_NOTHING)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "id: {0}, comment: {1}, username_id: {2}".format(
            self.id, self.comment, self.username
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Item Comments"
