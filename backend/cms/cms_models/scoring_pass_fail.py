from django.db import models
from simple_history.models import HistoricalRecords
from cms.cms_models.scoring_methods import ScoringMethods


MAX_CHAR_LEN = 100


# ** IMPORTANT **
# If any changes are made to this model, please make sure to notify CAMM team (CAT/CAMM Relation Logic)
class ScoringPassFail(models.Model):
    minimum_score = models.IntegerField()
    scoring_method = models.ForeignKey(ScoringMethods, on_delete=models.DO_NOTHING)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate history table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "id: {0} scoring method id: {1} minimum score: {2}".format(
            self.id, self.scoring_method, self.minimum_score
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Scoring Pass/Fail"
