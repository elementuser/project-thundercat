from django.db import models
from simple_history.models import HistoricalRecords
from cms.cms_models.bundle_rule import BundleRule


class BundleBundlesRule(models.Model):
    number_of_bundles = models.IntegerField(null=True, blank=False)
    shuffle_bundles = models.BooleanField(default=False)
    keep_items_together = models.BooleanField(default=False)
    display_order = models.IntegerField(default=1)
    bundle_rule = models.ForeignKey(
        BundleRule, to_field="id", on_delete=models.DO_NOTHING
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "id: {0}, number of bundles: {1}, bundle rule ID: {2}".format(
            self.id, self.number_of_bundles, self.bundle_rule
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Bundle Bundles Rule"
