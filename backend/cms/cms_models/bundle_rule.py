from django.db import models
from simple_history.models import HistoricalRecords
from cms.cms_models.bundles import Bundles
from cms.cms_models.bundle_rule_type import BundleRuleType


class BundleRule(models.Model):
    rule_type = models.ForeignKey(
        BundleRuleType, to_field="id", on_delete=models.DO_NOTHING
    )
    bundle = models.ForeignKey(Bundles, to_field="id", on_delete=models.DO_NOTHING)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "id: {0}, bundle ID: {1}, rule type: {2}".format(
            self.id, self.bundle, self.rule_type
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Bundle Rule"
