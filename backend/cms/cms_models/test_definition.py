from django.db import models
from simple_history.models import HistoricalRecords

MAX_CHAR_LEN = 500
MAX_CODE_LEN = 25
MAX_DESC_LEN = 175


# ** IMPORTANT **
# If any changes are made to this model, please make sure to notify CAMM team (CAT/CAMM Relation Logic)
class TestDefinition(models.Model):
    parent_code = models.CharField(max_length=MAX_CODE_LEN, default="change-me")
    test_code = models.CharField(max_length=MAX_CODE_LEN)
    version = models.IntegerField(null=False, blank=False)
    version_notes = models.CharField(max_length=MAX_CHAR_LEN, default="")
    en_name = models.CharField(max_length=MAX_DESC_LEN)
    fr_name = models.CharField(max_length=MAX_DESC_LEN)
    is_uit = models.BooleanField(default=False)
    is_public = models.BooleanField(default=True)
    count_up = models.BooleanField(default=False)
    active = models.BooleanField(default=False)
    retest_period = models.IntegerField(default=0)
    archived = models.BooleanField(default=False)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "id: {3}, {0} code: {1} version: {2}".format(
            self.en_name, self.test_code, self.version, self.id
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Test Definition"
        unique_together = (("test_code", "version", "parent_code"),)
        # this will order all returned results by these fields
        ordering = ["test_code", "version"]


# unique E639A1
