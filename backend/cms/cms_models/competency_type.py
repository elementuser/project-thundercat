from django.db import models
from simple_history.models import HistoricalRecords
from cms.cms_models.test_definition import TestDefinition


MAX_CHAR_LEN = 100


class CompetencyType(models.Model):
    fr_name = models.CharField(max_length=MAX_CHAR_LEN)
    en_name = models.CharField(max_length=MAX_CHAR_LEN)
    test_definition = models.ForeignKey(TestDefinition, on_delete=models.DO_NOTHING)
    max_score = models.IntegerField(default=0)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "id: {0} type name: {1}".format(self.id, self.en_name)
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Competency Type"
