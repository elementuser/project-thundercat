class ElementToUpdate:
    ITEM_BANK_DEFINITION = "item_bank_definition"
    ITEM_BANK_ATTRIBUTES = "item_bank_attributes"


class ItemActionType:
    SAVE_DRAFT = "save_draft"
    UPLOAD = "upload"
    UPLOAD_OVERWRITE = "upload_overwrite"
    ACTIVATE_DEACTIVATE = "activate_deactivate"


# unique codename of respective access types
class ItemBankAccessType:
    NONE = "is_none"
    OWNER = "is_owner"
    CONTRIBUTOR = "is_contributor"


DRAFT_VERSION = "DRAFT"
DRAFT_OPTION = {"item_id": DRAFT_VERSION, "version": DRAFT_VERSION}

DRAFT_VERSION = "DRAFT"
DRAFT_OPTION = {"item_id": DRAFT_VERSION, "version": DRAFT_VERSION}


class ItemContentType:
    MARKDOWN = 1
    SCREEN_READER = 2


class ItemOptionType:
    MULTIPLE_CHOICE = 1
    MULTIPLE_CHOICE_SCREEN_READER = 2


class AttributeValueType:
    TEXT = 1


# mirror of
# Constants.js in frontend/src/components/testBuilder/itemBank
