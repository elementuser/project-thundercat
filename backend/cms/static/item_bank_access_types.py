class ItemBankAccessType:
    NONE = "is_none"
    OWNER = "is_owner"
    CONTRIBUTOR = "is_contributor"


# mirror of
# ItemBankAccessType in frontend/src/components/testFactory/Constants.js
