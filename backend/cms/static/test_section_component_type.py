class TestSectionComponentType:
    SINGLE_PAGE = 1
    SIDE_NAVIGATION = 2
    INBOX = 3
    QUESTION_LIST = 4  # multiple choice questions
    ITEM_BANK = 5  # questions/items from the item bank


# mirror of
# Constants.js in frontend/src/components/testFactory
