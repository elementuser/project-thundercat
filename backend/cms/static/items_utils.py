class ItemResponseFormat:
    MULTIPLE_CHOICE = "is_mc"


class ItemDevelopmentStatus:
    PILOT = "is_pilot"
    REGULAR = "is_regular"
    RETIRED = "is_retired"
    DRAFT = "is_draft"


class ItemActiveStatus:
    ACTIVE = "is_active"
    DORMANT = "is_dormant"


# mirror of
# Constants.js in frontend/src/components/testBuilder/itemBank/items
