from operator import or_
from functools import reduce
from urllib import parse
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import permissions
from rest_framework import status
from django.db.models import Q
from backend.api_permissions.role_based_api_permissions import (
    HasSystemAdminPermission,
    HasTestDeveloperPermission,
    HasTestBuilderPermission,
)
from backend.custom_models.test_access_code_model import TestAccessCode
from backend.views.utils import get_user_info_from_jwt_token
from cms.cms_models.test_definition import TestDefinition
from cms.serializers.test_definition_serializer import (
    TestDefinitionVersionsCollectedSerializer,
    TestDefinitionTestCodesCollectedSerializer,
    TestDefinitionDataSerializer,
)
from cms.cms_models.test_permissions_model import TestPermissions
from cms.views.utils import get_needed_parameters
from user_management.views.utils import CustomPagination
from user_management.static.permission import Permission
from user_management.user_management_models.user_models import User
from user_management.user_management_models.custom_permissions_model import (
    CustomPermissions,
)
from user_management.user_management_models.custom_user_permissions_model import (
    CustomUserPermissions,
)

# oec-cat/api/get-test-definition-data
class GetTestDefinitionData(APIView):
    permission_classes = (
        permissions.IsAuthenticated
        & (HasTestBuilderPermission | HasTestDeveloperPermission),
    )

    # Please provide page and page_size parameters if you want to use the pagination (optional) ==> else provided it as "null"
    # Please set combine_versions to true if you want to only get the parent codes and the test codes without getting all the available versions
    # Please set only_active_versions to true if you want to get only active test definition versions (note that you cannot set that parameter to true and combine_versions setting to true at the same time)
    # Please provide parent_code and/or test_code and/or version parameters to refine the data you need (optional) ==> else provided them as "null"
    # Please provide keyword and current_language parameters if you want to get data from a search keyword (page and page_size need to be well defined as well)
    def get(self, request):
        user_info = get_user_info_from_jwt_token(request)

        combine_versions = request.query_params.get("combine_versions", None)
        parent_code = request.query_params.get("parent_code", None)
        test_code = request.query_params.get("test_code", None)
        version = request.query_params.get("version", None)
        page = request.query_params.get("page", None)
        page_size = request.query_params.get("page_size", None)
        # decode keyword to get special characters
        keyword = parse.unquote(request.query_params.get("keyword", None))
        current_language = request.query_params.get("current_language", None)
        only_active_versions = request.query_params.get("only_active_versions", None)
        archived = request.query_params.get("archived", None)

        # initializing required variables
        filters = {}
        has_pages = False
        is_a_search_request = False
        allowed_to_view_tests_not_using_any_item_bank = False

        # checking if user has the test builder role or is a super user
        test_builder_permission_id = CustomPermissions.objects.get(
            codename=Permission.TEST_BUILDER
        ).permission_id
        has_test_builder_role = CustomUserPermissions.objects.filter(
            permission_id=test_builder_permission_id, user_id=user_info["username"]
        )

        is_super_user = User.objects.get(username=user_info["username"]).is_staff

        # user has the Test Builder role or is a super user
        if has_test_builder_role or is_super_user:
            # updating allowed_to_view_tests_without_item_bank
            allowed_to_view_tests_not_using_any_item_bank = True

        # if keyword and current_language are defined
        if keyword != "null" and current_language != "null":
            # make sure that page and page_size parameters are also defined
            if page == "null" or page_size == "null":
                return Response(
                    {
                        "error": "please make sure that 'page' and 'page_size' parameters are well defined"
                    },
                    status=status.HTTP_400_BAD_REQUEST,
                )
            else:
                is_a_search_request = True

        # if only_active_versions and combine_versions parameters are set to True
        if only_active_versions == "true" and combine_versions == "true":
            # returning error message
            return Response(
                {
                    "error": "please make sure that either only_active_versions or combine_versions parameter is set to True (not both)"
                },
                status=status.HTTP_400_BAD_REQUEST,
            )

        # if page and page_size are defined
        if page != "null" and page_size != "null":
            # pagination class
            pagination_class = CustomPagination
            paginator = pagination_class()
            # initializing current_page and page_size
            current_page = int(page)
            page_size = int(page_size)
            has_pages = True

        # if only_active_versions is set to true
        if only_active_versions == "true":
            filters["active"] = 1

        # if archived is set to true
        if archived == "true":
            filters["archived"] = 1
        # if archived is unset or set to false
        else:
            filters["archived"] = 0

        # if this is a search call
        if is_a_search_request:
            # if blank search
            if keyword == " ":
                # initializing serialized
                serialized = None

                # combine_version is set to true
                if combine_versions == "true":
                    found_test_definition_tests = (
                        TestDefinition.objects.filter(**filters)
                        .order_by("parent_code", "test_code")
                        .values("parent_code", "test_code")
                        .distinct()
                    )
                    serialized = TestDefinitionTestCodesCollectedSerializer(
                        found_test_definition_tests,
                        many=True,
                        context={
                            "allowed_to_view_tests_not_using_any_item_bank": allowed_to_view_tests_not_using_any_item_bank
                        },
                    )
                # combine_version is not set or is set to false
                else:
                    found_test_definition_tests = TestDefinition.objects.filter(
                        **filters
                    ).order_by("parent_code", "test_code")
                    serialized = TestDefinitionDataSerializer(
                        found_test_definition_tests,
                        many=True,
                        context={
                            "allowed_to_view_tests_not_using_any_item_bank": allowed_to_view_tests_not_using_any_item_bank
                        },
                    )
            # regular search
            else:
                # search while interface is in English
                if current_language == "en":
                    # getting all test definition test ids based on keyword that is matching with en_name
                    test_definition_test_ids = TestDefinition.objects.filter(
                        Q(**filters)
                        & (
                            Q(parent_code__icontains=keyword)
                            | Q(test_code__icontains=keyword)
                            | Q(en_name__icontains=keyword)
                        )
                    )
                # search while interface is in French
                else:
                    # getting all test definition test ids based on keyword that is matching with en_name
                    test_definition_test_ids = TestDefinition.objects.filter(
                        Q(**filters)
                        & (
                            Q(parent_code__icontains=keyword)
                            | Q(test_code__icontains=keyword)
                            | Q(fr_name__icontains=keyword)
                        )
                    )

                if test_definition_test_ids.exists():
                    # combine_version is set to true
                    if combine_versions == "true":
                        # getting all test definition tests where there are matches with provided keyword
                        found_test_definition_tests = (
                            TestDefinition.objects.filter(
                                reduce(
                                    or_,
                                    [
                                        Q(id=test_definition_test_id.id)
                                        for test_definition_test_id in test_definition_test_ids
                                    ],
                                )
                            )
                            .order_by("parent_code", "test_code")
                            .values("parent_code", "test_code")
                            .distinct()
                        )
                        # only getting data for current selected page
                        new_found_test_definition_tests = found_test_definition_tests[
                            (current_page - 1) * page_size : current_page * page_size
                        ]
                        serialized = TestDefinitionTestCodesCollectedSerializer(
                            new_found_test_definition_tests,
                            many=True,
                            context={
                                "allowed_to_view_tests_not_using_any_item_bank": allowed_to_view_tests_not_using_any_item_bank
                            },
                        )
                    # combine_version is set to false
                    else:
                        # getting all test definition tests where there are matches with provided keyword
                        found_test_definition_tests = TestDefinition.objects.filter(
                            reduce(
                                or_,
                                [
                                    Q(id=test_definition_test_id.id)
                                    for test_definition_test_id in test_definition_test_ids
                                ],
                            )
                        ).order_by("parent_code", "test_code")
                        # only getting data for current selected page
                        new_found_test_definition_tests = found_test_definition_tests[
                            (current_page - 1) * page_size : current_page * page_size
                        ]
                        serialized = TestDefinitionDataSerializer(
                            found_test_definition_tests,
                            many=True,
                            context={
                                "allowed_to_view_tests_not_using_any_item_bank": allowed_to_view_tests_not_using_any_item_bank
                            },
                        )
                # there are no results found based on the keyword provided
                else:
                    return Response({"no results found"}, status=status.HTTP_200_OK)

            return paginator.get_paginated_response(
                serialized.data,
                found_test_definition_tests.count(),
                current_page,
                page_size,
            )

        # this is not a search call
        else:
            # parent_code is defined (not null)
            if parent_code != "null":
                filters["parent_code"] = parent_code

            # test_code is defined (not null)
            if test_code != "null":
                filters["test_code"] = test_code

            # version is defined (not null)
            if version != "null":
                filters["version"] = version

            # combine_version is set to true
            if combine_versions == "true":
                test_definition_data = (
                    TestDefinition.objects.filter(**filters)
                    .order_by("parent_code", "test_code")
                    .values("parent_code", "test_code")
                    .distinct()
                )

                # has_pages is True
                if has_pages:
                    # only getting data for current selected page
                    new_test_definition_data = test_definition_data[
                        (current_page - 1) * page_size : current_page * page_size
                    ]
                    serialized = TestDefinitionTestCodesCollectedSerializer(
                        new_test_definition_data,
                        many=True,
                        context={
                            "allowed_to_view_tests_not_using_any_item_bank": allowed_to_view_tests_not_using_any_item_bank
                        },
                    )
                    return paginator.get_paginated_response(
                        serialized.data,
                        test_definition_data.count(),
                        current_page,
                        page_size,
                    )
                # has_pages is False
                else:
                    serialized = TestDefinitionTestCodesCollectedSerializer(
                        test_definition_data,
                        many=True,
                        context={
                            "allowed_to_view_tests_not_using_any_item_bank": allowed_to_view_tests_not_using_any_item_bank
                        },
                    )
                    return Response(
                        {
                            "next_page_number": None,
                            "previous_page_number": None,
                            "count": None,
                            "current_page_number": None,
                            "results": serialized.data,
                        }
                    )

            # combine_version is not set or is set to false
            else:
                test_definition_data = TestDefinition.objects.filter(
                    **filters
                ).order_by("parent_code", "test_code")

                # has_pages is True
                if has_pages:
                    # only getting data for current selected page
                    new_test_definition_data = test_definition_data[
                        (current_page - 1) * page_size : current_page * page_size
                    ]
                    serialized = TestDefinitionDataSerializer(
                        new_test_definition_data,
                        many=True,
                        context={
                            "allowed_to_view_tests_not_using_any_item_bank": allowed_to_view_tests_not_using_any_item_bank
                        },
                    )
                    return paginator.get_paginated_response(
                        serialized.data,
                        test_definition_data.count(),
                        current_page,
                        page_size,
                    )
                # has_pages is False
                else:
                    serialized = TestDefinitionDataSerializer(
                        test_definition_data,
                        many=True,
                        context={
                            "allowed_to_view_tests_not_using_any_item_bank": allowed_to_view_tests_not_using_any_item_bank
                        },
                    )
                    return Response(
                        {
                            "next_page_number": None,
                            "previous_page_number": None,
                            "count": None,
                            "current_page_number": None,
                            "results": serialized.data,
                        }
                    )


# oec-cat/api/get-test-definition-versions-collected
class GetTestDefinitionsVersionsCollectedView(APIView):
    def get(self, request):
        return Response(
            TestDefinitionVersionsCollectedSerializer(
                TestDefinition.objects.filter(
                    is_public=False, active=True, archived=False
                )
                .order_by()
                .values("parent_code")
                .distinct(),
                many=True,
            ).data
        )
    
    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


# oec-cat/api/archive-test-definition-test-code
class ArchiveTestDefinitionTestCode(APIView):
    permission_classes = (
        permissions.IsAuthenticated
        & (HasTestBuilderPermission | HasTestDeveloperPermission),
    )

    def post(self, request):
        success, parameters = get_needed_parameters(
            # use the archive parameter to archive (true) or restore (false) the specified test definition test code
            ["parent_code", "test_code", "archive"],
            request,
        )
        if not success:
            return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

        # making sure that the archive parameter is well set
        if parameters["archive"] == "true" or parameters["archive"] == "false":
            # getting test code versions based on provided parameters
            test_code_versions = TestDefinition.objects.filter(
                parent_code=parameters["parent_code"], test_code=parameters["test_code"]
            )

            try:
                # looping in test_code_data
                for test_code in test_code_versions:
                    if parameters["archive"] == "true":
                        # deleting all test access codes related to this current test code
                        # getting test_access_codes
                        test_access_codes = TestAccessCode.objects.filter(
                            test_id=test_code.id
                        )
                        # looping in test_access_codes
                        for test_access_code in test_access_codes:
                            # deleting current test access code
                            test_access_code.delete()
                        # deleting all test permissions related to this current test code
                        # getting test_permissions
                        test_permissions = TestPermissions.objects.filter(
                            test_id=test_code.id
                        )
                        # looping in test_permissions
                        for test_permission in test_permissions:
                            # deleting current test permission
                            test_permission.delete()
                        # updating archived field to 1 (True)
                        test_code.archived = 1
                        test_code.active = 0
                        test_code.save()
                    else:
                        # updating archived field to 0 (False)
                        test_code.archived = 0
                        test_code.save()

                return Response({"message": "success"}, status=status.HTTP_200_OK)

            except:
                return Response(
                    {
                        "error": "Something went wrong during the archive test code process"
                    },
                    status=status.HTTP_400_BAD_REQUEST,
                )
        else:
            return Response(
                {
                    "error": "Please make sure to use 'true' or 'false' for the archive parameter"
                },
                status=status.HTTP_400_BAD_REQUEST,
            )
