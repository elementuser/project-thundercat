from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import permissions
from django.core.exceptions import ObjectDoesNotExist
from backend.static.assigned_test_status import AssignedTestStatus
from backend.custom_models.assigned_test import AssignedTest
from backend.views.utils import is_undefined
from cms.cms_models.test_definition import TestDefinition
from cms.cms_models.file_resource_details import FileResourceDetails

# This is the new api for getting test sections related to the factory


class GetFile(APIView):
    def get(self, request):
        document = request.query_params.get("fileName", None)
        test_definition = self.request.query_params.get("test_definition", None)
        test_definition = TestDefinition.objects.get(id=test_definition)

        response = Response()
        response["Content-Disposition"] = "attachment; filename={0}".format(document)

        response["X-Accel-Redirect"] = "/protected/{}/{}".format(
            test_definition.test_code, document
        )
        return response

    # assign the permissions depending on if the test is public or not

    def get_permissions(self):
        try:
            user = ""
            resource = self.request.query_params.get("fileName", None)
            test_definition = self.request.query_params.get("test_definition", None)
            if is_undefined(test_definition):
                return [RejectPermission()]
            assigned_test = self.request.query_params.get("assigned_test", None)

            if user_has_permissions(user, resource, test_definition, assigned_test):
                return [permissions.IsAuthenticatedOrReadOnly()]
            # is_public = false
            # else:
            #     return [permissions.IsAuthenticated()]
        # is_test_public does not exist
        except ObjectDoesNotExist:
            return [permissions.IsAdminUser()]


class RejectPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        return False


def user_has_permissions(user, resource, test_definition, assigned_test):
    try:
        test_definition = TestDefinition.objects.get(id=test_definition)
        resource = FileResourceDetails.objects.get(
            path=resource, test_definition=test_definition
        )
        if is_public_resource(resource):
            return True

        # check if user has an assigned test related to this resource
        # check uit assigned test -> change to all assigned test
        assigned_test = AssignedTest.objects.get(id=assigned_test)
        if is_undefined(assigned_test):
            return False

        # make sure the assigned test matches the file resource definition
        if assigned_test.test != resource.test_definition:
            return False

        # ensure the test is active
        if (
            assigned_test.status != AssignedTestStatus.ACTIVE
            or assigned_test.status != AssignedTestStatus.PRE_TEST
        ):
            return False

        return True
    except:
        return False


def is_public_resource(resource):
    return resource.test_definition.is_public
    # check if when the resource was created
