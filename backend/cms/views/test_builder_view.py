from django.core.exceptions import ObjectDoesNotExist
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from rest_framework import permissions
from rest_framework.renderers import JSONRenderer
from cms.cms_models.test_section_component import TestSectionComponent
from cms.cms_models.test_section import TestSection
from cms.cms_models.new_question import NewQuestion
from cms.cms_models.answer import Answer
from cms.views.utils import SCORING_METHOD_TYPE
from cms.views.try_test_section import try_test_section, Action
from cms.views.extract_test import extract_test
from cms.views.utils import get_needed_parameters
from cms.serializers.scoring_methods_serializer import ScoringMethodTypesSerializer
from cms.serializers.test_definition_serializer import TestDefinitionDataSerializer
from cms.cms_models.test_definition import TestDefinition
from cms.cms_models.scoring_method_types import ScoringMethodTypes
from cms.cms_models.scoring_methods import ScoringMethods
from cms.cms_models.scoring_threshold import ScoringThreshold
from backend.api_permissions.role_based_api_permissions import (
    HasTestBuilderPermission,
    HasTestDeveloperPermission,
    HasTestAdminPermission,
)

# This is for uploading an entirely new test
# currently it does not handle versioning


# URL: upload-test
class TestBuilderUploaderView(APIView):
    permission_classes = (
        permissions.IsAuthenticated
        & (HasTestBuilderPermission | HasTestDeveloperPermission),
    )

    def post(self, request):
        success, parameters = get_needed_parameters(["new_test"], request)

        if not success:
            return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

        return try_test_section(0, parameters["new_test"], "en", Action.UPLOAD)


# URL: try-test-section
class TryTestSectionView(APIView):
    permission_classes = (
        permissions.IsAuthenticated
        & (HasTestBuilderPermission | HasTestDeveloperPermission),
    )

    def post(self, request):
        success, parameters = get_needed_parameters(
            ["new_test", "order", "language"], request
        )

        if not success:
            return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

        # getting next version number
        test_definition_data = parameters["new_test"]

        # getting last version
        last_version_data = (
            TestDefinition.objects.filter(
                parent_code=test_definition_data["test_definition"][0]["parent_code"],
                test_code=test_definition_data["test_definition"][0]["test_code"],
            )
            .order_by("version")
            .last()
        )

        # if there is at least one existing version
        if last_version_data:
            # adding 1 to last version
            next_version_number = int(last_version_data.version) + 1
        else:
            next_version_number = 1

        # saving new version to test_definition_data object
        test_definition_data["test_definition"][0]["version"] = next_version_number

        return try_test_section(
            parameters["order"],
            test_definition_data,
            parameters["language"],
            Action.TRY,
        )


# URL: get-test-extract
class ExtractTestView(APIView):
    permission_classes = (
        permissions.IsAuthenticated
        & (HasTestBuilderPermission | HasTestDeveloperPermission),
    )

    renderer_classes = [JSONRenderer]

    def get(self, request):
        success, parameters = get_needed_parameters(
            ["test_definition", "current_language"], request
        )

        if not success:
            return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

        return extract_test(
            parameters["test_definition"], parameters["current_language"]
        )


# URL: oec-cat/api/test-definition
class TestDefinitionView(APIView):
    permission_classes = (
        permissions.IsAuthenticated
        & (HasTestBuilderPermission | HasTestDeveloperPermission),
    )

    def post(self, request):
        success, parameters = get_needed_parameters(
            ["test_definition", "active"], request
        )

        if not success:
            return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

        test = TestDefinition.objects.filter(id=parameters["test_definition"]).first()

        if not test:
            return Response(
                {"message": "could not find test definition"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        test.active = parameters["active"]
        test.save()

        return Response({"message": "success"}, status=status.HTTP_200_OK)


class TestBuilderGetScoringMethodTypes(APIView):
    permission_classes = (
        permissions.IsAuthenticated
        & (HasTestBuilderPermission | HasTestDeveloperPermission),
    )

    def get(self, request):
        success, parameters = get_needed_parameters(["current_language"], request)

        if not success:
            return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

        # determine the ordering based on provided language
        order_by_value = ""
        if parameters["current_language"] == "en":
            order_by_value = "en_name"
        else:
            order_by_value = "fr_name"

        scoring_method_types = ScoringMethodTypes.objects.all().order_by(order_by_value)
        serialized = ScoringMethodTypesSerializer(scoring_method_types, many=True).data
        return Response(serialized, status=status.HTTP_200_OK)


class GetSpecifiedTestScoringMethodData(APIView):
    def get(self, request):
        success, parameters = get_needed_parameters(["test_id"], request)

        if not success:
            return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

        # getting scoring method type
        scoring_method_data = ScoringMethods.objects.get(test_id=parameters["test_id"])

        # getting scoring method type
        scoring_method_type_data = ScoringMethodTypes.objects.get(
            id=scoring_method_data.method_type_id
        )

        # initializing needed variables for the final returned data
        scoring_method_type = None
        final_data = []

        # THRESHOLD
        if (
            scoring_method_type_data.method_type_codename
            == SCORING_METHOD_TYPE.THRESHOLD
        ):
            # initializing threshold_data_array
            threshold_data_array = []
            # setting scoring_method_type
            scoring_method_type = SCORING_METHOD_TYPE.THRESHOLD
            # getting threshold data
            threshold_data = ScoringThreshold.objects.filter(
                scoring_method_id=scoring_method_data.id
            )
            # looping in threshold_data
            for threshold in threshold_data:
                # populating threshold_data_array
                threshold_data_array.append(
                    {
                        "id": threshold.id,
                        "en_conversion_value": threshold.en_conversion_value,
                        "fr_conversion_value": threshold.fr_conversion_value,
                    }
                )
            # setting final_data
            final_data = threshold_data_array

        # PASS_FAIL
        elif (
            scoring_method_type_data.method_type_codename
            == SCORING_METHOD_TYPE.PASS_FAIL
        ):
            # setting scoring_method_type
            scoring_method_type = SCORING_METHOD_TYPE.PASS_FAIL

        # RAW
        elif scoring_method_type_data.method_type_codename == SCORING_METHOD_TYPE.RAW:
            # setting scoring_method_type
            scoring_method_type = SCORING_METHOD_TYPE.RAW
            # getting maximum score possible (might need some improvement in the future for more complex tests)
            # initializing maximum_possible_score
            maximum_possible_score = 0
            # getting related timed test_sections
            timed_test_sections = TestSection.objects.filter(
                test_definition_id=parameters["test_id"], default_time__isnull=False
            )
            # looping in timed_test_sections
            for timed_test_section in timed_test_sections:
                # getting related test section component
                test_section_components = TestSectionComponent.objects.filter(
                    test_section=timed_test_section.id
                )
                # looping in test_section_components
                for test_section_component in test_section_components:
                    # getting questions related to current test section component
                    questions = NewQuestion.objects.filter(
                        test_section_component_id=test_section_component.id
                    )
                    # looping in questions
                    for question in questions:
                        # getting answers
                        answers = Answer.objects.filter(question_id=question.id)
                        # looping in answers
                        for answer in answers:
                            # initializing temp_max_value
                            temp_max_value = 0
                            # getting maximum scoring for the current question
                            if answer.scoring_value > temp_max_value:
                                temp_max_value = answer.scoring_value
                        maximum_possible_score += temp_max_value

            # setting final_data
            final_data = {"maximum_possible_score": maximum_possible_score}

        # PERCENTAGE
        elif (
            scoring_method_type_data.method_type_codename
            == SCORING_METHOD_TYPE.PERCENTAGE
        ):
            # setting scoring_method_type
            scoring_method_type = SCORING_METHOD_TYPE.PERCENTAGE
            # TODO: TO BE COMPLETED

        # NONE
        elif scoring_method_type_data.method_type_codename == SCORING_METHOD_TYPE.NONE:
            # setting scoring_method_type
            scoring_method_type = SCORING_METHOD_TYPE.NONE

        final_obj = {"scoring_method_type": scoring_method_type, "data": final_data}

        return Response(final_obj)

    def get_permissions(self):
        try:
            return [permissions.IsAuthenticated(), HasTestAdminPermission()]

        except ObjectDoesNotExist:
            return [permissions.IsAdminUser()]


class GetTestDefinitionLatestVersion(APIView):
    permission_classes = (
        permissions.IsAuthenticated
        & (HasTestBuilderPermission | HasTestDeveloperPermission),
    )

    def get(self, request):
        success, parameters = get_needed_parameters(
            ["parent_code", "test_code"], request
        )

        if not success:
            return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

        test_definition_data = (
            TestDefinition.objects.filter(
                parent_code=parameters["parent_code"], test_code=parameters["test_code"]
            )
            .order_by("version")
            .last()
        )

        # if there is at least one existing version
        if test_definition_data:
            # return serialized data
            serialized = TestDefinitionDataSerializer(
                test_definition_data, many=False
            ).data
            return Response(serialized, status=status.HTTP_200_OK)
        else:
            return Response(
                {
                    "error": "There is no test version matching with the provided parameters"
                },
                status=status.HTTP_400_BAD_REQUEST,
            )
