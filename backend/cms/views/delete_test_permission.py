from operator import itemgetter
from rest_framework.response import Response
from rest_framework import status
from cms.cms_models.test_permissions_model import TestPermissions
from cms.views.utils import get_needed_parameters
from backend.custom_models.test_access_code_model import TestAccessCode

# deleting test permission
def delete_test_permission(request):
    success, parameters = get_needed_parameters(
        ["test_permission_id", "reason_for_deletion"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    test_permission_id, reason_for_deletion = itemgetter(
        "test_permission_id", "reason_for_deletion"
    )(parameters)

    try:
        # getting specified test permission
        test_permission = TestPermissions.objects.get(id=test_permission_id)

        # deleting active test accesse codes related to the current TA + current test order number
        test_access_codes = TestAccessCode.objects.filter(
            ta_username_id=test_permission.username_id,
            test_order_number=test_permission.test_order_number,
        )
        for test_access_code in test_access_codes:
            TestAccessCode.objects.get(id=test_access_code.id).delete()

        # updating reason_for_modif_or_del field
        test_permission.reason_for_modif_or_del = reason_for_deletion
        test_permission.save()

        # deleting current test permission
        test_permission.delete()
        return Response(status=status.HTTP_200_OK)
    except TestPermissions.DoesNotExist:
        return Response(
            {"error": "the specified test permission does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )
