from django.core.exceptions import ObjectDoesNotExist
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from rest_framework import permissions
from backend.views.utils import is_undefined
from backend.static.languages import Languages
from backend.api_permissions.assigned_test_permissions import (
    HasAssignedTestFromAssignedTestPermission,
)
from cms.views.retrieve_test_section_data import (
    retrieve_test_section_data,
    retrieve_public_test_section_data,
    is_test_public,
    getting_updated_time_after_lock_pause,
    validate_timeout_state,
)
from cms.cms_models.test_definition import TestDefinition
from cms.views.retrieve_test_section_data_by_id import (
    retrieve_test_section_data_by_id,
    retrieve_public_test_section_data_by_id,
)
from cms.serializers.test_section_component_serializer import get_question_list
from cms.views.utils import get_needed_parameters


class SpecialSection:
    FINISH = "finish"
    QUIT = "quit"


# This is the new api for getting test sections related to the factory


class TestSectionData(APIView):
    def get(self, request):
        order = request.query_params.get("order", None)
        special_section = request.query_params.get("special_section", None)
        assigned_test_id = request.query_params.get("assigned_test_id", None)
        language = request.query_params.get("language", Languages.EN)
        test_id = request.query_params.get("test_id", None)
        timed_out = request.query_params.get("timed_out", None)
        # always need a test definition param

        test_definition = TestDefinition.objects.get(id=test_id)
        public_test = test_definition.is_public

        # if the test is public, don't record any data
        if public_test and is_undefined(assigned_test_id):
            return retrieve_public_test_section_data(
                order, test_definition, special_section, language
            )

        if is_undefined(assigned_test_id):
            return Response(
                {"error": "no 'assigned_test_id' parameter"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        if is_undefined(order):
            return Response(
                {"error": "no 'order' parameter"}, status=status.HTTP_400_BAD_REQUEST
            )

        # check that parameters are valid
        if is_undefined(special_section):
            return retrieve_test_section_data(
                order, assigned_test_id, language, False, timed_out
            )

        elif special_section == SpecialSection.QUIT:
            return retrieve_test_section_data(
                order, assigned_test_id, language, True, False
            )

        else:
            return Response(
                {"error": "bad parameters"}, status=status.HTTP_400_BAD_REQUEST
            )

    # assign the permissions depending on if the test is public or not

    def get_permissions(self):
        try:
            # is_public = true
            if is_test_public(self.request.query_params.get("test_id", None)):
                return [permissions.AllowAny()]
            # is_public = false
            else:
                return [
                    permissions.IsAuthenticated(),
                    HasAssignedTestFromAssignedTestPermission(),
                ]
        # is_test_public does not exist
        except ObjectDoesNotExist:
            return [permissions.IsAdminUser()]


class TestSectionDataById(APIView):
    def get(self, request):
        assigned_test_id = request.query_params.get("assigned_test_id", None)
        # public test
        success, parameters = get_needed_parameters(
            ["section_id", "language", "test_id"], request
        )
        if not success:
            return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

        test_definition = TestDefinition.objects.get(id=parameters["test_id"])
        public_test = test_definition.is_public

        # if the test is public, don't record any data
        if public_test and is_undefined(assigned_test_id):
            return retrieve_public_test_section_data_by_id(
                parameters["section_id"], test_definition, parameters["language"]
            )

        # assigned test now
        success, parameters = get_needed_parameters(
            ["section_id", "language", "test_id", "assigned_test_id"], request
        )
        if not success:
            return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

        return retrieve_test_section_data_by_id(
            assigned_test_id, parameters["section_id"], parameters["language"]
        )

    # assign the permissions depending on if the test is public or not

    def get_permissions(self):
        try:
            # is_public = true
            if is_test_public(self.request.query_params.get("test_id", None)):
                return [permissions.AllowAny()]
            # is_public = false
            else:
                return [
                    permissions.IsAuthenticated(),
                    HasAssignedTestFromAssignedTestPermission(),
                ]
        # is_test_public does not exist
        except ObjectDoesNotExist:
            return [permissions.IsAdminUser()]


# URL: api/get-test-section


class GetUpdatedTimeAfterLockPause(APIView):
    def get(self, request):
        return getting_updated_time_after_lock_pause(request)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasAssignedTestFromAssignedTestPermission(),
        ]


class GetQuestionsList(APIView):
    def post(self, request):
        # test_definition = request.query_params.get("test_definition", None)
        success, parameters = get_needed_parameters(
            [
                "local_testing",
                "assigned_test_id",
                "test_section_component_id",
                "shuffle_all",
                "shuffle_question_blocks",
                "test_definition",
            ],
            request,
        )
        if not success:
            return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

        return get_question_list(
            parameters["local_testing"],
            parameters["assigned_test_id"],
            parameters["test_section_component_id"],
            parameters["shuffle_all"],
            parameters["shuffle_question_blocks"],
            parameters["test_definition"],
        )

    def get_permissions(self):
        return [permissions.AllowAny()]


class ValidateTimeoutState(APIView):
    def get(self, request):
        return validate_timeout_state(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]
