from django.core.exceptions import ObjectDoesNotExist
from rest_framework.views import APIView
from rest_framework import permissions
from backend.api_permissions.role_based_api_permissions import (
    HasTestDeveloperPermission,
    HasRdOperationsPermission,
    HasTestBuilderPermission,
)

# from cms.views.utils import get_needed_parameters
from cms.views.retrieve_item_bank_data import (
    get_all_item_banks,
    get_found_item_banks,
    get_item_banks_for_test_builder,
    get_item_bank_bundles_for_test_builder,
    get_selected_item_bank_data_as_rd_operations,
    get_selected_item_bank_data,
    create_new_item_bank,
    update_item_bank_data_as_rd_operations,
    add_test_developer,
    delete_test_developer,
    get_item_bank_access_types,
    update_item_bank_access_type,
    update_item_bank_data,
    get_all_items,
    get_found_items,
    get_item_data,
    get_selected_item_version_data,
    get_response_formats,
    get_development_statuses,
    create_new_items,
    update_item_data,
    create_item_comment,
    delete_item_comment,
    delete_item_draft,
    get_all_bundles,
    get_found_bundles,
    get_specific_bundle,
    create_new_bundle,
    upload_bundle,
    get_items_ready_to_be_added_to_bundle,
    get_found_items_ready_to_be_added_to_bundle,
    get_bundles_ready_to_be_added_to_bundle,
    get_found_bundles_ready_to_be_added_to_bundle,
    preview_items_list_from_bundle,
    run_get_items_list_from_bundle,
)


class GetAllItemBanks(APIView):
    permission_classes = (
        permissions.IsAuthenticated
        & (HasTestDeveloperPermission | HasRdOperationsPermission),
    )

    def get(self, request):
        return get_all_item_banks(request)


class GetFoundItemBanks(APIView):
    permission_classes = (
        permissions.IsAuthenticated
        & (HasTestDeveloperPermission | HasRdOperationsPermission),
    )

    def get(self, request):
        return get_found_item_banks(request)


class GetItemBanksForTestBuilder(APIView):
    permission_classes = (
        permissions.IsAuthenticated
        & (
            HasTestDeveloperPermission
            | HasTestBuilderPermission
            | HasRdOperationsPermission
        ),
    )

    def get(self, request):
        return get_item_banks_for_test_builder(request)


class GetItemBankBundlesForTestBuilder(APIView):
    permission_classes = (
        permissions.IsAuthenticated
        & (
            HasTestDeveloperPermission
            | HasTestBuilderPermission
            | HasRdOperationsPermission
        ),
    )

    def get(self, request):
        return get_item_bank_bundles_for_test_builder(request)


class GetSelectedItemBankDataAsRdOperations(APIView):
    def get(self, request):
        return get_selected_item_bank_data_as_rd_operations(request)

    def get_permissions(self):
        try:
            return [permissions.IsAuthenticated(), HasRdOperationsPermission()]

        except ObjectDoesNotExist:
            return [permissions.IsAdminUser()]


class GetSelectedItemBankData(APIView):
    def get(self, request):
        return get_selected_item_bank_data(request)

    def get_permissions(self):
        try:
            return [permissions.IsAuthenticated(), HasTestDeveloperPermission()]

        except ObjectDoesNotExist:
            return [permissions.IsAdminUser()]


class CreateNewItemBank(APIView):
    def post(self, request):
        return create_new_item_bank(request)

    def get_permissions(self):
        try:
            return [permissions.IsAuthenticated(), HasRdOperationsPermission()]

        except ObjectDoesNotExist:
            return [permissions.IsAdminUser()]


class UpdateItemBankDataAsRdOperations(APIView):
    def post(self, request):
        return update_item_bank_data_as_rd_operations(request)

    def get_permissions(self):
        try:
            return [permissions.IsAuthenticated(), HasRdOperationsPermission()]

        except ObjectDoesNotExist:
            return [permissions.IsAdminUser()]


class AddTestDeveloper(APIView):
    def post(self, request):
        return add_test_developer(request)

    def get_permissions(self):
        try:
            return [permissions.IsAuthenticated(), HasRdOperationsPermission()]

        except ObjectDoesNotExist:
            return [permissions.IsAdminUser()]


class DeleteTestDeveloper(APIView):
    def post(self, request):
        return delete_test_developer(request)

    def get_permissions(self):
        try:
            return [permissions.IsAuthenticated(), HasRdOperationsPermission()]

        except ObjectDoesNotExist:
            return [permissions.IsAdminUser()]


class GetItemBankAccessTypes(APIView):
    def get(self, request):
        return get_item_bank_access_types(request)

    def get_permissions(self):
        try:
            return [permissions.IsAuthenticated(), HasRdOperationsPermission()]

        except ObjectDoesNotExist:
            return [permissions.IsAdminUser()]


class UpdateItemBankAccessType(APIView):
    def post(self, request):
        return update_item_bank_access_type(request)

    def get_permissions(self):
        try:
            return [permissions.IsAuthenticated(), HasRdOperationsPermission()]

        except ObjectDoesNotExist:
            return [permissions.IsAdminUser()]


class UpdateItemBankData(APIView):
    def post(self, request):
        return update_item_bank_data(request)

    def get_permissions(self):
        try:
            return [permissions.IsAuthenticated(), HasTestDeveloperPermission()]

        except ObjectDoesNotExist:
            return [permissions.IsAdminUser()]


class GetAllItems(APIView):
    def get(self, request):
        return get_all_items(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestDeveloperPermission()]


class GetFoundItems(APIView):
    def get(self, request):
        return get_found_items(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestDeveloperPermission()]


class GetItemData(APIView):
    def get(self, request):
        return get_item_data(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestDeveloperPermission()]


class GetSelectedItemVersionData(APIView):
    def get(self, request):
        return get_selected_item_version_data(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestDeveloperPermission()]


class GetResponseFormats(APIView):
    def get(self, request):
        return get_response_formats(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestDeveloperPermission()]


class GetDevelopmentStatuses(APIView):
    def get(self, request):
        return get_development_statuses(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestDeveloperPermission()]


class CreateNewItem(APIView):
    def post(self, request):
        return create_new_items(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestDeveloperPermission()]


class UpdateItemData(APIView):
    def post(self, request):
        return update_item_data(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestDeveloperPermission()]


class DeleteItemDraft(APIView):
    def post(self, request):
        return delete_item_draft(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestDeveloperPermission()]


class CreateItemComment(APIView):
    def post(self, request):
        return create_item_comment(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestDeveloperPermission()]


class DeleteItemComment(APIView):
    def post(self, request):
        return delete_item_comment(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestDeveloperPermission()]


class GetAllBundles(APIView):
    def get(self, request):
        return get_all_bundles(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestDeveloperPermission()]


class GetFoundBundles(APIView):
    def get(self, request):
        return get_found_bundles(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestDeveloperPermission()]


class GetSpecificBundle(APIView):
    def get(self, request):
        return get_specific_bundle(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestDeveloperPermission()]


class CreateNewBundle(APIView):
    def post(self, request):
        return create_new_bundle(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestDeveloperPermission()]


class UploadBundle(APIView):
    def post(self, request):
        return upload_bundle(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestDeveloperPermission()]


class GetItemsReadyToBeAddedToBundle(APIView):
    def get(self, request):
        return get_items_ready_to_be_added_to_bundle(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestDeveloperPermission()]


class GetFoundItemsReadyToBeAddedToBundle(APIView):
    def get(self, request):
        return get_found_items_ready_to_be_added_to_bundle(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestDeveloperPermission()]


class GetBundlesReadyToBeAddedToBundle(APIView):
    def post(self, request):
        return get_bundles_ready_to_be_added_to_bundle(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestDeveloperPermission()]


class GetFoundBundlesReadyToBeAddedToBundle(APIView):
    def post(self, request):
        return get_found_bundles_ready_to_be_added_to_bundle(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestDeveloperPermission()]


class PreviewItemsListFromBundle(APIView):
    def post(self, request):
        return preview_items_list_from_bundle(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestDeveloperPermission()]


class GetItemsListFromBundle(APIView):
    def post(self, request):
        return run_get_items_list_from_bundle(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]
