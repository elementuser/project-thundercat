from datetime import timedelta, datetime
from operator import itemgetter
from django.utils import timezone
from rest_framework import status
from rest_framework.response import Response
from cms.static.test_section_component_type import TestSectionComponentType
from cms.views.test_break_bank import BreakBankActionsConstants
from cms.views.utils import get_needed_parameters
from cms.cms_models.test_section import TestSection
from cms.cms_models.test_definition import TestDefinition
from cms.serializers.test_section_serializer import TestSectionSerializer
from cms.static.test_section_type import TestSectionType
from backend.custom_models.assigned_test_section import AssignedTestSection
from backend.custom_models.assigned_test import AssignedTest
from backend.custom_models.assigned_test_section_access_times import (
    AssignedTestSectionAccessTimes,
)
from backend.custom_models.ta_actions import TaActions
from backend.custom_models.lock_test_actions import LockTestActions
from backend.views.utils import (
    is_undefined,
    handleViewedQuestionsLogic,
    get_new_break_bank_remaining_time,
    get_last_accessed_test_section,
)
from backend.views.test_administrator_actions import TaActionsConstants
from backend.custom_models.candidate_answers import CandidateAnswers

from backend.static.assigned_test_section_access_time_type import (
    AssignedTestSectionAccessTimeType,
)
from backend.static.assigned_test_status import AssignedTestStatus
from backend.views.uit_score_test import uit_scoring, Action
from backend.custom_models.accommodation_request import AccommodationRequest
from backend.custom_models.break_bank import BreakBank
from backend.custom_models.break_bank_actions import BreakBankActions


class SpecialSection:
    QUIT = "quit"


def is_test_public(test_id):
    # function that returns true if the test is public and false on the other hand
    return TestDefinition.objects.get(id=test_id).is_public


def retrieve_test_section_data(
    order, assigned_test_id, language, quit_section=False, timed_out=False
):
    context = {}
    context["language"] = language
    context["assigned_test_id"] = assigned_test_id
    # get the assigned test
    assigned_test = AssignedTest.objects.get(id=assigned_test_id)
    parameters = {"assigned_test_id": assigned_test.id}

    # check if the test is still active or assigned
    if assigned_test.status not in (
        AssignedTestStatus.PRE_TEST,
        AssignedTestStatus.ACTIVE,
        AssignedTestStatus.READY,
        AssignedTestStatus.LOCKED,
        AssignedTestStatus.PAUSED,
    ):
        return Response(
            {"error": "assigned test is in {} state".format(assigned_test.status)},
            status=status.HTTP_400_BAD_REQUEST,
        )

    # updating assigned test start date (only if null)
    if assigned_test.start_date is None:
        assigned_test.start_date = timezone.now()
        assigned_test.save()

    # initializing new_order
    new_order = None
    # getting assigned test secions
    assigned_test_section_object = AssignedTestSection.objects.filter(
        assigned_test_id=assigned_test.id
    )
    # populating assigned_test_sections_array
    assigned_test_sections_array = []
    for assigned_test_secion in assigned_test_section_object:
        assigned_test_sections_array.append(assigned_test_secion.id)
    # getting last assigned_test_setion_access_time (most recent one)
    assigned_test_setion_access_time = (
        AssignedTestSectionAccessTimes.objects.filter(
            assigned_test_section_id__in=assigned_test_sections_array,
            time_type=AssignedTestSectionAccessTimeType.START,
        )
        .order_by("time")
        .last()
    )
    if assigned_test_setion_access_time:
        # getting most recent assigned test section
        most_recent_assigned_test_section = AssignedTestSection.objects.get(
            id=assigned_test_setion_access_time.assigned_test_section_id
        )
        # getting order based on found mose recent assigned test section
        test_section_data = TestSection.objects.get(
            id=most_recent_assigned_test_section.test_section_id
        )
        new_order = test_section_data.order
        # if new_order greater than initial peovided order
        if new_order > int(order):
            # overriding order
            order = new_order - 1

    ###
    # finish current test section
    ###
    if int(order) != 0:
        # get the current test section
        test_section = TestSection.objects.get(
            test_definition_id=assigned_test.test_id, order=order
        )
        # get the assigned test section for the above
        assigned_test_section = AssignedTestSection.objects.get(
            assigned_test_id=assigned_test.id, test_section=test_section
        )
        # if timed_out flag is set to True and test_section_time is defined
        if timed_out == "true" and assigned_test_section.test_section_time is not None:
            # update assigned test section timed_out flag
            assigned_test_section.timed_out = True
            assigned_test_section.save()
        # create a "finish" time stamp for the current test section
        access_time = AssignedTestSectionAccessTimes(
            assigned_test_section=assigned_test_section,
            time_type=AssignedTestSectionAccessTimeType.FINISH,
        )
        access_time.save()

        # score the current test section (adds together in final scoring)
        parameters["test_section_id"] = test_section.id
        uit_scoring(parameters, Action.TEST_SECTION)
    # change the test status to pre-test (first time accessing) + removing previous_state (if it has one)
    else:
        # if assigned test status is not LOCKED or PAUSED
        if (
            assigned_test.status is not AssignedTestStatus.LOCKED
            and assigned_test.status is not AssignedTestStatus.PAUSED
        ):
            assigned_test.status = AssignedTestStatus.PRE_TEST
            assigned_test.previous_status = None
            assigned_test.save()

    ###
    # start new test section now
    ###
    next_test_section_order = ""

    if quit_section:
        # get the quit section of the test
        test_section = TestSection.objects.get(
            test_definition_id=assigned_test.test_id, section_type=TestSectionType.QUIT
        )

        next_test_section_order = str(test_section.order)

        try:
            # Adding an update to candidate answer historical data for question time spent report purposes
            last_candiate_answers_historical_data = (
                CandidateAnswers.history.filter(assigned_test_id=assigned_test.id)
                .order_by("history_date")
                .last()
            )

            # initializing the question_source to QUESTION_LIST
            question_source = TestSectionComponentType.QUESTION_LIST
            # question_id is NULL, that means the question source is from the ITEM BANK
            if last_candiate_answers_historical_data.question_id is None:
                # updating question_source to ITEM BANK
                question_source = TestSectionComponentType.ITEM_BANK

            # questions coming from test builder
            if question_source == TestSectionComponentType.QUESTION_LIST:
                candidate_answer = CandidateAnswers.objects.get(
                    assigned_test_id=assigned_test.id,
                    question_id=last_candiate_answers_historical_data.question_id,
                )
                candidate_answer.modify_date = timezone.now()
                candidate_answer.save()
            # questions coming from item bank
            else:
                candidate_answer = CandidateAnswers.objects.get(
                    assigned_test_id=assigned_test.id,
                    item_id=last_candiate_answers_historical_data.item_id,
                )
                candidate_answer.modify_date = timezone.now()
                candidate_answer.save()

            candidate_answer.modify_date = timezone.now()
            candidate_answer.save()
        except:
            pass

        # check if the candidate is on PAUSE
        if assigned_test.status == AssignedTestStatus.PAUSED:
            # getting new break bank remaining time
            new_remaining_time = get_new_break_bank_remaining_time(
                assigned_test.accommodation_request_id
            )
            # creating new UNPAUSE action in Break Bank Actions table
            BreakBankActions.objects.create(
                action_type=BreakBankActionsConstants.UNPAUSE,
                new_remaining_time=new_remaining_time,
                break_bank_id=AccommodationRequest.objects.get(
                    id=assigned_test.accommodation_request_id
                ).break_bank_id,
                test_section_id=test_section_data.id,
            )
        # set the test to quit + update test section id + update submit date
        assigned_test.status = AssignedTestStatus.QUIT
        assigned_test.submit_date = timezone.now()
        assigned_test.test_section_id = test_section.id
        assigned_test.save()
        # handling viewed questions logic
        handleViewedQuestionsLogic(assigned_test.id)
    else:
        next_section_invalid = True
        # get a valid test section order number
        while next_section_invalid:
            next_test_section_order = str(int(order) + 1)
            # find the new test section
            test_section = TestSection.objects.get(
                test_definition_id=assigned_test.test_id, order=next_test_section_order
            )

            assigned_test_section = AssignedTestSection.objects.get(
                assigned_test_id=assigned_test.id, test_section=test_section
            )

            # update test section id
            assigned_test.test_section_id = test_section.id
            assigned_test.save()

            # if the next section is not timed, we can return this section
            if is_undefined(test_section.default_time):
                break

            # if the next section is timed and current status is PRE_TEST
            if (
                (
                    AssignedTestSection.objects.get(
                        assigned_test_id=assigned_test.id,
                        test_section_id=test_section.id,
                    ).test_section_time
                    is not None
                )
                or (TestSection.object.get(id=test_section.id).default_time is not None)
            ) and assigned_test.status == AssignedTestStatus.PRE_TEST:
                # update assigned test status to ACTIVE
                assigned_test.status = AssignedTestStatus.ACTIVE
                assigned_test.save()

            # if the next test section is timed and already completed, get another one
            if (
                AssignedTestSectionAccessTimes.objects.filter(
                    assigned_test_section=assigned_test_section,
                    time_type=AssignedTestSectionAccessTimeType.FINISH,
                ).count()
                == 0
            ):
                break
            else:
                order = int(order) + 1

    # Check if the new test section is special (a finish section) and change test status
    if test_section.section_type == TestSectionType.FINISH:
        # Adding an update to candidate answer historical data for question time spent report purposes
        last_candiate_answers_historical_data = (
            CandidateAnswers.history.filter(assigned_test_id=assigned_test.id)
            .order_by("history_date")
            .last()
        )

        # initializing the question_source to QUESTION_LIST
        question_source = TestSectionComponentType.QUESTION_LIST
        # question_id is NULL, that means the question source is from the ITEM BANK
        if last_candiate_answers_historical_data.question_id is None:
            # updating question_source to ITEM BANK
            question_source = TestSectionComponentType.ITEM_BANK

        # questions coming from test builder
        if question_source == TestSectionComponentType.QUESTION_LIST:
            candidate_answer = CandidateAnswers.objects.get(
                assigned_test_id=assigned_test.id,
                question_id=last_candiate_answers_historical_data.question_id,
            )
            candidate_answer.modify_date = timezone.now()
            candidate_answer.save()
        # questions coming from item bank
        else:
            candidate_answer = CandidateAnswers.objects.get(
                assigned_test_id=assigned_test.id,
                item_id=last_candiate_answers_historical_data.item_id,
            )
            candidate_answer.modify_date = timezone.now()
            candidate_answer.save()

        # set the test to submitted
        assigned_test.status = AssignedTestStatus.SUBMITTED
        assigned_test.submit_date = timezone.now()
        assigned_test.save()
        # we need to score the whole test now
        uit_scoring(parameters, Action.TEST)

    # set the test section time to the new amount of time assigned by an admin
    test_section_set_time = assigned_test_section.test_section_time

    # check for oldest existing start times on this section
    # so we can set the new section start time to this
    access_time = (
        AssignedTestSectionAccessTimes.objects.filter(
            assigned_test_section=assigned_test_section,
            time_type=AssignedTestSectionAccessTimeType.START,
        )
        .order_by("time")
        .first()
    )
    start_time = ""
    if not is_undefined(access_time):
        start_time = access_time.time

    # create a "start" time stamp for the new test section for records
    access_time = AssignedTestSectionAccessTimes(
        assigned_test_section=assigned_test_section,
        time_type=AssignedTestSectionAccessTimeType.START,
    )
    access_time.save()

    # fetch test section data
    test_section = TestSection.objects.get(
        test_definition_id=assigned_test.test_id, order=next_test_section_order
    )
    # fetch the next section time
    next_test_section = TestSection.objects.filter(
        test_definition_id=assigned_test.test_id,
        order=str(int(next_test_section_order) + 1),
    ).first()
    if next_test_section:
        next_section_time = next_test_section.default_time
        next_assigned_test_section = AssignedTestSection.objects.get(
            assigned_test_id=assigned_test.id, test_section=next_test_section
        )
        if next_assigned_test_section.test_section_time:
            next_section_time = next_assigned_test_section.test_section_time
        context["next_test_section_time"] = next_section_time

    # providing needed context
    context["assigned_test_id"] = assigned_test_id
    # if next test section exists
    if next_test_section:
        context["next_test_section_id"] = next_test_section.id

    # serialize the data for return
    serialized = TestSectionSerializer(test_section, many=False, context=context).data

    # set the time on the test section
    if not is_undefined(test_section_set_time):
        serialized["default_time"] = test_section_set_time
    serialized["start_time"] = start_time

    return Response(serialized, status=status.HTTP_200_OK)


def retrieve_public_test_section_data(
    order, test_definition, special_section, language
):
    context = {}
    context["language"] = language
    ###
    # start new test section now
    ###
    next_test_section_order = ""

    next_test_section_order = str(int(order) + 1)
    # find the new test section
    test_section = TestSection.objects.get(
        test_definition_id=test_definition.id, order=next_test_section_order
    )

    start_time = ""

    # fetch test section data
    test_section = TestSection.objects.get(
        test_definition_id=test_definition.id, order=next_test_section_order
    )
    # fetch the next section time
    next_test_section = TestSection.objects.filter(
        test_definition_id=test_definition.id,
        order=str(int(next_test_section_order) + 1),
    ).first()
    if next_test_section:
        next_test_section_time = next_test_section.default_time
        if not is_undefined(next_test_section_time):
            context["next_test_section_time"] = next_test_section_time

    serialized = TestSectionSerializer(test_section, many=False, context=context).data
    serialized["start_time"] = start_time

    return Response(serialized, status=status.HTTP_200_OK)


def getting_updated_time_after_lock_pause(request):
    success, parameters = get_needed_parameters(
        ["assigned_test_id", "test_section_id"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    assigned_test_id, test_section_id = itemgetter(
        "assigned_test_id", "test_section_id"
    )(parameters)

    # initializing needed variables
    assigned_test_section_access_time = None
    initial_time = None
    updated_time = None
    locked_time = 0
    consumed_break_bank_time = 0

    try:
        # getting test_section to make sure that this is a timed section
        test_section = TestSection.objects.get(id=test_section_id)
        # this is a timed section (default_time exists)
        if test_section.default_time is not None:
            # getting assigned test section access time (first one)
            assigned_test_section = AssignedTestSection.objects.get(
                assigned_test_id=assigned_test_id,
                test_section_id=test_section_id,
            )
            assigned_test_section_access_time = (
                AssignedTestSectionAccessTimes.objects.filter(
                    assigned_test_section_id=assigned_test_section.id
                ).first()
            )
        else:
            return Response(
                {
                    "info": "test section id {} is not a timed section".format(
                        test_section.id
                    )
                },
                status.HTTP_200_OK,
            )

        # assigned test section access time exists
        if assigned_test_section_access_time is not None:
            # updating initial_time with assigned_test_section_access_time value
            initial_time = assigned_test_section_access_time.time
        else:
            return Response(
                {
                    "error": "unable to find any assigned test section access time based on provided parameters"
                },
                status=status.HTTP_400_BAD_REQUEST,
            )

        # test section provided has not been started yet
        if initial_time is None:
            return Response(
                {
                    "error": "test section id {} has not been started yet".format(
                        test_section_id
                    )
                },
                status=status.HTTP_400_BAD_REQUEST,
            )

        # getting LOCKED ta actions based on provided assigned_test_id and test_section_id
        ta_actions = TaActions.objects.filter(
            assigned_test_id=assigned_test_id,
            test_section_id=test_section_id,
            action_type_id=TaActionsConstants.LOCK,
        )
        # there is at least one existing LOCKED ta action
        if ta_actions:
            # looping in filtered ta_actions
            for ta_action in ta_actions:
                # LOCK action
                if ta_action.action_type_id == TaActionsConstants.LOCK:
                    # getting lock test action object
                    lock_test_action = LockTestActions.objects.get(
                        ta_action_id=ta_action.id
                    )

                    # no laock test action end date (still on lock)
                    if lock_test_action.lock_end_date is None:
                        return Response(
                            {
                                "info": "there is not lock end date yet for this lock test action {}".format(
                                    lock_test_action.id
                                )
                            },
                            status=status.HTTP_200_OK,
                        )
                    else:
                        # calculating locked time in seconds (keeping only 3 digits after the decimal point)
                        # incrementing locked_pause_time value
                        locked_time += round(
                            (
                                lock_test_action.lock_end_date
                                - lock_test_action.lock_start_date
                            ).total_seconds(),
                            3,
                        )

        # getting assigned_test data
        assigned_test_data = AssignedTest.objects.get(id=assigned_test_id)

        # there is an accommodation request
        if assigned_test_data.accommodation_request_id is not None:
            # getting break bank ID
            break_bank_id = AccommodationRequest.objects.get(
                id=assigned_test_data.accommodation_request_id
            ).break_bank_id
            # existing break bank accommodation request
            if break_bank_id is not None:
                # getting total break bank time
                initial_time_to_consider = BreakBank.objects.get(
                    id=break_bank_id
                ).break_time
                # getting last accessed assigned test section ID
                last_accessed_test_section_id = get_last_accessed_test_section(
                    assigned_test_data.id
                )
                # getting unpause break bank actions related to current test section (if they exist)
                test_section_related_unpause_break_bank_actions = (
                    BreakBankActions.objects.filter(
                        break_bank_id=break_bank_id,
                        action_type=BreakBankActionsConstants.UNPAUSE,
                        test_section_id=last_accessed_test_section_id,
                    ).order_by("modify_date")
                )

                if test_section_related_unpause_break_bank_actions:
                    # getting last unpause break bank action
                    last_unpause_break_bank_action = (
                        test_section_related_unpause_break_bank_actions.last()
                    )
                    # getting unpause break actions excluding the ones for the current test section (useful for multi sections test)
                    excluding_current_test_section_unpause_break_bank_actions = (
                        BreakBankActions.objects.filter(
                            break_bank_id=break_bank_id,
                            action_type=BreakBankActionsConstants.UNPAUSE,
                        )
                        .order_by("modify_date")
                        .exclude(
                            test_section_id=last_accessed_test_section_id,
                        )
                    )
                    # excluding_current_test_section_unpause_break_bank_actions exists
                    if excluding_current_test_section_unpause_break_bank_actions:
                        # reinitializing initial_time_to_consider
                        initial_time_to_consider = (
                            excluding_current_test_section_unpause_break_bank_actions.last().new_remaining_time
                        )
                    # calculating consumed break bank time (initial time to consider - last unpause action new remaining time)
                    consumed_break_bank_time = (
                        initial_time_to_consider
                        - last_unpause_break_bank_action.new_remaining_time
                    )

        # calculatingthe total additional time (locked_time + consumed_break_bank_time)
        total_additional_time = locked_time + consumed_break_bank_time

        # getting updated time
        updated_time = initial_time + timedelta(seconds=total_additional_time)
        return Response(updated_time)

    except AssignedTestSection.DoesNotExist:
        return Response(
            {
                "error": "unable to find any assigned test section based on provided parameters"
            },
            status=status.HTTP_400_BAD_REQUEST,
        )

    except AssignedTestSectionAccessTimes.DoesNotExist:
        return Response(
            {
                "error": "unable to find any assigned test section access time based on provided parameters"
            },
            status=status.HTTP_400_BAD_REQUEST,
        )


# this function is useful for celery tasks
# before calling this function, make sure that the test is already started to avoid unwanted returns
def get_updated_time_after_lock_pause_function(assigned_test_id):
    # initializing time calculation variables
    locked_time = 0
    consumed_break_bank_time = 0
    initial_time = None
    # getting timed assigned test sections
    timed_assigned_test_sections = AssignedTestSection.objects.filter(
        assigned_test_id=assigned_test_id, test_section_time__isnull=False
    )
    if timed_assigned_test_sections:
        time = AssignedTestSectionAccessTimes.objects.filter(
            # first assigned test section
            assigned_test_section_id=timed_assigned_test_sections.first().id,
            time_type=AssignedTestSectionAccessTimeType.START,
        ).first()
        if time:
            # updating initial_time
            initial_time = time.time
    timed_assigned_test_section_test_section_ids = []
    for timed_assigned_test_section in timed_assigned_test_sections:
        timed_assigned_test_section_test_section_ids.append(
            timed_assigned_test_section.test_section_id
        )
    # getting LOCKED ta actions based on provided assigned_test_id and test_section_id
    ta_actions = TaActions.objects.filter(
        assigned_test_id=assigned_test_id,
        test_section_id__in=timed_assigned_test_section_test_section_ids,
        action_type_id=TaActionsConstants.LOCK,
    )
    # there is at least one existing LOCKED ta action
    if ta_actions:
        # looping in filtered ta_actions
        for ta_action in ta_actions:
            # getting test_section to make sure that this is a timed section
            test_section = TestSection.objects.get(id=ta_action.test_section_id)
            # this is a timed section (default_time exists)
            if test_section.default_time is not None:
                # getting assigned test section access time (first one)
                assigned_test_section = AssignedTestSection.objects.get(
                    assigned_test_id=assigned_test_id,
                    test_section_id=ta_action.test_section_id,
                )
                assigned_test_section_access_time = (
                    AssignedTestSectionAccessTimes.objects.filter(
                        assigned_test_section_id=assigned_test_section.id
                    ).first()
                )

                # assigned test section access time exists
                if assigned_test_section_access_time is not None:
                    # LOCK action
                    if ta_action.action_type_id == TaActionsConstants.LOCK:
                        # getting lock test action object
                        lock_test_action = LockTestActions.objects.get(
                            ta_action_id=ta_action.id
                        )

                        # no laock test action end date (still on lock)
                        if lock_test_action.lock_end_date is not None:
                            # calculating locked time in seconds (keeping only 3 digits after the decimal point)
                            # incrementing locked_time value
                            locked_time += round(
                                (
                                    lock_test_action.lock_end_date
                                    - lock_test_action.lock_start_date
                                ).total_seconds(),
                                3,
                            )

    # getting assigned_test data
    assigned_test_data = AssignedTest.objects.get(id=assigned_test_id)

    # there is an accommodation request
    if assigned_test_data.accommodation_request_id is not None:
        # getting break bank ID
        break_bank_id = AccommodationRequest.objects.get(
            id=assigned_test_data.accommodation_request_id
        ).break_bank_id
        # existing break bank accommodation request
        if break_bank_id is not None:
            # getting total break bank time
            initial_time_to_consider = BreakBank.objects.get(
                id=break_bank_id
            ).break_time
            # getting last accessed assigned test section ID
            last_accessed_test_section_id = get_last_accessed_test_section(
                assigned_test_data.id
            )
            # getting unpause break bank actions related to current test section (if they exist)
            test_section_related_unpause_break_bank_actions = (
                BreakBankActions.objects.filter(
                    break_bank_id=break_bank_id,
                    action_type=BreakBankActionsConstants.UNPAUSE,
                    test_section_id=last_accessed_test_section_id,
                ).order_by("modify_date")
            )

            if test_section_related_unpause_break_bank_actions:
                # getting last unpause break bank action
                last_unpause_break_bank_action = (
                    test_section_related_unpause_break_bank_actions.last()
                )
                # getting unpause break actions excluding the ones for the current test section (useful for multi sections test)
                excluding_current_test_section_unpause_break_bank_actions = (
                    BreakBankActions.objects.filter(
                        break_bank_id=break_bank_id,
                        action_type=BreakBankActionsConstants.UNPAUSE,
                    )
                    .order_by("modify_date")
                    .exclude(
                        test_section_id=last_accessed_test_section_id,
                    )
                )
                # excluding_current_test_section_unpause_break_bank_actions exists
                if excluding_current_test_section_unpause_break_bank_actions:
                    # reinitializing initial_time_to_consider
                    initial_time_to_consider = (
                        excluding_current_test_section_unpause_break_bank_actions.last().new_remaining_time
                    )
                # calculating consumed break bank time (initial time to consider - last unpause action new remaining time)
                consumed_break_bank_time = (
                    initial_time_to_consider
                    - last_unpause_break_bank_action.new_remaining_time
                )

    # if initial_time is defined
    if initial_time is not None:
        # calculatingthe total additional time (locked_time + consumed_break_bank_time)
        total_additional_time = locked_time + consumed_break_bank_time

        # getting updated time
        updated_time = initial_time + timedelta(seconds=total_additional_time)
    else:
        # should only happen for tests from Jmeter
        updated_time = "no-access-times-test"
    return {
        "time": updated_time,
        "timed_assigned_test_section_ids": timed_assigned_test_section_test_section_ids,
    }


# this function is useful to get the updated test time of the candidate
def get_updated_time_during_lock(assigned_test_id, test_section_id):
    try:
        # getting LOCKED ta actions based on provided assigned_test_id and test_section_id
        ta_actions = TaActions.objects.filter(
            assigned_test_id=assigned_test_id,
            test_section_id=test_section_id,
            action_type_id=TaActionsConstants.LOCK,
        )
        # there is at least one existing PAUSED/LOCKED ta action
        if ta_actions:
            # initializing time calculation variables
            locked_paused_time = 0
            initial_time = None
            # looping in filtered ta_actions
            for ta_action in ta_actions:
                # getting test_section to make sure that this is a timed section
                test_section = TestSection.objects.get(id=ta_action.test_section_id)
                # this is a timed section (default_time exists)
                if test_section.default_time is not None:
                    # getting assigned test section access time (first one)
                    assigned_test_section = AssignedTestSection.objects.get(
                        assigned_test_id=assigned_test_id,
                        test_section_id=ta_action.test_section_id,
                    )
                    assigned_test_section_access_time = (
                        AssignedTestSectionAccessTimes.objects.filter(
                            assigned_test_section_id=assigned_test_section.id
                        ).first()
                    )

                    # assigned test section access time exists
                    if assigned_test_section_access_time is not None:
                        # updating initial_time with assigned_test_section_access_time value
                        initial_time = assigned_test_section_access_time.time

                        # LOCK action
                        if ta_action.action_type_id == TaActionsConstants.LOCK:
                            # getting lock test action object
                            lock_test_action = LockTestActions.objects.get(
                                ta_action_id=ta_action.id
                            )

                            # lock test action end date
                            if lock_test_action.lock_end_date is not None:
                                # calculating locked time in seconds (keeping only 3 digits after the decimal point)
                                # incrementing locked_pause_time value
                                locked_paused_time += round(
                                    (
                                        lock_test_action.lock_end_date
                                        - lock_test_action.lock_start_date
                                    ).total_seconds(),
                                    3,
                                )

            # initial_time has been updated at least once
            if initial_time is not None:
                # getting updated time
                updated_time = initial_time + timedelta(seconds=locked_paused_time)
                return updated_time
    except:
        pass


# getting total test time of an assigned test
def get_total_assigned_test_time_in_seconds(assigned_test_id):
    # initializing total_test_time
    total_test_time = 0
    # getting assigned test sections
    assigned_test_sections = AssignedTestSection.objects.filter(
        assigned_test_id=assigned_test_id
    )
    # looping in assigned_test_sections
    for assigned_test_section in assigned_test_sections:
        if assigned_test_section.test_section_time is not None:
            total_test_time += int(assigned_test_section.test_section_time)
    # returning total test time (in seconds)
    return total_test_time * 60


# getting total test time of a specified test (not necessarily assigned test)
# * does not consider accommodations
def get_total_test_time(test_id):
    # initializing total_test_time
    total_test_time = 0
    # getting test sections
    test_sections = TestSection.objects.filter(test_definition_id=test_id)
    # looping in test_sections
    for test_section in test_sections:
        if test_section.default_time is not None:
            total_test_time += int(test_section.default_time)
    # returning total test time
    return total_test_time


def validate_timeout_state(request):
    success, parameters = get_needed_parameters(
        ["assigned_test_id", "test_section_id"],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    # getting assigned test section data
    assigned_test_section_data = AssignedTestSection.objects.get(
        assigned_test_id=parameters["assigned_test_id"],
        test_section_id=parameters["test_section_id"],
    )
    # timed section
    if assigned_test_section_data.test_section_time is not None:
        # getting current time
        current_time = datetime.now(timezone.utc)
        # getting updated time after locks and pauses
        updated_time = getting_updated_time_after_lock_pause(request)

        # calculating time between current and updated time (in minutes)
        calculated_time = (current_time - updated_time.data).total_seconds() / 60

        # calculated time > test section time
        if calculated_time > int(assigned_test_section_data.test_section_time):
            return Response(
                {"info": "user should be timed out"}, status=status.HTTP_409_CONFLICT
            )
        else:
            return Response({"info": "no action needed"}, status=status.HTTP_200_OK)
    # not a timed section
    else:
        return Response({"info": "no action needed"}, status=status.HTTP_200_OK)
