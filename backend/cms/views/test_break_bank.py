from rest_framework.response import Response
from rest_framework import status
from cms.views.utils import get_needed_parameters
from backend.custom_models.accommodation_request import AccommodationRequest
from backend.custom_models.break_bank import BreakBank
from backend.custom_models.break_bank_actions import BreakBankActions
from backend.custom_models.assigned_test import AssignedTest
from backend.static.assigned_test_status import AssignedTestStatus
from backend.views.utils import get_new_break_bank_remaining_time

# Break Bank Actions Definition
# mirror of BreakBankActions under ...\frontend\src\components\testFactory\Constants.js
class BreakBankActionsConstants:
    PAUSE = "PAUSE"
    UNPAUSE = "UNPAUSE"


def get_break_bank_remaining_time(request):
    success, parameters = get_needed_parameters(["accommodation_request_id"], request)

    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

    # accommodation request exists
    try:
        # getting respective accommodation request data
        accommodation_request_data = AccommodationRequest.objects.get(
            id=parameters["accommodation_request_id"]
        )

        # no break bank defined
        if accommodation_request_data.break_bank_id is None:
            return Response({"existing_break_bank_accommodation_request": False})

        # break bank defined
        else:
            # getting break bank actions where action_type is "UNPAUSE" (if they exist)
            unpause_break_bank_actions = BreakBankActions.objects.filter(
                break_bank_id=accommodation_request_data.break_bank_id,
                action_type=BreakBankActionsConstants.UNPAUSE,
            ).order_by("modify_date")

            # unpause break bank actions contains data
            if unpause_break_bank_actions:
                time_remaining = unpause_break_bank_actions.last().new_remaining_time

            # no unpause break bank actions
            else:
                # getting total time of the break bank
                time_remaining = BreakBank.objects.get(
                    id=accommodation_request_data.break_bank_id
                ).break_time

            return Response(
                {
                    "existing_break_bank_accommodation_request": True,
                    "time_remaining": time_remaining,
                }
            )

    # accommodation request DOES NOT exist
    except:
        return Response({"existing_break_bank_accommodation_request": False})


def trigger_break_bank_action(request):
    success, parameters = get_needed_parameters(
        ["accommodation_request_id", "action_type", "test_section_id"], request
    )

    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

    # getting break bank ID related to provided accommodation request ID
    break_bank_id = AccommodationRequest.objects.get(
        id=parameters["accommodation_request_id"]
    ).break_bank_id

    # PAUSE action
    if parameters["action_type"] == BreakBankActionsConstants.PAUSE:
        # creating new PAUSE action in break bank actions table
        BreakBankActions.objects.create(
            action_type=BreakBankActionsConstants.PAUSE,
            new_remaining_time=None,
            break_bank_id=break_bank_id,
            test_section_id=parameters["test_section_id"],
        )
        # updating test status to PAUSE (15)
        assigned_test = AssignedTest.objects.get(
            accommodation_request_id=parameters["accommodation_request_id"]
        )
        assigned_test.status = AssignedTestStatus.PAUSED
        assigned_test.save()
    # UNPAUSE action
    elif parameters["action_type"] == BreakBankActionsConstants.UNPAUSE:
        try:
            # getting new break bank remaining time
            new_remaining_time = get_new_break_bank_remaining_time(
                parameters["accommodation_request_id"]
            )
            # creating new UNPAUSE action in break bank actions table
            BreakBankActions.objects.create(
                action_type=BreakBankActionsConstants.UNPAUSE,
                new_remaining_time=new_remaining_time,
                break_bank_id=break_bank_id,
                test_section_id=parameters["test_section_id"],
            )
            # updating test status to PAUSE (15)
            assigned_test = AssignedTest.objects.get(
                accommodation_request_id=parameters["accommodation_request_id"]
            )
            assigned_test.status = AssignedTestStatus.ACTIVE
            assigned_test.save()
        # should never happen
        except:
            return Response(
                {
                    "error": "something happened while trying to UNPAUSE (should never happen)"
                },
                status=status.HTTP_400_BAD_REQUEST,
            )
    # not supported action
    else:
        return Response(
            {"error": "not supported action_type"}, status=status.HTTP_400_BAD_REQUEST
        )

    return Response(None)
