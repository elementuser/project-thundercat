from operator import itemgetter, and_
from random import shuffle, sample
from ast import literal_eval
import json
import urllib.parse
from itertools import groupby
from urllib import parse
from itertools import chain
from functools import reduce
from django.db.models import Q, Count
from django.db import transaction, IntegrityError
from rest_framework import status
from rest_framework.response import Response
from text_resources_backend.text_resources import TextResources
from cms.cms_models.bundles import Bundles
from cms.views.utils import get_optional_parameters, has_duplicates
from cms.cms_models.item_attribute_value_drafts import ItemAttributeValueDrafts
from cms.cms_models.item_bank_attribute_values_text import (
    ItemBankAttributeValuesText,
)
from cms.static.item_bank_utils import AttributeValueType
from cms.cms_models.item_attribute_value import ItemAttributeValue
from cms.serializers.item_bank_serializer import (
    ItemCommentsSerializer,
    ItemLatestVersionsDataViewSimpleSerializer,
)
from cms.cms_models.item_content_drafts import ItemContentDrafts
from cms.cms_models.item_content_text_drafts import ItemContentTextDrafts
from cms.cms_models.item_option_drafts import ItemOptionDrafts
from cms.cms_models.item_option_text_drafts import ItemOptionTextDrafts
from cms.static.item_bank_utils import DRAFT_VERSION
from cms.cms_models.item_bank import ItemBank
from cms.cms_models.items import Items
from cms.cms_models.item_response_formats import ItemResponseFormats
from cms.cms_models.item_development_statuses import ItemDevelopmentStatuses
from cms.cms_models.item_historical_ids import ItemHistoricalIds
from cms.cms_models.item_bank_permissions import ItemBankPermissions
from cms.cms_models.item_bank_access_types import ItemBankAccessTypes
from cms.cms_models.item_bank_attributes import ItemBankAttributes
from cms.cms_models.item_bank_attributes_text import ItemBankAttributesText
from cms.cms_models.item_bank_attribute_values import ItemBankAttributeValues
from cms.cms_models.item_comments import ItemComments
from cms.cms_models.item_drafts import ItemDrafts
from cms.cms_models.item_content import ItemContent
from cms.cms_models.item_content_text import ItemContentText
from cms.cms_models.item_option import ItemOption
from cms.cms_models.item_option_text import ItemOptionText
from cms.cms_models.bundle_rule import BundleRule
from cms.cms_models.bundle_rule_type import BundleRuleType
from cms.cms_models.bundle_bundles_rule import BundleBundlesRule
from cms.cms_models.bundle_bundles_rule_associations import (
    BundleBundlesRuleAssociations,
)
from cms.cms_models.bundle_items_basic_rule import BundleItemsBasicRule
from cms.static.bundles_utils import BundleRuleTypeStatic
from cms.serializers.item_bank_serializer import (
    ItemBankSerializer,
    ItemBankSimpleSerializer,
    SelectedItemBankDataSerializer,
    SelectedRdOperationsItemBankDataSerializer,
    ItemBankAccessTypesSerializer,
    ResponseFormatsSerializer,
    DevelopmentStatusesSerializer,
    ItemContentSerializer,
    BundlesSimpleSerializer,
)
from cms.views.utils import get_needed_parameters
from cms.cms_models.bundle_items_association import BundleItemsAssociation
from cms.cms_models.bundle_bundles_association import BundleBundlesAssociation
from cms.static.item_bank_utils import ElementToUpdate, ItemActionType
from cms.static.items_utils import (
    ItemDevelopmentStatus,
)
from cms.static.item_bank_access_types import ItemBankAccessType
from cms.serializers.item_bank_serializer import (
    ItemLatestVersionsDataViewSerializer,
    AllItemsDataViewSerializer,
    ItemDraftsViewSerializer,
)
from cms.cms_models.item_bank_rule import ItemBankRule
from cms.cms_models.test_section_component import TestSectionComponent
from cms.cms_models.test_section import TestSection
from user_management.views.utils import CustomPagination
from user_management.static.permission import Permission
from user_management.user_management_models.user_models import User
from user_management.user_management_models.custom_permissions_model import (
    CustomPermissions,
)
from user_management.user_management_models.custom_user_permissions_model import (
    CustomUserPermissions,
)
from db_views.db_view_models.item_latest_versions_data_vw import (
    ItemLatestVersionsDataVW,
)
from db_views.db_view_models.all_items_data_vw import AllItemsDataVW
from db_views.db_view_models.item_drafts_vw import ItemDraftsVW
from db_views.db_view_models.item_bank_attribute_values_vw import (
    ItemBankAttributeValuesVW,
)
from db_views.db_view_models.bundle_data_vw import BundleDataVW
from db_views.serializers.bundle_serializers import (
    BundleDataViewSerializer,
    BundleDetailedDataViewSerializer,
)
from db_views.db_view_models.bundle_bundles_rule_data_vw import BundleBundlesRuleDataVW

from backend.ref_table_views.cat_ref_departments_vw import CatRefDepartmentsVW
from backend.static.psc_department import PscDepartment
from backend.custom_models.language import Language
from backend.views.utils import get_user_info_from_jwt_token
from backend.custom_models.assigned_questions_list import AssignedQuestionsList


def get_all_item_banks(request):
    user_info = get_user_info_from_jwt_token(request)

    success, parameters = get_needed_parameters(
        ["call_source", "page", "page_size"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    call_source, page, page_size = itemgetter("call_source", "page", "page_size")(
        parameters
    )

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # ==================== ITEM BANK ACCESSES/PERMISSIONS LOGIC ====================
    # checking if user has the R&D Operations role or is a super user
    rd_operations_permission_id = CustomPermissions.objects.get(
        codename=Permission.RD_OPERATIONS
    ).permission_id
    has_rd_operations_role = CustomUserPermissions.objects.filter(
        permission_id=rd_operations_permission_id, user_id=user_info["username"]
    )

    is_super_user = User.objects.get(username=user_info["username"]).is_staff

    # getting all item banks
    all_item_banks = ItemBank.objects.filter(archived=0)

    # called from the test builder
    if call_source == Permission.TEST_DEVELOPER:
        # setting has_rd_operations_role to false
        has_rd_operations_role = False

    # user has the R&D Operations role or is a super user
    if has_rd_operations_role or is_super_user:
        # user has access to all item banks
        allowed_item_bank_ids = []
        for item_bank in all_item_banks:
            allowed_item_bank_ids.append(item_bank.id)
    else:
        # getting item bank access type IDs
        item_bank_access_type_ids = ItemBankAccessTypes.objects.filter(
            codename__in=(ItemBankAccessType.OWNER, ItemBankAccessType.CONTRIBUTOR)
        ).values_list("id", flat=True)
        # getting item bank accesses/permissions
        allowed_item_bank_ids = ItemBankPermissions.objects.filter(
            username_id=user_info["username"],
            item_bank_access_type_id__in=item_bank_access_type_ids,
        ).values_list("item_bank_id", flat=True)
    # ==================== ITEM BANK ACCESSES/PERMISSIONS LOGIC (END) ====================

    # ordering item banks by custom item bank ID (ascending)
    ordered_item_banks = sorted(
        all_item_banks,
        key=lambda k: k.custom_item_bank_id.lower(),
        reverse=False,
    )

    # only getting data for current selected page
    new_all_item_banks = ordered_item_banks[
        (current_page - 1) * page_size : current_page * page_size
    ]

    # getting serialized item banks data
    serialized_ordered_item_banks_data = ItemBankSerializer(
        new_all_item_banks,
        many=True,
        context={"allowed_item_bank_ids": allowed_item_bank_ids},
    ).data

    # retuning final data using pagination library
    return paginator.get_paginated_response(
        serialized_ordered_item_banks_data, len(all_item_banks), current_page, page_size
    )


def get_found_item_banks(request):
    user_info = get_user_info_from_jwt_token(request)

    success, parameters = get_needed_parameters(
        ["keyword", "current_language", "call_source", "page", "page_size"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    (
        keyword,
        current_language,
        call_source,
        page,
        page_size,
    ) = itemgetter(
        "keyword",
        "current_language",
        "call_source",
        "page",
        "page_size",
    )(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # if blank search
    if keyword == " ":
        found_item_banks = ItemBank.objects.filter(archived=0)
    # regular search
    else:
        keyword = keyword.lower()

        # initializing needed variables
        found_dates_item_bank_ids = []
        found_department_item_bank_ids = []
        found_name_item_bank_ids = []

        # getting matching custom item bank ids
        found_custom_ids_item_bank_ids = ItemBank.objects.filter(
            Q(custom_item_bank_id__icontains=keyword)
        ).values_list("id", flat=True)

        # API called from a R&D Operations user (from the RDO page)
        if call_source == Permission.RD_OPERATIONS:
            # getting matching modify dates
            found_dates_item_bank_ids = ItemBank.objects.filter(
                Q(modify_date__icontains=keyword)
            ).values_list("id", flat=True)

            # search name while interface is in English
            if current_language == "en":
                # getting matching department IDs
                matching_dept_ids = list(
                    CatRefDepartmentsVW.objects.filter(
                        Q(edesc__icontains=keyword) | Q(eabrv__icontains=keyword)
                    ).values_list("dept_id", flat=True)
                )
                # checking if the keyword is matching with the PSC department (default department if the provided dept_id does not exist)
                matching_psc_dept = (
                    keyword in PscDepartment.EDESC.lower()
                    or keyword in PscDepartment.EABRV.lower()
                )
                # matching psc dept
                if matching_psc_dept:
                    matching_dept_ids.append(PscDepartment.DEPT_ID)
                # getting all item bank ids based on keyword that is matching with en_name
                found_department_item_bank_ids = ItemBank.objects.filter(
                    Q(department_id__in=matching_dept_ids)
                ).values_list("id", flat=True)
            # search name while interface is in French
            else:
                # getting matching department IDs
                matching_dept_ids = list(
                    CatRefDepartmentsVW.objects.filter(
                        Q(fdesc__icontains=keyword) | Q(fabrv__icontains=keyword)
                    ).values_list("dept_id", flat=True)
                )
                # checking if the keyword is matching with the PSC department (default department if the provided dept_id does not exist)
                matching_psc_dept = (
                    keyword in PscDepartment.FDESC.lower()
                    or keyword in PscDepartment.FABRV.lower()
                )
                # matching psc dept
                if matching_psc_dept:
                    matching_dept_ids.append(PscDepartment.DEPT_ID)
                # getting all item bank ids based on keyword that is matching with en_name
                found_department_item_bank_ids = ItemBank.objects.filter(
                    Q(department_id__in=matching_dept_ids)
                ).values_list("id", flat=True)

        # API called from a test developer user (from the test builder)
        elif call_source == Permission.TEST_DEVELOPER:
            # search name while interface is in English
            if current_language == "en":
                # getting matching names
                found_name_item_bank_ids = ItemBank.objects.filter(
                    Q(en_name__icontains=keyword)
                ).values_list("id", flat=True)
            # search name while interface is in French
            else:
                # getting matching names
                found_name_item_bank_ids = ItemBank.objects.filter(
                    Q(fr_name__icontains=keyword)
                ).values_list("id", flat=True)

        # combining found IDs (without duplicates)
        combined_item_bank_ids = list(
            chain(
                found_custom_ids_item_bank_ids,
                found_dates_item_bank_ids,
                found_department_item_bank_ids,
                found_name_item_bank_ids,
            )
        )

        # if at least one result has been found based on the keyword provided
        if (
            found_custom_ids_item_bank_ids
            or found_dates_item_bank_ids
            or found_department_item_bank_ids
            or found_name_item_bank_ids
        ):
            found_item_banks = ItemBank.objects.filter(
                id__in=combined_item_bank_ids, archived=0
            )

        # there are no results found based on the keyword provided
        else:
            return Response({"no results found"}, status=status.HTTP_200_OK)

    # creating new found_item_banks_array
    found_item_banks_array = []

    for item_bank in found_item_banks:
        found_item_banks_array.append(item_bank)

    # ==================== ITEM BANK ACCESSES/PERMISSIONS LOGIC ====================
    # checking if user has the R&D Operations role or is a super user
    rd_operations_permission_id = CustomPermissions.objects.get(
        codename=Permission.RD_OPERATIONS
    ).permission_id
    has_rd_operations_role = CustomUserPermissions.objects.filter(
        permission_id=rd_operations_permission_id, user_id=user_info["username"]
    )

    is_super_user = User.objects.get(username=user_info["username"]).is_staff

    # getting all item banks
    all_item_banks = ItemBank.objects.filter(archived=0)

    # called from the test builder
    if call_source == Permission.TEST_DEVELOPER:
        # setting has_rd_operations_role to false
        has_rd_operations_role = False

    # user has the R&D Operations role or is a super user
    if has_rd_operations_role or is_super_user:
        # user has access to all item banks
        allowed_item_bank_ids = []
        for item_bank in all_item_banks:
            allowed_item_bank_ids.append(item_bank.id)
    else:
        # getting item bank access type IDs
        item_bank_access_type_ids = ItemBankAccessTypes.objects.filter(
            codename__in=(ItemBankAccessType.OWNER, ItemBankAccessType.CONTRIBUTOR)
        ).values_list("id", flat=True)
        # getting item bank accesses/permissions
        allowed_item_bank_ids = ItemBankPermissions.objects.filter(
            username_id=user_info["username"],
            item_bank_access_type_id__in=item_bank_access_type_ids,
        ).values_list("item_bank_id", flat=True)
    # ==================== ITEM BANK ACCESSES/PERMISSIONS LOGIC (END) ====================

    # ordering item banks by custom item bank ID (ascending)
    ordered_item_banks = sorted(
        found_item_banks_array,
        key=lambda k: k.custom_item_bank_id.lower(),
        reverse=False,
    )

    # only getting data for current selected page
    new_found_item_banks = ordered_item_banks[
        (current_page - 1) * page_size : current_page * page_size
    ]

    # getting serialized item banks data
    serialized_item_banks_data = ItemBankSerializer(
        new_found_item_banks,
        many=True,
        context={"allowed_item_bank_ids": allowed_item_bank_ids},
    ).data

    # retuning final data using pagination library
    return paginator.get_paginated_response(
        serialized_item_banks_data, len(found_item_banks), current_page, page_size
    )


def get_item_banks_for_test_builder(request):
    # getting all items banks
    all_item_banks = ItemBank.objects.all()
    # serializing the data
    serialized_data = ItemBankSimpleSerializer(all_item_banks, many=True).data
    return Response(serialized_data)


def get_item_bank_bundles_for_test_builder(request):
    success, parameters = get_needed_parameters(["item_bank_id"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

    # getting related items bank bundles
    # TODO: add the active/pullable state in the filter to only get the proper bundles
    item_bank_bundles = Bundles.objects.filter(item_bank_id=parameters["item_bank_id"])
    # serializing the data
    serialized_data = BundlesSimpleSerializer(item_bank_bundles, many=True).data
    return Response(serialized_data)


def get_selected_item_bank_data_as_rd_operations(request):
    success, parameters = get_needed_parameters(["item_bank_id"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

    try:
        # getting item bank data
        item_bank_data = ItemBank.objects.get(id=parameters["item_bank_id"])
    except ItemBank.DoesNotExist:
        return Response(
            {"error": "The specified item_bank_id does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    # getting serialized data
    serialized_data = SelectedRdOperationsItemBankDataSerializer(
        item_bank_data, many=False
    ).data
    return Response(serialized_data)


def get_selected_item_bank_data(request):
    user_info = get_user_info_from_jwt_token(request)

    success, parameters = get_needed_parameters(["custom_item_bank_id"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    custom_item_bank_id = itemgetter("custom_item_bank_id")(parameters)

    # getting respective item bank data based on provided ID
    try:
        item_bank_data = ItemBank.objects.get(custom_item_bank_id=custom_item_bank_id)

        # serializing the data
        serialized = SelectedItemBankDataSerializer(
            item_bank_data, many=False, context={"username": user_info["username"]}
        )
        return Response(serialized.data, status=status.HTTP_200_OK)
    # item bank not found
    except ItemBank.DoesNotExist:
        return Response(
            {"error": "no item bank found based on provided parameters"},
            status=status.HTTP_400_BAD_REQUEST,
        )


def create_new_item_bank(request):
    success, parameters = get_needed_parameters(
        ["department_id", "custom_item_bank_id", "language_id", "comments"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    department_id, custom_item_bank_id, language_id, comments = itemgetter(
        "department_id", "custom_item_bank_id", "language_id", "comments"
    )(parameters)

    try:
        ItemBank.objects.create(
            department_id=department_id,
            custom_item_bank_id=custom_item_bank_id,
            en_name="",
            fr_name="",
            language_id=language_id,
            comments=comments,
            archived=0,
        )

        return Response(status=status.HTTP_200_OK)

    except:
        return Response(
            {"error": "Item Bank Already Exists"}, status=status.HTTP_400_BAD_REQUEST
        )


def update_item_bank_data_as_rd_operations(request):
    item_bank_data = json.loads(request.body)

    try:
        # getting item bank
        item_bank = ItemBank.objects.get(id=item_bank_data["id"])

        # checking if the provided custom_item_bank_id already exists
        custom_item_bank_id_already_exists = ItemBank.objects.filter(
            custom_item_bank_id=item_bank_data["custom_item_bank_id"]
        ).exclude(id=item_bank_data["id"])

        # custom_item_bank_id already exists
        if custom_item_bank_id_already_exists:
            return Response(
                {"error": "This Custom Item Bank ID Already Exists"},
                status=status.HTTP_409_CONFLICT,
            )
        else:
            # updating data
            item_bank.custom_item_bank_id = item_bank_data["custom_item_bank_id"]
            item_bank.en_name = item_bank_data["en_name"]
            item_bank.fr_name = item_bank_data["fr_name"]
            item_bank.department_id = item_bank_data["department_id"]
            item_bank.language_id = item_bank_data["language"]

            item_bank.save()
            return Response(status=status.HTTP_200_OK)
    except ItemBank.DoesNotExist:
        return Response(
            {"error": "The Item Bank Does Not Exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )


def add_test_developer(request):
    success, parameters = get_needed_parameters(
        ["username_id", "item_bank_id"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    username_id, item_bank_id = itemgetter("username_id", "item_bank_id")(parameters)

    # decoding username that might contain special characters
    username_id = urllib.parse.unquote(username_id)

    # making sure that the specified Test Developer combined with the item bank ID does not already exist
    if not ItemBankPermissions.objects.filter(
        username_id=username_id, item_bank_id=item_bank_id
    ):
        # getting none item bank access type id
        none_access_type_id = ItemBankAccessTypes.objects.get(
            codename=ItemBankAccessType.NONE
        ).id
        # creating new entry in Item Bank Permissions model
        ItemBankPermissions.objects.create(
            item_bank_id=item_bank_id,
            item_bank_access_type_id=none_access_type_id,
            username_id=username_id,
        )
        return Response(status=status.HTTP_200_OK)
    else:
        return Response(
            {
                "error": "The specified test developer is already associated to this item bank"
            },
            status=status.HTTP_400_BAD_REQUEST,
        )


def delete_test_developer(request):
    success, parameters = get_needed_parameters(
        ["username_id", "item_bank_id"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    username_id, item_bank_id = itemgetter("username_id", "item_bank_id")(parameters)

    # decoding username that might contain special characters
    username_id = urllib.parse.unquote(username_id)

    try:
        # getting item bank permissions data for the specified user
        item_bank_permissions_data = ItemBankPermissions.objects.get(
            username_id=username_id, item_bank_id=item_bank_id
        )
        # deleting entry
        item_bank_permissions_data.delete()
        return Response(status=status.HTTP_200_OK)

    except ItemBankPermissions.DoesNotExist:
        return Response(
            {
                "error": "The specified test developer does not exist in the current item bank permissions"
            },
            status=status.HTTP_400_BAD_REQUEST,
        )


def get_item_bank_access_types(request):
    # getting level accesses
    access_types = ItemBankAccessTypes.objects.all().order_by("priority")
    # serializing the data
    serialized_data = ItemBankAccessTypesSerializer(access_types, many=True).data
    return Response(serialized_data)


def update_item_bank_access_type(request):
    success, parameters = get_needed_parameters(
        ["username_id", "item_bank_id", "item_bank_access_type_id"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    username_id, item_bank_id, item_bank_access_type_id = itemgetter(
        "username_id", "item_bank_id", "item_bank_access_type_id"
    )(parameters)

    # decoding username that might contain special characters
    username_id = urllib.parse.unquote(username_id)

    try:
        # getting item bank permissions data for the specified user
        item_bank_permissions_data = ItemBankPermissions.objects.get(
            username_id=username_id, item_bank_id=item_bank_id
        )
        # updating the item bank access type
        item_bank_permissions_data.item_bank_access_type_id = item_bank_access_type_id
        item_bank_permissions_data.save()
        return Response(status=status.HTTP_200_OK)

    except ItemBankPermissions.DoesNotExist:
        return Response(
            {
                "error": "The specified test developer does not exist in the current item bank permissions"
            },
            status=status.HTTP_400_BAD_REQUEST,
        )

    return Response(None)


def update_item_bank_data(request):
    item_bank_data = json.loads(request.body)

    # making sure that element_to_update is defined
    try:
        item_bank_data["element_to_update"]
    except:
        return Response(
            {"error": "no element_to_update parameter"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    # getting the item bank definition data
    item_bank_definition = item_bank_data["item_bank_definition"][0]

    try:
        # getting item bank
        item_bank = ItemBank.objects.get(id=item_bank_definition["id"])

        # ==================== element_to_update = ITEM_BANK_DEFINITION ====================
        if item_bank_data["element_to_update"] == ElementToUpdate.ITEM_BANK_DEFINITION:
            # updating existing item bank definition
            item_bank.custom_item_bank_id = item_bank_definition["custom_item_bank_id"]
            item_bank.en_name = item_bank_definition["en_name"]
            item_bank.fr_name = item_bank_definition["fr_name"]
            item_bank.save()

            return Response(status=status.HTTP_200_OK)
        # ==================== element_to_update = ITEM_BANK_DEFINITION (END) ====================

        # ==================== element_to_update = ITEM_BANK_ATTRIBUTES ====================
        elif (
            item_bank_data["element_to_update"] == ElementToUpdate.ITEM_BANK_ATTRIBUTES
        ):
            # getting the item bank attributes data
            item_bank_attributes = item_bank_data["item_bank_attributes"]
            # getting existing item bank attributes related to item bank definition
            existing_item_bank_attributes = ItemBankAttributes.objects.filter(
                item_bank_id=item_bank.id
            )
            # initializing needed variables
            item_bank_attribute_ids = []
            considered_item_bank_attribute_ids = []
            item_bank_attribute_values_data = []
            # looping in item bank attributes
            for index, attribute in enumerate(item_bank_attributes):
                # initializing attribute_already_considered
                attribute_already_considered = False
                # there is at least one existing item bank attribute
                if existing_item_bank_attributes:
                    # looping in existing_item_bank_attributes
                    for existing_item_bank_attribute in existing_item_bank_attributes:
                        # if attribute id matches the existing item bank attribute id
                        if attribute["id"] == existing_item_bank_attribute.id:
                            # updating order
                            existing_item_bank_attribute.attribute_order = attribute[
                                "order"
                            ]
                            existing_item_bank_attribute.save()

                            # getting attribute text already in database
                            existing_item_bank_attributes_text = (
                                ItemBankAttributesText.objects.filter(
                                    item_bank_attribute=existing_item_bank_attribute.id
                                )
                            )

                            # update existing item bank attributes text
                            for (
                                existing_attribute_text
                            ) in existing_item_bank_attributes_text:
                                language = existing_attribute_text.language

                                attribute_text_id = attribute[
                                    "item_bank_attribute_text"
                                ][str(language)][0]["id"]

                                # if attribute item_bank_attribute_text's id matches the existing item bank attribute text id
                                if attribute_text_id == existing_attribute_text.id:
                                    # modify value of text in database
                                    existing_attribute_text.text = attribute[
                                        "item_bank_attribute_text"
                                    ][str(language)][0]["text"]
                                    existing_attribute_text.save()

                            # setting attribute_already_considered to True
                            attribute_already_considered = True
                            # adding id to item_bank_attribute_ids
                            item_bank_attribute_ids.append(attribute["id"])
                            # adding id to considered_item_bank_attribute_ids
                            considered_item_bank_attribute_ids.append(attribute["id"])

                # if current attribute has not been considered:
                # create ItemBankAttribute and ItemBankAttributesText
                if not attribute_already_considered:
                    # creating new item bank attributes
                    db_item_bank_attribute = ItemBankAttributes.objects.create(
                        attribute_order=attribute["order"],
                        item_bank_id=item_bank.id,
                    )

                    # creating new item bank attributes text

                    # creating english text
                    language_en = Language.objects.get(ISO_Code_1="en")
                    ItemBankAttributesText.objects.create(
                        text=attribute["item_bank_attribute_text"]["en"][0]["text"],
                        language=language_en,
                        item_bank_attribute=db_item_bank_attribute,
                    )

                    # creating french text
                    language_fr = Language.objects.get(ISO_Code_1="fr")
                    ItemBankAttributesText.objects.create(
                        text=attribute["item_bank_attribute_text"]["fr"][0]["text"],
                        language=language_fr,
                        item_bank_attribute=db_item_bank_attribute,
                    )

                    # adding id to item_bank_attribute_ids
                    item_bank_attribute_ids.append(db_item_bank_attribute.id)
                    # adding id to considered_item_bank_attribute_ids
                    considered_item_bank_attribute_ids.append(db_item_bank_attribute.id)

                # getting the item bank attribute values data
                item_bank_attribute_values = attribute["item_bank_attribute_values"]
                # getting existing item bank attribute values related to current item bank attribute
                existing_item_bank_attribute_values = (
                    ItemBankAttributeValues.objects.filter(
                        item_bank_attribute_id=item_bank_attribute_ids[index]
                    )
                )
                # initializing needed variables
                considered_item_bank_attribute_values_ids = []
                # looping in attribute values
                for attribute_value in item_bank_attribute_values:
                    # initializing attribute_value_already_considered
                    attribute_value_already_considered = False
                    # there is at least one existing item bank attribute value
                    if existing_item_bank_attribute_values:
                        # looping in existing_item_bank_attributes
                        for (
                            existing_item_bank_attribute_value
                        ) in existing_item_bank_attribute_values:
                            # if attribute value id matches the existing item bank attribute value id
                            if (
                                attribute_value["id"]
                                == existing_item_bank_attribute_value.id
                            ):
                                # updating item bank attribute value data - depending on attribute value type
                                if (
                                    existing_item_bank_attribute_value.attribute_value_type
                                    == AttributeValueType.TEXT
                                ):
                                    temp_value_text = ItemBankAttributeValuesText.objects.get(
                                        item_bank_attribute_values_id=attribute_value[
                                            "id"
                                        ]
                                    )
                                    temp_value_text.text = attribute_value["value"][
                                        "text"
                                    ]
                                    temp_value_text.save()

                                existing_item_bank_attribute_value.attribute_value_order = attribute_value[
                                    "order"
                                ]
                                existing_item_bank_attribute_value.save()
                                # setting attribute_value_already_considered to True
                                attribute_value_already_considered = True
                                # adding id to item_bank_attribute_values_data
                                item_bank_attribute_values_data.append(
                                    {
                                        "item_bank_attribute_id": item_bank_attribute_ids[
                                            index
                                        ],
                                        "item_bank_attribute_value_id": attribute_value[
                                            "id"
                                        ],
                                    }
                                )
                                # adding id to considered_item_bank_attribute_values_ids
                                considered_item_bank_attribute_values_ids.append(
                                    attribute_value["id"]
                                )

                    # if current attribute value has not been considered
                    if not attribute_value_already_considered:
                        # creating new item bank attribute values
                        # AttributeValueType == Text Value
                        if (
                            attribute_value["attribute_value_type"]
                            == AttributeValueType.TEXT
                        ):
                            db_item_bank_attribute_value = (
                                ItemBankAttributeValues.objects.create(
                                    attribute_value_type=attribute_value[
                                        "attribute_value_type"
                                    ],
                                    attribute_value_order=attribute_value["order"],
                                    item_bank_attribute_id=item_bank_attribute_ids[
                                        index
                                    ],
                                )
                            )
                            ItemBankAttributeValuesText.objects.create(
                                item_bank_attribute_values=db_item_bank_attribute_value,
                                text=attribute_value["value"]["text"],
                            )
                        # adding id to item_bank_attribute_ids
                        item_bank_attribute_values_data.append(
                            {
                                "item_bank_attribute_id": item_bank_attribute_ids[
                                    index
                                ],
                                "item_bank_attribute_value_id": db_item_bank_attribute_value.id,
                            }
                        )

                # deleting not considered item bank attribute values
                # looping in existing_item_bank_attribute_values
                for (
                    existing_item_bank_attribute_value
                ) in existing_item_bank_attribute_values:
                    # if id of existing_item_bank_attribute_value does not exist in considered_item_bank_attribute_values_ids
                    if (
                        existing_item_bank_attribute_value.id
                        not in considered_item_bank_attribute_values_ids
                    ):
                        # Delete Value Text
                        ItemBankAttributeValuesText.objects.get(
                            item_bank_attribute_values_id=existing_item_bank_attribute_value.id
                        ).delete()

                        # delete it
                        existing_item_bank_attribute_value.delete()

            # deleting not considered item bank attributes
            # looping in existing_item_bank_attributes
            for existing_item_bank_attribute in existing_item_bank_attributes:
                # if id of existing_item_bank_attribute does not exist in considered_item_bank_attribute_ids
                if (
                    existing_item_bank_attribute.id
                    not in considered_item_bank_attribute_ids
                ):
                    # delete associated item bank attribute values
                    associated_item_bank_attribute_values = (
                        ItemBankAttributeValues.objects.filter(
                            item_bank_attribute_id=existing_item_bank_attribute.id
                        )
                    )
                    for (
                        associated_item_bank_attribute_value
                    ) in associated_item_bank_attribute_values:
                        # delete associated item attribute values
                        associated_item_attribute_values = ItemAttributeValue.objects.filter(
                            item_bank_attribute_value_id=associated_item_bank_attribute_value.id
                        )
                        for (
                            associated_item_attribute_value
                        ) in associated_item_attribute_values:
                            associated_item_attribute_value.delete()

                        # delete associated item attribute values drafts
                        associated_item_attribute_values_drafts = ItemAttributeValueDrafts.objects.filter(
                            item_bank_attribute_value_id=associated_item_bank_attribute_value.id
                        )
                        for (
                            associated_item_attribute_value_drafts
                        ) in associated_item_attribute_values_drafts:
                            associated_item_attribute_value_drafts.delete()

                        # Delete ItemBankAttributeValues(Type) before deleting the ItemBankAttributeValues
                        if (
                            associated_item_bank_attribute_value.attribute_value_type
                            == AttributeValueType.TEXT
                        ):
                            # Delete Value Text
                            ItemBankAttributeValuesText.objects.get(
                                item_bank_attribute_values_id=associated_item_bank_attribute_value.id
                            ).delete()

                        associated_item_bank_attribute_value.delete()

                    # delete associated item bank attribute text
                    associated_item_bank_attribute_texts = (
                        ItemBankAttributesText.objects.filter(
                            item_bank_attribute_id=existing_item_bank_attribute.id
                        )
                    )
                    for (
                        associated_item_bank_attribute_text
                    ) in associated_item_bank_attribute_texts:
                        associated_item_bank_attribute_text.delete()
                    # delete item bank attribute
                    existing_item_bank_attribute.delete()

            # building the item bank attributes data array
            item_bank_attributes_data_array = []
            # looping in item_bank_attribute_ids
            for item_bank_attribute_id in item_bank_attribute_ids:
                # initializing temp_attribute_value_ids
                temp_attribute_value_ids = []
                # looping in item_bank_attribute_values_data
                for item_bank_attribute_value_data in item_bank_attribute_values_data:
                    # if current item_bank_attribute_id matches the id in the current item_bank_attribute_value_data.item_bank_attribute_id
                    if (
                        item_bank_attribute_id
                        == item_bank_attribute_value_data["item_bank_attribute_id"]
                    ):
                        # populating temp_attribute_value_ids
                        temp_attribute_value_ids.append(
                            item_bank_attribute_value_data[
                                "item_bank_attribute_value_id"
                            ]
                        )
                # populating item_bank_attributes_data_array
                item_bank_attributes_data_array.append(
                    {
                        "item_bank_attribute_id": item_bank_attribute_id,
                        "item_bank_attribute_value_ids": temp_attribute_value_ids,
                    }
                )

            # returning status 200 with needed data
            return Response(
                {
                    "item_bank_attributes_data": item_bank_attributes_data_array,
                },
                status=status.HTTP_200_OK,
            )
            # ==================== element_to_update = ITEM_BANK_ATTRIBUTES (END) ====================

        # if element_to_update is set with a non-existing value
        else:
            return Response(
                {"error": "element_to_update parameter not accepted"},
                status=status.HTTP_400_BAD_REQUEST,
            )

    except Exception as exception:
        # Exception printing will help for the maintenance of the application in case of an unexpected database issue
        print(exception)
        return Response(
            {
                "error": "Something happened during the item bank data update. Please investigate."
            },
            status=status.HTTP_400_BAD_REQUEST,
        )


def get_all_items(request):
    user_info = get_user_info_from_jwt_token(request)
    success, parameters = get_needed_parameters(
        ["item_bank_id", "page", "page_size"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    item_bank_id, page, page_size = itemgetter("item_bank_id", "page", "page_size")(
        parameters
    )

    optional_parameters = get_optional_parameters(["ignore_draft"], request)

    # initializing ignore_draft
    ignore_draft = False

    if optional_parameters["ignore_draft"]:
        ignore_draft = optional_parameters["ignore_draft"]

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # getting needed items
    latest_versions_items = ItemLatestVersionsDataVW.objects.filter(
        item_bank_id=item_bank_id
    )

    # ordering items by custom System ID (ascending)
    ordered_items = sorted(
        latest_versions_items,
        # converting second part of string (incrementing value) to int, so the ordering is respected
        key=lambda k: int(k.system_id.split("_")[1]),
        reverse=False,
    )

    # only getting data for current selected page
    new_all_items = ordered_items[
        (current_page - 1) * page_size : current_page * page_size
    ]

    # getting serialized item banks data
    serialized_ordered_items_data = ItemLatestVersionsDataViewSerializer(
        new_all_items,
        many=True,
    ).data

    # getting final data using pagination library
    final_obj = paginator.get_paginated_response(
        serialized_ordered_items_data, len(ordered_items), current_page, page_size
    )

    # initializing needed variables
    all_items_including_drafts = ItemLatestVersionsDataViewSerializer(
        ordered_items, many=True
    ).data
    related_item_drafts = []

    # making sure we have at least one item (to avoid errors)
    if all_items_including_drafts:
        # if ignore_draft is False
        if not ignore_draft or ignore_draft == "false":
            # getting all related item drafts
            related_item_drafts = ItemDraftsVW.objects.filter(
                # using the first element to get the item bank ID
                item_bank_id=all_items_including_drafts[0]["item_bank_id"],
                username_id=user_info["username"],
            )

    # looping in all_items_including_drafts
    for index, item_data in enumerate(all_items_including_drafts):
        # trying to get the index of the current related item draft (if it exists)
        try:
            index_of_current_related_item_draft = [
                x.system_id for x in related_item_drafts
            ].index(item_data["system_id"])
            context = {}
            context["contains_draft"] = True
            # updating the respective object with the item draft data
            all_items_including_drafts[index] = ItemDraftsViewSerializer(
                related_item_drafts[index_of_current_related_item_draft],
                many=False,
                context=context,
            ).data
        except:
            pass

    # adding all_items attribute to the final object variable (useful for frontend functionalities)
    final_obj.data["all_items"] = all_items_including_drafts

    # returning final_obj
    return final_obj

def get_found_items(request):

    user_info = get_user_info_from_jwt_token(request)
    success, parameters = get_needed_parameters(
        ["item_bank_id", "current_language", "keyword", "page", "page_size"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    item_bank_id, current_language, keyword, page, page_size = itemgetter(
        "item_bank_id", "current_language", "keyword", "page", "page_size"
    )(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # getting all item latest versions
    latest_versions_items = ItemLatestVersionsDataVW.objects.filter(
        item_bank_id=item_bank_id
    )

    # ordering items by custom System ID (ascending)
    ordered_items = sorted(
        latest_versions_items,
        # converting second part of string (incrementing value) to int, so the ordering is respected
        key=lambda k: int(k.system_id.split("_")[1]),
        reverse=False,
    )

    # if blank search
    if keyword == " ":
        # getting needed items
        found_items = latest_versions_items
    # regular search
    else:
        keyword = keyword.lower()

        # enabling comma split searches
        keyword_without_comma = keyword.replace(",", "")
        # splitting keyword string
        split_keyword = keyword_without_comma.split()

        # if keyword contains more than one word
        if len(split_keyword) > 1:
            # English Search
            if current_language == "en":
                found_items = ItemLatestVersionsDataVW.objects.filter(
                    reduce(
                        and_,
                        [
                            Q(item_bank_id=item_bank_id)
                            & (
                                Q(system_id__icontains=splitted_keyword)
                                | Q(development_status_name_en__icontains=splitted_keyword)
                                | Q(version__icontains=splitted_keyword)
                                | Q(historical_id__icontains=splitted_keyword)
                            )
                            for splitted_keyword in split_keyword
                        ],
                    )
                )
            else:
                found_items = ItemLatestVersionsDataVW.objects.filter(
                    reduce(
                        and_,
                        [
                            Q(item_bank_id=item_bank_id)
                            & (
                                Q(system_id__icontains=splitted_keyword)
                                | Q(development_status_name_fr__icontains=splitted_keyword)
                                | Q(version__icontains=splitted_keyword)
                                | Q(historical_id__icontains=splitted_keyword)
                            )
                            for splitted_keyword in split_keyword
                        ],
                    )
                )
        else:
            # English Search
            if current_language == "en":
                found_items = ItemLatestVersionsDataVW.objects.filter(
                    Q(item_bank_id=item_bank_id)
                    & (
                        Q(system_id__icontains=keyword)
                        | Q(development_status_name_en__icontains=keyword)
                        | Q(version__icontains=keyword)
                        | Q(historical_id__icontains=keyword)
                    )
                )
            else:
                found_items = ItemLatestVersionsDataVW.objects.filter(
                    Q(item_bank_id=item_bank_id)
                    & (
                        Q(system_id__icontains=keyword)
                        | Q(development_status_name_fr__icontains=keyword)
                        | Q(version__icontains=keyword)
                        | Q(historical_id__icontains=keyword)
                    )
                )

        # no results found
        if not found_items:
            return Response({"no results found"}, status=status.HTTP_200_OK)

    # ordering items by custom System ID (ascending)
    ordered_found_items = sorted(
        found_items,
        # converting second part of string (incrementing value) to int, so the ordering is respected
        key=lambda k: int(k.system_id.split("_")[1]),
        reverse=False,
    )

    # only getting data for current selected page
    new_found_items = ordered_found_items[
        (current_page - 1) * page_size : current_page * page_size
    ]

    # getting serialized item banks data
    serialized_found_ordered_items_data = ItemLatestVersionsDataViewSerializer(
        new_found_items,
        many=True,
    ).data

    # getting final data using pagination library
    final_obj = paginator.get_paginated_response(
        serialized_found_ordered_items_data,
        len(ordered_found_items),
        current_page,
        page_size,
    )

    # initializing needed variables
    all_items_including_drafts = ItemLatestVersionsDataViewSerializer(
        ordered_items, many=True
    ).data
    related_item_drafts = []

    # making sure we have at least one item (to avoid errors)
    if all_items_including_drafts:
        # getting all related item drafts
        related_item_drafts = ItemDraftsVW.objects.filter(
            # using the first element to get the item bank ID
            item_bank_id=all_items_including_drafts[0]["item_bank_id"],
            username_id=user_info["username"],
        )

    # looping in all_items_including_drafts
    for index, item_data in enumerate(all_items_including_drafts):
        # trying to get the index of the current related item draft (if it exists)
        try:
            index_of_current_related_item_draft = [
                x.system_id for x in related_item_drafts
            ].index(item_data["system_id"])
            context = {}
            context["contains_draft"] = True
            # updating the respective object with the item draft data
            all_items_including_drafts[index] = ItemDraftsViewSerializer(
                related_item_drafts[index_of_current_related_item_draft],
                many=False,
                context=context,
            ).data
        except:
            pass

    # adding all_items attribute to the final object variable (useful for frontend functionalities)
    final_obj.data["all_items"] = all_items_including_drafts

    # retuning final_obj
    return final_obj


def get_item_data(request):
    user_info = get_user_info_from_jwt_token(request)

    success, parameters = get_needed_parameters(["item_id"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    item_id = itemgetter("item_id")(parameters)

    optional_parameters = get_optional_parameters(["ignore_draft"], request)

    # getting specified item data
    item_data = AllItemsDataVW.objects.get(item_id=item_id)

    # initializing ignore_draft
    ignore_draft = False

    if optional_parameters["ignore_draft"]:
        ignore_draft = optional_parameters["ignore_draft"]

    try:
        # checking if draft for specified item exists
        draft_data = ItemDraftsVW.objects.filter(
            system_id=item_data.system_id, username_id=user_info["username"]
        )
        # draft exists and ignore draft is set to False (if defined)
        if draft_data and (not ignore_draft or ignore_draft == "false"):
            context = {}
            context["contains_draft"] = True

            serialized_item_data = ItemDraftsViewSerializer(
                draft_data.last(), many=False, context=context
            ).data
            serialized_item_data["can_be_uploaded"] = True
        else:
            serialized_item_data = AllItemsDataViewSerializer(
                item_data,
                many=False,
            ).data

        return Response(serialized_item_data)

    except Items.DoesNotExist:
        return Response(
            {"error": "The provided item ID does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )


def get_selected_item_version_data(request):
    user_info = get_user_info_from_jwt_token(request)
    success, parameters = get_needed_parameters(["item_id", "system_id"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    item_id, system_id = itemgetter("item_id", "system_id")(parameters)

    # selected version is a draft
    if item_id == DRAFT_VERSION:
        # getting draft item data
        draft_item_data = ItemDraftsVW.objects.get(
            username_id=user_info["username"], system_id=system_id
        )

        context = {}
        context["contains_draft"] = True

        serialized_item_data = ItemDraftsViewSerializer(
            draft_item_data, many=False, context=context
        ).data

        return Response(serialized_item_data)

    # selected version is a real version
    else:
        try:
            # getting specified item data
            item_data = AllItemsDataVW.objects.get(item_id=item_id)

            context = {}
            # checking if there is an active draft version
            draft = ItemDraftsVW.objects.filter(
                username_id=user_info["username"], system_id=item_data.system_id
            )
            if draft:
                context["contains_draft"] = True

            serialized_item_data = AllItemsDataViewSerializer(
                item_data, many=False, context=context
            ).data

            return Response(serialized_item_data)
        except Items.DoesNotExist:
            return Response(
                {"error": "The provided item ID does not exist"},
                status=status.HTTP_400_BAD_REQUEST,
            )


def get_response_formats(request):
    response_format_data = ItemResponseFormats.objects.all()
    serialized_data = ResponseFormatsSerializer(response_format_data, many=True).data
    return Response(serialized_data)


def get_development_statuses(request):
    development_status_data = ItemDevelopmentStatuses.objects.all()
    serialized_data = DevelopmentStatusesSerializer(
        development_status_data, many=True
    ).data
    return Response(serialized_data)


def create_new_items(request):
    user_info = get_user_info_from_jwt_token(request)

    success, parameters = get_needed_parameters(
        ["item_bank_id", "response_format", "historical_id"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    item_bank_id, response_format, historical_id = itemgetter(
        "item_bank_id", "response_format", "historical_id"
    )(parameters)

    # getting next item incrementation
    next_item_incrementation = (
        len(
            (
                Items.objects.filter(item_bank_id=item_bank_id)
                .values("system_id")
                .annotate(dcount=Count("system_id"))
                .order_by()
            )
        )
        + 1
    )

    try:
        with transaction.atomic():
            # creating system_id
            system_id = "{0}_{1}".format(item_bank_id, next_item_incrementation)

            # getting development status id
            development_status_id = ItemDevelopmentStatuses.objects.get(
                codename=ItemDevelopmentStatus.DRAFT
            ).id

            # getting response format id
            response_format_id = ItemResponseFormats.objects.get(
                codename=response_format
            ).id

            new_item = Items.objects.create(
                system_id=system_id,
                version=1,
                active_status=False,
                shuffle_options=False,
                development_status_id=development_status_id,
                last_modified_by_username_id=user_info["username"],
                response_format_id=response_format_id,
                item_bank_id=item_bank_id,
            )

            # historical_id is defined
            if historical_id != "":
                try:
                    # creating new historical ID
                    ItemHistoricalIds.objects.create(
                        historical_id=historical_id,
                        system_id=new_item.system_id,
                        item_bank_id=item_bank_id,
                    )
                # error triggered when the item bank already contains the current provided historical ID
                except IntegrityError:
                    return Response(
                        {
                            "error": "The provided historical ID has already been used in this Item Bank"
                        },
                        status=status.HTTP_409_CONFLICT,
                    )

            new_item_data = ItemLatestVersionsDataVW.objects.get(item_id=new_item.id)

            new_item_serialized_data = ItemLatestVersionsDataViewSerializer(
                new_item_data, many=False
            ).data

            new_item_serialized_data["ok"] = True

            return Response(new_item_serialized_data)

    except ItemResponseFormats.DoesNotExist:
        return Response(
            {"error": "The provided response format does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    except:
        return Response(
            {"error": "Something wrong happened during the create new item process"},
            status=status.HTTP_400_BAD_REQUEST,
        )


def update_item_data(request):
    user_info = get_user_info_from_jwt_token(request)

    success, parameters = get_needed_parameters(["section_to_update", "data"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    section_to_update, data = itemgetter("section_to_update", "data")(parameters)

    # handling historical_id data formatting
    if data["historical_id"] is None:
        data["historical_id"] = ""

    # getting item bank language data
    item_bank_language_id = ItemBank.objects.get(id=data["item_bank_id"]).language_id
    item_bank_language_code = None

    if item_bank_language_id is not None:
        item_bank_language_code = Language.objects.get(
            language_id=item_bank_language_id
        ).ISO_Code_1

    # General Section
    if section_to_update == ItemActionType.SAVE_DRAFT:
        # checking if draft data already exists
        draft_data = ItemDrafts.objects.filter(
            system_id=data["system_id"], username_id=user_info["username"]
        )

        # ==================== DRAFT DATA ====================
        # draft data already exists
        if draft_data:
            # updating data
            item_draft = draft_data.last()
            item_draft.item_id = data["item_id"]
            item_draft.system_id = data["system_id"]
            item_draft.username_id = user_info["username"]
            item_draft.item_bank_id = data["item_bank_id"]
            item_draft.historical_id = data["historical_id"]
            item_draft.active_status = False
            item_draft.shuffle_options = data["shuffle_options"]
            item_draft.development_status_id = data["development_status_id"]
            item_draft.response_format_id = data["response_format_id"]
            item_draft.save()
        # draft data does not already exist
        else:
            item_draft = ItemDrafts.objects.create(
                system_id=data["system_id"],
                username_id=user_info["username"],
                item_id=data["item_id"],
                item_bank_id=data["item_bank_id"],
                historical_id=data["historical_id"],
                active_status=False,
                shuffle_options=data["shuffle_options"],
                development_status_id=data["development_status_id"],
                response_format_id=data["response_format_id"],
            )
        # ==================== DRAFT DATA (END) ====================

        # ==================== ITEM ATTRIBUTE VALUE ====================
        # Start by deleting all ItemAttributeValues
        related_item_attribute_values_drafts = ItemAttributeValueDrafts.objects.filter(
            system_id=data["system_id"], username_id=user_info["username"]
        )

        for related_item_attribute_value_drafts in related_item_attribute_values_drafts:
            related_item_attribute_value_drafts.delete()

        # SAVE_DRAFT -> Create new ItemAttributeValuesDrafts
        if data["item_attribute_values"]:
            for item_attribute_value in data["item_attribute_values"]:
                if item_attribute_value["item_bank_attribute_value_id"] != "N/A":
                    ItemAttributeValueDrafts.objects.create(
                        item_bank_attribute_value_id=item_attribute_value[
                            "item_bank_attribute_value_id"
                        ],
                        item_id=data["item_id"],
                        system_id=data["system_id"],
                        username_id=user_info["username"],
                    )

        # ==================== ITEM ATTRIBUTE VALUE (END) ====================

        # ==================== ITEM CONTENT ====================
        # deleting existing data
        related_data = ItemContentDrafts.objects.filter(
            system_id=data["system_id"], username_id=user_info["username"]
        )
        for draft_data in related_data:
            item_content_text_drafts = ItemContentTextDrafts.objects.filter(
                item_content_id=draft_data.id
            )
            for draft in item_content_text_drafts:
                draft.delete()
            draft_data.delete()

        # checking if we have a defined item_content
        if data["item_content"]:
            # bilingual item bank
            if item_bank_language_id is None:
                # looping in languages
                languages = Language.objects.all()
                for language in languages:
                    # checking if item_content contains language of current iteration
                    if data["item_content"].get("{0}".format(language.ISO_Code_1)):
                        content_array = data["item_content"][
                            "{0}".format(language.ISO_Code_1)
                        ]

                        # looping in content_array
                        for content in content_array:
                            # checking if item content draft data has already been created for another language
                            item_content_draft = ItemContentDrafts.objects.filter(
                                system_id=data["system_id"],
                                content_type=content["content_type"],
                                content_order=content["content_order"],
                                item_id=content["item_id"],
                                username_id=user_info["username"],
                            )
                            # item_content_draft does not already exsit
                            if not item_content_draft:
                                # creating needed data
                                item_content_draft = ItemContentDrafts.objects.create(
                                    system_id=data["system_id"],
                                    content_type=content["content_type"],
                                    content_order=content["content_order"],
                                    item_id=content["item_id"],
                                    username_id=user_info["username"],
                                )

                                ItemContentTextDrafts.objects.create(
                                    text=content["text"],
                                    item_content_id=item_content_draft.id,
                                    language_id=content["language_id"],
                                )
                            # item_content_draft already exsits
                            else:
                                # only creating new text data
                                ItemContentTextDrafts.objects.create(
                                    text=content["text"],
                                    item_content_id=item_content_draft.first().id,
                                    language_id=content["language_id"],
                                )
            # unilangual item bank
            else:
                # getting content array data in item bank defined language
                content_array = data["item_content"][
                    "{0}".format(item_bank_language_code)
                ]

                # looping in content_array
                for content in content_array:
                    # creating needed data
                    item_content_draft = ItemContentDrafts.objects.create(
                        system_id=data["system_id"],
                        content_type=content["content_type"],
                        content_order=content["content_order"],
                        item_id=content["item_id"],
                        username_id=user_info["username"],
                    )

                    ItemContentTextDrafts.objects.create(
                        text=content["text"],
                        item_content_id=item_content_draft.id,
                        language_id=content["language_id"],
                    )
        # ==================== ITEM CONTENT (END) ====================

        # ==================== ITEM OPTIONS ====================
        # deleting existing data
        related_data = ItemOptionDrafts.objects.filter(
            system_id=data["system_id"], username_id=user_info["username"]
        )
        for draft_data in related_data:
            item_option_text_drafts = ItemOptionTextDrafts.objects.filter(
                item_option_id=draft_data.id
            )
            for draft in item_option_text_drafts:
                draft.delete()
            draft_data.delete()

        # checking if we have a defined item_content
        if data["item_options"]:
            # bilingual item bank
            if item_bank_language_id is None:
                # looping in languages
                languages = Language.objects.all()
                for language in languages:
                    # checking if item_content contains language of current iteration
                    if data["item_options"].get("{0}".format(language.ISO_Code_1)):
                        content_array = data["item_options"][
                            "{0}".format(language.ISO_Code_1)
                        ]

                        # looping in content_array
                        for content in content_array:
                            # checking if item content draft data has already been created for another language
                            item_option_draft = ItemOptionDrafts.objects.filter(
                                system_id=data["system_id"],
                                option_type=content["option_type"],
                                option_order=content["option_order"],
                                item_id=content["item_id"],
                                username_id=user_info["username"],
                            )
                            # item_option_draft does not already exsit
                            if not item_option_draft:
                                # creating needed data
                                item_option_draft = ItemOptionDrafts.objects.create(
                                    system_id=data["system_id"],
                                    option_type=content["option_type"],
                                    option_order=content["option_order"],
                                    exclude_from_shuffle=content[
                                        "exclude_from_shuffle"
                                    ],
                                    historical_option_id=content[
                                        "historical_option_id"
                                    ],
                                    rationale=content["rationale"],
                                    item_id=content["item_id"],
                                    score=content["score"],
                                    username_id=user_info["username"],
                                )

                                ItemOptionTextDrafts.objects.create(
                                    text=content["text"],
                                    item_option_id=item_option_draft.id,
                                    language_id=content["language_id"],
                                )
                            # item_option_draft already exsits
                            else:
                                # only creating new text data
                                ItemOptionTextDrafts.objects.create(
                                    text=content["text"],
                                    item_option_id=item_option_draft.first().id,
                                    language_id=content["language_id"],
                                )
            # unilangual item bank
            else:
                # getting content array data in item bank defined language
                content_array = data["item_options"][
                    "{0}".format(item_bank_language_code)
                ]

                # looping in content_array
                for content in content_array:
                    # creating needed data
                    item_option_draft = ItemOptionDrafts.objects.create(
                        system_id=data["system_id"],
                        option_type=content["option_type"],
                        option_order=content["option_order"],
                        exclude_from_shuffle=content["exclude_from_shuffle"],
                        historical_option_id=content["historical_option_id"],
                        rationale=content["rationale"],
                        item_id=content["item_id"],
                        score=content["score"],
                        username_id=user_info["username"],
                    )

                    ItemOptionTextDrafts.objects.create(
                        text=content["text"],
                        item_option_id=item_option_draft.id,
                        language_id=content["language_id"],
                    )
        # ==================== ITEM OPTIONS (END) ====================

        updated_data = ItemDraftsVW.objects.get(
            system_id=data["system_id"], username_id=user_info["username"]
        )

        context = {}
        context["contains_draft"] = True

        serialized_updated_data = ItemDraftsViewSerializer(
            updated_data, many=False, context=context
        ).data
        serialized_updated_data["ok"] = True
        serialized_updated_data["can_be_uploaded"] = True
        return Response(serialized_updated_data)

    # Upload Item
    elif section_to_update == ItemActionType.UPLOAD:
        # getting draft data
        draft_data = ItemDrafts.objects.filter(
            system_id=data["system_id"], username_id=user_info["username"]
        )
        # draft data exists
        if draft_data:
            # getting historical ID data
            historical_id_data = ItemHistoricalIds.objects.filter(
                system_id=data["system_id"], item_bank_id=data["item_bank_id"]
            )
            # historical ID data exsits
            if historical_id_data:
                # provided historical ID is empty
                if data["historical_id"] == "":
                    # deleting DB entries
                    for historical_id in historical_id_data:
                        historical_id.delete()
                else:
                    # checking if provided historical ID already exists in this current Item Bank
                    # provided historical ID does not exist
                    if not ItemHistoricalIds.objects.filter(
                        historical_id=data["historical_id"],
                        item_bank_id=data["item_bank_id"],
                    ).exclude(system_id=data["system_id"]):
                        # updating data
                        historical_data_to_update = historical_id_data.last()
                        historical_data_to_update.historical_id = data["historical_id"]
                        historical_data_to_update.save()
                    # provided historical ID already exists in this current Item Bank
                    else:
                        return Response(
                            {
                                "error": "The provided historical ID has already been used in this Item Bank"
                            },
                            status=status.HTTP_409_CONFLICT,
                        )

            # historical ID data does not exsit
            else:
                # if historical ID is defined (not NULL or empty)
                if data["historical_id"] != "":
                    # checking if provided historical ID already exists in this current Item Bank
                    # provided historical ID does not exist
                    if not ItemHistoricalIds.objects.filter(
                        historical_id=data["historical_id"]
                    ).exclude(system_id=data["system_id"]):
                        # creating new entry in DB
                        ItemHistoricalIds.objects.create(
                            historical_id=data["historical_id"],
                            item_bank_id=data["item_bank_id"],
                            system_id=data["system_id"],
                        )
                    # provided historical ID already exists in this current Item Bank
                    else:
                        return Response(
                            {
                                "error": "The provided historical ID has already been used in this Item Bank"
                            },
                            status=status.HTTP_409_CONFLICT,
                        )

            # getting next item version
            next_item_version = (
                Items.objects.filter(system_id=data["system_id"]).last().version + 1
            )
            # ==================== CREATING ITEM DATA ====================
            new_item = Items.objects.create(
                system_id=data["system_id"],
                version=next_item_version,
                active_status=False,
                shuffle_options=data["shuffle_options"],
                development_status_id=data["development_status_id"],
                last_modified_by_username_id=user_info["username"],
                response_format_id=data["response_format_id"],
                item_bank_id=data["item_bank_id"],
            )
            # ==================== CREATING ITEM DATA (END) ====================

            # ==================== CREATING ITEM ATTRIBUTE VALUE ====================
            if data["item_attribute_values"]:
                # UPLOAD -> Create new ItemAttributeValue
                for item_attribute_value in data["item_attribute_values"]:
                    if item_attribute_value["item_bank_attribute_value_id"] != "N/A":
                        ItemAttributeValue.objects.create(
                            item_bank_attribute_value_id=item_attribute_value[
                                "item_bank_attribute_value_id"
                            ],
                            item_id=new_item.id,
                        )
            # ==================== CREATING ITEM ATTRIBUTE VALUE (END) ====================

            # ==================== CREATING ITEM CONTENT ====================
            # checking if we have a defined item_content
            if data["item_content"]:
                # bilingual item bank
                if item_bank_language_id is None:
                    # looping in languages
                    languages = Language.objects.all()
                    for language in languages:
                        # checking if item_content contains language of current iteration
                        if data["item_content"].get("{0}".format(language.ISO_Code_1)):
                            content_array = data["item_content"][
                                "{0}".format(language.ISO_Code_1)
                            ]

                            # looping in content_array
                            for content in content_array:
                                # checking if item content draft data has already been created for another language
                                item_content = ItemContent.objects.filter(
                                    content_type=content["content_type"],
                                    content_order=content["content_order"],
                                    item_id=new_item.id,
                                )
                                # item_content does not already exsit
                                if not item_content:
                                    # creating needed data
                                    item_content = ItemContent.objects.create(
                                        content_type=content["content_type"],
                                        content_order=content["content_order"],
                                        item_id=new_item.id,
                                    )

                                    ItemContentText.objects.create(
                                        text=content["text"],
                                        item_content_id=item_content.id,
                                        language_id=content["language_id"],
                                    )
                                # item_content already exsits
                                else:
                                    # only creating new text data
                                    ItemContentText.objects.create(
                                        text=content["text"],
                                        item_content_id=item_content.first().id,
                                        language_id=content["language_id"],
                                    )
                # unilangual item bank
                else:
                    # getting content array data in item bank defined language
                    content_array = data["item_content"][
                        "{0}".format(item_bank_language_code)
                    ]

                    # looping in content_array
                    for content in content_array:
                        # creating needed data
                        item_content = ItemContent.objects.create(
                            content_type=content["content_type"],
                            content_order=content["content_order"],
                            item_id=new_item.id,
                        )

                        ItemContentText.objects.create(
                            text=content["text"],
                            item_content_id=item_content.id,
                            language_id=content["language_id"],
                        )
            # ==================== CREATING ITEM CONTENT (END) ====================

            # ==================== CREATING ITEM OPTIONS ====================
            # checking if we have a defined item_content
            if data["item_options"]:
                # bilingual item bank
                if item_bank_language_id is None:
                    # looping in languages
                    languages = Language.objects.all()
                    for language in languages:
                        # checking if item_content contains language of current iteration
                        if data["item_options"].get("{0}".format(language.ISO_Code_1)):
                            content_array = data["item_options"][
                                "{0}".format(language.ISO_Code_1)
                            ]

                            # looping in content_array
                            for content in content_array:
                                # checking if item content draft data has already been created for another language
                                item_option = ItemOption.objects.filter(
                                    option_type=content["option_type"],
                                    option_order=content["option_order"],
                                    item_id=new_item.id,
                                )
                                # item_option does not already exsit
                                if not item_option:
                                    # initializing exclude_from_shuffle
                                    exclude_from_shuffle = content[
                                        "exclude_from_shuffle"
                                    ]
                                    # shuffle_options is disabled
                                    if not data["shuffle_options"]:
                                        # setting value as False
                                        exclude_from_shuffle = False

                                    # creating needed data
                                    item_option = ItemOption.objects.create(
                                        option_type=content["option_type"],
                                        option_order=content["option_order"],
                                        exclude_from_shuffle=exclude_from_shuffle,
                                        historical_option_id=content[
                                            "historical_option_id"
                                        ],
                                        rationale=content["rationale"],
                                        item_id=new_item.id,
                                        score=content["score"],
                                    )

                                    ItemOptionText.objects.create(
                                        text=content["text"],
                                        item_option_id=item_option.id,
                                        language_id=content["language_id"],
                                    )
                                # item_option already exsits
                                else:
                                    # only creating new text data
                                    ItemOptionText.objects.create(
                                        text=content["text"],
                                        item_option_id=item_option.first().id,
                                        language_id=content["language_id"],
                                    )
                # unilangual item bank
                else:
                    # getting content array data in item bank defined language
                    content_array = data["item_options"][
                        "{0}".format(item_bank_language_code)
                    ]

                    # looping in content_array
                    for content in content_array:
                        # initializing exclude_from_shuffle
                        exclude_from_shuffle = content["exclude_from_shuffle"]
                        # shuffle_options is disabled
                        if not data["shuffle_options"]:
                            # setting value as False
                            exclude_from_shuffle = False

                        # creating needed data
                        item_option = ItemOption.objects.create(
                            option_type=content["option_type"],
                            option_order=content["option_order"],
                            exclude_from_shuffle=exclude_from_shuffle,
                            historical_option_id=content["historical_option_id"],
                            rationale=content["rationale"],
                            item_id=new_item.id,
                            score=content["score"],
                        )

                        ItemOptionText.objects.create(
                            text=content["text"],
                            item_option_id=item_option.id,
                            language_id=content["language_id"],
                        )
            # ==================== CREATING ITEM OPTIONS (END) ====================

            # ==================== DELETING DRAFT DATA ====================
            # draft data
            for draft in draft_data:
                draft.delete()

            # item content draft
            related_data = ItemContentDrafts.objects.filter(
                system_id=data["system_id"], username_id=user_info["username"]
            )
            for draft_data in related_data:
                item_content_text_drafts = ItemContentTextDrafts.objects.filter(
                    item_content_id=draft_data.id
                )
                for draft in item_content_text_drafts:
                    draft.delete()

                draft_data.delete()

            # item options draft
            related_data = ItemOptionDrafts.objects.filter(
                system_id=data["system_id"], username_id=user_info["username"]
            )
            for draft_data in related_data:
                item_option_text_drafts = ItemOptionTextDrafts.objects.filter(
                    item_option_id=draft_data.id
                )
                for draft in item_option_text_drafts:
                    draft.delete()
                draft_data.delete()

            # item attribute values
            # Deleting ItemAttributeValuesDrafts
            item_attribute_values_drafts = ItemAttributeValueDrafts.objects.filter(
                system_id=data["system_id"], username_id=user_info["username"]
            )
            for item_attribute_value_draft in item_attribute_values_drafts:
                item_attribute_value_draft.delete()

            # ==================== DELETING DRAFT DATA (END) ====================

            # getting new item data
            new_item_data = ItemLatestVersionsDataVW.objects.get(item_id=new_item.id)

            # returning serialized data
            updated_data = ItemLatestVersionsDataViewSerializer(
                new_item_data, many=False
            ).data
            updated_data["ok"] = True
            updated_data["can_be_uploaded"] = False
            return Response(updated_data)
        # draft data does not exist (should not happen)
        else:
            return Response(
                {"info": "There is no data to upload"},
                status=status.HTTP_304_NOT_MODIFIED,
            )
    # Upload Item (overwrite version)
    elif section_to_update == ItemActionType.UPLOAD_OVERWRITE:
        # getting draft data
        draft_data = ItemDrafts.objects.filter(
            system_id=data["system_id"], username_id=user_info["username"]
        )
        # draft data exists
        if draft_data:
            # getting historical ID data
            historical_id_data = ItemHistoricalIds.objects.filter(
                system_id=data["system_id"], item_bank_id=data["item_bank_id"]
            )
            # historical ID data exsits
            if historical_id_data:
                # provided historical ID is empty
                if data["historical_id"] == "":
                    # deleting DB entries
                    for historical_id in historical_id_data:
                        historical_id.delete()
                else:
                    # checking if provided historical ID already exists in this current Item Bank
                    # provided historical ID does not exist
                    if not ItemHistoricalIds.objects.filter(
                        historical_id=data["historical_id"],
                        item_bank_id=data["item_bank_id"],
                    ).exclude(system_id=data["system_id"]):
                        # updating data
                        historical_data_to_update = historical_id_data.last()
                        historical_data_to_update.historical_id = data["historical_id"]
                        historical_data_to_update.save()
                    # provided historical ID already exists in this current Item Bank
                    else:
                        return Response(
                            {
                                "error": "The provided historical ID has already been used in this Item Bank"
                            },
                            status=status.HTTP_409_CONFLICT,
                        )

            # historical ID data does not exsit
            else:
                # if historical ID is defined (not NULL or empty)
                if data["historical_id"] != "":
                    # checking if provided historical ID already exists in this current Item Bank
                    # provided historical ID does not exist
                    if not ItemHistoricalIds.objects.filter(
                        historical_id=data["historical_id"]
                    ).exclude(system_id=data["system_id"]):
                        # creating new entry in DB
                        ItemHistoricalIds.objects.create(
                            historical_id=data["historical_id"],
                            item_bank_id=data["item_bank_id"],
                            system_id=data["system_id"],
                        )
                    # provided historical ID already exists in this current Item Bank
                    else:
                        return Response(
                            {
                                "error": "The provided historical ID has already been used in this Item Bank"
                            },
                            status=status.HTTP_409_CONFLICT,
                        )

            # getting respective item data
            respective_item_data = Items.objects.get(id=data["item_id"])
            # initializing respective_item_active_status
            respective_item_active_status = respective_item_data.active_status
            # getting developement_status codename
            development_status_codename = ItemDevelopmentStatuses.objects.get(
                id=data["development_status_id"]
            ).codename
            # development_status_codename is ARCHIVED or DRAFT
            if (
                development_status_codename == ItemDevelopmentStatus.RETIRED
                or development_status_codename == ItemDevelopmentStatus.DRAFT
            ):
                # item with ARCHIVED or DRAFT development status cannot be activated
                respective_item_active_status = False
            # ==================== UPDATING ITEM DATA ====================
            respective_item_data.system_id = data["system_id"]
            respective_item_data.active_status = respective_item_active_status
            respective_item_data.shuffle_options = data["shuffle_options"]
            respective_item_data.development_status_id = data["development_status_id"]
            respective_item_data.last_modified_by_username_id = user_info["username"]
            respective_item_data.response_format_id = data["response_format_id"]
            respective_item_data.item_bank_id = data["item_bank_id"]
            respective_item_data.save()
            # ==================== UPDATING ITEM DATA (END) ====================

            # ==================== DELETING EXISTING ITEM CONTENT ====================
            related_content_data = ItemContent.objects.filter(item_id=data["item_id"])
            for content_data in related_content_data:
                item_content_text = ItemContentText.objects.filter(
                    item_content_id=content_data.id
                )
                for content_text_data in item_content_text:
                    content_text_data.delete()

                content_data.delete()
            # ==================== DELETING EXISTING ITEM CONTENT (END) ====================

            # ==================== CREATING ITEM CONTENT ====================
            # checking if we have a defined item_content
            if data["item_content"]:
                # bilingual item bank
                if item_bank_language_id is None:
                    # looping in languages
                    languages = Language.objects.all()
                    for language in languages:
                        # checking if item_content contains language of current iteration
                        if data["item_content"].get("{0}".format(language.ISO_Code_1)):
                            content_array = data["item_content"][
                                "{0}".format(language.ISO_Code_1)
                            ]

                            # looping in content_array
                            for content in content_array:
                                # checking if item content draft data has already been created for another language
                                item_content = ItemContent.objects.filter(
                                    content_type=content["content_type"],
                                    content_order=content["content_order"],
                                    item_id=content["item_id"],
                                )
                                # item_content does not already exsit
                                if not item_content:
                                    # creating needed data
                                    item_content = ItemContent.objects.create(
                                        content_type=content["content_type"],
                                        content_order=content["content_order"],
                                        item_id=data["item_id"],
                                    )

                                    ItemContentText.objects.create(
                                        text=content["text"],
                                        item_content_id=item_content.id,
                                        language_id=content["language_id"],
                                    )
                                # item_content already exsits
                                else:
                                    # only creating new text data
                                    ItemContentText.objects.create(
                                        text=content["text"],
                                        item_content_id=item_content.first().id,
                                        language_id=content["language_id"],
                                    )
                # unilangual item bank
                else:
                    # getting content array data in item bank defined language
                    content_array = data["item_content"][
                        "{0}".format(item_bank_language_code)
                    ]

                    # looping in content_array
                    for content in content_array:
                        # creating needed data
                        item_content = ItemContent.objects.create(
                            content_type=content["content_type"],
                            content_order=content["content_order"],
                            item_id=data["item_id"],
                        )

                        ItemContentText.objects.create(
                            text=content["text"],
                            item_content_id=item_content.id,
                            language_id=content["language_id"],
                        )
            # ==================== CREATING ITEM CONTENT (END) ====================

            # ==================== DELETING EXISTING ITEM OPTIONS ====================
            related_option_data = ItemOption.objects.filter(item_id=data["item_id"])
            for option_data in related_option_data:
                item_option_text = ItemOptionText.objects.filter(
                    item_option_id=option_data.id
                )
                for option_text_data in item_option_text:
                    option_text_data.delete()

                option_data.delete()
            # ==================== DELETING EXISTING ITEM OPTIONS (END) ====================

            # ==================== CREATING ITEM OPTIONS ====================
            # checking if we have a defined item_content
            if data["item_options"]:
                # bilingual item bank
                if item_bank_language_id is None:
                    # looping in languages
                    languages = Language.objects.all()
                    for language in languages:
                        # checking if item_content contains language of current iteration
                        if data["item_options"].get("{0}".format(language.ISO_Code_1)):
                            content_array = data["item_options"][
                                "{0}".format(language.ISO_Code_1)
                            ]

                            # looping in content_array
                            for content in content_array:
                                # checking if item content draft data has already been created for another language
                                item_option = ItemOption.objects.filter(
                                    option_type=content["option_type"],
                                    option_order=content["option_order"],
                                    item_id=content["item_id"],
                                )
                                # item_option does not already exsit
                                if not item_option:
                                    # initializing exclude_from_shuffle
                                    exclude_from_shuffle = content[
                                        "exclude_from_shuffle"
                                    ]
                                    # shuffle_options is disabled
                                    if not data["shuffle_options"]:
                                        # setting value as False
                                        exclude_from_shuffle = False

                                    # creating needed data
                                    item_option = ItemOption.objects.create(
                                        option_type=content["option_type"],
                                        option_order=content["option_order"],
                                        exclude_from_shuffle=exclude_from_shuffle,
                                        historical_option_id=content[
                                            "historical_option_id"
                                        ],
                                        rationale=content["rationale"],
                                        item_id=data["item_id"],
                                        score=content["score"],
                                    )

                                    ItemOptionText.objects.create(
                                        text=content["text"],
                                        item_option_id=item_option.id,
                                        language_id=content["language_id"],
                                    )
                                # item_option already exsits
                                else:
                                    # only creating new text data
                                    ItemOptionText.objects.create(
                                        text=content["text"],
                                        item_option_id=item_option.first().id,
                                        language_id=content["language_id"],
                                    )
                # unilangual item bank
                else:
                    # getting content array data in item bank defined language
                    content_array = data["item_options"][
                        "{0}".format(item_bank_language_code)
                    ]

                    # looping in content_array
                    for content in content_array:
                        # initializing exclude_from_shuffle
                        exclude_from_shuffle = content["exclude_from_shuffle"]
                        # shuffle_options is disabled
                        if not data["shuffle_options"]:
                            # setting value as False
                            exclude_from_shuffle = False

                        # creating needed data
                        item_option = ItemOption.objects.create(
                            option_type=content["option_type"],
                            option_order=content["option_order"],
                            exclude_from_shuffle=exclude_from_shuffle,
                            historical_option_id=content["historical_option_id"],
                            rationale=content["rationale"],
                            item_id=data["item_id"],
                            score=content["score"],
                        )

                        ItemOptionText.objects.create(
                            text=content["text"],
                            item_option_id=item_option.id,
                            language_id=content["language_id"],
                        )
            # ==================== CREATING ITEM OPTIONS (END) ====================

            # ==================== DELETING EXISTING ITEM ATTRIBUTE VALUES ====================

            # Deleting ItemAttributeValue
            item_attribute_values = ItemAttributeValue.objects.filter(
                item_id=data["item_id"]
            )
            for item_attribute_value in item_attribute_values:
                item_attribute_value.delete()

            # ==================== DELETING EXISTING ITEM ATTRIBUTE VALUES (END) ====================

            # ==================== CREATING ITEM ATTRIBUTE VALUES ====================

            if data["item_attribute_values"]:
                # UPLOAD_OVERWRITE -> Create new ItemAttributeValue
                for item_attribute_value in data["item_attribute_values"]:
                    if item_attribute_value["item_bank_attribute_value_id"] != "N/A":
                        ItemAttributeValue.objects.create(
                            item_bank_attribute_value_id=item_attribute_value[
                                "item_bank_attribute_value_id"
                            ],
                            item_id=data["item_id"],
                        )

            # ==================== CREATING ITEM ATTRIBUTE VALUES (END) ====================

            # ==================== DELETING DRAFT DATA ====================
            # draft data
            for draft in draft_data:
                draft.delete()

            # item content draft
            related_data = ItemContentDrafts.objects.filter(
                system_id=data["system_id"], username_id=user_info["username"]
            )
            for draft_data in related_data:
                item_content_text_drafts = ItemContentTextDrafts.objects.filter(
                    item_content_id=draft_data.id
                )
                for draft in item_content_text_drafts:
                    draft.delete()

                draft_data.delete()

            # item options draft
            related_data = ItemOptionDrafts.objects.filter(
                system_id=data["system_id"], username_id=user_info["username"]
            )
            for draft_data in related_data:
                item_option_text_drafts = ItemOptionTextDrafts.objects.filter(
                    item_option_id=draft_data.id
                )
                for draft in item_option_text_drafts:
                    draft.delete()
                draft_data.delete()
            # ==================== DELETING DRAFT DATA (END) ====================

            # getting new item data
            new_item_data = AllItemsDataVW.objects.get(item_id=data["item_id"])

            # returning serialized data
            updated_data = AllItemsDataViewSerializer(new_item_data, many=False).data
            updated_data["ok"] = True
            updated_data["can_be_uploaded"] = False
            return Response(updated_data)
        # draft data does not exist (should not happen)
        else:
            return Response(
                {"info": "There is no data to upload"},
                status=status.HTTP_304_NOT_MODIFIED,
            )
    # Activate/Deactivate Item
    elif section_to_update == ItemActionType.ACTIVATE_DEACTIVATE:
        # getting respective item data
        item_data = Items.objects.get(id=data["item_id"])
        # updating active state
        item_data.active_status = data["active_status"]
        item_data.save()
        return Response(
            {"info": "Action Successful"},
            status=status.HTTP_200_OK,
        )
    else:
        return Response(
            {"error": "The specified section to update does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )


def create_item_comment(request):
    user_info = get_user_info_from_jwt_token(request)

    success, parameters = get_needed_parameters(
        ["system_id", "comment", "version"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    system_id, comment, version = itemgetter("system_id", "comment", "version")(
        parameters
    )

    # version undefined ==> adding comments when in draft mode
    if version == " ":
        # getting latest version
        version = (
            Items.objects.filter(system_id=system_id).order_by("version").last().version
        )

    try:
        # creating new comment
        ItemComments.objects.create(
            comment=comment,
            version=version,
            system_id=system_id,
            username_id=user_info["username"],
        )

        # getting existing comments
        existing_comments = ItemComments.objects.filter(system_id=system_id).order_by(
            "-modify_date"
        )
        serialized_data = ItemCommentsSerializer(existing_comments, many=True).data

        data = {"item_comments": serialized_data, "ok": True}

        return Response(data)

    except:
        return Response(
            {
                "error": "Something wrong happened during the create item comment process"
            },
            status=status.HTTP_400_BAD_REQUEST,
        )


def delete_item_draft(request):
    user_info = get_user_info_from_jwt_token(request)

    success, parameters = get_needed_parameters(["system_id"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    system_id = itemgetter("system_id")(parameters)

    try:
        # ==================== DRAFT DATA ====================
        # getting draft data
        draft_data = ItemDrafts.objects.get(
            system_id=system_id, username_id=user_info["username"]
        )
        # deleting draft_data
        draft_data.delete()
        # ==================== DRAFT DATA (END) ====================

        # ==================== ITEM CONTENT DRAFT DATA ====================
        # getting item content draft data
        item_content_draft_data = ItemContentDrafts.objects.filter(
            system_id=system_id, username_id=user_info["username"]
        )
        # deleting related item content and item content text data
        for content_draft_data in item_content_draft_data:
            item_content_text_draft_data = ItemContentTextDrafts.objects.filter(
                item_content_id=content_draft_data.id
            )
            for content_text_draft_data in item_content_text_draft_data:
                content_text_draft_data.delete()
            content_draft_data.delete()
        # ==================== ITEM CONTENT DRAFT DATA (END) ====================

        # ==================== ITEM OPTIONS DRAFT DATA ====================
        # getting item option draft data
        item_option_draft_data = ItemOptionDrafts.objects.filter(
            system_id=system_id, username_id=user_info["username"]
        )
        # deleting related item content and item content text data
        for option_draft_data in item_option_draft_data:
            item_option_text_draft_data = ItemOptionTextDrafts.objects.filter(
                item_option_id=option_draft_data.id
            )
            for option_text_draft_data in item_option_text_draft_data:
                option_text_draft_data.delete()
            option_draft_data.delete()
        # ==================== ITEM OPTIONS DRAFT DATA (END) ====================
    except ItemDrafts.DoesNotExist:
        Response(
            {"info": "There is no draft data to delete"}, status=status.HTTP_200_OK
        )

    return Response(None)


def delete_item_comment(request):
    success, parameters = get_needed_parameters(["system_id", "comment_id"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    system_id, comment_id = itemgetter("system_id", "comment_id")(parameters)

    try:
        # getting specified comment
        comment = ItemComments.objects.get(id=comment_id)
        # deleting comment
        comment.delete()

        # getting existing comments
        existing_comments = ItemComments.objects.filter(system_id=system_id).order_by(
            "-modify_date"
        )
        serialized_data = ItemCommentsSerializer(existing_comments, many=True).data

        data = {"item_comments": serialized_data, "ok": True}

        return Response(data)

    except ItemComments.DoesNotExist:
        return Response(
            {"error": "The specified comment does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )


def get_all_bundles(request):
    success, parameters = get_needed_parameters(
        ["item_bank_id", "page", "page_size"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    item_bank_id, page, page_size = itemgetter("item_bank_id", "page", "page_size")(
        parameters
    )

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # getting all bundles
    all_bundles = BundleDataVW.objects.filter(item_bank_id=item_bank_id)

    # ordering items by custom System ID (ascending)
    ordered_bundles = sorted(
        all_bundles,
        # converting second part of string (incrementing value) to int, so the ordering is respected
        key=lambda k: k.name,
        reverse=False,
    )

    # only getting data for current selected page
    new_all_bundles = ordered_bundles[
        (current_page - 1) * page_size : current_page * page_size
    ]

    # getting serialized item banks data
    serialized_ordered_bundles_data = BundleDataViewSerializer(
        new_all_bundles,
        many=True,
    ).data

    # getting list of bundle id/names
    list_of_bundles = []

    for bundle in all_bundles:
        list_of_bundles.append({"id": bundle.id, "bundle_name": bundle.name})

    # ordering list_of_bundles by bundle name (ascending)
    list_of_bundles = sorted(
        list_of_bundles,
        # converting second part of string (incrementing value) to int, so the ordering is respected
        key=lambda k: k["bundle_name"],
        reverse=False,
    )

    final_object = paginator.get_paginated_response(
        serialized_ordered_bundles_data,
        len(ordered_bundles),
        current_page,
        page_size,
    )

    final_object.data["list_of_bundles"] = list_of_bundles

    # getting final data using pagination library
    return final_object


def get_found_bundles(request):
    success, parameters = get_needed_parameters(
        ["item_bank_id", "keyword", "page", "page_size"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    item_bank_id, keyword, page, page_size = itemgetter(
        "item_bank_id", "keyword", "page", "page_size"
    )(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # getting needed items
    all_bundles = BundleDataVW.objects.filter(item_bank_id=item_bank_id)

    # if blank search
    if keyword == " ":
        # getting needed items
        found_bundles = all_bundles
    # regular search
    else:
        keyword = keyword.lower()
        keyword = parse.unquote(keyword)

        found_bundles = BundleDataVW.objects.filter(
            Q(item_bank_id=item_bank_id)
            & (Q(name__icontains=keyword) | Q(version_text__icontains=keyword))
        )

        # ==================== ACTIVE STATUS SEARCH ====================
        # keyword contains wording matching with "active" wording
        if (
            keyword == TextResources.commons["active"]["en"].lower()
            or keyword == TextResources.commons["active"]["fr"].lower()
        ):
            # combining results found
            matching_active_bundles = BundleDataVW.objects.filter(
                item_bank_id=item_bank_id, active=1
            )
            found_bundles = list(
                set(list(found_bundles) + list(matching_active_bundles))
            )
        # ==================== ACTIVE STATUS SEARCH (END) ====================

        # ==================== INACTIVE STATUS SEARCH ====================
        # keyword contains wording matching with "inactive" wording
        if (
            keyword == TextResources.commons["inactive"]["en"].lower()
            or keyword == TextResources.commons["inactive"]["fr"].lower()
        ):
            # combining results found
            matching_inactive_bundles = BundleDataVW.objects.filter(
                item_bank_id=item_bank_id, active=0
            )
            found_bundles = list(
                set(list(found_bundles) + list(matching_inactive_bundles))
            )
        # ==================== INACTIVE STATUS SEARCH (END) ====================

        # no results found
        if not found_bundles:
            return Response({"no results found"}, status=status.HTTP_200_OK)

    # ordering items by custom System ID (ascending)
    ordered_found_bundles = sorted(
        found_bundles,
        # converting second part of string (incrementing value) to int, so the ordering is respected
        key=lambda k: k.name,
        reverse=False,
    )

    # only getting data for current selected page
    new_found_bundles = ordered_found_bundles[
        (current_page - 1) * page_size : current_page * page_size
    ]

    # getting serialized item banks data
    serialized_ordered_found_bundles_data = BundleDataViewSerializer(
        new_found_bundles,
        many=True,
    ).data

    # getting list of bundle id/names
    list_of_bundles = []

    for bundle in all_bundles:
        list_of_bundles.append({"id": bundle.id, "bundle_name": bundle.name})

    # ordering list_of_bundles by bundle name (ascending)
    list_of_bundles = sorted(
        list_of_bundles,
        # converting second part of string (incrementing value) to int, so the ordering is respected
        key=lambda k: k["bundle_name"],
        reverse=False,
    )

    final_object = paginator.get_paginated_response(
        serialized_ordered_found_bundles_data,
        len(ordered_found_bundles),
        current_page,
        page_size,
    )

    final_object.data["list_of_bundles"] = list_of_bundles

    # getting final data using pagination library
    return final_object


def get_specific_bundle(request):
    success, parameters = get_needed_parameters(["bundle_id"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    bundle_id = itemgetter("bundle_id")(parameters)

    try:
        # getting bundle
        specific_bundle = BundleDataVW.objects.get(id=bundle_id)

        serialized_specific_bundle = BundleDetailedDataViewSerializer(
            specific_bundle, many=False
        ).data

        # successful response
        return Response(serialized_specific_bundle)

    except Bundles.DoesNotExist:
        return Response(
            {"error": "The provided bundle id was not found"},
            status=status.HTTP_404_NOT_FOUND,
        )


def create_new_bundle(request):
    success, parameters = get_needed_parameters(
        ["item_bank_id", "name", "version_text"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    item_bank_id, name, version_text = itemgetter(
        "item_bank_id", "name", "version_text"
    )(parameters)

    # checking if name has already been used in this item bank
    name_already_used = Bundles.objects.filter(item_bank_id=item_bank_id, name=name)

    # name not already used
    if not name_already_used:
        new_bundle = Bundles.objects.create(
            name=name,
            version_text=version_text,
            shuffle_items=0,
            shuffle_bundles=0,
            active=0,
            item_bank_id=item_bank_id,
        )

        new_bundle_data = BundleDataVW.objects.get(id=new_bundle.id)

        serialized_data = BundleDetailedDataViewSerializer(
            new_bundle_data, many=False
        ).data

        serialized_data["ok"] = True

        return Response(serialized_data)
    # name already used
    else:
        return Response(
            {
                "error": "The provided bundle name has already been used in this Item Bank"
            },
            status=status.HTTP_409_CONFLICT,
        )


def handle_bundle_bundles_associations_data(
    bundle_data,
    bundle_provided_data,
    filtered_bundle_bundles_associations,
    filtered_bundle_link_ids,
    has_rule,
    ignore_all_validations,
):
    # deleting all existing bundle bundles associations
    bundle_bundles_associations_to_delete = BundleBundlesAssociation.objects.filter(
        bundle_source_id=bundle_data.id
    )
    for link in bundle_bundles_associations_to_delete:
        link.delete()

    # current bundle does not contain any rule
    if not has_rule:
        # looping in bundle_associations
        for index, bundle_bundles_association_data in enumerate(
            filtered_bundle_bundles_associations
        ):
            # creating bundle bundles association
            BundleBundlesAssociation.objects.create(
                bundle_source_id=bundle_bundles_association_data["bundle_source_id"],
                bundle_link_id=bundle_bundles_association_data["bundle_link_id"],
                display_order=index + 1,
            )

            # ==================== BUNDLE LINKS VALIDATION ====================
            # if not ignoring validations
            if not ignore_all_validations:
                # getting excluded bundle IDs (bundle that should be excluded based on the current selected bundle ID)
                excluded_bundle_ids = recursive_bundle_links_check(
                    bundle_provided_data["item_bank_id"],
                    int(bundle_provided_data["id"]),
                )["excluded_bundle_ids"]

                # getting all elements passed the index + 1 of filtered_bundle_link_ids array
                bundle_ids_passed_index = filtered_bundle_link_ids[index + 1 :]

                # any elements from bundle_ids_passed_index exists in excluded_bundle_ids
                for bundle_id_passed_index in bundle_ids_passed_index:
                    if bundle_id_passed_index in excluded_bundle_ids:
                        # returning validation error
                        return {"assertion_error_code": 2}
            # ==================== BUNDLE LINKS VALIDATION (END) ====================
    # returning assertion errpr code of None
    return {"assertion_error_code": None}


# if ignore_bundle_name_validation_and_update is not set or set to False (default) ==> real upload
# if ignore_bundle_name_validation_and_update is set to True ==> calling upload for the preview functionality (temp upload for data validation purposes)
def upload_bundle_data(
    bundle_provided_data,
    ignore_bundle_name_validation_and_update=False,
    ignore_all_validations=False,
):
    # initializing needed variables
    assertion_error_code = None
    has_rule = False

    # not ignoring bundle name validation and update (real upload)
    if not ignore_bundle_name_validation_and_update and not ignore_all_validations:
        # ==================== BUNDLE NAME VALIDATION ====================
        # checking if name has already been used in this item bank
        name_already_used = Bundles.objects.filter(
            item_bank_id=bundle_provided_data["item_bank_id"],
            name=bundle_provided_data["name"],
        ).exclude(id=bundle_provided_data["id"])
        # ==================== BUNDLE NAME VALIDATION (END) ====================

        # name already used
        if name_already_used:
            # returning validation error
            return {"assertion_error_code": 1}

    # not ignoring bundle name validation and update (real upload)
    if not ignore_bundle_name_validation_and_update and not ignore_all_validations:
        # ==================== BUNDLE NAME VALIDATION ====================
        # checking if name has already been used in this item bank
        name_already_used = Bundles.objects.filter(
            item_bank_id=bundle_provided_data["item_bank_id"],
            name=bundle_provided_data["name"],
        ).exclude(id=bundle_provided_data["id"])
        # ==================== BUNDLE NAME VALIDATION (END) ====================

        # name already used
        if name_already_used:
            # returning validation error
            return {"assertion_error_code": 1}

    # getting bundle data
    bundle_data = Bundles.objects.get(id=bundle_provided_data["id"])

    # ==================== BUNDLE DATA ====================
    # updating needed data
    # not ignoring bundle name validation and update (real upload)
    if not ignore_bundle_name_validation_and_update:
        bundle_data.name = bundle_provided_data["name"]
    bundle_data.version_text = bundle_provided_data["version_text"]
    bundle_data.shuffle_items = bundle_provided_data["shuffle_items"]
    bundle_data.shuffle_bundles = bundle_provided_data["shuffle_bundles"]
    bundle_data.shuffle_between_bundles = bundle_provided_data[
        "shuffle_between_bundles"
    ]
    bundle_data.active = bundle_provided_data["active"]

    bundle_data.save()
    # ==================== BUNDLE DATA (END) ====================

    # ==================== BUNDLE/ITEMS ASSOCIATION DATA ====================
    # deleting all existing bundle items associations
    bundle_items_associations_to_delete = BundleItemsAssociation.objects.filter(
        bundle_source_id=bundle_data.id
    )
    for link in bundle_items_associations_to_delete:
        link.delete()

    # filtering bundle items associations data
    filtered_bundle_items_associations = []
    for bundle_items_association in bundle_provided_data["bundle_associations"]:
        if bundle_items_association["system_id_link"] is not None:
            filtered_bundle_items_associations.append(bundle_items_association)

    # looping in bundle_associations
    for index, bundle_items_association_data in enumerate(
        filtered_bundle_items_associations
    ):
        BundleItemsAssociation.objects.create(
            bundle_source_id=bundle_items_association_data["bundle_source_id"],
            system_id_link=bundle_items_association_data["system_id_link"],
            version=None,
            display_order=index + 1,
        )
    # ==================== BUNDLE/ITEMS ASSOCIATION DATA (END) ====================

    # ==================== BUNDLE RULES DATA ====================
    # deleting all existing bundle rules
    bundle_bundles_rule_type_id = BundleRuleType.objects.get(
        codename=BundleRuleTypeStatic.BUNDLE_BUNDLES
    ).id
    all_existing_bundle_rules = BundleRule.objects.filter(
        bundle_id=bundle_data.id, rule_type_id=bundle_bundles_rule_type_id
    )
    for all_existing_bundle_rule in all_existing_bundle_rules:
        existing_bundle_bundles_rules = BundleBundlesRule.objects.filter(
            bundle_rule_id=all_existing_bundle_rule.id
        )
        for existing_bundle_bundles_rule in existing_bundle_bundles_rules:
            existing_bundle_bundles_rule_associations = (
                BundleBundlesRuleAssociations.objects.filter(
                    bundle_bundles_rule_id=existing_bundle_bundles_rule.id
                )
            )
            for (
                existing_bundle_bundles_rule_association
            ) in existing_bundle_bundles_rule_associations:
                existing_bundle_bundles_rule_association.delete()
            existing_bundle_bundles_rule.delete()
        all_existing_bundle_rule.delete()

    # getting bundle rules
    bundle_rules = bundle_provided_data["bundle_bundles_rule_data"]

    # getting needed bundle rule type ID
    bundle_bundles_rule_type_id = BundleRuleType.objects.get(
        codename=BundleRuleTypeStatic.BUNDLE_BUNDLES
    ).id

    # looping in bundle_rules
    for bundle_rule in bundle_rules:
        # updating has_rule flag
        has_rule = True

        # deleting all existing bundle bundles associations
        # need to delete those now, otherwise those bundle bundles associates will be part of the excluded_bundle_ids (from the recursive_bundle_links_check)
        # that being said, a validation error will be triggered
        bundle_bundles_associations_to_delete = BundleBundlesAssociation.objects.filter(
            bundle_source_id=bundle_data.id
        )
        for link in bundle_bundles_associations_to_delete:
            link.delete()

        # creating new bundle rule
        new_bundle_rule = BundleRule.objects.create(
            rule_type_id=bundle_bundles_rule_type_id,
            bundle_id=bundle_data.id,
        )

        # formatting number of bundles
        formatted_number_of_bundles = bundle_rule["number_of_bundles"]
        # formatted_number_of_bundles is empty string
        if formatted_number_of_bundles == "":
            # setting formatted_number_of_bundles to None
            formatted_number_of_bundles = None

        # creating new bundle bundles rule
        new_bundle_bundles_rule = BundleBundlesRule.objects.create(
            number_of_bundles=formatted_number_of_bundles,
            shuffle_bundles=bundle_rule["shuffle_bundles"],
            keep_items_together=bundle_rule["keep_items_together"],
            display_order=bundle_rule["display_order"],
            bundle_rule_id=new_bundle_rule.id,
        )

        # building the associated_bundles_ids_from_bundle_rules
        associated_bundles_ids_from_bundle_rules = []
        for bundle_rule_association in bundle_rule["bundle_bundles_rule_associations"]:
            associated_bundles_ids_from_bundle_rules.append(
                bundle_rule_association["bundle_link_id"]
            )

        # looping in bundle_bundles_rule_associations
        for index, bundle_bundles_rule_association in enumerate(
            bundle_rule["bundle_bundles_rule_associations"]
        ):
            # ==================== BUNDLE LINKS VALIDATION ====================
            # if not ignoring validations
            if not ignore_all_validations:
                # getting excluded bundle IDs (bundle that should be excluded based on the current selected bundle ID)
                excluded_bundle_ids = recursive_bundle_links_check(
                    bundle_provided_data["item_bank_id"],
                    int(bundle_provided_data["id"]),
                )["excluded_bundle_ids"]

                # getting all elements from current index of associated_bundles_ids_from_bundle_rules array
                bundle_ids_from_current_index = (
                    associated_bundles_ids_from_bundle_rules[index:]
                )

                # any elements from bundle_ids_from_current_index exists in excluded_bundle_ids
                for bundle_id_from_current_index in bundle_ids_from_current_index:
                    if bundle_id_from_current_index in excluded_bundle_ids:
                        # returning validation error
                        return {"assertion_error_code": 2}
            # ==================== BUNDLE LINKS VALIDATION (END) ====================

            # creating new bundle bundles rule associations
            BundleBundlesRuleAssociations.objects.create(
                display_order=bundle_bundles_rule_association["display_order"],
                bundle_id=bundle_bundles_rule_association["bundle_data"]["id"],
                bundle_bundles_rule_id=new_bundle_bundles_rule.id,
            )
    # ==================== BUNDLE RULES DATA (END) ====================

    # ==================== BUNDLE/BUNDLES ASSOCIATION DATA ====================
    # filtering bundle bundles associations data
    filtered_bundle_bundles_associations = []
    filtered_bundle_link_ids = []
    for bundle_bundles_association in bundle_provided_data["bundle_associations"]:
        if bundle_bundles_association["system_id_link"] is None:
            # populating bundle_bundles_association
            filtered_bundle_bundles_associations.append(bundle_bundles_association)
            filtered_bundle_link_ids.append(
                bundle_bundles_association["bundle_link_id"]
            )

    # creating a copy of filtered_bundle_bundles_associations and a copy of filtered_bundle_link_ids
    copy_of_filtered_bundle_bundles_associations = (
        filtered_bundle_bundles_associations.copy()
    )
    copy_of_filtered_bundle_link_ids = filtered_bundle_link_ids.copy()

    # reverting the order of both arrays
    copy_of_filtered_bundle_bundles_associations.reverse()
    copy_of_filtered_bundle_link_ids.reverse()

    # ==================== IMPORTANT ====================
    # we need to handle the bundle bundles associations data in reversed and normal order
    # this recursive function is not properly able to analyse the last item of the provided filtered_bundle_bundles_associations and filtered_bundle_link_ids
    # the reversed order needs to be called first, so we only keep and save the orginial filtered_bundle_bundles_associations and filtered_bundle_link_ids data created from that recursive function
    # ==================== IMPORTANT ====================

    # handling the bundle bundles associations data with the reversed filtered_bundle_bundles_associations and filtered_bundle_link_ids
    bundle_bundles_associations_data = handle_bundle_bundles_associations_data(
        bundle_data,
        bundle_provided_data,
        copy_of_filtered_bundle_bundles_associations,
        copy_of_filtered_bundle_link_ids,
        has_rule,
        ignore_all_validations,
    )

    # there is a validation error
    if bundle_bundles_associations_data["assertion_error_code"] is not None:
        # returning the assertion error code
        return {
            "assertion_error_code": bundle_bundles_associations_data[
                "assertion_error_code"
            ]
        }

    # handling the bundle bundles associations data with the filtered_bundle_bundles_associations and filtered_bundle_link_ids
    bundle_bundles_associations_data = handle_bundle_bundles_associations_data(
        bundle_data,
        bundle_provided_data,
        filtered_bundle_bundles_associations,
        filtered_bundle_link_ids,
        has_rule,
        ignore_all_validations,
    )
    # there is a validation error
    if bundle_bundles_associations_data["assertion_error_code"] is not None:
        # returning the assertion error code
        return {
            "assertion_error_code": bundle_bundles_associations_data[
                "assertion_error_code"
            ]
        }
    # ==================== BUNDLE/BUNDLES ASSOCIATION DATA (END) ====================

    # ==================== BUNDLE ITEMS BASIC RULE DATA ====================
    # deleting existing bundle items basic rule data
    bundle_items_basic_rule_type_id = BundleRuleType.objects.get(
        codename=BundleRuleTypeStatic.BUNDLE_ITEMS_BASIC
    ).id
    existing_bundle_items_basic_rules = BundleRule.objects.filter(
        bundle_id=bundle_data.id,
        rule_type_id=bundle_items_basic_rule_type_id,
    )
    for existing_bundle_items_basic_rule in existing_bundle_items_basic_rules:
        existing_bundle_items_basic_rule_details = BundleItemsBasicRule.objects.filter(
            bundle_rule_id=existing_bundle_items_basic_rule.id
        )
        for (
            existing_bundle_items_basic_rule_detail
        ) in existing_bundle_items_basic_rule_details:
            existing_bundle_items_basic_rule_detail.delete()
        existing_bundle_items_basic_rule.delete()

    # bundle_items_basic_rule_data is defined (not empty)
    if bundle_provided_data["bundle_items_basic_rule_data"] != "":
        # creating new bundle rule
        new_bundle_rule = BundleRule.objects.create(
            rule_type_id=bundle_items_basic_rule_type_id,
            bundle_id=bundle_data.id,
        )
        # creating new bundle items basic rule
        BundleItemsBasicRule.objects.create(
            number_of_items=int(bundle_provided_data["bundle_items_basic_rule_data"]),
            bundle_rule_id=new_bundle_rule.id,
        )
    # ==================== BUNDLE ITEMS BASIC RULE DATA (END) ====================

    # ==================== DUPLICATE ITEMS IN BUNDLE STRUCTURE VALIDATION ====================
    # if not ignoring validations
    if not ignore_all_validations:
        # getting all bundle associations paths
        bundle_associations_paths = recursive_bundle_links_check(
            bundle_provided_data["item_bank_id"],
            int(bundle_provided_data["id"]),
        )["bundle_associations_paths"]

        # looping in bundle_associations_paths
        for bundle_associations_path in bundle_associations_paths:
            # getting all associated items based on bundle_associations_path
            associated_items = BundleItemsAssociation.objects.filter(
                bundle_source_id__in=bundle_associations_path
            ).values_list("system_id_link", flat=True)

            # checking if duplicate system_id are found
            if has_duplicates(associated_items):
                # returning validation error
                return {"assertion_error_code": 3}
    # ==================== DUPLICATE ITEMS IN BUNDLE STRUCTURE VALIDATION (END) ====================

    # getting most updated bundle data
    updated_bundle_data = BundleDataVW.objects.get(id=bundle_data.id)
    serialized_bundle_data = BundleDetailedDataViewSerializer(
        updated_bundle_data, many=False
    ).data

    return {
        "serialized_bundle_data": serialized_bundle_data,
        "assertion_error_code": assertion_error_code,
    }


def upload_bundle(request):
    bundle_provided_data = json.loads(request.body)

    assertion_error_code = None
    try:
        with transaction.atomic():
            # getting uploaded bundle data
            uploaded_bundle_data = upload_bundle_data(bundle_provided_data)

            # handling assertion errors (validation errors)
            assertion_error_code = uploaded_bundle_data["assertion_error_code"]
            if assertion_error_code is not None:
                raise AssertionError("Trigger Validation Error")

            serialized_bundle_data = uploaded_bundle_data["serialized_bundle_data"]

            # creating updated ordered list_of_bundles list
            all_bundles = BundleDataVW.objects.filter(
                item_bank_id=bundle_provided_data["item_bank_id"]
            )
            list_of_bundles = []
            for bundle in all_bundles:
                list_of_bundles.append({"id": bundle.id, "bundle_name": bundle.name})
            list_of_bundles = sorted(
                list_of_bundles,
                # converting second part of string (incrementing value) to int, so the ordering is respected
                key=lambda k: k["bundle_name"],
                reverse=False,
            )

            # adding needed attributes (useful for the frontend response analysis)
            serialized_bundle_data["list_of_bundles"] = list_of_bundles
            serialized_bundle_data["ok"] = True
    except AssertionError:
        if assertion_error_code == 1:
            return Response(
                {
                    "error": "The provided bundle name has already been used in this Item Bank",
                    "error_code": 1,
                },
                status=status.HTTP_409_CONFLICT,
            )
        elif assertion_error_code == 2:
            return Response(
                {
                    "error": "There is a structural conflict with the bundle associations",
                    "error_code": 2,
                },
                status=status.HTTP_409_CONFLICT,
            )
        elif assertion_error_code == 3:
            return Response(
                {
                    "error": "Duplicate System IDs have been found in the bundle associations",
                    "error_code": 3,
                },
                status=status.HTTP_409_CONFLICT,
            )
        # should never happen
        else:
            return Response(
                {
                    "error": "Something wrong happened during the upload bundle process",
                    "error_code": 99,
                },
                status=status.HTTP_400_BAD_REQUEST,
            )

    # should never happen
    except:
        return Response(
            {
                "error": "Something wrong happened during the upload bundle process",
                "error_code": 99,
            },
            status=status.HTTP_400_BAD_REQUEST,
        )

    return Response(serialized_bundle_data)


def get_items_ready_to_be_added_to_bundle(request):
    success, parameters = get_needed_parameters(
        [
            "item_bank_id",
            "bundle_id",
            "all_linked_item_system_ids",
            "all_local_unlinked_item_system_ids",
            "default_search_criterias",
            "custom_search_criterias",
            "page",
            "page_size",
        ],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    (
        item_bank_id,
        bundle_id,
        all_linked_item_system_ids,
        all_local_unlinked_item_system_ids,
        default_search_criterias,
        custom_search_criterias,
        page,
        page_size,
    ) = itemgetter(
        "item_bank_id",
        "bundle_id",
        "all_linked_item_system_ids",
        "all_local_unlinked_item_system_ids",
        "default_search_criterias",
        "custom_search_criterias",
        "page",
        "page_size",
    )(
        parameters
    )

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # getting and setting the linked item system IDs array
    all_linked_item_system_ids_final = []
    if all_linked_item_system_ids != " ":
        all_linked_item_system_ids = all_linked_item_system_ids.replace("[", "")
        all_linked_item_system_ids = all_linked_item_system_ids.replace("]", "")
        all_linked_item_system_ids_final = all_linked_item_system_ids.split(",")

    # getting and setting the local unlinked item system IDs array
    all_local_unlinked_item_system_ids_final = []
    if all_local_unlinked_item_system_ids != " ":
        all_local_unlinked_item_system_ids = all_local_unlinked_item_system_ids.replace(
            "[", ""
        )
        all_local_unlinked_item_system_ids = all_local_unlinked_item_system_ids.replace(
            "]", ""
        )
        all_local_unlinked_item_system_ids_final = (
            all_local_unlinked_item_system_ids.split(",")
        )

    # getting all linked items related to respective bundle
    all_related_linked_item_ids = BundleItemsAssociation.objects.filter(
        bundle_source_id=bundle_id, system_id_link__isnull=False
    ).values_list("system_id_link", flat=True)

    # ==================== DEFAULT SEARCH CRITERIAS - DROPDOWNS ====================
    # initializing needed variables
    development_status_ids = []
    active_state_values = []
    # getting default search criterias
    default_search_criterias = json.loads(default_search_criterias)
    # development status data is defined
    if default_search_criterias["development_status_data"]:
        # looping in development status data
        for development_status_data in default_search_criterias[
            "development_status_data"
        ]:
            development_status_ids.append(development_status_data["value"])
    # active state data is defined
    if default_search_criterias["active_state_data"]:
        # looping in development status data
        for active_state_data in default_search_criterias["active_state_data"]:
            active_state_values.append(active_state_data["value"])
    # ==================== DEFAULT SEARCH CRITERIAS - DROPDOWNS (END) ====================

    # ==================== CUSTOM SEARCH CRITERIAS - DROPDOWNS ====================
    # initializing needed variables
    custom_search_criterias_provided = False
    matching_item_bank_attribute_value_ids = []
    # getting custom search criterias
    custom_search_criterias = json.loads(custom_search_criterias)
    # looping in custom_search_criterias
    for custom_search_criteria in custom_search_criterias:
        # search criteria defined
        if custom_search_criteria:
            # setting custom_search_criterias_provided to True
            custom_search_criterias_provided = True
            for search_criteria_data in custom_search_criteria:
                # populating matching_item_bank_attribute_value_ids array
                matching_item_bank_attribute_value_ids.append(
                    search_criteria_data["value"]
                )

    # getting respective item bank attribute values
    item_bank_attribute_values_view_data = ItemBankAttributeValuesVW.objects.filter(
        item_bank_attribute_value_id__in=matching_item_bank_attribute_value_ids
    ).order_by("item_bank_attribute_id")

    # initializing final_obj
    final_obj = {}

    # grouping by item bank attribute id
    for obj, inner_group in groupby(
        item_bank_attribute_values_view_data, lambda x: x.item_bank_attribute_id
    ):
        # populating obj
        final_obj["{0}".format(obj)] = list(inner_group)

    # initializing needed variables
    matching_item_ids = []
    item_bank_attribute_value_ids_array = []

    # looping in object attributes (attributes being the item bank attribute ids)
    for attr, values in final_obj.items():
        respective_ids = []
        # looping in values
        for value_data in values:
            respective_ids.append(value_data.item_bank_attribute_value_id)
        item_bank_attribute_value_ids_array.append(respective_ids)

    for index, value_ids in enumerate(item_bank_attribute_value_ids_array):
        # first iteration
        if index <= 0:
            matching_item_ids = ItemAttributeValue.objects.filter(
                item_bank_attribute_value_id__in=value_ids
            ).values_list("item_id", flat=True)
        # other iterations
        else:
            matching_item_ids = ItemAttributeValue.objects.filter(
                item_bank_attribute_value_id__in=value_ids,
                item_id__in=matching_item_ids,
            ).values_list("item_id", flat=True)
    # ==================== CUSTOM SEARCH CRITERIAS - DROPDOWNS (END) ====================

    # getting all items related to respective item bank
    all_related_items = ItemLatestVersionsDataVW.objects.filter(
        item_bank_id=item_bank_id
        # excluding needed system IDs
    ).exclude(
        Q(system_id__in=all_related_linked_item_ids)
        | Q(system_id__in=all_linked_item_system_ids_final)
    )

    # some items have been locally unlinked
    if all_local_unlinked_item_system_ids_final:
        # adding unlinked system IDs to all_related_items
        local_unlinked_system_ids_items = ItemLatestVersionsDataVW.objects.filter(
            item_bank_id=item_bank_id,
            system_id__in=all_local_unlinked_item_system_ids_final,
        )

        all_related_items = all_related_items | local_unlinked_system_ids_items

    # development_status_ids defined
    if development_status_ids:
        all_related_items = all_related_items.filter(
            development_status_id__in=development_status_ids
        )

    # active_state_values defined
    if active_state_values:
        all_related_items = all_related_items.filter(
            active_status__in=active_state_values
        )

    # custom_search_criterias_provided is set to True
    if custom_search_criterias_provided:
        all_related_items = all_related_items.filter(item_id__in=matching_item_ids)

    # ordering items by custom System ID (ascending)
    ordered_items = sorted(
        all_related_items,
        # converting second part of string (incrementing value) to int, so the ordering is respected
        key=lambda k: int(k.system_id.split("_")[1]),
        reverse=False,
    )

    # only getting data for current selected page
    new_all_related_items = ordered_items[
        (current_page - 1) * page_size : current_page * page_size
    ]

    # getting serialized data
    serialized_data = ItemLatestVersionsDataViewSimpleSerializer(
        new_all_related_items, many=True
    ).data

    # getting final data using pagination library
    return paginator.get_paginated_response(
        serialized_data, len(ordered_items), current_page, page_size
    )


def get_found_items_ready_to_be_added_to_bundle(request):
    success, parameters = get_needed_parameters(
        [
            "item_bank_id",
            "bundle_id",
            "all_linked_item_system_ids",
            "all_local_unlinked_item_system_ids",
            "default_search_criterias",
            "custom_search_criterias",
            "keyword",
            "current_language",
            "page",
            "page_size",
        ],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    (
        item_bank_id,
        bundle_id,
        all_linked_item_system_ids,
        all_local_unlinked_item_system_ids,
        default_search_criterias,
        custom_search_criterias,
        keyword,
        current_language,
        page,
        page_size,
    ) = itemgetter(
        "item_bank_id",
        "bundle_id",
        "all_linked_item_system_ids",
        "all_local_unlinked_item_system_ids",
        "default_search_criterias",
        "custom_search_criterias",
        "keyword",
        "current_language",
        "page",
        "page_size",
    )(
        parameters
    )

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # getting and setting the linked item system IDs array
    all_linked_item_system_ids_final = []
    if all_linked_item_system_ids != " ":
        all_linked_item_system_ids = all_linked_item_system_ids.replace("[", "")
        all_linked_item_system_ids = all_linked_item_system_ids.replace("]", "")
        all_linked_item_system_ids_final = all_linked_item_system_ids.split(",")

    # getting and setting the local unlinked item system IDs array
    all_local_unlinked_item_system_ids_final = []
    if all_local_unlinked_item_system_ids != " ":
        all_local_unlinked_item_system_ids = all_local_unlinked_item_system_ids.replace(
            "[", ""
        )
        all_local_unlinked_item_system_ids = all_local_unlinked_item_system_ids.replace(
            "]", ""
        )
        all_local_unlinked_item_system_ids_final = (
            all_local_unlinked_item_system_ids.split(",")
        )

    # getting all linked items related to respective bundle
    all_related_linked_item_ids = BundleItemsAssociation.objects.filter(
        bundle_source_id=bundle_id, system_id_link__isnull=False
    ).values_list("system_id_link", flat=True)

    # ==================== DEFAULT SEARCH CRITERIAS - DROPDOWNS ====================
    # initializing needed variables
    development_status_ids = []
    active_state_values = []
    # getting default search criterias
    default_search_criterias = json.loads(default_search_criterias)
    # development status data is defined
    if default_search_criterias["development_status_data"]:
        # looping in development status data
        for development_status_data in default_search_criterias[
            "development_status_data"
        ]:
            development_status_ids.append(development_status_data["value"])
    # active state data is defined
    if default_search_criterias["active_state_data"]:
        # looping in development status data
        for active_state_data in default_search_criterias["active_state_data"]:
            active_state_values.append(active_state_data["value"])
    # ==================== DEFAULT SEARCH CRITERIAS - DROPDOWNS (END) ====================

    # ==================== CUSTOM SEARCH CRITERIAS - DROPDOWNS ====================
    # initializing needed variables
    custom_search_criterias_provided = False
    matching_item_bank_attribute_value_ids = []
    # getting custom search criterias
    custom_search_criterias = json.loads(custom_search_criterias)
    # looping in custom_search_criterias
    for custom_search_criteria in custom_search_criterias:
        # search criteria defined
        if custom_search_criteria:
            # setting custom_search_criterias_provided to True
            custom_search_criterias_provided = True
            for search_criteria_data in custom_search_criteria:
                # populating matching_item_bank_attribute_value_ids array
                matching_item_bank_attribute_value_ids.append(
                    search_criteria_data["value"]
                )

    # getting respective item bank attribute values
    item_bank_attribute_values_view_data = ItemBankAttributeValuesVW.objects.filter(
        item_bank_attribute_value_id__in=matching_item_bank_attribute_value_ids
    ).order_by("item_bank_attribute_id")

    # initializing final_obj
    final_obj = {}

    # grouping by item bank attribute id
    for obj, inner_group in groupby(
        item_bank_attribute_values_view_data, lambda x: x.item_bank_attribute_id
    ):
        # populating obj
        final_obj["{0}".format(obj)] = list(inner_group)

    # initializing needed variables
    matching_item_ids = []
    item_bank_attribute_value_ids_array = []

    # looping in object attributes (attributes being the item bank attribute ids)
    for attr, values in final_obj.items():
        respective_ids = []
        # looping in values
        for value_data in values:
            respective_ids.append(value_data.item_bank_attribute_value_id)
        item_bank_attribute_value_ids_array.append(respective_ids)

    for index, value_ids in enumerate(item_bank_attribute_value_ids_array):
        # first iteration
        if index <= 0:
            matching_item_ids = ItemAttributeValue.objects.filter(
                item_bank_attribute_value_id__in=value_ids
            ).values_list("item_id", flat=True)
        # other iterations
        else:
            matching_item_ids = ItemAttributeValue.objects.filter(
                item_bank_attribute_value_id__in=value_ids,
                item_id__in=matching_item_ids,
            ).values_list("item_id", flat=True)
    # ==================== CUSTOM SEARCH CRITERIAS - DROPDOWNS (END) ====================

    # if blank search
    if keyword == " ":
        # getting needed items
        found_items = ItemLatestVersionsDataVW.objects.filter(
            item_bank_id=item_bank_id
            # excluding needed system IDs
        ).exclude(
            Q(system_id__in=all_related_linked_item_ids)
            | Q(system_id__in=all_linked_item_system_ids_final)
        )

        # some items have been locally unlinked
        if all_local_unlinked_item_system_ids_final:
            # adding unlinked system IDs to all_related_items
            local_unlinked_system_ids_items = ItemLatestVersionsDataVW.objects.filter(
                item_bank_id=item_bank_id,
                system_id__in=all_local_unlinked_item_system_ids_final,
            )

            found_items = found_items | local_unlinked_system_ids_items

        # development_status_ids defined
        if development_status_ids:
            found_items = found_items.filter(
                development_status_id__in=development_status_ids
            )

        # active_state_values defined
        if active_state_values:
            found_items = found_items.filter(active_status__in=active_state_values)

        # custom_search_criterias_provided is set to True
        if custom_search_criterias_provided:
            found_items = found_items.filter(item_id__in=matching_item_ids)

    # regular search
    else:
        keyword = keyword.lower()
        keyword = parse.unquote(keyword)

        # English Search
        if current_language == "en":
            found_items = ItemLatestVersionsDataVW.objects.filter(
                Q(item_bank_id=item_bank_id)
                & (
                    Q(system_id__icontains=keyword)
                    | Q(response_format_name_en__icontains=keyword)
                    | Q(development_status_name_en__icontains=keyword)
                    | Q(historical_id__icontains=keyword)
                )
                # excluding needed system IDs
            ).exclude(
                Q(system_id__in=all_related_linked_item_ids)
                | Q(system_id__in=all_linked_item_system_ids_final)
            )

            # some items have been locally unlinked
            if all_local_unlinked_item_system_ids_final:
                # adding unlinked system IDs to all_related_items
                local_unlinked_system_ids_items = (
                    ItemLatestVersionsDataVW.objects.filter(
                        (
                            Q(item_bank_id=item_bank_id)
                            & Q(system_id__in=all_local_unlinked_item_system_ids_final)
                        )
                        & (
                            Q(system_id__icontains=keyword)
                            | Q(response_format_name_en__icontains=keyword)
                            | Q(development_status_name_en__icontains=keyword)
                        )
                    )
                )

                found_items = found_items | local_unlinked_system_ids_items

        # French Search
        else:
            found_items = ItemLatestVersionsDataVW.objects.filter(
                Q(item_bank_id=item_bank_id)
                & (
                    Q(system_id__icontains=keyword)
                    | Q(response_format_name_fr__icontains=keyword)
                    | Q(development_status_name_fr__icontains=keyword)
                    | Q(historical_id__icontains=keyword)
                )
                # excluding needed system IDs
            ).exclude(
                Q(system_id__in=all_related_linked_item_ids)
                | Q(system_id__in=all_linked_item_system_ids_final)
            )

            # some items have been locally unlinked
            if all_local_unlinked_item_system_ids_final:
                # adding unlinked system IDs to all_related_items
                local_unlinked_system_ids_items = (
                    ItemLatestVersionsDataVW.objects.filter(
                        (
                            Q(item_bank_id=item_bank_id)
                            & Q(system_id__in=all_local_unlinked_item_system_ids_final)
                        )
                        & (
                            Q(system_id__icontains=keyword)
                            | Q(response_format_name_fr__icontains=keyword)
                            | Q(development_status_name_fr__icontains=keyword)
                        )
                    )
                )

                found_items = found_items | local_unlinked_system_ids_items

        # development_status_ids defined
        if development_status_ids:
            found_items = found_items.filter(
                development_status_id__in=development_status_ids
            )

        # active_state_values defined
        if active_state_values:
            found_items = found_items.filter(active_status__in=active_state_values)

        # custom_search_criterias_provided is set to True
        if custom_search_criterias_provided:
            found_items = found_items.filter(item_id__in=matching_item_ids)

        # no results found
        if not found_items:
            return Response({"no results found"}, status=status.HTTP_200_OK)

    # ordering items by custom System ID (ascending)
    ordered_found_items = sorted(
        found_items,
        # converting second part of string (incrementing value) to int, so the ordering is respected
        key=lambda k: int(k.system_id.split("_")[1]),
        reverse=False,
    )

    # only getting data for current selected page
    new_found_items = ordered_found_items[
        (current_page - 1) * page_size : current_page * page_size
    ]

    # getting serialized data
    serialized_data = ItemLatestVersionsDataViewSimpleSerializer(
        new_found_items, many=True
    ).data

    # getting final data using pagination library
    return paginator.get_paginated_response(
        serialized_data, len(ordered_found_items), current_page, page_size
    )


# getting all parent and child links
def get_all_parent_and_child_links(
    related_bundle_associations_list, parent_ids, child_ids
):
    # initialize needed arrays
    final_parent_ids = parent_ids
    final_child_ids = child_ids
    linked_bundles_ids_ref = []

    # looping in parent_ids
    for parent_id in final_parent_ids:
        parent_related_associations = [
            obj
            for obj in related_bundle_associations_list
            if obj.bundle_link_id == parent_id
        ]

        # looping in parent_related_associations
        for parent_related_association in parent_related_associations:
            # populating final_parent_ids (based on bundle_source_id)
            if parent_related_association.bundle_source_id not in final_parent_ids:
                final_parent_ids.append(parent_related_association.bundle_source_id)

            # populating linked_bundles_ids_ref
            if (
                parent_related_association.bundle_source_id
                not in linked_bundles_ids_ref
            ):
                linked_bundles_ids_ref.append(
                    parent_related_association.bundle_source_id
                )

    # looping in final_child_ids
    for child_id in final_child_ids:
        child_related_associations = [
            obj
            for obj in related_bundle_associations_list
            if obj.bundle_source_id == child_id
        ]

        # looping in child_related_associations
        for child_related_association in child_related_associations:
            # populating final_child_ids (based on bundle_link_id)
            if child_related_association.bundle_link_id not in final_child_ids:
                final_child_ids.append(child_related_association.bundle_link_id)

            # populating linked_bundles_ids_ref
            if child_related_association.bundle_link_id not in linked_bundles_ids_ref:
                linked_bundles_ids_ref.append(child_related_association.bundle_link_id)

    # returning the linked_bundles_ids_ref array
    return linked_bundles_ids_ref


# function that returns an array with bundle IDs that should be excluded from the available bundles ready to be linked filter
def recursive_bundle_links_check(
    item_bank_id,
    bundle_id,
):
    # initializing needed variables
    bundle_associations_paths = []
    considered_bundle_ids = []

    # ==================== GETTING LOCAL LISTS ====================
    # getting all related bundle associations
    all_related_bundles = Bundles.objects.filter(item_bank_id=item_bank_id)

    related_bundle_ids = []
    for bundle_data in all_related_bundles:
        related_bundle_ids.append(bundle_data.id)

    related_bundle_associations_list = BundleBundlesAssociation.objects.filter(
        Q(bundle_source_id__in=related_bundle_ids)
        | Q(bundle_link_id__in=related_bundle_ids)
    )

    # getting all bundle bundle
    all_bundle_rule_types = BundleRuleType.objects.all()

    # getting related bundle rules
    all_bundle_rules = BundleRule.objects.filter(bundle_id__in=related_bundle_ids)

    # getting all bundle bundles rule data (from view)
    all_bundle_bundles_rule_data_view = BundleBundlesRuleDataVW.objects.filter(
        bundle_id__in=related_bundle_ids
    )
    # ==================== GETTING LOCAL LISTS (END) ====================
    # getting bundle bundles rule type ID
    bundle_bundles_rule_type_id = [
        obj.id
        for obj in all_bundle_rule_types
        if obj.codename == BundleRuleTypeStatic.BUNDLE_BUNDLES
    ][0]

    # getting related bundle parents
    related_bundle_parents = [
        obj.bundle_source_id
        for obj in related_bundle_associations_list
        if obj.bundle_link_id == bundle_id
    ]

    # handling bundle rules
    # getting related bundle bundles rule data
    related_bundle_bundles_rule_data = [
        obj
        for obj in all_bundle_bundles_rule_data_view
        if obj.bundle_bundles_rule_associations_bundle_id == bundle_id
    ]

    # looping in related_bundle_bundles_rule_data
    for related_bundle_bundles_rule in related_bundle_bundles_rule_data:
        # populating related_bundle_parents
        related_bundle_parents.append(related_bundle_bundles_rule.bundle_id)

    # looping in related_bundle_parents
    for related_bundle_parent in related_bundle_parents:
        # populating bundle_associations_paths
        bundle_associations_paths.append([related_bundle_parent])

    # ==================== MANAGING PARENTS ====================
    # a bundle can have more that one parent, so in this logic we're creating bundle structure paths for every single parent found
    # looping in bundle_associations_paths
    for bundle_ids_array in bundle_associations_paths:
        # creating a copy of bundle_ids_array
        initial_bundle_ids_array = list(bundle_ids_array).copy()
        # looping in bundle_ids_array
        for bundle_id in bundle_ids_array:
            # bundle_id not already considered
            if bundle_id not in considered_bundle_ids:
                # populating considered_bundle_ids
                considered_bundle_ids.append(bundle_id)

                # getting bundle bundles associations (getting parents) based on the bundle ID of the current iteration
                temp_related_bundle_parents = [
                    obj.bundle_source_id
                    for obj in related_bundle_associations_list
                    if obj.bundle_link_id == bundle_id
                ]

                # handling bundle rules
                # getting related bundle bundles rule data
                temp_related_bundle_bundles_rule_data = [
                    obj
                    for obj in all_bundle_bundles_rule_data_view
                    if obj.bundle_bundles_rule_associations_bundle_id == bundle_id
                ]

                # looping in temp_related_bundle_bundles_rule_data
                for (
                    temp_related_bundle_bundles_rule
                ) in temp_related_bundle_bundles_rule_data:
                    temp_related_bundle_parents.append(
                        temp_related_bundle_bundles_rule.bundle_id
                    )

                # looping in found bundle bundles associations
                for temp_index, temp_related_bundle_parent in enumerate(
                    temp_related_bundle_parents
                ):
                    # creating copy of copy_of_bundle_ids_array
                    copy_of_copy_of_bundle_ids_array = initial_bundle_ids_array.copy()
                    # index 0
                    if temp_index == 0:
                        # populating bundle_ids_array
                        if temp_related_bundle_parent not in bundle_ids_array:
                            bundle_ids_array.append(temp_related_bundle_parent)
                    # index > 0 (multiple parents are found)
                    else:
                        # populating bundle_ids_array
                        if (
                            temp_related_bundle_parent
                            not in copy_of_copy_of_bundle_ids_array
                        ):
                            copy_of_copy_of_bundle_ids_array.append(
                                temp_related_bundle_parent
                            )
                        # populating bundle_associations_paths
                        bundle_associations_paths.append(
                            copy_of_copy_of_bundle_ids_array
                        )
    # ==================== MANAGING PARENTS (END) ====================

    # ==================== MANAGING CHILDREN ====================
    # no parents have been found
    if not bundle_associations_paths:
        # adding its own bundle_id to bundle_associations_paths
        bundle_associations_paths.append([bundle_id])

    # looping in bundle_associations_paths
    for branch_array in bundle_associations_paths:
        # looping in branch_array
        for bundle_id in branch_array:
            # getting bundle bundles associations (getting childlren) based on the bundle ID of the current iteration
            temp_related_bundle_children = [
                obj.bundle_link_id
                for obj in related_bundle_associations_list
                if obj.bundle_source_id == bundle_id
            ]

            # there are not bundle associations (we might have bundle rules)
            if not temp_related_bundle_children:
                # initializing temp_related_bundle_children
                temp_related_bundle_children = []
                # getting related bundle rules
                temp_related_bundle_rules = [
                    obj
                    for obj in all_bundle_rules
                    if obj.bundle_id == bundle_id
                    and obj.rule_type_id == bundle_bundles_rule_type_id
                ]
                # looping in temp_related_bundle_rules
                for temp_related_bundle_rule in temp_related_bundle_rules:
                    # getting related bundle bundles rule data
                    temp_related_bundle_bundles_rule_data = [
                        obj
                        for obj in all_bundle_bundles_rule_data_view
                        if obj.id == temp_related_bundle_rule.id
                    ]
                    # looping in temp_related_bundle_bundles_rule_data
                    for (
                        temp_related_bundle_bundles_rule
                    ) in temp_related_bundle_bundles_rule_data:
                        temp_related_bundle_children.append(
                            temp_related_bundle_bundles_rule.bundle_bundles_rule_associations_bundle_id
                        )

            # looping in found bundle bundles associations
            for temp_related_bundle_child in temp_related_bundle_children:
                # temp_related_bundle_child not already part of the current branch_array
                if temp_related_bundle_child not in branch_array:
                    # populating branch_array
                    branch_array.append(temp_related_bundle_child)
    # ==================== MANAGING CHILDREN (END) ====================

    # combining all lists together to create a single list
    excluded_bundle_ids = []

    for bundle_ids_list in bundle_associations_paths:
        excluded_bundle_ids = list(
            set(list(excluded_bundle_ids) + list(bundle_ids_list))
        )

    # returning final object
    return {
        "excluded_bundle_ids": excluded_bundle_ids,
        "bundle_associations_paths": bundle_associations_paths,
    }


def get_bundles_ready_to_be_added_to_bundle(request):
    # getting data from body
    loaded_data = json.loads(request.body)
    bundle_provided_data = loaded_data["bundle_provided_data"]
    page = loaded_data["page"]
    page_size = loaded_data["page_size"]

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # initializing serialized_data
    serialized_data = []

    try:
        with transaction.atomic():
            # temporairly upload bundle data (so local changes are considered)
            # ignoring all validations (third parameter set to True)
            upload_bundle_data(bundle_provided_data, True, True)

            # getting excluded_bundle_ids
            excluded_bundle_ids = recursive_bundle_links_check(
                bundle_provided_data["item_bank_id"], int(bundle_provided_data["id"])
            )["excluded_bundle_ids"]

            # getting all available bundles that are ready to be added
            # excluding ids that are from the excluded_bundle_ids array
            bundles_ready_to_be_added = BundleDataVW.objects.filter(
                item_bank_id=bundle_provided_data["item_bank_id"]
            ).exclude(id__in=excluded_bundle_ids)

            # ordering items by custom System ID (ascending)
            ordered_bundles_ready_to_be_added = sorted(
                bundles_ready_to_be_added,
                # converting second part of string (incrementing value) to int, so the ordering is respected
                key=lambda k: k.name,
                reverse=False,
            )

            # only getting data for current selected page
            new_bundles_ready_to_be_added = ordered_bundles_ready_to_be_added[
                (current_page - 1) * page_size : current_page * page_size
            ]

            # getting serialized data
            serialized_data = BundleDetailedDataViewSerializer(
                new_bundles_ready_to_be_added, many=True
            ).data

            # triggering assertion error to abort any changes done to the DB
            raise AssertionError("Abort DB Changes")

    except AssertionError:
        # getting final data using pagination library
        return paginator.get_paginated_response(
            serialized_data,
            len(ordered_bundles_ready_to_be_added),
            current_page,
            page_size,
        )


def get_found_bundles_ready_to_be_added_to_bundle(request):
    # getting data from body
    loaded_data = json.loads(request.body)
    bundle_provided_data = loaded_data["bundle_provided_data"]
    keyword = loaded_data["keyword"]
    page = loaded_data["page"]
    page_size = loaded_data["page_size"]

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # initializing serialized_data
    serialized_data = []

    try:
        with transaction.atomic():
            # temporairly upload bundle data (so local changes are considered)
            # ignoring all validations (third parameter set to True)
            upload_bundle_data(bundle_provided_data, True, True)

            # getting excluded_bundle_ids
            excluded_bundle_ids = recursive_bundle_links_check(
                bundle_provided_data["item_bank_id"], int(bundle_provided_data["id"])
            )["excluded_bundle_ids"]

            # if blank search
            if keyword == "":
                # getting all available bundles that are ready to be added
                # excluding ids that are from the excluded_bundle_ids array
                found_bundles_ready_to_be_added = BundleDataVW.objects.filter(
                    item_bank_id=bundle_provided_data["item_bank_id"]
                ).exclude(id__in=excluded_bundle_ids)
            # regular search
            else:
                keyword = keyword.lower()
                keyword = parse.unquote(keyword)

                # getting all available bundles that are ready to be added
                found_bundles_ready_to_be_added = BundleDataVW.objects.filter(
                    Q(item_bank_id=bundle_provided_data["item_bank_id"])
                    & (Q(name__icontains=keyword) | Q(version_text__icontains=keyword))
                ).exclude(id__in=excluded_bundle_ids)

                # ==================== ACTIVE STATUS SEARCH ====================
                # keyword contains wording matching with "active" wording
                if (
                    keyword == TextResources.commons["active"]["en"].lower()
                    or keyword == TextResources.commons["active"]["fr"].lower()
                ):
                    # combining results found
                    matching_active_bundles = BundleDataVW.objects.filter(
                        item_bank_id=bundle_provided_data["item_bank_id"], active=1
                    ).exclude(id__in=excluded_bundle_ids)
                    found_bundles_ready_to_be_added = list(
                        set(
                            list(found_bundles_ready_to_be_added)
                            + list(matching_active_bundles)
                        )
                    )
                # ==================== ACTIVE STATUS SEARCH (END) ====================

                # ==================== INACTIVE STATUS SEARCH ====================
                # keyword contains wording matching with "inactive" wording
                if (
                    keyword == TextResources.commons["inactive"]["en"].lower()
                    or keyword == TextResources.commons["inactive"]["fr"].lower()
                ):
                    # combining results found
                    matching_inactive_bundles = BundleDataVW.objects.filter(
                        item_bank_id=bundle_provided_data["item_bank_id"], active=0
                    ).exclude(id__in=excluded_bundle_ids)
                    found_bundles_ready_to_be_added = list(
                        set(
                            list(found_bundles_ready_to_be_added)
                            + list(matching_inactive_bundles)
                        )
                    )
                # ==================== INACTIVE STATUS SEARCH (END) ====================

                # no results found
                if not found_bundles_ready_to_be_added:
                    return Response({"no results found"}, status=status.HTTP_200_OK)

            # ordering items by custom System ID (ascending)
            ordered_found_bundles_ready_to_be_added = sorted(
                found_bundles_ready_to_be_added,
                # converting second part of string (incrementing value) to int, so the ordering is respected
                key=lambda k: k.name,
                reverse=False,
            )

            # only getting data for current selected page
            new_found_bundles_ready_to_be_added = (
                ordered_found_bundles_ready_to_be_added[
                    (current_page - 1) * page_size : current_page * page_size
                ]
            )

            # getting serialized data
            serialized_data = BundleDetailedDataViewSerializer(
                new_found_bundles_ready_to_be_added, many=True
            ).data

            # triggering assertion error to abort any changes done to the DB
            raise AssertionError("Abort DB Changes")

    except AssertionError:
        # getting final data using pagination library
        return paginator.get_paginated_response(
            serialized_data,
            len(ordered_found_bundles_ready_to_be_added),
            current_page,
            page_size,
        )


def get_associated_items_list(
    bundle_id, bundle_parameters, bundle_rules, bundle_rule_types
):
    # getting bundle items rules data
    bundle_items_basic_rule_data = []
    bundle_items_basic_rule_type_id = [
        obj.id
        for obj in bundle_rule_types
        if obj.codename == BundleRuleTypeStatic.BUNDLE_ITEMS_BASIC
    ][0]
    bundle_items_basic_rule_id = [
        obj.id
        for obj in bundle_rules
        if obj.rule_type_id == bundle_items_basic_rule_type_id
    ]

    if bundle_items_basic_rule_id:
        bundle_items_basic_rule_data = BundleItemsBasicRule.objects.get(
            bundle_rule_id=bundle_items_basic_rule_id[0]
        )

    # getting associated items
    associated_items = list(
        BundleItemsAssociation.objects.filter(bundle_source_id=bundle_id)
        .values_list("system_id_link", flat=True)
        .order_by("display_order")
    )

    # if there is a bundle items basic rule defined
    if bundle_items_basic_rule_data:
        # getting randomly the defined number_of_items (but keeping the defined order)
        all_indices = range(len(associated_items))
        random_indices = sample(
            all_indices, bundle_items_basic_rule_data.number_of_items
        )
        sorted_random_indices = sorted(random_indices)
        associated_items = [associated_items[index] for index in sorted_random_indices]

    # shuffle items is enables
    if bundle_parameters.shuffle_items:
        # randomizing associated_items
        shuffle(associated_items)

    return associated_items


def get_bundle_bundles_rule_associated_bundles(
    bundle_rule_types, bundle_rules, bundle_id
):
    # initializing associated_bundles
    associated_bundles = []

    # getting bundle bundles rules data
    bundle_bundles_rule_data = []

    bundle_bundles_rule_type_id = [
        obj.id
        for obj in bundle_rule_types
        if obj.codename == BundleRuleTypeStatic.BUNDLE_BUNDLES
    ][0]

    respective_bundle_rule_ids = [
        obj.id
        for obj in bundle_rules
        if obj.bundle_id == bundle_id
        and obj.rule_type_id == bundle_bundles_rule_type_id
    ]
    # there is at least one bundle bundles rule
    if respective_bundle_rule_ids:
        # getting bundle bundles rule data
        bundle_bundles_rule_data = BundleBundlesRule.objects.filter(
            bundle_rule_id__in=respective_bundle_rule_ids
        )
        # building bundle_bundles_rule_ids array
        bundle_bundles_rule_ids = []
        for bundle_bundles_rule in bundle_bundles_rule_data:
            bundle_bundles_rule_ids.append(bundle_bundles_rule.id)

        # getting all_rule_associated_bundle_data
        all_rule_associated_bundle_data = BundleBundlesRuleDataVW.objects.filter(
            bundle_bundles_rule_id__in=bundle_bundles_rule_ids
        ).order_by("display_order")

        # looping in bundle_bundles_rule_data
        for bundle_rule_data in bundle_bundles_rule_data:
            # getting rule related associated bundles
            temp_rule_associated_bundle_ids = [
                obj.bundle_bundles_rule_associations_bundle_id
                for obj in all_rule_associated_bundle_data
                if obj.bundle_bundles_rule_id == bundle_rule_data.id
            ]

            # number of bundle is None (select all bundles)
            if bundle_rule_data.number_of_bundles is None:
                # shuffle bundles is enabled
                if bundle_rule_data.shuffle_bundles:
                    # getting randomly the bundles
                    all_indices = range(len(temp_rule_associated_bundle_ids))
                    random_indices = sample(
                        all_indices, len(temp_rule_associated_bundle_ids)
                    )
                    temp_rule_associated_bundle_ids = [
                        temp_rule_associated_bundle_ids[index]
                        for index in random_indices
                    ]
                    # populating associated_bundles
                    for (
                        temp_rule_associated_bundle_id
                    ) in temp_rule_associated_bundle_ids:
                        associated_bundles.append(temp_rule_associated_bundle_id)

                # shuffle bundles is disabled
                else:
                    # populating associated_bundles
                    for (
                        temp_rule_associated_bundle_id
                    ) in temp_rule_associated_bundle_ids:
                        associated_bundles.append(temp_rule_associated_bundle_id)

            # number of bundle is defined
            else:
                # getting randomly the defined number_of_bundles
                all_indices = range(len(temp_rule_associated_bundle_ids))
                random_indices = sample(all_indices, bundle_rule_data.number_of_bundles)
                sorted_random_indices = random_indices
                # shuffle bundles is disabled (keeping the bundles order)
                if not bundle_rule_data.shuffle_bundles:
                    sorted_random_indices = sorted(random_indices)
                temp_rule_associated_bundle_ids = [
                    temp_rule_associated_bundle_ids[index]
                    for index in sorted_random_indices
                ]
                # populating associated_bundles
                for temp_rule_associated_bundle_id in temp_rule_associated_bundle_ids:
                    associated_bundles.append(temp_rule_associated_bundle_id)

    return associated_bundles


def get_associated_items_and_bundles_from_bundle(
    bundle_id,
    all_bundles_data,
    bundle_rule_types,
    all_bundle_rules,
    all_item_bank_related_bundle_bundles_associations,
):
    # initializing needed arrays
    associated_bundles = []
    # getting bundle rules of current iteration
    bundle_rules = [obj for obj in all_bundle_rules if obj.bundle_id == bundle_id]
    # getting bundle data of current iteration
    bundle_parameters = [obj for obj in all_bundles_data if obj.id == bundle_id][0]

    # getting associated bundles
    associated_bundles = [
        obj.bundle_link_id
        for obj in all_item_bank_related_bundle_bundles_associations
        if obj.bundle_source_id == bundle_id
    ]
    # checking if shuffle bundle is enabled
    if bundle_parameters.shuffle_bundles:
        # shuffling the associated_bundles
        all_indices = range(len(associated_bundles))
        random_indices = sample(all_indices, len(associated_bundles))
        associated_bundles = [associated_bundles[index] for index in random_indices]

    # there is no associated bundles (might have bundle bundles rules)
    if not associated_bundles:
        # getting associated bundles based on bundle bundles rules
        associated_bundles = get_bundle_bundles_rule_associated_bundles(
            bundle_rule_types, bundle_rules, bundle_id
        )

    # getting associated items
    associated_items = get_associated_items_list(
        bundle_id,
        bundle_parameters,
        bundle_rules,
        bundle_rule_types,
    )

    return {
        "associated_items": associated_items,
        "associated_bundles": associated_bundles,
        "bundle_parameters": bundle_parameters,
    }


def deep_get(dictionary, keys, default=None):
    return reduce(
        lambda d, key: d.get(key, default) if isinstance(d, dict) else default,
        keys.split("."),
        dictionary,
    )


def update_bundle_tree_view(
    children_level,
    bundle_tree_view_obj,
    tree_view_dict,
    bundle_id,
    associated_items_and_bundles_from_bundle,
):
    # looping in children_level (starting children_level (cl) at 1)
    # for (cl = 1; cl <= children_level; cl++)
    for cl in range(1, children_level + 1):
        # first children_level
        if cl == 1:
            # accessing children attribute of bundle_tree_view_obj
            bundle_tree_view_obj = deep_get(bundle_tree_view_obj, "children")
        # other children_level iterations
        else:
            # accessing children attribute based on the index of respective level (cl - 1)
            # example: children_level (cl) 3 ==> accessing bundle_tree_view_obj where index of level is 3-1 ==> bundle_tree_view_obj[index_of_level_2]["children"]
            bundle_tree_view_obj = deep_get(
                bundle_tree_view_obj[
                    tree_view_dict["index_of_level_{0}".format(cl - 1)]
                ],
                "children",
            )

    # populating respective children attribute
    bundle_tree_view_obj.append(
        {
            "bundle_id": bundle_id,
            "shuffle_items": associated_items_and_bundles_from_bundle[
                "bundle_parameters"
            ].shuffle_items,
            "shuffle_bundles": associated_items_and_bundles_from_bundle[
                "bundle_parameters"
            ].shuffle_bundles,
            "shuffle_between_bundles": associated_items_and_bundles_from_bundle[
                "bundle_parameters"
            ].shuffle_between_bundles,
            "associated_items": associated_items_and_bundles_from_bundle[
                "associated_items"
            ],
            "children": [],
        },
    )


def get_final_system_ids_array(bundle_tree_view_obj):
    # initializing needed variables
    final_system_ids_array = []
    dynamic_children_array = []

    # adding associated_items of first children level
    for bundle_id in bundle_tree_view_obj["associated_items"]:
        final_system_ids_array.append(bundle_id)

    # looping in first level children
    for child_data in bundle_tree_view_obj["children"]:
        dynamic_children_array.append(child_data)

    # looping in children of current level
    for index, child_data in enumerate(dynamic_children_array):
        # adding associated_items of current child
        for bundle_id in child_data["associated_items"]:
            final_system_ids_array.append(bundle_id)

        # checking if current child has children
        if child_data["children"]:
            # looping in next level children
            for temp_index, temp_child_data in enumerate(child_data["children"]):
                # adding current temp child data to dynamic_children_array
                dynamic_children_array.insert(index + 1 + temp_index, temp_child_data)

    return final_system_ids_array


def get_items_list_from_bundle(item_bank_id, bundle_id, assigned_test_id=None):
    # ==================== VARIABLE INITIALIZATION ====================
    final_system_ids_array = []
    final_items_data_array = []
    associated_items = []
    tree_view_dict = {}
    children_level = 1
    dynamic_associated_bundles = []
    # ==================== VARIABLE INITIALIZATION (END) ====================

    # ==================== DATA GATHERING ====================
    # getting bundle data for the whole item bank
    all_bundles_data = BundleDataVW.objects.filter(item_bank_id=item_bank_id)

    # getting all item bank related bundle IDs
    all_item_bank_related_bundle_ids = Bundles.objects.filter(
        item_bank_id=item_bank_id
    ).values_list("id", flat=True)

    # getting bundle rule types
    bundle_rule_types = BundleRuleType.objects.all()

    # getting bundle rules
    all_bundle_rules = BundleRule.objects.filter(
        bundle_id__in=all_item_bank_related_bundle_ids
    )

    # getting all item bank related item latest versions
    all_item_bank_related_item_latest_versions = (
        ItemLatestVersionsDataVW.objects.filter(item_bank_id=item_bank_id)
    )

    # getting all item bank related bundle bundles associations
    all_item_bank_related_bundle_bundles_associations = (
        BundleBundlesAssociation.objects.filter(
            Q(bundle_source_id__in=all_item_bank_related_bundle_ids)
            | Q(bundle_link_id__in=all_item_bank_related_bundle_ids)
        ).order_by("display_order")
    )
    # ==================== DATA GATHERING (END) ====================

    # ==================== ASSOCIATED ITEMS ====================
    # getting current selected bundle data
    bundle_parameters = [obj for obj in all_bundles_data if obj.id == bundle_id][0]

    # getting bundle rules of current selected bundle
    bundle_rules = BundleRule.objects.filter(bundle_id=bundle_id)

    # getting associated items of current bundle
    associated_items = get_associated_items_list(
        bundle_id, bundle_parameters, bundle_rules, bundle_rule_types
    )

    # initializing bundle_tree_view_obj
    bundle_tree_view_obj = {
        "bundle_id": bundle_id,
        "shuffle_items": bundle_parameters.shuffle_items,
        "shuffle_bundles": bundle_parameters.shuffle_bundles,
        "shuffle_between_bundles": bundle_parameters.shuffle_between_bundles,
        "associated_items": associated_items,
        "children": [],
    }
    # ==================== ASSOCIATED ITEMS (END) ====================

    # ==================== ASSOCIATED BUNDLES ====================
    # initializing needed arrays
    associated_bundles = []
    associated_bundles_array = []
    # getting associated bundles
    associated_bundles = [
        obj.bundle_link_id
        for obj in all_item_bank_related_bundle_bundles_associations
        if obj.bundle_source_id == bundle_id
    ]

    # checking if shuffle bundle is enabled
    if bundle_parameters.shuffle_bundles:
        # shuffling the associated_bundles
        all_indices = range(len(associated_bundles))
        random_indices = sample(all_indices, len(associated_bundles))
        associated_bundles = [associated_bundles[index] for index in random_indices]

    # there is no associated bundles (might have bundle bundles rules)
    if not associated_bundles:
        # getting associated bundles based on bundle bundles rules
        associated_bundles = get_bundle_bundles_rule_associated_bundles(
            bundle_rule_types, bundle_rules, bundle_id
        )

    # populating associated_bundles_array
    for associated_bundle_id in associated_bundles:
        associated_bundles_array.append(associated_bundle_id)

    # populating dynamic_associated_bundles
    for bundle_id in associated_bundles_array:
        dynamic_associated_bundles.append(bundle_id)

    # adding subtracted_snapshot_of_level_1 attribute with its value to tree_view_dict dict
    tree_view_dict[
        "subtracted_snapshot_of_level_{0}".format(children_level)
    ] = dynamic_associated_bundles.copy()

    # looping in associated bundles
    for index, associated_bundle_id in enumerate(dynamic_associated_bundles):
        # adding array of current children level value attribute with its value (value being the a copy of the current dynamic_associated_bundles array) to tree_view_dict dict
        tree_view_dict[
            "array_level_{0}".format(children_level)
        ] = dynamic_associated_bundles.copy()

        # getting all associated items and bundles from bundle of current iteration
        associated_items_and_bundles_from_bundle = (
            get_associated_items_and_bundles_from_bundle(
                associated_bundle_id,
                all_bundles_data,
                bundle_rule_types,
                all_bundle_rules,
                all_item_bank_related_bundle_bundles_associations,
            )
        )

        # looping in range of children_level + 1 (starting index of 1)
        # for (x = 1; x <= children_level; x++)
        for x in range(1, children_level + 1):
            # children_level is 1
            if children_level == 1:
                # adding needed attributes to tree_view_dict dict
                tree_view_dict[
                    "index_of_level_{0}".format(children_level)
                ] = tree_view_dict[
                    "subtracted_snapshot_of_level_{0}".format(children_level)
                ].index(
                    associated_bundle_id
                )
            # children_level greater than 1
            if children_level > 1:
                # ============================================================
                # goal of this logic is to create a snapshot of the bundle IDs (based on the array of current level - array of previous level)
                # example:
                # array_of_level_1: [1, 2, 3, 4]
                # array_of_level_2: [1, 2, 5, 6, 3, 4]
                # subtracted_arrays = array_of_level_2 - array_of_level_1
                # subtracted_arrays = [5, 6]
                # ============================================================

                # initializing subtracted_arrays
                subtracted_arrays = []
                # looping in array of level of current children_level (ex: array_level_2)
                for bundle_id in tree_view_dict[
                    "array_level_{0}".format(children_level)
                ]:
                    # bundle_id of current iteration is not in the array of level of previous children_level (children_level - 1) (ex: array_level_1)
                    if (
                        bundle_id
                        not in tree_view_dict[
                            "array_level_{0}".format(children_level - 1)
                        ]
                    ):
                        # adding bundle_id of current iteration to subtracted_arrays
                        subtracted_arrays.append(bundle_id)

                try:
                    # adding needed attributes to tree_view_dict dict
                    tree_view_dict[
                        "index_of_level_{0}".format(children_level)
                    ] = subtracted_arrays.index(associated_bundle_id)
                    tree_view_dict[
                        "subtracted_snapshot_of_level_{0}".format(children_level)
                    ] = subtracted_arrays
                # last one of the current children path (no more children to add to this current path)
                except:
                    # decrementing children_level
                    children_level -= 1

        try:
            # if snapshot of current children level + 1 exists
            tree_view_dict[
                "subtracted_snapshot_of_level_{0}".format(children_level + 1)
            ]

            # ============================================================
            # goal of this logic is to overwrite snapshot of the current children level (based on the snapshot of current level + 1)
            # this logic needs to be called when were decrementing the children level leving us with the subtracted array excluding the children of the current children level + 1 (level 4 in the following example)
            # example:
            # subtracted_snapshot_of_level_3: [1, 2, 3]
            # subtracted_snapshot_of_level_4: [2]
            # subtracted_arrays = subtracted_snapshot_of_level_3 - subtracted_snapshot_of_level_4
            # subtracted_arrays = [1, 3]
            # ============================================================

            # overriding subtracted snapshot of current decremented level
            subtracted_arrays = []
            for bundle_id in tree_view_dict[
                "subtracted_snapshot_of_level_{0}".format(children_level)
            ]:
                if (
                    bundle_id
                    not in tree_view_dict[
                        "subtracted_snapshot_of_level_{0}".format(children_level + 1)
                    ]
                ):
                    subtracted_arrays.append(bundle_id)
            tree_view_dict[
                "index_of_level_{0}".format(children_level)
            ] = subtracted_arrays.index(associated_bundle_id)
            tree_view_dict[
                "subtracted_snapshot_of_level_{0}".format(children_level)
            ] = subtracted_arrays
        except:
            pass

        # updating the bundle tree view
        # note that the bundle_tree_view_obj is used as reference, so no need to return anything from this function (the bundle_tree_view_obj will be updated automatically in this function)
        update_bundle_tree_view(
            children_level,
            bundle_tree_view_obj,
            tree_view_dict,
            associated_bundle_id,
            associated_items_and_bundles_from_bundle,
        )

        # there are bundle associations
        if associated_items_and_bundles_from_bundle["associated_bundles"]:
            # incrementing children_level
            children_level += 1
            # populating dynamic_associated_bundles (the index where we're adding the values is very important here)
            for bundle_index, bundle_id in enumerate(
                associated_items_and_bundles_from_bundle["associated_bundles"]
            ):
                dynamic_associated_bundles.insert(index + 1 + bundle_index, bundle_id)
        # there are no bundle associations
        else:
            # last element of the snapshot array of the current children level
            if (
                tree_view_dict["index_of_level_{0}".format(children_level)]
                == len(
                    tree_view_dict[
                        "subtracted_snapshot_of_level_{0}".format(children_level)
                    ]
                )
                - 1
            ):
                # decrementing children_level
                children_level -= 1

        # ==================== ASSOCIATED BUNDLES (END) ====================

    # leaving this print there for debugging purposes until we know for sure everything is stable
    # TODO: remove this print once the logic will be fully completed
    print("BUNDLE TREE VIEW OBJ:", bundle_tree_view_obj)

    # getting final_system_ids_array based on provided tree view
    final_system_ids_array = get_final_system_ids_array(bundle_tree_view_obj)

    # looping in final_system_ids_array
    for system_id in final_system_ids_array:
        # getting item data based on system_id of current iteration
        temp_item_data = [
            obj
            for obj in all_item_bank_related_item_latest_versions
            if obj.system_id == system_id
        ]

        # populating final_items_data_array
        # expecting only one item to be found, so populating array with first element of temp_item_data
        final_items_data_array.append(temp_item_data[0])

    # serializing the data
    serialized_data = ItemContentSerializer(
        final_items_data_array,
        many=True,
        context={"assigned_test_id": assigned_test_id},
    ).data
    return serialized_data


def get_item_bank_items_list(
    local_testing, test_section_component_id, assigned_test_id, local_testing_obj={}
):
    # initializing needed variables
    final_items_list = []
    item_ids = []
    test_section_id = None

    # not local testing (real or sample test)
    if not local_testing:
        # assigned test ID is defined (real test)
        if assigned_test_id is not None:
            # getting test section ID based on provided test_section_component_id
            test_section_id = TestSection.objects.get(
                testsectioncomponent=test_section_component_id
            ).id

            # check if a questions list already exists in assignedquestionslist table
            assigned_questions_list = AssignedQuestionsList.objects.filter(
                assigned_test_id=assigned_test_id, test_section_id=test_section_id
            )

            # assigned_questions_list already exists
            if assigned_questions_list:
                # getting respective questions_list (provided as list of strings)
                respective_questions_list_string = (
                    assigned_questions_list.first().questions_list
                )
                # converting respective_questions_list_string (string) in array
                respective_questions_list_array = literal_eval(
                    respective_questions_list_string
                )
                # building the items list based on the found assigned_questions_list
                respective_items = AllItemsDataVW.objects.filter(
                    item_id__in=respective_questions_list_array
                )
                # making sure that the order of the respective_questions_list_array is respected
                for item_from_questions_list_array in respective_questions_list_array:
                    for item_from_all_items_data_vw in respective_items:
                        if (
                            item_from_all_items_data_vw.item_id
                            == item_from_questions_list_array
                        ):
                            final_items_list.append(item_from_all_items_data_vw)
                            continue
                # returning serialized data
                serialized_data = ItemContentSerializer(
                    final_items_list,
                    many=True,
                    context={"assigned_test_id": assigned_test_id},
                ).data
                return serialized_data

        # getting shuffle_question_blocks state (shuffle item bank rules logic)
        shuffle_question_blocks = TestSectionComponent.objects.get(
            id=test_section_component_id
        ).shuffle_question_blocks

        # getting related item bank rules
        item_bank_rules = list(
            ItemBankRule.objects.filter(
                test_section_component_id=test_section_component_id
            ).values("item_bank_id", "item_bank_bundle_id")
        )
    # local testing (called from the test builder)
    else:
        # setting up and formatting variables
        shuffle_question_blocks = local_testing_obj["shuffle_rules"]
        item_bank_rules = list(local_testing_obj["item_bank_rules"])

        # formatting redux data
        for item_bank_rule in item_bank_rules:
            item_bank_rule["item_bank_id"] = item_bank_rule["item_bank"]["value"]
            item_bank_rule["item_bank_bundle_id"] = item_bank_rule["item_bank_bundle"][
                "value"
            ]

    # if shuffle_question_blocks is set to True
    if shuffle_question_blocks:
        # shuffling item bank rules
        shuffle(item_bank_rules)

    # looping in item_bank_rules
    for item_bank_rule in item_bank_rules:
        temp_related_items_list = get_items_list_from_bundle(
            item_bank_rule["item_bank_id"],
            item_bank_rule["item_bank_bundle_id"],
            assigned_test_id,
        )

        # looping in temp_related_items_list
        for item_data in temp_related_items_list:
            # populating final_items_list
            final_items_list.append(item_data)
            # populating item_ids
            item_ids.append(item_data["id"])

    # not local testing +  getting defined assigned_test_id (real test)
    if not local_testing and assigned_test_id is not None:
        # creating new row in assignedquestionslist table
        AssignedQuestionsList.objects.create(
            assigned_test_id=assigned_test_id,
            test_section_id=test_section_id,
            questions_list=item_ids,
            from_item_bank=True,
        )

    return final_items_list


def preview_items_list_from_bundle(request):
    bundle_provided_data = json.loads(request.body)

    # initializing generated_items_list
    generated_items_list = []

    # initializing assertion_error_code
    assertion_error_code = None

    try:
        with transaction.atomic():
            # temporairly upload bundle data (so local changes are considered)
            uploaded_bundle_data = upload_bundle_data(bundle_provided_data, True)

            # updating assertion_error_code value based on returned uploaded bundle data response
            assertion_error_code = uploaded_bundle_data["assertion_error_code"]

            # no validation error or bundle name already exists error (we don't care about that error for the preview functionality)
            if assertion_error_code is None or assertion_error_code == 1:
                # getting generated items list
                generated_items_list = get_items_list_from_bundle(
                    bundle_provided_data["item_bank_id"], bundle_provided_data["id"]
                )

            # triggering assertion error to abort any changes done to the DB
            raise AssertionError("Abort DB Changes")

    except AssertionError:
        # no validation error
        if assertion_error_code is None:
            return Response(generated_items_list)

        # validation error
        elif assertion_error_code == 2:
            return Response(
                {
                    "error": "There is a structural conflict with the bundle associations",
                    "error_code": 2,
                },
                status=status.HTTP_409_CONFLICT,
            )
        elif assertion_error_code == 3:
            return Response(
                {
                    "error": "Duplicate System IDs have been found in the bundle associations",
                    "error_code": 3,
                },
                status=status.HTTP_409_CONFLICT,
            )
        else:
            return Response(
                {
                    "error": "Something wrong happened during the upload bundle process",
                    "error_code": 99,
                },
                status=status.HTTP_400_BAD_REQUEST,
            )


def run_get_items_list_from_bundle(request):
    bundle_rules_provided_data = json.loads(request.body)

    local_testing_obj = {
        "item_bank_rules": bundle_rules_provided_data["item_bank_rules"],
        "shuffle_rules": bundle_rules_provided_data["shuffle_rules"],
    }

    generated_items_list = get_item_bank_items_list(True, None, None, local_testing_obj)

    return Response(generated_items_list)
