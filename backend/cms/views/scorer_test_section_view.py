from django.core.exceptions import ObjectDoesNotExist
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from rest_framework import permissions
from cms.views.retrieve_scorer_test_section_data import (
    retrieve_scorer_test_section_data,
)
from backend.views.utils import is_undefined


# get a read only test section
class ScorerTestSection(APIView):
    def get(self, request):
        assigned_test_id = request.query_params.get("assigned_test_id", None)

        if is_undefined(assigned_test_id):
            return Response(
                {"error": "no 'assigned_test_id' parameter"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        return retrieve_scorer_test_section_data(assigned_test_id)

    def get_permissions(self):
        try:
            # TODO check if test is assigned to scorer
            if True:
                return [permissions.IsAuthenticatedOrReadOnly()]
            # is_public = false
            else:
                return [permissions.IsAuthenticated()]
        # is_test_public does not exist
        except ObjectDoesNotExist:
            return [permissions.IsAdminUser()]


# URL: api/get-scorer-test-section
