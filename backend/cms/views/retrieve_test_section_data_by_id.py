from rest_framework import status
from rest_framework.response import Response
from cms.cms_models.test_section import TestSection
from cms.serializers.test_section_serializer import TestSectionSerializer
from cms.static.test_section_type import TestSectionType
from backend.custom_models.assigned_test_section import AssignedTestSection
from backend.custom_models.assigned_test import AssignedTest
from backend.views.utils import is_undefined
from backend.static.assigned_test_status import AssignedTestStatus


def retrieve_test_section_data_by_id(assigned_test_id, section_id, language):
    context = {}
    context["language"] = language
    context["assigned_test_id"] = assigned_test_id
    # get the assigned test
    assigned_test = AssignedTest.objects.get(id=assigned_test_id)
    # fetch test section data
    test_section = TestSection.objects.get(id=section_id)

    # check if the test is still active or assigned
    if assigned_test.status not in (
        AssignedTestStatus.PRE_TEST,
        AssignedTestStatus.ACTIVE,
        AssignedTestStatus.READY,
        AssignedTestStatus.LOCKED,
        AssignedTestStatus.PAUSED,
    ) and test_section.section_type not in (
        TestSectionType.FINISH,
        TestSectionType.QUIT,
    ):
        return Response(
            {"error": "assigned test is in {} state".format(assigned_test.status)},
            status=status.HTTP_400_BAD_REQUEST,
        )

    # fetch the next section time
    next_test_section = TestSection.objects.filter(
        test_definition_id=assigned_test.test_id, order=test_section.order + 1
    ).first()
    if next_test_section:
        next_section_time = next_test_section.default_time
        next_assigned_test_section = AssignedTestSection.objects.get(
            assigned_test_id=assigned_test.id, test_section=next_test_section
        )
        if next_assigned_test_section.test_section_time:
            next_section_time = next_assigned_test_section.test_section_time
        context["next_test_section_time"] = next_section_time

    # check if time has been updated by TA
    current_test_section_time = AssignedTestSection.objects.get(
        assigned_test_id=assigned_test_id, test_section_id=test_section.id
    ).test_section_time

    # TA did not update the test time for the current section
    if current_test_section_time is None:
        # default time
        current_test_section_time = test_section.default_time

    # providing needed context
    context["current_test_section_time"] = current_test_section_time
    context["assigned_test_id"] = assigned_test_id
    # if next test section exists
    if next_test_section:
        context["next_test_section_id"] = next_test_section.id

    # serialize the data for return
    serialized = TestSectionSerializer(test_section, many=False, context=context).data

    return Response(serialized, status=status.HTTP_200_OK)


def retrieve_public_test_section_data_by_id(section_id, test_definition, language):
    context = {}
    context["language"] = language

    start_time = ""

    # fetch test section data
    test_section = TestSection.objects.get(id=section_id)
    # fetch the next section time
    next_test_section = TestSection.objects.filter(
        test_definition_id=test_definition.id, order=test_section.order + 1
    ).first()
    if next_test_section:
        next_test_section_time = next_test_section.default_time
        if not is_undefined(next_test_section_time):
            context["next_test_section_time"] = next_test_section_time

    serialized = TestSectionSerializer(test_section, many=False, context=context).data
    serialized["start_time"] = start_time

    return Response(serialized, status=status.HTTP_200_OK)
