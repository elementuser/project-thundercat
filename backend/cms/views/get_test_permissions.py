from multiprocessing import context
from operator import or_, and_, itemgetter
from functools import reduce
from urllib import parse
import urllib.parse
from django.db.models import Q
from rest_framework.response import Response
from rest_framework import status
from backend.ref_table_views.cat_ref_departments_vw import CatRefDepartmentsVW
from user_management.serializers.permissions_serializers import (
    GetUsersBasedOnSpecifiedPermissionSerializer,
)
from user_management.user_management_models.custom_user_permissions_model import (
    CustomUserPermissions,
)
from user_management.user_management_models.custom_permissions_model import (
    CustomPermissions,
)
from user_management.user_management_models.user_models import User
from user_management.user_management_models.ta_extended_profile_models import (
    TaExtendedProfile,
)
from user_management.views.utils import CustomPagination
from cms.cms_models.test_permissions_model import TestPermissions
from cms.cms_models.orderless_test_permissions_model import OrderlessTestPermissions
from cms.cms_models.test_definition import TestDefinition
from cms.views.utils import is_undefined, get_user_info_from_jwt_token
from cms.views.utils import get_needed_parameters
from cms.serializers.get_test_information_serializer import (
    GetTestPermissionsSerializer,
    GetOrderlessTestAdministratorSerializer,
    GetOrderlessTestAccessesSerializer,
    GetOrderlessTestPermissionsSerializer,
)


# getting test permissions data for a specified user
def get_test_permissions(request):
    user_info = get_user_info_from_jwt_token(request)
    uit_only = request.query_params.get("uit_only", False)
    test_permissions = []
    if uit_only == "true":
        # get the tests that are UIT
        all_uats = TestDefinition.objects.filter(is_uit=1)
        # filter by username and UIT tests
        test_permissions = TestPermissions.objects.filter(
            username=user_info["username"], test__in=all_uats
        )
    else:
        # filter only by username
        test_permissions = TestPermissions.objects.filter(
            username=user_info["username"]
        )
    # run the serializer and return
    serialized_data = GetTestPermissionsSerializer(test_permissions, many=True).data

    # getting sorted serialized data by test order number (ascending)
    ordered_test_permissions = sorted(
        serialized_data,
        key=lambda k: k["test_order_number"].lower(),
        reverse=False,
    )

    return Response(ordered_test_permissions)


def get_ta_orderless_test_permissions(request):
    # getting user info
    user_info = get_user_info_from_jwt_token(request)
    # initializing orderless_test_permissions
    orderless_test_permissions_array = []
    try:
        # getting TA extended profile ID
        ta_extended_profile_id = TaExtendedProfile.objects.get(
            username_id=user_info["username"]
        ).id
        # getting all related orderless test permissions
        related_orderless_test_permissions = OrderlessTestPermissions.objects.filter(
            ta_extended_profile_id=ta_extended_profile_id
        )
        # looping in related_orderless_test_permissions
        for orderless_test_permissions in related_orderless_test_permissions:
            # getting all active versions (non sample) based on parent/test codes of current orderless_test_permissions
            active_non_sample_versions = TestDefinition.objects.filter(
                parent_code=orderless_test_permissions.parent_code,
                test_code=orderless_test_permissions.test_code,
                is_public=False,
                active=1,
                archived=False,
            )
            # looping in active_non_sample_versions
            for active_non_sample_version in active_non_sample_versions:
                # adding current active_non_sample_version to orderless_test_permissions
                orderless_test_permissions_array.append(active_non_sample_version)

        serialized_data = GetOrderlessTestPermissionsSerializer(
            orderless_test_permissions_array,
            many=True,
            context={"ta_username_id": user_info["username"]},
        ).data
        return Response(serialized_data)
    except TaExtendedProfile.DoesNotExist:
        return Response(
            {"info": "No TA Extended Profile found"}, status=status.HTTP_200_OK
        )

def get_ta_orderless_uit_test_permissions(request):
    # getting user info
    user_info = get_user_info_from_jwt_token(request)
    # initializing orderless_test_permissions
    orderless_test_permissions_array = []
    try:
        # getting TA extended profile ID
        ta_extended_profile_id = TaExtendedProfile.objects.get(
            username_id=user_info["username"]
        ).id
        # getting all related orderless test permissions
        related_orderless_test_permissions = OrderlessTestPermissions.objects.filter(
            ta_extended_profile_id=ta_extended_profile_id
        )
        # looping in related_orderless_test_permissions
        for orderless_test_permissions in related_orderless_test_permissions:
            # getting all active versions (non sample) based on parent/test codes of current orderless_test_permissions
            active_non_sample_versions = TestDefinition.objects.filter(
                parent_code=orderless_test_permissions.parent_code,
                test_code=orderless_test_permissions.test_code,
                is_public=False,
                # added is_uit to True to make sure we only get uit test definitions
                is_uit=True,
                active=1,
                archived=False,
            )
            # looping in active_non_sample_versions
            for active_non_sample_version in active_non_sample_versions:
                # adding current active_non_sample_version to orderless_test_permissions
                orderless_test_permissions_array.append(active_non_sample_version)

        serialized_data = GetOrderlessTestPermissionsSerializer(
            orderless_test_permissions_array,
            many=True,
            context={"ta_username_id": user_info["username"]},
        ).data
        return Response(serialized_data)
    except TaExtendedProfile.DoesNotExist:
        return Response(
            {"info": "No TA Extended Profile found"}, status=status.HTTP_200_OK
        )


# getting specific test permission's financial data
def get_test_permission_financial_data(request):
    user_info = get_user_info_from_jwt_token(request)
    # making sure that we have the needed parameters
    test_order_number = request.query_params.get("test_order_number", None)
    test_to_administer = request.query_params.get("test_to_administer", None)
    if is_undefined(test_order_number):
        return Response(
            {"error": "no 'test_order_number' parameter"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    if is_undefined(test_to_administer):
        return Response(
            {"error": "no 'test_to_administer' parameter"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    try:
        # getting test permission's financial data based on the ta username, test order number and test ID
        test_permission_financial_data = TestPermissions.objects.filter(
            username_id=user_info["username"],
            test_order_number=test_order_number,
            test_id=TestDefinition.objects.get(id=test_to_administer),
        )

        # if queryset is empty
        if not test_permission_financial_data:
            return Response(
                {
                    "error": "the specified test permission based on the paramters does not exist"
                },
                status=status.HTTP_400_BAD_REQUEST,
            )
        # getting serialized test permissions data
        serializer = GetTestPermissionsSerializer(
            test_permission_financial_data, many=True
        )
        return Response(serializer.data)
    except TestDefinition.DoesNotExist:
        return Response(
            {"error": "the specified test does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )


# getting all active test permissions
def get_all_active_test_permissions(request):
    success, parameters = get_needed_parameters(["page", "page_size"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    page, page_size = itemgetter("page", "page_size")(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # get all test permissions
    active_test_permissions = TestPermissions.objects.all()

    # getting serialized active test permissions data
    serialized_active_test_permissions_data = GetTestPermissionsSerializer(
        active_test_permissions, many=True
    ).data

    # getting sorted serialized data by last_name (ascending)
    ordered_active_test_permissions = sorted(
        serialized_active_test_permissions_data,
        key=lambda k: k["last_name"].lower(),
        reverse=False,
    )

    # only getting data for current selected page
    new_active_test_permissions = ordered_active_test_permissions[
        (current_page - 1) * page_size : current_page * page_size
    ]

    # retuning final data using pagination library
    return paginator.get_paginated_response(
        new_active_test_permissions,
        len(active_test_permissions),
        current_page,
        page_size,
    )


def get_found_active_test_permissions(request):
    success, parameters = get_needed_parameters(
        ["keyword", "current_language", "page", "page_size"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    page, page_size, current_language, keyword = itemgetter(
        "page", "page_size", "current_language", "keyword"
    )(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # if blank search
    if keyword == " ":
        found_active_test_permission_ids = TestPermissions.objects.all().values_list(
            "id", flat=True
        )
    # regular search
    else:
        # decode keyword to get special characters
        keyword = parse.unquote(keyword)

        # search for test names
        # search while interface is in English
        if current_language == "en":
            # getting all tests that contains the keyword string (compared with English test name only)
            found_test_names_test_ids = TestDefinition.objects.filter(
                en_name__icontains=keyword
            ).values_list("id", flat=True)
        # search while interface is in French
        else:
            # getting all tests that contains the keyword string (compared with French test name only)
            found_test_names_test_ids = TestDefinition.objects.filter(
                fr_name__icontains=keyword
            ).values_list("id", flat=True)

        # search for usernames (TA Usernames)
        # since there is a comma between last name and first name in active test permissions table, if the user put
        # a comma in one of the search keyword words, remove it
        keyword_without_comma = keyword.replace(",", "")
        # splitting keyword string
        split_keyword = keyword_without_comma.split()

        # if keyword contains more than one word
        if len(split_keyword) > 1:
            # getting all usernames based on matching first and last names
            usernames = User.objects.filter(
                reduce(
                    and_,
                    [
                        Q(first_name__icontains=splitted_keyword)
                        | Q(last_name__icontains=splitted_keyword)
                        for splitted_keyword in split_keyword
                    ],
                )
            ).values_list("username", flat=True)
        # keyword is empty or contains only one word
        else:
            # getting all usernames based on matching first name and/or last name
            usernames = User.objects.filter(
                Q(first_name__icontains=keyword) | Q(last_name__icontains=keyword)
            ).values_list("username", flat=True)

        # search for order numbers
        # getting all test order numbers that contains keyword string
        order_numbers = TestPermissions.objects.filter(
            test_order_number__icontains=keyword
        ).values_list("id", flat=True)

        # search for expiry dates
        expiry_dates = TestPermissions.objects.filter(
            expiry_date__contains=keyword
        ).values_list("id", flat=True)

        # if at least one result has been found based on the keyword provided
        if found_test_names_test_ids or usernames or order_numbers or expiry_dates:
            # initializing arrays
            found_test_permission_ids_based_on_test_names = []
            found_test_permission_ids_based_on_usernames = []
            found_test_permission_ids_based_on_order_numbers = []
            found_test_permission_ids_based_on_expiry_dates = []

            # there is at least one matching test_name
            if found_test_names_test_ids:
                # getting all test_permission_ids based on found_test_names object
                found_test_permission_ids_based_on_test_names = (
                    TestPermissions.objects.filter(
                        test_id__in=found_test_names_test_ids
                    ).values_list("id", flat=True)
                )

            # there is at least one matching name
            if usernames:
                found_test_permission_ids_based_on_usernames = (
                    TestPermissions.objects.filter(
                        username_id__in=usernames
                    ).values_list("id", flat=True)
                )

            # there is at least one matching order number
            if order_numbers:
                found_test_permission_ids_based_on_order_numbers = order_numbers

            # there is at least one matchin expiry date
            if expiry_dates:
                found_test_permission_ids_based_on_expiry_dates = expiry_dates

            # creating combined queryset
            found_active_test_permission_ids = list(
                set(
                    list(found_test_permission_ids_based_on_test_names)
                    + list(found_test_permission_ids_based_on_usernames)
                    + list(found_test_permission_ids_based_on_order_numbers)
                    + list(found_test_permission_ids_based_on_expiry_dates)
                )
            )

        else:
            return Response({"no results found"}, status=status.HTTP_200_OK)

    # getting found active test permissions based on found test permission IDs
    found_active_test_permissions = TestPermissions.objects.filter(
        id__in=found_active_test_permission_ids
    )

    # getting serialized active test permissions data
    serialized_found_active_test_permissions_data = GetTestPermissionsSerializer(
        found_active_test_permissions, many=True
    ).data

    # getting sorted serialized data by last_name (ascending)
    ordered_found_active_test_permissions = sorted(
        serialized_found_active_test_permissions_data,
        key=lambda k: k["last_name"].lower(),
        reverse=False,
    )

    # only getting data for current selected page
    new_found_active_test_permissions = ordered_found_active_test_permissions[
        (current_page - 1) * page_size : current_page * page_size
    ]

    # retuning final data using pagination library
    return paginator.get_paginated_response(
        new_found_active_test_permissions,
        len(found_active_test_permission_ids),
        current_page,
        page_size,
    )


# getting test permissions data for a specified user
def get_selected_user_test_permissions(request):
    username = request.query_params.get("username", None)

    # username is defined
    if username is not None:
        # decoding username that might contain special characters
        username = urllib.parse.unquote(username)

    test_permissions = TestPermissions.objects.filter(username=username)
    serializer = GetTestPermissionsSerializer(test_permissions, many=True)
    return Response(serializer.data)


def get_all_orderless_test_permissions(request):
    success, parameters = get_needed_parameters(["page", "page_size"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    page, page_size = itemgetter("page", "page_size")(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    orderless_test_administrators = TaExtendedProfile.objects.all()

    # getting serialized orderless test permissions data
    serialized_orderless_test_permissions_data = (
        GetOrderlessTestAdministratorSerializer(
            orderless_test_administrators, many=True
        ).data
    )
    # getting sorted serialized data by last_name (ascending)
    ordered_orderless_test_administrators = sorted(
        serialized_orderless_test_permissions_data,
        key=lambda k: k["last_name"].lower(),
        reverse=False,
    )

    # only getting data for current selected page
    new_orderless_test_administrators = ordered_orderless_test_administrators[
        (current_page - 1) * page_size : current_page * page_size
    ]

    # retuning final data using pagination library
    return paginator.get_paginated_response(
        new_orderless_test_administrators,
        len(orderless_test_administrators),
        current_page,
        page_size,
    )


def get_found_orderless_test_administrators(request):
    success, parameters = get_needed_parameters(
        ["keyword", "current_language", "page", "page_size"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    page, page_size, current_language, keyword = itemgetter(
        "page", "page_size", "current_language", "keyword"
    )(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # if blank search
    if keyword == " ":
        found_orderless_test_administrator_ids = (
            TaExtendedProfile.objects.all().values_list("id", flat=True)
        )
    # regular search
    else:
        # decode keyword to get special characters
        keyword = parse.unquote(keyword)

        # search for usernames (TA Usernames)
        # since there is a comma between last name and first name in active test permissions table, if the user put
        # a comma in one of the search keyword words, remove it
        keyword_without_comma = keyword.replace(",", "")
        # splitting keyword string
        split_keyword = keyword_without_comma.split()

        # if keyword contains more than one word
        if len(split_keyword) > 1:
            # getting all usernames based on matching first and last names
            found_usernames = User.objects.filter(
                reduce(
                    and_,
                    [
                        Q(first_name__icontains=splitted_keyword)
                        | Q(last_name__icontains=splitted_keyword)
                        | Q(email__icontains=splitted_keyword)
                        for splitted_keyword in split_keyword
                    ],
                )
            ).values_list("username", flat=True)
        # keyword is empty or contains only one word
        else:
            # getting all usernames based on matching first name and/or last name
            found_usernames = User.objects.filter(
                Q(first_name__icontains=keyword)
                | Q(last_name__icontains=keyword)
                | Q(email__icontains=keyword)
            ).values_list("username", flat=True)

        # search for department abrv and desc
        # search while interface is in English
        if current_language == "en":
            # getting all departments based on matching abrv and/or desc
            found_department_ids = CatRefDepartmentsVW.objects.filter(
                Q(eabrv__icontains=keyword) | Q(edesc__icontains=keyword)
            ).values_list("dept_id", flat=True)

        # search while interface is in French
        else:
            # getting all departments based on matching abrv and/or desc
            found_department_ids = CatRefDepartmentsVW.objects.filter(
                Q(fabrv__icontains=keyword) | Q(fdesc__icontains=keyword)
            ).values_list("dept_id", flat=True)

        # if at least one result has been found based on the keyword provided
        if found_usernames or found_department_ids:
            # initializing arrays
            found_usernames_ta_ext_profile_ids = []
            found_departments_ta_ext_profile_ids = []

            # there is at least one matching name
            if found_usernames:
                found_usernames_ta_ext_profile_ids = TaExtendedProfile.objects.filter(
                    username_id__in=found_usernames
                ).values_list("id", flat=True)

            # there is at least one matching department
            if found_department_ids:
                found_departments_ta_ext_profile_ids = TaExtendedProfile.objects.filter(
                    department_id__in=found_department_ids
                ).values_list("id", flat=True)

            # creating combined queryset
            found_orderless_test_administrator_ids = list(
                set(
                    list(found_usernames_ta_ext_profile_ids)
                    + list(found_departments_ta_ext_profile_ids)
                )
            )

        else:
            return Response({"no results found"}, status=status.HTTP_200_OK)

    # getting found ordlerss test administrators based on found orderless test administrators IDs
    found_orderless_test_administrators = TaExtendedProfile.objects.filter(
        id__in=found_orderless_test_administrator_ids
    )

    # getting serialized active test permissions data
    serialized_found_orderless_test_administrators_data = (
        GetOrderlessTestAdministratorSerializer(
            found_orderless_test_administrators, many=True
        ).data
    )
    # getting sorted serialized data by last_name (ascending)
    ordered_found_orderless_test_administrators = sorted(
        serialized_found_orderless_test_administrators_data,
        key=lambda k: k["last_name"].lower(),
        reverse=False,
    )

    # only getting data for current selected page
    new_found_orderless_test_administrators = (
        ordered_found_orderless_test_administrators[
            (current_page - 1) * page_size : current_page * page_size
        ]
    )

    # retuning final data using pagination library
    return paginator.get_paginated_response(
        new_found_orderless_test_administrators,
        len(found_orderless_test_administrators),
        current_page,
        page_size,
    )


def get_orderless_test_administrators(request):
    # getting permission ID
    permission_id = CustomPermissions.objects.get(
        codename="is_test_administrator"
    ).permission_id

    # getting all test administrators
    test_administrators = CustomUserPermissions.objects.filter(
        permission_id=permission_id
    )

    # initialize final_test_administrators_array
    test_administrators_array = []

    # looping in test_administrators
    for ta in test_administrators:
        # if current TA does not exist in ta_extended_profile table
        if not TaExtendedProfile.objects.filter(username_id=ta.user_id):
            # add it in test_administrators_array
            test_administrators_array.append(ta)

    serializer = GetUsersBasedOnSpecifiedPermissionSerializer(
        test_administrators_array, many=True
    )

    return Response(serializer.data)


def add_orderless_test_administrators(request):
    success, parameters = get_needed_parameters(
        ["test_administrators", "department"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

    # making sure that all provided test_administrators don't already exist in the database (in ta_extended_profile table)
    valid_ta_list = True
    for test_administrator in parameters["test_administrators"]:
        # getting current test administrator
        current_test_administrator = TaExtendedProfile.objects.filter(
            username_id=test_administrator
        )
        # if test administrator already exists
        if current_test_administrator:
            # set valid_ta_list to false
            valid_ta_list = False
            # return 400 with the validation error
            return Response(
                {"validation_error": current_test_administrator.first().username_id},
                status=status.HTTP_400_BAD_REQUEST,
            )

    # if valid test administrator list
    if valid_ta_list:
        # looping in test_administrators
        for test_administrator in parameters["test_administrators"]:
            # adding new entry to the database
            new_ta_entry = TaExtendedProfile.objects.create(
                username_id=test_administrator, department_id=parameters["department"]
            )
            new_ta_entry.save()

    return Response(None)


def get_orderless_test_accesses(request):
    success, parameters = get_needed_parameters(
        ["page", "page_size", "test_administrator"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    page, page_size, test_administrator = itemgetter(
        "page", "page_size", "test_administrator"
    )(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # get all existing test definitions
    all_existing_test_definition_tests = TestDefinition.objects.all()

    # initializing combined_parent_and_test_codes
    combined_parent_and_test_codes = []

    # looping in all_existing_test_definition_tests
    for test in all_existing_test_definition_tests:
        # getting parent code
        parent_code = test.parent_code
        # getting parent code
        test_code = test.test_code
        # concatenating parent code / test code
        combined_parent_and_test_code = parent_code + "/" + test_code
        # if combined parent code + test code not already in combined_parent_and_test_codes
        if combined_parent_and_test_code not in combined_parent_and_test_codes:
            # adding combined parent code + test code to table
            combined_parent_and_test_codes.append(combined_parent_and_test_code)

    # only getting data for current selected page
    new_combined_parent_and_test_codes = combined_parent_and_test_codes[
        (current_page - 1) * page_size : current_page * page_size
    ]

    serialized_data = GetOrderlessTestAccessesSerializer(
        new_combined_parent_and_test_codes,
        many=True,
        context={
            "test_administrator": test_administrator,
        },
    ).data

    # getting sorted serialized data by parent_code (ascending)
    ordered_combined_parent_and_test_codes = sorted(
        serialized_data,
        key=lambda k: k["parent_code"].lower(),
        reverse=False,
    )

    # retuning final data using pagination library
    return paginator.get_paginated_response(
        ordered_combined_parent_and_test_codes,
        len(combined_parent_and_test_codes),
        current_page,
        page_size,
    )


def building_found_test_names_based_on_test_latest_version_description(
    found_test_names,
):
    # initializing combined_parent_test_codes_with_versions_array
    combined_parent_test_codes_with_versions_array = []

    # looping in found_test_names
    for test in found_test_names:
        # initializing add_to_array
        add_to_array = True
        # looping in combined_parent_test_codes_with_versions_array
        for index, combined_object in enumerate(
            combined_parent_test_codes_with_versions_array
        ):
            # building the combined parent/test code string
            combined_parent_test_codes = test.parent_code + "/" + test.test_code
            # if combined parent/test code exists in the array
            if (
                combined_object["combined_parent_test_codes"]
                == combined_parent_test_codes
            ):
                # if version of current combined parent/test code > current test version
                if test.version > combined_object["version"]:
                    # updating the version of the current combined parent/test code
                    combined_parent_test_codes_with_versions_array[index][
                        "version"
                    ] = test.version
                    # setting add_to_array to False
                    add_to_array = False

        # if need to add to the array
        if add_to_array:
            # adding data of current test in combined_parent_test_codes_with_versions_array
            combined_parent_test_codes_with_versions_array.append(
                {
                    "combined_parent_test_codes": test.parent_code
                    + "/"
                    + test.test_code,
                    "version": test.version,
                }
            )

    # looping in combined_parent_test_codes_with_versions_array
    for index, data in enumerate(combined_parent_test_codes_with_versions_array):
        # getting parent code
        parent_code = data["combined_parent_test_codes"].split("/")[0]
        # getting test code
        test_code = data["combined_parent_test_codes"].split("/")[1]
        # checking if version of current data is the latest existing version
        latest_version = (
            TestDefinition.objects.filter(parent_code=parent_code, test_code=test_code)
            .order_by("version")
            .last()
            .version
        )
        if data["version"] < latest_version:
            # removing it from the array
            combined_parent_test_codes_with_versions_array.pop(index)

    # retuning final array
    return combined_parent_test_codes_with_versions_array


def get_found_orderless_test_accesses(request):
    success, parameters = get_needed_parameters(
        ["keyword", "current_language", "page", "page_size", "test_administrator"],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    keyword, current_language, page, page_size, test_administrator = itemgetter(
        "keyword", "current_language", "page", "page_size", "test_administrator"
    )(parameters)

    # decoding username that might contain special characters
    test_administrator = urllib.parse.unquote(test_administrator)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # get all existing test definitions
    all_existing_test_definition_tests = TestDefinition.objects.all()

    # if blank search
    if keyword == " ":
        # initializing combined_parent_and_test_codes
        combined_parent_and_test_codes = []

        # looping in all_existing_test_definition_tests
        for test in all_existing_test_definition_tests:
            # getting parent code
            parent_code = test.parent_code
            # getting parent code
            test_code = test.test_code
            # concatenating parent code / test code
            combined_parent_and_test_code = parent_code + "/" + test_code
            # if combined parent code + test code not already in combined_parent_and_test_codes
            if combined_parent_and_test_code not in combined_parent_and_test_codes:
                # adding combined parent code + test code to table
                combined_parent_and_test_codes.append(combined_parent_and_test_code)

        found_orderlesss_test_accesses = combined_parent_and_test_codes
    # regular search
    else:
        # decode keyword to get special characters
        keyword = parse.unquote(keyword)

        # since there is a dash between parent code and test code in orderless test accesses table, if the user put
        # a dash in one of the search keyword words, remove it
        keyword_without_comma = keyword.replace(" - ", " ")
        # splitting keyword string
        split_keyword = keyword_without_comma.split()

        # if keyword contains more than one word
        if len(split_keyword) > 1:
            # search for parent/test codes
            # getting all test definitions based on matching parent and test codes
            found_parent_test_codes = TestDefinition.objects.filter(
                reduce(
                    and_,
                    [
                        Q(parent_code=splitted_keyword)
                        | Q(test_code__icontains=splitted_keyword)
                        for splitted_keyword in split_keyword
                    ],
                )
            )
        # keyword is empty or contains only one word
        else:
            # getting all usernames based on matching first name and/or last name
            found_parent_test_codes = TestDefinition.objects.filter(
                Q(parent_code__icontains=keyword) | Q(test_code__icontains=keyword)
            )

        # search for test description
        # search while interface is in English
        if current_language == "en":
            # getting all tests that contains the keyword string (compared with English test name only)
            # and at least one of the test ids from tests object
            found_test_names = TestDefinition.objects.filter(
                reduce(
                    or_,
                    # TODO (consider name of latest version)
                    [
                        Q(id=test.id, en_name__icontains=keyword)
                        for test in all_existing_test_definition_tests
                    ],
                )
            )

            # overriding found_test_names with new built array
            found_test_names = (
                building_found_test_names_based_on_test_latest_version_description(
                    found_test_names
                )
            )
        # search while interface is in French
        else:
            # getting all tests that contains the keyword string (compared with French test name only)
            # and at least one of the test ids from tests object
            found_test_names = TestDefinition.objects.filter(
                reduce(
                    or_,
                    [
                        Q(id=test.id, fr_name__icontains=keyword)
                        for test in all_existing_test_definition_tests
                    ],
                )
            )

            # overriding found_test_names with new built array
            found_test_names = (
                building_found_test_names_based_on_test_latest_version_description(
                    found_test_names
                )
            )

        # if at least one result has been found based on the keyword provided
        if found_parent_test_codes.exists() or len(found_test_names) > 0:
            # initializing combined_parent_and_test_codes
            combined_parent_and_test_codes = []

            # there is at least one matching name
            if found_parent_test_codes.exists():
                # looping in found_parent_test_codes
                for test in found_parent_test_codes:
                    # getting parent code
                    parent_code = test.parent_code
                    # getting parent code
                    test_code = test.test_code
                    # concatenating parent code / test code
                    combined_parent_and_test_code = parent_code + "/" + test_code
                    # if combined parent code + test code not already in combined_parent_and_test_codes
                    if (
                        combined_parent_and_test_code
                        not in combined_parent_and_test_codes
                    ):
                        # adding combined parent code + test code to table
                        combined_parent_and_test_codes.append(
                            combined_parent_and_test_code
                        )

            # there is at least one matching name
            if len(found_test_names) > 0:
                # looping in found_test_names
                for test in found_test_names:
                    # if combined parent code + test code not already in combined_parent_and_test_codes
                    if (
                        test["combined_parent_test_codes"]
                        not in combined_parent_and_test_codes
                    ):
                        # adding combined parent code + test code to table
                        combined_parent_and_test_codes.append(
                            test["combined_parent_test_codes"]
                        )

        else:
            return Response({"no results found"}, status=status.HTTP_200_OK)

    # only getting data for current selected page
    new_combined_parent_and_test_codes = combined_parent_and_test_codes[
        (current_page - 1) * page_size : current_page * page_size
    ]

    serialized_data = GetOrderlessTestAccessesSerializer(
        new_combined_parent_and_test_codes,
        many=True,
        context={
            "test_administrator": test_administrator,
        },
    ).data

    # getting sorted serialized data by parent_code (ascending)
    ordered_combined_parent_and_test_codes = sorted(
        serialized_data,
        key=lambda k: k["parent_code"].lower(),
        reverse=False,
    )

    # retuning final data using pagination library
    return paginator.get_paginated_response(
        ordered_combined_parent_and_test_codes,
        len(combined_parent_and_test_codes),
        current_page,
        page_size,
    )


def add_delete_orderless_test_access(request):
    # add_test_access set to True ==> add new orderless test access
    # add_test_access set to False ==> delete existing orderless test access
    success, parameters = get_needed_parameters(
        ["parent_code", "test_code", "ta_extended_profile_id", "add_test_access"],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    parent_code, test_code, ta_extended_profile_id, add_test_access = itemgetter(
        "parent_code", "test_code", "ta_extended_profile_id", "add_test_access"
    )(parameters)

    # if adding new test access (add_test_access = True)
    if add_test_access == True:
        try:
            new_orderless_test_access = OrderlessTestPermissions.objects.create(
                parent_code=parent_code,
                test_code=test_code,
                ta_extended_profile_id=ta_extended_profile_id,
            )
            new_orderless_test_access.save()
            return Response(status=status.HTTP_200_OK)
        # should never happen
        except:
            return Response(
                {
                    "error:": "something happened during the add orderless test access process"
                },
                status=status.HTTP_400_BAD_REQUEST,
            )
    # if deleting existing test access (add_test_access = False)
    else:
        try:
            existing_orderless_test_access = OrderlessTestPermissions.objects.get(
                parent_code=parent_code,
                test_code=test_code,
                ta_extended_profile_id=ta_extended_profile_id,
            )
            existing_orderless_test_access.delete()
            return Response(status=status.HTTP_200_OK)
        # should never happen
        except:
            return Response(
                {
                    "error:": "something happened during the delete orderless test access process"
                },
                status=status.HTTP_400_BAD_REQUEST,
            )


def delete_orderless_test_administrator(request):
    success, parameters = get_needed_parameters(
        ["test_administrator"],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    test_administrator = itemgetter("test_administrator")(parameters)

    # getting ta extended profile ID
    ta_extended_profile = TaExtendedProfile.objects.get(username_id=test_administrator)
    # getting related orderless test permissions
    order_test_accesses = OrderlessTestPermissions.objects.filter(
        ta_extended_profile_id=ta_extended_profile.id
    )

    # looping in order_test_accesses
    for test_access in order_test_accesses:
        # deleting current test access
        test_access.delete()

    # deleting related ta extended profile data
    ta_extended_profile.delete()

    return Response(None)


def update_orderless_test_administrator_department(request):
    success, parameters = get_needed_parameters(
        ["test_administrator", "dept_id"],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    test_administrator, dept_id = itemgetter("test_administrator", "dept_id")(
        parameters
    )

    try:
        # getting related ta extended profile data
        ta_extended_profile = TaExtendedProfile.objects.get(
            username_id=test_administrator
        )

        # updating department ID
        ta_extended_profile.department_id = dept_id
        ta_extended_profile.save()

        return Response(status=status.HTTP_200_OK)

    except TaExtendedProfile.DoesNotExist:
        return Response(
            {
                "error:": "unable to find the TA extended profile data based on provided TA"
            },
            status=status.HTTP_400_BAD_REQUEST,
        )
