from operator import itemgetter
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from rest_framework import permissions
from cms.serializers.test_section_serializer import TestSectionTitleDescSerializer
from cms.cms_models.test_section import TestSection
from cms.views.utils import get_needed_parameters
from cms.serializers.get_test_information_serializer import (
    GetActiveNonPublicTestsSerializer,
)
from cms.cms_models.test_definition import TestDefinition
from backend.api_permissions.role_based_api_permissions import (
    HasSystemAdminPermission,
    HasAaePermission,
)

# getting non public tests (where is_public is set to false)
class GetActiveNonPublicTests(APIView):
    def get(self, request):
        return Response(get_active_non_public_tests())

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


def get_active_non_public_tests():
    active_non_public_test_name = TestDefinition.objects.filter(
        is_public=False, active=True, archived=False
    )
    serialized = GetActiveNonPublicTestsSerializer(
        active_non_public_test_name, many=True
    )
    return serialized.data


# getting default test time
class GetTestData(APIView):
    def get(self, request):
        return get_test_data(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasAaePermission()]


def get_test_data(request):
    success, parameters = get_needed_parameters(["test_definition_id"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    test_definition_id = itemgetter("test_definition_id")(parameters)

    # initializing default_test_time
    default_test_time = 0

    # getting timed test sections
    timed_test_sections = TestSection.objects.filter(
        test_definition_id=test_definition_id, default_time__isnull=False
    )

    # looping in timed_test_sections
    for timed_test_section in timed_test_sections:
        # incrementing default_test_time
        default_test_time += timed_test_section.default_time

    # serializing the timed test sections
    serialized_test_sections = TestSectionTitleDescSerializer(
        timed_test_sections, many=True
    ).data

    return Response(
        {
            "default_test_time": default_test_time,
            "test_sections": serialized_test_sections,
        }
    )
