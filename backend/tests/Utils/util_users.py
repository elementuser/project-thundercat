import pytz
import json
from user_management.user_management_models.user_models import User
from datetime import datetime

# Default variables
DEFAULT_PASSWORD = "123test"
DEFAULT_FIRST_NAME = "Name"
DEFAULT_LAST_NAME = "LastName"
DEFAULT_BIRTH_DATE = "1900-01-01"
DEFAULT_SECONDARY_EMAIL = "email@email.ca"
DEFAULT_PRI = "123456789"
DEFAULT_LAST_PASSWORD_CHANGE = None
DEFAULT_IS_STAFF = False
DEFAULT_PSRS_APPLICANT_ID = None
DEFAULT_LAST_LOGIN = datetime.now(tz=pytz.UTC)
DEFAULT_IS_PROFILE_COMPLETE = True


# -------------------------------------------------------------------------------------------------#
# Create User
# -------------------------------------------------------------------------------------------------#
# Function Used to Create a User
# -------------------------------------------------------------------------------------------------#
# Username Required - With Optional Parameters For Other Fields
# -------------------------------------------------------------------------------------------------#
def create_user(
    username,
    first_name=DEFAULT_FIRST_NAME,
    last_name=DEFAULT_LAST_NAME,
    birth_date=DEFAULT_BIRTH_DATE,
    secondary_email=DEFAULT_SECONDARY_EMAIL,
    pri=DEFAULT_PRI,
    last_password_change=DEFAULT_LAST_PASSWORD_CHANGE,
    is_staff=DEFAULT_IS_STAFF,
    psrs_applicant_id=DEFAULT_PSRS_APPLICANT_ID,
    last_login=DEFAULT_LAST_LOGIN,
    is_profile_complete=DEFAULT_IS_PROFILE_COMPLETE,
    password=DEFAULT_PASSWORD,
):
    user = User.objects.create(
        username=username,
        first_name=first_name,
        last_name=last_name,
        birth_date=birth_date,
        email=username,
        secondary_email=secondary_email,
        pri=pri,
        last_password_change=last_password_change,
        is_staff=is_staff,
        psrs_applicant_id=psrs_applicant_id,
        last_login=last_login,
        is_profile_complete=is_profile_complete,
    )
    # We set the password here since, if we set it up with the user creation,
    # it encrypts the password and we can't use our password string to authenticate
    user.set_password(password)
    user.save()

    return user


# -------------------------------------------------------------------------------------------------#
# Create Access Token
# -------------------------------------------------------------------------------------------------#
# Function Used Get Auth Token to Access Endpoints
# -------------------------------------------------------------------------------------------------#
# Self & Username Required - With Optional Password
# -------------------------------------------------------------------------------------------------#
def create_access_token(self, username, password=DEFAULT_PASSWORD):
    return self.client.post(
        "/oec-cat/api/auth/jwt/create_token/",
        data=json.dumps(
            {
                "username": str(username),
                "password": str(password),
            }
        ),
        content_type="application/json",
    ).data["access"]


# -------------------------------------------------------------------------------------------------#
# Create Token
# -------------------------------------------------------------------------------------------------#
# Function Used Get Auth Token Object (Complete) to Access Endpoints
# -------------------------------------------------------------------------------------------------#
# Self & Username Required - With Optional Password
# -------------------------------------------------------------------------------------------------#
def create_token(self, username, password=DEFAULT_PASSWORD):
    return self.client.post(
        "/oec-cat/api/auth/jwt/create_token/",
        data=json.dumps(
            {
                "username": str(username),
                "password": str(password),
            }
        ),
        content_type="application/json",
    ).data
