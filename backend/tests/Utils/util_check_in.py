from tests.Utils.util_print import (
    print_ok_level_3,
    print_level_1,
    print_level_2,
)
from backend.views.room_check_in import check_into_room
from rest_framework import status

# -------------------------------------------------------------------------------------------------#
# Checking-In Single User
# -------------------------------------------------------------------------------------------------#
# Function Used to Check In a User to a Test with a Test Access Code
# -------------------------------------------------------------------------------------------------#
def checking_in_single_user_with_uit_access_code(self, username, test_access_code):
    print_level_1("Checking-in a single user to the test: ")

    print_level_2("User, {0}: ".format(username))

    response_tester_user_1 = check_into_room(test_access_code, username)

    self.assertEqual(response_tester_user_1.status_code, status.HTTP_200_OK)

    print_ok_level_3()


# -------------------------------------------------------------------------------------------------#
# Checking-In Users
# -------------------------------------------------------------------------------------------------#
# Function Used to Check In Multiple Users to a Test with Test Access Codes
# -------------------------------------------------------------------------------------------------#
# Format for users_and_test_access_codes:
# [
#   {
#       "username": "username",
#       "test_access_code": "test_access_code",
#   },
#   {
#       "username": "username",
#       "test_access_code": "test_access_code",
#   },
# ]
# -------------------------------------------------------------------------------------------------#
def checking_in_users_with_uit_access_codes(self, users_and_test_access_codes):
    print_level_1("Checking-in users to the test: ")

    for user_and_access_code in users_and_test_access_codes:
        print_level_2("User, {0}: ".format(user_and_access_code["username"]))

        response_tester_user_1 = check_into_room(
            user_and_access_code["test_access_code"], user_and_access_code["username"]
        )

        self.assertEqual(response_tester_user_1.status_code, status.HTTP_200_OK)

        print_ok_level_3()
