from backend.celery.task_definition.utils import (
    ConsoleMessageColor,
    ConsoleColorCombo,
)

LINE_LENGHT = 100

# -------------------------------------------------------------------------------------------------#
# Print - Setup - Start
# -------------------------------------------------------------------------------------------------#
# Used to Print a Green Block Before the Setup
# -------------------------------------------------------------------------------------------------#
def print_setup_start(folder_name, file_name):
    first_line = add_spaces("# Setting Up Data - Folder: {0}".format(folder_name))
    second_line = add_spaces("# File --> {0}".format(file_name))

    print(
        "\n{0}{1}{2}".format(
            ConsoleColorCombo.GREEN_BACK_WHITE_TEXT,
            add_characters("#"),
            ConsoleColorCombo.DEFAULT_FORMAT,
        ),
        "\n{0}{1}{2}".format(
            ConsoleColorCombo.GREEN_BACK_WHITE_TEXT,
            first_line,
            ConsoleColorCombo.DEFAULT_FORMAT,
        ),
        "\n{0}{1}{2}".format(
            ConsoleColorCombo.GREEN_BACK_WHITE_TEXT,
            add_characters("-"),
            ConsoleColorCombo.DEFAULT_FORMAT,
        ),
        "\n{0}{1}{2}".format(
            ConsoleColorCombo.GREEN_BACK_WHITE_TEXT,
            second_line,
            ConsoleColorCombo.DEFAULT_FORMAT,
        ),
        "\n{0}{1}{2}".format(
            ConsoleColorCombo.GREEN_BACK_WHITE_TEXT,
            add_characters("#"),
            ConsoleColorCombo.DEFAULT_FORMAT,
        ),
        "\n\n",
    )


# -------------------------------------------------------------------------------------------------#
# Print - Setup - End
# -------------------------------------------------------------------------------------------------#
# Used to Print a Green Block After the Setup
# -------------------------------------------------------------------------------------------------#
def print_setup_end():
    print(
        "{0}{1}{2}\n\n\n".format(
            ConsoleColorCombo.GREEN_BACK_WHITE_TEXT,
            add_characters("#"),
            ConsoleColorCombo.DEFAULT_FORMAT,
        )
    )


# -------------------------------------------------------------------------------------------------#
# Print - Task - Start
# -------------------------------------------------------------------------------------------------#
# Used to Print a Blue Block Before Executing a Task
# -------------------------------------------------------------------------------------------------#
def print_task_start(folder_name, file_name, function_name):
    first_line = add_spaces("# Unit Tests - Folder: {0}".format(folder_name))
    second_line = add_spaces(
        "# File: {0} --> Function: {1}".format(file_name, function_name)
    )
    print(
        "\n{0}{1}{2}".format(
            ConsoleColorCombo.BLUE_BACK_WHITE_TEXT,
            add_characters("#"),
            ConsoleColorCombo.DEFAULT_FORMAT,
        ),
        "\n{0}{1}{2}".format(
            ConsoleColorCombo.BLUE_BACK_WHITE_TEXT,
            first_line,
            ConsoleColorCombo.DEFAULT_FORMAT,
        ),
        "\n{0}{1}{2}".format(
            ConsoleColorCombo.BLUE_BACK_WHITE_TEXT,
            add_characters("-"),
            ConsoleColorCombo.DEFAULT_FORMAT,
        ),
        "\n{0}{1}{2}".format(
            ConsoleColorCombo.BLUE_BACK_WHITE_TEXT,
            second_line,
            ConsoleColorCombo.DEFAULT_FORMAT,
        ),
        "\n{0}{1}{2}".format(
            ConsoleColorCombo.BLUE_BACK_WHITE_TEXT,
            add_characters("#"),
            ConsoleColorCombo.DEFAULT_FORMAT,
        ),
        "\n\n",
    )


# -------------------------------------------------------------------------------------------------#
# Print - Task - Start
# -------------------------------------------------------------------------------------------------#
# Used to Print a Blue Block After Executing a Task
# -------------------------------------------------------------------------------------------------#
def print_task_end():
    print(
        "{0}{1}{2}\n\n\n".format(
            ConsoleColorCombo.BLUE_BACK_WHITE_TEXT,
            add_characters("#"),
            ConsoleColorCombo.DEFAULT_FORMAT,
        )
    )


# -------------------------------------------------------------------------------------------------#
# Print - Sub-Task - Start
# -------------------------------------------------------------------------------------------------#
# Used to Print Green Text Before Executing a Sub-Task
# -------------------------------------------------------------------------------------------------#
def print_sub_task_start(text):
    print("{0} ----- BEGIN - {1} ---".format(ConsoleMessageColor.GREEN, text))


# -------------------------------------------------------------------------------------------------#
# Print - Sub-Task - End
# -------------------------------------------------------------------------------------------------#
# Used to Print Blue Text After Executing a Sub-Task
# -------------------------------------------------------------------------------------------------#
def print_sub_task_end(text):
    print("{0} ----- END - {1} ---\n".format(ConsoleMessageColor.CYAN, text))


# -------------------------------------------------------------------------------------------------#
# Print - Level 1
# -------------------------------------------------------------------------------------------------#
# Format (In White Text):
#   --> "Text"
# -------------------------------------------------------------------------------------------------#
def print_level_1(text):
    print("{0} \t--> {1}".format(ConsoleMessageColor.WHITE, text))


# -------------------------------------------------------------------------------------------------#
# Print - Level 2
# -------------------------------------------------------------------------------------------------#
# Format (In White Text):
#       --> "Text"
# -------------------------------------------------------------------------------------------------#
def print_level_2(text):
    print("{0} \t\t--> {1}".format(ConsoleMessageColor.WHITE, text))


# -------------------------------------------------------------------------------------------------#
# Print - Level 3
# -------------------------------------------------------------------------------------------------#
# Format (In White Text):
#           --> "Text"
# -------------------------------------------------------------------------------------------------#
def print_level_3(text):
    print("{0} \t\t\t--> {1}".format(ConsoleMessageColor.WHITE, text))


# -------------------------------------------------------------------------------------------------#
# Print - Level 4
# -------------------------------------------------------------------------------------------------#
# Format (In White Text):
#               --> "Text"
# -------------------------------------------------------------------------------------------------#
def print_level_4(text):
    print("{0} \t\t\t\t--> {1}".format(ConsoleMessageColor.WHITE, text))


# -------------------------------------------------------------------------------------------------#
# Print - Level 5
# -------------------------------------------------------------------------------------------------#
# Format (In White Text):
#                   --> "Text"
# -------------------------------------------------------------------------------------------------#
def print_level_5(text):
    print("{0} \t\t\t\t\t--> {1}".format(ConsoleMessageColor.WHITE, text))


# -------------------------------------------------------------------------------------------------#
# Print - Level 6
# -------------------------------------------------------------------------------------------------#
# Format (In White Text):
#                       --> "Text"
# -------------------------------------------------------------------------------------------------#
def print_level_6(text):
    print("{0} \t\t\t\t\t\t--> {1}".format(ConsoleMessageColor.WHITE, text))


# -------------------------------------------------------------------------------------------------#
# Print - OK -Level 1
# -------------------------------------------------------------------------------------------------#
# Format (In Green Text):
#   --> OK
# -------------------------------------------------------------------------------------------------#
def print_ok_level_1():
    print("{0} \t--> OK".format(ConsoleMessageColor.GREEN))


# -------------------------------------------------------------------------------------------------#
# Print - OK -Level 2
# -------------------------------------------------------------------------------------------------#
# Format (In Green Text):
#       --> OK
# -------------------------------------------------------------------------------------------------#
def print_ok_level_2():
    print("{0} \t\t--> OK".format(ConsoleMessageColor.GREEN))


# -------------------------------------------------------------------------------------------------#
# Print - OK -Level 3
# -------------------------------------------------------------------------------------------------#
# Format (In Green Text):
#           --> OK
# -------------------------------------------------------------------------------------------------#
def print_ok_level_3():
    print("{0} \t\t\t--> OK".format(ConsoleMessageColor.GREEN))


# -------------------------------------------------------------------------------------------------#
# Print - OK -Level 4
# -------------------------------------------------------------------------------------------------#
# Format (In Green Text):
#               --> OK
# -------------------------------------------------------------------------------------------------#
def print_ok_level_4():
    print("{0} \t\t\t\t--> OK".format(ConsoleMessageColor.GREEN))


# -------------------------------------------------------------------------------------------------#
# Print - OK -Level 5
# -------------------------------------------------------------------------------------------------#
# Format (In Green Text):
#                   --> OK
# -------------------------------------------------------------------------------------------------#
def print_ok_level_5():
    print("{0} \t\t\t\t\t--> OK".format(ConsoleMessageColor.GREEN))


# -------------------------------------------------------------------------------------------------#
# Print - OK -Level 6
# -------------------------------------------------------------------------------------------------#
# Format (In Green Text):
#                       --> OK
# -------------------------------------------------------------------------------------------------#
def print_ok_level_6():
    print("{0} \t\t\t\t\t\t--> OK".format(ConsoleMessageColor.GREEN))


# -------------------------------------------------------------------------------------------------#
# Print - OK -Level 7
# -------------------------------------------------------------------------------------------------#
# Format (In Green Text):
#                           --> OK
# -------------------------------------------------------------------------------------------------#
def print_ok_level_7():
    print("{0} \t\t\t\t\t\t\t--> OK".format(ConsoleMessageColor.GREEN))


# -------------------------------------------------------------------------------------------------#
# Add Spaces
# -------------------------------------------------------------------------------------------------#
# Adds spaces until we reach LINE_LENGHT (adds # at the end)
# -------------------------------------------------------------------------------------------------#
def add_spaces(text):
    current_length = len(text)
    final_text = text
    spaces_nbr = LINE_LENGHT - current_length

    for i in range(spaces_nbr):
        if i != spaces_nbr - 1:
            final_text += " "
        else:
            final_text += "#"

    return final_text


# -------------------------------------------------------------------------------------------------#
# Add Characters
# -------------------------------------------------------------------------------------------------#
# Adds the specified character until we get to LINE_LENGHT (starts with # and finishes with #)
# -------------------------------------------------------------------------------------------------#
def add_characters(char):
    final_text = "#"

    for i in range(LINE_LENGHT - 1):
        if i != LINE_LENGHT - 2:
            final_text += char
        else:
            final_text += "#"

    return final_text
