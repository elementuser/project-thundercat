import pytz
from cms.cms_models.test_permissions_model import TestPermissions
from user_management.user_management_models.custom_permissions_model import (
    CustomPermissions,
)
from user_management.static.permission import Permission
from user_management.user_management_models.permission_request_model import (
    PermissionRequest,
)
from user_management.user_management_models.custom_user_permissions_model import (
    CustomUserPermissions,
)
from datetime import datetime
from tests.Utils.util_users import (
    DEFAULT_FIRST_NAME,
    DEFAULT_SECONDARY_EMAIL,
    DEFAULT_PRI,
)
from tests.Utils.util_print import (
    print_level_1,
    print_level_2,
)
from dateutil.relativedelta import relativedelta

# used when you want a for loop of the custom permissions IDs
NBR_OF_CUSTOM_PERMISSIONS = 0

DEFAULT_DATE = datetime.now(tz=pytz.UTC)
DEFAULT_EN_DESCRIPTION = "EN"
DEFAULT_FR_NAME = "FR"
DEFAULT_FR_DESCRIPTION = "FR"
DEFAULT_CONTENT_TYPE_ID = 5


# -------------------------------------------------------------------------------------------------#
# Get Total Number of Custom Permissions in the Database
# -------------------------------------------------------------------------------------------------#
def get_number_of_custom_permissions():
    return CustomPermissions.objects.all().count()


# -------------------------------------------------------------------------------------------------#
# Create All Custom Permissions
# -------------------------------------------------------------------------------------------------#
# It Creates All Custom Permissions Needed for the Application
# -------------------------------------------------------------------------------------------------#
# IMPORTANT - Has to be updated every time we create a new type of custom permission
# -------------------------------------------------------------------------------------------------#
def create_all_custom_permissions():
    # create custom permissions data
    print_level_1("Creating Custom Permissions' Data")

    # Custom Permission - Test Administrator
    create_custom_permission(
        permission_id=1,
        en_name="Test Administrator",
        codename=Permission.TEST_ADMINISTRATOR,
    )

    # Custom Permission - Operations Level 1
    create_custom_permission(
        permission_id=2,
        en_name="Operations Level 1",
        codename=Permission.SYSTEM_ADMINISTRATOR,
    )

    # Custom Permission - PPC R&D Level 1
    create_custom_permission(
        permission_id=3,
        en_name="PPC R&D Level 1",
        codename=Permission.PPC_ADMINISTRATOR,
    )

    # Custom Permission - Test Scorer
    create_custom_permission(
        permission_id=4,
        en_name="Test Scorer",
        codename=Permission.SCORER,
    )

    # Custom Permission - Test Builder
    create_custom_permission(
        permission_id=5,
        en_name="Test Builder",
        codename=Permission.TEST_BUILDER,
    )

    # Custom Permission - Test Adaptations
    create_custom_permission(
        permission_id=6,
        en_name="Test Adaptations",
        codename=Permission.AAE,
    )

    # Custom Permission - R&D Operations
    create_custom_permission(
        permission_id=7,
        en_name="R&D Operations",
        codename=Permission.RD_OPERATIONS,
    )

    # Custom Permission - Test Developer
    create_custom_permission(
        permission_id=8,
        en_name="Test Developer",
        codename=Permission.TEST_DEVELOPER,
    )


# -------------------------------------------------------------------------------------------------#
# Create Custom Permission
# -------------------------------------------------------------------------------------------------#
# En_Name & Codename Required - With Optional Parameters For Other Fields
# -------------------------------------------------------------------------------------------------#
# If we don't want to specify the ID, the function will use NBR_OF_CUSTOM_PERMISSIONS+1 by default
# -------------------------------------------------------------------------------------------------#
def create_custom_permission(
    en_name,
    codename,
    permission_id=NBR_OF_CUSTOM_PERMISSIONS + 1,
    content_type_id=DEFAULT_CONTENT_TYPE_ID,
    fr_name=DEFAULT_FR_NAME,
    en_description=DEFAULT_EN_DESCRIPTION,
    fr_description=DEFAULT_FR_DESCRIPTION,
):
    new_permission = CustomPermissions.objects.create(
        permission_id=permission_id,
        en_name=en_name,
        fr_name=fr_name,
        en_description=en_description,
        fr_description=fr_description,
        codename=codename,
        content_type_id=content_type_id,
    )

    print_level_2(
        "ID: {0} \tPermission Name: {1}".format(
            new_permission.permission_id, new_permission.en_name
        )
    )


# -------------------------------------------------------------------------------------------------#
# Create Permission Request
# -------------------------------------------------------------------------------------------------#
# Username & CustomPermission Required - With Optional Parameters For Other Fields
# -------------------------------------------------------------------------------------------------#
def create_permission_request(
    username,
    permission_requested,
    goc_email=DEFAULT_SECONDARY_EMAIL,
    pri_or_military_nbr=DEFAULT_PRI,
    supervisor=DEFAULT_FIRST_NAME,
    supervisor_email=DEFAULT_SECONDARY_EMAIL,
    rationale=DEFAULT_FIRST_NAME,
    request_date=DEFAULT_DATE,
):
    new_permission_request = PermissionRequest.objects.create(
        goc_email=goc_email,
        pri_or_military_nbr=pri_or_military_nbr,
        supervisor=supervisor,
        supervisor_email=supervisor_email,
        rationale=rationale,
        permission_requested=permission_requested,
        request_date=request_date,
        username=username,
    )
    return new_permission_request


# -------------------------------------------------------------------------------------------------#
# Create Custom User Permission
# -------------------------------------------------------------------------------------------------#
# Username & CustomPermission Required - With Optional Parameters For Other Fields
# -------------------------------------------------------------------------------------------------#
def create_custom_user_permission(
    username,
    permission,
    goc_email=DEFAULT_SECONDARY_EMAIL,
    pri_or_military_nbr=DEFAULT_PRI,
    rationale=DEFAULT_FIRST_NAME,
    supervisor=DEFAULT_FIRST_NAME,
    supervisor_email=DEFAULT_SECONDARY_EMAIL,
):
    new_custom_user_permission = CustomUserPermissions.objects.create(
        user_id=username,
        permission=permission,
        goc_email=goc_email,
        pri_or_military_nbr=pri_or_military_nbr,
        rationale=rationale,
        supervisor=supervisor,
        supervisor_email=supervisor_email,
    )
    return new_custom_user_permission


DEFAULT_DATE_ASSIGNED = datetime.today()
DEFAULT_EXPIRY_DATE = (datetime.today()) + (relativedelta(years=1))
DEFAULT_TEST_ORDER_NUMBER = "TEST_ORDER_N"
DEFAULT_STAFFING_PROCESS_NUMBER = "default"
DEFAULT_DEPARTMENT_MINISTRY_CODE = "default"
DEFAULT_IS_ORG = "default"
DEFAULT_IS_REF = "default"
DEFAULT_BILLING_CONTACT = "default"
DEFAULT_BILLING_CONTACT_INFO = "default"
DEFAULT_REASON_FOR_MODIF_OR_DEL = "default"


# -------------------------------------------------------------------------------------------------#
# Create Test Permissions
# -------------------------------------------------------------------------------------------------#
# Username & TestDefinition Required - With Optional Parameters For Other Fields
# -------------------------------------------------------------------------------------------------#
def create_test_permissions(
    username,
    test,
    date_assigned=DEFAULT_DATE_ASSIGNED,
    expiry_date=DEFAULT_EXPIRY_DATE,
    test_order_number=DEFAULT_TEST_ORDER_NUMBER,
    staffing_process_number=DEFAULT_STAFFING_PROCESS_NUMBER,
    department_ministry_code=DEFAULT_DEPARTMENT_MINISTRY_CODE,
    is_org=DEFAULT_IS_ORG,
    is_ref=DEFAULT_IS_REF,
    billing_contact=DEFAULT_BILLING_CONTACT,
    billing_contact_info=DEFAULT_BILLING_CONTACT_INFO,
    reason_for_modif_or_del=DEFAULT_REASON_FOR_MODIF_OR_DEL,
):
    new_test_permissions = TestPermissions.objects.create(
        username=username,
        test=test,
        date_assigned=date_assigned,
        expiry_date=expiry_date,
        test_order_number=test_order_number,
        staffing_process_number=staffing_process_number,
        department_ministry_code=department_ministry_code,
        is_org=is_org,
        is_ref=is_ref,
        billing_contact=billing_contact,
        billing_contact_info=billing_contact_info,
        reason_for_modif_or_del=reason_for_modif_or_del,
    )
    return new_test_permissions


# -------------------------------------------------------------------------------------------------#
# Get Accessible Custom Permissions for BO
# -------------------------------------------------------------------------------------------------#
# Returns the number of custom permissions that the BO should have access
# -------------------------------------------------------------------------------------------------#
def get_accessible_custom_permissions_for_bo():
    return CustomPermissions.objects.filter(
        codename__in=[
            Permission.SYSTEM_ADMINISTRATOR,
            Permission.SCORER,
            Permission.TEST_ADMINISTRATOR,
            Permission.AAE,
        ]
    )


# -------------------------------------------------------------------------------------------------#
# Get Number of Permission Requests for BO
# -------------------------------------------------------------------------------------------------#
# Returns the number of permission requests the BO should see
# -------------------------------------------------------------------------------------------------#
def get_nb_of_permission_requests_for_bo(username):
    accessible_custom_permissions = get_accessible_custom_permissions_for_bo()

    return (
        PermissionRequest.objects.filter(
            permission_requested__in=accessible_custom_permissions
        )
        .exclude(username=username)
        .count()
    )


# -------------------------------------------------------------------------------------------------#
# Get Number of Permission Requests for BO (Specific User)
# -------------------------------------------------------------------------------------------------#
# Returns the number of permission requests the BO should see - For Specific Username
# -------------------------------------------------------------------------------------------------#
def get_nb_of_permission_requests_for_bo_specific_user(username):
    accessible_custom_permissions = get_accessible_custom_permissions_for_bo()

    return PermissionRequest.objects.filter(
        permission_requested__in=accessible_custom_permissions, username=username
    ).count()


# -------------------------------------------------------------------------------------------------#
# Get Accessible Custom Permissions for RD Operations
# -------------------------------------------------------------------------------------------------#
# Returns the number of custom permissions that the RD Operations should have access
# -------------------------------------------------------------------------------------------------#
def get_accessible_custom_permissions_for_rd_operations():
    return CustomPermissions.objects.filter(
        codename__in=[
            Permission.RD_OPERATIONS,
            Permission.PPC_ADMINISTRATOR,
            Permission.TEST_BUILDER,
            Permission.TEST_DEVELOPER,
        ]
    )


# -------------------------------------------------------------------------------------------------#
# Get Number of Permission Requests for RD Operations
# -------------------------------------------------------------------------------------------------#
# Returns the number of permission requests the RD Operations should see
# -------------------------------------------------------------------------------------------------#
def get_nb_of_permission_requests_for_rd_operations(username):
    accessible_custom_permissions = (
        get_accessible_custom_permissions_for_rd_operations()
    )

    return (
        PermissionRequest.objects.filter(
            permission_requested__in=accessible_custom_permissions
        )
        .exclude(username=username)
        .count()
    )


# -------------------------------------------------------------------------------------------------#
# Get Number of Permission Requests for RD Operations (Specific User)
# -------------------------------------------------------------------------------------------------#
# Returns the number of permission requests the RD Operations should see - For Specific Username
# -------------------------------------------------------------------------------------------------#
def get_nb_of_permission_requests_for_rd_operations_specific_user(username):
    accessible_custom_permissions = (
        get_accessible_custom_permissions_for_rd_operations()
    )

    return PermissionRequest.objects.filter(
        permission_requested__in=accessible_custom_permissions, username=username
    ).count()
