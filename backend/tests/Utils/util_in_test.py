from backend.static.languages import Languages

# -------------------------------------------------------------------------------------------------#
# Generate List of Questions/Answers
# -------------------------------------------------------------------------------------------------#
# Function Used to get a readable list of Questions/Answers
# -------------------------------------------------------------------------------------------------#
# Format for questions_with_answers_list:
# [
#   {
#       "question_id": question_id,
#       "question_order": question_order,
#       "question_text": question_text,
#       "answers": [
#           {
#               "id" : answer_id,
#               "order": answer_order,
#               "content": answer_content,
#               "answer": answer_id,
#               "language": answer_language,
#               "question": question_id
#           },
#           {
#               "id" : answer_id,
#               "order": answer_order,
#               "content": answer_content,
#               "answer": answer_id,
#               "language": answer_language,
#               "question": question_id
#           },
#       ]
#   },
#   {
#       "question_id": question_id,
#       "question_order": question_order,
#       "question_text": question_text,
#       "answers": [
#           {
#               "id" : answer_id,
#               "order": answer_order,
#               "content": answer_content,
#               "answer": answer_id,
#               "language": answer_language,
#               "question": question_id
#           },
#           {
#               "id" : answer_id,
#               "order": answer_order,
#               "content": answer_content,
#               "answer": answer_id,
#               "language": answer_language,
#               "question": question_id
#           },
#       ]
#   },
# ]
# -------------------------------------------------------------------------------------------------#
def get_questions_with_answers_list_from_response(
    test_section_response, language=Languages.EN
):
    questions_with_answers_list = []

    for question in test_section_response.data["localize"][language]["components"][1][
        "test_section_component"
    ]["questions"]:
        data = {
            "question_id": question["sections"][0]["id"],
            "question_order": question["sections"][0]["order"],
            "question_text": question["sections"][0]["question_section_content"][
                "content"
            ],
            "answers": question["answers"],
        }
        questions_with_answers_list.append(data)

    return questions_with_answers_list
