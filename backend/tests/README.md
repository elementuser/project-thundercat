# Table of Contents

1. [Run Test Suite](#how-to---run-test-suite)
   - [Verify Docker](#verify-docker)
   - [Run Tests](#run-test-suite)
2. [Create Unit Test](#how-to---create-unit-test)
   - [Preparation](#preparation)
   - [Create SetUp Function](#create-setup-function)
   - [Create Testing Function](#create-testing-function)
3. [Create Unit Tests Coverage Report](#how-to---create-coverage-report)

<br><br>

# How To - Run Test Suite

## Verify Docker

1. Make sure your environment is currently running.<br>
2. Verify that all the dockers are available in the Docker section in VSCode:<br>
   ![readme-vscode-docker.png](./images/readme-vscode-docker.png)

<br>

## Run Test Suite

1. Right-click on the backend container and select "Attach Shell":<br>
   ![readme-vscode-backend-shell.png](./images/readme-vscode-backend-shell.png)

2. You should have a terminal like this:<br>
   ![readme-vscode-backend-terminal.png](./images/readme-vscode-backend-terminal.png)

3. To run the tests, write this in the terminal:<br>
   `python manage.py test`

4. If the tests have passed successfully, you should have:<br>
   ![readme-vscode-backend-terminal-result.png](./images/readme-vscode-backend-terminal-result.png)

<br><br>

# How To - Create Unit Test

## Preparation

1. Go to the file [test_suite.py](test_suite.py)
2. Look for the section of code that is related to the functionnality that you will be creating a test for.
3. Select the type of test case you will need for your tests:
   - **TestCase**: For backend function testing
   - **APITestCase**: For endpoint testing (will work for function testing too)
4. Use the format:

   ```python
    # Class
    # As the function name, use:
    #   Name that represents what part of the code will be tested + "Test"
    class PermissionsCeleryTasksTest(APITestCase or TestCase):
        def setUp(self):
            # Database stuff that will be run before each functions under
            # As the function name, use:
            #   "setup_data_for_" + name of functionnality to test
            setup_data_for_permission_test()

        # Example of a function that will be tested
        # As the function name, use:
        #   "test_" + name of functionnality to test
        def test_deprovision_admin_after_inactivity_thirty_days(self):
            # This is the run function part
            # As the function name, use:
            #   "run_" + name of functionnality to test
            run_deprovision_admin_after_inactivity_thirty_days(self)
   ```

<br>

## Create SetUp Function

1. Make sure you have the variables `FOLDER_NAME` and `FILE_NAME`
2. As you may have seen already, you will have to use [util_print.py](Utils/util_print.py) functions for the printing:

   ```python
   # Start of the setup
   print_setup_start(FOLDER_NAME, FILE_NAME)

   # Start of each task
   print_sub_task_start("create_all_custom_permissions")

   # Running the task - Which in this case is in Utils/util_permissions.py
   create_all_custom_permissions()

   # End of each task
   print_sub_task_end("create_all_custom_permissions")

   # End of the setup
   print_setup_end()
   ```

<br>

## Create Testing Function

1. Make sure you have the variables `FOLDER_NAME` and `FILE_NAME`
2. Make sure you use Utils files for repetitive tasks.
3. Write the testing function in this format:

   ```python
   # Start of the task
   print_task_start(
       folder_name=FOLDER_NAME,
       file_name=FILE_NAME,
       function_name="get_all_permission_requests",
   )

   # Start of a sub-task
   print_sub_task_start("SETTING UP DATA FOR get_all_permission_requests")

   # Using Utils/util_users.py
   # Create Users - get_all_permission_requests
   (
       admin_user_1,
       bo_user_1,
       bo_user_2,
       bo_user_3,
       rd_user_1,
       rd_user_2,
       rd_user_3,
   ) = create_users_for_get_all_permission_requests()

    # Using a function that is definied at the end of the file - Makes it cleaner to see what's going on in the task
   # Create CustomUserPermissions - get_all_permission_requests
   (
       bo_custom_permission,
       rd_custom_permission,
   ) = create_custom_user_permission_for_get_all_permission_requests(
       bo_user_1,
       bo_user_2,
       bo_user_3,
       rd_user_1,
       rd_user_2,
       rd_user_3,
   )

   # End of a sub-task
   print_sub_task_end("SETTING UP DATA FOR get_all_permission_requests")

   # Start of a sub-task
   print_sub_task_start("EXECUTING ENDPOINT TESTS - get_all_permission_requests")

   # Execute Tests - get_all_permission_requests
   execute_tests_for_get_all_permission_requests(
       self,
       admin_user_1,
       bo_user_1,
       bo_user_2,
       bo_user_3,
       rd_user_1,
       rd_user_2,
       rd_user_3,
   )

   print_sub_task_end("EXECUTING ENDPOINT TESTS - get_all_permission_requests")

   # End of the task
   print_task_end()
   ```

# How To - Create Coverage Report

1. Connect to your Backend docker (see [Verify Docker](#verify-docker))
2. Install "Coverage":
   ```bash
   pip install coverage
   ```
3. Execute the Unit Tests:
   ```bash
   coverage run manage.py test
   ```
4. Generate the Coverage Report:
   ```bash
   coverage html -d tests/coverage_html
   ```
5. The files will be put in the folder [backend/tests/coverage_html](/backend/tests/coverage_html/)
6. To see the report, open [index.html](/backend/tests/coverage_html/index.html)
