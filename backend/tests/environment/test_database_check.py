from rest_framework.views import status
from tests.Utils.util_print import (
    print_setup_start,
    print_setup_end,
    print_sub_task_start,
    print_sub_task_end,
    print_task_start,
    print_task_end,
    print_level_1,
    print_ok_level_2,
)
from backend.views.database_check_view import DatabaseCheckModel
from backend.serializers import database_check_serializer

FOLDER_NAME = "Environment"
FILE_NAME = "Database Check"


def setup_database_entry():
    print_setup_start(FOLDER_NAME, FILE_NAME)

    print_sub_task_start("Creating DatabaseCheckModel Data")
    # create database entry
    DatabaseCheckModel.objects.create_database_entry(name="name1")
    print_level_1('Created an object with name="name1"')

    print_sub_task_end("Creating DatabaseCheckModel Data")

    print_setup_end()


def run_get_all_users(self):
    print_task_start(FOLDER_NAME, FILE_NAME, "get_all_users")

    response = self.client.get("http://localhost:81/oec-cat/api/database-check/")

    # SELECT * FROM backend_databasecheckmodel;
    expected = DatabaseCheckModel.objects.all()
    serialized = database_check_serializer.DatabaseCheckSerializer(expected, many=True)

    print_sub_task_start("TESTING OF RESULTS - DATABASE - run_get_all_users")

    print_level_1("Verify that the data in response is the same as the database")
    self.assertEqual(response.data, serialized.data)
    print_ok_level_2()

    print_level_1("Verify that the response status is HTTP_200_OK")
    self.assertEqual(response.status_code, status.HTTP_200_OK)
    print_ok_level_2()

    print_sub_task_end("TESTING OF RESULTS - DATABASE - run_get_all_users")

    print_task_end()


def run_get_specific_user(self):

    print_task_start(FOLDER_NAME, FILE_NAME, "get_specific_user")

    response = self.client.get("http://localhost:81/oec-cat/api/database-check/")

    # SELECT name FROM backend_databasecheckmodel WHERE name='name1';
    expected = DatabaseCheckModel.objects.values_list("name", flat=True).get(
        name="name1"
    )

    print_sub_task_start("TESTING OF RESULTS - DATABASE - run_get_specific_user")

    print_level_1(
        "Verify that the name in response is the same as the one in the database"
    )
    self.assertEqual(expected, "name1")
    print_ok_level_2()

    print_level_1("Verify that the response status is HTTP_200_OK")
    self.assertEqual(response.status_code, status.HTTP_200_OK)
    print_ok_level_2()

    print_sub_task_end("TESTING OF RESULTS - DATABASE - run_get_specific_user")

    print_task_end()
