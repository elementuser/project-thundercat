from django.test import TestCase
from backend.views.test_scorer_assignment_view import get_test_scorer_assignment

# TODO: NEVER USED, WE COULD DELETE IT
class RetrieveTestScorerAssignments(TestCase):
    def check_responses(
        self, scorer_username, expected_unassigned_tests, expected_assigned_tests
    ):
        unassigned_tests = get_test_scorer_assignment(None)
        assigned_tests = get_test_scorer_assignment(scorer_username)

        self.assertEqual(unassigned_tests, expected_unassigned_tests)
        self.assertEqual(assigned_tests, expected_assigned_tests)
