from tests.Utils.util_permissions import create_test_permissions
from cms.cms_models.test_definition import TestDefinition
from user_management.user_management_models.user_models import User
from cms.cms_models.test_section import TestSection
from tests.Utils.util_uit_invitations import send_uit_invitation
from user_management.static.permission import Permission
from user_management.user_management_models.custom_permissions_model import (
    CustomPermissions,
)
from backend.custom_models.unsupervised_test_access_code_model import (
    UnsupervisedTestAccessCode,
)
from tests.Utils.util_permissions import (
    create_all_custom_permissions,
    create_custom_user_permission,
)
from tests.Utils.util_print import (
    print_setup_start,
    print_sub_task_start,
    print_sub_task_end,
    print_setup_end,
    print_ok_level_3,
    print_ok_level_4,
    print_level_1,
    print_level_2,
    print_level_3,
    print_task_start,
    print_task_end,
)
from tests.Utils.util_test_data import upload_test
from tests.Utils.util_users import create_user

FOLDER_NAME = "Backend"
FILE_NAME = "UIT_Invitation"

TESTER_USERNAME_1 = "tester1@email.ca"
TA_USERNAME_1 = "ta1@email.ca"

NON_USER_EMAIL = "non_user@email.ca"
NON_USER_FIRST_NAME = "Tester-First-Name"
NON_USER_LAST_NAME = "Tester-Last-Name"

JSON_FILE_NAME_1 = "test_1.json"


def setup_data_for_uit_invitations():
    print_setup_start(FOLDER_NAME, FILE_NAME)

    print_sub_task_start("create_all_custom_permissions")

    create_all_custom_permissions()

    print_sub_task_end("create_all_custom_permissions")

    print_sub_task_start("upload_json_test_data")

    # Upload Tests
    upload_test(JSON_FILE_NAME_1)

    print_sub_task_end("upload_json_test_data")

    print_sub_task_start("create_users_data")

    # Create Users
    create_users_data()

    print_sub_task_end("create_users_data")

    print_setup_end()


def run_send_uit_invitations(self):

    print_task_start(
        folder_name=FOLDER_NAME,
        file_name=FILE_NAME,
        function_name="send_uit_invitations",
    )

    print_sub_task_start("SETTING UP DATA FOR send_uit_invitations")

    tester_users = []

    tester_user_1 = User.objects.get(username=TESTER_USERNAME_1)

    # Tester User - Current user in the application
    tester_users.append(
        {
            "first_name": tester_user_1.first_name,
            "last_name": tester_user_1.last_name,
            "email": tester_user_1.email,
        }
    )

    # Tester User - Not currently a user in the application
    tester_users.append(
        {
            "first_name": NON_USER_FIRST_NAME,
            "last_name": NON_USER_LAST_NAME,
            "email": NON_USER_EMAIL,
        }
    )

    ta_user_1 = User.objects.get(username=TA_USERNAME_1)

    # Give Custom User Permissions - Test Administrator
    create_custom_user_permissions_data(ta_user_1)

    # Verify that the new TestDefinition exists
    new_test_definiton = TestDefinition.objects.get(
        parent_code="8877", test_code="SQ F000 A1", version=1
    )

    # Give Custom Test Permission to TA - Test Definition
    new_test_permission = create_test_permission_data(new_test_definiton, ta_user_1)

    print_sub_task_end("SETTING UP DATA FOR send_uit_invitations")

    print_sub_task_start(
        "EXECUTING TESTS - Making sure that our Test Section is available"
    )

    # Get the Test Section for the questions + Test it
    test_section = get_and_test_test_section(self)

    print_sub_task_end(
        "EXECUTING TESTS - Making sure that our Test Section is available"
    )

    print_sub_task_start("Send UIT Invitation")

    send_uit_invitation(
        self,
        ta_user_1,
        tester_users,
        new_test_definiton,
        test_section,
        new_test_permission,
    )

    print_sub_task_end("Send UIT Invitation")

    print_sub_task_start(
        "EXECUTING TESTS - Making sure that UIT Invites have been created"
    )

    test_uit_invites_creation(self, new_test_definiton, ta_user_1)

    print_sub_task_end(
        "EXECUTING TESTS - Making sure that UIT Invites have been created"
    )

    print_task_end()


# Sub-Function - Create Users
def create_users_data():

    print_level_1("Creating Users: ")
    tester_user_1 = create_user(username=TESTER_USERNAME_1)
    print_level_2(tester_user_1.username)

    ta_user_1 = create_user(username=TA_USERNAME_1)
    print_level_2(ta_user_1.username)

    return tester_user_1, ta_user_1


# Sub-Function - Create Custom User Permissions
def create_custom_user_permissions_data(ta_user_1):
    print_level_1("Creating Custom User Permissions: ")
    ta_custom_permission = CustomPermissions.objects.filter(
        codename=Permission.TEST_ADMINISTRATOR
    ).last()

    ta_custom_user_permission_for_ta_user_1 = create_custom_user_permission(
        username=ta_user_1, permission=ta_custom_permission
    )
    print_level_2(
        "User: {0} Permission: {1}".format(
            ta_custom_user_permission_for_ta_user_1.user,
            ta_custom_user_permission_for_ta_user_1.permission,
        )
    )

    return ta_custom_user_permission_for_ta_user_1


# Sub-Function - Create Test Permissions
def create_test_permission_data(new_test_definition, ta_user_1):
    new_test_permissions = create_test_permissions(
        test=new_test_definition, username=ta_user_1
    )

    return new_test_permissions


# Sub-Function - Get and Test the Test Section we will be using for UIT Invitation
def get_and_test_test_section(self):
    print_level_1("Getting Our Test Section:")
    # Get the Test Section for the questions
    test_section = TestSection.objects.exclude(default_time=None).first()

    print_level_2(test_section)

    # Verify that this is the right Test Section
    # Note: We won't test the version since it can change depending on how many times we upload the test_1.json file
    print_level_1(
        "Verifying that the Test Definition related to our Test Section is correct:"
    )

    print_level_2("Parent Code is 8877")
    # Test Definition - Parent Code
    self.assertEqual(
        test_section.test_definition.parent_code,
        "8877",
    )
    print_ok_level_3()

    print_level_2('Test Code is "SQ F000 A1"')
    # Test Definition - Test Code
    self.assertEqual(
        test_section.test_definition.test_code,
        "SQ F000 A1",
    )
    print_ok_level_3()

    return test_section


# Sub-Function - Test the creation of UIT Invites
def test_uit_invites_creation(self, test_definiton, ta_user):
    print_level_1("Verifying all our testers have an UIT Invitation in our database:")

    # TESTER - Current User of our Application
    print_level_2("{0}:".format(TESTER_USERNAME_1))

    test_access_code = UnsupervisedTestAccessCode.objects.get(
        test=test_definiton.id,
        ta_username=ta_user.username,
        candidate_email=TESTER_USERNAME_1,
    ).test_access_code

    print_level_3(test_access_code)
    self.assertTrue(test_access_code)
    print_ok_level_4()

    # TESTER - Not Currently a User
    print_level_2("{0}:".format(NON_USER_EMAIL))

    test_access_code = UnsupervisedTestAccessCode.objects.get(
        test=test_definiton.id,
        ta_username=ta_user.username,
        candidate_email=NON_USER_EMAIL,
    ).test_access_code

    print_level_3(test_access_code)
    self.assertTrue(test_access_code)
    print_ok_level_4()
