from tests.Utils.util_print import (
    print_setup_start,
    print_setup_end,
    print_task_start,
    print_task_end,
    print_sub_task_start,
    print_sub_task_end,
    print_level_1,
    print_level_2,
    print_level_3,
    print_ok_level_2,
)
from tests.Utils.util_users import (
    create_user,
    create_access_token,
)
from tests.Utils.util_permissions import (
    create_all_custom_permissions,
    create_permission_request,
    create_custom_user_permission,
    get_nb_of_permission_requests_for_bo,
    get_number_of_custom_permissions,
    get_nb_of_permission_requests_for_bo_specific_user,
    get_nb_of_permission_requests_for_rd_operations,
)
from tests.Utils.util_endpoint import (
    get_header_for_endpoint_call,
)
from user_management.user_management_models.custom_permissions_model import (
    CustomPermissions,
)
from user_management.user_management_models.permission_request_model import (
    PermissionRequest,
)
from user_management.static.permission import Permission

FOLDER_NAME = "Backend"
FILE_NAME = "Permissions"


def setup_data_for_permission_test():
    print_setup_start(FOLDER_NAME, FILE_NAME)

    print_sub_task_start("create_all_custom_permissions")

    create_all_custom_permissions()

    print_sub_task_end("create_all_custom_permissions")

    print_setup_end()


def run_get_all_permission_requests(self):

    print_task_start(
        folder_name=FOLDER_NAME,
        file_name=FILE_NAME,
        function_name="get_all_permission_requests",
    )

    print_sub_task_start("SETTING UP DATA FOR get_all_permission_requests")

    # Create Users - get_all_permission_requests
    (
        admin_user_1,
        bo_user_1,
        bo_user_2,
        bo_user_3,
        rd_user_1,
        rd_user_2,
        rd_user_3,
    ) = create_users_for_get_all_permission_requests()

    # Create CustomUserPermissions - get_all_permission_requests
    (
        bo_custom_permission,
        rd_custom_permission,
    ) = create_custom_user_permission_for_get_all_permission_requests(
        bo_user_1,
        bo_user_2,
        bo_user_3,
        rd_user_1,
        rd_user_2,
        rd_user_3,
    )

    # Create PermissionRequests - get_all_permission_requests
    create_permission_requests_for_get_all_permission_requests(
        bo_custom_permission,
        rd_custom_permission,
        bo_user_1,
        bo_user_2,
        rd_user_1,
        rd_user_2,
    )

    print_sub_task_end("SETTING UP DATA FOR get_all_permission_requests")

    print_sub_task_start("EXECUTING ENDPOINT TESTS - get_all_permission_requests")

    # Execute Tests - get_all_permission_requests
    execute_tests_for_get_all_permission_requests(
        self,
        admin_user_1,
        bo_user_1,
        bo_user_2,
        bo_user_3,
        rd_user_1,
        rd_user_2,
        rd_user_3,
    )

    print_sub_task_end("EXECUTING ENDPOINT TESTS - get_all_permission_requests")

    print_task_end()


def run_search_specific_permissions_request(self):

    print_task_start(
        folder_name=FOLDER_NAME,
        file_name=FILE_NAME,
        function_name="search_specific_permissions_request",
    )

    print_sub_task_start("SETTING UP DATA FOR run_search_specific_permissions_request")

    # Create Users - search_specific_permissions_request
    (
        bo_user_1,
        bo_user_2,
        bo_user_3,
    ) = create_users_for_search_specific_permissions_request()

    # Create CustomUserPermissions - search_specific_permissions_request
    bo_custom_permission = (
        create_custom_user_permission_for_search_specific_permissions_request(
            bo_user_1, bo_user_2, bo_user_3
        )
    )

    # Create PermissionRequests - search_specific_permissions_request
    create_permission_requests_for_search_specific_permissions_request(
        bo_custom_permission,
        bo_user_1,
        bo_user_2,
    )

    print_sub_task_end("SETTING UP DATA FOR run_search_specific_permissions_request")

    print_sub_task_start(
        "EXECUTING ENDPOINT TESTS - run_search_specific_permissions_request"
    )

    # Execute Tests - search_specific_permissions_request
    execute_tests_for_get_search_specific_permissions_request(
        self,
        bo_user_1,
    )

    print_sub_task_end(
        "EXECUTING ENDPOINT TESTS - run_search_specific_permissions_request"
    )

    print_task_end()


# Sub-Function - Create all users for get_all_permission_requests
def create_users_for_get_all_permission_requests():
    print_level_1("Creating Users")

    # Admin Account
    admin_username = "admin@email.ca"
    print_level_2("Admin User")
    admin_user_1 = create_user(username=admin_username, is_staff=True)

    # B&O user 1
    bo1_username = "bo1_user@email.ca"
    print_level_2("First B&0 User")
    bo_user_1 = create_user(username=bo1_username)

    # B&O user 2
    bo2_username = "bo2_user@email.ca"
    print_level_2("Second B&0 User")
    bo_user_2 = create_user(username=bo2_username)

    # B&O user 3
    bo3_username = "bo3_user@email.ca"
    print_level_2("Third B&0 User")
    bo_user_3 = create_user(username=bo3_username)

    # RD Operations user 1
    rd1_username = "rd1_user@email.ca"
    print_level_2("First R&D Operations User")
    rd_user_1 = create_user(username=rd1_username)

    # RD Operations user 2
    rd2_username = "rd2_user@email.ca"
    print_level_2("Second R&D Operations User")
    rd_user_2 = create_user(username=rd2_username)

    # RD Operations user 3
    rd3_username = "rd3_user@email.ca"
    print_level_2("Third R&D Operations User")
    rd_user_3 = create_user(username=rd3_username)

    return (
        admin_user_1,
        bo_user_1,
        bo_user_2,
        bo_user_3,
        rd_user_1,
        rd_user_2,
        rd_user_3,
    )


# Sub-Function - Create all custom user permissions for get_all_permission_requests
def create_custom_user_permission_for_get_all_permission_requests(
    bo_user_1,
    bo_user_2,
    bo_user_3,
    rd_user_1,
    rd_user_2,
    rd_user_3,
):
    # create custom user permissions data
    print_level_1("Creating Custom User Permissions' Data")

    bo_custom_permission = CustomPermissions.objects.get(
        codename=Permission.SYSTEM_ADMINISTRATOR
    )

    print_level_2(
        "Assigning to first B&0 user: {0}".format(bo_custom_permission.en_name)
    )
    create_custom_user_permission(
        username=bo_user_1.username, permission=bo_custom_permission
    )

    print_level_2(
        "Assigning to second B&0 user: {0}".format(bo_custom_permission.en_name)
    )
    create_custom_user_permission(
        username=bo_user_2.username, permission=bo_custom_permission
    )

    print_level_2(
        "Assigning to third B&0 user: {0}".format(bo_custom_permission.en_name)
    )
    create_custom_user_permission(
        username=bo_user_3.username, permission=bo_custom_permission
    )

    rd_custom_permission = CustomPermissions.objects.get(
        codename=Permission.RD_OPERATIONS
    )

    print_level_2(
        "Assigning to first R&D Operations user: {0}".format(
            rd_custom_permission.en_name
        )
    )
    create_custom_user_permission(
        username=rd_user_1.username, permission=rd_custom_permission
    )

    print_level_2(
        "Assigning to second R&D Operations user: {0}".format(
            rd_custom_permission.en_name
        )
    )
    create_custom_user_permission(
        username=rd_user_2.username, permission=rd_custom_permission
    )

    print_level_2(
        "Assigning to third R&D Operations user: {0}".format(
            rd_custom_permission.en_name
        )
    )
    create_custom_user_permission(
        username=rd_user_3.username, permission=rd_custom_permission
    )

    return bo_custom_permission, rd_custom_permission


# Sub-Function - Create all permission requests for get_all_permission_requests
def create_permission_requests_for_get_all_permission_requests(
    bo_custom_permission,
    rd_custom_permission,
    bo_user_1,
    bo_user_2,
    rd_user_1,
    rd_user_2,
):
    # create permission requests for users
    print_level_1("Creating Permission Requests")
    print_level_2("Assigning permission requests to first B&0 user:")

    for i in range(1, get_number_of_custom_permissions()):
        if i != bo_custom_permission.pk:
            custom_permission = CustomPermissions.objects.get(pk=i)
            new_permission_request = create_permission_request(
                username=bo_user_1, permission_requested=custom_permission
            )
            print_level_3(new_permission_request.permission_requested.en_name)

    print_level_2("Assigning permission requests to second B&0 user:")
    for i in range(1, get_number_of_custom_permissions()):
        if i != bo_custom_permission.pk:
            custom_permission = CustomPermissions.objects.get(pk=i)
            new_permission_request = create_permission_request(
                username=bo_user_2, permission_requested=custom_permission
            )
            print_level_3(new_permission_request.permission_requested.en_name)

    print_level_2("Assigning permission requests to first R&D Operations user:")
    for i in range(1, get_number_of_custom_permissions()):
        if i != rd_custom_permission.pk:
            custom_permission = CustomPermissions.objects.get(pk=i)
            new_permission_request = create_permission_request(
                username=rd_user_1, permission_requested=custom_permission
            )
            print_level_3(new_permission_request.permission_requested.en_name)

    print_level_2("Assigning permission requests to second R&D Operations user:")
    for i in range(1, get_number_of_custom_permissions()):
        if i != rd_custom_permission.pk:
            custom_permission = CustomPermissions.objects.get(pk=i)
            new_permission_request = create_permission_request(
                username=rd_user_2, permission_requested=custom_permission
            )
            print_level_3(new_permission_request.permission_requested.en_name)


# Sub-Function - Execute tests for get_all_permission_requests
def execute_tests_for_get_all_permission_requests(
    self, admin_user_1, bo_user_1, bo_user_2, bo_user_3, rd_user_1, rd_user_2, rd_user_3
):
    # Admin User 1
    print_level_1(
        "Admin User - Verify that the user can see permission requests from others"
    )

    # Get token to access endpoints - For BO 1
    access_token = create_access_token(self=self, username=admin_user_1.username)

    # Get all pending permissions - For BO 1 User
    response = self.client.get(
        "/oec-cat/api/get-pending-permissions/?page=1&page_size=999999",
        **get_header_for_endpoint_call(access_token)
    )

    # Verify that user 1 can only see other permission requests
    self.assertEqual(
        PermissionRequest.objects.all().exclude(username=admin_user_1.username).count(),
        len(response.data["results"]),
    )
    print_ok_level_2()

    # BO User 1
    print_level_1(
        "B&O User 1 - Verify that the user can only see permission requests from others"
    )

    # Get token to access endpoints - For BO 1
    access_token = create_access_token(self=self, username=bo_user_1.username)

    # Get all pending permissions - For BO 1 User
    response = self.client.get(
        "/oec-cat/api/get-pending-permissions/?page=1&page_size=999999",
        **get_header_for_endpoint_call(access_token)
    )

    # Verify that user 1 can only see other permission requests
    self.assertEqual(
        get_nb_of_permission_requests_for_bo(bo_user_1.username),
        len(response.data["results"]),
    )
    print_ok_level_2()

    # BO User 2
    print_level_1(
        "B&O User 2 - Verify that the user can only see permission requests from others"
    )

    # Get token to access endpoints - For BO 2
    access_token = create_access_token(self=self, username=bo_user_2.username)

    # Get all pending permissions - For BO 2 User
    response = self.client.get(
        "/oec-cat/api/get-pending-permissions/?page=1&page_size=999999",
        **get_header_for_endpoint_call(access_token)
    )

    # Verify that user 2 can only see other permission requests
    self.assertEqual(
        get_nb_of_permission_requests_for_bo(bo_user_2.username),
        len(response.data["results"]),
    )
    print_ok_level_2()

    # BO User 3
    print_level_1(
        "B&O User 3 - Verify that user 3 can see all permission requests (since it doesn't have any)"
    )

    # Get token to access endpoints - For BO 3
    access_token = create_access_token(self=self, username=bo_user_3.username)

    # Get all pending permissions - For BO 3 User
    response = self.client.get(
        "/oec-cat/api/get-pending-permissions/?page=1&page_size=999999",
        **get_header_for_endpoint_call(access_token)
    )

    # Verify that user 3 can see all permission requests (since it doesn't have any)
    self.assertEqual(
        get_nb_of_permission_requests_for_bo(bo_user_3.username),
        len(response.data["results"]),
    )
    print_ok_level_2()

    # RD User 1
    print_level_1(
        "R&D Operations User 1 - Verify that the user can only see permission requests from others"
    )

    # Get token to access endpoints - For RD 1
    access_token = create_access_token(self=self, username=rd_user_1.username)

    # Get all pending permissions - For RD 1 User
    response = self.client.get(
        "/oec-cat/api/get-pending-permissions/?page=1&page_size=999999",
        **get_header_for_endpoint_call(access_token)
    )

    # Verify that user 1 can only see other permission requests
    self.assertEqual(
        get_nb_of_permission_requests_for_rd_operations(rd_user_1.username),
        len(response.data["results"]),
    )
    print_ok_level_2()

    # RD User 2
    print_level_1(
        "R&D Operations User 2 - Verify that the user can only see permission requests from others"
    )

    # Get token to access endpoints - For RD 2
    access_token = create_access_token(self=self, username=rd_user_2.username)

    # Get all pending permissions - For RD 2 User
    response = self.client.get(
        "/oec-cat/api/get-pending-permissions/?page=1&page_size=999999",
        **get_header_for_endpoint_call(access_token)
    )

    # Verify that user 2 can only see other permission requests
    self.assertEqual(
        get_nb_of_permission_requests_for_rd_operations(rd_user_2.username),
        len(response.data["results"]),
    )
    print_ok_level_2()

    # RD User 3
    print_level_1(
        "R&D Operations User 3 - Verify that user 3 can see all permission requests (since it doesn't have any)"
    )

    # Get token to access endpoints - For RD 3
    access_token = create_access_token(self=self, username=rd_user_3.username)

    # Get all pending permissions - For RD 3 User
    response = self.client.get(
        "/oec-cat/api/get-pending-permissions/?page=1&page_size=999999",
        **get_header_for_endpoint_call(access_token)
    )

    # Verify that user 3 can see all permission requests (since it doesn't have any)
    self.assertEqual(
        get_nb_of_permission_requests_for_rd_operations(rd_user_3.username),
        len(response.data["results"]),
    )
    print_ok_level_2()


# Sub-Function - Create all users for search_specific_permissions_request
def create_users_for_search_specific_permissions_request():
    print_level_1("Creating Users")
    # B&O user 1
    bo1_username = "bo1_user@email.ca"
    print_level_2("First B&O User")
    bo_user_1 = create_user(username=bo1_username)

    # B&O user 2
    bo2_username = "bo2_user@email.ca"
    print_level_2("Second B&0 User")
    bo_user_2 = create_user(username=bo2_username)

    # B&O user 3
    bo3_username = "bo3_user@email.ca"
    print_level_2("Third B&0 User")
    bo_user_3 = create_user(username=bo3_username)

    return bo_user_1, bo_user_2, bo_user_3


# Sub-Function - Create all custom user permissions for search_specific_permissions_request
def create_custom_user_permission_for_search_specific_permissions_request(
    bo_user_1, bo_user_2, bo_user_3
):
    # create custom user permissions data
    print_level_1("Creating Custom User Permissions' Data")

    bo_custom_permission = CustomPermissions.objects.get(
        codename=Permission.SYSTEM_ADMINISTRATOR
    )

    print_level_2(
        "Assigning to first B&0 user: {0}".format(bo_custom_permission.en_name)
    )
    create_custom_user_permission(
        username=bo_user_1.username, permission=bo_custom_permission
    )

    print_level_2(
        "Assigning to second B&0 user: {0}".format(bo_custom_permission.en_name)
    )
    create_custom_user_permission(
        username=bo_user_2.username, permission=bo_custom_permission
    )

    print_level_2(
        "Assigning to third B&0 user: {0}".format(bo_custom_permission.en_name)
    )
    create_custom_user_permission(
        username=bo_user_3.username, permission=bo_custom_permission
    )

    return bo_custom_permission


# Sub-Function - Create all permission requests for search_specific_permissions_request
def create_permission_requests_for_search_specific_permissions_request(
    bo_custom_permission,
    bo_user_1,
    bo_user_2,
):
    # create permission requests for users
    print_level_1("Creating Permission Requests")

    print_level_2("Assigning permission requests to first B&0 user:")
    for i in range(1, get_number_of_custom_permissions()):
        if i != bo_custom_permission.pk:
            custom_permission = CustomPermissions.objects.get(pk=i)
            new_permission_request = create_permission_request(
                username=bo_user_1, permission_requested=custom_permission
            )
            print_level_3(new_permission_request.permission_requested.en_name)

    print_level_2("Assigning permission requests to second B&0 user:")
    for i in range(1, get_number_of_custom_permissions()):
        if i != bo_custom_permission.pk:
            custom_permission = CustomPermissions.objects.get(pk=i)
            new_permission_request = create_permission_request(
                username=bo_user_2, permission_requested=custom_permission
            )
            print_level_3(new_permission_request.permission_requested.en_name)


# Sub-Function - Execute tests for search_specific_permissions_request
def execute_tests_for_get_search_specific_permissions_request(
    self,
    bo_user_1,
):
    # BO User 1 - Search with " "
    print_level_1(
        "B&O User 1 - Verify that the user can only see permission requests from others"
    )

    # Get token to access endpoints - For BO 1
    access_token = create_access_token(self=self, username=bo_user_1.username)

    # Create header for our get request - With Authorization Token
    header = get_header_for_endpoint_call(access_token)

    # Search for all pending permissions - For BO 1 User
    response = self.client.get(
        "/oec-cat/api/get-found-pending-permissions/?keyword= &current_language='en'&page=1&page_size=999999",
        **header
    )

    # Verify that user 1 can only see other permission requests
    self.assertEqual(
        get_nb_of_permission_requests_for_bo(username=bo_user_1.username),
        len(response.data["results"]),
    )

    print_ok_level_2()

    # BO User 1 - Search with "bo1_user@email.ca"
    print_level_1(
        "B&O User 1 - Verify that the user can't search and find it's own requests"
    )

    # Search for all pending permissions - For BO 1 User
    response = self.client.get(
        "/oec-cat/api/get-found-pending-permissions/?keyword=bo1_user@email.ca&current_language='en'&page=1&page_size=999999",
        **header
    )

    # Verify that user 1 can't search for it's own requests
    self.assertEqual(
        0,
        len(response.data["results"]),
    )
    print_ok_level_2()

    # BO User 1 - Search with "bo2_user@email.ca"
    print_level_1(
        "B&O User 1 - Verify that the user can search and find bo2_user@email.ca's requests"
    )

    # Search for all pending permissions - For BO 1 User
    response = self.client.get(
        "/oec-cat/api/get-found-pending-permissions/?keyword=bo2_user@email.ca&current_language='en'&page=1&page_size=999999",
        **header
    )

    # Verify that user 1 finds right number of permission request for keyword
    self.assertEqual(
        get_nb_of_permission_requests_for_bo_specific_user(username=bo_user_1.username),
        len(response.data["results"]),
    )
    print_ok_level_2()
