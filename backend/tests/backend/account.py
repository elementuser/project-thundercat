from tests.Utils.util_endpoint import get_header_for_endpoint_call
from tests.Utils.util_users import create_user, create_access_token
from tests.Utils.util_print import (
    print_task_start,
    print_task_end,
    print_level_1,
    print_level_2,
    print_level_3,
    print_ok_level_4,
    print_sub_task_start,
    print_sub_task_end,
)
from user_management.user_management_models.user_models import User
import json
from rest_framework import status
from django.http.response import HttpResponsePermanentRedirect

FOLDER_NAME = "Backend"
FILE_NAME = "Account"


# Default variables
DEFAULT_PASSWORD = "Test123!"
DEFAULT_FIRST_NAME = "Name"
DEFAULT_LAST_NAME = "LastName"
DEFAULT_BIRTH_DATE = "1900-01-01"
DEFAULT_EMAIL = "email@email.ca"
DEFAULT_PRI = "123456789"
DEFAULT_PSRS_APPLICANT_ID = None


def run_create_account(self):
    print_task_start(
        folder_name=FOLDER_NAME,
        file_name=FILE_NAME,
        function_name="create_account",
    )
    print_sub_task_start("EXECUTING ENDPOINT TESTS - create_account")

    data = {
        "first_name": DEFAULT_FIRST_NAME,
        "last_name": DEFAULT_LAST_NAME,
        "birth_date": DEFAULT_BIRTH_DATE,
        "psrs_applicant_id": DEFAULT_PSRS_APPLICANT_ID,
        "email": DEFAULT_EMAIL,
        "pri": DEFAULT_PRI,
        "password": DEFAULT_PASSWORD,
        "username": DEFAULT_EMAIL,
    }

    url = "/oec-cat/api/auth/users/"

    print_level_1("Testing the creation of an account: ")

    print_level_2("Username: {0}".format(DEFAULT_EMAIL))

    response = self.client.post(
        url,
        data=json.dumps(data),
        content_type="application/json",
    )
    self.assertEquals(response.status_code, status.HTTP_201_CREATED)
    print_level_3("HTTP_201_CREATED")
    print_ok_level_4()

    print_level_3("Making sure the user is now in the database: ")
    user = User.objects.get(username=DEFAULT_EMAIL)
    self.assertTrue(user)
    print_ok_level_4()

    # -------------------------------------------------------------------------------------------------#

    print_level_1("Testing the creation of a duplicate account: ")
    print_level_2("Username: {0}".format(DEFAULT_EMAIL))
    response = self.client.post(
        url,
        data=json.dumps(data),
        content_type="application/json",
    )
    self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)
    print_level_3("HTTP_400_BAD_REQUEST")
    print_ok_level_4()

    print_level_3(
        "Making sure we only have 1 user with that username in the database: "
    )
    user_count = User.objects.filter(username=DEFAULT_EMAIL).count()
    self.assertEquals(user_count, 1)
    print_ok_level_4()

    # Delete the user we have created before
    user.delete()

    # -------------------------------------------------------------------------------------------------#

    print_level_1("Testing the creation of an account with missing arguments: ")

    print_level_2("Missing First Name:")
    data = {
        # "first_name": DEFAULT_FIRST_NAME,
        "last_name": DEFAULT_LAST_NAME,
        "birth_date": DEFAULT_BIRTH_DATE,
        "psrs_applicant_id": DEFAULT_PSRS_APPLICANT_ID,
        "email": DEFAULT_EMAIL,
        "pri": DEFAULT_PRI,
        "password": DEFAULT_PASSWORD,
        "username": DEFAULT_EMAIL,
    }

    response = self.client.post(
        url,
        data=json.dumps(data),
        content_type="application/json",
    )
    self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)
    print_level_3("HTTP_400_BAD_REQUEST")
    print_ok_level_4()

    # -------------------------------------------------------------------------------------------------#

    print_level_2("Missing Last Name:")
    data = {
        "first_name": DEFAULT_FIRST_NAME,
        # "last_name": DEFAULT_LAST_NAME,
        "birth_date": DEFAULT_BIRTH_DATE,
        "psrs_applicant_id": DEFAULT_PSRS_APPLICANT_ID,
        "email": DEFAULT_EMAIL,
        "pri": DEFAULT_PRI,
        "password": DEFAULT_PASSWORD,
        "username": DEFAULT_EMAIL,
    }

    response = self.client.post(
        url,
        data=json.dumps(data),
        content_type="application/json",
    )
    self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)
    print_level_3("HTTP_400_BAD_REQUEST")
    print_ok_level_4()

    # -------------------------------------------------------------------------------------------------#

    print_level_2("Missing Birth Date:")
    data = {
        "first_name": DEFAULT_FIRST_NAME,
        "last_name": DEFAULT_LAST_NAME,
        # "birth_date": DEFAULT_BIRTH_DATE,
        "psrs_applicant_id": DEFAULT_PSRS_APPLICANT_ID,
        "email": DEFAULT_EMAIL,
        "pri": DEFAULT_PRI,
        "password": DEFAULT_PASSWORD,
        "username": DEFAULT_EMAIL,
    }

    response = self.client.post(
        url,
        data=json.dumps(data),
        content_type="application/json",
    )
    self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)
    print_level_3("HTTP_400_BAD_REQUEST")
    print_ok_level_4()

    # -------------------------------------------------------------------------------------------------#

    print_level_2("Missing Email (Works if username is included):")
    data = {
        "first_name": DEFAULT_FIRST_NAME,
        "last_name": DEFAULT_LAST_NAME,
        "birth_date": DEFAULT_BIRTH_DATE,
        "psrs_applicant_id": DEFAULT_PSRS_APPLICANT_ID,
        # "email": DEFAULT_EMAIL,
        "pri": DEFAULT_PRI,
        "password": DEFAULT_PASSWORD,
        "username": DEFAULT_EMAIL,
    }

    response = self.client.post(
        url,
        data=json.dumps(data),
        content_type="application/json",
    )
    self.assertEquals(response.status_code, status.HTTP_201_CREATED)
    print_level_3("HTTP_201_CREATED")
    print_ok_level_4()

    # Delete the created user
    User.objects.get(username=DEFAULT_EMAIL).delete()

    # -------------------------------------------------------------------------------------------------#

    print_level_2("Missing Password:")
    data = {
        "first_name": DEFAULT_FIRST_NAME,
        "last_name": DEFAULT_LAST_NAME,
        "birth_date": DEFAULT_BIRTH_DATE,
        "psrs_applicant_id": DEFAULT_PSRS_APPLICANT_ID,
        "email": DEFAULT_EMAIL,
        "pri": DEFAULT_PRI,
        # "password": DEFAULT_PASSWORD,
        "username": DEFAULT_EMAIL,
    }

    response = self.client.post(
        url,
        data=json.dumps(data),
        content_type="application/json",
    )
    self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)
    print_level_3("HTTP_400_BAD_REQUEST")
    print_ok_level_4()

    # -------------------------------------------------------------------------------------------------#

    print_level_2("Missing Username:")
    data = {
        "first_name": DEFAULT_FIRST_NAME,
        "last_name": DEFAULT_LAST_NAME,
        "birth_date": DEFAULT_BIRTH_DATE,
        "psrs_applicant_id": DEFAULT_PSRS_APPLICANT_ID,
        "email": DEFAULT_EMAIL,
        "pri": DEFAULT_PRI,
        "password": DEFAULT_PASSWORD,
        # "username": DEFAULT_EMAIL
    }

    response = self.client.post(
        url,
        data=json.dumps(data),
        content_type="application/json",
    )
    self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)
    print_level_3("HTTP_400_BAD_REQUEST")
    print_ok_level_4()

    # -------------------------------------------------------------------------------------------------#

    print_level_2("Missing PRI or Military Nbr (Works without it):")
    data = {
        "first_name": DEFAULT_FIRST_NAME,
        "last_name": DEFAULT_LAST_NAME,
        "birth_date": DEFAULT_BIRTH_DATE,
        "psrs_applicant_id": DEFAULT_PSRS_APPLICANT_ID,
        "email": DEFAULT_EMAIL,
        # "pri": DEFAULT_PRI,
        "password": DEFAULT_PASSWORD,
        "username": DEFAULT_EMAIL,
    }

    response = self.client.post(
        url,
        data=json.dumps(data),
        content_type="application/json",
    )
    self.assertEquals(response.status_code, status.HTTP_201_CREATED)
    print_level_3("HTTP_201_CREATED")
    print_ok_level_4()

    # Delete the created user
    User.objects.get(username=DEFAULT_EMAIL).delete()

    # -------------------------------------------------------------------------------------------------#

    print_level_2("Missing PSRS Applicant ID (Works without it):")
    data = {
        "first_name": DEFAULT_FIRST_NAME,
        "last_name": DEFAULT_LAST_NAME,
        "birth_date": DEFAULT_BIRTH_DATE,
        # "psrs_applicant_id": DEFAULT_PSRS_APPLICANT_ID,
        "email": DEFAULT_EMAIL,
        "pri": DEFAULT_PRI,
        "password": DEFAULT_PASSWORD,
        "username": DEFAULT_EMAIL,
    }

    response = self.client.post(
        url,
        data=json.dumps(data),
        content_type="application/json",
    )
    self.assertEquals(response.status_code, status.HTTP_201_CREATED)
    print_level_3("HTTP_201_CREATED")
    print_ok_level_4()

    # Delete the created user
    User.objects.get(username=DEFAULT_EMAIL).delete()

    # -------------------------------------------------------------------------------------------------#

    print_level_1(
        "Testing the creation of an account with password formatting issues: "
    )

    print_level_2("Password with only numbers (128 chars):")
    data = {
        "first_name": DEFAULT_FIRST_NAME,
        "last_name": DEFAULT_LAST_NAME,
        "birth_date": DEFAULT_BIRTH_DATE,
        "psrs_applicant_id": DEFAULT_PSRS_APPLICANT_ID,
        "email": DEFAULT_EMAIL,
        "pri": DEFAULT_PRI,
        "password": "12345648978151354685456165165156132165498165132798156132132156498546984169871981232737849456465651989415213274981651158489419181",
        "username": DEFAULT_EMAIL,
    }

    response = self.client.post(
        url,
        data=json.dumps(data),
        content_type="application/json",
    )
    self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)
    print_level_3("HTTP_400_BAD_REQUEST")
    print_ok_level_4()

    # -------------------------------------------------------------------------------------------------#

    print_level_2("Password working (128 chars):")
    data = {
        "first_name": DEFAULT_FIRST_NAME,
        "last_name": DEFAULT_LAST_NAME,
        "birth_date": DEFAULT_BIRTH_DATE,
        "psrs_applicant_id": DEFAULT_PSRS_APPLICANT_ID,
        "email": DEFAULT_EMAIL,
        "pri": DEFAULT_PRI,
        "password": "WvsvPktB$W@AfWWy@DsDzu8WwpzZpc$82E4kfMq*PkT7YQ6Vqy@Jy7wPPE6k%Dy6wbkTBf=KaKUvvXUK5KME~MSSn4K&*D4zz6xbWz^vCVTYWbZeXXqWBCVRbqtHhGSa",
        "username": DEFAULT_EMAIL,
    }

    response = self.client.post(
        url,
        data=json.dumps(data),
        content_type="application/json",
    )
    self.assertEquals(response.status_code, status.HTTP_201_CREATED)
    print_level_3("HTTP_201_CREATED")
    print_ok_level_4()

    # Delete the created user
    User.objects.get(username=DEFAULT_EMAIL).delete()

    # -------------------------------------------------------------------------------------------------#

    print_level_2("Password working (Very huge):")
    # This password should be working since the database is saving the password as hash
    # meaning that whatever the size of the password, it will be separated in parts before being encrypted
    data = {
        "first_name": DEFAULT_FIRST_NAME,
        "last_name": DEFAULT_LAST_NAME,
        "birth_date": DEFAULT_BIRTH_DATE,
        "psrs_applicant_id": DEFAULT_PSRS_APPLICANT_ID,
        "email": DEFAULT_EMAIL,
        "pri": DEFAULT_PRI,
        "password": "WvsvPktB$W@AfWWy@DsDzu8WwpzZpc$82E4kfMq*PkT7YQ6Vqy@Jy7wPPE6k%Dy6wbkTBf=KaKUvvXUK5KME~MSSn4K&*D4zz6xbWz^vCVTYWbZeXXqWBCVRbqtHhGSaWvsvPktB$W@AfWWy@DsDzu8WwpzZpc$82E4kfMq*PkT7YQ6Vqy@Jy7wPPE6k%Dy6wbkTBf=KaKUvvXUK5KME~MSSn4K&*D4zz6xbWz^vCVTYWbZeXXqWBCVRbqtHhGSaWvsvPktB$W@AfWWy@DsDzu8WwpzZpc$82E4kfMq*PkT7YQ6Vqy@Jy7wPPE6k%Dy6wbkTBf=KaKUvvXUK5KME~MSSn4K&*D4zz6xbWz^vCVTYWbZeXXqWBCVRbqtHhGSa",
        "username": DEFAULT_EMAIL,
    }

    response = self.client.post(
        url,
        data=json.dumps(data),
        content_type="application/json",
    )
    self.assertEquals(response.status_code, status.HTTP_201_CREATED)
    print_level_3("HTTP_201_CREATED")
    print_ok_level_4()

    # Delete the created user
    User.objects.get(username=DEFAULT_EMAIL).delete()

    # -------------------------------------------------------------------------------------------------#

    print_level_2("Password of 8 characters (too easy):")
    data = {
        "first_name": DEFAULT_FIRST_NAME,
        "last_name": DEFAULT_LAST_NAME,
        "birth_date": DEFAULT_BIRTH_DATE,
        "psrs_applicant_id": DEFAULT_PSRS_APPLICANT_ID,
        "email": DEFAULT_EMAIL,
        "pri": DEFAULT_PRI,
        "password": "12345678",
        "username": DEFAULT_EMAIL,
    }

    response = self.client.post(
        url,
        data=json.dumps(data),
        content_type="application/json",
    )
    self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)
    print_level_3("HTTP_400_BAD_REQUEST")
    print_ok_level_4()

    # -------------------------------------------------------------------------------------------------#

    print_level_2("Password of 8 characters (difficult):")
    data = {
        "first_name": DEFAULT_FIRST_NAME,
        "last_name": DEFAULT_LAST_NAME,
        "birth_date": DEFAULT_BIRTH_DATE,
        "psrs_applicant_id": DEFAULT_PSRS_APPLICANT_ID,
        "email": DEFAULT_EMAIL,
        "pri": DEFAULT_PRI,
        "password": "7!9Y=??W",
        "username": DEFAULT_EMAIL,
    }

    response = self.client.post(
        url,
        data=json.dumps(data),
        content_type="application/json",
    )
    self.assertEquals(response.status_code, status.HTTP_201_CREATED)
    print_level_3("HTTP_201_CREATED")
    print_ok_level_4()

    # Delete the created user
    User.objects.get(username=DEFAULT_EMAIL).delete()

    # -------------------------------------------------------------------------------------------------#

    print_level_2("Password of 7 characters (difficult) - Not long enough:")
    data = {
        "first_name": DEFAULT_FIRST_NAME,
        "last_name": DEFAULT_LAST_NAME,
        "birth_date": DEFAULT_BIRTH_DATE,
        "psrs_applicant_id": DEFAULT_PSRS_APPLICANT_ID,
        "email": DEFAULT_EMAIL,
        "pri": DEFAULT_PRI,
        "password": "7!9Y=??",
        "username": DEFAULT_EMAIL,
    }

    response = self.client.post(
        url,
        data=json.dumps(data),
        content_type="application/json",
    )
    self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)
    print_level_3("HTTP_400_BAD_REQUEST")
    print_ok_level_4()

    # -------------------------------------------------------------------------------------------------#

    print_sub_task_end("EXECUTING ENDPOINT TESTS - create_account")

    print_task_end()
