from rest_framework import status
from tests.Utils.util_in_test import get_questions_with_answers_list_from_response
from cms.static.test_section_component_type import TestSectionComponentType
from cms.cms_models.answer import Answer
from tests.Utils.util_endpoint import get_header_for_endpoint_call
from tests.Utils.util_users import create_access_token
from cms.static.test_section_type import TestSectionType
from cms.cms_models.test_section_component import TestSectionComponent
from cms.views.retrieve_test_section_data import retrieve_test_section_data
from backend.static.languages import Languages
from backend.static.assigned_test_status import (
    AssignedTestStatus,
    get_string_assigned_test_status,
)
from backend.custom_models.assigned_test import AssignedTest
from tests.Utils.util_check_in import (
    checking_in_users_with_uit_access_codes,
    checking_in_single_user_with_uit_access_code,
)
from tests.backend.uit_invitations import create_test_permission_data
from cms.cms_models.test_definition import TestDefinition
from cms.cms_models.test_section import TestSection
from backend.custom_models.uit_invites import UITInvites
from tests.Utils.util_uit_invitations import send_uit_invitation
from user_management.static.permission import Permission
from user_management.user_management_models.custom_permissions_model import (
    CustomPermissions,
)
from tests.Utils.util_permissions import (
    create_all_custom_permissions,
    create_custom_user_permission,
)
from tests.Utils.util_print import (
    print_setup_start,
    print_sub_task_start,
    print_sub_task_end,
    print_setup_end,
    print_task_start,
    print_task_end,
    print_ok_level_4,
    print_ok_level_5,
    print_ok_level_6,
    print_ok_level_7,
    print_level_1,
    print_level_2,
    print_level_3,
    print_level_4,
    print_level_5,
    print_level_6,
)
from tests.Utils.util_test_data import upload_test
from tests.Utils.util_users import create_user
from backend.custom_models.unsupervised_test_access_code_model import (
    UnsupervisedTestAccessCode,
)

FOLDER_NAME = "Backend"
FILE_NAME = "In_Test"

TESTER_USERNAME_1 = "tester1@email.ca"
TA_USERNAME_1 = "ta1@email.ca"

JSON_FILE_NAME_1 = "test_1.json"

TEST_DEFINITION_ID = 0


def setup_data_for_in_test_tests(self):
    print_setup_start(FOLDER_NAME, FILE_NAME)

    print_sub_task_start("create_all_custom_permissions")

    create_all_custom_permissions()

    print_sub_task_end("create_all_custom_permissions")

    print_sub_task_start("upload_json_test_data")

    # Upload Tests
    new_test_definition_id = upload_test(JSON_FILE_NAME_1).data
    new_test_definition = TestDefinition.objects.get(id=new_test_definition_id)

    print_sub_task_end("upload_json_test_data")

    print_sub_task_start("create_users_data")

    # Create Users
    tester_user_1, ta_user_1 = create_users_data()

    tester_users = []

    # Tester User - Current user in the application
    tester_users.append(
        {
            "first_name": tester_user_1.first_name,
            "last_name": tester_user_1.last_name,
            "email": tester_user_1.email,
        }
    )

    # Give Custom User Permissions
    # Test Administrator
    ta_custom_user_permission_for_ta_user_1 = create_custom_user_permissions_data(
        ta_user_1
    )

    # Give Custom Test Permission to TA - Test Definition
    new_test_permission = create_test_permission_data(new_test_definition, ta_user_1)

    # Get the Test Section for the questions
    test_section = TestSection.objects.exclude(default_time=None).first()

    # Create UIT Invites
    send_uit_invitation(
        self,
        ta_user_1,
        tester_users,
        new_test_definition,
        test_section,
        new_test_permission,
    )

    print_sub_task_end("create_users_data")

    print_setup_end()


# Function used to test if a user can use the test_access_code to check-in
def check_in_with_uit_test_access_code(self):
    print_task_start(
        folder_name=FOLDER_NAME,
        file_name=FILE_NAME,
        function_name="check_in_with_uit_test_access_code",
    )

    print_sub_task_start("SETTING UP DATA FOR check_in_with_uit_test_access_code")

    test_access_code_tester_user_1 = getting_uit_test_access_codes()

    # Array of Users with Their Test Access Code - Future Proof If We Need More Users
    users_and_test_access_codes = []

    users_and_test_access_codes.append(
        {
            "username": TESTER_USERNAME_1,
            "test_access_code": test_access_code_tester_user_1,
        }
    )

    checking_in_users_with_uit_access_codes(self, users_and_test_access_codes)

    print_sub_task_end("SETTING UP DATA FOR check_in_with_uit_test_access_code")

    print_sub_task_start(
        "EXECUTING ENDPOINT TESTS - check_in_with_uit_test_access_code"
    )

    # Making sure that an AssignedTest has been created for our users
    test_creation_of_assigned_tests(self, users_and_test_access_codes)

    print_sub_task_end("EXECUTING ENDPOINT TESTS - check_in_with_uit_test_access_code")

    print_task_end()


# Function used to test in-test functionnalities
def in_test_functionnalities_testing(self):
    print_task_start(
        folder_name=FOLDER_NAME,
        file_name=FILE_NAME,
        function_name="in_test_functionnalities_testing",
    )

    print_sub_task_start("SETTING UP DATA FOR in_test_functionnalities_testing")

    test_access_code_tester_user_1 = getting_uit_test_access_codes()

    checking_in_single_user_with_uit_access_code(
        self, TESTER_USERNAME_1, test_access_code_tester_user_1
    )

    print_sub_task_end("SETTING UP DATA FOR in_test_functionnalities_testing")

    print_sub_task_start("EXECUTING ENDPOINT TESTS - in_test_functionnalities_testing")

    assigned_test = AssignedTest.objects.get(
        username=TESTER_USERNAME_1, status=AssignedTestStatus.READY
    )

    # Getting the Current Test Section Data - Like when we call the endpoint
    print_level_1("Getting First Test Section Data")
    response = retrieve_test_section_data(0, assigned_test.id, Languages.EN)

    print_level_2("ASSIGNED TEST ID: {0}".format(assigned_test.id))
    print_level_2("SECTION ID: {0}".format(response.data["id"]))

    test_definition_id = TestSection.objects.get(
        id=response.data["id"]
    ).test_definition.id

    print_level_2("TEST DEFINITION ID: {0}".format(test_definition_id))

    test_sections = TestSection.objects.filter(
        test_definition=test_definition_id
    ).order_by("order")

    print_level_2("TEST SECTIONS LENGHT: {0}".format(len(test_sections)))

    print_level_1("Testing - Looping Into Test Sections")

    # Go through each Test Section to verify data
    for test_section in test_sections:
        print_level_2("CURRENT SECTION ORDER: {0}".format(test_section.order))
        print_level_3("Assigned Test Status Before Getting Current Section: ")
        print_level_4(
            "{0} ({1})".format(
                assigned_test.status,
                get_string_assigned_test_status(assigned_test.status),
            )
        )

        # If we did a "retrieve_test_section_data" and it did submit; reset to Active Status
        if assigned_test.status == AssignedTestStatus.SUBMITTED:
            print_level_4(
                "We got an AssignedTestStatus == SUBMITTED, reverting to AssignedTestStatus.ACTIVE"
            )
            assigned_test.status = AssignedTestStatus.ACTIVE
            assigned_test.save()
            self.assertEquals(
                AssignedTestStatus.ACTIVE,
                AssignedTest.objects.get(id=assigned_test.id).status,
            )
            print_ok_level_5()

        # Getting all the json - Questions/Answers included
        test_section_response = None

        # TestSectionType of QUIT is treated differently than others; need more parameters in "retrieve_test_section_data"
        # Since we are not using directly the endpoint, we have to specify the arguments
        if test_section.section_type == TestSectionType.QUIT:
            test_section_response = retrieve_test_section_data(
                test_section.order - 1, assigned_test.id, Languages.EN, True, False
            )

        else:
            test_section_response = retrieve_test_section_data(
                test_section.order - 1, assigned_test.id, Languages.EN
            )

        # Getting the AssignedTest after executing "retrieve_test_section_data"
        assigned_test = AssignedTest.objects.get(id=assigned_test.id)

        print_level_3("Same Test Section Order as response given by endpoint?")
        self.assertEquals(
            TestSection.objects.get(id=test_section_response.data["id"]).order,
            test_section.order,
        )
        print_ok_level_4()

        # We know that we are at the end of the sections - Needs to be FINISH
        # (Before Last - Test Sections)
        if test_section.order == len(test_sections) - 1:
            print_level_3("Assigned Test Status After Getting Current Section: ")
            print_level_4(
                "{0} ({1})".format(
                    assigned_test.status,
                    get_string_assigned_test_status(assigned_test.status),
                )
            )
            self.assertEquals(assigned_test.status, AssignedTestStatus.SUBMITTED)
            print_ok_level_5()

            # Verify that "next_section_button_type" is 0
            print_level_3("End of the Test - Next Section Button Type is now 0")
            self.assertEquals(test_section.next_section_button_type, 0)
            print_ok_level_4()

            print_level_3("Test Section Type Should Be FINISH")
            self.assertEquals(test_section.section_type, TestSectionType.FINISH)
            print_ok_level_4()

        # We know that we are at the end of the sections - Needs to be QUIT
        # (Last - Test Sections)
        elif test_section.order == len(test_sections):
            print_level_3("Assigned Test Status After Getting Current Section: ")
            print_level_4(
                "{0} ({1})".format(
                    assigned_test.status,
                    get_string_assigned_test_status(assigned_test.status),
                )
            )
            self.assertEquals(assigned_test.status, AssignedTestStatus.QUIT)
            print_ok_level_5()

            # Verify that "next_section_button_type" is 0
            print_level_3("End of the Test - Next Section Button Type is now 0")
            self.assertEquals(test_section.next_section_button_type, 0)
            print_ok_level_4()

            print_level_3("Test Section Type Should Be QUIT")
            self.assertEquals(test_section.section_type, TestSectionType.QUIT)
            print_ok_level_4()

        else:
            # When we have a "default_time", we know it is a test section with questions
            if test_section.default_time:
                print_level_3("Assigned Test Status After Getting Current Section: ")
                print_level_4(
                    "{0} ({1})".format(
                        assigned_test.status,
                        get_string_assigned_test_status(assigned_test.status),
                    )
                )
                self.assertEquals(assigned_test.status, AssignedTestStatus.ACTIVE)
                print_ok_level_5()

                # We are in a Test with Questions
                print_level_3("QUESTIONS SECTION:")

                # Create our List of Questions / Answers
                # (Goal: have a clean list to better view data)
                questions_with_answers_list = (
                    get_questions_with_answers_list_from_response(test_section_response)
                )

                (
                    viewed_question_id,
                    answered_question_id,
                    answered_answer_id,
                    mark_review_question_id,
                ) = get_data_for_unit_test(self, questions_with_answers_list)

                print_level_3("UNIT TESTS:")

                access_token = create_access_token(self, TESTER_USERNAME_1)
                headers = get_header_for_endpoint_call(access_token)

                # Get test section component ID
                test_section_component_id = (
                    TestSectionComponent.objects.filter(
                        test_section=test_section.id,
                        component_type=TestSectionComponentType.QUESTION_LIST,
                    )
                    .first()
                    .id
                )

                # Simulate a "Viewed Question" + Unit Tests
                simulate_viewed_question(
                    self,
                    assigned_test,
                    viewed_question_id,
                    test_section_component_id,
                    headers,
                )

                # Simulate "Answered Question" + Unit Tests
                simulate_answered_question(
                    self,
                    assigned_test,
                    answered_question_id,
                    answered_answer_id,
                    test_section_component_id,
                    headers,
                )

                # Simulate a "Mark for Review" + Unit Tests
                simulate_mark_for_review(
                    self,
                    test_section,
                    assigned_test,
                    mark_review_question_id,
                    test_section_component_id,
                    headers,
                )

            else:
                print_level_3("Assigned Test Status After Getting Current Section: ")
                print_level_4(
                    "{0} ({1})".format(
                        assigned_test.status,
                        get_string_assigned_test_status(assigned_test.status),
                    )
                )
                self.assertEquals(assigned_test.status, AssignedTestStatus.PRE_TEST)
                print_ok_level_5()

    print_sub_task_end("EXECUTING ENDPOINT TESTS - in_test_functionnalities_testing")

    print_task_end()


# Sub-Function - Create Users
def create_users_data():
    print_level_1("Creating Users: ")
    tester_user_1 = create_user(username=TESTER_USERNAME_1)
    print_level_2(tester_user_1.username)

    ta_user_1 = create_user(username=TA_USERNAME_1)
    print_level_2(ta_user_1.username)

    return tester_user_1, ta_user_1


# Sub-Function - Create Custom User Permissions
def create_custom_user_permissions_data(ta_user_1):
    print_level_1("Creating Custom User Permissions: ")
    ta_custom_permission = CustomPermissions.objects.filter(
        codename=Permission.TEST_ADMINISTRATOR
    ).last()

    ta_custom_user_permission_for_ta_user_1 = create_custom_user_permission(
        username=ta_user_1, permission=ta_custom_permission
    )
    print_level_2(
        "User: {0} Permission: {1}".format(
            ta_custom_user_permission_for_ta_user_1.user,
            ta_custom_user_permission_for_ta_user_1.permission,
        )
    )

    return ta_custom_user_permission_for_ta_user_1


# Sub-Function - Create UIT Invites
def create_uit_invite(ta_custom_user_permission_for_ta_user_1):
    print_level_1("Creating UIT Invites: ")
    uit_invite_1 = UITInvites.objects.create(
        username=ta_custom_user_permission_for_ta_user_1.username,
        ta_test_permissions=ta_custom_user_permission_for_ta_user_1.permission,
    )

    print_level_2(uit_invite_1)


# Sub-Function - Get UIT Invite Codes
def getting_uit_test_access_codes():
    test_definition_id = TestDefinition.objects.get(
        parent_code="8877", test_code="SQ F000 A1", version=1
    ).id

    print_level_1("Getting UIT Test Access Codes: ")

    # Tester User Currently in the System
    print_level_2("For {0}: ".format(TESTER_USERNAME_1))

    test_access_code_test_user_1 = UnsupervisedTestAccessCode.objects.get(
        test=test_definition_id,
        ta_username=TA_USERNAME_1,
        candidate_email=TESTER_USERNAME_1,
    ).test_access_code

    print_level_3("{0}".format(test_access_code_test_user_1))

    return test_access_code_test_user_1


# Sub-Function - Test If the Assigned Tests have been created
def test_creation_of_assigned_tests(self, users_and_test_access_codes):
    print_level_1("Testing if assigned tests have been created for our users: ")

    for user_and_access_code in users_and_test_access_codes:
        print_level_2("User, {0}: ".format(user_and_access_code["username"]))

        assigned_test = AssignedTest.objects.get(
            username=user_and_access_code["username"], status=AssignedTestStatus.READY
        )

        self.assertTrue(assigned_test)

        print_level_3("Assigned Test ID: {0}".format(assigned_test.id))

        print_ok_level_4()


# Sub-Function - Get data needed for upcoming Unit Tests
def get_data_for_unit_test(self, questions_with_answers_list):
    viewed_question_id = None
    answered_question_id = None
    answered_answer_id = None
    mark_review_question_id = None

    for index, question_and_answers in enumerate(questions_with_answers_list):
        print_level_4("QUESTION {0}:".format(index + 1))
        print_level_5("ID: {0}".format(question_and_answers["question_id"]))

        if viewed_question_id is None:
            viewed_question_id = question_and_answers["question_id"]
        elif answered_question_id is None:
            answered_question_id = question_and_answers["question_id"]
        elif mark_review_question_id is None:
            mark_review_question_id = question_and_answers["question_id"]

        print_level_5("ANSWERS:")
        nb_answers = 0
        for answer in question_and_answers["answers"]:
            nb_answers += 1
            if answered_question_id and answered_answer_id is None:
                answered_answer_id = answer["id"]
            print_level_6("ID: {0} Order: {1}".format(answer["id"], answer["order"]))
        print_level_5("UNIT TESTS:")
        print_level_6(
            "Are there really {0} answers for that question in the database?".format(
                nb_answers
            )
        )
        self.assertEquals(
            nb_answers,
            Answer.objects.filter(question=question_and_answers["question_id"]).count(),
        )
        print_ok_level_7()

    return (
        viewed_question_id,
        answered_question_id,
        answered_answer_id,
        mark_review_question_id,
    )


# Sub-Function - Simulate an Answered Question + Unit Tests
def simulate_answered_question(
    self,
    assigned_test,
    answered_question_id,
    answered_answer_id,
    test_section_component_id,
    headers,
):
    # Simulate an "Answered Question"
    print_level_4("Simulate an " "Answered Question" ": ")

    # "View" the question
    print_level_5('"View" the question - Using same endpoint tested before')
    url = "/oec-cat/api/uit-seen-question/?assigned_test_id={0}&question_id={1}&test_section_component_id={2}".format(
        assigned_test.id, answered_question_id, test_section_component_id
    )
    response = self.client.post(
        url,
        **headers,
    )
    self.assertEquals(response.status_code, status.HTTP_200_OK)
    print_ok_level_6()

    # Save the answer for that question
    url = "/oec-cat/api/uit-save-answer/?assigned_test_id={0}&question_id={1}&answer_id={2}&test_section_component_id={3}&selected_language_id={4}".format(
        assigned_test.id,
        answered_question_id,
        answered_answer_id,
        test_section_component_id,
        Languages.EN,
    )

    print_level_5("DATA:")
    print_level_6("Assigned_test_id: {0}".format(assigned_test.id))
    print_level_6("Question_id: {0}".format(answered_question_id))
    print_level_6("Answer_id: {0}".format(answered_answer_id))
    print_level_6("Language_id: {0}".format(Languages.EN))

    access_token = create_access_token(self, TESTER_USERNAME_1)

    headers = get_header_for_endpoint_call(access_token)

    response = self.client.post(
        url,
        **headers,
    )

    print_level_5("Calling Endpoint - UIT Save Answer:")
    print_level_6("Response: {0}".format(response.status_code))
    self.assertEquals(response.status_code, status.HTTP_200_OK)
    print_ok_level_7()


# Sub-Function - Simulate a Viewed Question + Unit Tests
def simulate_viewed_question(
    self, assigned_test, viewed_question_id, test_section_component_id, headers
):
    print_level_4("Simulate a " "Viewed Question" ": ")
    url = "/oec-cat/api/uit-seen-question/?assigned_test_id={0}&question_id={1}&test_section_component_id={2}".format(
        assigned_test.id, viewed_question_id, test_section_component_id
    )
    print_level_5("DATA:")
    print_level_6("Question_id: {0}".format(viewed_question_id))
    print_level_6("Assigned_test_id: {0}".format(assigned_test.id))

    response = self.client.post(
        url,
        **headers,
    )

    print_level_5("Calling Endpoint - UIT Seen Question:")
    print_level_6("Response: {0}".format(response.status_code))
    self.assertEquals(response.status_code, status.HTTP_200_OK)
    print_ok_level_7()


# Sub-Function - Simulate a Mark for Review Question + Unit Tests
def simulate_mark_for_review(
    self,
    test_section,
    assigned_test,
    mark_review_question_id,
    test_section_component_id,
    headers,
):
    print_level_4("Simulate a " "Mark for Review - Question" ": ")

    # "View" the question
    print_level_5('"View" the question - Using same endpoint tested before')
    url = "/oec-cat/api/uit-seen-question/?assigned_test_id={0}&question_id={1}&test_section_component_id={2}".format(
        assigned_test.id, mark_review_question_id, test_section_component_id
    )
    response = self.client.post(
        url,
        **headers,
    )
    self.assertEquals(response.status_code, status.HTTP_200_OK)
    print_ok_level_6()

    test_section_component = TestSectionComponent.objects.get(
        test_section=test_section,
        component_type=TestSectionComponentType.QUESTION_LIST,
    )

    # Mark for review that question
    url = "/oec-cat/api/mark-for-review/?assigned_test_id={0}&question_id={1}&test_section_component_id={2}".format(
        assigned_test.id, mark_review_question_id, test_section_component.id
    )

    print_level_5("DATA:")
    print_level_6("Assigned_test_id: {0}".format(assigned_test.id))
    print_level_6("Question_id: {0}".format(mark_review_question_id))
    print_level_6("Test_section_component_id: {0}".format(test_section_component.id))

    access_token = create_access_token(self, TESTER_USERNAME_1)

    headers = get_header_for_endpoint_call(access_token)

    response = self.client.post(
        url,
        **headers,
    )

    print_level_5("Calling Endpoint - Mark for Review a Question:")
    print_level_6("Response: {0}".format(response.status_code))
    self.assertEquals(response.status_code, status.HTTP_200_OK)
    print_ok_level_7()
