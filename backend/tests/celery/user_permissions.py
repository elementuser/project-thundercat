from tests.Utils.util_permissions import create_all_custom_permissions
from cms.cms_models.orderless_test_permissions_model import OrderlessTestPermissions
from backend.custom_models.orderless_financial_data import OrderlessFinancialData
from user_management.user_management_models.ta_extended_profile_models import (
    TaExtendedProfile,
)
from cms.cms_models.test_permissions_model import TestPermissions
from backend.custom_models.language import Language
from cms.cms_models.test_definition import TestDefinition
from backend.custom_models.test_access_code_model import TestAccessCode
from user_management.user_management_models.custom_permissions_model import (
    CustomPermissions,
)
from user_management.user_management_models.custom_user_permissions_model import (
    CustomUserPermissions,
)
from backend.celery.task_definition.user_permissions import (
    deprovision_admin_after_inactivity_thirty_days,
    deprovision_after_inactivity,
)
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
import pytz
from backend.celery.task_definition.utils import (
    ConsoleMessageColor,
)
from tests.Utils.util_users import (
    create_user,
)

from tests.Utils.util_print import (
    print_setup_start,
    print_setup_end,
    print_task_start,
    print_task_end,
    print_sub_task_start,
    print_sub_task_end,
    print_level_1,
    print_level_2,
    print_level_3,
    print_ok_level_2,
)

FOLDER_NAME = "Celery"
FILE_NAME = "User Permissions"


def setup_data_for_celery_tasks_permission_test():

    print_setup_start(FOLDER_NAME, FILE_NAME)

    create_all_custom_permissions()

    print_setup_end()


def run_deprovision_admin_after_inactivity_thirty_days(self):

    print_task_start(
        FOLDER_NAME,
        FILE_NAME,
        "deprovision_admin_after_inactivity_thirty_days",
    )

    print_sub_task_start(
        "SETTING UP DATA FOR deprovision_admin_after_inactivity_thirty_days"
    )

    # Creating Users
    (
        inactive_user,
        active_user,
    ) = create_users_for_deprovision_admin_after_inactivity_thirty_days()

    # Creating Custom User Permissions Data
    create_custom_user_permissions_for_deprovision_admin_after_inactivity_thirty_days(
        inactive_user, active_user
    )

    print_sub_task_end(
        "SETTING UP DATA FOR deprovision_admin_after_inactivity_thirty_days"
    )

    print_sub_task_start(
        "EXECUTING CELERY TASK - deprovision_admin_after_inactivity_thirty_days"
    )

    # execute celery task
    deprovision_admin_after_inactivity_thirty_days()

    print_sub_task_end(
        "EXECUTING CELERY TASK - deprovision_admin_after_inactivity_thirty_days"
    )

    print_sub_task_start(
        "TESTING OF RESULTS - CELERY TASK - deprovision_admin_after_inactivity_thirty_days"
    )

    # Run Tests for deprovision_admin_after_inactivity_thirty_days
    testing_of_deprovision_admin_after_inactivity_thirty_days(self)

    print_sub_task_end(
        "TESTING OF RESULTS - CELERY TASK - deprovision_admin_after_inactivity_thirty_days"
    )

    print_task_end()


def run_deprovision_after_inactivity(self):

    print_task_start(FOLDER_NAME, FILE_NAME, "deprovision_after_inactivity")

    print_sub_task_start("SETTING UP DATA FOR deprovision_after_inactivity")

    # Creating Users
    (
        inactive_user,
        active_user,
    ) = create_users_for_deprovision_after_inactivity()

    # Creating Custom User Permissions Data
    create_custom_user_permissions_for_deprovision_after_inactivity(
        inactive_user, active_user
    )

    # Creating Test Definition Data
    (
        test_definition_1,
        test_definition_2,
    ) = create_test_definition_data_for_deprovision_after_inactivity()

    # Creating Test Access Code Data
    create_test_access_code_data_for_deprovision_after_inactivity(
        inactive_user,
        active_user,
        test_definition_1,
    )

    # Creating Test Permission Data
    (
        inactive_ta_extended_profile_1,
        inactive_ta_extended_profile_2,
        active_ta_extended_profile_1,
        active_ta_extended_profile_2,
    ) = create_test_permission_data_for_deprovision_after_inactivity(
        inactive_user,
        active_user,
        test_definition_1,
        test_definition_2,
    )

    print_sub_task_end("SETTING UP DATA FOR deprovision_after_inactivity")

    print_sub_task_start("EXECUTING CELERY TASK - deprovision_after_inactivity")

    # execute celery task
    deprovision_after_inactivity()

    print_sub_task_end("EXECUTING CELERY TASK - deprovision_after_inactivity")

    print_sub_task_start(
        "TESTING OF RESULTS - CELERY TASK - deprovision_after_inactivity"
    )

    testing_of_run_deprovision_after_inactivity(
        self,
        inactive_ta_extended_profile_1,
        inactive_ta_extended_profile_2,
        active_ta_extended_profile_1,
        active_ta_extended_profile_2,
    )

    print_sub_task_end(
        "TESTING OF RESULTS - CELERY TASK - deprovision_after_inactivity"
    )

    print_task_end()


def create_users_for_deprovision_admin_after_inactivity_thirty_days():
    # required user variables
    # inactive user
    inactive_username = "inactive_celery_user@email.ca"
    inactive_last_login = datetime.now(tz=pytz.UTC) - timedelta(days=31)

    # active user
    active_username = "active_celery_user@email.ca"
    active_last_login = datetime.now(tz=pytz.UTC)

    # inactive user
    print_level_1("Creating Inactive User with 31 days last_login")
    inactive_user = create_user(
        username=inactive_username, last_login=inactive_last_login
    )

    # inactive user
    print_level_1("Creating Active User with today last_login")
    active_user = create_user(username=active_username, last_login=active_last_login)

    return inactive_user, active_user


def create_custom_user_permissions_for_deprovision_admin_after_inactivity_thirty_days(
    inactive_user, active_user
):
    # common variables
    email = "email@email.ca"
    pri = 123456789
    name = "name"

    # create custom user permissions data
    print_level_1("Creating Custom User Permissions' Data")

    print_level_2("Assigning to inactive user:")
    for permission in CustomPermissions.objects.all():
        print_level_3(permission.en_name)
        CustomUserPermissions.objects.create(
            user_id=inactive_user.username,
            permission=permission,
            goc_email=email,
            pri_or_military_nbr=pri,
            rationale=name,
            supervisor=name,
            supervisor_email=email,
        )

    print_level_2("Assigning to active user:")

    for permission in CustomPermissions.objects.all():
        print_level_3(permission.en_name)
        CustomUserPermissions.objects.create(
            user_id=active_user.username,
            permission=permission,
            goc_email=email,
            pri_or_military_nbr=pri,
            rationale=name,
            supervisor=name,
            supervisor_email=email,
        )


def testing_of_deprovision_admin_after_inactivity_thirty_days(self):
    # INACTIVE USER
    print_level_1("Inactive User - Verify that only 2 Custom User Permissions remain")
    # verify that all permissions but TA and Test Adaptations have been deleted
    self.assertEqual(
        CustomUserPermissions.objects.filter(
            user_id="inactive_celery_user@email.ca"
        ).count(),
        2,
    )
    print_ok_level_2()

    print_level_1(
        "Inactive User - Verify that the Custom User Permission remaining is Test Administrator"
    )

    # verify that all permissions but TA have been deleted
    self.assertEqual(
        CustomUserPermissions.objects.filter(user_id="inactive_celery_user@email.ca")
        .first()
        .permission.en_name,
        "Test Administrator",
    )
    print_ok_level_2()

    # ACTIVE USER
    total_count_user_permissions = CustomPermissions.objects.all().count()

    print_level_1(
        "Active User - Verify that {0} Custom User Permissions remain".format(
            total_count_user_permissions
        )
    )

    # verify that all permissions but TA have been deleted
    self.assertEqual(
        CustomUserPermissions.objects.filter(
            user_id="active_celery_user@email.ca"
        ).count(),
        total_count_user_permissions,
    )
    print_ok_level_2()


def create_users_for_deprovision_after_inactivity():
    # Required user variables
    # Variables - inactive user
    inactive_username = "inactive_celery_user@email.ca"
    inactive_last_login = datetime.now(tz=pytz.UTC) - (
        relativedelta(years=1) + timedelta(days=1)
    )

    # Variables - active user
    active_username = "active_celery_user@email.ca"
    active_last_login = datetime.now(tz=pytz.UTC)

    # Create - inactive user
    print(
        "{0} \t--> Creating Inactive User with 1 year last_login".format(
            ConsoleMessageColor.WHITE
        )
    )
    inactive_user = create_user(
        username=inactive_username, last_login=inactive_last_login
    )

    # Create - inactive user
    print(
        "{0} \t--> Creating Active User with today last_login".format(
            ConsoleMessageColor.WHITE
        )
    )
    active_user = create_user(username=active_username, last_login=active_last_login)

    return inactive_user, active_user


def create_custom_user_permissions_for_deprovision_after_inactivity(
    inactive_user, active_user
):
    # Common variables
    email = "email@email.ca"
    pri = 123456789
    name = "name"

    # Create custom user permissions data
    print_level_1("Creating Custom User Permissions' Data")

    # Create custom user permissions - inactive user
    print_level_2("Assigning Custom User Permission to Inactive User")
    for permission in CustomPermissions.objects.all():
        print_level_3(permission.en_name)
        CustomUserPermissions.objects.create(
            user_id=inactive_user.username,
            permission=permission,
            goc_email=email,
            pri_or_military_nbr=pri,
            rationale=name,
            supervisor=name,
            supervisor_email=email,
        )

    # Create custom user permissions - active user
    print_level_2("Assigning Custom User Permission to Active User")
    for permission in CustomPermissions.objects.all():
        print_level_3(permission.en_name)
        CustomUserPermissions.objects.create(
            user_id=active_user.username,
            permission=permission,
            goc_email=email,
            pri_or_military_nbr=pri,
            rationale=name,
            supervisor=name,
            supervisor_email=email,
        )


def create_test_definition_data_for_deprovision_after_inactivity():
    # Test Definition Data
    print_level_1("Creating Test Definition Data")

    # Create - Fake Test Definition Data
    test_definition_1 = TestDefinition.objects.create(
        parent_code="UNIT_TEST_1",
        test_code="UNIT_TEST_1",
        version=1,
        en_name="Unit Test - Test Definition",
        fr_name="FR Unit Test - Test Definition",
    )

    print_level_2(
        "New Test Definition Created: ID={0} TEST_CODE={1} NAME={2}".format(
            test_definition_1.id,
            test_definition_1.test_code,
            test_definition_1.en_name,
        )
    )

    test_definition_2 = TestDefinition.objects.create(
        parent_code="UNIT_TEST_2",
        test_code="UNIT_TEST_2",
        version=1,
        en_name="Unit Test - Test Definition",
        fr_name="FR Unit Test - Test Definition",
    )

    print_level_2(
        "New Test Definition Created: ID={0} TEST_CODE={1} NAME={2}".format(
            test_definition_2.id,
            test_definition_2.test_code,
            test_definition_2.en_name,
        )
    )

    return test_definition_1, test_definition_2


def create_test_access_code_data_for_deprovision_after_inactivity(
    inactive_user,
    active_user,
    test_definition_1,
):
    # Test Access Code Data
    print_level_1("Creating Test Access Code Data")

    # Test Access Code Data - Inactive User
    print_level_2("Assigning Test Access Code to inactive user:")
    for i in range(0, 5):
        test_access_code_str = "{0}{1}".format("UNITTEST_", i)

        # create some orderless financial data
        if i > 1:
            orderless_financial_data_object = OrderlessFinancialData.objects.create(
                reference_number=test_access_code_str,
                department_ministry_id=1,
                fis_organisation_code=test_access_code_str,
                fis_reference_code=test_access_code_str,
                billing_contact_name=test_access_code_str,
                billing_contact_info=test_access_code_str,
                level_required=test_access_code_str,
                reason_for_testing=None,
                uit_invite=None,
            )
            print_level_3(
                "TEST_ACCESS_CODE (ORDERLESS): {0}".format(test_access_code_str)
            )
            TestAccessCode.objects.create(
                test_access_code=test_access_code_str,
                test_order_number=None,
                test=test_definition_1,
                test_session_language=Language.objects.get(language_id=1),
                ta_username=inactive_user,
                orderless_financial_data=orderless_financial_data_object,
            )

        else:
            print_level_3("TEST_ACCESS_CODE: {0}".format(test_access_code_str))
            TestAccessCode.objects.create(
                test_access_code=test_access_code_str,
                test_order_number=None,
                test=test_definition_1,
                test_session_language=Language.objects.get(language_id=1),
                ta_username=inactive_user,
                orderless_financial_data=None,
            )

    # Test Access Code Data - Active User
    print_level_2("Assigning Test Access Code to active user:")
    for i in range(5, 10):
        test_access_code_str = "{0}{1}".format("UNITTEST_", i)

        # create some orderless financial data
        if i > 6:
            orderless_financial_data_object = OrderlessFinancialData.objects.create(
                reference_number=test_access_code_str,
                department_ministry_id=1,
                fis_organisation_code=test_access_code_str,
                fis_reference_code=test_access_code_str,
                billing_contact_name=test_access_code_str,
                billing_contact_info=test_access_code_str,
                level_required=test_access_code_str,
                reason_for_testing=None,
                uit_invite=None,
            )
            print_level_3(
                "TEST_ACCESS_CODE (ORDERLESS): {0}".format(test_access_code_str)
            )
            TestAccessCode.objects.create(
                test_access_code=test_access_code_str,
                test_order_number=None,
                test=test_definition_1,
                test_session_language=Language.objects.get(language_id=1),
                ta_username=active_user,
                orderless_financial_data=orderless_financial_data_object,
            )

        else:
            print_level_3("TEST_ACCESS_CODE: {0}".format(test_access_code_str))
            TestAccessCode.objects.create(
                test_access_code=test_access_code_str,
                test_order_number=None,
                test=test_definition_1,
                test_session_language=Language.objects.get(language_id=1),
                ta_username=active_user,
                orderless_financial_data=None,
            )


def create_test_permission_data_for_deprovision_after_inactivity(
    inactive_user,
    active_user,
    test_definition_1,
    test_definition_2,
):
    # Test Permission Data
    print_level_1("Creating Test Permission Data")

    # Test Permission Data - Inactive User
    print_level_2("Assigning Test Permissions to inactive user:")
    # Test Permission that would expire later
    test_permission_temp = TestPermissions.objects.create(
        username=inactive_user,
        test=test_definition_1,
        expiry_date="2024-01-01",
        test_order_number="UNIT_TEST",
        staffing_process_number="UNIT_TEST",
        department_ministry_code="UNIT_TEST",
        is_org="UNIT_TEST",
        is_ref="UNIT_TEST",
        billing_contact="UNIT_TEST",
        billing_contact_info="UNIT_TEST",
        reason_for_modif_or_del=None,
    )
    print_level_3(
        "TEST: {0} Expiry Date: {1}".format(
            test_permission_temp.test.test_code, test_permission_temp.expiry_date
        )
    )
    # Test Permission that is expired
    test_permission_temp = TestPermissions.objects.create(
        username=inactive_user,
        test=test_definition_2,
        expiry_date="2020-01-01",
        test_order_number="UNIT_TEST",
        staffing_process_number="UNIT_TEST",
        department_ministry_code="UNIT_TEST",
        is_org="UNIT_TEST",
        is_ref="UNIT_TEST",
        billing_contact="UNIT_TEST",
        billing_contact_info="UNIT_TEST",
        reason_for_modif_or_del=None,
    )
    print_level_3(
        "TEST: {0} Expiry Date: {1}".format(
            test_permission_temp.test.test_code, test_permission_temp.expiry_date
        )
    )

    # Test Permission Data - Active User
    print_level_2("Assigning Test Permissions to active user:")
    # Test Permission that would expire later
    test_permission_temp = TestPermissions.objects.create(
        username=active_user,
        test=test_definition_1,
        expiry_date="2024-01-01",
        test_order_number="UNIT_TEST",
        staffing_process_number="UNIT_TEST",
        department_ministry_code="UNIT_TEST",
        is_org="UNIT_TEST",
        is_ref="UNIT_TEST",
        billing_contact="UNIT_TEST",
        billing_contact_info="UNIT_TEST",
        reason_for_modif_or_del=None,
    )
    print_level_3(
        "TEST: {0} Expiry Date: {1}".format(
            test_permission_temp.test.test_code, test_permission_temp.expiry_date
        )
    )
    # Test Permission that is expired
    test_permission_temp = TestPermissions.objects.create(
        username=active_user,
        test=test_definition_2,
        expiry_date="2020-01-01",
        test_order_number="UNIT_TEST",
        staffing_process_number="UNIT_TEST",
        department_ministry_code="UNIT_TEST",
        is_org="UNIT_TEST",
        is_ref="UNIT_TEST",
        billing_contact="UNIT_TEST",
        billing_contact_info="UNIT_TEST",
        reason_for_modif_or_del=None,
    )
    print_level_3(
        "TEST: {0} Expiry Date: {1}".format(
            test_permission_temp.test.test_code, test_permission_temp.expiry_date
        )
    )

    # ORDERLESS TEST PERMISSIONS
    print_level_1("Creating Orderless Test Permission Data")

    # Create - TaExtendedProfile - Inactive User
    print_level_2("Creating TaExtendedProfiles - Inactive User")

    # We want to make sure that the system doesn't break in a rare case where we would have 2 TaExtendedProfiles for our user
    inactive_ta_extended_profile_1 = TaExtendedProfile.objects.create(
        username=inactive_user, department_id=1
    )
    print_level_3(
        "First TaExtendedProfile has a department_id={0}".format(
            inactive_ta_extended_profile_1.department_id
        )
    )
    inactive_ta_extended_profile_2 = TaExtendedProfile.objects.create(
        username=inactive_user, department_id=2
    )
    print_level_3(
        "Second TaExtendedProfile has a department_id={0}".format(
            inactive_ta_extended_profile_2.department_id
        )
    )

    # Create - TaExtendedProfile - Active User
    print_level_2("Creating TaExtendedProfiles - Active User")

    # We want to make sure that the system doesn't break in a rare case where we would have 2 TaExtendedProfiles for our user
    active_ta_extended_profile_1 = TaExtendedProfile.objects.create(
        username=active_user, department_id=1
    )
    print_level_3(
        "First TaExtendedProfile has a department_id={0}".format(
            active_ta_extended_profile_1.department_id
        )
    )
    active_ta_extended_profile_2 = TaExtendedProfile.objects.create(
        username=active_user, department_id=2
    )
    print_level_3(
        "Second TaExtendedProfile has a department_id={0}".format(
            active_ta_extended_profile_2.department_id
        )
    )

    # Create - OrderlessTestPermissions - Inactive User
    print_level_2("Creating OrderlessTestPermissions - Inactive User")

    # First OrderlessTestPermissions for the inactive_ta_extended_profile_1
    inactive_orderless_test_permissions_1 = OrderlessTestPermissions.objects.create(
        ta_extended_profile=inactive_ta_extended_profile_1,
        parent_code=test_definition_1.parent_code,
        test_code=test_definition_1.test_code,
    )
    print_level_3(
        "First OrderlessTestPermissions - TaExtendedProfile_pk={0}".format(
            inactive_orderless_test_permissions_1.ta_extended_profile.pk,
        )
    )

    # Second OrderlessTestPermissions for the inactive_ta_extended_profile_1
    inactive_orderless_test_permissions_2 = OrderlessTestPermissions.objects.create(
        ta_extended_profile=inactive_ta_extended_profile_1,
        parent_code=test_definition_2.parent_code,
        test_code=test_definition_2.test_code,
    )
    print_level_3(
        "Second OrderlessTestPermissions - TaExtendedProfile_pk={0}".format(
            inactive_orderless_test_permissions_2.ta_extended_profile.pk,
        )
    )

    # First OrderlessTestPermissions for the inactive_ta_extended_profile_2
    inactive_orderless_test_permissions_3 = OrderlessTestPermissions.objects.create(
        ta_extended_profile=inactive_ta_extended_profile_2,
        parent_code=test_definition_1.parent_code,
        test_code=test_definition_1.test_code,
    )
    print_level_3(
        "First OrderlessTestPermissions - TaExtendedProfile_pk={0}".format(
            inactive_orderless_test_permissions_3.ta_extended_profile.pk,
        )
    )

    # Second OrderlessTestPermissions for the inactive_ta_extended_profile_2
    inactive_orderless_test_permissions_4 = OrderlessTestPermissions.objects.create(
        ta_extended_profile=inactive_ta_extended_profile_2,
        parent_code=test_definition_2.parent_code,
        test_code=test_definition_2.test_code,
    )
    print_level_3(
        "Second OrderlessTestPermissions - TaExtendedProfile_pk={0}".format(
            inactive_orderless_test_permissions_4.ta_extended_profile.pk,
        )
    )

    # Create - OrderlessTestPermissions - Active User
    print_level_2("Creating OrderlessTestPermissions - Active User")

    # First OrderlessTestPermissions for the active_ta_extended_profile_1
    active_orderless_test_permissions_1 = OrderlessTestPermissions.objects.create(
        ta_extended_profile=active_ta_extended_profile_1,
        parent_code=test_definition_1.parent_code,
        test_code=test_definition_1.test_code,
    )
    print_level_3(
        "First OrderlessTestPermissions - TaExtendedProfile_pk={0}".format(
            active_orderless_test_permissions_1.ta_extended_profile.pk,
        )
    )

    # Second OrderlessTestPermissions for the active_ta_extended_profile_1
    active_orderless_test_permissions_2 = OrderlessTestPermissions.objects.create(
        ta_extended_profile=active_ta_extended_profile_1,
        parent_code=test_definition_2.parent_code,
        test_code=test_definition_2.test_code,
    )
    print_level_3(
        "Second OrderlessTestPermissions - TaExtendedProfile_pk={0}".format(
            active_orderless_test_permissions_2.ta_extended_profile.pk,
        )
    )

    # First OrderlessTestPermissions for the active_ta_extended_profile_2
    active_orderless_test_permissions_3 = OrderlessTestPermissions.objects.create(
        ta_extended_profile=active_ta_extended_profile_2,
        parent_code=test_definition_1.parent_code,
        test_code=test_definition_1.test_code,
    )
    print_level_3(
        "First OrderlessTestPermissions - TaExtendedProfile_pk={0}".format(
            active_orderless_test_permissions_3.ta_extended_profile.pk,
        )
    )

    # Second OrderlessTestPermissions for the active_ta_extended_profile_2
    active_orderless_test_permissions_4 = OrderlessTestPermissions.objects.create(
        ta_extended_profile=active_ta_extended_profile_2,
        parent_code=test_definition_2.parent_code,
        test_code=test_definition_2.test_code,
    )
    print_level_3(
        "Second OrderlessTestPermissions - TaExtendedProfile_pk={0}".format(
            active_orderless_test_permissions_4.ta_extended_profile.pk,
        )
    )

    return (
        inactive_ta_extended_profile_1,
        inactive_ta_extended_profile_2,
        active_ta_extended_profile_1,
        active_ta_extended_profile_2,
    )


def testing_of_run_deprovision_after_inactivity(
    self,
    inactive_ta_extended_profile_1,
    inactive_ta_extended_profile_2,
    active_ta_extended_profile_1,
    active_ta_extended_profile_2,
):
    # INACTIVE USER
    # TEST ACCESS CODES
    print_level_1("Inactive User - Verify that all Test Access Codes have been deleted")

    # verify that all test access codes have been deleted
    self.assertEqual(
        TestAccessCode.objects.filter(
            ta_username="inactive_celery_user@email.ca"
        ).count(),
        0,
    )
    print_ok_level_2()

    # TEST PERMISSIONS
    print_level_1("Inactive User - Verify that all Test Permissions have been deleted")

    self.assertEqual(
        TestPermissions.objects.filter(
            username="inactive_celery_user@email.ca"
        ).count(),
        0,
    )
    print_ok_level_2()

    # CUSTOM USER PERMISSIONS
    print_level_1(
        "Inactive User - Verify that all Custom User Permission have been deleted"
    )

    # verify that all permissions but TA have been deleted
    self.assertEqual(
        CustomUserPermissions.objects.filter(
            user_id="inactive_celery_user@email.ca"
        ).count(),
        0,
    )
    print_ok_level_2()

    # ORDERLESS TEST PERMISSIONS
    print_level_1(
        "Inactive User - Verify that all Orderless Test Permissions have been deleted"
    )

    # verify that all permissions but TA have been deleted
    self.assertEqual(
        OrderlessTestPermissions.objects.filter(
            ta_extended_profile__in=[
                inactive_ta_extended_profile_1,
                inactive_ta_extended_profile_2,
            ]
        ).count(),
        0,
    )
    print_ok_level_2()

    # ACTIVE USER
    # TEST ACCESS CODES
    print_level_1("Active User - Verify that all Test Access Codes remain")

    total_count_test_access_code = TestAccessCode.objects.all().count()
    # verify that all test access codes remain
    self.assertEqual(
        TestAccessCode.objects.filter(
            ta_username="active_celery_user@email.ca"
        ).count(),
        total_count_test_access_code,
    )
    print_ok_level_2()

    # TEST PERMISSIONS
    print_level_1("Active User - Verify that all Test Permissions remain")

    # verify that all test permissions remain
    self.assertEqual(
        TestPermissions.objects.filter(username="active_celery_user@email.ca").count(),
        TestPermissions.objects.all().count(),
    )
    print_ok_level_2()

    # CUSTOM USER PERMISSIONS
    total_count_user_permissions = CustomPermissions.objects.all().count()

    print_level_1(
        "Active User - Verify that {0} Custom User Permissions remain".format(
            total_count_user_permissions
        )
    )

    # verify that all permissions but TA have been deleted
    self.assertEqual(
        CustomUserPermissions.objects.filter(
            user_id="active_celery_user@email.ca"
        ).count(),
        total_count_user_permissions,
    )
    print_ok_level_2()

    # ORDERLESS TEST PERMISSIONS
    print_level_1("Active User - Verify that all Orderless Test Permissions remain")

    # verify that all permissions but TA have been deleted
    self.assertEqual(
        OrderlessTestPermissions.objects.filter(
            ta_extended_profile__in=[
                active_ta_extended_profile_1,
                active_ta_extended_profile_2,
            ]
        ).count(),
        OrderlessTestPermissions.objects.all().count(),
    )
    print_ok_level_2()
