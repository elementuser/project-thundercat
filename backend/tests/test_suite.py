from django.test import TestCase, override_settings
from rest_framework.test import APITestCase
from tests.backend.account import run_create_account
from tests.backend.uit_invitations import (
    setup_data_for_uit_invitations,
    run_send_uit_invitations,
)
from tests.backend.in_test import (
    setup_data_for_in_test_tests,
    check_in_with_uit_test_access_code,
    in_test_functionnalities_testing,
)
from tests.backend.upload_test import run_upload_test
from tests.environment.test_database_check import (
    setup_database_entry,
    run_get_all_users,
    run_get_specific_user,
)
from tests.celery.user_permissions import (
    setup_data_for_celery_tasks_permission_test,
    run_deprovision_after_inactivity,
    run_deprovision_admin_after_inactivity_thirty_days,
)
from tests.backend.permissions import (
    setup_data_for_permission_test,
    run_get_all_permission_requests,
    run_search_specific_permissions_request,
)

####################################################################################################
# Environment - Unit Tests
# -------------------------------------------------------------------------------------------------#
# Database
# -------------------------------------------------------------------------------------------------#
# It tests the queryset that is used in the view (database_check_view.py),
# but also the model itself (database_check_model.py)
####################################################################################################
class DatabaseEntriesTest(APITestCase):
    def setUp(self):
        setup_database_entry()

    def test_get_all_users(self):
        run_get_all_users(self)

    def test_get_specific_user(self):
        run_get_specific_user(self)


####################################################################################################

####################################################################################################
# Backend - Unit Tests
# -------------------------------------------------------------------------------------------------#
# Permissions
# -------------------------------------------------------------------------------------------------#
# It tests all the rights and permissions endpoints
####################################################################################################
class PermissionsTest(APITestCase):
    def setUp(self):
        setup_data_for_permission_test()

    def test_get_all_permission_requests(self):
        run_get_all_permission_requests(self)

    def test_search_specific_permissions_request(self):
        run_search_specific_permissions_request(self)

####################################################################################################
# -------------------------------------------------------------------------------------------------#
# Create an Account
# -------------------------------------------------------------------------------------------------#
# It tests all the endpoints related to the account creation
####################################################################################################
class CreateAccount(TestCase):
    def test_run_create_account(self):
        run_create_account(self)

####################################################################################################
# -------------------------------------------------------------------------------------------------#
# Upload JSon Test Data
# -------------------------------------------------------------------------------------------------#
# It tests the upload json functionnality
####################################################################################################
class UploadTestJSON(TestCase):
    def test_run_upload_test(self):
        run_upload_test(self)


####################################################################################################
# -------------------------------------------------------------------------------------------------#
# UIT Invitations
# -------------------------------------------------------------------------------------------------#
# It tests all the UIT Invitations functionnalities
####################################################################################################
class UitInvitationsTest(APITestCase):
    def setUp(self):
        setup_data_for_uit_invitations()

    @override_settings(CELERY_TASK_ALWAYS_EAGER=True, CELERY_TASK_EAGER_PROPOGATES=True)
    def test_run_send_uit_invitations(self):
        run_send_uit_invitations(self)


####################################################################################################
# -------------------------------------------------------------------------------------------------#
# In-Test
# -------------------------------------------------------------------------------------------------#
# It tests all the In-Test functionnalities
####################################################################################################
class InTestTest(APITestCase):
    @override_settings(CELERY_TASK_ALWAYS_EAGER=True, CELERY_TASK_EAGER_PROPOGATES=True)
    def setUp(self):
        setup_data_for_in_test_tests(self)

    # Testing - Check-In Function
    def test_check_in_with_uit_test_access_code(self):
        check_in_with_uit_test_access_code(self)

    # Testing - In-Test
    def test_in_test_functionnalities_testing(self):
        in_test_functionnalities_testing(self)


####################################################################################################
# Celery - Unit Tests
# -------------------------------------------------------------------------------------------------#
# Test Permissions
# -------------------------------------------------------------------------------------------------#
####################################################################################################
class PermissionsCeleryTasksTest(TestCase):
    def setUp(self):
        setup_data_for_celery_tasks_permission_test()

    def test_deprovision_admin_after_inactivity_thirty_days(self):
        run_deprovision_admin_after_inactivity_thirty_days(self)

    def test_deprovision_after_inactivity(self):
        run_deprovision_after_inactivity(self)


####################################################################################################
