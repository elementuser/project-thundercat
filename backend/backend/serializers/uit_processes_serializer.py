from rest_framework import serializers
from django.db.models.functions import Upper
from backend.serializers.cat_ref_departments_serializer import (
    CatRefDepartmentsSerializer,
)
from backend.ref_table_views.cat_ref_departments_vw import CatRefDepartmentsVW
from backend.custom_models.orderless_financial_data import OrderlessFinancialData
from backend.custom_models.reasons_for_testing import ReasonsForTesting
from backend.custom_models.uit_reasons_for_deletion import UITReasonsForDeletion
from backend.custom_models.uit_reasons_for_modification import UITReasonsForModification
from backend.custom_models.uit_invites import UITInvites
from backend.custom_models.unsupervised_test_access_code_model import (
    UnsupervisedTestAccessCode,
)
from backend.custom_models.uit_invite_related_candidates import (
    UITInviteRelatedCandidates,
)
from backend.custom_models.assigned_test import AssignedTest
from backend.views.utils import UIT_TEST_STATUS
from backend.static.assigned_test_status import AssignedTestStatus
from cms.cms_models.test_permissions_model import TestPermissions
from cms.serializers.get_test_information_serializer import (
    GetTestPermissionsSerializer,
    GetEnFrTestNameSerializer,
)
from cms.cms_models.test_definition import TestDefinition


def get_candidates_uit_data(uit_invite_id, candidates_list):
    # getting related historical unsupervised test access code data
    related_historical_unsupervised_test_access_codes_data = (
        UnsupervisedTestAccessCode.history.filter(
            uit_invite_id=uit_invite_id, history_type="+"
        )
    )

    # getting related historical unsupervised test access codes
    related_unsupervised_test_access_codes = UnsupervisedTestAccessCode.objects.filter(
        uit_invite_id=uit_invite_id
    ).values_list("test_access_code", flat=True)

    # getting related assigned test data
    related_assigned_test_data = AssignedTest.objects.filter(
        uit_invite_id=uit_invite_id
    )

    # getting related assigned test test access codes
    related_assigned_test_test_access_codes = AssignedTest.objects.filter(
        uit_invite_id=uit_invite_id
    ).values_list(Upper("test_access_code"), flat=True)

    # initializing final_array
    final_array = []

    # looping in candidates_list
    for candidate in candidates_list:
        # initializing needed variables
        related_assigned_test = []
        code_deactivated = False
        custom_test_status = UIT_TEST_STATUS.NOT_TAKEN

        # getting index of unsupervised test access code for the current iteration
        index_of_unsupervised_test_access_code = [
            x.candidate_email
            for x in related_historical_unsupervised_test_access_codes_data
        ].index(candidate.email)

        # getting respective test access code
        historical_uit_test_access_code = (
            related_historical_unsupervised_test_access_codes_data[
                index_of_unsupervised_test_access_code
            ].test_access_code
        )

        # there is no unsupervised test access code
        if (
            historical_uit_test_access_code
            not in related_unsupervised_test_access_codes
        ):
            # found in assigned tests (based on test access codes and uit invite id)
            if (
                historical_uit_test_access_code
                in related_assigned_test_test_access_codes
            ):
                # getting index of assigned test for the current iteration
                index_of_assigned_test = [
                    x.test_access_code.upper() for x in related_assigned_test_data
                ].index(historical_uit_test_access_code)
                # getting respective assigned test data
                related_assigned_test = related_assigned_test_data[
                    index_of_assigned_test
                ]

                # IN PROGRESS
                if related_assigned_test.status == AssignedTestStatus.ACTIVE:
                    custom_test_status = UIT_TEST_STATUS.IN_PROGRESS
                # TAKEN
                elif (
                    related_assigned_test.status == AssignedTestStatus.SUBMITTED
                    or related_assigned_test.status == AssignedTestStatus.QUIT
                ):
                    custom_test_status = UIT_TEST_STATUS.TAKEN
                # UNASSIGNED
                elif related_assigned_test.status == AssignedTestStatus.UNASSIGNED:
                    custom_test_status = UIT_TEST_STATUS.UNASSIGNED

            # no unsupervised test access code + no related assigned test = code deactivated
            else:
                code_deactivated = True

        final_array.append(
            {
                "first_name": candidate.first_name,
                "last_name": candidate.last_name,
                "email": candidate.email,
                "uit_invite_id": candidate.uit_invite_id,
                "code_deactivated": code_deactivated,
                "test_status": custom_test_status,
            }
        )

    return final_array


# Serializers define the API representation
class GetUITProcessesSerializer(serializers.ModelSerializer):
    test_permission_data = serializers.SerializerMethodField()
    total_candidates = serializers.SerializerMethodField()
    total_tests_taken = serializers.SerializerMethodField()

    def get_test_permission_data(self, request):
        # orderless test request
        if request.orderless_request:
            try:
                # getting test_id
                test_id = (
                    UnsupervisedTestAccessCode.history.filter(uit_invite_id=request.id)
                    .first()
                    .test_id
                )
                # getting test data
                test_data = GetEnFrTestNameSerializer(
                    TestDefinition.objects.get(id=test_id)
                ).data
                # getting financial data
                financial_data = OrderlessFinancialData.objects.filter(
                    uit_invite_id=request.id
                ).first()
                # getting department_ministry_code
                department_ministry_data = CatRefDepartmentsVW.objects.get(
                    dept_id=financial_data.department_ministry_id
                )
                # building test_permission_data object (financial data object in that case)
                test_permission_data = {
                    "test": test_data,
                    # reference number in this case
                    "staffing_process_number": financial_data.reference_number,
                    "billing_contact": financial_data.billing_contact_name,
                    "billing_contact_info": financial_data.billing_contact_info,
                    "department_ministry_data": CatRefDepartmentsSerializer(
                        department_ministry_data, many=False
                    ).data,
                    "is_org": financial_data.fis_organisation_code,
                    "is_ref": financial_data.fis_reference_code,
                }
            except:
                test_permission_data = None
        # request with associated test permission
        else:
            test_permission_data = (
                TestPermissions.history.filter(
                    id=request.ta_test_permissions_id,
                    username_id=request.ta_username_id,
                )
                .order_by("history_date")
                .last()
            )
            # test_permission_data exists
            if test_permission_data:
                test_permission_data = GetTestPermissionsSerializer(
                    test_permission_data, many=False
                ).data
            # test_permission_data does not exist (data has been added while building the orderless functionality ==> ta_test_permissions_id of NULL + orderless_request = 0 which should not be possible)
            else:
                test_permission_data = None
        return test_permission_data

    def get_total_candidates(self, request):
        candidates_list = UITInviteRelatedCandidates.objects.filter(
            uit_invite_id=request.id
        )
        total_candidates = len(candidates_list)
        return total_candidates

    # getting total tests taken by the candidate based on invite id
    def get_total_tests_taken(self, request):
        tests_taken = AssignedTest.objects.filter(
            uit_invite_id=request.id,
            status__in=(AssignedTestStatus.SUBMITTED, AssignedTestStatus.QUIT),
        )
        total_tests_taken = len(tests_taken)
        return total_tests_taken

    class Meta:
        model = UITInvites
        fields = "__all__"


class GetSelectedUITProcessSeializer(serializers.ModelSerializer):
    candidates_data = serializers.SerializerMethodField()

    def get_candidates_data(self, request):
        candidates_list = UITInviteRelatedCandidates.objects.filter(
            uit_invite_id=request.id
        )

        return get_candidates_uit_data(request.id, candidates_list)

    class Meta:
        model = UITInvites
        fields = "__all__"


class UITReasonsForDeletionSerializer(serializers.ModelSerializer):
    class Meta:
        model = UITReasonsForDeletion
        fields = "__all__"


class UITReasonsForModificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = UITReasonsForModification
        fields = "__all__"


class ReasonsForTestingSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReasonsForTesting
        fields = "__all__"
