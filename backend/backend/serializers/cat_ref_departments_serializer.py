from rest_framework import serializers
from backend.ref_table_views.cat_ref_departments_vw import CatRefDepartmentsVW


class CatRefDepartmentsSerializer(serializers.ModelSerializer):
    class Meta:
        model = CatRefDepartmentsVW
        fields = ["dept_id", "active_flg", "eabrv", "fabrv", "legacy_cd", "edesc", "fdesc", "efdt", "xdt", "esearch_desc", "fsearch_desc", "rcver_gnrl_nbr", "psea_id", "pssa_id", "pssra_id", "ola_id", "org_typ_id", "org_empl_id"]
