from rest_framework import serializers
from backend.custom_models.candidate_email_response_answers import (
    CandidateEmailResponseAnswers,
)


class CandidateEmailResponseAnswersSerializer(serializers.ModelSerializer):
    class Meta:
        model = CandidateEmailResponseAnswers
        fields = "__all__"
