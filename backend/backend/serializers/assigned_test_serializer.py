from operator import or_
from functools import reduce
from django.utils import timezone
from datetime import timedelta
import datetime
import pytz
from django.db.models import Q
from rest_framework import serializers
from backend.custom_models.additional_time import AdditionalTime
from backend.views.utils import get_last_accessed_test_section
from backend.custom_models.break_bank import BreakBank
from backend.custom_models.break_bank_actions import BreakBankActions
from backend.custom_models.accommodation_request import AccommodationRequest
from backend.custom_models.candidate_task_response_answers import (
    CandidateTaskResponseAnswers,
)
from backend.serializers.candidate_task_response_answers_serializer import (
    CandidateTaskResponseAnswersSerializer,
)
from backend.serializers.candidate_email_response_answers_serializer import (
    CandidateEmailResponseAnswersSerializer,
)
from backend.custom_models.candidate_email_response_answers import (
    CandidateEmailResponseAnswers,
)
from backend.custom_models.assigned_test import AssignedTest
from backend.custom_models.candidate_answers import CandidateAnswers
from backend.custom_models.candidate_multiple_choice_answers import (
    CandidateMultipleChoiceAnswers,
)
from backend.custom_models.assigned_test_section import AssignedTestSection
from backend.custom_models.ta_actions import TaActions
from backend.custom_models.etta_actions import EttaActions
from backend.custom_models.lock_test_actions import LockTestActions
from backend.custom_models.orderless_financial_data import OrderlessFinancialData
from backend.views.test_administrator_actions import TaActionsConstants
from backend.views.system_administrator_actions import EttaActionsConstants
from backend.static.assigned_test_status import AssignedTestStatus
from backend.custom_models.assigned_test_section_access_times import (
    AssignedTestSectionAccessTimes,
)
from backend.static.assigned_test_section_access_time_type import (
    AssignedTestSectionAccessTimeType,
)
from cms.cms_models.test_definition import TestDefinition
from cms.cms_models.test_section_component import TestSectionComponent
from cms.cms_models.test_section import TestSection
from cms.cms_models.test_permissions_model import TestPermissions
from cms.static.test_section_component_type import TestSectionComponentType
from cms.serializers.test_section_serializer import AssignedTestSectionSerializer
from cms.views.test_break_bank import BreakBankActionsConstants
from user_management.user_management_models.user_models import User
from cms.views.retrieve_test_section_data import (
    get_total_assigned_test_time_in_seconds,
    get_updated_time_during_lock,
)


class TestSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestDefinition
        fields = "__all__"


class AssignedTestSerializer(serializers.ModelSerializer):
    test = TestSerializer(many=False)
    assigned_test_sections = serializers.SerializerMethodField()

    candidate_id = serializers.SerializerMethodField()
    candidate_email = serializers.SerializerMethodField()
    candidate_first_name = serializers.SerializerMethodField()
    candidate_last_name = serializers.SerializerMethodField()
    candidate_dob = serializers.SerializerMethodField()
    pause_test_time = serializers.SerializerMethodField()
    pause_start_date = serializers.SerializerMethodField()
    previous_status = serializers.SerializerMethodField()
    ta_first_name = serializers.SerializerMethodField()
    ta_last_name = serializers.SerializerMethodField()
    test_time_remaining = serializers.SerializerMethodField()
    test_time_updated = serializers.SerializerMethodField()

    def get_assigned_test_sections(self, assigned_test):
        assigned_test_sections = AssignedTestSection.objects.filter(
            assigned_test_id=assigned_test.id
        )
        return AssignedTestSectionSerializer(assigned_test_sections, many=True).data

    def get_candidate_id(self, assigned_test):
        candidate_id = User.objects.get(username=assigned_test.username).id
        return candidate_id

    def get_candidate_email(self, assigned_test):
        candidate_email = User.objects.get(username=assigned_test.username).email
        return candidate_email

    def get_candidate_first_name(self, assigned_test):
        candidate_first_name = User.objects.get(
            username=assigned_test.username
        ).first_name
        return candidate_first_name

    def get_candidate_last_name(self, assigned_test):
        candidate_last_name = User.objects.get(
            username=assigned_test.username
        ).last_name
        return candidate_last_name

    def get_candidate_dob(self, assigned_test):
        candidate_dob = User.objects.get(username=assigned_test.username).birth_date
        return candidate_dob

    def get_ta_first_name(self, assigned_test):
        ta_first_name = User.objects.get(username=assigned_test.ta).first_name
        return ta_first_name

    def get_ta_last_name(self, assigned_test):
        ta_last_name = User.objects.get(username=assigned_test.ta).last_name
        return ta_last_name

    def get_pause_test_time(self, assigned_test):
        # defined accommodation request
        if assigned_test.accommodation_request_id is not None:
            # getting break bank ID
            break_bank_id = AccommodationRequest.objects.get(
                id=assigned_test.accommodation_request_id
            ).break_bank_id
            # existing break bank accommodation request
            if break_bank_id is not None:
                # getting break bank actions related to current test section where action_type is "UNPAUSE" (if they exist)
                unpause_break_bank_actions = BreakBankActions.objects.filter(
                    break_bank_id=break_bank_id,
                    action_type=BreakBankActionsConstants.UNPAUSE,
                ).order_by("modify_date")

                # unpause break bank actions contains data
                if unpause_break_bank_actions:
                    pause_test_time = (
                        # divided by 60 to get the time in minutes (provided in seconds)
                        unpause_break_bank_actions.last().new_remaining_time
                        / 60
                    )

                # no unpause break bank actions
                else:
                    # getting total time of the break bank
                    # divided by 60 to get the time in minutes (provided in seconds)
                    pause_test_time = (
                        BreakBank.objects.get(id=break_bank_id).break_time / 60
                    )
            # non-existing break bank accommodation request
            else:
                pause_test_time = None
        # undefined accommodation request
        else:
            pause_test_time = None
        return pause_test_time

    def get_pause_start_date(self, assigned_test):
        # defined accommodation request
        if assigned_test.accommodation_request_id is not None:
            # getting break bank ID
            break_bank_id = AccommodationRequest.objects.get(
                id=assigned_test.accommodation_request_id
            ).break_bank_id
            # existing break bank accommodation request
            if break_bank_id is not None:
                # getting last accessed assigned test section ID
                last_accessed_test_section_id = get_last_accessed_test_section(
                    assigned_test.id
                )
                # getting break bank actions related to current test section where action_type is "PAUSE" (if they exist)
                pause_break_bank_actions = BreakBankActions.objects.filter(
                    break_bank_id=break_bank_id,
                    action_type=BreakBankActionsConstants.PAUSE,
                    test_section_id=last_accessed_test_section_id,
                ).order_by("modify_date")
                # unpause break bank actions contains data
                if pause_break_bank_actions:
                    pause_start_date = pause_break_bank_actions.last().modify_date
                else:
                    pause_start_date = None
            # non-existing break bank accommodation request
            else:
                pause_start_date = None
        # undefined accommodation request
        else:
            pause_start_date = None

        return pause_start_date

    def get_previous_status(self, assigned_test):
        previous_status = AssignedTest.objects.get(id=assigned_test.id).previous_status
        return previous_status

    # TODO: fix the logic for multi sections tests
    def get_test_time_remaining(self, assigned_test):
        # initialize the variables
        test_section_start_time = 0
        test_section_updated_start_time = 0
        test_section_default_time = 0
        lock_start_date = 0
        consumed_pause_time = 0
        current_time = datetime.datetime.now(pytz.utc)
        test_time_elapsed = 0
        test_time_remaining = 0

        if assigned_test.test_section_id:
            # get assigned test section
            assigned_test_section = AssignedTestSection.objects.filter(
                assigned_test_id=assigned_test.id,
                test_section_id=assigned_test.test_section_id,
            )

            # assigned test section is defined
            if assigned_test_section:
                # get the start time of the test
                assigned_test_section_start_time = AssignedTestSectionAccessTimes.objects.filter(
                    # first assigned test section
                    assigned_test_section_id=assigned_test_section.first().id,
                    time_type=AssignedTestSectionAccessTimeType.START,
                ).first()

                if assigned_test_section_start_time is not None:
                    test_section_start_time = assigned_test_section_start_time.time

                # test section default time
                test_section_default_time = (
                    assigned_test_section.first().test_section_time
                )

            # to get lock start date of test
            try:
                ta_action_id_2 = (
                    TaActions.objects.filter(
                        assigned_test_id=assigned_test.id,
                        action_type_id=TaActionsConstants.LOCK,
                    )
                    .last()
                    .id
                )

                lock_start_date = LockTestActions.objects.get(
                    ta_action_id=ta_action_id_2
                ).lock_start_date

            except:
                lock_start_date = 0

            # existing accommodation request
            if assigned_test.accommodation_request_id is not None:
                # getting accommodation request
                accommodation_request_data = AccommodationRequest.objects.get(
                    id=assigned_test.accommodation_request_id
                )
                # getting break bank ID
                break_bank_id = accommodation_request_data.break_bank_id
                # break bank ID exists
                if break_bank_id:
                    # getting last accessed assigned test section ID
                    last_accessed_test_section_id = get_last_accessed_test_section(
                        assigned_test.id
                    )
                    # getting break bank actions related to current test section (if they exist)
                    break_bank_actions = BreakBankActions.objects.filter(
                        break_bank_id=break_bank_id,
                        test_section_id=last_accessed_test_section_id,
                    ).order_by("modify_date")
                    # existing break bank actions
                    if break_bank_actions:
                        # getting total break bank time
                        total_break_bank_time = BreakBank.objects.get(
                            id=break_bank_id
                        ).break_time
                        # if last break bank action is a PAUSE
                        if (
                            break_bank_actions.last().action_type
                            == BreakBankActionsConstants.PAUSE
                        ):
                            # checking for a previous UNPAUSE action
                            try:
                                previous_unpause_action = break_bank_actions[
                                    len(break_bank_actions) - 2
                                ]
                            except:
                                previous_unpause_action = None
                                pass
                            # calculating time between now and last PAUSE
                            time_between_now_and_last_pause_action = (
                                timezone.now() - break_bank_actions.last().modify_date
                            ).total_seconds()

                            # there is a previous unpause action
                            if previous_unpause_action is not None:
                                # calculating remaining time
                                remaining_time = datetime.timedelta(
                                    seconds=previous_unpause_action.new_remaining_time
                                ) - datetime.timedelta(
                                    seconds=time_between_now_and_last_pause_action
                                )

                                # updating consumed_pause_time
                                consumed_pause_time = (
                                    total_break_bank_time
                                    - remaining_time.total_seconds()
                                )

                            # there is no previous unpause action
                            else:
                                # calculating remaining time
                                remaining_time = datetime.timedelta(
                                    seconds=total_break_bank_time
                                ) - datetime.timedelta(
                                    seconds=time_between_now_and_last_pause_action
                                )

                                # updating consumed_pause_time
                                consumed_pause_time = (
                                    total_break_bank_time
                                    - remaining_time.total_seconds()
                                )
                        # if last break bank action is an UNPAUSE
                        else:
                            # caucliating consumed pause time (total break time - last UNPAUSE new remaining time)
                            consumed_pause_time = (
                                total_break_bank_time
                                - break_bank_actions.last().new_remaining_time
                            )

            # test is active or paused or locked and previous status is not ready or pre-test
            if (
                (
                    assigned_test.status == AssignedTestStatus.ACTIVE
                    or assigned_test.status == AssignedTestStatus.LOCKED
                    or assigned_test.status == AssignedTestStatus.PAUSED
                )
                and assigned_test.previous_status != AssignedTestStatus.READY
                and assigned_test.previous_status != AssignedTestStatus.PRE_TEST
            ):
                # get updated time of the test to check previous pause/lock actions
                if consumed_pause_time != 0 or lock_start_date != 0:
                    test_section_updated_start_time = get_updated_time_during_lock(
                        assigned_test.id, assigned_test.test_section_id
                    )
                    # if the time is updated since the beginning of the test then get the updated time
                    if test_section_updated_start_time is not None:
                        test_section_start_time = test_section_updated_start_time

                if (
                    current_time >= test_section_start_time
                    and test_section_default_time is not None
                ):
                    test_section_default_time_delta = timedelta(
                        minutes=test_section_default_time
                    ).total_seconds()

                    # if test status is active or paused
                    if (
                        assigned_test.status == AssignedTestStatus.ACTIVE
                        or assigned_test.status == AssignedTestStatus.PAUSED
                    ):
                        # getting elapsed time of the test
                        test_time_elapsed = current_time - test_section_start_time
                        # calculating time remaining
                        test_time_remaining = datetime.timedelta(
                            seconds=test_section_default_time_delta
                        ) - (
                            test_time_elapsed
                            - datetime.timedelta(seconds=consumed_pause_time)
                        )

                    # if test status is Locked
                    elif assigned_test.status == AssignedTestStatus.LOCKED:
                        # getting elapsed time of the test
                        test_time_elapsed = lock_start_date - test_section_start_time
                        # calculating time remaining
                        test_time_remaining = datetime.timedelta(
                            seconds=test_section_default_time_delta
                        ) - (
                            test_time_elapsed
                            - datetime.timedelta(seconds=consumed_pause_time)
                        )
            # return total time of the test
            else:
                test_time_remaining = get_total_assigned_test_time_in_seconds(
                    assigned_test.id
                )
        else:
            test_time_remaining = get_total_assigned_test_time_in_seconds(
                assigned_test.id
            )

        return test_time_remaining

    def get_test_time_updated(self, assigned_test):
        # there is no accommodation request associated to this assigned test
        if assigned_test.accommodation_request_id is None:
            return False
        # accommodation request associated to this assigned test
        else:
            # getting related test section IDs
            test_section_ids = AssignedTestSection.objects.filter(
                assigned_test_id=assigned_test.id
            ).values_list("test_section_id", flat=True)
            # getting test sections data (where default time is defined)
            test_sections = TestSection.objects.filter(
                id__in=test_section_ids, default_time__isnull=False
            )
            # looping in test_sections
            for test_section in test_sections:
                # checking if there is some additional time set based on the current test section and the accommodation request data
                if AdditionalTime.objects.filter(
                    accommodation_request_id=assigned_test.accommodation_request_id,
                    test_section_id=test_section.id,
                ):
                    return True
            return False

    class Meta:
        model = AssignedTest
        fields = "__all__"


class AssignedTestSimplifiedSerializer(serializers.ModelSerializer):
    test = TestSerializer(many=False)
    candidate_first_name = serializers.SerializerMethodField()
    candidate_last_name = serializers.SerializerMethodField()
    candidate_dob = serializers.SerializerMethodField()

    def get_candidate_first_name(self, assigned_test):
        candidate_first_name = User.objects.get(
            username=assigned_test.username
        ).first_name
        return candidate_first_name

    def get_candidate_last_name(self, assigned_test):
        candidate_last_name = User.objects.get(
            username=assigned_test.username
        ).last_name
        return candidate_last_name

    def get_candidate_dob(self, assigned_test):
        candidate_dob = User.objects.get(username=assigned_test.username).birth_date
        return candidate_dob

    class Meta:
        model = AssignedTest
        fields = "__all__"


class UitAssignedTestSerializer(serializers.ModelSerializer):
    class Meta:
        model = AssignedTest
        fields = "__all__"


class UitFormattedAnswersSerializer(serializers.ModelSerializer):
    answer_id = serializers.SerializerMethodField()

    def get_answer_id(self, candidate_answers):
        answer_id = CandidateMultipleChoiceAnswers.objects.get(
            candidate_answers_id=candidate_answers.id
        ).answer_id
        return answer_id

    class Meta:
        model = CandidateAnswers
        fields = ["question_id", "answer_id", "mark_for_review"]


class UitFormattedItemAnswersSerializer(serializers.ModelSerializer):
    question_id = serializers.SerializerMethodField()
    answer_id = serializers.SerializerMethodField()

    def get_question_id(self, candidate_answers):
        # getting the item_id since this serializer is for the Item Answers
        # we need to keep the returned variable names consistent for questions/items from the test builder or from the item bank
        return candidate_answers.item_id

    def get_answer_id(self, candidate_answers):
        # getting the item_answer_id since this serializer is for the Item Answers
        # we need to keep the returned variable names consistent for answers from the test builder or from the item bank
        answer_id = CandidateMultipleChoiceAnswers.objects.get(
            candidate_answers_id=candidate_answers.id
        ).item_answer_id
        return answer_id

    class Meta:
        model = CandidateAnswers
        fields = ["question_id", "answer_id", "mark_for_review"]


class UitFormattedInboxAnswersSerializer(serializers.ModelSerializer):
    email_answers = serializers.SerializerMethodField()
    task_answers = serializers.SerializerMethodField()

    def get_email_answers(self, candidate_answers):
        obj = CandidateEmailResponseAnswers.objects.filter(
            candidate_answers_id=candidate_answers.id
        )
        return CandidateEmailResponseAnswersSerializer(obj, many=True).data

    def get_task_answers(self, candidate_answers):
        obj = CandidateTaskResponseAnswers.objects.filter(
            candidate_answers_id=candidate_answers.id
        )
        return CandidateTaskResponseAnswersSerializer(obj, many=True).data

    class Meta:
        model = CandidateAnswers
        fields = ["question_id", "email_answers", "task_answers"]


class UitAnswersSerializer(serializers.ModelSerializer):
    answers = serializers.SerializerMethodField()

    def get_answers(self, assigned_test):
        test_section_component_id = self.context.get("test_section_component_id", None)
        test_section_component = TestSectionComponent.objects.get(
            id=test_section_component_id
        )
        # getting candidate_answers objects
        candidate_answers = CandidateAnswers.objects.filter(
            assigned_test_id=assigned_test.id
        )
        # get answers depending on question type
        if (
            test_section_component.component_type
            == TestSectionComponentType.QUESTION_LIST
        ):
            # getting related candidate answer IDs and ordering by modify_date (descending order)
            candidate_answers_ids = (
                CandidateMultipleChoiceAnswers.objects.filter(
                    reduce(
                        or_,
                        [
                            Q(candidate_answers_id=candidate_answer.id)
                            for candidate_answer in candidate_answers
                        ],
                    )
                )
                .values_list("candidate_answers_id", flat=True)
                .order_by("-modify_date")
            )
            # getting candidate_answers based on candidate_answers_ids array values
            candidate_answers = CandidateAnswers.objects.filter(
                id__in=candidate_answers_ids
            )
            # creating dictionary of candidate_answers with their id as key
            candidate_answers = dict(
                [
                    (candidate_answer.id, candidate_answer)
                    for candidate_answer in candidate_answers
                ]
            )
            # sorting candidate_answers by keeping the initial order of multiple_choice_answers (candidate_answers_id)
            sorted_candidate_answers = [
                candidate_answers[id] for id in candidate_answers_ids
            ]
            # getting answers data using UitFormattedAnswersSerializer serializer
            answers_serializer = UitFormattedAnswersSerializer(
                sorted_candidate_answers, many=True
            )

        elif (
            test_section_component.component_type == TestSectionComponentType.ITEM_BANK
        ):
            # getting related candidate answer IDs and ordering by modify_date (descending order)
            candidate_answers_ids = (
                CandidateMultipleChoiceAnswers.objects.filter(
                    reduce(
                        or_,
                        [
                            Q(candidate_answers_id=candidate_answer.id)
                            for candidate_answer in candidate_answers
                        ],
                    )
                )
                .values_list("candidate_answers_id", flat=True)
                .order_by("-modify_date")
            )
            # getting candidate_answers based on candidate_answers_ids array values
            candidate_answers = CandidateAnswers.objects.filter(
                id__in=candidate_answers_ids
            )
            # creating dictionary of candidate_answers with their id as key
            candidate_answers = dict(
                [
                    (candidate_answer.id, candidate_answer)
                    for candidate_answer in candidate_answers
                ]
            )
            # sorting candidate_answers by keeping the initial order of multiple_choice_answers (candidate_answers_id)
            sorted_candidate_answers = [
                candidate_answers[id] for id in candidate_answers_ids
            ]
            # getting answers data using UitFormattedAnswersSerializer serializer
            answers_serializer = UitFormattedItemAnswersSerializer(
                sorted_candidate_answers, many=True
            )

        elif test_section_component.component_type == TestSectionComponentType.INBOX:
            answers_serializer = UitFormattedInboxAnswersSerializer(
                candidate_answers, many=True
            )
        # returning serialized data
        return answers_serializer.data

    class Meta:
        model = AssignedTest
        fields = "__all__"


class SelectedUserAllTestsSerializer(serializers.ModelSerializer):
    ta_first_name = serializers.SerializerMethodField()
    ta_last_name = serializers.SerializerMethodField()
    invalidate_test_reason = serializers.SerializerMethodField()
    test = TestSerializer(many=False)
    assessment_process_reference_nbr = serializers.SerializerMethodField()

    def get_ta_first_name(self, assigned_test):
        ta_first_name = User.objects.get(username=assigned_test.ta).first_name
        return ta_first_name

    def get_ta_last_name(self, assigned_test):
        ta_last_name = User.objects.get(username=assigned_test.ta).last_name
        return ta_last_name

    def get_invalidate_test_reason(self, assigned_test):
        invalidate_test_reason = ""

        if assigned_test.is_invalid:
            etta_action_data = EttaActions.objects.filter(
                assigned_test_id=assigned_test.id,
                action_type=EttaActionsConstants.INVALIDATE_CANDIDATE,
            )
            if etta_action_data:
                invalidate_test_reason = etta_action_data.last().action_reason

        return invalidate_test_reason

    def get_assessment_process_reference_nbr(self, assigned_test):
        # orderless request (reference number)
        if assigned_test.orderless_financial_data_id is not None:
            return OrderlessFinancialData.objects.get(
                id=assigned_test.orderless_financial_data_id
            ).reference_number
        # ordered test (assessment process number)
        else:
            # trying to get current test permission data of respective TA
            try:
                ta_test_permission_data = TestPermissions.objects.get(
                    username_id=assigned_test.ta_id,
                    test_id=assigned_test.test_id,
                    test_order_number=assigned_test.test_order_number,
                )
                return ta_test_permission_data.staffing_process_number
            except TestPermissions.DoesNotExist:
                # getting historical test permission data of respective TA
                historical_ta_test_permission_data = TestPermissions.history.filter(
                    username_id=assigned_test.ta_id,
                    test_id=assigned_test.test_id,
                    test_order_number=assigned_test.test_order_number,
                    history_type="+",
                )
                if historical_ta_test_permission_data:
                    return (
                        historical_ta_test_permission_data.first().staffing_process_number
                    )
                else:
                    return None

    class Meta:
        model = AssignedTest
        fields = "__all__"
