from rest_framework import serializers
from backend.ref_table_views.cat_ref_classification_vw import CatRefClassificationVW


class CatRefClassificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = CatRefClassificationVW
        fields = ["classif_id", "class_grp_cd", "class_lvl_cd"]
