from decimal import Decimal
from ast import literal_eval
from django.utils.html import strip_tags
from django.utils import timezone
import pytz
from rest_framework import serializers
from django.conf import settings
from cms.cms_models.item_bank import ItemBank
from cms.serializers.item_bank_serializer import ItemOptionTestTakerReportSerializer
from cms.serializers.item_bank_serializer import ItemContentViewSerializer
from cms.serializers.get_test_information_serializer import GetTestPermissionsSerializer
from cms.static.test_section_component_type import TestSectionComponentType
from cms.cms_models.test_permissions_model import TestPermissions
from cms.cms_models.test_definition import TestDefinition
from cms.cms_models.test_section_component import TestSectionComponent
from cms.cms_models.new_question import NewQuestion
from cms.cms_models.question_section import QuestionSection
from cms.cms_models.question_section_type_markdown import QuestionSectionTypeMarkdown
from cms.cms_models.answer import Answer
from cms.cms_models.answer_details import AnswerDetails
from backend.custom_models.assigned_test import AssignedTest
from backend.custom_models.assigned_test_section import AssignedTestSection
from backend.custom_models.ta_actions import TaActions
from backend.custom_models.pause_test_actions import PauseTestActions
from backend.custom_models.lock_test_actions import LockTestActions
from backend.custom_models.candidate_answers import CandidateAnswers
from backend.custom_models.candidate_multiple_choice_answers import (
    CandidateMultipleChoiceAnswers,
)
from backend.custom_models.assigned_questions_list import AssignedQuestionsList
from backend.custom_models.orderless_financial_data import OrderlessFinancialData
from backend.views.test_administrator_actions import TaActionsConstants
from backend.views.utils import (
    remove_specific_line_chars,
    replace_single_double_quotes_with_two_double_quotes,
)
from backend.views.extended_profile_options_view import (
    get_employer_list,
    get_organisation_list,
    get_group_and_level_list,
    get_residence_list,
    get_education_list,
    get_gender_list,
)
from backend.static.assigned_test_status import AssignedTestStatus
from backend.custom_models.assigned_answer_choices import AssignedAnswerChoices
from backend.ref_table_views.cat_aboriginal_vw import CatRefAboriginalVW
from backend.ref_table_views.cat_disability_vw import CatRefDisabilityVW
from backend.ref_table_views.cat_visible_minority_vw import CatRefVisibleMinorityVW
from backend.serializers.orderless_serializer import OrderlessFinancialDataSerializer
from backend.static.languages import Language_id
from user_management.user_management_models.user_models import User
from user_management.user_management_models.user_extended_profile import (
    UserExtendedProfile,
)
from user_management.static.extended_profile import (
    ExtendedProfileEEInfo,
    VisibleMinorityId,
    DisabilityId,
)
from user_management.user_management_models.user_extended_profile_aboriginal import (
    UserExtendedProfileAboriginal,
)
from user_management.user_management_models.user_extended_profile_visible_minority import (
    UserExtendedProfileVisibleMinority,
)
from user_management.user_management_models.user_extended_profile_disability import (
    UserExtendedProfileDisability,
)
from db_views.db_view_models.all_items_data_vw import AllItemsDataVW
from db_views.db_view_models.item_content_vw import ItemContentVW
from db_views.db_view_models.item_option_vw import ItemOptionVW


class TestSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestDefinition
        fields = "__all__"


class TestDefinitionSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestDefinition
        fields = "__all__"


class QuestionSectionSerializer(serializers.ModelSerializer):
    question_sections_type_markdown_fr = serializers.SerializerMethodField()
    question_sections_type_markdown_en = serializers.SerializerMethodField()
    question_sections_type_markdown_html_fr = serializers.SerializerMethodField()
    question_sections_type_markdown_html_en = serializers.SerializerMethodField()

    def get_question_sections_type_markdown_fr(self, request):
        try:
            question_sections_type_markdown_fr = (
                QuestionSectionTypeMarkdown.objects.get(
                    question_section_id=request.id, language_id="fr"
                ).content
            )
            # replacing all html tags and all specific line characters with "" and returning result
            return '"{0}"'.format(
                strip_tags(
                    remove_specific_line_chars(question_sections_type_markdown_fr)
                )
            )
        except:
            return None

    def get_question_sections_type_markdown_en(self, request):
        try:
            question_sections_type_markdown_en = (
                QuestionSectionTypeMarkdown.objects.get(
                    question_section_id=request.id, language_id="en"
                ).content
            )
            # replacing all html tags and all specific line characters with "" and returning result
            return '"{0}"'.format(
                strip_tags(
                    remove_specific_line_chars(question_sections_type_markdown_en)
                )
            )
        except:
            return None

    def get_question_sections_type_markdown_html_fr(self, request):
        try:
            question_sections_type_markdown_html_fr = (
                QuestionSectionTypeMarkdown.objects.get(
                    question_section_id=request.id, language_id="fr"
                ).content
            )
            # need to replace single double quotes with two double quotes, so commas are well formatted
            return '"{0}"'.format(
                replace_single_double_quotes_with_two_double_quotes(
                    question_sections_type_markdown_html_fr
                )
            )
        except:
            return None

    def get_question_sections_type_markdown_html_en(self, request):
        try:
            question_sections_type_markdown_html_en = (
                QuestionSectionTypeMarkdown.objects.get(
                    question_section_id=request.id, language_id="en"
                ).content
            )
            # need to replace single double quotes with two double quotes, so commas are well formatted
            return '"{0}"'.format(
                replace_single_double_quotes_with_two_double_quotes(
                    question_sections_type_markdown_html_en
                )
            )
        except:
            return None

    class Meta:
        model = QuestionSection
        fields = "__all__"


class AnswersSerializer(serializers.ModelSerializer):
    answer_details_fr = serializers.SerializerMethodField()
    answer_details_en = serializers.SerializerMethodField()
    answer_details_html_fr = serializers.SerializerMethodField()
    answer_details_html_en = serializers.SerializerMethodField()
    # presentation order
    assigned_answer_choice_order = serializers.SerializerMethodField()

    def get_answer_details_fr(self, request):
        try:
            answer_details_fr = AnswerDetails.objects.get(
                answer_id=request.id, language_id="fr"
            ).content
            # replacing all html tags and all specific line characters with "" and returning result
            return '"{0}"'.format(
                strip_tags(remove_specific_line_chars(answer_details_fr))
            )
        except:
            return None

    def get_answer_details_en(self, request):
        try:
            answer_details_en = AnswerDetails.objects.get(
                answer_id=request.id, language_id="en"
            ).content
            # replacing all html tags and all specific line characters with "" and returning result
            return '"{0}"'.format(
                strip_tags(remove_specific_line_chars(answer_details_en))
            )
        except:
            return None

    def get_answer_details_html_fr(self, request):
        try:
            answer_details_html_fr = AnswerDetails.objects.get(
                answer_id=request.id, language_id="fr"
            ).content
            return '"{0}"'.format(
                replace_single_double_quotes_with_two_double_quotes(
                    answer_details_html_fr
                )
            )
        except:
            return None

    def get_answer_details_html_en(self, request):
        try:
            answer_details_html_en = AnswerDetails.objects.get(
                answer_id=request.id, language_id="en"
            ).content
            return '"{0}"'.format(
                replace_single_double_quotes_with_two_double_quotes(
                    answer_details_html_en
                )
            )
        except:
            return None

    def get_assigned_answer_choice_order(self, request):
        # if context["assigned_test_id"] is defined
        if self.context.get("assigned_test_id", None):
            try:
                # getting assigned answer choices
                assigned_answer_choices = literal_eval(
                    AssignedAnswerChoices.objects.get(
                        assigned_test_id=self.context["assigned_test_id"],
                        question_id=request.question_id,
                    ).answer_choices
                )
                # getting order
                assigned_answer_choice_order = (
                    assigned_answer_choices.index(request.id) + 1
                )
                return assigned_answer_choice_order
            except:
                return None
        return None

    class Meta:
        model = Answer
        fields = "__all__"


class TestContentReportDataSerializer(serializers.ModelSerializer):
    question_sections = serializers.SerializerMethodField()
    answers = serializers.SerializerMethodField()
    sample_test_indicator = serializers.SerializerMethodField()
    test_section_component_title_fr = serializers.SerializerMethodField()
    test_section_component_title_en = serializers.SerializerMethodField()

    def get_question_sections(self, request):
        question_sections = QuestionSection.objects.filter(
            question_id=request.id
        ).order_by("order")
        return QuestionSectionSerializer(question_sections, many=True).data

    def get_answers(self, request):
        answers = Answer.objects.filter(question_id=request.id)
        return AnswersSerializer(answers, many=True).data

    def get_sample_test_indicator(self, request):
        sample_test_indicator = TestDefinition.objects.get(
            parent_code=self.context.get("parent_code"),
            test_code=self.context.get("test_code"),
            version=self.context.get("version"),
        ).is_public
        return sample_test_indicator

    def get_test_section_component_title_fr(self, request):
        test_section_component_title_fr = TestSectionComponent.objects.get(
            id=request.test_section_component_id
        ).fr_title
        return test_section_component_title_fr

    def get_test_section_component_title_en(self, request):
        test_section_component_title_en = TestSectionComponent.objects.get(
            id=request.test_section_component_id
        ).en_title
        return test_section_component_title_en

    class Meta:
        model = NewQuestion
        fields = "__all__"


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = "__all__"


# Function used to modify the timespent of a question_id in our array
def updateTimeSpent(
    questions_time_spent,
    prev_question,
    current_question_history_date,
    ta_actions,
    from_item_bank,
):
    time_spent = (
        current_question_history_date - prev_question.history_date
    ).total_seconds()

    # question coming from Item Bank
    if from_item_bank:
        prev_question = prev_question.item_id
    # question coming from the Test Builder
    else:
        prev_question = prev_question.question_id

    for index, question in enumerate(questions_time_spent):
        if question["question_id"] == prev_question:
            questions_time_spent[index]["time_spent"] += round(Decimal(time_spent), 3)
            # initializing lock_pause_time
            lock_pause_time = 0
            # checking if there is a LOCK or PAUSE action between previous question history date and current question history date
            for ta_action in ta_actions:
                # PAUSE action
                if ta_action.action_type_id == TaActionsConstants.PAUSE:
                    # action has been called between previous question history date and current question history date
                    if (
                        ta_action.modify_date > prev_question.history_date
                        and ta_action.modify_date < current_question_history_date
                    ):
                        # getting pause_test_action_data
                        pause_test_action_data = PauseTestActions.objects.get(
                            ta_action_id=ta_action.id
                        )
                        # calculating PAUSE time (keeping only 3 digits after the decimal point)
                        try:
                            calculated_time = round(
                                (
                                    pause_test_action_data.pause_end_date
                                    - pause_test_action_data.pause_start_date
                                ).total_seconds(),
                                3,
                            )
                            if (
                                calculated_time / 60
                                > pause_test_action_data.pause_test_time
                            ):
                                lock_pause_time += (
                                    # converting time in seconds
                                    pause_test_action_data.pause_test_time
                                    * 60
                                )
                            else:
                                lock_pause_time += calculated_time
                        except:
                            lock_pause_time = 0

                # LOCK action
                if ta_action.action_type_id == TaActionsConstants.LOCK:
                    # action has been called between previous question history date and current question history date
                    if (
                        ta_action.modify_date > prev_question.history_date
                        and ta_action.modify_date < current_question_history_date
                    ):
                        # getting lock_test_action_data
                        lock_test_action_data = LockTestActions.objects.get(
                            ta_action_id=ta_action.id
                        )
                        # calculating LOCK time (keeping only 3 digits after the decimal point)
                        try:
                            lock_pause_time += round(
                                (
                                    lock_test_action_data.lock_end_date
                                    - lock_test_action_data.lock_start_date
                                ).total_seconds(),
                                3,
                            )
                        except:
                            lock_pause_time = 0

            questions_time_spent[index]["time_spent"] -= round(
                Decimal(lock_pause_time), 3
            )
    return questions_time_spent


class CandidateAnswersSerializer(serializers.ModelSerializer):
    selected_answer_id = serializers.SerializerMethodField()
    time_spent = serializers.SerializerMethodField()

    def get_selected_answer_id(self, request):
        # question coming from Item Bank
        if self.context.get("from_item_bank", None):
            # question coming from the Test Builder
            selected_answer_id = CandidateMultipleChoiceAnswers.objects.get(
                candidate_answers_id=request.id
            ).item_answer_id
        else:
            selected_answer_id = CandidateMultipleChoiceAnswers.objects.get(
                candidate_answers_id=request.id
            ).answer_id
        return selected_answer_id

    def get_time_spent(self, request):
        # Getting the CandidateAnswers ordered by history_date
        questions_history = CandidateAnswers.history.filter(
            assigned_test_id=request.assigned_test_id
        ).order_by("history_date")

        # getting TA actions (LOCK, UNLOCK, PAUSE, UNPAUSE)
        ta_actions = TaActions.objects.filter(
            assigned_test_id=request.assigned_test_id,
            action_type_id__in=(
                TaActionsConstants.LOCK,
                TaActionsConstants.UNLOCK,
                TaActionsConstants.PAUSE,
                TaActionsConstants.UNPAUSE,
            ),
        )

        # Result array of question_id with time_spent
        questions_time_spent = []

        # Generate time_spent for each answered questions of the assigned_test_id
        for index, question_history in enumerate(questions_history):
            # If first row or history_type is +, add it directly
            if index == 0 or question_history.history_type == "+":
                # question coming from Item Bank
                if self.context.get("from_item_bank", None):
                    questions_time_spent.append(
                        {"question_id": question_history.item_id, "time_spent": 0}
                    )
                # question coming from the Test Builder
                else:
                    questions_time_spent.append(
                        {"question_id": question_history.question_id, "time_spent": 0}
                    )
            # If not the first row, calculate time spent for previous question
            if index != 0:
                # Update time spent with index-1 (for previous question)
                questions_time_spent = updateTimeSpent(
                    questions_time_spent,
                    questions_history[index - 1],
                    question_history.history_date,
                    ta_actions,
                    self.context.get("from_item_bank", None),
                )
            continue

        # Return time_spent for the question_id requested
        for question in questions_time_spent:
            # question coming from Item Bank
            if self.context.get("from_item_bank", None):
                if question["question_id"] == request.item_id:
                    return question["time_spent"]
            # question coming from the Test Builder
            else:
                if question["question_id"] == request.question_id:
                    return question["time_spent"]

        # Should never happen
        return None

    class Meta:
        model = CandidateAnswers
        fields = "__all__"


class AssignedQuestionsSerializer(serializers.ModelSerializer):
    question_text_fr = serializers.SerializerMethodField()
    question_text_en = serializers.SerializerMethodField()
    presentation_order = serializers.SerializerMethodField()
    viewed_order = serializers.SerializerMethodField()
    answer_choices = serializers.SerializerMethodField()
    candidate_answer_data = serializers.SerializerMethodField()
    section_language = serializers.SerializerMethodField()

    def get_question_text_fr(self, request):
        question_sections = QuestionSection.objects.filter(
            question_id=request.id
        ).order_by("order")
        return QuestionSectionSerializer(question_sections, many=True).data

    def get_question_text_en(self, request):
        question_sections = QuestionSection.objects.filter(
            question_id=request.id
        ).order_by("order")
        return QuestionSectionSerializer(question_sections, many=True).data

    def get_presentation_order(self, request):
        # test_section_id context is defined
        if self.context.get("test_section_id", None):
            # getting assigned_question_list
            assigned_questions_list = AssignedQuestionsList.objects.get(
                assigned_test_id=self.context["assigned_test_id"],
                test_section_id=self.context["test_section_id"],
            ).questions_list
            # converting string assigned_questions_list to array/list
            # ====================================================================
            # removing '[' and ']' chars, so we have a list separated by ','
            assigned_questions_list = assigned_questions_list.replace("[", "")
            assigned_questions_list = assigned_questions_list.replace("]", "")
            # splitting values to get a list of question IDs
            assigned_questions_list = list(assigned_questions_list.split(", "))
            # converting string values to int values
            assigned_questions_list = [
                int(numeric_string) for numeric_string in assigned_questions_list
            ]
            # getting index of current question ID in assigned_questions_list
            index_of_question_id = assigned_questions_list.index(request.id) + 1
            return index_of_question_id
        else:
            return ""

    def get_viewed_order(self, request):
        # test_section_id context is defined
        if self.context.get("test_section_id", None):
            # getting test section component id based on provided test_section_id (from context)
            test_section_component_id = TestSectionComponent.objects.get(
                test_section=self.context["test_section_id"],
                component_type__in=[TestSectionComponentType.QUESTION_LIST],
            ).id
        # test_section_component_id context is defined
        elif self.context.get("test_section_component_id", None):
            test_section_component_id = self.context["test_section_component_id"]
        # missing context (return None)
        else:
            return None
        # making sure to order by ID, so they are in the order that the candidate saw them
        candidate_answers = list(
            CandidateAnswers.objects.filter(
                assigned_test_id=self.context["assigned_test_id"],
                test_section_component_id=test_section_component_id,
            )
            .order_by("id")
            .values_list("question_id", flat=True)
        )
        # question has been viewed
        try:
            viewed_order = candidate_answers.index(request.id) + 1
            return viewed_order
        # question has not been viewed
        except:
            return None

    def get_answer_choices(self, request):
        context = self.context
        context["assigned_test_id"] = self.context.get("assigned_test_id", None)
        answer_choices = Answer.objects.filter(question_id=request.id).order_by("order")
        answer_choices_data = AnswersSerializer(
            answer_choices, many=True, context=context
        ).data
        return answer_choices_data

    def get_candidate_answer_data(self, request):
        context = self.context
        context["from_item_bank"] = False
        try:
            candidate_answer = CandidateAnswers.objects.get(
                assigned_test_id=self.context["assigned_test_id"],
                question_id=request.id,
            )
            candidate_answer_data = CandidateAnswersSerializer(
                candidate_answer, many=False, context=context
            ).data
            return candidate_answer_data
        except:
            return None

    def get_section_language(self, request):
        # getting first section of question sections
        question_sections = (
            QuestionSection.objects.filter(question_id=request.id)
            .order_by("order")
            .first()
        )
        # making sure that question_sections have been found
        if question_sections:
            # checking if it's a bilingual, English or French question
            if QuestionSectionTypeMarkdown.objects.filter(
                question_section_id=question_sections.id, language_id="en"
            ) and QuestionSectionTypeMarkdown.objects.filter(
                question_section_id=question_sections.id, language_id="fr"
            ):
                return "bilingual"
            elif QuestionSectionTypeMarkdown.objects.filter(
                question_section_id=question_sections.id, language_id="fr"
            ):
                return "fr"
            else:
                return "en"
        # should not happen
        else:
            return "no_question_section_found"

    class Meta:
        model = NewQuestion
        fields = "__all__"


# we must have the exact same attribute names as the AssignedQuestionsSerializer since the attributes are called for the same report(s)
class AssignedItemsSerializer(serializers.ModelSerializer):
    # making sure we have the same terms as questions from the Test Builder
    # ======================================================
    order = serializers.SerializerMethodField()
    ppc_question_id = serializers.SerializerMethodField()
    # ======================================================
    question_text_fr = serializers.SerializerMethodField()
    question_text_en = serializers.SerializerMethodField()
    presentation_order = serializers.SerializerMethodField()
    viewed_order = serializers.SerializerMethodField()
    answer_choices = serializers.SerializerMethodField()
    candidate_answer_data = serializers.SerializerMethodField()
    section_language = serializers.SerializerMethodField()

    def get_order(self, request):
        return request.system_id.split("_")[1]

    def get_ppc_question_id(self, request):
        return request.historical_id

    def get_question_text_fr(self, request):
        item_content_fr = ItemContentVW.objects.filter(
            item_id=request.item_id, language_id=Language_id.FR
        ).order_by("content_order")
        return ItemContentViewSerializer(item_content_fr, many=True).data

    def get_question_text_en(self, request):
        item_content_en = ItemContentVW.objects.filter(
            item_id=request.item_id, language_id=Language_id.EN
        ).order_by("content_order")
        return ItemContentViewSerializer(item_content_en, many=True).data

    def get_presentation_order(self, request):
        # test_section_id context is defined
        if self.context.get("test_section_id", None):
            # getting assigned_question_list
            assigned_questions_list = AssignedQuestionsList.objects.get(
                assigned_test_id=self.context["assigned_test_id"],
                test_section_id=self.context["test_section_id"],
            ).questions_list
            # converting string assigned_questions_list to array/list
            # ====================================================================
            # removing '[' and ']' chars, so we have a list separated by ','
            assigned_questions_list = assigned_questions_list.replace("[", "")
            assigned_questions_list = assigned_questions_list.replace("]", "")
            # splitting values to get a list of question IDs
            assigned_questions_list = list(assigned_questions_list.split(", "))
            # converting string values to int values
            assigned_questions_list = [
                int(numeric_string) for numeric_string in assigned_questions_list
            ]
            # getting index of current question ID in assigned_questions_list
            index_of_question_id = assigned_questions_list.index(request.item_id) + 1
            return index_of_question_id
        else:
            return ""

    def get_viewed_order(self, request):
        # test_section_id context is defined
        if self.context.get("test_section_id", None):
            # getting test section component id based on provided test_section_id (from context)
            test_section_component_id = TestSectionComponent.objects.get(
                test_section=self.context["test_section_id"],
                component_type__in=[TestSectionComponentType.ITEM_BANK],
            ).id
        # test_section_component_id context is defined
        elif self.context.get("test_section_component_id", None):
            test_section_component_id = self.context["test_section_component_id"]
        # missing context (return None)
        else:
            return None
        # making sure to order by ID, so they are in the order that the candidate saw them
        candidate_answers = list(
            CandidateAnswers.objects.filter(
                assigned_test_id=self.context["assigned_test_id"],
                test_section_component_id=test_section_component_id,
            )
            .order_by("id")
            .values_list("item_id", flat=True)
        )
        # question has been viewed
        try:
            viewed_order = candidate_answers.index(request.item_id) + 1
            return viewed_order
        # question has not been viewed
        except:
            return None

    def get_answer_choices(self, request):
        context = self.context
        context["assigned_test_id"] = self.context.get("assigned_test_id", None)
        answer_choices = ItemOptionVW.objects.filter(item_id=request.item_id).order_by(
            "option_order"
        )
        answer_choices_data = ItemOptionTestTakerReportSerializer(
            answer_choices, many=True, context=context
        ).data
        return answer_choices_data

    def get_candidate_answer_data(self, request):
        context = self.context
        context["from_item_bank"] = True
        try:
            candidate_answer = CandidateAnswers.objects.get(
                assigned_test_id=self.context["assigned_test_id"],
                item_id=request.item_id,
            )
            candidate_answer_data = CandidateAnswersSerializer(
                candidate_answer, many=False, context=context
            ).data
            return candidate_answer_data
        except:
            return None

    def get_section_language(self, request):
        # getting item bank language ID
        item_bank_language_id = ItemBank.objects.get(
            id=request.item_bank_id
        ).language_id

        if item_bank_language_id == Language_id.FR:
            return "fr"
        # English
        elif item_bank_language_id == Language_id.EN:
            return "en"
        # Bilingual
        else:
            return "bilingual"

    class Meta:
        model = AllItemsDataVW
        fields = "__all__"


class UserExtendedProfileSerializer(serializers.ModelSerializer):
    current_employer = serializers.SerializerMethodField()
    organization = serializers.SerializerMethodField()
    group_and_level = serializers.SerializerMethodField()
    residence = serializers.SerializerMethodField()
    education = serializers.SerializerMethodField()
    gender = serializers.SerializerMethodField()
    identify_as_woman = serializers.SerializerMethodField()
    aboriginal = serializers.SerializerMethodField()
    visible_minority = serializers.SerializerMethodField()
    disability = serializers.SerializerMethodField()

    def get_current_employer(self, request):
        if request.current_employer_id != 0:
            current_employer = get_employer_list(request.current_employer_id)
            if current_employer[0]:
                return current_employer[0]
            else:
                return None
        else:
            return None

    def get_organization(self, request):
        if request.organization_id != 0:
            organization = get_organisation_list(request.organization_id)
            if organization[0]:
                return organization[0]
            else:
                return None
        else:
            return None

    def get_group_and_level(self, request):
        if request.level_id != 0:
            group_and_level = get_group_and_level_list(request.level_id)
            if group_and_level[0]:
                return group_and_level[0]
            else:
                return None
        elif request.group_id != 0:
            group_and_level = get_group_and_level_list(request.group_id)
            if group_and_level[0]:
                return [
                    {
                        "classif_id": group_and_level[0][0]["classif_id"],
                        "class_grp_cd": group_and_level[0][0]["class_grp_cd"],
                        "class_lvl_cd": None,
                    }
                ]
        else:
            return None

    def get_residence(self, request):
        if request.residence_id != 0:
            residence = get_residence_list(request.residence_id)
            if residence[0]:
                return residence[0]
            else:
                return None
        else:
            return None

    def get_education(self, request):
        if request.education_id != 0:
            education = get_education_list(request.education_id)
            if education[0]:
                return education[0]
            else:
                return None
        else:
            return None

    def get_gender(self, request):
        if request.gender_id != 0:
            gender = get_gender_list(request.gender_id)
            if gender[0]:
                return gender[0]
            else:
                return None
        else:
            return None

    def get_identify_as_woman(self, request):
        if request.identify_as_woman is not None:
            return {"identify_as_woman_id": request.identify_as_woman.opt_id}
        else:
            return {"identify_as_woman_id": None}

    def get_aboriginal(self, request):
        if request.aboriginal is not None:
            aboriginal_id = request.aboriginal.opt_id
            # aboriginal_id is NO or PREFER NOT TO ANSWER
            if (
                aboriginal_id == ExtendedProfileEEInfo.NO
                or aboriginal_id == ExtendedProfileEEInfo.PREFER_NOT_TO_SAY
            ):
                return {"aboriginal_id": aboriginal_id}
            else:
                # initializing aboriginal_sub_groups_array
                aboriginal_sub_groups_array = []
                # if context["start_date"] is defined
                if self.context.get("start_date", None):
                    try:
                        history_type = (
                            UserExtendedProfileAboriginal.history.filter(
                                user_extended_profile_id=request.id,
                                aboriginal_id=0,
                                history_date__lte=self.context["start_date"],
                            )
                            .order_by("history_date")
                            .last()
                            .history_type
                        )
                        checked = history_type == "+"
                    except:
                        checked = False
                    # pushing prefer not to answer status
                    aboriginal_sub_groups_array.append(
                        {"abrg_id": 0, "checked": checked}
                    )
                    # getting all existing aboriginal data (cat_aboriginal_vw)
                    all_existing_aboriginal_data = CatRefAboriginalVW.objects.all()
                    # looping in all_existing_aboriginal_data
                    for aboriginal_data in all_existing_aboriginal_data:
                        try:
                            history_type = (
                                UserExtendedProfileAboriginal.history.filter(
                                    user_extended_profile_id=request.id,
                                    aboriginal_id=aboriginal_data.abrg_id,
                                    history_date__lte=self.context["start_date"],
                                )
                                .order_by("history_date")
                                .last()
                                .history_type
                            )
                            checked = history_type == "+"
                        except:
                            checked = False
                        aboriginal_sub_groups_array.append(
                            {"abrg_id": aboriginal_data.abrg_id, "checked": checked}
                        )

                else:
                    # pushing prefer not to answer status
                    aboriginal_sub_groups_array.append(
                        {
                            "abrg_id": 0,
                            "checked": UserExtendedProfileAboriginal.objects.filter(
                                user_extended_profile_id=request.id,
                                aboriginal_id=0,
                            ).exists(),
                        }
                    )
                    # getting all existing aboriginal data (cat_aboriginal_vw)
                    all_existing_aboriginal_data = CatRefAboriginalVW.objects.all()
                    # looping in all_existing_aboriginal_data
                    for aboriginal_data in all_existing_aboriginal_data:
                        aboriginal_sub_groups_array.append(
                            {
                                "abrg_id": aboriginal_data.abrg_id,
                                "checked": UserExtendedProfileAboriginal.objects.filter(
                                    user_extended_profile_id=request.id,
                                    aboriginal_id=aboriginal_data.abrg_id,
                                ).exists(),
                            }
                        )
                return {
                    "aboriginal_id": aboriginal_id,
                    "aboriginal_sub_groups_array": aboriginal_sub_groups_array,
                }
        else:
            return {
                "aboriginal_id": None,
            }

    def get_visible_minority(self, request):
        if request.visible_minority is not None:
            visible_minority_id = request.visible_minority.opt_id
            # visible_minority_id is NO or PREFER NOT TO ANSWER
            if (
                visible_minority_id == ExtendedProfileEEInfo.NO
                or visible_minority_id == ExtendedProfileEEInfo.PREFER_NOT_TO_SAY
            ):
                return {"visible_minority_id": visible_minority_id}
            else:
                # initializing visible_minority_sub_groups_array
                visible_minority_sub_groups_array = []
                # if context["start_date"] is defined
                if self.context.get("start_date", None):
                    try:
                        history_type = (
                            UserExtendedProfileVisibleMinority.history.filter(
                                user_extended_profile_id=request.id,
                                visible_minority_id=0,
                                history_date__lte=self.context["start_date"],
                            )
                            .order_by("history_date")
                            .last()
                            .history_type
                        )
                        checked = history_type == "+"
                    except:
                        checked = False
                    # pushing prefer not to answer status
                    visible_minority_sub_groups_array.append(
                        {
                            "vismin_id": 0,
                            "checked": checked,
                            "other_specification": None,
                        }
                    )

                    # getting all existing visible_minority data (cat_visible_minority_vw)
                    all_existing_visible_minority_data = (
                        CatRefVisibleMinorityVW.objects.all()
                    )
                    # looping in all_existing_visible_minority_data
                    for visible_minority_data in all_existing_visible_minority_data:
                        # if other option
                        if visible_minority_data.vismin_id == VisibleMinorityId.OTHER:
                            try:
                                history_data = (
                                    UserExtendedProfileVisibleMinority.history.filter(
                                        user_extended_profile_id=request.id,
                                        visible_minority_id=visible_minority_data.vismin_id,
                                        history_date__lte=self.context["start_date"],
                                    )
                                    .order_by("history_date")
                                    .last()
                                )
                                checked = history_data.history_type == "+"
                                if checked:
                                    other_specification = (
                                        history_data.other_specification
                                    )
                                else:
                                    other_specification = None
                            except:
                                checked = False
                                other_specification = None
                            # pushing prefer not to answer status
                            visible_minority_sub_groups_array.append(
                                {
                                    "vismin_id": visible_minority_data.vismin_id,
                                    "checked": checked,
                                    "other_specification": other_specification,
                                }
                            )
                        else:
                            try:
                                history_data = (
                                    UserExtendedProfileVisibleMinority.history.filter(
                                        user_extended_profile_id=request.id,
                                        visible_minority_id=visible_minority_data.vismin_id,
                                        history_date__lte=self.context["start_date"],
                                    )
                                    .order_by("history_date")
                                    .last()
                                )
                                checked = history_data.history_type == "+"
                            except:
                                checked = False
                            # pushing prefer not to answer status
                            visible_minority_sub_groups_array.append(
                                {
                                    "vismin_id": visible_minority_data.vismin_id,
                                    "checked": checked,
                                    "other_specification": None,
                                }
                            )

                else:
                    # pushing prefer not to answer status
                    visible_minority_sub_groups_array.append(
                        {
                            "vismin_id": 0,
                            "checked": UserExtendedProfileVisibleMinority.objects.filter(
                                user_extended_profile_id=request.id,
                                visible_minority_id=0,
                            ).exists(),
                            "other_specification": None,
                        }
                    )
                    # getting all existing visible_minority data (cat_visible_minority_vw)
                    all_existing_visible_minority_data = (
                        CatRefVisibleMinorityVW.objects.all()
                    )
                    # looping in all_existing_visible_minority_data
                    for visible_minority_data in all_existing_visible_minority_data:
                        # if other option
                        if visible_minority_data.vismin_id == VisibleMinorityId.OTHER:
                            visible_minority_sub_groups_array.append(
                                {
                                    "vismin_id": visible_minority_data.vismin_id,
                                    "checked": UserExtendedProfileVisibleMinority.objects.filter(
                                        user_extended_profile_id=request.id,
                                        visible_minority_id=visible_minority_data.vismin_id,
                                    ).exists(),
                                    "other_specification": UserExtendedProfileVisibleMinority.objects.filter(
                                        user_extended_profile_id=request.id,
                                        visible_minority_id=visible_minority_data.vismin_id,
                                    )
                                    .first()
                                    .other_specification
                                    if UserExtendedProfileVisibleMinority.objects.filter(
                                        user_extended_profile_id=request.id,
                                        visible_minority_id=visible_minority_data.vismin_id,
                                    ).exists()
                                    else None,
                                }
                            )
                        else:
                            visible_minority_sub_groups_array.append(
                                {
                                    "vismin_id": visible_minority_data.vismin_id,
                                    "checked": UserExtendedProfileVisibleMinority.objects.filter(
                                        user_extended_profile_id=request.id,
                                        visible_minority_id=visible_minority_data.vismin_id,
                                    ).exists(),
                                    "other_specification": None,
                                }
                            )
                return {
                    "visible_minority_id": visible_minority_id,
                    "visible_minority_sub_groups_array": visible_minority_sub_groups_array,
                }
        else:
            return {"visible_minority_id": None}

    def get_disability(self, request):
        if request.disability is not None:
            disability_id = request.disability.opt_id
            # disability_id is NO or PREFER NOT TO ANSWER
            if (
                disability_id == ExtendedProfileEEInfo.NO
                or disability_id == ExtendedProfileEEInfo.PREFER_NOT_TO_SAY
            ):
                return {"disability_id": disability_id}
            else:
                # initializing disability_sub_groups_array
                disability_sub_groups_array = []
                # if context["start_date"] is defined
                if self.context.get("start_date", None):
                    try:
                        history_type = (
                            UserExtendedProfileDisability.history.filter(
                                user_extended_profile_id=request.id,
                                disability_id=0,
                                history_date__lte=self.context["start_date"],
                            )
                            .order_by("history_date")
                            .last()
                            .history_type
                        )
                        checked = history_type == "+"
                    except:
                        checked = False
                    # pushing prefer not to answer status
                    disability_sub_groups_array.append(
                        {
                            "dsbl_id": 0,
                            "checked": checked,
                            "other_specification": None,
                        }
                    )

                    # getting all existing disability data (cat_disability_vw)
                    all_existing_disability_data = CatRefDisabilityVW.objects.all()
                    # looping in all_existing_disability_data
                    for disability_data in all_existing_disability_data:
                        # if other option
                        if disability_data.dsbl_id == DisabilityId.OTHER:
                            try:
                                history_data = (
                                    UserExtendedProfileDisability.history.filter(
                                        user_extended_profile_id=request.id,
                                        disability_id=disability_data.dsbl_id,
                                        history_date__lte=self.context["start_date"],
                                    )
                                    .order_by("history_date")
                                    .last()
                                )
                                checked = history_data.history_type == "+"
                                if checked:
                                    other_specification = (
                                        history_data.other_specification
                                    )
                                else:
                                    other_specification = None
                            except:
                                checked = False
                                other_specification = None
                            # pushing prefer not to answer status
                            disability_sub_groups_array.append(
                                {
                                    "dsbl_id": disability_data.dsbl_id,
                                    "checked": checked,
                                    "other_specification": other_specification,
                                }
                            )
                        else:
                            try:
                                history_data = (
                                    UserExtendedProfileDisability.history.filter(
                                        user_extended_profile_id=request.id,
                                        disability_id=disability_data.dsbl_id,
                                        history_date__lte=self.context["start_date"],
                                    )
                                    .order_by("history_date")
                                    .last()
                                )
                                checked = history_data.history_type == "+"
                            except:
                                checked = False
                            # pushing prefer not to answer status
                            disability_sub_groups_array.append(
                                {
                                    "dsbl_id": disability_data.dsbl_id,
                                    "checked": checked,
                                    "other_specification": None,
                                }
                            )
                else:
                    # pushing prefer not to answer status
                    disability_sub_groups_array.append(
                        {
                            "dsbl_id": 0,
                            "checked": UserExtendedProfileDisability.objects.filter(
                                user_extended_profile_id=request.id,
                                disability_id=0,
                            ).exists(),
                            "other_specification": None,
                        }
                    )
                    # getting all existing disability data (cat_disability_vw)
                    all_existing_disability_data = CatRefDisabilityVW.objects.all()
                    # looping in all_existing_disability_data
                    for disability_data in all_existing_disability_data:
                        # if other option
                        if disability_data.dsbl_id == DisabilityId.OTHER:
                            disability_sub_groups_array.append(
                                {
                                    "dsbl_id": disability_data.dsbl_id,
                                    "checked": UserExtendedProfileDisability.objects.filter(
                                        user_extended_profile_id=request.id,
                                        disability_id=disability_data.dsbl_id,
                                    ).exists(),
                                    "other_specification": UserExtendedProfileDisability.objects.filter(
                                        user_extended_profile_id=request.id,
                                        disability_id=disability_data.dsbl_id,
                                    )
                                    .first()
                                    .other_specification
                                    if UserExtendedProfileDisability.objects.filter(
                                        user_extended_profile_id=request.id,
                                        disability_id=disability_data.dsbl_id,
                                    ).exists()
                                    else None,
                                }
                            )
                        else:
                            disability_sub_groups_array.append(
                                {
                                    "dsbl_id": disability_data.dsbl_id,
                                    "checked": UserExtendedProfileDisability.objects.filter(
                                        user_extended_profile_id=request.id,
                                        disability_id=disability_data.dsbl_id,
                                    ).exists(),
                                    "other_specification": None,
                                }
                            )
                return {
                    "disability_id": disability_id,
                    "disability_sub_groups_array": disability_sub_groups_array,
                }
        else:
            return {"disability_id": None}

    class Meta:
        model = UserExtendedProfile
        fields = [
            "current_employer",
            "organization",
            "group_and_level",
            "residence",
            "education",
            "gender",
            "identify_as_woman",
            "aboriginal",
            "visible_minority",
            "disability",
        ]


class TestTakerReportDataSerializer(serializers.ModelSerializer):
    user_data = serializers.SerializerMethodField()
    extended_profile_data = serializers.SerializerMethodField()
    test_data = serializers.SerializerMethodField()
    assigned_questions = serializers.SerializerMethodField()
    test_sections_score = serializers.SerializerMethodField()
    test_sections_timed_out_data = serializers.SerializerMethodField()
    first_timed_section_start_time = serializers.SerializerMethodField()
    financial_data = serializers.SerializerMethodField()
    orderless_financial_data = serializers.SerializerMethodField()

    def get_user_data(self, request):
        user_data = User.objects.filter(username=request.username_id)
        return UserSerializer(user_data, many=True).data

    def get_extended_profile_data(self, request):
        context = self.context
        context["start_date"] = request.start_date
        # getting last historical user extended profile data just before the start date (filter by history date)
        extended_profile_data = [
            (
                UserExtendedProfile.history.filter(
                    user_id=User.objects.get(username=request.username_id).id,
                    history_date__lte=request.start_date,
                )
                .order_by("history_date")
                .last()
            )
        ]
        return UserExtendedProfileSerializer(
            extended_profile_data, many=True, context=context
        ).data

    def get_test_data(self, request):
        test_data = TestDefinition.objects.filter(id=request.test_id)
        return TestSerializer(test_data, many=True).data

    def get_assigned_questions(self, request):
        context = self.context
        context["assigned_test_id"] = request.id
        # getting assigned questions list (question IDs)
        assigned_questions_lists = AssignedQuestionsList.objects.filter(
            assigned_test_id=request.id
        )
        # no assigned questions list (need to use the candidate answers data to build the assigned_questions_data_array)
        # should only happen for older data
        if not assigned_questions_lists:
            # initializing assigned_questions_data_array
            assigned_questions_data_array = []
            # getting candidate answers data
            candidate_answers_test_section_components = (
                CandidateAnswers.objects.filter(assigned_test_id=request.id)
                .values("test_section_component_id")
                .distinct()
            )
            # looping in candidate_answers_test_section_components
            for test_section_component in candidate_answers_test_section_components:
                # getting question IDs related to current candidate answers and current test section component
                question_ids = CandidateAnswers.objects.filter(
                    assigned_test_id=request.id,
                    test_section_component_id=test_section_component[
                        "test_section_component_id"
                    ],
                ).values_list("question_id")
                # getting questions data
                questions_data = NewQuestion.objects.filter(id__in=question_ids)

                # setting needed context variable
                context["test_section_component_id"] = test_section_component[
                    "test_section_component_id"
                ]

                assigned_questions_data_array.append(
                    AssignedQuestionsSerializer(
                        questions_data, many=True, context=context
                    ).data
                )

        # assigned questions list exists
        else:
            # initializing assigned_questions_data_array
            assigned_questions_data_array = []
            # looping in assigned_questions_lists
            for assigned_questions_list in assigned_questions_lists:
                if assigned_questions_list is not None:
                    # converting assigned_questions_list (string) in array
                    assigned_question_ids = literal_eval(
                        assigned_questions_list.questions_list
                    )
                    # questions are coming from the Item Bank
                    if assigned_questions_list.from_item_bank:
                        # getting questions data
                        questions_data = AllItemsDataVW.objects.filter(
                            item_id__in=assigned_question_ids
                        )

                        # getting test_section_id related to current assigned questions list
                        assigned_test_section = AssignedTestSection.objects.filter(
                            assigned_test_id=assigned_questions_list.assigned_test_id,
                            test_section_id=assigned_questions_list.test_section_id,
                        )
                        # assigned test section is defined
                        if assigned_test_section:
                            test_section_id = (
                                assigned_test_section.first().test_section_id
                            )
                            context["test_section_id"] = test_section_id

                            assigned_questions_data_array.append(
                                AssignedItemsSerializer(
                                    questions_data, many=True, context=context
                                ).data
                            )
                        # no assigned test section test (should only happen for tests from Jmeter execution)
                        else:
                            assigned_questions_data_array = []

                    # questions are coming from test builder
                    else:
                        # getting questions data
                        questions_data = NewQuestion.objects.filter(
                            id__in=assigned_question_ids
                        )
                        # getting test_section_id related to current assigned questions list
                        assigned_test_section = AssignedTestSection.objects.filter(
                            assigned_test_id=assigned_questions_list.assigned_test_id,
                            test_section_id=assigned_questions_list.test_section_id,
                        )
                        # assigned test section is defined
                        if assigned_test_section:
                            test_section_id = (
                                assigned_test_section.first().test_section_id
                            )
                            context["test_section_id"] = test_section_id

                            assigned_questions_data_array.append(
                                AssignedQuestionsSerializer(
                                    questions_data, many=True, context=context
                                ).data
                            )
                        # no assigned test section test (should only happen for tests from Jmeter execution)
                        else:
                            assigned_questions_data_array = []
        return assigned_questions_data_array

    def get_test_sections_score(self, request):
        assigned_test_sections = AssignedTestSection.objects.filter(
            assigned_test_id=request.id
        )
        assigned_test_sections_with_score = []
        for assigned_test_section in assigned_test_sections:
            if assigned_test_section.score is not None:
                assigned_test_sections_with_score.append(assigned_test_section.score)
        return assigned_test_sections_with_score

    def get_test_sections_timed_out_data(self, request):
        assigned_test_sections = AssignedTestSection.objects.filter(
            assigned_test_id=request.id
        )
        assigned_test_sections_timed_out_array = []
        for assigned_test_section in assigned_test_sections:
            if assigned_test_section.test_section_time is not None:
                assigned_test_sections_timed_out_array.append(
                    assigned_test_section.timed_out
                )
        return assigned_test_sections_timed_out_array

    def get_first_timed_section_start_time(self, request):
        assigned_test_historical_data = (
            AssignedTest.history.filter(id=request.id, status=AssignedTestStatus.ACTIVE)
            .order_by("modify_date")
            .first()
        )
        if assigned_test_historical_data:
            # converting time to local timezone (since by default, the dates/times in serializers are already converted)
            time = assigned_test_historical_data.modify_date
            system_tz = pytz.timezone(settings.TIME_ZONE)
            time = timezone.localtime(time, pytz.timezone(str(system_tz)))
            return time
        else:
            return None

    def get_financial_data(self, request):
        try:
            financial_data = (
                TestPermissions.history.filter(
                    test_id=request.test_id,
                    test_order_number=request.test_order_number,
                    history_date__lte=request.submit_date,
                    history_type="+",
                )
                .order_by("history_date")
                .last()
            )
            if financial_data is not None:
                serialized_data = GetTestPermissionsSerializer(
                    financial_data, many=False
                ).data
                return serialized_data
            else:
                return None
        except:
            return None

    def get_orderless_financial_data(self, request):
        try:
            orderless_financial_data = OrderlessFinancialData.objects.get(
                id=request.orderless_financial_data_id
            )
            serialized_data = OrderlessFinancialDataSerializer(
                orderless_financial_data, many=False
            ).data
            return serialized_data
        except:
            return None

    class Meta:
        model = AssignedTest
        fields = "__all__"
