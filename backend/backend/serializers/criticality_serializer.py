from rest_framework import serializers
from backend.custom_models.criticality import Criticality


class CriticalitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Criticality
        fields = "__all__"
