from rest_framework import serializers
from backend.ref_table_views.cat_aboriginal_vw import CatRefAboriginalVW


class CatAboriginalSerializer(serializers.ModelSerializer):
    class Meta:
        model = CatRefAboriginalVW
        fields = ["abrg_id", "edesc", "fdesc"]
