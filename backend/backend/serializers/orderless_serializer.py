from rest_framework import serializers
from backend.custom_models.unsupervised_test_access_code_model import (
    UnsupervisedTestAccessCode,
)
from backend.custom_models.test_access_code_model import TestAccessCode
from backend.custom_models.uit_invites import UITInvites
from backend.custom_models.reasons_for_testing import ReasonsForTesting
from backend.ref_table_views.cat_ref_departments_vw import CatRefDepartmentsVW
from backend.serializers.cat_ref_departments_serializer import (
    CatRefDepartmentsSerializer,
)
from backend.serializers.uit_processes_serializer import (
    ReasonsForTestingSerializer,
)
from backend.custom_models.orderless_financial_data import OrderlessFinancialData


class OrderlessFinancialDataSerializer(serializers.ModelSerializer):
    department_data = serializers.SerializerMethodField()
    reason_for_testing_data = serializers.SerializerMethodField()
    ta_username = serializers.SerializerMethodField()
    test_id = serializers.SerializerMethodField()

    def get_department_data(self, request):
        try:
            department_data = CatRefDepartmentsVW.objects.get(
                dept_id=request.department_ministry_id
            )
            serialized_data = CatRefDepartmentsSerializer(
                department_data, many=False
            ).data
            return serialized_data
        except:
            return None

    def get_reason_for_testing_data(self, request):
        try:
            reason_for_testing_data = ReasonsForTesting.objects.get(
                id=request.reason_for_testing.id
            )
            serialized_data = ReasonsForTestingSerializer(
                reason_for_testing_data, many=False
            ).data
            return serialized_data
        except:
            return None

    def get_ta_username(self, request):
        # uit_invite_id exists
        if request.uit_invite_id:
            ta_username = UITInvites.objects.get(
                id=request.uit_invite_id
            ).ta_username_id
        else:
            ta_username = (
                TestAccessCode.history.filter(orderless_financial_data_id=request.id)
                .first()
                .ta_username_id
            )
        return ta_username

    def get_test_id(self, request):
        # uit_invite_id exists
        if request.uit_invite_id:
            test_id = (
                UnsupervisedTestAccessCode.history.filter(
                    uit_invite_id=request.uit_invite_id
                )
                .first()
                .test_id
            )
        else:
            test_id = (
                TestAccessCode.history.filter(orderless_financial_data_id=request.id)
                .first()
                .test_id
            )
        return test_id

    class Meta:
        model = OrderlessFinancialData
        fields = "__all__"
