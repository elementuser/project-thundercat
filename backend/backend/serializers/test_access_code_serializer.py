from rest_framework import serializers
from backend.custom_models.orderless_financial_data import OrderlessFinancialData
from backend.custom_models.test_access_code_model import TestAccessCode
from cms.cms_models.test_definition import TestDefinition
from cms.cms_models.test_permissions_model import TestPermissions
from user_management.user_management_models.user_models import User


class TestAccessCodeSerializer(serializers.ModelSerializer):
    en_test_name = serializers.SerializerMethodField()
    fr_test_name = serializers.SerializerMethodField()
    staffing_process_number = serializers.SerializerMethodField()
    department_ministry_code = serializers.SerializerMethodField()
    is_org = serializers.SerializerMethodField()
    is_ref = serializers.SerializerMethodField()
    billing_contact = serializers.SerializerMethodField()
    billing_contact_info = serializers.SerializerMethodField()

    def get_test_permission_data(self, request):
        try:
            test_permission_data = TestPermissions.objects.get(
                username_id=request.ta_username_id,
                test_id=request.test_id,
                test_order_number=request.test_order_number,
            )
            return test_permission_data
        except:
            return None

    def get_en_test_name(self, request):
        en_test_name = TestDefinition.objects.get(id=request.test_id).en_name
        return en_test_name

    def get_fr_test_name(self, request):
        fr_test_name = TestDefinition.objects.get(id=request.test_id).fr_name
        return fr_test_name

    def get_staffing_process_number(self, request):
        if self.get_test_permission_data(request) is not None:
            return self.get_test_permission_data(request).staffing_process_number
        # orderless request
        else:
            return OrderlessFinancialData.objects.get(
                id=request.orderless_financial_data_id
            ).reference_number

    def get_department_ministry_code(self, request):
        if self.get_test_permission_data(request) is not None:
            return self.get_test_permission_data(request).department_ministry_code
        # orderless request
        else:
            return OrderlessFinancialData.objects.get(
                id=request.orderless_financial_data_id
            ).department_ministry_id

    def get_is_org(self, request):
        if self.get_test_permission_data(request) is not None:
            return self.get_test_permission_data(request).is_org
        # orderless request
        else:
            return OrderlessFinancialData.objects.get(
                id=request.orderless_financial_data_id
            ).fis_organisation_code

    def get_is_ref(self, request):
        if self.get_test_permission_data(request) is not None:
            return self.get_test_permission_data(request).is_ref
        # orderless request
        else:
            return OrderlessFinancialData.objects.get(
                id=request.orderless_financial_data_id
            ).fis_reference_code

    def get_billing_contact(self, request):
        if self.get_test_permission_data(request) is not None:
            return self.get_test_permission_data(request).billing_contact
        # orderless request
        else:
            return OrderlessFinancialData.objects.get(
                id=request.orderless_financial_data_id
            ).billing_contact_name

    def get_billing_contact_info(self, request):
        if self.get_test_permission_data(request) is not None:
            return self.get_test_permission_data(request).billing_contact_info
        # orderless request
        else:
            return OrderlessFinancialData.objects.get(
                id=request.orderless_financial_data_id
            ).billing_contact_info

    class Meta:
        model = TestAccessCode
        fields = "__all__"
