from rest_framework import serializers
from backend.ref_table_views.cat_ref_province_vw import CatRefProvinceVW


class CatRefProvinceSerializer(serializers.ModelSerializer):
    class Meta:
        model = CatRefProvinceVW
        fields = [
            "prov_id",
            "active_flg",
            "eabrv",
            "fabrv",
            "legacy_cd",
            "edesc",
            "fdesc",
            "efdt",
            "xdt",
        ]
