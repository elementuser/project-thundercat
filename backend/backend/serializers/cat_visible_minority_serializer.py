from rest_framework import serializers
from backend.ref_table_views.cat_visible_minority_vw import CatRefVisibleMinorityVW


class CatVisibleMinoritySerializer(serializers.ModelSerializer):
    class Meta:
        model = CatRefVisibleMinorityVW
        fields = ["vismin_id", "edesc", "fdesc", "full_edesc", "full_fdesc"]
