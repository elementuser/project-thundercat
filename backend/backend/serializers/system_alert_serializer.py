from rest_framework import serializers
from backend.custom_models.criticality import Criticality
from backend.serializers.criticality_serializer import CriticalitySerializer
from backend.custom_models.system_alert import SystemAlert


class SystemAlertSerializer(serializers.ModelSerializer):
    criticality_data = serializers.SerializerMethodField()

    def get_criticality_data(self, request):
        criticality_data = Criticality.objects.get(id=request.criticality_id)
        serialized_data = CriticalitySerializer(criticality_data, many=False).data
        return serialized_data

    class Meta:
        model = SystemAlert
        fields = "__all__"
