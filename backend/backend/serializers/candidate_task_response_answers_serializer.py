from rest_framework import serializers
from backend.custom_models.candidate_task_response_answers import (
    CandidateTaskResponseAnswers,
)


class CandidateTaskResponseAnswersSerializer(serializers.ModelSerializer):
    class Meta:
        model = CandidateTaskResponseAnswers
        fields = "__all__"
