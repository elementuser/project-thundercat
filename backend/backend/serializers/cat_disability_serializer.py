from rest_framework import serializers
from backend.ref_table_views.cat_disability_vw import CatRefDisabilityVW


class CatDisabilitySerializer(serializers.ModelSerializer):
    class Meta:
        model = CatRefDisabilityVW
        fields = ["dsbl_id", "edesc", "fdesc", "full_edesc", "full_fdesc"]
