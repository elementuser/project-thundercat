from rest_framework import permissions
from backend.views.utils import get_user_info_from_jwt_token
from user_management.user_management_models.user_models import User
from backend.custom_models.assigned_test import AssignedTest
from cms.views.utils import get_needed_parameters


class HasAssignedTestPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        if not permissions.IsAuthenticated():
            return False

        success, parameters = get_needed_parameters(["test_id"], request)
        if not parameters.get("test_id", True) or not success:

            return False

        if not permissions.IsAuthenticated():
            return False
        user_info = get_user_info_from_jwt_token(request)

        # if the user is an admin, grant access
        user = User.objects.get(username=user_info["username"])
        if user.is_staff:
            return True

        test = AssignedTest.objects.get(id=parameters["test_id"])

        if user.username != test.username.username:
            return False

        return True


class HasAssignedTestFromAssignedTestPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        if not permissions.IsAuthenticated():
            return False

        success, parameters = get_needed_parameters(["assigned_test_id"], request)

        if not parameters.get("assigned_test_id", True) or not success:
            return False

        if not permissions.IsAuthenticated():
            return False
        user_info = get_user_info_from_jwt_token(request)

        # if the user is an admin, grant access
        user = User.objects.get(username=user_info["username"])

        if user.is_staff:
            return True

        test = AssignedTest.objects.get(id=parameters["assigned_test_id"])

        if user.username != test.username.username:
            return False

        return True
