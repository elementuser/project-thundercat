from rest_framework import permissions
from backend.views.utils import get_user_info_from_jwt_token
from user_management.user_management_models.custom_permissions_model import (
    CustomPermissions,
)
from user_management.user_management_models.custom_user_permissions_model import (
    CustomUserPermissions,
)
from user_management.static.permission import Permission
from user_management.user_management_models.user_models import User


class HasTestAdminPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        return user_has_permission(request, Permission.TEST_ADMINISTRATOR)


class HasSystemAdminPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        return user_has_permission(request, Permission.SYSTEM_ADMINISTRATOR)


class HasPPCAdminPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        return user_has_permission(request, Permission.PPC_ADMINISTRATOR)


class HasScorerPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        return user_has_permission(request, Permission.SCORER)


class HasTestBuilderPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        return user_has_permission(request, Permission.TEST_BUILDER)


class HasAaePermission(permissions.BasePermission):
    def has_permission(self, request, view):
        return user_has_permission(request, Permission.AAE)


class HasAaePermission(permissions.BasePermission):
    def has_permission(self, request, view):
        return user_has_permission(request, Permission.AAE)


class HasRdOperationsPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        return user_has_permission(request, Permission.RD_OPERATIONS)


class HasTestDeveloperPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        return user_has_permission(request, Permission.TEST_DEVELOPER)


def user_has_permission(request, codename):
    user_info = get_user_info_from_jwt_token(request)

    # if the user is an admin, grant access
    user = User.objects.get(username=user_info["username"])
    if user.is_staff:
        return True

    user_permissions = CustomUserPermissions.objects.filter(
        user_id=user_info["username"],
        permission=CustomPermissions.objects.get(codename=codename),
    ).first()

    if user_permissions:
        return True

    return False
