from django.db import models
from simple_history.models import HistoricalRecords


class Criticality(models.Model):
    codename = models.CharField(max_length=50, default="temp", null=False, blank=False)
    description_en = models.CharField(max_length=150, null=False, blank=False)
    description_fr = models.CharField(max_length=150, null=False, blank=False)
    active = models.BooleanField(null=False, blank=False)
    priority = models.IntegerField(default=0, null=False, blank=False)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    class Meta:
        constraints = [
            models.UniqueConstraint(
                name="must_be_a_unique_criticality_codename",
                fields=["codename"],
            )
        ]
