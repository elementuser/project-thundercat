from django.db import models
from simple_history.models import HistoricalRecords
from backend.custom_models.break_bank import BreakBank


class AccommodationRequest(models.Model):
    break_bank = models.ForeignKey(
        BreakBank,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=True,
        blank=False,
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()
