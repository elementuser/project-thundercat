from django.db import models
from simple_history.models import HistoricalRecords

# model to track the UIT invitations
class ReasonsForTesting(models.Model):
    description_en = models.CharField(
        max_length=255, default="", null=False, blank=False
    )
    description_fr = models.CharField(
        max_length=255, default="", null=False, blank=False
    )
    active = models.BooleanField(default=False)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()
