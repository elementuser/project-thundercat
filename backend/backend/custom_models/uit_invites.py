from django.db import models
from django.utils import timezone
from simple_history.models import HistoricalRecords
from cms.cms_models.test_permissions_model import TestPermissions
from user_management.user_management_models.user_models import User
from backend.custom_models.uit_reasons_for_deletion import UITReasonsForDeletion
from backend.custom_models.uit_reasons_for_modification import UITReasonsForModification


# model to track the UIT invitations
class UITInvites(models.Model):
    ta_username = models.ForeignKey(
        User, to_field="username", on_delete=models.DO_NOTHING, null=True
    )
    ta_test_permissions = models.ForeignKey(
        TestPermissions, to_field="id", on_delete=models.SET_NULL, null=True
    )
    invite_date = models.DateField(default=timezone.now)
    validity_end_date = models.DateField(blank=False, null=False)
    orderless_request = models.BooleanField(default=False)
    reason_for_deletion = models.ForeignKey(
        UITReasonsForDeletion, to_field="id", on_delete=models.DO_NOTHING, null=True
    )
    reason_for_modification = models.ForeignKey(
        UITReasonsForModification, to_field="id", on_delete=models.DO_NOTHING, null=True
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()
