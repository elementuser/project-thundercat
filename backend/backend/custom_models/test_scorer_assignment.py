from django.db import models
from simple_history.models import HistoricalRecords
from backend.custom_models.assigned_test import AssignedTest
from user_management.user_management_models.user_models import User


# Link between


class TestScorerAssignment(models.Model):
    id = models.AutoField(primary_key=True)
    assigned_test = models.ForeignKey(
        AssignedTest, to_field="id", on_delete=models.DO_NOTHING
    )
    scorer_username = models.ForeignKey(
        User, to_field="username", on_delete=models.DO_NOTHING, null=True, blank=True
    )
    status = models.IntegerField(default=0)
    score_date = models.DateTimeField(blank=True, null=True)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()
