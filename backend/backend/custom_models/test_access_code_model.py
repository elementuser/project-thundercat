from django.db import models
from django.utils import timezone
from simple_history.models import HistoricalRecords
from cms.cms_models.test_definition import TestDefinition
from user_management.user_management_models.user_models import User
from backend.custom_models.language import Language
from backend.custom_models.orderless_financial_data import OrderlessFinancialData


# Room Number Model, containing the randomly generated room numbers
# Used to make sure that only authorized candidate get their test assigned through this room number


class TestAccessCode(models.Model):
    id = models.AutoField(primary_key=True)
    test_access_code = models.CharField(max_length=10, unique=True)
    test_order_number = models.CharField(max_length=12, blank=False, null=True)
    test = models.ForeignKey(TestDefinition, on_delete=models.DO_NOTHING, null=False)
    test_session_language = models.ForeignKey(
        Language, to_field="language_id", on_delete=models.DO_NOTHING, null=True
    )
    ta_username = models.ForeignKey(
        User, to_field="username", on_delete=models.DO_NOTHING, null=True
    )
    created_date = models.DateField(default=timezone.now)
    orderless_financial_data = models.ForeignKey(
        OrderlessFinancialData, on_delete=models.DO_NOTHING, null=True, blank=False
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()
