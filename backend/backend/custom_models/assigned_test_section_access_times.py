from django.utils import timezone
from django.db import models
from simple_history.models import HistoricalRecords
from backend.custom_models.assigned_test_section import AssignedTestSection


class AssignedTestSectionAccessTimes(models.Model):
    assigned_test_section = models.ForeignKey(
        AssignedTestSection, on_delete=models.DO_NOTHING
    )
    time_type = models.IntegerField()
    time = models.DateTimeField(default=timezone.now, blank=True)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "{0}".format(self.assigned_test_section.test_section.en_title)
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Assigned Test Section Access Times"
