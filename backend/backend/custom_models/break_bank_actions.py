from django.db import models
from simple_history.models import HistoricalRecords

from backend.custom_models.break_bank import BreakBank
from cms.cms_models.test_section import TestSection


class BreakBankActions(models.Model):
    action_type = models.CharField(max_length=15, null=False, blank=False)
    modify_date = models.DateTimeField(auto_now=True)
    # time in seconds
    new_remaining_time = models.IntegerField(null=True, blank=False)
    break_bank = models.ForeignKey(
        BreakBank,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=False,
        blank=False,
    )
    test_section = models.ForeignKey(
        TestSection,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=False,
        blank=False,
        default=1,
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()
