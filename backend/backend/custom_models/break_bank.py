from django.db import models
from simple_history.models import HistoricalRecords


class BreakBank(models.Model):
    # time in seconds
    break_time = models.IntegerField(blank=False, null=False)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()
