from django.db import models
from simple_history.models import HistoricalRecords
from cms.cms_models.test_definition import TestDefinition
from cms.cms_models.test_section import TestSection
from user_management.user_management_models.user_models import User
from backend.custom_models.language import Language
from backend.custom_models.orderless_financial_data import OrderlessFinancialData
from backend.custom_models.accommodation_request import AccommodationRequest


# ** IMPORTANT **
# If any changes are made to this model, please make sure to notify CAMM team (CAT/CAMM Relation Logic)
class AssignedTest(models.Model):
    username = models.ForeignKey(User, to_field="username", on_delete=models.DO_NOTHING)
    status = models.IntegerField(blank=True, null=False)
    previous_status = models.IntegerField(blank=True, null=True)
    start_date = models.DateTimeField(blank=True, null=True)
    modify_date = models.DateTimeField(auto_now=True)
    submit_date = models.DateTimeField(blank=True, null=True)
    test_access_code = models.CharField(max_length=10, null=True, blank=True)
    test_order_number = models.CharField(max_length=12, blank=False, null=True)
    total_score = models.IntegerField(blank=False, null=True)
    en_converted_score = models.CharField(max_length=50, null=True, blank=True)
    fr_converted_score = models.CharField(max_length=50, null=True, blank=True)
    test = models.ForeignKey(TestDefinition, on_delete=models.DO_NOTHING)
    test_session_language = models.ForeignKey(
        Language,
        to_field="language_id",
        on_delete=models.DO_NOTHING,
        null=True,
        blank=True,
    )
    ta = models.ForeignKey(
        User,
        to_field="username",
        related_name="ta_username",
        on_delete=models.DO_NOTHING,
        null=True,
        blank=True,
    )
    test_section = models.ForeignKey(
        TestSection, on_delete=models.DO_NOTHING, null=True, blank=True
    )
    uit_invite_id = models.IntegerField(blank=False, null=True)
    orderless_financial_data = models.ForeignKey(
        OrderlessFinancialData, on_delete=models.DO_NOTHING, null=True, blank=False
    )
    is_invalid = models.BooleanField(default=False)
    accommodation_request = models.ForeignKey(
        AccommodationRequest, on_delete=models.DO_NOTHING, null=True, blank=False
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    class Meta:
        constraints = [
            models.UniqueConstraint(
                name="must_be_a_unique_status_username_tac_test_combination",
                fields=["status", "username", "test_access_code", "test"],
            )
        ]
