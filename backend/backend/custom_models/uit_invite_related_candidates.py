from django.db import models
from simple_history.models import HistoricalRecords
from backend.custom_models.uit_invites import UITInvites
from backend.custom_models.uit_reasons_for_deletion import UITReasonsForDeletion


# model to track the UIT invitation related candidates
class UITInviteRelatedCandidates(models.Model):
    first_name = models.CharField(max_length=30, null=False, blank=False)
    last_name = models.CharField(max_length=150, null=False, blank=False)
    email = models.CharField(max_length=150, null=False, blank=False)
    uit_invite = models.ForeignKey(
        UITInvites, to_field="id", on_delete=models.DO_NOTHING, null=False, default=0
    )
    reason_for_deletion = models.ForeignKey(
        UITReasonsForDeletion, to_field="id", on_delete=models.DO_NOTHING, null=True
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()
