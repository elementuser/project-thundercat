from django.db import models
from simple_history.models import HistoricalRecords
from backend.custom_models.uit_invites import UITInvites
from backend.custom_models.reasons_for_testing import ReasonsForTesting


# model to track the UIT invitation orderless financial data
class OrderlessFinancialData(models.Model):
    reference_number = models.CharField(max_length=25, null=False, blank=False)
    department_ministry_id = models.IntegerField(null=False, blank=False)
    fis_organisation_code = models.CharField(max_length=16, null=False, blank=False)
    fis_reference_code = models.CharField(max_length=20, null=False, blank=False)
    billing_contact_name = models.CharField(max_length=180, null=False, blank=False)
    billing_contact_info = models.CharField(max_length=255, null=False, blank=False)
    level_required = models.CharField(max_length=50, null=True, blank=False)
    reason_for_testing = models.ForeignKey(
        ReasonsForTesting, to_field="id", on_delete=models.DO_NOTHING, null=True
    )
    uit_invite = models.ForeignKey(
        UITInvites, to_field="id", on_delete=models.DO_NOTHING, null=True, default=0
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()
