from django.db import models
from simple_history.models import HistoricalRecords
from backend.custom_models.ta_actions import TaActions


# Pause/Unpause Test Actions Model (data for all Pause/Unpause Actions)


class PauseTestActions(models.Model):
    pause_start_date = models.DateTimeField(blank=False, null=False)
    pause_test_time = models.IntegerField(blank=False, null=False)
    pause_end_date = models.DateTimeField(blank=True, null=True)
    ta_action = models.ForeignKey(
        TaActions, to_field="id", on_delete=models.DO_NOTHING, null=False, blank=False
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()
