from django.db import models
from simple_history.models import HistoricalRecords
from backend.custom_models.candidate_answers import CandidateAnswers
from cms.cms_models.answer import Answer


class CandidateMultipleChoiceAnswers(models.Model):
    # from test builder
    answer = models.ForeignKey(Answer, on_delete=models.DO_NOTHING, null=True)
    # from item bank
    item_answer_id = models.IntegerField(null=True, default=None)
    candidate_answers = models.ForeignKey(CandidateAnswers, on_delete=models.DO_NOTHING)
    modify_date = models.DateTimeField(auto_now=True)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()
