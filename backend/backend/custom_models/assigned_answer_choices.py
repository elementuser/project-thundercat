from django.db import models
from django.db.models import Q
from simple_history.models import HistoricalRecords
from cms.cms_models.items import Items
from backend.custom_models.assigned_test import AssignedTest
from cms.cms_models.new_question import NewQuestion


class AssignedAnswerChoices(models.Model):
    assigned_test = models.ForeignKey(
        AssignedTest, to_field="id", on_delete=models.DO_NOTHING
    )
    question = models.ForeignKey(
        NewQuestion, to_field="id", on_delete=models.DO_NOTHING, null=True
    )
    item = models.ForeignKey(
        Items, to_field="id", on_delete=models.DO_NOTHING, null=True
    )
    answer_choices = models.TextField(blank=False, null=False)

    # for auditing purpose and it stores create, update, delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    class Meta:
        # setting the model name in Django Admin Console
        constraints = [
            models.UniqueConstraint(
                condition=Q(question__isnull=False),
                fields=["assigned_test", "question"],
                name="combination_of_assigned_test_id_and_question_id_must_be_unique_in_assigned_answer_choices",
            ),
            models.UniqueConstraint(
                condition=Q(item__isnull=False),
                fields=["assigned_test", "item"],
                name="combination_of_assigned_test_id_and_item_id_must_be_unique_in_assigned_answer_choices",
            ),
        ]
