from django.db import models
from simple_history.models import HistoricalRecords
from backend.custom_models.assigned_test import AssignedTest
from backend.custom_models.etta_action_types import EttaActionTypes


# ETTA Actions Model (data for all ETTA Actions that needs DB entries)


class EttaActions(models.Model):
    action_type = models.ForeignKey(
        EttaActionTypes,
        to_field="action_type",
        on_delete=models.DO_NOTHING,
        blank=False,
        null=False,
    )
    assigned_test = models.ForeignKey(
        AssignedTest,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=False,
        blank=False,
    )
    modify_date = models.DateTimeField(auto_now=True)
    action_reason = models.CharField(max_length=300, null=True, blank=True)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()
