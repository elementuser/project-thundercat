from django.db import models
from simple_history.models import HistoricalRecords
from backend.custom_models.assigned_test import AssignedTest
from backend.custom_models.ta_action_types import TaActionTypes
from cms.cms_models.test_section import TestSection


# TA Actions Model (data for all TA Actions that needs DB entries)


class TaActions(models.Model):
    action_type = models.ForeignKey(
        TaActionTypes,
        to_field="action_type",
        on_delete=models.DO_NOTHING,
        blank=False,
        null=False,
    )
    assigned_test = models.ForeignKey(
        AssignedTest,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=False,
        blank=False,
    )
    test_section = models.ForeignKey(
        TestSection, on_delete=models.DO_NOTHING, null=True, blank=True
    )
    modify_date = models.DateTimeField(auto_now=True)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()
