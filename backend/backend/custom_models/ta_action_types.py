from django.db import models
from simple_history.models import HistoricalRecords

# TA Action Types Model (mapping action types data)


class TaActionTypes(models.Model):
    action_type = models.CharField(primary_key=True, max_length=15)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()
