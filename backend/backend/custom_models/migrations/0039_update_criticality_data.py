from django.db import migrations


def update_criticality_data(apps, schema_editor):
    # get models
    criticality = apps.get_model("custom_models", "criticality")
    # get db alias
    db_alias = schema_editor.connection.alias

    # updating criticality data
    criticality_1 = criticality.objects.using(db_alias).get(description_en="Danger")
    criticality_1.codename = "danger"
    criticality_1.priority = 1
    criticality_1.save()

    criticality_2 = criticality.objects.using(db_alias).get(description_en="Warning")
    criticality_2.codename = "warning"
    criticality_2.priority = 2
    criticality_2.save()

    criticality_3 = criticality.objects.using(db_alias).get(
        description_en="Information"
    )
    criticality_3.codename = "information"
    criticality_3.priority = 3
    criticality_3.save()


def rollback_changes(apps, schema_editor):
    # get models
    criticality = apps.get_model("custom_models", "criticality")
    # get db alias
    db_alias = schema_editor.connection.alias
    # updating to previous data
    criticality_1 = criticality.objects.using(db_alias).get(codename="danger")
    criticality_1.codename = "temp"
    criticality_1.priority = 0
    criticality_1.save()

    criticality_2 = criticality.objects.using(db_alias).get(codename="warning")
    criticality_2.codename = "temp"
    criticality_2.priority = 0
    criticality_2.save()

    criticality_3 = criticality.objects.using(db_alias).get(codename="information")
    criticality_3.codename = "temp"
    criticality_3.priority = 0
    criticality_3.save()


class Migration(migrations.Migration):
    dependencies = [("custom_models", "0038_auto_20220824_1346")]
    operations = [migrations.RunPython(update_criticality_data, rollback_changes)]
