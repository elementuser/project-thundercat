from django.db import migrations


def create_new_etta_action_types(apps, _):
    # get models
    etta_action_types = apps.get_model("custom_models", "ettaactiontypes")
    # creating all needed ETTA Action Types
    action_type_1 = etta_action_types(action_type="UN_ASSIGN_CANDIDATE")
    action_type_1.save()


def rollback_new_etta_action_types(apps, schema_editor):
    # get models
    etta_action_types = apps.get_model("custom_models", "ettaactiontypes")
    # get db alias
    db_alias = schema_editor.connection.alias
    # delete the types
    etta_action_types.objects.using(db_alias).filter(
        action_type="UN_ASSIGN_CANDIDATE"
    ).delete()


class Migration(migrations.Migration):
    dependencies = [
        (
            "custom_models",
            "0014_ettaactions_ettaactiontypes_historicalettaactions_historicalettaactiontypes",
        )
    ]
    operations = [
        migrations.RunPython(
            create_new_etta_action_types, rollback_new_etta_action_types
        )
    ]
