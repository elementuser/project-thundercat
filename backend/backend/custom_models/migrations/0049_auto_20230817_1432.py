# Generated by Django 3.2.18 on 2023-08-17 18:32

from django.db import migrations, models
import django.db.models.deletion
from django.conf import settings


def drop_index_if_exists(apps, schema_editor):
    # access to the connection since schema_editor.execute does not return the cursor
    with schema_editor.connection.cursor() as cursor:
        # initializing exists to False
        exists = False
        # checking if custom_models_candidateanswers_assigned_test_id_question_id_6f2348c6_uniq already exist
        cursor.execute(
            "SELECT * FROM sys.indexes WHERE name = 'custom_models_candidateanswers_assigned_test_id_question_id_6f2348c6_uniq'"
        )
        for result in cursor:
            exists = True

    # custom_models_candidateanswers_assigned_test_id_question_id_6f2348c6_uniq already exist
    if exists:
        # deleting respective index
        # if local
        if settings.IS_LOCAL:
            schema_editor.execute(
                "DROP INDEX custom_models_candidateanswers_assigned_test_id_question_id_6f2348c6_uniq ON dbo.custom_models_candidateanswers"
            )
        # legacy environment
        else:
            schema_editor.execute(
                "DROP INDEX custom_models_candidateanswers_assigned_test_id_question_id_6f2348c6_uniq ON cat.dbo.custom_models_candidateanswers"
            )


class Migration(migrations.Migration):
    dependencies = [
        ("cms_models", "0079_historicalitembankrule_itembankrule"),
        ("custom_models", "0048_create_language_text_data"),
    ]

    operations = [
        migrations.RunPython(drop_index_if_exists),
        migrations.AddField(
            model_name="assignedanswerchoices",
            name="from_item_bank",
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name="assignedquestionslist",
            name="from_item_bank",
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name="candidateanswers",
            name="item",
            field=models.ForeignKey(
                default=None,
                null=True,
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="cms_models.items",
            ),
        ),
        migrations.AddField(
            model_name="candidatemultiplechoiceanswers",
            name="item_answer",
            field=models.ForeignKey(
                default=None,
                null=True,
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="cms_models.itemoption",
            ),
        ),
        migrations.AddField(
            model_name="historicalassignedanswerchoices",
            name="from_item_bank",
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name="historicalassignedquestionslist",
            name="from_item_bank",
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name="historicalcandidateanswers",
            name="item",
            field=models.ForeignKey(
                blank=True,
                db_constraint=False,
                default=None,
                null=True,
                on_delete=django.db.models.deletion.DO_NOTHING,
                related_name="+",
                to="cms_models.items",
            ),
        ),
        migrations.AddField(
            model_name="historicalcandidatemultiplechoiceanswers",
            name="item_answer",
            field=models.ForeignKey(
                blank=True,
                db_constraint=False,
                default=None,
                null=True,
                on_delete=django.db.models.deletion.DO_NOTHING,
                related_name="+",
                to="cms_models.itemoption",
            ),
        ),
        migrations.AlterField(
            model_name="candidateanswers",
            name="question",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="cms_models.newquestion",
            ),
        ),
        migrations.AddConstraint(
            model_name="candidateanswers",
            constraint=models.UniqueConstraint(
                condition=models.Q(("question__isnull", False)),
                fields=("assigned_test", "question"),
                name="combination_of_assigned_test_id_and_question_id_must_be_unique",
            ),
        ),
        migrations.AddConstraint(
            model_name="candidateanswers",
            constraint=models.UniqueConstraint(
                condition=models.Q(("item__isnull", False)),
                fields=("assigned_test", "item"),
                name="combination_of_assigned_test_id_and_item_id_must_be_unique",
            ),
        ),
    ]
