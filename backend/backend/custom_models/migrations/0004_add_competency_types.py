# Created manually to resolve conflicts (moved from initial) on 2020/09/18

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):
    dependencies = [
        ("cms_models", "0003_auto_20200601_0911"),
        ("custom_models", "0003_create_ta_actions"),
    ]
    operations = [
        migrations.AddField(
            model_name="assignedtestanswerscore",
            name="competency",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="cms_models.CompetencyType",
            ),
        )
    ]

