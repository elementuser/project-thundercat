from django.db import migrations


def create_new_uit_reasons_for_deletion(apps, _):
    # get models
    uit_reason_for_deletion = apps.get_model("custom_models", "uitreasonsfordeletion")
    # creating all needed UIT reasons for deletion
    reason_1 = uit_reason_for_deletion(
        en_name="Candidate withdrawal", fr_name="Retrait du candidat"
    )
    reason_1.save()
    reason_2 = uit_reason_for_deletion(
        en_name="Wrong test sent", fr_name="Mauvais test envoyé"
    )
    reason_2.save()
    reason_3 = uit_reason_for_deletion(
        en_name="Confirmed valid results", fr_name="Résultats valides confirmés"
    )
    reason_3.save()
    reason_4 = uit_reason_for_deletion(
        en_name="Requested by department",
        fr_name="Annulation demandée par le département",
    )
    reason_4.save()
    reason_5 = uit_reason_for_deletion(
        en_name="Requested accommodation", fr_name="Mesures d’adaptation demandée"
    )
    reason_5.save()
    reason_6 = uit_reason_for_deletion(
        en_name="Process Expired", fr_name="Processus Expiré"
    )
    reason_6.save()
    reason_7 = uit_reason_for_deletion(en_name="Other", fr_name="Autre")
    reason_7.save()


def rollback_changes(apps, schema_editor):
    # get models
    uit_reason_for_deletion = apps.get_model("custom_models", "uitreasonsfordeletion")
    # get db alias
    db_alias = schema_editor.connection.alias
    # delete the types
    uit_reason_for_deletion.objects.using(db_alias).filter(
        en_name="Candidate withdrawal"
    ).delete()
    uit_reason_for_deletion.objects.using(db_alias).filter(
        en_name="Wrong test sent"
    ).delete()
    uit_reason_for_deletion.objects.using(db_alias).filter(
        en_name="Confirmed valid results"
    ).delete()
    uit_reason_for_deletion.objects.using(db_alias).filter(
        en_name="Requested by department"
    ).delete()
    uit_reason_for_deletion.objects.using(db_alias).filter(
        en_name="Requested accommodation"
    ).delete()
    uit_reason_for_deletion.objects.using(db_alias).filter(
        en_name="Process Expired"
    ).delete()
    uit_reason_for_deletion.objects.using(db_alias).filter(en_name="Other").delete()


class Migration(migrations.Migration):
    dependencies = [("custom_models", "0020_auto_20211203_1124")]
    operations = [
        migrations.RunPython(create_new_uit_reasons_for_deletion, rollback_changes)
    ]
