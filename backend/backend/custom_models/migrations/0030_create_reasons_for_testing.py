from django.db import migrations


def create_new_reasons_for_testing(apps, _):
    # get models
    reason_for_testing = apps.get_model("custom_models", "reasonsfortesting")
    # creating all needed UIT reasons for deletion
    reason_1 = reason_for_testing(
        description_en="Imperative staffing",
        description_fr="Dotation impérative",
        active=1,
    )
    reason_1.save()
    reason_2 = reason_for_testing(
        description_en="Non-imperative staffing",
        description_fr="Dotation non impérative",
        active=1,
    )
    reason_2.save()
    reason_3 = reason_for_testing(
        description_en="Full-time language training from the Canada School of Public Service",
        description_fr="Formation linguistique à temps plein offerte par l'École de la fonction publique du Canada",
        active=0,
    )
    reason_3.save()
    reason_4 = reason_for_testing(
        description_en="Part-time Language training from the Canada School of Public Service",
        description_fr="Formation linguistique à temps partiel dispensée par l'École de la fonction publique du Canada",
        active=0,
    )
    reason_4.save()
    reason_5 = reason_for_testing(
        description_en="Full-time language training from departmental program",
        description_fr="Formation linguistique à temps plein offerte par les ministères",
        active=1,
    )
    reason_5.save()
    reason_6 = reason_for_testing(
        description_en="Part-time language training from departmental program",
        description_fr="Formation linguistique à temps partiel offerte par les ministères",
        active=1,
    )
    reason_6.save()
    reason_7 = reason_for_testing(
        description_en="Full-time language training from private language school",
        description_fr="Form. ling. à temps plein disp. par un établissement ling.",
        active=0,
    )
    reason_7.save()
    reason_8 = reason_for_testing(
        description_en="Part-time language training from private language school",
        description_fr="Form. ling. à temps part. disp. par un établissement ling.",
        active=0,
    )
    reason_8.save()
    reason_9 = reason_for_testing(
        description_en="Re-identification", description_fr="Réidentification", active=1
    )
    reason_9.save()
    reason_10 = reason_for_testing(
        description_en="Bilingualism bonus",
        description_fr="Prime au bilinguisme",
        active=1,
    )
    reason_10.save()
    reason_11 = reason_for_testing(
        description_en="Record or other purposes",
        description_fr="Fins documentaires ou autres",
        active=1,
    )
    reason_11.save()
    reason_12 = reason_for_testing(
        description_en="COVID-19 or Essential Service",
        description_fr="COVID-19 ou Service Essential",
        active=1,
    )
    reason_12.save()


def rollback_changes(apps, schema_editor):
    # get models
    reason_for_testing = apps.get_model("custom_models", "reasonsfortesting")
    # get db alias
    db_alias = schema_editor.connection.alias
    # delete the types
    reason_for_testing.objects.using(db_alias).filter(
        description_en="Imperative staffing"
    ).delete()
    reason_for_testing.objects.using(db_alias).filter(
        description_en="Non-imperative staffing"
    ).delete()
    reason_for_testing.objects.using(db_alias).filter(
        description_en="Full-time language training from the Canada School of Public Service"
    ).delete()
    reason_for_testing.objects.using(db_alias).filter(
        description_en="Part-time Language training from the Canada School of Public Service"
    ).delete()
    reason_for_testing.objects.using(db_alias).filter(
        description_en="Full-time language training from departmental program"
    ).delete()
    reason_for_testing.objects.using(db_alias).filter(
        description_en="Part-time language training from departmental program"
    ).delete()
    reason_for_testing.objects.using(db_alias).filter(
        description_en="Full-time language training from private language school"
    ).delete()
    reason_for_testing.objects.using(db_alias).filter(
        description_en="Part-time language training from private language school"
    ).delete()
    reason_for_testing.objects.using(db_alias).filter(
        description_en="Re-identification"
    ).delete()
    reason_for_testing.objects.using(db_alias).filter(
        description_en="Bilingualism bonus"
    ).delete()
    reason_for_testing.objects.using(db_alias).filter(
        description_en="Record or other purposes"
    ).delete()
    reason_for_testing.objects.using(db_alias).filter(
        description_en="COVID-19 or Essential Service"
    ).delete()


class Migration(migrations.Migration):
    dependencies = [
        ("custom_models", "0029_historicalreasonsfortesting_reasonsfortesting")
    ]
    operations = [
        migrations.RunPython(create_new_reasons_for_testing, rollback_changes)
    ]
