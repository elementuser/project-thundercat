# Generated by Django 3.2.20 on 2023-08-24 20:08

from django.db import migrations
from django.conf import settings


def create_index_if_does_not_exist(apps, schema_editor):
    # access to the connection since schema_editor.execute does not return the cursor
    with schema_editor.connection.cursor() as cursor:
        # initializing exists to False
        exists = False
        # checking if temp_index already exists (should not exist)
        cursor.execute("SELECT * FROM sys.indexes WHERE name = 'temp_index'")
        for result in cursor:
            exists = True

    # temp_index does not exist
    if not exists:
        # creating temp_index
        # if local
        if settings.IS_LOCAL:
            schema_editor.execute(
                "CREATE UNIQUE NONCLUSTERED INDEX temp_index ON dbo.custom_models_candidateanswers ([assigned_test_id] ASC, [question_id] ASC) WHERE question_id IS NOT NULL"
            )
        # legacy environment
        else:
            schema_editor.execute(
                "CREATE UNIQUE NONCLUSTERED INDEX temp_index ON cat.dbo.custom_models_candidateanswers ([assigned_test_id] ASC, [question_id] ASC) WHERE question_id IS NOT NULL"
            )


class Migration(migrations.Migration):
    dependencies = [
        ("custom_models", "0051_auto_20230822_1118"),
    ]

    operations = [
        migrations.RunPython(create_index_if_does_not_exist),
        migrations.AlterUniqueTogether(
            name="candidateanswers",
            unique_together=set(),
        ),
    ]
