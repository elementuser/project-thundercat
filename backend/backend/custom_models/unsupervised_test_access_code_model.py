from django.db import models
from django.utils import timezone
from simple_history.models import HistoricalRecords
from cms.cms_models.test_definition import TestDefinition
from user_management.user_management_models.user_models import User
from backend.custom_models.uit_invites import UITInvites
from backend.custom_models.orderless_financial_data import (
    OrderlessFinancialData,
)
from backend.custom_models.accommodation_request import AccommodationRequest


# model to store UIT test access codes
class UnsupervisedTestAccessCode(models.Model):
    test_access_code = models.CharField(max_length=10, unique=True)
    test_order_number = models.CharField(max_length=12, blank=False, null=True)
    test = models.ForeignKey(TestDefinition, on_delete=models.DO_NOTHING, null=False)
    ta_username = models.ForeignKey(
        User, to_field="username", on_delete=models.DO_NOTHING, null=True
    )
    candidate_email = models.EmailField(max_length=254, blank=False, null=True)
    created_date = models.DateField(default=timezone.now)
    validity_end_date = models.DateField(blank=False, null=True)
    uit_invite = models.ForeignKey(
        UITInvites, to_field="id", on_delete=models.DO_NOTHING, null=False, default=0
    )
    orderless_financial_data = models.ForeignKey(
        OrderlessFinancialData,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=True,
        default=None,
    )
    accommodation_request = models.ForeignKey(
        AccommodationRequest, on_delete=models.DO_NOTHING, null=True, blank=False
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()
