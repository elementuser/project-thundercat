from __future__ import absolute_import, unicode_literals

from celery import shared_task
from backend.celery.task_definition.assigned_tests import (
    update_non_submitted_active_assigned_tests,
    unpause_assigned_tests,
    invalidate_locked_assigned_tests,
    unassign_ready_and_pre_test_assigned_tests,
)
from backend.celery.task_definition.test_permissions import (
    remove_expired_test_permissions,
)
from backend.celery.task_definition.uit import (
    handle_expired_uit_processes,
    handle_send_uit_invite_emails,
    handle_update_uit_validity_end_date_emails,
    handle_delete_uit_invite_emails,
)

from backend.celery.task_definition.user_permissions import (
    deprovision_after_inactivity,
    deprovision_admin_after_inactivity_thirty_days,
)
from backend.celery.task_definition.reset_completed_profile_flag import (
    reset_completed_profile_flag,
)


# ==================== *IMPORTANT* ====================
# if you do any update to those tasks, don't forget to restart the celery and celery-beat containers (for testing purposes)

# =============== ASSIGNED TESTS TASKS ===============
@shared_task
def run_every_night_at_11():
    update_non_submitted_active_assigned_tests()


@shared_task
def run_every_night_at_11_15():
    invalidate_locked_assigned_tests()


@shared_task
def run_every_60_minutes():
    unassign_ready_and_pre_test_assigned_tests()


@shared_task
def run_every_15_minutes():
    unpause_assigned_tests()


# =============== ASSIGNED TESTS TASKS (END) ===============

# =============== TEST PERMISSIONS TASKS ===============
@shared_task
def run_every_morning_at_1():
    remove_expired_test_permissions()


# =============== TEST PERMISSIONS TASKS (END) ===============

# =============== UIT TASKS ===============
@shared_task
def run_every_morning_at_00_01():
    handle_expired_uit_processes()


@shared_task
def run_every_morning_at_00_30():
    reset_completed_profile_flag()


# =============== UIT TASKS (END) ===============

# ========== DEPROVISION TASKS ==========
@shared_task
def run_every_evening_at_11_30():
    deprovision_after_inactivity()


@shared_task
def run_every_morning_at_00_45():
    deprovision_admin_after_inactivity_thirty_days()


# ========== DEPROVISION TASKS (END) ==========


# ========== EMAIL TASKS ==========
@shared_task(bind=True)
def send_uit_emails_celery_task(self, candidate_invitations_array, validity_end_date):
    handle_send_uit_invite_emails(candidate_invitations_array, validity_end_date)


@shared_task(bind=True)
def send_update_uit_validity_end_date_email_celery_task(self, candidates_to_send_email):
    handle_update_uit_validity_end_date_emails(candidates_to_send_email)


@shared_task(bind=True)
def delete_uit_invite_email_celery_task(self, candidates_to_send_email):
    handle_delete_uit_invite_emails(candidates_to_send_email)


# ========== EMAIL TASKS (END) ==========
