import pytz
from datetime import datetime
from dateutil.relativedelta import relativedelta
from backend.celery.task_definition.utils import ConsoleMessageColor


def reset_completed_profile_flag():
    # imports need to be here to avoid errors/exceptions when starting the containers
    from user_management.user_management_models.user_models import User

    # printing celery task name
    print(
        "{0} ==================== CELERY TASK EXECUTION: RESET COMPLETED PROFILE FLAG ====================".format(
            ConsoleMessageColor.GREEN
        )
    )

    # getting current date
    current_date = datetime.today()
    three_months_ago = current_date - relativedelta(months=3)
    three_months_ago = three_months_ago.replace(tzinfo=pytz.utc)

    # get all users with outdated profiles (i.e. have not saved an update to their profile in the last 3 months)
    outdated_users = User.objects.filter(
        last_profile_update__lte=three_months_ago,
        is_profile_complete=True,
    )

    for outdated_user in outdated_users:
        outdated_user.is_profile_complete = False
        outdated_user.save()

        print(
            "{0} ==================== CELERY TASK EXECUTION: {1} IS_PROFILE_COMPLETE RESET TO FALSE' ====================".format(
                ConsoleMessageColor.CYAN, outdated_user.username
            )
        )
