from __future__ import absolute_import, unicode_literals
from datetime import date
from backend.celery.task_definition.utils import ConsoleMessageColor

# this function is deleting the expired test permissions
def remove_expired_test_permissions():
    # imports need to be here to avoid errors/exceptions when starting the containers
    from cms.cms_models.test_permissions_model import TestPermissions
    from backend.custom_models.test_access_code_model import TestAccessCode

    # printing celery task name
    print(
        "{0} ==================== CELERY TASK EXECUTION: REMOVE EXPIRED TEST PERMISSIONS ====================".format(
            ConsoleMessageColor.GREEN
        )
    )

    # getting current date
    current_date = date.today()

    # getting all test permissions
    test_permissions = TestPermissions.objects.all()

    # looping in test permissions
    for test_permission in test_permissions:
        # expiry date is < today's date
        if test_permission.expiry_date < current_date:
            # saving test permission id
            test_permission_id = test_permission.id
            # deleting active test accesse codes related to the current TA + current test order number
            test_access_codes = TestAccessCode.objects.filter(
                ta_username_id=test_permission.username_id,
                test_order_number=test_permission.test_order_number,
            )
            for test_access_code in test_access_codes:
                TestAccessCode.objects.get(id=test_access_code.id).delete()
            # delete current test permission
            test_permission.delete()
            # printing successful messasge
            print(
                "{0} ==================== TEST_PERMISSION_ID: {1} HAS BEEN DELETED SUCCESSFULLY ====================".format(
                    ConsoleMessageColor.CYAN, test_permission_id
                )
            )
