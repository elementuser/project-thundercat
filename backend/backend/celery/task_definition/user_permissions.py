from datetime import datetime
import pytz
from dateutil.relativedelta import relativedelta
from backend.celery.task_definition.utils import ConsoleMessageColor


def deprovision_after_inactivity():
    # imports need to be here to avoid errors/exceptions when starting the containers
    from user_management.user_management_models.user_models import User
    from cms.cms_models.test_permissions_model import TestPermissions
    from backend.custom_models.test_access_code_model import TestAccessCode
    from user_management.user_management_models.custom_user_permissions_model import (
        CustomUserPermissions,
    )
    from user_management.user_management_models.ta_extended_profile_models import (
        TaExtendedProfile,
    )
    from cms.cms_models.orderless_test_permissions_model import (
        OrderlessTestPermissions,
    )

    deleted_by_system = "deleted by system"

    # printing celery task name
    print(
        "{0} ==================== CELERY TASK EXECUTION: DEPROVISION INACTIVE USERS ====================".format(
            ConsoleMessageColor.GREEN
        )
    )

    # getting current date
    current_date = datetime.today()
    one_year_ago = current_date - relativedelta(years=1)
    one_year_ago = one_year_ago.replace(tzinfo=pytz.utc)

    # getting all custom user permissions
    permissions = CustomUserPermissions.objects.all()

    for permission in permissions:
        # getting related user
        related_user = User.objects.get(username=permission.user_id)
        # last_login is None or <= 30 days
        if related_user.last_login is None or related_user.last_login <= one_year_ago:
            # deleting respective permissions
            related_user_permissions = CustomUserPermissions.objects.filter(
                user_id=related_user.username
            )
            # at least one permission has been found
            if related_user_permissions:
                # TEST ACCESS CODES
                # look up and delete test access codes related to the expired user
                test_access_codes = TestAccessCode.objects.filter(
                    ta_username_id=related_user.email
                )
                if test_access_codes:
                    print(
                        "{0} \t--> Deleting test access codes for user: {1}".format(
                            ConsoleMessageColor.WHITE, related_user.email
                        )
                    )
                for test_access_code in test_access_codes:
                    # delete test access code
                    print(
                        '{0} \t\t--> TEST_ACCESS_CODE = "{1}"'.format(
                            ConsoleMessageColor.WHITE,
                            test_access_code.test_access_code,
                        )
                    )
                    # Delete the TestAccessCode object
                    test_access_code.delete()

                # TEST PERMISSIONS
                # look up and delete test permissions
                test_permissions = TestPermissions.objects.filter(
                    username=related_user.email
                )
                if test_permissions:
                    print(
                        "{0} \t--> Delete all test permissions for user: {1}".format(
                            ConsoleMessageColor.WHITE, related_user.email
                        )
                    )
                for test_permission in test_permissions:
                    print(
                        "{0} \t\t--> Test Definition's Parent Code: {1}".format(
                            ConsoleMessageColor.WHITE, test_permission.test.parent_code
                        )
                    )
                    test_permission.reason_for_modif_or_del = deleted_by_system
                    test_permission.save()
                    test_permission.delete()

                # CUSTOM USER PERMISSIONS
                print(
                    "{0} \t--> Deleting custom user permissions for user: {1}".format(
                        ConsoleMessageColor.WHITE, related_user.email
                    )
                )
                for permission_to_be_deleted in related_user_permissions:
                    # delete the permission
                    print(
                        "{0} \t\t--> {1}".format(
                            ConsoleMessageColor.WHITE,
                            permission_to_be_deleted.permission.en_name,
                        )
                    )
                    # update the reason for modify or delete, save
                    permission_to_be_deleted.reason_for_modif_or_del = deleted_by_system
                    permission_to_be_deleted.save()
                    # delete permission
                    permission_to_be_deleted.delete()

                # ORDERLESS TEST PERMISSIONS
                # Initialize a queryset of OrderlessTestPermissions object to None
                orderless_test_permissions = OrderlessTestPermissions.objects.none()

                # Get all TaExtendedProfiles for our user
                ta_extended_profiles = TaExtendedProfile.objects.filter(
                    username=related_user.email
                )

                # make a list of orderless test permissions to delete
                if ta_extended_profiles:
                    for ta_extended_profile in ta_extended_profiles:
                        orderless_test_permissions_temp = (
                            OrderlessTestPermissions.objects.filter(
                                ta_extended_profile=ta_extended_profile
                            )
                        )
                        # add all orderless test permission in our list
                        # Reference: https://stackoverflow.com/questions/5195217/django-concatenate-a-db-object-with-empty-queryset
                        orderless_test_permissions = (
                            orderless_test_permissions | orderless_test_permissions_temp
                        )
                if orderless_test_permissions:
                    print(
                        "{0} \t--> Deleting orderless test permissions for user: {1}".format(
                            ConsoleMessageColor.WHITE, related_user.email
                        )
                    )
                    for orderless_test_permission in orderless_test_permissions:
                        # delete the orderless test permission
                        print(
                            "{0} \t\t--> parent_code: {1}\ttest_code: {2}".format(
                                ConsoleMessageColor.WHITE,
                                orderless_test_permission.parent_code,
                                orderless_test_permission.test_code,
                            )
                        )
                        orderless_test_permission.delete()

                # printing successful messasge
                print(
                    "{0} ==================== CELERY TASK EXECUTION: {1} HAS BEEN DEPROVISIONED SUCCESSFULLY ====================".format(
                        ConsoleMessageColor.CYAN, related_user.email
                    )
                )


# after 30 days of inactivity, automatically remove all custom permissions (other than TA) for the user
def deprovision_admin_after_inactivity_thirty_days():
    # imports need to be here to avoid errors/exceptions when starting the containers
    from user_management.user_management_models.user_models import User
    from user_management.user_management_models.custom_permissions_model import (
        CustomPermissions,
    )
    from user_management.user_management_models.custom_user_permissions_model import (
        CustomUserPermissions,
    )
    from user_management.static.permission import Permission

    deleted_by_system = "deleted by system"

    # printing celery task name
    print(
        "{0} ==================== CELERY TASK EXECUTION: DEPROVISION INACTIVE ADMINS AFTER 30 DAYS OF INACTIVITY ====================".format(
            ConsoleMessageColor.GREEN
        )
    )

    # getting current date
    current_date = datetime.today()
    thirty_days_ago = current_date - relativedelta(days=30)
    thirty_days_ago = thirty_days_ago.replace(tzinfo=pytz.utc)

    # getting excluded permission IDs (TA and Test Adaptation)
    excluded_permission_ids = CustomPermissions.objects.filter(
        codename__in=(Permission.TEST_ADMINISTRATOR, Permission.AAE)
    ).values_list("permission_id", flat=True)

    # getting all custom user permissions
    permissions = CustomUserPermissions.objects.all()

    for permission in permissions:
        if permission.permission_id not in excluded_permission_ids:
            # getting related user
            related_user = User.objects.get(username=permission.user_id)
            # last_login is None or <= 30 days
            if (
                related_user.last_login is None
                or related_user.last_login <= thirty_days_ago
            ):
                # deleting respective permissions
                related_user_permissions = CustomUserPermissions.objects.filter(
                    user_id=related_user.username
                ).exclude(permission_id__in=excluded_permission_ids)
                # at least one permission has been found
                if related_user_permissions:
                    print(
                        "{0} \t--> Deleting custom user permissions for user: {1}".format(
                            ConsoleMessageColor.WHITE, related_user.email
                        )
                    )
                    for permission_to_be_deleted in related_user_permissions:
                        # delete the permission
                        print(
                            "{0} \t\t--> {1}".format(
                                ConsoleMessageColor.WHITE,
                                permission_to_be_deleted.permission.en_name,
                            )
                        )
                        # update the reason for modify or delete, save
                        permission_to_be_deleted.reason_for_modif_or_del = (
                            deleted_by_system
                        )
                        permission_to_be_deleted.save()
                        # delete permission
                        permission_to_be_deleted.delete()
                    # printing successful messasge
                    print(
                        "{0} ==================== CELERY TASK EXECUTION: {1} HAS BEEN DEPROVISIONED SUCCESSFULLY ====================".format(
                            ConsoleMessageColor.CYAN, related_user.email
                        )
                    )
