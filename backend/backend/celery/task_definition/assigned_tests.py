from __future__ import absolute_import, unicode_literals
from datetime import timedelta
import datetime
import pytz
from backend.celery.task_definition.utils import ConsoleMessageColor

# this function is scoring and updating the status of the non-submitted assigned tests
def update_non_submitted_active_assigned_tests():
    # imports need to be here to avoid errors/exceptions when starting the containers
    from backend.custom_models.assigned_test import AssignedTest
    from backend.custom_models.assigned_test_section import AssignedTestSection
    from backend.views.uit_score_test import uit_scoring, Action
    from backend.static.assigned_test_status import AssignedTestStatus
    from cms.views.retrieve_test_section_data import (
        get_updated_time_after_lock_pause_function,
        get_total_assigned_test_time_in_seconds,
    )

    # printing celery task name
    print(
        "{0} ==================== CELERY TASK EXECUTION: UPDATE NON-SUBMITTED ACTIVE ASSIGNED TESTS ====================".format(
            ConsoleMessageColor.GREEN
        )
    )
    # getting current time
    current_time = datetime.datetime.now(pytz.utc)
    # getting all active assigned tests
    active_assigned_tests = AssignedTest.objects.filter(
        status=AssignedTestStatus.ACTIVE
    )
    # looping in active_assigned_tests
    for active_assigned_test in active_assigned_tests:
        # getting new test start time considering all lock/pause times of the current assigned test
        test_data = get_updated_time_after_lock_pause_function(active_assigned_test.id)
        # test_data is not None and not equals to "no-access-times-test"
        if (
            test_data["time"] is not None
            and test_data["time"] != "no-access-times-test"
        ):
            # getting total test time
            total_test_time = get_total_assigned_test_time_in_seconds(
                active_assigned_test.id
            )
            # if current_time > test_data + total test time
            if current_time > test_data["time"] + timedelta(seconds=total_test_time):
                # ========== scoring ==========
                # getting timed_assigned_test_section_ids
                timed_assigned_test_section_ids = test_data[
                    "timed_assigned_test_section_ids"
                ]
                # looping in timed_assigned_test_section_ids
                for timed_assigned_test_section_id in timed_assigned_test_section_ids:
                    # score test section
                    parameters = {
                        "assigned_test_id": active_assigned_test.id,
                        "test_section_id": timed_assigned_test_section_id,
                    }
                    uit_scoring(parameters, Action.TEST_SECTION)
                    # set timed out flag
                    assigned_test_section = AssignedTestSection.objects.get(
                        test_section_id=timed_assigned_test_section_id,
                        assigned_test_id=active_assigned_test.id,
                    )
                    assigned_test_section.timed_out = True
                    assigned_test_section.save()
                # make sure submit_date is updated to the datetime now
                active_assigned_test.submit_date = datetime.datetime.now(pytz.utc)
                # put the data in db
                active_assigned_test.save()
                # scoring test
                parameters = {"assigned_test_id": active_assigned_test.id}
                uit_scoring(parameters, Action.TEST)
                # ========== scoring (END) ==========
                # printing successful messasge
                print(
                    "{0} ==================== ASSIGNED_TEST_ID: {1} SCORED AND UPDATED SUCCESSFULLY TO STATUS SUBMITTED ====================".format(
                        ConsoleMessageColor.CYAN, active_assigned_test.id
                    )
                )
        # should only happen for tests from Jmeter executions
        elif test_data["time"] == "no-access-times-test":
            # update needed active_assigned_test fields
            active_assigned_test.submit_date = datetime.datetime.now(pytz.utc)
            active_assigned_test.save()
            # scoring test
            parameters = {"assigned_test_id": active_assigned_test.id}
            uit_scoring(parameters, Action.TEST)
            # printing warning/error message
            print(
                "{0} ==================== (SHOULD ONLY HAPPEN FOR TESTS FROM JMETER EXECUTION) ASSIGNED_TEST_ID: {1} SCORED AND UPDATED SUCCESSFULLY TO STATUS SUBMITTED ====================".format(
                    ConsoleMessageColor.RED, active_assigned_test.id
                )
            )
        # should not happen
        else:
            # TODO (fnormand): send email if that happens
            # printing error
            print(
                "{0} ==================== SOMETHING WENT WRONG RELATED TO ASSIGNED_TEST_ID: {1} ====================".format(
                    ConsoleMessageColor.RED, active_assigned_test.id
                )
            )


# this function is unpausing assigned tests that are stuck in PAUSED state
def unpause_assigned_tests():
    # imports need to be here to avoid errors/exceptions when starting the containers
    from backend.static.assigned_test_status import AssignedTestStatus
    from backend.custom_models.assigned_test import AssignedTest
    from backend.custom_models.accommodation_request import AccommodationRequest
    from backend.custom_models.break_bank import BreakBank
    from backend.custom_models.break_bank_actions import BreakBankActions
    from backend.views.utils import get_last_accessed_test_section
    from cms.views.test_break_bank import BreakBankActionsConstants

    # printing celery task name
    print(
        "{0} ==================== CELERY TASK EXECUTION: UNPAUSE ASSIGNED TESTS ====================".format(
            ConsoleMessageColor.GREEN
        )
    )
    # getting current time
    current_time = datetime.datetime.now(pytz.utc)
    # getting all paused assigned tests
    paused_assigned_tests = AssignedTest.objects.filter(
        status=AssignedTestStatus.PAUSED
    )
    # looping in paused_assigned_tests
    for paused_assigned_test in paused_assigned_tests:
        # getting related accommodation request data
        accommodation_request_data = AccommodationRequest.objects.get(
            id=paused_assigned_test.accommodation_request_id
        )
        # getting related break bank data
        break_bank_data = BreakBank.objects.get(
            id=accommodation_request_data.break_bank_id
        )
        # getting last accessed assigned test section ID
        last_accessed_test_section_id = get_last_accessed_test_section(
            paused_assigned_test.id
        )
        # getting related break bank actions related to current test section
        break_bank_actions = BreakBankActions.objects.filter(
            break_bank_id=break_bank_data.id,
            test_section_id=last_accessed_test_section_id,
        )
        # getting most updated break bank remaining time
        # if there is a previous UNPAUSE action ==> most updated remaining time is the one from that UNPAUSE action
        # if there is no previous UNPAUSE action ==> most updated remaining time is basically the break bank time (from the break bank table)
        try:
            most_updated_remaining_time = break_bank_actions[
                len(break_bank_actions - 2)
            ].new_remaining_time
        except:
            most_updated_remaining_time = break_bank_data.break_time
        # getting last break bank action (last action should be a PAUSE, since the test is currently on PAUSE)
        last_pause_action = break_bank_actions.last()
        # making sure that the last action is in fact a PAUSE
        if last_pause_action.action_type == BreakBankActionsConstants.PAUSE:
            # validating if there is still time left on the pause action based on the provided most_updated_remaining_time and last_pause_action
            # getting time (in seconds) between current time and last pause action
            time_between_last_pause_action_and_current_time = (
                current_time - last_pause_action.modify_date
            ).total_seconds()
            # time_between_last_pause_action_and_current_time greater than the most updated remaining time
            if (
                time_between_last_pause_action_and_current_time
                > most_updated_remaining_time
            ):
                # adding new UNPAUSE action with a remaining time of 0
                BreakBankActions.objects.create(
                    action_type=BreakBankActionsConstants.UNPAUSE,
                    new_remaining_time=0,
                    break_bank_id=break_bank_data.id,
                    test_section_id=last_pause_action.test_section_id,
                )
                # update test status to ACTIVE
                paused_assigned_test.status = AssignedTestStatus.ACTIVE
                paused_assigned_test.save()
                # printing successful messasge
                print(
                    "{0} ==================== ASSIGNED_TEST_ID: {1} UPDATED SUCCESSFULLY TO STATUS ACTIVE (UNPAUSED) ====================".format(
                        ConsoleMessageColor.CYAN, paused_assigned_test.id
                    )
                )
        # should never happen
        else:
            # TODO (fnormand): send email if that happens
            # printing error
            print(
                "{0} ==================== SOMETHING WENT WRONG RELATED TO ASSIGNED_TEST_ID: {1} ====================".format(
                    ConsoleMessageColor.RED, paused_assigned_test.id
                )
            )


# this function is invalidating assigned tests that are stuck in LOCKED state (indeterminate)
def invalidate_locked_assigned_tests():
    # imports need to be here to avoid errors/exceptions when starting the containers
    from backend.static.assigned_test_status import AssignedTestStatus
    from backend.custom_models.assigned_test import AssignedTest
    from backend.custom_models.assigned_test_section import AssignedTestSection

    # printing celery task name
    print(
        "{0} ==================== CELERY TASK EXECUTION: INVALIDATE LOCKED ASSIGNED TESTS ====================".format(
            ConsoleMessageColor.GREEN
        )
    )
    # getting current time
    current_time = datetime.datetime.now(pytz.utc)
    # getting all paused assigned tests
    locked_assigned_tests = AssignedTest.objects.filter(
        status=AssignedTestStatus.LOCKED
    )
    # looping in locked_assigned_test
    for locked_assigned_test in locked_assigned_tests:
        # initializing total_test_time (in minutes)
        total_test_time = 0
        # getting assigned test sections based on assigned_test_id of current locked assigned test
        assigned_test_sections = AssignedTestSection.objects.filter(
            assigned_test_id=locked_assigned_test.id
        )
        # looping in assigned_test_sections
        for assigned_test_section in assigned_test_sections:
            # test_section_time is defined
            if assigned_test_section.test_section_time is not None:
                # adding test_section_time to total_test_time
                total_test_time += assigned_test_section.test_section_time
        # modify_date of current locked assigned test + total_test_time + 30 days
        calculated_time = (
            locked_assigned_test.modify_date
            + timedelta(minutes=total_test_time)
            + timedelta(days=30)
        )
        # if current_time > calculated_time
        if current_time > calculated_time:
            # update test status to Quit and set is_invalid flag to true
            locked_assigned_test.status = AssignedTestStatus.QUIT
            locked_assigned_test.is_invalid = True
            locked_assigned_test.save()
            # printing successful messasge
            print(
                "{0} ==================== ASSIGNED_TEST_ID: {1} UPDATED SUCCESSFULLY TO STATUS QUIT WITH IS_INVALID FLAG SET TO TRUE ====================".format(
                    ConsoleMessageColor.CYAN, locked_assigned_test.id
                )
            )


# this function is unassigning assigned tests that are stuck/left in READY or PRE-TEST state
def unassign_ready_and_pre_test_assigned_tests():
    # imports need to be here to avoid errors/exceptions when starting the containers
    from backend.static.assigned_test_status import AssignedTestStatus
    from backend.custom_models.assigned_test import AssignedTest

    # printing celery task name
    print(
        "{0} ==================== CELERY TASK EXECUTION: UNASSIGN READY AND PRE-TEST ASSIGNED TESTS ====================".format(
            ConsoleMessageColor.GREEN
        )
    )
    # getting current time
    current_time = datetime.datetime.now(pytz.utc)
    # getting all ready/pre test assigned tests (supervised tests only)
    ready_and_pre_test_assigned_tests = AssignedTest.objects.filter(
        status__in=(AssignedTestStatus.READY, AssignedTestStatus.PRE_TEST),
        uit_invite_id=None,
    )
    # looping in ready_and_pre_test_assigned_tests
    for ready_and_pre_test_assigned_test in ready_and_pre_test_assigned_tests:
        # modify_date of current ready or pre-test assigned test + 1 hour
        calculated_time = ready_and_pre_test_assigned_test.modify_date + timedelta(
            hours=1
        )
        # if current_time > calculated_time
        if current_time > calculated_time:
            # update test status to UNASSIGNED
            ready_and_pre_test_assigned_test.status = AssignedTestStatus.UNASSIGNED
            ready_and_pre_test_assigned_test.save()
            # printing successful messasge
            print(
                "{0} ==================== ASSIGNED_TEST_ID: {1} UPDATED SUCCESSFULLY TO STATUS UNASSIGNED ====================".format(
                    ConsoleMessageColor.CYAN, ready_and_pre_test_assigned_test.id
                )
            )
