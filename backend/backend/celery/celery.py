from __future__ import absolute_import, unicode_literals
import os

from celery import Celery
from config.environment import SETTINGS_MODULE

# set the default Django settings module for the 'celery' program.
os.environ.setdefault("DJANGO_SETTINGS_MODULE", SETTINGS_MODULE)
# set the celery app
app = Celery("backend.celery")
# set the needed configurations
app.config_from_object("django.conf:settings", namespace="CELERY")
# Load task modules from all registered Django app configs.
app.autodiscover_tasks()
# printing the broker url
print("Celery Broker URL: ", app.conf.broker_url)
