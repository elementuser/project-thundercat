from django.db import models
from simple_history.models import HistoricalRecords


# ** IMPORTANT **
# If any changes are made to this model, please make sure to notify CAMM team (CAT/CAMM Relation Logic)
class CatRefClassificationVW(models.Model):
    classif_id = models.IntegerField(primary_key=True, db_column="CLASSIF_ID")
    active_flg = models.CharField(db_column="ACTIVE_FLG", max_length=1)
    class_grp_cd = models.CharField(db_column="CLASS_GRP_CD", max_length=2)
    class_sbgrp_cd = models.CharField(db_column="CLASS_SBGRP_CD", max_length=3, null=True, blank=True)
    class_lvl_cd = models.CharField(db_column="CLASS_LVL_CD", max_length=2)
    efdt = models.DateTimeField(db_column="EFDT")
    xdt = models.DateTimeField(db_column="XDT", null=True, blank=True)
    psc_class_ind = models.IntegerField(db_column="PSC_CLASS_IND")
    dflt_bud_cd = models.CharField(db_column="DFLT_BUD_CD", null=True, blank=True, max_length=5)
    dflt_pay_zn_cd = models.CharField(db_column="DFLT_PAY_ZN_CD", null=True, blank=True, max_length=3)
    eqlzn_amt = models.IntegerField(db_column="EQLZN_AMT", null=True, blank=True)
    eqlzn_amt_efdt = models.DateTimeField(db_column="EQLZN_AMT_EFDT", null=True, blank=True)
    eqlzn_amt_xdt = models.DateTimeField(db_column="EQLZN_AMT_XDT", null=True, blank=True)
    ofcr_lvl_ind = models.IntegerField(db_column="OFCR_LVL_IND")
    
    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    class Meta:
        managed = False
        db_table = "CAT_REF_CLASSIFICATION_VW"
