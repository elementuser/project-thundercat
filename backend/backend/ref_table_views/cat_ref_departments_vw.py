from django.db import models
from simple_history.models import HistoricalRecords


# ** IMPORTANT **
# If any changes are made to this model, please make sure to notify CAMM team (CAT/CAMM Relation Logic)
class CatRefDepartmentsVW(models.Model):
    dept_id = models.IntegerField(primary_key=True, db_column="DEPT_ID")
    active_flg = models.CharField(db_column="ACTIVE_FLG", max_length=1)
    eabrv = models.CharField(db_column="EABRV", max_length=20, null=True, blank=True)
    fabrv = models.CharField(db_column="FABRV", max_length=20, null=True, blank=True)
    legacy_cd = models.CharField(db_column="LEGACY_CD", null=True, blank=True, max_length=3)
    edesc = models.CharField(db_column="EDESC", max_length=200)
    fdesc = models.CharField(db_column="FDESC", max_length=200)
    efdt = models.DateTimeField(db_column="EFDT")
    xdt = models.DateTimeField(db_column="XDT", blank=True, null=True)
    esearch_desc = models.CharField(db_column="ESEARCH_DESC", max_length=200, blank=True, null=True)
    fsearch_desc = models.CharField(db_column="FSEARCH_DESC", max_length=200, blank=True, null=True)
    rcver_gnrl_nbr = models.IntegerField(db_column="RCVER_GNRL_NBR", blank=True, null=True)
    psea_id = models.IntegerField(db_column="PSEA_ID")
    pssa_id = models.IntegerField(db_column="PSSA_ID", blank=True, null=True)
    pssra_id = models.IntegerField(db_column="PSSRA_ID", blank=True, null=True)
    ola_id = models.IntegerField(db_column="OLA_ID", blank=True, null=True)
    org_typ_id = models.IntegerField(db_column="ORG_TYP_ID", blank=True, null=True)
    org_empl_id = models.IntegerField(db_column="ORG_EMPL_ID", blank=True, null=True)
    

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    class Meta:
        managed = False
        db_table = "CAT_REF_DEPARTMENTS_VW"
