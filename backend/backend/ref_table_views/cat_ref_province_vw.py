from django.db import models
from simple_history.models import HistoricalRecords


# ** IMPORTANT **
# If any changes are made to this model, please make sure to notify CAMM team (CAT/CAMM Relation Logic)
class CatRefProvinceVW(models.Model):
    prov_id = models.IntegerField(primary_key=True, db_column="PROV_ID")
    active_flg = models.CharField(db_column="ACTIVE_FLG", max_length=1)
    eabrv = models.CharField(db_column="EABRV", max_length=10, null=True, blank=True)
    fabrv = models.CharField(db_column="FABRV", max_length=10, null=True, blank=True)
    legacy_cd = models.CharField(
        db_column="LEGACY_CD", max_length=3, null=True, blank=True
    )
    edesc = models.CharField(db_column="EDESC", max_length=200)
    fdesc = models.CharField(db_column="FDESC", max_length=200)
    efdt = models.DateTimeField(db_column="EFDT")
    xdt = models.DateTimeField(db_column="XDT", blank=True, null=True)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    class Meta:
        managed = False
        db_table = "CAT_REF_PROVINCE_VW"
