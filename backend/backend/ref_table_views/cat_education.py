from django.db import models
from simple_history.models import HistoricalRecords


# ** IMPORTANT **
# If any changes are made to this model, please make sure to notify CAMM team (CAT/CAMM Relation Logic)
class CatEducation(models.Model):
    education_id = models.IntegerField(primary_key=True)
    active_flg = models.CharField(max_length=1)
    edesc = models.CharField(max_length=200)
    fdesc = models.CharField(max_length=200)
    efdt = models.DateTimeField()
    xdt = models.DateTimeField(blank=True, null=True)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()
