# Generated by Django 3.2.18 on 2023-07-24 11:41

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ref_table_views', '0024_create_cat_ref_visible_minority_vw_data'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='catrefaboriginalvw',
            options={'managed': False},
        ),
        migrations.AlterModelOptions(
            name='catrefclassificationvw',
            options={'managed': False},
        ),
        migrations.AlterModelOptions(
            name='catrefdepartmentsvw',
            options={'managed': False},
        ),
        migrations.AlterModelOptions(
            name='catrefdisabilityvw',
            options={'managed': False},
        ),
        migrations.AlterModelOptions(
            name='catrefemployerstsvw',
            options={'managed': False},
        ),
        migrations.AlterModelOptions(
            name='catrefgendervw',
            options={'managed': False},
        ),
        migrations.AlterModelOptions(
            name='catrefprovincevw',
            options={'managed': False},
        ),
        migrations.AlterModelOptions(
            name='catrefvisibleminorityvw',
            options={'managed': False},
        ),
    ]
