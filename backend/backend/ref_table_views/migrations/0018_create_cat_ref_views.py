from django.db import connection
from django.db import migrations
from django.conf import settings


def create_vw(apps, schema_editor):
    # Execute only in environments
    if settings.IS_LOCAL:
        return

    with connection.cursor() as cursor:
        cursor.execute(
            """
                CREATE OR ALTER VIEW [dbo].[CAT_REF_ABORIGINAL_VW]
                    AS
                        SELECT A.* FROM [REF].[dbo].[ABORIGINAL_CODES]  AS A
                        INNER JOIN [REF].[dbo].APPL_ABRG_CDS  AS B ON A.ABRG_ID = B.ABRG_ID
                        WHERE B.[APPL_ACRONYM] = 'CAT'
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )

        cursor.execute(
            """
                CREATE OR ALTER VIEW [dbo].[CAT_REF_CLASSIFICATION_VW]
                    AS
                        SELECT A.* FROM [REF].[dbo].CLASSIFICATION_CODES  AS A
                        INNER JOIN [REF].[dbo].APPL_CLASSIF_CDS  AS B ON A.CLASSIF_ID = B.CLASSIF_ID
                        WHERE B.[APPL_ACRONYM] = 'CAT'
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )

        cursor.execute(
            """
                CREATE OR ALTER VIEW [dbo].[CAT_REF_DEPARTMENTS_VW]
                    AS
                        SELECT A.* FROM [REF].[dbo].[DEPARTMENT_CODES] AS A
                        INNER JOIN [REF].[dbo].APPL_DEPT_CDS AS B ON A.DEPT_ID = B.DEPT_ID
                        WHERE B.[APPL_ACRONYM] = 'CAT'
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )

        cursor.execute(
            """
                CREATE OR ALTER VIEW [dbo].[CAT_REF_DISABILITY_VW]
                    AS
                        SELECT A.* FROM [REF].[dbo].[DISABILITY_CODES] AS A
                        INNER JOIN [REF].[dbo].APPL_DIST_CDS AS B ON A.DSBL_ID = B.DIST_ID
                        WHERE B.[APPL_ACRONYM] = 'CAT'
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )

        cursor.execute(
            """
                CREATE OR ALTER VIEW [dbo].[CAT_REF_EMPLOYER_STS_VW]
                    AS
                        SELECT A.* FROM REF.[dbo].EMPLOYMENT_STATUS_CODES AS A
                        INNER JOIN [REF].[dbo].APPL_EMPSTS_CDS AS B ON A.EMPSTS_ID = B.EMPSTS_ID
                        WHERE B.[APPL_ACRONYM] = 'CAT'
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )

        cursor.execute(
            """
                CREATE OR ALTER VIEW [dbo].[CAT_REF_GENDER_VW]
                    AS
                        SELECT A.* FROM REF.[dbo].GENDER_CODES AS A
                        INNER JOIN [REF].[dbo].APPL_GENDER_CDS AS B ON A.GND_ID = B.GND_ID
                        WHERE B.[APPL_ACRONYM] = 'CAT'
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )

        cursor.execute(
            """
                CREATE OR ALTER VIEW [dbo].[CAT_REF_PROVINCE_VW]
                    AS
                        SELECT A.* FROM REF.[dbo].PROVINCE_CODES AS A
                        INNER JOIN [REF].[dbo].APPL_PROV_CDS AS B ON A.PROV_ID = B.PROV_ID
                        WHERE B.[APPL_ACRONYM] = 'CAT'
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )

        cursor.execute(
            """
                CREATE OR ALTER VIEW [dbo].[CAT_REF_VISIBLE_MINORITY_VW]
                    AS
                        SELECT A.* FROM REF.[dbo].VISIBLE_MINORITY_CODES AS A
                        INNER JOIN [REF].[dbo].APPL_VIS_MNRT_CDS AS B ON A.VISMIN_ID = B.VISMIN_ID
                        WHERE B.[APPL_ACRONYM] = 'CAT'
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    # Execute only in environments
    if settings.IS_LOCAL:
        return

    with connection.cursor() as cursor:
        cursor.execute("DROP VIEW IF EXISTS [dbo].[CAT_REF_VISIBLE_MINORITY_VW]")
        cursor.execute("DROP VIEW IF EXISTS [dbo].[CAT_REF_PROVINCE_VW]")
        cursor.execute("DROP VIEW IF EXISTS [dbo].[CAT_REF_GENDER_VW]")
        cursor.execute("DROP VIEW IF EXISTS [dbo].[CAT_REF_EMPLOYER_STS_VW]")
        cursor.execute("DROP VIEW IF EXISTS [dbo].[CAT_REF_DISABILITY_VW]")
        cursor.execute("DROP VIEW IF EXISTS [dbo].[CAT_REF_DEPARTMENTS_VW]")
        cursor.execute("DROP VIEW IF EXISTS [dbo].[CAT_REF_CLASSIFICATION_VW]")
        cursor.execute("DROP VIEW IF EXISTS [dbo].[CAT_REF_ABORIGINAL_VW]")


class Migration(migrations.Migration):
    dependencies = [
        (
            "ref_table_views",
            "0017_creating_more_data_in_oltf_department_vw",
        ),
    ]
    operations = [migrations.RunPython(create_vw, rollback_changes)]
