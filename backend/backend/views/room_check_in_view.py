from rest_framework.views import APIView
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework import status
from backend.views.room_check_in import check_into_room
from backend.views.utils import get_user_info_from_jwt_token
from cms.views.utils import get_needed_parameters


class RoomCheckIn(APIView):
    def post(self, request):
        user_info = get_user_info_from_jwt_token(request)

        success, parameters = get_needed_parameters(["test_access_code"], request)
        if not success:
            return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

        return check_into_room(parameters["test_access_code"], user_info["username"])

    def get_permissions(self):
        return [permissions.IsAuthenticated()]
