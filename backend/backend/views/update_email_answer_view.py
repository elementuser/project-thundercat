from rest_framework.views import APIView
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework import status
from backend.views.utils import is_undefined, get_user_info_from_jwt_token
from backend.views.update_email_answer import email_answer_controller
from backend.api_permissions.assigned_test_permissions import (
    HasAssignedTestFromAssignedTestPermission,
)


class UpdateEmailAnswer(APIView):
    def post(self, request):
        user_info = get_user_info_from_jwt_token(request)

        needed_parameters = [
            "assignedTestId",
            "questionId",
            "questionId",
            "answerId",
            "OPERATION",
            "answerObj",
        ]
        parameters = {}
        for parameter in needed_parameters:
            parameter_name = "{}".format(parameter)
            parameter = request.data.get(parameter_name, None)
            if is_undefined(parameter):
                return Response(
                    {"error": "no '{}' parameter".format(parameter_name)},
                    status=status.HTTP_400_BAD_REQUEST,
                )
            if parameter_name == "OPERATION":
                parameters[parameter_name] = int(parameter)
            else:
                parameters[parameter_name] = parameter

        return email_answer_controller(user_info, parameters)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasAssignedTestFromAssignedTestPermission(),
        ]
