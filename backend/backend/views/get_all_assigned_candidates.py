from django.db.models import Q
from rest_framework.response import Response
from rest_framework import status
from operator import and_, itemgetter
from cms.views.utils import get_needed_parameters
from user_management.views.utils import CustomPagination
from functools import reduce
from django.db.models import Q
from backend.serializers.etta_actions_serializer import EttaActionSerializer
from backend.custom_models.etta_actions import EttaActions
from db_views.db_view_models.active_tests_vw import ActiveTestsVW
from db_views.serializers.active_tests_serializers import ActiveTestsViewSerializer
from urllib import parse


# getting all active tests
def get_all_active_tests(request):
    success, parameters = get_needed_parameters(["page", "page_size"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    page, page_size = itemgetter("page", "page_size")(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # getting all active tests
    active_candidates = ActiveTestsViewSerializer(
        ActiveTestsVW.objects.all(), many=True
    ).data

    # getting sorted serialized data by last_name (ascending)
    ordered_assigned_candidate = sorted(
        active_candidates,
        key=lambda k: k["candidate_last_name"].lower(),
        reverse=False,
    )
    # only getting data for current selected page
    new_active_candidates = ordered_assigned_candidate[
        (current_page - 1) * page_size : current_page * page_size
    ]
    # retuning final data using pagination library
    return paginator.get_paginated_response(
        new_active_candidates, len(active_candidates), current_page, page_size
    )


def get_found_active_tests(request):
    success, parameters = get_needed_parameters(
        ["keyword", "current_language", "page", "page_size"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    page, page_size, current_language, keyword = itemgetter(
        "page", "page_size", "current_language", "keyword"
    )(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # if blank search
    if keyword == " ":
        found_active_tests = ActiveTestsVW.objects.all()
    # regular search
    else:
        # decode keyword to get special characters
        keyword = parse.unquote(keyword)

        # since there is a comma between last name and first name in active tests table, if the user put
        # a comma in one of the search keyword words, remove it
        keyword_without_comma = keyword.replace(",", "")
        # splitting keyword string
        split_keyword = keyword_without_comma.split()

        # if keyword contains more than one word
        if len(split_keyword) > 1:
            found_active_tests = ActiveTestsVW.objects.filter(
                reduce(
                    and_,
                    [
                        Q(test_code__icontains=splitted_keyword)
                        | Q(candidate_first_name__icontains=splitted_keyword)
                        | Q(candidate_last_name__icontains=splitted_keyword)
                        | Q(candidate_email__icontains=splitted_keyword)
                        | Q(ta_first_name__icontains=splitted_keyword)
                        | Q(ta_last_name__icontains=splitted_keyword)
                        | Q(test_order_number__icontains=splitted_keyword)
                        | Q(reference_number__icontains=splitted_keyword)
                        for splitted_keyword in split_keyword
                    ],
                )
            )
        else:
            found_active_tests = ActiveTestsVW.objects.filter(
                Q(test_code__icontains=keyword)
                | Q(candidate_first_name__icontains=keyword)
                | Q(candidate_last_name__icontains=keyword)
                | Q(candidate_email__icontains=keyword)
                | Q(ta_first_name__icontains=keyword)
                | Q(ta_last_name__icontains=keyword)
                | Q(test_order_number__icontains=keyword)
                | Q(reference_number__icontains=keyword)
            )

        # no results found
        if not found_active_tests:
            return Response({"no results found"}, status=status.HTTP_200_OK)

    # getting serialized active tests data
    serialized_found_active_tests = ActiveTestsViewSerializer(
        found_active_tests, many=True
    ).data
    # getting sorted serialized data by last_name (ascending)
    ordered_found_active_candidates = sorted(
        serialized_found_active_tests,
        key=lambda k: k["candidate_last_name"].lower(),
        reverse=False,
    )

    # only getting data for current selected page
    new_found_active_candidates = ordered_found_active_candidates[
        (current_page - 1) * page_size : current_page * page_size
    ]

    # retuning final data using pagination library
    return paginator.get_paginated_response(
        new_found_active_candidates,
        len(found_active_tests),
        current_page,
        page_size,
    )


def get_reason_for_invalidating_the_test(request):
    assigned_test_id = request.query_params.get("assigned_test_id", None)

    if assigned_test_id:
        assigned_test = EttaActions.objects.filter(assigned_test_id=assigned_test_id)

    SerializedData = EttaActionSerializer(assigned_test, many=True)
    return Response(SerializedData.data)
