from datetime import datetime
import pytz
import urllib.parse
from dateutil.relativedelta import relativedelta
from rest_framework.response import Response
from backend.views.utils import get_user_info_from_jwt_token
from backend.static.assigned_test_status import AssignedTestStatus
from backend.custom_models.assigned_test import AssignedTest
from backend.custom_models.uit_invites import UITInvites
from backend.serializers.assigned_test_serializer import (
    SelectedUserAllTestsSerializer,
    AssignedTestSimplifiedSerializer,
)


# retrieving TA assigned test order numbers
def retrieve_scored_tests(request):
    user_info = get_user_info_from_jwt_token(request)
    # getting assigned scored tests (where status is TIMED_OUT, QUIT or SUBMITTED)
    # TODO: this will need to be updated when we'll have more test types,
    # since a submitted tests does not necessary mean that the test is scored

    # getting scored tests
    scored_tests = AssignedTest.objects.filter(
        username_id=user_info["username"],
        status__in=(AssignedTestStatus.QUIT, AssignedTestStatus.SUBMITTED),
    ).order_by("-start_date")

    # getting related scored tests uit invite ID (where it's defined)
    scored_tests_uit_invite_ids = (
        AssignedTest.objects.filter(
            username_id=user_info["username"],
            status__in=(AssignedTestStatus.QUIT, AssignedTestStatus.SUBMITTED),
            uit_invite_id__isnull=False,
        )
        .order_by("-start_date")
        .values_list("uit_invite_id", flat=True)
    )

    # getting realted uit invite data based on provided uit invite IDs
    uit_invite_data = UITInvites.objects.filter(id__in=scored_tests_uit_invite_ids)

    # getting needed time variables
    current_datetime = datetime.today()
    two_days_ago = current_datetime - relativedelta(days=2)
    two_days_ago_utc = two_days_ago.astimezone(pytz.utc)

    # initializing the result_set
    result_set = []

    for scored_test in scored_tests:
        # scored_test is a supervised test
        # also include supervised tests with null submit_date if status == QUIT
        if scored_test.uit_invite_id is None:
            # legacy data may have status == QUIT with submit_date == NULL
            # (since legacy implies older than 48 hours)
            if scored_test.submit_date is None:
                result_set.append(scored_test)
            # only include if submit date is 2+ days ago
            elif scored_test.submit_date <= two_days_ago_utc:
                result_set.append(scored_test)
        # scored_test is unsupervised, include test if UIT invite expired 2+ days ago
        else:
            # getting index of uit invite for the current iteration
            index_of_related_uit_invite = [x.id for x in uit_invite_data].index(
                scored_test.uit_invite_id
            )
            uit_invite_validity_end_date = uit_invite_data[
                index_of_related_uit_invite
            ].validity_end_date
            # less than (not less than or equal) because the validity_end_date is not a timestamp (basically want to see those tests "3" days after)
            if uit_invite_validity_end_date < two_days_ago_utc.date():
                result_set.append(scored_test)

    # serializing the data
    serialized_test_data = AssignedTestSimplifiedSerializer(result_set, many=True).data

    # returning data
    return Response(serialized_test_data)


# retrieving All Tests for a selected user
def retrieve_All_tests(request):
    username = request.query_params.get("username", None)

    # username is defined
    if username is not None:
        # decoding username that might contain special characters
        username = urllib.parse.unquote(username)

    all_tests = AssignedTest.objects.filter(username_id=username)

    # returning data
    data = SelectedUserAllTestsSerializer(all_tests, many=True)
    return Response(data.data)
