from operator import itemgetter
from urllib import parse
from django.db.models import Q
from rest_framework.response import Response
from rest_framework import status
from user_management.views.utils import CustomPagination
from cms.views.utils import get_needed_parameters
from db_views.db_view_models.user_look_up_vw import UserLookUpVW
from db_views.serializers.user_look_up_serializers import UserLookUpViewSerializer


# retrieving all users
def retrieve_all_users(request):
    success, parameters = get_needed_parameters(["page", "page_size"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    page, page_size = itemgetter("page", "page_size")(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # getting all users
    all_users = UserLookUpVW.objects.all()

    # getting serialized all users data
    serialized_all_users_data = UserLookUpViewSerializer(all_users, many=True).data

    # getting sorted serialized data by last_name (ascending)
    ordered_all_users = sorted(
        serialized_all_users_data,
        key=lambda k: k["last_name"].lower(),
        reverse=False,
    )

    # only getting data for current selected page
    new_all_users = ordered_all_users[
        (current_page - 1) * page_size : current_page * page_size
    ]

    # retuning final data using pagination library
    return paginator.get_paginated_response(
        new_all_users, len(all_users), current_page, page_size
    )


# retrieving all found users
def retrieve_all_found_users(request):
    success, parameters = get_needed_parameters(
        ["keyword", "current_language", "page", "page_size"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    page, page_size, current_language, keyword = itemgetter(
        "page", "page_size", "current_language", "keyword"
    )(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # if blank search
    if keyword == " ":
        found_users = UserLookUpVW.objects.all()
    # regular search
    else:
        # decode keyword to get special characters
        keyword = parse.unquote(keyword)

        found_users = UserLookUpVW.objects.filter(
            Q(email__icontains=keyword)
            | Q(first_name__icontains=keyword)
            | Q(last_name__icontains=keyword)
            | Q(date_of_birth__icontains=keyword)
        )

        # no results found
        if not found_users:
            return Response({"no results found"}, status=status.HTTP_200_OK)

    # getting serialized users data
    serialized_found_users = UserLookUpViewSerializer(found_users, many=True).data

    # getting sorted serialized data by last_name (ascending)
    ordered_found_users = sorted(
        serialized_found_users,
        key=lambda k: k["last_name"].lower(),
        reverse=False,
    )

    # only getting data for current selected page
    new_found_users = ordered_found_users[
        (current_page - 1) * page_size : current_page * page_size
    ]

    # retuning final data using pagination library
    return paginator.get_paginated_response(
        new_found_users, len(found_users), current_page, page_size
    )
