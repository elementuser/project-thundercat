from operator import or_
from functools import reduce
from django.db.models import Q
from rest_framework.response import Response
from rest_framework import status
from backend.static.assigned_test_status import AssignedTestStatus
from backend.custom_models.candidate_answers import CandidateAnswers
from backend.custom_models.candidate_multiple_choice_answers import (
    CandidateMultipleChoiceAnswers,
)
from backend.custom_models.test_scorer_assignment import TestScorerAssignment
from backend.custom_models.assigned_test import AssignedTest
from backend.custom_models.assigned_test_section import AssignedTestSection
from backend.static.test_section_scoring_type import TestSectionScoringType
from backend.views.utils import handleViewedQuestionsLogic
from cms.cms_models.new_question import NewQuestion
from cms.cms_models.answer import Answer
from cms.cms_models.test_section_component import TestSectionComponent
from cms.cms_models.test_section import TestSection
from cms.cms_models.scoring_method_types import ScoringMethodTypes
from cms.cms_models.scoring_methods import ScoringMethods
from cms.cms_models.scoring_threshold import ScoringThreshold
from cms.cms_models.scoring_pass_fail import ScoringPassFail
from cms.static.test_section_component_type import TestSectionComponentType
from cms.views.utils import SCORING_METHOD_TYPE, CONVERTED_SCORE
from cms.cms_models.items import Items
from cms.cms_models.item_development_statuses import ItemDevelopmentStatuses
from cms.static.items_utils import ItemDevelopmentStatus
from cms.cms_models.item_option import ItemOption


class Action:
    TEST_SECTION = "TEST_SECTION"
    TEST = "TEST"


def get_converted_score(total_score, test_id, language):
    try:
        # getting scoring method based on provided test ID
        scoring_method = ScoringMethods.objects.get(test_id=test_id)
        # getting scoring method type based on provided scoring method
        scoring_method_type = ScoringMethodTypes.objects.get(
            id=scoring_method.method_type_id
        ).method_type_codename

        # method type is THRESHOLD
        if scoring_method_type == SCORING_METHOD_TYPE.THRESHOLD:
            # getting scoring threshold items based on provided scoring method
            scoring_threshold_items = ScoringThreshold.objects.filter(
                scoring_method_id=scoring_method.id
            )
            # looping in scoring_threshold_items
            for scoring_threshold_item in scoring_threshold_items:
                # if total score is between minimum and maximum score of the current threshold item
                if (
                    total_score >= scoring_threshold_item.minimum_score
                    and total_score <= scoring_threshold_item.maximum_score
                ):
                    if language == "en":
                        # return conversion value
                        return scoring_threshold_item.en_conversion_value
                    else:
                        # return conversion value
                        return scoring_threshold_item.fr_conversion_value
            # total score is not part of the threshold items (if this error happens, that means there is probably an error in the scoring threshold collection definition)
            return CONVERTED_SCORE.INVALID_CONVERSION

        # method type is PASS_FAIL
        elif scoring_method_type == SCORING_METHOD_TYPE.PASS_FAIL:
            scoring_pass_fail_minimum_score = ScoringPassFail.objects.get(
                scoring_method_id=scoring_method.id
            ).minimum_score
            if total_score >= scoring_pass_fail_minimum_score:
                return CONVERTED_SCORE.PASS
            else:
                return CONVERTED_SCORE.FAIL

        # method type is NONE
        elif scoring_method_type == SCORING_METHOD_TYPE.NONE:
            return CONVERTED_SCORE.NONE

        # TODO (fnormand): implement PERCENTAGE scoring type logic

        # method type is other
        else:
            return str(total_score)
    # if no scoring method is not find (should never happen)
    except ScoringMethods.DoesNotExist:
        # return invalid conversion
        return CONVERTED_SCORE.INVALID_CONVERSION


def uit_scoring(parameters, action):
    try:
        # Score Test Section
        if action == Action.TEST_SECTION:
            try:
                # getting test section scoring_type
                scoring_type = TestSection.objects.get(
                    id=parameters["test_section_id"]
                ).scoring_type

            except TestSection.DoesNotExist:
                return Response(
                    {"error": "the specified test section does not exist"},
                    status=status.HTTP_400_BAD_REQUEST,
                )

            # test section scoring_type
            if scoring_type == TestSectionScoringType.AUTO_SCORE:
                # getting test section component id based on provided test_section_id
                test_section_component_data = TestSectionComponent.objects.get(
                    test_section=parameters["test_section_id"],
                    component_type__in=[
                        TestSectionComponentType.QUESTION_LIST,
                        TestSectionComponentType.ITEM_BANK,
                    ],
                )

                # getting candidate answers objects for specified assigned test
                candidate_answers = CandidateAnswers.objects.filter(
                    assigned_test_id=parameters["assigned_test_id"],
                    test_section_component_id=test_section_component_data.id,
                )

                # candidate answers contains at least one object
                if candidate_answers:
                    # getting questions answered
                    questions_answered = CandidateMultipleChoiceAnswers.objects.filter(
                        reduce(
                            or_,
                            [
                                Q(candidate_answers_id=candidate_answer.id)
                                for candidate_answer in candidate_answers
                            ],
                        )
                    ).values("candidate_answers_id", "answer_id", "item_answer_id")

                    # initializing score
                    score = 0

                    # questions coming from test builder
                    if (
                        test_section_component_data.component_type
                        == TestSectionComponentType.QUESTION_LIST
                    ):
                        for question_answered in questions_answered:
                            # making sure that the current question has an answer (not null)
                            if question_answered["answer_id"] is not None:
                                # making sure that the current question is not a pilot question and that it is part of the current test section
                                is_pilot_question = NewQuestion.objects.get(
                                    id=CandidateAnswers.objects.get(
                                        id=question_answered["candidate_answers_id"]
                                    ).question_id
                                ).pilot

                                # current question is not a pilot question
                                if not is_pilot_question:
                                    # getting current question's scoring value
                                    scoring_value = Answer.objects.get(
                                        id=question_answered["answer_id"]
                                    ).scoring_value
                                    # adding current scoring value to score
                                    score += scoring_value
                    # questions coming from item bank
                    elif (
                        test_section_component_data.component_type
                        == TestSectionComponentType.ITEM_BANK
                    ):
                        for question_answered in questions_answered:
                            # making sure that the current question has an answer (not null)
                            if question_answered["item_answer_id"] is not None:
                                # making sure that the current question is a regular/operational question and that it is part of the current test section
                                regular_development_status_id = (
                                    ItemDevelopmentStatuses.objects.get(
                                        codename=ItemDevelopmentStatus.REGULAR
                                    ).id
                                )
                                item_development_status_id = Items.objects.get(
                                    id=CandidateAnswers.objects.get(
                                        id=question_answered["candidate_answers_id"]
                                    ).item_id
                                ).development_status_id

                                # current question is regular/operational
                                if (
                                    item_development_status_id
                                    == regular_development_status_id
                                ):
                                    # getting current question's scoring value
                                    scoring_value = ItemOption.objects.get(
                                        id=question_answered["item_answer_id"]
                                    ).score
                                    # adding current scoring value to score
                                    score += int(scoring_value)

                    # unsupported test section component type (should never happen)
                    else:
                        return Response(
                            {"error": "unsupported test section component type"},
                            status=status.HTTP_400_BAD_REQUEST,
                        )

                    try:
                        # saving score in assigned test section table
                        assigned_test_section = AssignedTestSection.objects.get(
                            assigned_test_id=parameters["assigned_test_id"],
                            test_section_id=parameters["test_section_id"],
                        )
                        assigned_test_section.score = score
                        assigned_test_section.save()

                    except AssignedTestSection.DoesNotExist:
                        return Response(
                            {
                                "error": "the specified assigned test section does not exist"
                            },
                            status=status.HTTP_400_BAD_REQUEST,
                        )

                    # returning calculated score
                    # temp json response
                    return Response({"status": status.HTTP_200_OK, "score": score})

                # no question
                else:
                    return Response(
                        {
                            "error": "there are no questions answered based on provided parameters"
                        },
                        status=status.HTTP_400_BAD_REQUEST,
                    )

            # test section not auto-scored
            else:
                # returning calculated score
                # temp json response
                return Response(
                    {
                        "status": status.HTTP_200_OK,
                        "info": "this test section is not auto-scored",
                    }
                )

        # Score Test
        elif action == Action.TEST:
            # initializing total score
            total_score = 0

            # getting all assigned test sections of current assigned test
            assigned_test_sections = AssignedTestSection.objects.filter(
                assigned_test_id=parameters["assigned_test_id"]
            )

            # flag if this test will need a scorer or not
            requires_scorer = False

            # looping in assigned test sections
            for test_section in assigned_test_sections:
                # Check if the section is manually scored
                if (
                    test_section.test_section.scoring_type
                    == TestSectionScoringType.COMPETENCY
                ):
                    requires_scorer = True
                # making sure that the score is not null (will be null if the test section is not auto-scored)
                if test_section.score is not None:
                    # adding current test section score to total score
                    total_score += test_section.score

            # if it requires a scorer, then add it to the unassigned tests
            if requires_scorer:
                # ensure it is not already in the scorer assignment test table
                if not TestScorerAssignment.objects.filter(
                    assigned_test_id=parameters["assigned_test_id"]
                ):
                    # create create a test that is not assigned to any scorer
                    assignment = TestScorerAssignment(
                        assigned_test_id=parameters["assigned_test_id"]
                    )
                    assignment.save()

            try:
                # saving total score in uit assigned test table
                assigned_test = AssignedTest.objects.get(
                    id=parameters["assigned_test_id"]
                )
                assigned_test.total_score = total_score
                assigned_test.en_converted_score = get_converted_score(
                    total_score, assigned_test.test_id, "en"
                )
                assigned_test.fr_converted_score = get_converted_score(
                    total_score, assigned_test.test_id, "fr"
                )
                assigned_test.status = AssignedTestStatus.SUBMITTED
                assigned_test.save()
                # handling viewed questions logic
                handleViewedQuestionsLogic(assigned_test.id)

            except AssignedTest.DoesNotExist:
                return Response(
                    {"error": "the specified assigned test does not exist"},
                    status=status.HTTP_400_BAD_REQUEST,
                )

            # returning calculated total score
            # temp json response
            return Response({"status": status.HTTP_200_OK, "total_score": total_score})

        # Action does not exist
        else:
            return Response(
                {"error": "{} action does not exist".format(action)},
                status=status.HTTP_400_BAD_REQUEST,
            )

    except ValueError:
        return Response(
            {"error": "the specified parameters are invalid"},
            status=status.HTTP_400_BAD_REQUEST,
        )
