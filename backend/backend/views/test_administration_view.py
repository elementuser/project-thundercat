from rest_framework.views import APIView
from rest_framework import permissions
from backend.views.get_ta_assigned_candidates import get_ta_assigned_candidates
from backend.views.test_administrator_actions import (
    test_administrator_actions,
    TaActionsConstants,
    lock_unlock_all_candidates_test,
)
from backend.api_permissions.role_based_api_permissions import (
    HasTestAdminPermission,
    HasAaePermission,
)


class GetTestAdministratorAssignedCandidates(APIView):
    def get(self, request):
        return get_ta_assigned_candidates(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestAdminPermission()]


class UpdateTestTime(APIView):
    def post(self, request):
        needed_parameters = [
            "assigned_test_section_id",
            "test_time",
            "first_iteration",
            "last_iteration",
        ]
        optional_parameters = []
        return test_administrator_actions(
            request,
            needed_parameters,
            optional_parameters,
            TaActionsConstants.UPDATE_TIME,
        )

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasAaePermission()]


class UpdateBreakBank(APIView):
    def post(self, request):
        needed_parameters = ["assigned_test_id", "break_bank_time"]
        optional_parameters = []
        return test_administrator_actions(
            request,
            needed_parameters,
            optional_parameters,
            TaActionsConstants.UPDATE_BREAK_BANK,
        )

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasAaePermission()]


class UpdateBreakBank(APIView):
    def post(self, request):
        needed_parameters = ["assigned_test_id", "break_bank_time"]
        optional_parameters = []
        return test_administrator_actions(
            request,
            needed_parameters,
            optional_parameters,
            TaActionsConstants.UPDATE_BREAK_BANK,
        )

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestAdminPermission()]


class ApproveCandidate(APIView):
    def post(self, request):
        needed_parameters = ["username_id", "test_id"]
        optional_parameters = []
        return test_administrator_actions(
            request, needed_parameters, optional_parameters, TaActionsConstants.APPROVE
        )

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestAdminPermission()]


class LockCandidateTest(APIView):
    def post(self, request):
        needed_parameters = ["username_id", "test_id"]
        optional_parameters = ["test_section_id"]
        return test_administrator_actions(
            request, needed_parameters, optional_parameters, TaActionsConstants.LOCK
        )

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestAdminPermission()]


class UnlockCandidateTest(APIView):
    def post(self, request):
        needed_parameters = ["username_id", "test_id"]
        optional_parameters = ["test_section_id"]
        return test_administrator_actions(
            request, needed_parameters, optional_parameters, TaActionsConstants.UNLOCK
        )

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestAdminPermission()]


class LockAllCandidatesTest(APIView):
    def post(self, request):
        return lock_unlock_all_candidates_test(request, TaActionsConstants.LOCK_ALL)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestAdminPermission()]


class UnlockAllCandidatesTest(APIView):
    def post(self, request):
        return lock_unlock_all_candidates_test(request, TaActionsConstants.UNLOCK_ALL)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestAdminPermission()]


class UnAssignCandidate(APIView):
    def post(self, request):
        needed_parameters = ["username_id", "test_id"]
        optional_parameters = []
        return test_administrator_actions(
            request, needed_parameters, optional_parameters, TaActionsConstants.UNASSIGN
        )

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestAdminPermission()]
