from rest_framework.response import Response
from backend.custom_models.assigned_test import AssignedTest
from backend.static.assigned_test_status import AssignedTestStatus
from backend.serializers.assigned_test_serializer import (
    AssignedTestSerializer,
    UitAssignedTestSerializer,
)
from backend.views.utils import get_user_info_from_jwt_token


def retrieve_assigned_tests(request):
    assigned_test_id = request.query_params.get("assigned_test_id", None)
    user_info = get_user_info_from_jwt_token(request)
    if assigned_test_id:
        query_list = AssignedTest.objects.filter(id=assigned_test_id)
    else:
        query_list = AssignedTest.objects.filter(
            username=user_info["username"],
            status__in=[
                AssignedTestStatus.ASSIGNED,
                AssignedTestStatus.READY,
                AssignedTestStatus.PRE_TEST,
                AssignedTestStatus.ACTIVE,
                AssignedTestStatus.LOCKED,
                AssignedTestStatus.PAUSED,
            ],
        )

    data = AssignedTestSerializer(query_list, many=True)
    return Response(data.data)


def retrieve_uit_assigned_tests(request):
    user_info = get_user_info_from_jwt_token(request)
    query_list = AssignedTest.objects.filter(
        username=user_info["username"],
        status__in=[
            AssignedTestStatus.READY,
            AssignedTestStatus.PRE_TEST,
            AssignedTestStatus.ACTIVE,
        ],
    )

    data = UitAssignedTestSerializer(query_list, many=True)
    return Response(data.data)
