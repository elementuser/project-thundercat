from rest_framework import permissions
from rest_framework.views import APIView
from backend.api_permissions.role_based_api_permissions import HasSystemAdminPermission
from backend.views.system_alert import (
    create_system_alert,
    modify_system_alert,
    archive_system_alert,
    get_active_system_alerts_to_display_on_home_page,
    get_system_alert_data,
    get_all_active_system_alerts,
    get_all_archived_system_alerts,
    get_found_archived_system_alerts,
)

# create new system alert
class CreateSystemAlert(APIView):
    def post(self, request):
        return create_system_alert(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


# modify system alert
class ModifySystemAlert(APIView):
    def post(self, request):
        return modify_system_alert(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


# archive system alert
class ArchiveSystemAlert(APIView):
    def post(self, request):
        return archive_system_alert(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


# get all active system alerts that need to be displayed on the home page
class GetActiveSystemAlertsToDisplayOnHomePage(APIView):
    def get(self, request):
        return get_active_system_alerts_to_display_on_home_page(request)

    def get_permissions(self):
        return [permissions.AllowAny()]


# get all active system alerts that need to be displayed on the home page
class GetSystemAlertData(APIView):
    def get(self, request):
        return get_system_alert_data(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


# get all active system alerts
class GetAllActiveSystemAlerts(APIView):
    def get(self, request):
        return get_all_active_system_alerts(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


# get all archived system alerts
class GetAllArchivedSystemAlerts(APIView):
    def get(self, request):
        return get_all_archived_system_alerts(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


# get found archived system alerts
class GetFoundArchivedSystemAlerts(APIView):
    def get(self, request):
        return get_found_archived_system_alerts(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]
