from rest_framework.response import Response
from rest_framework import status
import requests
from backend.views.utils import is_undefined
from backend.views.oauth_provider import get_oauth_token
from backend.views.discovery import get_discovery_url, DiscoveryUrlTarget


# getting tics data
def get_data(request):
    order_number = request.query_params.get("order_number", None)
    # verify that all parameters were properly passed in
    if is_undefined(order_number):
        return Response(
            {"error": "no 'order_number' parameter"}, status=status.HTTP_400_BAD_REQUEST
        )

    # building ppc-order-service endpoint url
    endpoint = get_discovery_url(DiscoveryUrlTarget.PPC_ORDER_SERVICE) + "orders/"

    try:
        # getting oauth token
        token = get_oauth_token()

    # unable to get oauth token
    except:
        return Response(
            {"error": "unable to get oauth token"}, status=status.HTTP_400_BAD_REQUEST
        )

    headers = {"Authorization": "bearer " + token}
    params = {"order_number": order_number}

    try:
        # GET call that is getting the needed tics data based on the provided order number
        response = requests.get(endpoint, headers=headers, params=params)

        # unable to access ordering service
        if response.status_code == 503:
            return Response(
                {"error": "unable to access ordering-service endpoint"},
                status=status.HTTP_503_SERVICE_UNAVAILABLE,
            )

        # endpoint not found
        elif response.status_code == 404:
            return Response(
                {"error": "unable to access ordering-service endpoint"},
                status=status.HTTP_404_NOT_FOUND,
            )

        # invalid token error
        elif response.status_code == 401:
            return Response(
                {"error": "invalid token"}, status=status.HTTP_401_UNAUTHORIZED
            )

    # catching all other errors
    except:
        return Response(
            {"error": "something happened while trying to access ordering service"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    # converting response to json format
    data = response.json()

    # there is data related to provided order number
    if data["responseCode"] == 200:
        # returning data
        return Response(data)
    # there is no data related to provided order number
    elif data["responseCode"] == 204:
        return Response({"info": "no results found"}, status=status.HTTP_204_NO_CONTENT)
    # should never happen
    else:
        return Response(
            {"error": "something happened during getting tics data process"},
            status=status.HTTP_400_BAD_REQUEST,
        )
