from rest_framework.views import APIView
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework import status
from backend.views.uit_score_test import uit_scoring, Action
from backend.api_permissions.role_based_api_permissions import (
    HasSystemAdminPermission,
)
from backend.custom_models.assigned_test import AssignedTest
from backend.custom_models.assigned_test_section import AssignedTestSection
from backend.static.assigned_test_status import AssignedTestStatus
from cms.views.utils import get_needed_parameters


class ReScoreSubmittedTest(APIView):
    def post(self, request):
        success, parameters = get_needed_parameters(["assigned_test_id"], request)
        if not success:
            return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

        try:
            # getting previous total score and previous converted score
            previous_assigned_test_data = AssignedTest.objects.get(
                id=parameters["assigned_test_id"]
            )

            previous_total_score = previous_assigned_test_data.total_score
            previous_converted_score_en = previous_assigned_test_data.en_converted_score
            previous_converted_score_fr = previous_assigned_test_data.fr_converted_score

            # making sure that the respective test has been submitted at one point (since we're only scoring submitted tests)
            has_been_submitted = False
            historical_assigned_test_data = AssignedTest.history.filter(
                id=parameters["assigned_test_id"]
            )
            for data in historical_assigned_test_data:
                if data.status == AssignedTestStatus.SUBMITTED:
                    has_been_submitted = True
                    break

            # test has been submitted at one point
            if has_been_submitted:
                # getting all timed sections
                timed_assigned_test_sections = AssignedTestSection.objects.filter(
                    assigned_test_id=parameters["assigned_test_id"],
                    test_section_time__isnull=False,
                )

                # looping in timed_assigned_test_sections
                for assigned_test_section in timed_assigned_test_sections:
                    parameters[
                        "test_section_id"
                    ] = assigned_test_section.test_section_id
                    # scoring current test section
                    uit_scoring(
                        parameters,
                        Action.TEST_SECTION,
                    )

                # scoring the test
                returned_uit_scoring_values = uit_scoring(parameters, Action.TEST)

                # getting updated converted score values
                updated_assigned_test_data = AssignedTest.objects.get(
                    id=parameters["assigned_test_id"]
                )

                updated_converted_score_en = (
                    updated_assigned_test_data.en_converted_score
                )
                updated_converted_score_fr = (
                    updated_assigned_test_data.fr_converted_score
                )

                return Response(
                    {
                        "status": returned_uit_scoring_values.data["status"],
                        "previous_data": {
                            "previous_total_score": previous_total_score,
                            "previous_converted_score_en": previous_converted_score_en,
                            "previous_converted_score_fr": previous_converted_score_fr,
                        },
                        "updated_data": {
                            "updated_total_score": returned_uit_scoring_values.data[
                                "total_score"
                            ],
                            "updated_converted_score_en": updated_converted_score_en,
                            "updated_converted_score_fr": updated_converted_score_fr,
                        },
                    },
                    status.HTTP_200_OK,
                )
            # test has not been submitted (status is not 20)
            else:
                return Response(
                    {
                        "info": "The test you are trying to re-score has never been submitted before"
                    },
                    status=status.HTTP_200_OK,
                )
        except AssignedTest.DoesNotExist:
            return Response(
                {"error": "The specified assigned test ID does not exist"},
                status=status.HTTP_400_BAD_REQUEST,
            )

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]
