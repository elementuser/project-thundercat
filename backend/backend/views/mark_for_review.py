from django.utils import timezone
from rest_framework.response import Response
from rest_framework import status
from cms.static.test_section_component_type import TestSectionComponentType
from cms.cms_models.test_section_component import TestSectionComponent
from backend.views.utils import is_undefined
from backend.custom_models.candidate_answers import CandidateAnswers
from backend.custom_models.candidate_multiple_choice_answers import (
    CandidateMultipleChoiceAnswers,
)


class Action:
    UPDATE = "UPDATE"


def mark_for_review(request, needed_parameters, action):
    parameters = []
    # handling missing parameters
    for parameter in needed_parameters:
        parameter_name = "{}".format(parameter)
        parameter = request.query_params.get(parameter_name, None)
        if is_undefined(parameter):
            return Response(
                {"error": "no '{}' parameter".format(parameter_name)},
                status=status.HTTP_400_BAD_REQUEST,
            )
        parameters.append(parameter)

    try:
        # getting test section component type
        test_section_component_type = TestSectionComponent.objects.get(
            id=parameters[2]
        ).component_type

        # questions coming from test builder
        if test_section_component_type == TestSectionComponentType.QUESTION_LIST:
            # getting candidate answer object
            candidate_answer = CandidateAnswers.objects.raw(
                """ SELECT TOP 1
                    id AS 'id',
                    mark_for_review AS 'mark_for_review'
                FROM custom_models_candidateanswers
                WHERE assigned_test_id={0} AND question_id={1} AND test_section_component_id={2}
            """.format(
                    parameters[0], parameters[1], parameters[2]
                ),
            )[0]
        # questions coming from item bank
        elif test_section_component_type == TestSectionComponentType.ITEM_BANK:
            # getting candidate answer object
            candidate_answer = CandidateAnswers.objects.raw(
                """ SELECT TOP 1
                    id AS 'id',
                    mark_for_review AS 'mark_for_review'
                FROM custom_models_candidateanswers
                WHERE assigned_test_id={0} AND item_id={1} AND test_section_component_id={2}
            """.format(
                    parameters[0], parameters[1], parameters[2]
                ),
            )[0]
        # unsupported test section component type (should never happen)
        else:
            return Response(
                {"error": "unsupported test section component type"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        # update review
        if action == Action.UPDATE:
            # updating mark for review value
            candidate_answer.mark_for_review = not candidate_answer.mark_for_review
            # saving result
            candidate_answer.save()
            # updating modify_date of candidate multiple choice answers (if it exists for the given candidate answer)
            multiple_choice_answer = CandidateMultipleChoiceAnswers.objects.filter(
                candidate_answers_id=candidate_answer.id
            ).first()
            if multiple_choice_answer:
                multiple_choice_answer.modify_date = timezone.now()
                multiple_choice_answer.save()
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(
                {"error": "{} action does not exist".format(action)},
                status=status.HTTP_400_BAD_REQUEST,
            )

    except CandidateAnswers.DoesNotExist:
        return Response(
            {"error": "the specified candidate answer does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    except ValueError:
        return Response(
            {"error": "the specified parameters are invalid"},
            status=status.HTTP_400_BAD_REQUEST,
        )
