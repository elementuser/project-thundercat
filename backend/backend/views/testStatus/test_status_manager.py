from datetime import datetime
from rest_framework.response import Response
from rest_framework import status
from backend.custom_models.break_bank_actions import BreakBankActions
from backend.custom_models.accommodation_request import AccommodationRequest
from backend.custom_models.assigned_test import AssignedTest
from backend.static.assigned_test_status import AssignedTestStatus
from backend.views.retrieve_notepad_view import delete_notepad
from backend.custom_models.test_scorer_assignment import TestScorerAssignment
from backend.views.utils import is_undefined, get_user_info_from_jwt_token
from cms.views.utils import get_needed_parameters
from cms.views.test_break_bank import BreakBankActionsConstants


def update_test_status(test_id, new_status):
    test = AssignedTest.objects.get(id=test_id)
    test.status = new_status
    return save_and_return(test)


# create a new instance of assigned_test_status for this test, if it was submitted or timedout
def prepare_test_for_scoring(test_id, new_status):
    if new_status == AssignedTestStatus.SUBMITTED:
        check = TestScorerAssignment.objects.filter(assigned_test_id=test_id)
        if not check:
            assignment = TestScorerAssignment(assigned_test_id=test_id)
            assignment.save()


def submit_test(test_id, new_status):
    test = AssignedTest.objects.get(id=test_id)

    if new_status in (
        AssignedTestStatus.SUBMITTED,
        AssignedTestStatus.QUIT,
    ):
        test.submit_date = datetime.now()

    test.status = new_status

    # clean out the notepad
    delete_notepad(test_id)

    prepare_test_for_scoring(test_id, new_status)

    return save_and_return(test)


def save_and_return(test):
    test.save()
    return Response()


def update_candidate_test_status(request):
    user_info = get_user_info_from_jwt_token(request)
    assigned_test_id = request.query_params.get("assigned_test_id", None)
    test_status = request.query_params.get("test_status", None)
    previous_status = request.query_params.get("previous_status", None)
    if is_undefined(assigned_test_id):
        return Response(
            {"error": "The assigned_test_id was not defined."},
            status=status.HTTP_400_BAD_REQUEST,
        )
    if is_undefined(test_status):
        return Response(
            {"error": "The test_status was not defined."},
            status=status.HTTP_400_BAD_REQUEST,
        )
    test_status_exists = False
    # looping in AssignedTestStatus attributes
    for attr in vars(AssignedTestStatus):
        # exclude unwanted attributes (generic python attributes)
        if not attr.startswith("__"):
            if getattr(AssignedTestStatus, attr) == int(test_status):
                test_status_exists = True
    # specified test status does not exists
    if not test_status_exists:
        return Response(
            {"error": "the specified test status does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    else:
        try:
            assigned_test = AssignedTest.objects.get(
                id=assigned_test_id, username_id=user_info["username"]
            )
            assigned_test.status = test_status
            assigned_test.save()

            # making sure that the previous_status is defined
            if previous_status != "undefined":
                # previous status is provided, but is "null"
                if previous_status == "null":
                    assigned_test.previous_status = None
                    assigned_test.save()
                # previous status is PAUSED + current status is ACTIVE (unpausing test on timer timeout action)
                elif int(previous_status) == AssignedTestStatus.PAUSED and (
                    int(test_status) == AssignedTestStatus.ACTIVE
                ):
                    # if there is an accommodation request
                    if assigned_test.accommodation_request_id is not None:
                        # getting accommodation request data
                        accommodation_request_data = AccommodationRequest.objects.get(
                            id=assigned_test.accommodation_request_id
                        )
                        # if there is a defined break bank
                        if accommodation_request_data.break_bank_id is not None:
                            # getting break_bank_id
                            break_bank_id = accommodation_request_data.break_bank_id
                            # getting last break bank PAUSE action
                            last_break_bank_pause_action = (
                                BreakBankActions.objects.filter(
                                    break_bank_id=break_bank_id,
                                    action_type=BreakBankActionsConstants.PAUSE,
                                )
                                .order_by("modify_date")
                                .last()
                            )
                            # creating last UNPAUSE break bank action
                            BreakBankActions.objects.create(
                                action_type=BreakBankActionsConstants.UNPAUSE,
                                new_remaining_time=0,
                                break_bank_id=break_bank_id,
                                test_section_id=last_break_bank_pause_action.test_section_id,
                            )
            return Response(status=status.HTTP_200_OK)

        except AssignedTest.DoesNotExist:
            return Response(
                {"error": "the specified assigned test id does not exist"},
                status=status.HTTP_400_BAD_REQUEST,
            )


def get_previous_test_status(request):
    # making sure that we have the needed parameters
    success, parameters = get_needed_parameters(["assigned_test_id"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

    try:
        assigned_test = AssignedTest.objects.get(id=parameters["assigned_test_id"])
        return Response(assigned_test.previous_status)
    except AssignedTest.DoesNotExist:
        return Response(
            {"error": "unable to find the specified assigned test"},
            status=status.HTTP_400_BAD_REQUEST,
        )


def get_current_test_status(request):
    # making sure that we have the needed parameters
    success, parameters = get_needed_parameters(["assigned_test_id"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

    try:
        assigned_test = AssignedTest.objects.get(id=parameters["assigned_test_id"])
        return Response(
            {"status": assigned_test.status, "is_invalid": assigned_test.is_invalid}
        )
    except AssignedTest.DoesNotExist:
        return Response(
            {"error": "unable to find the specified assigned test"},
            status=status.HTTP_400_BAD_REQUEST,
        )
