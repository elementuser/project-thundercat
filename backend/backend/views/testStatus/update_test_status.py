from rest_framework.views import APIView
from rest_framework import permissions
from backend.views.testStatus.test_status_manager import (
    update_test_status,
    submit_test,
    update_candidate_test_status,
    get_previous_test_status,
    get_current_test_status,
)
from backend.static.assigned_test_status import AssignedTestStatus
from backend.api_permissions.assigned_test_permissions import HasAssignedTestPermission

# api/activate-test


class SubmitTest(APIView):
    def post(self, request):
        return submit_test(
            request.query_params.get("test_id", None), AssignedTestStatus.SUBMITTED
        )

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasAssignedTestPermission()]


class QuitTest(APIView):
    def post(self, request):
        return submit_test(
            request.query_params.get("test_id", None), AssignedTestStatus.QUIT
        )

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasAssignedTestPermission()]


    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasAssignedTestPermission()]


class SubmitTestTimeout(APIView):
    def post(self, request):
        return submit_test(
            request.query_params.get("test_id", None), AssignedTestStatus.TIMED_OUT
        )

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasAssignedTestPermission()]


class UpdateTestStatus(APIView):
    def post(self, request):
        return update_candidate_test_status(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class GetPreviousTestStatus(APIView):
    def get(self, request):
        return get_previous_test_status(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class GetCurrentTestStatus(APIView):
    def get(self, request):
        return get_current_test_status(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]
