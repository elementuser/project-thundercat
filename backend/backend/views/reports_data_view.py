from rest_framework.views import APIView
from rest_framework import permissions
from rest_framework import status
from rest_framework.response import Response
from cms.views.utils import get_needed_parameters
from backend.views.retrieve_reports_data import (
    retrieve_ta_assigned_test_order_numbers,
    retrieve_all_existing_test_order_numbers,
    retrieve_tests_based_on_test_order_number,
    retrieve_candidate_tests_data,
    retrieve_results_report_data,
    retrieve_financial_report_data,
    retrieve_test_definition_data,
    retrieve_test_content_report_data,
    retrieve_test_taker_report_data,
    retrieve_candidate_actions_report_data,
    retrieve_candidates_base_on_selected_test,
)
from backend.api_permissions.role_based_api_permissions import (
    HasTestAdminPermission,
    HasSystemAdminPermission,
    HasPPCAdminPermission,
)


class GetTaAssignedTestOrderNumbers(APIView):
    def get(self, request):
        return retrieve_ta_assigned_test_order_numbers(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestAdminPermission()]


class GetAllExistingTestOrderNumbers(APIView):
    permission_classes = (
        permissions.IsAuthenticated
        & (HasSystemAdminPermission | HasPPCAdminPermission),
    )

    def get(self, request):
        return retrieve_all_existing_test_order_numbers(request)


class GetTestsBasedOnTestOrderNumber(APIView):
    permission_classes = (
        permissions.IsAuthenticated
        & (HasSystemAdminPermission | HasPPCAdminPermission | HasTestAdminPermission),
    )

    def get(self, request):
        success, parameters = get_needed_parameters(
            [
                "test_order_number",
                "orderless_request",
                "requested_by_ta",
                "provided_ta_username",
            ],
            request,
        )
        if not success:
            return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
        return retrieve_tests_based_on_test_order_number(request, parameters)


class GetCandidatesBasedOnSelectedTest(APIView):
    permission_classes = (
        permissions.IsAuthenticated
        & (HasSystemAdminPermission | HasPPCAdminPermission | HasTestAdminPermission),
    )

    def get(self, request):
        success, parameters = get_needed_parameters(
            [
                "test_order_or_reference_number",
                "orderless_request",
                "test_id",
                "requested_by_ta",
                "provided_ta_username",
            ],
            request,
        )
        if not success:
            return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
        return retrieve_candidates_base_on_selected_test(request, parameters)


class GetCandidateTests(APIView):
    permission_classes = (permissions.IsAuthenticated & (HasPPCAdminPermission),)

    def get(self, request):
        success, parameters = get_needed_parameters(
            ["test_order_number", "test_id", "username_id", "orderless_request"],
            request,
        )
        if not success:
            return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
        return retrieve_candidate_tests_data(parameters)


class GetResultsReportData(APIView):
    permission_classes = (
        permissions.IsAuthenticated
        & (HasSystemAdminPermission | HasPPCAdminPermission | HasTestAdminPermission),
    )

    def get(self, request):
        success, parameters = get_needed_parameters(
            ["test_order_number", "orderless_request", "test_ids", "remove_duplicates"],
            request,
        )
        if not success:
            return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
        return retrieve_results_report_data(request, parameters)


class GetFinancialReportData(APIView):
    def get(self, request):
        success, parameters = get_needed_parameters(["date_from", "date_to"], request)
        if not success:
            return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
        return retrieve_financial_report_data(parameters)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


class GetTestDefinitionData(APIView):
    permission_classes = (
        permissions.IsAuthenticated
        & (HasSystemAdminPermission | HasPPCAdminPermission),
    )

    def get(self, request):
        return retrieve_test_definition_data(request)


class GetTestContentReportData(APIView):
    permission_classes = (permissions.IsAuthenticated & (HasPPCAdminPermission),)

    def get(self, request):
        success, parameters = get_needed_parameters(
            ["parent_code", "test_code", "version"], request
        )
        if not success:
            return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
        return retrieve_test_content_report_data(parameters)


class GetTestTakerReportData(APIView):
    permission_classes = (permissions.IsAuthenticated & (HasPPCAdminPermission),)

    def get(self, request):
        success, parameters = get_needed_parameters(
            ["parent_code", "test_code", "date_from", "date_to"], request
        )
        if not success:
            return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
        return retrieve_test_taker_report_data(parameters)


class GetCandidateActionsReportData(APIView):
    permission_classes = (permissions.IsAuthenticated & (HasPPCAdminPermission | HasSystemAdminPermission),)

    def get(self, request):
        success, parameters = get_needed_parameters(["assigned_test_id"], request)
        if not success:
            return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
        return retrieve_candidate_actions_report_data(parameters)
