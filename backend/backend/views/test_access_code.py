from operator import and_, itemgetter
from functools import reduce
from urllib import parse
import random
import string
import datetime
import pytz
from rest_framework.response import Response
from rest_framework import status
from django.db.models import Q
from cms.views.utils import get_needed_parameters
from cms.cms_models.test_definition import TestDefinition
from cms.cms_models.test_permissions_model import TestPermissions
from backend.views.utils import get_user_info_from_jwt_token
from backend.custom_models.test_access_code_model import TestAccessCode
from backend.custom_models.unsupervised_test_access_code_model import (
    UnsupervisedTestAccessCode,
)
from backend.custom_models.language import Language
from backend.custom_models.orderless_financial_data import OrderlessFinancialData
from backend.serializers.test_access_code_serializer import (
    TestAccessCodeSerializer,
)
from user_management.user_management_models.user_models import User
from user_management.views.utils import CustomPagination
from db_views.db_view_models.bo_test_access_codes_vw import BoTestAccessCodesVW
from db_views.serializers.bo_test_access_codes_serializers import (
    BoTestAccessCodesViewSerializer,
)


# get a new randomly generated room number
def generate_random_test_access_code():
    allowed_chars = string.ascii_uppercase + string.digits
    random_test_access_code = "".join(random.choice(allowed_chars) for i in range(10))
    return random_test_access_code


# check if test access code already exists in TestAccessCode or UnsupervisedTestAccessCode
def access_code_already_exists(random_code):
    if TestAccessCode.objects.filter(test_access_code=random_code).count() > 0:
        return True
    elif (
        UnsupervisedTestAccessCode.objects.filter(test_access_code=random_code).count()
        > 0
    ):
        return True
    else:
        return False


# populate the room number table with the needed parameters, which contains the generated room number
def populate_test_access_code_table(request):
    user_info = get_user_info_from_jwt_token(request)
    # making sure that we have the needed parameters
    success, parameters = get_needed_parameters(
        ["test_order_number", "test_session_language", "test_id", "orderless_request"],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

    # call the function that generates the random room number
    random_test_access_code = generate_random_test_access_code()
    while access_code_already_exists(random_test_access_code):
        random_test_access_code = generate_random_test_access_code()

    # orderless request
    if parameters["orderless_request"]:
        # optional parameters
        reference_number = request.data.get("reference_number", None)
        department_ministry_id = request.data.get("department_ministry_id", None)
        fis_organisation_code = request.data.get("fis_organisation_code", None)
        fis_reference_code = request.data.get("fis_reference_code", None)
        billing_contact_name = request.data.get("billing_contact_name", None)
        billing_contact_info = request.data.get("billing_contact_info", None)

        # creating new entry in OrderlessFinancialData table
        orderless_financial_data = OrderlessFinancialData.objects.create(
            reference_number=reference_number,
            department_ministry_id=department_ministry_id,
            fis_organisation_code=fis_organisation_code,
            fis_reference_code=fis_reference_code,
            billing_contact_name=billing_contact_name,
            billing_contact_info=billing_contact_info,
            reason_for_testing_id=None,
            level_required=None,
            uit_invite_id=None,
        )

        # creating new entry in TestAccessCode table
        TestAccessCode.objects.create(
            test_access_code=random_test_access_code,
            ta_username=User.objects.get(username=user_info["username"]),
            test_order_number=None,
            test_session_language=Language.objects.get(
                language_id=parameters["test_session_language"]
            ),
            test=TestDefinition.objects.get(id=parameters["test_id"]),
            created_date=datetime.datetime.now(pytz.utc),
            orderless_financial_data_id=orderless_financial_data.id,
        )

    # request with associated test permission
    else:
        # creating new entry in TestAccessCode table
        TestAccessCode.objects.create(
            test_access_code=random_test_access_code,
            ta_username=User.objects.get(username=user_info["username"]),
            test_order_number=parameters["test_order_number"],
            test_session_language=Language.objects.get(
                language_id=parameters["test_session_language"]
            ),
            test=TestDefinition.objects.get(id=parameters["test_id"]),
            created_date=datetime.datetime.now(pytz.utc),
            orderless_financial_data_id=None,
        )
    # return test access code
    return Response(random_test_access_code)


def delete_test_access_code_row(request):
    # making sure that we have the needed parameters
    success, parameters = get_needed_parameters(["test_access_code"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    test_access_code = itemgetter("test_access_code")(parameters)

    # getting test access code
    test_access_code_data = TestAccessCode.objects.filter(
        test_access_code=test_access_code
    )

    # test access code found
    if test_access_code_data:
        # deleting test access code
        test_access_code_data.first().delete()
        return Response(status=status.HTTP_200_OK)
    else:
        # getting unsupervised test access code
        unsupervised_test_access_code_data = UnsupervisedTestAccessCode.objects.filter(
            test_access_code=test_access_code
        )

        # unsupervised test access code found
        if unsupervised_test_access_code_data:
            # deleting unsupervised test access code
            unsupervised_test_access_code_data.first().delete()
            return Response(status=status.HTTP_200_OK)

        # no test access code (supervised/unsupervised) found at all
        else:
            return Response(
                {"error": "no room number found"}, status=status.HTTP_400_BAD_REQUEST
            )


def get_active_test_access_codes(request):
    user_info = get_user_info_from_jwt_token(request)
    try:
        active_test_access_codes = TestAccessCode.objects.filter(
            ta_username_id=user_info["username"]
        )
        serializer = TestAccessCodeSerializer(active_test_access_codes, many=True)
        return Response(serializer.data)
    # should not happen
    except TestPermissions.DoesNotExist:
        return Response(
            {
                "error": "there are no matching results between test permissions and test access codes tables"
            },
            status=status.HTTP_400_BAD_REQUEST,
        )


def get_all_active_test_access_codes(request):
    success, parameters = get_needed_parameters(["page", "page_size"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    page, page_size = itemgetter("page", "page_size")(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # get all test access codes
    active_test_access_codes = BoTestAccessCodesVW.objects.all()

    # getting serialized active test access codes data
    serialized_active_test_access_codes_data = BoTestAccessCodesViewSerializer(
        active_test_access_codes, many=True
    ).data

    # getting sorted serialized data by TA last_name (ascending)
    ordered_active_test_access_codes = sorted(
        serialized_active_test_access_codes_data,
        key=lambda k: k["ta_last_name"].lower(),
        reverse=False,
    )

    # only getting data for current selected page
    new_active_test_access_codes = ordered_active_test_access_codes[
        (current_page - 1) * page_size : current_page * page_size
    ]

    # retuning final data using pagination library
    return paginator.get_paginated_response(
        new_active_test_access_codes,
        len(active_test_access_codes),
        current_page,
        page_size,
    )


def get_found_active_test_access_codes(request):
    success, parameters = get_needed_parameters(
        ["keyword", "current_language", "page", "page_size"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    page, page_size, current_language, keyword = itemgetter(
        "page", "page_size", "current_language", "keyword"
    )(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # if blank search
    if keyword == " ":
        found_active_test_access_codes = BoTestAccessCodesVW.objects.all()
    # regular search
    else:
        # decode keyword to get special characters
        keyword = parse.unquote(keyword)

        # since there is a comma between last name and first name in active test permissions table, if the user put
        # a comma in one of the search keyword words, remove it
        keyword_without_comma = keyword.replace(",", "")
        # splitting keyword string
        split_keyword = keyword_without_comma.split()

        # if keyword contains more than one word
        if len(split_keyword) > 1:
            # search while interface is in English
            if current_language == "en":
                found_active_test_access_codes = BoTestAccessCodesVW.objects.filter(
                    reduce(
                        and_,
                        [
                            Q(test_access_code__icontains=splitted_keyword)
                            | Q(test_order_number__icontains=splitted_keyword)
                            | Q(reference_number__icontains=splitted_keyword)
                            | Q(created_date__icontains=splitted_keyword)
                            | Q(ta_first_name__icontains=splitted_keyword)
                            | Q(ta_last_name__icontains=splitted_keyword)
                            | Q(ta_goc_email__icontains=splitted_keyword)
                            | Q(en_test_name__icontains=splitted_keyword)
                            for splitted_keyword in split_keyword
                        ],
                    )
                )
            # search while interface is in French
            else:
                found_active_test_access_codes = BoTestAccessCodesVW.objects.filter(
                    reduce(
                        and_,
                        [
                            Q(test_access_code__icontains=splitted_keyword)
                            | Q(test_order_number__icontains=splitted_keyword)
                            | Q(reference_number__icontains=splitted_keyword)
                            | Q(created_date__icontains=splitted_keyword)
                            | Q(ta_first_name__icontains=splitted_keyword)
                            | Q(ta_last_name__icontains=splitted_keyword)
                            | Q(ta_goc_email__icontains=splitted_keyword)
                            | Q(fr_test_name__icontains=splitted_keyword)
                            for splitted_keyword in split_keyword
                        ],
                    )
                )
        else:
            # search while interface is in English
            if current_language == "en":
                found_active_test_access_codes = BoTestAccessCodesVW.objects.filter(
                    Q(test_access_code__icontains=keyword)
                    | Q(test_order_number__icontains=keyword)
                    | Q(reference_number__icontains=keyword)
                    | Q(created_date__icontains=keyword)
                    | Q(ta_first_name__icontains=keyword)
                    | Q(ta_last_name__icontains=keyword)
                    | Q(ta_goc_email__icontains=keyword)
                    | Q(en_test_name__icontains=keyword)
                )
            # search while interface is in French
            else:
                found_active_test_access_codes = BoTestAccessCodesVW.objects.filter(
                    Q(test_access_code__icontains=keyword)
                    | Q(test_order_number__icontains=keyword)
                    | Q(reference_number__icontains=keyword)
                    | Q(created_date__icontains=keyword)
                    | Q(ta_first_name__icontains=keyword)
                    | Q(ta_last_name__icontains=keyword)
                    | Q(ta_goc_email__icontains=keyword)
                    | Q(fr_test_name__icontains=keyword)
                )

        # no results found
        if not found_active_test_access_codes:
            return Response({"no results found"}, status=status.HTTP_200_OK)

    # getting serialized active test access codes data
    serialized_found_active_test_access_codes = BoTestAccessCodesViewSerializer(
        found_active_test_access_codes, many=True
    ).data

    # getting sorted serialized data by TA last_name (ascending)
    ordered_found_active_test_access_codes = sorted(
        serialized_found_active_test_access_codes,
        key=lambda k: k["ta_last_name"].lower(),
        reverse=False,
    )

    # only getting data for current selected page
    new_found_active_test_access_codes = ordered_found_active_test_access_codes[
        (current_page - 1) * page_size : current_page * page_size
    ]

    # retuning final data using pagination library
    return paginator.get_paginated_response(
        new_found_active_test_access_codes,
        len(found_active_test_access_codes),
        current_page,
        page_size,
    )
