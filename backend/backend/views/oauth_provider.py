import base64
import requests
from django.conf import settings
from backend.views.discovery import get_discovery_url, DiscoveryUrlTarget


# getting oauth token
def get_oauth_token():
    endpoint = (
        get_discovery_url(DiscoveryUrlTarget.OAUTH_PROVIDER)
        + "oauth/token?grant_type=client_credentials"
    )
    # Base64 encoding username/password
    headers = {
        "Authorization": "basic "
        + base64.b64encode(
            (
                settings.OAUTH_PROVIDER_USERNAME
                + ":"
                + settings.OAUTH_PROVIDER_PASSWORD
            ).encode("ascii")
        ).decode("ascii"),
        "Content-Type": "application/x-www-form-urlencoded",
    }

    # POST call that is getting the oauth token
    response = requests.post(endpoint, headers=headers)

    # converting response to json format
    data = response.json()

    # returning oauth access token
    return data["access_token"]
