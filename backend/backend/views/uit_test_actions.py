from django.utils import timezone
from rest_framework.response import Response
from rest_framework import status
from backend.views.utils import is_undefined
from backend.custom_models.candidate_answers import CandidateAnswers
from backend.custom_models.candidate_multiple_choice_answers import (
    CandidateMultipleChoiceAnswers,
)
from backend.custom_models.assigned_test import AssignedTest
from backend.custom_models.language import Language
from backend.serializers.assigned_test_serializer import UitAnswersSerializer
from cms.cms_models.test_section_component import TestSectionComponent
from cms.cms_models.new_question import NewQuestion
from cms.cms_models.answer import Answer
from cms.static.question_type import QuestionType
from cms.static.test_section_component_type import TestSectionComponentType
from cms.cms_models.items import Items
from cms.cms_models.item_option import ItemOption
from cms.static.items_utils import ItemResponseFormat
from cms.cms_models.item_response_formats import ItemResponseFormats
from cms.views.utils import get_needed_parameters


class Action:
    SEEN_QUESTION = "SEEN_QUESTION"
    SAVE_ANSWERS = "SAVE_ANSWERS"
    GET_ANSWERS = "GET_ANSWERS"
    UPDATE_MODIFY_DATE = "UPDATE_MODIFY_DATE"


def test_actions(request, needed_parameters, action):
    # handling missing parameters
    success, params = get_needed_parameters(needed_parameters, request)
    if not success:
        return Response(params, status=status.HTTP_400_BAD_REQUEST)

    parameters = {**params}

    # getting test section component type
    test_section_component_type = TestSectionComponent.objects.get(
        id=parameters["test_section_component_id"]
    ).component_type

    try:
        # get question object based on question id
        if action != Action.GET_ANSWERS:
            # questions coming from test builder
            if test_section_component_type == TestSectionComponentType.QUESTION_LIST:
                question = NewQuestion.objects.get(id=parameters["question_id"])

                # initializing question already seen flag
                question_already_seen = False

                # verifying if question has already been seen
                try:
                    # getting assigned_test_id
                    assigned_test_id = AssignedTest.objects.get(
                        id=parameters["assigned_test_id"]
                    ).id
                    # trying to get the candidate answers object
                    candidate_answers_obj = CandidateAnswers.objects.raw(
                        """ SELECT
                                id AS 'id',
                                question_id AS 'question_id',
                                item_id AS 'item_id',
                                selected_language_id AS 'selected_language_id'
                            FROM custom_models_candidateanswers
                            WHERE assigned_test_id={0} AND question_id={1} AND test_section_component_id={2}
                        """.format(
                            assigned_test_id,
                            parameters["question_id"],
                            question.test_section_component_id,
                        ),
                    )

                    # candidate answers object exists
                    if candidate_answers_obj:
                        candidate_answers_obj = candidate_answers_obj[0]
                        # set question_already_seen to true
                        question_already_seen = True
                    else:
                        # set question_already_seen to false
                        question_already_seen = False

                except AssignedTest.DoesNotExist:
                    return Response(
                        {"error": "the specified assigned test does not exist"},
                        status=status.HTTP_400_BAD_REQUEST,
                    )
            # questions coming from item bank
            elif test_section_component_type == TestSectionComponentType.ITEM_BANK:
                item = Items.objects.get(id=parameters["question_id"])

                # initializing question already seen flag
                question_already_seen = False

                # verifying if item/question has already been seen
                try:
                    # getting assigned_test_id
                    assigned_test_id = AssignedTest.objects.get(
                        id=parameters["assigned_test_id"]
                    ).id
                    # trying to get the candidate answers object
                    candidate_answers_obj = CandidateAnswers.objects.raw(
                        """ SELECT
                                id AS 'id',
                                question_id AS 'question_id',
                                item_id AS 'item_id',
                                selected_language_id AS 'selected_language_id'
                            FROM custom_models_candidateanswers
                            WHERE assigned_test_id={0} AND item_id={1} AND test_section_component_id={2}
                        """.format(
                            assigned_test_id,
                            parameters["question_id"],
                            parameters["test_section_component_id"],
                        ),
                    )

                    # candidate answers object exists
                    if candidate_answers_obj:
                        candidate_answers_obj = candidate_answers_obj[0]
                        # set question_already_seen to true
                        question_already_seen = True
                    else:
                        # set question_already_seen to false
                        question_already_seen = False
                except AssignedTest.DoesNotExist:
                    return Response(
                        {"error": "the specified assigned test does not exist"},
                        status=status.HTTP_400_BAD_REQUEST,
                    )

            # unsupported test section component type (should never happen)
            else:
                return Response(
                    {"error": "unsupported test section component type"},
                    status=status.HTTP_400_BAD_REQUEST,
                )

        # seen question (add row to CandidateAnswers and CandidateMultipleChoiceAnswers tables)
        if action == Action.SEEN_QUESTION:
            # questions coming from test builder
            if test_section_component_type == TestSectionComponentType.QUESTION_LIST:
                # question has never been seen
                if not question_already_seen:
                    # creating new candidate answers object
                    candidate_answers_obj = CandidateAnswers.objects.create(
                        assigned_test_id=parameters["assigned_test_id"],
                        question_id=parameters["question_id"],
                        item_id=None,
                        test_section_component_id=question.test_section_component_id,
                        selected_language_id=None,
                    )
                    # create a new row in candidate multiple choice answers table only if the question type is 'multiple_choice'
                    if question.question_type == QuestionType.MULTIPLE_CHOICE:
                        # creating new candidate multiple choice answers object
                        CandidateMultipleChoiceAnswers.objects.create(
                            answer_id=None,
                            item_answer_id=None,
                            candidate_answers_id=candidate_answers_obj.id,
                        )
            # questions coming from item bank
            elif test_section_component_type == TestSectionComponentType.ITEM_BANK:
                # question has never been seen
                if not question_already_seen:
                    # creating new candidate answers object
                    candidate_answers_obj = CandidateAnswers.objects.create(
                        assigned_test_id=parameters["assigned_test_id"],
                        question_id=None,
                        item_id=parameters["question_id"],
                        test_section_component_id=parameters[
                            "test_section_component_id"
                        ],
                        selected_language_id=None,
                    )
                    # getting response format ID
                    multiple_choice_response_format_id = (
                        ItemResponseFormats.objects.get(
                            codename=ItemResponseFormat.MULTIPLE_CHOICE
                        ).id
                    )
                    # create a new row in candidate multiple choice answers table only if the item type is 'multiple_choice'
                    if item.response_format_id == multiple_choice_response_format_id:
                        # creating new candidate multiple choice answers object
                        CandidateMultipleChoiceAnswers.objects.create(
                            answer_id=None,
                            item_answer_id=None,
                            candidate_answers_id=candidate_answers_obj.id,
                        )

            # unsupported test section component type (should never happen)
            else:
                return Response(
                    {"error": "unsupported test section component type"},
                    status=status.HTTP_400_BAD_REQUEST,
                )

            return Response(status=status.HTTP_200_OK)
        elif action == Action.SAVE_ANSWERS:
            # questions coming from test builder
            if test_section_component_type == TestSectionComponentType.QUESTION_LIST:
                try:
                    # getting question object
                    question_obj = NewQuestion.objects.get(
                        id=candidate_answers_obj.question_id
                    )
                    # getting test section component object (based on current question)
                    test_section_component_obj = TestSectionComponent.objects.get(
                        id=question_obj.test_section_component_id
                    )
                    # current question is unilingual
                    if test_section_component_obj.language_id is not None:
                        # updating candidate answers object with the current question language
                        candidate_answers_obj.selected_language_id = (
                            Language.objects.get(
                                ISO_Code_1=test_section_component_obj.language_id
                            ).language_id
                        )
                    # current question is bilingual
                    else:
                        # updating candidate answers object with the interface language (provided by the parameter)
                        candidate_answers_obj.selected_language_id = (
                            Language.objects.get(
                                ISO_Code_1=parameters["selected_language_id"]
                            ).language_id
                        )
                    candidate_answers_obj.save()
                    # getting candidate multiple choice answers object
                    candidate_multiple_choice_answers_obj = (
                        CandidateMultipleChoiceAnswers.objects.get(
                            candidate_answers_id=candidate_answers_obj.id
                        )
                    )
                    # updating candidate answers object
                    candidate_multiple_choice_answers_obj.answer_id = (
                        Answer.objects.get(
                            id=parameters["answer_id"],
                            question_id=parameters["question_id"],
                        ).id
                    )
                    candidate_multiple_choice_answers_obj.save()
                    return Response(status=status.HTTP_200_OK)

                except CandidateMultipleChoiceAnswers.DoesNotExist:
                    return Response(
                        {
                            "error": "the specified candidate multiple choice answer does not exist"
                        },
                        status=status.HTTP_400_BAD_REQUEST,
                    )

                except Answer.DoesNotExist:
                    return Response(
                        {"error": "the specified answer does not exist"},
                        status=status.HTTP_400_BAD_REQUEST,
                    )
            # questions coming from item bank
            elif test_section_component_type == TestSectionComponentType.ITEM_BANK:
                try:
                    # getting test section component object (based on current question)
                    test_section_component_obj = TestSectionComponent.objects.get(
                        id=parameters["test_section_component_id"]
                    )
                    # current question is unilingual
                    if test_section_component_obj.language_id is not None:
                        # updating candidate answers object with the current question language
                        candidate_answers_obj.selected_language_id = (
                            Language.objects.get(
                                ISO_Code_1=test_section_component_obj.language_id
                            ).language_id
                        )
                    # current question is bilingual
                    else:
                        # updating candidate answers object with the interface language (provided by the parameter)
                        candidate_answers_obj.selected_language_id = (
                            Language.objects.get(
                                ISO_Code_1=parameters["selected_language_id"]
                            ).language_id
                        )
                    candidate_answers_obj.save()
                    # getting candidate multiple choice answers object
                    candidate_multiple_choice_answers_obj = (
                        CandidateMultipleChoiceAnswers.objects.get(
                            candidate_answers_id=candidate_answers_obj.id
                        )
                    )
                    # updating candidate answers object
                    candidate_multiple_choice_answers_obj.item_answer_id = (
                        ItemOption.objects.get(
                            id=parameters["answer_id"],
                            item_id=parameters["question_id"],
                        ).id
                    )
                    candidate_multiple_choice_answers_obj.save()
                    return Response(status=status.HTTP_200_OK)

                except CandidateMultipleChoiceAnswers.DoesNotExist:
                    return Response(
                        {
                            "error": "the specified candidate multiple choice answer does not exist"
                        },
                        status=status.HTTP_400_BAD_REQUEST,
                    )

                except ItemOption.DoesNotExist:
                    return Response(
                        {"error": "the specified item option does not exist"},
                        status=status.HTTP_400_BAD_REQUEST,
                    )

            # unsupported test section component type (should never happen)
            else:
                return Response(
                    {"error": "unsupported test section component type"},
                    status=status.HTTP_400_BAD_REQUEST,
                )

        elif action == Action.GET_ANSWERS:
            # making sure that the test_section_component_id exists

            if not TestSectionComponent.objects.filter(
                id=parameters["test_section_component_id"]
            ):
                return Response(
                    {"error": "the specified test section component does not exist"},
                    status=status.HTTP_400_BAD_REQUEST,
                )
            # getting assigned test object
            assigned_test = AssignedTest.objects.filter(
                id=parameters["assigned_test_id"]
            )

            # assigned test exists
            if assigned_test:
                serializer = UitAnswersSerializer(
                    assigned_test,
                    many=True,
                    context={
                        "test_section_component_id": parameters[
                            "test_section_component_id"
                        ]
                    },
                )
                return Response(serializer.data)
            # assigned test does not exist
            else:
                return Response(
                    {"error": "the specified assigned test does not exist"},
                    status=status.HTTP_400_BAD_REQUEST,
                )

        elif action == Action.UPDATE_MODIFY_DATE:
            candiate_answers_historical_data = CandidateAnswers.history.filter(
                assigned_test_id=parameters["assigned_test_id"]
            ).order_by("history_date")

            # questions coming from test builder
            if test_section_component_type == TestSectionComponentType.QUESTION_LIST:
                # If the last question_id is different than the one in parameters, update modified date; avoids multiple calls at the same time
                if int(candiate_answers_historical_data.last().question_id) != int(
                    parameters["question_id"]
                ):
                    # making sure that the candidate_answer_obj exists
                    if candidate_answers_obj:
                        candidate_answers_obj.modify_date = timezone.now()
                        candidate_answers_obj.save()

                return Response(status=status.HTTP_200_OK)

            # questions coming from item bank
            elif test_section_component_type == TestSectionComponentType.ITEM_BANK:
                # If the last question_id is different than the one in parameters, update modified date; avoids multiple calls at the same time
                if int(candiate_answers_historical_data.last().item_id) != int(
                    parameters["question_id"]
                ):
                    # making sure that the candidate_answer_obj exists
                    if candidate_answers_obj:
                        candidate_answers_obj.modify_date = timezone.now()
                        candidate_answers_obj.save()

                return Response(status=status.HTTP_200_OK)

            # unsupported test section component type (should never happen)
            else:
                return Response(
                    {"error": "unsupported test section component type"},
                    status=status.HTTP_400_BAD_REQUEST,
                )

        else:
            return Response(
                {"error": "{} action does not exist".format(action)},
                status=status.HTTP_400_BAD_REQUEST,
            )

    except CandidateAnswers.DoesNotExist:
        return Response(
            {"error": "the specified candidate answer does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    except ValueError:
        return Response(
            {"error": "the specified parameters are invalid"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    except NewQuestion.DoesNotExist:
        return Response(
            {"error": "the specified question does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )
