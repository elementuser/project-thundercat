from rest_framework.response import Response
from rest_framework import status
from backend.custom_models.additional_time import AdditionalTime
from backend.custom_models.test_access_code_model import TestAccessCode
from backend.custom_models.unsupervised_test_access_code_model import (
    UnsupervisedTestAccessCode,
)
from backend.custom_models.assigned_test import AssignedTest
from backend.static.assigned_test_status import AssignedTestStatus
from backend.custom_models.assigned_test_section import AssignedTestSection
from user_management.user_management_models.user_models import User
from cms.cms_models.test_section import TestSection


def check_into_room(test_access_code, username):
    # initializing unsupervised_test
    unsupervised_test = False
    # check if the provided test access code exists in TestAccessCode table
    room = TestAccessCode.objects.filter(test_access_code=test_access_code).first()
    # test access code does not exist in TestAccessCode table
    if not room:
        # check if the provided test access code exists in UnsupervisedTestAccessCode table
        room = UnsupervisedTestAccessCode.objects.filter(
            test_access_code=test_access_code
        ).first()
        if room:
            unsupervised_test = True
    # no room has been found based on provided test access code
    if not room:
        return Response(
            {"error": "No room has been found based on provided test access code."},
            status=status.HTTP_400_BAD_REQUEST,
        )
    # check if room has already been checked in by the current username
    elif AssignedTest.objects.filter(
        test_access_code=test_access_code, username_id=username
    ).first():
        return Response(
            {
                "error": "This room has already been checked in using the provided test access code."
            },
            status=status.HTTP_400_BAD_REQUEST,
        )
    # check if the test (based on the test access code) is already in an active state in assigned test table
    elif AssignedTest.objects.filter(
        username_id=username,
        test_id=room.test_id,
        status__in=(
            AssignedTestStatus.ASSIGNED,
            AssignedTestStatus.READY,
            AssignedTestStatus.PRE_TEST,
            AssignedTestStatus.ACTIVE,
            AssignedTestStatus.LOCKED,
            AssignedTestStatus.PAUSED,
        ),
    ):
        return Response(
            {
                "error": "The test you are trying to check-in is already in an active state."
            },
            status=status.HTTP_400_BAD_REQUEST,
        )
    else:
        # getting needed data
        test_admin = User.objects.get(username=room.ta_username_id)
        user = User.objects.get(username=username)
        test_id = room.test_id

        # don't allow a TA to take their own exam
        if user.username == test_admin.username:
            return Response(
                {
                    "error": "You cannot be a candidate in an exam you are administering."
                },
                status=status.HTTP_400_BAD_REQUEST,
            )

        # check if this rooms test has already been assigned
        if not has_test_for_test_access_code_already_been_assigned(
            test_access_code, username
        ):
            # supervised test
            if not unsupervised_test:
                # creating new row in AssignedTest table
                assigned_test = AssignedTest(
                    status=AssignedTestStatus.ASSIGNED,
                    test_id=test_id,
                    username_id=user.username,
                    ta=test_admin,
                    test_access_code=test_access_code,
                    test_order_number=room.test_order_number,
                    test_session_language=room.test_session_language,
                    uit_invite_id=None,
                    orderless_financial_data_id=room.orderless_financial_data_id,
                    accommodation_request_id=None,
                )
                assigned_test.save()
            # unsupervised test
            else:
                # creating new row in AssignedTest table
                assigned_test = AssignedTest(
                    status=AssignedTestStatus.READY,
                    test_id=test_id,
                    username_id=user.username,
                    ta=test_admin,
                    test_access_code=test_access_code,
                    test_order_number=room.test_order_number,
                    test_session_language=None,
                    uit_invite_id=room.uit_invite_id,
                    orderless_financial_data_id=room.orderless_financial_data_id,
                    accommodation_request_id=room.accommodation_request_id,
                )
                assigned_test.save()

                # deleting concerned row in UnsupervisedTestAccessCode table
                unsupervised_test_access_code = UnsupervisedTestAccessCode.objects.get(
                    test_access_code=test_access_code
                )
                unsupervised_test_access_code.delete()

            # find all test sections for this test and create an assigned test section for it
            test_sections = TestSection.objects.filter(test_definition_id=test_id)
            for test_section in test_sections:
                # initializing test_section_time
                test_section_time = test_section.default_time

                # ==================== HANDLING ACCOMMODATION - ADDITIONAL TIME ====================
                # unsupervised test
                if unsupervised_test:
                    # contains accommodation configurations
                    if room.accommodation_request_id is not None:
                        # getting additional time configurations
                        additional_time_related_to_current_test_section = (
                            AdditionalTime.objects.filter(
                                accommodation_request_id=room.accommodation_request_id,
                                test_section_id=test_section.id,
                            )
                        )
                        # contains related additional time
                        if additional_time_related_to_current_test_section:
                            # updating test_section_time value
                            test_section_time = (
                                additional_time_related_to_current_test_section.last().test_section_time
                            )
                        # ==================== HANDLING ACCOMMODATION - ADDITIONAL TIME (END) ====================

                # creating new assigned test section entry
                assigned_test_section = AssignedTestSection(
                    test_section=test_section,
                    assigned_test=assigned_test,
                    test_section_time=test_section_time,
                )
                assigned_test_section.save()

            # deleting test access code only if the specified test access code has no assigned ta username (meaning that this is a unsupervised test)
            if room.ta_username_id is None:
                TestAccessCode.objects.get(test_access_code=test_access_code).delete()

    return Response(status=status.HTTP_200_OK)


def has_test_for_test_access_code_already_been_assigned(test_access_code, username):
    return (
        AssignedTest.objects.filter(
            username__username=username, test_access_code=test_access_code
        ).count()
        > 0
    )
