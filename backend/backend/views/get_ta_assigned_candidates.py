from rest_framework.response import Response
from rest_framework import status
from backend.views.utils import get_user_info_from_jwt_token
from backend.custom_models.assigned_test import AssignedTest
from backend.serializers.assigned_test_serializer import AssignedTestSerializer
from backend.static.assigned_test_status import AssignedTestStatus
from user_management.user_management_models.user_models import User
from user_management.user_management_models.custom_user_permissions_model import (
    CustomUserPermissions,
)
from user_management.user_management_models.custom_permissions_model import (
    CustomPermissions,
)
from user_management.static.permission import Permission


# getting test administrator assigned candidates (candidates that are checked in with current ta's test access code)
def get_ta_assigned_candidates(request):
    user_info = get_user_info_from_jwt_token(request)
    try:
        # getting all assigned candidates based on the specified TA (supervised tests only)
        active_candidates = AssignedTest.objects.filter(
            ta_id=user_info["username"],
            status__in=(
                AssignedTestStatus.ASSIGNED,
                AssignedTestStatus.READY,
                AssignedTestStatus.LOCKED,
                AssignedTestStatus.PAUSED,
                AssignedTestStatus.PRE_TEST,
                AssignedTestStatus.ACTIVE,
            ),
            uit_invite_id=None,
        )

        # ordering assigned candidates by last_name
        # getting serialized assigned candidates data
        serialized_assigned_candidates_data = AssignedTestSerializer(
            active_candidates, many=True
        ).data
        # getting sorted serialized data by last_name (ascending)
        ordered_assigned_candidate = sorted(
            serialized_assigned_candidates_data,
            key=lambda k: k["candidate_last_name"].lower(),
            reverse=False,
        )
        # initializing assigned candidates ids array (the assigned_candidates_ids_array order here represents the order)
        assigned_candidates_ids_array = []
        # looping in ordered assigned candidates
        for i in ordered_assigned_candidate:
            # inserting user permissions ids (ordered respectively by last name) in an array
            assigned_candidates_ids_array.insert(
                len(assigned_candidates_ids_array), i["id"]
            )
        # sorting assigned candidates queryset based on ordered (by last name) assigned candidates ids
        new_active_candidates = list(
            AssignedTest.objects.filter(id__in=assigned_candidates_ids_array)
        )
        new_active_candidates.sort(
            key=lambda t: assigned_candidates_ids_array.index(t.id)
        )

        # making sure that the specified user is a TA
        # getting user' permissions
        user_permissions = CustomUserPermissions.objects.filter(
            user_id=user_info["username"]
        )
        # initializing is_ta flag to false
        is_ta = False
        # looping in user' permissions
        for user_permission in user_permissions:
            # specified user has TA permission
            if (
                user_permission.permission_id
                == CustomPermissions.objects.get(
                    codename=Permission.TEST_ADMINISTRATOR
                ).permission_id
            ):
                # set is_ta flag to true
                is_ta = True

        # if specified user is a super user
        if User.objects.get(username=user_info["username"]).is_staff:
            # set is_ta flag to true
            is_ta = True

        # specified user is a TA or a super user
        if is_ta:
            # serializing and returning data
            serializer = AssignedTestSerializer(new_active_candidates, many=True)
            return Response(serializer.data)

        # specified user is NOT a TA
        else:
            return Response(
                {"error": "the specified user is not a test administrator"},
                status=status.HTTP_400_BAD_REQUEST,
            )

    except User.DoesNotExist:
        return Response(
            {"error": "the specified user does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )
