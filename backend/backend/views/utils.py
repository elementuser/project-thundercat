import datetime
import jwt
from django.utils import timezone
from django.conf import settings
from rest_framework import permissions
from rest_framework.views import APIView
from rest_framework.response import Response
from text_resources_backend.text_resources import TextResources
from user_management.user_management_models.user_models import User
from backend.custom_models.candidate_answers import CandidateAnswers
from backend.custom_models.assigned_test import AssignedTest
from backend.custom_models.accommodation_request import AccommodationRequest
from backend.custom_models.break_bank_actions import BreakBankActions
from backend.static.assigned_test_section_access_time_type import (
    AssignedTestSectionAccessTimeType,
)
from backend.custom_models.assigned_test_section_access_times import (
    AssignedTestSectionAccessTimes,
)
from backend.custom_models.break_bank import BreakBank
from backend.custom_models.assigned_test_section import AssignedTestSection
from cms.cms_models.new_question import NewQuestion
from cms.cms_models.viewed_questions import ViewedQuestions
from cms.static.test_section_component_type import TestSectionComponentType

# function that verifies if the specified parameter is undefined


def is_undefined(value):
    if value is None:
        return True
    if value == "undefined":
        return True
    if value == "null":
        return True
    return False


# function that is getting the user information from decrypted provided auth token (JWT token)
def get_user_info_from_jwt_token(request):
    # getting complete auth token
    complete_auth_token = request.headers["Authorization"]
    # removing "JWT" from token
    auth_token = complete_auth_token[4:]
    # decrypting jwt token to get user's information
    user_info = jwt.decode(auth_token, settings.SECRET_KEY, algorithms=["HS256"])
    # returning user info object
    return user_info


# function that is replacing all specific line characters with "" based on provided text
# Useful for reports in csv
def remove_specific_line_chars(text):
    return (
        text.replace("\n", "")
        .replace("\r", "")
        .replace("\t", "")
        .replace("\\", "")
        .replace('"', "")
        .replace("&nbsp;", "")
    )


# function that is replacing single double quotes with two double quotes
# Useful for reports in csv
def replace_single_double_quotes_with_two_double_quotes(text):
    return text.replace('"', '""')


# function useful to confert an array that is provided as a string in an array/list
# format: [<value_1>, <value_2>, ...]
def string_array_to_array(string_array):
    # initializing final_array
    final_array = string_array

    # converting string final_array to array/list
    # ====================================================================
    # removing '[' and ']' chars, so we have a list separated by ','
    final_array = final_array.replace("[", "")
    final_array = final_array.replace("]", "")
    # splitting values
    final_array = list(final_array.split(", "))
    # ====================================================================

    return final_array


# function for UIT test status
class UIT_TEST_STATUS:
    TAKEN = 91
    NOT_TAKEN = 92
    IN_PROGRESS = 93
    UNASSIGNED = 94


# getting weekday
# date format expected: yyyy-mm-dd
def get_weekday_based_on_provided_date(date):
    # converting date to datetime
    converted_date = datetime.datetime.strptime(date, "%Y-%m-%d")
    # getting weekday (0 to 6)
    weekday = converted_date.weekday()
    # converting numbers to words
    if weekday == 0:
        return TextResources.weekday["monday"]
    elif weekday == 1:
        return TextResources.weekday["tuesday"]
    elif weekday == 2:
        return TextResources.weekday["wednesday"]
    elif weekday == 3:
        return TextResources.weekday["thursday"]
    elif weekday == 4:
        return TextResources.weekday["friday"]
    elif weekday == 5:
        return TextResources.weekday["saturday"]
    elif weekday == 6:
        return TextResources.weekday["sunday"]


# getting month in word
def get_month_as_a_word(month):
    if month == "1" or month == "01" or month == 1:
        return TextResources.month["january"]
    elif month == "2" or month == "02" or month == 2:
        return TextResources.month["february"]
    elif month == "3" or month == "03" or month == 3:
        return TextResources.month["march"]
    elif month == "4" or month == "04" or month == 4:
        return TextResources.month["april"]
    elif month == "5" or month == "05" or month == 5:
        return TextResources.month["may"]
    elif month == "6" or month == "06" or month == 6:
        return TextResources.month["june"]
    elif month == "7" or month == "07" or month == 7:
        return TextResources.month["july"]
    elif month == "8" or month == "08" or month == 8:
        return TextResources.month["august"]
    elif month == "9" or month == "09" or month == 9:
        return TextResources.month["september"]
    elif month == "10" or month == 10:
        return TextResources.month["october"]
    elif month == "11" or month == 11:
        return TextResources.month["november"]
    elif month == "12" or month == 12:
        return TextResources.month["december"]


def handleViewedQuestionsLogic(assigned_test_id):
    # initializing considered_questions
    considered_questions = []
    # getting user_id
    user_id = User.objects.get(
        username=AssignedTest.objects.get(id=assigned_test_id).username_id
    ).id

    # initializing the question_source to QUESTION_LIST
    question_source = TestSectionComponentType.QUESTION_LIST

    # getting candidate answers based on provided assigned test ID
    candidate_answers = CandidateAnswers.objects.filter(
        assigned_test_id=assigned_test_id
    )

    # we have one or more candidate answers
    if candidate_answers:
        # question_id is NULL, that means the question source is from the ITEM BANK
        if candidate_answers[0].question_id is None:
            # updating question_source to ITEM BANK
            question_source = TestSectionComponentType.ITEM_BANK

    # questions coming from test builder
    if question_source == TestSectionComponentType.QUESTION_LIST:
        # looping in candidate answers
        for answer in candidate_answers:
            # making sure that the current question (based on current answer) has not been considered yet
            if answer.question_id not in considered_questions:
                # getting question data
                question_data = NewQuestion.objects.get(id=answer.question_id)
                # ppc_question_id is defined for current question_id
                if question_data.ppc_question_id != "":
                    # ==================== HANDLING DEPENDENCIES ====================
                    # checking if current question has dependencies
                    if question_data.dependencies.all():
                        # looping in dependencies
                        for dependency in question_data.dependencies.all():
                            # checking if this user has already seen this question before
                            viewed_question = ViewedQuestions.objects.filter(
                                ppc_question_id=dependency.ppc_question_id,
                                user_id=user_id,
                            ).first()
                            # this user has already seen this question before
                            if viewed_question:
                                # updating viewed question object (incrementing count)
                                viewed_question.count = viewed_question.count + 1
                                viewed_question.save()
                            # this user has never seen this question before
                            else:
                                ViewedQuestions.objects.create(
                                    ppc_question_id=dependency.ppc_question_id,
                                    count=1,
                                    user_id=user_id,
                                )
                            considered_questions.append(dependency.id)
                    # ==================== HANDLING DEPENDENCIES (END) ====================

                    # creating new viewed question object
                    # checking if this user has already seen this question before
                    viewed_question = ViewedQuestions.objects.filter(
                        ppc_question_id=question_data.ppc_question_id, user_id=user_id
                    ).first()
                    # this user has already seen this question before
                    if viewed_question:
                        # updating viewed question object (incrementing count)
                        viewed_question.count = viewed_question.count + 1
                        viewed_question.save()
                    # this user has never seen this question before
                    else:
                        ViewedQuestions.objects.create(
                            ppc_question_id=question_data.ppc_question_id,
                            count=1,
                            user_id=user_id,
                        )
                    considered_questions.append(question_data.id)
            else:
                continue


# this function needs to be called when the test is on PAUSE
def get_new_break_bank_remaining_time(accommodation_request_id):
    # getting related accommodation request data
    accommodation_request_data = AccommodationRequest.objects.get(
        id=accommodation_request_id
    )
    # getting related break bank actions
    break_bank_actions = BreakBankActions.objects.filter(
        break_bank_id=accommodation_request_data.break_bank_id,
    ).order_by("modify_date")
    # checking for a previous UNPAUSE action
    try:
        previous_unpause_action = break_bank_actions[len(break_bank_actions) - 2]
    except:
        previous_unpause_action = None
        pass
    # calculating time between now and last PAUSE
    time_between_now_and_last_pause_action = (
        timezone.now() - break_bank_actions.last().modify_date
    ).total_seconds()

    # there is a previous unpause action
    if previous_unpause_action is not None:
        # calculating remaining time
        remaining_time = datetime.timedelta(
            seconds=previous_unpause_action.new_remaining_time
        ) - datetime.timedelta(seconds=time_between_now_and_last_pause_action)
    # there is no previous unpause action
    else:
        # getting total break bank time
        total_break_bank_time = BreakBank.objects.get(
            id=accommodation_request_data.break_bank_id
        ).break_time
        # calculating remaining time
        remaining_time = datetime.timedelta(
            seconds=total_break_bank_time
        ) - datetime.timedelta(seconds=time_between_now_and_last_pause_action)
    # returning remaining time (in seconds)
    return remaining_time.total_seconds()


# getting last accessed test section based on provided assigned test ID
def get_last_accessed_test_section(assigned_test_id):
    try:
        # getting assigned test section IDs
        assigned_test_section_ids = AssignedTestSection.objects.filter(
            assigned_test_id=assigned_test_id
        ).values_list("id", flat=True)
        # getting last accessed assigned test section ID
        last_accessed_assigned_test_section_id = (
            AssignedTestSectionAccessTimes.objects.filter(
                assigned_test_section_id__in=assigned_test_section_ids,
                time_type=AssignedTestSectionAccessTimeType.START,
            )
            .order_by("time")
            .last()
            .assigned_test_section_id
        )
        # getting test_section_id based on last accessed assigned test section ID
        test_section_id = AssignedTestSection.objects.get(
            id=last_accessed_assigned_test_section_id
        ).test_section_id
    except:
        test_section_id = None

    return test_section_id


class GetServerTime(APIView):
    def get_permissions(self):
        return [permissions.AllowAny()]

    def get(self, request):
        return Response(datetime.datetime.now(timezone.utc))
