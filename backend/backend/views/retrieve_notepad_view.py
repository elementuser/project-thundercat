from operator import itemgetter
from rest_framework import status
from rest_framework.response import Response
from cms.views.utils import get_needed_parameters
from backend.custom_models.notepad import Notepad


def save_notepad(request):
    success, parameters = get_needed_parameters(
        ["assigned_test_id", "notepad_content"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    assigned_test_id, notepad_content = itemgetter(
        "assigned_test_id", "notepad_content"
    )(parameters)

    # see if it exists
    try:
        notepad = Notepad.objects.get(assigned_test=assigned_test_id)
    except Notepad.DoesNotExist:
        # otherwise make a new one
        notepad = Notepad(assigned_test_id=assigned_test_id)
    # update the notepad content and save
    notepad.notepad = notepad_content
    notepad.save()
    return Response(status=status.HTTP_200_OK)


def delete_notepad(assigned_test_id):
    # see if it exists
    try:
        notepad = Notepad.objects.get(assigned_test=assigned_test_id)
    except Notepad.DoesNotExist:
        # if not, do nothing
        return
    # otherwise delete it
    notepad.delete()
    return


def get_notepad(request):
    success, parameters = get_needed_parameters(["assigned_test_id"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    assigned_test_id = itemgetter("assigned_test_id")(parameters)
    # see if it exists
    try:
        notepad = Notepad.objects.get(assigned_test=assigned_test_id).notepad
    except Notepad.DoesNotExist:
        # otherwise return an empty one
        notepad = ""
    return Response({"notepad": notepad}, status=status.HTTP_200_OK)
