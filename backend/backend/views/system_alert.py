from datetime import datetime
from operator import itemgetter
from urllib import parse
from django.db.models import Q
from rest_framework.response import Response
from rest_framework import status
from backend.serializers.system_alert_serializer import SystemAlertSerializer
from backend.custom_models.system_alert import SystemAlert
from backend.custom_models.criticality import Criticality
from cms.views.utils import get_needed_parameters
from user_management.views.utils import CustomPagination


def create_system_alert(request):
    # making sure that we have the needed parameters
    success, params = get_needed_parameters(
        [
            "title",
            "criticality",
            "active_date",
            "end_date",
            "message_text_en",
            "message_text_fr",
        ],
        request,
    )
    if not success:
        return Response(params, status=status.HTTP_400_BAD_REQUEST)

    # creating new entry in System Alert table
    SystemAlert.objects.create(
        title=params["title"],
        criticality=Criticality.objects.get(id=params["criticality"]),
        active_date=params["active_date"],
        end_date=params["end_date"],
        message_text_en=params["message_text_en"],
        message_text_fr=params["message_text_fr"],
    )

    # return a status to confirm that the object has been successfully created
    return Response(
        status=status.HTTP_201_CREATED,
    )


def modify_system_alert(request):
    # making sure that we have the needed parameters
    success, params = get_needed_parameters(
        [
            "id",
            "title",
            "criticality",
            "active_date",
            "end_date",
            "message_text_en",
            "message_text_fr",
        ],
        request,
    )
    if not success:
        return Response(params, status=status.HTTP_400_BAD_REQUEST)

    # creating new entry in System Alert table
    SystemAlert.objects.filter(id=params["id"]).update(
        title=params["title"],
        criticality=Criticality.objects.get(id=params["criticality"]),
        active_date=params["active_date"],
        end_date=params["end_date"],
        message_text_en=params["message_text_en"],
        message_text_fr=params["message_text_fr"],
    )

    # return a status to confirm that the object has been successfully updated
    return Response(
        status=status.HTTP_200_OK,
    )


def archive_system_alert(request):
    # making sure that we have the needed parameters
    success, params = get_needed_parameters(
        ["id"],
        request,
    )
    if not success:
        return Response(params, status=status.HTTP_400_BAD_REQUEST)

    # changing the archived status to true
    SystemAlert.objects.filter(id=params["id"]).update(archived=True)

    # return a status to confirm that the object has been successfully archived
    return Response(
        status=status.HTTP_200_OK,
    )


def get_active_system_alerts_to_display_on_home_page(request):
    # getting current date
    current_date = datetime.now()

    # ==================== archiving passed system alerts ====================
    # getting active system alerts that must be archived (where the end date is passed)
    active_system_alerts_to_be_archived = SystemAlert.objects.filter(
        archived=0, end_date__lte=current_date
    )
    # looping in active_system_alerts_to_be_archived
    for system_alert_to_be_archived in active_system_alerts_to_be_archived:
        # archiving current system alert
        system_alert_to_be_archived.archived = 1
        system_alert_to_be_archived.save()
    # ==================== archiving passed system alerts (END) ====================

    # ==================== system alerts to be displayed ====================
    # getting active system alerts to be displayed based on the current date
    active_system_alerts_to_be_displayed = SystemAlert.objects.filter(
        archived=0, active_date__lte=current_date, end_date__gte=current_date
    )

    # getting serialized data
    serialized_data = SystemAlertSerializer(
        active_system_alerts_to_be_displayed, many=True
    ).data

    # getting sorted serialized data by priority (ascending)
    ordered_active_system_alerts_to_be_displayed = sorted(
        serialized_data,
        key=lambda k: k["criticality_data"]["priority"],
        reverse=False,
    )
    # ==================== system alerts to be displayed (END) ====================

    return Response(ordered_active_system_alerts_to_be_displayed)


def get_system_alert_data(request):
    success, parameters = get_needed_parameters(["id"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    id = itemgetter("id")(parameters)

    # getting system alert data based on provided ID
    system_alert = SystemAlert.objects.get(id=id)

    # getting serialized data
    serialized_data = SystemAlertSerializer(system_alert, many=False).data

    return Response(serialized_data)


def get_all_active_system_alerts(request):
    # getting current date
    current_date = datetime.now()

    # getting active system alerts to be displayed based on the current date
    active_system_alerts = SystemAlert.objects.filter(
        archived=0, end_date__gte=current_date
    )

    # getting serialized data
    serialized_data = SystemAlertSerializer(active_system_alerts, many=True).data

    # getting sorted serialized data by priority (ascending)
    ordered_active_system_alerts = sorted(
        serialized_data,
        key=lambda k: k["active_date"],
        reverse=False,
    )

    return Response(ordered_active_system_alerts)


def get_all_archived_system_alerts(request):
    success, parameters = get_needed_parameters(["page", "page_size"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    page, page_size = itemgetter("page", "page_size")(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # getting all archived system alerts
    archived_system_alerts = SystemAlert.objects.filter(archived=1)

    # getting serialized system alerts data
    serialized_archived_system_alerts = SystemAlertSerializer(
        archived_system_alerts, many=True
    ).data

    # getting sorted serialized data by last_name (ascending)
    ordered_archived_system_alerts = sorted(
        serialized_archived_system_alerts,
        key=lambda k: k["active_date"].lower(),
        reverse=False,
    )

    # only getting data for current selected page
    new_archived_system_alerts = ordered_archived_system_alerts[
        (current_page - 1) * page_size : current_page * page_size
    ]

    # retuning final data using pagination library
    return paginator.get_paginated_response(
        new_archived_system_alerts,
        len(archived_system_alerts),
        current_page,
        page_size,
    )


def get_found_archived_system_alerts(request):
    success, parameters = get_needed_parameters(
        ["keyword", "current_language", "page", "page_size"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    keyword, current_language, page, page_size = itemgetter(
        "keyword", "current_language", "page", "page_size"
    )(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # if blank search
    if keyword == " ":
        # getting all archived system alerts
        found_archived_system_alert_ids = SystemAlert.objects.filter(
            archived=1
        ).values_list("id", flat=True)
    # regular search
    else:
        # decode keyword to get special characters
        keyword = parse.unquote(keyword)

        # getting all matching criticalities
        # English Search
        if current_language == "en":
            corresponding_criticality_ids = Criticality.objects.filter(
                description_en__icontains=keyword
            ).values_list("id", flat=True)
        # French Search
        else:
            corresponding_criticality_ids = Criticality.objects.filter(
                description_fr__icontains=keyword
            ).values_list("id", flat=True)

        # getting all archived system alerts based on matching keyword
        matching_system_alert_ids = SystemAlert.objects.filter(
            Q(archived=1, title__icontains=keyword)
            | Q(archived=1, criticality_id__in=corresponding_criticality_ids)
            | Q(archived=1, active_date__icontains=keyword)
            | Q(archived=1, end_date__icontains=keyword)
        ).values_list("id", flat=True)

        # if there is at least one matching element
        if matching_system_alert_ids:
            found_archived_system_alert_ids = matching_system_alert_ids

        else:
            return Response({"no results found"}, status=status.HTTP_200_OK)

    # getting found system alerts based on found system alert IDs
    found_archived_system_alerts = SystemAlert.objects.filter(
        id__in=found_archived_system_alert_ids
    )

    # getting serialized system alerts data
    serialized_found_archived_system_alerts = SystemAlertSerializer(
        found_archived_system_alerts, many=True
    ).data

    # getting sorted serialized data by last_name (ascending)
    ordered_found_archived_system_alerts = sorted(
        serialized_found_archived_system_alerts,
        key=lambda k: k["active_date"].lower(),
        reverse=False,
    )

    # only getting data for current selected page
    new_found_archived_system_alerts = ordered_found_archived_system_alerts[
        (current_page - 1) * page_size : current_page * page_size
    ]

    # retuning final data using pagination library
    return paginator.get_paginated_response(
        new_found_archived_system_alerts,
        len(found_archived_system_alerts),
        current_page,
        page_size,
    )
