from rest_framework.views import APIView
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework import status
from backend.ref_table_views.cat_ref_departments_vw import CatRefDepartmentsVW
from backend.ref_table_views.cat_education import CatEducation
from backend.ref_table_views.cat_employer_sts_vw import CatRefEmployerStsVW
from backend.ref_table_views.cat_ref_classification_vw import CatRefClassificationVW
from backend.ref_table_views.cat_ref_gender_vw import CatRefGenderVW
from backend.ref_table_views.cat_ref_province_vw import CatRefProvinceVW
from backend.ref_table_views.cat_aboriginal_vw import CatRefAboriginalVW
from backend.ref_table_views.cat_disability_vw import CatRefDisabilityVW
from backend.ref_table_views.cat_visible_minority_vw import CatRefVisibleMinorityVW

from backend.serializers.cat_ref_departments_serializer import (
    CatRefDepartmentsSerializer,
)
from backend.serializers.cat_education_serialier import CatEducationSerializer
from backend.serializers.cat_employer_sts_serializer import CatEmployerStsSerializer
from backend.serializers.cat_aboriginal_serializer import CatAboriginalSerializer
from backend.serializers.cat_visible_minority_serializer import (
    CatVisibleMinoritySerializer,
)
from backend.serializers.cat_disability_serializer import CatDisabilitySerializer
from backend.serializers.cat_ref_classification_serializer import (
    CatRefClassificationSerializer,
)
from backend.serializers.cat_ref_gender_serializer import CatRefGenderSerializer
from backend.serializers.cat_ref_province_serializer import CatRefProvinceSerializer

from cms.views.utils import get_needed_parameters


class GetExtendedProfileOptions(APIView):
    def get(self, request):
        success, parameters = get_needed_parameters(["list_name"], request)
        if not success:
            return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
        return retrieve_extended_profile_options(parameters)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


def retrieve_extended_profile_options(parameters):
    # get the list name
    list_name = parameters.get("list_name")
    # form a dict of list names to functions
    switch = {
        "employer": get_employer_list,
        "organization": get_organisation_list,
        "group": get_group_and_subclassification_list,
        "subclassification": get_subclassification_and_level_list,
        "residence": get_residence_list,
        "education": get_education_list,
        "gender": get_gender_list,
        "aboriginal": get_aboriginal_list,
        "visible_minority": get_visible_minority_list,
        "disability": get_disability_list,
    }
    # get the function; if does not exist then send 400
    get_list_func = switch.get(
        list_name,
        lambda: ["Invalid List Name " + list_name, status.HTTP_400_BAD_REQUEST],
    )
    # get the data and the status result and return it
    [data, status_result] = get_list_func()
    return Response(data, status=status_result)


def get_employer_list(provided_id=None):
    if provided_id:
        query_list = CatRefEmployerStsVW.objects.filter(
            empsts_id=provided_id, active_flg=1
        )
    else:
        query_list = CatRefEmployerStsVW.objects.filter(active_flg=1)
    # returning data
    data = CatEmployerStsSerializer(query_list, many=True)
    return return_success(data)


def get_organisation_list(provided_id=None):
    if provided_id:
        query_list = CatRefDepartmentsVW.objects.filter(
            dept_id=provided_id, active_flg=1
        )
    else:
        query_list = CatRefDepartmentsVW.objects.filter(active_flg=1).distinct()

    # returning data
    data = CatRefDepartmentsSerializer(query_list, many=True)
    return return_success(data)


def get_group_and_subclassification_list(provided_id=None):
    if provided_id:
        query_list = CatRefClassificationVW.objects.filter(
            classif_id=provided_id, active_flg=1
        )
        # returning data
        data = CatRefClassificationSerializer(query_list, many=True)
        return return_success(data)
    else:
        query_list = CatRefClassificationVW.objects.filter(active_flg=1).order_by(
            "class_grp_cd"
        )
        # building group options
        group_options = []
        # looping in query_list
        for classification_data in query_list:
            if not any(
                x["group"] == classification_data.class_grp_cd for x in group_options
            ):
                group_options.append(
                    {
                        "classif_id": classification_data.classif_id,
                        "group": classification_data.class_grp_cd,
                    }
                )
        # sorting group options
        group_options.sort(key=lambda x: x["group"], reverse=False)

        # building group_levels options
        group_subclassifications = []
        # looping in group_options
        for group_data in group_options:
            # getting subclassifications associated to current group
            associated_subclassifications = CatRefClassificationVW.objects.filter(
                class_grp_cd=group_data["group"], active_flg=1
            ).values("classif_id", "class_grp_cd", "class_sbgrp_cd")
            # initializing subclassifications
            subclassifications = []

            null_subgroup_counter = 0
            # looping in associated_subclassifications
            for associated_subclassification in associated_subclassifications:
                if not any(
                    x["subclassification"]
                    == associated_subclassification["class_sbgrp_cd"]
                    for x in subclassifications
                ):
                    # if the subclassification code is null, the level codes are mapped to the parent group, and so we append the group code instead
                    if (
                        associated_subclassification["class_sbgrp_cd"] is None
                        and null_subgroup_counter == 0
                    ):
                        subclassifications.append(
                            {
                                "classif_id": associated_subclassification[
                                    "classif_id"
                                ],
                                "subclassification": associated_subclassification[
                                    "class_grp_cd"
                                ],
                            }
                        )
                        null_subgroup_counter += 1

                    elif associated_subclassification["class_sbgrp_cd"] is not None:
                        subclassifications.append(
                            {
                                "classif_id": associated_subclassification[
                                    "classif_id"
                                ],
                                "subclassification": associated_subclassification[
                                    "class_sbgrp_cd"
                                ],
                            }
                        )
            # populating group_subclassifications
            group_subclassifications.append(
                {"{0}".format(group_data["group"]): subclassifications}
            )

        # building data
        data = {
            "group_options": group_options,
            "group_subclassifications": group_subclassifications,
        }
        return [data, status.HTTP_200_OK]


def get_subclassification_and_level_list(provided_id=None):
    if provided_id:
        query_list = CatRefClassificationVW.objects.filter(
            classif_id=provided_id, active_flg=1
        )
        # returning data
        data = CatRefClassificationSerializer(query_list, many=True)
        return return_success(data)
    else:
        query_list = CatRefClassificationVW.objects.filter(active_flg=1).order_by(
            "class_grp_cd"
        )
        # building subclassification options
        subclassification_options = []

        # looping in query_list
        for subclassification_data in query_list:
            # ignore duplicate subclassification / subgroup codes
            if not any(
                x["subclassification"] == subclassification_data.class_sbgrp_cd
                for x in subclassification_options
            ):
                # if the subclassification/subgroup code is null, then the level codes are mapped directly to the parent group
                if subclassification_data.class_sbgrp_cd is None:
                    # since the subclassification is null, we replace with parent group and similarly ignore duplicates.
                    if not any(
                        x["subclassification"] == subclassification_data.class_grp_cd
                        for x in subclassification_options
                    ):
                        subclassification_options.append(
                            {
                                "classif_id": subclassification_data.classif_id,
                                "subclassification": subclassification_data.class_grp_cd,
                                "parent_group": subclassification_data.class_grp_cd,
                            }
                        )
                else:
                    subclassification_options.append(
                        {
                            "classif_id": subclassification_data.classif_id,
                            "subclassification": subclassification_data.class_sbgrp_cd,
                            "parent_group": None,
                        }
                    )

        # sorting subclassification options
        subclassification_options.sort(
            key=lambda x: x["subclassification"], reverse=False
        )

        # building subclassification_levels options
        subclassification_levels = []
        # looping in subclassification_options
        for current_subclassification in subclassification_options:
            # getting levels associated to current subclassification
            associated_levels = []

            # if the subclassification exists, retrieve its level mappings
            if current_subclassification["parent_group"] is None:
                associated_levels = CatRefClassificationVW.objects.filter(
                    class_sbgrp_cd=current_subclassification["subclassification"],
                    active_flg=1,
                ).values("classif_id", "class_lvl_cd")

            # else the subclassification is null, retrieve the parent groups' level mappings
            else:
                associated_levels = CatRefClassificationVW.objects.filter(
                    class_grp_cd=current_subclassification["parent_group"],
                    class_sbgrp_cd=None,
                    active_flg=1,
                ).values("classif_id", "class_lvl_cd")
            # initializing levels
            levels = []
            # looping in associated_levels
            for associated_level in associated_levels:
                levels.append(
                    {
                        "classif_id": associated_level["classif_id"],
                        "level": associated_level["class_lvl_cd"],
                    }
                )
            # populating subclassification_levels
            subclassification_levels.append(
                {"{0}".format(current_subclassification["subclassification"]): levels}
            )

        # building data
        data = {
            "subclassification_options": subclassification_options,
            "subclassification_levels": subclassification_levels,
        }
        return [data, status.HTTP_200_OK]


def get_group_and_level_list(provided_id=None):
    if provided_id:
        query_list = CatRefClassificationVW.objects.filter(
            classif_id=provided_id, active_flg=1
        )
        # returning data
        data = CatRefClassificationSerializer(query_list, many=True)
        return return_success(data)
    else:
        query_list = CatRefClassificationVW.objects.filter(active_flg=1).order_by(
            "class_grp_cd"
        )
        # building group options
        group_options = []
        # looping in query_list
        for classification_data in query_list:
            if not any(
                x["group"] == classification_data.class_grp_cd for x in group_options
            ):
                group_options.append(
                    {
                        "classif_id": classification_data.classif_id,
                        "group": classification_data.class_grp_cd,
                    }
                )
        # sorting group options
        group_options.sort(key=lambda x: x["group"], reverse=False)

        # building group_levels options
        group_levels = []
        # looping in group_options
        for group_data in group_options:
            # getting levels associated to current group
            associated_levels = CatRefClassificationVW.objects.filter(
                class_grp_cd=group_data["group"], active_flg=1
            ).values("classif_id", "class_lvl_cd")
            # initializing levels
            levels = []
            # looping in associated_levels
            for associated_level in associated_levels:
                levels.append(
                    {
                        "classif_id": associated_level["classif_id"],
                        "level": associated_level["class_lvl_cd"],
                    }
                )
            # populating group_levels
            group_levels.append({"{0}".format(group_data["group"]): levels})

        # building data
        data = {"group_options": group_options, "group_levels": group_levels}
        return [data, status.HTTP_200_OK]


def get_residence_list(provided_id=None):
    if provided_id:
        query_list = CatRefProvinceVW.objects.filter(prov_id=provided_id, active_flg=1)
    else:
        query_list = CatRefProvinceVW.objects.filter(active_flg=1)
    # returning data
    data = CatRefProvinceSerializer(query_list, many=True)
    return return_success(data)


def get_education_list(provided_id=None):
    if provided_id:
        query_list = CatEducation.objects.filter(education_id=provided_id, active_flg=1)
    else:
        query_list = CatEducation.objects.filter(active_flg=1)
    # returning data
    data = CatEducationSerializer(query_list, many=True)
    return return_success(data)


def get_gender_list(provided_id=None):
    if provided_id:
        query_list = CatRefGenderVW.objects.filter(gnd_id=provided_id, active_flg=1)
    else:
        query_list = CatRefGenderVW.objects.filter(active_flg=1)
    # returning data
    data = CatRefGenderSerializer(query_list, many=True)
    return return_success(data)


def get_aboriginal_list(provided_id=None):
    if provided_id:
        query_list = CatRefAboriginalVW.objects.filter(
            abrg_id=provided_id, active_flg=1
        )
    else:
        query_list = CatRefAboriginalVW.objects.filter(active_flg=1)
    # returning data
    data = CatAboriginalSerializer(query_list, many=True)
    return return_success(data)


def get_visible_minority_list(provided_id=None):
    if provided_id:
        query_list = CatRefVisibleMinorityVW.objects.filter(
            vismin_id=provided_id, active_flg=1
        )
    else:
        query_list = CatRefVisibleMinorityVW.objects.filter(active_flg=1)
    # returning data
    data = CatVisibleMinoritySerializer(query_list, many=True)
    return return_success(data)


def get_disability_list(provided_id=None):
    if provided_id:
        query_list = CatRefDisabilityVW.objects.filter(
            dsbl_id=provided_id, active_flg=1
        )
    else:
        query_list = CatRefDisabilityVW.objects.filter(active_flg=1)
    # returning data
    data = CatDisabilitySerializer(query_list, many=True)
    return return_success(data)


def return_success(data):
    return [data.data, status.HTTP_200_OK]
