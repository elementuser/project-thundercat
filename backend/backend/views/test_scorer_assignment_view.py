import urllib.parse
from rest_framework.views import APIView
from rest_framework import permissions, status
from rest_framework.response import Response
from django.db.models import Q
from backend.serializers.test_scorer_assignment_serializer import (
    GetTestScorerAssignmentSerializer,
)
from backend.custom_models.test_scorer_assignment import TestScorerAssignment
from backend.api_permissions.role_based_api_permissions import HasScorerPermission
from user_management.views.utils import CustomPagination
from cms.views.utils import is_undefined


# wrapper for get_test_scorer_assignment that handles the pagination needs
def get_paginated_test_scorer_assignment(scorer_username, request):
    page = request.query_params.get("page", None)
    page_size = request.query_params.get("page_size", None)
    # making sure that we have the needed parameters
    if is_undefined(page):
        return Response(
            {"error": "no 'page' parameter"}, status=status.HTTP_400_BAD_REQUEST
        )
    if is_undefined(page_size):
        return Response(
            {"error": "no 'page_size' parameter"}, status=status.HTTP_400_BAD_REQUEST
        )
    serialized_test_scorer_assignment = get_test_scorer_assignment(scorer_username)

    ordered_test_scorer_assignment = sorted(
        serialized_test_scorer_assignment,
        key=lambda k: k["assigned_test_submit_date"],
        reverse=False,
    )

    test_scorer_assignment_ids_array = []
    # looping in ordered active scorer assignments
    for i in ordered_test_scorer_assignment:
        # inserting scorer assignments ids (ordered respectively by test submission date) in an array
        test_scorer_assignment_ids_array.insert(
            len(test_scorer_assignment_ids_array), i["id"]
        )

    # sorting active scorer assignments queryset based on ordered (by test submission date) scorer assignments
    new_test_scorer_assignment = list(
        TestScorerAssignment.objects.filter(id__in=test_scorer_assignment_ids_array)
    )
    new_test_scorer_assignment.sort(
        key=lambda t: test_scorer_assignment_ids_array.index(t.id)
    )

    # handle pagination modifiers
    pagination_class = CustomPagination
    paginator = pagination_class()

    # getting page results based on queryset (new_test_scorer_assignment)
    page = paginator.paginate_queryset(new_test_scorer_assignment, request)
    # serializing the data
    serialized = GetTestScorerAssignmentSerializer(page, many=True)
    # retuning final data using pagination library
    return paginator.get_paginated_response(serialized.data)


# seperated out to make testing easier
def get_test_scorer_assignment(scorer_username):
    if scorer_username is None:
        test_scorer_assignment = TestScorerAssignment.objects.filter(
            scorer_username__isnull=True
        )
    else:
        test_scorer_assignment = TestScorerAssignment.objects.filter(
            Q(scorer_username=scorer_username)
            | Q(
                scorer_username__isnull=True
            )  # TODO drop the second half when ETTA assigns tests; also fix tests in test_test_scorer_assignment_view
        )
    return GetTestScorerAssignmentSerializer(test_scorer_assignment, many=True).data


def assign_test_to_scorer(request):
    scorer_assigned_test_id = request.query_params.get("scorer_assigned_test_id", None)
    scorer_username = request.query_params.get("scorer_username", None)
    # making sure that we have the needed parameters
    if is_undefined(scorer_assigned_test_id):
        return Response(
            {"error": "no 'scorer_assigned_test_id' parameter"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    if is_undefined(scorer_username):
        return Response(
            {"error": "no 'scorer_username' parameter"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    try:
        test_scorer_assignment = TestScorerAssignment.objects.get(
            id=scorer_assigned_test_id
        )
    except TestScorerAssignment.DoesNotExist:
        return Response(
            {"error": "no test scorer assignment object"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    test_scorer_assignment.scorer_username_id = scorer_username

    test_scorer_assignment.save()

    return Response(status=status.HTTP_200_OK)


class GetScorerAssignedTests(APIView):
    def get(self, request):
        scorer_username = request.query_params.get("scorer_username", None)

        # username is defined
        if scorer_username is not None:
            # decoding username that might contain special characters
            scorer_username = urllib.parse.unquote(scorer_username)

        if is_undefined(scorer_username):
            return Response(
                {"error": "no 'scorer_username' parameter"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        return get_paginated_test_scorer_assignment(scorer_username, request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasScorerPermission()]


class GetScorerUnassignedTests(APIView):
    def get(self, request):
        return get_paginated_test_scorer_assignment(None, request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasScorerPermission()]


class AssignTestToScorer(APIView):
    def get(self, request):
        return assign_test_to_scorer(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasScorerPermission()]
