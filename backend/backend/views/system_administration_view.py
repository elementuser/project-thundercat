from rest_framework.views import APIView
from rest_framework import permissions, status
from rest_framework.response import Response
from backend.views.get_all_assigned_candidates import (
    get_all_active_tests,
    get_found_active_tests,
    get_reason_for_invalidating_the_test,
)
from backend.api_permissions.role_based_api_permissions import HasSystemAdminPermission
from backend.views.system_administrator_actions import (
    system_administrator_actions,
    EttaActionsConstants,
)
from cms.views.utils import get_needed_parameters


class GetAllActiveTests(APIView):
    def get(self, request):
        return get_all_active_tests(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


class GetAllFoundActiveTests(APIView):
    def get(self, request):
        return get_found_active_tests(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


class invalidateCandidateAsETTA(APIView):
    def post(self, request):
        success, parameters = get_needed_parameters(
            ["username_id", "test_id", "invalidate_test_reason", "id"], request
        )
        if not success:
            return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

        return system_administrator_actions(
            parameters, EttaActionsConstants.INVALIDATE_CANDIDATE
        )

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


class validateCandidateAsETTA(APIView):
    def post(self, request):
        success, parameters = get_needed_parameters(
            ["username_id", "test_id", "validate_test_reason", "id"], request
        )
        if not success:
            return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

        return system_administrator_actions(
            parameters, EttaActionsConstants.VALIDATE_CANDIDATE
        )

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


class GetReasonForInvalidatingTheTest(APIView):
    def get(self, request):
        return get_reason_for_invalidating_the_test(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]
