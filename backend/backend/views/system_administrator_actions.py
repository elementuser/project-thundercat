from rest_framework.response import Response
from rest_framework import status
from backend.views.utils import handleViewedQuestionsLogic
from backend.custom_models.accommodation_request import AccommodationRequest
from backend.custom_models.break_bank_actions import BreakBankActions
from backend.views.utils import (
    get_new_break_bank_remaining_time,
    get_last_accessed_test_section,
)
from backend.custom_models.assigned_test import AssignedTest
from backend.custom_models.etta_actions import EttaActions
from backend.static.assigned_test_status import AssignedTestStatus
from user_management.user_management_models.user_models import User
from cms.cms_models.test_definition import TestDefinition
from cms.views.test_break_bank import BreakBankActionsConstants


# ETTA Actions Definition
class EttaActionsConstants:
    # TODO: update the term "unassign" for "invalidate" (will be tricky to do, since that's used as a foreign key in ettaactiontypes)
    INVALIDATE_CANDIDATE = "UN_ASSIGN_CANDIDATE"
    VALIDATE_CANDIDATE = "VALIDATE_CANDIDATE"


# Assigned Test Statuses Definition based on Selected Action (statuses to include in assigned test filter)
class CandidateAssignedTestStatuses:
    UN_ASSIGN_CANDIDATE = [
        AssignedTestStatus.SUBMITTED,
        AssignedTestStatus.UNASSIGNED,
        AssignedTestStatus.QUIT,
        AssignedTestStatus.ASSIGNED,
        AssignedTestStatus.READY,
        AssignedTestStatus.PRE_TEST,
        AssignedTestStatus.ACTIVE,
        AssignedTestStatus.LOCKED,
        AssignedTestStatus.PAUSED,
    ]
    VALIDATE_CANDIDATE = [
        AssignedTestStatus.SUBMITTED,
        AssignedTestStatus.UNASSIGNED,
        AssignedTestStatus.QUIT,
    ]


# generic function for system administrator actions (terminate candidate test at any state)
# needed_parameters must be in order in your request
def system_administrator_actions(parameters, action):
    try:
        # getting current assigned_test
        # using filter and first() to avoid error in case there is a user checked in to the same test twice
        assigned_test = AssignedTest.objects.get(
            id=parameters["id"],
            username_id=User.objects.get(username=parameters["username_id"]),
            test_id=TestDefinition.objects.get(id=parameters["test_id"]),
            status__in=(getattr(CandidateAssignedTestStatuses, action)),
        )

        if action == EttaActionsConstants.INVALIDATE_CANDIDATE:
            # initializing viewed_question_logic_already_called
            viewed_question_logic_already_called = False
            # test has been invalidated before
            if AssignedTest.history.filter(id=parameters["id"], is_invalid=True):
                # updating viewed_question_logic_already_called
                viewed_question_logic_already_called = True

            # if viewed_question_logic_already_called is still false
            if not viewed_question_logic_already_called:
                # test status is QUIT or SUBMIT (meaning that the logic has already been called from those actions)
                if (
                    assigned_test.status == AssignedTestStatus.QUIT
                    or assigned_test.status == AssignedTestStatus.SUBMITTED
                ):
                    # updating viewed_question_logic_already_called
                    viewed_question_logic_already_called = True

            # current status is ASSIGNED/READY/PRE-TEST
            if (
                assigned_test.status == AssignedTestStatus.ASSIGNED
                or assigned_test.status == AssignedTestStatus.READY
                or assigned_test.status == AssignedTestStatus.PRE_TEST
            ):
                # set status to UNASSIGNED
                assigned_test.status = AssignedTestStatus.UNASSIGNED
            # current status is LOCKED
            elif assigned_test.status == AssignedTestStatus.LOCKED:
                # previous status is READY/PRE-TEST
                if (
                    assigned_test.previous_status == AssignedTestStatus.READY
                    or assigned_test.previous_status == AssignedTestStatus.PRE_TEST
                ):
                    # set status to UNASSIGNED
                    assigned_test.status = AssignedTestStatus.UNASSIGNED
                else:
                    # set status to QUIT
                    assigned_test.status = AssignedTestStatus.QUIT
            # current status is PAUSED
            elif assigned_test.status == AssignedTestStatus.PAUSED:
                # getting new break bank remaining time
                new_remaining_time = get_new_break_bank_remaining_time(
                    assigned_test.accommodation_request_id
                )
                # getting last accessed assigned test section ID
                last_accessed_test_section_id = get_last_accessed_test_section(
                    parameters["id"]
                )
                # creating new UNPAUSE action in Break Bank Actions table
                BreakBankActions.objects.create(
                    action_type=BreakBankActionsConstants.UNPAUSE,
                    new_remaining_time=new_remaining_time,
                    break_bank_id=AccommodationRequest.objects.get(
                        id=assigned_test.accommodation_request_id
                    ).break_bank_id,
                    test_section_id=last_accessed_test_section_id,
                )
                # set status to QUIT
                assigned_test.status = AssignedTestStatus.QUIT
            # current status is SUBMITTED
            elif assigned_test.status == AssignedTestStatus.SUBMITTED:
                # make sure that the status does not change
                assigned_test.status = AssignedTestStatus.SUBMITTED
            else:
                # set status to QUIT
                assigned_test.status = AssignedTestStatus.QUIT

            # set is_invalid flag to true
            assigned_test.is_invalid = True
            # set submit_date to null
            assigned_test.submit_date = None
            assigned_test.save()

            # creating new entry in etta actions table
            etta_action = EttaActions.objects.create(
                action_type_id=EttaActionsConstants.INVALIDATE_CANDIDATE,
                assigned_test_id=assigned_test.id,
                action_reason=parameters["invalidate_test_reason"],
            )
            etta_action.save()

            # if viewed question logic has never been called before
            if not viewed_question_logic_already_called:
                # handling viewed questions logic
                handleViewedQuestionsLogic(assigned_test.id)
            return Response(status=status.HTTP_200_OK)

        elif action == EttaActionsConstants.VALIDATE_CANDIDATE:
            # set is_invalid flag to false
            assigned_test.is_invalid = False
            # set submit_date to historical's last submit_date
            # to bring back the fact that the test is now valid and has been submitted successfully
            if assigned_test.history.all().exclude(submit_date__isnull=True).last():
                assigned_test.submit_date = assigned_test.history.all().exclude(submit_date__isnull=True).last().submit_date
            assigned_test.save()

            # creating new entry in etta actions table
            etta_action = EttaActions.objects.create(
                action_type_id=EttaActionsConstants.VALIDATE_CANDIDATE,
                assigned_test_id=assigned_test.id,
                action_reason=parameters["validate_test_reason"],
            )
            etta_action.save()
            return Response(status=status.HTTP_200_OK)

        # no action found
        else:
            return Response(
                {"error": "'{}' action does not exist".format(action)},
                status=status.HTTP_400_BAD_REQUEST,
            )

    except AssignedTest.DoesNotExist:
        return Response(
            {
                "error": "the specified Assigned Test does not exist based on provided parameters"
            },
            status=status.HTTP_400_BAD_REQUEST,
        )
