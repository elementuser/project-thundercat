import datetime
import pytz
from django.contrib.auth import authenticate
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import permissions
from user_management.user_management_models.user_models import (
    User,
    NUM_OF_ATTEMPTS,
    LOCK_OUT_TIME,
)
from django.utils import timezone
from text_resources_backend.text_resources import TextResources


def create_simple_jwt(request):
    """
    Generates and returns the SimpleJWT token for the user from a request
    Also populates the token with some custom fields

    Reference: https://django-rest-framework-simplejwt.readthedocs.io/en/latest/creating_tokens_manually.html
    """

    username = request.data.get("username", None)

    # users can login with username or email
    user = User.objects.filter(email=username).first()
    if not user:
        user = User.objects.filter(username=username).first()

    # Get user info if the account exists
    if user:
        authenticate_kwargs = {
            "username": user.username,
            "password": request.data.get("password", None),
            "request": request,
        }

        if user.locked:
            if user.locked_until <= datetime.datetime.now(pytz.utc):
                # not authenticated yet
                if not authenticate(**authenticate_kwargs):
                    if user.login_attempts >= NUM_OF_ATTEMPTS:
                        # resetting login_attemps and locked only if the 4 hours has passed and the login_attempts field is greater or equal to 5
                        user.login_attempts = 0
                # after a successful login
                else:
                    # unlock account and update locked_until to Null
                    user.locked = False
                    user.locked_until = None

            else:
                return Response(
                    TextResources.lockedAccount, status=status.HTTP_400_BAD_REQUEST
                )

        # user is not locked
        else:
            # verify not null
            if user.last_login_attempt is not None:
                # if current time is more than the last login attempt + lock time
                if datetime.datetime.now(
                    pytz.utc
                ) > user.last_login_attempt + datetime.timedelta(
                    hours=LOCK_OUT_TIME.hours, minutes=LOCK_OUT_TIME.minutes
                ):
                    # resetting login_attempts to 0
                    user.login_attempts = 0

        if not authenticate(**authenticate_kwargs):
            user.login_attempts = user.login_attempts + 1

            # update last login attempt to current time
            user.last_login_attempt = datetime.datetime.now(pytz.utc)
            user.save()

            # lock the user out
            if user.login_attempts >= NUM_OF_ATTEMPTS:
                user.locked = True
                user.locked_until = datetime.datetime.now(
                    pytz.utc
                ) + datetime.timedelta(
                    hours=LOCK_OUT_TIME.hours, minutes=LOCK_OUT_TIME.minutes
                )
                user.save()
                return Response(
                    TextResources.lockedAccount, status=status.HTTP_400_BAD_REQUEST
                )

            user.save()
            return Response(
                TextResources.invalidLogin, status=status.HTTP_400_BAD_REQUEST
            )

        try:
            token = RefreshToken.for_user(user)
            # Customize token fields
            token["username"] = username
            last_login = user.last_login
            user.login_attempts = 0
            user.save()
            return Response(
                {
                    "refresh": str(token),
                    "access": str(token.access_token),
                    "last_login": str(last_login),
                },
                status=status.HTTP_200_OK,
            )
        finally:
            user.last_login = datetime.datetime.now(pytz.utc)
            user.save()

    # Return a bad response if the user does not exist
    else:
        return Response(TextResources.invalidLogin, status=status.HTTP_400_BAD_REQUEST)


class CreateJWTtoken(APIView):
    def get_permissions(self):
        return [permissions.AllowAny()]

    # API call
    def post(self, request):
        return create_simple_jwt(request)
