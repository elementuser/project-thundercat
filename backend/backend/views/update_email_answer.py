from rest_framework.response import Response
from rest_framework import status
from backend.static.update_email_answer_type import UpdateEmailAnswerType
from backend.custom_models.assigned_test import AssignedTest
from backend.static.assigned_test_status import AssignedTestStatus
from backend.custom_models.candidate_email_response_answers import (
    CandidateEmailResponseAnswers,
)
from backend.custom_models.candidate_answers import CandidateAnswers
from backend.custom_models.candidate_task_response_answers import (
    CandidateTaskResponseAnswers,
)
from cms.cms_models.new_question import NewQuestion

from cms.cms_models.address_book_contact import AddressBookContact
from user_management.user_management_models.user_models import User


def email_answer_controller(user, parameters):
    # check if the user has an active test
    user = User.objects.get(username=user["username"])
    assigned_test = AssignedTest.objects.get(
        id=parameters["assignedTestId"], username=user.username
    )
    # make sure the test is active
    if assigned_test.status != AssignedTestStatus.ACTIVE:
        return Response(
            {"error": "this users test is not active"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    if parameters["OPERATION"] == UpdateEmailAnswerType.ADD_EMAIL:
        add_email_answer(parameters, assigned_test)

    elif parameters["OPERATION"] == UpdateEmailAnswerType.UPDATE_EMAIL:
        update_email_answer(parameters, assigned_test)

    elif parameters["OPERATION"] == UpdateEmailAnswerType.DELETE_EMAIL:
        delete_email_answer(parameters, assigned_test)

    elif parameters["OPERATION"] == UpdateEmailAnswerType.ADD_TASK:
        add_task_answer(parameters, assigned_test)

    elif parameters["OPERATION"] == UpdateEmailAnswerType.UPDATE_TASK:
        update_task_answer(parameters, assigned_test)

    elif parameters["OPERATION"] == UpdateEmailAnswerType.DELETE_TASK:
        delete_task_answer(parameters, assigned_test)

    return Response({"message": "received"}, status=status.HTTP_200_OK)


def add_email_answer(parameters, assigned_test):
    # get test section question
    question = NewQuestion.objects.get(id=parameters["questionId"])
    candidate_answer = CandidateAnswers.objects.get(
        assigned_test=assigned_test, question=question
    )

    # create an email answer from given parameters
    email_response = CandidateEmailResponseAnswers(
        candidate_answers=candidate_answer,
        answer_id=parameters["answerId"],
        email_type=parameters["answerObj"]["emailType"],
        email_body=parameters["answerObj"]["emailBody"],
        reasons_for_action=parameters["answerObj"]["reasonsForAction"],
    )

    # save email answer
    email_response.save()

    # add contacts to email to
    for contact in parameters["answerObj"]["emailTo"]:
        email_response.email_to.add(AddressBookContact.objects.get(id=contact["id"]))

    for contact in parameters["answerObj"]["emailCc"]:
        email_response.email_cc.add(AddressBookContact.objects.get(id=contact["id"]))

    return Response(status=status.HTTP_200_OK)


def update_email_answer(parameters, assigned_test):
    # get test section question
    question = NewQuestion.objects.get(id=parameters["questionId"])
    candidate_answer = CandidateAnswers.objects.get(
        assigned_test=assigned_test, question=question
    )

    # get an email answer from given parameters
    email_response = CandidateEmailResponseAnswers.objects.get(
        candidate_answers=candidate_answer, answer_id=parameters["answerId"]
    )
    email_response = CandidateEmailResponseAnswers(
        id=email_response.id,
        candidate_answers=candidate_answer,
        answer_id=parameters["answerId"],
        email_type=parameters["answerObj"]["emailType"],
        email_body=parameters["answerObj"]["emailBody"],
        reasons_for_action=parameters["answerObj"]["reasonsForAction"],
    )

    # save email answer
    email_response.save()

    # clear all contacts and add new ones
    email_response.email_to.clear()
    email_response.email_cc.clear()

    # add contacts to email to
    for contact in parameters["answerObj"]["emailTo"]:
        email_response.email_to.add(AddressBookContact.objects.get(id=contact["id"]))

    for contact in parameters["answerObj"]["emailCc"]:
        email_response.email_cc.add(AddressBookContact.objects.get(id=contact["id"]))

    return Response(status=status.HTTP_200_OK)


def delete_email_answer(parameters, assigned_test):
    # get test section question
    question = NewQuestion.objects.get(id=parameters["questionId"])
    candidate_answer = CandidateAnswers.objects.get(
        assigned_test=assigned_test, question=question
    )

    email_response = CandidateEmailResponseAnswers.objects.get(
        candidate_answers=candidate_answer, answer_id=parameters["answerId"]
    )

    # clear related manager
    email_response.email_to.clear()
    email_response.email_cc.clear()

    # delete email answer
    email_response.delete()
    # return
    return


def add_task_answer(parameters, assigned_test):
    # get test section question
    question = NewQuestion.objects.get(id=parameters["questionId"])
    candidate_answer = CandidateAnswers.objects.get(
        assigned_test=assigned_test, question=question
    )

    # create an email answer from given parameters
    task_response = CandidateTaskResponseAnswers(
        candidate_answers=candidate_answer,
        answer_id=parameters["answerId"],
        task=parameters["answerObj"]["task"],
        reasons_for_action=parameters["answerObj"]["reasonsForAction"],
    )

    # save task answer
    task_response.save()

    return Response(status=status.HTTP_200_OK)


def update_task_answer(parameters, assigned_test):
    # get test section question
    question = NewQuestion.objects.get(id=parameters["questionId"])
    candidate_answer = CandidateAnswers.objects.get(
        assigned_test=assigned_test, question=question
    )

    # get a task answer from given parameters
    task_response = CandidateTaskResponseAnswers.objects.get(
        candidate_answers=candidate_answer, answer_id=parameters["answerId"]
    )
    task_response = CandidateTaskResponseAnswers(
        id=task_response.id,
        candidate_answers=candidate_answer,
        answer_id=parameters["answerId"],
        task=parameters["answerObj"]["task"],
        reasons_for_action=parameters["answerObj"]["reasonsForAction"],
    )

    # save task answer
    task_response.save()

    return Response(status=status.HTTP_200_OK)


def delete_task_answer(parameters, assigned_test):
    # get test section question
    question = NewQuestion.objects.get(id=parameters["questionId"])
    candidate_answer = CandidateAnswers.objects.get(
        assigned_test=assigned_test, question=question
    )

    task_response = CandidateTaskResponseAnswers.objects.get(
        candidate_answers=candidate_answer, answer_id=parameters["answerId"]
    )

    # delete email answer
    task_response.delete()
    # return
    return
