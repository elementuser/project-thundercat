from rest_framework.views import APIView
from rest_framework import permissions
from backend.views.retrieve_tests_data import (
    retrieve_scored_tests,
    retrieve_All_tests,
)
from backend.api_permissions.role_based_api_permissions import (
    HasSystemAdminPermission,
)


class AssignedScoredTests(APIView):
    def get(self, request):
        return retrieve_scored_tests(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class GetAllTestsForSelectedUser(APIView):
    def get(self, request):
        return retrieve_All_tests(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]
