# This file is a collection of all possilble candidate testing process test statuses.


class AssignedTestStatus:
    # candidate testing process (from 11 to 30)
    # if you update these statuses, don't forget to also update ".../components/ta/Constants.jsx" file (frontend)
    ASSIGNED = 11  # "Assigned to a Candidate"
    READY = 12  # "Approved from Responsible TA"
    ACTIVE = 13  # "Testing (timed sections)"
    LOCKED = 14  # "Test Locked"
    PAUSED = 15  # "Test Paused"
    QUIT = 19  # "User Quit"
    SUBMITTED = 20  # "Submitted"
    UNASSIGNED = 21  # "un-assigned by TA (user never got access to the test, meaning that he was never able to see the content)"
    PRE_TEST = 22  # "Pre-Test (instructions)"
    # scoring process (from 31 to 39)
    # TODO: add needed statuses (start at 31)


# Util for Printing AssignedTestStatus - String
def get_string_assigned_test_status(status):
    if status == AssignedTestStatus.ASSIGNED:
        return "AssignedTestStatus.ASSIGNED"
    elif status == AssignedTestStatus.READY:
        return "AssignedTestStatus.READY"
    elif status == AssignedTestStatus.ACTIVE:
        return "AssignedTestStatus.ACTIVE"
    elif status == AssignedTestStatus.LOCKED:
        return "AssignedTestStatus.LOCKED"
    elif status == AssignedTestStatus.PAUSED:
        return "AssignedTestStatus.PAUSED"
    elif status == AssignedTestStatus.QUIT:
        return "AssignedTestStatus.QUIT"
    elif status == AssignedTestStatus.SUBMITTED:
        return "AssignedTestStatus.SUBMITTED"
    elif status == AssignedTestStatus.UNASSIGNED:
        return "AssignedTestStatus.UNASSIGNED"
    elif status == AssignedTestStatus.PRE_TEST:
        return "AssignedTestStatus.PRE_TEST"
