# This file is a collection of all scoring types


class TestSectionScoringType:
    NOT_SCORABLE = 0
    AUTO_SCORE = 1
    COMPETENCY = 2  # eMIB style scoring


# mirror of
# Constants.js in frontend/src/components/testFactory
