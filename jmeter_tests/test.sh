#!/bin/bash

# Author(s): William Lam

# ======
# Info
# ======
# This script greatly simplifies running the JMeter performance test & then creating the HTML report


# Remove generated files if they already exist (JMeter will through an error otherwise and won't run)
rm cat_test_outputs.jtl
rm -rf html_report

# Run JMeter in headless (non-GUI) mode for test results accuracy, and then generate an HTML report folder afterwards
jmeter -n -Jjmeterengine.force.system.exit=true -t "cat_test_plan.jmx" -l "cat_test_outputs.jtl" -e -o "html_report"