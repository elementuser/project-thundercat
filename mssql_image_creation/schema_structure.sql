USE [master]
GO
/****** Object:  UserDefinedFunction [dbo].[status2str]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   FUNCTION [dbo].[status2str] 
( 
	@lang_code varchar(2),
	@status_id numeric
)
RETURNS varchar(100)
AS 
BEGIN
	DECLARE @Result		varchar(100)
    IF (@lang_code = 'fr')
		BEGIN
			SELECT @Result	= CASE 
			WHEN @status_id = 11 THEN 'ATTRIBUÉ'
			WHEN @status_id = 12 THEN 'PRÊT'
			WHEN @status_id = 13 THEN 'ACTIF'
			WHEN @status_id = 14 THEN 'VERROUILLÉ'
			WHEN @status_id = 15 THEN 'EN PAUSE'			
			WHEN @status_id = 19 THEN 'ABANDONNÉ'
			WHEN @status_id = 20 THEN 'SOUMIS'
			WHEN @status_id = 21 THEN 'NON ATTRIBUÉ'
			WHEN @status_id = 22 THEN 'PRÉ_TEST'
			ELSE CAST(@status_id AS varchar(4))
			END
		END
	ELSE
		BEGIN
			SELECT @Result	= CASE 
			WHEN @status_id = 11 THEN 'ASSIGNED'
			WHEN @status_id = 12 THEN 'READY'
			WHEN @status_id = 13 THEN 'ACTIVE'
			WHEN @status_id = 14 THEN 'LOCKED'
			WHEN @status_id = 15 THEN 'PAUSED'			
			WHEN @status_id = 19 THEN 'QUIT'
			WHEN @status_id = 20 THEN 'SUBMITTED'
			WHEN @status_id = 21 THEN 'UNASSIGNED'
			WHEN @status_id = 22 THEN 'PRE_TEST'
			ELSE CAST(@status_id AS varchar(4))
			END
		END
	RETURN @Result
END
GO
/****** Object:  Table [dbo].[cms_models_bundlerule]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_bundlerule](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[bundle_id] [int] NOT NULL,
	[rule_type_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_bundlebundlesruleassociations]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_bundlebundlesruleassociations](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[display_order] [int] NOT NULL,
	[bundle_id] [int] NOT NULL,
	[bundle_bundles_rule_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_bundlebundlesrule]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_bundlebundlesrule](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[number_of_bundles] [int] NULL,
	[shuffle_bundles] [bit] NOT NULL,
	[keep_items_together] [bit] NOT NULL,
	[bundle_rule_id] [int] NOT NULL,
	[display_order] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_bundles]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_bundles](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[version_text] [nvarchar](50) NULL,
	[shuffle_items] [bit] NOT NULL,
	[shuffle_bundles] [bit] NOT NULL,
	[active] [bit] NOT NULL,
	[item_bank_id] [int] NOT NULL,
	[shuffle_between_bundles] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [bundle_name_must_be_unique_in_item_bank] UNIQUE NONCLUSTERED 
(
	[name] ASC,
	[item_bank_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_bundleitemsassociation]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_bundleitemsassociation](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[system_id_link] [nvarchar](15) NOT NULL,
	[version] [int] NULL,
	[display_order] [int] NOT NULL,
	[bundle_source_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_bundlebundlesassociation]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_bundlebundlesassociation](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[display_order] [int] NOT NULL,
	[bundle_link_id] [int] NOT NULL,
	[bundle_source_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[bundle_data_vw]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[bundle_data_vw] AS
                SELECT * FROM
                    (SELECT
                        b.id,
                        b.name,
                        b.version_text,
                        b.shuffle_items,
                        b.shuffle_bundles,
                        b.shuffle_between_bundles,
                        b.active,
                        b.item_bank_id,
                        bundle_items_data.item_count
                    FROM master..cms_models_bundles b
                    OUTER APPLY (SELECT count(id) as 'item_count' 
                                FROM master..cms_models_bundleitemsassociation bia 
                                WHERE bia.bundle_source_id= b.id
                                ) bundle_items_data
                    ) a

                    OUTER APPLY (SELECT TOP 1 * FROM (
                                                SELECT count(bbra.id) as 'bundle_count' 
                                                    FROM master..cms_models_bundlebundlesruleassociations bbra 
                                                    JOIN master..cms_models_bundlerule br on br.bundle_id = a.id
                                                    JOIN master..cms_models_bundlebundlesrule bbr on bbr.bundle_rule_id = br.id
                                                    WHERE bbra.bundle_bundles_rule_id = bbr.id
                                                UNION 
                                                SELECT count(bundle_source_id) as 'bundle_count' 
                                                    FROM master..cms_models_bundlebundlesassociation bba 
                                                    WHERE bba.bundle_source_id= a.id
                                                ) bundle_rule_data
                                ORDER BY bundle_count DESC
                                ) bundle_count
        
GO
/****** Object:  Table [dbo].[cms_models_bundleruletype]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_bundleruletype](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[codename] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[bundle_bundles_rule_data_vw]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[bundle_bundles_rule_data_vw] AS
                SELECT 
                    br.id,
                    br.bundle_id,
                    bbr.id as 'bundle_bundles_rule_id',
                    bbr.number_of_bundles,
                    bbr.shuffle_bundles,
                    bbr.keep_items_together,
                    bbr.display_order as 'bundle_bundles_rule_display_order',
                    bbra.id as 'bundle_bundles_rule_associations_id',
                    bbra.bundle_id as 'bundle_bundles_rule_associations_bundle_id',
                    bbra.display_order
                FROM master..cms_models_bundlerule br
                JOIN master..cms_models_bundlebundlesrule bbr on bbr.bundle_rule_id = br.id
                JOIN master..cms_models_bundlebundlesruleassociations bbra on bbra.bundle_bundles_rule_id = bbr.id
                WHERE br.rule_type_id = (SELECT id from master..cms_models_bundleruletype WHERE codename = 'bundle_bundles_rule')
        
GO
/****** Object:  Table [dbo].[cms_models_historicaltestpermissions]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicaltestpermissions](
	[id] [int] NOT NULL,
	[date_assigned] [datetime2](7) NOT NULL,
	[expiry_date] [date] NOT NULL,
	[test_order_number] [nvarchar](12) NOT NULL,
	[staffing_process_number] [nvarchar](50) NOT NULL,
	[department_ministry_code] [nvarchar](10) NOT NULL,
	[is_org] [nvarchar](16) NOT NULL,
	[is_ref] [nvarchar](20) NOT NULL,
	[billing_contact] [nvarchar](180) NOT NULL,
	[billing_contact_info] [nvarchar](255) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[test_id] [int] NULL,
	[username_id] [nvarchar](150) NULL,
	[reason_for_modif_or_del] [nvarchar](300) NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_historicaluser]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_historicaluser](
	[id] [int] NOT NULL,
	[password] [nvarchar](128) NOT NULL,
	[is_superuser] [bit] NOT NULL,
	[username] [nvarchar](150) NOT NULL,
	[email] [nvarchar](254) NOT NULL,
	[is_staff] [bit] NOT NULL,
	[is_active] [bit] NOT NULL,
	[date_joined] [datetime2](7) NOT NULL,
	[secondary_email] [nvarchar](254) NULL,
	[first_name] [nvarchar](30) NOT NULL,
	[last_name] [nvarchar](150) NOT NULL,
	[birth_date] [date] NOT NULL,
	[pri] [nvarchar](10) NULL,
	[last_password_change] [date] NULL,
	[psrs_applicant_id] [nvarchar](8) NULL,
	[last_login] [datetime2](7) NULL,
	[last_login_attempt] [datetime2](7) NULL,
	[login_attempts] [int] NOT NULL,
	[locked] [bit] NOT NULL,
	[locked_until] [datetime2](7) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[is_profile_complete] [bit] NOT NULL,
	[last_profile_update] [datetime2](7) NULL,
	[military_service_nbr] [nvarchar](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_testdefinition]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_testdefinition](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[test_code] [nvarchar](25) NOT NULL,
	[version] [int] NOT NULL,
	[en_name] [nvarchar](150) NOT NULL,
	[fr_name] [nvarchar](150) NOT NULL,
	[is_public] [bit] NOT NULL,
	[active] [bit] NOT NULL,
	[parent_code] [nvarchar](25) NOT NULL,
	[retest_period] [int] NOT NULL,
	[archived] [bit] NOT NULL,
	[version_notes] [nvarchar](500) NOT NULL,
	[count_up] [bit] NOT NULL,
	[is_uit] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_orderlessfinancialdata]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_orderlessfinancialdata](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[reference_number] [nvarchar](25) NOT NULL,
	[department_ministry_id] [int] NOT NULL,
	[fis_organisation_code] [nvarchar](16) NOT NULL,
	[fis_reference_code] [nvarchar](20) NOT NULL,
	[billing_contact_name] [nvarchar](180) NOT NULL,
	[billing_contact_info] [nvarchar](255) NOT NULL,
	[level_required] [nvarchar](50) NULL,
	[reason_for_testing_id] [int] NULL,
	[uit_invite_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_assignedtest]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_assignedtest](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[status] [int] NOT NULL,
	[previous_status] [int] NULL,
	[start_date] [datetime2](7) NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[submit_date] [datetime2](7) NULL,
	[test_access_code] [nvarchar](10) NULL,
	[total_score] [int] NULL,
	[ta_id] [nvarchar](150) NULL,
	[test_id] [int] NOT NULL,
	[test_section_id] [int] NULL,
	[test_session_language_id] [int] NULL,
	[username_id] [nvarchar](150) NOT NULL,
	[test_order_number] [nvarchar](12) NULL,
	[en_converted_score] [nvarchar](50) NULL,
	[fr_converted_score] [nvarchar](50) NULL,
	[is_invalid] [bit] NOT NULL,
	[uit_invite_id] [int] NULL,
	[orderless_financial_data_id] [int] NULL,
	[accommodation_request_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [must_be_a_unique_status_username_tac_test_combination] UNIQUE NONCLUSTERED 
(
	[status] ASC,
	[username_id] ASC,
	[test_access_code] ASC,
	[test_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_assignedtestsection]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_assignedtestsection](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[test_section_time] [int] NULL,
	[score] [int] NULL,
	[assigned_test_id] [int] NOT NULL,
	[test_section_id] [int] NOT NULL,
	[timed_out] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[active_tests_vw]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[active_tests_vw] AS
				SELECT
					at.id as 'assigned_test_id',
					u.first_name as 'candidate_first_name',
					u.last_name as 'candidate_last_name',
                    u.username as 'candidate_username',
					u.email as 'candidate_email',
					ta.email as 'ta_id',
					ta.first_name as 'ta_first_name',
					ta.last_name as 'ta_last_name',
					at.status as 'test_status',
					tp.test_order_number,
					ofd.reference_number,
					ats.time_limit,
                    td.id as 'test_id',
                    td.test_code
				FROM master..custom_models_assignedtest at
				OUTER APPLY (SELECT TOP 1 * FROM master..user_management_models_historicaluser u1 WHERE u1.username = at.username_id ORDER BY u1.history_date DESC) u
                OUTER APPLY (SELECT TOP 1 * FROM master..user_management_models_historicaluser u1 WHERE u1.username = at.ta_id ORDER BY u1.history_date DESC) ta
				OUTER APPLY (SELECT TOP 1 * FROM master..cms_models_historicaltestpermissions tp1 WHERE tp1.test_order_number = at.test_order_number AND tp1.username_id = at.ta_id ORDER BY tp1.history_date DESC) tp
				LEFT JOIN master..custom_models_orderlessfinancialdata ofd on ofd.id = at.orderless_financial_data_id
				OUTER APPLY (SELECT SUM(ats1.test_section_time) as 'time_limit' FROM master..custom_models_assignedtestsection ats1 WHERE ats1.assigned_test_id = at.id) ats
                LEFT JOIN master..cms_models_testdefinition td on td.id = at.test_id
				WHERE at.status in (11, 12, 13, 14, 15, 22)
        
GO
/****** Object:  Table [dbo].[user_management_models_user]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_user](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[password] [nvarchar](128) NOT NULL,
	[last_login] [datetime2](7) NULL,
	[is_superuser] [bit] NOT NULL,
	[username] [nvarchar](150) NOT NULL,
	[email] [nvarchar](254) NOT NULL,
	[is_staff] [bit] NOT NULL,
	[is_active] [bit] NOT NULL,
	[date_joined] [datetime2](7) NOT NULL,
	[first_name] [nvarchar](30) NOT NULL,
	[last_name] [nvarchar](150) NOT NULL,
	[birth_date] [date] NOT NULL,
	[pri] [nvarchar](10) NULL,
	[secondary_email] [nvarchar](254) NULL,
	[last_password_change] [date] NULL,
	[psrs_applicant_id] [nvarchar](8) NULL,
	[locked] [bit] NOT NULL,
	[locked_until] [datetime2](7) NULL,
	[login_attempts] [int] NOT NULL,
	[last_login_attempt] [datetime2](7) NULL,
	[is_profile_complete] [bit] NOT NULL,
	[last_profile_update] [datetime2](7) NULL,
	[military_service_nbr] [nvarchar](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[user_look_up_vw]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[user_look_up_vw] AS
				SELECT
                    u.id as 'user_id',
                    u.first_name,
                    u.last_name,
                    u.birth_date as 'date_of_birth',
                    u.email,
                    u.username
                FROM master..user_management_models_user u
        
GO
/****** Object:  Table [dbo].[custom_models_testaccesscode]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_testaccesscode](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[test_access_code] [nvarchar](10) NOT NULL,
	[test_order_number] [nvarchar](12) NULL,
	[ta_username_id] [nvarchar](150) NULL,
	[test_id] [int] NOT NULL,
	[test_session_language_id] [int] NULL,
	[created_date] [date] NOT NULL,
	[orderless_financial_data_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[test_access_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_historicalcustomuserpermissions]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_historicalcustomuserpermissions](
	[user_permission_id] [int] NOT NULL,
	[goc_email] [nvarchar](254) NOT NULL,
	[pri_or_military_nbr] [nvarchar](10) NOT NULL,
	[supervisor] [nvarchar](180) NOT NULL,
	[supervisor_email] [nvarchar](254) NOT NULL,
	[rationale] [nvarchar](300) NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[permission_id] [int] NULL,
	[user_id] [nvarchar](150) NULL,
	[reason_for_modif_or_del] [nvarchar](300) NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_unsupervisedtestaccesscode]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_unsupervisedtestaccesscode](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[test_access_code] [nvarchar](10) NOT NULL,
	[test_order_number] [nvarchar](12) NULL,
	[candidate_email] [nvarchar](254) NULL,
	[created_date] [date] NOT NULL,
	[validity_end_date] [date] NULL,
	[ta_username_id] [nvarchar](150) NULL,
	[test_id] [int] NOT NULL,
	[uit_invite_id] [int] NOT NULL,
	[orderless_financial_data_id] [int] NULL,
	[accommodation_request_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[test_access_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[bo_test_access_codes_vw]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[bo_test_access_codes_vw] AS
				SELECT
	tac.test_access_code,
	tac.test_order_number,
	ofd.reference_number,
	tac.created_date,
	ta.username as 'ta_username',
	ta.email as 'ta_email',
	ta.first_name as 'ta_first_name',
	ta.last_name as 'ta_last_name',
    up.goc_email as 'ta_goc_email',
	td.id as 'test_id',
	td.en_name as 'en_test_name',
	td.fr_name as 'fr_test_name'
  FROM master..custom_models_testaccesscode tac
  OUTER APPLY (SELECT TOP 1 * FROM master..user_management_models_historicaluser u1 WHERE u1.username = tac.ta_username_id ORDER BY u1.history_date DESC) ta
  JOIN master..cms_models_testdefinition td on td.id = tac.test_id
  LEFT JOIN master..custom_models_orderlessfinancialdata ofd on ofd.id = tac.orderless_financial_data_id
  OUTER APPLY (SELECT TOP 1 * FROM master..user_management_models_historicalcustomuserpermissions up1 WHERE up1.user_id = tac.ta_username_id AND up1.history_date < tac.created_date ORDER BY up1.history_date DESC) up

  UNION

  SELECT 
	utac.test_access_code,
	utac.test_order_number,
	ofd.reference_number,
	utac.created_date,
	ta.username as 'ta_username',
	ta.email as 'ta_email',
	ta.first_name as 'ta_first_name',
	ta.last_name as 'ta_last_name',
    up.goc_email as 'ta_goc_email',
	td.id as 'test_id',
	td.en_name as 'en_test_name',
	td.fr_name as 'fr_test_name'
  FROM master..custom_models_unsupervisedtestaccesscode utac
  OUTER APPLY (SELECT TOP 1 * FROM master..user_management_models_historicaluser u1 WHERE u1.username = utac.ta_username_id ORDER BY u1.history_date DESC) ta
  JOIN master..cms_models_testdefinition td on td.id = utac.test_id
  LEFT JOIN master..custom_models_orderlessfinancialdata ofd on ofd.id = utac.orderless_financial_data_id
  OUTER APPLY (SELECT TOP 1 * FROM master..user_management_models_historicalcustomuserpermissions up1 WHERE up1.user_id = utac.ta_username_id AND up1.history_date < utac.created_date ORDER BY up1.history_date DESC) up
        
GO
/****** Object:  View [dbo].[bundle_association_vw]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[bundle_association_vw] AS
                SELECT 
                    NULL as 'bundle_bundles_association_id',
                    bia.id as 'bundle_items_association_id', 
                    bia.bundle_source_id,
                    bia.system_id_link as 'system_id_link',
                    bia.version as 'version',
                    NULL as 'bundle_link_id',
                    bia.display_order
                FROM master..cms_models_bundleitemsassociation bia
                UNION
                SELECT 
                    bba.id as 'bundle_bundles_association_id',
                    NULL as 'bundle_items_association_id',
                    bba.bundle_source_id,
                    NULL as 'system_id_link',
                    NULL as 'version',
                    bba.bundle_link_id as 'bundle_link_id',
                    bba.display_order 
                FROM master..cms_models_bundlebundlesassociation bba
        
GO
/****** Object:  Table [dbo].[cms_models_itemdevelopmentstatuses]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itemdevelopmentstatuses](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[en_name] [nvarchar](25) NOT NULL,
	[fr_name] [nvarchar](25) NOT NULL,
	[codename] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [must_be_a_unique_item_development_status_codename] UNIQUE NONCLUSTERED 
(
	[codename] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_itemresponseformats]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itemresponseformats](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[en_name] [nvarchar](25) NOT NULL,
	[fr_name] [nvarchar](25) NOT NULL,
	[codename] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [must_be_a_unique_item_response_format_codename] UNIQUE NONCLUSTERED 
(
	[codename] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_items]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_items](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[system_id] [nvarchar](15) NOT NULL,
	[version] [int] NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[active_status] [bit] NOT NULL,
	[development_status_id] [int] NOT NULL,
	[last_modified_by_username_id] [nvarchar](150) NOT NULL,
	[response_format_id] [int] NOT NULL,
	[item_bank_id] [int] NOT NULL,
	[shuffle_options] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [system_id_combined_with_version_must_be_unique_in_item_bank] UNIQUE NONCLUSTERED 
(
	[system_id] ASC,
	[version] ASC,
	[item_bank_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_itemhistoricalids]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itemhistoricalids](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[historical_id] [nvarchar](50) NOT NULL,
	[item_bank_id] [int] NOT NULL,
	[system_id] [nvarchar](15) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [must_be_a_unique_item_historical_id_and_item_bank_combination] UNIQUE NONCLUSTERED 
(
	[historical_id] ASC,
	[item_bank_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[item_latest_versions_data_vw]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[item_latest_versions_data_vw] AS
				SELECT
					i.id as 'item_id',
					i.system_id,
					i.version,
					v.number_of_versions as 'number_of_versions',
					i.item_bank_id,
					i.modify_date,
					i.last_modified_by_username_id,
					i.active_status,
                    i.shuffle_options,
					ds.id as 'development_status_id',
					ds.codename as 'development_status_codename',
					ds.en_name as 'development_status_name_en',
					ds.fr_name as 'development_status_name_fr',
					rf.id as 'response_format_id',
					rf.codename as 'response_format_codename',
					rf.en_name as 'response_format_name_en',
					rf.fr_name as 'response_format_name_fr',
					IIF (hi.id is not NULL, hi.historical_id, NULL) as 'historical_id'
				FROM master..cms_models_items i
				JOIN master..cms_models_itemdevelopmentstatuses ds on ds.id = i.development_status_id
				JOIN master..cms_models_itemresponseformats rf on rf.id = i.response_format_id
				LEFT JOIN master..cms_models_itemhistoricalids hi on hi.system_id = i.system_id
				OUTER APPLY (SELECT MAX(version) as 'number_of_versions' FROM master..cms_models_items it WHERE it.system_id = i.system_id) v
				WHERE i.id in (SELECT MAX(it.id) FROM master..cms_models_items it GROUP BY it.system_id)
        
GO
/****** Object:  View [dbo].[all_items_data_vw]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[all_items_data_vw] AS
				SELECT
					i.id as 'item_id',
					i.system_id,
					i.version,
					v.number_of_versions as 'number_of_versions',
					i.item_bank_id,
					i.modify_date,
					i.last_modified_by_username_id,
					i.active_status,
                    i.shuffle_options,
					ds.id as 'development_status_id',
					ds.codename as 'development_status_codename',
					ds.en_name as 'development_status_name_en',
					ds.fr_name as 'development_status_name_fr',
					rf.id as 'response_format_id',
					rf.codename as 'response_format_codename',
					rf.en_name as 'response_format_name_en',
					rf.fr_name as 'response_format_name_fr',
					IIF (hi.id is not NULL, hi.historical_id, NULL) as 'historical_id'
				FROM master..cms_models_items i
				JOIN master..cms_models_itemdevelopmentstatuses ds on ds.id = i.development_status_id
				JOIN master..cms_models_itemresponseformats rf on rf.id = i.response_format_id
				LEFT JOIN master..cms_models_itemhistoricalids hi on hi.system_id = i.system_id
				OUTER APPLY (SELECT MAX(version) as 'number_of_versions' FROM master..cms_models_items it WHERE it.system_id = i.system_id) v
        
GO
/****** Object:  Table [dbo].[cms_models_itemdrafts]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itemdrafts](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[system_id] [nvarchar](15) NOT NULL,
	[historical_id] [nvarchar](50) NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[development_status_id] [int] NOT NULL,
	[item_id] [int] NOT NULL,
	[item_bank_id] [int] NOT NULL,
	[response_format_id] [int] NOT NULL,
	[username_id] [nvarchar](150) NOT NULL,
	[active_status] [bit] NOT NULL,
	[shuffle_options] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[item_drafts_vw]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[item_drafts_vw] AS
				SELECT
					id.item_id as 'item_id',
					id.system_id,
					id.item_bank_id,
					id.modify_date,
					id.username_id,
					id.active_status,
                    id.shuffle_options,
					ds.id as 'development_status_id',
					ds.codename as 'development_status_codename',
					ds.en_name as 'development_status_name_en',
					ds.fr_name as 'development_status_name_fr',
					rf.id as 'response_format_id',
					rf.codename as 'response_format_codename',
					rf.en_name as 'response_format_name_en',
					rf.fr_name as 'response_format_name_fr',
					id.historical_id as 'historical_id'
				FROM master..cms_models_itemdrafts id
				JOIN master..cms_models_itemdevelopmentstatuses ds on ds.id = id.development_status_id
				JOIN master..cms_models_itemresponseformats rf on rf.id = id.response_format_id
        
GO
/****** Object:  Table [dbo].[cms_models_itemcontent]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itemcontent](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[content_type] [int] NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[content_order] [int] NOT NULL,
	[item_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_itemcontenttext]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itemcontenttext](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[text] [nvarchar](max) NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[item_content_id] [int] NOT NULL,
	[language_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [dbo].[item_content_vw]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[item_content_vw] AS
				SELECT
					ic.id as 'item_content_id',
					ic.content_type,
					ic.content_order,
					ic.modify_date as 'item_content_modify_date',
					ic.item_id,
					ict.id as 'item_content_text_id',
					ict.text as 'text',
					ict.modify_date as 'item_content_text_modify_date',
					ict.language_id
				FROM master..cms_models_itemcontent ic
				LEFT JOIN master..cms_models_itemcontenttext ict on ict.item_content_id = ic.id
        
GO
/****** Object:  Table [dbo].[OLTF_DEPARTEMENTS_VW]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OLTF_DEPARTEMENTS_VW](
	[DEPT_ID] [int] NOT NULL,
	[ACTIVE_FLG] [nvarchar](1) NOT NULL,
	[EABRV] [nvarchar](20) NULL,
	[FABRV] [nvarchar](20) NULL,
	[LEGACY_CD] [nvarchar](3) NULL,
	[EDESC] [nvarchar](200) NOT NULL,
	[FDESC] [nvarchar](200) NOT NULL,
	[EFDT] [datetime2](7) NOT NULL,
	[XDT] [datetime2](7) NULL,
	[ESEARCH_DESC] [nvarchar](200) NULL,
	[FSEARCH_DESC] [nvarchar](200) NULL,
	[RCVER_GNRL_NBR] [int] NULL,
	[PSEA_ID] [int] NOT NULL,
	[PSSA_ID] [int] NULL,
	[PSSRA_ID] [int] NULL,
	[OLA_ID] [int] NULL,
	[ORG_TYP_ID] [int] NULL,
	[ORG_EMPL_ID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[DEPT_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_taextendedprofile]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_taextendedprofile](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[department_id] [int] NOT NULL,
	[username_id] [nvarchar](150) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_historicalpermissionrequest]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_historicalpermissionrequest](
	[permission_request_id] [int] NOT NULL,
	[goc_email] [nvarchar](254) NOT NULL,
	[pri_or_military_nbr] [nvarchar](10) NOT NULL,
	[supervisor] [nvarchar](180) NOT NULL,
	[supervisor_email] [nvarchar](254) NOT NULL,
	[rationale] [nvarchar](300) NOT NULL,
	[request_date] [datetime2](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[permission_requested_id] [int] NULL,
	[username_id] [nvarchar](150) NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[financial_report_vw]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[financial_report_vw]
                    AS	

                    SELECT TOP 100 percent
                        IIF (u.id is null, v.last_name, u.last_name) as "candidate_last_name",
                        IIF (u.id is null, v.first_name, u.first_name) as "candidate_first_name",
                        IIF (u.id is null, v.pri_or_military_nbr, u.pri_or_military_nbr) as "candidate_pri",
                        ta.goc_email as "ta_email",
                        de.edesc as "ta_org_en",
                        de.fdesc as "ta_org_fr",
                        IIF (dp.edesc is null and a.orderless_financial_data_id is not null, odp.edesc, IIF(dp.edesc is null, 'Public Service Commission', dp.edesc)) as "requesting_dep_en",
                        IIF (dp.fdesc is null and a.orderless_financial_data_id is not null, odp.fdesc, IIF(dp.fdesc is null, 'Public Service Commission', dp.fdesc)) as "requesting_dep_fr",
                        a.test_order_number as "order_no",	
                        p.staffing_process_number as "assessment_process", 
                        o.reference_number as "reference_number",    
                        CASE 
                            WHEN a.orderless_financial_data_id is not null THEN o.department_ministry_id
                            WHEN ISNUMERIC(p.department_ministry_code) = 1 THEN (p.department_ministry_code) ELSE (104)
                        END as "org_code",
                        IIF (a.orderless_financial_data_id is not null, o.fis_organisation_code , p.is_org) as "fis_org_code",
                        IIF (a.orderless_financial_data_id is not null, o.fis_reference_code, p.is_ref) as "fis_ref_code",
                        IIF (a.orderless_financial_data_id is not null, o.billing_contact_name, p.billing_contact) as "billing_contact_name",
                        dbo.status2str('fr', a.status) AS "test_status_fr",
                        dbo.status2str('en', a.status) AS "test_status_en",     
                        a.is_invalid as "is_invalid",
                        cast(a.submit_date as date) as "submit_date",    
                        d.test_code as "test_code",
                        d.fr_name as "test_description_fr",
                        d.en_name as "test_description_en",
                        a.orderless_financial_data_id,
                        a.status as "test_status",		
                        a.test_id as "test_id",
                        a.id as "assigned_test_id"
                    FROM	
                        master..custom_models_assignedtest a
                        OUTER APPLY (SELECT TOP 1 id, last_name, first_name, pri_or_military_nbr FROM master..user_management_models_historicaluser u1 WHERE u1.username = a.username_id AND u1.history_date < a.submit_date ORDER BY u1.history_date DESC) u
                        OUTER APPLY (SELECT TOP 1 * FROM master..user_management_models_historicalpermissionrequest pr WHERE pr.username_id = a.ta_id and pr.history_type = '+' and pr.permission_requested_id = 1 and pr.history_date < a.submit_date order by history_date desc) ta
                        JOIN master..user_management_models_user v on v.username = a.username_id
                        LEFT JOIN master..user_management_models_taextendedprofile e ON e.username_id = a.ta_id
                        LEFT JOIN master..OLTF_DEPARTEMENTS_VW de ON de.DEPT_ID = e.department_id
                        OUTER APPLY (SELECT TOP 1 staffing_process_number, department_ministry_code, is_org, is_ref, billing_contact FROM master..cms_models_historicaltestpermissions h WHERE h.test_order_number = a.test_order_number AND h.username_id = a.ta_id AND h.history_date < a.submit_date AND h.history_type = '+' ORDER BY h.history_date DESC) p
                        LEFT JOIN master..OLTF_DEPARTEMENTS_VW dp ON cast(dp.DEPT_ID as nvarchar) = p.department_ministry_code
                        JOIN master..cms_models_testdefinition d ON d.id = a.test_id
                        LEFT JOIN master..custom_models_orderlessfinancialdata o ON o.id = a.orderless_financial_data_id
                        LEFT JOIN master..OLTF_DEPARTEMENTS_VW odp ON cast(odp.DEPT_ID as nvarchar) = o.department_ministry_id

                    WHERE
                        a.status IN (19, 20)

                    ORDER BY
                        a.test_order_number, a.test_id, a.status, a.id DESC
GO
/****** Object:  Table [dbo].[cms_models_itemoption]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itemoption](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[option_type] [int] NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[score] [nvarchar](25) NULL,
	[option_order] [int] NOT NULL,
	[item_id] [int] NOT NULL,
	[exclude_from_shuffle] [bit] NOT NULL,
	[historical_option_id] [nvarchar](50) NULL,
	[rationale] [nvarchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_itemoptiontext]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itemoptiontext](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[text] [nvarchar](max) NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[item_option_id] [int] NOT NULL,
	[language_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [dbo].[item_option_vw]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[item_option_vw] AS
				SELECT
                    io.id as 'item_option_id',
                    io.option_type,
                    io.option_order,
					io.exclude_from_shuffle,
                    io.historical_option_id,
                    io.rationale,
                    io.modify_date as 'item_option_modify_date',
                    io.item_id,
                    io.score,
                    iot.id as 'item_option_text_id',
                    iot.text as 'text',
                    iot.modify_date as 'item_option_text_modify_date',
                    iot.language_id
                FROM master..cms_models_itemoption io
                LEFT JOIN master..cms_models_itemoptiontext iot on iot.item_option_id = io.id
        
GO
/****** Object:  Table [dbo].[cms_models_itemcontentdrafts]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itemcontentdrafts](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[system_id] [nvarchar](15) NOT NULL,
	[content_type] [int] NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[content_order] [int] NOT NULL,
	[item_id] [int] NOT NULL,
	[username_id] [nvarchar](150) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_itemcontenttextdrafts]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itemcontenttextdrafts](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[text] [nvarchar](max) NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[item_content_id] [int] NOT NULL,
	[language_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [dbo].[item_content_drafts_vw]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[item_content_drafts_vw] AS
				SELECT 
                    c.id as 'item_content_id',
                    c.system_id,
                    c.content_type,
                    c.modify_date as 'item_content_modify_date',
                    c.content_order,
                    c.item_id,
                    c.username_id,
                    ct.id as 'item_content_text_id',
                    ct.text,
                    ct.modify_date as 'item_content_text_modify_date',
                    ct.language_id
                FROM master..cms_models_itemcontentdrafts c
                JOIN master..cms_models_itemcontenttextdrafts ct on ct.item_content_id = c.id
        
GO
/****** Object:  Table [dbo].[cms_models_itemoptiondrafts]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itemoptiondrafts](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[system_id] [nvarchar](15) NOT NULL,
	[option_type] [int] NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[score] [nvarchar](25) NULL,
	[option_order] [int] NOT NULL,
	[item_id] [int] NOT NULL,
	[username_id] [nvarchar](150) NOT NULL,
	[exclude_from_shuffle] [bit] NOT NULL,
	[historical_option_id] [nvarchar](50) NULL,
	[rationale] [nvarchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_itemoptiontextdrafts]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itemoptiontextdrafts](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[text] [nvarchar](max) NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[item_option_id] [int] NOT NULL,
	[language_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [dbo].[item_option_drafts_vw]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[item_option_drafts_vw] AS
				SELECT 
                    o.id as 'item_option_id',
                    o.system_id,
                    o.option_type,
                    o.modify_date as 'item_option_modify_date',
                    o.option_order,
					o.exclude_from_shuffle,
                    o.historical_option_id,
                    o.rationale,
                    o.score,
                    o.item_id,
                    o.username_id,
                    ot.id as 'item_option_text_id',
                    ot.text,
                    ot.modify_date as 'item_option_text_modify_date',
                    ot.language_id
                FROM master..cms_models_itemoptiondrafts o
                JOIN master..cms_models_itemoptiontextdrafts ot on ot.item_option_id = o.id
        
GO
/****** Object:  Table [dbo].[custom_models_historicaltestaccesscode]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaltestaccesscode](
	[id] [int] NOT NULL,
	[test_access_code] [nvarchar](10) NOT NULL,
	[test_order_number] [nvarchar](12) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[ta_username_id] [nvarchar](150) NULL,
	[test_id] [int] NULL,
	[test_session_language_id] [int] NULL,
	[created_date] [date] NOT NULL,
	[orderless_financial_data_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_uitinvites]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_uitinvites](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[invite_date] [date] NOT NULL,
	[validity_end_date] [date] NOT NULL,
	[ta_test_permissions_id] [int] NULL,
	[ta_username_id] [nvarchar](150) NULL,
	[reason_for_deletion_id] [int] NULL,
	[reason_for_modification_id] [int] NULL,
	[orderless_request] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalunsupervisedtestaccesscode]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalunsupervisedtestaccesscode](
	[id] [int] NOT NULL,
	[test_access_code] [nvarchar](10) NOT NULL,
	[test_order_number] [nvarchar](12) NULL,
	[candidate_email] [nvarchar](254) NULL,
	[created_date] [date] NOT NULL,
	[validity_end_date] [date] NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[ta_username_id] [nvarchar](150) NULL,
	[test_id] [int] NULL,
	[uit_invite_id] [int] NULL,
	[orderless_financial_data_id] [int] NULL,
	[accommodation_request_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[test_order_and_reference_numbers_vw]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[test_order_and_reference_numbers_vw] AS
				SELECT
	test_order_number,
	reference_number,
	ta_username,
	IIF(u.id is not null, u.email, v.email) AS 'ta_email', 
	staffing_process_number
FROM (

	SELECT DISTINCT 
		htp.test_order_number as 'test_order_number', 
		NULL as 'reference_number',
		htp.username_id as 'ta_username', 
		htp.staffing_process_number as 'staffing_process_number'
	FROM master..cms_models_historicaltestpermissions htp

	UNION

	SELECT DISTINCT
		NULL as 'test_order_number',
		ofd.reference_number as 'reference_number',
		CASE
			WHEN htac.ta_username_id is not NULL THEN htac.ta_username_id
			WHEN hutac.ta_username_id is not NULL THEN hutac.ta_username_id
			WHEN at.orderless_financial_data_id is not NULL THEN at.ta_id
			WHEN ui.ta_username_id is not NULL THEN ui.ta_username_id
		ELSE
			NULL
		END as 'ta_username',
		NULL as 'staffing_process_number'
	FROM master..custom_models_orderlessfinancialdata ofd
	OUTER APPLY (SELECT TOP 1 * FROM master..custom_models_historicaltestaccesscode htac1 WHERE htac1.orderless_financial_data_id = ofd.id AND htac1.history_type = '+' AND htac1.orderless_financial_data_id is not NULL ORDER BY htac1.history_date DESC) htac
	OUTER APPLY (SELECT TOP 1 * FROM master..custom_models_historicalunsupervisedtestaccesscode hutac1 WHERE hutac1.orderless_financial_data_id = ofd.id AND hutac1.history_type = '+' AND hutac1.orderless_financial_data_id is not NULL ORDER BY hutac1.history_date DESC) hutac
	LEFT JOIN master..custom_models_assignedtest at on at.orderless_financial_data_id = ofd.id
	LEFT JOIN master..custom_models_uitinvites ui on ui.id = ofd.uit_invite_id
) r
OUTER APPLY (SELECT TOP 1 * FROM master..user_management_models_historicaluser u1 WHERE u1.username = r.ta_username ORDER BY u1.history_date DESC) u
JOIN master..user_management_models_user v on v.username = r.ta_username
        
GO
/****** Object:  View [dbo].[test_data_vw]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[test_data_vw] AS
				SELECT 
					at.id as 'assigned_test_id',
					at.status as 'test_status',
					IIF (hta.id is not NULL, hta.username, ta.username) as 'ta_username',
					IIF (hta.id is not NULL, hta.email, ta.email) as 'ta_email',
					IIF (hu.id is not NULL, hu.username, u.username) as 'candidate_username',
					IIF (hu.id is not NULL, hu.email, u.email) as 'candidate_email',
					IIF (hu.id is not NULL, hu.first_name, u.first_name) as 'candidate_first_name',
					IIF (hu.id is not NULL, hu.last_name, u.last_name) as 'candidate_last_name',
					td.id as 'test_id',
					td.en_name as 'en_test_name',
					td.fr_name as 'fr_test_name',
					td.version as 'test_version',
					IIF (htp.id is not NULL, htp.test_order_number, htp2.test_order_number) as 'test_order_number',
					IIF (htp.id is not NULL, htp.staffing_process_number, htp2.staffing_process_number) as 'staffing_process_number',
					htp3.ta_usernames as 'allowed_ta_usernames',
					ofd.reference_number as 'reference_number'
				FROM master..custom_models_assignedtest at
				OUTER APPLY (SELECT TOP 1 * FROM master..user_management_models_historicaluser u1 WHERE u1.username = at.ta_id AND u1.history_date < at.submit_date ORDER BY u1.history_date DESC) hta
				JOIN master..user_management_models_user ta on ta.username = at.ta_id
				OUTER APPLY (SELECT TOP 1 * FROM master..user_management_models_historicaluser u2 WHERE u2.username = at.username_id AND u2.history_date < at.submit_date ORDER BY u2.history_date DESC) hu
				JOIN master..user_management_models_user u on u.username = at.username_id
				JOIN master..cms_models_testdefinition td on td.id = at.test_id
				OUTER APPLY (SELECT TOP 1 * FROM master..cms_models_historicaltestpermissions htp1 WHERE htp1.test_order_number = at.test_order_number AND htp1.history_date < at.submit_date ORDER BY htp1.history_date DESC) htp
				OUTER APPLY (SELECT TOP 1 * FROM master..cms_models_historicaltestpermissions htp1 WHERE htp1.test_order_number = at.test_order_number ORDER BY htp1.history_date DESC) htp2
				OUTER APPLY (SELECT string_agg(ta_username_list.username_id, ',') within group (ORDER BY ta_username_list.username_id) as 'ta_usernames' FROM (SELECT DISTINCT username_id FROM master..cms_models_historicaltestpermissions htp WHERE htp.history_type = '+' AND htp.test_order_number = at.test_order_number) ta_username_list) htp3
				LEFT JOIN master..custom_models_orderlessfinancialdata ofd on ofd.id = at.orderless_financial_data_id
        
GO
/****** Object:  Table [dbo].[cms_models_itembankattributestext]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itembankattributestext](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[text] [nvarchar](max) NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[item_bank_attribute_id] [int] NOT NULL,
	[language_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_itembankattributevaluestext]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itembankattributevaluestext](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[text] [nvarchar](max) NULL,
	[item_bank_attribute_values_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_itembankattributevalues]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itembankattributevalues](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[attribute_value_order] [int] NOT NULL,
	[item_bank_attribute_id] [int] NOT NULL,
	[attribute_value_type] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[item_bank_attribute_values_vw]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[item_bank_attribute_values_vw] AS
                SELECT 
                    av.id as 'item_bank_attribute_value_id', 
                    av.attribute_value_order,
                    av.item_bank_attribute_id,
                    at_e.text as 'attribute_text_en',
	                at_f.text as 'attribute_text_fr',
                    av.attribute_value_type,
                    avt.id as 'item_bank_attribute_values_text_id',
                    avt.text
                FROM master..cms_models_itembankattributevalues av 
                JOIN master..cms_models_itembankattributevaluestext avt ON avt.item_bank_attribute_values_id = av.id
                JOIN master..cms_models_itembankattributestext at_e on at_e.item_bank_attribute_id = av.item_bank_attribute_id AND at_e.language_id = 1
                JOIN master..cms_models_itembankattributestext at_f on at_f.item_bank_attribute_id = av.item_bank_attribute_id AND at_f.language_id = 2
        
GO
/****** Object:  View [dbo].[test_result_vw]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[test_result_vw]
AS

	SELECT
		assigned_test_id,
		user_id, 
        user_firstname AS 'candidate_first_name',
        user_lastname AS 'candidate_last_name',
        username,
		uit_candidate_email,
		candidate_email,
		candidate_pri_or_military_nbr,
        ta_id,
		reference_number,
		test_order_number,
		process_number,
		history_id,
		td_test_code,
		td_fr_name,
		td_en_name,
		submit_date,
		test_score,
		CASE 
			WHEN test_is_invalid = 1 THEN 'Invalide'
			ELSE
				CASE 
					WHEN test_score IS NULL THEN '-' 
					WHEN test_fr_converted_score IS NULL THEN 'Conversion invalide du résultat'
				ELSE
					test_fr_converted_score
				END
		END AS 'level_fr',
		CASE 
			WHEN test_is_invalid = 1 THEN 'Invalid'
			ELSE	
				CASE
					WHEN test_score IS NULL THEN '-' 
					WHEN test_en_converted_score IS NULL THEN 'Invalid Score Conversion'
				ELSE
					test_en_converted_score
				END
		END AS 'level_en',
		dbo.status2str('fr', test_status) AS test_status_fr,
		dbo.status2str('en', test_status) AS test_status_en,
		test_status,
		test_id,
        allowed_ta_usernames

	FROM
	(
		SELECT 
			at.id AS 'assigned_test_id',
			at.test_id,
            at.ta_id AS 'ta_id',
			ofd.reference_number AS 'reference_number',
			at.test_order_number AS 'test_order_number',
			at.submit_date AS 'submit_date',
			at.total_score AS 'test_score',
			at.en_converted_score AS 'test_en_converted_score',
			at.fr_converted_score AS 'test_fr_converted_score',
			at.status AS 'test_status',
			at.is_invalid AS 'test_is_invalid',			

			IIF(u.id is not null, u.id, v.id) AS 'user_id', 
            IIF(u.id is not null, u.username, v.username) AS 'username',
			t.candidate_email AS 'uit_candidate_email',
			IIF(u.id is not null, u.email, v.email) AS 'candidate_email', 
			IIF(u.id is not null, u.first_name, v.first_name) AS 'user_firstname', 
			IIF(u.id is not null, u.last_name, v.last_name) AS 'user_lastname', 	
			IIF(u.id is not null, u.pri_or_military_nbr, v.pri_or_military_nbr) AS 'candidate_pri_or_military_nbr',
	
			p.staffing_process_number AS 'process_number',
			p.history_id AS 'history_id',
	
			td.test_code AS 'td_test_code',
			td.fr_name AS 'td_fr_name',
			td.en_name AS 'td_en_name',
            
            htp3.ta_usernames as 'allowed_ta_usernames'

		FROM master..custom_models_assignedtest at
		OUTER APPLY (SELECT TOP 1 * FROM master..user_management_models_historicaluser u1 WHERE u1.username = at.username_id AND u1.history_date < at.submit_date ORDER BY u1.history_date DESC) u
		JOIN master..user_management_models_user v on v.username = at.username_id
		OUTER APPLY (SELECT TOP 1 * FROM master..cms_models_historicaltestpermissions p1 WHERE p1.test_order_number = at.test_order_number AND p1.username_id = at.ta_id AND p1.history_date < at.submit_date ORDER BY p1.history_date DESC) p
		JOIN master..cms_models_testdefinition td ON td.id = at.test_id
		LEFT JOIN master..custom_models_orderlessfinancialdata ofd on at.orderless_financial_data_id = ofd.id
		OUTER APPLY (SELECT TOP 1 * FROM master..custom_models_historicalunsupervisedtestaccesscode t1 WHERE t1.test_access_code = at.test_access_code AND t1.uit_invite_id = at.uit_invite_id ORDER BY t1.history_date DESC) t
        OUTER APPLY (SELECT string_agg(ta_username_list.username_id, ',') within group (ORDER BY ta_username_list.username_id) as 'ta_usernames' FROM (SELECT DISTINCT username_id FROM master..cms_models_historicaltestpermissions htp WHERE htp.history_type = '+' AND htp.test_order_number = at.test_order_number) ta_username_list) htp3
	) r
GO
/****** Object:  Table [dbo].[auth_group]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[auth_group](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](150) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [auth_group_name_a6ea08ec_uniq] UNIQUE NONCLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[auth_group_permissions]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[auth_group_permissions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[group_id] [int] NOT NULL,
	[permission_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[auth_permission]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[auth_permission](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](255) NOT NULL,
	[content_type_id] [int] NOT NULL,
	[codename] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[authtoken_token]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[authtoken_token](
	[key] [nvarchar](40) NOT NULL,
	[created] [datetime2](7) NOT NULL,
	[user_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CAT_ABORIGINAL_VW]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CAT_ABORIGINAL_VW](
	[ABRG_ID] [int] NOT NULL,
	[ACTIVE_FLG] [nvarchar](1) NOT NULL,
	[EABRV] [nvarchar](10) NULL,
	[FABRV] [nvarchar](10) NULL,
	[LEGACY_CD] [int] NULL,
	[EDESC] [nvarchar](200) NOT NULL,
	[FDESC] [nvarchar](200) NOT NULL,
	[EFDT] [datetime2](7) NOT NULL,
	[XDT] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[ABRG_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CAT_DISABILITY_VW]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CAT_DISABILITY_VW](
	[DSBL_ID] [int] NOT NULL,
	[ACTIVE_FLG] [nvarchar](1) NOT NULL,
	[EABRV] [nvarchar](10) NULL,
	[FABRV] [nvarchar](10) NULL,
	[LEGACY_CD] [int] NULL,
	[EDESC] [nvarchar](200) NOT NULL,
	[FDESC] [nvarchar](200) NOT NULL,
	[FULL_EDESC] [nvarchar](200) NULL,
	[FULL_FDESC] [nvarchar](200) NULL,
	[EFDT] [datetime2](7) NOT NULL,
	[XDT] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[DSBL_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cat_employer_sts_vw]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cat_employer_sts_vw](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[empsts_id] [int] NOT NULL,
	[appl_acronym] [nvarchar](10) NOT NULL,
	[active_flg] [nvarchar](1) NOT NULL,
	[edesc] [nvarchar](200) NOT NULL,
	[fdesc] [nvarchar](200) NOT NULL,
	[efdt] [datetime2](7) NOT NULL,
	[xdt] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CAT_REF_ABORIGINAL_VW]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CAT_REF_ABORIGINAL_VW](
	[ABRG_ID] [int] NOT NULL,
	[ACTIVE_FLG] [nvarchar](1) NOT NULL,
	[EABRV] [nvarchar](10) NULL,
	[FABRV] [nvarchar](10) NULL,
	[LEGACY_CD] [int] NULL,
	[EDESC] [nvarchar](200) NOT NULL,
	[FDESC] [nvarchar](200) NOT NULL,
	[EFDT] [datetime2](7) NOT NULL,
	[XDT] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[ABRG_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CAT_REF_CLASSIFICATION_VW]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CAT_REF_CLASSIFICATION_VW](
	[CLASSIF_ID] [int] NOT NULL,
	[ACTIVE_FLG] [nvarchar](1) NOT NULL,
	[CLASS_GRP_CD] [nvarchar](2) NOT NULL,
	[CLASS_SBGRP_CD] [nvarchar](3) NULL,
	[CLASS_LVL_CD] [nvarchar](2) NOT NULL,
	[EFDT] [datetime2](7) NOT NULL,
	[XDT] [datetime2](7) NULL,
	[PSC_CLASS_IND] [int] NOT NULL,
	[DFLT_BUD_CD] [nvarchar](5) NULL,
	[DFLT_PAY_ZN_CD] [nvarchar](3) NULL,
	[EQLZN_AMT] [int] NULL,
	[EQLZN_AMT_EFDT] [datetime2](7) NULL,
	[EQLZN_AMT_XDT] [datetime2](7) NULL,
	[OFCR_LVL_IND] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[CLASSIF_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CAT_REF_DEPARTMENTS_VW]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CAT_REF_DEPARTMENTS_VW](
	[DEPT_ID] [int] NOT NULL,
	[ACTIVE_FLG] [nvarchar](1) NOT NULL,
	[EABRV] [nvarchar](20) NULL,
	[FABRV] [nvarchar](20) NULL,
	[LEGACY_CD] [nvarchar](3) NULL,
	[EDESC] [nvarchar](200) NOT NULL,
	[FDESC] [nvarchar](200) NOT NULL,
	[EFDT] [datetime2](7) NOT NULL,
	[XDT] [datetime2](7) NULL,
	[ESEARCH_DESC] [nvarchar](200) NULL,
	[FSEARCH_DESC] [nvarchar](200) NULL,
	[RCVER_GNRL_NBR] [int] NULL,
	[PSEA_ID] [int] NOT NULL,
	[PSSA_ID] [int] NULL,
	[PSSRA_ID] [int] NULL,
	[OLA_ID] [int] NULL,
	[ORG_TYP_ID] [int] NULL,
	[ORG_EMPL_ID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[DEPT_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CAT_REF_DISABILITY_VW]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CAT_REF_DISABILITY_VW](
	[DSBL_ID] [int] NOT NULL,
	[ACTIVE_FLG] [nvarchar](1) NOT NULL,
	[EABRV] [nvarchar](10) NULL,
	[FABRV] [nvarchar](10) NULL,
	[LEGACY_CD] [int] NULL,
	[EDESC] [nvarchar](200) NOT NULL,
	[FDESC] [nvarchar](200) NOT NULL,
	[FULL_EDESC] [nvarchar](200) NULL,
	[FULL_FDESC] [nvarchar](200) NULL,
	[EFDT] [datetime2](7) NOT NULL,
	[XDT] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[DSBL_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CAT_REF_EMPLOYER_STS_VW]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CAT_REF_EMPLOYER_STS_VW](
	[EMPSTS_ID] [int] NOT NULL,
	[ACTIVE_FLG] [nvarchar](1) NOT NULL,
	[EDESC] [nvarchar](200) NOT NULL,
	[FDESC] [nvarchar](200) NOT NULL,
	[EFDT] [datetime2](7) NOT NULL,
	[XDT] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[EMPSTS_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CAT_REF_GENDER_VW]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CAT_REF_GENDER_VW](
	[GND_ID] [int] NOT NULL,
	[ACTIVE_FLG] [nvarchar](1) NOT NULL,
	[EABRV] [nvarchar](10) NULL,
	[FABRV] [nvarchar](10) NULL,
	[LEGACY_CD] [int] NULL,
	[EDESC] [nvarchar](200) NOT NULL,
	[FDESC] [nvarchar](200) NOT NULL,
	[EFDT] [datetime2](7) NOT NULL,
	[XDT] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[GND_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CAT_REF_PROVINCE_VW]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CAT_REF_PROVINCE_VW](
	[PROV_ID] [int] NOT NULL,
	[ACTIVE_FLG] [nvarchar](1) NOT NULL,
	[EABRV] [nvarchar](10) NULL,
	[FABRV] [nvarchar](10) NULL,
	[LEGACY_CD] [nvarchar](3) NULL,
	[EDESC] [nvarchar](200) NOT NULL,
	[FDESC] [nvarchar](200) NOT NULL,
	[EFDT] [datetime2](7) NOT NULL,
	[XDT] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[PROV_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CAT_REF_VISIBLE_MINORITY_VW]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CAT_REF_VISIBLE_MINORITY_VW](
	[VISMIN_ID] [int] NOT NULL,
	[ACTIVE_FLG] [nvarchar](1) NOT NULL,
	[EABRV] [nvarchar](10) NULL,
	[FABRV] [nvarchar](10) NULL,
	[LEGACY_CD] [int] NULL,
	[EDESC] [nvarchar](200) NOT NULL,
	[FDESC] [nvarchar](200) NOT NULL,
	[FULL_EDESC] [nvarchar](200) NULL,
	[FULL_FDESC] [nvarchar](200) NULL,
	[EFDT] [datetime2](7) NOT NULL,
	[XDT] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[VISMIN_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CAT_VISIBLE_MINORITY_VW]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CAT_VISIBLE_MINORITY_VW](
	[VISMIN_ID] [int] NOT NULL,
	[ACTIVE_FLG] [nvarchar](1) NOT NULL,
	[EABRV] [nvarchar](10) NULL,
	[FABRV] [nvarchar](10) NULL,
	[LEGACY_CD] [int] NULL,
	[EDESC] [nvarchar](200) NOT NULL,
	[FDESC] [nvarchar](200) NOT NULL,
	[FULL_EDESC] [nvarchar](200) NULL,
	[FULL_FDESC] [nvarchar](200) NULL,
	[EFDT] [datetime2](7) NOT NULL,
	[XDT] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[VISMIN_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_addressbookcontact]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_addressbookcontact](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](30) NULL,
	[parent_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_addressbookcontact_test_section]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_addressbookcontact_test_section](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[addressbookcontact_id] [int] NOT NULL,
	[testsection_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_addressbookcontactdetails]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_addressbookcontactdetails](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](150) NOT NULL,
	[contact_id] [int] NOT NULL,
	[language_id] [nvarchar](2) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_answer]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_answer](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[scoring_value] [int] NOT NULL,
	[question_id] [int] NOT NULL,
	[order] [int] NOT NULL,
	[ppc_answer_id] [nvarchar](25) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_answerdetails]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_answerdetails](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[content] [nvarchar](max) NOT NULL,
	[answer_id] [int] NOT NULL,
	[language_id] [nvarchar](2) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_bundleitemsbasicrule]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_bundleitemsbasicrule](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[number_of_items] [int] NOT NULL,
	[bundle_rule_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_competencytype]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_competencytype](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[fr_name] [nvarchar](100) NOT NULL,
	[en_name] [nvarchar](100) NOT NULL,
	[max_score] [int] NOT NULL,
	[test_definition_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_emailquestion]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_emailquestion](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[email_id] [int] NOT NULL,
	[from_field_id] [int] NOT NULL,
	[question_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_emailquestion_cc_field]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_emailquestion_cc_field](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[emailquestion_id] [int] NOT NULL,
	[addressbookcontact_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_emailquestion_competency_types]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_emailquestion_competency_types](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[emailquestion_id] [int] NOT NULL,
	[competencytype_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_emailquestion_to_field]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_emailquestion_to_field](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[emailquestion_id] [int] NOT NULL,
	[addressbookcontact_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_emailquestiondetails]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_emailquestiondetails](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[subject_field] [nvarchar](max) NOT NULL,
	[date_field] [nvarchar](max) NOT NULL,
	[body] [nvarchar](max) NOT NULL,
	[email_question_id] [int] NOT NULL,
	[language_id] [nvarchar](2) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_fileresourcedetails]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_fileresourcedetails](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[path] [nvarchar](200) NOT NULL,
	[test_definition_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicaladdressbookcontact]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicaladdressbookcontact](
	[id] [int] NOT NULL,
	[name] [nvarchar](30) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[parent_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicaladdressbookcontactdetails]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicaladdressbookcontactdetails](
	[id] [int] NOT NULL,
	[title] [nvarchar](150) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[contact_id] [int] NULL,
	[history_user_id] [int] NULL,
	[language_id] [nvarchar](2) NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalanswer]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalanswer](
	[id] [int] NOT NULL,
	[scoring_value] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[question_id] [int] NULL,
	[order] [int] NOT NULL,
	[ppc_answer_id] [nvarchar](25) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalanswerdetails]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalanswerdetails](
	[id] [int] NOT NULL,
	[content] [nvarchar](max) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[answer_id] [int] NULL,
	[history_user_id] [int] NULL,
	[language_id] [nvarchar](2) NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalbundlebundlesassociation]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalbundlebundlesassociation](
	[id] [int] NOT NULL,
	[display_order] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[bundle_link_id] [int] NULL,
	[bundle_source_id] [int] NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalbundlebundlesrule]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalbundlebundlesrule](
	[id] [int] NOT NULL,
	[number_of_bundles] [int] NULL,
	[shuffle_bundles] [bit] NOT NULL,
	[keep_items_together] [bit] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[bundle_rule_id] [int] NULL,
	[history_user_id] [int] NULL,
	[display_order] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalbundlebundlesruleassociations]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalbundlebundlesruleassociations](
	[id] [int] NOT NULL,
	[display_order] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[bundle_id] [int] NULL,
	[history_user_id] [int] NULL,
	[bundle_bundles_rule_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalbundleitemsassociation]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalbundleitemsassociation](
	[id] [int] NOT NULL,
	[system_id_link] [nvarchar](15) NOT NULL,
	[version] [int] NULL,
	[display_order] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[bundle_source_id] [int] NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalbundleitemsbasicrule]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalbundleitemsbasicrule](
	[id] [int] NOT NULL,
	[number_of_items] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[bundle_rule_id] [int] NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalbundlerule]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalbundlerule](
	[id] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[bundle_id] [int] NULL,
	[history_user_id] [int] NULL,
	[rule_type_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalbundleruletype]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalbundleruletype](
	[id] [int] NOT NULL,
	[codename] [nvarchar](50) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalbundles]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalbundles](
	[id] [int] NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[version_text] [nvarchar](50) NULL,
	[shuffle_items] [bit] NOT NULL,
	[shuffle_bundles] [bit] NOT NULL,
	[active] [bit] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[item_bank_id] [int] NULL,
	[shuffle_between_bundles] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalcompetencytype]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalcompetencytype](
	[id] [int] NOT NULL,
	[fr_name] [nvarchar](100) NOT NULL,
	[en_name] [nvarchar](100) NOT NULL,
	[max_score] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[test_definition_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalemailquestion]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalemailquestion](
	[id] [int] NOT NULL,
	[email_id] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[from_field_id] [int] NULL,
	[history_user_id] [int] NULL,
	[question_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalemailquestiondetails]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalemailquestiondetails](
	[id] [int] NOT NULL,
	[subject_field] [nvarchar](max) NOT NULL,
	[date_field] [nvarchar](max) NOT NULL,
	[body] [nvarchar](max) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[email_question_id] [int] NULL,
	[history_user_id] [int] NULL,
	[language_id] [nvarchar](2) NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalfileresourcedetails]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalfileresourcedetails](
	[id] [int] NOT NULL,
	[path] [nvarchar](200) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[test_definition_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitemattributevalue]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitemattributevalue](
	[id] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[item_id] [int] NULL,
	[item_bank_attribute_value_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitemattributevaluedrafts]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitemattributevaluedrafts](
	[id] [int] NOT NULL,
	[system_id] [nvarchar](15) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[item_id] [int] NULL,
	[item_bank_attribute_value_id] [int] NULL,
	[username_id] [nvarchar](150) NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitembank]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitembank](
	[id] [int] NOT NULL,
	[custom_item_bank_id] [nvarchar](25) NOT NULL,
	[en_name] [nvarchar](150) NOT NULL,
	[fr_name] [nvarchar](150) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[archived] [bit] NOT NULL,
	[department_id] [int] NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[comments] [nvarchar](200) NOT NULL,
	[language_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitembankaccesstypes]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitembankaccesstypes](
	[id] [int] NOT NULL,
	[en_description] [nvarchar](150) NOT NULL,
	[fr_description] [nvarchar](150) NOT NULL,
	[codename] [nvarchar](25) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[priority] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitembankattributes]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitembankattributes](
	[id] [int] NOT NULL,
	[attribute_order] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[item_bank_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitembankattributestext]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitembankattributestext](
	[id] [int] NOT NULL,
	[text] [nvarchar](max) NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[item_bank_attribute_id] [int] NULL,
	[language_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitembankattributevalues]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitembankattributevalues](
	[id] [int] NOT NULL,
	[attribute_value_order] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[item_bank_attribute_id] [int] NULL,
	[attribute_value_type] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitembankattributevaluestext]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitembankattributevaluestext](
	[id] [int] NOT NULL,
	[text] [nvarchar](max) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[item_bank_attribute_values_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitembankpermissions]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitembankpermissions](
	[id] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[item_bank_id] [int] NULL,
	[item_bank_access_type_id] [int] NULL,
	[username_id] [nvarchar](150) NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitembankrule]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitembankrule](
	[id] [int] NOT NULL,
	[order] [int] NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[item_bank_id] [int] NULL,
	[item_bank_bundle_id] [int] NULL,
	[test_section_component_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitemcomments]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitemcomments](
	[id] [int] NOT NULL,
	[comment] [nvarchar](255) NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[username_id] [nvarchar](150) NULL,
	[system_id] [nvarchar](15) NOT NULL,
	[version] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitemcontent]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitemcontent](
	[id] [int] NOT NULL,
	[content_type] [int] NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[content_order] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[item_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitemcontentdrafts]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitemcontentdrafts](
	[id] [int] NOT NULL,
	[system_id] [nvarchar](15) NOT NULL,
	[content_type] [int] NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[content_order] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[item_id] [int] NULL,
	[username_id] [nvarchar](150) NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitemcontenttext]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitemcontenttext](
	[id] [int] NOT NULL,
	[text] [nvarchar](max) NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[item_content_id] [int] NULL,
	[language_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitemcontenttextdrafts]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitemcontenttextdrafts](
	[id] [int] NOT NULL,
	[text] [nvarchar](max) NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[item_content_id] [int] NULL,
	[language_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitemdevelopmentstatuses]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitemdevelopmentstatuses](
	[id] [int] NOT NULL,
	[en_name] [nvarchar](25) NOT NULL,
	[fr_name] [nvarchar](25) NOT NULL,
	[codename] [nvarchar](50) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitemdrafts]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitemdrafts](
	[id] [int] NOT NULL,
	[system_id] [nvarchar](15) NOT NULL,
	[historical_id] [nvarchar](50) NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[development_status_id] [int] NULL,
	[history_user_id] [int] NULL,
	[item_id] [int] NULL,
	[item_bank_id] [int] NULL,
	[response_format_id] [int] NULL,
	[username_id] [nvarchar](150) NULL,
	[active_status] [bit] NOT NULL,
	[shuffle_options] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitemhistoricalids]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitemhistoricalids](
	[id] [int] NOT NULL,
	[historical_id] [nvarchar](50) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[item_bank_id] [int] NULL,
	[system_id] [nvarchar](15) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitemoption]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitemoption](
	[id] [int] NOT NULL,
	[option_type] [int] NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[score] [nvarchar](25) NULL,
	[option_order] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[item_id] [int] NULL,
	[exclude_from_shuffle] [bit] NOT NULL,
	[historical_option_id] [nvarchar](50) NULL,
	[rationale] [nvarchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitemoptiondrafts]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitemoptiondrafts](
	[id] [int] NOT NULL,
	[system_id] [nvarchar](15) NOT NULL,
	[option_type] [int] NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[score] [nvarchar](25) NULL,
	[option_order] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[item_id] [int] NULL,
	[username_id] [nvarchar](150) NULL,
	[exclude_from_shuffle] [bit] NOT NULL,
	[historical_option_id] [nvarchar](50) NULL,
	[rationale] [nvarchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitemoptiontext]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitemoptiontext](
	[id] [int] NOT NULL,
	[text] [nvarchar](max) NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[item_option_id] [int] NULL,
	[language_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitemoptiontextdrafts]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitemoptiontextdrafts](
	[id] [int] NOT NULL,
	[text] [nvarchar](max) NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[item_option_id] [int] NULL,
	[language_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitemresponseformats]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitemresponseformats](
	[id] [int] NOT NULL,
	[en_name] [nvarchar](25) NOT NULL,
	[fr_name] [nvarchar](25) NOT NULL,
	[codename] [nvarchar](50) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitems]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitems](
	[id] [int] NOT NULL,
	[system_id] [nvarchar](15) NOT NULL,
	[version] [int] NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[active_status] [bit] NOT NULL,
	[development_status_id] [int] NULL,
	[history_user_id] [int] NULL,
	[last_modified_by_username_id] [nvarchar](150) NULL,
	[response_format_id] [int] NULL,
	[item_bank_id] [int] NULL,
	[shuffle_options] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalmultiplechoicequestion]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalmultiplechoicequestion](
	[id] [int] NOT NULL,
	[question_difficulty_type] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[question_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalnewquestion]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalnewquestion](
	[id] [int] NOT NULL,
	[question_type] [int] NOT NULL,
	[pilot] [bit] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[question_block_type_id] [int] NULL,
	[test_section_component_id] [int] NULL,
	[order] [int] NOT NULL,
	[dependent_order] [int] NOT NULL,
	[shuffle_answer_choices] [bit] NOT NULL,
	[ppc_question_id] [nvarchar](25) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalnextsectionbuttontypepopup]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalnextsectionbuttontypepopup](
	[id] [int] NOT NULL,
	[content] [nvarchar](max) NOT NULL,
	[title] [nvarchar](max) NOT NULL,
	[button_text] [nvarchar](50) NOT NULL,
	[confirm_proceed] [bit] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[language_id] [nvarchar](2) NULL,
	[test_section_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalnextsectionbuttontypeproceed]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalnextsectionbuttontypeproceed](
	[id] [int] NOT NULL,
	[button_text] [nvarchar](max) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[language_id] [nvarchar](2) NULL,
	[test_section_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalorderlesstestpermissions]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalorderlesstestpermissions](
	[id] [int] NOT NULL,
	[parent_code] [nvarchar](25) NOT NULL,
	[test_code] [nvarchar](25) NOT NULL,
	[date_assigned] [datetime2](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[ta_extended_profile_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalpagesection]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalpagesection](
	[id] [int] NOT NULL,
	[order] [int] NOT NULL,
	[page_section_type] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[section_component_page_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalpagesectiontypeimagezoom]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalpagesectiontypeimagezoom](
	[id] [int] NOT NULL,
	[large_image] [nvarchar](250) NOT NULL,
	[small_image] [nvarchar](250) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[language_id] [nvarchar](2) NULL,
	[page_section_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalpagesectiontypemarkdown]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalpagesectiontypemarkdown](
	[id] [int] NOT NULL,
	[content] [nvarchar](max) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[language_id] [nvarchar](2) NULL,
	[page_section_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalpagesectiontypesampleemail]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalpagesectiontypesampleemail](
	[id] [int] NOT NULL,
	[email_id] [int] NOT NULL,
	[subject_field] [nvarchar](max) NOT NULL,
	[from_field] [nvarchar](max) NOT NULL,
	[to_field] [nvarchar](max) NOT NULL,
	[date_field] [nvarchar](max) NOT NULL,
	[body] [nvarchar](max) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[language_id] [nvarchar](2) NULL,
	[page_section_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalpagesectiontypesampleemailresponse]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalpagesectiontypesampleemailresponse](
	[id] [int] NOT NULL,
	[to_field] [nvarchar](max) NOT NULL,
	[cc_field] [nvarchar](max) NULL,
	[response] [nvarchar](max) NULL,
	[reason] [nvarchar](max) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[language_id] [nvarchar](2) NULL,
	[page_section_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalpagesectiontypesampletaskresponse]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalpagesectiontypesampletaskresponse](
	[id] [int] NOT NULL,
	[response] [nvarchar](max) NULL,
	[reason] [nvarchar](max) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[language_id] [nvarchar](2) NULL,
	[page_section_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalpagesectiontypetreedescription]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalpagesectiontypetreedescription](
	[id] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[address_book_contact_id] [int] NULL,
	[history_user_id] [int] NULL,
	[language_id] [nvarchar](2) NULL,
	[page_section_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalquestionblocktype]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalquestionblocktype](
	[id] [int] NOT NULL,
	[name] [nvarchar](150) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[test_definition_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalquestionlistrule]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalquestionlistrule](
	[id] [int] NOT NULL,
	[number_of_questions] [int] NOT NULL,
	[order] [int] NULL,
	[shuffle] [bit] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[test_section_component_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalquestionsection]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalquestionsection](
	[id] [int] NOT NULL,
	[order] [int] NOT NULL,
	[question_section_type] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[question_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalquestionsectiontypemarkdown]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalquestionsectiontypemarkdown](
	[id] [int] NOT NULL,
	[content] [nvarchar](max) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[language_id] [nvarchar](2) NULL,
	[question_section_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalquestionsituation]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalquestionsituation](
	[id] [int] NOT NULL,
	[situation] [nvarchar](max) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[language_id] [nvarchar](2) NULL,
	[question_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalreducercontrol]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalreducercontrol](
	[reducer] [nvarchar](20) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalscoringmethods]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalscoringmethods](
	[id] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[method_type_id] [int] NULL,
	[test_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalscoringmethodtypes]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalscoringmethodtypes](
	[id] [int] NOT NULL,
	[method_type_codename] [nvarchar](50) NOT NULL,
	[en_name] [nvarchar](max) NOT NULL,
	[fr_name] [nvarchar](max) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalscoringpassfail]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalscoringpassfail](
	[id] [int] NOT NULL,
	[minimum_score] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[scoring_method_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalscoringthreshold]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalscoringthreshold](
	[id] [int] NOT NULL,
	[minimum_score] [int] NOT NULL,
	[maximum_score] [int] NOT NULL,
	[en_conversion_value] [nvarchar](max) NOT NULL,
	[fr_conversion_value] [nvarchar](max) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[scoring_method_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalsectioncomponentpage]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalsectioncomponentpage](
	[id] [int] NOT NULL,
	[order] [int] NOT NULL,
	[en_title] [nvarchar](150) NOT NULL,
	[fr_title] [nvarchar](150) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[test_section_component_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalsituationexamplerating]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalsituationexamplerating](
	[id] [int] NOT NULL,
	[score] [float] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[competency_type_id] [int] NULL,
	[history_user_id] [int] NULL,
	[question_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalsituationexampleratingdetails]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalsituationexampleratingdetails](
	[id] [int] NOT NULL,
	[example] [nvarchar](max) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[example_rating_id] [int] NULL,
	[history_user_id] [int] NULL,
	[language_id] [nvarchar](2) NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicaltestdefinition]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicaltestdefinition](
	[id] [int] NOT NULL,
	[parent_code] [nvarchar](25) NOT NULL,
	[test_code] [nvarchar](25) NOT NULL,
	[version] [int] NOT NULL,
	[en_name] [nvarchar](150) NOT NULL,
	[fr_name] [nvarchar](150) NOT NULL,
	[is_public] [bit] NOT NULL,
	[active] [bit] NOT NULL,
	[retest_period] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[archived] [bit] NOT NULL,
	[version_notes] [nvarchar](500) NOT NULL,
	[count_up] [bit] NOT NULL,
	[is_uit] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicaltestsection]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicaltestsection](
	[id] [int] NOT NULL,
	[order] [int] NOT NULL,
	[section_type] [int] NOT NULL,
	[default_time] [int] NULL,
	[en_title] [nvarchar](150) NOT NULL,
	[fr_title] [nvarchar](150) NOT NULL,
	[next_section_button_type] [int] NOT NULL,
	[scoring_type] [int] NOT NULL,
	[minimum_score] [int] NULL,
	[uses_notepad] [bit] NULL,
	[block_cheating] [bit] NULL,
	[default_tab] [int] NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[test_definition_id] [int] NULL,
	[uses_calculator] [bit] NOT NULL,
	[item_exposure] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicaltestsectioncomponent]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicaltestsectioncomponent](
	[id] [int] NOT NULL,
	[order] [int] NOT NULL,
	[component_type] [int] NOT NULL,
	[en_title] [nvarchar](150) NOT NULL,
	[fr_title] [nvarchar](150) NOT NULL,
	[shuffle_all_questions] [bit] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[language_id] [nvarchar](2) NULL,
	[shuffle_question_blocks] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicaltestsectionreducer]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicaltestsectionreducer](
	[id] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[reducer_id] [nvarchar](20) NULL,
	[test_section_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalviewedquestions]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalviewedquestions](
	[id] [int] NOT NULL,
	[ppc_question_id] [nvarchar](25) NOT NULL,
	[count] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_itemattributevalue]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itemattributevalue](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[item_id] [int] NOT NULL,
	[item_bank_attribute_value_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_itemattributevaluedrafts]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itemattributevaluedrafts](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[system_id] [nvarchar](15) NOT NULL,
	[item_id] [int] NOT NULL,
	[item_bank_attribute_value_id] [int] NOT NULL,
	[username_id] [nvarchar](150) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_itembank]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itembank](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[custom_item_bank_id] [nvarchar](25) NOT NULL,
	[en_name] [nvarchar](150) NOT NULL,
	[fr_name] [nvarchar](150) NOT NULL,
	[archived] [bit] NOT NULL,
	[department_id] [int] NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[comments] [nvarchar](200) NOT NULL,
	[language_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [must_be_a_unique_custom_item_bank_id] UNIQUE NONCLUSTERED 
(
	[custom_item_bank_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_itembankaccesstypes]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itembankaccesstypes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[en_description] [nvarchar](150) NOT NULL,
	[fr_description] [nvarchar](150) NOT NULL,
	[codename] [nvarchar](25) NOT NULL,
	[priority] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [must_be_a_unique_item_bank_access_type_codename] UNIQUE NONCLUSTERED 
(
	[codename] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[codename] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_itembankattributes]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itembankattributes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[attribute_order] [int] NOT NULL,
	[item_bank_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_itembankpermissions]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itembankpermissions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[item_bank_id] [int] NOT NULL,
	[item_bank_access_type_id] [int] NOT NULL,
	[username_id] [nvarchar](150) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_itembankrule]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itembankrule](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[order] [int] NULL,
	[item_bank_id] [int] NOT NULL,
	[item_bank_bundle_id] [int] NOT NULL,
	[test_section_component_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_itemcomments]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itemcomments](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[comment] [nvarchar](255) NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[username_id] [nvarchar](150) NOT NULL,
	[system_id] [nvarchar](15) NOT NULL,
	[version] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_multiplechoicequestion]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_multiplechoicequestion](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[question_difficulty_type] [int] NOT NULL,
	[question_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_newquestion]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_newquestion](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[question_type] [int] NOT NULL,
	[pilot] [bit] NOT NULL,
	[question_block_type_id] [int] NULL,
	[test_section_component_id] [int] NOT NULL,
	[order] [int] NOT NULL,
	[dependent_order] [int] NOT NULL,
	[shuffle_answer_choices] [bit] NOT NULL,
	[ppc_question_id] [nvarchar](25) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_newquestion_dependencies]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_newquestion_dependencies](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[from_newquestion_id] [int] NOT NULL,
	[to_newquestion_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_nextsectionbuttontypepopup]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_nextsectionbuttontypepopup](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[content] [nvarchar](max) NOT NULL,
	[title] [nvarchar](max) NOT NULL,
	[button_text] [nvarchar](50) NOT NULL,
	[confirm_proceed] [bit] NOT NULL,
	[language_id] [nvarchar](2) NOT NULL,
	[test_section_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_nextsectionbuttontypeproceed]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_nextsectionbuttontypeproceed](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[button_text] [nvarchar](max) NOT NULL,
	[language_id] [nvarchar](2) NOT NULL,
	[test_section_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_orderlesstestpermissions]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_orderlesstestpermissions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[parent_code] [nvarchar](25) NOT NULL,
	[test_code] [nvarchar](25) NOT NULL,
	[date_assigned] [datetime2](7) NOT NULL,
	[ta_extended_profile_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_pagesection]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_pagesection](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[order] [int] NOT NULL,
	[page_section_type] [int] NOT NULL,
	[section_component_page_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_pagesectiontypeimagezoom]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_pagesectiontypeimagezoom](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[large_image] [nvarchar](250) NOT NULL,
	[small_image] [nvarchar](250) NOT NULL,
	[language_id] [nvarchar](2) NOT NULL,
	[page_section_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_pagesectiontypemarkdown]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_pagesectiontypemarkdown](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[content] [nvarchar](max) NOT NULL,
	[language_id] [nvarchar](2) NOT NULL,
	[page_section_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_pagesectiontypesampleemail]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_pagesectiontypesampleemail](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[email_id] [int] NOT NULL,
	[subject_field] [nvarchar](max) NOT NULL,
	[from_field] [nvarchar](max) NOT NULL,
	[to_field] [nvarchar](max) NOT NULL,
	[date_field] [nvarchar](max) NOT NULL,
	[body] [nvarchar](max) NOT NULL,
	[language_id] [nvarchar](2) NOT NULL,
	[page_section_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_pagesectiontypesampleemailresponse]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_pagesectiontypesampleemailresponse](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[to_field] [nvarchar](max) NOT NULL,
	[cc_field] [nvarchar](max) NULL,
	[response] [nvarchar](max) NULL,
	[reason] [nvarchar](max) NULL,
	[language_id] [nvarchar](2) NOT NULL,
	[page_section_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_pagesectiontypesampletaskresponse]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_pagesectiontypesampletaskresponse](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[response] [nvarchar](max) NULL,
	[reason] [nvarchar](max) NULL,
	[language_id] [nvarchar](2) NOT NULL,
	[page_section_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_pagesectiontypetreedescription]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_pagesectiontypetreedescription](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[address_book_contact_id] [int] NOT NULL,
	[language_id] [nvarchar](2) NOT NULL,
	[page_section_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_questionblocktype]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_questionblocktype](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](150) NOT NULL,
	[test_definition_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_questionlistrule]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_questionlistrule](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[number_of_questions] [int] NOT NULL,
	[order] [int] NULL,
	[shuffle] [bit] NOT NULL,
	[test_section_component_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_questionlistrule_question_block_type]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_questionlistrule_question_block_type](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[questionlistrule_id] [int] NOT NULL,
	[questionblocktype_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_questionsection]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_questionsection](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[order] [int] NOT NULL,
	[question_section_type] [int] NOT NULL,
	[question_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_questionsectiontypemarkdown]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_questionsectiontypemarkdown](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[content] [nvarchar](max) NOT NULL,
	[language_id] [nvarchar](2) NOT NULL,
	[question_section_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_questionsituation]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_questionsituation](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[situation] [nvarchar](max) NOT NULL,
	[language_id] [nvarchar](2) NOT NULL,
	[question_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_reducercontrol]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_reducercontrol](
	[reducer] [nvarchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[reducer] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_scoringmethods]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_scoringmethods](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[method_type_id] [int] NOT NULL,
	[test_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_scoringmethodtypes]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_scoringmethodtypes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[method_type_codename] [nvarchar](50) NOT NULL,
	[en_name] [nvarchar](max) NOT NULL,
	[fr_name] [nvarchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [must_be_a_unique_scoring_method_type_codename] UNIQUE NONCLUSTERED 
(
	[method_type_codename] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_scoringpassfail]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_scoringpassfail](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[minimum_score] [int] NOT NULL,
	[scoring_method_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_scoringthreshold]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_scoringthreshold](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[minimum_score] [int] NOT NULL,
	[maximum_score] [int] NOT NULL,
	[en_conversion_value] [nvarchar](max) NOT NULL,
	[scoring_method_id] [int] NOT NULL,
	[fr_conversion_value] [nvarchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_sectioncomponentpage]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_sectioncomponentpage](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[order] [int] NOT NULL,
	[en_title] [nvarchar](150) NOT NULL,
	[fr_title] [nvarchar](150) NOT NULL,
	[test_section_component_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_situationexamplerating]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_situationexamplerating](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[score] [float] NOT NULL,
	[question_id] [int] NOT NULL,
	[competency_type_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_situationexampleratingdetails]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_situationexampleratingdetails](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[example] [nvarchar](max) NOT NULL,
	[example_rating_id] [int] NOT NULL,
	[language_id] [nvarchar](2) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_testpermissions]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_testpermissions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[date_assigned] [datetime2](7) NOT NULL,
	[expiry_date] [date] NOT NULL,
	[test_order_number] [nvarchar](12) NOT NULL,
	[staffing_process_number] [nvarchar](50) NOT NULL,
	[department_ministry_code] [nvarchar](10) NOT NULL,
	[is_org] [nvarchar](16) NOT NULL,
	[is_ref] [nvarchar](20) NOT NULL,
	[billing_contact] [nvarchar](180) NOT NULL,
	[billing_contact_info] [nvarchar](255) NOT NULL,
	[test_id] [int] NOT NULL,
	[username_id] [nvarchar](150) NOT NULL,
	[reason_for_modif_or_del] [nvarchar](300) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_testsection]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_testsection](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[order] [int] NOT NULL,
	[section_type] [int] NOT NULL,
	[default_time] [int] NULL,
	[en_title] [nvarchar](150) NOT NULL,
	[fr_title] [nvarchar](150) NOT NULL,
	[next_section_button_type] [int] NOT NULL,
	[scoring_type] [int] NOT NULL,
	[uses_notepad] [bit] NULL,
	[block_cheating] [bit] NULL,
	[default_tab] [int] NULL,
	[test_definition_id] [int] NOT NULL,
	[minimum_score] [int] NULL,
	[uses_calculator] [bit] NOT NULL,
	[item_exposure] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_testsectioncomponent]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_testsectioncomponent](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[order] [int] NOT NULL,
	[component_type] [int] NOT NULL,
	[en_title] [nvarchar](150) NOT NULL,
	[fr_title] [nvarchar](150) NOT NULL,
	[shuffle_all_questions] [bit] NOT NULL,
	[language_id] [nvarchar](2) NULL,
	[shuffle_question_blocks] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_testsectioncomponent_test_section]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_testsectioncomponent_test_section](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[testsectioncomponent_id] [int] NOT NULL,
	[testsection_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_testsectionreducer]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_testsectionreducer](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[reducer_id] [nvarchar](20) NOT NULL,
	[test_section_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_viewedquestions]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_viewedquestions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ppc_question_id] [nvarchar](25) NOT NULL,
	[count] [int] NOT NULL,
	[user_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_accommodationrequest]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_accommodationrequest](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[break_bank_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_additionaltime]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_additionaltime](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[test_section_time] [int] NOT NULL,
	[accommodation_request_id] [int] NOT NULL,
	[test_section_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_assignedanswerchoices]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_assignedanswerchoices](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[answer_choices] [nvarchar](max) NOT NULL,
	[assigned_test_id] [int] NOT NULL,
	[question_id] [int] NULL,
	[item_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_assignedquestionslist]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_assignedquestionslist](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[questions_list] [nvarchar](max) NOT NULL,
	[assigned_test_id] [int] NOT NULL,
	[test_section_id] [int] NOT NULL,
	[from_item_bank] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_assignedtestanswerscore]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_assignedtestanswerscore](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[rationale] [nvarchar](max) NOT NULL,
	[score] [float] NOT NULL,
	[question_id] [int] NOT NULL,
	[scorer_assigned_test_id] [int] NOT NULL,
	[competency_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_assignedtestsectionaccesstimes]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_assignedtestsectionaccesstimes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[time_type] [int] NOT NULL,
	[time] [datetime2](7) NOT NULL,
	[assigned_test_section_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_breakbank]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_breakbank](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[break_time] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_breakbankactions]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_breakbankactions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[action_type] [nvarchar](15) NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[new_remaining_time] [int] NULL,
	[break_bank_id] [int] NOT NULL,
	[test_section_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_candidateanswers]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_candidateanswers](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[assigned_test_id] [int] NOT NULL,
	[question_id] [int] NULL,
	[test_section_component_id] [int] NOT NULL,
	[mark_for_review] [bit] NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[selected_language_id] [int] NULL,
	[item_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_candidateemailresponseanswers]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_candidateemailresponseanswers](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[answer_id] [int] NOT NULL,
	[email_type] [nvarchar](25) NOT NULL,
	[email_body] [nvarchar](max) NULL,
	[reasons_for_action] [nvarchar](max) NULL,
	[candidate_answers_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_candidateemailresponseanswers_email_cc]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_candidateemailresponseanswers_email_cc](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[candidateemailresponseanswers_id] [int] NOT NULL,
	[addressbookcontact_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_candidateemailresponseanswers_email_to]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_candidateemailresponseanswers_email_to](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[candidateemailresponseanswers_id] [int] NOT NULL,
	[addressbookcontact_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_candidatemultiplechoiceanswers]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_candidatemultiplechoiceanswers](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[answer_id] [int] NULL,
	[candidate_answers_id] [int] NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[item_answer_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_candidatetaskresponseanswers]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_candidatetaskresponseanswers](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[answer_id] [int] NOT NULL,
	[task] [nvarchar](max) NULL,
	[reasons_for_action] [nvarchar](max) NULL,
	[candidate_answers_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_criticality]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_criticality](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[description_en] [nvarchar](150) NOT NULL,
	[description_fr] [nvarchar](150) NOT NULL,
	[active] [bit] NOT NULL,
	[codename] [nvarchar](50) NOT NULL,
	[priority] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [must_be_a_unique_criticality_codename] UNIQUE NONCLUSTERED 
(
	[codename] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_databasecheckmodel]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_databasecheckmodel](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_ettaactions]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_ettaactions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[action_reason] [nvarchar](300) NULL,
	[action_type_id] [nvarchar](25) NOT NULL,
	[assigned_test_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_ettaactiontypes]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_ettaactiontypes](
	[action_type] [nvarchar](25) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[action_type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalaccommodationrequest]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalaccommodationrequest](
	[id] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[break_bank_id] [int] NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaladditionaltime]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaladditionaltime](
	[id] [int] NOT NULL,
	[test_section_time] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[accommodation_request_id] [int] NULL,
	[history_user_id] [int] NULL,
	[test_section_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalassignedanswerchoices]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalassignedanswerchoices](
	[id] [int] NOT NULL,
	[answer_choices] [nvarchar](max) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[assigned_test_id] [int] NULL,
	[history_user_id] [int] NULL,
	[question_id] [int] NULL,
	[item_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalassignedquestionslist]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalassignedquestionslist](
	[id] [int] NOT NULL,
	[questions_list] [nvarchar](max) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[assigned_test_id] [int] NULL,
	[history_user_id] [int] NULL,
	[test_section_id] [int] NULL,
	[from_item_bank] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalassignedtest]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalassignedtest](
	[id] [int] NOT NULL,
	[status] [int] NOT NULL,
	[previous_status] [int] NULL,
	[start_date] [datetime2](7) NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[submit_date] [datetime2](7) NULL,
	[test_access_code] [nvarchar](10) NULL,
	[test_order_number] [nvarchar](12) NULL,
	[total_score] [int] NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[ta_id] [nvarchar](150) NULL,
	[test_id] [int] NULL,
	[test_section_id] [int] NULL,
	[test_session_language_id] [int] NULL,
	[username_id] [nvarchar](150) NULL,
	[en_converted_score] [nvarchar](50) NULL,
	[fr_converted_score] [nvarchar](50) NULL,
	[is_invalid] [bit] NOT NULL,
	[uit_invite_id] [int] NULL,
	[orderless_financial_data_id] [int] NULL,
	[accommodation_request_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalassignedtestanswerscore]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalassignedtestanswerscore](
	[id] [int] NOT NULL,
	[rationale] [nvarchar](max) NOT NULL,
	[score] [float] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[competency_id] [int] NULL,
	[history_user_id] [int] NULL,
	[question_id] [int] NULL,
	[scorer_assigned_test_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalassignedtestsection]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalassignedtestsection](
	[id] [int] NOT NULL,
	[test_section_time] [int] NULL,
	[score] [int] NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[assigned_test_id] [int] NULL,
	[history_user_id] [int] NULL,
	[test_section_id] [int] NULL,
	[timed_out] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalassignedtestsectionaccesstimes]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalassignedtestsectionaccesstimes](
	[id] [int] NOT NULL,
	[time_type] [int] NOT NULL,
	[time] [datetime2](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[assigned_test_section_id] [int] NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalbreakbank]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalbreakbank](
	[id] [int] NOT NULL,
	[break_time] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalbreakbankactions]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalbreakbankactions](
	[id] [int] NOT NULL,
	[action_type] [nvarchar](15) NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[new_remaining_time] [int] NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[break_bank_id] [int] NULL,
	[history_user_id] [int] NULL,
	[test_section_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalcandidateanswers]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalcandidateanswers](
	[id] [int] NOT NULL,
	[mark_for_review] [bit] NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[assigned_test_id] [int] NULL,
	[history_user_id] [int] NULL,
	[question_id] [int] NULL,
	[selected_language_id] [int] NULL,
	[test_section_component_id] [int] NULL,
	[item_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalcandidateemailresponseanswers]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalcandidateemailresponseanswers](
	[id] [int] NOT NULL,
	[answer_id] [int] NOT NULL,
	[email_type] [nvarchar](25) NOT NULL,
	[email_body] [nvarchar](max) NULL,
	[reasons_for_action] [nvarchar](max) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[candidate_answers_id] [int] NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalcandidatemultiplechoiceanswers]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalcandidatemultiplechoiceanswers](
	[id] [int] NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[answer_id] [int] NULL,
	[candidate_answers_id] [int] NULL,
	[history_user_id] [int] NULL,
	[item_answer_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalcandidatetaskresponseanswers]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalcandidatetaskresponseanswers](
	[id] [int] NOT NULL,
	[answer_id] [int] NOT NULL,
	[task] [nvarchar](max) NULL,
	[reasons_for_action] [nvarchar](max) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[candidate_answers_id] [int] NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalcriticality]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalcriticality](
	[id] [int] NOT NULL,
	[description_en] [nvarchar](150) NOT NULL,
	[description_fr] [nvarchar](150) NOT NULL,
	[active] [bit] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[codename] [nvarchar](50) NOT NULL,
	[priority] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalettaactions]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalettaactions](
	[id] [int] NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[action_reason] [nvarchar](300) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[action_type_id] [nvarchar](25) NULL,
	[assigned_test_id] [int] NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalettaactiontypes]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalettaactiontypes](
	[action_type] [nvarchar](25) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicallanguage]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicallanguage](
	[language_id] [int] NOT NULL,
	[ISO_Code_1] [nvarchar](2) NOT NULL,
	[ISO_Code_2] [nvarchar](5) NOT NULL,
	[date_created] [datetime2](7) NOT NULL,
	[date_from] [datetime2](7) NOT NULL,
	[date_to] [datetime2](7) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicallanguagetext]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicallanguagetext](
	[id] [int] NOT NULL,
	[text] [nvarchar](50) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[language_id] [int] NULL,
	[language_ref_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicallocktestactions]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicallocktestactions](
	[id] [int] NOT NULL,
	[lock_start_date] [datetime2](7) NOT NULL,
	[lock_end_date] [datetime2](7) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[ta_action_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalnotepad]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalnotepad](
	[notepad] [nvarchar](max) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[assigned_test_id] [int] NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalorderlessfinancialdata]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalorderlessfinancialdata](
	[id] [int] NOT NULL,
	[reference_number] [nvarchar](25) NOT NULL,
	[department_ministry_id] [int] NOT NULL,
	[fis_organisation_code] [nvarchar](16) NOT NULL,
	[fis_reference_code] [nvarchar](20) NOT NULL,
	[billing_contact_name] [nvarchar](180) NOT NULL,
	[billing_contact_info] [nvarchar](255) NOT NULL,
	[level_required] [nvarchar](50) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[reason_for_testing_id] [int] NULL,
	[uit_invite_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalpausetestactions]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalpausetestactions](
	[id] [int] NOT NULL,
	[pause_start_date] [datetime2](7) NOT NULL,
	[pause_test_time] [int] NOT NULL,
	[pause_end_date] [datetime2](7) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[ta_action_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalreasonsfortesting]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalreasonsfortesting](
	[id] [int] NOT NULL,
	[description_en] [nvarchar](255) NOT NULL,
	[description_fr] [nvarchar](255) NOT NULL,
	[active] [bit] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalsystemalert]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalsystemalert](
	[id] [int] NOT NULL,
	[title] [nvarchar](255) NOT NULL,
	[criticality_id] [int] NULL,
	[active_date] [datetime2](7) NOT NULL,
	[end_date] [datetime2](7) NOT NULL,
	[message_text_en] [nvarchar](max) NOT NULL,
	[message_text_fr] [nvarchar](max) NOT NULL,
	[archived] [bit] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaltaactions]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaltaactions](
	[id] [int] NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[action_type_id] [nvarchar](15) NULL,
	[assigned_test_id] [int] NULL,
	[history_user_id] [int] NULL,
	[test_section_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaltaactiontypes]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaltaactiontypes](
	[action_type] [nvarchar](15) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaltestscorerassignment]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaltestscorerassignment](
	[id] [int] NOT NULL,
	[status] [int] NOT NULL,
	[score_date] [datetime2](7) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[assigned_test_id] [int] NULL,
	[history_user_id] [int] NULL,
	[scorer_username_id] [nvarchar](150) NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaluitinviterelatedcandidates]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaluitinviterelatedcandidates](
	[id] [int] NOT NULL,
	[first_name] [nvarchar](30) NOT NULL,
	[last_name] [nvarchar](150) NOT NULL,
	[email] [nvarchar](150) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[uit_invite_id] [int] NULL,
	[reason_for_deletion_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaluitinvites]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaluitinvites](
	[id] [int] NOT NULL,
	[invite_date] [date] NOT NULL,
	[validity_end_date] [date] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[ta_test_permissions_id] [int] NULL,
	[ta_username_id] [nvarchar](150) NULL,
	[reason_for_deletion_id] [int] NULL,
	[reason_for_modification_id] [int] NULL,
	[orderless_request] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaluitreasonsfordeletion]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaluitreasonsfordeletion](
	[id] [int] NOT NULL,
	[en_name] [nvarchar](50) NOT NULL,
	[fr_name] [nvarchar](50) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaluitreasonsformodification]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaluitreasonsformodification](
	[id] [int] NOT NULL,
	[en_name] [nvarchar](50) NOT NULL,
	[fr_name] [nvarchar](50) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_language]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_language](
	[language_id] [int] IDENTITY(1,1) NOT NULL,
	[ISO_Code_1] [nvarchar](2) NOT NULL,
	[ISO_Code_2] [nvarchar](5) NOT NULL,
	[date_created] [datetime2](7) NOT NULL,
	[date_from] [datetime2](7) NOT NULL,
	[date_to] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[language_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[ISO_Code_1] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_languagetext]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_languagetext](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[text] [nvarchar](50) NOT NULL,
	[language_id] [int] NOT NULL,
	[language_ref_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_locktestactions]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_locktestactions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[lock_start_date] [datetime2](7) NOT NULL,
	[lock_end_date] [datetime2](7) NULL,
	[ta_action_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_notepad]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_notepad](
	[assigned_test_id] [int] NOT NULL,
	[notepad] [nvarchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[assigned_test_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_pausetestactions]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_pausetestactions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[pause_start_date] [datetime2](7) NOT NULL,
	[pause_test_time] [int] NOT NULL,
	[pause_end_date] [datetime2](7) NULL,
	[ta_action_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_reasonsfortesting]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_reasonsfortesting](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[description_en] [nvarchar](255) NOT NULL,
	[description_fr] [nvarchar](255) NOT NULL,
	[active] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_systemalert]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_systemalert](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](255) NOT NULL,
	[criticality_id] [int] NOT NULL,
	[active_date] [datetime2](7) NOT NULL,
	[end_date] [datetime2](7) NOT NULL,
	[message_text_en] [nvarchar](max) NOT NULL,
	[message_text_fr] [nvarchar](max) NOT NULL,
	[archived] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_taactions]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_taactions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[action_type_id] [nvarchar](15) NOT NULL,
	[assigned_test_id] [int] NOT NULL,
	[test_section_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_taactiontypes]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_taactiontypes](
	[action_type] [nvarchar](15) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[action_type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_testscorerassignment]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_testscorerassignment](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[status] [int] NOT NULL,
	[score_date] [datetime2](7) NULL,
	[assigned_test_id] [int] NOT NULL,
	[scorer_username_id] [nvarchar](150) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_uitinviterelatedcandidates]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_uitinviterelatedcandidates](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[first_name] [nvarchar](30) NOT NULL,
	[last_name] [nvarchar](150) NOT NULL,
	[email] [nvarchar](150) NOT NULL,
	[uit_invite_id] [int] NOT NULL,
	[reason_for_deletion_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_uitreasonsfordeletion]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_uitreasonsfordeletion](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[en_name] [nvarchar](50) NOT NULL,
	[fr_name] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_uitreasonsformodification]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_uitreasonsformodification](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[en_name] [nvarchar](50) NOT NULL,
	[fr_name] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[django_admin_log]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[django_admin_log](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[action_time] [datetime2](7) NOT NULL,
	[object_id] [nvarchar](max) NULL,
	[object_repr] [nvarchar](200) NOT NULL,
	[action_flag] [smallint] NOT NULL,
	[change_message] [nvarchar](max) NOT NULL,
	[content_type_id] [int] NULL,
	[user_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[django_celery_beat_clockedschedule]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[django_celery_beat_clockedschedule](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[clocked_time] [datetime2](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[django_celery_beat_crontabschedule]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[django_celery_beat_crontabschedule](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[minute] [nvarchar](240) NOT NULL,
	[hour] [nvarchar](96) NOT NULL,
	[day_of_week] [nvarchar](64) NOT NULL,
	[day_of_month] [nvarchar](124) NOT NULL,
	[month_of_year] [nvarchar](64) NOT NULL,
	[timezone] [nvarchar](63) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[django_celery_beat_intervalschedule]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[django_celery_beat_intervalschedule](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[every] [int] NOT NULL,
	[period] [nvarchar](24) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[django_celery_beat_periodictask]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[django_celery_beat_periodictask](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](200) NOT NULL,
	[task] [nvarchar](200) NOT NULL,
	[args] [nvarchar](max) NOT NULL,
	[kwargs] [nvarchar](max) NOT NULL,
	[queue] [nvarchar](200) NULL,
	[exchange] [nvarchar](200) NULL,
	[routing_key] [nvarchar](200) NULL,
	[expires] [datetime2](7) NULL,
	[enabled] [bit] NOT NULL,
	[last_run_at] [datetime2](7) NULL,
	[total_run_count] [int] NOT NULL,
	[date_changed] [datetime2](7) NOT NULL,
	[description] [nvarchar](max) NOT NULL,
	[crontab_id] [int] NULL,
	[interval_id] [int] NULL,
	[solar_id] [int] NULL,
	[one_off] [bit] NOT NULL,
	[start_time] [datetime2](7) NULL,
	[priority] [int] NULL,
	[headers] [nvarchar](max) NOT NULL,
	[clocked_id] [int] NULL,
	[expire_seconds] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[django_celery_beat_periodictasks]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[django_celery_beat_periodictasks](
	[ident] [smallint] NOT NULL,
	[last_update] [datetime2](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ident] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[django_celery_beat_solarschedule]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[django_celery_beat_solarschedule](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[event] [nvarchar](24) NOT NULL,
	[latitude] [numeric](9, 6) NOT NULL,
	[longitude] [numeric](9, 6) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[django_content_type]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[django_content_type](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[app_label] [nvarchar](100) NOT NULL,
	[model] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[django_migrations]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[django_migrations](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[app] [nvarchar](255) NOT NULL,
	[name] [nvarchar](255) NOT NULL,
	[applied] [datetime2](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[django_rest_passwordreset_resetpasswordtoken]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[django_rest_passwordreset_resetpasswordtoken](
	[created_at] [datetime2](7) NOT NULL,
	[key] [nvarchar](64) NOT NULL,
	[ip_address] [nvarchar](39) NULL,
	[user_agent] [nvarchar](256) NOT NULL,
	[user_id] [int] NOT NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [django_rest_passwordreset_resetpasswordtoken_key_f1b65873_uniq] UNIQUE NONCLUSTERED 
(
	[key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[django_session]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[django_session](
	[session_key] [nvarchar](40) NOT NULL,
	[session_data] [nvarchar](max) NOT NULL,
	[expire_date] [datetime2](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[session_key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[oltf_classification_vw]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[oltf_classification_vw](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[classif_id] [int] NOT NULL,
	[active_flg] [nvarchar](1) NOT NULL,
	[class_sbgrp_cd] [nvarchar](3) NOT NULL,
	[class_grp_cd] [nvarchar](2) NOT NULL,
	[class_lvl_cd] [nvarchar](2) NOT NULL,
	[efdt] [datetime2](7) NOT NULL,
	[xdt] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[oltf_gender_vw]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[oltf_gender_vw](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[gnd_id] [int] NOT NULL,
	[active_flg] [nvarchar](1) NOT NULL,
	[eabrv] [nvarchar](10) NULL,
	[fabrv] [nvarchar](10) NULL,
	[legacy_cd] [int] NULL,
	[edesc] [nvarchar](200) NOT NULL,
	[fdesc] [nvarchar](200) NOT NULL,
	[efdt] [datetime2](7) NOT NULL,
	[xdt] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[oltf_organisation_vw]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[oltf_organisation_vw](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[dept_id] [int] NOT NULL,
	[active_flg] [nvarchar](1) NOT NULL,
	[eabrv] [nvarchar](20) NULL,
	[fabrv] [nvarchar](20) NULL,
	[legacy_cd] [nvarchar](3) NULL,
	[edesc] [nvarchar](200) NOT NULL,
	[fdesc] [nvarchar](200) NOT NULL,
	[efdt] [datetime2](7) NOT NULL,
	[xdt] [datetime2](7) NULL,
	[org_empl_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[oltf_province_vw]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[oltf_province_vw](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[prov_id] [int] NOT NULL,
	[active_flg] [nvarchar](1) NOT NULL,
	[eabrv] [nvarchar](10) NULL,
	[fabrv] [nvarchar](10) NULL,
	[legacy_cd] [nvarchar](2) NULL,
	[edesc] [nvarchar](200) NOT NULL,
	[fdesc] [nvarchar](200) NOT NULL,
	[efdt] [datetime2](7) NOT NULL,
	[xdt] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ref_table_views_cateducation]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ref_table_views_cateducation](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[education_id] [int] NOT NULL,
	[active_flg] [nvarchar](1) NOT NULL,
	[edesc] [nvarchar](200) NOT NULL,
	[fdesc] [nvarchar](200) NOT NULL,
	[efdt] [datetime2](7) NOT NULL,
	[xdt] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ref_table_views_historicalcateducation]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ref_table_views_historicalcateducation](
	[education_id] [int] NOT NULL,
	[active_flg] [nvarchar](1) NOT NULL,
	[edesc] [nvarchar](200) NOT NULL,
	[fdesc] [nvarchar](200) NOT NULL,
	[efdt] [datetime2](7) NOT NULL,
	[xdt] [datetime2](7) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ref_table_views_historicalcatrefaboriginalvw]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ref_table_views_historicalcatrefaboriginalvw](
	[ABRG_ID] [int] NOT NULL,
	[ACTIVE_FLG] [nvarchar](1) NOT NULL,
	[EABRV] [nvarchar](10) NULL,
	[FABRV] [nvarchar](10) NULL,
	[LEGACY_CD] [int] NULL,
	[EDESC] [nvarchar](200) NOT NULL,
	[FDESC] [nvarchar](200) NOT NULL,
	[EFDT] [datetime2](7) NOT NULL,
	[XDT] [datetime2](7) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ref_table_views_historicalcatrefclassificationvw]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ref_table_views_historicalcatrefclassificationvw](
	[CLASSIF_ID] [int] NOT NULL,
	[ACTIVE_FLG] [nvarchar](1) NOT NULL,
	[CLASS_GRP_CD] [nvarchar](2) NOT NULL,
	[CLASS_SBGRP_CD] [nvarchar](3) NULL,
	[CLASS_LVL_CD] [nvarchar](2) NOT NULL,
	[EFDT] [datetime2](7) NOT NULL,
	[XDT] [datetime2](7) NULL,
	[PSC_CLASS_IND] [int] NOT NULL,
	[DFLT_BUD_CD] [nvarchar](5) NULL,
	[DFLT_PAY_ZN_CD] [nvarchar](3) NULL,
	[EQLZN_AMT] [int] NULL,
	[EQLZN_AMT_EFDT] [datetime2](7) NULL,
	[EQLZN_AMT_XDT] [datetime2](7) NULL,
	[OFCR_LVL_IND] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ref_table_views_historicalcatrefdepartmentsvw]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ref_table_views_historicalcatrefdepartmentsvw](
	[DEPT_ID] [int] NOT NULL,
	[ACTIVE_FLG] [nvarchar](1) NOT NULL,
	[EABRV] [nvarchar](20) NULL,
	[FABRV] [nvarchar](20) NULL,
	[LEGACY_CD] [nvarchar](3) NULL,
	[EDESC] [nvarchar](200) NOT NULL,
	[FDESC] [nvarchar](200) NOT NULL,
	[EFDT] [datetime2](7) NOT NULL,
	[XDT] [datetime2](7) NULL,
	[ESEARCH_DESC] [nvarchar](200) NULL,
	[FSEARCH_DESC] [nvarchar](200) NULL,
	[RCVER_GNRL_NBR] [int] NULL,
	[PSEA_ID] [int] NOT NULL,
	[PSSA_ID] [int] NULL,
	[PSSRA_ID] [int] NULL,
	[OLA_ID] [int] NULL,
	[ORG_TYP_ID] [int] NULL,
	[ORG_EMPL_ID] [int] NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ref_table_views_historicalcatrefdisabilityvw]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ref_table_views_historicalcatrefdisabilityvw](
	[DSBL_ID] [int] NOT NULL,
	[ACTIVE_FLG] [nvarchar](1) NOT NULL,
	[EABRV] [nvarchar](10) NULL,
	[FABRV] [nvarchar](10) NULL,
	[LEGACY_CD] [int] NULL,
	[EDESC] [nvarchar](200) NOT NULL,
	[FDESC] [nvarchar](200) NOT NULL,
	[FULL_EDESC] [nvarchar](200) NULL,
	[FULL_FDESC] [nvarchar](200) NULL,
	[EFDT] [datetime2](7) NOT NULL,
	[XDT] [datetime2](7) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ref_table_views_historicalcatrefemployerstsvw]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ref_table_views_historicalcatrefemployerstsvw](
	[EMPSTS_ID] [int] NOT NULL,
	[ACTIVE_FLG] [nvarchar](1) NOT NULL,
	[EDESC] [nvarchar](200) NOT NULL,
	[FDESC] [nvarchar](200) NOT NULL,
	[EFDT] [datetime2](7) NOT NULL,
	[XDT] [datetime2](7) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ref_table_views_historicalcatrefgendervw]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ref_table_views_historicalcatrefgendervw](
	[GND_ID] [int] NOT NULL,
	[ACTIVE_FLG] [nvarchar](1) NOT NULL,
	[EABRV] [nvarchar](10) NULL,
	[FABRV] [nvarchar](10) NULL,
	[LEGACY_CD] [int] NULL,
	[EDESC] [nvarchar](200) NOT NULL,
	[FDESC] [nvarchar](200) NOT NULL,
	[EFDT] [datetime2](7) NOT NULL,
	[XDT] [datetime2](7) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ref_table_views_historicalcatrefprovincevw]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ref_table_views_historicalcatrefprovincevw](
	[PROV_ID] [int] NOT NULL,
	[ACTIVE_FLG] [nvarchar](1) NOT NULL,
	[EABRV] [nvarchar](10) NULL,
	[FABRV] [nvarchar](10) NULL,
	[LEGACY_CD] [nvarchar](3) NULL,
	[EDESC] [nvarchar](200) NOT NULL,
	[FDESC] [nvarchar](200) NOT NULL,
	[EFDT] [datetime2](7) NOT NULL,
	[XDT] [datetime2](7) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ref_table_views_historicalcatrefvisibleminorityvw]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ref_table_views_historicalcatrefvisibleminorityvw](
	[VISMIN_ID] [int] NOT NULL,
	[ACTIVE_FLG] [nvarchar](1) NOT NULL,
	[EABRV] [nvarchar](10) NULL,
	[FABRV] [nvarchar](10) NULL,
	[LEGACY_CD] [int] NULL,
	[EDESC] [nvarchar](200) NOT NULL,
	[FDESC] [nvarchar](200) NOT NULL,
	[FULL_EDESC] [nvarchar](200) NULL,
	[FULL_FDESC] [nvarchar](200) NULL,
	[EFDT] [datetime2](7) NOT NULL,
	[XDT] [datetime2](7) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_accommodations]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_accommodations](
	[user_id] [int] NOT NULL,
	[font_family] [nvarchar](75) NULL,
	[font_size] [nvarchar](10) NULL,
	[spacing] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_custompermissions]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_custompermissions](
	[permission_id] [int] IDENTITY(1,1) NOT NULL,
	[en_name] [nvarchar](75) NOT NULL,
	[en_description] [nvarchar](max) NOT NULL,
	[fr_description] [nvarchar](max) NOT NULL,
	[codename] [nvarchar](25) NOT NULL,
	[date_assigned] [datetime2](7) NOT NULL,
	[expiry_date] [datetime2](7) NULL,
	[content_type_id] [int] NOT NULL,
	[fr_name] [nvarchar](75) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[permission_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [must_be_a_unique_codename] UNIQUE NONCLUSTERED 
(
	[codename] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[codename] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_customuserpermissions]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_customuserpermissions](
	[user_permission_id] [int] IDENTITY(1,1) NOT NULL,
	[permission_id] [int] NOT NULL,
	[user_id] [nvarchar](150) NOT NULL,
	[goc_email] [nvarchar](254) NOT NULL,
	[pri_or_military_nbr] [nvarchar](10) NOT NULL,
	[rationale] [nvarchar](300) NOT NULL,
	[supervisor] [nvarchar](180) NOT NULL,
	[supervisor_email] [nvarchar](254) NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[reason_for_modif_or_del] [nvarchar](300) NULL,
PRIMARY KEY CLUSTERED 
(
	[user_permission_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [one_username_per_permission_id_instance] UNIQUE NONCLUSTERED 
(
	[user_id] ASC,
	[permission_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_eeinfooptions]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_eeinfooptions](
	[opt_id] [int] NOT NULL,
	[opt_value] [nvarchar](25) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[opt_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_historicalaccommodations]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_historicalaccommodations](
	[font_family] [nvarchar](75) NULL,
	[font_size] [nvarchar](10) NULL,
	[spacing] [bit] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_historicalcustompermissions]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_historicalcustompermissions](
	[permission_id] [int] NOT NULL,
	[en_name] [nvarchar](75) NOT NULL,
	[fr_name] [nvarchar](75) NOT NULL,
	[en_description] [nvarchar](max) NOT NULL,
	[fr_description] [nvarchar](max) NOT NULL,
	[codename] [nvarchar](25) NOT NULL,
	[date_assigned] [datetime2](7) NOT NULL,
	[expiry_date] [datetime2](7) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[content_type_id] [int] NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_historicaleeinfooptions]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_historicaleeinfooptions](
	[opt_id] [int] NOT NULL,
	[opt_value] [nvarchar](25) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_historicaltaextendedprofile]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_historicaltaextendedprofile](
	[id] [int] NOT NULL,
	[department_id] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[username_id] [nvarchar](150) NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_historicaluserextendedprofile]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_historicaluserextendedprofile](
	[id] [int] NOT NULL,
	[current_employer_id] [int] NOT NULL,
	[organization_id] [int] NOT NULL,
	[group_id] [int] NOT NULL,
	[level_id] [int] NOT NULL,
	[residence_id] [int] NOT NULL,
	[education_id] [int] NOT NULL,
	[gender_id] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[user_id] [int] NULL,
	[aboriginal_id] [int] NULL,
	[disability_id] [int] NULL,
	[identify_as_woman_id] [int] NULL,
	[visible_minority_id] [int] NULL,
	[subgroup_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_historicaluserextendedprofileaboriginal]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_historicaluserextendedprofileaboriginal](
	[id] [int] NOT NULL,
	[aboriginal_id] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[user_extended_profile_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_historicaluserextendedprofiledisability]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_historicaluserextendedprofiledisability](
	[id] [int] NOT NULL,
	[other_specification] [nvarchar](200) NULL,
	[disability_id] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[user_extended_profile_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_historicaluserextendedprofilevisibleminority]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_historicaluserextendedprofilevisibleminority](
	[id] [int] NOT NULL,
	[other_specification] [nvarchar](200) NULL,
	[visible_minority_id] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[user_extended_profile_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_historicaluserpasswordresettracking]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_historicaluserpasswordresettracking](
	[id] [int] NOT NULL,
	[request_attempts] [int] NOT NULL,
	[last_request_date] [datetime2](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_historicaluserprofilechangerequest]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_historicaluserprofilechangerequest](
	[id] [int] NOT NULL,
	[current_first_name] [nvarchar](30) NOT NULL,
	[current_last_name] [nvarchar](150) NOT NULL,
	[current_birth_date] [date] NOT NULL,
	[new_first_name] [nvarchar](30) NULL,
	[new_last_name] [nvarchar](150) NULL,
	[new_birth_date] [date] NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[comments] [nvarchar](max) NULL,
	[reason_for_deny] [nvarchar](255) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_permissionrequest]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_permissionrequest](
	[permission_request_id] [int] IDENTITY(1,1) NOT NULL,
	[goc_email] [nvarchar](254) NOT NULL,
	[pri_or_military_nbr] [nvarchar](10) NOT NULL,
	[supervisor] [nvarchar](180) NOT NULL,
	[supervisor_email] [nvarchar](254) NOT NULL,
	[rationale] [nvarchar](300) NOT NULL,
	[request_date] [datetime2](7) NOT NULL,
	[permission_requested_id] [int] NOT NULL,
	[username_id] [nvarchar](150) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[permission_request_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [one_username_per_permission_requested_instance] UNIQUE NONCLUSTERED 
(
	[username_id] ASC,
	[permission_requested_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_user_groups]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_user_groups](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NOT NULL,
	[group_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_user_user_permissions]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_user_user_permissions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NOT NULL,
	[permission_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_userextendedprofile]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_userextendedprofile](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[current_employer_id] [int] NOT NULL,
	[organization_id] [int] NOT NULL,
	[group_id] [int] NOT NULL,
	[level_id] [int] NOT NULL,
	[residence_id] [int] NOT NULL,
	[education_id] [int] NOT NULL,
	[gender_id] [int] NOT NULL,
	[user_id] [int] NOT NULL,
	[aboriginal_id] [int] NOT NULL,
	[disability_id] [int] NOT NULL,
	[identify_as_woman_id] [int] NOT NULL,
	[visible_minority_id] [int] NOT NULL,
	[subgroup_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_userextendedprofileaboriginal]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_userextendedprofileaboriginal](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[aboriginal_id] [int] NOT NULL,
	[user_extended_profile_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_userextendedprofiledisability]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_userextendedprofiledisability](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[other_specification] [nvarchar](200) NULL,
	[disability_id] [int] NOT NULL,
	[user_extended_profile_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_userextendedprofilevisibleminority]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_userextendedprofilevisibleminority](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[other_specification] [nvarchar](200) NULL,
	[visible_minority_id] [int] NOT NULL,
	[user_extended_profile_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_userpasswordresettracking]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_userpasswordresettracking](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[request_attempts] [int] NOT NULL,
	[last_request_date] [datetime2](7) NOT NULL,
	[user_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_userprofilechangerequest]    Script Date: 2023-11-02 07:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_userprofilechangerequest](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[current_first_name] [nvarchar](30) NOT NULL,
	[current_last_name] [nvarchar](150) NOT NULL,
	[current_birth_date] [date] NOT NULL,
	[new_first_name] [nvarchar](30) NULL,
	[new_last_name] [nvarchar](150) NULL,
	[new_birth_date] [date] NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[comments] [nvarchar](max) NULL,
	[reason_for_deny] [nvarchar](255) NULL,
	[user_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[auth_group_permissions]  WITH CHECK ADD  CONSTRAINT [auth_group_permissions_group_id_b120cbf9_fk_auth_group_id] FOREIGN KEY([group_id])
REFERENCES [dbo].[auth_group] ([id])
GO
ALTER TABLE [dbo].[auth_group_permissions] CHECK CONSTRAINT [auth_group_permissions_group_id_b120cbf9_fk_auth_group_id]
GO
ALTER TABLE [dbo].[auth_group_permissions]  WITH CHECK ADD  CONSTRAINT [auth_group_permissions_permission_id_84c5c92e_fk_auth_permission_id] FOREIGN KEY([permission_id])
REFERENCES [dbo].[auth_permission] ([id])
GO
ALTER TABLE [dbo].[auth_group_permissions] CHECK CONSTRAINT [auth_group_permissions_permission_id_84c5c92e_fk_auth_permission_id]
GO
ALTER TABLE [dbo].[auth_permission]  WITH CHECK ADD  CONSTRAINT [auth_permission_content_type_id_2f476e4b_fk_django_content_type_id] FOREIGN KEY([content_type_id])
REFERENCES [dbo].[django_content_type] ([id])
GO
ALTER TABLE [dbo].[auth_permission] CHECK CONSTRAINT [auth_permission_content_type_id_2f476e4b_fk_django_content_type_id]
GO
ALTER TABLE [dbo].[authtoken_token]  WITH CHECK ADD  CONSTRAINT [authtoken_token_user_id_35299eff_fk_user_management_models_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[authtoken_token] CHECK CONSTRAINT [authtoken_token_user_id_35299eff_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_addressbookcontact]  WITH CHECK ADD  CONSTRAINT [cms_models_addressbookcontact_parent_id_79b1b083_fk_cms_models_addressbookcontact_id] FOREIGN KEY([parent_id])
REFERENCES [dbo].[cms_models_addressbookcontact] ([id])
GO
ALTER TABLE [dbo].[cms_models_addressbookcontact] CHECK CONSTRAINT [cms_models_addressbookcontact_parent_id_79b1b083_fk_cms_models_addressbookcontact_id]
GO
ALTER TABLE [dbo].[cms_models_addressbookcontact_test_section]  WITH CHECK ADD  CONSTRAINT [cms_models_addressbookcontact_test_section_addressbookcontact_id_fbad927a_fk_cms_models_addressbookcontact_id] FOREIGN KEY([addressbookcontact_id])
REFERENCES [dbo].[cms_models_addressbookcontact] ([id])
GO
ALTER TABLE [dbo].[cms_models_addressbookcontact_test_section] CHECK CONSTRAINT [cms_models_addressbookcontact_test_section_addressbookcontact_id_fbad927a_fk_cms_models_addressbookcontact_id]
GO
ALTER TABLE [dbo].[cms_models_addressbookcontact_test_section]  WITH CHECK ADD  CONSTRAINT [cms_models_addressbookcontact_test_section_testsection_id_160077cf_fk_cms_models_testsection_id] FOREIGN KEY([testsection_id])
REFERENCES [dbo].[cms_models_testsection] ([id])
GO
ALTER TABLE [dbo].[cms_models_addressbookcontact_test_section] CHECK CONSTRAINT [cms_models_addressbookcontact_test_section_testsection_id_160077cf_fk_cms_models_testsection_id]
GO
ALTER TABLE [dbo].[cms_models_addressbookcontactdetails]  WITH CHECK ADD  CONSTRAINT [cms_models_addressbookcontactdetails_contact_id_f1faffff_fk_cms_models_addressbookcontact_id] FOREIGN KEY([contact_id])
REFERENCES [dbo].[cms_models_addressbookcontact] ([id])
GO
ALTER TABLE [dbo].[cms_models_addressbookcontactdetails] CHECK CONSTRAINT [cms_models_addressbookcontactdetails_contact_id_f1faffff_fk_cms_models_addressbookcontact_id]
GO
ALTER TABLE [dbo].[cms_models_addressbookcontactdetails]  WITH CHECK ADD  CONSTRAINT [cms_models_addressbookcontactdetails_language_id_83f6f951_fk_custom_models_language_ISO_Code_1] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([ISO_Code_1])
GO
ALTER TABLE [dbo].[cms_models_addressbookcontactdetails] CHECK CONSTRAINT [cms_models_addressbookcontactdetails_language_id_83f6f951_fk_custom_models_language_ISO_Code_1]
GO
ALTER TABLE [dbo].[cms_models_answer]  WITH CHECK ADD  CONSTRAINT [cms_models_answer_question_id_806baab0_fk_cms_models_newquestion_id] FOREIGN KEY([question_id])
REFERENCES [dbo].[cms_models_newquestion] ([id])
GO
ALTER TABLE [dbo].[cms_models_answer] CHECK CONSTRAINT [cms_models_answer_question_id_806baab0_fk_cms_models_newquestion_id]
GO
ALTER TABLE [dbo].[cms_models_answerdetails]  WITH CHECK ADD  CONSTRAINT [cms_models_answerdetails_answer_id_a8677b2a_fk_cms_models_answer_id] FOREIGN KEY([answer_id])
REFERENCES [dbo].[cms_models_answer] ([id])
GO
ALTER TABLE [dbo].[cms_models_answerdetails] CHECK CONSTRAINT [cms_models_answerdetails_answer_id_a8677b2a_fk_cms_models_answer_id]
GO
ALTER TABLE [dbo].[cms_models_answerdetails]  WITH CHECK ADD  CONSTRAINT [cms_models_answerdetails_language_id_6036a3f6_fk_custom_models_language_ISO_Code_1] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([ISO_Code_1])
GO
ALTER TABLE [dbo].[cms_models_answerdetails] CHECK CONSTRAINT [cms_models_answerdetails_language_id_6036a3f6_fk_custom_models_language_ISO_Code_1]
GO
ALTER TABLE [dbo].[cms_models_bundlebundlesassociation]  WITH CHECK ADD  CONSTRAINT [cms_models_bundlebundlesassociation_bundle_link_id_6c6b165e_fk_cms_models_bundles_id] FOREIGN KEY([bundle_link_id])
REFERENCES [dbo].[cms_models_bundles] ([id])
GO
ALTER TABLE [dbo].[cms_models_bundlebundlesassociation] CHECK CONSTRAINT [cms_models_bundlebundlesassociation_bundle_link_id_6c6b165e_fk_cms_models_bundles_id]
GO
ALTER TABLE [dbo].[cms_models_bundlebundlesassociation]  WITH CHECK ADD  CONSTRAINT [cms_models_bundlebundlesassociation_bundle_source_id_a18a1360_fk_cms_models_bundles_id] FOREIGN KEY([bundle_source_id])
REFERENCES [dbo].[cms_models_bundles] ([id])
GO
ALTER TABLE [dbo].[cms_models_bundlebundlesassociation] CHECK CONSTRAINT [cms_models_bundlebundlesassociation_bundle_source_id_a18a1360_fk_cms_models_bundles_id]
GO
ALTER TABLE [dbo].[cms_models_bundlebundlesrule]  WITH CHECK ADD  CONSTRAINT [cms_models_bundlebundlesrule_bundle_rule_id_064bd8a7_fk_cms_models_bundlerule_id] FOREIGN KEY([bundle_rule_id])
REFERENCES [dbo].[cms_models_bundlerule] ([id])
GO
ALTER TABLE [dbo].[cms_models_bundlebundlesrule] CHECK CONSTRAINT [cms_models_bundlebundlesrule_bundle_rule_id_064bd8a7_fk_cms_models_bundlerule_id]
GO
ALTER TABLE [dbo].[cms_models_bundlebundlesruleassociations]  WITH CHECK ADD  CONSTRAINT [cms_models_bundlebundlesruleassociations_bundle_bundles_rule_id_916d9ccf_fk_cms_models_bundlebundlesrule_id] FOREIGN KEY([bundle_bundles_rule_id])
REFERENCES [dbo].[cms_models_bundlebundlesrule] ([id])
GO
ALTER TABLE [dbo].[cms_models_bundlebundlesruleassociations] CHECK CONSTRAINT [cms_models_bundlebundlesruleassociations_bundle_bundles_rule_id_916d9ccf_fk_cms_models_bundlebundlesrule_id]
GO
ALTER TABLE [dbo].[cms_models_bundlebundlesruleassociations]  WITH CHECK ADD  CONSTRAINT [cms_models_bundlebundlesruleassociations_bundle_id_9580884a_fk_cms_models_bundles_id] FOREIGN KEY([bundle_id])
REFERENCES [dbo].[cms_models_bundles] ([id])
GO
ALTER TABLE [dbo].[cms_models_bundlebundlesruleassociations] CHECK CONSTRAINT [cms_models_bundlebundlesruleassociations_bundle_id_9580884a_fk_cms_models_bundles_id]
GO
ALTER TABLE [dbo].[cms_models_bundleitemsassociation]  WITH CHECK ADD  CONSTRAINT [cms_models_bundleitemsassociation_bundle_source_id_9aa01f76_fk_cms_models_bundles_id] FOREIGN KEY([bundle_source_id])
REFERENCES [dbo].[cms_models_bundles] ([id])
GO
ALTER TABLE [dbo].[cms_models_bundleitemsassociation] CHECK CONSTRAINT [cms_models_bundleitemsassociation_bundle_source_id_9aa01f76_fk_cms_models_bundles_id]
GO
ALTER TABLE [dbo].[cms_models_bundleitemsbasicrule]  WITH CHECK ADD  CONSTRAINT [cms_models_bundleitemsbasicrule_bundle_rule_id_4d81112d_fk_cms_models_bundlerule_id] FOREIGN KEY([bundle_rule_id])
REFERENCES [dbo].[cms_models_bundlerule] ([id])
GO
ALTER TABLE [dbo].[cms_models_bundleitemsbasicrule] CHECK CONSTRAINT [cms_models_bundleitemsbasicrule_bundle_rule_id_4d81112d_fk_cms_models_bundlerule_id]
GO
ALTER TABLE [dbo].[cms_models_bundlerule]  WITH CHECK ADD  CONSTRAINT [cms_models_bundlerule_bundle_id_83cd1eb1_fk_cms_models_bundles_id] FOREIGN KEY([bundle_id])
REFERENCES [dbo].[cms_models_bundles] ([id])
GO
ALTER TABLE [dbo].[cms_models_bundlerule] CHECK CONSTRAINT [cms_models_bundlerule_bundle_id_83cd1eb1_fk_cms_models_bundles_id]
GO
ALTER TABLE [dbo].[cms_models_bundlerule]  WITH CHECK ADD  CONSTRAINT [cms_models_bundlerule_rule_type_id_c3dcf271_fk_cms_models_bundleruletype_id] FOREIGN KEY([rule_type_id])
REFERENCES [dbo].[cms_models_bundleruletype] ([id])
GO
ALTER TABLE [dbo].[cms_models_bundlerule] CHECK CONSTRAINT [cms_models_bundlerule_rule_type_id_c3dcf271_fk_cms_models_bundleruletype_id]
GO
ALTER TABLE [dbo].[cms_models_bundles]  WITH CHECK ADD  CONSTRAINT [cms_models_bundles_item_bank_id_0b261390_fk_cms_models_itembank_id] FOREIGN KEY([item_bank_id])
REFERENCES [dbo].[cms_models_itembank] ([id])
GO
ALTER TABLE [dbo].[cms_models_bundles] CHECK CONSTRAINT [cms_models_bundles_item_bank_id_0b261390_fk_cms_models_itembank_id]
GO
ALTER TABLE [dbo].[cms_models_competencytype]  WITH CHECK ADD  CONSTRAINT [cms_models_competencytype_test_definition_id_c6db2ef3_fk_cms_models_testdefinition_id] FOREIGN KEY([test_definition_id])
REFERENCES [dbo].[cms_models_testdefinition] ([id])
GO
ALTER TABLE [dbo].[cms_models_competencytype] CHECK CONSTRAINT [cms_models_competencytype_test_definition_id_c6db2ef3_fk_cms_models_testdefinition_id]
GO
ALTER TABLE [dbo].[cms_models_emailquestion]  WITH CHECK ADD  CONSTRAINT [cms_models_emailquestion_from_field_id_672cf5e1_fk_cms_models_addressbookcontact_id] FOREIGN KEY([from_field_id])
REFERENCES [dbo].[cms_models_addressbookcontact] ([id])
GO
ALTER TABLE [dbo].[cms_models_emailquestion] CHECK CONSTRAINT [cms_models_emailquestion_from_field_id_672cf5e1_fk_cms_models_addressbookcontact_id]
GO
ALTER TABLE [dbo].[cms_models_emailquestion]  WITH CHECK ADD  CONSTRAINT [cms_models_emailquestion_question_id_8722face_fk_cms_models_newquestion_id] FOREIGN KEY([question_id])
REFERENCES [dbo].[cms_models_newquestion] ([id])
GO
ALTER TABLE [dbo].[cms_models_emailquestion] CHECK CONSTRAINT [cms_models_emailquestion_question_id_8722face_fk_cms_models_newquestion_id]
GO
ALTER TABLE [dbo].[cms_models_emailquestion_cc_field]  WITH CHECK ADD  CONSTRAINT [cms_models_emailquestion_cc_field_addressbookcontact_id_a5b39c8a_fk_cms_models_addressbookcontact_id] FOREIGN KEY([addressbookcontact_id])
REFERENCES [dbo].[cms_models_addressbookcontact] ([id])
GO
ALTER TABLE [dbo].[cms_models_emailquestion_cc_field] CHECK CONSTRAINT [cms_models_emailquestion_cc_field_addressbookcontact_id_a5b39c8a_fk_cms_models_addressbookcontact_id]
GO
ALTER TABLE [dbo].[cms_models_emailquestion_cc_field]  WITH CHECK ADD  CONSTRAINT [cms_models_emailquestion_cc_field_emailquestion_id_247929dd_fk_cms_models_emailquestion_id] FOREIGN KEY([emailquestion_id])
REFERENCES [dbo].[cms_models_emailquestion] ([id])
GO
ALTER TABLE [dbo].[cms_models_emailquestion_cc_field] CHECK CONSTRAINT [cms_models_emailquestion_cc_field_emailquestion_id_247929dd_fk_cms_models_emailquestion_id]
GO
ALTER TABLE [dbo].[cms_models_emailquestion_competency_types]  WITH CHECK ADD  CONSTRAINT [cms_models_emailquestion_competency_types_competencytype_id_740ad965_fk_cms_models_competencytype_id] FOREIGN KEY([competencytype_id])
REFERENCES [dbo].[cms_models_competencytype] ([id])
GO
ALTER TABLE [dbo].[cms_models_emailquestion_competency_types] CHECK CONSTRAINT [cms_models_emailquestion_competency_types_competencytype_id_740ad965_fk_cms_models_competencytype_id]
GO
ALTER TABLE [dbo].[cms_models_emailquestion_competency_types]  WITH CHECK ADD  CONSTRAINT [cms_models_emailquestion_competency_types_emailquestion_id_9dd0bbbe_fk_cms_models_emailquestion_id] FOREIGN KEY([emailquestion_id])
REFERENCES [dbo].[cms_models_emailquestion] ([id])
GO
ALTER TABLE [dbo].[cms_models_emailquestion_competency_types] CHECK CONSTRAINT [cms_models_emailquestion_competency_types_emailquestion_id_9dd0bbbe_fk_cms_models_emailquestion_id]
GO
ALTER TABLE [dbo].[cms_models_emailquestion_to_field]  WITH CHECK ADD  CONSTRAINT [cms_models_emailquestion_to_field_addressbookcontact_id_4dbb8281_fk_cms_models_addressbookcontact_id] FOREIGN KEY([addressbookcontact_id])
REFERENCES [dbo].[cms_models_addressbookcontact] ([id])
GO
ALTER TABLE [dbo].[cms_models_emailquestion_to_field] CHECK CONSTRAINT [cms_models_emailquestion_to_field_addressbookcontact_id_4dbb8281_fk_cms_models_addressbookcontact_id]
GO
ALTER TABLE [dbo].[cms_models_emailquestion_to_field]  WITH CHECK ADD  CONSTRAINT [cms_models_emailquestion_to_field_emailquestion_id_e2c8020e_fk_cms_models_emailquestion_id] FOREIGN KEY([emailquestion_id])
REFERENCES [dbo].[cms_models_emailquestion] ([id])
GO
ALTER TABLE [dbo].[cms_models_emailquestion_to_field] CHECK CONSTRAINT [cms_models_emailquestion_to_field_emailquestion_id_e2c8020e_fk_cms_models_emailquestion_id]
GO
ALTER TABLE [dbo].[cms_models_emailquestiondetails]  WITH CHECK ADD  CONSTRAINT [cms_models_emailquestiondetails_email_question_id_c2edf1c2_fk_cms_models_emailquestion_id] FOREIGN KEY([email_question_id])
REFERENCES [dbo].[cms_models_emailquestion] ([id])
GO
ALTER TABLE [dbo].[cms_models_emailquestiondetails] CHECK CONSTRAINT [cms_models_emailquestiondetails_email_question_id_c2edf1c2_fk_cms_models_emailquestion_id]
GO
ALTER TABLE [dbo].[cms_models_emailquestiondetails]  WITH CHECK ADD  CONSTRAINT [cms_models_emailquestiondetails_language_id_0224dc80_fk_custom_models_language_ISO_Code_1] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([ISO_Code_1])
GO
ALTER TABLE [dbo].[cms_models_emailquestiondetails] CHECK CONSTRAINT [cms_models_emailquestiondetails_language_id_0224dc80_fk_custom_models_language_ISO_Code_1]
GO
ALTER TABLE [dbo].[cms_models_fileresourcedetails]  WITH CHECK ADD  CONSTRAINT [cms_models_fileresourcedetails_test_definition_id_b42e598c_fk_cms_models_testdefinition_id] FOREIGN KEY([test_definition_id])
REFERENCES [dbo].[cms_models_testdefinition] ([id])
GO
ALTER TABLE [dbo].[cms_models_fileresourcedetails] CHECK CONSTRAINT [cms_models_fileresourcedetails_test_definition_id_b42e598c_fk_cms_models_testdefinition_id]
GO
ALTER TABLE [dbo].[cms_models_historicaladdressbookcontact]  WITH CHECK ADD  CONSTRAINT [cms_models_historicaladdressbookcontact_history_user_id_169d2cd5_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicaladdressbookcontact] CHECK CONSTRAINT [cms_models_historicaladdressbookcontact_history_user_id_169d2cd5_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicaladdressbookcontactdetails]  WITH CHECK ADD  CONSTRAINT [cms_models_historicaladdressbookcontactdetails_history_user_id_d61a839a_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicaladdressbookcontactdetails] CHECK CONSTRAINT [cms_models_historicaladdressbookcontactdetails_history_user_id_d61a839a_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalanswer]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalanswer_history_user_id_a46c511b_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalanswer] CHECK CONSTRAINT [cms_models_historicalanswer_history_user_id_a46c511b_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalanswerdetails]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalanswerdetails_history_user_id_0bdcd587_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalanswerdetails] CHECK CONSTRAINT [cms_models_historicalanswerdetails_history_user_id_0bdcd587_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalbundlebundlesassociation]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalbundlebundlesassociation_history_user_id_687fa5f8_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalbundlebundlesassociation] CHECK CONSTRAINT [cms_models_historicalbundlebundlesassociation_history_user_id_687fa5f8_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalbundlebundlesrule]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalbundlebundlesrule_history_user_id_6a6b65c1_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalbundlebundlesrule] CHECK CONSTRAINT [cms_models_historicalbundlebundlesrule_history_user_id_6a6b65c1_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalbundlebundlesruleassociations]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalbundlebundlesruleassociations_history_user_id_591e87f8_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalbundlebundlesruleassociations] CHECK CONSTRAINT [cms_models_historicalbundlebundlesruleassociations_history_user_id_591e87f8_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalbundleitemsassociation]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalbundleitemsassociation_history_user_id_ae061ebd_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalbundleitemsassociation] CHECK CONSTRAINT [cms_models_historicalbundleitemsassociation_history_user_id_ae061ebd_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalbundleitemsbasicrule]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalbundleitemsbasicrule_history_user_id_3fa59b22_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalbundleitemsbasicrule] CHECK CONSTRAINT [cms_models_historicalbundleitemsbasicrule_history_user_id_3fa59b22_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalbundlerule]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalbundlerule_history_user_id_52ef23e5_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalbundlerule] CHECK CONSTRAINT [cms_models_historicalbundlerule_history_user_id_52ef23e5_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalbundleruletype]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalbundleruletype_history_user_id_3d33bed6_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalbundleruletype] CHECK CONSTRAINT [cms_models_historicalbundleruletype_history_user_id_3d33bed6_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalbundles]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalbundles_history_user_id_c6ce3cf0_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalbundles] CHECK CONSTRAINT [cms_models_historicalbundles_history_user_id_c6ce3cf0_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalcompetencytype]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalcompetencytype_history_user_id_03723bb8_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalcompetencytype] CHECK CONSTRAINT [cms_models_historicalcompetencytype_history_user_id_03723bb8_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalemailquestion]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalemailquestion_history_user_id_8fe38777_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalemailquestion] CHECK CONSTRAINT [cms_models_historicalemailquestion_history_user_id_8fe38777_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalemailquestiondetails]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalemailquestiondetails_history_user_id_be7314d8_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalemailquestiondetails] CHECK CONSTRAINT [cms_models_historicalemailquestiondetails_history_user_id_be7314d8_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalfileresourcedetails]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalfileresourcedetails_history_user_id_4fcbed7a_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalfileresourcedetails] CHECK CONSTRAINT [cms_models_historicalfileresourcedetails_history_user_id_4fcbed7a_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitemattributevalue]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitemattributevalue_history_user_id_d97a2eb7_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitemattributevalue] CHECK CONSTRAINT [cms_models_historicalitemattributevalue_history_user_id_d97a2eb7_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitemattributevaluedrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitemattributevaluedrafts_history_user_id_e3aa635b_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitemattributevaluedrafts] CHECK CONSTRAINT [cms_models_historicalitemattributevaluedrafts_history_user_id_e3aa635b_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitembank]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitembank_history_user_id_04c046c4_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitembank] CHECK CONSTRAINT [cms_models_historicalitembank_history_user_id_04c046c4_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitembankaccesstypes]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitembankaccesstypes_history_user_id_d238ee07_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitembankaccesstypes] CHECK CONSTRAINT [cms_models_historicalitembankaccesstypes_history_user_id_d238ee07_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitembankattributes]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitembankattributes_history_user_id_d5ec1401_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitembankattributes] CHECK CONSTRAINT [cms_models_historicalitembankattributes_history_user_id_d5ec1401_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitembankattributestext]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitembankattributestext_history_user_id_a07cf2c6_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitembankattributestext] CHECK CONSTRAINT [cms_models_historicalitembankattributestext_history_user_id_a07cf2c6_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitembankattributevalues]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitembankattributevalues_history_user_id_672987d7_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitembankattributevalues] CHECK CONSTRAINT [cms_models_historicalitembankattributevalues_history_user_id_672987d7_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitembankattributevaluestext]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitembankattributevaluestext_history_user_id_5c546f91_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitembankattributevaluestext] CHECK CONSTRAINT [cms_models_historicalitembankattributevaluestext_history_user_id_5c546f91_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitembankpermissions]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitembankpermissions_history_user_id_d485f3e7_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitembankpermissions] CHECK CONSTRAINT [cms_models_historicalitembankpermissions_history_user_id_d485f3e7_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitembankrule]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitembankrule_history_user_id_e08f44cb_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitembankrule] CHECK CONSTRAINT [cms_models_historicalitembankrule_history_user_id_e08f44cb_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitemcomments]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitemcomments_history_user_id_ac0c70f0_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitemcomments] CHECK CONSTRAINT [cms_models_historicalitemcomments_history_user_id_ac0c70f0_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitemcontent]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitemcontent_history_user_id_c55c24c2_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitemcontent] CHECK CONSTRAINT [cms_models_historicalitemcontent_history_user_id_c55c24c2_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitemcontentdrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitemcontentdrafts_history_user_id_1ce23c77_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitemcontentdrafts] CHECK CONSTRAINT [cms_models_historicalitemcontentdrafts_history_user_id_1ce23c77_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitemcontenttext]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitemcontenttext_history_user_id_4ec1df1e_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitemcontenttext] CHECK CONSTRAINT [cms_models_historicalitemcontenttext_history_user_id_4ec1df1e_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitemcontenttextdrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitemcontenttextdrafts_history_user_id_ff112d0f_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitemcontenttextdrafts] CHECK CONSTRAINT [cms_models_historicalitemcontenttextdrafts_history_user_id_ff112d0f_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitemdevelopmentstatuses]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitemdevelopmentstatuses_history_user_id_edf80216_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitemdevelopmentstatuses] CHECK CONSTRAINT [cms_models_historicalitemdevelopmentstatuses_history_user_id_edf80216_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitemdrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitemdrafts_history_user_id_431d565d_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitemdrafts] CHECK CONSTRAINT [cms_models_historicalitemdrafts_history_user_id_431d565d_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitemhistoricalids]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitemhistoricalids_history_user_id_6036519a_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitemhistoricalids] CHECK CONSTRAINT [cms_models_historicalitemhistoricalids_history_user_id_6036519a_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitemoption]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitemoption_history_user_id_38210b29_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitemoption] CHECK CONSTRAINT [cms_models_historicalitemoption_history_user_id_38210b29_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitemoptiondrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitemoptiondrafts_history_user_id_71b44451_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitemoptiondrafts] CHECK CONSTRAINT [cms_models_historicalitemoptiondrafts_history_user_id_71b44451_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitemoptiontext]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitemoptiontext_history_user_id_f95f76bd_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitemoptiontext] CHECK CONSTRAINT [cms_models_historicalitemoptiontext_history_user_id_f95f76bd_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitemoptiontextdrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitemoptiontextdrafts_history_user_id_7e77ea68_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitemoptiontextdrafts] CHECK CONSTRAINT [cms_models_historicalitemoptiontextdrafts_history_user_id_7e77ea68_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitemresponseformats]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitemresponseformats_history_user_id_fdc1c6eb_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitemresponseformats] CHECK CONSTRAINT [cms_models_historicalitemresponseformats_history_user_id_fdc1c6eb_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitems]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitems_history_user_id_26c39f49_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitems] CHECK CONSTRAINT [cms_models_historicalitems_history_user_id_26c39f49_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalmultiplechoicequestion]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalmultiplechoicequestion_history_user_id_2127596e_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalmultiplechoicequestion] CHECK CONSTRAINT [cms_models_historicalmultiplechoicequestion_history_user_id_2127596e_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalnewquestion]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalnewquestion_history_user_id_646fa9db_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalnewquestion] CHECK CONSTRAINT [cms_models_historicalnewquestion_history_user_id_646fa9db_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalnextsectionbuttontypepopup]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalnextsectionbuttontypepopup_history_user_id_d79535e3_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalnextsectionbuttontypepopup] CHECK CONSTRAINT [cms_models_historicalnextsectionbuttontypepopup_history_user_id_d79535e3_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalnextsectionbuttontypeproceed]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalnextsectionbuttontypeproceed_history_user_id_702f68da_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalnextsectionbuttontypeproceed] CHECK CONSTRAINT [cms_models_historicalnextsectionbuttontypeproceed_history_user_id_702f68da_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalorderlesstestpermissions]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalorderlesstestpermissions_history_user_id_745dc4e7_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalorderlesstestpermissions] CHECK CONSTRAINT [cms_models_historicalorderlesstestpermissions_history_user_id_745dc4e7_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalpagesection]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalpagesection_history_user_id_637d9cfc_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalpagesection] CHECK CONSTRAINT [cms_models_historicalpagesection_history_user_id_637d9cfc_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalpagesectiontypeimagezoom]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalpagesectiontypeimagezoom_history_user_id_3bad7e1b_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalpagesectiontypeimagezoom] CHECK CONSTRAINT [cms_models_historicalpagesectiontypeimagezoom_history_user_id_3bad7e1b_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalpagesectiontypemarkdown]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalpagesectiontypemarkdown_history_user_id_ba0c29c9_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalpagesectiontypemarkdown] CHECK CONSTRAINT [cms_models_historicalpagesectiontypemarkdown_history_user_id_ba0c29c9_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalpagesectiontypesampleemail]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalpagesectiontypesampleemail_history_user_id_e5f75b8e_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalpagesectiontypesampleemail] CHECK CONSTRAINT [cms_models_historicalpagesectiontypesampleemail_history_user_id_e5f75b8e_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalpagesectiontypesampleemailresponse]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalpagesectiontypesampleemailresponse_history_user_id_f14ecc3f_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalpagesectiontypesampleemailresponse] CHECK CONSTRAINT [cms_models_historicalpagesectiontypesampleemailresponse_history_user_id_f14ecc3f_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalpagesectiontypesampletaskresponse]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalpagesectiontypesampletaskresponse_history_user_id_be1e02c8_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalpagesectiontypesampletaskresponse] CHECK CONSTRAINT [cms_models_historicalpagesectiontypesampletaskresponse_history_user_id_be1e02c8_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalpagesectiontypetreedescription]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalpagesectiontypetreedescription_history_user_id_727dfa3e_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalpagesectiontypetreedescription] CHECK CONSTRAINT [cms_models_historicalpagesectiontypetreedescription_history_user_id_727dfa3e_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalquestionblocktype]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalquestionblocktype_history_user_id_a3a4cf61_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalquestionblocktype] CHECK CONSTRAINT [cms_models_historicalquestionblocktype_history_user_id_a3a4cf61_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalquestionlistrule]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalquestionlistrule_history_user_id_2e9fcc9e_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalquestionlistrule] CHECK CONSTRAINT [cms_models_historicalquestionlistrule_history_user_id_2e9fcc9e_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalquestionsection]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalquestionsection_history_user_id_1c550235_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalquestionsection] CHECK CONSTRAINT [cms_models_historicalquestionsection_history_user_id_1c550235_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalquestionsectiontypemarkdown]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalquestionsectiontypemarkdown_history_user_id_f1873bfb_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalquestionsectiontypemarkdown] CHECK CONSTRAINT [cms_models_historicalquestionsectiontypemarkdown_history_user_id_f1873bfb_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalquestionsituation]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalquestionsituation_history_user_id_bdaf9cc7_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalquestionsituation] CHECK CONSTRAINT [cms_models_historicalquestionsituation_history_user_id_bdaf9cc7_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalreducercontrol]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalreducercontrol_history_user_id_92c6c3b4_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalreducercontrol] CHECK CONSTRAINT [cms_models_historicalreducercontrol_history_user_id_92c6c3b4_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalscoringmethods]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalscoringmethods_history_user_id_9dd32302_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalscoringmethods] CHECK CONSTRAINT [cms_models_historicalscoringmethods_history_user_id_9dd32302_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalscoringmethodtypes]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalscoringmethodtypes_history_user_id_2b11daf1_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalscoringmethodtypes] CHECK CONSTRAINT [cms_models_historicalscoringmethodtypes_history_user_id_2b11daf1_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalscoringpassfail]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalscoringpassfail_history_user_id_8b7e957d_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalscoringpassfail] CHECK CONSTRAINT [cms_models_historicalscoringpassfail_history_user_id_8b7e957d_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalscoringthreshold]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalscoringthreshold_history_user_id_c7fb14a6_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalscoringthreshold] CHECK CONSTRAINT [cms_models_historicalscoringthreshold_history_user_id_c7fb14a6_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalsectioncomponentpage]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalsectioncomponentpage_history_user_id_68763e21_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalsectioncomponentpage] CHECK CONSTRAINT [cms_models_historicalsectioncomponentpage_history_user_id_68763e21_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalsituationexamplerating]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalsituationexamplerating_history_user_id_6e3cf95d_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalsituationexamplerating] CHECK CONSTRAINT [cms_models_historicalsituationexamplerating_history_user_id_6e3cf95d_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalsituationexampleratingdetails]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalsituationexampleratingdetails_history_user_id_9c4de5ce_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalsituationexampleratingdetails] CHECK CONSTRAINT [cms_models_historicalsituationexampleratingdetails_history_user_id_9c4de5ce_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicaltestdefinition]  WITH CHECK ADD  CONSTRAINT [cms_models_historicaltestdefinition_history_user_id_0707a2c7_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicaltestdefinition] CHECK CONSTRAINT [cms_models_historicaltestdefinition_history_user_id_0707a2c7_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicaltestpermissions]  WITH CHECK ADD  CONSTRAINT [cms_models_historicaltestpermissions_history_user_id_dd4a0896_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicaltestpermissions] CHECK CONSTRAINT [cms_models_historicaltestpermissions_history_user_id_dd4a0896_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicaltestsection]  WITH CHECK ADD  CONSTRAINT [cms_models_historicaltestsection_history_user_id_820e6892_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicaltestsection] CHECK CONSTRAINT [cms_models_historicaltestsection_history_user_id_820e6892_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicaltestsectioncomponent]  WITH CHECK ADD  CONSTRAINT [cms_models_historicaltestsectioncomponent_history_user_id_973cd7d1_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicaltestsectioncomponent] CHECK CONSTRAINT [cms_models_historicaltestsectioncomponent_history_user_id_973cd7d1_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicaltestsectionreducer]  WITH CHECK ADD  CONSTRAINT [cms_models_historicaltestsectionreducer_history_user_id_badc22a0_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicaltestsectionreducer] CHECK CONSTRAINT [cms_models_historicaltestsectionreducer_history_user_id_badc22a0_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalviewedquestions]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalviewedquestions_history_user_id_0390a32d_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalviewedquestions] CHECK CONSTRAINT [cms_models_historicalviewedquestions_history_user_id_0390a32d_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_itemattributevalue]  WITH CHECK ADD  CONSTRAINT [cms_models_itemattributevalue_item_bank_attribute_value_id_d8372e5c_fk_cms_models_itembankattributevalues_id] FOREIGN KEY([item_bank_attribute_value_id])
REFERENCES [dbo].[cms_models_itembankattributevalues] ([id])
GO
ALTER TABLE [dbo].[cms_models_itemattributevalue] CHECK CONSTRAINT [cms_models_itemattributevalue_item_bank_attribute_value_id_d8372e5c_fk_cms_models_itembankattributevalues_id]
GO
ALTER TABLE [dbo].[cms_models_itemattributevalue]  WITH CHECK ADD  CONSTRAINT [cms_models_itemattributevalue_item_id_8d85287b_fk_cms_models_items_id] FOREIGN KEY([item_id])
REFERENCES [dbo].[cms_models_items] ([id])
GO
ALTER TABLE [dbo].[cms_models_itemattributevalue] CHECK CONSTRAINT [cms_models_itemattributevalue_item_id_8d85287b_fk_cms_models_items_id]
GO
ALTER TABLE [dbo].[cms_models_itemattributevaluedrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_itemattributevaluedrafts_item_bank_attribute_value_id_0b89d116_fk_cms_models_itembankattributevalues_id] FOREIGN KEY([item_bank_attribute_value_id])
REFERENCES [dbo].[cms_models_itembankattributevalues] ([id])
GO
ALTER TABLE [dbo].[cms_models_itemattributevaluedrafts] CHECK CONSTRAINT [cms_models_itemattributevaluedrafts_item_bank_attribute_value_id_0b89d116_fk_cms_models_itembankattributevalues_id]
GO
ALTER TABLE [dbo].[cms_models_itemattributevaluedrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_itemattributevaluedrafts_item_id_73f7efa8_fk_cms_models_items_id] FOREIGN KEY([item_id])
REFERENCES [dbo].[cms_models_items] ([id])
GO
ALTER TABLE [dbo].[cms_models_itemattributevaluedrafts] CHECK CONSTRAINT [cms_models_itemattributevaluedrafts_item_id_73f7efa8_fk_cms_models_items_id]
GO
ALTER TABLE [dbo].[cms_models_itemattributevaluedrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_itemattributevaluedrafts_username_id_2f324766_fk_user_management_models_user_username] FOREIGN KEY([username_id])
REFERENCES [dbo].[user_management_models_user] ([username])
GO
ALTER TABLE [dbo].[cms_models_itemattributevaluedrafts] CHECK CONSTRAINT [cms_models_itemattributevaluedrafts_username_id_2f324766_fk_user_management_models_user_username]
GO
ALTER TABLE [dbo].[cms_models_itembank]  WITH CHECK ADD  CONSTRAINT [cms_models_itembank_language_id_d0cd7692_fk_custom_models_language_language_id] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([language_id])
GO
ALTER TABLE [dbo].[cms_models_itembank] CHECK CONSTRAINT [cms_models_itembank_language_id_d0cd7692_fk_custom_models_language_language_id]
GO
ALTER TABLE [dbo].[cms_models_itembankattributes]  WITH CHECK ADD  CONSTRAINT [cms_models_itembankattributes_item_bank_id_3ce78b18_fk_cms_models_itembank_id] FOREIGN KEY([item_bank_id])
REFERENCES [dbo].[cms_models_itembank] ([id])
GO
ALTER TABLE [dbo].[cms_models_itembankattributes] CHECK CONSTRAINT [cms_models_itembankattributes_item_bank_id_3ce78b18_fk_cms_models_itembank_id]
GO
ALTER TABLE [dbo].[cms_models_itembankattributestext]  WITH CHECK ADD  CONSTRAINT [cms_models_itembankattributestext_item_bank_attribute_id_cf1c91d8_fk_cms_models_itembankattributes_id] FOREIGN KEY([item_bank_attribute_id])
REFERENCES [dbo].[cms_models_itembankattributes] ([id])
GO
ALTER TABLE [dbo].[cms_models_itembankattributestext] CHECK CONSTRAINT [cms_models_itembankattributestext_item_bank_attribute_id_cf1c91d8_fk_cms_models_itembankattributes_id]
GO
ALTER TABLE [dbo].[cms_models_itembankattributestext]  WITH CHECK ADD  CONSTRAINT [cms_models_itembankattributestext_language_id_253b28db_fk_custom_models_language_language_id] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([language_id])
GO
ALTER TABLE [dbo].[cms_models_itembankattributestext] CHECK CONSTRAINT [cms_models_itembankattributestext_language_id_253b28db_fk_custom_models_language_language_id]
GO
ALTER TABLE [dbo].[cms_models_itembankattributevalues]  WITH CHECK ADD  CONSTRAINT [cms_models_itembankattributevalues_item_bank_attribute_id_f483be53_fk_cms_models_itembankattributes_id] FOREIGN KEY([item_bank_attribute_id])
REFERENCES [dbo].[cms_models_itembankattributes] ([id])
GO
ALTER TABLE [dbo].[cms_models_itembankattributevalues] CHECK CONSTRAINT [cms_models_itembankattributevalues_item_bank_attribute_id_f483be53_fk_cms_models_itembankattributes_id]
GO
ALTER TABLE [dbo].[cms_models_itembankattributevaluestext]  WITH CHECK ADD  CONSTRAINT [cms_models_itembankattributevaluestext_item_bank_attribute_values_id_a7e74aa0_fk_cms_models_itembankattributevalues_id] FOREIGN KEY([item_bank_attribute_values_id])
REFERENCES [dbo].[cms_models_itembankattributevalues] ([id])
GO
ALTER TABLE [dbo].[cms_models_itembankattributevaluestext] CHECK CONSTRAINT [cms_models_itembankattributevaluestext_item_bank_attribute_values_id_a7e74aa0_fk_cms_models_itembankattributevalues_id]
GO
ALTER TABLE [dbo].[cms_models_itembankpermissions]  WITH CHECK ADD  CONSTRAINT [cms_models_itembankpermissions_item_bank_access_type_id_16005ee4_fk_cms_models_itembankaccesstypes_id] FOREIGN KEY([item_bank_access_type_id])
REFERENCES [dbo].[cms_models_itembankaccesstypes] ([id])
GO
ALTER TABLE [dbo].[cms_models_itembankpermissions] CHECK CONSTRAINT [cms_models_itembankpermissions_item_bank_access_type_id_16005ee4_fk_cms_models_itembankaccesstypes_id]
GO
ALTER TABLE [dbo].[cms_models_itembankpermissions]  WITH CHECK ADD  CONSTRAINT [cms_models_itembankpermissions_item_bank_id_64659e7d_fk_cms_models_itembank_id] FOREIGN KEY([item_bank_id])
REFERENCES [dbo].[cms_models_itembank] ([id])
GO
ALTER TABLE [dbo].[cms_models_itembankpermissions] CHECK CONSTRAINT [cms_models_itembankpermissions_item_bank_id_64659e7d_fk_cms_models_itembank_id]
GO
ALTER TABLE [dbo].[cms_models_itembankpermissions]  WITH CHECK ADD  CONSTRAINT [cms_models_itembankpermissions_username_id_827b9e60_fk_user_management_models_user_username] FOREIGN KEY([username_id])
REFERENCES [dbo].[user_management_models_user] ([username])
GO
ALTER TABLE [dbo].[cms_models_itembankpermissions] CHECK CONSTRAINT [cms_models_itembankpermissions_username_id_827b9e60_fk_user_management_models_user_username]
GO
ALTER TABLE [dbo].[cms_models_itembankrule]  WITH CHECK ADD  CONSTRAINT [cms_models_itembankrule_item_bank_bundle_id_aae4b25f_fk_cms_models_bundles_id] FOREIGN KEY([item_bank_bundle_id])
REFERENCES [dbo].[cms_models_bundles] ([id])
GO
ALTER TABLE [dbo].[cms_models_itembankrule] CHECK CONSTRAINT [cms_models_itembankrule_item_bank_bundle_id_aae4b25f_fk_cms_models_bundles_id]
GO
ALTER TABLE [dbo].[cms_models_itembankrule]  WITH CHECK ADD  CONSTRAINT [cms_models_itembankrule_item_bank_id_29e59d03_fk_cms_models_itembank_id] FOREIGN KEY([item_bank_id])
REFERENCES [dbo].[cms_models_itembank] ([id])
GO
ALTER TABLE [dbo].[cms_models_itembankrule] CHECK CONSTRAINT [cms_models_itembankrule_item_bank_id_29e59d03_fk_cms_models_itembank_id]
GO
ALTER TABLE [dbo].[cms_models_itembankrule]  WITH CHECK ADD  CONSTRAINT [cms_models_itembankrule_test_section_component_id_47b6d059_fk_cms_models_testsectioncomponent_id] FOREIGN KEY([test_section_component_id])
REFERENCES [dbo].[cms_models_testsectioncomponent] ([id])
GO
ALTER TABLE [dbo].[cms_models_itembankrule] CHECK CONSTRAINT [cms_models_itembankrule_test_section_component_id_47b6d059_fk_cms_models_testsectioncomponent_id]
GO
ALTER TABLE [dbo].[cms_models_itemcomments]  WITH CHECK ADD  CONSTRAINT [cms_models_itemcomments_username_id_f6cede5a_fk_user_management_models_user_username] FOREIGN KEY([username_id])
REFERENCES [dbo].[user_management_models_user] ([username])
GO
ALTER TABLE [dbo].[cms_models_itemcomments] CHECK CONSTRAINT [cms_models_itemcomments_username_id_f6cede5a_fk_user_management_models_user_username]
GO
ALTER TABLE [dbo].[cms_models_itemcontent]  WITH CHECK ADD  CONSTRAINT [cms_models_itemcontent_item_id_43e12653_fk_cms_models_items_id] FOREIGN KEY([item_id])
REFERENCES [dbo].[cms_models_items] ([id])
GO
ALTER TABLE [dbo].[cms_models_itemcontent] CHECK CONSTRAINT [cms_models_itemcontent_item_id_43e12653_fk_cms_models_items_id]
GO
ALTER TABLE [dbo].[cms_models_itemcontentdrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_itemcontentdrafts_item_id_3284e8a9_fk_cms_models_items_id] FOREIGN KEY([item_id])
REFERENCES [dbo].[cms_models_items] ([id])
GO
ALTER TABLE [dbo].[cms_models_itemcontentdrafts] CHECK CONSTRAINT [cms_models_itemcontentdrafts_item_id_3284e8a9_fk_cms_models_items_id]
GO
ALTER TABLE [dbo].[cms_models_itemcontentdrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_itemcontentdrafts_username_id_3b3cdc26_fk_user_management_models_user_username] FOREIGN KEY([username_id])
REFERENCES [dbo].[user_management_models_user] ([username])
GO
ALTER TABLE [dbo].[cms_models_itemcontentdrafts] CHECK CONSTRAINT [cms_models_itemcontentdrafts_username_id_3b3cdc26_fk_user_management_models_user_username]
GO
ALTER TABLE [dbo].[cms_models_itemcontenttext]  WITH CHECK ADD  CONSTRAINT [cms_models_itemcontenttext_item_content_id_b2168cac_fk_cms_models_itemcontent_id] FOREIGN KEY([item_content_id])
REFERENCES [dbo].[cms_models_itemcontent] ([id])
GO
ALTER TABLE [dbo].[cms_models_itemcontenttext] CHECK CONSTRAINT [cms_models_itemcontenttext_item_content_id_b2168cac_fk_cms_models_itemcontent_id]
GO
ALTER TABLE [dbo].[cms_models_itemcontenttext]  WITH CHECK ADD  CONSTRAINT [cms_models_itemcontenttext_language_id_16fe5c73_fk_custom_models_language_language_id] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([language_id])
GO
ALTER TABLE [dbo].[cms_models_itemcontenttext] CHECK CONSTRAINT [cms_models_itemcontenttext_language_id_16fe5c73_fk_custom_models_language_language_id]
GO
ALTER TABLE [dbo].[cms_models_itemcontenttextdrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_itemcontenttextdrafts_item_content_id_b1757bf8_fk_cms_models_itemcontentdrafts_id] FOREIGN KEY([item_content_id])
REFERENCES [dbo].[cms_models_itemcontentdrafts] ([id])
GO
ALTER TABLE [dbo].[cms_models_itemcontenttextdrafts] CHECK CONSTRAINT [cms_models_itemcontenttextdrafts_item_content_id_b1757bf8_fk_cms_models_itemcontentdrafts_id]
GO
ALTER TABLE [dbo].[cms_models_itemcontenttextdrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_itemcontenttextdrafts_language_id_a51222e0_fk_custom_models_language_language_id] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([language_id])
GO
ALTER TABLE [dbo].[cms_models_itemcontenttextdrafts] CHECK CONSTRAINT [cms_models_itemcontenttextdrafts_language_id_a51222e0_fk_custom_models_language_language_id]
GO
ALTER TABLE [dbo].[cms_models_itemdrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_itemdrafts_development_status_id_bb6cfb29_fk_cms_models_itemdevelopmentstatuses_id] FOREIGN KEY([development_status_id])
REFERENCES [dbo].[cms_models_itemdevelopmentstatuses] ([id])
GO
ALTER TABLE [dbo].[cms_models_itemdrafts] CHECK CONSTRAINT [cms_models_itemdrafts_development_status_id_bb6cfb29_fk_cms_models_itemdevelopmentstatuses_id]
GO
ALTER TABLE [dbo].[cms_models_itemdrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_itemdrafts_item_bank_id_81328a9e_fk_cms_models_itembank_id] FOREIGN KEY([item_bank_id])
REFERENCES [dbo].[cms_models_itembank] ([id])
GO
ALTER TABLE [dbo].[cms_models_itemdrafts] CHECK CONSTRAINT [cms_models_itemdrafts_item_bank_id_81328a9e_fk_cms_models_itembank_id]
GO
ALTER TABLE [dbo].[cms_models_itemdrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_itemdrafts_item_id_0413531b_fk_cms_models_items_id] FOREIGN KEY([item_id])
REFERENCES [dbo].[cms_models_items] ([id])
GO
ALTER TABLE [dbo].[cms_models_itemdrafts] CHECK CONSTRAINT [cms_models_itemdrafts_item_id_0413531b_fk_cms_models_items_id]
GO
ALTER TABLE [dbo].[cms_models_itemdrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_itemdrafts_response_format_id_bac4b698_fk_cms_models_itemresponseformats_id] FOREIGN KEY([response_format_id])
REFERENCES [dbo].[cms_models_itemresponseformats] ([id])
GO
ALTER TABLE [dbo].[cms_models_itemdrafts] CHECK CONSTRAINT [cms_models_itemdrafts_response_format_id_bac4b698_fk_cms_models_itemresponseformats_id]
GO
ALTER TABLE [dbo].[cms_models_itemdrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_itemdrafts_username_id_6adcab27_fk_user_management_models_user_username] FOREIGN KEY([username_id])
REFERENCES [dbo].[user_management_models_user] ([username])
GO
ALTER TABLE [dbo].[cms_models_itemdrafts] CHECK CONSTRAINT [cms_models_itemdrafts_username_id_6adcab27_fk_user_management_models_user_username]
GO
ALTER TABLE [dbo].[cms_models_itemhistoricalids]  WITH CHECK ADD  CONSTRAINT [cms_models_itemhistoricalids_item_bank_id_3a1acc99_fk_cms_models_itembank_id] FOREIGN KEY([item_bank_id])
REFERENCES [dbo].[cms_models_itembank] ([id])
GO
ALTER TABLE [dbo].[cms_models_itemhistoricalids] CHECK CONSTRAINT [cms_models_itemhistoricalids_item_bank_id_3a1acc99_fk_cms_models_itembank_id]
GO
ALTER TABLE [dbo].[cms_models_itemoption]  WITH CHECK ADD  CONSTRAINT [cms_models_itemoption_item_id_13766378_fk_cms_models_items_id] FOREIGN KEY([item_id])
REFERENCES [dbo].[cms_models_items] ([id])
GO
ALTER TABLE [dbo].[cms_models_itemoption] CHECK CONSTRAINT [cms_models_itemoption_item_id_13766378_fk_cms_models_items_id]
GO
ALTER TABLE [dbo].[cms_models_itemoptiondrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_itemoptiondrafts_item_id_5c99e7fc_fk_cms_models_items_id] FOREIGN KEY([item_id])
REFERENCES [dbo].[cms_models_items] ([id])
GO
ALTER TABLE [dbo].[cms_models_itemoptiondrafts] CHECK CONSTRAINT [cms_models_itemoptiondrafts_item_id_5c99e7fc_fk_cms_models_items_id]
GO
ALTER TABLE [dbo].[cms_models_itemoptiondrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_itemoptiondrafts_username_id_d7bbfa85_fk_user_management_models_user_username] FOREIGN KEY([username_id])
REFERENCES [dbo].[user_management_models_user] ([username])
GO
ALTER TABLE [dbo].[cms_models_itemoptiondrafts] CHECK CONSTRAINT [cms_models_itemoptiondrafts_username_id_d7bbfa85_fk_user_management_models_user_username]
GO
ALTER TABLE [dbo].[cms_models_itemoptiontext]  WITH CHECK ADD  CONSTRAINT [cms_models_itemoptiontext_item_option_id_973f732c_fk_cms_models_itemoption_id] FOREIGN KEY([item_option_id])
REFERENCES [dbo].[cms_models_itemoption] ([id])
GO
ALTER TABLE [dbo].[cms_models_itemoptiontext] CHECK CONSTRAINT [cms_models_itemoptiontext_item_option_id_973f732c_fk_cms_models_itemoption_id]
GO
ALTER TABLE [dbo].[cms_models_itemoptiontext]  WITH CHECK ADD  CONSTRAINT [cms_models_itemoptiontext_language_id_6665c0f2_fk_custom_models_language_language_id] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([language_id])
GO
ALTER TABLE [dbo].[cms_models_itemoptiontext] CHECK CONSTRAINT [cms_models_itemoptiontext_language_id_6665c0f2_fk_custom_models_language_language_id]
GO
ALTER TABLE [dbo].[cms_models_itemoptiontextdrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_itemoptiontextdrafts_item_option_id_82ec1a3f_fk_cms_models_itemoptiondrafts_id] FOREIGN KEY([item_option_id])
REFERENCES [dbo].[cms_models_itemoptiondrafts] ([id])
GO
ALTER TABLE [dbo].[cms_models_itemoptiontextdrafts] CHECK CONSTRAINT [cms_models_itemoptiontextdrafts_item_option_id_82ec1a3f_fk_cms_models_itemoptiondrafts_id]
GO
ALTER TABLE [dbo].[cms_models_itemoptiontextdrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_itemoptiontextdrafts_language_id_853b7c7a_fk_custom_models_language_language_id] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([language_id])
GO
ALTER TABLE [dbo].[cms_models_itemoptiontextdrafts] CHECK CONSTRAINT [cms_models_itemoptiontextdrafts_language_id_853b7c7a_fk_custom_models_language_language_id]
GO
ALTER TABLE [dbo].[cms_models_items]  WITH CHECK ADD  CONSTRAINT [cms_models_items_development_status_id_5fa3efeb_fk_cms_models_itemdevelopmentstatuses_id] FOREIGN KEY([development_status_id])
REFERENCES [dbo].[cms_models_itemdevelopmentstatuses] ([id])
GO
ALTER TABLE [dbo].[cms_models_items] CHECK CONSTRAINT [cms_models_items_development_status_id_5fa3efeb_fk_cms_models_itemdevelopmentstatuses_id]
GO
ALTER TABLE [dbo].[cms_models_items]  WITH CHECK ADD  CONSTRAINT [cms_models_items_item_bank_id_4d7618ce_fk_cms_models_itembank_id] FOREIGN KEY([item_bank_id])
REFERENCES [dbo].[cms_models_itembank] ([id])
GO
ALTER TABLE [dbo].[cms_models_items] CHECK CONSTRAINT [cms_models_items_item_bank_id_4d7618ce_fk_cms_models_itembank_id]
GO
ALTER TABLE [dbo].[cms_models_items]  WITH CHECK ADD  CONSTRAINT [cms_models_items_last_modified_by_username_id_6665a610_fk_user_management_models_user_username] FOREIGN KEY([last_modified_by_username_id])
REFERENCES [dbo].[user_management_models_user] ([username])
GO
ALTER TABLE [dbo].[cms_models_items] CHECK CONSTRAINT [cms_models_items_last_modified_by_username_id_6665a610_fk_user_management_models_user_username]
GO
ALTER TABLE [dbo].[cms_models_items]  WITH CHECK ADD  CONSTRAINT [cms_models_items_response_format_id_05d3fd3a_fk_cms_models_itemresponseformats_id] FOREIGN KEY([response_format_id])
REFERENCES [dbo].[cms_models_itemresponseformats] ([id])
GO
ALTER TABLE [dbo].[cms_models_items] CHECK CONSTRAINT [cms_models_items_response_format_id_05d3fd3a_fk_cms_models_itemresponseformats_id]
GO
ALTER TABLE [dbo].[cms_models_multiplechoicequestion]  WITH CHECK ADD  CONSTRAINT [cms_models_multiplechoicequestion_question_id_0a0ba0f5_fk_cms_models_newquestion_id] FOREIGN KEY([question_id])
REFERENCES [dbo].[cms_models_newquestion] ([id])
GO
ALTER TABLE [dbo].[cms_models_multiplechoicequestion] CHECK CONSTRAINT [cms_models_multiplechoicequestion_question_id_0a0ba0f5_fk_cms_models_newquestion_id]
GO
ALTER TABLE [dbo].[cms_models_newquestion]  WITH CHECK ADD  CONSTRAINT [cms_models_newquestion_question_block_type_id_427156c0_fk_cms_models_questionblocktype_id] FOREIGN KEY([question_block_type_id])
REFERENCES [dbo].[cms_models_questionblocktype] ([id])
GO
ALTER TABLE [dbo].[cms_models_newquestion] CHECK CONSTRAINT [cms_models_newquestion_question_block_type_id_427156c0_fk_cms_models_questionblocktype_id]
GO
ALTER TABLE [dbo].[cms_models_newquestion]  WITH CHECK ADD  CONSTRAINT [cms_models_newquestion_test_section_component_id_91088323_fk_cms_models_testsectioncomponent_id] FOREIGN KEY([test_section_component_id])
REFERENCES [dbo].[cms_models_testsectioncomponent] ([id])
GO
ALTER TABLE [dbo].[cms_models_newquestion] CHECK CONSTRAINT [cms_models_newquestion_test_section_component_id_91088323_fk_cms_models_testsectioncomponent_id]
GO
ALTER TABLE [dbo].[cms_models_newquestion_dependencies]  WITH CHECK ADD  CONSTRAINT [cms_models_newquestion_dependencies_from_newquestion_id_d41f8c55_fk_cms_models_newquestion_id] FOREIGN KEY([from_newquestion_id])
REFERENCES [dbo].[cms_models_newquestion] ([id])
GO
ALTER TABLE [dbo].[cms_models_newquestion_dependencies] CHECK CONSTRAINT [cms_models_newquestion_dependencies_from_newquestion_id_d41f8c55_fk_cms_models_newquestion_id]
GO
ALTER TABLE [dbo].[cms_models_newquestion_dependencies]  WITH CHECK ADD  CONSTRAINT [cms_models_newquestion_dependencies_to_newquestion_id_f4ac3126_fk_cms_models_newquestion_id] FOREIGN KEY([to_newquestion_id])
REFERENCES [dbo].[cms_models_newquestion] ([id])
GO
ALTER TABLE [dbo].[cms_models_newquestion_dependencies] CHECK CONSTRAINT [cms_models_newquestion_dependencies_to_newquestion_id_f4ac3126_fk_cms_models_newquestion_id]
GO
ALTER TABLE [dbo].[cms_models_nextsectionbuttontypepopup]  WITH CHECK ADD  CONSTRAINT [cms_models_nextsectionbuttontypepopup_language_id_d78f96a9_fk_custom_models_language_ISO_Code_1] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([ISO_Code_1])
GO
ALTER TABLE [dbo].[cms_models_nextsectionbuttontypepopup] CHECK CONSTRAINT [cms_models_nextsectionbuttontypepopup_language_id_d78f96a9_fk_custom_models_language_ISO_Code_1]
GO
ALTER TABLE [dbo].[cms_models_nextsectionbuttontypepopup]  WITH CHECK ADD  CONSTRAINT [cms_models_nextsectionbuttontypepopup_test_section_id_88d07a1e_fk_cms_models_testsection_id] FOREIGN KEY([test_section_id])
REFERENCES [dbo].[cms_models_testsection] ([id])
GO
ALTER TABLE [dbo].[cms_models_nextsectionbuttontypepopup] CHECK CONSTRAINT [cms_models_nextsectionbuttontypepopup_test_section_id_88d07a1e_fk_cms_models_testsection_id]
GO
ALTER TABLE [dbo].[cms_models_nextsectionbuttontypeproceed]  WITH CHECK ADD  CONSTRAINT [cms_models_nextsectionbuttontypeproceed_language_id_4884bd07_fk_custom_models_language_ISO_Code_1] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([ISO_Code_1])
GO
ALTER TABLE [dbo].[cms_models_nextsectionbuttontypeproceed] CHECK CONSTRAINT [cms_models_nextsectionbuttontypeproceed_language_id_4884bd07_fk_custom_models_language_ISO_Code_1]
GO
ALTER TABLE [dbo].[cms_models_nextsectionbuttontypeproceed]  WITH CHECK ADD  CONSTRAINT [cms_models_nextsectionbuttontypeproceed_test_section_id_87c9536a_fk_cms_models_testsection_id] FOREIGN KEY([test_section_id])
REFERENCES [dbo].[cms_models_testsection] ([id])
GO
ALTER TABLE [dbo].[cms_models_nextsectionbuttontypeproceed] CHECK CONSTRAINT [cms_models_nextsectionbuttontypeproceed_test_section_id_87c9536a_fk_cms_models_testsection_id]
GO
ALTER TABLE [dbo].[cms_models_orderlesstestpermissions]  WITH CHECK ADD  CONSTRAINT [cms_models_orderlesstestpermissions_ta_extended_profile_id_24e99afe_fk_user_management_models_taextendedprofile_id] FOREIGN KEY([ta_extended_profile_id])
REFERENCES [dbo].[user_management_models_taextendedprofile] ([id])
GO
ALTER TABLE [dbo].[cms_models_orderlesstestpermissions] CHECK CONSTRAINT [cms_models_orderlesstestpermissions_ta_extended_profile_id_24e99afe_fk_user_management_models_taextendedprofile_id]
GO
ALTER TABLE [dbo].[cms_models_pagesection]  WITH CHECK ADD  CONSTRAINT [cms_models_pagesection_section_component_page_id_5f2d5b02_fk_cms_models_sectioncomponentpage_id] FOREIGN KEY([section_component_page_id])
REFERENCES [dbo].[cms_models_sectioncomponentpage] ([id])
GO
ALTER TABLE [dbo].[cms_models_pagesection] CHECK CONSTRAINT [cms_models_pagesection_section_component_page_id_5f2d5b02_fk_cms_models_sectioncomponentpage_id]
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypeimagezoom]  WITH CHECK ADD  CONSTRAINT [cms_models_pagesectiontypeimagezoom_language_id_ea2bd6c0_fk_custom_models_language_ISO_Code_1] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([ISO_Code_1])
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypeimagezoom] CHECK CONSTRAINT [cms_models_pagesectiontypeimagezoom_language_id_ea2bd6c0_fk_custom_models_language_ISO_Code_1]
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypeimagezoom]  WITH CHECK ADD  CONSTRAINT [cms_models_pagesectiontypeimagezoom_page_section_id_2dc05d0b_fk_cms_models_pagesection_id] FOREIGN KEY([page_section_id])
REFERENCES [dbo].[cms_models_pagesection] ([id])
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypeimagezoom] CHECK CONSTRAINT [cms_models_pagesectiontypeimagezoom_page_section_id_2dc05d0b_fk_cms_models_pagesection_id]
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypemarkdown]  WITH CHECK ADD  CONSTRAINT [cms_models_pagesectiontypemarkdown_language_id_6a33fb46_fk_custom_models_language_ISO_Code_1] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([ISO_Code_1])
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypemarkdown] CHECK CONSTRAINT [cms_models_pagesectiontypemarkdown_language_id_6a33fb46_fk_custom_models_language_ISO_Code_1]
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypemarkdown]  WITH CHECK ADD  CONSTRAINT [cms_models_pagesectiontypemarkdown_page_section_id_7056e4ea_fk_cms_models_pagesection_id] FOREIGN KEY([page_section_id])
REFERENCES [dbo].[cms_models_pagesection] ([id])
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypemarkdown] CHECK CONSTRAINT [cms_models_pagesectiontypemarkdown_page_section_id_7056e4ea_fk_cms_models_pagesection_id]
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypesampleemail]  WITH CHECK ADD  CONSTRAINT [cms_models_pagesectiontypesampleemail_language_id_e2c97cd3_fk_custom_models_language_ISO_Code_1] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([ISO_Code_1])
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypesampleemail] CHECK CONSTRAINT [cms_models_pagesectiontypesampleemail_language_id_e2c97cd3_fk_custom_models_language_ISO_Code_1]
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypesampleemail]  WITH CHECK ADD  CONSTRAINT [cms_models_pagesectiontypesampleemail_page_section_id_2408a69f_fk_cms_models_pagesection_id] FOREIGN KEY([page_section_id])
REFERENCES [dbo].[cms_models_pagesection] ([id])
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypesampleemail] CHECK CONSTRAINT [cms_models_pagesectiontypesampleemail_page_section_id_2408a69f_fk_cms_models_pagesection_id]
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypesampleemailresponse]  WITH CHECK ADD  CONSTRAINT [cms_models_pagesectiontypesampleemailresponse_language_id_ea345371_fk_custom_models_language_ISO_Code_1] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([ISO_Code_1])
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypesampleemailresponse] CHECK CONSTRAINT [cms_models_pagesectiontypesampleemailresponse_language_id_ea345371_fk_custom_models_language_ISO_Code_1]
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypesampleemailresponse]  WITH CHECK ADD  CONSTRAINT [cms_models_pagesectiontypesampleemailresponse_page_section_id_ee8b98df_fk_cms_models_pagesection_id] FOREIGN KEY([page_section_id])
REFERENCES [dbo].[cms_models_pagesection] ([id])
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypesampleemailresponse] CHECK CONSTRAINT [cms_models_pagesectiontypesampleemailresponse_page_section_id_ee8b98df_fk_cms_models_pagesection_id]
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypesampletaskresponse]  WITH CHECK ADD  CONSTRAINT [cms_models_pagesectiontypesampletaskresponse_language_id_7d71a824_fk_custom_models_language_ISO_Code_1] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([ISO_Code_1])
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypesampletaskresponse] CHECK CONSTRAINT [cms_models_pagesectiontypesampletaskresponse_language_id_7d71a824_fk_custom_models_language_ISO_Code_1]
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypesampletaskresponse]  WITH CHECK ADD  CONSTRAINT [cms_models_pagesectiontypesampletaskresponse_page_section_id_c784cc05_fk_cms_models_pagesection_id] FOREIGN KEY([page_section_id])
REFERENCES [dbo].[cms_models_pagesection] ([id])
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypesampletaskresponse] CHECK CONSTRAINT [cms_models_pagesectiontypesampletaskresponse_page_section_id_c784cc05_fk_cms_models_pagesection_id]
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypetreedescription]  WITH CHECK ADD  CONSTRAINT [cms_models_pagesectiontypetreedescription_address_book_contact_id_c2d58539_fk_cms_models_addressbookcontact_id] FOREIGN KEY([address_book_contact_id])
REFERENCES [dbo].[cms_models_addressbookcontact] ([id])
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypetreedescription] CHECK CONSTRAINT [cms_models_pagesectiontypetreedescription_address_book_contact_id_c2d58539_fk_cms_models_addressbookcontact_id]
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypetreedescription]  WITH CHECK ADD  CONSTRAINT [cms_models_pagesectiontypetreedescription_language_id_71256d48_fk_custom_models_language_ISO_Code_1] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([ISO_Code_1])
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypetreedescription] CHECK CONSTRAINT [cms_models_pagesectiontypetreedescription_language_id_71256d48_fk_custom_models_language_ISO_Code_1]
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypetreedescription]  WITH CHECK ADD  CONSTRAINT [cms_models_pagesectiontypetreedescription_page_section_id_a3dd572f_fk_cms_models_pagesection_id] FOREIGN KEY([page_section_id])
REFERENCES [dbo].[cms_models_pagesection] ([id])
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypetreedescription] CHECK CONSTRAINT [cms_models_pagesectiontypetreedescription_page_section_id_a3dd572f_fk_cms_models_pagesection_id]
GO
ALTER TABLE [dbo].[cms_models_questionblocktype]  WITH CHECK ADD  CONSTRAINT [cms_models_questionblocktype_test_definition_id_6cbb6eae_fk_cms_models_testdefinition_id] FOREIGN KEY([test_definition_id])
REFERENCES [dbo].[cms_models_testdefinition] ([id])
GO
ALTER TABLE [dbo].[cms_models_questionblocktype] CHECK CONSTRAINT [cms_models_questionblocktype_test_definition_id_6cbb6eae_fk_cms_models_testdefinition_id]
GO
ALTER TABLE [dbo].[cms_models_questionlistrule]  WITH CHECK ADD  CONSTRAINT [cms_models_questionlistrule_test_section_component_id_5464ef8c_fk_cms_models_testsectioncomponent_id] FOREIGN KEY([test_section_component_id])
REFERENCES [dbo].[cms_models_testsectioncomponent] ([id])
GO
ALTER TABLE [dbo].[cms_models_questionlistrule] CHECK CONSTRAINT [cms_models_questionlistrule_test_section_component_id_5464ef8c_fk_cms_models_testsectioncomponent_id]
GO
ALTER TABLE [dbo].[cms_models_questionlistrule_question_block_type]  WITH CHECK ADD  CONSTRAINT [cms_models_questionlistrule_question_block_type_questionblocktype_id_fcad318b_fk_cms_models_questionblocktype_id] FOREIGN KEY([questionblocktype_id])
REFERENCES [dbo].[cms_models_questionblocktype] ([id])
GO
ALTER TABLE [dbo].[cms_models_questionlistrule_question_block_type] CHECK CONSTRAINT [cms_models_questionlistrule_question_block_type_questionblocktype_id_fcad318b_fk_cms_models_questionblocktype_id]
GO
ALTER TABLE [dbo].[cms_models_questionlistrule_question_block_type]  WITH CHECK ADD  CONSTRAINT [cms_models_questionlistrule_question_block_type_questionlistrule_id_c6d3d4c2_fk_cms_models_questionlistrule_id] FOREIGN KEY([questionlistrule_id])
REFERENCES [dbo].[cms_models_questionlistrule] ([id])
GO
ALTER TABLE [dbo].[cms_models_questionlistrule_question_block_type] CHECK CONSTRAINT [cms_models_questionlistrule_question_block_type_questionlistrule_id_c6d3d4c2_fk_cms_models_questionlistrule_id]
GO
ALTER TABLE [dbo].[cms_models_questionsection]  WITH CHECK ADD  CONSTRAINT [cms_models_questionsection_question_id_3008549b_fk_cms_models_newquestion_id] FOREIGN KEY([question_id])
REFERENCES [dbo].[cms_models_newquestion] ([id])
GO
ALTER TABLE [dbo].[cms_models_questionsection] CHECK CONSTRAINT [cms_models_questionsection_question_id_3008549b_fk_cms_models_newquestion_id]
GO
ALTER TABLE [dbo].[cms_models_questionsectiontypemarkdown]  WITH CHECK ADD  CONSTRAINT [cms_models_questionsectiontypemarkdown_language_id_235f31cd_fk_custom_models_language_ISO_Code_1] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([ISO_Code_1])
GO
ALTER TABLE [dbo].[cms_models_questionsectiontypemarkdown] CHECK CONSTRAINT [cms_models_questionsectiontypemarkdown_language_id_235f31cd_fk_custom_models_language_ISO_Code_1]
GO
ALTER TABLE [dbo].[cms_models_questionsectiontypemarkdown]  WITH CHECK ADD  CONSTRAINT [cms_models_questionsectiontypemarkdown_question_section_id_f8a74e6d_fk_cms_models_questionsection_id] FOREIGN KEY([question_section_id])
REFERENCES [dbo].[cms_models_questionsection] ([id])
GO
ALTER TABLE [dbo].[cms_models_questionsectiontypemarkdown] CHECK CONSTRAINT [cms_models_questionsectiontypemarkdown_question_section_id_f8a74e6d_fk_cms_models_questionsection_id]
GO
ALTER TABLE [dbo].[cms_models_questionsituation]  WITH CHECK ADD  CONSTRAINT [cms_models_questionsituation_language_id_c900854a_fk_custom_models_language_ISO_Code_1] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([ISO_Code_1])
GO
ALTER TABLE [dbo].[cms_models_questionsituation] CHECK CONSTRAINT [cms_models_questionsituation_language_id_c900854a_fk_custom_models_language_ISO_Code_1]
GO
ALTER TABLE [dbo].[cms_models_questionsituation]  WITH CHECK ADD  CONSTRAINT [cms_models_questionsituation_question_id_38282eb0_fk_cms_models_newquestion_id] FOREIGN KEY([question_id])
REFERENCES [dbo].[cms_models_newquestion] ([id])
GO
ALTER TABLE [dbo].[cms_models_questionsituation] CHECK CONSTRAINT [cms_models_questionsituation_question_id_38282eb0_fk_cms_models_newquestion_id]
GO
ALTER TABLE [dbo].[cms_models_scoringmethods]  WITH CHECK ADD  CONSTRAINT [cms_models_scoringmethods_method_type_id_43694ccc_fk_cms_models_scoringmethodtypes_id] FOREIGN KEY([method_type_id])
REFERENCES [dbo].[cms_models_scoringmethodtypes] ([id])
GO
ALTER TABLE [dbo].[cms_models_scoringmethods] CHECK CONSTRAINT [cms_models_scoringmethods_method_type_id_43694ccc_fk_cms_models_scoringmethodtypes_id]
GO
ALTER TABLE [dbo].[cms_models_scoringmethods]  WITH CHECK ADD  CONSTRAINT [cms_models_scoringmethods_test_id_3601f271_fk_cms_models_testdefinition_id] FOREIGN KEY([test_id])
REFERENCES [dbo].[cms_models_testdefinition] ([id])
GO
ALTER TABLE [dbo].[cms_models_scoringmethods] CHECK CONSTRAINT [cms_models_scoringmethods_test_id_3601f271_fk_cms_models_testdefinition_id]
GO
ALTER TABLE [dbo].[cms_models_scoringpassfail]  WITH CHECK ADD  CONSTRAINT [cms_models_scoringpassfail_scoring_method_id_991811eb_fk_cms_models_scoringmethods_id] FOREIGN KEY([scoring_method_id])
REFERENCES [dbo].[cms_models_scoringmethods] ([id])
GO
ALTER TABLE [dbo].[cms_models_scoringpassfail] CHECK CONSTRAINT [cms_models_scoringpassfail_scoring_method_id_991811eb_fk_cms_models_scoringmethods_id]
GO
ALTER TABLE [dbo].[cms_models_scoringthreshold]  WITH CHECK ADD  CONSTRAINT [cms_models_scoringthreshold_scoring_method_id_03081867_fk_cms_models_scoringmethods_id] FOREIGN KEY([scoring_method_id])
REFERENCES [dbo].[cms_models_scoringmethods] ([id])
GO
ALTER TABLE [dbo].[cms_models_scoringthreshold] CHECK CONSTRAINT [cms_models_scoringthreshold_scoring_method_id_03081867_fk_cms_models_scoringmethods_id]
GO
ALTER TABLE [dbo].[cms_models_sectioncomponentpage]  WITH CHECK ADD  CONSTRAINT [cms_models_sectioncomponentpage_test_section_component_id_9728c6d0_fk_cms_models_testsectioncomponent_id] FOREIGN KEY([test_section_component_id])
REFERENCES [dbo].[cms_models_testsectioncomponent] ([id])
GO
ALTER TABLE [dbo].[cms_models_sectioncomponentpage] CHECK CONSTRAINT [cms_models_sectioncomponentpage_test_section_component_id_9728c6d0_fk_cms_models_testsectioncomponent_id]
GO
ALTER TABLE [dbo].[cms_models_situationexamplerating]  WITH CHECK ADD  CONSTRAINT [cms_models_situationexamplerating_competency_type_id_ac381784_fk_cms_models_competencytype_id] FOREIGN KEY([competency_type_id])
REFERENCES [dbo].[cms_models_competencytype] ([id])
GO
ALTER TABLE [dbo].[cms_models_situationexamplerating] CHECK CONSTRAINT [cms_models_situationexamplerating_competency_type_id_ac381784_fk_cms_models_competencytype_id]
GO
ALTER TABLE [dbo].[cms_models_situationexamplerating]  WITH CHECK ADD  CONSTRAINT [cms_models_situationexamplerating_question_id_49e6c856_fk_cms_models_newquestion_id] FOREIGN KEY([question_id])
REFERENCES [dbo].[cms_models_newquestion] ([id])
GO
ALTER TABLE [dbo].[cms_models_situationexamplerating] CHECK CONSTRAINT [cms_models_situationexamplerating_question_id_49e6c856_fk_cms_models_newquestion_id]
GO
ALTER TABLE [dbo].[cms_models_situationexampleratingdetails]  WITH CHECK ADD  CONSTRAINT [cms_models_situationexampleratingdetails_example_rating_id_104f8c7b_fk_cms_models_situationexamplerating_id] FOREIGN KEY([example_rating_id])
REFERENCES [dbo].[cms_models_situationexamplerating] ([id])
GO
ALTER TABLE [dbo].[cms_models_situationexampleratingdetails] CHECK CONSTRAINT [cms_models_situationexampleratingdetails_example_rating_id_104f8c7b_fk_cms_models_situationexamplerating_id]
GO
ALTER TABLE [dbo].[cms_models_situationexampleratingdetails]  WITH CHECK ADD  CONSTRAINT [cms_models_situationexampleratingdetails_language_id_dda792a3_fk_custom_models_language_ISO_Code_1] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([ISO_Code_1])
GO
ALTER TABLE [dbo].[cms_models_situationexampleratingdetails] CHECK CONSTRAINT [cms_models_situationexampleratingdetails_language_id_dda792a3_fk_custom_models_language_ISO_Code_1]
GO
ALTER TABLE [dbo].[cms_models_testpermissions]  WITH CHECK ADD  CONSTRAINT [cms_models_testpermissions_test_id_8048e01b_fk_cms_models_testdefinition_id] FOREIGN KEY([test_id])
REFERENCES [dbo].[cms_models_testdefinition] ([id])
GO
ALTER TABLE [dbo].[cms_models_testpermissions] CHECK CONSTRAINT [cms_models_testpermissions_test_id_8048e01b_fk_cms_models_testdefinition_id]
GO
ALTER TABLE [dbo].[cms_models_testpermissions]  WITH CHECK ADD  CONSTRAINT [cms_models_testpermissions_username_id_6fc416b0_fk_user_management_models_user_username] FOREIGN KEY([username_id])
REFERENCES [dbo].[user_management_models_user] ([username])
GO
ALTER TABLE [dbo].[cms_models_testpermissions] CHECK CONSTRAINT [cms_models_testpermissions_username_id_6fc416b0_fk_user_management_models_user_username]
GO
ALTER TABLE [dbo].[cms_models_testsection]  WITH CHECK ADD  CONSTRAINT [cms_models_testsection_test_definition_id_8528a699_fk_cms_models_testdefinition_id] FOREIGN KEY([test_definition_id])
REFERENCES [dbo].[cms_models_testdefinition] ([id])
GO
ALTER TABLE [dbo].[cms_models_testsection] CHECK CONSTRAINT [cms_models_testsection_test_definition_id_8528a699_fk_cms_models_testdefinition_id]
GO
ALTER TABLE [dbo].[cms_models_testsectioncomponent]  WITH CHECK ADD  CONSTRAINT [cms_models_testsectioncomponent_language_id_629fef2b_fk_custom_models_language_ISO_Code_1] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([ISO_Code_1])
GO
ALTER TABLE [dbo].[cms_models_testsectioncomponent] CHECK CONSTRAINT [cms_models_testsectioncomponent_language_id_629fef2b_fk_custom_models_language_ISO_Code_1]
GO
ALTER TABLE [dbo].[cms_models_testsectioncomponent_test_section]  WITH CHECK ADD  CONSTRAINT [cms_models_testsectioncomponent_test_section_testsection_id_94e1c7f9_fk_cms_models_testsection_id] FOREIGN KEY([testsection_id])
REFERENCES [dbo].[cms_models_testsection] ([id])
GO
ALTER TABLE [dbo].[cms_models_testsectioncomponent_test_section] CHECK CONSTRAINT [cms_models_testsectioncomponent_test_section_testsection_id_94e1c7f9_fk_cms_models_testsection_id]
GO
ALTER TABLE [dbo].[cms_models_testsectioncomponent_test_section]  WITH CHECK ADD  CONSTRAINT [cms_models_testsectioncomponent_test_section_testsectioncomponent_id_4be93fd7_fk_cms_models_testsectioncomponent_id] FOREIGN KEY([testsectioncomponent_id])
REFERENCES [dbo].[cms_models_testsectioncomponent] ([id])
GO
ALTER TABLE [dbo].[cms_models_testsectioncomponent_test_section] CHECK CONSTRAINT [cms_models_testsectioncomponent_test_section_testsectioncomponent_id_4be93fd7_fk_cms_models_testsectioncomponent_id]
GO
ALTER TABLE [dbo].[cms_models_testsectionreducer]  WITH CHECK ADD  CONSTRAINT [cms_models_testsectionreducer_reducer_id_f963998d_fk_cms_models_reducercontrol_reducer] FOREIGN KEY([reducer_id])
REFERENCES [dbo].[cms_models_reducercontrol] ([reducer])
GO
ALTER TABLE [dbo].[cms_models_testsectionreducer] CHECK CONSTRAINT [cms_models_testsectionreducer_reducer_id_f963998d_fk_cms_models_reducercontrol_reducer]
GO
ALTER TABLE [dbo].[cms_models_testsectionreducer]  WITH CHECK ADD  CONSTRAINT [cms_models_testsectionreducer_test_section_id_22fe65f5_fk_cms_models_testsection_id] FOREIGN KEY([test_section_id])
REFERENCES [dbo].[cms_models_testsection] ([id])
GO
ALTER TABLE [dbo].[cms_models_testsectionreducer] CHECK CONSTRAINT [cms_models_testsectionreducer_test_section_id_22fe65f5_fk_cms_models_testsection_id]
GO
ALTER TABLE [dbo].[cms_models_viewedquestions]  WITH CHECK ADD  CONSTRAINT [cms_models_viewedquestions_user_id_363d8337_fk_user_management_models_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_viewedquestions] CHECK CONSTRAINT [cms_models_viewedquestions_user_id_363d8337_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_accommodationrequest]  WITH CHECK ADD  CONSTRAINT [custom_models_accommodationrequest_break_bank_id_4981951a_fk_custom_models_breakbank_id] FOREIGN KEY([break_bank_id])
REFERENCES [dbo].[custom_models_breakbank] ([id])
GO
ALTER TABLE [dbo].[custom_models_accommodationrequest] CHECK CONSTRAINT [custom_models_accommodationrequest_break_bank_id_4981951a_fk_custom_models_breakbank_id]
GO
ALTER TABLE [dbo].[custom_models_additionaltime]  WITH CHECK ADD  CONSTRAINT [custom_models_additionaltime_accommodation_request_id_1c869f3a_fk_custom_models_accommodationrequest_id] FOREIGN KEY([accommodation_request_id])
REFERENCES [dbo].[custom_models_accommodationrequest] ([id])
GO
ALTER TABLE [dbo].[custom_models_additionaltime] CHECK CONSTRAINT [custom_models_additionaltime_accommodation_request_id_1c869f3a_fk_custom_models_accommodationrequest_id]
GO
ALTER TABLE [dbo].[custom_models_additionaltime]  WITH CHECK ADD  CONSTRAINT [custom_models_additionaltime_test_section_id_c5195f14_fk_cms_models_testsection_id] FOREIGN KEY([test_section_id])
REFERENCES [dbo].[cms_models_testsection] ([id])
GO
ALTER TABLE [dbo].[custom_models_additionaltime] CHECK CONSTRAINT [custom_models_additionaltime_test_section_id_c5195f14_fk_cms_models_testsection_id]
GO
ALTER TABLE [dbo].[custom_models_assignedanswerchoices]  WITH CHECK ADD  CONSTRAINT [custom_models_assignedanswerchoices_assigned_test_id_f747586b_fk_custom_models_assignedtest_id] FOREIGN KEY([assigned_test_id])
REFERENCES [dbo].[custom_models_assignedtest] ([id])
GO
ALTER TABLE [dbo].[custom_models_assignedanswerchoices] CHECK CONSTRAINT [custom_models_assignedanswerchoices_assigned_test_id_f747586b_fk_custom_models_assignedtest_id]
GO
ALTER TABLE [dbo].[custom_models_assignedanswerchoices]  WITH CHECK ADD  CONSTRAINT [custom_models_assignedanswerchoices_item_id_df0a3d61_fk_cms_models_items_id] FOREIGN KEY([item_id])
REFERENCES [dbo].[cms_models_items] ([id])
GO
ALTER TABLE [dbo].[custom_models_assignedanswerchoices] CHECK CONSTRAINT [custom_models_assignedanswerchoices_item_id_df0a3d61_fk_cms_models_items_id]
GO
ALTER TABLE [dbo].[custom_models_assignedanswerchoices]  WITH CHECK ADD  CONSTRAINT [custom_models_assignedanswerchoices_question_id_3605d91d_fk_cms_models_newquestion_id] FOREIGN KEY([question_id])
REFERENCES [dbo].[cms_models_newquestion] ([id])
GO
ALTER TABLE [dbo].[custom_models_assignedanswerchoices] CHECK CONSTRAINT [custom_models_assignedanswerchoices_question_id_3605d91d_fk_cms_models_newquestion_id]
GO
ALTER TABLE [dbo].[custom_models_assignedquestionslist]  WITH CHECK ADD  CONSTRAINT [custom_models_assignedquestionslist_assigned_test_id_3853595b_fk_custom_models_assignedtest_id] FOREIGN KEY([assigned_test_id])
REFERENCES [dbo].[custom_models_assignedtest] ([id])
GO
ALTER TABLE [dbo].[custom_models_assignedquestionslist] CHECK CONSTRAINT [custom_models_assignedquestionslist_assigned_test_id_3853595b_fk_custom_models_assignedtest_id]
GO
ALTER TABLE [dbo].[custom_models_assignedquestionslist]  WITH CHECK ADD  CONSTRAINT [custom_models_assignedquestionslist_test_section_id_98703e7c_fk_cms_models_testsection_id] FOREIGN KEY([test_section_id])
REFERENCES [dbo].[cms_models_testsection] ([id])
GO
ALTER TABLE [dbo].[custom_models_assignedquestionslist] CHECK CONSTRAINT [custom_models_assignedquestionslist_test_section_id_98703e7c_fk_cms_models_testsection_id]
GO
ALTER TABLE [dbo].[custom_models_assignedtest]  WITH CHECK ADD  CONSTRAINT [custom_models_assignedtest_accommodation_request_id_bed6e914_fk_custom_models_accommodationrequest_id] FOREIGN KEY([accommodation_request_id])
REFERENCES [dbo].[custom_models_accommodationrequest] ([id])
GO
ALTER TABLE [dbo].[custom_models_assignedtest] CHECK CONSTRAINT [custom_models_assignedtest_accommodation_request_id_bed6e914_fk_custom_models_accommodationrequest_id]
GO
ALTER TABLE [dbo].[custom_models_assignedtest]  WITH CHECK ADD  CONSTRAINT [custom_models_assignedtest_orderless_financial_data_id_7f81cd1b_fk_custom_models_orderlessfinancialdata_id] FOREIGN KEY([orderless_financial_data_id])
REFERENCES [dbo].[custom_models_orderlessfinancialdata] ([id])
GO
ALTER TABLE [dbo].[custom_models_assignedtest] CHECK CONSTRAINT [custom_models_assignedtest_orderless_financial_data_id_7f81cd1b_fk_custom_models_orderlessfinancialdata_id]
GO
ALTER TABLE [dbo].[custom_models_assignedtest]  WITH CHECK ADD  CONSTRAINT [custom_models_assignedtest_ta_id_d616f64f_fk_user_management_models_user_username] FOREIGN KEY([ta_id])
REFERENCES [dbo].[user_management_models_user] ([username])
GO
ALTER TABLE [dbo].[custom_models_assignedtest] CHECK CONSTRAINT [custom_models_assignedtest_ta_id_d616f64f_fk_user_management_models_user_username]
GO
ALTER TABLE [dbo].[custom_models_assignedtest]  WITH CHECK ADD  CONSTRAINT [custom_models_assignedtest_test_id_3f85b2bd_fk_cms_models_testdefinition_id] FOREIGN KEY([test_id])
REFERENCES [dbo].[cms_models_testdefinition] ([id])
GO
ALTER TABLE [dbo].[custom_models_assignedtest] CHECK CONSTRAINT [custom_models_assignedtest_test_id_3f85b2bd_fk_cms_models_testdefinition_id]
GO
ALTER TABLE [dbo].[custom_models_assignedtest]  WITH CHECK ADD  CONSTRAINT [custom_models_assignedtest_test_section_id_4b099ca4_fk_cms_models_testsection_id] FOREIGN KEY([test_section_id])
REFERENCES [dbo].[cms_models_testsection] ([id])
GO
ALTER TABLE [dbo].[custom_models_assignedtest] CHECK CONSTRAINT [custom_models_assignedtest_test_section_id_4b099ca4_fk_cms_models_testsection_id]
GO
ALTER TABLE [dbo].[custom_models_assignedtest]  WITH CHECK ADD  CONSTRAINT [custom_models_assignedtest_test_session_language_id_51c9e503_fk_custom_models_language_language_id] FOREIGN KEY([test_session_language_id])
REFERENCES [dbo].[custom_models_language] ([language_id])
GO
ALTER TABLE [dbo].[custom_models_assignedtest] CHECK CONSTRAINT [custom_models_assignedtest_test_session_language_id_51c9e503_fk_custom_models_language_language_id]
GO
ALTER TABLE [dbo].[custom_models_assignedtest]  WITH CHECK ADD  CONSTRAINT [custom_models_assignedtest_username_id_3e1912bd_fk_user_management_models_user_username] FOREIGN KEY([username_id])
REFERENCES [dbo].[user_management_models_user] ([username])
GO
ALTER TABLE [dbo].[custom_models_assignedtest] CHECK CONSTRAINT [custom_models_assignedtest_username_id_3e1912bd_fk_user_management_models_user_username]
GO
ALTER TABLE [dbo].[custom_models_assignedtestanswerscore]  WITH CHECK ADD  CONSTRAINT [custom_models_assignedtestanswerscore_competency_id_d76589c4_fk_cms_models_competencytype_id] FOREIGN KEY([competency_id])
REFERENCES [dbo].[cms_models_competencytype] ([id])
GO
ALTER TABLE [dbo].[custom_models_assignedtestanswerscore] CHECK CONSTRAINT [custom_models_assignedtestanswerscore_competency_id_d76589c4_fk_cms_models_competencytype_id]
GO
ALTER TABLE [dbo].[custom_models_assignedtestanswerscore]  WITH CHECK ADD  CONSTRAINT [custom_models_assignedtestanswerscore_question_id_b91975dc_fk_cms_models_newquestion_id] FOREIGN KEY([question_id])
REFERENCES [dbo].[cms_models_newquestion] ([id])
GO
ALTER TABLE [dbo].[custom_models_assignedtestanswerscore] CHECK CONSTRAINT [custom_models_assignedtestanswerscore_question_id_b91975dc_fk_cms_models_newquestion_id]
GO
ALTER TABLE [dbo].[custom_models_assignedtestanswerscore]  WITH CHECK ADD  CONSTRAINT [custom_models_assignedtestanswerscore_scorer_assigned_test_id_10b59bb0_fk_custom_models_testscorerassignment_id] FOREIGN KEY([scorer_assigned_test_id])
REFERENCES [dbo].[custom_models_testscorerassignment] ([id])
GO
ALTER TABLE [dbo].[custom_models_assignedtestanswerscore] CHECK CONSTRAINT [custom_models_assignedtestanswerscore_scorer_assigned_test_id_10b59bb0_fk_custom_models_testscorerassignment_id]
GO
ALTER TABLE [dbo].[custom_models_assignedtestsection]  WITH CHECK ADD  CONSTRAINT [custom_models_assignedtestsection_assigned_test_id_987da5cb_fk_custom_models_assignedtest_id] FOREIGN KEY([assigned_test_id])
REFERENCES [dbo].[custom_models_assignedtest] ([id])
GO
ALTER TABLE [dbo].[custom_models_assignedtestsection] CHECK CONSTRAINT [custom_models_assignedtestsection_assigned_test_id_987da5cb_fk_custom_models_assignedtest_id]
GO
ALTER TABLE [dbo].[custom_models_assignedtestsection]  WITH CHECK ADD  CONSTRAINT [custom_models_assignedtestsection_test_section_id_6ad5b8e1_fk_cms_models_testsection_id] FOREIGN KEY([test_section_id])
REFERENCES [dbo].[cms_models_testsection] ([id])
GO
ALTER TABLE [dbo].[custom_models_assignedtestsection] CHECK CONSTRAINT [custom_models_assignedtestsection_test_section_id_6ad5b8e1_fk_cms_models_testsection_id]
GO
ALTER TABLE [dbo].[custom_models_assignedtestsectionaccesstimes]  WITH CHECK ADD  CONSTRAINT [custom_models_assignedtestsectionaccesstimes_assigned_test_section_id_791d317d_fk_custom_models_assignedtestsection_id] FOREIGN KEY([assigned_test_section_id])
REFERENCES [dbo].[custom_models_assignedtestsection] ([id])
GO
ALTER TABLE [dbo].[custom_models_assignedtestsectionaccesstimes] CHECK CONSTRAINT [custom_models_assignedtestsectionaccesstimes_assigned_test_section_id_791d317d_fk_custom_models_assignedtestsection_id]
GO
ALTER TABLE [dbo].[custom_models_breakbankactions]  WITH CHECK ADD  CONSTRAINT [custom_models_breakbankactions_break_bank_id_c476530a_fk_custom_models_breakbank_id] FOREIGN KEY([break_bank_id])
REFERENCES [dbo].[custom_models_breakbank] ([id])
GO
ALTER TABLE [dbo].[custom_models_breakbankactions] CHECK CONSTRAINT [custom_models_breakbankactions_break_bank_id_c476530a_fk_custom_models_breakbank_id]
GO
ALTER TABLE [dbo].[custom_models_breakbankactions]  WITH CHECK ADD  CONSTRAINT [custom_models_breakbankactions_test_section_id_84fc0f7e_fk_cms_models_testsection_id] FOREIGN KEY([test_section_id])
REFERENCES [dbo].[cms_models_testsection] ([id])
GO
ALTER TABLE [dbo].[custom_models_breakbankactions] CHECK CONSTRAINT [custom_models_breakbankactions_test_section_id_84fc0f7e_fk_cms_models_testsection_id]
GO
ALTER TABLE [dbo].[custom_models_candidateanswers]  WITH CHECK ADD  CONSTRAINT [custom_models_candidateanswers_assigned_test_id_b9450261_fk_custom_models_assignedtest_id] FOREIGN KEY([assigned_test_id])
REFERENCES [dbo].[custom_models_assignedtest] ([id])
GO
ALTER TABLE [dbo].[custom_models_candidateanswers] CHECK CONSTRAINT [custom_models_candidateanswers_assigned_test_id_b9450261_fk_custom_models_assignedtest_id]
GO
ALTER TABLE [dbo].[custom_models_candidateanswers]  WITH CHECK ADD  CONSTRAINT [custom_models_candidateanswers_item_id_e674b8d3_fk_cms_models_items_id] FOREIGN KEY([item_id])
REFERENCES [dbo].[cms_models_items] ([id])
GO
ALTER TABLE [dbo].[custom_models_candidateanswers] CHECK CONSTRAINT [custom_models_candidateanswers_item_id_e674b8d3_fk_cms_models_items_id]
GO
ALTER TABLE [dbo].[custom_models_candidateanswers]  WITH CHECK ADD  CONSTRAINT [custom_models_candidateanswers_question_id_b058e438_fk_cms_models_newquestion_id] FOREIGN KEY([question_id])
REFERENCES [dbo].[cms_models_newquestion] ([id])
GO
ALTER TABLE [dbo].[custom_models_candidateanswers] CHECK CONSTRAINT [custom_models_candidateanswers_question_id_b058e438_fk_cms_models_newquestion_id]
GO
ALTER TABLE [dbo].[custom_models_candidateanswers]  WITH CHECK ADD  CONSTRAINT [custom_models_candidateanswers_selected_language_id_fabe2905_fk_custom_models_language_language_id] FOREIGN KEY([selected_language_id])
REFERENCES [dbo].[custom_models_language] ([language_id])
GO
ALTER TABLE [dbo].[custom_models_candidateanswers] CHECK CONSTRAINT [custom_models_candidateanswers_selected_language_id_fabe2905_fk_custom_models_language_language_id]
GO
ALTER TABLE [dbo].[custom_models_candidateanswers]  WITH CHECK ADD  CONSTRAINT [custom_models_candidateanswers_test_section_component_id_d53f08dc_fk_cms_models_testsectioncomponent_id] FOREIGN KEY([test_section_component_id])
REFERENCES [dbo].[cms_models_testsectioncomponent] ([id])
GO
ALTER TABLE [dbo].[custom_models_candidateanswers] CHECK CONSTRAINT [custom_models_candidateanswers_test_section_component_id_d53f08dc_fk_cms_models_testsectioncomponent_id]
GO
ALTER TABLE [dbo].[custom_models_candidateemailresponseanswers]  WITH CHECK ADD  CONSTRAINT [custom_models_candidateemailresponseanswers_candidate_answers_id_778d7cf8_fk_custom_models_candidateanswers_id] FOREIGN KEY([candidate_answers_id])
REFERENCES [dbo].[custom_models_candidateanswers] ([id])
GO
ALTER TABLE [dbo].[custom_models_candidateemailresponseanswers] CHECK CONSTRAINT [custom_models_candidateemailresponseanswers_candidate_answers_id_778d7cf8_fk_custom_models_candidateanswers_id]
GO
ALTER TABLE [dbo].[custom_models_candidateemailresponseanswers_email_cc]  WITH CHECK ADD  CONSTRAINT [custom_models_candidateemailresponseanswer_candidateemailresponseanswers_id_133a2c98_fk_custom_models_candidateemailre] FOREIGN KEY([candidateemailresponseanswers_id])
REFERENCES [dbo].[custom_models_candidateemailresponseanswers] ([id])
GO
ALTER TABLE [dbo].[custom_models_candidateemailresponseanswers_email_cc] CHECK CONSTRAINT [custom_models_candidateemailresponseanswer_candidateemailresponseanswers_id_133a2c98_fk_custom_models_candidateemailre]
GO
ALTER TABLE [dbo].[custom_models_candidateemailresponseanswers_email_cc]  WITH CHECK ADD  CONSTRAINT [custom_models_candidateemailresponseanswers_email_cc_addressbookcontact_id_ec3b48ac_fk_cms_models_addressbookcontact_id] FOREIGN KEY([addressbookcontact_id])
REFERENCES [dbo].[cms_models_addressbookcontact] ([id])
GO
ALTER TABLE [dbo].[custom_models_candidateemailresponseanswers_email_cc] CHECK CONSTRAINT [custom_models_candidateemailresponseanswers_email_cc_addressbookcontact_id_ec3b48ac_fk_cms_models_addressbookcontact_id]
GO
ALTER TABLE [dbo].[custom_models_candidateemailresponseanswers_email_to]  WITH CHECK ADD  CONSTRAINT [custom_models_candidateemailresponseanswer_candidateemailresponseanswers_id_6cf729a1_fk_custom_models_candidateemailre] FOREIGN KEY([candidateemailresponseanswers_id])
REFERENCES [dbo].[custom_models_candidateemailresponseanswers] ([id])
GO
ALTER TABLE [dbo].[custom_models_candidateemailresponseanswers_email_to] CHECK CONSTRAINT [custom_models_candidateemailresponseanswer_candidateemailresponseanswers_id_6cf729a1_fk_custom_models_candidateemailre]
GO
ALTER TABLE [dbo].[custom_models_candidateemailresponseanswers_email_to]  WITH CHECK ADD  CONSTRAINT [custom_models_candidateemailresponseanswers_email_to_addressbookcontact_id_dd690d67_fk_cms_models_addressbookcontact_id] FOREIGN KEY([addressbookcontact_id])
REFERENCES [dbo].[cms_models_addressbookcontact] ([id])
GO
ALTER TABLE [dbo].[custom_models_candidateemailresponseanswers_email_to] CHECK CONSTRAINT [custom_models_candidateemailresponseanswers_email_to_addressbookcontact_id_dd690d67_fk_cms_models_addressbookcontact_id]
GO
ALTER TABLE [dbo].[custom_models_candidatemultiplechoiceanswers]  WITH CHECK ADD  CONSTRAINT [custom_models_candidatemultiplechoiceanswers_answer_id_48929347_fk_cms_models_answer_id] FOREIGN KEY([answer_id])
REFERENCES [dbo].[cms_models_answer] ([id])
GO
ALTER TABLE [dbo].[custom_models_candidatemultiplechoiceanswers] CHECK CONSTRAINT [custom_models_candidatemultiplechoiceanswers_answer_id_48929347_fk_cms_models_answer_id]
GO
ALTER TABLE [dbo].[custom_models_candidatemultiplechoiceanswers]  WITH CHECK ADD  CONSTRAINT [custom_models_candidatemultiplechoiceanswers_candidate_answers_id_04d6bdbc_fk_custom_models_candidateanswers_id] FOREIGN KEY([candidate_answers_id])
REFERENCES [dbo].[custom_models_candidateanswers] ([id])
GO
ALTER TABLE [dbo].[custom_models_candidatemultiplechoiceanswers] CHECK CONSTRAINT [custom_models_candidatemultiplechoiceanswers_candidate_answers_id_04d6bdbc_fk_custom_models_candidateanswers_id]
GO
ALTER TABLE [dbo].[custom_models_candidatetaskresponseanswers]  WITH CHECK ADD  CONSTRAINT [custom_models_candidatetaskresponseanswers_candidate_answers_id_c31f131d_fk_custom_models_candidateanswers_id] FOREIGN KEY([candidate_answers_id])
REFERENCES [dbo].[custom_models_candidateanswers] ([id])
GO
ALTER TABLE [dbo].[custom_models_candidatetaskresponseanswers] CHECK CONSTRAINT [custom_models_candidatetaskresponseanswers_candidate_answers_id_c31f131d_fk_custom_models_candidateanswers_id]
GO
ALTER TABLE [dbo].[custom_models_ettaactions]  WITH CHECK ADD  CONSTRAINT [custom_models_ettaactions_action_type_id_cb7d22be_fk_custom_models_ettaactiontypes_action_type] FOREIGN KEY([action_type_id])
REFERENCES [dbo].[custom_models_ettaactiontypes] ([action_type])
GO
ALTER TABLE [dbo].[custom_models_ettaactions] CHECK CONSTRAINT [custom_models_ettaactions_action_type_id_cb7d22be_fk_custom_models_ettaactiontypes_action_type]
GO
ALTER TABLE [dbo].[custom_models_ettaactions]  WITH CHECK ADD  CONSTRAINT [custom_models_ettaactions_assigned_test_id_72b6dd96_fk_custom_models_assignedtest_id] FOREIGN KEY([assigned_test_id])
REFERENCES [dbo].[custom_models_assignedtest] ([id])
GO
ALTER TABLE [dbo].[custom_models_ettaactions] CHECK CONSTRAINT [custom_models_ettaactions_assigned_test_id_72b6dd96_fk_custom_models_assignedtest_id]
GO
ALTER TABLE [dbo].[custom_models_historicalaccommodationrequest]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalaccommodationrequest_history_user_id_48058e1a_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalaccommodationrequest] CHECK CONSTRAINT [custom_models_historicalaccommodationrequest_history_user_id_48058e1a_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaladditionaltime]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaladditionaltime_history_user_id_79ecc5ad_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaladditionaltime] CHECK CONSTRAINT [custom_models_historicaladditionaltime_history_user_id_79ecc5ad_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalassignedanswerchoices]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalassignedanswerchoices_history_user_id_c362dfcc_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalassignedanswerchoices] CHECK CONSTRAINT [custom_models_historicalassignedanswerchoices_history_user_id_c362dfcc_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalassignedquestionslist]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalassignedquestionslist_history_user_id_2432475f_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalassignedquestionslist] CHECK CONSTRAINT [custom_models_historicalassignedquestionslist_history_user_id_2432475f_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalassignedtest]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalassignedtest_history_user_id_35caf1b3_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalassignedtest] CHECK CONSTRAINT [custom_models_historicalassignedtest_history_user_id_35caf1b3_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalassignedtestanswerscore]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalassignedtestanswerscore_history_user_id_6d006e79_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalassignedtestanswerscore] CHECK CONSTRAINT [custom_models_historicalassignedtestanswerscore_history_user_id_6d006e79_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalassignedtestsection]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalassignedtestsection_history_user_id_2bc9c72e_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalassignedtestsection] CHECK CONSTRAINT [custom_models_historicalassignedtestsection_history_user_id_2bc9c72e_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalassignedtestsectionaccesstimes]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalassignedtestsectionaccesstimes_history_user_id_f8454526_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalassignedtestsectionaccesstimes] CHECK CONSTRAINT [custom_models_historicalassignedtestsectionaccesstimes_history_user_id_f8454526_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalbreakbank]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalbreakbank_history_user_id_d599effa_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalbreakbank] CHECK CONSTRAINT [custom_models_historicalbreakbank_history_user_id_d599effa_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalbreakbankactions]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalbreakbankactions_history_user_id_96fa1293_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalbreakbankactions] CHECK CONSTRAINT [custom_models_historicalbreakbankactions_history_user_id_96fa1293_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalcandidateanswers]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalcandidateanswers_history_user_id_01cd4f89_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalcandidateanswers] CHECK CONSTRAINT [custom_models_historicalcandidateanswers_history_user_id_01cd4f89_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalcandidateemailresponseanswers]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalcandidateemailresponseanswers_history_user_id_2186ad1f_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalcandidateemailresponseanswers] CHECK CONSTRAINT [custom_models_historicalcandidateemailresponseanswers_history_user_id_2186ad1f_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalcandidatemultiplechoiceanswers]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalcandidatemultiplechoiceanswers_history_user_id_c8affaca_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalcandidatemultiplechoiceanswers] CHECK CONSTRAINT [custom_models_historicalcandidatemultiplechoiceanswers_history_user_id_c8affaca_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalcandidatetaskresponseanswers]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalcandidatetaskresponseanswers_history_user_id_bc92de0a_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalcandidatetaskresponseanswers] CHECK CONSTRAINT [custom_models_historicalcandidatetaskresponseanswers_history_user_id_bc92de0a_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalcriticality]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalcriticality_history_user_id_a66b50d4_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalcriticality] CHECK CONSTRAINT [custom_models_historicalcriticality_history_user_id_a66b50d4_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalettaactions]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalettaactions_history_user_id_6c9d09f7_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalettaactions] CHECK CONSTRAINT [custom_models_historicalettaactions_history_user_id_6c9d09f7_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalettaactiontypes]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalettaactiontypes_history_user_id_7a3aa15e_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalettaactiontypes] CHECK CONSTRAINT [custom_models_historicalettaactiontypes_history_user_id_7a3aa15e_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicallanguage]  WITH CHECK ADD  CONSTRAINT [custom_models_historicallanguage_history_user_id_15b8a210_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicallanguage] CHECK CONSTRAINT [custom_models_historicallanguage_history_user_id_15b8a210_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicallanguagetext]  WITH CHECK ADD  CONSTRAINT [custom_models_historicallanguagetext_history_user_id_dc7e850a_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicallanguagetext] CHECK CONSTRAINT [custom_models_historicallanguagetext_history_user_id_dc7e850a_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicallocktestactions]  WITH CHECK ADD  CONSTRAINT [custom_models_historicallocktestactions_history_user_id_07a0396d_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicallocktestactions] CHECK CONSTRAINT [custom_models_historicallocktestactions_history_user_id_07a0396d_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalnotepad]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalnotepad_history_user_id_746fd39e_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalnotepad] CHECK CONSTRAINT [custom_models_historicalnotepad_history_user_id_746fd39e_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalorderlessfinancialdata]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalorderlessfinancialdata_history_user_id_ceae4655_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalorderlessfinancialdata] CHECK CONSTRAINT [custom_models_historicalorderlessfinancialdata_history_user_id_ceae4655_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalpausetestactions]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalpausetestactions_history_user_id_200ee504_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalpausetestactions] CHECK CONSTRAINT [custom_models_historicalpausetestactions_history_user_id_200ee504_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalreasonsfortesting]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalreasonsfortesting_history_user_id_44a16c2b_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalreasonsfortesting] CHECK CONSTRAINT [custom_models_historicalreasonsfortesting_history_user_id_44a16c2b_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalsystemalert]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalsystemalert_history_user_id_0a714e2a_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalsystemalert] CHECK CONSTRAINT [custom_models_historicalsystemalert_history_user_id_0a714e2a_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaltaactions]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaltaactions_history_user_id_08fb14d6_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaltaactions] CHECK CONSTRAINT [custom_models_historicaltaactions_history_user_id_08fb14d6_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaltaactiontypes]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaltaactiontypes_history_user_id_fd011f0b_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaltaactiontypes] CHECK CONSTRAINT [custom_models_historicaltaactiontypes_history_user_id_fd011f0b_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaltestaccesscode]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaltestaccesscode_history_user_id_afcad320_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaltestaccesscode] CHECK CONSTRAINT [custom_models_historicaltestaccesscode_history_user_id_afcad320_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaltestscorerassignment]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaltestscorerassignment_history_user_id_fdae91ec_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaltestscorerassignment] CHECK CONSTRAINT [custom_models_historicaltestscorerassignment_history_user_id_fdae91ec_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaluitinviterelatedcandidates]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaluitinviterelatedcandidates_history_user_id_8663059b_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaluitinviterelatedcandidates] CHECK CONSTRAINT [custom_models_historicaluitinviterelatedcandidates_history_user_id_8663059b_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaluitinvites]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaluitinvites_history_user_id_2f97b781_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaluitinvites] CHECK CONSTRAINT [custom_models_historicaluitinvites_history_user_id_2f97b781_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaluitreasonsfordeletion]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaluitreasonsfordeletion_history_user_id_bd145b99_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaluitreasonsfordeletion] CHECK CONSTRAINT [custom_models_historicaluitreasonsfordeletion_history_user_id_bd145b99_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaluitreasonsformodification]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaluitreasonsformodification_history_user_id_84a56fd9_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaluitreasonsformodification] CHECK CONSTRAINT [custom_models_historicaluitreasonsformodification_history_user_id_84a56fd9_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalunsupervisedtestaccesscode]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalunsupervisedtestaccesscode_history_user_id_a604e192_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalunsupervisedtestaccesscode] CHECK CONSTRAINT [custom_models_historicalunsupervisedtestaccesscode_history_user_id_a604e192_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_languagetext]  WITH CHECK ADD  CONSTRAINT [custom_models_languagetext_language_id_59918655_fk_custom_models_language_language_id] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([language_id])
GO
ALTER TABLE [dbo].[custom_models_languagetext] CHECK CONSTRAINT [custom_models_languagetext_language_id_59918655_fk_custom_models_language_language_id]
GO
ALTER TABLE [dbo].[custom_models_languagetext]  WITH CHECK ADD  CONSTRAINT [custom_models_languagetext_language_ref_id_23ecc389_fk_custom_models_language_language_id] FOREIGN KEY([language_ref_id])
REFERENCES [dbo].[custom_models_language] ([language_id])
GO
ALTER TABLE [dbo].[custom_models_languagetext] CHECK CONSTRAINT [custom_models_languagetext_language_ref_id_23ecc389_fk_custom_models_language_language_id]
GO
ALTER TABLE [dbo].[custom_models_locktestactions]  WITH CHECK ADD  CONSTRAINT [custom_models_locktestactions_ta_action_id_5bdbaa00_fk_custom_models_taactions_id] FOREIGN KEY([ta_action_id])
REFERENCES [dbo].[custom_models_taactions] ([id])
GO
ALTER TABLE [dbo].[custom_models_locktestactions] CHECK CONSTRAINT [custom_models_locktestactions_ta_action_id_5bdbaa00_fk_custom_models_taactions_id]
GO
ALTER TABLE [dbo].[custom_models_notepad]  WITH CHECK ADD  CONSTRAINT [custom_models_notepad_assigned_test_id_97426b23_fk_custom_models_assignedtest_id] FOREIGN KEY([assigned_test_id])
REFERENCES [dbo].[custom_models_assignedtest] ([id])
GO
ALTER TABLE [dbo].[custom_models_notepad] CHECK CONSTRAINT [custom_models_notepad_assigned_test_id_97426b23_fk_custom_models_assignedtest_id]
GO
ALTER TABLE [dbo].[custom_models_orderlessfinancialdata]  WITH CHECK ADD  CONSTRAINT [custom_models_orderlessfinancialdata_reason_for_testing_id_f5867705_fk_custom_models_reasonsfortesting_id] FOREIGN KEY([reason_for_testing_id])
REFERENCES [dbo].[custom_models_reasonsfortesting] ([id])
GO
ALTER TABLE [dbo].[custom_models_orderlessfinancialdata] CHECK CONSTRAINT [custom_models_orderlessfinancialdata_reason_for_testing_id_f5867705_fk_custom_models_reasonsfortesting_id]
GO
ALTER TABLE [dbo].[custom_models_orderlessfinancialdata]  WITH CHECK ADD  CONSTRAINT [custom_models_orderlessfinancialdata_uit_invite_id_5e98d5d1_fk_custom_models_uitinvites_id] FOREIGN KEY([uit_invite_id])
REFERENCES [dbo].[custom_models_uitinvites] ([id])
GO
ALTER TABLE [dbo].[custom_models_orderlessfinancialdata] CHECK CONSTRAINT [custom_models_orderlessfinancialdata_uit_invite_id_5e98d5d1_fk_custom_models_uitinvites_id]
GO
ALTER TABLE [dbo].[custom_models_pausetestactions]  WITH CHECK ADD  CONSTRAINT [custom_models_pausetestactions_ta_action_id_35e9bc1f_fk_custom_models_taactions_id] FOREIGN KEY([ta_action_id])
REFERENCES [dbo].[custom_models_taactions] ([id])
GO
ALTER TABLE [dbo].[custom_models_pausetestactions] CHECK CONSTRAINT [custom_models_pausetestactions_ta_action_id_35e9bc1f_fk_custom_models_taactions_id]
GO
ALTER TABLE [dbo].[custom_models_systemalert]  WITH CHECK ADD  CONSTRAINT [custom_models_systemalert_criticality_id_b6852b8d_fk_custom_models_criticality_id] FOREIGN KEY([criticality_id])
REFERENCES [dbo].[custom_models_criticality] ([id])
GO
ALTER TABLE [dbo].[custom_models_systemalert] CHECK CONSTRAINT [custom_models_systemalert_criticality_id_b6852b8d_fk_custom_models_criticality_id]
GO
ALTER TABLE [dbo].[custom_models_taactions]  WITH CHECK ADD  CONSTRAINT [custom_models_taactions_action_type_id_805b1ae2_fk_custom_models_taactiontypes_action_type] FOREIGN KEY([action_type_id])
REFERENCES [dbo].[custom_models_taactiontypes] ([action_type])
GO
ALTER TABLE [dbo].[custom_models_taactions] CHECK CONSTRAINT [custom_models_taactions_action_type_id_805b1ae2_fk_custom_models_taactiontypes_action_type]
GO
ALTER TABLE [dbo].[custom_models_taactions]  WITH CHECK ADD  CONSTRAINT [custom_models_taactions_assigned_test_id_c10f40b0_fk_custom_models_assignedtest_id] FOREIGN KEY([assigned_test_id])
REFERENCES [dbo].[custom_models_assignedtest] ([id])
GO
ALTER TABLE [dbo].[custom_models_taactions] CHECK CONSTRAINT [custom_models_taactions_assigned_test_id_c10f40b0_fk_custom_models_assignedtest_id]
GO
ALTER TABLE [dbo].[custom_models_taactions]  WITH CHECK ADD  CONSTRAINT [custom_models_taactions_test_section_id_10d7e562_fk_cms_models_testsection_id] FOREIGN KEY([test_section_id])
REFERENCES [dbo].[cms_models_testsection] ([id])
GO
ALTER TABLE [dbo].[custom_models_taactions] CHECK CONSTRAINT [custom_models_taactions_test_section_id_10d7e562_fk_cms_models_testsection_id]
GO
ALTER TABLE [dbo].[custom_models_testaccesscode]  WITH CHECK ADD  CONSTRAINT [custom_models_testaccesscode_orderless_financial_data_id_5bb96800_fk_custom_models_orderlessfinancialdata_id] FOREIGN KEY([orderless_financial_data_id])
REFERENCES [dbo].[custom_models_orderlessfinancialdata] ([id])
GO
ALTER TABLE [dbo].[custom_models_testaccesscode] CHECK CONSTRAINT [custom_models_testaccesscode_orderless_financial_data_id_5bb96800_fk_custom_models_orderlessfinancialdata_id]
GO
ALTER TABLE [dbo].[custom_models_testaccesscode]  WITH CHECK ADD  CONSTRAINT [custom_models_testaccesscode_ta_username_id_891d0506_fk_user_management_models_user_username] FOREIGN KEY([ta_username_id])
REFERENCES [dbo].[user_management_models_user] ([username])
GO
ALTER TABLE [dbo].[custom_models_testaccesscode] CHECK CONSTRAINT [custom_models_testaccesscode_ta_username_id_891d0506_fk_user_management_models_user_username]
GO
ALTER TABLE [dbo].[custom_models_testaccesscode]  WITH CHECK ADD  CONSTRAINT [custom_models_testaccesscode_test_id_72b65694_fk_cms_models_testdefinition_id] FOREIGN KEY([test_id])
REFERENCES [dbo].[cms_models_testdefinition] ([id])
GO
ALTER TABLE [dbo].[custom_models_testaccesscode] CHECK CONSTRAINT [custom_models_testaccesscode_test_id_72b65694_fk_cms_models_testdefinition_id]
GO
ALTER TABLE [dbo].[custom_models_testaccesscode]  WITH CHECK ADD  CONSTRAINT [custom_models_testaccesscode_test_session_language_id_a177b849_fk_custom_models_language_language_id] FOREIGN KEY([test_session_language_id])
REFERENCES [dbo].[custom_models_language] ([language_id])
GO
ALTER TABLE [dbo].[custom_models_testaccesscode] CHECK CONSTRAINT [custom_models_testaccesscode_test_session_language_id_a177b849_fk_custom_models_language_language_id]
GO
ALTER TABLE [dbo].[custom_models_testscorerassignment]  WITH CHECK ADD  CONSTRAINT [custom_models_testscorerassignment_assigned_test_id_47becc54_fk_custom_models_assignedtest_id] FOREIGN KEY([assigned_test_id])
REFERENCES [dbo].[custom_models_assignedtest] ([id])
GO
ALTER TABLE [dbo].[custom_models_testscorerassignment] CHECK CONSTRAINT [custom_models_testscorerassignment_assigned_test_id_47becc54_fk_custom_models_assignedtest_id]
GO
ALTER TABLE [dbo].[custom_models_testscorerassignment]  WITH CHECK ADD  CONSTRAINT [custom_models_testscorerassignment_scorer_username_id_c6e383dc_fk_user_management_models_user_username] FOREIGN KEY([scorer_username_id])
REFERENCES [dbo].[user_management_models_user] ([username])
GO
ALTER TABLE [dbo].[custom_models_testscorerassignment] CHECK CONSTRAINT [custom_models_testscorerassignment_scorer_username_id_c6e383dc_fk_user_management_models_user_username]
GO
ALTER TABLE [dbo].[custom_models_uitinviterelatedcandidates]  WITH CHECK ADD  CONSTRAINT [custom_models_uitinviterelatedcandidates_reason_for_deletion_id_8f7d89ce_fk_custom_models_uitreasonsfordeletion_id] FOREIGN KEY([reason_for_deletion_id])
REFERENCES [dbo].[custom_models_uitreasonsfordeletion] ([id])
GO
ALTER TABLE [dbo].[custom_models_uitinviterelatedcandidates] CHECK CONSTRAINT [custom_models_uitinviterelatedcandidates_reason_for_deletion_id_8f7d89ce_fk_custom_models_uitreasonsfordeletion_id]
GO
ALTER TABLE [dbo].[custom_models_uitinviterelatedcandidates]  WITH CHECK ADD  CONSTRAINT [custom_models_uitinviterelatedcandidates_uit_invite_id_74d00ec5_fk_custom_models_uitinvites_id] FOREIGN KEY([uit_invite_id])
REFERENCES [dbo].[custom_models_uitinvites] ([id])
GO
ALTER TABLE [dbo].[custom_models_uitinviterelatedcandidates] CHECK CONSTRAINT [custom_models_uitinviterelatedcandidates_uit_invite_id_74d00ec5_fk_custom_models_uitinvites_id]
GO
ALTER TABLE [dbo].[custom_models_uitinvites]  WITH CHECK ADD  CONSTRAINT [custom_models_uitinvites_reason_for_deletion_id_fb429165_fk_custom_models_uitreasonsfordeletion_id] FOREIGN KEY([reason_for_deletion_id])
REFERENCES [dbo].[custom_models_uitreasonsfordeletion] ([id])
GO
ALTER TABLE [dbo].[custom_models_uitinvites] CHECK CONSTRAINT [custom_models_uitinvites_reason_for_deletion_id_fb429165_fk_custom_models_uitreasonsfordeletion_id]
GO
ALTER TABLE [dbo].[custom_models_uitinvites]  WITH CHECK ADD  CONSTRAINT [custom_models_uitinvites_reason_for_modification_id_51e74101_fk_custom_models_uitreasonsformodification_id] FOREIGN KEY([reason_for_modification_id])
REFERENCES [dbo].[custom_models_uitreasonsformodification] ([id])
GO
ALTER TABLE [dbo].[custom_models_uitinvites] CHECK CONSTRAINT [custom_models_uitinvites_reason_for_modification_id_51e74101_fk_custom_models_uitreasonsformodification_id]
GO
ALTER TABLE [dbo].[custom_models_uitinvites]  WITH CHECK ADD  CONSTRAINT [custom_models_uitinvites_ta_test_permissions_id_723fe8ab_fk_cms_models_testpermissions_id] FOREIGN KEY([ta_test_permissions_id])
REFERENCES [dbo].[cms_models_testpermissions] ([id])
GO
ALTER TABLE [dbo].[custom_models_uitinvites] CHECK CONSTRAINT [custom_models_uitinvites_ta_test_permissions_id_723fe8ab_fk_cms_models_testpermissions_id]
GO
ALTER TABLE [dbo].[custom_models_uitinvites]  WITH CHECK ADD  CONSTRAINT [custom_models_uitinvites_ta_username_id_b55cfd0d_fk_user_management_models_user_username] FOREIGN KEY([ta_username_id])
REFERENCES [dbo].[user_management_models_user] ([username])
GO
ALTER TABLE [dbo].[custom_models_uitinvites] CHECK CONSTRAINT [custom_models_uitinvites_ta_username_id_b55cfd0d_fk_user_management_models_user_username]
GO
ALTER TABLE [dbo].[custom_models_unsupervisedtestaccesscode]  WITH CHECK ADD  CONSTRAINT [custom_models_unsupervisedtestaccesscode_accommodation_request_id_9c0dd526_fk_custom_models_accommodationrequest_id] FOREIGN KEY([accommodation_request_id])
REFERENCES [dbo].[custom_models_accommodationrequest] ([id])
GO
ALTER TABLE [dbo].[custom_models_unsupervisedtestaccesscode] CHECK CONSTRAINT [custom_models_unsupervisedtestaccesscode_accommodation_request_id_9c0dd526_fk_custom_models_accommodationrequest_id]
GO
ALTER TABLE [dbo].[custom_models_unsupervisedtestaccesscode]  WITH CHECK ADD  CONSTRAINT [custom_models_unsupervisedtestaccesscode_orderless_financial_data_id_25b01bf8_fk_custom_models_orderlessfinancialdata_id] FOREIGN KEY([orderless_financial_data_id])
REFERENCES [dbo].[custom_models_orderlessfinancialdata] ([id])
GO
ALTER TABLE [dbo].[custom_models_unsupervisedtestaccesscode] CHECK CONSTRAINT [custom_models_unsupervisedtestaccesscode_orderless_financial_data_id_25b01bf8_fk_custom_models_orderlessfinancialdata_id]
GO
ALTER TABLE [dbo].[custom_models_unsupervisedtestaccesscode]  WITH CHECK ADD  CONSTRAINT [custom_models_unsupervisedtestaccesscode_ta_username_id_18c15fae_fk_user_management_models_user_username] FOREIGN KEY([ta_username_id])
REFERENCES [dbo].[user_management_models_user] ([username])
GO
ALTER TABLE [dbo].[custom_models_unsupervisedtestaccesscode] CHECK CONSTRAINT [custom_models_unsupervisedtestaccesscode_ta_username_id_18c15fae_fk_user_management_models_user_username]
GO
ALTER TABLE [dbo].[custom_models_unsupervisedtestaccesscode]  WITH CHECK ADD  CONSTRAINT [custom_models_unsupervisedtestaccesscode_test_id_bb7066b0_fk_cms_models_testdefinition_id] FOREIGN KEY([test_id])
REFERENCES [dbo].[cms_models_testdefinition] ([id])
GO
ALTER TABLE [dbo].[custom_models_unsupervisedtestaccesscode] CHECK CONSTRAINT [custom_models_unsupervisedtestaccesscode_test_id_bb7066b0_fk_cms_models_testdefinition_id]
GO
ALTER TABLE [dbo].[custom_models_unsupervisedtestaccesscode]  WITH CHECK ADD  CONSTRAINT [custom_models_unsupervisedtestaccesscode_uit_invite_id_e8fb2831_fk_custom_models_uitinvites_id] FOREIGN KEY([uit_invite_id])
REFERENCES [dbo].[custom_models_uitinvites] ([id])
GO
ALTER TABLE [dbo].[custom_models_unsupervisedtestaccesscode] CHECK CONSTRAINT [custom_models_unsupervisedtestaccesscode_uit_invite_id_e8fb2831_fk_custom_models_uitinvites_id]
GO
ALTER TABLE [dbo].[django_admin_log]  WITH CHECK ADD  CONSTRAINT [django_admin_log_content_type_id_c4bce8eb_fk_django_content_type_id] FOREIGN KEY([content_type_id])
REFERENCES [dbo].[django_content_type] ([id])
GO
ALTER TABLE [dbo].[django_admin_log] CHECK CONSTRAINT [django_admin_log_content_type_id_c4bce8eb_fk_django_content_type_id]
GO
ALTER TABLE [dbo].[django_admin_log]  WITH CHECK ADD  CONSTRAINT [django_admin_log_user_id_c564eba6_fk_user_management_models_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[django_admin_log] CHECK CONSTRAINT [django_admin_log_user_id_c564eba6_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[django_celery_beat_periodictask]  WITH CHECK ADD  CONSTRAINT [django_celery_beat_periodictask_clocked_id_47a69f82_fk_django_celery_beat_clockedschedule_id] FOREIGN KEY([clocked_id])
REFERENCES [dbo].[django_celery_beat_clockedschedule] ([id])
GO
ALTER TABLE [dbo].[django_celery_beat_periodictask] CHECK CONSTRAINT [django_celery_beat_periodictask_clocked_id_47a69f82_fk_django_celery_beat_clockedschedule_id]
GO
ALTER TABLE [dbo].[django_celery_beat_periodictask]  WITH CHECK ADD  CONSTRAINT [django_celery_beat_periodictask_crontab_id_d3cba168_fk_django_celery_beat_crontabschedule_id] FOREIGN KEY([crontab_id])
REFERENCES [dbo].[django_celery_beat_crontabschedule] ([id])
GO
ALTER TABLE [dbo].[django_celery_beat_periodictask] CHECK CONSTRAINT [django_celery_beat_periodictask_crontab_id_d3cba168_fk_django_celery_beat_crontabschedule_id]
GO
ALTER TABLE [dbo].[django_celery_beat_periodictask]  WITH CHECK ADD  CONSTRAINT [django_celery_beat_periodictask_interval_id_a8ca27da_fk_django_celery_beat_intervalschedule_id] FOREIGN KEY([interval_id])
REFERENCES [dbo].[django_celery_beat_intervalschedule] ([id])
GO
ALTER TABLE [dbo].[django_celery_beat_periodictask] CHECK CONSTRAINT [django_celery_beat_periodictask_interval_id_a8ca27da_fk_django_celery_beat_intervalschedule_id]
GO
ALTER TABLE [dbo].[django_celery_beat_periodictask]  WITH CHECK ADD  CONSTRAINT [django_celery_beat_periodictask_solar_id_a87ce72c_fk_django_celery_beat_solarschedule_id] FOREIGN KEY([solar_id])
REFERENCES [dbo].[django_celery_beat_solarschedule] ([id])
GO
ALTER TABLE [dbo].[django_celery_beat_periodictask] CHECK CONSTRAINT [django_celery_beat_periodictask_solar_id_a87ce72c_fk_django_celery_beat_solarschedule_id]
GO
ALTER TABLE [dbo].[django_rest_passwordreset_resetpasswordtoken]  WITH CHECK ADD  CONSTRAINT [django_rest_passwordreset_resetpasswordtoken_user_id_e8015b11_fk_user_management_models_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[django_rest_passwordreset_resetpasswordtoken] CHECK CONSTRAINT [django_rest_passwordreset_resetpasswordtoken_user_id_e8015b11_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[ref_table_views_historicalcateducation]  WITH CHECK ADD  CONSTRAINT [ref_table_views_historicalcateducation_history_user_id_c293ef98_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[ref_table_views_historicalcateducation] CHECK CONSTRAINT [ref_table_views_historicalcateducation_history_user_id_c293ef98_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[ref_table_views_historicalcatrefaboriginalvw]  WITH CHECK ADD  CONSTRAINT [ref_table_views_historicalcataboriginalvw_history_user_id_8324f28f_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[ref_table_views_historicalcatrefaboriginalvw] CHECK CONSTRAINT [ref_table_views_historicalcataboriginalvw_history_user_id_8324f28f_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[ref_table_views_historicalcatrefclassificationvw]  WITH CHECK ADD  CONSTRAINT [ref_table_views_historicalcatrefclassificationvw_history_user_id_2483771d_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[ref_table_views_historicalcatrefclassificationvw] CHECK CONSTRAINT [ref_table_views_historicalcatrefclassificationvw_history_user_id_2483771d_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[ref_table_views_historicalcatrefdepartmentsvw]  WITH CHECK ADD  CONSTRAINT [ref_table_views_historicalcatrefdepartmentsvw_history_user_id_c317800b_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[ref_table_views_historicalcatrefdepartmentsvw] CHECK CONSTRAINT [ref_table_views_historicalcatrefdepartmentsvw_history_user_id_c317800b_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[ref_table_views_historicalcatrefdisabilityvw]  WITH CHECK ADD  CONSTRAINT [ref_table_views_historicalcatdisabilityvw_history_user_id_2d889408_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[ref_table_views_historicalcatrefdisabilityvw] CHECK CONSTRAINT [ref_table_views_historicalcatdisabilityvw_history_user_id_2d889408_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[ref_table_views_historicalcatrefemployerstsvw]  WITH CHECK ADD  CONSTRAINT [ref_table_views_historicalcatrefemployerstsvw_history_user_id_0a65cee4_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[ref_table_views_historicalcatrefemployerstsvw] CHECK CONSTRAINT [ref_table_views_historicalcatrefemployerstsvw_history_user_id_0a65cee4_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[ref_table_views_historicalcatrefgendervw]  WITH CHECK ADD  CONSTRAINT [ref_table_views_historicaloltfgendervw_history_user_id_74ac4795_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[ref_table_views_historicalcatrefgendervw] CHECK CONSTRAINT [ref_table_views_historicaloltfgendervw_history_user_id_74ac4795_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[ref_table_views_historicalcatrefprovincevw]  WITH CHECK ADD  CONSTRAINT [ref_table_views_historicalcatrefprovincevw_history_user_id_aee32566_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[ref_table_views_historicalcatrefprovincevw] CHECK CONSTRAINT [ref_table_views_historicalcatrefprovincevw_history_user_id_aee32566_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[ref_table_views_historicalcatrefvisibleminorityvw]  WITH CHECK ADD  CONSTRAINT [ref_table_views_historicalcatvisibleminorityvw_history_user_id_9012a568_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[ref_table_views_historicalcatrefvisibleminorityvw] CHECK CONSTRAINT [ref_table_views_historicalcatvisibleminorityvw_history_user_id_9012a568_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_accommodations]  WITH CHECK ADD  CONSTRAINT [user_management_models_accommodations_user_id_5af49f7b_fk_user_management_models_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_accommodations] CHECK CONSTRAINT [user_management_models_accommodations_user_id_5af49f7b_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_custompermissions]  WITH CHECK ADD  CONSTRAINT [user_management_models_custompermissions_content_type_id_e81880f7_fk_django_content_type_id] FOREIGN KEY([content_type_id])
REFERENCES [dbo].[django_content_type] ([id])
GO
ALTER TABLE [dbo].[user_management_models_custompermissions] CHECK CONSTRAINT [user_management_models_custompermissions_content_type_id_e81880f7_fk_django_content_type_id]
GO
ALTER TABLE [dbo].[user_management_models_customuserpermissions]  WITH CHECK ADD  CONSTRAINT [user_management_models_customuserpermissions_permission_id_e289ff04_fk_user_management_models_custompermissions_permission_id] FOREIGN KEY([permission_id])
REFERENCES [dbo].[user_management_models_custompermissions] ([permission_id])
GO
ALTER TABLE [dbo].[user_management_models_customuserpermissions] CHECK CONSTRAINT [user_management_models_customuserpermissions_permission_id_e289ff04_fk_user_management_models_custompermissions_permission_id]
GO
ALTER TABLE [dbo].[user_management_models_customuserpermissions]  WITH CHECK ADD  CONSTRAINT [user_management_models_customuserpermissions_user_id_9e1b2b30_fk_user_management_models_user_username] FOREIGN KEY([user_id])
REFERENCES [dbo].[user_management_models_user] ([username])
GO
ALTER TABLE [dbo].[user_management_models_customuserpermissions] CHECK CONSTRAINT [user_management_models_customuserpermissions_user_id_9e1b2b30_fk_user_management_models_user_username]
GO
ALTER TABLE [dbo].[user_management_models_historicalaccommodations]  WITH CHECK ADD  CONSTRAINT [user_management_models_historicalaccommodations_history_user_id_c359668c_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_historicalaccommodations] CHECK CONSTRAINT [user_management_models_historicalaccommodations_history_user_id_c359668c_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_historicalcustompermissions]  WITH CHECK ADD  CONSTRAINT [user_management_models_historicalcustompermissions_history_user_id_86d03e32_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_historicalcustompermissions] CHECK CONSTRAINT [user_management_models_historicalcustompermissions_history_user_id_86d03e32_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_historicalcustomuserpermissions]  WITH CHECK ADD  CONSTRAINT [user_management_models_historicalcustomuserpermissions_history_user_id_8b8967d6_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_historicalcustomuserpermissions] CHECK CONSTRAINT [user_management_models_historicalcustomuserpermissions_history_user_id_8b8967d6_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_historicaleeinfooptions]  WITH CHECK ADD  CONSTRAINT [user_management_models_historicaleeinfooptions_history_user_id_6c433a59_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_historicaleeinfooptions] CHECK CONSTRAINT [user_management_models_historicaleeinfooptions_history_user_id_6c433a59_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_historicalpermissionrequest]  WITH CHECK ADD  CONSTRAINT [user_management_models_historicalpermissionrequest_history_user_id_78f13c90_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_historicalpermissionrequest] CHECK CONSTRAINT [user_management_models_historicalpermissionrequest_history_user_id_78f13c90_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_historicaltaextendedprofile]  WITH CHECK ADD  CONSTRAINT [user_management_models_historicaltaextendedprofile_history_user_id_e7baf289_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_historicaltaextendedprofile] CHECK CONSTRAINT [user_management_models_historicaltaextendedprofile_history_user_id_e7baf289_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_historicaluser]  WITH CHECK ADD  CONSTRAINT [user_management_models_historicaluser_history_user_id_470afeb5_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_historicaluser] CHECK CONSTRAINT [user_management_models_historicaluser_history_user_id_470afeb5_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_historicaluserextendedprofile]  WITH CHECK ADD  CONSTRAINT [user_management_models_historicaluserextendedprofile_history_user_id_3793f527_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_historicaluserextendedprofile] CHECK CONSTRAINT [user_management_models_historicaluserextendedprofile_history_user_id_3793f527_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_historicaluserextendedprofileaboriginal]  WITH CHECK ADD  CONSTRAINT [user_management_models_historicaluserextendedprofileaboriginal_history_user_id_e07411e0_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_historicaluserextendedprofileaboriginal] CHECK CONSTRAINT [user_management_models_historicaluserextendedprofileaboriginal_history_user_id_e07411e0_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_historicaluserextendedprofiledisability]  WITH CHECK ADD  CONSTRAINT [user_management_models_historicaluserextendedprofiledisability_history_user_id_77c65c1b_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_historicaluserextendedprofiledisability] CHECK CONSTRAINT [user_management_models_historicaluserextendedprofiledisability_history_user_id_77c65c1b_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_historicaluserextendedprofilevisibleminority]  WITH CHECK ADD  CONSTRAINT [user_management_models_historicaluserextendedprofilevisibleminority_history_user_id_36d81667_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_historicaluserextendedprofilevisibleminority] CHECK CONSTRAINT [user_management_models_historicaluserextendedprofilevisibleminority_history_user_id_36d81667_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_historicaluserpasswordresettracking]  WITH CHECK ADD  CONSTRAINT [user_management_models_historicaluserpasswordresettracking_history_user_id_ed693536_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_historicaluserpasswordresettracking] CHECK CONSTRAINT [user_management_models_historicaluserpasswordresettracking_history_user_id_ed693536_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_historicaluserprofilechangerequest]  WITH CHECK ADD  CONSTRAINT [user_management_models_historicaluserprofilechangerequest_history_user_id_22f23c73_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_historicaluserprofilechangerequest] CHECK CONSTRAINT [user_management_models_historicaluserprofilechangerequest_history_user_id_22f23c73_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_permissionrequest]  WITH CHECK ADD  CONSTRAINT [user_management_models_permissionrequest_permission_requested_id_d064f657_fk_user_management_models_customp] FOREIGN KEY([permission_requested_id])
REFERENCES [dbo].[user_management_models_custompermissions] ([permission_id])
GO
ALTER TABLE [dbo].[user_management_models_permissionrequest] CHECK CONSTRAINT [user_management_models_permissionrequest_permission_requested_id_d064f657_fk_user_management_models_customp]
GO
ALTER TABLE [dbo].[user_management_models_permissionrequest]  WITH CHECK ADD  CONSTRAINT [user_management_models_permissionrequest_username_id_f98a2856_fk_user_management_models_user_username] FOREIGN KEY([username_id])
REFERENCES [dbo].[user_management_models_user] ([username])
GO
ALTER TABLE [dbo].[user_management_models_permissionrequest] CHECK CONSTRAINT [user_management_models_permissionrequest_username_id_f98a2856_fk_user_management_models_user_username]
GO
ALTER TABLE [dbo].[user_management_models_taextendedprofile]  WITH CHECK ADD  CONSTRAINT [user_management_models_taextendedprofile_username_id_79a6daee_fk_user_management_models_user_username] FOREIGN KEY([username_id])
REFERENCES [dbo].[user_management_models_user] ([username])
GO
ALTER TABLE [dbo].[user_management_models_taextendedprofile] CHECK CONSTRAINT [user_management_models_taextendedprofile_username_id_79a6daee_fk_user_management_models_user_username]
GO
ALTER TABLE [dbo].[user_management_models_user_groups]  WITH CHECK ADD  CONSTRAINT [user_management_models_user_groups_group_id_315780fa_fk_auth_group_id] FOREIGN KEY([group_id])
REFERENCES [dbo].[auth_group] ([id])
GO
ALTER TABLE [dbo].[user_management_models_user_groups] CHECK CONSTRAINT [user_management_models_user_groups_group_id_315780fa_fk_auth_group_id]
GO
ALTER TABLE [dbo].[user_management_models_user_groups]  WITH CHECK ADD  CONSTRAINT [user_management_models_user_groups_user_id_87f76697_fk_user_management_models_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_user_groups] CHECK CONSTRAINT [user_management_models_user_groups_user_id_87f76697_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_user_user_permissions]  WITH CHECK ADD  CONSTRAINT [user_management_models_user_user_permissions_permission_id_c6b7b475_fk_auth_permission_id] FOREIGN KEY([permission_id])
REFERENCES [dbo].[auth_permission] ([id])
GO
ALTER TABLE [dbo].[user_management_models_user_user_permissions] CHECK CONSTRAINT [user_management_models_user_user_permissions_permission_id_c6b7b475_fk_auth_permission_id]
GO
ALTER TABLE [dbo].[user_management_models_user_user_permissions]  WITH CHECK ADD  CONSTRAINT [user_management_models_user_user_permissions_user_id_663216bc_fk_user_management_models_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_user_user_permissions] CHECK CONSTRAINT [user_management_models_user_user_permissions_user_id_663216bc_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_userextendedprofile]  WITH CHECK ADD  CONSTRAINT [user_management_models_userextendedprofile_aboriginal_id_2706213d_fk_user_management_models_eeinfooptions_opt_id] FOREIGN KEY([aboriginal_id])
REFERENCES [dbo].[user_management_models_eeinfooptions] ([opt_id])
GO
ALTER TABLE [dbo].[user_management_models_userextendedprofile] CHECK CONSTRAINT [user_management_models_userextendedprofile_aboriginal_id_2706213d_fk_user_management_models_eeinfooptions_opt_id]
GO
ALTER TABLE [dbo].[user_management_models_userextendedprofile]  WITH CHECK ADD  CONSTRAINT [user_management_models_userextendedprofile_disability_id_34b76084_fk_user_management_models_eeinfooptions_opt_id] FOREIGN KEY([disability_id])
REFERENCES [dbo].[user_management_models_eeinfooptions] ([opt_id])
GO
ALTER TABLE [dbo].[user_management_models_userextendedprofile] CHECK CONSTRAINT [user_management_models_userextendedprofile_disability_id_34b76084_fk_user_management_models_eeinfooptions_opt_id]
GO
ALTER TABLE [dbo].[user_management_models_userextendedprofile]  WITH CHECK ADD  CONSTRAINT [user_management_models_userextendedprofile_identify_as_woman_id_4a2e3adf_fk_user_management_models_eeinfooptions_opt_id] FOREIGN KEY([identify_as_woman_id])
REFERENCES [dbo].[user_management_models_eeinfooptions] ([opt_id])
GO
ALTER TABLE [dbo].[user_management_models_userextendedprofile] CHECK CONSTRAINT [user_management_models_userextendedprofile_identify_as_woman_id_4a2e3adf_fk_user_management_models_eeinfooptions_opt_id]
GO
ALTER TABLE [dbo].[user_management_models_userextendedprofile]  WITH CHECK ADD  CONSTRAINT [user_management_models_userextendedprofile_user_id_01a1740c_fk_user_management_models_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_userextendedprofile] CHECK CONSTRAINT [user_management_models_userextendedprofile_user_id_01a1740c_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_userextendedprofile]  WITH CHECK ADD  CONSTRAINT [user_management_models_userextendedprofile_visible_minority_id_67436112_fk_user_management_models_eeinfooptions_opt_id] FOREIGN KEY([visible_minority_id])
REFERENCES [dbo].[user_management_models_eeinfooptions] ([opt_id])
GO
ALTER TABLE [dbo].[user_management_models_userextendedprofile] CHECK CONSTRAINT [user_management_models_userextendedprofile_visible_minority_id_67436112_fk_user_management_models_eeinfooptions_opt_id]
GO
ALTER TABLE [dbo].[user_management_models_userextendedprofileaboriginal]  WITH CHECK ADD  CONSTRAINT [user_management_models_userextendedprofile_user_extended_profile_id_b182177f_fk_user_management_models_userext] FOREIGN KEY([user_extended_profile_id])
REFERENCES [dbo].[user_management_models_userextendedprofile] ([id])
GO
ALTER TABLE [dbo].[user_management_models_userextendedprofileaboriginal] CHECK CONSTRAINT [user_management_models_userextendedprofile_user_extended_profile_id_b182177f_fk_user_management_models_userext]
GO
ALTER TABLE [dbo].[user_management_models_userextendedprofiledisability]  WITH CHECK ADD  CONSTRAINT [user_management_models_userextendedprofile_user_extended_profile_id_602f0aa4_fk_user_management_models_userext] FOREIGN KEY([user_extended_profile_id])
REFERENCES [dbo].[user_management_models_userextendedprofile] ([id])
GO
ALTER TABLE [dbo].[user_management_models_userextendedprofiledisability] CHECK CONSTRAINT [user_management_models_userextendedprofile_user_extended_profile_id_602f0aa4_fk_user_management_models_userext]
GO
ALTER TABLE [dbo].[user_management_models_userextendedprofilevisibleminority]  WITH CHECK ADD  CONSTRAINT [user_management_models_userextendedprofile_user_extended_profile_id_631fa36c_fk_user_management_models_userext] FOREIGN KEY([user_extended_profile_id])
REFERENCES [dbo].[user_management_models_userextendedprofile] ([id])
GO
ALTER TABLE [dbo].[user_management_models_userextendedprofilevisibleminority] CHECK CONSTRAINT [user_management_models_userextendedprofile_user_extended_profile_id_631fa36c_fk_user_management_models_userext]
GO
ALTER TABLE [dbo].[user_management_models_userpasswordresettracking]  WITH CHECK ADD  CONSTRAINT [user_management_models_userpasswordresettracking_user_id_5fa82965_fk_user_management_models_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_userpasswordresettracking] CHECK CONSTRAINT [user_management_models_userpasswordresettracking_user_id_5fa82965_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_userprofilechangerequest]  WITH CHECK ADD  CONSTRAINT [user_management_models_userprofilechangerequest_user_id_782279f4_fk_user_management_models_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_userprofilechangerequest] CHECK CONSTRAINT [user_management_models_userprofilechangerequest_user_id_782279f4_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_bundles]  WITH CHECK ADD  CONSTRAINT [shuffle_bundles_and_shuffle_between_bundles_are_mutually_exclusive] CHECK  (([shuffle_between_bundles]=(0) AND [shuffle_bundles]=(1) OR [shuffle_between_bundles]=(1) AND [shuffle_bundles]=(0) OR [shuffle_between_bundles]=(0) AND [shuffle_bundles]=(0)))
GO
ALTER TABLE [dbo].[cms_models_bundles] CHECK CONSTRAINT [shuffle_bundles_and_shuffle_between_bundles_are_mutually_exclusive]
GO
ALTER TABLE [dbo].[django_admin_log]  WITH CHECK ADD  CONSTRAINT [django_admin_log_action_flag_a8637d59_check] CHECK  (([action_flag]>=(0)))
GO
ALTER TABLE [dbo].[django_admin_log] CHECK CONSTRAINT [django_admin_log_action_flag_a8637d59_check]
GO
ALTER TABLE [dbo].[django_celery_beat_periodictask]  WITH CHECK ADD CHECK  (([expire_seconds]>=(0)))
GO
ALTER TABLE [dbo].[django_celery_beat_periodictask]  WITH CHECK ADD CHECK  (([priority]>=(0)))
GO
ALTER TABLE [dbo].[django_celery_beat_periodictask]  WITH CHECK ADD  CONSTRAINT [django_celery_beat_periodictask_total_run_count_cf45f5ae_check] CHECK  (([total_run_count]>=(0)))
GO
ALTER TABLE [dbo].[django_celery_beat_periodictask] CHECK CONSTRAINT [django_celery_beat_periodictask_total_run_count_cf45f5ae_check]
GO
