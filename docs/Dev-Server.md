# [THIS ENTIRE PAGE IS NOT UP TO DATE DUE TO MSSQL CONVERSION - TO BE COMPLETED/UPDATED]

# Accessing the Dev Server

In order to access the dev server you will need to do the following:

- remote desktop into the azure jump server (because port 443 is used by the application, we can't ssh directly into the containers)
- ssh(PuTTy) from the jump server into the docker machine.

the passwords and IP addresses can be found in KeePass

# Accessing the Dev DB

Do all the steps listed above, then, inside putty:

- sudo docker exec -it project-thundercat_cat-dev-db_1 bash
- psql postgres postgres
- run commands in the DB as usual

Alternatively, if you are trying to run a script:

NOTE: This is not currently viable on dev; see "Uploading scripts to run on dev db"

- sudo docker exec -it project-thundercat_cat-dev-db_1 bash
- psql -U postgres -d postgres -a -f <filename>

# Uploading scripts to run on dev db

- Note, this is not easy to do with the current set up.
- This process will become much easier when dev is kubernetes based
- Will revist after the change over
