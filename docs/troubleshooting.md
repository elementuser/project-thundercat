# Known issues and troubleshooting resolution techniques

---

# General Docker issues

## Docker and the command line

- Cannot use VM on windows 10 and Docker at the same time
- Cannot VPN into network and use Docker (nginx crashes) when working remotely
  - This seems to be related to the network drives?
  - If docker is running and then I vpn into the network, the computer detects no network drives
  - After running 'docker compose down' and restating docker, the network drives appear
- Cannot execute 'docker exec' command in git bash
- If you are using DockerToolbox rather than Docker for Windows on Windows, see rm-docker-toolbox-setup

## Docker doesn't want to succeed for some reason

If you have run Docker pretty extensively in the past, try running `docker image prune -a`, `docker volume prune`, and `docker system prune`. Doing so will remove any images & force them to be obtained & rebuilt from scratch.

## Docker says some containers are already running

You are attempting to run a container that is already running, or the previous Docker containers did not stop properly. To stop all Docker containers, run this command in the terminal:

`docker stop $(docker ps -a -q)`

## Cannot start service nginx

On Windows 10, sometimes docker shows the following errors when starting up

```shell
$ docker compose up
Creating network "project-thundercat_default" with the default driver
Creating project-thundercat-frontend-1 ... done
Creating project-thundercat_msSQL       ... done
Creating project-thundercat-backend-1  ... done
Creating project-thundercat-nginx-1    ... error

ERROR: for project-thundercat-nginx-1  Cannot start service nginx: OCI runtime create failed: container_linux.go:344: starting container process caused "process_linux.go:424: container init caused \"rootfs_linux.go:58: mounting \\\"/host_mnt/c/_DEV/git/project-thundercat/nginx/nginx-proxy.conf\\\" to rootfs \\\"/var/lib/docker/overlay2/393d58faaa3c6a244293fcff1a14b5bb93f9d2aec735e29346454824d30556c3/merged\\\" at \\\"/var/lib/docker/overlay2/393d58faaa3c6a244293fcff1a14b5bb93f9d2aec735e29346454824d30556c3/merged/etc/nginx/conf.d/default.conf\\\" caused \\\"not a directory\\\"\"": unknown: Are you trying to mount a directory onto a file (or vice-versa)? Check if the specified host path exists and is the expected type

ERROR: for nginx  Cannot start service nginx: OCI runtime create failed: container_linux.go:344: starting container process caused "process_linux.go:424: container init caused \"rootfs_linux.go:58: mounting \\\"/host_mnt/c/_DEV/git/project-thundercat/nginx/nginx-proxy.conf\\\" to rootfs \\\"/var/lib/docker/overlay2/393d58faaa3c6a244293fcff1a14b5bb93f9d2aec735e29346454824d30556c3/merged\\\" at \\\"/var/lib/docker/overlay2/393d58faaa3c6a244293fcff1a14b5bb93f9d2aec735e29346454824d30556c3/merged/etc/nginx/conf.d/default.conf\\\" caused \\\"not a directory\\\"\"": unknown: Are you trying to mount a directory onto a file (or vice-versa)? Check if the specified host path exists and is the expected type
Encountered errors while bringing up the project.
```

To solve this, do the following steps

Ensure the application is not running

```shell
docker compose down
```

Run PowerShell as admin.

Run

```shell
Set-NetConnectionProfile -interfacealias "vEthernet (DockerNAT)" -NetworkCategory Private
```

Close powershell

Restart docker (right-click on the docker icon -> restart)

After the restart, run

```shell
docker compose up
```

And everything should be working again

## Drive sharing errors

if you get a similar error to the following:
`ERROR: for project-thundercat-backend-1 Cannot create container for service backend: b'Drive sharing seems blocked by a firewall'`
Then the following steps can be performed to remedy the situation.

1. Go to the network adapters
2. Go to DockerNAT adapter properties
3. Uninstall File and Printer Sharing
4. Install File and Printer Sharing under Microsoft
5. Run the the "error 1" instructions above

All errors should be resolved when using docker compose up

## [emerg] 1#1: host not found in upstream "backend:8000" in /etc/nginx/conf.d/default.conf:3

After making changes to the docker config, nginx will not start properly

To fix this, run

```shell
docker compose up --build
```

rather than

```shell
docker compose up
```

## Changes from DockerFile need to be applied

If for some reason changes from a DockerFile need to be applied (e.g. transitioning from an older version of CAT that used PostgreSQL and multiple changes to the DockerFile were made to use MS SQL instead), then you have to run this command to rerun the logic in the DockerFile:

```sh
docker compose build
```

This rebuilds the Docker containers from scratch.

Example of an error encountered that indicates that a rebuild for the Docker containers may need to be done:

![transition-from-old-thundercat-build-error](images/transition-from-old-thundercat-build-error.png)

## Docker containers are running but unable to access the application locally

If you were able to successfully run all the docker containers but are getting a 'Localhost Refused to Connect' error when accessing the site through http://localhost:81/oec-cat then you need might be running an older version of Docker Desktop that needs to be updated, run the following commands before updating docker desktop:

```shell
docker compose down
```

followed by `docker image prune -a`, `docker volume prune`, and `docker system prune`. Doing so will remove any cached images and volumes. Also delete the `C:\MSSQL` folder.

Exit the runing version of docker desktop and uninstall it. After it's been completed uninstalling, restart your machine and then download and install the newest version.

After the newest version has been sucessfully installed run the following command:

```shell
docker compose up --build
```

## File sharing permissions

If Docker requests file sharing permissions, accept (otherwise, the CAT stack will not work):

![docker-desktop-file-sharing-permissions](images/docker-desktop-file-sharing-permissions.png)

For Docker Desktop v4.14+, bypass manual file-sharing approval via the following Docker resource configuration:

![docker-file-sharing-directories-config](images/docker-file-sharing-directories-config.png)

# Backend & Database Containers

## Unable to get MS SQL related files in a volume

Delete the `C:\MSSQL` folder and then try doing docker compose up again.

Example of an error of this nature:

![mssql-loading-error](images/mssql-loading-error.png)

## Backend cannot connect to the Database Docker container (invalid login credentials)

Simply either wait a little longer or `Ctrl+C` to gracefully shutdown the other Docker containers & then do `docker compose up` to restart the Docker compose process. The issue here is that the Database container may take a longer time to finish loading than the Backend container, so the Backend container will automatically try again after some time and will be able to connect to the Database container properly. Otherwise, this error may be caused by WSL2 - see [Error connecting to MS SQL container ](#error-connecting-to-MS-SQL-container) for more info.

Example screenshots of this error:

![docker-invalid-db-credentials-error](images/docker-invalid-db-credentials-error.png)

![docker-invalid-db-credentials-error-2](images/docker-invalid-db-credentials-error-2.png)

## MS SQL: out of memory error for running long SQL scripts

If you encounter this error:

`There is insufficient system memory in resource pool 'default' to run this query.`

Go to Docker -> Settings -> Advanced and make sure "Memory" is set to a higher value (recommended minimum: 4096 MB).

## Error connecting to MS SQL container

If other Docker containers encounter errors connecting to the MS SQL container or via the SQL Server VSCode extension like so:

![docker-sql-connect-error](images/sql-connect-error.png)

![sql-server-profile-vscode-error](images/sql-server-profile-vscode-error.png)

Ensure that Docker is using Hyper-V to manage its containers (and not WSL2 - there is an incompatibility being caused by WSL2) by ensuring that the WSL2 option is unticked:

![docker-hyper-v-setting](images/hyper-v-setting.png)

After this issue is resolved, you should also be able to properly connect to the SQL Server via the SQL Server VSCode extension.

## Django Assigns Null id on inserts (localhost only)

pyodbc.IntegrityError: ('23000', "[23000] [Microsoft][odbc driver 17 for sql server][SQL Server]Cannot insert the value NULL into column 'id', table 'master.dbo.django_rest_passwordreset_resetpasswordtoken'; column does not allow nulls. INSERT fails. (515) (SQLExecDirectW)")

To resolve:

1. Open Microsoft SQL Server Management Studio and connect to your local database;
2. Right click the offending table -> "Script Table As" -> "DROP and CREATE To" -> "New Query Editor Window";
3. Replace the offending column definition in the CREATE TABLE statement (in this case 'id') with the following (minus quotation marks):
   "[id] int IDENTITY(1,1) NOT NULL"

# Frontend

## Every single yarn test is failing

Run `yarn test --clearCache` and then run `yarn test` once again.

## Frontend container fails to build: react-scripts not found

If you encounter this error:

```shell
frontend_1  | /bin/sh: 1: react-scripts: not found
```

Try deleting the node_modules, .yarn/cache and .yarn/unplugged folders and making sure your `yarn.lock` file contents do not differ from the repo's.

Afterwards, run `docker compose build` in your project, and then run `docker compose up` afterwards.

## Frontend container aborts while running frontend tests (npm test):

ERROR: Windows named pipe error: The pipe has been ended. (code: 109)

Docker requires more memory: open Docker Desktop -> Settings -> Resources -> Advanced -> increase "memory" to 6GB -> "Apply & restart"

## Frontend: you should not use <Link /> outside of <Router />

If you encounter the error shown here:

![link-outside-error-error](./images/link-outside-router-error.png)

Either one of 2 things are likely happening:

- You referenced a wrong localization string key in `frontend/src/resources/textResources.js` (in this case, please double-check the string key you are referencing)
- Redux is causing some kind of an infinite loop based on how the React components are set up (in this case, please review the [official React Component Lifecycle document](https://reactjs.org/docs/react-component.html) - it is likely you need to use `componentDidUpdate()` and/or `componentDidMount()`)

## Frontend: slow response times from rendering large lists

With the help of the Google Performance Profiler, it was determined that the `react-axe` dev dependency is causing a massive slowdown in our application when rendering large lists in the DOM.

- To avoid this error, the .env file now contains the following environment variable: `REACT_APP_ENABLE_REACT_AXE=false`
  - Note that all environment variables read into `process.env` (to be detected by the web app) must be prefixed with `REACT_APP_`.
  - Also note that `NODE_ENV` **cannot be overridden manually** (intentional by the React developers). This will automatically update depending on the command used to build & run the application.
  - See the official React docs for more details: https://create-react-app.dev/docs/adding-custom-environment-variables

---

# Archives (resolved issues)

### Port 80 error

```shell
PS C:\_DEV\IdeaProjects\thundercat\project-thundercat> docker compose up
Creating network "project-thundercat_default" with the default driver
Creating project-thundercat_msSQL   ... done
Creating project-thundercat-frontend-1 ... done
Creating project-thundercat-backend-1  ... done
Creating project-thundercat-nginx-1    ... error

ERROR: for project-thundercat-nginx-1  Cannot start service nginx: driver failed programming external connectivity on endpoint project-thundercat-nginx-1 (31bebcca94c04c380b7ad1af1bc492488d3bf3fb5b19db9356592e0b5a7028ed): Error starting userland proxy: Bind for 0.0.0.0:80: unexpected error Permission denied

ERROR: for nginx  Cannot start service nginx: driver failed programming external connectivity on endpoint project-thundercat-nginx-1 (31bebcca94c04c380b7ad1af1bc492488d3bf3fb5b19db9356592e0b5a7028ed): Error starting userland proxy: Bind for 0.0.0.0:80: unexpected error Permission denied
ERROR: Encountered errors while bringing up the project.
```

To fix this issue, you simply need change the nginx port in _docker-compose.yml_ file. You can use ports: 81:8080. If you do that, make sure that you are not committing these changes to master.

And everything should be working again

**Note that this is a temporary fix and we need to discuss it later.**

## Fixing issues related to line terminator characters '...\r'

To permanently fix this issue, if adding .gitattributes alone does not fix the issue in the repo, please run some of the git commands here (it should resolve the issue after fresh cloning the repo after trying some of these git commands): https://stackoverflow.com/questions/2517190/how-do-i-force-git-to-use-lf-instead-of-crlf-under-windows

We have seen issues with how new lines are saved in different environemnts depending on where they were created. This _should_ be fixed with the `.vscode/settings.json` file and the `.gitattributes` file, but this will be kept in the document for archive purposes.

Open '.gitattributes'

Add '<file_pattern> -crlf'

Commit '.gitattributes'

Open file in question in notepad++

Open Find+Replace dialog

Replace '\r' with ''. Make sure that 'Search Mode' is in 'Extended mode'

i.e.

' /usr/bin/env: ‘python\r’: No such file or directory' in error message

Open '.gitattributes'

Add '\*.py -crlf'

Commit, replace in notepad++

## project-thundercat_msSQL cannot start service db

ERROR: for project-thundercat_msSQL Cannot start service db: driver failed programming external connectivity on endpoint project-thundercat_msSQL (4647e822d6e7d0d74b234698002d69c27f9054e1e59d47dba72a3510797ce3d8): Bind for 0.0.0.0:1433 failed: port is already allocated

To resolve this issue, simply restart docker desktop, then execute "docker compose up --build"

## Celery is throwing errors and MsSQL crashes

(Moved this to the Archives section due to the primary cause being incompatibility with WSL2 - this is not needed if Docker is running with Hyper-V to manage its containers)

```
project-thundercat_msSQL | ** ERROR: [AppLoader] Failed to load LSA: 0xc0070102
project-thundercat_msSQL | AppLoader: Exiting with status=0xc0070102
project-thundercat_msSQL | This program has encountered a fatal error and cannot continue running at Mon Oct 18 09:38:58 2021
project-thundercat_msSQL | The following diagnostic information is available:
project-thundercat_msSQL |
project-thundercat_msSQL | Reason: 0x00000006
project-thundercat_msSQL | Message: Termination of \SystemRoot\system32\AppLoader.exe was due to fatal error 0xC0000001
project-thundercat_msSQL | Address: 0x3fff87a53094
project-thundercat_msSQL | Stack Trace:
```

and

```
project-thundercat-celery-beat-1 | [2021-10-13 07:45:10,872: ERROR/MainProcess] beat: Connection error: Error -3 connecting to project-thundercat-redis-1:6379. Temporary failure in name resolution.. Trying again in 6.0 seconds...
project-thundercat-celery-1 | [2021-10-13 07:45:13,910: ERROR/MainProcess] consumer: Cannot connect to redis://project-thundercat-redis-1:6379/0: Error -3 connecting to project-thundercat-redis-1:6379. Temporary failure in name resolution..
```

This appears to be triggered by newer versions of Docker which has WSL 2 enabled in the backend.

Docker version [Docker Community Edition 18.06.1-ce-win73 2018-08-29](https://docs.docker.com/desktop/windows/release-notes/archive/#docker-community-edition-18061-ce-win73-2018-08-29) is confirmed to work with our current build.

To fix:

Shutdown and prune docker

```shell
docker compose down
docker system prune
```

Delete all containers. Delete frontent/node_modules, frontend/.yarn/cache, and frontend/.yarn/unplugged

Unistall your current docker

Install the version linked above.

## Celery is throwing errors related to 'Name or Service not Known'

```
project-thundercat-celery-beat-1  | [2023-02-16 13:33:55,202: ERROR/MainProcess] beat: Connection error: Error -2 connecting to project-thundercat-redis-1:6379. Name or service not known.. Trying again in 32.0 seconds...
project-thundercat-celery-1       | [2023-02-16 13:34:22,398: ERROR/MainProcess] consumer: Cannot connect to redis://project-thundercat-redis-1:6379/0: Error -2 connecting to project-thundercat-redis-1:6379. Name or service not known..
project-thundercat-celery-1       | Trying again in 32.00 seconds... (16/100)
project-thundercat-celery-1
```

To fix:

- Open Docker for Desktop and go to: Settings --> General
- Uncheck the option 'Use Docker Compose V2'

This option (when checked) makes Docker generate container names with dashes instead of underscores. That being said, when Celery is starting and trying to connect to the redis container, the name or service is not found.

Shutdown and Restart:

```shell
docker compose down
docker compose up
```
