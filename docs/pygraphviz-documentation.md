# PyGraphViz

## Description

<br />

- Detects all the models that are used by the Django project

- Create a visual representation of the tables with their references

<br />

---

<br />

## Requirements

<br />

- Python (version 3.7, 3.8, or 3.9)

- C/C++ Compiler

  - https://visualstudio.microsoft.com/downloads

### Provided when executing the runner [run-pygraphviz.sh](../../run-pygraphviz.sh) with Git Bash:

- django-extensions

- GraphViz

- PyGraphViz

<br />

---

<br />

## Command Used In This Project

<br />

### CREATE DATABASE DIAGRAM - ALL TABLES

```Shell
python manage.py graph_models -a -o database-diagrams/database-diagram-full.png
```

<br />

### CREATE DATABASE DIAGRAM - WITHOUT HISTORICAL TABLES

```Shell
python manage.py graph_models -a -X Historical\* -o database-diagrams/database-diagram-without-historical.png
```

<br />

### CREATE DATABASE DIAGRAM - ONLY HISTORICAL TABLES

```Shell
python manage.py graph_models -a -I Historical\* -o database-diagrams/database-diagram-only-historical.png
```

<br />

---

<br />

## References

<br />

- [Django-Extensions](https://django-extensions.readthedocs.io/en/latest/index.html)

- [GraphViz](https://graphviz.org/)

- PyGraphViz:
  - [How To Install](https://pygraphviz.github.io/documentation/stable/install.html)
  - [How To Use](https://django-extensions.readthedocs.io/en/latest/graph_models.html?highlight=graphviz#graph-models)
