## New Hire Training

## Overview

During this training you will be implementing an existing feature that has been removed from the application. In this document there are a series of challenges presented and you will have to implement solutions for the problems. There is a solution already implemented but it is not the only solution. We want to see your creativity and problem solving skills.

The CAT team is here to validate your solution and help you, but try to solve the challenge on your own (no pair programming) as best as you can.

**This exercise is designed to teach you:**

- How to design features on our stack
- How our approval process works
- How we use JIRA
- How we create tests
- How we write documentation

**What this training does not do:**

- Teach you how to use/program in React, Python, Javascript, or SQL
- Teach you how to use/program in Docker or Kubernetes

We encourage anyone trying to learn these skills to read the training documentation on the repository.

## Description

When a candidate logs into the application they are taken to a dashboard. This page is responsible for the assigned tests component and the candidate check-in component. Your job is to implement these components. The requirements and description for each component is listed below.

## Getting Started

Create a branch off of the **training** branch in the format of \<your-name\>/training/complete and a second branch in the format of \<your-name\>/training/in-progress. The .../in-progress branch is where you can make your changes to solve the tasks presented in this document and the code. The .../complete branch is where you will be the target branch for your merge requests. There is a merge request template that you must choose as a template and complete to the best of your ability to aide reviewers in understanding your changes.

When you begin this training, create a story for yourself in JIRA (if not already created) in the format of TRAINING/\<your-name\>/CandidateCheckIn. All stories and tasks you create are worth 0 points (so the sprint is unaffected). Throughout this exercise create tasks and child tasks under your story so that the team and yourself can break large tasks into small achievable elements. This is to teach you to better estimate how long features take.

The descriptions and requirments of each task is listed below and designs/guides are available in the dev package linked at the bottom in Resources.

In the Context Diagrams section there is a diagram illustrating the areas of the code you will need to make changes to in order to complete this exercise.

## Components

### Frontend

Requirements

- Displays welcome message
- Displays assigned test table
- Displays candidate check-in button
- Display tests that have been assigned to the user in a table
- Displays single empty row with message if there are no tests
- Makes an API call to get the list of assigned tests.
- Has a popup that accepts a users input for Candidate Check-In
- Makes an API call to validate the Candidate Check-In input
- Closes on valid code
- Error displays on invalid code
- Displays French language when selected on the top navigation bar

### Backend

##### Requirements

- Endpoint that takes a username and test access code as parameters
- If valid code
  - Creates an assigned test row using the test name found in the test access code table on the row with the matching code
- Only assigns one test per user per code
- Returns 200 on success
- Returns 400 on error with a message
- Endpoint that takes a username
- Returns all the assigned tests for the given user

## Context Diagrams

![context diagram](https://gitlab.com/thundercat-transition/project-thundercat/-/raw/9c86edc40045c80039a581209e4b86b55944b970/docs/CAT-training-context-diagram.png)

## Resources

Dev Package: \
A design guide to creating the visual aspects of the feature
[https://gitlab.com/thundercat-transition/project-thundercat/-/blob/training/docs/Candidate Check-in.pdf](https://gitlab.com/thundercat-transition/project-thundercat/-/blob/training/docs/Candidate-Check-in.pdf)

Training:

[https://gitlab.com/thundercat-transition/project-thundercat/-/blob/training/docs/Training.md](https://gitlab.com/thundercat-transition/project-thundercat/-/blob/training/docs/Training.md)

Any other questions can be directed to the CAT team.
