# Create a New Docker Image for our Pipelines

<br>

## Why would you want to create a new image?

A docker image is a package used by each container created by your pipelines on Gitlab. It containes multiple dependencies that won't have to be downloaded each time we create a new container when starting a pipeline.

<br>

## What is a Dockerfile?

- See our main Dockerfiles: [Dockerfile_Create_Image](https://gitlab.com/thundercat-transition/project-thundercat/-/blob/develop/Dockerfile_Create_Image) & [Dockerfile_Create_Image_mssql](https://gitlab.com/thundercat-transition/project-thundercat/-/blob/develop/Dockerfile_Create_Image_mssql)
- You will find out that the Dockerfiles are used to create a [Unix Alpine](https://alpinelinux.org/) or an MSSQL environment
- Add the dependencies and/or the database structure and data that will have to be used primarily in each container used in the pipelines or the ones that have to be installed directly on Unix using "apk add".

<br>

## Login to our Docker Hub

- Open WindowsPowerShell
- cd to our project

  ```cmd
  cd C:\_dev\IdeaProjects\project-thundercat\
  ```

- To login to Docker Hub:https://hub.docker.com/repository/docker/projectthundercat/cat-build, use the command:

  ```cmd
  docker login
  ```

- Put the ID/Password from our keepass (Connect to our VPN first):

  ```
  Folder: U:\CMB-DGGM\ITSD-DSTI\ASD-DSA\Application Development\python dev

  File Name: ServersAndDatabases.kdbx
  ```

<br>

## Pull, Generate and Push to Docker Hub

### CAT BUILD Image:

- Do a Pull to get the latest version on Docker Hub:

  ```cmd
  docker pull projectthundercat/cat-build
  ```

- Create/Generate a New Image from our Dockerfile:

  ```cmd
  docker build -f .\Dockerfile_Create_Image -t projectthundercat/cat-build .
  ```

- When your new Image has been created, do a Push:

  ```cmd
  docker push projectthundercat/cat-build
  ```

### MSSQL Image:

- Do a Pull to get the latest version on Docker Hub:

  ```cmd
  docker pull projectthundercat/cat-mssql
  ```

- Generate the [schema_structure.sql](https://gitlab.com/thundercat-transition/project-thundercat/-/tree/develop/mssql_image_creation/schema_structure.sql) file based on the latest version of the database structure (including all models/tables and most updated migrations):

  - Open Microsoft SQL Server Management Studio and connect to your local database.
  - Right click on your local database and select 'Tasks - Generate Scripts...': <br><br>
    ![generate_schema_structure_1.png](../mssql_image_creation/documentation_screenshots/generate_schema_structure_1.png) <br><br>
  - Then follow those screenshots below to generate the new 'schema_structure.sql' file: <br><br>
    ![generate_schema_structure_2.png](../mssql_image_creation/documentation_screenshots/generate_schema_structure_2.png) <br><br>
    ![generate_schema_structure_3.png](../mssql_image_creation/documentation_screenshots/generate_schema_structure_3.png) <br><br>
    ![generate_schema_structure_4.png](../mssql_image_creation/documentation_screenshots/generate_schema_structure_4.png) <br><br>
    ![generate_schema_structure_5.png](../mssql_image_creation/documentation_screenshots/generate_schema_structure_5.png) <br><br>
    ![generate_schema_structure_6.png](../mssql_image_creation/documentation_screenshots/generate_schema_structure_6.png) <br><br>
  - Most updated version of 'schema_structure.sql' should now be generated are updates should be visible as part of your changes in VS Code. <br><br>

- Update the [static_table_inserts.sql](https://gitlab.com/thundercat-transition/project-thundercat/-/tree/develop/mssql_image_creation/static_table_inserts.sql) file if needed:

  - If you've added or updated model(s)/table(s) with static data (such as user permissions, reasons for deletion, view data, etc.), you'll need to update the 'static_table_inserts.sql' script to add the needed inserts in the preloaded database of the MSSQL image.
  - To do so, right click on your local database and select 'Tasks - Generate Scripts...': <br><br>
    ![generate_schema_structure_1.png](../mssql_image_creation/documentation_screenshots/generate_schema_structure_1.png) <br><br>
  - Then follow those screenshots below: <br><br>
    ![generate_schema_structure_2.png](../mssql_image_creation/documentation_screenshots/generate_schema_structure_2.png) <br><br>
    ![generate_schema_structure_7.png](../mssql_image_creation/documentation_screenshots/generate_schema_structure_7.png) <br><br>
    ![generate_schema_structure_8.png](../mssql_image_creation/documentation_screenshots/generate_schema_structure_8.png) <br><br>
    ![generate_schema_structure_9.png](../mssql_image_creation/documentation_screenshots/generate_schema_structure_9.png) <br><br>
    ![generate_schema_structure_10.png](../mssql_image_creation/documentation_screenshots/generate_schema_structure_10.png) <br><br>
    ![generate_schema_structure_11.png](../mssql_image_creation/documentation_screenshots/generate_schema_structure_11.png) <br><br>
    ![generate_schema_structure_12.png](../mssql_image_creation/documentation_screenshots/generate_schema_structure_12.png) <br><br>
  - Then copy the SETS and INSERTS from the generated script (you don't need to copy the USE(s) and the GO(s)): <br><br>
    ![generate_schema_structure_13.png](../mssql_image_creation/documentation_screenshots/generate_schema_structure_13.png) <br><br>
  - Finally open the 'static_table_inserts.sql' file, add your copied SETS and INSERTS and save: <br><br>
    ![generate_schema_structure_14.png](../mssql_image_creation/documentation_screenshots/generate_schema_structure_14.png) <br><br>
  - Most updated version of 'static_table_inserts.sql' should now be visible as part of your changes in VS Code. <br><br>

- Create/Generate a New Image from our Dockerfile:

  ```cmd
  docker build -f .\Dockerfile_Create_Image_mssql -t projectthundercat/cat-mssql .
  ```

- When your new Image has been created, do a Push:

  ```cmd
  docker push projectthundercat/cat-mssql
  ```

- Testing your new MSSQL image locally:

  - First, you'll need to do some temporary changes in entrypoint.sh (backend) and in docker-compose.yml files: <br><br>
    ![generate_schema_structure_15.png](../mssql_image_creation/documentation_screenshots/generate_schema_structure_15.png) <br><br>
    ![generate_schema_structure_16.png](../mssql_image_creation/documentation_screenshots/generate_schema_structure_16.png) <br><br>
  - Then simply do a docker down and up build:
    ```cmd
    docker compose down
    docker compose up --build
    ```
  - Once everything is up and running, you should be able to see your preloaded database with all the static data from your MSSQL image.
