# Back End - Django

as of February 2021

### Diagram Description

This diagram gives a rough overview of the flow of data on the django back end server.
![CAT back end diagram](./diagrams/CAT-how-it-works-Back-End.png "CAT back end diagram")

### System Level Diagram

Data flow always starts from an outside source like the Front End or Postman and is routed through nginx always before reaching Django. In production the WSGI/ASGI is uvicorn and in local development it is Django's runserver in order to use hot reloading.

1. Uvicorn/runserver maps an http request or ws (web sockets) request to a definition in routing.py (web sockets) or urls.py
1. Views/consumers are the controllers for the endpoints. They parse out the query or body parameters and apply permissions.
1. Business logic functions are called by the controllers and use the repositories to execute logic.
1. Business logic calls "models" which are the repository objects. These objects are serialized using Django Rest Framework (DRF) serializers to turn the objects into readable JSON. which is at some point returned to the user.
1. If a websocket connection is established then a channel is created in Redis. Django uses Redis out of the box to manage websocket channels in a distributed system. Although all of our servers are on one machine we use multiple uvicorn workers and this technically makes our system distributed.  
   Channels ensure that when two different users are connected that their messages are received by each other when different processes manage each user.

![CAT back end test factory diagram](./diagrams/CAT-how-it-works-Back-End-Factory.png "CAT back end test factory diagram")

### Link to Diagram

The database model diagram was created on dbdiagram.io and can be found here:  
https://dbdiagram.io/d/602d4c0980d742080a3aec1c
