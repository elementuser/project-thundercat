# JWT (JSON Web Token) Authentication

We assign a JWT to the user's browser when an API event is called, and it is temporarily stored in the browser's sessionStorage.

In Django, JWTs **must be associated with a User in the application's User model**.

Reference documentation:

- https://jwt.io/introduction/
- https://simpleisbetterthancomplex.com/tutorial/2018/12/19/how-to-use-jwt-authentication-with-django-rest-framework.html

# Djoser & JWT

Djoser is a library that provides a set of Django Rest Framework views to handle basic operations involving user accounts. It also contains some endpoints to handle JWT Authentication.

The Djoser JWT Endpoints & the required payload in the request to create, refresh & verify JWTs can be found [here](https://djoser.readthedocs.io/en/latest/jwt_endpoints.html). For setting up the actual URLs though in `urls.py`, I followed the setup instructions [here](https://django-rest-framework-simplejwt.readthedocs.io/en/latest/getting_started.html) and imported the appropriate views from the DRF-SimpleJWT framework.

In general, Djoser documentation can be found here: https://djoser.readthedocs.io/en/latest/introduction.html

## SimpleJWT - creating tokens manually with custom fields from the user

We wish to populate the JWT with some custom information when generating it.

First off, we create a function that generates a token manually based on this link: https://django-rest-framework-simplejwt.readthedocs.io/en/latest/creating_tokens_manually.html

Afterwards, we set up a custom Django API View for this function, and then use the View in the `create_token` / `token_obtain_pair` endpoint to `urls.py`.

To add custom information to a token, we create a new key/value pair in the token: https://django-rest-framework-simplejwt.readthedocs.io/en/latest/customizing_token_claims.html

Example code (from `create_simple_jwt_view.py`):

```python
def create_simple_jwt(request):
    """
    Generates and returns the SimpleJWT token for the user from a request
    Also populates the token with some custom fields
    """

    username = request.data.get("username", None)

    user = User.objects.filter(username=username).first()

    # Get user info if the account exists
    if user:
        token = RefreshToken.for_user(user)

        # Customize token fields
        token["username"] = username

        return Response(
            {"refresh": str(token), "access": str(token.access_token)},
            status=status.HTTP_200_OK,
        )

    # Return a bad response if the user does not exist
    else:
        return Response(
            {"error": "User does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )


class CreateJWTtoken(APIView):
    def get_permissions(self):
        return [permissions.AllowAny()]

    # API call
    def post(self, request):
        return create_simple_jwt(request)
```

## SimpleJWT - Frontend token handling

In the previous JWT module (DRF JWT), there was only 1 token included when receiving a token from the backend.

With Djoser's SimpleJWT module, the response object received from the backend upon creating a token contains 2 fields:

- "access": this is the analog to DRF JWT's token
- "refresh": this token is only used to refresh the user's existing access token (since the refresh token has a longer expiry time than the access token)

This change means the frontend has to keep track of 2 different tokens (instead of just 1 token), so SessionStorage now holds an instance of `REFRESH_TOKEN` as well as `AUTH_TOKEN`.
