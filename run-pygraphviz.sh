#!/bin/bash
# Constants to add colour to text
CYAN='\e[0;36m'
MAGENTA='\e[30;45m'
GREEN_BACKGROUND='\e[30;42m'
GREEN='\e[0;32m'
YELLOW='\e[0;33m'
NC='\e[0m' # No Color

printf "${GREEN_BACKGROUND}Starting PyGraphViz Runner...${NC}\n\n\n"

# Build Docker (can comment out if already built)
printf "${MAGENTA}Building Project${NC}\n"
docker compose build
printf "\n\n ${CYAN}Completed Successfully${NC} \n"

printf "\n\n ${GREEN}------------------------------------------------------------------${NC} \n"

# Run backend docker only
printf "\n\n\n ${MAGENTA}Run Backend Docker${NC}\n"
docker compose up -d backend
printf "\n\n ${CYAN}Completed Successfully${NC} \n"

printf "\n\n ${GREEN}------------------------------------------------------------------${NC} \n"

# Start Running PyGraphViz
printf "\n\n\n ${MAGENTA}PyGraphViz - Begin generating database diagrams ${NC}\n"

# Full Database Diagram
printf "\n\n ${YELLOW}Generate full database diagram${NC} \n"
winpty docker exec -it project-thundercat-backend-1 sh -c "python manage.py graph_models -a -o database-diagrams/database-diagram-full.png"
printf "\n\n ${CYAN}Completed Successfully${NC} \n"

printf "\n\n ${GREEN}------------------------------------------------------------------${NC} \n"

# Database Diagram Without Historical Tables
printf "\n\n\n ${YELLOW}Generate database diagram without historical tables${NC} \n"
winpty docker exec -it project-thundercat-backend-1 sh -c "python manage.py graph_models -a -X Historical\* -o database-diagrams/database-diagram-without-historical.png"
printf "\n\n ${CYAN}Completed Successfully${NC} \n"

printf "\n\n ${GREEN}------------------------------------------------------------------${NC} \n"

# Database Diagram only for Historical Tables
printf "\n\n\n ${YELLOW}Generate database diagram only for historical tables${NC} \n"
winpty docker exec -it project-thundercat-backend-1 sh -c "python manage.py graph_models -a -I Historical\* -o database-diagrams/database-diagram-only-historical.png"
printf "\n\n ${CYAN}Complete Successfully${NC} \n"

printf "\n\n\n\n ${GREEN_BACKGROUND}PyGraphViz - Execution Completed!${NC}\n"