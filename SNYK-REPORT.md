# SNYK REPORTS

## Install Snyk on Windows

1. Install Yarn on Windows:
   https://classic.yarnpkg.com/lang/en/docs/install/#windows-stable
2. Install Snyk (in Windows PowerShell):

   ```shell
   yarn global add snyk
   ```

3. Install Snyk-to-html:

   ```shell
   yarn global add snyk-to-html
   ```

4. Go to your project path:

   ```shell
   C:\_dev\Ideaprojects\project-thundercat
   ```

5. Authenticate with Snyk:

   1. Execute:
      ```shell
      snyk auth
      ```
   2. Connect with your GitHub/Google account
   3. Accept the invite to our Snyk project by clicking:

      https://app.snyk.io/invite/link/accept?invite=52ea5231-2913-4623-8794-ac6511fd2b96&utm_source=link_invite&utm_medium=referral&utm_campaign=product-link-invite&from=link_invite

## Execute Snyk Reports

### In PowerShell

1. Go to your project path:

   ```shell
   cd C:\_dev\Ideaprojects\project-thundercat
   ```

2. Execute (choose GitBash as the executor):

   ```shell
   .\run-snyk-tests.sh
   ```

   This script will generate both backend and frontend snyk reports

3. These reports will be created and saved in _reports_ folder under _./project-thundercat/_.

### In GitBash

1. Go to your project path:

   ```shell
   cd C:/_dev/Ideaprojects/project-thundercat
   ```

2. Execute (choose GitBash as the executor):

   ```shell
   ./run-snyk-tests.sh
   ```

   This script will generate both backend and frontend snyk reports

3. These reports will be created and saved in _reports_ folder under _./project-thundercat/_.
