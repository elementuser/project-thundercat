# Description

Please include a summary of the change and which issue is fixed. Please also include relevant motivation and context.

## JIRA Ticket Number

Please enter a link to the JIRA ticket

## Type of change

Please delete options that are not relevant.

- Bug fix (non-breaking change which fixes an issue)
- New feature (non-breaking change which adds functionality)
- Documentation update
- Code cleanliness or refactor
- Dependency change
- Katalon Tests

## Screenshots

Please provide if applicable (browser for `frontend` or any visual way to show a `backend` change).

# Katalon Tests

Please provide a screenshot of successful Katalon tests

# Testing

Manual steps to reproduce this functionality:

1. Go to \_\_\_ page.
2. Click \_\_\_. Notice \_\_\_.

# Checklist

- [ ] I have added or updating documentation/one pagers related to my code changes
- [ ] I have commented my code, particularly in hard-to-understand areas
- [ ] My changes generate no new compiler warnings
- [ ] I have added tests that prove my fix is effective or that my feature works
- [ ] I have researched WCAG2.1 accessibility standards and met them in this PR
- [ ] My changes look good on Chrome, Firefox and/or Edge
- [ ] My changes look good on various screen sizes (mobile, square screen)
- [ ] I have translated new text or created a JIRA to do it
- [ ] I have given or scheduled a demo of the PR to/with the Product Owner
- [ ] I have labeled my merge request with the right release number
