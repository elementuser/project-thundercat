# Testing

## Testing on IE 10

you can change IE to run in emulated mode (mock IE 10) by doing the following:

1.  Open IE
2.  Open dev tools (F12)
3.  Click "Document mode emulated" button and select 10
    ![IE Document mode emulated](/docs/images/IE-compatiblity-1.PNG)

## All Tests

This script will run backend, frontend and API tests (note that the MSSQL image with the preloaded empty database will be used during the execution of this script, meaning that no data should be created and/or modified in your official local database)

<b>\*If the 'newman' command does not work, you might need to execute the following commands:</b>

```shell
cd C:/_dev/IdeaProjects/thundercat/project-thundercat
npm install newman
```

In gitbash:

```shell
./run-tests.sh
```

In powershell:

```shell
.\run-tests.sh
```

Note: will need to press [Ctrl]+[C] after npm tests finish

## Frontend tests

The frontend ReactJS application currently has a test of unit tests using Enzyme.

To run these tests, you'll need to enter the frontend Docker container.

In powershell:

```shell
docker exec -it project-thundercat-frontend-1 /bin/bash

npm run test
```

## Backend tests

### Unit Tests

The backend Django application has a set of tests for it's views and uses Postman to manage API tests.

See: [README - Backend Unit Test](./backend/tests/README.md)

<br>

### API Tests (with Newman)

To run Newman tests, you'll need 'Newman' installed. If it's not already installed, simply execute those commands:

```shell
cd C:/_dev/IdeaProjects/thundercat/project-thundercat
npm install
```

Now, to execute the API test collection with Newman, follow those steps:

1.  Make sure you have a running local environment.
2.  Open Windows PowerShell.
3.  Go to the right directory: <br><br>
    ```shell
    cd C:/_dev/IdeaProjects/thundercat/project-thundercat/node_modules/newman/bin
    ```
4.  If you do not already have a super user created or if you are currently using the MSSQL image (empty database), you'll need to execute the following command:
    ```shell
    docker exec -i project-thundercat-backend-1 python manage.py shell -c "from django.contrib.auth import get_user_model; import datetime; import pytz; User = get_user_model(); User.objects.create_superuser(username='admin', first_name='Admin', last_name='Account', birth_date='1975-01-01', pri='', military_service_nbr='', email='admin@email.ca', secondary_email='', last_password_change='2021-01-01', is_staff=True, psrs_applicant_id='', last_login=datetime.datetime.now(pytz.utc), password='Admin1234!');"
    ```
5.  Execute the following Newman command: <br><br>
    ```shell
    node newman run C:/_dev/IdeaProjects/thundercat/project-thundercat/backend/tests/postman/ThunderCAT-new.postman_collection.json -e C:/_dev/IdeaProjects/thundercat/project-thundercat/backend/tests/postman/environments/local/ThunderCAT.local.postman_environment.json
    ```

<br>

### API Tests (with Postman)

To run Postman tests, you'll need to launch Postman desktop app and setup your environment.

1. Import ThunderCAT local environment:

   ![postman screenshot](/docs/images/import-postman-env.png)

   ![postman screenshot](/docs/images/import-postman-env2.png)

   ![postman screenshot](/docs/images/import-postman-env3.png)

   Then select the following file:
   _\project-thundercat\backend\tests\postman\environments\local\ThunderCAT.local.postman_environment.json_.

   You can then put the name of your choice for this new imported environment (e.g. _"ThunderCAT - Local"_).

2. Make sure that you new environment is selected:

   ![postman screenshot](/docs/images/postman-selected-env.png)

3. Import ThunderCAT collection:

   ![postman screenshot](/docs/images/import-postman-collection.png)

   ![postman screenshot](/docs/images/import-postman-collection2.png)

   Then select the following file: _\project-thundercat\backend\tests\postman\ThunderCAT.postman_collection.json_.

   You have now your new ThunderCAT collection:

   ![postman screenshot](/docs/images/import-postman-collection3.png)

4. You can either run individual tests or run the whole collection:

   Individual test:

   ![postman screenshot](/docs/images/postman-run-individual-test.png)

   Whole collection:

   ![postman screenshot](/docs/images/postman-run-whole-collection.png)

   ![postman screenshot](/docs/images/postman-run-whole-collection2.png)

   **Note that if you run the whole collection, you may need to delete some data, since the tests are creating new accounts and you also may need to add authorization for protected APIs.**

   ![postman screenshot](/docs/images/postman-authorization-for-protected-apis.png)
