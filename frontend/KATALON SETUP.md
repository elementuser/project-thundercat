# Katalon Setup

## Solution to make Katalon connect to the database using Windows User

1. goto: https://www.microsoft.com/en-us/download/details.aspx?displaylang=en&id=11774
2. Download the JDBC file and extract to your preferred location
3. Open the auth folder matching your OS x64 or x86
4. Copy sqljdbc_auth.dll filepaste in: **KatalonStudioDirectory**\jre\bin
