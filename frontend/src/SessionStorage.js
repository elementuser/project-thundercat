/* eslint-disable no-param-reassign */
import CryptoJS from "crypto-js";
import SecureStorage from "secure-web-storage";

const SECRET_KEY = process.env.REACT_APP_SECRET_KEY;

// ref: https://www.npmjs.com/package/secure-web-storage
const secureStorage = new SecureStorage(sessionStorage, {
  hash: function hash(key) {
    key = CryptoJS.SHA256(key, SECRET_KEY);

    return key.toString();
  },
  encrypt: function encrypt(data) {
    data = CryptoJS.AES.encrypt(data, SECRET_KEY);

    data = data.toString();

    return data;
  },
  decrypt: function decrypt(data) {
    data = CryptoJS.AES.decrypt(data, SECRET_KEY);

    data = data.toString(CryptoJS.enc.Utf8);

    return data;
  }
});

export const ACTION = {
  SET: "SET",
  GET: "GET",
  REMOVE: "REMOVE",
  CLEAR: "CLEAR"
};

export const ITEM = {
  STATE: "state",
  CAT_LANGUAGE: "catLanguage",
  AUTH_TOKEN: "authToken",
  REFRESH_TOKEN: "refreshToken",
  TEST_ACTIVE: "testActive"
};

function SessionStorage(action, item, value) {
  // SET
  if (action === ACTION.SET) {
    return secureStorage.setItem(item, value);
    // GET
  }
  if (action === ACTION.GET) {
    return secureStorage.getItem(item);
    // REMOVE
  }
  if (action === ACTION.REMOVE) {
    return secureStorage.removeItem(item);
    // CLEAR
  }
  if (action === ACTION.CLEAR) {
    return secureStorage.clear();
  }
}

export default SessionStorage;
