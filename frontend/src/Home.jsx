import React, { Component } from "react";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import LOCALIZE from "./text_resources";
import ContentContainer from "./components/commons/ContentContainer";
import AuthenticationTabs from "./components/authentication/AuthenticationTabs";
import { updatePageHasErrorState } from "./modules/LoginRedux";
import { getActiveSystemAlertsToBeDisplayedOnHomePage } from "./modules/SystemAlertsRedux";
import { Helmet } from "react-helmet";
import { connect } from "react-redux";
import AlertMessage from "./components/commons/AlertMessage";
import getCriticalityDataForSystemAlert from "./helpers/criticality";

export const styles = {
  appPadding: {
    padding: "15px"
  },
  paragraph: {
    paddingBottom: "15px"
  },
  systemAlertsContainer: {
    maxWidth: "75%",
    margin: "0 auto"
  }
};

class Home extends Component {
  static propTypes = {
    // Props from Redux
    authenticated: PropTypes.bool,
    updatePageHasErrorState: PropTypes.func
  };

  state = {
    activeSystemAlerts: []
  };

  // updating pageHasError state to 'false' on render, so the error isn't displayed on refresh
  componentDidMount = () => {
    this.props.updatePageHasErrorState(false);
    this.handleSystemAlerts();
  };

  handleSystemAlerts = () => {
    // getting all active system alerts that need to be displayed at the moment
    this.props.getActiveSystemAlertsToBeDisplayedOnHomePage().then(response => {
      this.setState({ activeSystemAlerts: response });
    });
  };

  render() {
    return (
      <div className="app" style={styles.appPadding}>
        <Helmet>
          <html lang={this.props.currentLanguage} />
          {!this.props.pageHasError && (
            <title className="notranslate">{LOCALIZE.titles.home}</title>
          )}
          {this.props.pageHasError && (
            <title className="notranslate">{LOCALIZE.titles.homeWithError}</title>
          )}
        </Helmet>
        <ContentContainer>
          <div id="main-content" role="main" tabIndex={0} className="notranslate">
            <h1>{LOCALIZE.homePage.welcomeMsg}</h1>
            <p style={styles.paragraph}>{LOCALIZE.homePage.description}</p>
            <div style={styles.systemAlertsContainer}>
              {this.state.activeSystemAlerts.map(systemAlertData => {
                const alertType = getCriticalityDataForSystemAlert(
                  systemAlertData.criticality_data.codename
                );
                return (
                  <AlertMessage
                    alertType={alertType}
                    message={systemAlertData[`message_text_${this.props.currentLanguage}`]}
                  />
                );
              })}
            </div>
            <div>
              <AuthenticationTabs />
            </div>
          </div>
        </ContentContainer>
      </div>
    );
  }
}

export { Home as UnconnectedHome };
const mapStateToProps = (state, ownProps) => {
  return {
    authenticated: state.login.authenticated,
    pageHasError: state.login.pageHasError,
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updatePageHasErrorState,
      getActiveSystemAlertsToBeDisplayedOnHomePage
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Home);
