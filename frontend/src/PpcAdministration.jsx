import React, { Component } from "react";
import LOCALIZE from "./text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import { styles as SystemAdministrationStyles } from "./components/etta/SystemAdministration";
import ContentContainer from "./components/commons/ContentContainer";
import SideNavigation from "./components/eMIB/SideNavigation";
import Reports from "./components/ppc/Reports";
import LastLogin from "./components/authentication/LastLogin";
import { PATH } from "./components/commons/Constants";

export const styles = {
  appPadding: {
    padding: "15px"
  }
};

class PpcAdministration extends Component {
  state = {
    isPPCAdminDashboard: false
  };

  componentDidMount = () => {
    // to display last login if it is a PPC dashboard
    if (this.props.currentHomePage === PATH.ppcAdministration) {
      this.setState({
        isPPCAdminDashboard: true
      });
    }
  };

  // Returns array where each item indicates specifications related to How To Page including the title and the body
  getPpcAdministrationSections = () => {
    return [
      {
        menuString: LOCALIZE.ppcAdministration.sideNavItems.reports,
        body: <Reports />
      }
    ];
  };

  render() {
    const specs = this.getPpcAdministrationSections();
    return (
      <div>
        {this.state.isPPCAdminDashboard && <LastLogin lastLoginDate={this.props.lastLogin} />}
        <div className="app" style={styles.appPadding}>
          <Helmet>
            <html lang={this.props.currentLanguage} />
            <title className="notranslate">{LOCALIZE.titles.ppcAdministration}</title>
          </Helmet>
          <ContentContainer>
            <div id="main-content" role="main">
              <div
                id="user-welcome-message-div"
                style={SystemAdministrationStyles.header}
                aria-labelledby="user-welcome-message"
                className="notranslate"
              >
                <h1 id="user-welcome-message" className="green-divider">
                  {LOCALIZE.formatString(
                    LOCALIZE.ppcAdministration.title,
                    this.props.firstName,
                    this.props.lastName
                  )}
                </h1>
              </div>
              <div>
                <div style={SystemAdministrationStyles.sectionContainerLabelDiv}>
                  <div>
                    <label style={SystemAdministrationStyles.sectionContainerLabel}>
                      {LOCALIZE.ppcAdministration.containerLabel}
                      <span style={SystemAdministrationStyles.tabStyleBorder}></span>
                    </label>
                  </div>
                </div>
                <div style={SystemAdministrationStyles.sectionContainer}>
                  <section aria-label={LOCALIZE.ariaLabel.sideNavigationSection}>
                    <SideNavigation
                      specs={specs}
                      startIndex={0}
                      displayNextPreviousButton={false}
                      isMain={true}
                      tabContainerStyle={SystemAdministrationStyles.tabContainer}
                      tabContentStyle={SystemAdministrationStyles.tabContent}
                      navStyle={SystemAdministrationStyles.nav}
                      bodyContentCustomStyle={SystemAdministrationStyles.sideNavBodyContent}
                    />
                  </section>
                </div>
              </div>
            </div>
          </ContentContainer>
        </div>
      </div>
    );
  }
}

export { PpcAdministration as unconnectedPpcAdministration };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    firstName: state.user.firstName,
    lastName: state.user.lastName,
    username: state.user.username,
    lastLogin: state.user.lastLogin,
    currentHomePage: state.userPermissions.currentHomePage
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(PpcAdministration);
