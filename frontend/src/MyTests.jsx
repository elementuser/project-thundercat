import React, { Component } from "react";
import ContentContainer from "./components/commons/ContentContainer";
import { Helmet } from "react-helmet";
import LOCALIZE from "./text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import MyTestsTable from "./components/myTests/MyTestsTable";
import getScoredTests from "./modules/MyTestsRedux";
import CustomButton, { THEME } from "./components/commons/CustomButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBinoculars } from "@fortawesome/free-solid-svg-icons";
import getRetestDate from "./helpers/retestPeriodCalculations";
import getConvertedScore from "./helpers/scoreConversion";
import AlertMessage, { ALERT_TYPE } from "./components/commons/AlertMessage";

const styles = {
  mainDiv: {
    marginBottom: 48
  },
  firstRowStyle: {
    textAlign: "left"
  },
  viewEditDetailsBtn: {
    minWidth: 100
  },
  buttonLabel: {
    margin: 0,
    paddingLeft: 6
  },
  appPadding: {
    padding: "15px"
  },
  alertMessageContainer: {
    padding: "12px 0"
  }
};

class MyTests extends Component {
  state = {
    currentlyLoading: true,
    rowsDefinition: {},
    selectedTestDetails: {},
    triggerPopup: false
  };

  componentDidMount = () => {
    this.populateRowsDefinition();
  };

  // populating table rows
  populateRowsDefinition = () => {
    // initializing needed object and array for rowsDefinition props (needed for GenericTable component)
    let rowsDefinition = {};
    const data = [];
    this.props
      .getScoredTests()
      .then(response => {
        for (let i = 0; i < response.length; i++) {
          // pushing needed data in data array (needed for GenericTable component)
          data.push({
            column_1: `${response[i].test[`${this.props.currentLanguage}_name`]} (${
              response[i].test.test_code
            })`,
            column_2:
              response[i].start_date !== null
                ? response[i].start_date.split("T")[0]
                : LOCALIZE.commons.na,
            column_3: response[i].is_invalid
              ? LOCALIZE.commons.status.invalid
              : getConvertedScore(
                  response[i][`${this.props.currentLanguage}_converted_score`],
                  response[i].status
                ),
            column_4: (
              <CustomButton
                buttonId={`results-button-row-${i}`}
                label={
                  <>
                    <FontAwesomeIcon icon={faBinoculars} />
                    <span style={styles.buttonLabel}>
                      {LOCALIZE.myTests.table.viewResultsButton}
                    </span>
                  </>
                }
                action={() => {
                  this.handleViewResults(response[i]);
                }}
                customStyle={styles.viewEditDetailsBtn}
                buttonTheme={THEME.PRIMARY}
                ariaLabel={LOCALIZE.formatString(
                  LOCALIZE.myTests.table.viewResultsButtonAriaLabel,
                  `${response[i].test[`${this.props.currentLanguage}_name`]} (${
                    response[i].test.test_code
                  })`,
                  response[i].start_date !== null
                    ? response[i].start_date.split("T")[0]
                    : LOCALIZE.commons.na,
                  parseInt(response[i].test.retest_period) !== 0
                    ? getRetestDate(
                        response[i].start_date,
                        parseInt(response[i].test.retest_period)
                      )
                    : LOCALIZE.commons.na,
                  getConvertedScore(
                    response[i][`${this.props.currentLanguage}_converted_score`],
                    response[i].status
                  )
                )}
              />
            )
          });
        }
        // updating rowsDefinition object with provided data and needed style
        rowsDefinition = {
          column_1_style: styles.firstRowStyle,
          column_2_style: styles.basicRowStyle,
          column_3_style: styles.basicRowStyle,
          column_4_style: styles.basicRowStyle,
          data: data
        };

        // saving results in state
        this.setState({
          rowsDefinition: rowsDefinition
        });
      })
      .then(() => {
        this.setState({ currentlyLoading: false });
      });
  };

  handleViewResults = data => {
    this.setState({ selectedTestDetails: data }, () => {
      this.setState({ triggerPopup: !this.state.triggerPopup });
    });
  };

  render() {
    return (
      <div className="app" style={{ ...styles.mainDiv, ...styles.appPadding }}>
        <Helmet>
          <html lang={this.props.currentLanguage} />
          <title className="notranslate">{LOCALIZE.titles.home}</title>
        </Helmet>
        <ContentContainer>
          <div id="main-content" role="main">
            <div
              id="user-welcome-message-div"
              aria-labelledby="user-welcome-message"
              className="notranslate"
            >
              <h1 id="user-welcome-message" className="green-divider">
                {LOCALIZE.formatString(
                  LOCALIZE.myTests.title,
                  this.props.firstName,
                  this.props.lastName
                )}
              </h1>
              <div style={styles.alertMessageContainer}>
                <AlertMessage
                  alertType={ALERT_TYPE.light}
                  message={
                    <>
                      <p>{LOCALIZE.myTests.alertCatTestsOnly1}</p>
                      <p>{LOCALIZE.myTests.alertCatTestsOnly2}</p>
                      <p>{LOCALIZE.myTests.alertCatTestsOnly3}</p>
                    </>
                  }
                  usesMarkdownOrHtmlTags={false}
                />
              </div>
            </div>
            <MyTestsTable
              currentlyLoading={this.state.currentlyLoading}
              rowsDefinition={this.state.rowsDefinition}
              selectedTestDetails={this.state.selectedTestDetails}
              triggerPopup={this.state.triggerPopup}
            />
          </div>
        </ContentContainer>
      </div>
    );
  }
}
export { MyTests as UnconnectedMyTests };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    firstName: state.user.firstName,
    lastName: state.user.lastName
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getScoredTests
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(MyTests);
