import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  getUserAccommodations,
  setAccommodations,
  saveUserAccommodations,
  initialState,
  setAccommodationsSavedAndLoaded
} from "./modules/AccommodationsRedux";
import PopupBox, { BUTTON_TYPE } from "./components/commons/PopupBox";
import LOCALIZE from "./text_resources";

class Accommodations extends Component {
  state = {
    showPopup: false
  };

  componentDidUpdate = () => {
    if (this.props.authenticated && !this.props.loadedSaved) {
      this.getUserAccommodations();
    }
  };

  getUserAccommodations = () => {
    this.props.getUserAccommodations().then(response => {
      const dbAccomms = {
        fontFamily: response.body.font_family,
        fontSize: response.body.font_size,
        spacing: response.body.spacing
      };
      // if accomms are not saved to db and the accomms don't equal default
      if (
        response.body.empty &&
        !this.accommodationsEqual(this.props.accommodations, initialState)
      ) {
        // save accomms to db
        this.props.saveUserAccommodations(this.props.accommodations);
        this.setState({ showPopup: true });
      }
      // if the accoms in db are not equal to current accomms
      else if (!this.accommodationsEqual(dbAccomms, this.props.accommodations)) {
        if (response.body && !response.body.empty) {
          this.props.setAccommodations(response.body);
        }
      }
      this.props.setAccommodationsSavedAndLoaded();
    });
  };

  accommodationsEqual = (obj1, obj2) => {
    return (
      obj1.fontFamily === obj2.fontFamily &&
      obj1.fontSize === obj2.fontSize &&
      obj1.spacing === obj2.spacing
    );
  };

  handleClose = () => {
    this.setState({ showPopup: false });
  };

  render() {
    return (
      <>
        <PopupBox
          show={this.state.showPopup}
          title={LOCALIZE.accommodations.notificationPopup.title}
          handleClose={() => this.handleClose()}
          description={
            <div>
              <p>{LOCALIZE.accommodations.notificationPopup.description1}</p>
              <p>{LOCALIZE.accommodations.notificationPopup.description2}</p>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.ok}
          rightButtonAction={() => this.handleClose()}
        />
      </>
    );
  }
}

export { Accommodations as UnnconnectedAccommodations };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    authenticated: state.login.authenticated,
    loadedSaved: state.accommodations.loadedSaved
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getUserAccommodations,
      setAccommodations,
      saveUserAccommodations,
      setAccommodationsSavedAndLoaded
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Accommodations);
