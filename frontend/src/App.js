import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Router, Route, Redirect, Switch } from "react-router-dom";
import {
  authenticateAction,
  logoutAction,
  isTokenStillValid,
  refreshAuthToken
} from "./modules/LoginRedux";
import { updateAssignedTestId, getAssignedTests } from "./modules/AssignedTestsRedux";
import "./css/lib/aurora.min.css";
import "./css/cat-theme.css";
import { Helmet } from "react-helmet";
import Status from "./Status";
import Home from "./Home";
import TestAdministration from "./components/ta/TestAdministration";
import { history } from "./store-index";
import SelectLanguage from "./SelectLanguage";
import SiteNavBar from "./SiteNavBar";
import { PATH, URL_PREFIX } from "./components/commons/Constants";

import SampleTestsRoutes from "./components/samples/SampleTestRoutes";
import {
  getUserPermissions,
  updateCurrentHomePageState,
  updatePermissionsState,
  resetPermissionsState
} from "./modules/PermissionsRedux";
import { setIsUserLoading, resetUserState } from "./modules/UserRedux";
import PrivateRoute from "./components/commons/PrivateRoute";
import Dashboard from "./Dashboard";
import Profile from "./components/profile/Profile";
import { PERMISSION } from "./components/profile/Constants";
// import IncidentReport from "./IncidentReport";
import MyTests from "./MyTests";
// import ContactUs from "./ContactUs";
import SystemAdministration from "./components/etta/SystemAdministration";
import PpcAdministration from "./PpcAdministration";
import ScorerRoutes from "./components/scorer/ScorerRoutes";
import { resetTestStatusState } from "./modules/TestStatusRedux";
import { resetNotepadState } from "./modules/NotepadRedux";
import { resetTestStatusState as resetSampleTestStatusState } from "./modules/SampleTestStatusRedux";

import { resetAccommodations, getLineSpacingCSS } from "./modules/AccommodationsRedux";

import ExpiredTokenPopup from "./ExpiredTokenPopup";
import TestLanding from "./components/testFactory/TestLanding";
import SessionStorage, { ACTION, ITEM } from "./SessionStorage";
import TEST_STATUS from "./components/ta/Constants";
import TestBuilder from "./components/testBuilder/TestBuilder";
import TestBuilderTestPage from "./components/testFactory/TestBuilderTestPage";
import SelectedItemBank from "./components/etta/rd_item_banks/selected_item_bank/SelectedItemBank";
import ItemBankEditor from "./components/testBuilder/itemBank/ItemBankEditor";
import Accommodations from "./Accommodations";
import ResetPasswordPage from "./ResetPasswordPage";
import InvalidateTestScreen from "./components/commons/InvalidateTestScreen";
import UserLookUpDetails from "./components/etta/UserLookUpDetails";
import { resetUserLookUpState } from "./modules/UserLookUpRedux";
import SelectedOrderlessTestAdministratorDetails from "./components/etta/test_accesses/SelectedOrderlessTestAdministratorDetails";
import ItemEditor from "./components/testBuilder/itemBank/items/ItemEditor";
import BundleEditor from "./components/testBuilder/itemBank/bundles/BundleEditor";

class App extends Component {
  static propTypes = {
    // Props from Redux
    authenticateAction: PropTypes.func,
    logoutAction: PropTypes.func,
    currentTestId: PropTypes.string,
    refreshAuthToken: PropTypes.func,
    getUserPermissions: PropTypes.func,
    updatePermissionsState: PropTypes.func,
    resetTestStatusState: PropTypes.func,
    resetSampleTestStatusState: PropTypes.func,
    resetNotepadState: PropTypes.func,
    resetPermissionsState: PropTypes.func,
    resetAccommodations: PropTypes.func,
    isTokenStillValid: PropTypes.func,
    getAssignedTests: PropTypes.func,
    updateCurrentHomePageState: PropTypes.func,
    setIsUserLoading: PropTypes.func,
    resetUserState: PropTypes.func,
    resetUserLookUpState: PropTypes.func
  };

  state = {
    triggerRerender: false,
    setFocusOnQuitTestButton: false,
    isLanguageSelected: SessionStorage(ACTION.GET, ITEM.CAT_LANGUAGE),
    showTokenExpiredDialog: false,
    jwtCountdown: undefined,
    alive: false
  };

  handleExpiredTokenAction = () => {
    // reset all states in case of token expiration
    this.props.logoutAction();
    this.props.resetUserState();
    this.props.resetTestStatusState();
    this.props.resetSampleTestStatusState();
    this.props.resetNotepadState();
    this.props.resetPermissionsState();
    this.props.resetAccommodations();
    this.props.resetUserLookUpState();
    // push to select language page (application root)
    history.push("/oec-cat/");
    // display token expired popup
    this.setState({ showTokenExpiredDialog: true });
  };

  closePopup = () => {
    this.setState({ showTokenExpiredDialog: false });
    // reloading page to make sure that all states are reseted
    window.location.reload();
  };

  initializeLanguage = () => {
    this.setState({ isLanguageSelected: true });
  };

  componentDidMount = () => {
    // set focus on quit test button on component load
    this.setState({ setFocusOnQuitTestButton: true });
    // getting the authentication token from the local storage
    const token = SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN);

    // if there is no token, then there is no point in trying to verify it
    if (token === undefined) {
      // update authenticated state to false
      this.props.authenticateAction(false);
    }
    // refresh the token
    this.refreshToken();
    if (this.state.jwtCountdown) {
      clearInterval(this.state.jwtCountdown);
    }
    const interval = setInterval(this.refreshToken, 450000);
    this.setState({ jwtCountdown: interval });
  };

  componentWillUnmount = () => {
    clearInterval(this.state.jwtCountdown);
  };

  componentDidUpdate = prevProps => {
    // if triggerAppRerender gets updated
    if (prevProps.triggerAppRerender !== this.props.triggerAppRerender) {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }
    this.props.setIsUserLoading(true);
    // if the user is authenticated and if the user is not yet defined (avoid to constantly call this function)
    // as soon as the user is defined, this function will not no longer be called
    if (this.props.authenticated) {
      // checks if the token is still valid
      this.props.isTokenStillValid().then(bool => {
        // token is still valid
        if (bool) {
          // update authenticated state to true
          this.props.authenticateAction(true);
          // initialize permission flags
          this.props.getUserPermissions().then(response => {
            // initializing permission flags
            const { isSuperUser } = this.props;
            let isEtta = false;
            let isPpc = false;
            let isTa = false;
            let isScorer = false;
            let isTb = false;
            let isAae = false;
            let isRdOperations = false;
            let isTd = false;
            // specified user is a super user
            if (isSuperUser) {
              // setting all permission flags to true
              isEtta = true;
              isPpc = true;
              isTa = true;
              isScorer = true;
              isTb = true;
              isAae = true;
              isRdOperations = true;
              isTd = true;
              // // specified user is not a super user
            } else {
              // looping in user permissions to update the needed permission flags
              for (let i = 0; i < response.length; i++) {
                if (response[i].codename === PERMISSION.systemAdministrator) {
                  isEtta = true;
                } else if (response[i].codename === PERMISSION.ppcAdministrator) {
                  isPpc = true;
                } else if (response[i].codename === PERMISSION.scorer) {
                  isScorer = true;
                } else if (response[i].codename === PERMISSION.testAdministrator) {
                  isTa = true;
                } else if (response[i].codename === PERMISSION.testBuilder) {
                  isTb = true;
                } else if (response[i].codename === PERMISSION.aae) {
                  isAae = true;
                } else if (response[i].codename === PERMISSION.rdOperations) {
                  isRdOperations = true;
                } else if (response[i].codename === PERMISSION.testDeveloper) {
                  isTd = true;
                }
              }
            }
            // update permissions states based on the user permission flags
            this.props.updatePermissionsState({
              isEtta: isEtta,
              isPpc: isPpc,
              isTa: isTa,
              isScorer: isScorer,
              isTb: isTb,
              isAae: isAae,
              isRdOperations: isRdOperations,
              isTd: isTd
            });
            // update the current home page state based on the permissions
            // is super user or ETTA
            if (isEtta || isRdOperations || isSuperUser) {
              this.props.updateCurrentHomePageState(PATH.systemAdministration);
              // is PPC
            } else if (isPpc) {
              this.props.updateCurrentHomePageState(PATH.ppcAdministration);
              // is scorer
            } else if (isScorer) {
              this.props.updateCurrentHomePageState(PATH.scorerBase);
              // is TA
            } else if (isTa) {
              this.props.updateCurrentHomePageState(PATH.testAdministration);
              // is Test Builder
            } else if (isTb) {
              this.props.updateCurrentHomePageState(PATH.testBuilder);
              // is Test Developer
            } else if (isTd) {
              this.props.updateCurrentHomePageState(PATH.testBuilder);
              //   // is candidate
            } else {
              this.props.updateCurrentHomePageState(PATH.dashboard);
            }
            // No more user data is loading
            this.props.setIsUserLoading(false);
          });

          // getting assigned test for the current user
          this.props.getAssignedTests().then(assigned_tests => {
            // check for each assigned test
            for (let i = 0; i < assigned_tests.length; i++) {
              // if there is an active test (active, locked or paused)
              if (
                assigned_tests[i].status === TEST_STATUS.PRE_TEST ||
                assigned_tests[i].status === TEST_STATUS.ACTIVE ||
                assigned_tests[i].status === TEST_STATUS.LOCKED ||
                assigned_tests[i].status === TEST_STATUS.PAUSED
              ) {
                // redirect to test page
                this.props.updateAssignedTestId(
                  assigned_tests[i].id.toString(),
                  assigned_tests[i].test.id,
                  assigned_tests[i].accommodation_request
                );
                history.push(PATH.testBase);
              }
            }
          });
          // token is expired
        } else {
          // update authenticated state to false
          this.props.authenticateAction(false);
          this.handleExpiredTokenAction();
          // this.props.getUserPermissions is not called in this case, so not loading user data
          this.props.setIsUserLoading(false);
        }
      });
      // if home page changes, push new path
      if (prevProps.currentHomePage !== this.props.currentHomePage) {
        history.push(this.props.currentHomePage);
      }
    } else {
      // this.props.isTokenStillValid is not called in this case, so not loading user data
      this.props.setIsUserLoading(false);
    }
    // if triggerFocusToMainNavLink gets updated
    if (
      prevProps.accommodations.triggerFocusToMainNavLink !==
      this.props.accommodations.triggerFocusToMainNavLink
    ) {
      // focusing on Home link (located in the navigation bar) if it exists
      if (document.getElementById("navigation-bar-main-link-id")) {
        document.getElementById("navigation-bar-main-link-id").focus();
      }
    }
  };

  // refreshing user auth token
  refreshToken = () => {
    const token = SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN);
    const userIsActive = this.state.alive;
    // token exists
    if (token !== null && this.props.authenticated) {
      this.props.isTokenStillValid().then(tokenIsValid => {
        // kick the user out if they are inactive and have an expired token
        if (!tokenIsValid && !userIsActive) {
          this.handleExpiredTokenAction();
        }
        // if the user was active refresh the token and reset the activty marker
        if (userIsActive) {
          this.props.refreshAuthToken().then(response => {
            // removing the old token from the local storage
            SessionStorage(ACTION.REMOVE, ITEM.AUTH_TOKEN);
            SessionStorage(ACTION.REMOVE, ITEM.REFRESH_TOKEN);

            // updating the local storage with the new refreshed token
            SessionStorage(ACTION.SET, ITEM.AUTH_TOKEN, response.access);
            SessionStorage(ACTION.SET, ITEM.REFRESH_TOKEN, response.refresh);
          });
          this.setState({ alive: false });
          // token is expired
        }
      });
    }
  };

  onBlur = () => {
    if (!this.state.alive) {
      this.setState({ alive: true });
    }
  };

  render() {
    let accommodationStyles = {
      fontFamily: this.props.accommodations.fontFamily,
      fontSize: this.props.accommodations.fontSize
    };
    if (this.props.accommodations.spacing) {
      accommodationStyles = {
        ...accommodationStyles,
        ...getLineSpacingCSS()
      };
    }

    // Determine if user has already selected a language. Based on local storage, not props
    const isLanguageSelected = SessionStorage(ACTION.GET, ITEM.CAT_LANGUAGE);
    return (
      <div
        className="notranslate"
        id="AppRoot"
        style={
          window.location.href.includes(URL_PREFIX.itemBank)
            ? { ...accommodationStyles, ...{ height: "100vh", overflow: "hidden" } }
            : accommodationStyles
        }
        onBlur={this.onBlur}
        translate="no"
      >
        {!isLanguageSelected && <SelectLanguage initializeLanguage={this.initializeLanguage} />}
        {isLanguageSelected && (
          <div>
            <Helmet>
              <html lang={this.props.currentLanguage} translate="no" />
              <meta name="google" content="notranslate" />
            </Helmet>
            <div>
              <Accommodations accommodations={this.props.accommodations} />
              <Router history={history}>
                <div>
                  <Route component={SiteNavBar} />
                  <Switch>
                    <PrivateRoute
                      exact
                      auth={!this.props.authenticated}
                      path={PATH.login}
                      component={Home}
                      redirectTo={this.props.currentHomePage}
                    />
                    <Route path={PATH.status} component={Status} />
                    <Route path={PATH.sampleTests} component={SampleTestsRoutes} />
                    <Route path={PATH.resetPassword} component={ResetPasswordPage} />
                    <Route
                      auth={this.props.authenticated}
                      path={PATH.testBuilderTest}
                      component={() => {
                        return <TestBuilderTestPage />;
                      }}
                    />
                    <Route
                      auth={this.props.authenticated}
                      path={PATH.testBase}
                      component={TestLanding}
                    />
                    <PrivateRoute
                      auth={this.props.authenticated}
                      path={this.props.currentHomePage}
                      component={
                        this.props.isSuperUser || this.props.isEtta || this.props.isRdOperations
                          ? SystemAdministration
                          : this.props.isPpc
                          ? PpcAdministration
                          : this.props.isScorer
                          ? ScorerRoutes
                          : this.props.isTa
                          ? TestAdministration
                          : this.props.isTb
                          ? TestBuilder
                          : this.props.isTd
                          ? TestBuilder
                          : Dashboard
                      }
                      redirectTo={PATH.login}
                    />
                    <PrivateRoute
                      auth={this.props.isTa && this.props.authenticated}
                      path={PATH.testAdministration}
                      component={TestAdministration}
                      redirectTo={this.props.currentHomePage}
                    />
                    <PrivateRoute
                      auth={
                        (this.props.isEtta || this.props.isRdOperations) && this.props.authenticated
                      }
                      path={PATH.systemAdministration}
                      component={SystemAdministration}
                      redirectTo={this.props.currentHomePage}
                    />
                    <PrivateRoute
                      auth={this.props.isPpc && this.props.authenticated}
                      path={PATH.ppcAdministration}
                      component={PpcAdministration}
                      redirectTo={this.props.currentHomePage}
                    />
                    <PrivateRoute
                      auth={this.props.isScorer && this.props.authenticated}
                      path={PATH.scorerBase}
                      component={ScorerRoutes}
                      redirectTo={this.props.currentHomePage}
                    />
                    <PrivateRoute
                      auth={this.props.authenticated}
                      path={PATH.dashboard}
                      component={Dashboard}
                      redirectTo={PATH.login}
                    />
                    <PrivateRoute
                      auth={this.props.authenticated}
                      path={PATH.profile}
                      component={Profile}
                      redirectTo={PATH.login}
                    />
                    {/* <PrivateRoute
                      auth={this.props.authenticated}
                      path={PATH.incidentReport}
                      component={IncidentReport}
                      redirectTo={PATH.login}
                    /> */}
                    <PrivateRoute
                      auth={(this.props.isTb || this.props.isTd) && this.props.authenticated}
                      path={PATH.testBuilder}
                      component={TestBuilder}
                      redirectTo={PATH.login}
                    />
                    <PrivateRoute
                      auth={this.props.isRdOperations && this.props.authenticated}
                      path={PATH.itemBankAccesses}
                      component={SelectedItemBank}
                      redirectTo={PATH.login}
                    />
                    <PrivateRoute
                      auth={this.props.isTd && this.props.authenticated}
                      path={PATH.itemBankEditor}
                      component={ItemBankEditor}
                      redirectTo={PATH.login}
                    />
                    <PrivateRoute
                      auth={this.props.isTd && this.props.authenticated}
                      path={PATH.itemEditor}
                      component={ItemEditor}
                      redirectTo={PATH.login}
                    />
                    <PrivateRoute
                      auth={this.props.isTd && this.props.authenticated}
                      path={PATH.bundleEditor}
                      component={BundleEditor}
                      redirectTo={PATH.login}
                    />
                    <PrivateRoute
                      auth={this.props.authenticated}
                      path={PATH.myTests}
                      component={MyTests}
                      redirectTo={PATH.login}
                    />
                    {/* <PrivateRoute
                      auth={this.props.authenticated}
                      path={PATH.contactUs}
                      component={ContactUs}
                      redirectTo={PATH.login}
                    /> */}
                    <PrivateRoute
                      auth={this.props.isTestInvalidated && this.props.authenticated}
                      path={PATH.invalidateTest}
                      component={InvalidateTestScreen}
                      redirectTo={this.props.currentHomePage}
                    />
                    <PrivateRoute
                      auth={this.props.isEtta && this.props.authenticated}
                      path={PATH.userLookUpDetails}
                      component={UserLookUpDetails}
                      redirectTo={this.props.currentHomePage}
                    />
                    <PrivateRoute
                      auth={this.props.isEtta && this.props.authenticated}
                      path={PATH.orderlessTestAdministratorDetails}
                      component={SelectedOrderlessTestAdministratorDetails}
                      redirectTo={this.props.currentHomePage}
                    />
                    <Redirect to={PATH.login} />
                  </Switch>
                </div>
              </Router>
            </div>
          </div>
        )}
        <ExpiredTokenPopup
          showTokenExpiredDialog={this.state.showTokenExpiredDialog}
          closePopupFunction={this.closePopup}
        />
      </div>
    );
  }
}
export { PATH };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    authenticated: state.login.authenticated,
    testNameId: state.testStatus.currentTestId,
    isSuperUser: state.user.isSuperUser,
    isEtta: state.userPermissions.isEtta,
    isRdOperations: state.userPermissions.isRdOperations,
    isPpc: state.userPermissions.isPpc,
    isTa: state.userPermissions.isTa,
    isScorer: state.userPermissions.isScorer,
    isTb: state.userPermissions.isTb,
    isTd: state.userPermissions.isTd,
    currentHomePage: state.userPermissions.currentHomePage,
    username: state.user.username,
    activeTestPath: state.testStatus.activeTestPath,
    accommodations: state.accommodations,
    isTestInvalidated: state.testStatus.isTestInvalidated,
    triggerAppRerender: state.app.triggerAppRerender
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      authenticateAction,
      logoutAction,
      refreshAuthToken,
      getUserPermissions,
      updatePermissionsState,
      resetTestStatusState,
      resetSampleTestStatusState,
      resetNotepadState,
      resetPermissionsState,
      isTokenStillValid,
      getAssignedTests,
      updateCurrentHomePageState,
      setIsUserLoading,
      resetAccommodations,
      resetUserState,
      updateAssignedTestId,
      resetUserLookUpState
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(App);
