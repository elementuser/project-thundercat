import LOCALIZE from "../text_resources";
import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

export const LANGUAGES = {
  english: "en",
  french: "fr",
  bilingual: "bilingual"
};
export const LANGUAGE_IDS = {
  english: 1,
  french: 2
};
export const LANGUAGES_SHORT = {
  en: "en",
  fr: "fr"
};

// Action Types
export const SET_LANGUAGE = "localize/SET_LANGUAGE";
export const SET_LANGUAGE_DATA = "localize/SET_LANGUAGE_DATA";

// Action Creators
const setLanguage = language => {
  // Save the language preference in the browser so we don't have
  // to ask again.
  SessionStorage(ACTION.SET, ITEM.CAT_LANGUAGE, language);
  // Set the language of the string localizer.
  LOCALIZE.setLanguage(language);
  // Return the redux action object.
  return { type: SET_LANGUAGE, language };
};

const setLanguageData = languageData => ({
  type: SET_LANGUAGE_DATA,
  languageData
});

function getLanguageData() {
  return async function () {
    const response = await fetch("/oec-cat/api/get-language-data", {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseJson = await response.json();
    return responseJson;
  };
}

const initializeLanguage = () => {
  // Determine if a language is already saved to local storage.
  const language = SessionStorage(ACTION.GET, ITEM.CAT_LANGUAGE) || "";
  // Set the default language of the string localizer.
  LOCALIZE.setLanguage(language);
  return language;
};

// Initial State
// language: string in LANGUAGES, initially empty until a language is selected.
const initialState = {
  language: initializeLanguage(),
  languageData: {}
};

// Reducer
const localize = (state = initialState, action) => {
  switch (action.type) {
    case SET_LANGUAGE:
      return {
        ...state,
        language: action.language
      };
    case SET_LANGUAGE_DATA:
      return {
        ...state,
        languageData: action.languageData
      };

    default:
      return state;
  }
};

export default localize;
export { setLanguage, setLanguageData, initialState, getLanguageData };
