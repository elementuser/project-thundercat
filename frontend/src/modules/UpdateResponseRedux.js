import SessionStorage, { ACTION, ITEM } from "../SessionStorage";
import getBackendAndDbStatus from "./helpers";

// Action Types
export const SET_BACKEND_AND_DB_STATUS_STATE = "response_redux/SET_BACKEND_AND_DB_STATUS_STATE";

const updateBackendAndDbStatusState = isBackendAndDbUp => ({
  type: SET_BACKEND_AND_DB_STATUS_STATE,
  isBackendAndDbUp
});

function seenUitQuestion(assignedTestId, questionId, testSectionComponentId) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/uit-seen-question/?assigned_test_id=${assignedTestId}&question_id=${questionId}&test_section_component_id=${testSectionComponentId}`,
      {
        method: "POST",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    return response;
  };
}

function saveUitTestResponse(
  assignedTestId,
  questionId,
  answerId,
  testSectionComponentId,
  selectedLanguage
) {
  return async function () {
    // checking if backend and DB are up and running
    const backendIsUp = await getBackendAndDbStatus();
    if (backendIsUp) {
      const response = await fetch(
        `/oec-cat/api/uit-save-answer/?assigned_test_id=${assignedTestId}&question_id=${questionId}&answer_id=${answerId}&test_section_component_id=${testSectionComponentId}&selected_language_id=${selectedLanguage}`,
        {
          method: "POST",
          headers: {
            Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
            Accept: "application/json",
            "Content-Type": "application/json",
            cache: "default"
          }
        }
      );
      return response;
    }
    // service unavailable
    return { status: 503 };
  };
}

function getTestResponses(assigned_test_id, test_section_component_id) {
  return async function () {
    const url = `/oec-cat/api/get-test-answers/?assigned_test_id=${assigned_test_id}&test_section_component_id=${test_section_component_id}`;
    const tests = await fetch(url, {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const response = {
      body: await tests.json(),
      ok: await tests.ok
    };
    return response;
  };
}

function updateCandidateAnswerModifyDate(assigned_test_id, question_id, test_section_component_id) {
  return async function () {
    const url = `/oec-cat/api/update-candidate-answer-modify-date/?assigned_test_id=${assigned_test_id}&question_id=${question_id}&test_section_component_id=${test_section_component_id}`;
    const response = await fetch(url, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    return response;
  };
}

// New Email Inbox functions
// if you update these statuses, don't forget to also update
// "UpdateEmailAnswerType" in ".../backend/static/update_email_answer_type.py" file (backend)
export const UPDATE_EMAIL_ACTIONS = {
  ADD_EMAIL: 1,
  UPDATE_EMAIL: 2,
  DELETE_EMAIL: 3,
  ADD_TASK: 4,
  UPDATE_TASK: 5,
  DELETE_TASK: 6
};

function updateEmailTestResponse(OPERATION, assignedTestId, questionId, answerId, answerObj = {}) {
  const newObj = {
    answerObj,
    assignedTestId,
    questionId,
    answerId,
    OPERATION
  };

  return async function () {
    const response = await fetch(`/oec-cat/api/update-email-answer/`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(newObj)
    });
    const responseObj = {
      body: await response.json(),
      ok: response.ok
    };
    return responseObj;
  };
}

// Initial State
const initialState = {
  isBackendAndDbUp: true
};

// Reducer
const testActions = (state = initialState, action) => {
  switch (action.type) {
    case SET_BACKEND_AND_DB_STATUS_STATE:
      return {
        ...state,
        isBackendAndDbUp: action.isBackendAndDbUp
      };
    default:
      return state;
  }
};

export default testActions;
export {
  seenUitQuestion,
  saveUitTestResponse,
  getTestResponses,
  updateEmailTestResponse,
  updateCandidateAnswerModifyDate,
  updateBackendAndDbStatusState
};
