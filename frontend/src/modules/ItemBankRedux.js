/* eslint-disable no-sequences */
/* eslint-disable no-restricted-globals */
import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// constant strings for the python data object
export const ITEM_BANK_DEFINITION = "item_bank_definition";
export const ITEM_BANK_ATTRIBUTES = "item_bank_attributes";

// Action Types
const SET_ITEM_BANKS_PAGINATION_PAGE = "itemBank/SET_PERMISSIONS_PAGINATION_PAGE";
const SET_ITEM_BANKS_PAGINATION_PAGE_SIZE = "itemBank/SET_ITEM_BANKS_PAGINATION_PAGE_SIZE";
const SET_ITEM_BANKS_SEARCH_PARAMETERS = "itemBank/SET_ITEM_BANKS_SEARCH_PARAMETERS";
const SET_ITEM_BANK_DATA = "itemBank/SET_ITEM_BANK_DATA";
const SET_ITEM_BANK_INITIAL_DATA = "itemBank/SET_ITEM_BANK_INITIAL_DATA";
const SET_ITEM_BANK_ATTRIBUTES_INITIAL_DATA = "itemBank/SET_ITEM_BANK_ATTRIBUTES_INITIAL_DATA";
const ADD_FIELD = "itemBank/ADD_FIELD";
const MODIFY_FIELD = "itemBank/MODIFY_FIELD";
const REPLACE_FIELD = "itemBank/REPLACE_FIELD";
const SET_ITEM_BANK_SIDE_NAV_STATE = "itemBank/SET_ITEM_BANK_SIDE_NAV_STATE";
const SET_ITEMS_PAGINATION_PAGE = "itemBank/SET_ITEMS_PAGINATION_PAGE";
const SET_ITEMS_PAGINATION_PAGE_SIZE = "itemBank/SET_ITEMS_PAGINATION_PAGE_SIZE";
const SET_ITEMS_SEARCH_PARAMETERS = "itemBank/SET_ITEMS_SEARCH_PARAMETERS";
const SET_ITEM_DATA = "itemBank/SET_ITEM_DATA";
const SET_ITEM_INITIAL_DATA = "itemBank/SET_ITEM_INITIAL_DATA";
const SET_ITEM_VALIDATION_ERRORS = "itemBank/SET_ITEM_VALIDATION_ERRORS";
const SET_FIRST_COLUMN_TAB = "itemBank/SET_FIRST_COLUMN_TAB";
const SET_SECOND_COLUMN_TAB = "itemBank/SET_SECOND_COLUMN_TAB";
const SET_THIRD_COLUMN_TAB = "itemBank/SET_THIRD_COLUMN_TAB";
const SET_CONDITIONAL_CONTENT_TABS_DATA = "itemBank/SET_CONDITIONAL_CONTENT_TABS_DATA";
const GET_AND_SET_SELECTED_ITEM = "itemBank/GET_AND_SET_SELECTED_ITEM";
const DATA_CHANGES_DETECTED = "itemBank/DATA_CHANGES_DETECTED";
const SET_DATA_READY_FOR_UPLOAD = "itemBank/SET_DATA_READY_FOR_UPLOAD";
const SET_VALID_BUNDLE_ITEMS_BASIC_RULE_STATE = "itemBank/SET_VALID_BUNDLE_ITEMS_BASIC_RULE_STATE";
const SET_VALID_BUNDLE_BUNDLES_RULE_STATE = "itemBank/SET_VALID_BUNDLE_BUNDLES_RULE_STATE";
const SET_SELECTED_CONTENT_LANGUAGE = "itemBank/SET_SELECTED_CONTENT_LANGUAGE";
const SET_SELECTED_CONTENT_SCREEN_READER_LANGUAGE =
  "itemBank/SET_SELECTED_CONTENT_SCREEN_READER_LANGUAGE";
const SET_UPLOAD_ITEM_DATA_PARAMETERS = "itemBank/SET_UPLOAD_ITEM_DATA_PARAMETERS";
const SET_BUNDLES_PAGINATION_PAGE = "itemBank/SET_BUNDLES_PAGINATION_PAGE";
const SET_BUNDLES_PAGINATION_PAGE_SIZE = "itemBank/SET_BUNDLES_PAGINATION_PAGE_SIZE";
const SET_BUNDLES_SEARCH_PARAMETERS = "itemBank/SET_BUNDLES_SEARCH_PARAMETERS";
const SET_BUNDLE_DATA = "itemBank/SET_BUNDLE_DATA";
const SET_BUNDLE_LOCAL_UNLINKED_SYSTEM_IDS = "itemBank/SET_BUNDLE_LOCAL_UNLINKED_SYSTEM_IDS";
const SET_BUNDLE_INITIAL_DATA = "itemBank/SET_BUNDLE_INITIAL_DATA";
const SET_BUNDLES_ADD_ITEMS_PAGINATION_PAGE = "itemBank/SET_BUNDLES_ADD_ITEMS_PAGINATION_PAGE";
const SET_BUNDLES_ADD_ITEMS_PAGINATION_PAGE_SIZE =
  "itemBank/SET_BUNDLES_ADD_ITEMS_PAGINATION_PAGE_SIZE";
const SET_BUNDLES_ADD_ITEMS_SEARCH_PARAMETERS = "itemBank/SET_BUNDLES_ADD_ITEMS_SEARCH_PARAMETERS";
const SET_BUNDLES_ADD_BUNDLES_PAGINATION_PAGE = "itemBank/SET_BUNDLES_ADD_BUNDLES_PAGINATION_PAGE";
const SET_BUNDLES_ADD_BUNDLES_PAGINATION_PAGE_SIZE =
  "itemBank/SET_BUNDLES_ADD_BUNDLES_PAGINATION_PAGE_SIZE";
const SET_BUNDLES_ADD_BUNDLES_SEARCH_PARAMETERS =
  "itemBank/SET_BUNDLES_ADD_BUNDLES_SEARCH_PARAMETERS";
const TRIGGER_BUNDLES_TABLE_RERENDER = "itemBank/TRIGGER_BUNDLES_TABLE_RERENDER";
const ITEMS_TABLE_RENDERING = "itemBank/ITEMS_TABLE_RENDERING";
const RESET_ITEM_BANKS_STATES = "itemBank/RESET_ITEM_BANKS_STATES";
const RESET_ITEMS_STATES = "itemBank/RESET_ITEMS_STATES";
const RESET_BUNDLES_STATES = "itemBank/RESET_BUNDLES_STATES";
const RESET_BUNDLES_ADD_ITEMS_STATES = "itemBank/RESET_BUNDLES_ADD_ITEMS_STATES";
const RESET_BUNDLES_ADD_BUNDLES_STATES = "itemBank/RESET_BUNDLES_ADD_BUNDLES_STATES";
const RESET_ITEMS_VALIDATION_ERROR_STATES = "itemBank/RESET_ITEMS_VALIDATION_ERROR_STATES";
const RESET_TOP_TABS_STATES = "itemBank/RESET_TOP_TABS_STATES";
const RESET_ALL_STATES = "itemBank/RESET_ALL_STATES";

const updateItemBanksPageState = itemBanksPaginationPage => ({
  type: SET_ITEM_BANKS_PAGINATION_PAGE,
  itemBanksPaginationPage
});

const updateItemBanksPageSizeState = itemBanksPaginationPageSize => ({
  type: SET_ITEM_BANKS_PAGINATION_PAGE_SIZE,
  itemBanksPaginationPageSize
});

const updateSearchItemBanksStates = (itemBanksKeyword, itemBanksActiveSearch) => ({
  type: SET_ITEM_BANKS_SEARCH_PARAMETERS,
  itemBanksKeyword,
  itemBanksActiveSearch
});

const setItemBankData = item_bank_data => ({
  type: SET_ITEM_BANK_DATA,
  item_bank_data
});

const setItemBankInitialData = itemBankInitialData => ({
  type: SET_ITEM_BANK_INITIAL_DATA,
  itemBankInitialData
});

const setItemBankAttributesInitialData = itemBankAttributesInitialData => ({
  type: SET_ITEM_BANK_ATTRIBUTES_INITIAL_DATA,
  itemBankAttributesInitialData
});

const addItemBankField = (rootObj, newField, reorderBy = undefined) => ({
  type: ADD_FIELD,
  rootObj,
  newField,
  reorderBy
});

const modifyItemBankField = (rootObj, newField, id, reorderBy = undefined) => ({
  type: MODIFY_FIELD,
  rootObj,
  newField,
  id,
  reorderBy
});

const replaceItemBankField = (rootObj, newField, reorderBy = undefined) => ({
  type: REPLACE_FIELD,
  rootObj,
  newField,
  reorderBy
});

const setItemBankSideNavState = selectedSideNavItem => ({
  type: SET_ITEM_BANK_SIDE_NAV_STATE,
  selectedSideNavItem
});

const updateItemsPageState = itemsPaginationPage => ({
  type: SET_ITEMS_PAGINATION_PAGE,
  itemsPaginationPage
});

const updateItemsPageSizeState = itemsPaginationPageSize => ({
  type: SET_ITEMS_PAGINATION_PAGE_SIZE,
  itemsPaginationPageSize
});

const updateSearchItemsStates = (itemsKeyword, itemsActiveSearch) => ({
  type: SET_ITEMS_SEARCH_PARAMETERS,
  itemsKeyword,
  itemsActiveSearch
});

const setItemData = (itemData, languageData = []) => ({
  type: SET_ITEM_DATA,
  itemData,
  languageData
});

const setItemInitialData = (itemInitialData, languageData = []) => ({
  type: SET_ITEM_INITIAL_DATA,
  itemInitialData,
  languageData
});

const setItemValidationErrors = itemValidationErrors => ({
  type: SET_ITEM_VALIDATION_ERRORS,
  itemValidationErrors
});

const setFirstColumnTab = firstColumnTab => ({
  type: SET_FIRST_COLUMN_TAB,
  firstColumnTab
});

const setSecondColumnTab = secondColumnTab => ({
  type: SET_SECOND_COLUMN_TAB,
  secondColumnTab
});

const setThirdColumnTab = thirdColumnTab => ({
  type: SET_THIRD_COLUMN_TAB,
  thirdColumnTab
});

const setConditionalContentTabsData = conditionalContentTabsData => ({
  type: SET_CONDITIONAL_CONTENT_TABS_DATA,
  conditionalContentTabsData
});

const getAndSetSelectedItem = itemData => ({
  type: GET_AND_SET_SELECTED_ITEM,
  itemData
});

const setDataChangesDetected = dataChangesDetected => ({
  type: DATA_CHANGES_DETECTED,
  dataChangesDetected
});

const setDataReadyForUpload = dataReadyForUpload => ({
  type: SET_DATA_READY_FOR_UPLOAD,
  dataReadyForUpload
});

const setValidBundleItemsBasicRuleState = validBundleItemsBasicRuleState => ({
  type: SET_VALID_BUNDLE_ITEMS_BASIC_RULE_STATE,
  validBundleItemsBasicRuleState
});

const setValidBundleBundlesRuleState = validBundleBundlesRuleState => ({
  type: SET_VALID_BUNDLE_BUNDLES_RULE_STATE,
  validBundleBundlesRuleState
});

const setSelectedContentLanguage = selectedContentLanguage => ({
  type: SET_SELECTED_CONTENT_LANGUAGE,
  selectedContentLanguage
});

const setSelectedContentScreenReaderLanguage = selectedContentScreenReaderLanguage => ({
  type: SET_SELECTED_CONTENT_SCREEN_READER_LANGUAGE,
  selectedContentScreenReaderLanguage
});

const setUploadItemDataParameters = uploadItemDataParameters => ({
  type: SET_UPLOAD_ITEM_DATA_PARAMETERS,
  uploadItemDataParameters
});

const updateBundlesPageState = bundlesPaginationPage => ({
  type: SET_BUNDLES_PAGINATION_PAGE,
  bundlesPaginationPage
});

const updateBundlesPageSizeState = bundlesPaginationPageSize => ({
  type: SET_BUNDLES_PAGINATION_PAGE_SIZE,
  bundlesPaginationPageSize
});

const updateSearchBundlesStates = (bundlesKeyword, bundlesActiveSearch) => ({
  type: SET_BUNDLES_SEARCH_PARAMETERS,
  bundlesKeyword,
  bundlesActiveSearch
});

const setBundleData = bundleData => ({
  type: SET_BUNDLE_DATA,
  bundleData
});

const setBundleLocalUnlinkedSystemIds = bundleLocalUnlinkedSystemIds => ({
  type: SET_BUNDLE_LOCAL_UNLINKED_SYSTEM_IDS,
  bundleLocalUnlinkedSystemIds
});

const setBundleInitialData = bundleInitialData => ({
  type: SET_BUNDLE_INITIAL_DATA,
  bundleInitialData
});

const updateBundlesAddItemsPageState = bundlesAddItemsPaginationPage => ({
  type: SET_BUNDLES_ADD_ITEMS_PAGINATION_PAGE,
  bundlesAddItemsPaginationPage
});

const updateBundlesAddItemsPageSizeState = bundlesAddItemsPaginationPageSize => ({
  type: SET_BUNDLES_ADD_ITEMS_PAGINATION_PAGE_SIZE,
  bundlesAddItemsPaginationPageSize
});

const updateSearchAddItemsBundlesStates = (
  bundlesAddItemsKeyword,
  bundlesAddItemsActiveSearch
) => ({
  type: SET_BUNDLES_ADD_ITEMS_SEARCH_PARAMETERS,
  bundlesAddItemsKeyword,
  bundlesAddItemsActiveSearch
});

const updateBundlesAddBundlesPageState = bundlesAddBundlesPaginationPage => ({
  type: SET_BUNDLES_ADD_BUNDLES_PAGINATION_PAGE,
  bundlesAddBundlesPaginationPage
});

const updateBundlesAddBundlesPageSizeState = bundlesAddBundlesPaginationPageSize => ({
  type: SET_BUNDLES_ADD_BUNDLES_PAGINATION_PAGE_SIZE,
  bundlesAddBundlesPaginationPageSize
});

const updateSearchAddBundlesBundlesStates = (
  bundlesAddBundlesKeyword,
  bundlesAddBundlesActiveSearch
) => ({
  type: SET_BUNDLES_ADD_BUNDLES_SEARCH_PARAMETERS,
  bundlesAddBundlesKeyword,
  bundlesAddBundlesActiveSearch
});

const triggerBundlesTableRerender = () => ({
  type: TRIGGER_BUNDLES_TABLE_RERENDER
});

const setItemsTableRendering = status => ({
  type: ITEMS_TABLE_RENDERING,
  status
});

const resetItemBanksStates = () => ({
  type: RESET_ITEM_BANKS_STATES
});

const resetItemsStates = () => ({
  type: RESET_ITEMS_STATES
});

const resetBundlesStates = () => ({
  type: RESET_BUNDLES_STATES
});

const resetBundlesAddItemsStates = () => ({
  type: RESET_BUNDLES_ADD_ITEMS_STATES
});

const resetBundlesAddBundlesStates = () => ({
  type: RESET_BUNDLES_ADD_BUNDLES_STATES
});

const resetItemsValidationErrorStates = () => ({
  type: RESET_ITEMS_VALIDATION_ERROR_STATES
});

const resetTopTabsStates = () => ({
  type: RESET_TOP_TABS_STATES
});

const resetAllItemBanksStates = () => ({
  type: RESET_ALL_STATES
});

const sortByOrder = (a, b) => (a.order > b.order ? 1 : -1);

const orderByCustomAttribute = orderByAttribute => {
  return function (a, b) {
    const firstElement = isNaN(a[orderByAttribute])
      ? a[orderByAttribute]
      : parseInt(a[orderByAttribute]);
    const secondElement = isNaN(b[orderByAttribute])
      ? b[orderByAttribute]
      : parseInt(b[orderByAttribute]);
    if (firstElement < secondElement) {
      return -1;
    }
    if (firstElement > secondElement) {
      return 1;
    }
    return 0;
  };
};

// getting all item banks
function getAllItemBanks(callSource, page, pageSize) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-all-item-banks?call_source=${callSource}&page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// getting found item banks
function getFoundItemBanks(providedKeyword, currentLanguage, callSource, page, pageSize) {
  // if provided keyword is empty
  let keyword = providedKeyword;
  if (keyword === "") {
    // send a space as the keyword parameter
    keyword = " ";
  }

  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-found-item-banks?keyword=${keyword}&current_language=${currentLanguage}&call_source=${callSource}&page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// getting all item banks for the Test Builder
function getItemBanksForTestBuilder() {
  return async function () {
    const response = await fetch("/oec-cat/api/get-item-banks-for-test-builder", {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseJson = await response.json();
    return responseJson;
  };
}

// getting item bank bundles for the Test Builder
function getItemBankBundlesForTestBuilder(itemBankId) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-item-bank-bundles-for-test-builder?item_bank_id=${itemBankId}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// getting selected item bank data
function getSelectedItemBankData(customItemBankId) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-selected-item-bank-data?custom_item_bank_id=${customItemBankId}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// updating item bank data
function updateItemBankData(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/update-item-bank-data", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    try {
      const responseJson = await response.json();
      return responseJson;
    } catch {
      return response;
    }
  };
}

// getting all items
function getAllItems(itemBankId, page, pageSize, ignoreDraft) {
  let ignoreDraftParam = false;
  if (typeof ignoreDraft !== "undefined") {
    ignoreDraftParam = ignoreDraft;
  }
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-all-items?item_bank_id=${itemBankId}&page=${page}&page_size=${pageSize}&ignore_draft=${ignoreDraftParam}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// getting found items
function getFoundItems(itemBankId, currentLanguage, providedKeyword, page, pageSize) {
  // if provided keyword is empty
  let keyword = providedKeyword;
  if (keyword === "") {
    // send a space as the keyword parameter
    keyword = " ";
  }

  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-found-items?item_bank_id=${itemBankId}&current_language=${currentLanguage}&keyword=${keyword}&page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// getting specified item data
function getItemData(itemId, ignoreDraft) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-item-data?item_id=${itemId}&ignore_draft=${ignoreDraft}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// getting selected item version data
function getSelectedItemVersionData(itemId, systemId) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-selected-item-version-data?item_id=${itemId}&system_id=${systemId}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// updating specified item data
function updateItemData(data) {
  return async function () {
    const response = await fetch(`/oec-cat/api/update-item-data`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    if (response.status !== 304 && response.status !== 409) {
      const responseJson = await response.json();
      return responseJson;
    }
    return response;
  };
}

// deleting specified item draft
function deleteItemDraft(data) {
  return async function () {
    const response = await fetch(`/oec-cat/api/delete-item-draft`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    return response;
  };
}

// getting response formats
function getResponseFormats() {
  return async function () {
    const response = await fetch(`/oec-cat/api/get-response-formats`, {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseJson = await response.json();
    return responseJson;
  };
}

// getting development statuses
function getDevelopmentStatuses() {
  return async function () {
    const response = await fetch(`/oec-cat/api/get-development-statuses`, {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseJson = await response.json();
    return responseJson;
  };
}

// creating new item
function createNewItem(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/create-new-item", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    if (response.status === 409) {
      return response;
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// creating item comment
function createItemComment(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/create-item-comment", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    const responseJson = await response.json();
    return responseJson;
  };
}

// deleting item comment
function deleteItemComment(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/delete-item-comment", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    const responseJson = await response.json();
    return responseJson;
  };
}

// getting all bundles
function getAllBundles(itemBankId, page, pageSize) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-all-bundles?item_bank_id=${itemBankId}&page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// getting specific bundle
function getSpecificBundle(bundleId) {
  return async function () {
    const response = await fetch(`/oec-cat/api/get-specific-bundle?bundle_id=${bundleId}`, {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseJson = await response.json();
    return responseJson;
  };
}

// getting found bundles
function getFoundBundles(itemBankId, providedKeyword, page, pageSize) {
  // Encode search keyword so we can use special characters
  let keyword = encodeURIComponent(providedKeyword);

  // if provided keyword is empty
  keyword = providedKeyword;
  if (keyword === "") {
    // send a space as the keyword parameter
    keyword = " ";
  }

  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-found-bundles?item_bank_id=${itemBankId}&keyword=${keyword}&page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// creating new bundle
function createNewBundle(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/create-new-bundle", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    if (response.status === 409) {
      return response;
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// uploading bundle data
function uploadBundle(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/upload-bundle", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    const responseJson = await response.json();
    return responseJson;
  };
}

// getting all items that are ready to be added to specific bundle
function getItemsReadyToBeAddedToBundle(
  itemBankId,
  bundleId,
  allLinkedItemSystemIds,
  allLocalUnlinkedItemSystemIds,
  defaultSearchCriterias,
  customSearchCriterias,
  page,
  pageSize
) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-items-ready-to-be-added-to-bundle?item_bank_id=${itemBankId}&bundle_id=${bundleId}&all_linked_item_system_ids=${allLinkedItemSystemIds}&all_local_unlinked_item_system_ids=${allLocalUnlinkedItemSystemIds}&default_search_criterias=${defaultSearchCriterias}&custom_search_criterias=${customSearchCriterias}&page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// getting found items that are ready to be added to specific bundle
function getFoundItemsReadyToBeAddedToBundle(
  itemBankId,
  bundleId,
  allLinkedItemSystemIds,
  allLocalUnlinkedItemSystemIds,
  defaultSearchCriterias,
  customSearchCriterias,
  providedKeyword,
  currentLanguage,
  page,
  pageSize
) {
  // Encode search keyword so we can use special characters
  let keyword = encodeURIComponent(providedKeyword);

  // if provided keyword is empty
  keyword = providedKeyword;
  if (keyword === "") {
    // send a space as the keyword parameter
    keyword = " ";
  }

  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-items-found-ready-to-be-added-to-bundle?item_bank_id=${itemBankId}&bundle_id=${bundleId}&all_linked_item_system_ids=${allLinkedItemSystemIds}&all_local_unlinked_item_system_ids=${allLocalUnlinkedItemSystemIds}&default_search_criterias=${defaultSearchCriterias}&custom_search_criterias=${customSearchCriterias}&keyword=${keyword}&current_language=${currentLanguage}&page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// getting all bundles that are ready to be added to specific bundle
function getBundlesReadyToBeAddedToBundle(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/get-bundles-ready-to-be-added-to-bundle", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    const responseJson = await response.json();
    return responseJson;
  };
}

// getting found bundles that are ready to be added to specific bundle
function getFoundBundlesReadyToBeAddedToBundle(data) {
  // Encode search keyword so we can use special characters
  // eslint-disable-next-line no-param-reassign
  data.keyword = encodeURIComponent(data.keyword);

  return async function () {
    const response = await fetch("/oec-cat/api/get-all-found-bundles-ready-to-be-added-to-bundle", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    const responseJson = await response.json();
    return responseJson;
  };
}

// preview generated items list from specified bundle
function previewGeneratedItemsListFromBundle(bundleData) {
  return async function () {
    const response = await fetch(`/oec-cat/api/preview-generated-items-list-from-bundle`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(bundleData)
    });
    const responseJson = await response.json();
    return responseJson;
  };
}

// getting all bundles that are ready to be added to specific bundle
function getGeneratedItemsListFromBundle(bundleRulesData) {
  return async function () {
    const response = await fetch("/oec-cat/api/get-generated-items-list-from-bundle", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(bundleRulesData)
    });
    const responseJson = await response.json();
    return responseJson;
  };
}

// Initial Item Bank Editor States
const initialItemBankEditorStates = {
  item_bank_data: {
    item_bank_definition: [
      {
        id: null,
        custom_item_bank_id: "",
        en_name: "",
        fr_name: ""
      }
    ],
    item_bank_attributes: [],
    item_bank_items: [],
    item_bank_number_of_found_items: 0
  },
  triggerItemBankFieldUpdates: false,
  selectedContentLanguage: [],
  selectedContentScreenReaderLanguage: [],
  uploadItemDataParameters: {}
};

// Initial Items States
const initialItemsStates = {
  itemsPaginationPage: 1,
  itemsPaginationPageSize: 25,
  itemsKeyword: "",
  itemsActiveSearch: false,
  itemData: {},
  itemInitialData: {}
};

// Initial Items States
const initialBundlesStates = {
  bundlesPaginationPage: 1,
  bundlesPaginationPageSize: 25,
  bundlesKeyword: "",
  bundlesActiveSearch: false,
  bundleData: {},
  bundleLocalUnlinkedSystemIds: [],
  bundleInitialData: {}
};

const initialBundlesAddItemsStates = {
  bundlesAddItemsPaginationPage: 1,
  bundlesAddItemsPaginationPageSize: 25,
  bundlesAddItemsKeyword: "",
  bundlesAddItemsActiveSearch: false
};

const initialBundlesAddBundlesStates = {
  bundlesAddBundlesPaginationPage: 1,
  bundlesAddBundlesPaginationPageSize: 25,
  bundlesAddBundlesKeyword: "",
  bundlesAddBundlesActiveSearch: false
};

// Initial Item Validation Error States
const initialItemValidationErrorStates = {
  itemValidationErrors: {
    historicalIdAlreadyExists: false
  }
};

// Initial Top Tabs States
const initialTopTabsStates = {
  firstColumnTab: "",
  secondColumnTab: "",
  thirdColumnTab: "",
  conditionalContentTabsData: []
};

// Initial State
const initialState = {
  itemBanksPaginationPage: 1,
  itemBanksPaginationPageSize: 25,
  itemBanksKeyword: "",
  itemBanksActiveSearch: false,
  selectedSideNavItem: 0,
  dataChangesDetected: false,
  dataReadyForUpload: false,
  validBundleItemsBasicRuleState: true,
  validBundleBundlesRuleState: true,
  selectedBundleChangesDetected: false,
  itemBankAttributesInitialData: {},
  itemBankInitialData: {},
  triggerBundlesTableRerender: false,
  itemsTableRendering: false,
  ...initialTopTabsStates,
  ...initialItemBankEditorStates,
  ...initialItemsStates,
  ...initialBundlesStates,
  ...initialBundlesAddItemsStates,
  ...initialBundlesAddBundlesStates,
  ...initialItemValidationErrorStates
};

// Reducer
const itemBank = (state = initialState, action) => {
  let newState = {
    ...state
  };
  let newStateArray = [];
  switch (action.type) {
    case SET_ITEM_BANKS_PAGINATION_PAGE:
      return {
        ...state,
        itemBanksPaginationPage: action.itemBanksPaginationPage
      };
    case SET_ITEM_BANKS_PAGINATION_PAGE_SIZE:
      return {
        ...state,
        itemBanksPaginationPageSize: action.itemBanksPaginationPageSize
      };
    case SET_ITEM_BANKS_SEARCH_PARAMETERS:
      return {
        ...state,
        itemBanksKeyword: action.itemBanksKeyword,
        itemBanksActiveSearch: action.itemBanksActiveSearch
      };
    case SET_ITEM_BANK_DATA:
      return {
        ...state,
        item_bank_data: action.item_bank_data
      };
    case SET_ITEM_BANK_INITIAL_DATA:
      return {
        ...state,
        itemBankInitialData: action.itemBankInitialData
      };
    case SET_ITEM_BANK_ATTRIBUTES_INITIAL_DATA:
      return {
        ...state,
        itemBankAttributesInitialData: action.itemBankAttributesInitialData
      };
    case SET_ITEMS_PAGINATION_PAGE:
      return {
        ...state,
        itemsPaginationPage: action.itemsPaginationPage
      };
    case SET_ITEMS_PAGINATION_PAGE_SIZE:
      return {
        ...state,
        itemsPaginationPageSize: action.itemsPaginationPageSize
      };
    case SET_ITEMS_SEARCH_PARAMETERS:
      return {
        ...state,
        itemsKeyword: action.itemsKeyword,
        itemsActiveSearch: action.itemsActiveSearch
      };
    case ADD_FIELD:
      newStateArray = [];
      newState = {
        ...state,
        triggerItemBankFieldUpdates: !state.triggerItemBankFieldUpdates
      };
      newStateArray = newState.item_bank_data[`${action.rootObj}`];

      // if newStateArray is undefined
      if (typeof newStateArray === "undefined") {
        // initialize it with an empty array
        newStateArray = [];
      }

      newStateArray.push(action.newField);

      // re-order based on obj property
      if (action.reorderBy !== undefined) {
        newStateArray = newStateArray.sort(orderByCustomAttribute(action.reorderBy));
      }
      // end re-ordering
      else {
        newStateArray.sort(sortByOrder);
      }
      // trigger new state
      const newArray = newStateArray.map(x => {
        return x;
      });
      newState.item_bank_data[`${action.rootObj}`] = newArray;
      return {
        ...newState
      };
    case MODIFY_FIELD:
      newStateArray = [];
      newState = {
        ...state,
        triggerItemBankFieldUpdates: !state.triggerItemBankFieldUpdates
      };
      let modified = false;
      newStateArray = newState.item_bank_data[`${action.rootObj}`].map(content => {
        if (content.id === action.id) {
          modified = true;
          return action.newField;
        }
        return content;
      });
      if (!modified) {
        newStateArray.push(action.newField);
      }

      // re-order based on obj property
      if (action.reorderBy !== undefined) {
        newStateArray = newStateArray.sort(orderByCustomAttribute(action.reorderBy));
      }
      // end re-ordering
      else {
        // eslint-disable-next-line no-lonely-if
        if (action.newField.order !== undefined) {
          newStateArray.sort(sortByOrder);
        }
      }

      newState.item_bank_data[`${action.rootObj}`] = newStateArray;
      return {
        ...newState
      };
    case REPLACE_FIELD:
      newStateArray = action.newField;
      newState = {
        ...state,
        triggerItemBankFieldUpdates: !state.triggerItemBankFieldUpdates
      };

      // re-order based on obj property
      if (action.reorderBy !== undefined) {
        newStateArray = newStateArray.sort(orderByCustomAttribute(action.reorderBy));
      }
      // end re-ordering
      else {
        newStateArray.sort(sortByOrder);
      }

      newState.item_bank_data[`${action.rootObj}`] = newStateArray;
      return {
        ...newState
      };
    case SET_ITEM_BANK_SIDE_NAV_STATE:
      return {
        ...state,
        selectedSideNavItem: action.selectedSideNavItem
      };
    case SET_ITEM_DATA:
      const itemDataVar = action.itemData;

      // ==================== ORDERING ITEM STEMS/OPTIONS ====================
      // looping in all available languages
      for (let i = 0; i < action.languageData.length; i++) {
        // item content of language of current iteration is defined and contains data
        if (
          typeof itemDataVar.item_content !== "undefined" &&
          typeof itemDataVar.item_content[action.languageData[i].ISO_Code_1] !== "undefined" &&
          itemDataVar.item_content[action.languageData[i].ISO_Code_1].length > 0
        ) {
          // sorting item content
          itemDataVar.item_content[action.languageData[i].ISO_Code_1].sort(
            (a, b) => a.content_type - b.content_type || a.content_order - b.content_order
          );
        }
        // item options of language of current iteration is defined and contains data
        if (
          typeof itemDataVar.item_options !== "undefined" &&
          typeof itemDataVar.item_options[action.languageData[i].ISO_Code_1] !== "undefined" &&
          itemDataVar.item_options[action.languageData[i].ISO_Code_1].length > 0
        ) {
          // sorting item options
          itemDataVar.item_options[action.languageData[i].ISO_Code_1].sort(
            (a, b) => a.option_type - b.option_type || a.option_order - b.option_order
          );
        }
      }
      // ==================== ORDERING ITEM STEMS/OPTIONS (END) ====================

      return {
        ...state,
        itemData: itemDataVar
      };
    case SET_ITEM_INITIAL_DATA:
      const itemInitialDataVar = action.itemInitialData;

      // ==================== ORDERING ITEM STEMS/OPTIONS ====================
      // looping in all available languages
      for (let i = 0; i < action.languageData.length; i++) {
        // item content of language of current iteration is defined and contains data
        if (
          typeof itemInitialDataVar.item_content !== "undefined" &&
          typeof itemInitialDataVar.item_content[action.languageData[i].ISO_Code_1] !==
            "undefined" &&
          itemInitialDataVar.item_content[action.languageData[i].ISO_Code_1].length > 0
        ) {
          // sorting item content
          itemInitialDataVar.item_content[action.languageData[i].ISO_Code_1].sort(
            (a, b) => a.content_type - b.content_type || a.content_order - b.content_order
          );
        }
        // item options of language of current iteration is defined and contains data
        if (
          typeof itemInitialDataVar.item_options !== "undefined" &&
          typeof itemInitialDataVar.item_options[action.languageData[i].ISO_Code_1] !==
            "undefined" &&
          itemInitialDataVar.item_options[action.languageData[i].ISO_Code_1].length > 0
        ) {
          // sorting item options
          itemInitialDataVar.item_options[action.languageData[i].ISO_Code_1].sort(
            (a, b) => a.option_type - b.option_type || a.option_order - b.option_order
          );
        }
      }
      // ==================== ORDERING ITEM STEMS/OPTIONS (END) ====================

      return {
        ...state,
        itemInitialData: itemInitialDataVar
      };
    case SET_ITEM_VALIDATION_ERRORS:
      return {
        ...state,
        itemValidationErrors: {
          historicalIdAlreadyExists: action.itemValidationErrors.historicalIdAlreadyExists
        }
      };
    case SET_FIRST_COLUMN_TAB:
      return {
        ...state,
        firstColumnTab: action.firstColumnTab
      };
    case SET_SECOND_COLUMN_TAB:
      return {
        ...state,
        secondColumnTab: action.secondColumnTab
      };
    case SET_THIRD_COLUMN_TAB:
      return {
        ...state,
        thirdColumnTab: action.thirdColumnTab
      };
    case SET_CONDITIONAL_CONTENT_TABS_DATA:
      return {
        ...state,
        conditionalContentTabsData: action.conditionalContentTabsData
      };
    case GET_AND_SET_SELECTED_ITEM:
      // getting index of selected item
      const indexOfSelectedItem = state.item_bank_data.item_bank_items.findIndex(obj => {
        return obj.system_id === action.itemData.system_id;
      });
      const newSelectedItem = state.item_bank_data.item_bank_items[indexOfSelectedItem];
      return {
        ...state,
        itemData: newSelectedItem,
        itemInitialData: newSelectedItem
      };
    case DATA_CHANGES_DETECTED:
      return {
        ...state,
        dataChangesDetected: action.dataChangesDetected
      };
    case SET_DATA_READY_FOR_UPLOAD:
      return {
        ...state,
        dataReadyForUpload: action.dataReadyForUpload
      };
    case SET_VALID_BUNDLE_ITEMS_BASIC_RULE_STATE:
      return {
        ...state,
        validBundleItemsBasicRuleState: action.validBundleItemsBasicRuleState
      };
    case SET_VALID_BUNDLE_BUNDLES_RULE_STATE:
      return {
        ...state,
        validBundleBundlesRuleState: action.validBundleBundlesRuleState
      };
    case SET_SELECTED_CONTENT_LANGUAGE:
      return {
        ...state,
        selectedContentLanguage: action.selectedContentLanguage
      };
    case SET_SELECTED_CONTENT_SCREEN_READER_LANGUAGE:
      return {
        ...state,
        selectedContentScreenReaderLanguage: action.selectedContentScreenReaderLanguage
      };
    case SET_UPLOAD_ITEM_DATA_PARAMETERS:
      return {
        ...state,
        uploadItemDataParameters: action.uploadItemDataParameters
      };
    case SET_BUNDLES_PAGINATION_PAGE:
      return {
        ...state,
        bundlesPaginationPage: action.bundlesPaginationPage
      };
    case SET_BUNDLES_PAGINATION_PAGE_SIZE:
      return {
        ...state,
        bundlesPaginationPageSize: action.bundlesPaginationPageSize
      };
    case SET_BUNDLES_SEARCH_PARAMETERS:
      return {
        ...state,
        bundlesKeyword: action.bundlesKeyword,
        bundlesActiveSearch: action.bundlesActiveSearch
      };
    case SET_BUNDLE_DATA:
      return {
        ...state,
        bundleData: action.bundleData
      };
    case SET_BUNDLE_LOCAL_UNLINKED_SYSTEM_IDS:
      return {
        ...state,
        bundleLocalUnlinkedSystemIds: action.bundleLocalUnlinkedSystemIds
      };
    case SET_BUNDLE_INITIAL_DATA:
      return {
        ...state,
        bundleInitialData: action.bundleInitialData
      };
    case SET_BUNDLES_ADD_ITEMS_PAGINATION_PAGE:
      return {
        ...state,
        bundlesAddItemsPaginationPage: action.bundlesAddItemsPaginationPage
      };
    case SET_BUNDLES_ADD_ITEMS_PAGINATION_PAGE_SIZE:
      return {
        ...state,
        bundlesAddItemsPaginationPageSize: action.bundlesAddItemsPaginationPageSize
      };
    case SET_BUNDLES_ADD_ITEMS_SEARCH_PARAMETERS:
      return {
        ...state,
        bundlesAddItemsKeyword: action.bundlesAddItemsKeyword,
        bundlesAddItemsActiveSearch: action.bundlesAddItemsActiveSearch
      };
    case SET_BUNDLES_ADD_BUNDLES_PAGINATION_PAGE:
      return {
        ...state,
        bundlesAddBundlesPaginationPage: action.bundlesAddBundlesPaginationPage
      };
    case SET_BUNDLES_ADD_BUNDLES_PAGINATION_PAGE_SIZE:
      return {
        ...state,
        bundlesAddBundlesPaginationPageSize: action.bundlesAddBundlesPaginationPageSize
      };
    case SET_BUNDLES_ADD_BUNDLES_SEARCH_PARAMETERS:
      return {
        ...state,
        bundlesAddBundlesKeyword: action.bundlesAddBundlesKeyword,
        bundlesAddBundlesActiveSearch: action.bundlesAddBundlesActiveSearch
      };
    case TRIGGER_BUNDLES_TABLE_RERENDER:
      return {
        ...state,
        triggerBundlesTableRerender: !state.triggerBundlesTableRerender
      };
    case ITEMS_TABLE_RENDERING:
      return {
        ...state,
        itemsTableRendering: action.status
      };
    case RESET_ITEM_BANKS_STATES:
      return {
        ...state,
        ...initialItemBankEditorStates
      };
    case RESET_ITEMS_STATES:
      return {
        ...state,
        ...initialItemsStates
      };
    case RESET_BUNDLES_STATES:
      return {
        ...state,
        ...initialBundlesStates
      };
    case RESET_BUNDLES_ADD_ITEMS_STATES:
      return {
        ...state,
        ...initialBundlesAddItemsStates
      };
    case RESET_BUNDLES_ADD_BUNDLES_STATES:
      return {
        ...state,
        ...initialBundlesAddBundlesStates
      };
    case RESET_ITEMS_VALIDATION_ERROR_STATES:
      return {
        ...state,
        ...initialItemValidationErrorStates
      };
    case RESET_TOP_TABS_STATES:
      return {
        ...state,
        ...initialTopTabsStates
      };
    case RESET_ALL_STATES:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

export default itemBank;
export {
  initialState,
  getAllItemBanks,
  getFoundItemBanks,
  getItemBanksForTestBuilder,
  getItemBankBundlesForTestBuilder,
  getSelectedItemBankData,
  updateItemBankData,
  updateItemBanksPageState,
  updateItemBanksPageSizeState,
  updateSearchItemBanksStates,
  setItemBankData,
  addItemBankField,
  modifyItemBankField,
  replaceItemBankField,
  setItemBankSideNavState,
  updateItemsPageState,
  updateItemsPageSizeState,
  updateSearchItemsStates,
  resetItemBanksStates,
  resetItemsStates,
  resetAllItemBanksStates,
  getAllItems,
  getFoundItems,
  getItemData,
  getSelectedItemVersionData,
  getResponseFormats,
  getDevelopmentStatuses,
  createNewItem,
  updateItemData,
  createItemComment,
  deleteItemComment,
  setItemData,
  setItemInitialData,
  setItemValidationErrors,
  resetItemsValidationErrorStates,
  setFirstColumnTab,
  setSecondColumnTab,
  setThirdColumnTab,
  resetTopTabsStates,
  getAndSetSelectedItem,
  setDataChangesDetected,
  setDataReadyForUpload,
  setSelectedContentLanguage,
  setSelectedContentScreenReaderLanguage,
  deleteItemDraft,
  setUploadItemDataParameters,
  setConditionalContentTabsData,
  setItemBankAttributesInitialData,
  setItemBankInitialData,
  updateBundlesPageState,
  updateBundlesPageSizeState,
  updateSearchBundlesStates,
  resetBundlesStates,
  getAllBundles,
  getFoundBundles,
  createNewBundle,
  setBundleData,
  setBundleInitialData,
  uploadBundle,
  updateBundlesAddItemsPageState,
  updateBundlesAddItemsPageSizeState,
  updateSearchAddItemsBundlesStates,
  resetBundlesAddItemsStates,
  getItemsReadyToBeAddedToBundle,
  getFoundItemsReadyToBeAddedToBundle,
  setBundleLocalUnlinkedSystemIds,
  getSpecificBundle,
  updateBundlesAddBundlesPageState,
  updateBundlesAddBundlesPageSizeState,
  updateSearchAddBundlesBundlesStates,
  resetBundlesAddBundlesStates,
  getBundlesReadyToBeAddedToBundle,
  getFoundBundlesReadyToBeAddedToBundle,
  triggerBundlesTableRerender,
  setValidBundleItemsBasicRuleState,
  setValidBundleBundlesRuleState,
  previewGeneratedItemsListFromBundle,
  getGeneratedItemsListFromBundle,
  setItemsTableRendering
};
