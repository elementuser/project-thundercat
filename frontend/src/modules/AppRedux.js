// Action Types
const TRIGGER_APP_RERENDER = "app/TRIGGER_APP_RERENDER";

const triggerAppRerender = triggerAppRerender => ({
  type: TRIGGER_APP_RERENDER,
  triggerAppRerender
});

// Initial State
const initialState = {
  triggerAppRerender: false
};

const app = (state = initialState, action) => {
  switch (action.type) {
    case TRIGGER_APP_RERENDER:
      return {
        ...state,
        triggerAppRerender: !state.triggerAppRerender
      };
    default:
      return state;
  }
};

export default app;
export { triggerAppRerender };
