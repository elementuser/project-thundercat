import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// Action Types
export const SET_UIT_TEST_ORDER_NUMBER_SELECTED_VALUE =
  "uit/SET_UIT_TEST_ORDER_NUMBER_SELECTED_VALUE";
export const SET_UIT_TESTS_SELECTED_VALUE = "uit/SET_UIT_TESTS_SELECTED_VALUE";
export const SET_UIT_TESTS_DROPDOWN_DISABLED = "uit/SET_UIT_TESTS_DROPDOWN_DISABLED";
export const SET_UIT_COMPLETED_PAGINATION_PAGE = "uit/SET_UIT_COMPLETED_PAGINATION_PAGE";
export const SET_UIT_COMPLETED_PAGINATION_PAGE_SIZE = "uit/SET_UIT_COMPLETED_PAGINATION_PAGE_SIZE";
export const SET_UIT_COMPLETED_SEARCH_PARAMETERS = "uit/SET_UIT_COMPLETED_SEARCH_PARAMETERS";
export const SET_UIT_ACTIVE_PROCESSES_PAGINATION_PAGE =
  "uit/SET_UIT_ACTIVE_PROCESSES_PAGINATION_PAGE";
export const SET_UIT_ACTIVE_PROCESSES_PAGINATION_PAGE_SIZE =
  "uit/SET_UIT_ACTIVE_PROCESSES_PAGINATION_PAGE_SIZE";
export const SET_UIT_ACTIVE_PROCESSES_SEARCH_PARAMETERS =
  "uit/SET_UIT_ACTIVE_PROCESSES_SEARCH_PARAMETERS";
export const TRIGGER_TABLES_RERENDER = "uit/TRIGGER_TABLES_RERENDER";
export const RESET_INVITATIONS_RELATED_STATES = "uit/RESET_INVITATIONS_RELATED_STATES";
const RESET_STATE = "uit/RESET_STATE";

// update test order number state
const updateUitTestOrderNumberState = uitTestOrderNumber => ({
  type: SET_UIT_TEST_ORDER_NUMBER_SELECTED_VALUE,
  uitTestOrderNumber
});

// update test to administer state
const updateUitTestsState = uitTests => ({
  type: SET_UIT_TESTS_SELECTED_VALUE,
  uitTests
});

// update test to administer dropwdown disabled state
const updateUitTestsDropdownDisabledState = uitTestsDropdownDisabled => ({
  type: SET_UIT_TESTS_DROPDOWN_DISABLED,
  uitTestsDropdownDisabled
});

// triggering active/completed processes tables rerender
const triggerTablesRerender = () => ({
  type: TRIGGER_TABLES_RERENDER
});
// update uit completed pagination page
const updateUITCompletedPaginationPage = uitCompletedPaginationPage => ({
  type: SET_UIT_COMPLETED_PAGINATION_PAGE,
  uitCompletedPaginationPage
});

// update uit completed pagination page size
const updateUITCompletedPaginationPageSize = uitCompletedPaginationPageSize => ({
  type: SET_UIT_COMPLETED_PAGINATION_PAGE_SIZE,
  uitCompletedPaginationPageSize
});

// update search keyword state (active test access codes)
const updateSearchUitCompletedStates = (uitCompletedKeyword, uitCompletedActiveSearch) => ({
  type: SET_UIT_COMPLETED_SEARCH_PARAMETERS,
  uitCompletedKeyword,
  uitCompletedActiveSearch
});

// update uit active processes pagination page
const updateUITActiveProcessesPaginationPage = uitActiveProcessesPaginationPage => ({
  type: SET_UIT_ACTIVE_PROCESSES_PAGINATION_PAGE,
  uitActiveProcessesPaginationPage
});

// update uit active processes pagination page size
const updateUITActiveProcessesPaginationPageSize = uitActiveProcessesPaginationPageSize => ({
  type: SET_UIT_ACTIVE_PROCESSES_PAGINATION_PAGE_SIZE,
  uitActiveProcessesPaginationPageSize
});

// update search keyword state (active test access codes)
const updateSearchUitActiveProcessesStates = (
  uitActiveProcessesKeyword,
  uitActiveProcessesActiveSearch
) => ({
  type: SET_UIT_ACTIVE_PROCESSES_SEARCH_PARAMETERS,
  uitActiveProcessesKeyword,
  uitActiveProcessesActiveSearch
});

// reset invitations related states
const resetUitInvitationsRelatedStates = () => ({
  type: RESET_INVITATIONS_RELATED_STATES
});

// reset states
const resetUitStates = () => ({
  type: RESET_STATE
});

// getting reasons for testing
function getReasonsForTesting() {
  return async function () {
    const response = await fetch(`/oec-cat/api/get-reasons-for-testing`, {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseJson = await response.json();
    return responseJson;
  };
}

// sending UIT invitations
function sendUitInvitations(data) {
  return async function () {
    const response = await fetch(`/oec-cat/api/send-uit-invitations/`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    // if successful request
    if (response.status === 200) {
      return response;
      // else there is an error
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// UIT invitation end date validation
function uitInvitationEndDateValidation(endDate, totalNumberOfCandidates) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/uit-invitation-end-date-validation/?end_date=${endDate}&total_number_of_candidates=${totalNumberOfCandidates}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// getting all the active processes for specified TA
function getAllActiveUITProcessesForTA(page, pageSize) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-all-active-uit-processes-for-TA/?page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// get selected UIT active processes details
function getSelectedUITActiveProcessesDetails(inviteId) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-selected-uit-active-processes-details/?id=${inviteId}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// update selected UIT active process validity end date
function updateSelectedUITActiveProcessValidityEndDate(data) {
  return async function () {
    const response = await fetch(
      "/oec-cat/api/update-selected-uit-active-process-validity-end-date/",
      {
        method: "POST",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        },
        body: JSON.stringify(data)
      }
    );
    // if successful request
    if (response.status === 200) {
      return response;
      // else there is an error
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// getting found active processes for specified TA
function getFoundActiveUITProcessesForTA(language, providedKeyword, page, pageSize) {
  // Encode search keyword so we can use special characters
  let keyword = encodeURIComponent(providedKeyword);

  // if provided keyword is empty
  if (keyword === "") {
    // send a space as the keyword parameter
    keyword = " ";
  }
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-found-active-uit-processes-for-TA/?language=${language}&keyword=${keyword}&page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// getting all completed processes for specified TA
function getAllCompletedUITProcessesForTA(page, pageSize) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-all-completed-uit-processes-for-TA/?page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// getting found completed processes for specified TA
function getFoundCompletedUITProcessesForTA(language, providedKeyword, page, pageSize) {
  // Encode search keyword so we can use special characters
  let keyword = encodeURIComponent(providedKeyword);

  // if provided keyword is empty
  if (keyword === "") {
    // send a space as the keyword parameter
    keyword = " ";
  }
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-found-completed-uit-processes-for-TA/?language=${language}&keyword=${keyword}&page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// getting selected completed processes details for specified TA
function getSelectedUITCompletedProcessesDetails(uitInviteId) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-selected-completed-uit-processes-details-for-TA/?uit_invite_id=${uitInviteId}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// deactivating single UIT test from an active process
function deactivateSingleUitTest(data) {
  return async function () {
    const response = await fetch(`/oec-cat/api/deactivate-single-uit-test/`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    // if successful request
    if (response.status === 200) {
      return response;
      // else there is an error
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// deactivating UIT process from an active process
function deactivateUitProcess(data) {
  return async function () {
    const response = await fetch(`/oec-cat/api/deactivate-uit-process/`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    // if successful request
    if (response.status === 200) {
      return response;
      // else there is an error
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// Initial State
const initialState = {
  uitTestOrderNumber: "",
  uitTests: "",
  uitTestsDropdownDisabled: true,
  uitCompletedPaginationPage: 1,
  uitCompletedPaginationPageSize: 25,
  uitCompletedKeyword: "",
  uitCompletedActiveSearch: false,
  uitActiveProcessesPaginationPage: 1,
  uitActiveProcessesPaginationPageSize: 25,
  uitActiveProcessesKeyword: "",
  uitActiveProcessesActiveSearch: false,
  triggerUitTablesRerender: false
};

const uit = (state = initialState, action) => {
  switch (action.type) {
    case SET_UIT_TEST_ORDER_NUMBER_SELECTED_VALUE:
      return {
        ...state,
        uitTestOrderNumber: action.uitTestOrderNumber
      };
    case SET_UIT_TESTS_SELECTED_VALUE:
      return {
        ...state,
        uitTests: action.uitTests
      };
    case SET_UIT_TESTS_DROPDOWN_DISABLED:
      return {
        ...state,
        uitTestsDropdownDisabled: action.uitTestsDropdownDisabled
      };
    case SET_UIT_COMPLETED_PAGINATION_PAGE:
      return {
        ...state,
        uitCompletedPaginationPage: action.uitCompletedPaginationPage
      };
    case SET_UIT_COMPLETED_PAGINATION_PAGE_SIZE:
      return {
        ...state,
        uitCompletedPaginationPageSize: action.uitCompletedPaginationPageSize
      };
    case SET_UIT_COMPLETED_SEARCH_PARAMETERS:
      return {
        ...state,
        uitCompletedKeyword: action.uitCompletedKeyword,
        uitCompletedActiveSearch: action.uitCompletedActiveSearch
      };
    case SET_UIT_ACTIVE_PROCESSES_PAGINATION_PAGE:
      return {
        ...state,
        uitActiveProcessesPaginationPage: action.uitActiveProcessesPaginationPage
      };
    case SET_UIT_ACTIVE_PROCESSES_PAGINATION_PAGE_SIZE:
      return {
        ...state,
        uitActiveProcessesPaginationPageSize: action.uitActiveProcessesPaginationPageSize
      };
    case SET_UIT_ACTIVE_PROCESSES_SEARCH_PARAMETERS:
      return {
        ...state,
        uitActiveProcessesKeyword: action.uitActiveProcessesKeyword,
        uitActiveProcessesActiveSearch: action.uitActiveProcessesActiveSearch
      };
    case TRIGGER_TABLES_RERENDER:
      return {
        ...state,
        triggerUitTablesRerender: !state.triggerUitTablesRerender
      };
    case RESET_INVITATIONS_RELATED_STATES:
      return {
        ...state,
        uitTestOrderNumber: "",
        uitTests: "",
        uitTestsDropdownDisabled: true
      };
    case RESET_STATE:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

export default uit;
export {
  initialState,
  updateUitTestOrderNumberState,
  updateUitTestsState,
  updateUitTestsDropdownDisabledState,
  resetUitStates,
  getReasonsForTesting,
  sendUitInvitations,
  getAllActiveUITProcessesForTA,
  getAllCompletedUITProcessesForTA,
  getSelectedUITActiveProcessesDetails,
  updateUITCompletedPaginationPage,
  updateUITCompletedPaginationPageSize,
  updateSearchUitCompletedStates,
  triggerTablesRerender,
  deactivateSingleUitTest,
  deactivateUitProcess,
  getFoundCompletedUITProcessesForTA,
  getSelectedUITCompletedProcessesDetails,
  resetUitInvitationsRelatedStates,
  updateSelectedUITActiveProcessValidityEndDate,
  updateUITActiveProcessesPaginationPage,
  updateUITActiveProcessesPaginationPageSize,
  updateSearchUitActiveProcessesStates,
  getFoundActiveUITProcessesForTA,
  uitInvitationEndDateValidation
};
