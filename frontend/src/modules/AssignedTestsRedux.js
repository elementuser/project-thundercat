import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// Action Types
export const SET_UIT_ASSIGNED_TEST_ID = "assigned_tests/SET_UIT_ASSIGNED_TEST_ID";
export const RESET_STATE = "assigned_tests/RESET_STATE";

// update UIT assigned test ID
const updateAssignedTestId = (assignedTestId, testId, accommodationRequestId) => ({
  type: SET_UIT_ASSIGNED_TEST_ID,
  assignedTestId,
  testId,
  accommodationRequestId
});

const resetAssignedTestState = () => ({
  type: RESET_STATE
});

// getting the assigned test of the specified user
const getAssignedTests = assignedTestId => {
  let endpoint = "";
  if (assignedTestId) {
    endpoint = `/oec-cat/api/assigned-tests/?assigned_test_id=${assignedTestId}`;
  } else {
    endpoint = "/oec-cat/api/assigned-tests/";
  }
  return async function () {
    const tests = await fetch(endpoint, {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const testsJson = await tests.json();
    return testsJson;
  };
};

// getting the UIT assigned test of the specified user
const getPublicTests = () => {
  return async function () {
    const response = await fetch("/oec-cat/api/public-tests/", {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseObj = {
      body: await response.json(),
      ok: response.ok
    };
    return responseObj;
  };
};

// Initial State
const initialState = {
  assignedTestId: null,
  testId: null,
  accommodationRequestId: null
};

// Reducer
const assignedTest = (state = initialState, action) => {
  switch (action.type) {
    case SET_UIT_ASSIGNED_TEST_ID:
      return {
        ...state,
        assignedTestId: action.assignedTestId,
        testId: action.testId,
        accommodationRequestId: action.accommodationRequestId
      };
    case RESET_STATE:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

export default assignedTest;
export { getAssignedTests, getPublicTests, updateAssignedTestId, resetAssignedTestState };
