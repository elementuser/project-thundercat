// Action Types
const SET_USER_INFORMATION = "user/SET_USER_INFORMATION";
const UPDATE_PRI = "user/UPDATE_PRI";
const UPDATE_MILITARY_NBR = "user/UPDATE_MILITARY_NBR";
const SET_LAST_PASSWORD_CHANGE = "user/SET_LAST_PASSWORD_CHANGE";
const SET_IS_USER_LOADING = "user/SET_IS_USER_LOADING";
const SET_LAST_LOGIN_DATE = "user/SET_LAST_LOGIN_DATE";
const RESET_STATE = "RESET_STATE";

// Action Creators
const setUserInformation = (
  firstName,
  lastName,
  username,
  isSuperUser,
  lastPasswordChange,
  psrsAppID,
  primaryEmail,
  secondaryEmail,
  dateOfBirth,
  pri,
  militaryNbr,
  profileCompleted
) => ({
  type: SET_USER_INFORMATION,
  firstName,
  lastName,
  username,
  isSuperUser,
  lastPasswordChange,
  psrsAppID,
  primaryEmail,
  secondaryEmail,
  dateOfBirth,
  pri,
  militaryNbr,
  profileCompleted
});

const updatePri = pri => ({
  type: UPDATE_PRI,
  pri
});

const updateMilitaryNbr = militaryNbr => ({
  type: UPDATE_MILITARY_NBR,
  militaryNbr
});

const setLastPasswordChange = lastPasswordChange => ({
  type: SET_LAST_PASSWORD_CHANGE,
  lastPasswordChange
});

const setIsUserLoading = isUserLoading => ({
  type: SET_IS_USER_LOADING,
  isUserLoading
});

const setLastLoginDate = lastLogin => ({
  type: SET_LAST_LOGIN_DATE,
  lastLogin
});

const resetUserState = () => ({ type: RESET_STATE });

// Initial State
const initialState = {
  firstName: "",
  lastName: "",
  username: "",
  isSuperUser: "",
  lastPasswordChange: "",
  psrsAppID: "",
  primaryEmail: "",
  secondaryEmail: "",
  dateOfBirth: "",
  pri: "",
  militaryNbr: "",
  isUserLoading: false,
  lastLogin: "",
  profileCompleted: false
};

const user = (state = initialState, action) => {
  switch (action.type) {
    case SET_USER_INFORMATION:
      // set the user information
      return {
        ...state,
        firstName: action.firstName,
        lastName: action.lastName,
        username: action.username,
        isSuperUser: action.isSuperUser,
        lastPasswordChange: action.lastPasswordChange,
        psrsAppID: action.psrsAppID,
        primaryEmail: action.primaryEmail,
        secondaryEmail: action.secondaryEmail,
        dateOfBirth: action.dateOfBirth,
        pri: action.pri,
        militaryNbr: action.militaryNbr,
        profileCompleted: action.profileCompleted
      };
    case UPDATE_PRI:
      return {
        ...state,
        pri: action.pri
      };
    case UPDATE_MILITARY_NBR:
      return {
        ...state,
        militaryNbr: action.militaryNbr
      };
    case SET_LAST_PASSWORD_CHANGE:
      return {
        ...state,
        lastPasswordChange: action.lastPasswordChange
      };
    case SET_IS_USER_LOADING:
      return {
        ...state,
        isUserLoading: action.isUserLoading
      };
    case SET_LAST_LOGIN_DATE:
      return {
        ...state,
        lastLogin: action.lastLogin
      };
    case RESET_STATE:
      // Ensure local storage is cleaned up once the candidate is timed out of the test.
      return {
        ...initialState
      };
    default:
      return state;
  }
};

export default user;
export {
  setUserInformation,
  updatePri,
  updateMilitaryNbr,
  setLastPasswordChange,
  setIsUserLoading,
  resetUserState,
  setLastLoginDate
};
