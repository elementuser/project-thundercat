import moment from "moment";
import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// getting the test section content based on the id
const getTestSection = (
  assignedTestId,
  order = -1,
  specialSection = "",
  testId,
  language,
  timedOut
) => {
  return async function () {
    const headers = {
      Accept: "application/json",
      "Content-Type": "application/json",
      cache: "default"
    };
    if (SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)) {
      headers.Authorization = `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`;
    }
    const url = `${
      `/oec-cat/api/get-test-section?assigned_test_id=${assignedTestId}` +
      `&test_id=${testId}&language=${language}`
    }${order !== -1 ? `&order=${order}` : ``}${
      specialSection !== "" ? `&special_section=${specialSection}` : ``
    }&timed_out=${timedOut}`;
    const response = await fetch(url, {
      method: "GET",
      headers: headers
    });
    const testSection = await response.json();
    return testSection;
  };
};

// getting the test section content based on the id
export const getCurrentTestSection = (assignedTestId, sectionId, testId, language) => {
  return async function () {
    const headers = {
      Accept: "application/json",
      "Content-Type": "application/json",
      cache: "default"
    };
    if (SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)) {
      headers.Authorization = `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`;
    }
    const url = `/oec-cat/api/get-current-test-section?assigned_test_id=${assignedTestId}&section_id=${sectionId}&language=${language}&test_id=${testId}`;
    const response = await fetch(url, {
      method: "GET",
      headers: headers
    });
    const testSection = await response.json();
    return testSection;
  };
};

// getting updated test start time after lock/pause actions
function getUpdatedTestStartTime(assigned_test_id, test_section_id) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-updated-time-after-lock-pause-actions/?assigned_test_id=${assigned_test_id}&test_section_id=${test_section_id}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// getting updated test start time after lock/pause actions
function getQuestionsList(body) {
  const customBody = body;

  // if assigned_test_id parameter is set to null
  if (customBody.assigned_test_id === null) {
    // send it as an empty string on the API call
    customBody.assigned_test_id = "";
  }

  return async function () {
    const response = await fetch("/oec-cat/api/get-questions-list/", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify({ ...customBody })
    });
    const responseObj = {
      body: await response.json(),
      ok: response.ok
    };
    return responseObj;
  };
}

// validating timeout state (in-test timed section timer validation)
function validateTimeoutState(assigned_test_id, test_section_id) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/validate-timeout-state/?assigned_test_id=${assigned_test_id}&test_section_id=${test_section_id}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          cache: "default"
        }
      }
    );
    const res = await response;
    return res;
  };
}

// getting the server time
function getServerTime() {
  return async function () {
    const response = await fetch("/oec-cat/api/get-server-time/", {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseObj = await response.json();
    return responseObj;
  };
}

// Action creators
const UPDATE_TEST_SECTION = "testSection/UPDATE_TEST_SECTION";
const SET_CONTENT_LOADED = "testSection/SET_CONTENT_LOADED";
const SET_SCORER_CONTENT_LOADED = "testSection/SET_SCORER_CONTENT_LOADED";
const SET_CONTENT_UNLOADED = "testSection/SET_CONTENT_UNLOADED";
const RESET_STATE = "testSection/RESET_STATE";
const SET_TEST_HEIGHT = "testSection/SET_TEST_HEIGHT";
const SET_CURRENT_TIME = "testSection/SET_CURRENT_TIME";
const SET_START_TIME = "testSection/SET_START_TIME";
const SET_CHEATING_ATTEMPTS = "testSection/SET_CHEATING_ATTEMPTS";
const ERASE_OPPOSITE_LANG = "testSection/ERASE_OPPOSITE_LANG";
const SET_NAV_BAR_HEIGHT = "testSection/SET_NAV_BAR_HEIGHT";
const SET_TOP_TABS_HEIGHT = "testSection/SET_TOP_TABS_HEIGHT";
const SET_TEST_FOOTER_HEIGHT = "testSection/SET_TEST_FOOTER_HEIGHT";
const SET_NOTEPAD_HEADER_HEIGHT = "testSection/SET_NOTEPAD_HEADER_HEIGHT";

const updateTestSection = (testSection, language) => ({
  type: UPDATE_TEST_SECTION,
  testSection,
  language
});
const eraseOppositeLoadedLanguage = language => ({
  type: ERASE_OPPOSITE_LANG,
  language
});
const setContentLoaded = () => ({
  type: SET_CONTENT_LOADED
});
const setScorerContentLoaded = () => ({
  type: SET_SCORER_CONTENT_LOADED
});
const setContentUnLoaded = () => ({
  type: SET_CONTENT_UNLOADED
});
const setCurrentTime = currentTime => ({
  type: SET_CURRENT_TIME,
  currentTime
});
const setStartTime = startTime => ({
  type: SET_START_TIME,
  startTime
});
const updateCheatingAttempts = cheatingAttempts => ({
  type: SET_CHEATING_ATTEMPTS,
  cheatingAttempts
});
const resetTestFactoryState = () => ({
  type: RESET_STATE
});
const setTestHeight = height => ({
  type: SET_TEST_HEIGHT,
  height
});
const setNavBarHeight = height => ({
  type: SET_NAV_BAR_HEIGHT,
  height
});
const setTopTabsHeight = height => ({
  type: SET_TOP_TABS_HEIGHT,
  height
});
const setTestFooterHeight = height => ({
  type: SET_TEST_FOOTER_HEIGHT,
  height
});
const setNotepadHeaderHeight = height => ({
  type: SET_NOTEPAD_HEADER_HEIGHT,
  height
});

const initialState = {
  testSection: {},
  isLoaded: false,
  isScorerLoaded: false,
  currentTime: "",
  startTime: "",
  testHeight: 0,
  cheatingAttempts: 0,
  defaultTab: null,
  readOnly: false,
  languagesLoaded: [],
  testSections: {},
  testId: null,
  testNavBarHeight: 0,
  topTabsHeight: 0,
  testFooterHeight: 0,
  notepadHeaderHeight: 0
};

// Reducer
const testSection = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_TEST_SECTION:
      const newTestSections = state.testSections;
      const newArray = state.languagesLoaded;
      newTestSections[`${action.language}`] = action.testSection.localize[`${action.language}`];
      if (!newArray.includes(action.language)) {
        newArray.push(action.language);
      }
      return {
        ...state,
        testSection: action.testSection,
        readOnly: action.testSection.read_only || false,
        testSections: newTestSections,
        languagesLoaded: newArray,
        testId: action.testSection.id
      };
    case SET_CONTENT_LOADED:
      return {
        ...state,
        isLoaded: true
      };
    case SET_SCORER_CONTENT_LOADED:
      return {
        ...state,
        isScorerLoaded: true
      };
    case ERASE_OPPOSITE_LANG:
      const langToKeep = action.language === "en" ? "en" : "fr";
      return {
        ...state,
        languagesLoaded: [action.language === "en" ? "en" : "fr"],
        testSections: { ...state.testSections[langToKeep] }
      };
    case SET_CONTENT_UNLOADED:
      return {
        ...state,
        isLoaded: false,
        isScorerLoaded: false
      };
    case SET_CURRENT_TIME:
      return {
        ...state,
        currentTime: moment(action.currentTime)
      };
    case SET_START_TIME:
      return {
        ...state,
        startTime: moment(action.startTime)
      };
    case SET_CHEATING_ATTEMPTS:
      return {
        ...state,
        cheatingAttempts: action.cheatingAttempts
      };
    case RESET_STATE:
      return {
        ...initialState
      };
    case SET_TEST_HEIGHT:
      return {
        ...state,
        testHeight: action.height
      };
    case SET_NAV_BAR_HEIGHT:
      return {
        ...state,
        testNavBarHeight: action.height
      };
    case SET_TOP_TABS_HEIGHT:
      return {
        ...state,
        topTabsHeight: action.height
      };
    case SET_TEST_FOOTER_HEIGHT:
      return {
        ...state,
        testFooterHeight: action.height
      };
    case SET_NOTEPAD_HEADER_HEIGHT:
      return {
        ...state,
        notepadHeaderHeight: action.height
      };
    default:
      return state;
  }
};

export default testSection;
export {
  getTestSection,
  eraseOppositeLoadedLanguage,
  updateTestSection,
  resetTestFactoryState,
  setCurrentTime,
  setStartTime,
  setContentLoaded,
  setScorerContentLoaded,
  setContentUnLoaded,
  setTestHeight,
  updateCheatingAttempts,
  getUpdatedTestStartTime,
  setTopTabsHeight,
  setTestFooterHeight,
  setNavBarHeight,
  setNotepadHeaderHeight,
  getQuestionsList,
  validateTimeoutState,
  getServerTime
};
