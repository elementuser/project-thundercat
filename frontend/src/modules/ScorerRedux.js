import SessionStorage, { ACTION, ITEM } from "../SessionStorage";
// TODO this is a placeholder for when the API for the soring key is created/completed

// Scorer Assigned Tests Pagination
export const SET_SCORER_ASSIGNED_TEST_PAGINATION_PAGE =
  "scorer/SET_SCORER_ASSIGNED_TEST_PAGINATION_PAGE";
export const SET_SCORER_ASSIGNED_TEST_PAGINATION_PAGE_SIZE =
  "scorer/SET_SCORER_ASSIGNED_TEST_PAGINATION_PAGE_SIZE";
export const TOGGLE_SIDEBAR = "scorer/TOGGLE_SIDEBAR";
const RESET_STATE = "scorer/REST_SCORER_STATE";
export const UPDATE_COMPETENCY = "scorer/UPDATE_COMPETENCY";
export const UPDATE_RATIONALE = "scorer/UPDATE_RATIONALE";
export const UPDATE_RATINGS = "scorer/UPDATE_RATINGS";
export const SET_SCORER_TEST = "scorer/SET_SCORER_TEST";
export const LOAD_SCORES = "scorer/LOAD_SCORES";
// calls
// update pagination page state (scorer assigned tests)
const updateCurrentScorerAssignedTestPageState = scorerPaginationPage => ({
  type: SET_SCORER_ASSIGNED_TEST_PAGINATION_PAGE,
  scorerPaginationPage
});
// update pagination pageSize state (scorer assigned tests)
const updateScorerAssignedTestPageSizeState = scorerPaginationPageSize => ({
  type: SET_SCORER_ASSIGNED_TEST_PAGINATION_PAGE_SIZE,
  scorerPaginationPageSize
});
export const loadScores = scores => ({
  type: LOAD_SCORES,
  scores
});
export const setScorerTest = testId => ({
  type: SET_SCORER_TEST,
  testId
});
export const updateCompetencies = scorerInfo => ({
  type: UPDATE_COMPETENCY,
  scorerInfo
});
export const updateRatings = question => ({
  type: UPDATE_RATINGS,
  question
});
export const updateQuestionScore = obj => ({
  type: UPDATE_RATIONALE,
  obj
});
// Toggle the scorer sidebar
const toggleSidebar = () => ({
  type: TOGGLE_SIDEBAR
});
const resetScorerState = () => ({
  type: RESET_STATE
});

function saveScore(scoreObj) {
  return async function () {
    const response = await fetch(`/oec-cat/api/save-score/`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(scoreObj)
    });
    const responseObj = {
      ok: response.ok
    };
    return responseObj;
  };
}

const getScores = assignedTestId => {
  return async function () {
    const headers = {
      Accept: "application/json",
      "Content-Type": "application/json",
      cache: "default"
    };
    if (SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)) {
      headers.Authorization = `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`;
    }
    const url = `/oec-cat/api/get-scores?scorer_test=${assignedTestId}`;
    const response = await fetch(url, {
      method: "GET",
      headers: headers
    });
    const testSection = await response.json();
    return testSection;
  };
};

// get scorer assigned tests (all tests assigned to this scorer)
function getAssignedTests(username, page, pageSize) {
  return async function () {
    const response = await fetch(
      "/oec-cat/api/get_scorer_assigned_tests" +
        `?scorer_username=${encodeURIComponent(username)}&page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// getting the test section content based on the id
const getInboxTestSection = assignedTestId => {
  return async function () {
    const headers = {
      Accept: "application/json",
      "Content-Type": "application/json",
      cache: "default"
    };
    if (SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)) {
      headers.Authorization = `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`;
    }
    const url = `/oec-cat/api/get-scorer-test-section?assigned_test_id=${assignedTestId}`;
    const response = await fetch(url, {
      method: "GET",
      headers: headers
    });
    const testSection = await response.json();
    return testSection;
  };
};

// Initial State
const initialState = {
  scorerPaginationPage: 1,
  scorerPaginationPageSize: 25,
  isSidebarHidden: false,
  currentQuestionIndex: 0,
  currentCompetencyIndex: 0,
  testKey: undefined,
  currentCompetency: undefined,
  currentQuestion: undefined,
  assignedTestId: undefined,
  answerScores: []
};

// Reducer
const scorer = (state = initialState, action) => {
  switch (action.type) {
    case SET_SCORER_TEST:
      return {
        ...state,
        assignedTestId: action.testId
      };
    case SET_SCORER_ASSIGNED_TEST_PAGINATION_PAGE:
      return {
        ...state,
        scorerPaginationPage: action.scorerPaginationPage
      };
    case SET_SCORER_ASSIGNED_TEST_PAGINATION_PAGE_SIZE:
      return {
        ...state,
        scorerPaginationPageSize: action.scorerPaginationPageSize
      };
    case TOGGLE_SIDEBAR:
      return {
        ...state,
        isSidebarHidden: !state.isSidebarHidden
      };
    case LOAD_SCORES:
      return {
        ...state,
        answerScores: action.scores
      };
    case UPDATE_COMPETENCY:
      const answerScores = [];
      for (const q of action.scorerInfo.questions) {
        for (const c of q.email.competency_types) {
          answerScores.push({ question: q.id, competency: c, rationale: "" });
        }
      }
      return {
        ...state,
        competencyTypes: action.scorerInfo.competency_types,
        questions: action.scorerInfo.questions,
        answerScores: answerScores
      };
    case UPDATE_RATIONALE:
      let newScore = action.obj;
      let found = false;
      const newStateArray = state.answerScores.map(score => {
        if (score.question === newScore.question && score.competency === newScore.competency) {
          found = true;
          newScore = { ...score, ...newScore };
          newScore.rationale = newScore.rationale || "";
          return newScore;
        }
        return score;
      });
      if (!found) {
        newStateArray.push(newScore);
      }
      return {
        ...state,
        answerScores: newStateArray
      };

    case UPDATE_RATINGS:
      const newQuestions = state.questions.map(q => {
        if (action.question.id === q.id) {
          return { ...q, ...action.question };
        }
        return q;
      });
      return {
        ...state,
        questions: newQuestions
      };
    case RESET_STATE:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

export default scorer;
export {
  getAssignedTests,
  getInboxTestSection,
  saveScore,
  getScores,
  updateCurrentScorerAssignedTestPageState,
  updateScorerAssignedTestPageSizeState,
  toggleSidebar,
  resetScorerState
};
