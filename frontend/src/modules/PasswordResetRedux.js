// checking if the specified active username/account exists (useful for forgot password functionality)
function doesThisEmailExist(email) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/does-this-email-exist/?email=${encodeURIComponent(email)}`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

function sendPasswordResetEmail(data) {
  return async () => {
    const response = await fetch("/oec-cat/api/password_reset/", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(data)
    });
    const responseJson = response.json();
    return responseJson;
  };
}

function resetPasswordTokenValidation(data) {
  return async () => {
    const response = await fetch("/oec-cat/api/password_reset/validate_token/", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(data)
    });
    const responseJson = response.json();
    return responseJson;
  };
}

function setNewPassword(data) {
  return async () => {
    const response = await fetch("/oec-cat/api/password_reset/confirm/", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(data)
    });
    const responseJson = response.json();
    return responseJson;
  };
}

export default doesThisEmailExist;
export { sendPasswordResetEmail, resetPasswordTokenValidation, setNewPassword };
