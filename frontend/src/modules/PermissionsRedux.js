import { PATH } from "../components/commons/Constants";
import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// Action Types
// Permissions
export const SET_PERMISSIONS = "permissions/SET_PERMISSIONS";
// Home Page
export const SET_CURRENT_HOME_PAGE = "permissions/SET_CURRENT_HOME_PAGE";
// Active Permissions Pagination
export const SET_PERMISSIONS_PAGINATION_PAGE = "permissions/SET_PERMISSIONS_PAGINATION_PAGE";
export const SET_PERMISSIONS_PAGINATION_PAGE_SIZE =
  "permissions/SET_PERMISSIONS_PAGINATION_PAGE_SIZE";
export const SET_PERMISSIONS_SEARCH_PARAMETERS = "permissions/SET_PERMISSIONS_SEARCH_PARAMETERS";
// Pending Permissions Pagination
export const SET_PENDING_PERMISSIONS_PAGINATION_PAGE =
  "permissions/SET_PENDING_PERMISSIONS_PAGINATION_PAGE";
export const SET_PENDING_PERMISSIONS_PAGINATION_PAGE_SIZE =
  "permissions/SET_PENDING_PERMISSIONS_PAGINATION_PAGE_SIZE";
export const SET_PENDING_PERMISSIONS_SEARCH_PARAMETERS =
  "permissions/SET_PENDING_PERMISSIONS_SEARCH_PARAMETERS";
// Active Test Permissions Pagination
export const SET_TEST_PERMISSIONS_PAGINATION_PAGE =
  "permissions/SET_TEST_PERMISSIONS_PAGINATION_PAGE";
export const SET_TEST_PERMISSIONS_PAGINATION_PAGE_SIZE =
  "permissions/SET_TEST_PERMISSIONS_PAGINATION_PAGE_SIZE";
export const SET_TEST_PERMISSIONS_SEARCH_PARAMETERS =
  "permissions/SET_TEST_PERMISSIONS_SEARCH_PARAMETERS";
export const SET_TRIGGER_POPULATE_ACTIVE_TEST_PERMISSIONS =
  "permissions/SET_TRIGGER_POPULATE_ACTIVE_TEST_PERMISSIONS";
// Orderless Test Administrators Pagination
export const SET_ORDERLESS_TEST_ADMINISTRATORS_PAGINATION_PAGE =
  "permissions/SET_ORDERLESS_TEST_ADMINISTRATORS_PAGINATION_PAGE";
export const SET_ORDERLESS_TEST_ADMINISTRATORS_PAGINATION_PAGE_SIZE =
  "permissions/SET_ORDERLESS_TEST_ADMINISTRATORS_PAGINATION_PAGE_SIZE";
export const SET_ORDERLESS_TEST_ADMINISTRATORS_SEARCH_PARAMETERS =
  "permissions/SET_ORDERLESS_TEST_ADMINISTRATORS_SEARCH_PARAMETERS";
export const SET_TRIGGER_POPULATE_ORDERLESS_TEST_ADMINISTRATORS =
  "permissions/SET_TRIGGER_POPULATE_ORDERLESS_TEST_ADMINISTRATORS";
export const SET_SELECTED_ORDERLESS_TEST_ADMINISTRATOR =
  "permissions/SET_SELECTED_ORDERLESS_TEST_ADMINISTRATOR";
// Orderless Test Accesses Pagination
export const SET_ORDERLESS_TEST_ACCESSES_PAGINATION_PAGE =
  "permissions/SET_ORDERLESS_TEST_ACCESSES_PAGINATION_PAGE";
export const SET_ORDERLESS_TEST_ACCESSES_PAGINATION_PAGE_SIZE =
  "permissions/SET_ORDERLESS_TEST_ACCESSES_PAGINATION_PAGE_SIZE";
export const SET_ORDERLESS_TEST_ACCESSES_SEARCH_PARAMETERS =
  "permissions/SET_ORDERLESS_TEST_ACCESSES_SEARCH_PARAMETERS";
export const SET_TRIGGER_POPULATE_ORDERLESS_TEST_ACCESSES =
  "permissions/SET_TRIGGER_POPULATE_ORDERLESS_TEST_ACCESSES";
export const SET_ORDERLESS_TEST_ACCESSES_DATA = "permissions/SET_ORDERLESS_TEST_ACCESSES_DATA";
export const SET_ORDERLESS_TEST_ORDER_OPTION_SELECTED =
  "permissions/SET_ORDERLESS_TEST_ORDER_OPTION_SELECTED";
const RESET_STATE = "permissions/SET_IS_TEST_ADMINISTRATOR";

// update permissions state
const updatePermissionsState = permissions => ({
  type: SET_PERMISSIONS,
  permissions
});
// update current home page state
const updateCurrentHomePageState = currentHomePage => ({
  type: SET_CURRENT_HOME_PAGE,
  currentHomePage
});
// update pagination page state (active permissions)
const updateCurrentPermissionsPageState = permissionsPaginationPage => ({
  type: SET_PERMISSIONS_PAGINATION_PAGE,
  permissionsPaginationPage
});
// update pagination pageSize state (active permissions)
const updatePermissionsPageSizeState = permissionsPaginationPageSize => ({
  type: SET_PERMISSIONS_PAGINATION_PAGE_SIZE,
  permissionsPaginationPageSize
});
// update search keyword state (active permissions)
const updateSearchActivePermissionsStates = (permissionsKeyword, permissionsActiveSearch) => ({
  type: SET_PERMISSIONS_SEARCH_PARAMETERS,
  permissionsKeyword,
  permissionsActiveSearch
});
// update pagination page state (active permissions)
const updateCurrentPendingPermissionsPageState = pendingPermissionsPaginationPage => ({
  type: SET_PENDING_PERMISSIONS_PAGINATION_PAGE,
  pendingPermissionsPaginationPage
});
// update pagination pageSize state (active permissions)
const updatePendingPermissionsPageSizeState = pendingPermissionsPaginationPageSize => ({
  type: SET_PENDING_PERMISSIONS_PAGINATION_PAGE_SIZE,
  pendingPermissionsPaginationPageSize
});
// update search keyword state (active permissions)
const updateSearchActivePendingPermissionsStates = (
  pendingPermissionsKeyword,
  pendingPermissionsActiveSearch
) => ({
  type: SET_PENDING_PERMISSIONS_SEARCH_PARAMETERS,
  pendingPermissionsKeyword,
  pendingPermissionsActiveSearch
});
// update pagination page state (active test permissions)
const updateCurrentTestPermissionsPageState = testPermissionsPaginationPage => ({
  type: SET_TEST_PERMISSIONS_PAGINATION_PAGE,
  testPermissionsPaginationPage
});
// update pagination pageSize state (active test permissions)
const updateTestPermissionsPageSizeState = testPermissionsPaginationPageSize => ({
  type: SET_TEST_PERMISSIONS_PAGINATION_PAGE_SIZE,
  testPermissionsPaginationPageSize
});
// update search keyword state (active test permissions)
const updateSearchActiveTestPermissionsStates = (
  testPermissionsKeyword,
  testPermissionsActiveSearch
) => ({
  type: SET_TEST_PERMISSIONS_SEARCH_PARAMETERS,
  testPermissionsKeyword,
  testPermissionsActiveSearch
});
// update triggerPopulateTestPermissions state (active test permissions)
const updateTriggerPopulateTestPermissionsState = triggerPopulateTestPermissions => ({
  type: SET_TRIGGER_POPULATE_ACTIVE_TEST_PERMISSIONS,
  triggerPopulateTestPermissions
});
// update pagination page state (orderless test administrators)
const updateCurrentOrderlessTestAdministratorsPageState =
  orderlessTestAdministratorsPaginationPage => ({
    type: SET_ORDERLESS_TEST_ADMINISTRATORS_PAGINATION_PAGE,
    orderlessTestAdministratorsPaginationPage
  });
// update pagination pageSize state (orderless test administrators)
const updateOrderlessTestAdministratorsPageSizeState =
  orderlessTestAdministratorsPaginationPageSize => ({
    type: SET_ORDERLESS_TEST_ADMINISTRATORS_PAGINATION_PAGE_SIZE,
    orderlessTestAdministratorsPaginationPageSize
  });
// update search keyword state (orderless test administrators)
const updateSearchOrderlessTestAdministratorsStates = (
  orderlessTestAdministratorsKeyword,
  orderlessTestAdministratorsActiveSearch
) => ({
  type: SET_ORDERLESS_TEST_ADMINISTRATORS_SEARCH_PARAMETERS,
  orderlessTestAdministratorsKeyword,
  orderlessTestAdministratorsActiveSearch
});
// update triggerPopulateOrderlessTestAdministrators state (orderless test administrators)
const updateTriggerPopulateOrderlessTestAdministratorsState =
  triggerPopulateOrderlessTestAdministrators => ({
    type: SET_TRIGGER_POPULATE_ORDERLESS_TEST_ADMINISTRATORS,
    triggerPopulateOrderlessTestAdministrators
  });
// setting selectedOrderlessTestAdministrator state (orderless test administrators)
const setSelectedOrderlesstestAdministratorState = selectedOrderlessTestAdministratorData => ({
  type: SET_SELECTED_ORDERLESS_TEST_ADMINISTRATOR,
  selectedOrderlessTestAdministratorData
});
// update pagination page state (orderless test accesses)
const updateCurrentOrderlessTestAccessesPageState = orderlessTestAccessesPaginationPage => ({
  type: SET_ORDERLESS_TEST_ACCESSES_PAGINATION_PAGE,
  orderlessTestAccessesPaginationPage
});
// update pagination pageSize state (orderless test accesses)
const updateOrderlessTestAccessesPageSizeState = orderlessTestAccessesPaginationPageSize => ({
  type: SET_ORDERLESS_TEST_ACCESSES_PAGINATION_PAGE_SIZE,
  orderlessTestAccessesPaginationPageSize
});
// update search keyword state (orderless test accesses)
const updateSearchOrderlessTestAccessesStates = (
  orderlessTestAccessesKeyword,
  orderlessTestAccessesActiveSearch
) => ({
  type: SET_ORDERLESS_TEST_ACCESSES_SEARCH_PARAMETERS,
  orderlessTestAccessesKeyword,
  orderlessTestAccessesActiveSearch
});
// update triggerPopulateOrderlessTestAccesses state (orderless test accesses)
const updateTriggerPopulateOrderlessTestAccessesState = triggerPopulateOrderlessTestAccesses => ({
  type: SET_TRIGGER_POPULATE_ORDERLESS_TEST_ACCESSES,
  triggerPopulateOrderlessTestAccesses
});
// update orderlessTestAccessesData state (orderless test accesses)
const updateOrderlessTestAccessesData = orderlessTestAccessesData => ({
  type: SET_ORDERLESS_TEST_ACCESSES_DATA,
  orderlessTestAccessesData
});
// update orderlessTestOrderOptionSelected state (orderless test - TA)
const updateOrderlessTestOrderOptionSelected = orderlessTestOrderOptionSelected => ({
  type: SET_ORDERLESS_TEST_ORDER_OPTION_SELECTED,
  orderlessTestOrderOptionSelected
});
const resetPermissionsState = () => ({
  type: RESET_STATE
});

// get the test definitions with versions
function getTestDefinitionVersionsCollected() {
  return async function () {
    const response = await fetch(`/oec-cat/api/get-test-definition-versions-collected/`, {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseObj = {
      body: await response.json(),
      ok: response.ok
    };
    return responseObj;
  };
}

// get permissions (all existing permissions)
function getExistingPermissions() {
  return async function () {
    const response = await fetch("/oec-cat/api/get-permissions/", {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseJson = await response.json();
    return responseJson;
  };
}

// get user permissions based on the username
function getUserPermissions() {
  return async function () {
    const response = await fetch("/oec-cat/api/get-user-permissions/", {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseJson = await response.json();
    return responseJson;
  };
}

// getting specified user'pending permissions
function getUserPendingPermissions() {
  return async function () {
    const response = await fetch("/oec-cat/api/get-user-pending-permissions/", {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseJson = await response.json();
    return responseJson;
  };
}

// getting all pending permissions
function getPendingPermissions(page, pageSize) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-pending-permissions?page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// getting found pending permissions
function getFoundPendingPermissions(currentLanguage, providedKeyword, page, pageSize) {
  // Encode search keyword so we can use special characters
  let keyword = encodeURIComponent(providedKeyword);

  // if provided keyword is empty
  if (keyword === "") {
    // send a space as the keyword parameter
    keyword = " ";
  }
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-found-pending-permissions?current_language=${currentLanguage}&keyword=${keyword}&page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// getting available permissions for permission requests
// only getting permissions that are not part or specified user' permissions and user' pending permissions
function getAvailablePermissions() {
  return async function () {
    const response = await fetch("/oec-cat/api/get-available-permissions/", {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseJson = await response.json();
    return responseJson;
  };
}

// grant permission to user
function grantPermission(username, permissionId) {
  const body = {
    username: username,
    permission_id: permissionId
  };
  return async function () {
    const response = await fetch(`/oec-cat/api/grant-permission/`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify({ ...body })
    });
    // if successful request
    if (response.status === 200) {
      return response;
      // else there is an error
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// deny requested permission
function denyPermission(permissionRequestId) {
  const body = {
    permission_request_id: permissionRequestId
  };
  return async function () {
    const response = await fetch(`/oec-cat/api/deny-permission/`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify({ ...body })
    });
    // if successful request
    if (response.status === 200) {
      return response;
      // else there is an error
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// get active permissions
function getActivePermissions(page, pageSize) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-active-permissions?page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// get found active permissions
function getFoundActivePermissions(currentLanguage, providedKeyword, page, pageSize) {
  // if provided keyword is empty
  let keyword = encodeURIComponent(providedKeyword);
  if (keyword === "") {
    // send a space as the keyword parameter
    keyword = " ";
  }
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-found-active-permissions?current_language=${currentLanguage}&keyword=${keyword}&page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// update specified active permission
function updateActivePermission(data) {
  const body = {
    user_permission_id: data.userPermissionId,
    goc_email: data.gocEmail,
    supervisor: data.supervisor,
    supervisor_email: data.supervisorEmail,
    reason_for_modification: data.reason_for_modification
  };
  return async function () {
    const response = await fetch(`/oec-cat/api/update-active-permission/`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify({ ...body })
    });
    // if successful request
    if (response.status === 200) {
      return response;
      // else there is an error
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// delete specified active permission
function deleteActivePermission(data) {
  return async function () {
    const body = {
      user_permission_id: data.user_permission_id,
      reason_for_deletion: data.reason_for_deletion
    };
    const response = await fetch(`/oec-cat/api/delete-active-permission/`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify({ ...body })
    });
    // if successful request
    if (response.status === 200) {
      return response;
      // else there is an error
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// grant test permission to user
function grantTestPermission(usernames, test_ids, data) {
  const body = {
    expiry_date: data.expiryDate,
    test_order_number: data.testOrderNumber,
    staffing_process_number: data.staffingProcessNumber,
    department_ministry_code: data.departmentMinistryCode,
    is_org: data.isOrg,
    is_ref: data.isRef,
    billing_contact: data.billingContact,
    billing_contact_info: data.billingContactInfo,
    usernames: usernames.join(),
    test_ids: test_ids.join()
  };
  return async function () {
    const response = await fetch(`/oec-cat/api/grant-test-permission/`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify({ ...body })
    });
    // if successful request
    if (response.status === 200) {
      return response;
      // else there is an error
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// get test permissions for a specific user, but only UIT tests
function getTestPermissions(uit_only) {
  return async function () {
    const response = await fetch(`/oec-cat/api/get-test-permissions/?uit_only=${uit_only}`, {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseJson = await response.json();
    return responseJson;
  };
}

// get orderless test options
function getOrderlessTestOptions() {
  return async function () {
    const response = await fetch(`/oec-cat/api/get-ta-orderless-test-options`, {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseJson = await response.json();
    return responseJson;
  };
}

// get orderless uit test options
function getOrderlessUitTestOptions() {
  return async function () {
    const response = await fetch(`/oec-cat/api/get-ta-orderless-uit-test-options`, {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseJson = await response.json();
    return responseJson;
  };
}

// get test permission's financial data for a specific ta, test order number and test ID
function getTestPermissionFinancialData(testOrderNumber, testToAdminister) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-test-permission-financial-data/?test_order_number=${testOrderNumber}&test_to_administer=${testToAdminister}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// get all active test permissions
function getAllActiveTestPermissions(page, pageSize) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-all-active-test-permissions/?page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// get found active test permissions
function getFoundActiveTestPermissions(currentLanguage, providedKeyword, page, pageSize) {
  // if provided keyword is empty
  let keyword = encodeURIComponent(providedKeyword);
  if (keyword === "") {
    // send a space as the keyword parameter
    keyword = " ";
  }
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-found-active-test-permissions/?current_language=${currentLanguage}&keyword=${keyword}&page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// delete test permission
function deleteTestPermission(data) {
  const body = {
    test_permission_id: data.test_permission_id,
    reason_for_deletion: data.reason_for_deletion
  };
  return async function () {
    const response = await fetch(`/oec-cat/api/delete-test-permission/`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify({ ...body })
    });
    // if successful request
    if (response.status === 200) {
      return response;
      // else there is an error
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// update test permission
function updateTestPermission(data) {
  const body = {
    test_permission_id: data.test_permission_id,
    expiry_date: data.expiry_date,
    reason_for_modification: data.reason_for_modification
  };
  return async function () {
    const response = await fetch(`/oec-cat/api/update-test-permission/`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify({ ...body })
    });
    // if successful request
    if (response.status === 200) {
      return response;
      // else there is an error
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// get all users that have the specified permission
function getUsersBasedOnSpecifiedPermission(codename) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-users-based-on-specified-permission/?permission_codename=${codename}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// get user permissions based on the username
function getSelectedUserPermissions(username) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-selected-user-permissions/?username=${encodeURIComponent(username)}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// getting specified user'pending permissions
function getSelectedUserPendingPermissions(username) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-selected-user-pending-permissions/?username=${encodeURIComponent(
        username
      )}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// get test permissions for a specific user
function getSelectedUserTestPermissions(username) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-selected-user-test-permissions/?username=${encodeURIComponent(username)}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// get unassigned orderless test administrators
function getUnassignedOrderlessTestAdministrators() {
  return async function () {
    const response = await fetch(`/oec-cat/api/get-unassigned-orderless-test-administrators`, {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseJson = await response.json();
    return responseJson;
  };
}

// get all orderless test administrators
function getAllOrderlessTestAdministrators(page, pageSize) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-all-orderless-test-administrators/?page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// get found orderless test administrators
function getFoundOrderlessTestAdministrators(providedKeyword, currentLanguage, page, pageSize) {
  // Encode search keyword so we can use special characters
  let keyword = encodeURIComponent(providedKeyword);

  // if provided keyword is empty
  if (keyword === "") {
    // send a space as the keyword parameter
    keyword = " ";
  }
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-found-orderless-test-administrators/?keyword=${keyword}&current_language=${currentLanguage}&page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// add orderless test administrators
function addOrderlessTestAdministrators(body) {
  const newObj = {
    ...body
  };
  return async function () {
    const response = await fetch(`/oec-cat/api/add-orderless-test-administrators`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(newObj)
    });
    // if successful request
    if (response.status === 200) {
      return response;
      // else there is an error
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// get orderless test accesses
function getOrderlessTestAccesses(page, pageSize, testAdministrator) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-orderless-test-accesses/?page=${page}&page_size=${pageSize}&test_administrator=${encodeURIComponent(
        testAdministrator
      )}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// get orderless test accesses
function getFoundOrderlessTestAccesses(
  providedKeyword,
  currentLanguage,
  page,
  pageSize,
  testAdministrator
) {
  // Encode search keyword so we can use special characters
  let keyword = encodeURIComponent(providedKeyword);

  // if provided keyword is empty
  if (keyword === "") {
    // send a space as the keyword parameter
    keyword = " ";
  }
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-found-orderless-test-accesses/?keyword=${keyword}&current_language=${currentLanguage}&page=${page}&page_size=${pageSize}&test_administrator=${encodeURIComponent(
        testAdministrator
      )}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// add/delete orderless test access
function addDeleteOrderlessTestAccess(body) {
  const newObj = {
    ...body
  };
  return async function () {
    const response = await fetch(`/oec-cat/api/add-delete-orderless-test-access`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(newObj)
    });
    // if successful request
    if (response.status === 200) {
      return response;
      // else there is an error
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// delete orderless test administrator
function deleteOrderlessTestAdministrator(body) {
  const newObj = {
    ...body
  };
  return async function () {
    const response = await fetch(`/oec-cat/api/delete-orderless-test-administrator`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(newObj)
    });
    // if successful request
    if (response.status === 200) {
      return response;
      // else there is an error
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// update orderless test administrator department
function updateOrderlessTestAdministratorDepartment(body) {
  const newObj = {
    ...body
  };
  return async function () {
    const response = await fetch(`/oec-cat/api/update-orderless-test-administrator-department`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(newObj)
    });
    // if successful request
    if (response.status === 200) {
      return response;
      // else there is an error
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// Initial State
const initialState = {
  isEtta: false,
  isPpc: false,
  isTa: false,
  isScorer: false,
  isTb: false,
  isAae: false,
  isRdOperations: false,
  isTd: false,
  currentHomePage: PATH.dashboard,
  permissionsPaginationPage: 1,
  permissionsPaginationPageSize: 25,
  permissionsKeyword: "",
  permissionsActiveSearch: false,
  pendingPermissionsPaginationPage: 1,
  pendingPermissionsPaginationPageSize: 25,
  pendingPermissionsKeyword: "",
  pendingPermissionsActiveSearch: false,
  testPermissionsPaginationPage: 1,
  testPermissionsPaginationPageSize: 25,
  testPermissionsKeyword: "",
  testPermissionsActiveSearch: false,
  triggerPopulateTestPermissions: false,
  orderlessTestAdministratorsPaginationPage: 1,
  orderlessTestAdministratorsPaginationPageSize: 25,
  orderlessTestAdministratorsKeyword: "",
  orderlessTestAdministratorsActiveSearch: false,
  triggerPopulateOrderlessTestAdministrators: false,
  selectedOrderlessTestAdministratorData: "",
  orderlessTestAccessesPaginationPage: 1,
  orderlessTestAccessesPaginationPageSize: 25,
  orderlessTestAccessesKeyword: "",
  orderlessTestAccessesActiveSearch: false,
  triggerPopulateOrderlessTestAccesses: false,
  orderlessTestAccessesData: [],
  orderlessTestOrderOptionSelected: false
};

// Reducer
const userPermissions = (state = initialState, action) => {
  switch (action.type) {
    case SET_PERMISSIONS:
      return {
        ...state,
        isEtta: action.permissions.isEtta,
        isPpc: action.permissions.isPpc,
        isTa: action.permissions.isTa,
        isScorer: action.permissions.isScorer,
        isTb: action.permissions.isTb,
        isAae: action.permissions.isAae,
        isRdOperations: action.permissions.isRdOperations,
        isTd: action.permissions.isTd
      };
    case SET_CURRENT_HOME_PAGE:
      return {
        ...state,
        currentHomePage: action.currentHomePage
      };
    case SET_PERMISSIONS_PAGINATION_PAGE:
      return {
        ...state,
        permissionsPaginationPage: action.permissionsPaginationPage
      };
    case SET_PERMISSIONS_PAGINATION_PAGE_SIZE:
      return {
        ...state,
        permissionsPaginationPageSize: action.permissionsPaginationPageSize
      };
    case SET_PERMISSIONS_SEARCH_PARAMETERS:
      return {
        ...state,
        permissionsKeyword: action.permissionsKeyword,
        permissionsActiveSearch: action.permissionsActiveSearch
      };
    case SET_PENDING_PERMISSIONS_PAGINATION_PAGE:
      return {
        ...state,
        pendingPermissionsPaginationPage: action.pendingPermissionsPaginationPage
      };
    case SET_PENDING_PERMISSIONS_PAGINATION_PAGE_SIZE:
      return {
        ...state,
        pendingPermissionsPaginationPageSize: action.pendingPermissionsPaginationPageSize
      };
    case SET_PENDING_PERMISSIONS_SEARCH_PARAMETERS:
      return {
        ...state,
        pendingPermissionsKeyword: action.pendingPermissionsKeyword,
        pendingPermissionsActiveSearch: action.pendingPermissionsActiveSearch
      };
    case SET_TEST_PERMISSIONS_PAGINATION_PAGE:
      return {
        ...state,
        testPermissionsPaginationPage: action.testPermissionsPaginationPage
      };
    case SET_TEST_PERMISSIONS_PAGINATION_PAGE_SIZE:
      return {
        ...state,
        testPermissionsPaginationPageSize: action.testPermissionsPaginationPageSize
      };
    case SET_TEST_PERMISSIONS_SEARCH_PARAMETERS:
      return {
        ...state,
        testPermissionsKeyword: action.testPermissionsKeyword,
        testPermissionsActiveSearch: action.testPermissionsActiveSearch
      };
    case SET_TRIGGER_POPULATE_ACTIVE_TEST_PERMISSIONS:
      return {
        ...state,
        triggerPopulateTestPermissions: action.triggerPopulateTestPermissions
      };
    case SET_ORDERLESS_TEST_ADMINISTRATORS_PAGINATION_PAGE:
      return {
        ...state,
        orderlessTestAdministratorsPaginationPage: action.orderlessTestAdministratorsPaginationPage
      };
    case SET_ORDERLESS_TEST_ADMINISTRATORS_PAGINATION_PAGE_SIZE:
      return {
        ...state,
        orderlessTestAdministratorsPaginationPageSize:
          action.orderlessTestAdministratorsPaginationPageSize
      };
    case SET_ORDERLESS_TEST_ADMINISTRATORS_SEARCH_PARAMETERS:
      return {
        ...state,
        orderlessTestAdministratorsKeyword: action.orderlessTestAdministratorsKeyword,
        orderlessTestAdministratorsActiveSearch: action.orderlessTestAdministratorsActiveSearch
      };
    case SET_TRIGGER_POPULATE_ORDERLESS_TEST_ADMINISTRATORS:
      return {
        ...state,
        triggerPopulateOrderlessTestAdministrators:
          action.triggerPopulateOrderlessTestAdministrators
      };
    case SET_SELECTED_ORDERLESS_TEST_ADMINISTRATOR:
      return {
        ...state,
        selectedOrderlessTestAdministratorData: action.selectedOrderlessTestAdministratorData
      };
    case SET_ORDERLESS_TEST_ACCESSES_PAGINATION_PAGE:
      return {
        ...state,
        orderlessTestAccessesPaginationPage: action.orderlessTestAccessesPaginationPage
      };
    case SET_ORDERLESS_TEST_ACCESSES_PAGINATION_PAGE_SIZE:
      return {
        ...state,
        orderlessTestAccessesPaginationPageSize: action.orderlessTestAccessesPaginationPageSize
      };
    case SET_ORDERLESS_TEST_ACCESSES_SEARCH_PARAMETERS:
      return {
        ...state,
        orderlessTestAccessesKeyword: action.orderlessTestAccessesKeyword,
        orderlessTestAccessesActiveSearch: action.orderlessTestAccessesActiveSearch
      };
    case SET_TRIGGER_POPULATE_ORDERLESS_TEST_ACCESSES:
      return {
        ...state,
        triggerPopulateOrderlessTestAccesses: action.triggerPopulateOrderlessTestAccesses
      };
    case SET_ORDERLESS_TEST_ACCESSES_DATA:
      return {
        ...state,
        orderlessTestAccessesData: action.orderlessTestAccessesData
      };
    case SET_ORDERLESS_TEST_ORDER_OPTION_SELECTED:
      return {
        ...state,
        orderlessTestOrderOptionSelected: action.orderlessTestOrderOptionSelected
      };
    case RESET_STATE:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

export default userPermissions;
export {
  initialState,
  getTestDefinitionVersionsCollected,
  getExistingPermissions,
  getUserPermissions,
  getPendingPermissions,
  getFoundPendingPermissions,
  getUserPendingPermissions,
  getAvailablePermissions,
  grantPermission,
  denyPermission,
  getActivePermissions,
  getFoundActivePermissions,
  updateActivePermission,
  deleteActivePermission,
  grantTestPermission,
  getTestPermissions,
  getTestPermissionFinancialData,
  getAllActiveTestPermissions,
  getFoundActiveTestPermissions,
  updatePermissionsState,
  updateCurrentHomePageState,
  updateCurrentPermissionsPageState,
  updatePermissionsPageSizeState,
  updateSearchActivePermissionsStates,
  updateCurrentPendingPermissionsPageState,
  updatePendingPermissionsPageSizeState,
  updateSearchActivePendingPermissionsStates,
  updateCurrentTestPermissionsPageState,
  updateTestPermissionsPageSizeState,
  updateSearchActiveTestPermissionsStates,
  getUsersBasedOnSpecifiedPermission,
  updateTriggerPopulateTestPermissionsState,
  deleteTestPermission,
  updateTestPermission,
  resetPermissionsState,
  getSelectedUserPermissions,
  getSelectedUserPendingPermissions,
  getSelectedUserTestPermissions,
  getAllOrderlessTestAdministrators,
  getFoundOrderlessTestAdministrators,
  updateCurrentOrderlessTestAdministratorsPageState,
  updateOrderlessTestAdministratorsPageSizeState,
  updateSearchOrderlessTestAdministratorsStates,
  updateTriggerPopulateOrderlessTestAdministratorsState,
  addOrderlessTestAdministrators,
  setSelectedOrderlesstestAdministratorState,
  updateCurrentOrderlessTestAccessesPageState,
  updateOrderlessTestAccessesPageSizeState,
  updateSearchOrderlessTestAccessesStates,
  updateTriggerPopulateOrderlessTestAccessesState,
  updateOrderlessTestOrderOptionSelected,
  getOrderlessTestAccesses,
  getFoundOrderlessTestAccesses,
  updateOrderlessTestAccessesData,
  addDeleteOrderlessTestAccess,
  deleteOrderlessTestAdministrator,
  updateOrderlessTestAdministratorDepartment,
  getUnassignedOrderlessTestAdministrators,
  getOrderlessTestOptions,
  getOrderlessUitTestOptions
};
