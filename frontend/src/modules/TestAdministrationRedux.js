import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// Action Types
export const SET_TEST_ORDER_NUMBER_SELECTED_VALUE =
  "testAdministration/SET_TEST_ORDER_NUMBER_SELECTED_VALUE";
export const SET_TEST_TO_ADMINISTER_SELECTED_VALUE =
  "testAdministration/SET_TEST_TO_ADMINISTER_SELECTED_VALUE";
export const SET_TEST_TO_ADMINISTER_DROPDOWN_DISABLED =
  "testAdministration/SET_TEST_TO_ADMINISTER_DROPDOWN_DISABLED";
export const SET_TEST_SESSION_LANGUAGE_SELECTED_VALUE =
  "testAdministration/SET_TEST_SESSION_LANGUAGE_SELECTED_VALUE";
export const SET_ORDERLESS_FINANCIAL_DATA_STATE =
  "testAdministration/SET_ORDERLESS_FINANCIAL_DATA_STATE";
export const SET_TEST_SESSION_LANGUAGE_DROPDOWN_DISABLED =
  "testAdministration/SET_TEST_SESSION_LANGUAGE_DROPDOWN_DISABLED";
export const SET_GENERATE_BUTTON_DISABLED_STATE =
  "testAdministration/SET_GENERATE_BUTTON_DISABLED_STATE";
export const SET_TA_SIDE_NAV_STATE = "testAdministration/SET_TA_SIDE_NAV_STATE";
const RESET_STATE = "testAdministration/RESET_STATE";

// update test order number state
const updateTestOrderNumberState = testOrderNumber => ({
  type: SET_TEST_ORDER_NUMBER_SELECTED_VALUE,
  testOrderNumber
});

// update test to administer state
const updateTestToAdministerState = testToAdminister => ({
  type: SET_TEST_TO_ADMINISTER_SELECTED_VALUE,
  testToAdminister
});

// update test to administer dropwdown disabled state
const updateTestToAdministerDropdownDisabledState = testToAdministerDropdownDisabled => ({
  type: SET_TEST_TO_ADMINISTER_DROPDOWN_DISABLED,
  testToAdministerDropdownDisabled
});

// update test session language state
const updateTestSessionLanguageState = testSessionLanguage => ({
  type: SET_TEST_SESSION_LANGUAGE_SELECTED_VALUE,
  testSessionLanguage
});

// update orderless financial data state
const updateOrderlessFinancialDataState = orderlessFinancialData => ({
  type: SET_ORDERLESS_FINANCIAL_DATA_STATE,
  orderlessFinancialData
});

// update test session language dropwdown disabled state
const updateTestSessionLanguageDropdownDisabledState = testSessionLanguageDropdownDisabled => ({
  type: SET_TEST_SESSION_LANGUAGE_DROPDOWN_DISABLED,
  testSessionLanguageDropdownDisabled
});

// update generate button disabled state
const updateGenerateButtonDisabledState = generateButtonDisabled => ({
  type: SET_GENERATE_BUTTON_DISABLED_STATE,
  generateButtonDisabled
});

// set TA User side nav state
const setTAUserSideNavState = selectedSideNavItem => ({
  type: SET_TA_SIDE_NAV_STATE,
  selectedSideNavItem
});

// reset states
const resetTestAdministrationStates = () => ({
  type: RESET_STATE
});

// gets randomly generated test access code
function getNewTestAccessCode(body) {
  return async function () {
    const response = await fetch(`/oec-cat/api/get-new-test-access-code/`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify({ ...body })
    });
    return response;
  };
}

// delete test access code
function deleteTestAccessCode(testAccessCode) {
  const body = {
    test_access_code: testAccessCode
  };
  return async function () {
    const response = await fetch(`/oec-cat/api/delete-test-access-code/`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify({ ...body })
    });
    // if successful request
    if (response.status === 200) {
      return response;
      // else there is an error
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// get active test access code data for a specified TA username
function getActiveTestAccessCodesRedux() {
  return async function () {
    const response = await fetch("/oec-cat/api/get-active-test-access-codes/", {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseJson = await response.json();
    return responseJson;
  };
}

// get assigned candidates for a specified TA username
function getTestAdministratorAssignedCandidates() {
  return async function () {
    const response = await fetch("/oec-cat/api/get-test-administrator-assigned-candidates/", {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseJson = await response.json();
    return responseJson;
  };
}

// updating test time (useful for TAs to allow them to customize test time for a specific candidate)
const updateTestTime = (assignedTestSectionId, testTime, firstIteration, lastIteration) => {
  const body = {
    assigned_test_section_id: assignedTestSectionId,
    test_time: testTime
  };
  return async function () {
    const response = await fetch(
      `/oec-cat/api/update-test-time/?assigned_test_section_id=${assignedTestSectionId}&test_time=${testTime}&first_iteration=${firstIteration}&last_iteration=${lastIteration}`,
      {
        method: "POST",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        },
        body: JSON.stringify({ ...body })
      }
    );
    return response;
  };
};

// updating break bank (useful for TAs to allow them to add break bank for a specific candidate)
const updateBreakBank = (assignedTestId, breakBankTime) => {
  const body = {
    assigned_test__id: assignedTestId,
    break_bank_time: breakBankTime
  };
  return async function () {
    const response = await fetch(
      `/oec-cat/api/update-break-bank/?assigned_test_id=${assignedTestId}&break_bank_time=${breakBankTime}`,
      {
        method: "POST",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        },
        body: JSON.stringify({ ...body })
      }
    );
    return response;
  };
};

// approving candidate
const approveCandidate = (username, test) => {
  const body = {
    username_id: username,
    test_id: test
  };
  return async function () {
    const response = await fetch(`/oec-cat/api/approve-candidate/`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify({ ...body })
    });
    const responseJson = await response.json();
    return responseJson;
  };
};

// locking single candidate's test
const lockCandidateTest = (username, test, testSectionId) => {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/lock-candidate-test/?username_id=${encodeURIComponent(
        username
      )}&test_id=${test}&test_section_id=${testSectionId}`,
      {
        method: "POST",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
};

// locking all candidates test
const lockAllCandidatesTest = () => {
  return async function () {
    const response = await fetch("/oec-cat/api/lock-all-candidates-test/", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseJson = await response.json();
    return responseJson;
  };
};

// unlocking single candidate's test
const unlockCandidateTest = (username, test, testSectionId) => {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/unlock-candidate-test/?username_id=${encodeURIComponent(
        username
      )}&test_id=${test}&test_section_id=${testSectionId}`,
      {
        method: "POST",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
};

// unlocking all candidates test
const unlockAllCandidatesTest = () => {
  return async function () {
    const response = await fetch("/oec-cat/api/unlock-all-candidates-test/", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseJson = await response.json();
    return responseJson;
  };
};

// un-assign candidate's test
const unAssignCandidate = (username, test) => {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/un-assign-candidate/?username_id=${encodeURIComponent(
        username
      )}&test_id=${test}`,
      {
        method: "POST",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
};

// Initial State
const initialState = {
  testOrderNumber: "",
  testToAdminister: "",
  testToAdministerDropdownDisabled: true,
  testSessionLanguage: "",
  testSessionLanguageDropdownDisabled: true,
  generateButtonDisabled: true,
  orderlessFinancialData: {},
  selectedSideNavItem: 1
};

const testAdministration = (state = initialState, action) => {
  switch (action.type) {
    case SET_TEST_ORDER_NUMBER_SELECTED_VALUE:
      return {
        ...state,
        testOrderNumber: action.testOrderNumber
      };
    case SET_TEST_TO_ADMINISTER_SELECTED_VALUE:
      return {
        ...state,
        testToAdminister: action.testToAdminister
      };
    case SET_TEST_TO_ADMINISTER_DROPDOWN_DISABLED:
      return {
        ...state,
        testToAdministerDropdownDisabled: action.testToAdministerDropdownDisabled
      };
    case SET_TEST_SESSION_LANGUAGE_SELECTED_VALUE:
      return {
        ...state,
        testSessionLanguage: action.testSessionLanguage
      };
    case SET_ORDERLESS_FINANCIAL_DATA_STATE:
      return {
        ...state,
        orderlessFinancialData: action.orderlessFinancialData
      };
    case SET_TEST_SESSION_LANGUAGE_DROPDOWN_DISABLED:
      return {
        ...state,
        testSessionLanguageDropdownDisabled: action.testSessionLanguageDropdownDisabled
      };
    case SET_GENERATE_BUTTON_DISABLED_STATE:
      return {
        ...state,
        generateButtonDisabled: action.generateButtonDisabled
      };
    case SET_TA_SIDE_NAV_STATE:
      return {
        ...state,
        selectedSideNavItem: action.selectedSideNavItem
      };
    case RESET_STATE:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

export default testAdministration;
export {
  initialState,
  getNewTestAccessCode,
  updateTestOrderNumberState,
  updateTestToAdministerState,
  updateTestToAdministerDropdownDisabledState,
  updateTestSessionLanguageState,
  updateTestSessionLanguageDropdownDisabledState,
  updateGenerateButtonDisabledState,
  resetTestAdministrationStates,
  deleteTestAccessCode,
  getActiveTestAccessCodesRedux,
  getTestAdministratorAssignedCandidates,
  updateTestTime,
  approveCandidate,
  lockCandidateTest,
  lockAllCandidatesTest,
  unlockCandidateTest,
  unlockAllCandidatesTest,
  unAssignCandidate,
  updateOrderlessFinancialDataState,
  updateBreakBank,
  setTAUserSideNavState
};
