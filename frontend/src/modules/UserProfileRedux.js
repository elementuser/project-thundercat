import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// Action Types
export const SET_PERMISSION_REQUEST_DATE = "user_profile/SET_PERMISSION_REQUEST_DATE";
export const TRIGGER_RERENDER = "user_profile/TRIGGER_RERENDER";
export const SET_EXTENDED_PROFILE = "user_profile/SET_EXTENDED_PROFILE";
export const RESET_STATE = "user_profile/RESET_STATE";
export const SET_CURRENT_EMPLOYER = "user_profile/SET_CURRENT_EMPLOYER";
export const SET_ORGANIZATION = "user_profile/SET_ORGANIZATION";
export const SET_GROUP = "user_profile/SET_GROUP";
export const SET_SUB_CLASSIFICATION = "user_profile/SET_SUB_CLASSIFICATION";
export const SET_LEVEL = "user_profile/SET_LEVEL";
export const SET_RESIDENCE = "user_profile/SET_RESIDENCE";
export const SET_EDUCATION = "user_profile/SET_EDUCATION";
export const SET_GENDER = "user_profile/SET_GENDER";

// update permission request data state
const updatePermissionRequestDataState = permissionRequestData => ({
  type: SET_PERMISSION_REQUEST_DATE,
  permissionRequestData
});

// update triggerRerender state
const triggerRerender = () => ({
  type: TRIGGER_RERENDER
});

const updateExtendedProfileState = (
  currentEmployer,
  organization,
  group,
  subclassification,
  level,
  residence,
  education,
  gender
) => ({
  type: SET_EXTENDED_PROFILE,
  currentEmployer,
  organization,
  group,
  subclassification,
  level,
  residence,
  education,
  gender
});

const updateCurrentEmployer = currentEmployer => ({
  type: SET_CURRENT_EMPLOYER,
  currentEmployer
});

const updateOrganization = organization => ({
  type: SET_ORGANIZATION,
  organization
});

const updateGroup = group => ({
  type: SET_GROUP,
  group
});

const updateSubclassification = subclassification => ({
  type: SET_SUB_CLASSIFICATION,
  subclassification
});

const updateLevel = level => ({
  type: SET_LEVEL,
  level
});

const updateResidence = residence => ({
  type: SET_RESIDENCE,
  residence
});

const updateEducation = education => ({
  type: SET_EDUCATION,
  education
});

const updateGender = gender => ({
  type: SET_GENDER,
  gender
});

// reset states
const resetUserProfileStates = () => ({ type: RESET_STATE });

// updating user's personal info
function updateUserPersonalInfo(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/update-user-personal-info/", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify({ ...data })
    });
    if (response.status === 200) {
      const responseObj = {
        body: await response.json(),
        ok: response.ok
      };
      return responseObj;
    }
    return response;
  };
}

// sending user profile change request
function sendUserProfileChangeRequest(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/send-user-profile-change-request/", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    return response;
  };
}

// getting user profile change request
function getUserProfileChangeRequest(username) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-user-profile-change-request/?username=${username}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseObj = {
      body: await response.json(),
      ok: response.ok
    };
    return responseObj;
  };
}

// deleting user profile change request
function deleteUserProfileChangeRequest(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/delete-user-profile-change-request/", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    return response;
  };
}

// updating user's extended info
function updateUserExtendedProfile(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/set-extended-profile/", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify({ ...data })
    });
    if (response.status === 200) {
      const responseObj = {
        body: await response.json(),
        ok: response.ok
      };
      return responseObj;
    }
    return response;
  };
}

function getUserExtendedProfile() {
  return async function () {
    const response = await fetch(`/oec-cat/api/get-extended-profile/`, {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseObj = {
      body: await response.json(),
      ok: response.ok
    };
    return responseObj;
  };
}

// Get Extended Profile for the selected user
function getSelectedUserExtendedProfile(username) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-selected-user-extended-profile/?username=${encodeURIComponent(username)}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseObj = {
      body: await response.json(),
      ok: response.ok
    };
    return responseObj;
  };
}

// Get TA Extended Profile
function getTaExtendedProfile(username) {
  return async function () {
    const response = await fetch(`/oec-cat/api/get-ta-extended-profile`, {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseObj = {
      body: await response.json(),
      ok: response.ok
    };
    return responseObj;
  };
}

// updating user's password
function updateUserPassword(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/auth/users/set_password/", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    // password updated successfully
    if (response.status === 204) {
      return response;
      // json response is useful for password errors, such as password too common, password too similar to username, first name, last name and email (response.status !== 204)
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// validating user credentials
function validateUserCredentials(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/auth/token/login/", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(data)
    });
    return response;
  };
}

// updating user's last password change time (date)
function updateUserLastPasswordChangeTime() {
  return async function () {
    const response = await fetch("/oec-cat/api/update-user-last-password-change-time/", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseJson = await response.json();
    return responseJson;
  };
}

// sending permission request
function sendPermissionRequest(data, priOrMilitaryNbr, i) {
  const body = {
    goc_email: data.gocEmail,
    pri_or_military_nbr: priOrMilitaryNbr,
    supervisor: data.supervisor,
    supervisor_email: data.supervisorEmail,
    rationale: data.rationale,
    permission_requested: data.permissions[i]
  };
  return async function () {
    const response = await fetch(`/oec-cat/api/send-permission-request/`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify({ ...body })
    });
    // if successful request
    if (response.status === 200) {
      return response;
      // else there is an error
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// Initial State
const initialState = {
  priOrMilitaryNbr: null,
  permissionRequestData: {
    gocEmail: "",
    supervisor: "",
    supervisorEmail: "",
    rationale: "",
    permissions: [],
    isValidForm: false
  },
  triggerRerender: false,
  extendedProfile: {
    currentEmployer: 0,
    organization: 0,
    group: 0,
    subclassification: 0,
    level: 0,
    residence: 0,
    education: 0,
    gender: 0
  }
};

// Reducer
const userProfile = (state = initialState, action) => {
  switch (action.type) {
    case SET_PERMISSION_REQUEST_DATE:
      return {
        ...state,
        permissionRequestData: {
          gocEmail: state.permissionRequestData.gocEmail,
          supervisor: state.permissionRequestData.supervisor,
          supervisorEmail: state.permissionRequestData.supervisorEmail,
          rationale: state.permissionRequestData.rationale,
          permissions: state.permissionRequestData.permissions,
          isValidForm: state.permissionRequestData.isValidForm
        }
      };
    case TRIGGER_RERENDER:
      return {
        ...state,
        triggerRerender: !state.triggerRerender
      };
    case SET_EXTENDED_PROFILE:
      return {
        ...state,
        extendedProfile: {
          currentEmployer: action.currentEmployer,
          organization: action.organization,
          group: action.group,
          subclassification: action.subclassification,
          level: action.level,
          residence: action.residence,
          education: action.education,
          gender: action.gender
        }
      };
    case SET_CURRENT_EMPLOYER:
      return {
        ...state,
        extendedProfile: { ...state.extendedProfile, currentEmployer: action.currentEmployer }
      };
    case SET_ORGANIZATION:
      return {
        ...state,
        extendedProfile: { ...state.extendedProfile, organization: action.organization }
      };
    case SET_GROUP:
      return {
        ...state,
        extendedProfile: { ...state.extendedProfile, group: action.group }
      };
    case SET_SUB_CLASSIFICATION:
      return {
        ...state,
        extendedProfile: { ...state.extendedProfile, subclassification: action.subclassification }
      };
    case SET_LEVEL:
      return {
        ...state,
        extendedProfile: { ...state.extendedProfile, level: action.level }
      };
    case SET_RESIDENCE:
      return {
        ...state,
        extendedProfile: { ...state.extendedProfile, residence: action.residence }
      };
    case SET_EDUCATION:
      return {
        ...state,
        extendedProfile: { ...state.extendedProfile, education: action.education }
      };
    case SET_GENDER:
      return {
        ...state,
        extendedProfile: { ...state.extendedProfile, gender: action.gender }
      };
    case RESET_STATE:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

export default userProfile;
export {
  updateUserPersonalInfo,
  updateExtendedProfileState,
  updateUserExtendedProfile,
  getUserExtendedProfile,
  getSelectedUserExtendedProfile,
  updateUserPassword,
  validateUserCredentials,
  updateUserLastPasswordChangeTime,
  updatePermissionRequestDataState,
  resetUserProfileStates,
  sendPermissionRequest,
  triggerRerender,
  updateCurrentEmployer,
  updateOrganization,
  updateGroup,
  updateSubclassification,
  updateLevel,
  updateResidence,
  updateEducation,
  updateGender,
  getTaExtendedProfile,
  sendUserProfileChangeRequest,
  getUserProfileChangeRequest,
  deleteUserProfileChangeRequest
};
