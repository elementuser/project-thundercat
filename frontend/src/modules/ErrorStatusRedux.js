// Action Types
const SET_ERROR_STATUS = "errorStatus/SET_ERROR_STATUS";
const RESET_STATE = "errorStatus/RESET_STATE";

// Action Creators
const setErrorStatus = errorStatus => ({
  type: SET_ERROR_STATUS,
  errorStatus
});
const resetErrorStatusState = () => ({ type: RESET_STATE });

// Initial State
const initialState = {
  errorStatus: false
};

// Reducer
const errorStatusRedux = (state = initialState, action) => {
  switch (action.type) {
    case SET_ERROR_STATUS:
      return {
        ...state,
        errorStatus: action.errorStatus
      };
    case RESET_STATE:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

export default errorStatusRedux;
export { resetErrorStatusState, setErrorStatus };
