// Action Types
const SET_TIME = "timer/SET_TIME";
const SET_DEFAULT_TIMES = "timer/SET_DEFAULT_TIMES";
const TRIGGER_TIME_UPDATE = "timer/TRIGGER_TIME_UPDATE";
const SET_VALID_TIME_STATE = "timer/SET_VALID_TIME_STATE";
const RESET_STATE = "timer/RESET_STATE";

// Action Creators
const updateTimerState = currentTimer => ({
  type: SET_TIME,
  currentTimer
});

const setDefaultTimes = defaultTimes => ({
  type: SET_DEFAULT_TIMES,
  defaultTimes
});

const triggerTimeUpdate = () => ({
  type: TRIGGER_TIME_UPDATE
});

const updateValidTimerState = validTimer => ({
  type: SET_VALID_TIME_STATE,
  validTimer
});

const resetTimerStates = () => ({ type: RESET_STATE });

// Initial State
const initialState = {
  currentTimer: [],
  defaultTimes: [],
  validTimer: true,
  timeUpdated: false
};

const timer = (state = initialState, action) => {
  switch (action.type) {
    case SET_TIME:
      return {
        ...state,
        currentTimer: action.currentTimer
      };
    case SET_DEFAULT_TIMES:
      return {
        ...state,
        defaultTimes: action.defaultTimes
      };
    case TRIGGER_TIME_UPDATE:
      return {
        ...state,
        timeUpdated: !state.timeUpdated
      };
    case SET_VALID_TIME_STATE:
      return {
        ...state,
        validTimer: action.validTimer
      };
    case RESET_STATE:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

export default timer;
export {
  initialState,
  updateTimerState,
  setDefaultTimes,
  triggerTimeUpdate,
  updateValidTimerState,
  resetTimerStates
};
