// ref: https://stackoverflow.com/questions/46946380/fetch-api-request-timeout
AbortSignal.timeout ??= function timeout(ms) {
  const ctrl = new AbortController();
  setTimeout(() => ctrl.abort(), ms);
  return ctrl.signal;
};

// function that allows us to make sure that the backend AND DB are up and running
// customizable timeout parameter (default timeout time is 5 seconds)
async function getBackendAndDbStatus(timeout = 5000) {
  // initializing backendIsUp
  let backendIsUp = true;
  try {
    // checking if backend is up
    await fetch("/oec-cat/api/backend-status", {
      method: "GET",
      Accept: "application/json",
      "Content-Type": "application/json",
      cache: "default",
      signal: AbortSignal.timeout(timeout)
    });
    // checking if DB is up
    await fetch("/oec-cat/api/database-check", {
      method: "GET",
      Accept: "application/json",
      "Content-Type": "application/json",
      cache: "default",
      signal: AbortSignal.timeout(timeout)
    });
  } catch {
    // setting backendIsUp to false
    backendIsUp = false;
  }
  return backendIsUp;
}

export default getBackendAndDbStatus;
