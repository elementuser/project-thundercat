// Action Types
const SET_CURRENT_TEST = "sampleTestStatus/SET_CURRENT_TEST";
const SET_PAGE = "sampleTestStatus/SET_PAGE";
const ACTIVATE_TEST = "sampleTestStatus/ACTIVATE_TEST";
const START_TEST = "sampleTestStatus/START_TEST";
const DEACTIVATE_TEST = "sampleTestStatus/DEACTIVATE_TEST";
const QUIT_TEST = "sampleTestStatus/QUIT_TEST";
const TIMEOUT_TEST = "sampleTestStatus/TIMEOUT_TEST";
const RESET_STATE = "sampleTestStatus/RESET_STATE";

// Action Creators
const setCurrentTest = (currentAssignedTestId, testId, startTime) => ({
  type: SET_CURRENT_TEST,
  currentAssignedTestId,
  testId,
  startTime
});
const setCurrentPage = page => ({
  type: SET_PAGE,
  page
});
const activateTest = () => ({ type: ACTIVATE_TEST });
const startTest = (timeLimit, currentAssignedTestId, startTime) => ({
  type: START_TEST,
  timeLimit,
  currentAssignedTestId,
  startTime
});
const deactivateTest = () => ({ type: DEACTIVATE_TEST });
const quitTest = () => ({ type: QUIT_TEST });
const timeoutTest = () => ({ type: TIMEOUT_TEST });
const resetTestStatusState = () => ({ type: RESET_STATE });

const PAGES = {
  preTest: "preTest",
  instructions: "instructions",
  background: "background",
  inbox: "inbox",
  confirm: "confirm",
  quit: "quit",
  timeout: "timeout"
};

// Initial State
const initialState = {
  isTestActive: false,
  currentPage: PAGES.preTest,
  startTime: null,
  timeLimit: null,
  currentAssignedTestId: null,
  currentTestId: null,
  isTestStarted: false
};

// Reducer
const sampleTestStatus = (state = initialState, action) => {
  switch (action.type) {
    case SET_CURRENT_TEST:
      return {
        ...state,
        currentAssignedTestId: action.currentAssignedTestId,
        currentTestId: action.testId
      };
    case SET_PAGE:
      return {
        ...state,
        currentPage: action.page
      };
    case ACTIVATE_TEST:
      return {
        ...state,
        isTestActive: true
      };
    case START_TEST:
      // TODO save start time in DB
      // TODO pass in time limit as a prop.
      const limit = action.timeLimit === undefined ? null : action.timeLimit;

      return {
        ...state,
        isTestStarted: true,
        currentAssignedTestId: action.currentAssignedTestId,
        startTime: action.startTime,
        timeLimit: limit,
        currentPage: PAGES.background
      };
    case DEACTIVATE_TEST:
      // TODO save end time in DB
      // TODO change status to submitted in DB
      return {
        ...state,
        isTestActive: false,
        isTestStarted: false,
        currentPage: PAGES.confirm,
        currentAssignedTestId: null,
        currentTestId: null,
        startTime: null
      };
    case QUIT_TEST:
      // TODO save end time in DB
      // TODO change status to quit in DB
      return {
        ...state,
        isTestActive: false,
        isTestStarted: false,
        currentPage: PAGES.quit,
        currentAssignedTestId: null,
        currentTestId: null,
        startTime: null
      };
    case TIMEOUT_TEST:
      // TODO save end time in DB
      // TODO change status to submitted in DB
      return {
        ...state,
        isTestStarted: false,
        currentPage: PAGES.timeout,
        currentAssignedTestId: null,
        currentTestId: null,
        startTime: null
      };
    case RESET_STATE:
      // TODO save end time in DB
      // TODO change status to submitted in DB
      return {
        ...initialState
      };
    default:
      return state;
  }
};

export default sampleTestStatus;
export {
  setCurrentTest,
  setCurrentPage,
  activateTest,
  startTest,
  initialState,
  deactivateTest,
  quitTest,
  timeoutTest,
  resetTestStatusState,
  PAGES
};
