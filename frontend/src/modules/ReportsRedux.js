import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// Action Types
export const SET_GENERATE_REPORT_BUTTON_DISABLED_STATE =
  "reports/SET_GENERATE_REPORT_BUTTON_DISABLED_STATE";
export const SET_SELECTED_PARAMETERS = "reports/SET_SELECTED_PARAMETERS";
export const RESET_STATE = "reports/RESET_STATE";

// Action Creator
// generate report disabled state
const setGenerateReportDisabledState = generateReportButtonDisabled => ({
  type: SET_GENERATE_REPORT_BUTTON_DISABLED_STATE,
  generateReportButtonDisabled
});

// selected parameters
const setReportSelectedParameters = (
  orderlessRequest,
  selectedTestOrderNumber,
  selectedTest,
  selectedCandidate,
  selectedParentCode,
  selectedTestCode,
  selectedTestVersion,
  selectedDateFrom,
  selectedDateTo,
  selectedAssignedTest
) => ({
  type: SET_SELECTED_PARAMETERS,
  orderlessRequest,
  selectedTestOrderNumber,
  selectedTest,
  selectedCandidate,
  selectedParentCode,
  selectedTestCode,
  selectedTestVersion,
  selectedDateFrom,
  selectedDateTo,
  selectedAssignedTest
});

// reset states
const resetReportsStates = () => ({
  type: RESET_STATE
});

// getting all TA assigned test order numbers
function getTaAssignedTestOrderNumbers(ta_id) {
  let endpoint = "";
  // ta_id parameter is not provided
  if (!ta_id) {
    endpoint = "/oec-cat/api/get-ta-assigned-test-order-numbers";
  }
  // ta_id is provided
  else {
    endpoint = `/oec-cat/api/get-ta-assigned-test-order-numbers?ta_id=${ta_id}`;
  }
  return async function () {
    const response = await fetch(endpoint, {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseJson = await response.json();
    return responseJson;
  };
}

// getting all existing test order numbers
function getAllExistingTestOrderNumbers() {
  return async function () {
    const response = await fetch("/oec-cat/api/get-all-existing-test-order-numbers", {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseJson = await response.json();
    return responseJson;
  };
}

// getting tests based on provided test order number/reference number
function getTestsBasedOnTestOrderNumber(
  test_order_number,
  orderless_request,
  requested_by_ta,
  provided_ta_username
) {
  let provided_ta_username_updated = provided_ta_username;
  if (provided_ta_username === null || provided_ta_username === "") {
    provided_ta_username_updated = " ";
  }
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-tests-based-on-test-order-number?test_order_number=${test_order_number}&orderless_request=${orderless_request}&provided_ta_username=${encodeURIComponent(
        provided_ta_username_updated
      )}&requested_by_ta=${requested_by_ta}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// getting tests based on provided test order number/reference number
function getCandidatesBasedOnSelectedTest(
  test_order_or_reference_number,
  orderless_request,
  test_id,
  requested_by_ta,
  provided_ta_username
) {
  let provided_ta_username_updated = provided_ta_username;
  if (provided_ta_username === null || provided_ta_username === "") {
    provided_ta_username_updated = " ";
  }
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-candidates-based-on-selected-test?test_order_or_reference_number=${test_order_or_reference_number}&orderless_request=${orderless_request}&test_id=${test_id}&provided_ta_username=${encodeURIComponent(
        provided_ta_username_updated
      )}&requested_by_ta=${requested_by_ta}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// getting report data (based on test order number, test_id and potentially ta_id and username_id)
// this API can also be used to get all the candidates based on the provided test_order_number and test_ids
function getResultsReportData(
  test_order_number,
  orderless_request,
  test_ids,
  remove_duplicates,
  username_id,
  // provide parameter as null if you want the results regardless the TAs
  ta_username
) {
  let endpoint = `/oec-cat/api/get-results-report-data?test_order_number=${test_order_number}&orderless_request=${orderless_request}&test_ids=${test_ids}&remove_duplicates=${remove_duplicates}`;
  // username_id parameter is provided
  if (username_id) {
    endpoint = endpoint.concat(`&username_id=${encodeURIComponent(username_id)}`);
  }
  // ta_username parameter is provided or null
  if (ta_username !== undefined) {
    endpoint = endpoint.concat(`&ta_username=${encodeURIComponent(ta_username)}`);
  }
  return async function () {
    const response = await fetch(endpoint, {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    // successful request
    if (response.status === 200) {
      const responseJson = await response.json();
      return responseJson;
    }
    return response;
  };
}

// getting financial report data
function getFinancialReportData(date_from, date_to) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-financial-report-data?date_from=${date_from}&date_to=${date_to}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    // successful request
    if (response.status === 200) {
      const responseJson = await response.json();
      return responseJson;
    }
    return response;
  };
}

// getting test definition data
function getTestDefinitionData(parent_code, test_code) {
  let endpoint = "";
  // no parameters provided
  if (!parent_code && !test_code) {
    endpoint = "/oec-cat/api/reports-get-test-definition-data";
    // parent_code parameter provided
  } else if (parent_code && !test_code) {
    endpoint = `/oec-cat/api/reports-get-test-definition-data/?parent_code=${parent_code}`;
    // parent_code and test_code parameters provided
  } else if (parent_code && test_code) {
    endpoint = `/oec-cat/api/reports-get-test-definition-data/?parent_code=${parent_code}&test_code=${test_code}`;
    // trigger error
  } else {
    throw new Error("Provided parameters don't match the API requirements");
  }
  return async function () {
    const response = await fetch(endpoint, {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseJson = await response.json();
    return responseJson;
  };
}

// getting test content report data
function getTestContentReportData(parent_code, test_code, version) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-test-content-report-data/?parent_code=${parent_code}&test_code=${test_code}&version=${version}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    // successful request
    if (response.status === 200) {
      const responseJson = await response.json();
      return responseJson;
    }
    return response;
  };
}

// getting test taker report data
function getTestTakerReportData(parent_code, test_code, date_from, date_to) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-test-taker-report-data/?parent_code=${parent_code}&test_code=${test_code}&date_from=${date_from}&date_to=${date_to}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    // successful request
    if (response.status === 200) {
      const responseJson = await response.json();
      return responseJson;
    }
    return response;
  };
}

// getting related tests based on provided test order number, test and candidate (username)
function getCandidateTests(test_order_number, test_id, username_id, orderless_request) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-candidate-tests?test_order_number=${test_order_number}&test_id=${test_id}&username_id=${encodeURIComponent(
        username_id
      )}&orderless_request=${orderless_request}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    // successful request
    if (response.status === 200) {
      const responseJson = await response.json();
      return responseJson;
    }
    return response;
  };
}

// getting candidate actions report data
function getCandidateActionsReportData(assigned_test_id) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-candidate-actions-report-data/?assigned_test_id=${assigned_test_id}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    // successful request
    if (response.status === 200) {
      const responseJson = await response.json();
      return responseJson;
    }
    return response;
  };
}

// Initial State
const initialState = {
  generateReportButtonDisabled: true,
  orderlessRequest: false,
  selectedTestOrderNumber: "",
  selectedTest: "",
  selectedCandidate: "",
  selectedParentCode: "",
  selectedTestCode: "",
  selectedTestVersion: "",
  selectedDateFrom: "",
  selectedDateTo: "",
  selectedAssignedTest: ""
};

// Reducer
const reports = (state = initialState, action) => {
  switch (action.type) {
    case SET_SELECTED_PARAMETERS:
      return {
        ...state,
        orderlessRequest: action.orderlessRequest,
        selectedTestOrderNumber: action.selectedTestOrderNumber,
        selectedTest: action.selectedTest,
        selectedCandidate: action.selectedCandidate,
        selectedParentCode: action.selectedParentCode,
        selectedTestCode: action.selectedTestCode,
        selectedTestVersion: action.selectedTestVersion,
        selectedDateFrom: action.selectedDateFrom,
        selectedDateTo: action.selectedDateTo,
        selectedAssignedTest: action.selectedAssignedTest
      };
    case SET_GENERATE_REPORT_BUTTON_DISABLED_STATE:
      return {
        ...state,
        generateReportButtonDisabled: action.generateReportButtonDisabled
      };
    case RESET_STATE:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

export default reports;
export {
  initialState,
  setReportSelectedParameters,
  setGenerateReportDisabledState,
  getTaAssignedTestOrderNumbers,
  getAllExistingTestOrderNumbers,
  getTestsBasedOnTestOrderNumber,
  getCandidatesBasedOnSelectedTest,
  getResultsReportData,
  getFinancialReportData,
  getTestDefinitionData,
  getTestContentReportData,
  getTestTakerReportData,
  getCandidateActionsReportData,
  getCandidateTests,
  resetReportsStates
};
