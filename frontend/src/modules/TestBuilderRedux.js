/* eslint-disable no-restricted-globals */
import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

function tryTestSection(order, newTestDef, language) {
  const newObj = {
    order,
    new_test: newTestDef,
    language
  };

  return async function () {
    const response = await fetch(`/oec-cat/api/try-test-section/`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(newObj)
    });
    const responseObj = {
      body: await response.json(),
      ok: response.ok
    };
    return responseObj;
  };
}

function getTestDefinitionData(
  combineVersions,
  parentCode,
  testCode,
  version,
  page,
  pageSize,
  keyword,
  currentLanguage,
  onlyActiveVersions,
  archived
) {
  // Encode search keyword so we can use special characters
  const encoded_keyword = encodeURIComponent(keyword);
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-test-definition-data/?combine_versions=${combineVersions}&parent_code=${parentCode}&test_code=${testCode}&version=${version}&page=${page}&page_size=${pageSize}&keyword=${encoded_keyword}&current_language=${currentLanguage}&only_active_versions=${onlyActiveVersions}&archived=${archived}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseObj = {
      body: await response.json(),
      ok: response.ok
    };
    return responseObj;
  };
}

// set archive to true in order to archive the specified test code and to false to restore it
function archiveTestDefinitionTestCode(parentCode, testCode, archive) {
  const newObj = {
    parent_code: parentCode,
    test_code: testCode,
    archive: archive.toString()
  };

  return async function () {
    const response = await fetch(`/oec-cat/api/archive-test-definition-test-code`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(newObj)
    });
    const responseObj = {
      body: await response.json(),
      ok: response.ok
    };
    return responseObj;
  };
}

function activateTestDefinition(test_definition, active) {
  const newObj = {
    test_definition,
    active
  };

  return async function () {
    const response = await fetch(`/oec-cat/api/test-definition`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(newObj)
    });
    const responseObj = {
      body: await response.json(),
      ok: response.ok
    };
    return responseObj;
  };
}

function getTestDefinitionExtract(test, currentLanguage) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-test-extract?test_definition=${test}&current_language=${currentLanguage}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseObj = {
      body: await response.json(),
      ok: response.ok
    };
    return responseObj;
  };
}

function uploadTest(newTestDef) {
  const newObj = {
    new_test: newTestDef
  };

  return async function () {
    const response = await fetch(`/oec-cat/api/upload-test`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(newObj)
    });
    const responseObj = {
      body: await response.json(),
      ok: response.ok
    };
    return responseObj;
  };
}

function getScoringMethodTypes(currentLanguage) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-scoring-method-types/?current_language=${currentLanguage}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseObj = {
      body: await response.json(),
      ok: response.ok
    };
    return responseObj;
  };
}

function getSpecifiedTestScoringMethodData(testId) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-specified-test-scoring-method-data/?test_id=${testId}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseObj = {
      body: await response.json(),
      ok: response.ok
    };
    return responseObj;
  };
}

function getTestDefinitionLatestVersion(parentCode, testCode) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-test-definition-latest-version/?parent_code=${parentCode}&test_code=${testCode}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseObj = {
      body: await response.json(),
      ok: response.ok
    };
    return responseObj;
  };
}

// constant strings for the python data object
export const TEST_DEFINITION = "test_definition";
export const TEST_SECTIONS = "test_sections";
export const NEXT_SECTION_BUTTONS = "next_section_buttons";
export const TEST_SECTION_COMPONENTS = "test_section_components";
export const SECTION_COMPONENT_PAGES = "section_component_pages";
export const PAGE_SECTIONS = "page_sections";
export const PAGE_SECTION_DEFINITIONS = "page_section_definitions";
export const ADDRESS_BOOK = "address_book";
export const CONTACT_DETAILS = "contact_details";
export const QUESTIONS = "questions";
export const EMAILS = "emails";
export const EMAIL_DETAILS = "email_details";
export const QUESTION_SECTIONS = "question_sections";
export const QUESTION_SECTION_DEFINITIONS = "question_section_definitions";
export const ANSWERS = "answers";
export const ANSWER_DETAILS = "answer_details";
export const MULTIPLE_CHOICE_QUESTION_DETAILS = "multiple_choice_question_details";
export const COMPETENCY_TYPES = "competency_types";
export const QUESTION_BLOCK_TYPE = "question_block_type";
export const QUESTION_BLOCK_TYPES = "question_block_types";
export const DEPENDENCIES = "dependencies";
export const QUESTION_LIST_RULES = "question_list_rules";
export const ITEM_BANK_RULES = "item_bank_rules";
export const QUESTION_SITUATIONS = "question_situations";
export const SITUATION_EXAMPLE_RATINGS = "situation_example_ratings";
export const SITUATION_EXAMPLE_RATING_DETAILS = "situation_example_rating_details";
export const SCORING_THRESHOLD = "scoring_threshold";

// Action Types
const LOAD_DEFINITION = "testBuilder/LOAD_DEFINITION";
const MODIFY_FIELD = "testBuilder/MODIFY_FIELD";
const ADD_FIELD = "testBuilder/ADD_FIELD";
const REPLACE_FIELD = "testBuilder/REPLACE_FIELD";
const REPLACE_ARRAY = "testBuilder/REPLACE_ARRAY";
const SET_TEST_SECTION_TO_VIEW = "testBuilder/SET_TEST_SECTION_TO_VIEW";
const SET_SCORING_METHOD_TYPE = "testBuilder/SET_SCORING_METHOD_TYPE";
const SET_SCORING_THRESHOLD = "testBuilder/SET_SCORING_THRESHOLD";
const SET_SCORING_PASS_FAIL_MINIMUM_SCORE = "testBuilder/SET_SCORING_PASS_FAIL_MINIMUM_SCORE";
const SET_FINAL_SCORING_THRESHOLD_CONVERSIONS =
  "testBuilder/SET_FINAL_SCORING_THRESHOLD_CONVERSIONS";
const SET_SCORING_METHOD_VALID_STATE = "testBuilder/SET_SCORING_METHOD_VALID_STATE";
const SET_TEST_DEFINITION_VALIDATION_ERRORS = "testBuilder/SET_TEST_DEFINITION_VALIDATION_ERRORS";
const SET_QUESTION_RULES_VALID_STATE = "testBuilder/SET_QUESTION_RULES_VALID_STATE";
const SET_QUESTION_RULES_VALID_STATE_WHEN_ITEM_EXPOSURE_ENABLED =
  "testBuilder/SET_QUESTION_RULES_VALID_STATE_WHEN_ITEM_EXPOSURE_ENABLED";
const SET_TEST_DEFINITION_ALL_TESTS_PAGINATION_PAGE =
  "testBuilder/SET_TEST_DEFINITION_ALL_TESTS_PAGINATION_PAGE";
const SET_TEST_DEFINITION_ALL_TESTS_PAGINATION_PAGE_SIZE =
  "testBuilder/SET_TEST_DEFINITION_ALL_TESTS_PAGINATION_PAGE_SIZE";
const SET_TEST_DEFINITION_ALL_TESTS_SEARCH_PARAMETERS =
  "testBuilder/SET_TEST_DEFINITION_ALL_TESTS_SEARCH_PARAMETERS";
const SET_TEST_DEFINITION_ALL_ACTIVE_TEST_VERSIONS_PAGINATION_PAGE =
  "testBuilder/SET_TEST_DEFINITION_ALL_ACTIVE_TEST_VERSIONS_PAGINATION_PAGE";
const SET_TEST_DEFINITION_ALL_ACTIVE_TEST_VERSIONS_PAGINATION_PAGE_SIZE =
  "testBuilder/SET_TEST_DEFINITION_ALL_ACTIVE_TEST_VERSIONS_PAGINATION_PAGE_SIZE";
const SET_TEST_DEFINITION_ALL_ACTIVE_TEST_VERSIONS_SEARCH_PARAMETERS =
  "testBuilder/SET_TEST_DEFINITION_ALL_ACTIVE_TEST_VERSIONS_SEARCH_PARAMETERS";
const SET_TEST_DEFINITION_ARCHIVED_TEST_CODES_PAGINATION_PAGE =
  "testBuilder/SET_TEST_DEFINITION_ARCHIVED_TEST_CODES_PAGINATION_PAGE";
const SET_TEST_DEFINITION_ARCHIVED_TEST_CODES_PAGINATION_PAGE_SIZE =
  "testBuilder/SET_TEST_DEFINITION_ARCHIVED_TEST_CODES_PAGINATION_PAGE_SIZE";
const SET_TEST_DEFINITION_ARCHIVED_TEST_CODES_SEARCH_PARAMETERS =
  "testBuilder/SET_TEST_DEFINITION_ARCHIVED_TEST_CODES_SEARCH_PARAMETERS";
const SET_SELECTED_TEST_DEFINITION_ID = "testBuilder/SET_SELECTED_TEST_DEFINITION_ID";
const TRIGGER_SELECT_TEST_DEFINITION_TABLES_REFRESH =
  "testBuilder/TRIGGER_SELECT_TEST_DEFINITION_TABLES_REFRESH";
const TRIGGER_TEST_DEFINITION_LOADING = "testBuilder/TRIGGER_TEST_DEFINITION_LOADING";
const TRIGGER_TEST_DEFINITION_LOADING_FROM_JSON =
  "testBuilder/TRIGGER_TEST_DEFINITION_LOADING_FROM_JSON";
const TRIGGER_TEST_DEFINITION_FIELD_UPDATES = "testBuilder/TRIGGER_TEST_DEFINITION_FIELD_UPDATES";
const SET_TEST_DEFINITION_SELECTION_SIDE_NAV_STATE =
  "testBuilder/SET_TEST_DEFINITION_SELECTION_SIDE_NAV_STATE";
const SET_SIDE_NAV_STATE = "testBuilder/SET_SIDE_NAV_STATE";
const SET_NEW_TEST_DEFINITION_UPLOADED_STATE = "testBuilder/SET_NEW_TEST_DEFINITION_UPLOADED_STATE";
const SET_TEST_BUILDER_TOP_TAB = "testBuilder/SET_TEST_BUILDER_TOP_TAB";
const RESET_STATE = "testBuilder/RESET_STATE";

// Action Creators
const loadTestDefinition = state => ({ type: LOAD_DEFINITION, state });
const modifyTestDefinitionField = (rootObj, newField, id, reorderBy = undefined) => ({
  type: MODIFY_FIELD,
  rootObj,
  newField,
  id,
  reorderBy
});
const addTestDefinitionField = (rootObj, newField, reorderBy = undefined) => ({
  type: ADD_FIELD,
  rootObj,
  newField,
  reorderBy
});
const replaceTestDefinitionField = (rootObj, newField, reorderBy = undefined) => ({
  type: REPLACE_FIELD,
  rootObj,
  newField,
  reorderBy
});
const setTestSectionToView = order => ({
  type: SET_TEST_SECTION_TO_VIEW,
  order
});
const setScoringMethodType = scoring_method_type => ({
  type: SET_SCORING_METHOD_TYPE,
  scoring_method_type
});
const modifyScoringThresholdField = (rootObj, newField, id, reorderBy = undefined) => ({
  type: MODIFY_FIELD,
  rootObj,
  newField,
  id,
  reorderBy
});
const addScoringThresholdField = (rootObj, newField, reorderBy = undefined) => ({
  type: ADD_FIELD,
  rootObj,
  newField,
  reorderBy
});
const setScoringThreshold = scoring_threshold => ({
  type: SET_SCORING_THRESHOLD,
  scoring_threshold
});
const replaceScoringThresholdField = (rootObj, newField, reorderBy = undefined) => ({
  type: REPLACE_FIELD,
  rootObj,
  newField,
  reorderBy
});
const setScoringPassFailMinimumScore = scoring_pass_fail_minimum_score => ({
  type: SET_SCORING_PASS_FAIL_MINIMUM_SCORE,
  scoring_pass_fail_minimum_score
});
const setFinalScoringThresholdConversions = final_scoring_threshold_conversions => ({
  type: SET_FINAL_SCORING_THRESHOLD_CONVERSIONS,
  final_scoring_threshold_conversions
});
const setScoringMethodValidState = scoring_method_valid_state => ({
  type: SET_SCORING_METHOD_VALID_STATE,
  scoring_method_valid_state
});
const setTestDefinitionValidationErrors = validation_errors => ({
  type: SET_TEST_DEFINITION_VALIDATION_ERRORS,
  validation_errors
});
const setQuestionRulesValidState = question_rules_valid_state => ({
  type: SET_QUESTION_RULES_VALID_STATE,
  question_rules_valid_state
});
const setQuestionRulesValidStateWhenItemExposureEnabled = (
  question_rules_valid_state,
  question_rules_error_type
) => ({
  type: SET_QUESTION_RULES_VALID_STATE_WHEN_ITEM_EXPOSURE_ENABLED,
  question_rules_valid_state,
  question_rules_error_type
});
const updateCurrentTestDefinitionAllTestsPageState = testDefinitionAllTestsPaginationPage => ({
  type: SET_TEST_DEFINITION_ALL_TESTS_PAGINATION_PAGE,
  testDefinitionAllTestsPaginationPage
});
const updateCurrentTestDefinitionAllTestsPageSizeState =
  testDefinitionAllTestsPaginationPageSize => ({
    type: SET_TEST_DEFINITION_ALL_TESTS_PAGINATION_PAGE_SIZE,
    testDefinitionAllTestsPaginationPageSize
  });
const updateSearchAllTestDefinitionsStates = (
  testDefinitionAllTestsKeyword,
  testDefinitionAllTestsActiveSearch
) => ({
  type: SET_TEST_DEFINITION_ALL_TESTS_SEARCH_PARAMETERS,
  testDefinitionAllTestsKeyword,
  testDefinitionAllTestsActiveSearch
});
const updateCurrentTestDefinitionAllActiveTestVersionsPageState =
  testDefinitionAllActiveTestVersionsPaginationPage => ({
    type: SET_TEST_DEFINITION_ALL_ACTIVE_TEST_VERSIONS_PAGINATION_PAGE,
    testDefinitionAllActiveTestVersionsPaginationPage
  });
const updateCurrentTestDefinitionAllActiveTestVersionsPageSizeState =
  testDefinitionAllActiveTestVersionsPaginationPageSize => ({
    type: SET_TEST_DEFINITION_ALL_ACTIVE_TEST_VERSIONS_PAGINATION_PAGE_SIZE,
    testDefinitionAllActiveTestVersionsPaginationPageSize
  });
const updateSearchAllActiveTestVersionsDefinitionsStates = (
  testDefinitionAllActiveTestVersionsKeyword,
  testDefinitionAllActiveTestVersionsActiveSearch
) => ({
  type: SET_TEST_DEFINITION_ALL_ACTIVE_TEST_VERSIONS_SEARCH_PARAMETERS,
  testDefinitionAllActiveTestVersionsKeyword,
  testDefinitionAllActiveTestVersionsActiveSearch
});
const updateCurrentTestDefinitionArchivedTestCodesPageState =
  testDefinitionArchivedTestCodesPaginationPage => ({
    type: SET_TEST_DEFINITION_ARCHIVED_TEST_CODES_PAGINATION_PAGE,
    testDefinitionArchivedTestCodesPaginationPage
  });
const updateCurrentTestDefinitionArchivedTestCodesPageSizeState =
  testDefinitionArchivedTestCodesPaginationPageSize => ({
    type: SET_TEST_DEFINITION_ARCHIVED_TEST_CODES_PAGINATION_PAGE_SIZE,
    testDefinitionArchivedTestCodesPaginationPageSize
  });
const updateSearchArchivedTestCodesDefinitionsStates = (
  testDefinitionArchivedTestCodesKeyword,
  testDefinitionArchivedTestCodesActiveSearch
) => ({
  type: SET_TEST_DEFINITION_ARCHIVED_TEST_CODES_SEARCH_PARAMETERS,
  testDefinitionArchivedTestCodesKeyword,
  testDefinitionArchivedTestCodesActiveSearch
});
const setSelectedTestDefinitionId = selectedTestDefinitionId => ({
  type: SET_SELECTED_TEST_DEFINITION_ID,
  selectedTestDefinitionId
});
const triggerSelectTestDefinitionTablesRefresh = () => ({
  type: TRIGGER_SELECT_TEST_DEFINITION_TABLES_REFRESH
});
const triggerTestDefinitionLoading = () => ({
  type: TRIGGER_TEST_DEFINITION_LOADING
});
const triggerTestDefinitionLoadingFromJson = state => ({
  type: TRIGGER_TEST_DEFINITION_LOADING_FROM_JSON,
  state
});
const triggerTestDefinitionFieldUpdates = () => ({
  type: TRIGGER_TEST_DEFINITION_FIELD_UPDATES
});
const replaceTestDefinitionArray = (rootArray, newArray) => ({
  type: REPLACE_ARRAY,
  rootArray,
  newArray
});
const setTestDefinitionSelectionSideNavState = selectedTestDefinitionSelectionSideNavItem => ({
  type: SET_TEST_DEFINITION_SELECTION_SIDE_NAV_STATE,
  selectedTestDefinitionSelectionSideNavItem
});
const setTestBuilderSideNavState = selectedSideNavItem => ({
  type: SET_SIDE_NAV_STATE,
  selectedSideNavItem
});
const setNewTestDefinitionUploadedState = newTestDefinitionUploaded => ({
  type: SET_NEW_TEST_DEFINITION_UPLOADED_STATE,
  newTestDefinitionUploaded
});
const setTestBuilderTopTab = testBuilderTopTab => ({
  type: SET_TEST_BUILDER_TOP_TAB,
  testBuilderTopTab
});
const resetTestBuilderState = () => ({
  type: RESET_STATE
});
const sortByOrder = (a, b) => (a.order > b.order ? 1 : -1);
export const sortById = (a, b) => (a.id > b.id ? 1 : -1);
const sortByEmailId = (a, b) => (a.email_id > b.email_id ? 1 : -1);

const orderByCustomAttribute = orderByAttribute => {
  return function (a, b) {
    const firstElement = isNaN(a[orderByAttribute])
      ? a[orderByAttribute]
      : parseInt(a[orderByAttribute]);
    const secondElement = isNaN(b[orderByAttribute])
      ? b[orderByAttribute]
      : parseInt(b[orderByAttribute]);
    if (firstElement < secondElement) {
      return -1;
    }
    if (firstElement > secondElement) {
      return 1;
    }
    return 0;
  };
};

export const emptyState = {
  test_definition: [
    {
      id: null,
      parent_code: "",
      test_code: "",
      version: null,
      retest_period: 0,
      en_name: "",
      fr_name: "",
      is_public: null,
      count_up: false,
      active: false,
      test_language: null
    }
  ],
  test_sections: [],
  next_section_buttons: [],
  test_section_components: [],
  section_component_pages: [],
  page_sections: [],
  page_section_definitions: [],
  address_book: [],
  address_book_contacts: [],
  questions: [],
  question_list_rules: [],
  item_bank_rules: [],
  question_block_types: [],
  question_sections: [],
  question_section_definitions: [],
  emails: [],
  email_details: [],
  contact_details: [],
  answers: [],
  answer_details: [],
  multiple_choice_question_details: [],
  competency_types: [],
  question_situations: [],
  situation_example_ratings: [],
  situation_example_rating_details: [],
  scoring_method_type: "",
  scoring_threshold: [],
  scoring_pass_fail_minimum_score: "",
  final_scoring_threshold_conversions: [],
  scoring_method_valid_state: false,
  validation_errors: {},
  question_rules_valid_state: true,
  question_rules_error_type: null,
  testDefinitionAllTestsPaginationPage: 1,
  testDefinitionAllTestsPaginationPageSize: 25,
  testDefinitionAllTestsKeyword: "",
  testDefinitionAllTestsActiveSearch: false,
  testDefinitionAllActiveTestVersionsPaginationPage: 1,
  testDefinitionAllActiveTestVersionsPaginationPageSize: 25,
  testDefinitionAllActiveTestVersionsKeyword: "",
  testDefinitionAllActiveTestVersionsActiveSearch: false,
  testDefinitionArchivedTestCodesPaginationPage: 1,
  testDefinitionArchivedTestCodesPaginationPageSize: 25,
  testDefinitionArchivedTestCodesKeyword: "",
  testDefinitionArchivedTestCodesActiveSearch: false,
  selectedTestDefinitionId: null,
  triggerSelectTestDefinitionTablesRefreshState: false,
  triggerTestDefinitionLoading: false,
  triggerTestDefinitionLoadingFromJson: false,
  triggerTestDefinitionFieldUpdates: false,
  selectedTestDefinitionSelectionSideNavItem: 0,
  selectedSideNavItem: 0,
  newTestDefinitionUploaded: false,
  testBuilderTopTab: ""
};

// Reducer
const testBuilder = (state = emptyState, action) => {
  let newState = {
    ...state
  };
  let newStateArray = [];

  switch (action.type) {
    case LOAD_DEFINITION:
      return {
        ...action.state,
        triggerTestDefinitionFieldUpdates: !state.triggerTestDefinitionFieldUpdates
      };

    case MODIFY_FIELD:
      newStateArray = [];
      newState = {
        ...state,
        triggerTestDefinitionFieldUpdates: !state.triggerTestDefinitionFieldUpdates
      };
      let modified = false;
      newStateArray = newState[`${action.rootObj}`].map(content => {
        if (content.id === action.id) {
          modified = true;
          return action.newField;
        }
        return content;
      });
      if (!modified) {
        newStateArray.push(action.newField);
      }

      // re-order based on obj property
      if (action.reorderBy !== undefined) {
        newStateArray = newStateArray.sort(orderByCustomAttribute(action.reorderBy));
      }
      // end re-ordering
      else {
        // sort by email id to keep questions ordered
        // eslint-disable-next-line no-lonely-if
        if (action.rootObj === EMAILS) {
          newStateArray.sort(sortByEmailId);
        } else if (action.newField.order !== undefined) {
          newStateArray.sort(sortByOrder);
        }
      }

      newState[`${action.rootObj}`] = newStateArray;
      return {
        ...newState
      };

    case ADD_FIELD:
      newStateArray = [];
      newState = {
        ...state
      };
      newStateArray = newState[`${action.rootObj}`];

      // if newStateArray is undefined
      if (typeof newStateArray === "undefined") {
        // initialize it with an empty array
        newStateArray = [];
      }

      newStateArray.push(action.newField);

      // re-order based on obj property
      if (action.reorderBy !== undefined) {
        newStateArray = newStateArray.sort(orderByCustomAttribute(action.reorderBy));
      }
      // end re-ordering
      else {
        newStateArray.sort(sortByOrder);
      }
      // trigger new state
      const newArray = newStateArray.map(x => {
        return x;
      });
      newState[`${action.rootObj}`] = newArray;
      return {
        ...newState
      };

    case REPLACE_FIELD:
      newStateArray = action.newField;
      newState = {
        ...state
      };

      // re-order based on obj property
      if (action.reorderBy !== undefined) {
        newStateArray = newStateArray.sort(orderByCustomAttribute(action.reorderBy));
      }
      // end re-ordering
      else {
        newStateArray.sort(sortByOrder);
      }

      newState[`${action.rootObj}`] = newStateArray;
      return {
        ...newState
      };

    case REPLACE_ARRAY:
      newStateArray = action.newArray;
      newState = {
        ...state
      };

      newState[`${action.rootArray}`] = newStateArray;
      return {
        ...newState
      };

    case SET_TEST_SECTION_TO_VIEW:
      return {
        ...state,
        viewTestSectionOrderNumber: action.order
      };

    case SET_SCORING_METHOD_TYPE:
      return {
        ...state,
        scoring_method_type: action.scoring_method_type
      };

    case SET_SCORING_THRESHOLD:
      return {
        ...state,
        scoring_threshold: action.scoring_threshold
      };

    case SET_SCORING_PASS_FAIL_MINIMUM_SCORE:
      return {
        ...state,
        scoring_pass_fail_minimum_score: action.scoring_pass_fail_minimum_score
      };

    case SET_FINAL_SCORING_THRESHOLD_CONVERSIONS:
      return {
        ...state,
        final_scoring_threshold_conversions: action.final_scoring_threshold_conversions
      };

    case SET_SCORING_METHOD_VALID_STATE:
      return {
        ...state,
        scoring_method_valid_state: action.scoring_method_valid_state
      };

    case SET_TEST_DEFINITION_VALIDATION_ERRORS:
      return {
        ...state,
        validation_errors: action.validation_errors
      };

    case SET_QUESTION_RULES_VALID_STATE:
      return {
        ...state,
        question_rules_valid_state: action.question_rules_valid_state
      };

    case SET_QUESTION_RULES_VALID_STATE_WHEN_ITEM_EXPOSURE_ENABLED:
      return {
        ...state,
        question_rules_valid_state: action.question_rules_valid_state,
        question_rules_error_type: action.question_rules_error_type
      };

    case SET_TEST_DEFINITION_ALL_TESTS_PAGINATION_PAGE:
      return {
        ...state,
        testDefinitionAllTestsPaginationPage: action.testDefinitionAllTestsPaginationPage
      };

    case SET_TEST_DEFINITION_ALL_TESTS_PAGINATION_PAGE_SIZE:
      return {
        ...state,
        testDefinitionAllTestsPaginationPageSize: action.testDefinitionAllTestsPaginationPageSize
      };

    case SET_TEST_DEFINITION_ALL_TESTS_SEARCH_PARAMETERS:
      return {
        ...state,
        testDefinitionAllTestsKeyword: action.testDefinitionAllTestsKeyword,
        testDefinitionAllTestsActiveSearch: action.testDefinitionAllTestsActiveSearch
      };

    case SET_TEST_DEFINITION_ALL_ACTIVE_TEST_VERSIONS_PAGINATION_PAGE:
      return {
        ...state,
        testDefinitionAllActiveTestVersionsPaginationPage:
          action.testDefinitionAllActiveTestVersionsPaginationPage
      };

    case SET_TEST_DEFINITION_ALL_ACTIVE_TEST_VERSIONS_PAGINATION_PAGE_SIZE:
      return {
        ...state,
        testDefinitionAllActiveTestVersionsPaginationPageSize:
          action.testDefinitionAllActiveTestVersionsPaginationPageSize
      };

    case SET_TEST_DEFINITION_ALL_ACTIVE_TEST_VERSIONS_SEARCH_PARAMETERS:
      return {
        ...state,
        testDefinitionAllActiveTestVersionsKeyword:
          action.testDefinitionAllActiveTestVersionsKeyword,
        testDefinitionAllActiveTestVersionsActiveSearch:
          action.testDefinitionAllActiveTestVersionsActiveSearch
      };

    case SET_TEST_DEFINITION_ARCHIVED_TEST_CODES_PAGINATION_PAGE:
      return {
        ...state,
        testDefinitionArchivedTestCodesPaginationPage:
          action.testDefinitionArchivedTestCodesPaginationPage
      };

    case SET_TEST_DEFINITION_ARCHIVED_TEST_CODES_PAGINATION_PAGE_SIZE:
      return {
        ...state,
        testDefinitionArchivedTestCodesPaginationPageSize:
          action.testDefinitionArchivedTestCodesPaginationPageSize
      };

    case SET_TEST_DEFINITION_ARCHIVED_TEST_CODES_SEARCH_PARAMETERS:
      return {
        ...state,
        testDefinitionArchivedTestCodesKeyword: action.testDefinitionArchivedTestCodesKeyword,
        testDefinitionArchivedTestCodesActiveSearch:
          action.testDefinitionArchivedTestCodesActiveSearch
      };

    case SET_SELECTED_TEST_DEFINITION_ID:
      return {
        ...state,
        selectedTestDefinitionId: action.selectedTestDefinitionId
      };

    case TRIGGER_SELECT_TEST_DEFINITION_TABLES_REFRESH:
      return {
        ...state,
        triggerSelectTestDefinitionTablesRefreshState:
          !state.triggerSelectTestDefinitionTablesRefreshState
      };

    case TRIGGER_TEST_DEFINITION_LOADING:
      return {
        ...state,
        triggerTestDefinitionLoading: !state.triggerTestDefinitionLoading
      };

    case TRIGGER_TEST_DEFINITION_LOADING_FROM_JSON:
      return {
        ...action.state,
        triggerTestDefinitionLoadingFromJson: !state.triggerTestDefinitionLoadingFromJson
      };

    case TRIGGER_TEST_DEFINITION_FIELD_UPDATES:
      return {
        ...state,
        triggerTestDefinitionFieldUpdates: !state.triggerTestDefinitionFieldUpdates
      };

    case SET_TEST_DEFINITION_SELECTION_SIDE_NAV_STATE:
      return {
        ...state,
        selectedTestDefinitionSelectionSideNavItem:
          action.selectedTestDefinitionSelectionSideNavItem
      };

    case SET_SIDE_NAV_STATE:
      return {
        ...state,
        selectedSideNavItem: action.selectedSideNavItem
      };

    case SET_NEW_TEST_DEFINITION_UPLOADED_STATE:
      return {
        ...state,
        newTestDefinitionUploaded: action.newTestDefinitionUploaded
      };

    case SET_TEST_BUILDER_TOP_TAB:
      return {
        ...state,
        testBuilderTopTab: action.testBuilderTopTab
      };

    case RESET_STATE:
      return {
        ...emptyState
      };

    default:
      return {
        ...state
      };
  }
};

export default testBuilder;
export {
  loadTestDefinition,
  modifyTestDefinitionField,
  resetTestBuilderState,
  addTestDefinitionField,
  setTestSectionToView,
  replaceTestDefinitionField,
  // oec-cat/api
  tryTestSection,
  uploadTest,
  getTestDefinitionData,
  getTestDefinitionExtract,
  activateTestDefinition,
  getScoringMethodTypes,
  setScoringMethodType,
  modifyScoringThresholdField,
  addScoringThresholdField,
  replaceScoringThresholdField,
  setScoringPassFailMinimumScore,
  setFinalScoringThresholdConversions,
  setScoringMethodValidState,
  setScoringThreshold,
  setTestDefinitionValidationErrors,
  updateCurrentTestDefinitionAllTestsPageState,
  updateCurrentTestDefinitionAllTestsPageSizeState,
  updateSearchAllTestDefinitionsStates,
  updateCurrentTestDefinitionAllActiveTestVersionsPageState,
  updateCurrentTestDefinitionAllActiveTestVersionsPageSizeState,
  updateSearchAllActiveTestVersionsDefinitionsStates,
  updateCurrentTestDefinitionArchivedTestCodesPageState,
  updateCurrentTestDefinitionArchivedTestCodesPageSizeState,
  updateSearchArchivedTestCodesDefinitionsStates,
  setSelectedTestDefinitionId,
  triggerTestDefinitionLoading,
  triggerTestDefinitionLoadingFromJson,
  getTestDefinitionLatestVersion,
  archiveTestDefinitionTestCode,
  triggerSelectTestDefinitionTablesRefresh,
  replaceTestDefinitionArray,
  triggerTestDefinitionFieldUpdates,
  setQuestionRulesValidState,
  setQuestionRulesValidStateWhenItemExposureEnabled,
  setTestDefinitionSelectionSideNavState,
  setTestBuilderSideNavState,
  setNewTestDefinitionUploadedState,
  getSpecifiedTestScoringMethodData,
  setTestBuilderTopTab
};
