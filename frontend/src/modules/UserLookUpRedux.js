import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// Action types
// User Look-Up Pagination
export const SET_USER_LOOK_UP_PAGINATION_PAGE = "userLookUp/SET_USER_LOOK_UP_PAGINATION_PAGE";
export const SET_USER_LOOK_UP_PAGINATION_PAGE_SIZE =
  "userLookUp/SET_USER_LOOK_UP_PAGINATION_PAGE_SIZE";
export const SET_USER_LOOK_UP_SEARCH_PARAMETERS = "userLookUp/SET_USER_LOOK_UP_SEARCH_PARAMETERS";
export const SET_SELECTED_USERNAME = "userLookUp/SET_SELECTED_USERNAME";
export const SET_SELECTED_USER_FIRSTNAME = "userLookUp/SET_SELECTED_USER_FIRSTNAME";
export const SET_SELECTED_USER_LASTNAME = "userLookUp/SET_SELECTED_USER_LASTNAME";
export const SET_BUSINESS_OPERATIONS_SIDE_NAV_STATE =
  "userLookUp/SET_BUSINESS_OPERATIONS_SIDE_NAV_STATE";
export const RESET_STATE = "userLookUp/RESET_STATE";

// update pagination page state (User Look-up)
const updateUserLookUpPageState = userLookUpPaginationPage => ({
  type: SET_USER_LOOK_UP_PAGINATION_PAGE,
  userLookUpPaginationPage
});
// update pagination pageSize state (User Look-up)
const updateUserLookUpPageSizeState = userLookUpPaginationPageSize => ({
  type: SET_USER_LOOK_UP_PAGINATION_PAGE_SIZE,
  userLookUpPaginationPageSize
});
// update search keyword state (User Look-up)
const updateSearchUserLookUpStates = (userLookUpKeyword, userLookUpActiveSearch) => ({
  type: SET_USER_LOOK_UP_SEARCH_PARAMETERS,
  userLookUpKeyword,
  userLookUpActiveSearch
});
// set selected user username
const setSelectedUsername = selectedUsername => ({
  type: SET_SELECTED_USERNAME,
  selectedUsername
});
// set selected user firstname
const setSelectedUserFirstname = selectedUserFirstname => ({
  type: SET_SELECTED_USER_FIRSTNAME,
  selectedUserFirstname
});
// set selected user lastname
const setSelectedUserLastname = selectedUserLastname => ({
  type: SET_SELECTED_USER_LASTNAME,
  selectedUserLastname
});
// set BO user side nav state
const setBOUserSideNavState = selectedSideNavItem => ({
  type: SET_BUSINESS_OPERATIONS_SIDE_NAV_STATE,
  selectedSideNavItem
});
const resetUserLookUpState = () => ({ type: RESET_STATE });

// getting list of all users
const getAllUsers = (page, pageSize) => {
  return async function () {
    const tests = await fetch(`/oec-cat/api/get-all-users/?page=${page}&page_size=${pageSize}`, {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const testsJson = await tests.json();
    return testsJson;
  };
};

// getting list of found users
const getAllFoundUsers = (currentLanguage, providedKeyword, page, pageSize) => {
  // if provided keyword is empty
  let keyword = providedKeyword;
  if (keyword === "") {
    // send a space as the keyword parameter
    keyword = " ";
  }
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-found-users/?current_language=${currentLanguage}&keyword=${keyword}&page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
};

const getSelectedUserPersonalInfo = selectedUser => {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-selected-user-personal-info/?username=${encodeURIComponent(selectedUser)}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
};

// Initial State
const initialState = {
  userLookUpPaginationPage: 1,
  userLookUpPaginationPageSize: 25,
  userLookUpKeyword: null,
  userLookUpActiveSearch: false,
  selectedUsername: null,
  selectedUserFirstname: null,
  selectedUserLastname: null,
  selectedSideNavItem: 1
};

const userLookUp = (state = initialState, action) => {
  switch (action.type) {
    case SET_USER_LOOK_UP_PAGINATION_PAGE:
      return {
        ...state,
        userLookUpPaginationPage: action.userLookUpPaginationPage
      };
    case SET_USER_LOOK_UP_PAGINATION_PAGE_SIZE:
      return {
        ...state,
        userLookUpPaginationPageSize: action.userLookUpPaginationPageSize
      };
    case SET_USER_LOOK_UP_SEARCH_PARAMETERS:
      return {
        ...state,
        userLookUpKeyword: action.userLookUpKeyword,
        userLookUpActiveSearch: action.userLookUpActiveSearch
      };
    case SET_SELECTED_USERNAME:
      return {
        ...state,
        selectedUsername: action.selectedUsername
      };
    case SET_SELECTED_USER_FIRSTNAME:
      return {
        ...state,
        selectedUserFirstname: action.selectedUserFirstname
      };
    case SET_SELECTED_USER_LASTNAME:
      return {
        ...state,
        selectedUserLastname: action.selectedUserLastname
      };
    case SET_BUSINESS_OPERATIONS_SIDE_NAV_STATE:
      return {
        ...state,
        selectedSideNavItem: action.selectedSideNavItem
      };
    case RESET_STATE:
      // Ensure local storage is cleaned up on logout
      return {
        ...initialState
      };
    default:
      return state;
  }
};

export default userLookUp;
export {
  initialState,
  getAllUsers,
  getAllFoundUsers,
  getSelectedUserPersonalInfo,
  updateUserLookUpPageState,
  updateUserLookUpPageSizeState,
  updateSearchUserLookUpStates,
  setSelectedUsername,
  setSelectedUserFirstname,
  setSelectedUserLastname,
  resetUserLookUpState,
  setBOUserSideNavState
};
