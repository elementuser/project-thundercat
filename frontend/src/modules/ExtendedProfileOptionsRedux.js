import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// Action Types
const SET_EMPLOYER_OPTIONS = "extendedProfileOptions/SET_EMPLOYER_OPTIONS";
const SET_ORGANIZATION_OPTIONS = "extendedProfileOptions/SET_ORGANIZATION_OPTIONS";
const SET_GROUP_OPTIONS = "extendedProfileOptions/SET_GROUP_OPTIONS";
const SET_SUBCLASSIFICATION_OPTIONS = "extendedProfileOptions/SET_SUBCLASSIFICATION_OPTIONS";
const SET_RESIDENCE_OPTIONS = "extendedProfileOptions/SET_RESIDENCE_OPTIONS";
const SET_EDUCATION_OPTIONS = "extendedProfileOptions/SET_EDUCATION_OPTIONS";
const SET_GENDER_OPTIONS = "extendedProfileOptions/SET_GENDER_OPTIONS";
const RESET_STATE = "extendedProfileOptions/RESET_STATE";

// Action Creators
// ...
const setEmployerOptions = options => ({ type: SET_EMPLOYER_OPTIONS, options });
const setOrganizationOptions = options => ({ type: SET_ORGANIZATION_OPTIONS, options });
const setGroupOptions = options => ({ type: SET_GROUP_OPTIONS, options });
const setSubClassificationOptions = options => ({ type: SET_SUBCLASSIFICATION_OPTIONS, options });
const setResidenceOptions = options => ({ type: SET_RESIDENCE_OPTIONS, options });
const setEducationOptions = options => ({ type: SET_EDUCATION_OPTIONS, options });
const setGenderOptions = options => ({ type: SET_GENDER_OPTIONS, options });

// API Calls
function getExtendedProfileOptions(list_name) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-extended-profile-dropdown-options?list_name=${list_name}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseObj = {
      body: await response.json(),
      ok: response.ok
    };
    return responseObj;
  };
}

function getEmployer() {
  return getExtendedProfileOptions("employer");
}

function getOrganization() {
  return getExtendedProfileOptions("organization");
}

function getGroup() {
  return getExtendedProfileOptions("group");
}

function getSubClassifications() {
  return getExtendedProfileOptions("subclassification");
}

function getResidence() {
  return getExtendedProfileOptions("residence");
}

function getEducation() {
  return getExtendedProfileOptions("education");
}

function getGender() {
  return getExtendedProfileOptions("gender");
}

function getAboriginal() {
  return getExtendedProfileOptions("aboriginal");
}

function getVisibleMinority() {
  return getExtendedProfileOptions("visible_minority");
}

function getDisability() {
  return getExtendedProfileOptions("disability");
}

function isLoaded(
  employerOptions,
  organizationOptions,
  groupOptions,
  subClassificationOptions,
  residenceOptions,
  educationOptions,
  genderOptions,
  minorityOptions
) {
  if (employerOptions === []) {
    return false;
  }
  if (organizationOptions === []) {
    return false;
  }
  if (groupOptions === []) {
    return false;
  }
  if (subClassificationOptions === []) {
    return false;
  }
  if (residenceOptions === []) {
    return false;
  }
  if (educationOptions === []) {
    return false;
  }
  if (genderOptions === []) {
    return false;
  }
  if (minorityOptions === []) {
    return false;
  }
  return true;
}

// Initial State
const initialState = {
  loaded: false,
  employerOptions: { en: [], fr: [] },
  organizationOptions: { en: [], fr: [] },
  groupOptions: { groups: [], groupSubClassifications: [] },
  subClassificationOptions: { subClassifications: [], subClassificationLevels: [] },
  residenceOptions: { en: [], fr: [] },
  educationOptions: { en: [], fr: [] },
  genderOptions: { en: [], fr: [] },
  minorityOptions: { en: [], fr: [] }
};

// Reducer
const extendedProfileOptions = (state = initialState, action) => {
  let loaded = false;
  switch (action.type) {
    case SET_EMPLOYER_OPTIONS:
      loaded = isLoaded(
        action.options,
        state.organizationOptions,
        state.groupOptions,
        state.subClassificationOptions,
        state.residenceOptions,
        state.educationOptions,
        state.genderOptions,
        state.minorityOptions
      );
      return {
        ...state,
        loaded: loaded,
        employerOptions: action.options
      };
    case SET_ORGANIZATION_OPTIONS:
      loaded = isLoaded(
        state.employerOptions,
        action.options,
        state.groupOptions,
        state.subClassificationOptions,
        state.residenceOptions,
        state.educationOptions,
        state.genderOptions,
        state.minorityOptions
      );
      return {
        ...state,
        loaded: loaded,
        organizationOptions: action.options
      };
    case SET_GROUP_OPTIONS:
      loaded = isLoaded(
        state.employerOptions,
        state.organizationOptions,
        action.options,
        state.subClassificationOptions,
        state.residenceOptions,
        state.educationOptions,
        state.genderOptions,
        state.minorityOptions
      );
      return {
        ...state,
        loaded: loaded,
        groupOptions: action.options
      };
    case SET_SUBCLASSIFICATION_OPTIONS:
      loaded = isLoaded(
        state.employerOptions,
        state.organizationOptions,
        state.groupOptions,
        action.options,
        state.residenceOptions,
        state.educationOptions,
        state.genderOptions,
        state.minorityOptions
      );
      return {
        ...state,
        loaded: loaded,
        subClassificationOptions: action.options
      };
    case SET_RESIDENCE_OPTIONS:
      loaded = isLoaded(
        state.employerOptions,
        state.organizationOptions,
        state.groupOptions,
        state.subClassificationOptions,
        action.options,
        state.educationOptions,
        state.genderOptions,
        state.minorityOptions
      );
      return {
        ...state,
        loaded: loaded,
        residenceOptions: action.options
      };
    case SET_EDUCATION_OPTIONS:
      loaded = isLoaded(
        state.employerOptions,
        state.organizationOptions,
        state.groupOptions,
        state.subClassificationOptions,
        state.residenceOptions,
        action.options,
        state.genderOptions,
        state.minorityOptions
      );
      return {
        ...state,
        loaded: loaded,
        educationOptions: action.options
      };
    case SET_GENDER_OPTIONS:
      loaded = isLoaded(
        state.employerOptions,
        state.organizationOptions,
        state.groupOptions,
        state.subClassificationOptions,
        state.residenceOptions,
        state.educationOptions,
        action.options,
        state.minorityOptions
      );
      return {
        ...state,
        loaded: loaded,
        genderOptions: action.options
      };

    case RESET_STATE:
      return {
        ...initialState
      };

    default:
      return {
        ...state
      };
  }
};

export default extendedProfileOptions;
export {
  getEmployer,
  getOrganization,
  getGroup,
  getSubClassifications,
  getResidence,
  getEducation,
  getGender,
  getAboriginal,
  getVisibleMinority,
  getDisability,
  setEmployerOptions,
  setOrganizationOptions,
  setGroupOptions,
  setSubClassificationOptions,
  setResidenceOptions,
  setEducationOptions,
  setGenderOptions
};
