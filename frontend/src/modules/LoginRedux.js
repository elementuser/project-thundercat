import clearTestSessionStorage from "../helpers/localStorage";
import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// Action Types
// AUTH ACTIONS
export const AUTHENTICATED = "AUTHENTICATED";
export const UNAUTHENTICATED = "UNAUTHENTICATED";
export const REGISTRATION = "REGISTRATION";
const RESET_STATE = "login/RESET_STATE";

// CHANGE PASSWORD ACTIONS
export const IS_CHANGING_PASSWORD = "IS_CHANGING_PASSWORD";
export const CHANGE_PASSWORD_SUCCESS = "CHANGE_PASSWORD_SUCCESS";
export const CHANGE_PASSWORD_FAILURE = "CHANGE_PASSWORD_FAILURE";

// updates authenticated state
const authenticateAction = authenticated => ({ type: AUTHENTICATED, authenticated });

// saves the authentication token in local storage
function handleAuthResponseAndState(response) {
  return async () => {
    if (navigator.cookieEnabled) {
      SessionStorage(ACTION.SET, ITEM.AUTH_TOKEN, response.access);
      SessionStorage(ACTION.SET, ITEM.REFRESH_TOKEN, response.refresh);
    }
  };
}

// updates pageHasError state
const updatePageHasErrorState = pageHasError => ({
  type: REGISTRATION,
  pageHasError
});
const resetLoginState = () => ({
  type: RESET_STATE
});

function registerAction(data) {
  return async () => {
    const response = await fetch("/oec-cat/api/auth/users/", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(data)
    });
    const responseJson = response.json();
    return responseJson;
  };
}

// getting user's token
function loginAction(data) {
  return async () => {
    const response = await fetch("/oec-cat/api/auth/jwt/create_token/", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(data)
    });
    const responseJson = await response.json();
    return responseJson;
  };
}

// updating last login field in DB
function updateLastLoginField(data) {
  return async () => {
    const response = await fetch("/oec-cat/api/auth/token/login/", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(data)
    });
    return response;
  };
}

// check if the token is still valid
function isTokenStillValid() {
  return async () => {
    const authToken = SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN);
    const response = await fetch("/oec-cat/api/auth/jwt/verify_token/", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({ token: authToken })
    });

    return response.ok;
  };
}

// refreshing user auth token
function refreshAuthToken() {
  return async () => {
    const refreshToken = SessionStorage(ACTION.GET, ITEM.REFRESH_TOKEN);
    const response = await fetch("/oec-cat/api/auth/jwt/refresh_token/", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({ refresh: refreshToken })
    });
    const responseJson = await response.json();
    return responseJson;
  };
}

// getting user's information (id, first name, last name, birth date, email, username and pri or military number)
function getUserInformation() {
  return async () => {
    const accountInfo = await fetch("/oec-cat/api/auth/users/me/", {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const accountInfoResponseJson = await accountInfo.json();
    return accountInfoResponseJson;
  };
}

// JWT tokens are not stored in our DB
function logoutAction() {
  clearTestSessionStorage();
  return { type: UNAUTHENTICATED };
}

// Initial State
const initialState = {
  authenticated: false,
  pageHasError: false
};

// Reducer
const login = (state = initialState, action) => {
  switch (action.type) {
    case AUTHENTICATED:
      return {
        ...state,
        authenticated: action.authenticated
      };
    case UNAUTHENTICATED:
      return { authenticated: false };
    case REGISTRATION:
      return {
        ...state,
        pageHasError: action.pageHasError
      };
    case RESET_STATE:
      return { ...initialState };
    default:
      return state;
  }
};

export default login;
export {
  initialState,
  registerAction,
  loginAction,
  authenticateAction,
  handleAuthResponseAndState,
  logoutAction,
  refreshAuthToken,
  getUserInformation,
  updatePageHasErrorState,
  updateLastLoginField,
  resetLoginState,
  isTokenStillValid
};
