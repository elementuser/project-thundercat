import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// getting the assigned test of the specified user
const getInvalidateTestReasonRedux = assignedTestId => {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-reason-for-invalidating-the-test/?assigned_test_id=${assignedTestId}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
};
export default getInvalidateTestReasonRedux;
