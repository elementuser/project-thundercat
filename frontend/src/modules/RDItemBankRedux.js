/* eslint-disable no-restricted-globals */
import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// Action Types
const SET_ACTIVE_ITEM_BANKS_PAGINATION_PAGE = "rdItemBanks/SET_ACTIVE_ITEM_BANKS_PAGINATION_PAGE";
const SET_ACTIVE_ITEM_BANKS_PAGINATION_PAGE_SIZE =
  "rdItemBanks/SET_ACTIVE_ITEM_BANKS_PAGINATION_PAGE_SIZE";
const SET_ACTIVE_ITEM_BANKS_SEARCH_PARAMETERS =
  "rdItemBanks/SET_ACTIVE_ITEM_BANKS_SEARCH_PARAMETERS";
const TRIGGER_TABLE_UPDATES = "rdItemBanks/TRIGGER_TABLE_UPDATES";
const SET_ITEM_BANK_DATA = "rdItemBanks/SET_ITEM_BANK_DATA";
const TRIGGER_RERENDER = "rdItemBanks/TRIGGER_RERENDER";
const RESET_SELECTED_ACTIVE_ITEM_BANK_STATES = "rdItemBanks/RESET_SELECTED_ACTIVE_ITEM_BANK_STATES";
const RESET_STATE = "rdItemBanks/RESET_STATE";

const updateActiveItemBanksPageState = activeItemBanksPaginationPage => ({
  type: SET_ACTIVE_ITEM_BANKS_PAGINATION_PAGE,
  activeItemBanksPaginationPage
});

const updateActiveItemBanksPageSizeState = activeItemBanksPaginationPageSize => ({
  type: SET_ACTIVE_ITEM_BANKS_PAGINATION_PAGE_SIZE,
  activeItemBanksPaginationPageSize
});

const updateActiveSearchItemBanksStates = (
  activeItemBanksKeyword,
  activeItemBanksActiveSearch
) => ({
  type: SET_ACTIVE_ITEM_BANKS_SEARCH_PARAMETERS,
  activeItemBanksKeyword,
  activeItemBanksActiveSearch
});

const triggerTableUpdates = () => ({
  type: TRIGGER_TABLE_UPDATES
});

const setItemBankData = itemBankData => ({
  type: SET_ITEM_BANK_DATA,
  itemBankData
});

const triggerRerender = () => ({
  type: TRIGGER_RERENDER
});

const resetSelectedActiveItemBanksStates = () => ({
  type: RESET_SELECTED_ACTIVE_ITEM_BANK_STATES
});

const resetActiveItemBanksState = () => ({
  type: RESET_STATE
});

// getting all item banks (duplicate of getAllItemBanks in ItemBankRedux file - did that to better organise the redux states/functions)
function getAllItemBanks(callSource, page, pageSize) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-all-item-banks?call_source=${callSource}&page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// getting found item banks
function getFoundItemBanks(providedKeyword, currentLanguage, callSource, page, pageSize) {
  // if provided keyword is empty
  let keyword = providedKeyword;
  if (keyword === "") {
    // send a space as the keyword parameter
    keyword = " ";
  }

  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-found-item-banks?keyword=${keyword}&current_language=${currentLanguage}&call_source=${callSource}&page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// creating new item bank
function createNewItemBank(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/create-new-item-bank", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    return response;
  };
}

// getting all item banks (duplicate of getAllItemBanks in ItemBankRedux file - did that to better organise the redux states/functions)
function getSelectedItemBankData(itemBankId) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-selected-item-bank-permissions?item_bank_id=${itemBankId}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// updating item bank data
function updateItemBankDataAsRdOperations(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/update-item-bank-data-as-rd-operations", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    if (response.status === 200 || response.status === 409) {
      return response;
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// adding test developer for item bank permissions/accesses
function addTestDeveloper(usernameId, itemBankId) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/add-test-developer-item-bank-permissions/?username_id=${encodeURIComponent(
        usernameId
      )}&item_bank_id=${itemBankId}`,
      {
        method: "POST",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    return response;
  };
}

// deleting test developer for item bank permissions/accesses
function deleteTestDeveloper(usernameId, itemBankId) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/delete-test-developer-item-bank-permissions/?username_id=${encodeURIComponent(
        usernameId
      )}&item_bank_id=${itemBankId}`,
      {
        method: "POST",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    return response;
  };
}

// getting item bank access types
function getItemBankAccessTypes(usernameId, itemBankId) {
  return async function () {
    const response = await fetch("/oec-cat/api/get-item-bank-access-types", {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseJson = await response.json();
    return responseJson;
  };
}

// updating test developer item bank permission/access
function updateTestDeveloperAccess(usernameId, itemBankId, itemBankAccessTypeId) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/update-item-bank-access-types/?username_id=${encodeURIComponent(
        usernameId
      )}&item_bank_id=${itemBankId}&item_bank_access_type_id=${itemBankAccessTypeId}`,
      {
        method: "POST",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    return response;
  };
}

// Initial State
const initialState = {
  activeItemBanksPaginationPage: 1,
  activeItemBanksPaginationPageSize: 25,
  activeItemBanksKeyword: "",
  activeItemBanksActiveSearch: false,
  triggerTableUpdates: false,
  itemBankData: {},
  triggerRerender: false
};

// Reducer
const rdItemBanks = (state = initialState, action) => {
  switch (action.type) {
    case SET_ACTIVE_ITEM_BANKS_PAGINATION_PAGE:
      return {
        ...state,
        activeItemBanksPaginationPage: action.activeItemBanksPaginationPage
      };
    case SET_ACTIVE_ITEM_BANKS_PAGINATION_PAGE_SIZE:
      return {
        ...state,
        activeItemBanksPaginationPageSize: action.activeItemBanksPaginationPageSize
      };
    case SET_ACTIVE_ITEM_BANKS_SEARCH_PARAMETERS:
      return {
        ...state,
        activeItemBanksKeyword: action.activeItemBanksKeyword,
        activeItemBanksActiveSearch: action.activeItemBanksActiveSearch
      };
    case TRIGGER_TABLE_UPDATES:
      return {
        ...state,
        triggerTableUpdates: !state.triggerTableUpdates
      };
    case SET_ITEM_BANK_DATA:
      return {
        ...state,
        itemBankData: action.itemBankData
      };
    case TRIGGER_RERENDER:
      return {
        ...state,
        triggerRerender: !state.triggerRerender
      };
    case RESET_SELECTED_ACTIVE_ITEM_BANK_STATES:
      return {
        ...state,
        itemBankData: {}
      };
    case RESET_STATE:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

export default rdItemBanks;
export {
  initialState,
  updateActiveItemBanksPageState,
  updateActiveItemBanksPageSizeState,
  updateActiveSearchItemBanksStates,
  getAllItemBanks,
  getFoundItemBanks,
  createNewItemBank,
  getSelectedItemBankData,
  triggerTableUpdates,
  setItemBankData,
  updateItemBankDataAsRdOperations,
  addTestDeveloper,
  deleteTestDeveloper,
  getItemBankAccessTypes,
  updateTestDeveloperAccess,
  triggerRerender,
  resetSelectedActiveItemBanksStates,
  resetActiveItemBanksState
};
