import { combineReducers } from "redux";
import localize from "./LocalizeRedux";
import login from "./LoginRedux";
import testStatus from "./TestStatusRedux";
import sampleTestStatus from "./SampleTestStatusRedux";
import userPermissions from "./PermissionsRedux";
import notepad from "./NotepadRedux";
import datePicker from "./DatePickerRedux";
import userProfile from "./UserProfileRedux";
import testSection from "./TestSectionRedux";
import user from "./UserRedux";
import navTabs from "./NavTabsRedux";
import scorer from "./ScorerRedux";
import testAdministration from "./TestAdministrationRedux";
import questionList from "./QuestionListRedux";
import assignedTest from "./AssignedTestsRedux";
import emailInbox from "./EmailInboxRedux";
import timer from "./SetTimerRedux";
import testBuilder from "./TestBuilderRedux";
import errorStatusRedux from "./ErrorStatusRedux";
import accommodations from "./AccommodationsRedux";
import reports from "./ReportsRedux";
import extendedProfileOptions from "./ExtendedProfileOptionsRedux";
import testAccessCodes from "./TestAccessCodesRedux";
import activeTests from "./ActiveTestsRedux";
import invalidateTestReasonRedux from "./InvalidateTestRedux";
import userLookUp from "./UserLookUpRedux";
import uit from "./UitRedux";
import calculator from "./CalculatorRedux";
import itemBank from "./ItemBankRedux";
import systemAlerts from "./SystemAlertsRedux";
import rdItemBanks from "./RDItemBankRedux";
import app from "./AppRedux";
import testActions from "./UpdateResponseRedux";

export default combineReducers({
  localize,
  login,
  testStatus,
  sampleTestStatus,
  userPermissions,
  notepad,
  datePicker,
  userProfile,
  testSection,
  user,
  navTabs,
  scorer,
  testAdministration,
  questionList,
  assignedTest,
  emailInbox,
  timer,
  testBuilder,
  errorStatusRedux,
  accommodations,
  reports,
  extendedProfileOptions,
  testAccessCodes,
  activeTests,
  invalidateTestReasonRedux,
  userLookUp,
  uit,
  calculator,
  itemBank,
  systemAlerts,
  rdItemBanks,
  app,
  testActions
});
