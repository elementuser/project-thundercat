import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// getting the assigned test of the specified user
const getScoredTests = () => {
  return async function () {
    const tests = await fetch("/oec-cat/api/assigned-scored-tests/", {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const testsJson = await tests.json();
    return testsJson;
  };
};

// getting All tests of the specified user
const getAllTests = username => {
  return async function () {
    const tests = await fetch(
      `/oec-cat/api/get-all-tests-for-selected-user/?username=${encodeURIComponent(username)}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const testsJson = await tests.json();
    return testsJson;
  };
};
export default getScoredTests;
export { getAllTests };
