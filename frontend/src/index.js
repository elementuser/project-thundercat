import "react-app-polyfill/ie9";
import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { Provider } from "react-redux";
import store from "./store-index";
import CacheBuster from "react-cache-buster";
import packageInfo from "../package.json";

ReactDOM.render(
  <CacheBuster
    currentVersion={packageInfo.version}
    isEnabled={true} // If false, the library is disabled.
    isVerboseMode={false} // If true, the library writes verbose logs to console.
    metaFileDirectory={"./"}
  >
    <Provider store={store}>
      <App />
    </Provider>
  </CacheBuster>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
