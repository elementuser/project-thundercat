import React from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import LOCALIZE from "./text_resources";
import "./css/site-nav-menu.css";
import { PATH } from "./SiteNavBar";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCalendarCheck,
  faUserCircle,
  // faFile,
  faTachometerAlt,
  // faEnvelope,
  faSignOutAlt,
  faUserClock,
  faUserShield,
  faUserCog,
  faUserEdit,
  faHammer,
  faSortDown
} from "@fortawesome/free-solid-svg-icons";
import { resetTestStatusState } from "./modules/TestStatusRedux";
import { resetAccommodations, getLineSpacingCSS } from "./modules/AccommodationsRedux";
import { resetNotepadState } from "./modules/NotepadRedux";
import { resetTestStatusState as resetSampleTestStatusState } from "./modules/SampleTestStatusRedux";
import { logoutAction } from "./modules/LoginRedux";
import { resetErrorStatusState } from "./modules/ErrorStatusRedux";
import { useMenuState, Menu, MenuButton, MenuItem } from "reakit/Menu";
import { resetUserState } from "./modules/UserRedux";
import { resetUserLookUpState } from "./modules/UserLookUpRedux";
import { resetAllItemBanksStates } from "./modules/ItemBankRedux";
import { resetPermissionsState } from "./modules/PermissionsRedux";
import { resetActiveItemBanksState } from "./modules/RDItemBankRedux";
import { resetTestAdministrationStates } from "./modules/TestAdministrationRedux";

// This file uses react hooks (a functional component feature) and classes.
// Normally these two cannot be used together (we need to for the menu package).
// I use a higher order component at the bottom to wrap the react hooks because
// they cannot be used inside a render function.
// https://infinum.com/the-capsized-eight/how-to-use-react-hooks-in-class-components

const styles = {
  menuButton: {
    marginTop: "12px",
    border: "none",
    backgroundColor: "#fff",
    color: "#00565e"
  },
  icon: {
    marginRight: 12,
    color: "white"
  },
  text: {
    color: "white"
  },
  menuItem: {
    position: "relative",
    display: "block",
    border: "none"
  },
  menu: {
    zIndex: 9999,
    backgroundColor: "#00565e",
    border: "none"
  },
  menuArrow: {
    verticalAlign: "inherit",
    marginLeft: 4
  }
};

class SiteNavMenu extends React.Component {
  static props = {
    // Props from Redux
    resetInboxState: PropTypes.func,
    resetMetaDataState: PropTypes.func,
    resetTestStatusState: PropTypes.func,
    resetSampleTestStatusState: PropTypes.func,
    resetNotepadState: PropTypes.func,
    logoutAction: PropTypes.func,
    resetErrorStatusState: PropTypes.func,
    resetUserState: PropTypes.func,
    resetUserLookUpState: PropTypes.func
  };

  render() {
    let style = {
      fontFamily: this.props.accommodations.fontFamily,
      fontSize: this.props.accommodations.fontSize
    };
    if (this.props.accommodations.spacing) {
      style = { ...style, ...getLineSpacingCSS() };
    }
    const { myHookValue } = this.props;
    return <>{myHookValue(style)}</>;
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    isEtta: state.userPermissions.isEtta,
    isRdOperations: state.userPermissions.isRdOperations,
    isPpc: state.userPermissions.isPpc,
    isTa: state.userPermissions.isTa,
    isScorer: state.userPermissions.isScorer,
    isTb: state.userPermissions.isTb,
    isTd: state.userPermissions.isTd,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      logoutAction,
      resetTestStatusState,
      resetSampleTestStatusState,
      resetNotepadState,
      resetErrorStatusState,
      resetAccommodations,
      resetUserState,
      resetUserLookUpState,
      resetAllItemBanksStates,
      resetPermissionsState,
      resetActiveItemBanksState,
      resetTestAdministrationStates
    },
    dispatch
  );

const getItemList = (props, menu, style) => {
  const items = [];
  const className = "dropdown-item";
  const classNameActive = "dropdown-item dropdown-item-active";

  if (props.isEtta || props.isRdOperations) {
    items.push([
      <MenuItem
        className={
          window.location.pathname === PATH.systemAdministration ? classNameActive : className
        }
        {...menu}
        key={1}
        style={{ ...styles.menuItem, ...style }}
        onClick={() => {
          window.location.href = PATH.systemAdministration;
        }}
        id={1}
      >
        <FontAwesomeIcon style={styles.icon} icon={faUserShield} />
        <span style={styles.text}>
          {props.isEtta && !props.isRdOperations
            ? LOCALIZE.menu.etta
            : !props.isEtta && props.isRdOperations
            ? LOCALIZE.menu.rdOperations
            : LOCALIZE.menu.operations}
        </span>
      </MenuItem>
    ]);
  }

  if (props.isPpc) {
    items.push([
      <MenuItem
        {...menu}
        key={2}
        style={{ ...styles.menuItem, ...style }}
        onClick={() => {
          window.location.href = PATH.ppcAdministration;
        }}
        className={
          window.location.pathname === PATH.ppcAdministration ? classNameActive : className
        }
        id={2}
      >
        <FontAwesomeIcon style={styles.icon} icon={faUserCog} />
        <span style={styles.text}>{LOCALIZE.menu.ppc}</span>
      </MenuItem>
    ]);
  }
  if (props.isScorer) {
    items.push([
      <MenuItem
        {...menu}
        key={3}
        style={{ ...styles.menuItem, ...style }}
        onClick={() => {
          window.location.href = PATH.scorerBase;
        }}
        className={window.location.pathname === PATH.scorerBase ? classNameActive : className}
        id={3}
      >
        <FontAwesomeIcon style={styles.icon} icon={faUserEdit} />
        <span style={styles.text}>{LOCALIZE.menu.scorer}</span>
      </MenuItem>
    ]);
  }

  if (props.isTa) {
    items.push([
      <MenuItem
        {...menu}
        key={4}
        style={{ ...styles.menuItem, ...style }}
        onClick={() => {
          window.location.href = PATH.testAdministration;
        }}
        className={
          window.location.pathname === PATH.testAdministration ? classNameActive : className
        }
        id={4}
      >
        <FontAwesomeIcon style={styles.icon} icon={faUserClock} />
        <span style={styles.text}>{LOCALIZE.menu.ta}</span>
      </MenuItem>
    ]);
  }
  if (props.isTb || props.isTd) {
    items.push([
      <MenuItem
        {...menu}
        key={5}
        style={{ ...styles.menuItem, ...style }}
        onClick={() => {
          window.location.href = PATH.testBuilder;
        }}
        className={
          window.location.pathname === PATH.testBuilder ||
          window.location.pathname === PATH.itemBankEditor ||
          window.location.pathname === PATH.itemEditor ||
          window.location.pathname === PATH.bundleEditor
            ? classNameActive
            : className
        }
        id={5}
      >
        <FontAwesomeIcon style={styles.icon} icon={faHammer} />
        <span style={styles.text}>{LOCALIZE.menu.testBuilder}</span>
      </MenuItem>
    ]);
  }

  items.push([
    <MenuItem
      {...menu}
      key={6}
      style={{ ...styles.menuItem, ...style }}
      onClick={() => {
        window.location.href = PATH.dashboard;
      }}
      className={window.location.pathname === PATH.dashboard ? classNameActive : className}
      id={6}
    >
      <FontAwesomeIcon style={styles.icon} icon={faCalendarCheck} />
      <span style={styles.text}>{LOCALIZE.menu.checkIn}</span>
    </MenuItem>
  ]);

  items.push([
    <MenuItem
      {...menu}
      key={7}
      style={{ ...styles.menuItem, ...style }}
      onClick={() => {
        window.location.href = PATH.profile;
      }}
      className={window.location.pathname === PATH.profile ? classNameActive : className}
      id={7}
    >
      <FontAwesomeIcon style={styles.icon} icon={faUserCircle} />
      <span style={styles.text}>{LOCALIZE.menu.profile}</span>
    </MenuItem>
  ]);
  // items.push([
  //   <MenuItem
  //     {...menu}
  //     key={8}
  //     style={styles.menuItem}
  //     onClick={() => {
  //       window.location.href = PATH.incidentReport;
  //     }}
  //     className={window.location.pathname === PATH.incidentReport ? classNameActive : className}
  //     id={8}
  //   >
  //     <FontAwesomeIcon style={styles.icon} icon={faFile} />
  //     <span style={{ ...styles.text, ...style }}>{LOCALIZE.menu.incidentReport}</span>
  //   </MenuItem>
  // ]);
  items.push([
    <MenuItem
      {...menu}
      key={9}
      style={{ ...styles.menuItem, ...style }}
      onClick={() => {
        window.location.href = PATH.myTests;
      }}
      className={window.location.pathname === PATH.myTests ? classNameActive : className}
      id={9}
    >
      <FontAwesomeIcon style={styles.icon} icon={faTachometerAlt} />
      <span style={styles.text}>{LOCALIZE.menu.MyTests}</span>
    </MenuItem>
  ]);
  // items.push([
  //   <MenuItem
  //     {...menu}
  //     key={10}
  //     style={styles.menuItem}
  //     onClick={() => {
  //       window.location.href = PATH.contactUs;
  //     }}
  //     className={window.location.pathname === PATH.contactUs ? classNameActive : className}
  //     id={10}
  //   >
  //     <FontAwesomeIcon style={styles.icon} icon={faEnvelope} />
  //     <span style={{ ...styles.text, ...style }}>{LOCALIZE.menu.ContactUs}</span>
  //   </MenuItem>
  // ]);
  items.push([
    <MenuItem
      className={"dropdown-item"}
      {...menu}
      key={11}
      style={{ ...styles.menuItem, ...style }}
      onClick={() => {
        props.resetTestStatusState();
        props.resetSampleTestStatusState();
        props.resetAccommodations();
        props.resetNotepadState();
        props.logoutAction();
        props.resetUserState();
        props.resetErrorStatusState();
        props.resetUserLookUpState();
        props.resetAllItemBanksStates();
        props.resetPermissionsState();
        props.resetActiveItemBanksState();
        props.resetTestAdministrationStates();
      }}
      id={11}
    >
      <FontAwesomeIcon style={styles.icon} icon={faSignOutAlt} />
      <span style={styles.text}>{LOCALIZE.menu.logout}</span>
    </MenuItem>
  ]);

  return items;
};

function MenuHooks(props) {
  const menu = useMenuState({ loop: true });
  return function renderReakit(style) {
    return (
      <>
        <MenuButton
          {...menu}
          style={{ ...styles.menuButton, ...style, paddingBottom: 0 }}
          id="navigation-bar-main-link-id"
        >
          {LOCALIZE.mainTabs.menu}
          <FontAwesomeIcon style={styles.menuArrow} icon={faSortDown} />
        </MenuButton>
        <Menu {...menu} aria-label={LOCALIZE.mainTabs.menu} style={styles.menu}>
          {getItemList(props, menu, style).map((item, id) => (
            <div key={id}>{item}</div>
          ))}
        </Menu>
      </>
    );
  };
}

function withMyHook(Component) {
  return function WrappedComponent(props) {
    const myHookValue = MenuHooks(props);
    return <Component {...props} myHookValue={myHookValue} />;
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(withMyHook(SiteNavMenu)));
