import React, { Component } from "react";
import PropTypes from "prop-types";
import LOCALIZE from "./text_resources";
import PopupBox, { BUTTON_TYPE } from "./components/commons/PopupBox";
import SystemMessage, { MESSAGE_TYPE } from "./components/commons/SystemMessage";

class ExpiredTokenPopup extends Component {
  static propTypes = {
    showTokenExpiredDialog: PropTypes.bool.isRequired,
    closePopupFunction: PropTypes.func.isRequired
  };

  render() {
    return (
      <div>
        <PopupBox
          show={this.props.showTokenExpiredDialog}
          title={LOCALIZE.tokenExpired.title}
          handleClose={() => {}}
          description={
            <div>
              <p>
                {
                  <SystemMessage
                    messageType={MESSAGE_TYPE.info}
                    title={LOCALIZE.commons.info}
                    message={LOCALIZE.tokenExpired.description}
                  />
                }
              </p>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.ok}
          rightButtonAction={this.props.closePopupFunction}
          size="lg"
        />
      </div>
    );
  }
}

export default ExpiredTokenPopup;
