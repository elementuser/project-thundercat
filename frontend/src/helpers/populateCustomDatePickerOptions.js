import LOCALIZE from "../text_resources";

// populate custom date year options (from current year to current year + 5)
function populateCustomFutureYearsDateOptions(numberOfYears) {
  const yearOptionsArray = [];
  const currentYear = new Date().getFullYear();
  // loop from current year to current year + <provided_number> years
  for (let i = currentYear; i <= currentYear + numberOfYears; i++) {
    // push each value in yearOptionsArray
    yearOptionsArray.push({ value: i, label: `${i}` });
  }
  // returning array
  return yearOptionsArray;
}

// populate custom date year options (from 2020 to current year)
export function populateCustomYearsFutureDateOptions() {
  const yearOptionsArray = [];
  const currentYear = new Date().getFullYear();
  // loop from current year to current year
  for (let i = 2020; i <= currentYear; i++) {
    // push each value in yearOptionsArray
    yearOptionsArray.push({ value: i, label: `${i}` });
  }
  // returning array
  return yearOptionsArray;
}

export default populateCustomFutureYearsDateOptions;
