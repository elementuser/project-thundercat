import SessionStorage, { ACTION } from "../SessionStorage";

export default function clearTestSessionStorage() {
  SessionStorage(ACTION.CLEAR);
}
