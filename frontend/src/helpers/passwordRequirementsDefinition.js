// these are the password requirements from django.contrib.auth.password_validation
const PASSWORD_REQUIREMENTS = {
  passwordTooCommon: "This password is too common.",
  passwordTooSimilarToUsername: "The password is too similar to the username.",
  passwordTooSimilarToFirstName: "The password is too similar to the first name.",
  passwordTooSimilarToLastName: "The password is too similar to the last name.",
  passwordTooSimilarToEmail: "The password is too similar to the email address."
};

export default PASSWORD_REQUIREMENTS;
