// ref: https://stackoverflow.com/questions/2049502/what-characters-are-allowed-in-an-email-address
export default function usernameFormatter(username) {
  // allowing specific special characters in emails, but not in usernames
  // converting "!" to UTF-16
  let formattedUsername = username.replaceAll("!", "0x0021");
  // converting "#" to UTF-16
  formattedUsername = formattedUsername.replaceAll("#", "0x0023");
  // converting "$" to UTF-16
  formattedUsername = formattedUsername.replaceAll("$", "0x0024");
  // converting "%" to UTF-16
  formattedUsername = formattedUsername.replaceAll("%", "0x0025");
  // converting "&" to UTF-16
  formattedUsername = formattedUsername.replaceAll("&", "0x0026");
  // converting "'" to UTF-16
  formattedUsername = formattedUsername.replaceAll("'", "0x0027");
  // converting "*" to UTF-16
  formattedUsername = formattedUsername.replaceAll("*", "0x002A");
  // converting "/" to UTF-16
  formattedUsername = formattedUsername.replaceAll("/", "0x002F");
  // converting "=" to UTF-16
  formattedUsername = formattedUsername.replaceAll("=", "0x003D");
  // converting "?" to UTF-16
  formattedUsername = formattedUsername.replaceAll("?", "0x003D");
  // converting "^" to UTF-16
  formattedUsername = formattedUsername.replaceAll("^", "0x005E");
  // converting "`" to UTF-16
  formattedUsername = formattedUsername.replaceAll("`", "0x0060");
  // converting "{" to UTF-16
  formattedUsername = formattedUsername.replaceAll("{", "0x007B");
  // converting "|" to UTF-16
  formattedUsername = formattedUsername.replaceAll("|", "0x007C");
  // converting "}" to UTF-16
  formattedUsername = formattedUsername.replaceAll("}", "0x007D");
  // converting "~" to UTF-16
  formattedUsername = formattedUsername.replaceAll("~", "0x007E");

  return formattedUsername;
}
