// getting retest date based on provided test date and number of days (retest period)
// date needs to be provided in format 'yyyy-mm-dd'
function getRetestDate(testDate, numberOfDays) {
  // initializing new date with the provided one
  const newDate = new Date(testDate);
  // setting the new date with the provided number of days added
  newDate.setDate(newDate.getDate() + numberOfDays + 1);
  // formatting month and day
  const formattedMonth =
    newDate.getMonth() + 1 < 10 ? `0${newDate.getMonth() + 1}` : newDate.getMonth() + 1;
  const formattedDay =
    newDate.getDate() + 1 < 10 ? `0${newDate.getDate() + 1}` : newDate.getDate() + 1;
  // returning new date in format 'yyyy-mm-dd'
  return `${newDate.getFullYear()}-${formattedMonth}-${formattedDay}`;
}

export default getRetestDate;
