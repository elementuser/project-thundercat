// getting content height based on provided dynamic values
function getInTestHeightCalculations(
  fontSize,
  spacingEnabled,
  testNavBarHeight,
  topTabsHeight,
  testFooterHeight
) {
  // calculating height based on provided dynamic values
  // "24" is for the padding top in test section component factory (TestSectionComponentFactory.jsx)
  let heightAdjustments = testNavBarHeight + topTabsHeight + testFooterHeight + 24;
  // if spacing enabled
  if (spacingEnabled) {
    // adding necessary adjustments
    heightAdjustments += parseInt(fontSize.split("px")[0]) / 2 + 2;
  }
  // if contains top tabs
  if (topTabsHeight !== 0) {
    // "20" is for the top tabs padding top
    heightAdjustments += 20;
  }
  // returning style object
  return { height: `calc(100vh - ${heightAdjustments}px)` };
}

// getting notepad height based on provided dynamic values
export function getInTestNotepadHeightCalculations(
  fontSize,
  spacingEnabled,
  testNavBarHeight,
  topTabsHeight,
  notepadHeaderHeight,
  testFooterHeight,
  calculatorHeight
) {
  // setting calculator height (if defined)
  let calculatorHeightVar = calculatorHeight;
  if (calculatorHeight === null) {
    calculatorHeightVar = 0;
  }

  // calculating height based on provided dynamic values
  // "20" is for the top tabs padding top
  // "24" is for the padding top in test section component factory (TestSectionComponentFactory.jsx)
  let heightAdjustments =
    testNavBarHeight +
    topTabsHeight +
    notepadHeaderHeight +
    testFooterHeight +
    calculatorHeightVar +
    20 +
    24;
  // if spacing enabled
  if (spacingEnabled) {
    // adding necessary adjustments
    heightAdjustments += parseInt(fontSize.split("px")[0]) / 2;
  }
  // returning style object
  return { height: `calc(100vh - ${heightAdjustments}px)` };
  // notepad is hidden
}

// getting calculator height based on provided dynamic values
export function getInTestCalculatorHeightCalculations(
  fontSize,
  spacingEnabled,
  testNavBarHeight,
  topTabsHeight,
  calculatorHeaderHeight,
  testFooterHeight
) {
  // calculating height based on provided dynamic values
  // -5 is for minor adjustment
  let heightAdjustments =
    testNavBarHeight + topTabsHeight + calculatorHeaderHeight + testFooterHeight - 5;
  // if spacing enabled
  if (spacingEnabled) {
    // adding necessary adjustments
    heightAdjustments += parseInt(fontSize.split("px")[0]) / 2;
  }
  // returning style object
  return { height: `calc(100vh - ${heightAdjustments}px)` };
  // notepad is hidden
}

// getting notepad height based on provided dynamic values
export function getItemBankHeightCalculations(
  fontSize,
  spacingEnabled,
  pageNavBarHeight,
  topPageHeight,
  testFooterHeight
) {
  // calculating height based on provided dynamic values
  // "15" + "20" is for the padding of the main containers
  let heightAdjustments = pageNavBarHeight + topPageHeight + testFooterHeight + 15 + 20;
  // if spacing enabled
  if (spacingEnabled) {
    // adding necessary adjustments
    heightAdjustments += parseInt(fontSize.split("px")[0]) / 2;
  }
  // returning style object
  return {
    maxHeight: { maxHeight: `calc(100vh - ${heightAdjustments}px)` },
    height: { height: `calc(100vh - ${heightAdjustments}px)` }
  };
  // notepad is hidden
}

export default getInTestHeightCalculations;
