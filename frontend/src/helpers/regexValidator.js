function validateName(string) {
  const regexExpression = /^([a-zA-zÀ-ÿ .'-]{2,})$/;
  if (regexExpression.test(string)) {
    return true;
  }
  return false;
}

export function validateNameWithParentheses(string) {
  const regexExpression = /^([a-zA-zÀ-ÿ ().'-]{2,})$/;
  if (regexExpression.test(string)) {
    return true;
  }
  return false;
}

export function validatePSRSAppID(string) {
  // valid if the field is empty (since this field is not mandatory)
  if (string == null) {
    return true;
  }
  if (string.length === 0) {
    return true;
  }
  const regex = /^[A-Za-z][0-9]{6}$/;
  // string must contains 7 characters (1 letter and 6 numbers)
  return regex.test(string);
}

// ref: http://emailregex.com/
export function validateEmail(string) {
  const regexExpression =
    // We added the accented characters to the unaccepted characters since they are not compatible with email servers
    /^(([^À-ÿ<>()\]\\.,;:\s@"]+(\.[^À-ÿ<>()\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (regexExpression.test(string)) {
    return true;
  }
  return false;
}

// ref: https://stackoverflow.com/questions/19605150/regex-for-password-must-contain-at-least-eight-characters-at-least-one-number-a

/*
Conditions:
  - At least one uppercase
  - At least one lowercase
  - At least one digit
  - At least one special character
  - Minimum 5 and maximum 15 in length
*/

export const PASSWORD_REQUIREMENTS_REGEX = {
  UPPERCASE: "uppercase",
  LOWERCASE: "lowercase",
  DIGIT: "digit",
  SPECIAL_CHARS: "special_char",
  NUMBER_OF_CHARS: "number_of_chars"
};

export function validatePassword(string) {
  const array = [];
  const regexExpression = /^(?=.*?[A-ZÀ-ÿ])(?=.*?[a-zÀ-ÿ])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,15}$/;
  if (regexExpression.test(string)) {
    // empty array
    return array;
  }
  const uppercaseRegex = /(?=.*[A-Z])/;
  const lowercaseRegex = /(?=.*[a-z])/;
  const digitRegex = /(?=.*\d)/;
  const specialCharRegex = /(?=.*?[#?!@$%^&*-])/;
  const numberOfCharRegex = /^(?=.{8,15}$).*/;
  // at least one uppercase
  if (!uppercaseRegex.test(string)) {
    array.push(PASSWORD_REQUIREMENTS_REGEX.UPPERCASE);
  }
  // at least one lowercase
  if (!lowercaseRegex.test(string)) {
    array.push(PASSWORD_REQUIREMENTS_REGEX.LOWERCASE);
  }
  // at least one digit
  if (!digitRegex.test(string)) {
    array.push(PASSWORD_REQUIREMENTS_REGEX.DIGIT);
  }
  // at least one special character
  if (!specialCharRegex.test(string)) {
    array.push(PASSWORD_REQUIREMENTS_REGEX.SPECIAL_CHARS);
  }
  // minimum of 5 and maximum of 15 characters in length
  if (!numberOfCharRegex.test(string)) {
    array.push(PASSWORD_REQUIREMENTS_REGEX.NUMBER_OF_CHARS);
  }
  return array;
}

/*
PRI Condition:
  - Contains 8 or 9 numbers
*/
export function validatePri(string) {
  // valid if the field is empty (since this field is not mandatory)
  if (string.length === 0) {
    return true;
  }

  // string must contains 8 or 9 characters (8 or 9 numbers)
  let stringCopy = string;

  // add leading 0 for calculation if missing
  if (string.length === 8) {
    stringCopy = `0${string}`;
  }

  // invalid if it doesn't start with 0
  if (string.length === 9 && string[0] !== "0") {
    return false;
  }

  // Mod11 Check Digit calculation
  // This is a form of redundancy check used for error detection on identification numbers such as PRIs
  let mod11 = 0;
  mod11 += stringCopy[1] * 8;
  mod11 += stringCopy[2] * 7;
  mod11 += stringCopy[3] * 6;
  mod11 += stringCopy[4] * 5;
  mod11 += stringCopy[5] * 4;
  mod11 += stringCopy[6] * 3;
  mod11 += stringCopy[7] * 2;
  mod11 += stringCopy[8] * 1;
  mod11 %= 11;

  return (string.length === 8 || string.length === 9) && mod11 === 0;
}

/*
Military Number Condition:
  - starts with a letter and followed by 6 numbers
*/
export function validateMilitaryNbr(string) {
  // valid if the field is empty (since this field is not mandatory)
  if (string.length === 0) {
    return true;
  }
  // if the string contains a letter ==> military number
  if (string.match(/[A-Za-z]/)) {
    // string must contains 7 characters (1 letter and 6 numbers)
    return string.length === 7;
  }
}

export default validateName;
