// definition of tests
export const TEST_DEFINITION = {
  emib: {
    // public test
    sampleTest: "emibSampleTest",
    // non-public test
    pizzaTest: "emibPizzaTest",
    //emib Test A
    emibTestA: "emibTestA",
    //emib Test B
    emibTestB: "emibTestB"
  }
};
