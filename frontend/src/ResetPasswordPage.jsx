import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import LOCALIZE from "./text_resources";
import ContentContainer from "./components/commons/ContentContainer";
import ResetPasswordTab from "./components/authentication/ResetPasswordTab";
import { resetPasswordTokenValidation } from "./modules/PasswordResetRedux";
import { PATH } from "./SiteNavBar";

import { Helmet } from "react-helmet";

export const styles = {
  appPadding: {
    padding: "15px"
  }
};

class ResetPasswordPage extends Component {
  static propTypes = {
    // Props from Redux
    resetPasswordTokenValidation: PropTypes.func
  };

  state = {
    isLoading: true,
    passwordResetToken: ""
  };

  componentDidMount = () => {
    // getting token value from URL
    const passwordResetToken = window.location.href.split("token=")[1];
    // updating passwordResetToken state
    this.setState({ passwordResetToken: passwordResetToken }, () => {
      // checking if the url contains a valid password reset token
      this.props.resetPasswordTokenValidation({ token: passwordResetToken }).then(response => {
        // password reset token is valid
        if (response.status === "OK") {
          this.setState({ isLoading: false });
          // password reset token is invalid
        } else {
          this.props.history.push(PATH.login);
        }
      });
    });
  };

  render() {
    return (
      <div>
        {!this.state.isLoading && (
          <div className="app" style={styles.appPadding}>
            <Helmet>
              <html lang={this.props.currentLanguage} />
              <title className="notranslate">{LOCALIZE.titles.resetPassword}</title>
            </Helmet>
            <ContentContainer>
              <div id="main-content" role="main" tabIndex={0}>
                <h1>{LOCALIZE.homePage.welcomeMsg}</h1>
                <div>
                  <ResetPasswordTab passwordResetToken={this.state.passwordResetToken} />
                </div>
              </div>
            </ContentContainer>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {};
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      resetPasswordTokenValidation
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ResetPasswordPage);
