/* eslint-disable no-irregular-whitespace */
import LocalizedStrings from "react-localization";

const LOCALIZE_OBJ = {
  en: {
    // Main Tabs
    mainTabs: {
      homeTabTitleUnauthenticated: "Home",
      homeTabTitleAuthenticated: "Home",
      dashboardTabTitle: "Dashboard",
      sampleTest: "Sample e-MIB",
      sampleTests: "Sample Tests",
      statusTabTitle: "Status",
      psc: "Public Service Commission of Canada",
      canada: "Government of Canada",
      skipToMain: "Skip to main content",
      menu: "MENU"
    },

    // HTML Page Titles
    titles: {
      CAT: "OÉC/CAT - CFP/PSC",
      sampleTests: "CAT - Sample Tests",
      eMIBOverview: "e-MIB Overview",
      eMIB: "e-MIB Assessment",
      uitTest: "CAT - Test: {0}",
      status: "CAT - System Status",
      home: "CAT - Home",
      homeWithError: "Error CAT - Home",
      profile: "CAT - Profile",
      systemAdministration: "CAT - Business Operations",
      rdOperations: "CAT - R&D Operations",
      systemAdminAndRdOperations: "CAT - Operations",
      testAdministration: "CAT - Test Administrator",
      testBuilder: "CAT - Test Builder",
      itemBankPermissions: "CAT - Item Bank Accesses",
      itemBankEditor: "CAT - Item Bank Editor",
      itemEditor: "CAT - Item Editor",
      ppcAdministration: "CAT - R&D Dashboard",
      resetPassword: "CAT - Reset Password"
    },

    // Site Nav Bar
    siteNavBar: {
      settings: "Display"
    },

    accommodations: {
      notificationPopup: {
        title: "Accessibility Display Settings",
        description1: "The current display settings have been saved.",
        description2: "When you log in, these settings will be automatically applied."
      },
      defaultFonts: {
        fontSize: "16px (default)",
        fontFamily: "Nunito Sans (Default)"
      }
    },

    // authentication
    authentication: {
      login: {
        title: "Login",
        content: {
          title: "Login",
          inputs: {
            emailTitle: "Email Address:",
            passwordTitle: "Password:",
            showPassword: "Show Password"
          }
        },
        button: "Login",
        forgotPassword: "Forgot Password?",
        forgotPasswordPopup: {
          title: "Forgot Password?",
          description: "Enter the email address of the account that requires a password reset.",
          emailAddressLabel: "Email Address:",
          sendResetLinkButton: "Send password reset link",
          invalidEmailAddressErrorMessage: "Must be a valid email address",
          usernameDoesNotExistErrorMessage: "Email provided does not match an active account",
          emailSentSuccessfully: "An email has been sent to this address",
          emailSentSuccessfullMessage: `If you didn't receive the email, verify the email address and click the "Resend Email" button below.`,
          resendEmail: "Resend Email"
        },
        invalidCredentials: "Email address and/or password is invalid",
        passwordFieldSelected: "Password field selected."
      },
      createAccount: {
        title: "Create an account",
        content: {
          title: "Create an account",
          description: "Please fill out the following information to create an account.",
          inputs: {
            valid: "Valid",
            firstNameTitle: "First Name:",
            firstNameError: "Must be a valid First Name",
            lastNameTitle: "Last Name:",
            lastNameError: "Must be a valid Last Name",
            dobDayTitle: "Date of Birth:",
            dobError: "Must be a valid Date",
            psrsAppIdTitle: "PSRS Applicant ID (if applicable):",
            psrsAppIdError: "Must be a valid PSRS Applicant ID",
            emailTitle: "Email Address:",
            emailError: "Must be a valid Email Address",
            priTitle: "PRI (if applicable):",
            militaryNbrTitle: "Service Number (if applicable):",
            priError: "Must be a valid PRI",
            militaryNbrError: "Must be a valid Service Number",
            passwordTitle: "Password:",
            showPassword: "Show Password",
            passwordErrors: {
              description: "Your password must contain the following:",
              upperCase: "At least one uppercase letter",
              lowerCase: "At least one lowercase letter",
              digit: "At least one number",
              specialCharacter: "At least one special character (#?!@$%^&*-)",
              length: "Minimum of 8 characters and maximum of 15"
            },
            passwordConfirmationTitle: "Confirm Password:",
            showPasswordConfirmation: "Show Password Confirmation",
            passwordConfirmationError: "Your password confirmation must match your New Password"
          }
        },
        privacyNotice:
          "I have read the {0} and I accept the manner in which the Public Service Commission collects, uses and discloses personal information.",
        privacyNoticeLink: "Privacy Notice",
        privacyNoticeError: "You must accept the privacy notice by clicking on the checkbox",
        button: "Create account",
        accountAlreadyExistsError: "An account is already associated to this email address",
        passwordTooCommonError: "This password is too common",
        passwordTooSimilarToUsernameError: "The password is too similar to the username",
        passwordTooSimilarToFirstNameError: "The password is too similar to the first name",
        passwordTooSimilarToLastNameError: "The password is too similar to the last name",
        passwordTooSimilarToEmailError: "The password is too similar to the email"
      },
      resetPassword: {
        title: "Reset Password",
        content: {
          title: "Reset Password",
          newPassword: "New Password:",
          showNewPassword: "Show New Password",
          confirmNewPassword: "Confirm New Password:",
          showConfirmNewPassword: "Show New Password Confirmation",
          button: "Reset Password"
        },
        popUpBox: {
          title: "Password Reset Successful",
          description: `Click "OK" to log in.`
        }
      }
    },

    // Menu Items
    menu: {
      scorer: "Test Scorer",
      etta: "Business Operations",
      rdOperations: "R&D Operations",
      operations: "Operations",
      ppc: "R&D Dashboard",
      ta: "Test Administrator",
      testBuilder: "Test Developer",
      checkIn: "Check-in",
      profile: "My Profile",
      incidentReport: "Incident Reports",
      MyTests: "My Tests",
      ContactUs: "Contact Us",
      logout: "Logout"
    },

    // Token Expired Popup Box
    tokenExpired: {
      title: "Session Expired",
      description:
        "Your session has expired due to inactivity. Please log in again to access your account."
    },

    // Home Page
    homePage: {
      welcomeMsg: "Welcome to the Candidate Assessment Tool",
      description: `This application is used to assess candidates for positions in the federal public service. Please login to access your test. If you do not have an account, select the "Create an account" link to register.`
    },

    // Sample Tests Page
    sampleTestDashboard: {
      title: "Sample Tests",
      description:
        "Sample tests are not scored and answers are not submitted at the end of a test. Click on “Start” to access the sample test.",
      table: {
        columnOne: "Sample Test",
        columnTwo: "Action",
        viewButton: "Start",
        viewButtonAccessibilityLabel: "Start the {0} test."
      }
    },

    // Dashboard Page
    dashboard: {
      title: "Welcome, {0} {1}.",
      description:
        "To start your test, select the Check-in button below, and enter your test access code.",
      lastLogin: "[Last Logged In: {0}]",
      lastLoginNever: "never",
      table: {
        columnOne: "Test",
        columnTwo: "Action",
        viewButton: "Start",
        viewButtonAccessibilityLabel: "Start the {0} test.",
        noTests: "You have no assigned tests"
      }
    },

    // Checkin action
    candidateCheckIn: {
      button: "Check-in",
      loading: "Searching for active tests...",
      popup: {
        title: "Enter the Test Access Code",
        incompletedProfileTitle: "Update Your Profile Information",
        description:
          'Please type the Test Access Code provided by the test administrator in the box below. Then select "Check-in" to access your test.',
        textLabel: "Test Access Code:",
        textLabelError: "Must be a valid Test Access Code",
        testAccessCodeAlreadyUsedError: "You have already used this test access code",
        currentTestAlreadyInActiveStateError: "You have already checked in for this test",
        incompletedProfileSystemMessage:
          "You must complete or review your profile before you can check-in a test. Additional information is required for statistical purposes.",
        incompletedProfileDescription:
          "Go to the {0} page to complete this information before proceeding.",
        incompletedProfileLink: "My Profile",
        incompletedProfileRightButton: "Update Profile"
      }
    },

    // Lock Screen
    candidateLockScreen: {
      tabTitle: "Test Locked",
      description: {
        part1: "Your test has been locked by the test administrator.",
        part2: "Your test timer is on hold.",
        part3: "Your responses have been saved.",
        part4: "Please contact your test administrator for further instructions."
      }
    },

    // Pause Screen
    candidatePauseScreen: {
      tabTitle: "Test Paused",
      description: {
        part1:
          "You have paused your test. It will resume when you select Resume Test or once the timer reaches 00:00:00.",
        part2: "Your test timer is on pause.",
        part3: "Your responses have been saved."
      }
    },

    // Invalidate Test Screen
    candidateInvalidateTestScreen: {
      tabTitle: "Test Unassigned/Invalidated",
      description: {
        part1: "Your test has either been unassigned or invalidated.",
        part2: "Please contact the test administrator for further instructions.",
        returnToDashboard: "Return to Home Page"
      }
    },

    // My Test
    myTests: {
      title: "Welcome, {0} {1}.",
      alertCatTestsOnly1:
        "Below are your results for all tests that you’ve completed using the Candidate Assessment Tool. Other types of test results are not included.",
      alertCatTestsOnly2:
        "These results are valid when certain conditions, such as respecting the retest period, are met.",
      alertCatTestsOnly3:
        "Results from unsupervised reading comprehension and written expression tests taken on or after September\u00A01,\u00A02022, are portable across federal departments and agencies and valid for 5\u00A0years.",
      table: {
        nameOfTest: "Test",
        testDate: "Test Date",
        score: "Result",
        results: "Action",
        viewResultsButton: "View",
        viewResultsButtonAriaLabel:
          "View results of the {0} test, completed on {1}, where a retest is possible as of {2} and the score is {3}.",
        viewResultsPopup: {
          title: "Test Result",
          testLabel: "Test:",
          testDescriptionLabel: "Test Description:",
          // languageLabel: "Test Language:",
          testDateLabel: "Test Date:",
          // retestDateLabel: "Date after which a retest is possible:",
          // retestPeriodLabel: "Mininum number of days before a retest:",
          // retestPeriodUnits: "days",
          scoreLabel: "Result:"
        },
        noDataMessage: "No tests have been scored yet."
      }
    },

    // Test Builder Page
    testBuilder: {
      backToTestDefinitionSelection: "Back to the list of Test Definitions",
      backToTestDefinitionSelectionPopup: {
        title: "Return to Test Definition Selection?",
        description: "Are you sure you want to continue?",
        systemMessageDescription: "You will lose everything!"
      },
      containerLabel: "Test Definition",
      itemBanksContainerLabel: "Item Banks",
      goBackButton: "Back to Test Builder",
      inTestView: "In-Test View",
      errors: {
        nondescriptError:
          "Non descript error. Report this to IT. Maybe a user is logged into the adminconsole.",
        cheatingPopup: {
          title: "Caught Cheating Attempt",
          description:
            "You are not allowed to switch or open new tabs. Any attempt to minimize the screen or change focus will be considered a cheating attempt.",
          warning:
            "After {0} more detections your test will be terminated and your results invalidated. If this was an error, alert your TA."
        },
        backendAndDbDownPopup: {
          title: "Connection Issue!",
          description:
            "The connection to your test has been interrupted. To continue the test, make sure you are connected to the Internet, then try to reconnect to your CAT account.",
          rightButton: "Return to Home page"
        }
      },
      testDefinitionSelectPlaceholder: "Select a Test Section",
      sideNavItems: {
        testDefinition: "Test Information",
        testSections: "Test Sections",
        testSectionComponents: "Test Sub-Sections",
        sectionComponentPages: "Section Titles",
        componentPageSections: "Section Content",
        questionBlockTypes: "Item Block Names",
        questions: "Items",
        scoring: "Scoring Method",
        answers: "Answers",
        addressBook: "Address Book Contacts",
        testManagement: "Test Activation"
      },
      // some values are underscored because python uses underscores
      testDefinition: {
        title: "Test Definition and Version",
        description: "These are the top level properties of a test.",
        validationErrors: {
          invalidTestDefinitionDataError:
            "Error: All Test Information fields are mandatory. Please fill out the missing fields and try again."
        },
        uploadFailedErrorMessage: "Upload Error: Review the highlighted sections",
        uploadFailedComplementaryErrorMessages: "Upload Error. Please review {0}",
        uploadJsonFailedComplementaryErrorMessages: "JSON Upload Error: Review JSON content",
        uploadTestDefinition: {
          title1: "Upload Test Version to server",
          title2: "Test Version Uploaded Successfully",
          description1:
            "Uploading this Test Definition will create a new CAT version. Are you sure you want to proceed?",
          description2:
            "The Test Version has been uploaded successfully. However, please note that this new version is currently deactivated. To activate this new test version, update the Test Status under the Test Activation section."
        },
        uploadFailedPopup: {
          title: "Upload Failed",
          systemMessage:
            "All Test Sections with a Scoring Method set to AUTO SCORE must contain at least one Test Sub-Section with a Sub-Section Type set at ITEMS_(TEST_BUILDER)."
        },
        undefinedVersion: "0",
        selectTestDefinition: {
          sideNavigationItems: {
            allTests: "All Test Definitions",
            AllActiveTestVersions: "Active Test Definitions",
            archivedTestCodes: "Archived Test Definitions",
            uploadJson: "Upload JSON"
          },
          newTestDefinitionButton: "New Test Definition",
          allTests: {
            title: "All Test Definitions",
            description: "Create, modify and upload Test Definitions.",
            table: {
              column1: "Parent Code",
              column2: "Test Code",
              column3: "Test Name",
              column4: "Actions",
              noData: "There is no test definition data",
              selectSpecificTestVersionTooltip: "Select Test Definition Version",
              selectSpecificTestVersionLabel:
                "Select a specific version of this test definition where the parent code is: {0}, the test code is: {1} and the test name is: {2}",
              selectLatestTestVersionTooltip: "View Latest Test Definition Version",
              selectLatestTestVersionLabel:
                "View latest test version of this test definition where the parent code is: {0}, the test code is: {1} and the test name is: {2}",
              archiveTestCodeTooltip: "Archive Test Definition",
              archiveTestCodeLabel:
                "Archive test code of the this test definition where the parent code is: {0}, the test code is: {1} and the test name is: {2}"
            },
            testDefinitionPopup: {
              title: "Select Test Definition Version",
              parentCode: "Parent Code:",
              testCode: "Test Code:",
              versionNotes: "Version Notes:",
              uitStatus: "UIT:",
              testStatus: "Test Status:",
              sampleTestStatus: "Sample Test:",
              name: "Test Name:",
              rightButton: "Select Test Definition",
              dropdown: {
                title: "CAT Version:",
                titleTooltip: "To be completed"
              }
            },
            archiveTestCodePopup: {
              title: "Archive Test Definition",
              systemMessageDescription: "All test accesses related to {0} will be revoked.",
              description: "Are you sure you want to continue?",
              rightButton: "Archive Test Definition"
            }
          },
          allActiveTestVersions: {
            title: "Active Test Definitions",
            description: "View and deactivate active Test Versions",
            table: {
              column1: "Parent Code",
              column2: "Test Code",
              column3: "Test Name",
              column4: "CAT Version",
              column5: "Actions",
              noData: "There is no test definition data",
              viewButtonTooltip: "View Test Definition",
              viewButtonAriaLabel:
                "View details of the following test definition where the parent code is: {0}, the test code is: {1}, the test name is: {2} and the version is: {3}",
              deactivateButtonTooltip: "Deactivate Test Definition",
              deactivateButtonAriaLabel:
                "Deactivate test definition version where the parent code is: {0}, the test code is: {1}, the test name is: {2} and the version is: {3}"
            },
            popup: {
              title: "Deactivate Test Definition",
              description: "All test accesses related to {0} will be revoked.",
              rightButton: "Deactivate Test Definition"
            }
          },
          archivedTestCodes: {
            title: "Archived Test Definitions",
            description: "Restore deactivated Test Definitions.",
            table: {
              column1: "Parent Code",
              column2: "Test Code",
              column3: "Test Name",
              column4: "Actions",
              noData: "There is no test definition data",
              restoreButton: "Restore Test Definition",
              restoreButtonAriaLabel:
                "Restore test definition test code where the parent code is: {0}, the test code is: {1} and the test name is: {2}"
            },
            popup: {
              title: "Restore Test Definition",
              description: "Test Definition {0} will be restored",
              rightButton: "Restore Test Definition"
            }
          },
          uploadJson: {
            title: "Upload JSON",
            description: "Upload JSON files to the server.",
            textAreaLabel: "Please select the JSON file:",
            uploadJsonButton: "Upload JSON",
            invalidJsonError: "Upload Error. Please review the JSON file.",
            uploadJsonSuccessful: "JSON File Uploaded Successfully: {0} {1} v{2}",
            fileSelectedAccessibility: "The selected file is {0}",
            browseButton: "Browse...",
            wrongFileTypeError: "Must be a valid JSON file."
          },
          itemBanks: {
            title: "Item Banks",
            table: {
              column1: "Item Bank ID",
              column2: "Item Bank Name",
              column3: "Actions",
              viewButtonTooltip: "View",
              viewButtonAriaLabel: "View item bank {0}",
              downloadReportButtonTooltip: "Download Report",
              noData: "There are no results found based on your search"
            }
          },
          itemBankEditor: {
            backToItemBankSelectionButton: "Back to List of Item Banks",
            backToItemBankSelectionPopup: {
              title: "Return to Item Bank Selection?",
              systemMessageDescription: "All unsaved data will be lost!",
              description: "Are you sure you want to proceed?"
            },
            sideNavigationItems: {
              itemBankDefinition: "Item Bank Definition",
              itemAttributes: "Custom Variables",
              items: "Items ({0})",
              bundles: "Grouping ({0})"
            },
            itemBankDefinition: {
              custom_item_bank_id: {
                title: "Item Bank ID:",
                errorMessage: "Must be a valid Item Bank ID"
              },
              custom_item_bank_id_already_exists: {
                title: "Item Bank ID:",
                errorMessage: "This Item Bank ID already exists"
              },
              en_name: {
                title: "Item Bank Name (English):",
                errorMessage: "Must be a valid name"
              },
              fr_name: {
                title: "Item Bank Name (French):",
                errorMessage: "Must be a valid name"
              },
              updateExistingItemBankPopup: {
                title: "Update Existing Item Bank Definition",
                systemMessageDescription:
                  "You are about to update an existing item bank definition!",
                description: "Are you sure you want to proceed?"
              }
            },
            itemAttributes: {
              createNewButton: "Variable",
              collapseAll: "Collapse All Variables",
              uploadDataButton: "Upload Data",
              uploadDataErrorMessage:
                "Error found in item attributes. Please make sure that all parameters are defined.",
              itemAttributeCollapsingItem: {
                title: "Custom Variable {0}: ",
                values: "Values: ",
                en_name: {
                  title: "Variable Name in English:",
                  errorMessage: "Must be a valid name"
                },
                fr_name: {
                  title: "Variable Name in French:",
                  errorMessage: "Must be a valid name"
                },
                attributeTitle: "Variable Name:",
                attributeValuesTitle: "Values:",
                createNewAttributeValueButton: "Add Value",
                attributeTranslationsButtonLabel: "Modify Translations",
                attributeValueCollapsingItem: {
                  title: "Attribute Value {0}: {1}",
                  value_identifier: {
                    title: "Value Identifier",
                    errorMessage: "Must be a valid value identifier"
                  }
                },
                attributeNameTranslationsPopup: {
                  title: "Translations - Variable Name"
                }
              },
              dataUploadedSuccessfullyPopup: {
                title: "Item Bank Attributes Data Uploaded Successfully",
                systemMessage:
                  "The item bank attributes data has been uploaded successfully in the database."
              }
            },
            items: {
              itemTitle: "{0} {1} {2} - System ID: {3} ",
              createNewItemButton: "New Item",
              createItemPopup: {
                title: "Create Item",
                description: "Please provide the required information to create a new item.",
                responseFormatLabel: "Response Format:",
                historicalIdLabel: "R&D Item ID:",
                historicalIdAlreadyBeenUsedError:
                  "This R&D Item ID is already used in this item bank.",
                rightButton: "Create Item"
              },
              table: {
                itemId: "System Item ID",
                itemType: "Item Status",
                version: "Version",
                actions: "Actions",
                viewAction: "Preview",
                viewActionAccessibilityLabel: "View Item {0}",
                editAction: "Edit",
                editActionAccessibilityLabel: "Edit Item ID {0}",
                noData: "There are no items in this bank.",
                rndItemId: "R&D Item ID"
              },
              viewSelectedItemPopup: {
                title: "Item Preview: {0}",
                questionLabel: "Question"
              },
              backToItemBankEditorButton: "Back to Item Bank",
              sideNavigationItems: {
                general: "General",
                properties: "Properties",
                statistics: "Statistics",
                socialRelationships: "Social Relationships",
                history: "History",
                versionComparison: "Version Comparision",
                uploadData: "Upload Data"
              },
              topTabs: {
                generalTabs: {
                  generaL: "General"
                },
                contentTabs: {
                  content: "Content",
                  screenReader: "Screen Reader"
                },
                otherTabs: {
                  categories: "Custom Variables",
                  comments: "Comments",
                  translations: "Translations"
                }
              },
              general: {
                systemIdLabel: "System Item ID:",
                historicalIdLabel: "R&D Item ID:",
                editHistoricalIdTooltip: "Edit",
                addEditHistoricalIdPopup: {
                  title: "R&D Item ID",
                  description: "Please provide the required information.",
                  historicalIdLabel: "R&D Item ID:"
                },
                developmentStatusLabel: "Item Status:",
                versionLabel: "Version:",
                deleteDraftTooltip: "Delete Item Draft",
                deleteDraftConfirmationPopup: {
                  title: "Delete Item Draft?",
                  systemMessageDescription: "This draft will be deleted!",
                  description: "Are you sure you want to continue?"
                },
                draftVersionLabel: "<Draft>",
                responseFormatLabel: "Response Format:",
                scoringFormatLabel: "Scoring Method:",
                shuffleOptionsLabel: "Randomize Answer Choices:",
                activeStatusLabel: "Active:",
                historicalIdAlreadyExistsError:
                  "This R&D Item ID is already used in this item bank."
              },
              content: {
                languageDropdownLabel: "Language:",
                addStemButton: "Stem",
                stemSections: "Stem(s):",
                addOptionButton: "Answer Choice",
                options: "Answer Choices:",
                score: "Scoring Value:",
                text: "Text:",
                excludeFromShuffle: "Exclude From Randomization:",
                detailsButton: "Details",
                detailsPopup: {
                  title: "Additional Answer Choice Information",
                  textLabel: "Text:",
                  historicalOptionIdLabel: "R&D Answer Choice ID:",
                  rationaleLabel: "Rationale:",
                  excludeFromShuffleLabel: "Exclude From Randomization:",
                  scoreLabel: "Scoring Value: "
                },
                addAlternativeContentPopup: {
                  title: "Add Alternative Item Content",
                  description: "Please provide the required information.",
                  alternativeContentTypeLabel: "Alternative Content Type:",
                  alternativeContentTypeOptions: {
                    screenReader: "Screen Reader"
                  },
                  rightButton: "Save"
                },
                deleteAlternativeContentPopup: {
                  title: "Delete Alternative Item Content Tab?",
                  systemMessageDescription: "The alternative item content tab will be deleted.",
                  description: "Are you sure you want to delete this tab?"
                }
              },
              comments: {
                addCommentButton: "Comment",
                addCommentPopup: {
                  title: "Add Comment",
                  commentLabel: "Comment:",
                  addButton: "Save"
                },
                deleteCommentPopup: {
                  title: "Delete Comment",
                  systemMessageDescription1: "Are you sure you want to delete this comment?",
                  systemMessageDescription2: "- {0}"
                }
              },
              previewItemPopup: {
                title: "Item Preview: {0}"
              },
              uploadConfirmationPopup: {
                title: "Upload Item",
                titleSuccessfully: "Uploaded Successfully!",
                systemMessageDescription:
                  "Uploading this item will update the content from the following fields from the previous version:",
                systemMessageDescriptionSuccessfully:
                  "Item version {0} has been uploaded successfully.",
                systemMessageDescriptionSuccessfullyOverwrite:
                  "Item version {0} has been uploaded successfully.",
                itemModificationsTable: {
                  itemElements: {
                    stemSections: "Stem(s)",
                    options: "Answer Choices",
                    categories: "Custom Variables",
                    historicalId: "R&D Item ID",
                    itemStatus: "Item Status",
                    responseFormat: "Response Format",
                    scoringFormat: "Scoring Method",
                    shuffleOptions: "Randomize Answer Choices"
                  },
                  column1: "Fields",
                  column2: "Update",
                  column3: "Warnings",
                  warnings: {
                    stemsNotDefined: "Stem(s) not defined.",
                    containsBlankStems: "Contains blank stems(s).",
                    optionsNotDefined: "Answer choice(s) not defined.",
                    containsBlankOptions: "Contains blank answer choice(s)."
                  }
                },
                whatDoYouWantToDo: "What do you want to do?",
                createNewVersionRadio: "Create a new version number for this item",
                overwriteCurrentVersionRadio: "Keep the current version number for the item",
                reviewedWarningsCheckboxLabel: "I have reviewed and accept the warnings."
              },
              uploadData: {
                title: "Upload Data: {0} ({1})",
                uploadLabel: "Upload Item:",
                uploadButton: "Upload",
                uploadPopup: {
                  title: "Upload Item",
                  description: "Please insert your comment if needed.",
                  comment: "Comment (optional):",
                  uploadButton: "Upload",
                  uploadSuccessful: "Item version {0} has been uploaded successfully."
                },
                uploadFailedErrorMessage: "Upload Error: Review the highlighted sections"
              },
              backToItemBankEditorPopup: {
                title: "Return to Item Bank Editor?",
                systemMessageDescription: "All unsaved data will be lost!",
                description: "Are you sure you want to proceed?"
              },
              changeItemConfirmationPopup: {
                goPreviousTitle: "Navigate to previous item?",
                goNextTitle: "Navigate to next item?",
                changeItemTitle: "Navigate to another item?",
                systemMessageDescription: "All unsaved data will be lost!",
                description: "Are you sure you want to proceed?"
              }
            },
            bundles: {
              bundleTitle: "{0}",
              createNewBundleButton: "New Grouping",
              createBundlePopup: {
                title: "Create Grouping",
                description: "Please provide the required information to create a new grouping.",
                bundleNameLabel: "Grouping Name:",
                bundleNameAlreadyBeenUsedError:
                  "This Grouping Name is already used in this item bank.",
                bundleVersionLabel: "Grouping Version:",
                rightButton: "Create Grouping"
              },
              backToItemBankEditorButton: "Back to Item Bank",
              table: {
                bundleName: "Grouping Name",
                bundleVersion: "Grouping Version",
                itemCount: "Number of Items",
                bundleCount: "Number of Groupings",
                status: "Status",
                actions: "Actions",
                editAction: "Edit",
                editActionAccessibilityLabel: "Edit Grouping {0}",
                duplicateAction: "Duplicate",
                duplicateActionAccessibilityLabel: "Duplicate Grouping {0}",
                noData: "There are no groupings in this bank"
              },
              topTabs: {
                generalTabs: {
                  general: "General"
                },
                addTabs: {
                  addItems: "Add Items",
                  addBundles: "Add Groupings"
                },
                contentTabs: {
                  itemContent: "Item Content ({0})",
                  bundleContent: "Grouping Content ({0})"
                }
              },
              previewBundlePopup: {
                title: "Grouping Preview",
                systemMessageErrorBundleStructuralConflictError:
                  "The Grouping being uploaded contains a Grouping and a Sub-Grouping. Please remove the conflicting Sub-Grouping and try again.",
                systemMessageErrorDuplicateSystemIdsFoundInBundleAssociationsError:
                  "The Grouping is being previewed contains duplicate items. Please remove duplicate items and try again.",
                close: "Close"
              },
              changeBundleConfirmationPopup: {
                goPreviousTitle: "Navigate to previous grouping?",
                goNextTitle: "Navigate to next grouping?",
                changeBundleTitle: "Change Grouping?",
                systemMessageDescription: "All unsaved data will be lost!",
                description: "Are you sure you want to proceed?"
              },
              uploadSuccessfulPopup: {
                title: "Uploaded Successfully!",
                titleWithError: "Grouping cannot be uploaded",
                systemMessageDescription: "Changes have been uploaded successfully.",
                systemMessageErrorBundleNameAlreadyUsed:
                  "This Grouping Name is already used in this item bank.",
                systemMessageErrorBundleStructuralConflictError:
                  "The Grouping being uploaded contains a Grouping and a Sub-Grouping. Please remove the conflicting Sub-Grouping and try again.",
                systemMessageErrorDuplicateSystemIdsFoundInBundleAssociationsError:
                  "The Grouping being uploaded contains duplicate items. Please remove duplicate items and try again."
              },
              general: {
                bundleNameLabel: "Grouping Name:",
                bundleVersionLabel: "Grouping Version:",
                shuffleItemsLabel: "Randomize Items:",
                shuffleBundlesLabel: "Randomize Groupings:",
                shuffleBetweenBundlesLabel: "Randomize Between Groupings:",
                activeLabel: "Active:",
                editBundleNameTooltip: "Edit",
                editBundleNamePopup: {
                  title: "Edit Grouping Name",
                  description: "Please provide the required information.",
                  bundleNameLabel: "Grouping Name:",
                  bundleNameMustBeValid: "Grouping Name must be provided."
                }
              },
              addItems: {
                table: {
                  systemId: "System Item ID",
                  rndItemId: "R&D Item ID",
                  responseFormat: "Response Format",
                  itemStatus: "Item Status",
                  version: "Version",
                  noData: "There are no results found based on your search"
                },
                searchDropdowns: {
                  developmentStatusLabel: "Item Status:",
                  activeStateLabel: "Active:"
                },
                addItemsToBundleButton: "Add to Grouping"
              },
              itemContent: {
                basicRuleLabel: "Number of items to select:",
                basicRuleErrorLabel: "Must be a valid number"
              },
              addBundles: {
                table: {
                  bundleName: "Grouping Name",
                  bundleVersion: "Version",
                  itemCount: "Item Count",
                  bundleCount: "Grouping Count",
                  status: "Status",
                  noData: "There are no results found based on your search"
                },
                addBundlesToBundleButton: "Add to Grouping"
              },
              bundleContent: {
                createNewBundleRuleButton: "New Grouping Rule",
                bundleRuleCollapsingItemContainer: {
                  title: "Rule {0}",
                  numberOfBundlesToPull: "Number of groupings to be selected:",
                  numberOfBundlesToPullErrorMessage: "Must be a valid number",
                  shuffleBundles: "Randomize Groupings:",
                  keepItemsTogether: "Keep items together:"
                },
                bundleAssociationsSystemMessage:
                  "All groupings that are not part of a rule will be removed upon uploading."
              }
            },
            deleteConfirmationPopup: {
              title: "Delete Confirmation",
              description:
                "Deleting this item will delete all the content displayed and all child objects that are related to this object. Are you sure you want to delete this object?"
            }
          }
        },
        test_code: {
          title: "Full Test Code",
          titleTooltip: "Unique Identifier for a test definition",
          errorMessage: "Must be a valid Full Test Code"
        },
        parent_code: {
          title: "Parent Code",
          titleTooltip: "The test code digits that group all version of this test.",
          errorMessage: "Must be a valid Test Number"
        },
        version: {
          title: "CAT Version",
          titleTooltip: "The latest version of the test definition",
          errorMessage: "Not a valid Version Number"
        },
        retest_period: {
          title: "Retest Period",
          titleTooltip: "Retest period in number of days",
          errorMessage: "Not a valid number."
        },
        version_notes: {
          title: "Version Notes",
          titleTooltip: "Some wording here..."
        },
        fr_name: {
          title: "French Name of the test",
          titleTooltip: "Name display to candidates when they have an active test",
          errorMessage: "Not a valid Name"
        },
        en_name: {
          title: "Test Name (French)",
          titleTooltip: "Name display to candidates when they have an active test",
          errorMessage: "Not a valid Name"
        },
        is_uit: {
          title: "Unsupervised Test",
          titleTooltip: "Is this a unsupervised internet test?"
        },
        is_public: {
          title: "Sample Test",
          titleTooltip: "Is this a sample test?"
        },
        count_up: {
          title: "Timer Counts Up",
          titleTooltip: "Timed section(s) will count up instead of counting down - TO BE COMPLETED"
        },
        active_status: {
          title: "Test Status",
          titleTooltip: "Is this test active and available to use?"
        },
        test_language: {
          title: "Test Language",
          titleTooltip: "Is this test presented in a single language?",
          errorMessage: "not a valid choice",
          selectPlaceholder: "--"
        },
        constants: {
          testSectionScoringTypeObject: {
            not_scorable: "NOT SCORABLE",
            auto_score: "AUTO SCORE",
            competency: "COMPENTENCY SCORE"
          },
          nextSectionButtonTypeObject: {
            proceed: "PROCEED TO NEXT SECTION",
            popup: "POP-UP WINDOW"
          }
        }
      },
      testSection: {
        title: "Test Sections",
        validationErrors: {
          mustContainAtLeastThreeTestSectionError:
            "Error: Must contain at least three test sections",
          mustContainQuitTestSectionError: "Error: Must contain a 'Quit' test section",
          mustContainFinishTestSectionError: "Error: Must contain a 'Finish' test section"
        },
        description:
          "Create a test by adding and defining Test Sections. Typical Test Sections may include the following pages: Welcome, Test Security, Terms and Conditions, Navigation, Specific Instructions, Test Questions, Test Submitted and Quit Test.",
        collapsableItemName: "Test Section {0}: {1}",
        addButton: "New Test Section",
        delete: {
          title: "Delete Test Section Component",
          description:
            "Deleting an element will delete all child elements as well. Are you sure you wish to continue?"
        },
        fr_title: {
          title: "Test Section Name (French)",
          titleTooltip: "Hidden name used for easy identification by internal users",
          errorMessage: "Not a valid Name"
        },
        en_title: {
          title: "Test Section Name (English)",
          titleTooltip: "Hidden name used for easy identification by internal users",
          errorMessage: "Not a valid Name"
        },
        uses_notepad: {
          title: "Notepad",
          titleTooltip: "Does this test section use the notepad component?",
          errorMessage: "not a valid choice"
        },
        uses_calculator: {
          title: "Calculator",
          titleTooltip: "Does this test section use the calculator component?"
        },
        block_cheating: {
          title: "Monitor Window Switching",
          titleTooltip: "Should this test section report the user for switching tabs etc.?",
          errorMessage: "not a valid choice"
        },
        item_exposure: {
          title: "Item Exposure",
          titleTooltip: "Should this test section apply the item exposure logic?"
        },
        scoring_type: {
          title: "Scoring",
          titleTooltip: "Set the correct scoring method for the components in this test section.",
          errorMessage: "not a valid choice"
        },
        minimum_score: {
          title: "Minimum Score",
          titleTooltip:
            "Set minimum score for a test section. Currently this value is only for reference in reports.",
          errorMessage: "not a valid choice"
        },
        default_time: {
          title: "Default Time Limit",
          titleTooltip:
            "How long does a candidate have on this section? If the section is not timed, then leave this empty. This field can be overridden by a TA",
          errorMessage: "Only numbers are allowed"
        },
        default_tab: {
          title: "Default Tab",
          titleTooltip: "Which top tab should be displayed when a candidate begins this section",
          errorMessage: "not a valid choice",
          selectPlaceholder: "0"
        },
        order: {
          title: "Order of Presentation",
          titleTooltip: "The order in the test that this section appears.",
          errorMessage: "not a valid choice",
          selectPlaceholder: "0"
        },
        next_section_button: {
          title: "Next Section Button",
          titleTooltip: "Define a button to take the user to the next section.",
          errorMessage: "not a valid choice",
          selectPlaceholder: "0"
        },
        reducers: {
          title: "Reducers",
          titleTooltip:
            "Which aspects of the test should be cleared when a user navigates to this section?",
          errorMessage: "not a valid choice",
          selectPlaceholder: "--"
        },
        section_type: {
          title: "Section Type",
          titleTooltip:
            "The type of test section. Such as: Single page, Top Tab navigation, Finish, Quit...",
          errorMessage: "Not a valid Type"
        },
        deletePopup: {
          title: "Confirm Deletion",
          content: "Are you sure you want to delete this section?"
        }
      },
      nextSectionButton: {
        header: "Next Section Button",
        button_type: {
          title: "Button Type",
          errorMessage:
            "Selected values for Section Type and Button Type are not compatible. Please change.",
          titleTooltip: "This is selected based on the test section component."
        },
        confirm_proceed: {
          title: "Confirmation Checkbox",
          titleTooltip:
            "A checkbox the user has to click before being allowed to press the action button."
        },
        button_text: {
          title: "Button Label",
          titleTooltip: "The text that the button displays to the user"
        },
        title: {
          title: "Pop-up Window Title",
          titleTooltip:
            "The text that appears on the header of the popup when the next section button is clicked."
        },
        content: {
          title: "Pop-up Window Content",
          titleTooltip:
            "The text that appears in the body of the popup when the next section button is clicked."
        }
      },
      testSectionComponents: {
        title: "Test Sub-Sections",
        validationErrors: {
          mustFixTheQuestionsRulesErrors:
            "Error: Fix the Test Assembly Rules (Item Blocks) below and try again.",
          undefinedPpcQuestionIdInDependentQuestion:
            "Please make sure that all dependent items have a defined 'R&D Item ID'",
          notEnoughDefinedPpcQuestionIdBasedOnNbOfQuestions:
            "Error: Insufficient number of items associated to item blocks. Please return to the Items section and assign item blocks to a sufficient number of items that meets the Number of Items as specified in the Test Assembly Rules (Item Blocks) below.",
          mustContainQuestionListComponentType:
            'Error: Please make sure that any Test Section with a "Scoring Method" set at "AUTO SCORE" contains at least one "Sub-Section Type" set at "ITEMS_(TEST_BUILDER)".',
          mustContainItemBankRules:
            "All test section component of component type ITEM_BANK must have defined and valid rules"
        },
        description:
          'Create and define Test Sub-Sections for all Test Sections of the test. Typical Test Sub-Sections may include "Test Instructions" and "Test Questions" under the associated "Test Questions" section. All Test Sections must include a Test Sub-Section. If no Test Sub-Section is needed, please create a Test Sub-Section and enter the same name as its parent section.',
        collapsableItemName: "Test Sub-Section {0}: {1}",
        addButton: "Add Test Sub-Section",
        delete: {
          title: "Delete",
          description:
            "Deleting an element will delete all child elements as well. Are you sure you wish to continue?"
        },
        parentTestSection: {
          title: "Test Section",
          titleTooltip: "The test section that the displayed objects appear in."
        },
        fr_title: {
          title: "French Name",
          titleTooltip: "Hidden name used for easy identification by internal users",
          errorMessage: "Not a valid Name"
        },
        en_title: {
          title: "English Name",
          titleTooltip: "Hidden name used for easy identification by internal users",
          errorMessage: "Not a valid Name"
        },
        order: {
          title: "Order",
          titleTooltip: "The order in which this component appears in a Top Tab navigation.",
          errorMessage: "Not a valid Name"
        },
        component_type: {
          title: "Sub-Section Type",
          titleTooltip: "The type of component. Such as: Inbox, Single Page, Side Navigation...",
          errorMessage:
            "Make sure that this is a valid parent test section data and component type combination"
        },
        shuffle_all_questions: {
          title: "Randomize Items (Test Builder)",
          titleTooltip:
            "Shuffle the questions in this question list? Question dependencies remain in the order they were given."
        },
        shuffle_question_blocks: {
          title: "Randomize Blocks (Test Builder)",
          titleTooltip: "Shuffle the question blocks in this question list? (TO BE COMPLETED)"
        },
        shuffle_item_bank_rules: {
          title: "Shuffle Item Bank Rules",
          titleTooltip: "Shuffle the item bank rules?"
        },
        language: {
          title: "Sub-Section Language",
          titleTooltip:
            "Which language will the content in this section be provided in? This is mandatory for screen readers to use the correct accent.",
          errorMessage: "not a valid choice",
          selectPlaceholder: "--"
        }
      },
      questionListRules: {
        title: "Test Assembly Rules (Item Blocks)",
        collapsableItemName: "Rule {0}",
        addButton: {
          title: "Add Block",
          titleTooltip: "You must create Question Block Types in order to add rules."
        },
        delete: {
          title: "Delete",
          description:
            "Deleting an element will delete all child elements as well. Are you sure you wish to continue?"
        },
        number_of_questions: {
          title: "Number of Items",
          titleTooltip: "The number of questions to select of the following question block type.",
          errorMessage: "Must be a valid Number of Items"
        },
        question_block_type: {
          title: "Block:",
          titleTooltip: "The question block type from which to select questions.",
          errorMessage: "Must select a Test Assembly Rule (Item Blocks)"
        },
        order: {
          title: "Block Order:",
          titleTooltip:
            "The order that this rule will appear in the question list. If there is no order, leave this field empty."
        },
        shuffle: {
          title: "Randomize Items:",
          titleTooltip:
            "Shuffle the questions in this rule? Question dependencies remain in the order they were given."
        }
      },
      itemBankRules: {
        title: "Item Bank Rules",
        collapsableItemName: "Rule {0}",
        order: {
          title: "Order",
          titleTooltip: "The order that this rule will appear."
        },
        item_bank: {
          title: "Item Bank:",
          titleTooltip: "Item bank from which you want to pull the respective grouping from."
        },
        item_bank_bundle: {
          title: "Bundle:",
          titleTooltip: "Respective Grouping from which the items list will be generated."
        }
      },
      sectionComponentPages: {
        title: "Test Sub-Sections",
        description:
          'Create and define Test Sub-Sections for all Test Sections of the test. Typical Test Sub-Sections may include "Test Instructions" and "Test Questions" under the associated "Test Questions" section. All Test Sections must include a Test Sub-Section. If no Test Sub-Section is needed, please create a Test Sub-Section and enter the same name as its parent section.  ',
        validationErrors: {
          mustContainPageContentTitlesError:
            'Error: All Test Sub-Sections except the ones with Sub-Section Type set to "ITEMS_(TEST_BUILDER)" must be associated with a Section Title.'
        },
        addButton: "Add Test Sub-Section",
        delete: {
          title: "Delete",
          description:
            "Deleting an element will delete all child elements as well. Are you sure you wish to continue?"
        },
        collapsableItemName: "Test Sub-Section {0}",
        parentTestSectionComponent: {
          title: "Test Sub-Section",
          titleTooltip: "The parent test section the displayed objects are displayed in."
        },
        fr_title: {
          title: "French Name",
          titleTooltip:
            "Name of the side navigation tab when the site language is French. This field is hidden when the Test Section Component is SINGLE_PAGE.",
          errorMessage: "Not a valid Name"
        },
        en_title: {
          title: "English Name",
          titleTooltip:
            "Name of the side navigation tab when the site language is French. This field is hidden when the Test Section Component is SINGLE_PAGE.",
          errorMessage: "Not a valid Name"
        },
        order: {
          title: "Sub-Section Order",
          titleTooltip: "The order in which this page appears in a side navigation panel.",
          errorMessage: "Not a valid Name"
        }
      },
      componentPageSections: {
        title: "Section Content",
        description: "Add content to Test Sections and Sub-Sections.",
        collapsableItemName: "Section Component Page Order: {0}, Type: {1}",
        parentSectionComponentPage: {
          title: "Section Titles",
          titleTooltip: "The parent side navigation tab that the objects displayed will belong to."
        },
        pageSectionLaguage: {
          title: "Language",
          titleTooltip:
            "If a test is bilingual, you must create page sections for each language. This will show all the page sections created for the selected language."
        },
        addButton: "Add Section Component Page",
        delete: {
          title: "Delete",
          description:
            "Deleting an element will delete all child elements as well. Are you sure you wish to continue?"
        },
        order: {
          title: "Section Content Order:",
          titleTooltip: "The order in which this page appears in a side navigation panel.",
          errorMessage: "Not a valid Name"
        },
        page_section_type: {
          title: "Content Type:",
          titleTooltip:
            "The type of section to display (i.e. Markdwon, sample email, sample teask response).",
          errorMessage: "Not a valid type."
        }
      },
      markdownPageSection: {
        content: {
          title: "Content",
          titleTooltip: "The markdown content to display."
        }
      },
      sampleEmailPageSection: {
        email_id: {
          title: "Email ID",
          titleTooltip: "The email ID to display"
        },
        from_field: {
          title: "From",
          titleTooltip: "Who the email is from."
        },
        to_field: {
          title: "To",
          titleTooltip: "Who the email is to"
        },
        date_field: {
          title: "Date",
          titleTooltip: "The date displayed on the email."
        },
        subject_field: {
          title: "Subject",
          titleTooltip: "The subject displayed on the email."
        },
        body: {
          title: "Body",
          titleTooltip: "The body of the email to display."
        }
      },
      sampleEmailResonsePageSection: {
        cc_field: {
          title: "CC",
          titleTooltip: "Who is CC'd on the email"
        },
        to_field: {
          title: "To",
          titleTooltip: "Who the email is to"
        },
        response: {
          title: "Response",
          titleTooltip: "The email response."
        },
        reason: {
          title: "Reason",
          titleTooltip: "The reason for responding with this email."
        }
      },
      sampleTaskResonsePageSection: {
        response: {
          title: "Response",
          titleTooltip: "The task response."
        },
        reason: {
          title: "Reason",
          titleTooltip: "The reason for responding with this task."
        }
      },
      zoomImagePageSection: {
        small_image: {
          title: "Full Image Name",
          titleTooltip: "name of the image file including extension."
        },
        note: "Note",
        noteDescription:
          "In order for this image to be displayed properly you must provide the image to the IT team to upload to the application server."
      },
      treeDescriptionPageSection: {
        address_book_contact: {
          title: "Root Contact",
          titleTooltip: "The root contact that the tree description will start from."
        }
      },
      addressBook: {
        topTitle: "Address Book",
        description:
          "These are email like contacts used in Inbox style Test Section Components and Tree Description Component Page Sections.",
        collapsableItemName: "{0}",
        addButton: "Add Address Book Contact",
        name: {
          title: "Name",
          titleTooltip: "The name of the contact."
        },
        title: {
          title: "Title",
          titleTooltip: "The title of the contact."
        },
        parent: {
          title: "Parent Contact",
          titleTooltip: "The contact that this contact reports to."
        },
        test_section: {
          title: "Test Sections",
          titleTooltip:
            "Which test sections can a user send emails to this contact or if there is a tree description in a test section then this contact must be in that test section."
        }
      },
      questions: {
        topTitle: "Items",
        description:
          'Create items using the Test Builder. To create items, a "ITEMS_(TEST_BUILDER)" Sub-Section Type must be defined for a given Test Sub-Section.',
        validationErrors: {
          uploadFailedMissingQuestionListQuestions:
            'Error: Please make sure that all Test Sub-Sections with a Sub-Section Type set to "ITEMS_(TEST_BUILDER)" contain at least one item.'
        },
        collapsableItemNames: {
          block: "[Block]",
          id: "[ID]",
          text: "[Text]"
        },
        emailCollapsableItemName: "Email ID: {0}, From: {1}",
        addButton: "New Item",
        searchResults: "{0} Item(s) found",
        pilot: {
          title: "Experimental Item",
          titleTooltip:
            "Is the question a pilot question? It's answer will not be counted towards a final score.",
          description: ""
        },
        questionBlockSelection: {
          title: "Item Block Name",
          titleTooltip: "The Question Block Type to narrow the number of results."
        },
        order: {
          title: "Item Order:",
          titleTooltip:
            "Order that questions will be displayed in the test (if there are no dependencies and/or shuffle) - TO BE COMPLETED"
        },
        ppc_question_id: {
          title: "R&D Item ID",
          titleTooltip: "PPC Question ID Tooltip Content - TO BE COMPLETED"
        },
        question_type: {
          title: "Response Format",
          titleTooltip:
            "i.e. multiple choice, email. This is pre-determined by the Test Section Component Type (Question List or Inbox)"
        },
        question_block_type: {
          title: "Item Block Name",
          titleTooltip:
            "The question block type identifier. Used to determine what questions are returned for a question lists rule set.",
          selectPlaceholder: "Please Select"
        },
        dependencies: {
          title: "Dependents",
          titleTooltip: "Questions that this question must be presented with (in order)."
        },
        dependent_order: {
          title: "Dependent Order",
          titleTooltip:
            "The order this question is presented in when dependents exist. This is only used for ordering questions in a dependency sitaution."
        },
        shuffle_answer_choices: {
          title: "Randomize Answer Choices",
          titleTooltip: "If enabled, the answer choices will be shuffled - TO BE COMPLETED"
        },
        previewPopup: {
          title: "Question Preview"
        }
      },
      scoringMethod: {
        title: "Scoring Method",
        validationErrors: {
          mustApplyScoringMethodError: "Error: Must apply a scoring method type"
        },
        description: "Select the scoring method for this test.",
        scoringMethodUndefinedError: "Please select a valid scoring method.",
        scoringMethodType: {
          title: "Scoring Method:",
          titleTooltip: "Scoring mechanism to use for score conversion"
        },
        scoringPassFailMinimumScore: {
          title: "Pass mark:",
          titleTooltip: "Minimum total score required to get a 'pass' mark",
          errorMessage: "Must be a valid number"
        },
        scoringThreshold: {
          addButton: "Score Band",
          collapsingItem: {
            title: "Score Band: {0} to {1} = {2}",
            minimumScore: {
              title: "Minimum Score:",
              placeholder: "{minimum_score}",
              titleTooltip: "Minimum required score to get the conversion value",
              errorMessage: "Must be a valid Minimum Score"
            },
            maximumScore: {
              title: "Maximum Score:",
              placeholder: "{maximum_score}",
              titleTooltip: "Maximum required score to get the conversion value",
              errorMessage: "Must be a valid Maximum Score"
            },
            enConversionValue: {
              title: "Value (English):",
              placeholder: "{conversion_value}",
              titleTooltip:
                "Conversion value to display when respecting the minimum and maximum scores",
              errorMessage: "Must be a valid Value"
            },
            frConversionValue: {
              title: "Value (French):",
              placeholder: "{conversion_value}",
              titleTooltip:
                "Conversion value to display when respecting the minimum and maximum scores",
              errorMessage: "Must be a valid Value"
            }
          },
          deletePopup: {
            title: "Delete Confirmation",
            content:
              "Deleting this item will delete all the content displayed. Are you sure you want to delete this object?"
          },
          invalidCollectionError: "Must be valid band ranges"
        }
      },
      questionSituations: {
        situation: {
          title: "Situation",
          titleTooltip:
            "The situation text to be displayed at the top of the question when a test is viewed in scorer mode."
        }
      },
      questionBlockTypes: {
        topTitle: "Item Block Names",
        description:
          'Define block names to be used with items. Blocks must only be used for items created within the "Items" section of the Test Builder. For items pulled from the Item Bank, use the "Grouping" functionality within the Item Bank instead.',
        collapsableItemName: "Question Block Type: {0}",
        addButton: "Add Item Black Name",
        name: {
          title: "Name",
          titleTooltip: "The unique identifier for a question block type.",
          errorMessage: "Must be a valid name."
        }
      },
      competencyTypes: {
        topTitle: "Competency Types",
        description:
          "These are types associated with questions. These types are assigned to questions for manual marking.",
        collapsableItemName: "Competency Type: {0}",
        addButton: "Add Competency Type",
        en_name: {
          title: "English Name",
          titleTooltip: "The unique identifier for a question block type."
        },
        fr_name: {
          title: "French Name",
          titleTooltip: "The unique identifier for a question block type."
        },
        max_score: {
          title: "Max Score",
          titleTooltip: "The max score a candidate can get for this competency."
        }
      },
      questionSections: {
        title: "Stem(s):",
        collapsableItemName: "Stem {0}",
        description:
          "These are the test section components belonging to the Test Section below. An example of a Test Section Component would be a Question List, Inbox, Side Navigation, or Single Page appearance/component.",
        addButton: "Stem",
        delete: {
          title: "Delete",
          description:
            "Deleting an element will delete all child elements as well. Are you sure you wish to continue?"
        },
        order: {
          title: "Stem Order",
          titleTooltip: "The order in which this component appears in a Top Tab navigation.",
          errorMessage: "Not a valid Name"
        },
        question_section_type: {
          title: "Content Type:",
          titleTooltip: "The type of section. Such as: Markdown, Image, Video...",
          errorMessage: "Not a valid Type"
        }
      },
      answers: {
        title: "Answer Choices:",
        collapsableItemNames: {
          id: "[ID]",
          value: "[Value]",
          text: "[Text]"
        },
        addButton: "Answer Choice",
        delete: {
          title: "Delete",
          description:
            "Deleting an element will delete all child elements as well. Are you sure you wish to continue?"
        },
        content: {
          title: "content",
          titleTooltip: "The text to display for this answer.",
          errorMessage: "Not a valid input"
        },
        order: {
          title: "Answer Choice Order",
          titleTooltip: "The order in which this answer appears."
        },
        ppc_answer_id: {
          title: "R&D Item ID",
          titleTooltip: "PPC Answer ID Tooltip Content - TO BE COMPLETED"
        },
        scoring_value: {
          title: "Scoring Value",
          titleTooltip:
            "The score to be added to the total test score when this answer is selected.",
          errorMessage: "Not a valid value"
        }
      },
      questionSectionMarkdown: {
        content: {
          title: "Content",
          titleTooltip: "Markdown to appear on the question."
        },
        delete: {
          title: "Confirm Deletion",
          content:
            "Deleting this item will delete all the content displayed. Are you sure you want to delete this object?"
        }
      },
      questionSectionEnd: {
        content: {
          title: "Specific Question",
          titleTooltip:
            "The literal question after context to be answered. This allows screen readers to navigate directly to this section."
        },
        delete: {
          title: "Confirm Deletion",
          content:
            "Deleting this item will delete all the content displayed. Are you sure you want to delete this object?"
        }
      },
      multipleChoiceQuestions: {
        header: "Question Sections"
      },
      emailQuestions: {
        scoringTitle: "Scoring",
        exampleRatingsTitle: "Example Ratings",
        competency_types: {
          title: "Competencies",
          titleTooltip: "The competencies being assessed in this question."
        },
        deleteDescription:
          "Deleting this item will delete the current object and any children this object may have. Are you sure you want to delete this object?",
        email_id: {
          title: "Email Id",
          titleTooltip:
            "Email Id is displayed at the top of the email and is the order in which emails are displayed."
        },
        to_field: {
          title: "To Field",
          titleTooltip: "The contact the email is to."
        },
        from_field: {
          title: "From Field",
          titleTooltip: "The Contact the email is from."
        },
        cc_field: {
          title: "CC Field",
          titleTooltip: "Contacts that are CC'd on this email."
        },
        date_field: {
          title: "Date",
          titleTooltip: "The date text to display on the email"
        },
        subject_field: {
          title: "Subject",
          titleTooltip: "The subject field to display on the email"
        },
        body: {
          title: "Body",
          titleTooltip: "The content of the email to display."
        }
      },
      situationRating: {
        score: {
          title: "Rating Score",
          titleTooltip:
            "This is the value that the example answered would be scored. Keep this to one decimal place."
        },
        example: {
          title: "Example Rationale",
          titleTooltip: "The Rationale behind giving the candidates answer this score."
        }
      },
      testManagement: {
        description: "Manage the status of this Test Version.",
        deactivateTestVersion: {
          title: "Test Version Status",
          titleTooltip:
            "De-activate this test version: Remove this test version from the sample tests pages or make it unavailable to be assigned by business operations (this does not invalidate currently active test accesses)."
        },
        activateTestVersion: {
          title: "Test Version Status",
          titleTooltip:
            "Make this test version available for use: Visible on the sample tests page or available to be assigned by business operations (as appropriate)."
        },
        uploadTestVersion: {
          title: "Upload Test Version to server",
          titleTooltip:
            "Upload the changes made to this test version to the server as a new version."
        },
        uploadTestVersionButton: "Upload",
        downloadTestVersion: {
          title: "Download Test Version JSON",
          titleTooltip: "Extract a JSON file of this test version; this excludes any changes made."
        },
        downloadTestVersionButton: "Download"
      }
    },
    // Test Administration Page
    testAdministration: {
      title: "Welcome, {0} {1}.",
      containerLabel: "Test Administrator",
      sideNavItems: {
        testAccessCodes: "Test Access Codes",
        activeCandidates: "Active Candidates",
        uat: "Unsupervised Internet Testing",
        reports: "Reports"
      },
      testAccessCodes: {
        description: "Manage the Test Access Codes you have generated.",
        table: {
          testAccessCode: "Test Access Code",
          test: "Test",
          staffingProcessNumber: "Assessment Process / Reference Number",
          action: "Action",
          actionButton: "Delete",
          generateNewCode: "Generate Test Access Code"
        },
        generateTestAccessCodePopup: {
          title: "Generate a Test Access Code",
          description: "Provide the following information to generate a Test Access Code:",
          testOrderNumber: "Test Order Number:",
          testOrderNumberCurrentValueAccessibility: "The selected test order number is:",
          orderlessTestOption: "No Test Order",
          referenceNumber: "Internal Reference Number:",
          departmentMinistry: "Organization:",
          fisOrganisationCode: "FIS Organisation Code:",
          fisReferenceCode: "FIS Reference Code:",
          billingContactName: "Billing Contact Name:",
          billingInfo: "Contact Email for Candidates:",
          mustBeAValidEmailErrorMessage: "Must be a valid email address",
          testToAdminister: "Test:",
          testToAdministerCurrentValueAccessibility: "The selected test to administer is:",
          testSessionLanguage: "Test Session Language",
          testSessionLanguageCurrentValueAccessibility: "The selected test session language is:",
          billingInformation: {
            title: "Billing Information:",
            staffingProcessNumber: "Assessment Process Number:",
            departmentMinistry: "Organization Code:",
            contact: "Billing Contact Name:",
            isOrg: "FIS Organization Code:",
            isRef: "FIS Reference Code:"
          },
          generateButton: "Generate"
        },
        disableTestAccessCodeConfirmationPopup: {
          title: "Delete Test Access Code",
          description:
            "If you delete the Test Access Code {0}, candidates will not be able to use it to check in."
        }
      },
      activeCandidates: {
        description:
          "As candidates check in, you will see their names appear in the list of Active Candidates below. After all candidates in attendance are checked in, delete the Test Access Code.",
        lockAllButton: "Lock All",
        unlockAllButton: "Unlock All",
        table: {
          candidate: "Candidate Name and Contact Details",
          dob: "Date of Birth",
          testCode: "Test Form",
          status: "Status",
          timer: "Time Limit",
          timeRemaining: "Time Left",
          actions: "Action",
          actionTooltips: {
            updateTestTimer: "Edit Time Limit",
            addEditBreakBank: "Add/Edit Break Bank",
            approve: "Approve Candidate",
            unAssign: "Remove Access",
            lock: "Lock Test",
            unlock: "Unlock Test",
            report: "Report Candidate"
          },
          updateTestTimerAriaLabel:
            "Edit the test timer of user {0} {1}. Their date of birth is {2}. Their test form is {3}. Their current test status is {4}. Their test timer is currently set to {5} hours and {6} minutes.",
          addEditBreakBankAriaLabel:
            "Add / Edit the break bank of user {0} {1}. Their date of birth is {2}. Their test form is {3}. Their current test status is {4}. Their test limt is currently set to {5} hours and {6} minutes.",
          approveAriaLabel:
            "Approve user {0} {1}. Their date of birth is {2}. Their test form is {3}. Their current test status is {4}. Their test timer is currently set to {5} hours and {6} minutes.",
          unAssignAriaLabel:
            "Remove access from user {0} {1}. Their date of birth is {2}. Their test form is {3}. Their current test status is {4}. Their test timer is currently set to {5} hours and {6} minutes.",
          lockAriaLabel:
            "Lock {0} {1}'s test. Their date of birth is {2}. Their test form is {3}. Their current test status is {4}. Their test timer is currently set to {5} hours and {6} minutes.",
          unlockAriaLabel:
            "Unlock {0} {1}'s test. Their date of birth is {2}. Their test form is {3}. Their current test status is {4}. Their test timer is currently set to {5} hours and {6} minutes.",
          reportAriaLabel: "Report {0} {1} user."
        },
        noActiveCandidates: "There are no active candidates",
        editTimePopup: {
          title: "Edit Time Limit",
          description1: "This should be used for accommodation purposes only.",
          description2: "Adjust the time limit for {0} {1}.",
          description3:
            "The timers cannot be set for less than the original amount of time allowed for the respective test section.",
          description4: "Total Test Time: {0} Hours {1} Minutes",
          hours: "HOURS",
          minutes: "MINUTES",
          incrementHoursButton: "Increase the number of hours. Current value is {0}.",
          decrementHoursButton: "Decrease the number of hours. Current value is {0}.",
          incrementMinutesButton: "Increase the number of minutes. Current value is {0}.",
          decrementMinutesButton: "Decrease the number of minutes. Current value is {0}.",
          timerCurrentValue: "The timer value is {0} hours and {1} minutes.",
          setTimerButton: "Set Timer"
        },
        addEditBreakBankPopup: {
          title: "Set Break Bank",
          description: "Allocate break bank to {0} {1} for this test.",
          setTimeTitle: "Break Bank",
          setBreakBankButton: "Enter"
        },
        lockPopup: {
          title: "Lock a Test",
          description1: "You are about to lock {0} {1}'s test.",
          lockButton: "Lock the Test"
        },
        unlockPopup: {
          title: "Resume Test",
          description1: "You are about to unlock {0} {1}'s test.",
          description2: "The test timer will resume once you unlock the test.",
          description3: "Be sure to complete an incident report regarding this candidate.",
          unlockButton: "Unlock Test"
        },
        lockAllPopup: {
          title: "Lock All Candidate Tests",
          description1: "You are about to lock this test for all of your active candidates.",
          description2:
            "If, after the incident that has caused you to lock the tests, it is determined that the test can resume, you will need to unlock the tests for the candidates that wish to continue testing.",
          description3:
            "Be sure to complete an incident report and a withdrawal from examination form for any candidates not able to continue after the incident.",
          lockTestsButton: "Lock Tests"
        },
        unlockAllPopup: {
          title: "Unlock All Candidate Tests",
          description1: "You are about to unlock this test for all of your active candidates.",
          description2:
            "Be sure to lock any tests for candidates who do not wish to continue with the test or who are unable to continue.",
          description3:
            "Be sure to file a 'withdrawal from examination' form for those candidates, as well as an incident report.",
          unlockTestsButton: "Unlock All Tests"
        },
        unAssignPopup: {
          title: "Remove Access",
          description: "Are you sure you want to remove {0} {1}'s access to the test?",
          confirm: "Remove Access"
        }
      },
      uit: {
        title: "Unsupervised Internet Testing (UIT)",
        description: "Manage your UIT invitations and processes.",
        tabs: {
          invitations: {
            title: "Invitations",
            testOrderNumber: "Test Order Number:",
            testOrderNumberCurrentValueAccessibility: "The selected test order number is:",
            testOrderNumberErrorMessage: "Please select the test order number",
            orderlessTestOption: "No Test Order",
            reuseData: "Reuse Data:",
            referenceNumber: "Internal Reference Number:",
            referenceNumberError: "Please Provide an Internal Reference Number",
            departmentMinistry: "Organization:",
            fisOrganisationCode: "FIS Organisation Code:",
            fisOrganisationCodeError: "Please Provide the FIS Organisation Code",
            fisReferenceCode: "FIS Reference Code:",
            fisReferenceCodeError: "Please Provide the FIS Reference Code",
            billingContactName: "Billing Contact Name:",
            billingContactNameError: "Please Provide the Billing Contact Name",
            billingInformation: "Contact Email for Candidates:",
            billingInformationError: "Must be a valid email address",
            test: "Test:",
            providedInCsvOption: "Provided in the list",
            reasonForTesting: "Reason for Testing:",
            invalidReasonForTesting: "Please select the reason for testing",
            levelRequired: "Level Required:",
            levelRequiredRawScore: "Raw Score Required:",
            invalidLevelRequired: "Please select the level required",
            invalidLevelRequiredRawScore: "Provided number exceeds the maximum possible score",
            testCurrentValueAccessibility: "The selected tests are: {0}",
            testsErrorMessage: "Please select at least one test",
            validityEndDate: "Expiry Date:",
            numberOfCandidatesBasedOnEndDateExceededError:
              "Maximum number of candidates ending the same day exceeded",
            emailTemplateCurrentValueAccessibility: "The selected email template is:",
            emailTemplate: "Email Template:",
            candidates: "List of Candidates:",
            candidatesCurrentValueAccessibility: "The selected candidates are:",
            browseButton: "Browse...",
            fileSelectedAccessibility: "The selected file is {0}",
            downloadTemplateButton: "Download Template",
            csvTemplateFileName: "List of Candidates.xlsx",
            wrongFileTypeError: "Please select a CSV file type",
            invalidFileError:
              "Error found in file. Please fix the information in the following cells: {0}",
            noDataInCsvFileError: "Please make sure that your CSV file contains data",
            noCsvFileSelectedError: "Please select a valid CSV file",
            maximumInvitedCandideReachedError:
              "Cannot exceed a total of 500 invitations at once (including multi-tests requests)",
            duplicateEmailsFoundInCsvError:
              "Duplicate emails found. Please fix the information related to cells {0} and {1}.",
            sendInvitesButton: "Send Invitations",
            resetAllFields: "Reset All Fields",
            invitesConfirmationPopup: {
              title: "Review and Send Invitation to Candidates",
              titleInvitationsSentSuccessfull: "Invitations Sent Successfully",
              titleInvitationsSentFail: "Invitations Not Sent",
              inviteData: {
                processNumber: "Assessment Process Number:",
                referenceNumber: "Internal Reference Number:",
                requestingDepartment: "Requesting Organization:",
                fisOrganisationCode: "FIS Organisation Code:",
                fisReferenceCode: "FIS Reference Code:",
                HrCoordinator: "Billing Contact Name:",
                billingContactInfo: "Contact Email for Candidates:",
                test: "Test:",
                validityEndDate: "Expiry Date:",
                reasonForTesting: "Reason for Testing:",
                levelRequired: "Level Required:",
                levelRequiredRawScore: "Raw Score Required:",
                table: {
                  title: "List of Candidates ({0}):",
                  emailAddress: "Email Address",
                  firstName: "First Name",
                  lastName: "Last Name",
                  totalTime: "Total Time",
                  breakBank: "Break Bank",
                  actions: "Actions",
                  editTestTimeTooltip: "Edit Time Limit",
                  editBreakBankTooltip: "Edit Break Bank"
                },
                invitationSentSuccessfullMessage:
                  "The invitations have been sent to the candidates.",
                invitationSentFailMessage:
                  "Something happened and the invitations haven't been sent! Please contact the HelpDesk."
              },
              sendButton: "Send"
            },
            resetAllFieldsPopup: {
              title: "Reset All Fields?",
              description: "Are you sure you want to reset all fields?",
              resetButton: "Reset"
            }
          },
          activeProcessess: {
            title: "Active Processes",
            table: {
              columnOne: "Assessment Process / Reference Number",
              columnTwo: "Test",
              columnThree: "Number of Tests Taken",
              columnFour: "Requesting Organization",
              columnFive: "Invitation Date",
              columnSix: "Expiry Date",
              columnSeven: "Actions",
              noActiveProcessess: "There are no active processes",
              viewSelectedProcessTooltip: "View Active Process Details",
              updateSelectedProcessExpiryDate: "Update Process Expiry Date",
              deleteSelectedProcessTooltip: "Delete Active Process",
              viewButtonAccessibility: "View assessment process number {0}",
              updateSelectedProcessExpiryDateAccessibility: "Update expiry date of process {0}",
              deleteButtonAccessibility: "Delete assessment process number {0}"
            },
            selectedProcessPopup: {
              title: "Active Process Details",
              financialData: {
                processNumber: "Assessment Process / Reference Number:",
                requestingDepartment: "Requesting Organization:",
                HrCoordinator: "Billing Contact Name:",
                validityEndDate: "Expiry Date:",
                test: "Test:"
              },
              description: "The following candidates ({0}) have been invited to take the UIT:",
              columnOne: "Email Address",
              columnTwo: "Status",
              columnThree: "Action",
              testInProgress: "In Progress",
              testTaken: "Taken",
              testNotTaken: "Not Taken",
              testUnassigned: "Unassigned",
              testInvalidated: "Invalidated",
              testCodeDeactivated: "Deactivated",
              deleteCandidateTooltip: "Unassign/Deactivate Test Access",
              deleteCandidateAccessibility: "Unassign/deactivate the test access of {0}"
            },
            updateProcessExpiryDatePopup: {
              title: "Modify Expiry Date",
              titleSuccess: "Process Successfully Updated: {0}",
              validityEndDate: "Expiry Date:",
              reasonForModification: "Reason for modification:",
              rightButton: "Modify",
              successMessage: "The expiry date has been updated and the emails have been sent."
            },
            deleteProcessPopup: {
              title: "Delete Assessment Process Number: {0}",
              titleSuccess: "Process Successfully Deleted: {0}",
              systemMessageDescription:
                "You are about to delete the following process. Note that there are still {0} candidate(s) who have not taken their test yet. Are you sure you want to continue?",
              description: "Process Details:",
              deleteButton: "Delete Process",
              successMessage:
                "The assessment process number has been deleted and cancellation emails have been sent."
            },
            deleteSingleUitTestPopup: {
              title: "Confirmation to Unassign/Deactivate Test Access",
              description: "Are you sure you want to unassign/deactivate {0}'s access to the test?",
              deleteButton: "Unassign/Deactivate",
              selectReason: "Please select a reason:"
            },
            uitTestAlreadyStartedPopup: {
              title: "Action Denied"
            }
          },
          completedProcessess: {
            title: "Completed Processess",
            table: {
              process: "Assessment Process / Reference Number",
              test: "Test",
              totalCandidates: "Number of Tests Taken",
              testsTaken: "Requesting Organization",
              endDate: "Expiry Date",
              actions: "Action",
              actionTooltip: "View Completed Process Details"
            },
            noCompletedProcessess: "There are no completed processes",
            popup: {
              title: "UIT: {0} - {1}",
              description: "The following candidates ({0}) have been invited to take the UIT:"
            }
          }
        }
      },
      reports: {
        description: "Generate a results report for a Test Order you administered."
      }
    },

    // PPC  Administration Page
    ppcAdministration: {
      title: "Welcome, {0} {1}.",
      containerLabel: "R&D Dashboard",
      sideNavItems: {
        reports: "Reports"
      },
      reports: {
        title: "Reports",
        description: "Generate a report based on a Test Order."
      }
    },

    // Report Generator Component
    reports: {
      reportTypeLabel: "Report Type:",
      reportTypes: {
        individualScoreSheet: "Individual Test Result",
        resultsReport: "Group Test Results",
        financialReport: "Financial Report",
        financialReportFromDateLabel: "From",
        financialReportToDateLabel: "To",
        testContentReport: "Test Content Report",
        testTakerReport: "Candidate Test Information Report",
        candidateActionsReport: "Candidate Actions Report"
      },
      testOrderNumberLabel: "Test Order / Reference Number:",
      testLabel: "Test:",
      testsLabel: "Test(s):",
      testLabelAccessibility: "The selected tests are:",
      candidateLabel: "Candidate:",
      dateFromLabel: "From:",
      dateToLabel: "To:",
      invalidDate: "Must be a valid Date",
      dateError: "The first date must be before or the same as the second date",
      parentCodeLabel: "Test Number:",
      testCodeLabel: "Test Form:",
      testVersionLabel: "v:",
      generateButton: "Generate Report",
      noDataPopup: {
        title: "No Results ",
        description: "No results found with parameters provided."
      },
      testStatus: {
        assigned: "CHECKED-IN",
        ready: "READY",
        preTest: "PRE-TEST",
        active: "TESTING",
        locked: "LOCKED",
        paused: "PAUSED",
        quit: "QUIT",
        submitted: "SUBMITTED",
        unassigned: "UNASSIGNED"
      },
      timedOut: "Timed Out",
      orderlessRequest: "Orderless",
      individualScoreSheet: {
        personID: "Person ID",
        emailAddress: "Email Address",
        name: "Name",
        pri: "PRI",
        orderNumber: "Order No.",
        assessmentProcessOrReferenceNb: "Assessment Process / Reference Number",
        test: "Test",
        testDescriptionFR: "Test Description (FR)",
        testDescriptionEN: "Test Description (EN)",
        testStartDate: "Test Start Date",
        score: "Score",
        level: "Level",
        testStatus: "Test Status"
      },
      resultsReport: {
        lastname: "Last Name",
        firstname: "First Name",
        emailAddress: "Email Address",
        uitInvitationEmail: "UIT Invitation Email",
        pri: "PRI",
        orderNumber: "Order No.",
        assessmentProcessOrReferenceNb: "Assessment Process / Reference Number",
        test: "Test",
        testDescriptionFR: "Test Description (FR)",
        testDescriptionEN: "Test Description (EN)",
        testDate: "Test Date",
        score: "Score",
        level: "Level",
        testStatus: "Test Status"
      },
      financialReport: {
        candidateLastName: "Last Name",
        candidateFirstName: "First Name",
        candidatePri: "PRI",
        taWorkEmail: "Test Administrator's Work Email",
        taOrg: "Test Administrator's Organization",
        requestingDepartment: "Requesting Departement",
        orderNumber: "Order No.",
        assessmentProcess: "Assessment Process / Reference Number",
        orgCode: "Organization Code",
        fisOrgCode: "FIS Organization Code",
        fisRefCode: "FIS Reference Code",
        billingContactName: "Contact Email for Candidates",
        testStatus: "Test Status",
        isInvalid: "Is Invalid",
        testDate: "Test Date",
        testVersion: "Test Version",
        testDescriptionFR: "Test Description (FR)",
        testDescriptionEN: "Test Description (EN)"
      },
      testContentReport: {
        questionSectionNumber: "Question Section No.",
        questionSectionTitleFR: "Question Section Title (FR)",
        questionSectionTitleEN: "Question Section Title (EN)",
        rdItemID: "R&D Item ID",
        questionNumber: "Question No.",
        questionTextFR: "Item Text (FR)",
        questionTextFrHTML: "Item Text HTML (FR)",
        questionTextEN: "Item Text (EN)",
        questionTextEnHTML: "Item Text HTML (EN)",
        answerChoiceID: "Choice {0} - ID",
        answerChoiceTextFR: "Choice {0} - Text (FR)",
        answerChoiceTextFrHTML: "Choice {0} - Text HTML (FR)",
        answerChoiceTextEN: "Choice {0} - Text (EN)",
        answerChoiceTextEnHTML: "Choice {0} - Text HTML (EN)",
        scoringValue: "Choice {0} - Value",
        sampleTest: "Sample Test"
      },
      testTakerReport: {
        selectedLanguage: "en",
        userCode: "User code",
        username: "Username",
        firstname: "First Name",
        lastname: "Last Name",
        pri: "PRI/Service number",
        dateOfBirth: "Date of Birth",
        currentEmployer: "Current Employer",
        org: "Organization",
        group: "Occupational Group",
        level: "Occupational Level",
        residence: "Residence",
        education: "Education",
        gender: "Gender",
        identifyAsWoman: "Woman",
        aboriginal: "Indigenous",
        aboriginalInuit: "Indigenous - Inuit",
        aboriginalMetis: "Indigenous - Métis",
        aboriginalNAIndianFirstNation: "Indigenous - NA Indian/First Nation",
        visibleMinority: "Visible Minority",
        visibleMinorityBlack: "VM - Black",
        visibleMinorityChinese: "VM - Chinese",
        visibleMinorityFilipino: "VM - Filipino",
        visibleMinorityJapanese: "VM - Japanese",
        visibleMinorityKorean: "VM - Korean",
        visibleMinorityNonWhiteLatinx: "VM - Non-white Latinx",
        visibleMinorityNonWhiteMiddleEastern: "VM - Non-white Middle Eastern",
        visibleMinorityMixedOrigin: "VM - Mixed Origin",
        visibleMinoritySouthAsianEastIndian: "VM - South Asian/East Indian",
        visibleMinoritySoutheastAsian: "VM - Southeast Asian",
        visibleMinorityOther: "VM - Other",
        visibleMinorityOtherDetails: "VM - Other (details)",
        disability: "Disability",
        disabilityVisual: "Disability - Visual",
        disabilityDexterity: "Disability - Dexterity",
        disabilityHearing: "Disability - Hearing",
        disabilityMobility: "Disability - Mobility",
        disabilitySpeech: "Disability - Speech",
        disabilityOther: "Disability - Other",
        disabilityOtherDetails: "Disability - Other (details)",
        requestingDepartment: "Requesting Organization",
        requestingDepartmentLanguage: "eabrv",
        administrationMode: "Administration Mode",
        assisted: "Assisted",
        automated: "Automated",
        reasonForTesting: "Reason for Testing",
        levelRequired: "Required Level",
        testNumber: "Test Number",
        testSectionLanguage: "Test Section Language",
        testForm: "Test Form",
        catVersion: "CAT Version",
        testDate: "Test Date",
        testStartDate: "Test Start Date",
        submitTestDate: "Submit Test Date",
        questionSectionNumber: "Question Section No.",
        rdItemID: "R&D Item ID",
        pilotItem: "Pilot Item",
        itemOrderMaster: "Item Order - Master",
        itemOrderPresentation: "Item Order - Presentation",
        itemOrderViewed: "Item Order - View",
        questionTextFr: "Item Text (FR)",
        questionTextEn: "Item Text (EN)",
        questionTextHtmlFr: "Item Text HTML (FR)",
        questionTextHtmlEn: "Item Text HTML (EN)",
        choiceXPpcAnswerId: "Choice {0} - ID",
        choiceXPoints: "Choice {0} - Value",
        choiceXPresentation: "Choice {0} - Presentation",
        choiceXTextFr: "Choice {0} - Text (FR)",
        choiceXTextEn: "Choice {0} - Text (EN)",
        choiceXTextHtmlFr: "Choice {0} - Text HTML (FR)",
        choiceXTextHtmlEn: "Choice {0} - Text HTML (EN)",
        languageOfAnswer: "Language of Answer",
        timeSpent: "Total Time Spent on Item (s)",
        responseMasterOrder: "Response",
        responseChoiceId: "Response - CAT Choice ID",
        scoringValue: "Item Score",
        sectionScore: "Section Score",
        totalScore: "Total Score",
        convertedScore: "Level",
        testStatus: "Test Status",
        testSubmissionMode: "Section Submission Mode",
        scoreValidity: "Result Validity"
      },
      candidateActionsReport: {
        multiAssignedTestsSelectionOptions: {
          label: "Test Session:",
          startDate: "Test Date:",
          submitDate: "Submit Date:",
          modifyDate: "Modified Date:"
        },
        origin: "Entry Type",
        historyDate: "Entry Date",
        historyType: "Initial or Modified Entry",
        status: "Test Status",
        previousStatus: "Previous Status",
        startDate: "Test Date",
        submitDate: "Submit Test Date",
        testAccessCode: "Test Access Code",
        totalScore: "Total Score",
        testAdministrator: "Test Administrator",
        testSessionLanguage: "Test Session Language",
        enConvertedScore: "Level (EN)",
        frConvertedScore: "Level (FR)",
        uitInviteId: "UIT Invite ID",
        isInvalid: "Score Validity",
        timeType: "Section Entry or Exit",
        assignedTestSectionId: "Test Section Name",
        candidateAnswerId: "Candidate Item Session ID",
        markForReview: "Marked for Review",
        questionId: "CAT Item ID",
        selectedLanguage: "Interface Language",
        answerId: "Response - CAT Choice ID",
        candidateAnswerIdRef: "Candidate Item Session ID (Ref.)"
      },
      itemBankReport: {
        name: "Item Bank Content Report - {0} [{1}]",
        selectedLanguage: "en",
        systemId: "System ID",
        version: "Version",
        historicalId: "R&D Item ID",
        itemStatus: "Item Status",
        scoringKey: "Scoring Key",
        scoringKeyId: "Scoring Key ID",
        stemId: "Stem {0} - ID",
        stemTextEn: "Stem {0} - Text (EN)",
        stemTextHtmlEn: "Stem {0} - Text HTML (EN)",
        stemTextFr: "Stem {0} - Text (FR)",
        stemTextHtmlFr: "Stem {0} - Text HTML (FR)",
        stemScreenReaderId: "Stem {0} - Screen Reader ID",
        stemScreenReaderTextEn: "Stem {0} - Screen Reader Text (EN)",
        stemScreenReaderTextHtmlEn: "Stem {0} - Screen Reader Text HTML (EN)",
        stemScreenReaderTextFr: "Stem {0} - Screen Reader Text (FR)",
        stemScreenReaderTextHtmlFr: "Stem {0} - Screen Reader Text HTML (FR)",
        optionHistoricalId: "Choice {0} - ID",
        optionTextEn: "Choice {0} - Text (EN)",
        optionTextHtmlEn: "Choice {0} - Text HTML (EN)",
        optionTextFr: "Choice {0} - Text (FR)",
        optionTextHtmlFr: "Choice {0} - Text HTML (FR)",
        optionScore: "Choice {0} - Value",
        optionScreenReaderHistoricalId: "Choice {0} - Screen Reader ID",
        optionScreenReaderTextEn: "Choice {0} - Screen Reader Text (EN)",
        optionScreenReaderTextHtmlEn: "Choice {0} - Screen Reader Text HTML (EN)",
        optionScreenReaderTextFr: "Choice {0} - Screen Reader Text (FR)",
        optionScreenReaderTextHtmlFr: "Choice {0} - Screen Reader Text HTML (FR)",
        optionScreenReaderScore: "Choice {0} - Screen Reader Value",
        itemType: "Item Type"
      }
    },

    // DatePicker Component
    datePicker: {
      dayField: "Day",
      dayFieldSelected: "Day field selected",
      monthField: "Month",
      monthFieldSelected: "Month field selected",
      yearField: "Year",
      yearFieldSelected: "Year field selected",
      hourField: "Hour",
      hourFieldSelected: "Hour field selected",
      minuteField: "Minute",
      minuteFieldSelected: "Minute field selected",
      currentValue: "The current value is:",
      none: "Select",
      datePickedError: "Must be a valid Date",
      futureDatePickedError: "Must be a future date",
      futureDatePickedIncludingTodayError: "Cannot be earlier than today's date",
      comboStartEndDatesPickedError: "Start date must be before end date",
      comboFromToDatesPickedError: "From date must be before To date",
      deselectOption: "Deselect"
    },

    // PrivacyNoticeStatement Component
    privacyNoticeStatement: {
      title: "Privacy Notice Statement",
      descriptionTitle: "Privacy Notice Statement",
      paragraph1:
        "Personal information is used to provide assessment services to clients of the Personnel Psychology Centre. It is collected for staffing related purposes, in accordance with sections 11, 30 and 36 of the {0}. For organizations not subject to this act, personal information is collected under the authority of that organization’s enabling statute and section 35 of the {1}. Second language evaluation results will be disclosed to authorized organizational officials. Results of all other tests will be disclosed only to the requesting organization. Test results may be disclosed to the Investigations Directorate of the Public Service Commission if an investigation is conducted pursuant to section 66 or 69 of the {2}. Your information may also be used for statistical and analytical research purposes. In some cases, information may be disclosed without your consent, pursuant to section 8(2) of the {3}. Provision of your personal information is voluntary, but if you choose not to provide your information, you may not be able to receive Personnel Psychology Centre services.",
      paragraph2:
        "The information is collected and used as described in the Personnel Psychology Centre’s (PSC PCU 025) PIB, found in the Public Service Commission’s {0}.",
      paragraph3:
        "You have the right to access and correct your personal information, and to request corrections where you believe there is an error or omission. You also have the right to file a complaint to the {0} regarding the handling of your personal information.",
      publicServiceEmploymentActLinkTitle: "Public Service Employment Act",
      publicServiceEmploymentActLink: "https://laws-lois.justice.gc.ca/eng/acts/p-33.01/",
      privacyActLinkTitle: "PA",
      privacyActLink: "https://laws-lois.justice.gc.ca/eng/acts/p-21/",
      infoSourceLinkTitle: "Info Source",
      infoSourceLink:
        "https://www.canada.ca/en/public-service-commission/corporate/about-us/access-information-privacy-office/info-source-sources-federal-government-employee-information.html#122",
      privacyCommissionerOfCanadaLinkTitle: "Privacy Commissioner of Canada",
      privacyCommissionerOfCanadaLink: "https://www.priv.gc.ca/en/"
    },

    // DropdownSelect Component
    dropdownSelect: {
      pleaseSelect: "Select",
      noOptions: "No Options"
    },

    // SearchBarWithDisplayOptions component
    SearchBarWithDisplayOptions: {
      searchBarTitle: "Search:",
      resultsFound: "{0} result(s)",
      clearSearch: "Clear Search",
      noResultsFound: "There are no results found based on your search.",
      displayOptionLabel: "Display:",
      displayOptionAccessibility: "Number of results per page",
      displayOptionCurrentValueAccessibility: "The selected value is:"
    },

    // Profile Page
    profile: {
      completeProfilePrompt: "To update your profile, complete required fields and click Save.",
      title: "Welcome, {0} {1}.",
      sideNavItems: {
        personalInfo: "Personal Information",
        password: "Password",
        preferences: "Preferences",
        permissions: "Rights and Permissions",
        profileMerge: "Merge Accounts"
      },
      personalInfo: {
        title: "My Personal Information",
        nameSection: {
          firstName: "First Name:",
          lastName: "Last Name:"
        },
        emailAddressesSection: {
          primary: "Primary Email Address:",
          secondary: "Secondary Email Address (optional):",
          emailError: "Must be a valid Email Address"
        },
        dateOfBirth: {
          title: "Date of Birth:",
          titleTooltip:
            "Adding your date of birth will facilitate the consolidation of all your results in your test result record."
        },
        pri: {
          title: "PRI (optional):",
          titleTooltip:
            "Adding your PRI will facilitate the consolidation of all your results in your test result record",
          priError: "Must be a valid PRI"
        },
        militaryNbr: {
          title: "Service Number (optional):",
          titleTooltip:
            "Adding your Service number will facilitate the consolidation of all your results in your test result record",
          militaryNbrError: "Must be a valid Service Number"
        },
        psrsAppId: {
          title: "PSRS Applicant ID (optional):",
          psrsAppIdError: "Must be a valid PSRS Applicant ID"
        },
        optionalField: "(optional)",
        saveConfirmationPopup: {
          title: "Personal Information Updated",
          description: "Your personal information has been saved successfully."
        },
        profileChangeRequest: {
          tooltip: "Profile Change Request",
          existingRequestTooltip: "View Profile Change Request",
          requestPopup: {
            title: "Profile Change Request",
            successTitle: "Request Sent Successfully",
            deleteTitle: "Discard?",
            description:
              "Due to the portability of certain tests and the need to adequately validate identity... (to be completed)",
            successDescription: "Your request has been sent successfully.",
            current: "Current",
            new: "New",
            firstNameLabel: "First Name:",
            firstNameError: "Must be a valid First Name",
            lastNameLabel: "Last Name:",
            lastNameError: "Must be a valid Last Name",
            dobLabel: "Date of Birth:",
            comments: "Comments (optional):",
            deleteConfirmationSystemMessage:
              "There changes will be removed from the queue to be reviewed.",
            deleteConfirmationDescription: "Are you sure you want to continue?"
          }
        }
      },
      eeInformation: {
        title: "Employment Equity",
        description1:
          "The Public Service Commission (PSC) needs your help to ensure its assessments are working as intended. The personal information you share below will be accessible exclusively to the PSC and solely used for research, analysis, and test development purposes. Participation is voluntary: you can select the option “Prefer not to say” for any questions below. The information you provide will not be shared with human resource personnel or those responsible for hiring decisions.",
        description2:
          "The PSC is committed to protecting the privacy rights of individuals. Here is our {0}.",
        privacyNoticeLink: "Privacy Notice",
        identifyAsWoman: "Do you identify as a woman?",
        invalidIdentifyAsWomanErrorMessage: "Please select a response option",
        aboriginal: "Are you an Indigenous person?",
        invalidAboriginalErrorMessage: "Please select a response option",
        subAboriginal: "Please select all that apply:",
        invalidSubAboriginalErrorMessage: "Please select at least one option",
        visibleMinority: "Are you a member of a visible minority group?",
        invalidVisibleMinorityErrorMessage: "Please select a response option",
        subVisibleMinority: "Please select all that apply:",
        subVisibleMinorityOtherTextAreaLabel: "If other, please specify:",
        invalidSubVisibleMinorityErrorMessage: "Please select at least one option",
        disability: "Are you a person with a disability?",
        invalidDisabilityErrorMessage: "Please select a response option",
        subDisability: "Please select all that apply:",
        subDisabilityOtherTextAreaLabel: "If other, please specify:",
        invalidSubDisabilityErrorMessage: "Please select at least one option"
      },
      additionalInfo: {
        title: "Additional Information (Optional)",
        current_employer: {
          title: "Current Employer:"
        },
        organization: {
          title: "Organization:"
        },
        group: {
          title: "Group:"
        },
        subclassification: {
          title: "Subgroup:"
        },
        level: {
          title: "Level:"
        },
        residence: {
          title: "Residence:"
        },
        education: {
          title: "Education:"
        },
        gender: {
          title: "Gender:"
        }
      },
      password: {
        newPassword: {
          title: "My Password",
          updatedDate: "Your password was last updated on: {0}",
          updatedDateNever: "never",
          currentPassword: "Current Password:",
          showCurrentPassword: "Show Current Password",
          newPassword: "New Password:",
          showNewPassword: "Show New Password",
          confirmPassword: "Confirm Password:",
          showConfirmPassword: "Show Password Confirmation",
          invalidPasswordError: "Invalid password",
          popup: {
            title: "Password Updated",
            description: "Your password has been updated successfully."
          }
        },
        passwordRecovery: {
          title: "Password Recovery",
          secretQuestion: "Secret Question:",
          secretAnswer: "Secret Answer:",
          secretQuestionUpdatedConfirmation: "Your secret question has been updated successfully"
        }
      },
      preferences: {
        title: "My Preferences",
        description: "Modify my preferences.",
        notifications: {
          title: "Notifications",
          checkBoxOne: "Always send notifications to my primary email address",
          checkBoxTwo: "Always send notifications to my secondary email address"
        },
        display: {
          title: "Display",
          checkBoxOne: "Allow others to see my profile picture",
          checkBoxTwo: "Hide Tooltip icons"
        },
        accessibility: {
          title: "Accessibility:",
          description: "Adjust the font, font size, and text spacing to be displayed:"
        }
      },
      permissions: {
        title: "My Rights and Permissions",
        description: "You do not need additional rights and permissions to take tests.",
        systemPermissionInformation: {
          title: "Information about your user role",
          addPermission: "Obtain Rights and Permissions",
          pending: "Pending",
          superUser: {
            title: "Super User",
            permission: "User has full system access and permissions"
          }
        },
        addPermissionPopup: {
          title: "Rights and Permissions Request",
          description:
            "Please fill out the following information and select the requested user role(s).",
          gocEmail: "Government of Canada Email Address:",
          gocEmailTooltip: "You must enter your Government of Canada email address to register.",
          gocEmailError: "Must be a valid Email Address",
          pri: "PRI:",
          militaryNbr: "Service Number:",
          priError: "Must be a valid PRI",
          militaryNbrError: "Must be a valid Service Number",
          supervisor: "Supervisor Name:",
          supervisorError: "The name contains one or more invalid characters",
          supervisorEmail: "Supervisor Email Address:",
          supervisorEmailError: "Must be a valid Email Address",
          rationale: "Reason for the request:",
          rationaleError: "Must provide a reason for this request",
          permissions: "Obtain permissions for the following user roles:",
          permissionsError: "Must select at least one user role"
        },
        noPermissions: "You have no permissions."
      },
      testPermissions: {
        title: "Test Access Permissions",
        description: "As a Test Administrator, you have access to administer the following tests:",
        table: {
          title: "Test Accesses",
          column1: "Test Form",
          column1OrderItem: "Order by test version",
          column2: "Test Order Number",
          column2OrderItem: "Order by test order number",
          column3: "Assessment Process Number",
          column3OrderItem: "Order by assessment process number",
          column4: "Expiry Date",
          column4OrderItem: "Order by expiry date"
        }
      },
      saveButton: "Save"
    },

    // ETTA Page
    systemAdministrator: {
      title: "Welcome, {0} {1}.",
      containerLabelEtta: "Business Operations",
      containerLabelRD: "R&D Operations",
      containerLabelBoth: "Operations",
      sideNavItems: {
        dashboard: "Dashboard",
        activeTests: "Active Tests",
        testAccessCodes: "Test Access Codes",
        permissions: "Rights and Permissions",
        itemBanks: "Item Bank Administration",
        testAccesses: "Test Access Management",
        incidentReports: "Incident Reports",
        scoring: "Scoring",
        reScoring: "Re-Scoring",
        reports: "Reports",
        userLookUp: "User Look-Up",
        systemAlert: "System Alerts"
      },
      permissions: {
        description: "Manage and assign rights and permissions to users.",
        tabs: {
          permissionRequests: {
            title: "Rights and Permissions Requests",
            table: {
              columnOne: "User Role",
              columnTwo: "User Name and Contact Details",
              columnThree: "Request Date",
              columnFour: "Action",
              noPermission: "There are no rights or permissions requests at the moment."
            },
            viewButton: "View",
            viewButtonAccessibility: "View {0} {1}",
            popup: {
              title: "Rights and Permissions Request",
              description:
                "Please approve or deny the request for rights and permissions made by {0}.",
              permissionRequested: "Obtain rights for the following user role:",
              deniedReason: "Reason for denial of request:",
              denyButton: "Deny",
              grantButton: "Approve",
              grantPermissionErrors: {
                notEmptyDeniedReasonError:
                  "The field must be empty if you want to approve this request",
                missingDeniedReasonError: "You must enter a reason to deny the permission",
                usernameDoesNotExistError: "This username no longer exists"
              }
            }
          },
          activePermissions: {
            title: "Active Rights and Permissions",
            table: {
              permission: "User Role",
              user: "User Name and Contact Details",
              action: "Action",
              actionButtonLabel: "Modify",
              actionButtonAriaLabel: "Modify {0} {1}"
            },
            viewEditDetailsPopup: {
              title: "Modify or Delete User Roles",
              description: "Modify or delete {0}'s rights and permissions.",
              deleteButton: "Delete",
              saveButton: "Modify",
              permission: "User Role:",
              reasonForModification: "Reason for modification or deletion:",
              reasonForModificationError:
                "You must provide a reason for modifying or deleting this request"
            },
            deletePermissionConfirmationPopup: {
              title: "Confirm deletion of rights and permissions",
              systemMessageDescription:
                "This action will revoke {0} permission from {1}'s account. Are you sure you want to proceed?"
            },
            updatePermissionDataConfirmationPopup: {
              title: "Confirmation of updates",
              description: "{0}'s rights and permissions have been updated successfully."
            }
          }
        }
      },
      itemBanks: {
        title: "Item Bank Administration",
        description: "Create item banks and grant bank accesses to test developers.",
        tabs: {
          activeItemBanks: {
            title: "Active Item Banks",
            newItemBankButton: "New Item Bank",
            table: {
              column1: "Item Bank",
              column2: "Last Modified",
              column3: "Organization",
              column4: "Actions",
              noData: "There are no results found based on your search"
            },
            newItemBankPopup: {
              title: "Create Item Bank",
              description: "Complete the following fields to create a new item bank.",
              itemBankDepartment: "Owner organization:",
              itemBankPrefix: "Item bank prefix:",
              itemBankLanguage: "Item language:",
              itemBankComments: "Comments (optional):",
              rightButton: "Create",
              successMessage: "The new item bank has been created.",
              itemBankAlreadyExistsError: "This prefix is already used by another item bank."
            },
            selectedItemBank: {
              backToRdOperationsButton: "Back to Item Bank Administration",
              backToRdOperationsPopup: {
                title: "Return to Item Bank Administration?",
                warningDescription: "All unsaved data will be lost.",
                description: "Do you want to continue?"
              },
              tabs: {
                selectedItemBank: {
                  title: "Item Bank {0}",
                  sideNavigationItems: {
                    ItemBankAccesses: {
                      ItemTitle: "Item Bank Accesses",
                      title: "Item Bank: {0}",
                      description: "Manage test developer accesses to this item bank.",
                      itemBankPrefix: "Item bank prefix:",
                      itemBankAlreadyExistsError:
                        "This prefix is already used by another item bank.",
                      itemBankNameEn: "English name of item bank:",
                      itemBankNameFr: "French name of item bank:",
                      itemBankDepartment: "Owner organization:",
                      itemBankLanguage: "Item language:",
                      addTestDeveloperButton: "Add test developer",
                      addTestDeveloperPopup: {
                        title: "Add Test Developer to Item Bank {0}",
                        description: "Select a test developer to add to the item bank.",
                        testDeveloperLabel: "Test developer:",
                        addButton: "Test developer"
                      },
                      deleteTestDeveloperPopup: {
                        title: "This action will delete the item bank accesses of {0}.",
                        description: "This action will delete the item bank accesses of {0}.",
                        deleteButton: "Delete"
                      },
                      table: {
                        column1: "Test Developer",
                        column2: "Access Type",
                        column3: "Actions",
                        deleteButtonTooltip: "Delete",
                        deleteButtonAriaLabel: "Delete access to this item bank for {0}",
                        noData: "There are no test developers associated with this item bank."
                      }
                    }
                  }
                }
              }
            }
          },
          archivedItemBanks: {
            title: "Archived Item Banks"
          }
        }
      },
      testAccesses: {
        title: "Test Access Management",
        description: "Assign and manage test accesses for test administrators.",
        tabs: {
          assignTestAccesses: {
            version: {
              title: "{0} v{1}"
            },
            testDescription: "{0} v{1}",
            title: "Assign Test Accesses",
            testOrderNumberLabel: "Test Order Number:",
            staffingProcessNumber: "Assessment Process Number:",
            departmentMinistry: "Organization Code:",
            isOrg: "FIS Organization Code:",
            isRef: "FIS Reference Code:",
            billingContact: "Billing Contact Name:",
            billingContactInfo: "Contact Email for Candidates:",
            users: "Test Administrator(s):",
            usersCurrentValueAccessibility: "The selected test administrators are:",
            expiryDate: "Expiry Date:",
            testAccesses: "Test Accesses:",
            testAccess: "Test Access:",
            testAccessesError: "Must select at least one test",
            testAccessAlreadyExistsError:
              "{0} has already been granted access to the {1} for Test Order Number {2}.",
            emptyFieldError:
              "This field is empty or you have exceeded the maximum amount of characters allowed for this field",
            mustBeAnEmailError: "Must be a valid email address",
            unableToAccessOrderingServiceError:
              "Unable to connect to the ordering service. Please enter financial data manually.",
            searchButton: "Search",
            manuelEntryButton: "Manual Entry",
            noTestOrderNumberFound: "This order number cannot be found, an exact match is required",
            refreshButton: "Clear All Fields",
            refreshConfirmationPopup: {
              title: "Clear all fields?",
              description: "This action will clear all fields.",
              confirm: "Clear"
            },
            saveButton: "Save",
            saveConfirmationPopup: {
              title: "Confirm Test Access Assignment",
              usersSection: "Names and contact details for users being assigned access:",
              testsSection: "Tests for which rights are assigned:",
              orderInfoSection: {
                title: "Test Order Information:",
                testOrderNumber: "Test Order Number:",
                staffingProcessNumber: "Staffing Process Number:",
                departmentMinistry: "Organization Code:"
              },
              expiriDateSection: "Test Access Expiration Date:",
              confirmation: "Please review and confirm the following Test Access Assignment(s).",
              testAccessesGrantedConfirmationPopup: {
                title: "Confirmation of Test Access Rights",
                description: "Test accesses have been assigned successfully."
              }
            }
          },
          activeTestAccesses: {
            title: "Active Test Accesses",
            table: {
              testAdministrator: "Test Administrator",
              test: "Test",
              orderNumber: "Test Order Number",
              expiryDate: "Expiry Date",
              action: "Action",
              actionButtonLabel: "Modify",
              actionButtonAriaLabel:
                "Modify test access details of user {0} {1}, test {2}, order number {3}, expiry date {4}."
            },
            viewTestPermissionPopup: {
              title: "Modify or Delete Test Access",
              description: `Modify or delete {0}'s test access.`,
              username: "Government of Canada Email:",
              deleteButtonAccessibility: "Delete",
              saveButtonAccessibility: "Modify"
            },
            deleteConfirmationPopup: {
              title: "Confirm Deletion of Test Access",
              systemMessageDescription:
                "This action will remove {0} test access (Test Order Number: {1}) from {2}'s account. Are you sure you want to proceed?"
            },
            saveConfirmationPopup: {
              title: "Confirmation of updates",
              description: "{0}'s test access has been updated successfully."
            }
          },
          orderlessTestAccesses: {
            title: "Orderless Test Accesses",
            addNewOrderlessTaButton: "Add TA",
            addOrderlessTaPopup: {
              title: "Add Test Administrator(s)",
              description:
                "Select the test administrator(s) you would like to add to the list of TAs with orderless test accesses.",
              testAdministrators: "Test Administrator(s):",
              invalidTestAdministrators: '"{0}" already has orderless test access',
              departmentMinistry: "Organization:",
              successfulMessage: "Test administrator(s) have been added successfully."
            },
            confirmDeleteOrderlessTaPopup: {
              title: "Delete Confirmation",
              systemMessageDescription: "All orderless test accesses for {0} will be revoked.",
              description: "Are you sure you want to proceed?"
            },
            table: {
              column_1: "Test Administrator",
              column_2: "Organization",
              column_3: "Actions",
              viewTooltip: "View Details",
              deleteTooltip: "Delete All Test Accesses"
            },
            selectedOrderlessTestAdministrator: {
              backToOrderlessTestAdministratorSelectionButton: "Back to Orderless Test Access",
              tabTitle: "Orderless Test Accesses ({0})",
              sideNavItem1Title: "TA Test Accesses",
              testAccesses: {
                title: "Add/Remove Test Accesses",
                departmentUpdateTitle: "Edit TA's organization",
                departmentLabel: "Organization:",
                description: "Assign or revoke {0} {1} test accesses.",
                table: {
                  column_1: "Test Code",
                  column_2: "Test Name",
                  column_3: "Assigned",
                  noResultsFound: "No test accesses found"
                }
              },
              saveConfirmationPopup: {
                title: "Edit TA's Organization",
                systemMessageDescription: "The TA's organization has been updated successfully."
              }
            }
          }
        }
      },
      testAccessCodes: {
        description: "Displays all active test access codes",
        table: {
          columnOne: "Test Access Code",
          columnTwo: "Test",
          columnThree: "Test Order / Reference Number",
          columnFour: "Test Administrator",
          columnFive: "Date Created",
          columnSix: "Action",
          noTestAccessCode: "There are no test access codes",
          deleteButtonAccessibility: "Delete {0}"
        }
      },
      activeTests: {
        description: "Displays all active candidates",
        table: {
          columnOne: "Candidate Name",
          columnTwo: "Test Administrator",
          columnThree: "Test Form",
          columnFour: "Test Order / Reference Number:",
          columnFive: "Status",
          columnSix: "Time Limit",
          columnSeven: "Action",
          noActiveCandidates: "There are no active candidates.",
          actionButton: "Invalidate",
          invalidateButtonAccessibility: "Invalidate {0}"
        },
        invalidatePopup: {
          title: "Invalidate Test Access",
          description: "Are you sure you want to invalidate {0} {1}'s access to the test?",
          actionButton: "Invalidate Access",
          reasonForInvalidatingTheTest: "Reason for invalidating test access:"
        },
        validatePopup: {
          title: "Restore test access",
          description: "Are you sure you want to restore {0} {1}'s access to the test?",
          actionButton: "Restore Acess",
          reasonForInvalidatingTheTest: "Reason for restoring the test access:"
        }
      },
      userLookUp: {
        description: "Displays list of all users",
        table: {
          columnOne: "Email",
          columnTwo: "First Name",
          columnThree: "Last Name",
          columnFour: "Date of Birth",
          columnFive: "Action",
          viewButton: "View",
          noUsers: "There are no active users."
        },
        viewButtonAccessibility: "View {0} {1}"
      },
      userLookUpDetails: {
        sideNavigationItems: {
          userPersonalInfo: "Personal Information",
          myTests: "Tests",
          rightsAndPermissions: "Rights and Permissions",
          testPermissions: "Test Permissions"
        },
        backToUserLookUp: "Back to User Look-Up results",
        containerLabel: "User Details ({0} {1})",
        testPermissions: {
          title: "Test Access Permissions",
          description: "As a Test Administrator, {0} {1} has access to the following tests:",
          noTestPermissions: "The user does not have any test access permissions."
        },
        userRights: {
          title: "Rights and Permissions",
          description: "Displays {0} {1}'s rights",
          noRights: "The user does not have any rights."
        },
        Tests: {
          title: "View {0} {1}'s Tests",
          description: "Displays all the tests taken by {0} {1}",
          table: {
            columnOne: "Test",
            columnTwo: "Test Date",
            columnThree: "Retest possible as of",
            columnFour: "Score",
            columnFive: "Status",
            columnSix: "Invalidated",
            columnSeven: "Action",
            noTests: "The user does not have any tests. ",
            viewTestDetailsTooltip: "View Test Details",
            invalidateTestTooltip: "Invalidate Test",
            validateTestTooltip: "Cancel the invalidation",
            invalidateButton: "Invalidate",
            viewButtonAccessibility: "View {0}",
            invalidateButtonAccessibility: "Invalidate {0}",
            validateButtonAccessibility: "Cancel the Invalidation of the {0}",
            downloadCandidateActionReport: "Generate Candidate Actions Report",
            candidateActionReportAria: "Generate Candidate Actions Report for {0}"
          },
          viewSelectedTestPopup: {
            title: "View Test Details",
            description: "Displays all the details of the test",
            testName: "Test Name:",
            testCode: "Test Code:",
            testAccessCode: "Test Access Code:",
            testOrderNumber: "Test Order Number:",
            assessmentProcessOrReferenceNb: "Assessment Process / Reference Number:",
            testAdministrator: "Test Administrator:",
            totalScore: "Total Score:",
            version: "CAT Version:",
            invalidTestReason: "Reason for Invalid Test:"
          }
        },
        userPersonalInfo: {
          title: "Personal Information",
          firstName: "First Name:",
          lastName: "Last Name:",
          primaryEmailAddress: "Primary Email Address:",
          secondaryEmailAddress: "Secondary Email Address:",
          dateOfBirth: "Date of Birth:",
          psrsApplicantId: "PSRS Applicant ID:",
          pri: "PRI or Service Number:",
          lastLogin: "Last Login:",
          lastPasswordChange: "Last Password Change:",
          accountCreationDate: "Account Creation Date:",
          additionalInfo: "Additional Information",
          currentEmployer: "Current Employer:",
          organization: "Organization:",
          group: "Group:",
          subGroup: "Subgroup:",
          level: "Level:",
          residence: "Residence:",
          education: "Education:",
          gender: "Gender:",
          noPersonalInformation: "There is no personal information on the user.",
          noAdditionalInfomration: "There is no additional information on the user."
        }
      },
      reports: {
        title: "Reports",
        description: "Generate a results report for a Test Order you administered."
      },
      systemAlerts: {
        title: "System Alerts",
        description: "Manage System Alerts",
        tabs: {
          activeSystemAlerts: {
            title: "Active System Alerts",
            addNewSystemAlertButton: "Add System Alert",
            addSystemAlertPopup: {
              title: "Add System Alert",
              fields: {
                title: "Title:",
                criticality: "Criticality:",
                startDate: "Start Date:",
                endDate: "End Date:",
                messageText: "Message Text:",
                messageTextTabs: {
                  english: "English",
                  french: "French"
                }
              },
              buttons: {
                rightButton: "Add"
              },
              successfulMessage: "System Alert has been added successfully."
            },
            archiveSystemAlertPopup: {
              title: "Archive System Alert",
              systemMessageTitle: "Archive this system alert?",
              systemMessageMessage:
                "Archiving this system alert will remove it from the login page, and move it to the archived System Alerts.",
              archiveSuccessfullyMessage: "System Alert has been archived successfully.",
              archiveRightButton: "Archive"
            },
            table: {
              column_1: "Alert",
              column_2: "Criticality",
              column_3: "Active Date",
              column_4: "End Date",
              column_5: "Actions",
              viewTooltip: "View",
              modifyTooltip: "Modify",
              archiveToolTip: "Archive"
            },
            viewPopupTitle: "View System Alert",
            modifyPopupTitle: "Modify System Alert",
            modifyRightButton: "Modify",
            modifySuccessfullyMessage: "System Alert has been modified successfully."
          },
          archivedSystemAlerts: {
            title: "Archived System Alerts",
            table: {
              column_1: "Alert",
              column_2: "Criticality",
              column_3: "Active Date",
              column_4: "End Date",
              column_5: "Actions",
              viewTooltip: "View",
              cloneTooltip: "Clone"
            },
            viewPopupTitle: "View System Alert",
            clonePopupTitle: "Clone System Alert",
            cloneSuccessfullyMessage: "System Alert has been cloned successfully."
          }
        }
      }
    },

    // Scorer Pages
    scorer: {
      situationTitle: "Situation",
      assignedTest: {
        displayOptionLabel: "Display:",
        displayOptionAccessibility: "Number of results per page",
        displayOptionCurrentValueAccessibility: "The selected value is:",
        table: {
          assignedTestId: "Assigned Test ID",
          completionDate: "Completion Date",
          testName: "Test Name",
          testLanguage: "Test Language",
          action: "Action",
          actionButtonLabel: "Score Test",
          en: "English",
          fr: "French",
          actionButtonAriaLabel:
            "Score test of: assigned test id: {0}, completion date: {1}, test name: {2}, test language: {3}."
        },
        noResultsFound: "There are no tests waiting to be scored."
      },
      sidebar: {
        title: "Scoring Panel",
        competency: "Competency",
        rational: "Rating rationale",
        placeholder: "Input rationale here..."
      },
      submitButton: "Submit Final Scores",
      unsupportedScoringType:
        "The scoring type: {0} is not supported. The test definition must be incorrect.",
      selectQuestion: "Select a question to continue",
      competency: {
        bar: {
          title: "Behavioural Achored Ratings",
          description:
            "Below is a complete view of the BAR for this question and competency. Once selected, you can use the arrows to navigate, expand, and collapse information.",
          barButton: "View BAR"
        }
      }
    },

    // Status Page
    statusPage: {
      title: "CAT Status",
      logo: "Thunder CAT Logo",
      welcomeMsg:
        "Internal status page to quickly determine the status / health of the Candidate Assessment Tool.",
      versionMsg: "Candidate Assessment Tool Version: ",
      gitHubRepoBtn: "GitHub Repository",
      serviceStatusTable: {
        title: "Service Status",
        frontendDesc: "Front end application built and serving successfully",
        backendDesc: "Back end application completing API requests successfully",
        databaseDesc: "Database completing API requests sccessfully"
      },
      systemStatusTable: {
        title: "System Status",
        javaScript: "JavaScript",
        browsers: "Chrome, Firefox, Edge",
        screenResolution: "Screen resolution minimum of 1024 x 768"
      },
      additionalRequirements:
        "Additionally, the following requirements must be met to use this application in a test centre.",
      secureSockets: "Secure Socket Layer (SSL) encryption enabled",
      fullScreen: "Full-screen mode enabled",
      copyPaste: "Copy + paste functionality enabled"
    },

    // Settings Dialog
    settings: {
      systemSettings: "Accessibility Display Settings",
      zoom: {
        title: "Zoom (+/-)",
        description:
          "Use your browser settings to change the zoom level. Alternatively, you can hold down CTRL and the + / - keys on your keyboard to zoom in or out."
      },
      fontSize: {
        title: "Font Size",
        label: "Font Size:",
        description: "Select the size of your text:"
      },
      fontStyle: {
        title: "Font Style",
        label: "Font Style:",
        description: "Select a new font for your text:"
      },
      lineSpacing: {
        title: "Text Spacing",
        label: "Text Spacing:",
        description: "Turn on accessibility text spacing."
      },
      color: {
        title: "Text and background colour",
        description: "Use your browser settings to select the text and background colour."
      }
    },

    // Multiple Choice  test
    mcTest: {
      questionList: {
        questionIdShort: "Q{0}",
        questionIdLong: "Question {0}",
        reviewButton: "Mark for Review",
        markedReviewButton: "Unmark for Review",
        seenQuestion: "This question has been viewed",
        unseenQuestion: "This question has not been viewed",
        answeredQuestion: "This question has been answered",
        unansweredQuestion: "This question has not been answered",
        reviewQuestion: "This question has been marked for review."
      },
      finishPage: {
        homeButton: "Return to Home Page"
      }
    },
    // eMIB Test
    emibTest: {
      // Home Page
      homePage: {
        testTitle: "Sample e-MIB",
        welcomeMsg: "Welcome to the eMIB Sample Test"
      },

      // HowTo Page
      howToPage: {
        tipsOnTest: {
          title: "Tips on taking the e-MIB",
          part1: {
            description:
              "The e-MIB presents you with situations that will give you the opportunity to demonstrate the Key Leadership Competencies. Here are some tips that will help you provide assessors with the information they need to evaluate your performance on the Key Leadership Competencies:",
            bullet1:
              "Answer all the questions asked in the emails you received. Also, note that there may be situations that span several emails and for which no specific questions are asked, but that you can respond to. This will ensure that you take advantage of all the opportunities provided to demonstrate the competencies.",
            bullet2:
              "Don’t hesitate to provide your recommendations where appropriate, even if they are only initial thoughts.  If needed, you can then note other information you would need to reach a final decision.",
            bullet3:
              "For situations in which you feel you need to speak with someone in person before you can make a decision, you should indicate what information you need and how this would affect your decision one way or the other.",
            bullet4:
              "Use only the information provided in the emails and the background information. Do not make any inferences based on the culture of your own organization. Avoid making assumptions that are not reasonably supported by either the background information or the emails.",
            bullet5:
              "The e-MIB is set within a specific industry in order to provide enough context for the situations presented. However, effective responses rely on the competency targeted and not on specific knowledge of the industry."
          },
          part2: {
            title: "Other important information",
            bullet1:
              "You will be assessed on the content of your emails, the information provided in your task list and the reasons listed for your actions. Information left on your notepad will not be evaluated.",
            bullet2:
              "You will not be assessed on how you write. No points will be deducted for spelling, grammar, punctuation or incomplete sentences. However, your writing will need to be clear enough to ensure that the assessors understand which situation you are responding to and your main points.",
            bullet3: "You can answer the emails in any order you want.",
            bullet4: "You are responsible for managing your own time."
          }
        },
        testInstructions: {
          title: "Test instructions",
          hiddenTabNameComplementary: 'Under "Instructions"',
          onlyOneTabAvailableForNowMsg:
            "Note that there is only one main tab available for now. As soon as you start the test, the other main tabs will become available.",
          para1:
            "When you start the test, first read the background information which describes your position and the fictitious organization you work for. We recommend you take approximately 10 minutes to read it. Then proceed to the in-box where you can read the emails you received and take action to respond to them as though you were a manager within the fictitious organization.",
          para2: "While you are in the email in-box, you will have access to the following:",
          bullet1: "The test instructions;",
          bullet2:
            "The background information describing your job as the manager and the fictitious organization where you work;",
          bullet3: "A notepad to serve as scrap paper.",
          step1Section: {
            title: "Step 1 - Responding to emails",
            description:
              "You can respond to the emails you received in two ways: by writing an email or by adding tasks to your task list. A description of both ways of responding is presented below, followed by examples.",
            part1: {
              title: "Example of an email you have received:",
              para1:
                "Two options are provided below to demonstrate different ways of responding to the email. You can choose one of the two options presented or a combination of the two. The responses are only provided to illustrate how to use each of the two ways of responding. They do not necessarily demonstrate the Key Leadership Competencies assessed in this situation."
            },
            part2: {
              title: "Responding with an email response",
              para1:
                "You can write an email in response to one you received in your in-box. The written response should reflect how you would respond as a manager.",
              para2:
                "You can use the following options: reply, reply all or forward. If you choose to forward an email, you will have access to a directory with all of your contacts. You can write as many emails as you like to respond to an email or to manage situations you identified across several of the emails you received."
            },
            part3: {
              title: "Example of responding with an email response:"
            },
            part4: {
              title: "Adding a task to the task list",
              para1:
                "In addition to, or instead of, responding by email, you can add tasks to the task list. A task is an action that you intend to take to address a situation presented in the emails. For example, tasks could include planning a meeting or asking a colleague for information. You should provide enough information in your task description to ensure it is clear which situation you are addressing. You should also specify what actions you plan to take, and with whom. You can return to the email to add, delete or edit tasks at any time."
            },
            part5: {
              title: "Example of adding a task to the task list:"
            },
            part6: {
              title: "How to choose a way of responding",
              para1:
                "There are no right or wrong ways to respond. When responding to an email, you can:",
              bullet1: "send an email or emails; or",
              bullet2: "add a task or tasks to your task list; or",
              bullet3: "do both.",
              para2:
                "You will be assessed on the content of your responses, and not on your method of responding (i.e. whether you responded by email and/or by adding a task to your task list). As such, answers need to be detailed and clear enough for assessors to evaluate how you are addressing the situation. For example, if you plan to schedule a meeting, you will need to specify what will be discussed at that meeting.",
              para3Part1:
                "When responding to an email you received, if you decide to write an email ",
              para3Part2: "and",
              para3Part3:
                " to add a task to your task list, you do not need to repeat the same information in both places. For example, if you mention in an email that you will schedule a meeting with a co-worker, you do not need to add that meeting to your task list."
            }
          },
          step2Section: {
            title: "Step 2 - Adding reasons for your actions (optional)",
            description:
              "After writing an email or adding a task, if you feel the need to explain why you took a specific action, you can write it in the reasons for your actions section, located below your email response or tasks. Providing reasons for your actions is optional. Note that you may choose to explain some actions and not others if they do not require any additional explanations. Similarly, you may decide to add the reasons for your actions when responding to some emails and not to others. This also applies to tasks in the task list.",
            part1: {
              title: "Example of an email response with reasons for your actions:"
            },
            part2: {
              title: "Example of a task list with reasons for your actions:"
            }
          },
          exampleEmail: {
            to: "T.C. Bernier (Manager, Quality Assurance Team)",
            from: "Geneviève Bédard (Director, Research and Innovation Unit)",
            subject: "Preparing Mary for her assignment",
            date: "Friday, November 4",
            body: "Hello T.C.,\n\nI was pleased to hear that one of your quality assurance analysts, Mary Woodside, has accepted a six-month assignment with my team, starting on January 2. I understand she has experience in teaching and using modern teaching tools from her previous work as a college professor. My team needs help developing innovative teaching techniques that promote employee productivity and general well-being. Therefore, I think Mary’s experience will be a good asset for the team.\n\nAre there any areas in which you might want Mary to gain more experience and which could be of value when she returns to your team? I want to maximize the benefits of the assignment for both our teams.\n\nLooking forward to getting your input,\n\nGeneviève"
          },
          exampleEmailResponse: {
            emailBody:
              "Hi Geneviève,\n\nI agree that we should plan Mary’s assignment so both our teams can benefit from it. I suggest that we train Mary in the synthesis of data from multiple sources. Doing so could help her broaden her skill set and would be beneficial to my team when she comes back. Likewise, your team members could benefit from her past experience in teaching. I’ll consult her directly as I would like to have her input on this. I’ll get back to you later this week once I have more information to provide you on this matter.\n\nThat being said, what are your expectations? Are there any current challenges or specific team dynamics that should be taken into account? I’d like to consider all factors, such as the current needs of your team, challenges and team dynamics before I meet with Mary to discuss her assignment.\n\nThanks,\n\nT.C.",
            reasonsForAction:
              "I am planning to meet with Mary to discuss her expectations regarding the assignment and to set clear objectives. I want to ensure she feels engaged and knows what is expected of her, to help her prepare accordingly. I will also check what her objectives were for the year to ensure that what I propose is consistent with her professional development plan."
          },
          exampleTaskResponse: {
            task: "- Reply to Geneviève’s email:\n     > Suggest training Mary in the synthesis of information from multiple sources so that she can broaden her skill set.\n     > Ask what her expectations are and what the challenges are on her team so I can consider all factors when determining how her team could benefit from Mary’s experience in providing training.\n     > Inform her that I will get more information from Mary and will respond with suggestions by the end of the week.\n- Schedule a meeting with Mary to discuss her assignment objectives and ensure she feels engaged and knows what is expected of her.\n- Refer to Mary’s past and current objectives to ensure that what I propose is in line with her professional development plan.",
            reasonsForAction:
              "Training Mary in the synthesis of information from multiple sources would be beneficial to my team which needs to consolidate information gathered from many sources. Asking Geneviève what her expectations and challenges are will help me better prepare Mary and ensure that the assignment is beneficial to both our teams."
          }
        },
        evaluation: {
          title: "Evaluation",
          bullet1:
            "Both the actions you take and the explanations you give will be considered when assessing your performance on each of the Key Leadership Competencies (described below). You will be assessed on the extent to which they demonstrate the Key Leadership Competencies.",
          bullet2:
            "Your actions will be evaluated on effectiveness. Effectiveness is measured by whether your actions would have a positive or a negative impact on resolving the situations presented and by how widespread that impact would be.",
          bullet3:
            "Your responses will also be evaluated on how well they meet the organizational objectives presented in the background information.",
          keyLeadershipCompetenciesSection: {
            title: "Key Leadership Competencies",
            para1Title: "Create Vision and Strategy: ",
            para1:
              "Managers help to define the future and chart a path forward. To do so, they take into account context. They leverage their knowledge and seek and integrate information from diverse sources to implement concrete activities. They consider different perspectives and consult as needed. Managers balance organizational priorities and improve outcomes.",
            para2Title: "Mobilize People: ",
            para2:
              "Managers inspire and motivate the people they lead. They manage the performance of their employees and provide constructive and respectful feedback to encourage and enable performance excellence. They lead by example, setting goals for themselves that are more demanding than those that they set for others.",
            para3Title: "Uphold Integrity and Respect: ",
            para3:
              "Managers exemplify ethical practices, professionalism and personal integrity, acting in the interest of Canada and Canadians. They create respectful, inclusive and trusting work environments where sound advice is valued. They encourage the expression of diverse opinions and perspectives, while fostering collegiality.",
            para4Title: "Collaborate with Partners and Stakeholders: ",
            para4:
              "Managers are deliberate and resourceful about seeking a wide spectrum of perspectives. In building partnerships, they manage expectations and aim to reach consensus. They demonstrate openness and flexibility to improve outcomes and bring a whole-of-organization perspective to their interactions. Managers acknowledge the role of partners in achieving outcomes.",
            para5Title: "Promote Innovation and Guide Change: ",
            para5:
              "Managers create an environment that supports bold thinking, experimentation and intelligent risk taking. When implementing change, managers maintain momentum, address resistance and anticipate consequences. They use setbacks as a valuable source of insight and learning.",
            para6Title: "Achieve Results: ",
            para6:
              "Managers ensure that they meet team objectives by managing resources. They anticipate, plan, monitor progress and adjust as needed. They demonstrate awareness of the context when making decisions. Managers take personal responsibility for their actions and the outcomes of their decisions."
          }
        }
      },

      // Background Page
      background: {
        hiddenTabNameComplementary: 'under "Background Information"',
        orgCharts: {
          link: "Image Description",
          ariaLabel: "Image description of the organization chart",
          treeViewInstructions:
            "Below is a tree view of the organization chart. Once selected, you can use the arrow keys to navigation, expand, and collapse information."
        }
      },

      // Inbox Page
      inboxPage: {
        tabName: 'under "In-Box"',
        emailId: " email ",
        subject: "Subject",
        to: "To",
        from: "From",
        date: "Date",
        addReply: "Add email response",
        addTask: "Add task list",
        yourActions: `You responded with {0} emails and {1} tasks`,
        editActionDialog: {
          addEmail: "Add email response",
          editEmail: "Edit email response",
          addTask: "Add task list",
          editTask: "Edit task",
          save: "Save response"
        },
        characterLimitReached: "(Limit reached)",
        emailCommons: {
          to: "To:",
          toFieldSelected: "To field selected.",
          cc: "CC:",
          ccFieldSelected: "Cc field selected.",
          currentSelectedPeople: "Current selected people are: {0}",
          currentSelectedPeopleAreNone: "None",
          reply: "Reply",
          replyAll: "Reply all",
          forward: "Forward",
          editButton: "Edit response",
          deleteButton: "Delete response",
          originalEmail: "Original email",
          toAndCcFieldsPlaceholder: "Select from address book",
          yourResponse: "Your response"
        },
        addEmailResponse: {
          selectResponseType: "Please select how you would like to respond to the original email:",
          response: "Your email response: {0} character limit",
          reasonsForAction: "Your reasons for actions (optional): {0} character limit",
          emailResponseTooltip: "Write a response to the email you received.",
          reasonsForActionTooltip:
            "Here, you can explain why you took a specific action in response to a situation if you feel you need to provide additional information",
          invalidToFieldError: "This field cannot be empty"
        },
        emailResponse: {
          title: "Email Response #",
          description: "For this response, you've chosen to:",
          response: "Your email response:",
          reasonsForAction: "Your reasons for action (optional):"
        },
        addEmailTask: {
          header: "Email #{0}: {1}",
          task: "Your task list response: {0} character limit",
          reasonsForAction: "Your reasons for actions (optional): {0} character limit"
        },
        taskContent: {
          title: "Task List Response #",
          task: "Your task list response:",
          taskTooltipPart1: "An action you intend to take to address a situation in the emails.",
          taskTooltipPart2: "Example: Planning a meeting, asking a colleague for information.",
          reasonsForAction: "Your reasons for action:",
          reasonsForActionTooltip:
            "Here, you can explain why you took a specific action in response to a situation if you feel you need to provide additional information"
        },
        deleteResponseConfirmation: {
          title: "Are you sure you want to cancel this response?",
          systemMessageDescription:
            "Your response will not be saved if you proceed. If you wish to save your answer, you may return to the response. All of your responses can be edited or deleted before submission.",
          description:
            'If you do not wish to save the response, click the "Delete response" button.'
        }
      },

      // Confirmation Page
      confirmationPage: {
        submissionConfirmedTitle: "Congratulations! Your test has been submitted.",
        feedbackSurvey:
          "We would appreciate your feedback on the assessment. Please fill out this optional {0} before logging out and leaving.",
        optionalSurvey: "15 minute survey",
        surveyLink: "https://surveys-sondages.psc-cfp.gc.ca/s/se.ashx?s=25113745259E9113&c=en-US",
        logout:
          "For security reasons, please ensure you log out of your account in the top right corner of this page. You may quietly gather your belongings and leave the test session. If you have any questions or concerns about your test, please contact {0}.",
        thankYou: "Thank you for completing your assessment. Good luck!"
      },

      // Quit Confirmation Page
      quitConfirmationPage: {
        title: "You have quit the test",
        instructionsTestNotScored1: "Your test ",
        instructionsTestNotScored2: "will not be scored.",
        instructionsRaiseHand:
          "Please raise your hand. The test administrator will come see you with further instructions.",
        instructionsEmail:
          "If you have any questions or concerns about your test, please contact {0}."
      },

      // Timeout Page
      timeoutConfirmation: {
        timeoutConfirmedTitle: "Your test time is over...",
        timeoutSaved:
          "Your responses have been saved and submitted for scoring. Please note that the information in the notepad is not saved.",
        timeoutIssue:
          'If there was an issue, please advise your test administrator. Click "Continue" to exit this test session and to receive further instructions.'
      },

      // Test tabs
      tabs: {
        instructionsTabTitle: "Instructions",
        backgroundTabTitle: "Background Information",
        inboxTabTitle: "In-Box",
        disabled: "You cannot access the inbox until you start the test."
      },

      // Test Footer
      testFooter: {
        timer: {
          timer: "Timer",
          timeLeft: "Time left in test session:",
          timerHidden: "Timer hidden.",
          timerHide: "Hide timer",
          timeRemaining: "Time remaining: {0} Hours, {1} Minutes, {2} Seconds"
        },
        pauseButton: "Pause Test",
        unpauseButton: "Resume Test",
        unpausePopup: {
          title: "Resume Test",
          description1: "Are you sure you want to resume your test?",
          description2:
            "The remaining time in your break bank will be re-calculated when you select Resume Test.",
          unpauseButton: "Resume Test"
        },
        submitTestPopupBox: {
          title: "Submit test",
          warning: {
            title: "Warning! The notepad will not be saved.",
            message1:
              "This will end your test session. Before submitting your answers, ensure that you have answered all the questions as you will not be able to go back to make changes. Anything written in the notepad will not be submitted with the test for scoring.",
            message2: "Do you want to proceed?"
          },
          description:
            "If you are ready to send your test in for scoring, click the “Submit Test” button. You will be exited out of this test session and provided further instructions."
        },
        quitTestPopupBox: {
          title: "Quit Test",
          description1:
            "Are you sure you want to quit this test? All answers will be deleted. You will not be able to recover your answers, and will lose access to the test.",
          description2: "To quit, you must acknowledge the following:",
          checkboxOne: "I choose to quit this test",
          checkboxTwo: "I understand that my test will not be scored",
          checkboxThree:
            "I am aware that there may be a delay before being retested, should I wish to take this test again"
        }
      }
    },

    // Screen Reader
    ariaLabel: {
      mainMenu: "Main Menu",
      tabMenu: "eMIB Tab Menu",
      instructionsMenu: "Instructions Menu",
      languageToggleBtn: "language-toggle-button",
      authenticationMenu: "Authentication Menu",
      emailHeader: "email header",
      responseDetails: "response details",
      reasonsForActionDetails: "reasons for action details",
      taskDetails: "task details",
      emailOptions: "email options",
      taskOptions: "task options",
      taskTooltip: "task tooltip",
      emailResponseTooltip: "email response tooltip",
      reasonsForActionTooltip: "reasons for action tooltip",
      passwordConfirmationRequirements: "It must match your password",
      dobDayField: "Day field selected",
      dobMonthField: "Month field selected",
      dobYearField: "Year field selected",
      emailsList: "Emails list",
      questionList: "Question list",
      topNavigationSection: "Top navigation",
      sideNavigationSection: "Side navigation menu",
      quitTest: "Quit Test",
      selectedPermission: "Selected permission:",
      description: "Description:",
      pendingStatus: "Status pending:",
      scorerPanel: "Scorer Panel",
      navExpandButton: "Toggle navigation"
    },

    // Commons
    commons: {
      psc: "Public Service Commission",
      loading: "Loading",
      nextButton: "Next",
      backButton: "Back",
      enterEmibSample: "Proceed to sample e-MIB test",
      enterEmibReal: "Continue to test instructions",
      resumeEmibReal: "Resume e-MIB",
      startTest: "Start test",
      resumeTest: "Resume test",
      confirmStartTest: {
        aboutToStart: "You are about to start the test.",
        breakBankTitle: "Break Bank:",
        breakBankWarning: "You have a {0} break bank for this test.",
        testSectionTimeTitle: "Total Time:",
        newtimerWarning: "You will have a total of {0} to complete the test.",
        additionalTimeWarning: "This includes {0} of additional time.",
        wishToProceed: "Do you wish to proceed?",
        timeUnlimited: "unlimited time",
        confirmProceed: "Yes, I wish to proceed."
      },
      submitTestButton: "Submit test",
      quitTest: "Quit Test",
      returnToTest: "Return to test",
      returnToResponse: "Return to response",
      passStatus: "Pass",
      failStatus: "Fail",
      error: "Error",
      success: "Success",
      info: "Information",
      warning: "Warning!",
      enabled: "Enabled",
      disabled: "Disabled",
      backToTop: "Back to top",
      notepad: {
        title: "Notepad",
        hideNotepad: "Hide notepad",
        placeholder: "Put your notes here..."
      },
      calculator: {
        title: "Calculator",
        errorMessage: "Error",
        calculatorResultAccessibility: "Calculator diplays",
        backspaceButton: "Backspace",
        hideCalculator: "Hide calculator"
      },
      cancel: "Cancel",
      cancelChanges: "Cancel changes",
      cancelResponse: "Cancel response",
      addButton: "Add",
      deleteButton: "Delete",
      saveButton: "Save",
      saveDraftButton: "Save",
      applyButton: "Apply",
      duplicateButton: "Duplicate",
      previewButton: "Preview",
      close: "Close",
      login: "Login",
      ok: "OK",
      deleteConfirmation: "Confirm Deletion",
      continue: "Continue",
      sendRequest: "Send request",
      submit: "Submit",
      na: "N/A",
      valid: "Valid",
      invalid: "Invalidated",
      confirm: "Confirm",
      english: "English",
      french: "French",
      bilingual: "Bilingual",
      status: {
        checkedIn: "Checked-in",
        ready: "Ready",
        preTest: "Pre-Test",
        active: "Testing",
        locked: "Locked",
        paused: "Paused",
        neverStarted: "Not Started Yet",
        inactivity: "Inactivity",
        timedOut: "Timed Out",
        quit: "Quit",
        submitted: "Submitted",
        unassigned: "Unassigned",
        invalid: "Invalid"
      },
      seconds: "second(s)",
      minutes: "minute(s)",
      hours: "hour(s)",
      pleaseSelect: "Please select",
      none: "None",
      convertedScore: {
        invalidScoreConversion: "Invalid Score Conversion",
        pass: "Pass",
        fail: "Fail",
        none: "No Score",
        notScored: "Not Scored",
        invalid: "Invalid"
      },
      yes: "Yes",
      no: "No",
      preferNotToAnswer: "Prefer not to say",
      active: "Active",
      inactive: "Inactive",
      moveUp: "Move Up",
      moveDown: "Move Down",
      default: "Default",
      tools: "Tools",
      pagination: {
        nextPageButton: "Next page",
        previousPageButton: "Previous page",
        skipToPagesLink: "Skip to list of pages",
        breakAriaLabel: "Move multiple pages"
      },
      saved: "Saved",
      fieldCannotBeEmptyErrorMessage: "This field cannot be empty",
      currentLanguage: "en",
      true: "TRUE",
      false: "FALSE",
      next: "Next",
      previous: "Previous",
      upload: "Upload",
      all: "All",
      generate: "Regenerate",
      unchanged: "(unchanged)",
      discard: "Discard"
    }
  },

  fr: {
    // Main Tabs
    mainTabs: {
      homeTabTitleUnauthenticated: "Accueil",
      homeTabTitleAuthenticated: "Accueil",
      dashboardTabTitle: "Tableau de bord",
      sampleTest: "Échantillon de la BRG-e",
      sampleTests: "Exemples de tests",
      statusTabTitle: "Statut",
      psc: "Commission de la fonction publique du Canada",
      canada: "Gouvernement du canada",
      skipToMain: "Passer au contenu principal",
      menu: "MENU"
    },

    // HTML Page Titles
    titles: {
      CAT: "OÉC/CAT - CFP/PSC",
      sampleTests: "OÉC - Exemples de tests",
      eMIBOverview: "Aperçu de la BRG-e",
      eMIB: "Évaluation BRG-e ",
      uitTest: "OÉC - Test\u00A0: {0}",
      status: "OÉC - État du system",
      home: "OÉC - Accueil",
      homeWithError: "Erreur OÉC - Accueil",
      profile: "OÉC - Profil",
      systemAdministration: "OÉC - Activités d’affaires",
      rdOperations: "OÉC - Activités R&D",
      systemAdminAndRdOperations: "OÉC - Activités ",
      testAdministration: "OÉC - Administrateur de tests",
      testBuilder: "OÉC - Constructeur de tests",
      itemBankPermissions: "OÉC - Accès à la banque d'items",
      itemBankEditor: "OÉC - Éditeur de banque d'items",
      itemEditor: "OÉC - Éditeur d'items",
      ppcAdministration: "OÉC - Tableau de bord de R&D",
      resetPassword: "OÉC - Réinitialiser le mot de passe"
    },

    // Site Nav Bar
    siteNavBar: {
      settings: "Affichage"
    },

    accommodations: {
      notificationPopup: {
        title: "Paramètres d'affichage pour l'accessibilité",
        description1: "Les paramètres d'affichage utilisés actuellement ont été sauvegardés.",
        description2:
          "Lors de votre prochaine connexion, ces paramètres seront automatiquement appliqués."
      },
      defaultFonts: {
        fontSize: "16px (défaut)",
        fontFamily: "Nunito Sans (défaut)"
      }
    },

    // authentication
    authentication: {
      login: {
        title: "Ouvrir une session",
        content: {
          title: "Ouvrir une session",
          inputs: {
            emailTitle: "Adresse courriel\u00A0:",
            passwordTitle: "Mot de passe\u00A0:",
            showPassword: "Afficher le mot de passe"
          }
        },
        button: "Ouvrir une session",
        forgotPassword: "Avez-vous oublié votre mot de passe ?",
        forgotPasswordPopup: {
          title: "Avez-vous oublié votre mot de passe ?",
          description:
            "Veuillez inscrire l’adresse courriel du compte qui nécessite une réinitialisation du mot de passe.",
          emailAddressLabel: "Adresse courriel\u00A0:",
          sendResetLinkButton: "Envoyer un lien pour réinitialiser le mot de passe",
          invalidEmailAddressErrorMessage: "Doit être une adresse courriel valide",
          usernameDoesNotExistErrorMessage:
            "L'adresse courriel fournie ne correspond pas à un compte actif",
          emailSentSuccessfully: "Un courriel a été envoyé à cette adresse",
          emailSentSuccessfullMessage:
            "Si vous n'avez pas reçu le courriel, vérifiez l’adresse courriel et cliquez sur le bouton «\u00A0Renvoyer le courriel\u00A0» ci-dessous.",
          resendEmail: "Renvoyer le courriel"
        },
        invalidCredentials: "Adresse courriel et/ou mot de passe invalide",
        passwordFieldSelected: "Champ Mot de passe sélectionné"
      },
      createAccount: {
        title: "Créer un compte",
        content: {
          title: "Créer un compte",
          description: "Veuillez remplir le formulaire suivant pour créer un compte.",
          inputs: {
            valid: "Valide",
            firstNameTitle: "Prénom\u00A0:",
            firstNameError: "Le prénom contient un ou plusieurs caractères invalides",
            lastNameTitle: "Nom de famille\u00A0:",
            lastNameError: "Le nom de famille contient un ou plusieurs caractères invalides",
            dobDayTitle: "Date de naissance\u00A0:",
            dobError: "Doit être une date valide",
            psrsAppIdTitle: "Numéro d'identification du SRFP (s'il y a lieu)\u00A0:",
            psrsAppIdError: "Doit être un numéro d'identification du SRFP valide",
            emailTitle: "Adresse courriel\u00A0:",
            emailError: "Doit être une adresse courriel valide",
            priTitle: "CIDP (s'il y a lieu)\u00A0:",
            militaryNbrTitle: "Numéro de matricule (s'il y a lieu)\u00A0:",
            priError: "Doit être un CIDP valide",
            militaryNbrError: "Doit être un numéro de matricule valide",
            passwordTitle: "Mot de passe\u00A0:",
            showPassword: "Afficher le mot de passe",
            passwordErrors: {
              description: "Votre mot de passe doit contenir les éléments suivants\u00A0:",
              upperCase: "Au moins une lettre majuscule",
              lowerCase: "Au moins une lettre minuscule",
              digit: "Au moins un chiffre",
              specialCharacter: "Au moins un caractère spécial (#?!@$%^&*-)",
              length: "Un minimum de 8 caractères et un maximum de 15 caractères"
            },
            passwordConfirmationTitle: "Confirmer le mot de passe\u00A0:",
            showPasswordConfirmation: "Afficher la confirmation du mot de passe",
            passwordConfirmationError:
              "La confirmation de votre mot de passe doit correspondre à votre nouveau mot de passe"
          }
        },
        privacyNotice:
          "J'ai lu {0} et j'accepte la manière dont la Commission de la fonction publique recueille, utilise et communique les renseignements personnels.",
        privacyNoticeLink: "l'énoncé de confidentialité",
        privacyNoticeError: "Vous devez accepter l’avis de confidentialité en cliquant sur la case",
        button: "Créer un compte",
        accountAlreadyExistsError: "Un compte est déjà associé à cette adresse de courriel",
        passwordTooCommonError: "Ce mot de passe est trop commun",
        passwordTooSimilarToUsernameError:
          "Le mot de passe est trop semblable au nom d’utilisateur",
        passwordTooSimilarToFirstNameError: "Le mot de passe est trop semblable au prénom",
        passwordTooSimilarToLastNameError: "Le mot de passe est trop semblable au nom de famille",
        passwordTooSimilarToEmailError: "Le mot de passe est trop semblable au courriel"
      },
      resetPassword: {
        title: "Réinitialiser le mot de passe",
        content: {
          title: "Réinitialiser le mot de passe",
          newPassword: "Nouveau mot de passe\u00A0:",
          showNewPassword: "Afficher le nouveau mot de passe",
          confirmNewPassword: "Confirmer le nouveau mot de passe\u00A0:",
          showConfirmNewPassword: "Afficher la confirmation du nouveau mot de passe",
          button: "Réinitialiser le mot de passe"
        },
        popUpBox: {
          title: "Réinitialisation du mot de passe réussie",
          description: "Cliquez sur «\u00A0OK\u00A0» pour ouvrir une session."
        }
      }
    },

    // Menu Items
    menu: {
      scorer: "Correcteur de test",
      etta: "Activités d’affaires",
      rdOperations: "Activités R&D",
      operations: "Activités",
      ppc: "Tableau de bord de R&D",
      ta: "Administrateur de tests",
      testBuilder: "Développeur de test",
      checkIn: "M'enregistrer",
      profile: "Mon profil",
      incidentReport: "Rapports d'incidents",
      MyTests: "Mes tests",
      ContactUs: "Nous joindre",
      logout: "Fermer la session"
    },

    // Token Expired Popup Box
    tokenExpired: {
      title: "Session expirée",
      description:
        "Votre session a pris fin en raison de votre inactivité. Veuillez entrer votre adresse courriel et votre mot de passe pour ouvrir une nouvelle session."
    },

    // Home Page
    homePage: {
      welcomeMsg: "Bienvenue dans l'Outil d'évaluation des candidats",
      description:
        "Cette application est utilisée pour évaluer les candidats pour des postes au sein de la fonction publique fédérale. Veuillez ouvrir une session pour accéder à un test. Si vous n'avez pas de compte, veuillez cliquer sur le lien «\u00A0Créer un compte\u00A0» pour vous inscrire."
    },

    // Sample Tests Page
    sampleTestDashboard: {
      title: "Exemples de tests",
      description:
        "Les exemples de tests ne sont pas notés, les réponses ne sont pas transmises à la fin d'un exemple de test. Cliquez sur «\u00A0Commencer\u00A0» pour accéder à l’exemple de test sélectionné.",
      table: {
        columnOne: "Exemple de test",
        columnTwo: "Action",
        viewButton: "Commencer",
        viewButtonAccessibilityLabel: "Commencer le test {0}."
      }
    },

    // Dashboard Page
    dashboard: {
      title: "Bienvenue {0} {1}.",
      description:
        "Pour commencer votre test, cliquez sur le bouton M'enregistrer ci-dessous, et entrez votre code d'accès au test.",
      lastLogin: "[Date de la dernière ouverture de session\u00A0: {0}]",
      lastLoginNever: "jamais",
      table: {
        columnOne: "Test",
        columnTwo: "Action",
        viewButton: "Commencer",
        viewButtonAccessibilityLabel: "Commencer le test {0}.",
        noTests: "Aucun test ne vous est attribué"
      }
    },

    // My Test
    myTests: {
      title: "Bienvenue {0} {1}.",
      alertCatTestsOnly1:
        "Vous trouverez ci-après vos résultats à tous les tests que vous avez passés en utilisant l’Outil d’évaluation des candidats. Vos résultats à d’autres types de tests ne sont pas inclus.",
      alertCatTestsOnly2:
        "Ces résultats sont valides lorsque les conditions ont été respectées, notamment le respect du délai d’attente avant de reprendre un test.",
      alertCatTestsOnly3:
        "Vos résultats aux tests en ligne non supervisés pour les examens de compréhension de l’écrit et d’expression écrite passés depuis le 1er\u00A0septembre\u00A02022 sont valides pendant 5\u00A0ans et sont transférables à tous les ministères et organismes fédéraux.",
      table: {
        nameOfTest: "Test",
        testDate: "Date du test",
        score: "Résultat",
        results: "Action",
        viewResultsButton: "Afficher",
        viewResultsButtonAriaLabel:
          "Afficher les résultats du test {0}, passé le {1}, reprise possible à compter du {2}, et votre résultat est {3}.",
        viewResultsPopup: {
          title: "Résultat du test",
          testLabel: "Test\u00A0:",
          testDescriptionLabel: "Description du test\u00A0:",
          // languageLabel: "Langue du test\u00A0:",
          testDateLabel: "Date du test\u00A0:",
          // retestDateLabel: "Date à partir de laquelle une reprise est possible\u00A0:",
          // retestPeriodLabel: "Nombre de jours minimal avant une reprise\u00A0:",
          // retestPeriodUnits: "jours",
          scoreLabel: "Résultat\u00A0:"
        },
        noDataMessage: "Aucun test n'a encore été corrigé."
      }
    },

    // Checkin action
    candidateCheckIn: {
      button: "M'enregistrer",
      loading: "Recherche de tests actifs...",
      popup: {
        title: "Entrer le code d'accès au test",
        incompletedProfileTitle: "Mise à jour des renseignements de votre profil",
        description: `Veuillez inscrire le code d'accès au test, reçu de l'administrateur de tests, dans le champ ci-dessous. Ensuite, cliquez sur «\u00A0M'enregistrer\u00A0».`,
        textLabel: "Code d'accès au test\u00A0:",
        textLabelError: "Doit être un code d'accès au test valide",
        testAccessCodeAlreadyUsedError: "Vous avez déjà utilisé ce code d'accès au test",
        currentTestAlreadyInActiveStateError: "Vous êtes déjà enregistré pour ce test",
        incompletedProfileSystemMessage:
          "Vous devez remplir ou réviser votre profil avant de vous enregistrer pour un test. Des renseignements supplémentaires sont requis à des fins statistiques.",
        incompletedProfileDescription:
          "Avant de poursuivre, allez à la page {0} pour inscrire ces renseignements. ",
        incompletedProfileLink: "Mon profil",
        incompletedProfileRightButton: "Mettre à jour le profil"
      }
    },

    // Lock Screen
    candidateLockScreen: {
      tabTitle: "Test verrouillé",
      description: {
        part1: "Votre test a été verrouillé par l'administrateur du test.",
        part2: "Le chronomètre de votre test a été mis en pause.",
        part3: "Vos réponses ont été sauvegardées.",
        part4: "Veuillez contacter l'administrateur du test pour obtenir davantage d'instructions."
      }
    },

    // Pause Screen
    candidatePauseScreen: {
      tabTitle: "Test en pause",
      description: {
        part1:
          "Vous avez mis votre test en pause. Il reprendra lorsque vous sélectionnerez Reprendre le test ou lorsque la minuterie atteindra 00:00:00.",
        part2: "La minuterie de votre test est sur pause.",
        part3: "Vos réponses ont été enregistrées."
      }
    },

    // Invalidate Test Screen
    candidateInvalidateTestScreen: {
      tabTitle: "Test désassigné/invalidé",
      description: {
        part1: "Votre test a été désassigné ou invalidé.",
        part2: "Veuillez contacter l'administrateur de test pour obtenir davantage d'instructions.",
        returnToDashboard: "Retour à la page d'accueil"
      }
    },

    // Test Builder Page
    testBuilder: {
      backToTestDefinitionSelection: "Retour à la liste des définitions de tests",
      backToTestDefinitionSelectionPopup: {
        title: "FR Return to Test Definition Selection?",
        description: "FR Are you sure you want to continue?",
        systemMessageDescription: "FR You will lose everything!"
      },
      containerLabel: "Définition de test",
      itemBanksContainerLabel: "Banque d'items",
      goBackButton: "FR Back to Test Builder",
      inTestView: "FR In-Test View",
      errors: {
        nondescriptError:
          "FR Non descript error. Report this to IT. Maybe a user is logged into the adminconsole.",
        cheatingPopup: {
          title: "FR Caught Cheating Attempt",
          description:
            "FR You are not allowed to switch or open new tabs. Any attempt to minimize the screen or change focus will be considered a cheating attempt.",
          warning:
            "FR After {0} more detections your test will be terminated and your results invalidated. If this was an error, alert your TA."
        },
        backendAndDbDownPopup: {
          title: "Problème de connexion !",
          description:
            "La connexion à votre test a été interrompue. Pour continuer le test, assurez-vous d’être connecté à Internet, puis essayez de vous reconnecter à votre compte de l'OÉC.",
          rightButton: "Retour à la page d'accueil"
        }
      },
      testDefinitionSelectPlaceholder: "Sélectionner un section de test",
      sideNavItems: {
        testDefinition: "Information du test",
        testSections: "Sections de test",
        testSectionComponents: "Sous-sections de test",
        sectionComponentPages: "Titres des sections",
        componentPageSections: "Contenu des sections",
        questionBlockTypes: "Noms des blocs d'items",
        questions: "Items",
        scoring: "Méthode de notation",
        answers: "FR Answers",
        addressBook: "Contacts du carnet d'adresses",
        testManagement: "Activation du test"
      },
      // some values are underscored because python uses underscores
      testDefinition: {
        title: "Définition du test ",
        description: "FR These are the top level properties of a test.",
        validationErrors: {
          invalidTestDefinitionDataError:
            "Erreur : Tous les champs de l'Information sur le test sont obligatoires. Veuillez remplir les champs manquants et réessayer."
        },
        uploadFailedErrorMessage: "FR Upload Error: Review the highlighted sections",
        uploadFailedComplementaryErrorMessages: "Erreur de téléversement. Veuillez réviser {0}.",
        uploadJsonFailedComplementaryErrorMessages: "FR JSON Upload Error: Review JSON content",
        uploadTestDefinition: {
          title1: "Téléverser la version de test au serveur",
          title2: "Cette version de test a été téléversée avec succès",
          description1:
            "Une fois téléversée, une nouvelle version CAT de cette définition de test sera créée. Êtes-vous sûr de vouloir continuer ?",
          description2:
            "La version test a été téléversée avec succès. Cependant, veuillez noter que cette nouvelle version est actuellement désactivée. Pour activer cette nouvelle version de test, mettez à jour le statut du test dans la section Activation du test."
        },
        uploadFailedPopup: {
          title: "Échec de téléversment",
          systemMessage:
            "Toutes les sections de test dont la méthode de notation est réglée à NOTATION AUTOMATIQUE doivent contenir au moins une sous-section de test dont le type de sous-section est réglé à ITEMS_(BANQUE_D'ITEMS)."
        },
        undefinedVersion: "<FR undefined>",
        selectTestDefinition: {
          sideNavigationItems: {
            allTests: "Toutes les définitions de tests",
            AllActiveTestVersions: "Définitions de tests actives",
            archivedTestCodes: "Définitions de tests archivées",
            uploadJson: "Téléverser le JSON"
          },
          newTestDefinitionButton: "Nouvelle définition de test",
          allTests: {
            title: "Toutes les définitions de tests",
            description: "Création, modification et téléversement de définitions de tests.",
            table: {
              column1: "Numéro du test",
              column2: "FR Test Code",
              column3: "Nom du test",
              column4: "Actions",
              noData: "FR There is no test definition data",
              selectSpecificTestVersionTooltip: "Sélectionner une version de la définition de test",
              selectSpecificTestVersionLabel:
                "FR Select a specific version of this test definition where the parent code is: {0}, the test code is: {1} and the test name is: {2}",
              selectLatestTestVersionTooltip:
                "Afficher la version la plus récente de la définition de test",
              selectLatestTestVersionLabel:
                "FR View latest test version of this test definition where the parent code is: {0}, the test code is: {1} and the test name is: {2}",
              archiveTestCodeTooltip: "Archiver la définition de test",
              archiveTestCodeLabel:
                "FR Archive test code of the this test definition where the parent code is: {0}, the test code is: {1} and the test name is: {2}"
            },
            testDefinitionPopup: {
              title: "Sélectionner une version de la définition de test",
              parentCode: "FR Parent Code:",
              testCode: "FR Test Code:",
              versionNotes: "Notes sur la version :",
              uitStatus: "TELNS :",
              testStatus: "Statut du test :",
              sampleTestStatus: "Exemple de test :",
              name: "Nom du test :",
              rightButton: "Sélectionner la définition du test",
              dropdown: {
                title: "Version OÉC :",
                titleTooltip: "FR To be completed"
              }
            },
            archiveTestCodePopup: {
              title: "Archiver la définition de test",
              systemMessageDescription: "Tous les accès reliés au {0} seront révoquées.",
              description: "Êtes-vous sûr de vouloir continuer ?",
              rightButton: "Archiver la définition de test"
            }
          },
          allActiveTestVersions: {
            title: "Définitions de tests actives",
            description: "Affichage et désactivation de versions de tests actives",
            table: {
              column1: "FR Parent Code",
              column2: "FR Test Code",
              column3: "Nom du test",
              column4: "Version OÉC",
              column5: "Actions",
              noData: "FR There is no test definition data",
              viewButtonTooltip: "Afficher la définition de test",
              viewButtonAriaLabel:
                "FR View details of the following test definition where the parent code is: {0}, the test code is: {1}, the test name is: {2} and the version is: {3}",
              deactivateButtonTooltip: "Désactiver la définition de test",
              deactivateButtonAriaLabel:
                "FR Deactivate test definition version where the parent code is: {0}, the test code is: {1}, the test name is: {2} and the version is: {3}"
            },
            popup: {
              title: "Désactiver la définition de test",
              description: "Tous les accès reliés au {0} seront révoquées.",
              rightButton: "Désactiver la définition de test"
            }
          },
          archivedTestCodes: {
            title: "Définitions de tests archivées",
            description: "Restauration des définitions de tests désactivées.",
            table: {
              column1: "FR Parent Code",
              column2: "FR Test Code",
              column3: "Nom du test",
              column4: "Actions",
              noData: "FR There is no test definition data",
              restoreButton: "Restaurer la définition de test",
              restoreButtonAriaLabel:
                "FR Restore test definition test code where the parent code is: {0}, the test code is: {1} and the test name is: {2}"
            },
            popup: {
              title: "Restaurer la définition de test",
              description: "La définition du test {0} sera restaurée",
              rightButton: "Restaurer la définition de test"
            }
          },
          uploadJson: {
            title: "Téléverser le JSON",
            description: "Téléversement de fichiers JSON au serveur",
            textAreaLabel: "Veuillez sélectionner le fichier JSON :",
            uploadJsonButton: "Téléverser le JSON",
            invalidJsonError: "Erreur de téléversement. Veuillez réviser le fichier JSON",
            uploadJsonSuccessful: "Le fichier JSON « {0} {1} v{2} » a été téléversée avec succès.",
            fileSelectedAccessibility: "FR The selected file is {0}",
            browseButton: "Parcourir...",
            wrongFileTypeError: "Doit être un fichier JSON valide"
          },
          itemBanks: {
            title: "Banque d'items",
            table: {
              column1: "ID banque d'items",
              column2: "Nom de la banque d'items",
              column3: "Actions",
              viewButtonTooltip: "Afficher",
              viewButtonAriaLabel: "Afficher la banque d'items {0}",
              downloadReportButtonTooltip: "Télécharger le rapport",
              noData: "Votre recherche n'a généré aucun résultat"
            }
          },
          itemBankEditor: {
            backToItemBankSelectionButton: "Retour à la liste des banques d'items",
            backToItemBankSelectionPopup: {
              title: "FR Return to Item Bank Selection?",
              systemMessageDescription: "Toutes les données non enregistrées seront perdues !",
              description: "Êtes-vous sûr de vouloir continuer?"
            },
            sideNavigationItems: {
              itemBankDefinition: "Définition de la banque d'items",
              itemAttributes: "Variables personnalisées",
              items: "Items ({0})",
              bundles: "Groupement ({0})"
            },
            itemBankDefinition: {
              custom_item_bank_id: {
                title: "ID de la banque d'items\u00A0:",
                errorMessage: "FR Must be a valid Item Bank ID"
              },
              custom_item_bank_id_already_exists: {
                title: "ID de la banque d'items\u00A0:",
                errorMessage: "FR This Item Bank ID already exists"
              },
              en_name: {
                title: "Nom de la banque d'items (anglais)\u00A0:",
                errorMessage: "FR Must be a valid name"
              },
              fr_name: {
                title: "Nom de la banque d'items (français)\u00A0:",
                errorMessage: "FR Must be a valid name"
              },
              updateExistingItemBankPopup: {
                title: "FR Update Existing Item Bank Definition",
                systemMessageDescription:
                  "FR You are about to update an existing item bank definition!",
                description: "FR Are you sure you want to proceed?"
              }
            },
            itemAttributes: {
              createNewButton: "Variable",
              collapseAll: "Réduire toutes les variables",
              uploadDataButton: "FR Upload Data",
              uploadDataErrorMessage:
                "FR Error found in item attributes. Please make sure that all parameters are defined.",
              itemAttributeCollapsingItem: {
                title: "Variable personnalisée {0}\u00A0: ",
                values: "Valeurs\u00A0: ",
                en_name: {
                  title: "Nom de variable en anglais\u00A0:",
                  errorMessage: "FR Must be a valid name"
                },
                fr_name: {
                  title: "Nom de variable en français\u00A0:",
                  errorMessage: "FR Must be a valid name"
                },
                attributeTitle: "Nom de variable\u00A0:",
                attributeValuesTitle: "Valeurs\u00A0:",
                createNewAttributeValueButton: "FR Add Value",
                attributeTranslationsButtonLabel: "Modifier les traductions",
                attributeValueCollapsingItem: {
                  title: "FR Attribute Value {0}: {1}",
                  value_identifier: {
                    title: "FR Value Identifier",
                    errorMessage: "FR Must be a valid value identifier"
                  }
                },
                attributeNameTranslationsPopup: {
                  title: "Traductions - Nom de variable"
                }
              },
              dataUploadedSuccessfullyPopup: {
                title: "FR Item Bank Attributes Data Uploaded Successfully",
                systemMessage:
                  "FR The item bank attributes data has been uploaded successfully in the database."
              }
            },
            items: {
              itemTitle: "{0} {1} {2} - ID item système\u00A0: {3} ",
              createNewItemButton: "Nouvel item",
              createItemPopup: {
                title: "Créer un item",
                description:
                  "Veuillez fournir les informations requises pour créer un nouvel item.",
                responseFormatLabel: "Format de réponse\u00A0:",
                historicalIdLabel: "ID item R&D\u00A0:",
                historicalIdAlreadyBeenUsedError:
                  "Cet ID item R&D est déjà utilisé dans cette banque d'item.",
                rightButton: "Créer un item"
              },
              table: {
                itemId: "ID item système",
                itemType: "Statut de l'item",
                version: "Version",
                actions: "Actions",
                viewAction: "Aperçu",
                viewActionAccessibilityLabel: "Afficher l'item {0}",
                editAction: "Modifier",
                editActionAccessibilityLabel: "Modifier l'identifiant de l'item {0}",
                noData: "Il n'y a pas d'items dans cette banque.",
                rndItemId: "ID item R&D"
              },
              viewSelectedItemPopup: {
                title: "Aperçu de l'item\u00A0: {0}",
                questionLabel: "FR Question"
              },
              backToItemBankEditorButton: "Retour à la banque d'items",
              sideNavigationItems: {
                general: "Général",
                properties: "Propriétés",
                statistics: "Statistiques",
                socialRelationships: "Rapports sociaux",
                history: "Historique",
                versionComparison: "Comparaison de versions",
                uploadData: "FR Upload Data"
              },
              topTabs: {
                generalTabs: {
                  generaL: "Général"
                },
                contentTabs: {
                  content: "Contenu",
                  screenReader: "Lecteur d'écran"
                },
                otherTabs: {
                  categories: "Variables personnalisées",
                  comments: "Commentaires",
                  translations: "Traductions"
                }
              },
              general: {
                systemIdLabel: "ID item système\u00A0:",
                historicalIdLabel: "ID item R&D\u00A0:",
                editHistoricalIdTooltip: "Modifier",
                addEditHistoricalIdPopup: {
                  title: "ID item R&D",
                  description: "Veuillez fournir les informations requises.",
                  historicalIdLabel: "ID item R&D\u00A0:"
                },
                developmentStatusLabel: "Statut de l'item\u00A0:",
                versionLabel: "Version\u00A0:",
                deleteDraftTooltip: "Supprimer l'ébauche de l'item",
                deleteDraftConfirmationPopup: {
                  title: "Supprimer l'ébauche de l'item ?",
                  systemMessageDescription: "Cette ébauche sera supprimée !",
                  description: "Êtes-vous sûr de vouloir continuer ?"
                },
                draftVersionLabel: "<Ébauche>",
                responseFormatLabel: "Format de réponse\u00A0:",
                scoringFormatLabel: "Méthode de notation\u00A0:",
                shuffleOptionsLabel: "Randomiser choix de réponses\u00A0:",
                activeStatusLabel: "Actif\u00A0:",
                historicalIdAlreadyExistsError:
                  "Cet ID item R&D est déjà utilisé dans cette banque d'item."
              },
              content: {
                languageDropdownLabel: "Langue\u00A0:",
                addStemButton: "Énoncé",
                stemSections: "Énoncé(s)\u00A0:",
                addOptionButton: "Choix de réponse",
                options: "Choix de réponses\u00A0:",
                score: "Valeur de la notation\u00A0:",
                text: "Texte\u00A0:",
                excludeFromShuffle: "Exclure de la randomisation\u00A0:",
                detailsButton: "Détails",
                detailsPopup: {
                  title: "Information supplémentaire du choix de réponse",
                  textLabel: "Texte\u00A0:",
                  historicalOptionIdLabel: "ID choix de réponse R&D\u00A0:",
                  rationaleLabel: "Justification\u00A0:",
                  excludeFromShuffleLabel: "Exclure de la randomisation\u00A0:",
                  scoreLabel: "Valeur de la notation\u00A0:"
                },
                addAlternativeContentPopup: {
                  title: "Ajouter du contenu d'item alternatif ",
                  description: "Veuillez fournir les informations requises.",
                  alternativeContentTypeLabel: "Type de contenu alternatif\u00A0:",
                  alternativeContentTypeOptions: {
                    screenReader: "Lecteur d'écran"
                  },
                  rightButton: "Sauvegarder"
                },
                deleteAlternativeContentPopup: {
                  title: "Supprimer l'onglet de contenu d'item alternatif ?",
                  systemMessageDescription: "L'onglet de contenu d'item alternatif sera supprimé.",
                  description: "Êtes-vous sûr de vouloir supprimer cet onglet ?"
                }
              },
              comments: {
                addCommentButton: "Commentaire",
                addCommentPopup: {
                  title: "Ajouter un commentaire",
                  commentLabel: "Commentaire\u00A0:",
                  addButton: "Sauvegarder"
                },
                deleteCommentPopup: {
                  title: "Supprimer le commentaire",
                  systemMessageDescription1: "Êtes-vous sûr de vouloir supprimer ce commentaire ?",
                  systemMessageDescription2: "FR - {0}"
                }
              },
              previewItemPopup: {
                title: "Aperçu de l'item\u00A0: {0}"
              },
              uploadConfirmationPopup: {
                title: "Téléverser l'item",
                titleSuccessfully: "Téléversé avec succès !",
                systemMessageDescription:
                  "Le contenu des champs suivants sera mis à jour à la suite du téléversement de l'item\u00A0:",
                systemMessageDescriptionSuccessfully:
                  "La version {0} de l'item a été téléversée avec succès.",
                systemMessageDescriptionSuccessfullyOverwrite:
                  "La version {0} de l'item a été téléversée avec succès.",
                itemModificationsTable: {
                  itemElements: {
                    stemSections: "Énoncé(s)",
                    options: "Choix de réponses",
                    categories: "Variables personnalisées",
                    historicalId: "ID item R&D",
                    itemStatus: "Statut de l'item",
                    responseFormat: "Format de réponse",
                    scoringFormat: "Méthode de notation",
                    shuffleOptions: "Randomiser choix de réponses"
                  },
                  column1: "Champs",
                  column2: "Mise à jour",
                  column3: "Avertissements",
                  warnings: {
                    stemsNotDefined: "Énoncé(s) non défini(s).",
                    containsBlankStems: "Contient un ou des énoncés manquants.",
                    optionsNotDefined: "Choix de réponse(s) non définie(s).",
                    containsBlankOptions: "Contient un ou des choix de réponses manquants."
                  }
                },
                whatDoYouWantToDo: "Que voulez-vous faire ?",
                createNewVersionRadio: "Créer un nouveau numéro de version pour cet item",
                overwriteCurrentVersionRadio: "Conserver le numéro de version actuel de l'item",
                reviewedWarningsCheckboxLabel: "J'ai lu et accepté les avertissements."
              },
              uploadData: {
                title: "FR Upload Data: {0} ({1})",
                uploadLabel: "FR Upload Item:",
                uploadButton: "FR Upload",
                uploadPopup: {
                  title: "FR Upload Item",
                  description: "FR Please insert your comment if needed.",
                  comment: "FR Comment (optional):",
                  uploadButton: "FR Upload",
                  uploadSuccessful: "FR Item version {0} has been uploaded successfully."
                },
                uploadFailedErrorMessage: "FR Upload Error: Review the highlighted sections"
              },
              backToItemBankEditorPopup: {
                title: "Revenir à l'éditeur de banque d'items ?",
                systemMessageDescription: "Toutes les données non enregistrées seront perdues !",
                description: "Êtes-vous sûr de vouloir continuer ?"
              },
              changeItemConfirmationPopup: {
                goPreviousTitle: "Naviguer à l'item précédent ?",
                goNextTitle: "Naviguer au prochain item ?",
                changeItemTitle: "Naviguer vers un autre item ?",
                systemMessageDescription: "Toutes les données non enregistrées seront perdues !",
                description: "Êtes-vous sûr de vouloir continuer?"
              }
            },
            bundles: {
              bundleTitle: "{0}",
              createNewBundleButton: "Nouveau groupement",
              createBundlePopup: {
                title: "Créer un groupement",
                description:
                  "Veuillez fournir les informations requises pour créer un nouveau groupement.",
                bundleNameLabel: "Nom du groupement\u00A0:",
                bundleNameAlreadyBeenUsedError:
                  "Ce Nom du groupement est déjà utilisé dans cette banque d'item.",
                bundleVersionLabel: "Version du groupement\u00A0:",
                rightButton: "Créer un groupement"
              },
              backToItemBankEditorButton: "Retour à la banque d'items",
              table: {
                bundleName: "Nom du groupement",
                bundleVersion: "Version du groupement",
                itemCount: "Nombre d'items",
                bundleCount: "Nombre de groupements",
                status: "Statut",
                actions: "Actions",
                editAction: "Modifier",
                editActionAccessibilityLabel: "Modifier le groupement {0}",
                duplicateAction: "Dupliquer",
                duplicateActionAccessibilityLabel: "Dupliquer le groupement {0}",
                noData: "Cette banque ne contient aucun groupement"
              },
              topTabs: {
                generalTabs: {
                  general: "Général"
                },
                addTabs: {
                  addItems: "Ajouter des items",
                  addBundles: "Ajouter des groupements"
                },
                contentTabs: {
                  itemContent: "Contenu de l'item ({0})",
                  bundleContent: "Contenu du groupement ({0})"
                }
              },
              previewBundlePopup: {
                title: "Aperçu du groupement",
                systemMessageErrorBundleStructuralConflictError:
                  "Le groupement téléversé contient à la fois un groupement ainsi qu'un sous-groupement. Veuillez supprimer le sous-groupement conflictuel et essayer à nouveau.",
                systemMessageErrorDuplicateSystemIdsFoundInBundleAssociationsError:
                  "Le groupement en cours de prévisualisation contient des items en double. Veuillez supprimer les doublons et essayer à nouveau.",
                close: "Fermer"
              },
              changeBundleConfirmationPopup: {
                goPreviousTitle: "Naviguer au groupement précédent ?",
                goNextTitle: "Naviguer au prochain groupement ?",
                changeBundleTitle: "Changer de groupement ?",
                systemMessageDescription: "Toutes les données non enregistrées seront perdues !",
                description: "Êtes-vous sûr de vouloir continuer?"
              },
              uploadSuccessfulPopup: {
                title: "Téléversé avec succès !",
                titleWithError: "Le groupement ne peut être téléversé",
                systemMessageDescription: "Les modifications ont été téléversées avec succès.",
                systemMessageErrorBundleNameAlreadyUsed:
                  "Ce Nom du groupement est déjà utilisé dans cette banque d'item.",
                systemMessageErrorBundleStructuralConflictError:
                  "Le groupement téléversé contient à la fois un groupement ainsi qu'un sous-groupement. Veuillez supprimer le sous-groupement conflictuel et essayer à nouveau.",
                systemMessageErrorDuplicateSystemIdsFoundInBundleAssociationsError:
                  "Le groupement téléversé contient des items en double. Veuillez supprimer les doublons et essayer à nouveau."
              },
              general: {
                bundleNameLabel: "Nom du groupement\u00A0:",
                bundleVersionLabel: "Version du groupement\u00A0:",
                shuffleItemsLabel: "Randomiser les items\u00A0:",
                shuffleBundlesLabel: "Randomiser les groupements\u00A0:",
                shuffleBetweenBundlesLabel: "FR Randomize Between Groupings:",
                activeLabel: "Actif\u00A0:",
                editBundleNameTooltip: "Modifier",
                editBundleNamePopup: {
                  title: "Modifier le nom du groupement",
                  description: "Veuillez fournir les informations requises.",
                  bundleNameLabel: "Nom du groupement\u00A0:",
                  bundleNameMustBeValid: "Le Nom du groupement doit être être fourni."
                }
              },
              addItems: {
                table: {
                  systemId: "ID item système",
                  rndItemId: "ID item R&D",
                  responseFormat: "Format de réponse",
                  itemStatus: "Statut de l'item",
                  version: "Version",
                  noData: "Votre recherche n'a généré aucun résultat"
                },
                searchDropdowns: {
                  developmentStatusLabel: "Statut de l'item\u00A0:",
                  activeStateLabel: "Actif\u00A0:"
                },
                addItemsToBundleButton: "Ajouter au groupement"
              },
              itemContent: {
                basicRuleLabel: "Nombre d'items à sélectionner\u00A0:",
                basicRuleErrorLabel: "Doit être un nombre valide"
              },
              addBundles: {
                table: {
                  bundleName: "Nom du groupement",
                  bundleVersion: "Version",
                  itemCount: "Nombre d'items",
                  bundleCount: "Nombre de groupement",
                  status: "Statut",
                  noData: "Votre recherche n'a généré aucun résultat"
                },
                addBundlesToBundleButton: "Ajouter au groupement"
              },
              bundleContent: {
                createNewBundleRuleButton: "Nouvelle règle de groupement",
                bundleRuleCollapsingItemContainer: {
                  title: "Règle {0}",
                  numberOfBundlesToPull: "Nombre de groupements à sélectionner\u00A0:",
                  numberOfBundlesToPullErrorMessage: "Doit être un nombre valide",
                  shuffleBundles: "Randomiser les groupements\u00A0:",
                  keepItemsTogether: "Conserver les items ensemble\u00A0:"
                },
                bundleAssociationsSystemMessage:
                  "Tous les groupements qui ne font pas partie d'une règle seront supprimés lors du téléversement."
              }
            },
            deleteConfirmationPopup: {
              title: "FR Delete Confirmation",
              description:
                "FR Deleting this item will delete all the content displayed and all child objects that are related to this object. Are you sure you want to delete this object?"
            }
          }
        },
        test_code: {
          title: "Code complet du test",
          titleTooltip: "FR Unique Identifier for a test definition",
          errorMessage: "Dois être un Code de test complet valide"
        },
        parent_code: {
          title: "FR Parent Code",
          titleTooltip: "FR The test code digits that group all version of this test.",
          errorMessage: "Dois être un Numéro de test valide"
        },
        version: {
          title: "Version OÉC",
          titleTooltip: "FR The latest version of the test definition",
          errorMessage: "FR Not a valid Version Number"
        },
        retest_period: {
          title: "Période d'attente",
          titleTooltip: "FR Retest period in number of days",
          errorMessage: "FR Not a valid number."
        },
        version_notes: {
          title: "Notes sur la version",
          titleTooltip: "FR Some wording here..."
        },
        fr_name: {
          title: "Nom du test (français)",
          titleTooltip: "FR Name display to candidates when they have an active test",
          errorMessage: "FR Not a valid Name"
        },
        en_name: {
          title: "Nom du test (anglais)",
          titleTooltip: "FR Name display to candidates when they have an active test",
          errorMessage: "FR Not a valid Name"
        },
        is_uit: {
          title: "Test non-supervisé",
          titleTooltip: "FR Is this a unsupervised internet test?"
        },
        is_public: {
          title: "Exemple de test",
          titleTooltip: "FR Is this a sample test?"
        },
        count_up: {
          title: "Chronomètre croissant",
          titleTooltip:
            "FR Timed section(s) will count up instead of counting down - TO BE COMPLETED"
        },
        active_status: {
          title: "Statut du test",
          titleTooltip: "FR Is this test active and available to use?"
        },
        test_language: {
          title: "FR Test Language",
          titleTooltip: "FR Is this test presented in a single language?",
          errorMessage: "FR not a valid choice",
          selectPlaceholder: "--"
        },
        constants: {
          testSectionScoringTypeObject: {
            not_scorable: "NON NOTÉ",
            auto_score: "NOTATION AUTOMATIQUE",
            competency: "NOTATION PAR COMPÉTENCE"
          },
          nextSectionButtonTypeObject: {
            proceed: "CONTINUER À LA SECTION SUIVANTE",
            popup: "FENÊTRE DE DIALOGIE"
          }
        }
      },
      testSection: {
        title: "Sections de test",
        validationErrors: {
          mustContainAtLeastThreeTestSectionError:
            "FR Error: Must contain at least three test sections",
          mustContainQuitTestSectionError: "FR Error: Must contain a 'Quit' test section",
          mustContainFinishTestSectionError: "FR Error: Must contain a 'Finish' test section"
        },
        description:
          "Créez un test en ajoutant et en définissant des sections de test. Les sections typiques d'un test peuvent comprendre les pages suivantes : Bienvenue, Sécurité du test, Conditions d'utilisation, Navigation, Directives spécifiques, Questions du test, Test soumis et Quitter le test.",
        collapsableItemName: "Section de test {0}: {1}",
        addButton: "Nouvelle section de test",
        delete: {
          title: "FR Delete Test Section Component",
          description:
            "FR Deleting an element will delete all child elements as well. Are you sure you wish to continue?"
        },
        fr_title: {
          title: "Nom de la section du test (français)",
          titleTooltip: "FR Hidden name used for easy identification by internal users",
          errorMessage: "FR Not a valid Name"
        },
        en_title: {
          title: "Nom de la section du test (anglais)",
          titleTooltip: "FR Hidden name used for easy identification by internal users",
          errorMessage: "FR Not a valid Name"
        },
        uses_notepad: {
          title: "Bloc-notes",
          titleTooltip: "FR Does this test section use the notepad component?",
          errorMessage: "FR not a valid choice"
        },
        uses_calculator: {
          title: "Calculatrice",
          titleTooltip: "FR Does this test section use the calculator component?"
        },
        block_cheating: {
          title: "Détection du changement de fenêtre",
          titleTooltip: "FR Should this test section report the user for switching tabs etc.?",
          errorMessage: "FR not a valid choice"
        },
        item_exposure: {
          title: "Exposition aux items",
          titleTooltip: "FR Should this test section apply the Item Exposure logic?"
        },
        scoring_type: {
          title: "Notation",
          titleTooltip:
            "FR Set the correct scoring method for the components in this test section.",
          errorMessage: "FR not a valid choice"
        },
        minimum_score: {
          title: "Note minimale",
          titleTooltip:
            "FR Set minimum score for a test section. Currently this value is only for reference in reports.",
          errorMessage: "FR not a valid choice"
        },
        default_time: {
          title: "Temps limite par défaut",
          titleTooltip:
            "FR How long does a candidate have on this section? If the section is not timed, then leave this empty. This field can be overridden by a TA",
          errorMessage: "FR Only numbers are allowed"
        },
        default_tab: {
          title: "Onglet par défaut",
          titleTooltip: "FR Which top tab should be displayed when a candidate begins this section",
          errorMessage: "FR not a valid choice",
          selectPlaceholder: "0"
        },
        order: {
          title: "Ordre de présentation",
          titleTooltip: "FR The order in the test that this section appears.",
          errorMessage: "FR not a valid choice",
          selectPlaceholder: "0"
        },
        next_section_button: {
          title: "FR Next Section Button",
          titleTooltip: "FR Define a button to take the user to the next section.",
          errorMessage: "FR not a valid choice",
          selectPlaceholder: "0"
        },
        reducers: {
          title: "FR Reducers",
          titleTooltip:
            "FR Which aspects of the test should be cleared when a user navigates to this section?",
          errorMessage: "FR not a valid choice",
          selectPlaceholder: "--"
        },
        section_type: {
          title: "Type de section",
          titleTooltip:
            "FR The type of test section. Such as: Single page, Top Tab navigation, Finish, Quit...",
          errorMessage: "FR Not a valid Type"
        },
        deletePopup: {
          title: "Confirmer la suppression",
          content: "Voulez-vous vraiment supprimer cette section?"
        }
      },
      nextSectionButton: {
        header: "Bouton de la section suivante",
        button_type: {
          title: "Type de bouton",
          errorMessage:
            "Les valeurs sélectionnées pour le Type de section et le Type de bouton ne sont pas compatibles. Veuillez les modifier.",
          titleTooltip: "FR This is selected based on the test section component."
        },
        confirm_proceed: {
          title: "Boîte de confirmation",
          titleTooltip:
            "FR A checkbox the user has to click before being allowed to press the action button."
        },
        button_text: {
          title: "Libellé du bouton",
          titleTooltip: "FR The text that the button displays to the user"
        },
        title: {
          title: "Titre de la fenêtre de dialogue",
          titleTooltip:
            "FR The text that appears on the header of the popup when the next section button is clicked."
        },
        content: {
          title: "Contenu de la fenêtre de dialogue",
          titleTooltip:
            "FR The text that appears in the body of the popup when the next section button is clicked."
        }
      },
      testSectionComponents: {
        title: "Sous-sections du test",
        validationErrors: {
          mustFixTheQuestionsRulesErrors:
            "Erreur: Veuillez corriger les Règles d'assemblage de tests (Blocs d'items) ci-bas et réessayer.",
          undefinedPpcQuestionIdInDependentQuestion:
            'Assurez-vous que tous les items dépendants ont un "ID item R&D" défini.',
          notEnoughDefinedPpcQuestionIdBasedOnNbOfQuestions:
            "Erreur : Nombre insuffisant d'items associés aux blocs d'items. Veuillez retourner à la section Items et assigner des blocs d'items à un nombre suffisant d'items qui correspond au nombre d'items spécifié dans les Règles d'assemblage de tests (Blocs d'items) ci-dessous.",
          mustContainQuestionListComponentType:
            'Erreur : Assurez-vous que toute section de test dont la "méthode de notation" est réglée sur "NOTATION AUTOMATIQUE" contient au moins un type de sous-section réglé sur "ITEMS_(CONSTRUCTEUR_DE_TEST)".',
          mustContainItemBankRules:
            "FR All test section component of component type ITEM_BANK must have defined and valid rules"
        },
        description:
          'Création de définitions des sous-sections du test. Les sous-sections typiques peuvent inclure les "Directives du test" et les "Questions du test" dans la section "Questions du test" associée. Toutes les sections du test doivent comporter une sous-section. Si aucune sous-section n\'est nécessaire, veuillez en créer une et lui donner le même nom que sa section mère.',
        collapsableItemName: "Sous-section de test {0}: {1}",
        addButton: "Ajouter une sous-section de test",
        delete: {
          title: "FR Delete",
          description:
            "FR Deleting an element will delete all child elements as well. Are you sure you wish to continue?"
        },
        parentTestSection: {
          title: "Section de test",
          titleTooltip: "FR The test section that the displayed objects appear in."
        },
        fr_title: {
          title: "Nom français",
          titleTooltip: "FR Hidden name used for easy identification by internal users",
          errorMessage: "FR Not a valid Name"
        },
        en_title: {
          title: "Nom anglais",
          titleTooltip: "FR Hidden name used for easy identification by internal users",
          errorMessage: "FR Not a valid Name"
        },
        order: {
          title: "Ordre de la sous-section",
          titleTooltip: "FR The order in which this component appears in a Top Tab navigation.",
          errorMessage: "FR Not a valid Name"
        },
        component_type: {
          title: "Type de la sous-section",
          titleTooltip: "FR The type of component. Such as: Inbox, Single Page, Side Navigation...",
          errorMessage:
            "FR Make sure that this is a valid parent test section data and component type combination"
        },
        shuffle_all_questions: {
          title: "Randomiser les items (Constructeur de tests)",
          titleTooltip:
            "FR Shuffle the questions in this question list? Question dependencies remain in the order they were given."
        },
        shuffle_question_blocks: {
          title: "Randomiser les blocs (Constructeur de tests)",
          titleTooltip: "FR Shuffle the question blocks in this question list? (TO BE COMPLETED)"
        },
        shuffle_item_bank_rules: {
          title: "FR Shuffle Item Bank Rules",
          titleTooltip: "FR Shuffle the item bank rules?"
        },
        language: {
          title: "Langue de la sous-section",
          titleTooltip:
            "FR Which language will the content in this section be provided in? This is mandatory for screen readers to use the correct accent.",
          errorMessage: "FR not a valid choice",
          selectPlaceholder: "--"
        }
      },
      questionListRules: {
        title: "Règles d'assemblage de tests (Blocs d'items)",
        collapsableItemName: "FR Rule {0}",
        addButton: {
          title: "Ajouter un bloc",
          titleTooltip: "FR You must create Question Block Types in order to add rules."
        },
        delete: {
          title: "FR Delete",
          description:
            "FR Deleting an element will delete all child elements as well. Are you sure you wish to continue?"
        },
        number_of_questions: {
          title: "Nombre d'items :",
          titleTooltip:
            "FR The number of questions to select of the following question block type.",
          errorMessage: "Dois être un Nombre d'items valide"
        },
        question_block_type: {
          title: "Bloc :",
          titleTooltip: "FR The question block type from which to select questions.",
          errorMessage: "Dois sélectionner une Règle d'assemblage de tests (Blocs d'items)"
        },
        order: {
          title: "Ordre du bloc :",
          titleTooltip:
            "FR The order that this rule will appear in the question list. If there is no order, leave this field empty."
        },
        shuffle: {
          title: "Randomiser les items :",
          titleTooltip:
            "FR Shuffle the questions in this rule? Question dependencies remain in the order they were given."
        }
      },
      itemBankRules: {
        title: "FR Item Bank Rules",
        collapsableItemName: "FR Rule {0}",
        order: {
          title: "FR Order",
          titleTooltip: "FR The order that this rule will appear."
        },
        item_bank: {
          title: "FR Item Bank:",
          titleTooltip: "FR Item bank from which you want to pull the respective grouping from."
        },
        item_bank_bundle: {
          title: "FR Bundle:",
          titleTooltip: "FR Respective Grouping from which the items list will be generated."
        }
      },
      sectionComponentPages: {
        title: "Sous-sections du test",
        description:
          'Création de définitions des sous-sections du test. Les sous-sections typiques peuvent inclure les "Directives du test" et les "Questions du test" dans la section "Questions du test" associée. Toutes les sections du test doivent comporter une sous-section. Si aucune sous-section n\'est nécessaire, veuillez en créer une et lui donner le même nom que sa section mère.',
        validationErrors: {
          mustContainPageContentTitlesError:
            'Erreur : Toutes les Sous-sections de test, à l\'exception de celles dont le Type de sous-section est "ITEMS_(TEST_BUILDER)", doivent être associées à un Titre de section.'
        },
        addButton: "Ajouter une sous-section de test",
        delete: {
          title: "FR Delete",
          description:
            "FR Deleting an element will delete all child elements as well. Are you sure you wish to continue?"
        },
        collapsableItemName: "Sous-section de test {0}",
        parentTestSectionComponent: {
          title: "Sous-section de test",
          titleTooltip: "FR The parent test section the displayed objects are displayed in."
        },
        fr_title: {
          title: "Nom français",
          titleTooltip:
            "FR Name of the side navigation tab when the site language is French. This field is hidden when the Test Section Component is SINGLE_PAGE.",
          errorMessage: "FR Not a valid Name"
        },
        en_title: {
          title: "Nom anglais",
          titleTooltip:
            "FR Name of the side navigation tab when the site language is French. This field is hidden when the Test Section Component is SINGLE_PAGE.",
          errorMessage: "FR Not a valid Name"
        },
        order: {
          title: "Ordre de la sous-section",
          titleTooltip: "FR The order in which this page appears in a side navigation panel.",
          errorMessage: "FR Not a valid Name"
        }
      },
      componentPageSections: {
        title: "Contenu des sections",
        description: "Ajouter du contenu aux sections et sous-sections de test.",
        collapsableItemName: "FR Section Component Page Order: {0}, Type: {1}",
        parentSectionComponentPage: {
          title: "Titres des sections",
          titleTooltip:
            "FR The parent side navigation tab that the objects displayed will belong to."
        },
        pageSectionLaguage: {
          title: "FR Language",
          titleTooltip:
            "FR If a test is bilingual, you must create page sections for each language. This will show all the page sections created for the selected language."
        },
        addButton: "Ajouter du contenu",
        delete: {
          title: "FR Delete",
          description:
            "FR Deleting an element will delete all child elements as well. Are you sure you wish to continue?"
        },
        order: {
          title: "Ordre du contenu de la section :",
          titleTooltip: "FR The order in which this page appears in a side navigation panel.",
          errorMessage: "FR Not a valid Name"
        },
        page_section_type: {
          title: "Type de contenu :",
          titleTooltip:
            "FR The type of section to display (i.e. Markdwon, sample email, sample teask response).",
          errorMessage: "FR Not a valid type."
        }
      },
      markdownPageSection: {
        content: {
          title: "Contenu",
          titleTooltip: "FR The markdown content to display."
        }
      },
      sampleEmailPageSection: {
        email_id: {
          title: "FR Email ID",
          titleTooltip: "FR The email ID to display"
        },
        from_field: {
          title: "FR From",
          titleTooltip: "FR Who the email is from."
        },
        to_field: {
          title: "FR To",
          titleTooltip: "FR Who the email is to"
        },
        date_field: {
          title: "FR Date",
          titleTooltip: "FR The date displayed on the email."
        },
        subject_field: {
          title: "FR Subject",
          titleTooltip: "FR The subject displayed on the email."
        },
        body: {
          title: "FR Body",
          titleTooltip: "FR The body of the email to display."
        }
      },
      sampleEmailResonsePageSection: {
        cc_field: {
          title: "FR CC",
          titleTooltip: "FR Who is CC'd on the email"
        },
        to_field: {
          title: "FR To",
          titleTooltip: "FR Who the email is to"
        },
        response: {
          title: "FR Response",
          titleTooltip: "FR The email response."
        },
        reason: {
          title: "FR Reason",
          titleTooltip: "FR The reason for responding with this email."
        }
      },
      sampleTaskResonsePageSection: {
        response: {
          title: "FR Response",
          titleTooltip: "FR The task response."
        },
        reason: {
          title: "FR Reason",
          titleTooltip: "FR The reason for responding with this task."
        }
      },
      zoomImagePageSection: {
        small_image: {
          title: "FR Full Image Name",
          titleTooltip: "FR name of the image file including extension."
        },
        note: "FR Note",
        noteDescription:
          "FR In order for this image to be displayed properly you must provide the image to the IT team to upload to the application server."
      },
      treeDescriptionPageSection: {
        address_book_contact: {
          title: "FR Root Contact",
          titleTooltip: "FR The root contact that the tree description will start from."
        }
      },
      addressBook: {
        topTitle: "FR Address Book",
        description:
          "FR These are email like contacts used in Inbox style Test Section Components and Tree Description Component Page Sections.",
        collapsableItemName: "{0}",
        addButton: "FR Add Address Book Contact",
        name: {
          title: "FR Name",
          titleTooltip: "FR The name of the contact."
        },
        title: {
          title: "FR Title",
          titleTooltip: "FR The title of the contact."
        },
        parent: {
          title: "FR Parent Contact",
          titleTooltip: "FR The contact that this contact reports to."
        },
        test_section: {
          title: "FR Test Sections",
          titleTooltip:
            "FR Which test sections can a user send emails to this contact or if there is a tree description in a test section then this contact must be in that test section."
        }
      },
      questions: {
        topTitle: "Items",
        description:
          'Créer des items à l\'aide du Constructeur de tests. Pour créer des items, un type de sous-section "ITEMS_(CONSTRUCTEUR_DE_TEST)" doit être défini pour une sous-section de test donnée.',
        validationErrors: {
          uploadFailedMissingQuestionListQuestions:
            'Erreur : Assurez-vous que toutes les Sous-sections de test dont le Type de sous-section est "ITEMS_(CONSTRUCTEUR_DE_TEST)" contiennent au moins un item.'
        },
        collapsableItemNames: {
          block: "[Bloc]",
          id: "[ID]",
          text: "[Texte]"
        },
        emailCollapsableItemName: "FR Email ID: {0}, From: {1}",
        addButton: "Nouvel item",
        searchResults: "{0} Item(s) trouvé(s)",
        pilot: {
          title: "Item expérimental",
          titleTooltip:
            "FR Is the question a pilot question? It's answer will not be counted towards a final score.",
          description: ""
        },
        questionBlockSelection: {
          title: "Nom du bloc d'items",
          titleTooltip: "FR The Question Block Type to narrow the number of results."
        },
        order: {
          title: "Ordre de l'item :",
          titleTooltip:
            "FR Order that questions will be displayed in the test (if there are no dependencies and/or shuffle) - TO BE COMPLETED"
        },
        ppc_question_id: {
          title: "ID item R&D",
          titleTooltip: "FR PPC Question ID Tooltip Content - TO BE COMPLETED"
        },
        question_type: {
          title: "Format de réponse",
          titleTooltip:
            "FR i.e. multiple choice, email. This is pre-determined by the Test Section Component Type (Question List or Inbox)"
        },
        question_block_type: {
          title: "Nom du bloc d'items",
          titleTooltip:
            "FR The question block type identifier. Used to determine what questions are returned for a question lists rule set.",
          selectPlaceholder: "FR Please Select"
        },
        dependencies: {
          title: "Dépendants",
          titleTooltip: "FR Questions that this question must be presented with (in order)."
        },
        dependent_order: {
          title: "FR Dependent Order",
          titleTooltip:
            "FR The order this question is presented in when dependents exist. This is only used for ordering questions in a dependency sitaution."
        },
        shuffle_answer_choices: {
          title: "Randomiser les choix de réponses",
          titleTooltip: "FR If enabled, the answer choices will be shuffled - TO BE COMPLETED"
        },
        previewPopup: {
          title: "FR Question Preview"
        }
      },
      scoringMethod: {
        title: "Méthode de notation",
        validationErrors: {
          mustApplyScoringMethodError: "FR Error: Must apply a scoring method type"
        },
        description: "Sélectionnez la méthode de notation pour ce test.",
        scoringMethodUndefinedError: "Veuillez choisir une méthode de notation valide.",
        scoringMethodType: {
          title: "Méthode de notation :",
          titleTooltip: "FR Scoring mechanism to use for score conversion"
        },
        scoringPassFailMinimumScore: {
          title: "Note de passage :",
          placeholder: "{FR minimum_score}",
          titleTooltip: "FR Minimum total score required to get a 'pass' mark",
          errorMessage: "Dois être un nombre valide"
        },
        scoringThreshold: {
          addButton: "Bande de scores",
          collapsingItem: {
            title: "Bande de scores : {0} to {1} = {2}",
            minimumScore: {
              title: "Score minimal :",
              placeholder: "{FR maximum_score}",
              titleTooltip: "FR Minimum required score to get the conversion value",
              errorMessage: "Dois être un score minimal valide"
            },
            maximumScore: {
              title: "Score maximal :",
              placeholder: "{FR conversion_value}",
              titleTooltip: "FR Maximum required score to get the conversion value",
              errorMessage: "Dois être un score maximal valide"
            },
            enConversionValue: {
              title: "Valeur (anglais) :",
              placeholder: "FR {conversion_value}",
              titleTooltip:
                "FR Conversion value to display when respecting the minimum and maximum scores",
              errorMessage: "Dois être une valeur valide"
            },
            frConversionValue: {
              title: "Valeur (français) :",
              placeholder: "FR {conversion_value}",
              titleTooltip:
                "FR Conversion value to display when respecting the minimum and maximum scores",
              errorMessage: "Dois être une valeur valide"
            }
          },
          deletePopup: {
            title: "Confirmer la suppression",
            content:
              "FR Deleting this item will delete all the content displayed. Are you sure you want to delete this object?"
          },
          invalidCollectionError: "Dois s'agir d'étendues de bandes valides"
        }
      },
      questionSituations: {
        situation: {
          title: "FR Situation",
          titleTooltip:
            "FR The situation text to be displayed at the top of the question when a test is viewed in scorer mode."
        }
      },
      questionBlockTypes: {
        topTitle: "Noms des blocs d'items",
        description:
          'Définition des noms des blocs à utiliser avec les items. Les blocs ne doivent être utilisés que pour les items créés dans la section "Items" du Constructeur de tests. Pour les items tirés de la banque d\'items, utiliser plutôt la fonctionnalité "Groupement" de la banque d\'items.',
        collapsableItemName: "FR Question Block Type: {0}",
        addButton: "Ajouter un nom de bloc d'items",
        name: {
          title: "Nom",
          titleTooltip: "FR The unique identifier for a question block type.",
          errorMessage: "Dois être un nom valide."
        }
      },
      competencyTypes: {
        topTitle: "FR Competency Types",
        description:
          "FR These are types associated with questions. These types are assigned to questions for manual marking.",
        collapsableItemName: "FR Competency Type: {0}",
        addButton: "FR Add Competency Type",
        en_name: {
          title: "FR English Name",
          titleTooltip: "FR The unique identifier for a question block type."
        },
        fr_name: {
          title: "FR French Name",
          titleTooltip: "FR The unique identifier for a question block type."
        },
        max_score: {
          title: "FR Max Score",
          titleTooltip: "FR The max score a candidate can get for this competency."
        }
      },
      questionSections: {
        title: "Énoncé(s) :",
        collapsableItemName: "Énoncé {0}",
        description:
          "FR These are the test section components belonging to the Test Section below. An example of a Test Section Component would be a Question List, Inbox, Side Navigation, or Single Page appearance/component.",
        addButton: "Énoncé",
        delete: {
          title: "FR Delete",
          description:
            "FR Deleting an element will delete all child elements as well. Are you sure you wish to continue?"
        },
        order: {
          title: "Ordre de l'énoncé",
          titleTooltip: "FR The order in which this component appears in a Top Tab navigation.",
          errorMessage: "FR Not a valid Name"
        },
        question_section_type: {
          title: "Type de contenu :",
          titleTooltip: "FR The type of section. Such as: Markdown, Image, Video...",
          errorMessage: "FR Not a valid Type"
        }
      },
      answers: {
        title: "Choix de réponses :",
        collapsableItemNames: {
          id: "[ID]",
          value: "[Valeur]",
          text: "[Texte]"
        },
        addButton: "Choix de réponse",
        delete: {
          title: "FR Delete",
          description:
            "FR Deleting an element will delete all child elements as well. Are you sure you wish to continue?"
        },
        content: {
          title: "Contenu",
          titleTooltip: "FR The text to display for this answer.",
          errorMessage: "FR Not a valid input"
        },
        order: {
          title: "Ordre du choix de réponse",
          titleTooltip: "FR The order in which this answer appears."
        },
        ppc_answer_id: {
          title: "ID item R&D",
          titleTooltip: " FR PPC Answer ID Tooltip Content - TO BE COMPLETED"
        },
        scoring_value: {
          title: "Valeur de la notation",
          titleTooltip:
            "FR The score to be added to the total test score when this answer is selected.",
          errorMessage: "FR Not a valid value"
        }
      },
      questionSectionMarkdown: {
        content: {
          title: "Contenu",
          titleTooltip: "FR Markdown to appear on the question."
        },
        delete: {
          title: "Confirmer la suppression",
          content:
            "FR Deleting this item will delete all the content displayed. Are you sure you want to delete this object?"
        }
      },
      questionSectionEnd: {
        content: {
          title: "FR Specific Question",
          titleTooltip:
            "FR The literal question after context to be answered. This allows screen readers to navigate directly to this section."
        },
        delete: {
          title: "Confirmer la suppression",
          content:
            "FR Deleting this item will delete all the content displayed. Are you sure you want to delete this object?"
        }
      },
      multipleChoiceQuestions: {
        header: "FR Question Sections"
      },
      emailQuestions: {
        scoringTitle: "FR Scoring",
        exampleRatingsTitle: "FR Example Ratings",
        competency_types: {
          title: "FR Competencies",
          titleTooltip: "FR The competencies being assessed in this question."
        },
        deleteDescription:
          "FR Deleting this item will delete the current object and any children this object may have. Are you sure you want to delete this object?",
        email_id: {
          title: "FR Email Id",
          titleTooltip:
            "FR Email Id is displayed at the top of the email and is the order in which emails are displayed."
        },
        to_field: {
          title: "FR To Field",
          titleTooltip: "FR The contact the email is to."
        },
        from_field: {
          title: "FR From Field",
          titleTooltip: "FR The Contact the email is from."
        },
        cc_field: {
          title: "FR CC Field",
          titleTooltip: "FR Contacts that are CC'd on this email."
        },
        date_field: {
          title: "FR Date",
          titleTooltip: "FR The date text to display on the email"
        },
        subject_field: {
          title: "FR Subject",
          titleTooltip: "FR The subject field to display on the email"
        },
        body: {
          title: "FR Body",
          titleTooltip: "FR The content of the email to display."
        }
      },
      situationRating: {
        score: {
          title: "FR Rating Score",
          titleTooltip:
            "FR This is the value that the example answered would be scored. Keep this to one decimal place."
        },
        example: {
          title: "FR Example Rationale",
          titleTooltip: "FR The Rationale behind giving the candidates answer this score."
        }
      },
      testManagement: {
        description: "Gérer le statut de cette version de test.",
        deactivateTestVersion: {
          // todo: Remove because on and off are the same text
          title: "Statut de la version de test",
          titleTooltip:
            "FR De-activate this test version: Remove this test version from the sample tests pages or make it unavailable to be assigned by business operations (this does not invalidate currently active test accesses)."
        },
        activateTestVersion: {
          title: "Statut de la version de test",
          titleTooltip:
            "FR Make this test version available for use: Visible on the sample tests page or available to be assigned by business operations (as appropriate)."
        },
        uploadTestVersion: {
          title: "Téléverser la version de test au serveur",
          titleTooltip:
            "FR Upload the changes made to this test version to the server as a new version."
        },
        uploadTestVersionButton: "Téléverser",
        downloadTestVersion: {
          title: "Télécharger le JSON de la version de test",
          titleTooltip:
            "FR Extract a JSON file of this test version; this excludes any changes made."
        },
        downloadTestVersionButton: "Télécharger"
      }
    },
    // Test Administration Page
    testAdministration: {
      title: "Bienvenue {0} {1}.",
      containerLabel: "Administrateur de tests",
      sideNavItems: {
        testAccessCodes: "Codes d'accès aux tests",
        activeCandidates: "Candidats actifs",
        uat: "Tests en ligne non supervisés",
        reports: "Rapports"
      },
      testAccessCodes: {
        description: "Gérer les codes d'accès que vous avez générés.",
        table: {
          testAccessCode: "Code d'accès au test",
          test: "Test",
          staffingProcessNumber: "Processus d'évaluation / Numéro de référence",
          action: "Action",
          actionButton: "Supprimer",
          generateNewCode: "Générer le code d'accès au test"
        },
        generateTestAccessCodePopup: {
          title: "Générer un code d'accès au test",
          description:
            "Veuillez fournir les renseignements suivants pour générer un code d'accès au test\u00A0:",
          testOrderNumber: "Numéro de commande du test\u00A0:",
          testOrderNumberCurrentValueAccessibility:
            "Le numéro de commande du test sélectionné est\u00A0:",
          orderlessTestOption: "Pas de commande de test",
          referenceNumber: "Numéro de référence interne\u00A0:",
          departmentMinistry: "Organisation\u00A0:",
          fisOrganisationCode: "Code de SIF de l'organisation\u00A0:",
          fisReferenceCode: "Code de référence de SIF\u00A0:",
          billingContactName: "Personne-ressource pour la facturation\u00A0:",
          billingInfo: "Courriel de contact pour les candidats\u00A0:",
          mustBeAValidEmailErrorMessage: "Doit être une adresse courriel valide",
          testToAdminister: "Test\u00A0:",
          testToAdministerCurrentValueAccessibility: "Le test sélectionné à administrer est\u00A0:",
          testSessionLanguage: "Langue de la séance de test\u00A0:",
          testSessionLanguageCurrentValueAccessibility:
            "La langue de la séance de test sélectionnée est\u00A0:",
          billingInformation: {
            title: "Renseignements sur la facturation\u00A0:",
            staffingProcessNumber: "Numéro du processus d'évaluation\u00A0:",
            departmentMinistry: "Code de l'organisation\u00A0:",
            contact: "Nom du responsable de la facturation\u00A0:",
            isOrg: "Code d'organisation SIF\u00A0:",
            isRef: "Code de référence SIF\u00A0:"
          },
          generateButton: "Générer"
        },
        disableTestAccessCodeConfirmationPopup: {
          title: "Supprimer le code d'accès au test",
          description:
            "Si vous supprimez le code d'accès au test {0}, les candidats ne pourront pas l'utiliser pour s'enregistrer."
        }
      },
      activeCandidates: {
        description:
          "Au fur et à mesure que les candidats s'enregistrent, leurs noms apparaîtront dans la liste des candidats actifs ci-dessous. Une fois que tous les candidats présents à la séance se seront enregistrés, veuillez supprimer le code d'accès au test.",
        lockAllButton: "Tout verrouiller",
        unlockAllButton: "Tout déverrouiller ",
        table: {
          candidate: "Nom et coordonnées du candidat",
          dob: "Date de naissance",
          testCode: "Version de test",
          status: "Statut",
          timer: "Durée du test",
          timeRemaining: "Temps restant",
          actions: "Action",
          actionTooltips: {
            updateTestTimer: "Modifier la durée du test",
            addEditBreakBank: "Ajouter/Modifier la banque de pauses",
            approve: "Admettre un candidat",
            unAssign: "Enlever l'accès",
            lock: "Verrouiller un test",
            unlock: "Déverrouiller un test",
            report: "FR Report Candidate"
          },
          updateTestTimerAriaLabel:
            "Modifier la durée du test de l'utilisateur {0} {1}. Sa date de naissance est {2}. La version actuelle de son test est {3}. Le statut actuel de son test est {4}. La durée du test est actuellement réglée à {5} heures et {6} minutes.",
          addEditBreakBankAriaLabel:
            "Ajouter / Modifier la banque de pauses de l'utilisateur {0} {1}. Sa date de naissance est {2}. Sa version de test est {3}. Son statut actuel de test est {4}. Sa limite de temps est actuellement réglée sur {5} heures et {6} minutes.",
          approveAriaLabel:
            "Admettre le candidat {0} {1}. Sa date de naissance est {2}. La version actuelle de son test est {3}. Le statut actuel de son test est {4}. La durée du test est actuellement réglée à {4} heures et {6} minutes.",
          unAssignAriaLabel:
            "Enlever l'accès de l’utilisateur {0} {1}. Sa date de naissance est {2}. La version actuelle de son test est {3}. Le statut actuel de son test est {4}. La durée du test est actuellement réglée à {5} heures et {6} minutes.",
          lockAriaLabel:
            "Verrouiller le test de {0} {1}. Sa date de naissance est {2}. La version actuelle de son test est {3}. Le statut actuel de son test est {4}. La durée du test est actuellement réglée à {5} heures et {6} minutes.",
          unlockAriaLabel:
            "Déverrouiller le test de {0} {1}. Sa date de naissance est {2}. La version actuelle de son test est {3}. Le statut actuel de son test est {4}. La durée du test est actuellement réglée à {5} heures et {6} minutes.",
          reportAriaLabel: "FR Report {0} {1} user."
        },
        noActiveCandidates: "Il n'y a aucun candidat actif",
        editTimePopup: {
          title: "Modifier la durée du test",
          description1: "À utiliser en tant que mesure d'adaptation seulement.",
          description2: "Modifier la durée du test pour {0} {1}.",
          description3:
            "Le chronomètre ne peut être réglé pour une durée moindre que la durée initialement prévue pour compléter une section spécifique.",
          description4: "Durée totale du test\u00A0: {0} Heures {1} Minutes",
          hours: "HEURES",
          minutes: "MINUTES",
          incrementHoursButton: "Augmenter le nombre d'heures. La valeur actuelle est {0}.",
          decrementHoursButton: "Réduire le nombre d'heures. La valeur actuelle est {0}.",
          incrementMinutesButton: "Augmenter le nombre de minutes. La valeur actuelle est {0}.",
          decrementMinutesButton: "Réduire le nombre de minutes. La valeur actuelle est {0}.",
          timerCurrentValue: "La durée du test est de {0} heures {1} minutes.",
          setTimerButton: "Régler le chronomètre"
        },
        addEditBreakBankPopup: {
          title: "Régler la banque de pauses",
          description: "Attribuer la banque de pauses à {0} {1} pour ce test.",
          setTimeTitle: "Banque de pauses",
          setBreakBankButton: "Inscrire"
        },
        lockPopup: {
          title: "Verrouiller un test",
          description1: "Vous êtes sur le point de verrouiller le test de {0} {1}.",
          lockButton: "Verrouiller le test"
        },
        unlockPopup: {
          title: "Reprendre un test",
          description1: "Vous êtes sur le point de déverrouiller le test de {0} {1}.",
          description2: "Le chronomètre du test sera réactivé une fois le test déverrouillé.",
          description3: "Assurez-vous de remplir un rapport d'incident pour ce candidat.",
          unlockButton: "Déverrouiller le test"
        },
        lockAllPopup: {
          title: "Verrouiller les tests de tous les candidats",
          description1:
            "Vous êtes sur le point de verrouiller ce test pour tous les candidats actifs.",
          description2:
            "Si, suite à l'incident qui a nécessité de verrouiller ce test, il est possible de reprendre le test, vous devrez déverrouiller les tests des candidats qui souhaitent poursuivre.",
          description3:
            "Assurez-vous de compléter un rapport d'incident et de remplir un formulaire de retrait du candidat pour tous les candidats qui ne poursuivront pas le test.",
          lockTestsButton: "Verrouiller les tests"
        },
        unlockAllPopup: {
          title: "Déverrouiller les tests de tous les candidats",
          description1:
            "Vous êtes sur le point de déverrouiller ce test pour tous les candidats actifs.",
          description2:
            "Assurez-vous de verrouiller les tests des candidats ne souhaitant pas ou ne pouvant pas poursuivre.",
          description3:
            "De plus, veuillez remplir un formulaire de retrait du candidat ainsi qu'un rapport d'incident pour les candidats qui ne poursuivent pas le test.",
          unlockTestsButton: "Déverrouiller les tests"
        },
        unAssignPopup: {
          title: "Enlever l'accès",
          description: "Voulez-vous vraiment enlever l'accès au test de {0} {1} ?",
          confirm: "Enlever l'accès"
        }
      },
      uit: {
        title: "Tests en ligne non supervisés (TELNS)",
        description: "Gérer vos invitations et processus de TELNS.",
        tabs: {
          invitations: {
            title: "Invitations",
            testOrderNumber: "Numéro de commande du test\u00A0:",
            testOrderNumberCurrentValueAccessibility:
              "Le numéro de commande du test sélectionné est\u00A0:",
            testOrderNumberErrorMessage: "Veuillez sélectionner le numéro de commande du test",
            orderlessTestOption: "Pas de commande de test",
            reuseData: "Réutiliser les données\u00A0:",
            referenceNumber: "Numéro de référence interne\u00A0:",
            referenceNumberError: "Veuillez indiquer un numéro de référence interne",
            departmentMinistry: "Organisation\u00A0:",
            fisOrganisationCode: "Code de SIF de l'organisation\u00A0:",
            fisOrganisationCodeError: "Veuillez indiquer un code de SIF de l'organisation",
            fisReferenceCode: "Code de référence de SIF\u00A0:",
            fisReferenceCodeError: "Veuillez indiquer un code de référence de SIF",
            billingContactName: "Personne-ressource pour la facturation\u00A0:",
            billingContactNameError: "Veuillez indiquer une personne-ressource pour la facturation",
            billingInformation: "Courriel de contact pour les candidats\u00A0:",
            billingInformationError: "Doit être une adresse courriel valide",
            test: "Test\u00A0:",
            providedInCsvOption: "Fourni dans la liste",
            reasonForTesting: "Motif du test\u00A0:",
            invalidReasonForTesting: "Veuillez choisir le motif du test",
            levelRequired: "Niveau demandé\u00A0:",
            levelRequiredRawScore: "Score brut requis\u00A0:",
            invalidLevelRequired: "Veuillez choisir le niveau demandé",
            invalidLevelRequiredRawScore: "Le nombre fourni dépasse le score maximal possible",
            testCurrentValueAccessibility: "Les tests sélectionnées sont\u00A0: {0}",
            testsErrorMessage: "Veuillez sélectionner au moins un test",
            validityEndDate: "Date d'échéance\u00A0:",
            numberOfCandidatesBasedOnEndDateExceededError:
              "Le nombre maximum de candidats terminant le même jour est dépassé. Veuillez choisir une autre date d'échéance.",
            emailTemplate: "Gabarit du courriel\u00A0:",
            emailTemplateCurrentValueAccessibility: "Le gabarit du courriel sélectionné est\u00A0:",
            candidates: "Liste des candidats\u00A0:",
            candidatesCurrentValueAccessibility: "Les candidats sélectionnés sont\u00A0:",
            browseButton: "Parcourir...",
            fileSelectedAccessibility: "Le fichier sélectionné est {0}",
            downloadTemplateButton: "Télécharger le gabarit",
            csvTemplateFileName: "Liste des candidats.xlsx",
            wrongFileTypeError: "Veuillez sélectionner un fichier de type CSV",
            invalidFileError:
              "Erreur trouvée dans le fichier. Veuillez corriger les renseignements dans les cellules suivantes\u00A0: {0}",
            noDataInCsvFileError:
              "Veuillez vous assurer que votre fichier CSV contient des données",
            noCsvFileSelectedError: "Veuillez sélectionner un fichier CSV valide",
            maximumInvitedCandideReachedError:
              "Ne peut pas dépasser un total de 500 invitations à la fois (y compris les demandes pour des tests multiples)",
            duplicateEmailsFoundInCsvError:
              "Des courriels en double ont été trouvés. Veuillez corriger les renseignements relatifs aux cellules {0} et {1}.",
            sendInvitesButton: "Envoyer les invitations",
            resetAllFields: "Réinitialiser tous les champs",
            invitesConfirmationPopup: {
              title: "Réviser et envoyer l'invitation aux candidats",
              titleInvitationsSentSuccessfull: "Invitations envoyées avec succès",
              titleInvitationsSentFail: "Invitations non envoyées",
              inviteData: {
                processNumber: "Numéro du processus d'évaluation\u00A0:",
                referenceNumber: "Numéro de référence interne\u00A0:",
                requestingDepartment: "Organisation requérante\u00A0:",
                fisOrganisationCode: "Code de SIF de l'organisation\u00A0:",
                fisReferenceCode: "Code de référence de SIF\u00A0:",
                HrCoordinator: "Personne-ressource pour la facturation\u00A0:",
                billingContactInfo: "Courriel de contact pour les candidats\u00A0:",
                test: "Test\u00A0:",
                validityEndDate: "Date d'échéance\u00A0:",
                reasonForTesting: "Motif du test\u00A0:",
                levelRequired: "Niveau demandé\u00A0:",
                levelRequiredRawScore: "Score brut requis\u00A0:",
                table: {
                  title: "Liste des candidats\u00A0({0})\u00A0:",
                  emailAddress: "Adresse courriel",
                  firstName: "Prénom",
                  lastName: "Nom de famille",
                  totalTime: "Durée totale",
                  breakBank: "Banque de pauses",
                  actions: "Actions",
                  editTestTimeTooltip: "Modifier la limite de temps",
                  editBreakBankTooltip: "Modifier la banque de pauses"
                },
                invitationSentSuccessfullMessage: "Les invitations ont été envoyées aux candidats.",
                invitationSentFailMessage:
                  "Quelque chose s'est produit et les invitations n'ont pas été envoyées ! Veuillez contacter le service de dépannage."
              },
              sendButton: "Envoyer"
            },
            resetAllFieldsPopup: {
              title: "Réinitialiser tous les champs ?",
              description: "Êtes-vous sûr de vouloir réinitialiser tous les champs ?",
              resetButton: "Réinitialiser"
            }
          },
          activeProcessess: {
            title: "Processus actifs",
            table: {
              columnOne: "Processus d'évaluation/numéro de référence",
              columnTwo: "Test",
              columnThree: "Nombre de tests passés",
              columnFour: "Organisation requérante",
              columnFive: "Date de l'invitation",
              columnSix: "Date d'échéance",
              columnSeven: "Actions",
              noActiveProcessess: "Il n'y a aucun processus en cours",
              viewSelectedProcessTooltip: "Afficher les renseignements liés au processus actif",
              updateSelectedProcessExpiryDate: "Mise à jour de la date d'échéance du processus",
              deleteSelectedProcessTooltip: "Supprimer le processus actif",
              viewButtonAccessibility: "Afficher le numéro du processus d'évaluation {0}",
              updateSelectedProcessExpiryDateAccessibility:
                "Mise à jour de la date d'échéance du processus {0}",
              deleteButtonAccessibility: "Supprimer le numéro du processus d'évaluation {0}"
            },
            selectedProcessPopup: {
              title: "Renseignements liés au processus actif",
              financialData: {
                processNumber: "Processus d'évaluation/numéro de référence\u00A0:",
                requestingDepartment: "Organisation requérante\u00A0:",
                HrCoordinator: "Nom du responsable de la facturation\u00A0:",
                validityEndDate: "Date d'échéance\u00A0:",
                test: "Test\u00A0:"
              },
              description: "Les candidats suivants ({0}) ont été invités à faire le TELNS\u00A0:",
              columnOne: "Adresse courriel",
              columnTwo: "Statut",
              columnThree: "Action",
              testInProgress: "En cours",
              testTaken: "Fait",
              testNotTaken: "Non fait",
              testUnassigned: "Désassigné",
              testInvalidated: "Invalidé",
              testCodeDeactivated: "Désactivé",
              deleteCandidateTooltip: "Désassigner/désactiver l'accès au test",
              deleteCandidateAccessibility: "Désassignation/désactivation de l'accès au test de {0}"
            },
            updateProcessExpiryDatePopup: {
              title: "Modifier la date d'échéance",
              titleSuccess: "Mise à jour du processus réussie\u00A0: {0}",
              validityEndDate: "Date d'échéance\u00A0:",
              reasonForModification: "Raison de la modification\u00A0:",
              rightButton: "Modifier",
              successMessage: "La date d'échéance a été modifiée et les courriels ont été envoyés."
            },
            deleteProcessPopup: {
              title: "Supprimer le numéro du processus d'évaluation\u00A0: {0}",
              titleSuccess: "Supression du processus réussie\u00A0: {0}",
              systemMessageDescription:
                "Vous êtes sur le point de supprimer le processus suivant. Notez qu'il y a encore {0} candidat(s) qui n'ont pas encore fait leur test. Êtes-vous sûr de vouloir continuer ?",
              description: "Renseignements sur le processus\u00A0:",
              deleteButton: "Supprimer le processus",
              successMessage:
                "Le numéro du processus d'évaluation a été supprimé et les courriels d'annulation ont été envoyés."
            },
            deleteSingleUitTestPopup: {
              title: "Confirmer la désassignation/désactivation de l'accès au test",
              description:
                "Êtes-vous sûr de vouloir désassigner/désactiver l'accès de {0} au test ?",
              deleteButton: "Désassigner/désactiver",
              selectReason: "Veuillez choisir une raison\u00A0:"
            },
            uitTestAlreadyStartedPopup: {
              title: "Action refusée"
            }
          },
          completedProcessess: {
            title: "Processus complétés",
            table: {
              process: "Processus d'évaluation/numéro de référence",
              test: "Test",
              totalCandidates: "Nombre de tests passés",
              testsTaken: "Organisation requérante",
              endDate: "Date d'échéance",
              actions: "Action",
              actionTooltip: "Afficher les renseignements liés au processus complété"
            },
            noCompletedProcessess: "Il n'y a aucun processus complété",
            popup: {
              title: "TELNS\u00A0: {0} - {1}",
              description: "Les candidats suivants ({0}) ont été invités à passer le TELNS\u00A0:"
            }
          }
        }
      },
      reports: {
        description:
          "Générer un rapport de résultats pour une commande de test que vous avez administré."
      }
    },

    // PPC  Administration Page
    ppcAdministration: {
      title: "Bienvenue {0} {1}.",
      containerLabel: "Tableau de bord de R&D",
      sideNavItems: {
        reports: "Rapports"
      },
      reports: {
        title: "Rapports",
        description: "Générer un rapport en lien avec une commande de test."
      }
    },

    // Report Generator Component
    reports: {
      reportTypeLabel: "Type de rapport\u00A0:",
      reportTypes: {
        individualScoreSheet: "Résultat de test individuel",
        resultsReport: "Sommaire des résultats de test",
        financialReport: "Rapport financier",
        financialReportFromDateLabel: "Du",
        financialReportToDateLabel: "Au",
        testContentReport: "Rapport sur le contenu du test",
        testTakerReport: "Rapport d'information sur le test du candidat",
        candidateActionsReport: "Rapport sur les actions du candidat"
      },
      testOrderNumberLabel: "Commande de test / Numéro de référence\u00A0:",
      testLabel: "Test\u00A0:",
      testsLabel: "Test(s)\u00A0:",
      testLabelAccessibility: "Les tests sélectionnés sont\u00A0:",
      candidateLabel: "Candidat\u00A0:",
      dateFromLabel: "Du\u00A0:",
      dateToLabel: "Au\u00A0:",
      invalidDate: "Doit être une date valide",
      dateError: "La première date doit être antérieure ou égale à la deuxième",
      parentCodeLabel: "Numéro de test\u00A0:",
      testCodeLabel: "Version de test\u00A0:",
      testVersionLabel: "v\u00A0:",
      generateButton: "Générer le rapport",
      noDataPopup: {
        title: "Aucun résultat",
        description: "Les paramètres fournis ne génèrent aucun résultat."
      },
      testStatus: {
        assigned: "ENREGISTRÉ",
        ready: "PRÊT",
        preTest: "PRÉ-TEST",
        active: "EN TEST",
        locked: "VERROUILLÉ",
        paused: "EN PAUSE",
        quit: "QUITTÉ",
        submitted: "SOUMIS",
        unassigned: "DÉSASSIGNÉ"
      },
      timedOut: "Temps écoulé",
      orderlessRequest: "Non commandé",
      individualScoreSheet: {
        personID: "No. de personne",
        emailAddress: "Adresse courriel",
        name: "Nom",
        pri: "CIDP",
        orderNumber: "No. de commande",
        assessmentProcessOrReferenceNb: "Processus d'évaluation / Numéro de référence",
        test: "Test",
        testDescriptionFR: "Description du test (FR)",
        testDescriptionEN: "Description du test (ANG)",
        testStartDate: "Date de début du test",
        score: "Résultat",
        level: "Niveau",
        testStatus: "Statut du test"
      },
      resultsReport: {
        lastname: "Nom de famille",
        firstname: "Prénom",
        emailAddress: "Adresse courriel",
        uitInvitationEmail: "Courriel d'invitation au TELNS",
        pri: "CIDP",
        orderNumber: "No. de commande",
        assessmentProcessOrReferenceNb: "Processus d'évaluation / Numéro de référence",
        test: "Test",
        testDescriptionFR: "Description du test (FR)",
        testDescriptionEN: "Description du test (ANG)",
        testDate: "Date du test",
        score: "Résultat",
        level: "Niveau",
        testStatus: "Statut du test"
      },
      financialReport: {
        candidateLastName: "Nom de famille",
        candidateFirstName: "Prénom",
        candidatePri: "CIDP",
        taWorkEmail: "Adresse courriel professionnelle de l'administrateur de test",
        taOrg: "Organisation de l'administrateur de test",
        requestingDepartment: "Organisation requérante",
        orderNumber: "No. de commande",
        assessmentProcess: "Processus d'évaluation / Numéro de référence",
        orgCode: "Code de l'organisation",
        fisOrgCode: "Code d'organisation SIF",
        fisRefCode: "Code de référence SIF",
        billingContactName: "Courriel de contact pour les candidats",
        testStatus: "Statut du test",
        isInvalid: "Est invalide",
        testDate: "Date du test",
        testVersion: "Version de test",
        testDescriptionFR: "Description du test (FR)",
        testDescriptionEN: "Description du test (ANG)"
      },
      testContentReport: {
        questionSectionNumber: "No. de section de la question",
        questionSectionTitleFR: "Titre de section de la question (FR)",
        questionSectionTitleEN: "Titre de section de la question (ANG)",
        rdItemID: "ID item R&D",
        questionNumber: "No. de la question",
        questionTextFR: "Texte de l'item (FR)",
        questionTextFrHTML: "Texte de l'item HTML (FR)",
        questionTextEN: "Texte de l'item (ANG)",
        questionTextEnHTML: "Texte de l'item HTML (ANG)",
        answerChoiceID: "Choix {0} - ID",
        answerChoiceTextFR: "Choix {0} - texte (FR)",
        answerChoiceTextFrHTML: "Choix {0} - texte HMTL (FR)",
        answerChoiceTextEN: "Choix {0} - texte (ANG)",
        answerChoiceTextEnHTML: "Choix {0} - texte HTML (ANG)",
        scoringValue: "Choix {0} - valeur",
        sampleTest: "Exemple de test"
      },
      testTakerReport: {
        selectedLanguage: "fr",
        userCode: "Code d'utilisateur",
        username: "Nom d'utilisateur",
        firstname: "Prénom",
        lastname: "Nom de famille",
        pri: "CIDP/numéro de matricule ",
        dateOfBirth: "Date de naissance",
        currentEmployer: "Employeur actuel",
        org: "Organisation",
        group: "Groupe occupationnel",
        level: "Niveau occupationnel",
        residence: "Résidence",
        education: "Études",
        gender: "Genre",
        identifyAsWoman: "Femme",
        aboriginal: "Autochtone",
        aboriginalInuit: "Indigenous - Inuit",
        aboriginalMetis: "Indigenous - Métis",
        aboriginalNAIndianFirstNation: "Indigenous - Indien d'AN/Première nation",
        visibleMinority: "Minorité visible",
        visibleMinorityBlack: "MV - Noir",
        visibleMinorityChinese: "MV - Chinois",
        visibleMinorityFilipino: "MV - Philippin",
        visibleMinorityJapanese: "MV - Japonais",
        visibleMinorityKorean: "MV - Coréen",
        visibleMinorityNonWhiteLatinx: "MV - Latinx non blanc",
        visibleMinorityNonWhiteMiddleEastern: "MV - Moyen Orient non blanc",
        visibleMinorityMixedOrigin: "MV - Origine mixte",
        visibleMinoritySouthAsianEastIndian: "MV - Asiatique du Sud-est",
        visibleMinoritySoutheastAsian: "MV - Asiatique du Sud/Indien de l'Est",
        visibleMinorityOther: "MV - Autre",
        visibleMinorityOtherDetails: "MV - Autre (précisions)",
        disability: "Handicap",
        disabilityVisual: "Handicap - Visuel",
        disabilityDexterity: "Handicap - Dextérité",
        disabilityHearing: "Handicap - Audition",
        disabilityMobility: "Handicap - Mobilité",
        disabilitySpeech: "Handicap - Élocution",
        disabilityOther: "Handicap - Autre",
        disabilityOtherDetails: "Handicap - Autre (précisions)",
        requestingDepartment: "Ministère demandeur",
        requestingDepartmentLanguage: "fabrv",
        administrationMode: "Mode d'administration",
        assisted: "Assisté",
        automated: "Automatisé",
        reasonForTesting: "Motif du test",
        levelRequired: "Niveau exigé",
        testNumber: "Numéro de test",
        testSectionLanguage: "Langue de la section de test",
        testForm: "Version de test",
        catVersion: "Version OÉC",
        testDate: "Date du test",
        testStartDate: "Date de début du test",
        submitTestDate: "Date de la remise du test",
        questionSectionNumber: "No. de section de la question",
        rdItemID: "ID item R&D",
        pilotItem: "Item pilote",
        itemOrderMaster: "Ordre d'item - maître",
        itemOrderPresentation: "Ordre d'item - présentation",
        itemOrderViewed: "Ordre d'item - visualisation",
        questionTextFr: "Texte de l'item (FR)",
        questionTextEn: "Texte de l'item (ANG)",
        questionTextHtmlFr: "Texte de l'item HTML (FR)",
        questionTextHtmlEn: "Texte de l'item HTML (ANG)",
        choiceXPpcAnswerId: "Choice {0} - ID",
        choiceXPoints: "Choice {0} - valeur",
        choiceXPresentation: "Choix {0} - présentation",
        choiceXTextFr: "Choix {0} - texte (FR)",
        choiceXTextEn: "Choix {0} - texte (ANG)",
        choiceXTextHtmlFr: "Choix {0} - texte HMTL (FR)",
        choiceXTextHtmlEn: "Choix {0} - texte HTML (ANG)",
        languageOfAnswer: "Langue de la réponse",
        timeSpent: "Temps total passé sur l'item (s)",
        responseMasterOrder: "Réponse",
        responseChoiceId: "Réponse - ID choix OÉC",
        scoringValue: "Score de l'item",
        sectionScore: "Score de la section",
        totalScore: "Score total",
        convertedScore: "Niveau",
        testStatus: "Statut du test",
        testSubmissionMode: "Mode de soumission de la section",
        scoreValidity: "Validité du résultat"
      },
      candidateActionsReport: {
        multiAssignedTestsSelectionOptions: {
          label: "Séance de test\u00A0:",
          startDate: "Date du test\u00A0:",
          submitDate: "Date de remise du test\u00A0:",
          modifyDate: "Date de modification\u00A0:"
        },
        origin: "Type d'entrée",
        historyDate: "Date de l'entrée",
        historyType: "Entrée initale ou modifiée",
        status: "Statut du test",
        previousStatus: "Statut précédent",
        startDate: "Date du test",
        submitDate: "Date de la remise du test",
        testAccessCode: "Code d'accès au test",
        totalScore: "Score total",
        testAdministrator: "Administrateur de test",
        testSessionLanguage: "Langue de la séance de test",
        enConvertedScore: "Niveau (ANG)",
        frConvertedScore: "Niveau (FR)",
        uitInviteId: "ID invitation TELNS",
        isInvalid: "Validité du résultat",
        timeType: "Entrée ou sortie de section",
        assignedTestSectionId: "Nom de la section de test",
        candidateAnswerId: "ID séance d'item du candidat",
        markForReview: "Coché pour révision",
        questionId: "ID item OÉC",
        selectedLanguage: "Langue de l'interface",
        answerId: "Réponse - ID choix OÉC",
        candidateAnswerIdRef: "ID séance d'item du candidat (réf.)"
      },
      itemBankReport: {
        name: "Rapport de contenu de la banque d'items - {0} [{1}]",
        selectedLanguage: "fr",
        systemId: "ID du système",
        version: "Version",
        historicalId: "ID item R&D",
        itemStatus: "Statut de l'item",
        scoringKey: "Clé de correction",
        scoringKeyId: "ID de la clé de réponse",
        stemId: "Énoncé {0} - ID",
        stemTextEn: "Énoncé {0} - Texte (ANG)",
        stemTextHtmlEn: "Énoncé {0} - Texte HTML (ANG)",
        stemTextFr: "Énoncé {0} - Texte (FR)",
        stemTextHtmlFr: "Énoncé {0} - Texte HTML (FR)",
        stemScreenReaderId: "Énoncé {0} - ID lecteur d'écran",
        stemScreenReaderTextEn: "Énoncé {0} - Texte lecteur d'écran (ANG)",
        stemScreenReaderTextHtmlEn: "Énoncé {0} - Texte lecteur d'écran HTML (ANG)",
        stemScreenReaderTextFr: "Énoncé {0} - Texte lecteur d'écran (FR)",
        stemScreenReaderTextHtmlFr: "Énoncé {0} - Texte lecteur d'écran HTML (FR)",
        optionHistoricalId: "Choix {0} - ID",
        optionTextEn: "Choix {0} - Texte (ANG)",
        optionTextHtmlEn: "Choix {0} - Texte HTML (ANG)",
        optionTextFr: "Choix {0} - Texte (FR)",
        optionTextHtmlFr: "Choix {0} - Texte HTML (FR)",
        optionScore: "Choix {0} - Valeur",
        optionScreenReaderHistoricalId: "Choix {0} - ID lecteur d'écran",
        optionScreenReaderTextEn: "Choix {0} - Texte lecteur d'écran (ANG)",
        optionScreenReaderTextHtmlEn: "Choix {0} - Texte lecteur d'écran HTML (ANG)",
        optionScreenReaderTextFr: "Choix {0} - Texte lecteur d'écran (FR)",
        optionScreenReaderTextHtmlFr: "Choix {0} - Texte lecteur d'écran HTML (FR)",
        optionScreenReaderScore: "Choix {0} - Valeur lecteur d'écran",
        itemType: "Type d'item"
      }
    },

    // DatePicker Component
    datePicker: {
      dayField: "Jour",
      dayFieldSelected: "Champ Jour sélectionné",
      monthField: "Mois",
      monthFieldSelected: "Champ Mois sélectionné",
      yearField: "Année",
      yearFieldSelected: "Champ Année sélectionné",
      hourField: "Heure",
      hourFieldSelected: "Champ Heure sélectionné",
      minuteField: "Minute",
      minuteFieldSelected: "Champ Minute sélectionné",
      currentValue: "La valeur actuelle est\u00A0:",
      none: "Sélectionnez",
      datePickedError: "Doit être une date valide",
      futureDatePickedError: "Doit être une date ultérieure",
      futureDatePickedIncludingTodayError: "Ne peut être une date antérieure à aujourd'hui",
      comboStartEndDatesPickedError: "La date de début doit être antérieure à la date de fin",
      comboFromToDatesPickedError: "La date de début doit être antérieure à la date de fin",
      deselectOption: "FR Deselect"
    },

    // PrivacyNoticeStatement Component
    privacyNoticeStatement: {
      title: "Énoncé de confidentialité",
      descriptionTitle: "Énoncé de confidentialité",
      paragraph1:
        "Les renseignements personnels servent à fournir des services d’évaluation aux clients du Centre de psychologie du personnel. Ils sont recueillis à des fins de dotation, conformément aux articles 11, 30 et 36 de la {0}. Dans le cas des organisations qui ne sont pas assujetties à cette loi, les renseignements personnels sont recueillis en vertu de la loi habilitante de l’organisation concernée ainsi que de l’article 35 de la {1}. Les résultats aux tests d’évaluation de langue seconde seront divulgués aux responsables de l’organisation autorisés. Les résultats obtenus à tous les autres examens seront communiqués exclusivement à l’organisation qui en a fait la demande. Les résultats obtenus aux examens pourraient être communiqués à la Direction des enquêtes de la Commission de la fonction publique si une enquête a lieu en vertu des articles 66 ou 69 de la {2}. Vos renseignements pourraient également être utilisés à des fins de recherche statistique et analytique. En vertu du paragraphe 8(2) de la {3}, des renseignements pourraient, dans certains cas, être divulgués sans votre autorisation. La communication de vos renseignements personnels est volontaire, mais si vous choisissez de ne pas fournir ces renseignements, il se peut que vous ne puissiez pas recevoir les services du Centre de psychologie du personnel.",
      paragraph2:
        "Les renseignements sont recueillis et utilisés de la façon décrite dans le fichier de renseignements personnels du Centre de psychologie du personnel (CFP PCU 025), qui se trouve dans {0} de la Commission de la fonction publique.",
      paragraph3:
        "Vous avez le droit d’accéder à vos renseignements personnels et de les corriger, ainsi que de demander qu’ils soient modifiés si vous croyez qu’ils sont erronés ou incomplets. Vous avez également le droit de porter plainte auprès du {0} au sujet du traitement de vos renseignements personnels.",
      publicServiceEmploymentActLinkTitle: "Loi sur l’emploi dans la fonction publique",
      publicServiceEmploymentActLink: "https://laws-lois.justice.gc.ca/fra/lois/p-33.01/",
      privacyActLinkTitle: "Loi sur la protection des renseignements personnels",
      privacyActLink: "https://laws-lois.justice.gc.ca/fra/lois/p-21/",
      infoSourceLinkTitle: "l’Info Source",
      infoSourceLink:
        "https://www.canada.ca/fr/commission-fonction-publique/organisation/propos-nous/bureau-acces-information-protection-renseignements-personnels/info-source.html#122",
      privacyCommissionerOfCanadaLinkTitle:
        "Commissaire à la protection de la vie privée du Canada",
      privacyCommissionerOfCanadaLink: "https://www.priv.gc.ca/fr/"
    },

    // DropdownSelect Component
    dropdownSelect: {
      pleaseSelect: "Sélectionnez",
      noOptions: "Aucune option"
    },

    // SearchBarWithDisplayOptions component
    SearchBarWithDisplayOptions: {
      searchBarTitle: "Recherche\u00A0:",
      resultsFound: "{0} résultat(s)",
      clearSearch: "Effacer la recherche",
      noResultsFound: "Votre recherche n'a généré aucun résultat.",
      displayOptionLabel: "Affichage\u00A0:",
      displayOptionAccessibility: "Nombre de résultats par page",
      displayOptionCurrentValueAccessibility: "La valeur sélectionnée est\u00A0:"
    },

    // Profile Page
    profile: {
      completeProfilePrompt:
        "Pour mettre à jour votre profil, veuillez remplir les champs requis puis cliquez sur Sauvegarder.",
      title: "Bienvenue {0} {1}.",
      sideNavItems: {
        personalInfo: "Renseignements personnels",
        password: "Mot de passe",
        preferences: "Préférences",
        permissions: "Droits et autorisations",
        profileMerge: "Fusion des comptes"
      },
      personalInfo: {
        title: "Mes renseignements personnels",
        nameSection: {
          firstName: "Prénom\u00A0:",
          lastName: "Nom de famille\u00A0:"
        },
        emailAddressesSection: {
          primary: "Adresse courriel primaire\u00A0:",
          secondary: "Adresse courriel secondaire (facultatif)\u00A0:",
          emailError: "Doit être une adresse courriel valide"
        },
        dateOfBirth: {
          title: "Date de naissance\u00A0:",
          titleTooltip:
            "Le fait d'ajouter votre date de naissance facilitera le regroupement de tous vos résultats dans votre dossier de résultats de tests."
        },
        pri: {
          title: "CIDP (facultatif)\u00A0:",
          titleTooltip:
            "Le fait d'ajouter votre CIDP facilitera le regroupement de vos résultats dans votre dossier de résultats de tests.",
          priError: "Doit être un CIDP valide"
        },
        militaryNbr: {
          title: "Numéro de matricule (facultatif)\u00A0:",
          titleTooltip:
            "Le fait d'ajouter votre votre numéro de matricule facilitera le regroupement de vos résultats dans votre dossier de résultats de tests.",
          militaryNbrError: "Doit être un numéro de matricule valide"
        },
        psrsAppId: {
          title: "Numéro d'identification du SRFP (facultatif)\u00A0:",
          psrsAppIdError: "Doit être un numéro d'identification du SRFP valide"
        },
        optionalField: "(facultatif)",
        saveConfirmationPopup: {
          title: "Vos renseignements personnels ont été mis à jour",
          description: "Vos renseignements personnels ont été sauvegardés avec succès."
        },
        profileChangeRequest: {
          tooltip: "FR Profile Change Request",
          existingRequestTooltip: "FR View Profile Change Request",
          requestPopup: {
            title: "FR Profile Change Request",
            successTitle: "FR Request Sent Successfully",
            deleteTitle: "FR Discard?",
            description:
              "FR Due to the portability of certain tests and the need to adequately validate identity... (to be completed)",
            successDescription: "FR Your request has been sent successfully.",
            current: "FR Current",
            new: "FR New",
            firstNameLabel: "FR First Name:",
            firstNameError: "FR Must be a valid First Name",
            lastNameLabel: "FR Last Name:",
            lastNameError: "FR Must be a valid Last Name",
            dobLabel: "FR Date of Birth:",
            comments: "FR Comments (optional):",
            deleteConfirmationSystemMessage:
              "FR There changes will be removed from the queue to be reviewed.",
            deleteConfirmationDescription: "FR Are you sure you want to continue?"
          }
        }
      },
      eeInformation: {
        title: "Équité en matière d'emploi",
        description1:
          "La Commission de la fonction publique du Canada (CFP) a besoin de votre aide afin de s’assurer que ses évaluations fonctionnent comme prévu. Les renseignements personnels que vous partagez ci-dessous seront exclusivement accessibles à la CFP et utilisés uniquement à des fins de recherche, d'analyse et d'élaboration de tests. La participation est volontaire\u00A0: vous pouvez choisir l’option «\u00A0Préfère ne pas répondre\u00A0» pour n’importe laquelle des questions ci-dessous. L’information fournie ne sera pas communiquée au personnel des ressources humaines ou aux responsables de l’embauche.",
        description2:
          "La CFP s’engage à protéger le droit des personnes à la vie privée. Vous pouvez consulter notre {0}.",
        privacyNoticeLink: "énoncé de confidentialité",
        identifyAsWoman: "Est-ce que vous vous désignez comme étant une femme ?",
        invalidIdentifyAsWomanErrorMessage: "Veuillez sélectionner un choix de réponse",
        aboriginal: "Êtes-vous Autochtone ?",
        invalidAboriginalErrorMessage: "Veuillez sélectionner un choix de réponse",
        subAboriginal:
          "Veuillez sélectionner tous les énoncés qui s’appliquent à votre situation\u00A0:",
        invalidSubAboriginalErrorMessage: "Veuillez sélectionner au moins un choix de réponse",
        visibleMinority: "Êtes-vous membre d’un groupe de minorité visible ?",
        invalidVisibleMinorityErrorMessage: "Veuillez sélectionner un choix de réponse",
        subVisibleMinority:
          "Veuillez sélectionner tous les énoncés qui s’appliquent à votre situation\u00A0:",
        subVisibleMinorityOtherTextAreaLabel: "Si autre, veuillez préciser\u00A0:",
        invalidSubVisibleMinorityErrorMessage: "Veuillez sélectionner au moins un choix de réponse",
        disability: "Êtes-vous une personne en situation de handicap ?",
        invalidDisabilityErrorMessage: "Veuillez sélectionner un choix de réponse",
        subDisability:
          "Veuillez sélectionner tous les énoncés qui s’appliquent à votre situation\u00A0:",
        subDisabilityOtherTextAreaLabel: "Si autre, veuillez préciser\u00A0:",
        invalidSubDisabilityErrorMessage: "Veuillez sélectionner au moins un choix de réponse"
      },
      additionalInfo: {
        title: "Renseignements supplémentaires (facultatif)",
        current_employer: {
          title: "Employeur actuel\u00A0:"
        },
        organization: {
          title: "Organisation\u00A0:"
        },
        group: {
          title: "Groupe\u00A0:"
        },
        subclassification: {
          title: "Sous-groupe\u00A0:"
        },
        level: {
          title: "Niveau\u00A0:"
        },
        residence: {
          title: "Résidence\u00A0:"
        },
        education: {
          title: "Études\u00A0:"
        },
        gender: {
          title: "Genre\u00A0:"
        }
      },
      password: {
        newPassword: {
          title: "Mon mot de passe",
          updatedDate: "La dernière mise à jour de votre mot de passe était le\u00A0: {0}",
          updatedDateNever: "jamais",
          currentPassword: "Mot de passe actuel\u00A0:",
          showCurrentPassword: "Afficher le mot de passe actuel",
          newPassword: "Nouveau mot de passe\u00A0:",
          showNewPassword: "Afficher le nouveau mot de passe",
          confirmPassword: "Confirmer le mot de passe\u00A0:",
          showConfirmPassword: "Afficher la confirmation du mot de passe",
          invalidPasswordError: "Mot de passe invalide",
          popup: {
            title: "Le mot de passe a été mis à jour",
            description: "Votre mot de passe a été mis à jour avec succès."
          }
        },
        passwordRecovery: {
          title: "FR Password Recovery",
          secretQuestion: "FR Secret Question:",
          secretAnswer: "FR Secret Answer:",
          secretQuestionUpdatedConfirmation: "FR Your secret question has been updated successfully"
        }
      },
      preferences: {
        title: "Mes préférences",
        description: "Modifier mes préférences.",
        notifications: {
          title: "Notifications",
          checkBoxOne: "Toujours envoyer un courriel à mon adresse courriel principale",
          checkBoxTwo: "Toujours envoyer un courriel à ma deuxième adresse courriel"
        },
        display: {
          title: "Affichage",
          checkBoxOne: "Permettre à d'autres de voir ma photo de profil",
          checkBoxTwo: "Masquer les icônes d'infobulles"
        },
        accessibility: {
          title: "Accessibilité\u00A0:",
          description:
            "Ajuster la police de caractères, la taille de la police et l'espacement de texte qui sera affiché\u00A0:"
        }
      },
      permissions: {
        title: "Mes droits et autorisations",
        description: "Vous n'avez pas besoin d'autres droits et autorisations pour faire un test.",
        systemPermissionInformation: {
          title: "Information concernant votre rôle d'utilisateur",
          addPermission: "Obtenir des droits et autorisations",
          pending: "En attente",
          superUser: {
            title: "Super Utilisateur",
            permission: "L'utilisateur détient tous les rôles et accès dans le système"
          }
        },
        addPermissionPopup: {
          title: "Demande de droits et d'autorisations",
          description:
            "Veuillez inscrire les renseignements demandés et cocher le ou les rôles d'utilisateur(s) requis.",
          gocEmail: "Adresse courriel du gouvernement du Canada\u00A0:",
          gocEmailTooltip:
            "Vous devez entrer votre adresse courriel du gouvernement du Canada pour vous inscrire.",
          gocEmailError: "Doit être une adresse courriel valide",
          pri: "CIDP\u00A0:",
          militaryNbr: "Numéro de matricule\u00A0:",
          priError: "Doit être un CIDP ou un numéro de matricule valide",
          militaryNbrError: "Doit êtreun numéro de matricule valide",
          supervisor: "Nom du superviseur\u00A0:",
          supervisorError: "Le nom entré contient un ou plusieurs caractères invalides",
          supervisorEmail: "Adresse courriel du superviseur\u00A0:",
          supervisorEmailError: "Doit être une adresse courriel valide",
          rationale: "Raison de la demande\u00A0:",
          rationaleError: "La raison de la demande doit être entrée",
          permissions: "Obtenir les droits pour ces rôles d'utilisateurs\u00A0:",
          permissionsError: "Au moins un rôle d'utilisateur doit être sélectionné"
        },
        noPermissions: "Vous n’avez aucune autorisation."
      },
      testPermissions: {
        title: "Autorisations d'accès aux tests",
        description:
          "En tant qu'administrateur de tests, vous avez les autorisations d'accès requises pour faire passer les tests suivants\u00A0:",
        table: {
          title: "Accès aux tests",
          column1: "Version de test",
          column1OrderItem: "Classer par version du test",
          column2: "Numéro de commande du test",
          column2OrderItem: "Classer par numéro de commande de test",
          column3: "Numéro du processus d'évaluation",
          column3OrderItem: "Classer par numéro de processus d'évaluation",
          column4: "Date d'échéance",
          column4OrderItem: "Classer par date d'échéance"
        }
      },
      saveButton: "Sauvegarder"
    },

    // ETTA Page
    systemAdministrator: {
      title: "Bienvenue {0} {1}.",
      containerLabelEtta: "Activités d’affaires",
      containerLabelRD: "Activités R&D",
      containerLabelBoth: "Activités",
      sideNavItems: {
        dashboard: "Tableau de bord",
        activeTests: "Tests actifs",
        testAccessCodes: "Codes d'accès aux tests",
        permissions: "Droits et autorisations",
        itemBanks: "Gestion des banques d'items",
        testAccesses: "Gestion des accès aux tests",
        incidentReports: "Rapports d'incidents",
        scoring: "Résultats",
        reScoring: "Révision des résultats",
        reports: "Rapports",
        userLookUp: "Liste des utilisateurs",
        systemAlert: "Alertes de système"
      },
      permissions: {
        description: "Attribuer et gérer les droits et autorisations des utilisateurs.",
        tabs: {
          permissionRequests: {
            title: "Demandes de droits et d'autorisations",
            table: {
              columnOne: "Rôle d'utilisateur",
              columnTwo: "Nom et coordonnées de l'utilisateur",
              columnThree: "Date de la demande",
              columnFour: "Action",
              noPermission: "Il n'y a aucune demande de droits et d'autorisations actuellement."
            },
            viewButton: "Afficher",
            viewButtonAccessibility: "Afficher {0} {1}",
            popup: {
              title: "Demande de droits et d'autorisations",
              description:
                "Veuillez accepter ou refuser la demande de droits et d'autorisations formulée par {0}.",
              permissionRequested: "Obtenir les droits pour ce rôle d'utilisateur\u00A0:",
              deniedReason: "Raison du refus\u00A0:",
              denyButton: "Refuser",
              grantButton: "Accepter",
              grantPermissionErrors: {
                notEmptyDeniedReasonError:
                  "Ce champ doit être libre pour pouvoir accepter la demande",
                missingDeniedReasonError: "La raison du refus doit être entrée",
                usernameDoesNotExistError: "Ce nom d'utilisateur n'existe plus"
              }
            }
          },
          activePermissions: {
            title: "Droits et autorisations actifs",
            table: {
              permission: "Rôle d'utilisateur",
              user: "Nom et coordonnées de l'utilisateur",
              action: "Action",
              actionButtonLabel: "Modifier",
              actionButtonAriaLabel: "Modifier {0} {1}"
            },
            viewEditDetailsPopup: {
              title: "Modifier ou supprimer le rôle d'un utilisateur",
              description: "Modifier ou supprimer les droits et autorisations de {0}.",
              deleteButton: "Supprimer",
              saveButton: "Modifier",
              permission: "Rôle d'utilisateur\u00A0:",
              reasonForModification: "Raison de la modification ou de la supression\u00A0:",
              reasonForModificationError:
                "Vous devez fournir une raison pour modifier ou supprimer cette demande"
            },
            deletePermissionConfirmationPopup: {
              title: "Confirmer la supression des droits et autorisations",
              systemMessageDescription:
                "Vous êtes sur le point de révoquer les droits et autorisations {0} de {1}. Souhaitez-vous poursuivre ?"
            },
            updatePermissionDataConfirmationPopup: {
              title: "Confirmation de la modification",
              description: "Les droits et autorisations de {0} ont été mis à jour avec succès."
            }
          }
        }
      },
      itemBanks: {
        title: "Gestion des banques d'items",
        description:
          "Créer des banques d'items et attribuer des accès à ces banques aux développeurs de test.",
        tabs: {
          activeItemBanks: {
            title: "Banques d'items actives",
            newItemBankButton: "Nouvelle banque d'items",
            table: {
              column1: "Banque d'items",
              column2: "Dernière modification",
              column3: "Organisation",
              column4: "Actions",
              noData: "Votre recherche n'a généré aucun résultat"
            },
            newItemBankPopup: {
              title: "Créer une banque d'items",
              description:
                "Remplissez les champs ci-dessous pour créer une nouvelle banque d'items.",
              itemBankDepartment: "Organisation propriétaire\u00A0:",
              itemBankPrefix: "Préfixe de la banque d'items\u00A0:",
              itemBankLanguage: "Langue des items\u00A0:",
              itemBankComments: "Commentaires (facultatif)\u00A0:",
              rightButton: "Créer",
              successMessage: "La nouvelle banque d'items a été créée.",
              itemBankAlreadyExistsError:
                "Ce préfixe est déjà utilisé par une autre banque d'items."
            },
            selectedItemBank: {
              backToRdOperationsButton: "Retour à l'administration des banques d'items",
              backToRdOperationsPopup: {
                title: "Retourner à l'administration des banques d'items ?",
                warningDescription: "Les données qui non pas été sauvegardées seront perdues.",
                description: "Voulez-vous continuer ?"
              },
              tabs: {
                selectedItemBank: {
                  title: "Banque d'items {0}",
                  sideNavigationItems: {
                    ItemBankAccesses: {
                      ItemTitle: "Accès à la banque d'items",
                      title: "Banque d'items\u00A0: {0}",
                      description:
                        "Gérer les accès des développeurs de test à cette banque d'items.",
                      itemBankPrefix: "Préfixe de la banque d'items\u00A0:",
                      itemBankAlreadyExistsError:
                        "Ce préfixe est déjà utilisé par une autre banque d'items.",
                      itemBankNameEn: "Nom anglais de la banque d'items\u00A0:",
                      itemBankNameFr: "Nom français de la banque d'items\u00A0:",
                      itemBankDepartment: "Organisation propriétaire\u00A0:",
                      itemBankLanguage: "Langue des items\u00A0:",
                      addTestDeveloperButton: "Ajouter un développeur de test",
                      addTestDeveloperPopup: {
                        title: "Ajouter un développeur de test à la banque d'items {0}",
                        description:
                          "Sélectionnez un développeur de test à ajouter à la banque d'items.",
                        testDeveloperLabel: "Développeur de test\u00A0:",
                        addButton: "Développeur de test"
                      },
                      deleteTestDeveloperPopup: {
                        title: "Supprimer un développeur de test de la banque d'items {0}.",
                        description:
                          "Cette action supprimera l'accès de {0} à cette banque d'items.",
                        deleteButton: "Supprimer"
                      },
                      table: {
                        column1: "Développeur de test",
                        column2: "Type d'accès",
                        column3: "Actions",
                        deleteButtonTooltip: "Supprimer",
                        deleteButtonAriaLabel: "Supprimer l'accès de {0} à cette banque d'items",
                        noData: "Aucun développeur de test n'est associé à cette banque d'items."
                      }
                    }
                  }
                }
              }
            }
          },
          archivedItemBanks: {
            title: "Banques d'items archivées"
          }
        }
      },
      testAccesses: {
        title: "Gestion des accès aux tests",
        description:
          "Attribuer et gérer les droits d'accès aux tests pour les administrateurs de tests.",
        tabs: {
          assignTestAccesses: {
            version: {
              title: "{0} v{1}"
            },
            testDescription: "{0} v{1}",
            title: "Attribuer des droits d'accès aux tests",
            testOrderNumberLabel: "Numéro de commande du test\u00A0:",
            staffingProcessNumber: "Numéro du processus d'évaluation\u00A0:",
            departmentMinistry: "Code de l'organisation\u00A0:",
            isOrg: "Code d'organisation SIF\u00A0:",
            isRef: "Code de référence SIF\u00A0:",
            billingContact: "Nom du responsable de la facturation\u00A0:",
            billingContactInfo: "Courriel de contact pour les candidats\u00A0:",
            users: "Administrateur(s) de tests\u00A0:",
            usersCurrentValueAccessibility: "Les administrateurs de tests sélectionnés sont\u00A0:",
            expiryDate: "Date d'échéance\u00A0:",
            testAccesses: "Accès aux tests\u00A0:",
            testAccess: "Accès au test\u00A0:",
            testAccessesError: "Au moins un test doit être sélectionné",
            testAccessAlreadyExistsError:
              "{0} a déja accès au {1} dont numéro de commande de test est {2}.",
            emptyFieldError:
              "Ce champ est vide ou le nombre de caractères dépasse la limite permise",
            mustBeAnEmailError: "Doit être une adresse courriel valide",
            unableToAccessOrderingServiceError:
              "Impossible d'accéder au service de commande de tests. Veuillez entrer les données financières manuellement.",
            searchButton: "Recherche",
            manuelEntryButton: "Entrée manuelle",
            noTestOrderNumberFound:
              "Numéro de commande introuvable; le numéro entré doit correspondre à un numéro de commande existant",
            refreshButton: "Réinitialiser les champs",
            refreshConfirmationPopup: {
              title: "Voulez-vous effacer tous les champs ?",
              description: "Cette action effacera tous les champs.",
              confirm: "Effacer"
            },
            saveButton: "Sauvegarder",
            saveConfirmationPopup: {
              title: "Confirmer l'attribution des droits d'accès aux tests",
              usersSection:
                "Noms et coordonnées des utilisateurs recevant des droits d'accès\u00A0:",
              testsSection: "Tests pour lesquels les droits sont attribués\u00A0:",
              orderInfoSection: {
                title: "Renseignements sur la commande des tests\u00A0:",
                testOrderNumber: "Numéro de commande du test\u00A0:",
                staffingProcessNumber: "Numéro du processus d'évaluation\u00A0:",
                departmentMinistry: "Code de l'organisation\u00A0:"
              },
              expiriDateSection: "Date d'échéance de l'accès aux tests\u00A0:",
              confirmation: "Veuillez réviser et confirmer les accès aux tests.",
              testAccessesGrantedConfirmationPopup: {
                title: "Confirmation des droits d'accès",
                description: "Les droits d'accès aux tests ont été attribués avec succès."
              }
            }
          },
          activeTestAccesses: {
            title: "Droits d'accès actifs",
            table: {
              testAdministrator: "Administrateur de tests",
              test: "Test",
              orderNumber: "Numéro de commande du test",
              expiryDate: "Date d'échéance",
              action: "Action",
              actionButtonLabel: "Modifier",
              actionButtonAriaLabel:
                "Modifier les données sur l'accès aux tests de l'utilisateur {0} {1}, test {2}, numéro de commande {3}, date d'échéance {4}."
            },
            viewTestPermissionPopup: {
              title: "Modifier ou supprimer les accès au test",
              description: `Modifier ou supprimer les accès au test de {0}.`,
              username: "Adresse courriel du gouvernement du Canada\u00A0:",
              deleteButtonAccessibility: "Supprimer",
              saveButtonAccessibility: "Modifier"
            },
            deleteConfirmationPopup: {
              title: "Confirmer la supression des accès au test",
              systemMessageDescription:
                "Vous êtes sur le point de révoquer les accès au(x) test(s) {0} (Numéro de commande du test\u00A0: {1}) du compte de {2}. Souhaitez-vous poursuivre ?"
            },
            saveConfirmationPopup: {
              title: "Confirmation de la modification",
              description: "Les accès au test de {0} ont été mis à jour avec succès."
            }
          },
          orderlessTestAccesses: {
            title: "Accès aux tests non commandés",
            addNewOrderlessTaButton: "Ajouter un AT",
            addOrderlessTaPopup: {
              title: "Ajouter un (des) administrateur(s) de test",
              description:
                "Sélectionnez le (les) administrateur(s) que vous désirez ajouter à la liste des AT avec des accès aux tests non commandés.",
              testAdministrators: "Administrateur(s) de test\u00A0:",
              invalidTestAdministrators: '"{0}" a déjà accès aux tests non commandés',
              departmentMinistry: "Organisation\u00A0:",
              successfulMessage: "Le (les) administrateur(s) de test ont été ajoutés avec succès."
            },
            confirmDeleteOrderlessTaPopup: {
              title: "Confirmer la suppression des accès aux tests",
              systemMessageDescription:
                "Tous les accès au test non commandé pour {0} seront révoqués.",
              description: "Êtes-vous sûr de vouloir continuer ?"
            },
            table: {
              column_1: "Administrateur de test",
              column_2: "Organisation",
              column_3: "Actions",
              viewTooltip: "Afficher les détails",
              deleteTooltip: "Supprimer tous les accès aux tests"
            },
            selectedOrderlessTestAdministrator: {
              backToOrderlessTestAdministratorSelectionButton:
                "Retour aux accès aux tests non commandés",
              tabTitle: "Accès aux tests non commandés ({0})",
              sideNavItem1Title: "Accès aux tests de l'AT",
              testAccesses: {
                title: "Ajouter/retirer les accès aux tests",
                departmentUpdateTitle: "Modifier l'organisation de l'AT",
                departmentLabel: "Organisation\u00A0:",
                description: "Attribuer ou révoquer les accès au test de {0} {1}.",
                table: {
                  column_1: "Code du test",
                  column_2: "Nom du test",
                  column_3: "Assigné",
                  noResultsFound: "Aucun accès aux tests trouvés"
                }
              },
              saveConfirmationPopup: {
                title: "Modifier l'organisation de l'AT",
                systemMessageDescription: "L'organisation de l'AT a été mis à jour avec succès."
              }
            }
          }
        }
      },
      testAccessCodes: {
        description: "Affichage de tous les codes d'accès des tests actifs",
        table: {
          columnOne: "Code d'accès au test",
          columnTwo: "Test",
          columnThree: "Commande de test / Numéro de référence",
          columnFour: "Administrateur de tests",
          columnFive: "Date de création",
          columnSix: "Action",
          noTestAccessCode: "Il n'y a aucun code d'accès au test",
          deleteButtonAccessibility: "Supprimer {0}"
        }
      },
      activeTests: {
        description: "Affichage de tous les candidats actifs",
        table: {
          columnOne: "Nom du candidat",
          columnTwo: "Administrateur de tests",
          columnThree: "Version de test",
          columnFour: "Commande de test / Numéro de référence\u00A0:",
          columnFive: "Statut",
          columnSix: "Durée du test",
          columnSeven: "Action",
          noActiveCandidates: "Il n'y a aucun candidat actif.",
          actionButton: "Invalider",
          invalidateButtonAccessibility: "Invalider {0}"
        },
        invalidatePopup: {
          title: "Invalider l'accès au test",
          description: "Voulez-vous vraiment invalider l'accès au test de {0} {1} ?",
          actionButton: "Invalider l'accès",
          reasonForInvalidatingTheTest: "Raison de l'invalidation de l'accès au test\u00A0:"
        },
        validatePopup: {
          title: "Rétablir l'accès au test",
          description: "Voulez-vous vraiment rétablir l'accès au test de {0} {1}\u00A0?",
          actionButton: "Rétablir l'accès",
          reasonForInvalidatingTheTest: "Raison du rétablissement de l'accès au test\u00A0:"
        }
      },
      userLookUp: {
        description: "Affichage de la liste de tous les utilisateurs",
        table: {
          columnOne: "Courriel",
          columnTwo: "Prénom",
          columnThree: "Nom de famille",
          columnFour: "Date de naissance",
          columnFive: "Action",
          viewButton: "Afficher",
          noUsers: "Il n'y a aucun candidat actif."
        },
        viewButtonAccessibility: "Afficher {0} {1}"
      },
      userLookUpDetails: {
        sideNavigationItems: {
          userPersonalInfo: "Renseignements personnels",
          myTests: "Tests",
          rightsAndPermissions: "Droits et autorisations",
          testPermissions: "Test Permissions"
        },
        backToUserLookUp: "Retour aux résultats de la recherche",
        containerLabel: "Renseignements sur l'utilisateur ({0} {1})",
        testPermissions: {
          title: "Autorisations d'accès aux tests",
          description:
            "En tant qu'administrateur de tests, {0} {1} a accès aux tests suivants\u00A0:",
          noTestPermissions: "L'utilisateur n'a aucune autorisation d'accès aux tests."
        },
        userRights: {
          title: "Droits et autorisations",
          description: "Affichage des droits de {0} {1}",
          noRights: "L'utilisateur n'a aucune autorisation."
        },
        Tests: {
          title: "Tests de {0} {1}",
          description: "Affichage de tous les tests effectués par {0} {1}",
          table: {
            columnOne: "Test",
            columnTwo: "Date du test",
            columnThree: "Date à partir de laquelle une reprise est possible",
            columnFour: "Résultat",
            columnFive: "Statut",
            columnSix: "Invalidé",
            columnSeven: "Action",
            noTests: "Il n'y a aucun test pour cet utilisateur.",
            viewTestDetailsTooltip: "Afficher les renseignements sur le test",
            invalidateTestTooltip: "Invalider le test",
            validateTestTooltip: "Annuler l'invalidation",
            invalidateButton: "Invalider",
            viewButtonAccessibility: "Afficher {0}",
            invalidateButtonAccessibility: "Invalider {0}",
            validateButtonAccessibility: "Annuler l'invalidation du {0}",
            downloadCandidateActionReport: "Générer le rapport sur les actions du candidat",
            candidateActionReportAria: "Générer les Rapport sur les actions du candidat pour le {0}"
          },
          viewSelectedTestPopup: {
            title: "Afficher les renseignements sur le test",
            description: "Affichage de tous les renseignements sur le test",
            testName: "Nom du test\u00A0:",
            testCode: "Code du test\u00A0:",
            testAccessCode: "Code d'accès au test\u00A0:",
            testOrderNumber: "Numéro de commande du test\u00A0:",
            assessmentProcessOrReferenceNb: "Processus d'évaluation / Numéro de référence\u00A0:",
            testAdministrator: "Administrateur de tests\u00A0:",
            totalScore: "Score total\u00A0:",
            version: "Version OÉC\u00A0:",
            invalidTestReason: "Raison de l'invalidation du test\u00A0:"
          }
        },
        userPersonalInfo: {
          title: "Renseignements personnels",
          firstName: "Prénom\u00A0:",
          lastName: "Nom de famille\u00A0:",
          primaryEmailAddress: "Adresse courriel primaire\u00A0:",
          secondaryEmailAddress: "Adresse courriel secondaire\u00A0:",
          dateOfBirth: "Date de naissance\u00A0:",
          psrsApplicantId: "Numéro d'identification du SRFP\u00A0:",
          pri: "CIDP ou numéro de matricule\u00A0:",
          lastLogin: "Date de la dernière ouverture de session\u00A0:",
          lastPasswordChange: "Date de la dernière modification du mot de passe\u00A0:",
          accountCreationDate: "Date de création du compte\u00A0:",
          additionalInfo: "Renseignements supplémentaires",
          currentEmployer: "Employeur actuel\u00A0:",
          organization: "Organisation\u00A0:",
          group: "Groupe\u00A0:",
          subGroup: "Sous-groupe\u00A0:",
          level: "Niveau\u00A0:",
          residence: "Résidence\u00A0:",
          education: "Études\u00A0:",
          gender: "Genre\u00A0:",
          noPersonalInformation: "Il n'y a aucun renseignement personnel relatif à l'utilisateur.",
          noAdditionalInfomration: "Il n'y a aucun renseignement supplémentaire sur l'utilisateur."
        }
      },
      reports: {
        title: "Rapports",
        description:
          "Générer un rapport de résultats pour une commande de test que vous avez administré."
      },
      systemAlerts: {
        title: "Alertes de système",
        description: "Gérer les alertes de système",
        tabs: {
          activeSystemAlerts: {
            title: "Alertes de système actives",
            addNewSystemAlertButton: "Ajouter une alerte de système",
            addSystemAlertPopup: {
              title: "Ajouter une alerte de système",
              fields: {
                title: "Titre\u00A0:",
                criticality: "Criticité\u00A0:",
                startDate: "Date de début\u00A0:",
                endDate: "Date de fin\u00A0:",
                messageText: "Texte du message\u00A0:",
                messageTextTabs: {
                  english: "Anglais",
                  french: "Français"
                }
              },
              buttons: {
                rightButton: "Ajouter"
              },
              successfulMessage: "L'alerte de système a été ajoutée avec succès."
            },
            archiveSystemAlertPopup: {
              title: "Archiver les alertes de système",
              systemMessageTitle: "Archiver cette alerte de système ?",
              systemMessageMessage:
                "L'archivage de cette alerte de système la supprimera de la page de connexion et la déplacera vers les alertes de système archivées.",
              archiveSuccessfullyMessage: "L'alerte de système a été archivée avec succès.",
              archiveRightButton: "Archiver"
            },
            table: {
              column_1: "Alerte",
              column_2: "Criticité",
              column_3: "Date d'entrée en vigueur",
              column_4: "Date de fin",
              column_5: "Actions",
              viewTooltip: "Afficher",
              modifyTooltip: "Modifier",
              archiveToolTip: "Archiver"
            },
            viewPopupTitle: "Afficher l'alerte de système",
            modifyPopupTitle: "Modifier l'alerte de système",
            modifyRightButton: "Modifier",
            modifySuccessfullyMessage: "L'alerte de système a été modifiée avec succès."
          },
          archivedSystemAlerts: {
            title: "Alertes de système archivées",
            table: {
              column_1: "Alerte",
              column_2: "Criticité",
              column_3: "Date d'entrée en vigueur",
              column_4: "Date de fin",
              column_5: "Actions",
              viewTooltip: "Afficher",
              cloneTooltip: "Cloner"
            },
            viewPopupTitle: "Afficher l'alerte de système",
            clonePopupTitle: "Cloner l'alerte de système",
            cloneSuccessfullyMessage: "L'alerte de système a été clonée avec succès."
          }
        }
      }
    },

    // Scorer Pages
    scorer: {
      situationTitle: "Situation",
      assignedTest: {
        displayOptionLabel: "Affichage\u00A0:",
        displayOptionAccessibility: "Nombre de résultats par page",
        displayOptionCurrentValueAccessibility: "La valeur sélectionnée est\u00A0:",
        table: {
          assignedTestId: "FR Assigned Test ID",
          completionDate: "FR Completion Date",
          testName: "FR Test Name",
          testLanguage: "FR Test Language",
          action: "Action",
          actionButtonLabel: "FR Score Test",
          en: "Anglais",
          fr: "Français",
          actionButtonAriaLabel:
            "FR Score test of: assigned test id: {0}, completion date: {1}, test name: {2}, test language: {3}."
        },
        noResultsFound: "FR There are no tests waiting to be scored."
      },
      sidebar: {
        title: "FR Scoring Panel",
        competency: "FR Competency",
        rational: "FR Rating rationale",
        placeholder: "FR Input rationale here..."
      },
      submitButton: "FR Submit Final Scores",
      unsupportedScoringType:
        "FR The scoring type: {0} is not supported. The test definition must be incorrect.",
      selectQuestion: "FR Select a question to continue",
      competency: {
        bar: {
          title: "FR Behavioural Achored Ratings",
          description:
            "FR Below is a complete view of the BAR for this question and competency. Once selected, you can use the arrows to navigate, expand, and collapse information.",
          barButton: "FR View BAR"
        }
      }
    },

    // Status Page
    statusPage: {
      title: "Statut de OÉC",
      logo: "Logo Thunder CAT",
      welcomeMsg:
        "Page de statut interne afin de déterminer rapidement l'état / la santé de l'outil d'évaluation des candidats.",
      versionMsg: "Version de l'Outil d'évaluation des candidats\u00A0: ",
      gitHubRepoBtn: "Répertoire GitHub",
      serviceStatusTable: {
        title: "Statut des services",
        frontendDesc: "La Face avant de l'application est construite et utilisée avec succès",
        backendDesc: "La Face arrière de l'application réussit les demandes API avec succès",
        databaseDesc: "La Base de données réussit les demandes API avec succès"
      },
      systemStatusTable: {
        title: "Statut du système",
        javaScript: "JavaScript",
        browsers: "Chrome, Firefox, Edge",
        screenResolution: "Résolution d'écran minimum de 1024 x 768"
      },
      additionalRequirements:
        "De plus, les exigences suivantes doivent être respectées pour l’utilisation de cette application dans un centre de test.",
      secureSockets: "Chiffrement Secure Socket Layer (SSL) activé",
      fullScreen: "Mode plein écran activé",
      copyPaste: "Fonction copier-coller activée"
    },

    // Settings Dialog
    settings: {
      systemSettings: "Paramètres d'affichage pour l'accessibilité",
      zoom: {
        title: "Zoom avant et zoom arrière (+ / -)",
        description:
          "Utilisez les paramètres de votre navigateur pour modifier le niveau de zoom. Vous pouvez également appuyer simultanément sur les touches CTRL et + / - de votre clavier pour effectuer un zoom avant ou un zoom arrière."
      },
      fontSize: {
        title: "Taille de la police",
        label: "Taille de la police\u00A0:",
        description: "Veuillez sélectionner la taille du texte\u00A0:"
      },
      fontStyle: {
        title: "Police",
        label: "Police\u00A0:",
        description: "Sélectionner une nouvelle police pour votre texte\u00A0:"
      },
      lineSpacing: {
        title: "Espacement de texte",
        label: "Espacement de texte\u00A0:",
        description: "Activer l'accessibilité pour l'espacement de texte."
      },
      color: {
        title: "Couleur du texte et de l’arrière plan",
        description:
          "Utilisez les paramètres de votre navigateur pour modifier la couleur du texte et de l’arrière-plan."
      }
    },

    // Multiple Choice  test
    mcTest: {
      questionList: {
        questionIdShort: "Q{0}",
        questionIdLong: "Question {0}",
        reviewButton: "Cocher pour révision",
        markedReviewButton: "Décocher la révision",
        seenQuestion: "Cette question a été vue",
        unseenQuestion: "Cette question n'a pas été vue",
        answeredQuestion: "Une réponse a été entrée pour cette question",
        unansweredQuestion: "Aucune réponse n'a été entrée pour cette question",
        reviewQuestion: "Cette question a été cochée pour révision."
      },
      finishPage: {
        homeButton: "Retour à la page d'accueil"
      }
    },
    // eMIB Test
    emibTest: {
      // Home Page
      homePage: {
        testTitle: "Échantillon de la BRG-e",
        welcomeMsg: "Bienvenue dans le test pratique de BRG-e"
      },

      // HowTo Page
      howToPage: {
        tipsOnTest: {
          title: "Conseils pour répondre à la BRG-e",
          part1: {
            description:
              "La BRG-e vous présente des situations qui vous donneront l’occasion de démontrer les compétences clés en matière de leadership. Voici quelques conseils qui vous aideront à fournir aux évaluateurs l’information dont ils ont besoin pour évaluer votre rendement par rapport à ces compétences clés en leadership\u00A0:",
            bullet1:
              "Répondez à toutes les questions posées dans les courriels que vous avez reçus. Notez également que des situations pour lesquelles on ne pose pas de questions précises peuvent être abordées dans plusieurs courriels. Vous profiterez ainsi de toutes les occasions qui vous sont offertes de démontrer ces compétences.",
            bullet2:
              "N’hésitez pas à présenter vos recommandations si nécessaire, et ce, même s’il s’agit seulement de réflexions préliminaires. S’il le faut, vous pouvez ensuite noter les autres renseignements dont vous auriez besoin pour en arriver à une décision finale.",
            bullet3:
              "Si dans certaines situations vous croyez avoir besoin de parler en personne avec quelqu’un avant de prendre une décision, indiquez les renseignements dont vous avez besoin et comment cela pourrait influencer votre décision.",
            bullet4:
              "Utilisez uniquement l’information fournie dans les courriels et l’information contextuelle. Ne tirez aucune conclusion fondée sur la culture de votre propre organisation. Évitez de faire des suppositions qui ne sont pas raisonnablement corroborées par l’information contextuelle ou les courriels.",
            bullet5:
              "Les situations présentées dans la BRG-e s’inscrivent dans un domaine particulier afin de vous donner suffisamment de contexte pour y répondre.  Pour être efficaces, vos réponses doivent démontrer la compétence ciblée, et non la connaissance du domaine en question."
          },
          part2: {
            title: "Autres renseignements importants",
            bullet1:
              "Le contenu de vos courriels, l'information fournie dans votre liste de tâches et les justifications des mesures prises seront évalués. Le contenu du bloc-notes ne sera pas évalué.",
            bullet2:
              "Votre rédaction ne sera pas évaluée. Aucun point ne sera enlevé pour les fautes d’orthographe, de grammaire, de ponctuation ou pour les phrases incomplètes. Votre rédaction devra toutefois être suffisamment claire pour que les évaluateurs comprennent la situation que vous traitez et vos principaux arguments.",
            bullet3: "Vous pouvez répondre aux courriels dans l’ordre que vous désirez.",
            bullet4: "Vous êtes responsable de la gestion de votre temps."
          }
        },
        testInstructions: {
          title: "Instructions du test",
          hiddenTabNameComplementary: "Sous «\u00A0Instructions\u00A0»",
          onlyOneTabAvailableForNowMsg:
            "Veuillez noter qu’il n’y a qu’un seul onglet principal disponible pour l’instant. Dès que vous commencerez le test, les autres principaux onglets deviendront disponibles.",
          para1:
            "Lorsque vous commencez le test, lisez d’abord l’information contextuelle qui décrit votre poste et l’organisation fictive où vous travaillez. Nous vous recommandons de prendre environ 10 minutes pour la lire. Passez ensuite à la boîte de réception pour lire les courriels que vous avez reçus et prenez des mesures pour y répondre, comme si vous étiez gestionnaire dans cette organisation fictive.",
          para2:
            "Lorsque vous serez dans la boîte de réception, vous aurez accès aux éléments suivants\u00A0:",
          bullet1: "les instructions du test;",
          bullet2:
            "l’information contextuelle décrivant votre rôle en tant que gestionnaire et l’organisation fictive où vous travaillez;",
          bullet3: "un bloc-notes pouvant servir de papier brouillon.",
          step1Section: {
            title: "Étape 1 — Répondre aux courriels",
            description:
              "Vous pouvez répondre aux courriels que vous avez reçus de deux façons\u00A0: en écrivant un courriel ou en ajoutant des tâches à votre liste de tâches. Ces deux façons de répondre sont décrites ci-dessous, suivies d’exemples.",
            part1: {
              title: "Exemple d’un courriel que vous avez reçu\u00A0:",
              para1:
                "Vous trouverez ci-dessous deux façons de répondre au courriel. Vous pouvez choisir l’une ou l’autre des deux options présentées, ou combiner les deux. Les réponses fournies ne sont présentées que pour illustrer comment utiliser chacune des deux façons de répondre. Elles ne démontrent pas nécessairement les compétences clés en leadership qui sont évaluées dans cette situation."
            },
            part2: {
              title: "Ajouter une réponse par courriel",
              para1:
                "Vous pouvez écrire un courriel pour répondre à celui que vous avez reçu dans votre boîte de réception. Votre réponse écrite devrait refléter la façon dont vous répondriez en tant que gestionnaire.",
              para2:
                "Vous pouvez utiliser les fonctions suivantes\u00A0: répondre, répondre à tous ou transférer. Si vous choisissez de transférer un courriel, vous aurez accès à un répertoire qui contient tous vos contacts. Vous pouvez écrire autant de courriels que vous le souhaitez pour répondre à un courriel ou pour gérer des situations que vous remarquez dans plusieurs des courriels reçus."
            },
            part3: {
              title: "Exemple d’une réponse par courriel\u00A0:"
            },
            part4: {
              title: "Ajouter une tâche à la liste de tâches",
              para1:
                "En plus de répondre par courriel, ou au lieu d’en écrire un, vous pouvez ajouter des tâches à la liste de tâches. Une tâche représente une mesure que vous comptez prendre pour gérer une situation présentée dans les courriels. Voici des exemples de tâches\u00A0: planifier une rencontre ou communiquer avec un collègue afin d’obtenir de l’information. Assurez-vous de fournir suffisamment d’information dans votre description de la tâche pour que nous sachions à quelle situation vous répondez. Vous devez également préciser quelles mesures vous comptez prendre et qui devra participer à cette tâche. Vous pouvez en tout temps retourner à un courriel pour ajouter, supprimer ou modifier des tâches."
            },
            part5: {
              title: "Exemple d’ajout d’une tâche à la liste de tâches\u00A0:"
            },
            part6: {
              title: "Comment choisir une façon de répondre",
              para1:
                "Il n’y a pas de bonne ou de mauvaise façon de répondre. Lorsque vous répondez à un courriel, vous pouvez\u00A0:",
              bullet1: "envoyer un ou des courriels;",
              bullet2: "ajouter une ou des tâches à votre liste de tâches;",
              bullet3: "faire les deux.",
              para2:
                "C’est le contenu de vos réponses qui sera évalué, et non la façon de répondre (c’est-à-dire si vous avez répondu par courriel ou en ajoutant une tâche à votre liste de tâches). Par conséquent, vos réponses doivent être suffisamment détaillées et claires pour que les évaluateurs puissent évaluer comment vous gérez la situation. Par exemple, si vous prévoyez organiser une réunion, vous devez préciser de quoi il y sera question.",
              para3Part1: "Si vous décidez d’écrire un courriel ",
              para3Part2: "et",
              para3Part3:
                " d’ajouter une tâche à votre liste de tâches pour répondre à un courriel que vous avez reçu, vous n’avez pas à répéter la même information aux deux endroits. Par exemple, si vous mentionnez dans un courriel que vous organiserez une réunion avec un collègue, vous n’avez pas à ajouter cette réunion à votre liste de tâches."
            }
          },
          step2Section: {
            title: "Étape 2 — Justifier les mesures prises (facultatif)",
            description:
              "Après avoir écrit un courriel ou ajouté une tâche, vous pouvez expliquer votre raisonnement dans la section réservée à cet effet, si vous le souhaitez. Cette section se situe au bas des réponses par courriel et des tâches. La justification des mesures prises est facultative. Notez que vous pouvez choisir d’expliquer certaines mesures que vous avez prises tandis que d’autres ne nécessitent pas d’explications supplémentaires. De même, vous pouvez décider de justifier les mesures prises lorsque vous répondez à certains courriels, et non à d'autres. Cela s'applique également aux tâches de la liste de tâches.",
            part1: {
              title:
                "Exemple d’une réponse par courriel accompagnée de justifications des mesures prises\u00A0:"
            },
            part2: {
              title:
                "Exemple d’une liste de tâches accompagnée de justifications des mesures prises\u00A0:"
            }
          },
          exampleEmail: {
            to: "T.C. Bernier (gestionnaire, Équipe de l’assurance de la qualité)",
            from: "Geneviève Bédard (directrice, Unité de recherche et innovation)",
            subject: "Préparer Mary à son affectation",
            date: "vendredi 4 novembre",
            body: "Bonjour T.C.,\n\nJ’ai été ravie d’apprendre qu’une de tes analystes de l’assurance de la qualité, Mary Woodside, avait accepté une affectation de six mois avec mon équipe, à compter du 2 janvier. Je crois comprendre qu’elle a de l’expérience en enseignement et en utilisation d’outils pédagogiques modernes dans le cadre de son travail antérieur de professeure au niveau collégial. Mon équipe a besoin d’aide pour mettre au point des techniques d’enseignement novatrices qui favorisent la productivité et le bien-être général des employés. Je pense donc que l’expérience de Mary sera un bon atout pour l’équipe.\n\nY a-t-il des domaines dans lesquels tu aimerais que Mary acquière plus d’expérience, laquelle serait utile lors de son retour dans ton équipe ? Je tiens à maximiser les avantages de l’affectation pour nos deux équipes.\n\nAu plaisir de recevoir tes commentaires.\n\nGeneviève"
          },
          exampleEmailResponse: {
            emailBody:
              "Bonjour Geneviève,\n\nJe suis d’accord que nous devrions planifier l’affectation de Mary afin que nos deux équipes en tirent parti. Je suggère de former Mary à la synthèse de données provenant de sources multiples. Cela l’aiderait à élargir ses compétences et serait utile à mon équipe à son retour. De même, les membres de ton équipe pourraient profiter de son expérience en enseignement. Je la consulterai directement, car j’aimerais connaître son avis à ce sujet. Je te recontacterai au cours de la semaine, quand j’aurai plus d’information à te fournir.\n\nCela dit, quelles sont tes attentes ? Y a-t-il présentement certains défis ou des aspects particuliers de la dynamique de ton équipe dont il faudrait tenir compte ? Avant de rencontrer Mary pour discuter de son affectation, j’aimerais tenir compte de tous les facteurs, tels que les besoins actuels, les défis et la dynamique de ton équipe.\n\nMerci.\n\nT.C.",
            reasonsForAction:
              "Je compte rencontrer Mary pour discuter de ses attentes concernant l’affectation et pour établir des objectifs clairs. Je veux qu’elle se sente motivée et sache ce qu’on attend d’elle, afin de l’aider à se préparer en conséquence. J’examinerai également ses objectifs pour l’année afin de m’assurer que ce que je propose cadre bien avec son plan de perfectionnement professionnel."
          },
          exampleTaskResponse: {
            task: "- Répondre au courriel de Geneviève\u00A0:\n     > lui proposer de former Mary à la synthèse de l’information provenant de sources multiples afin qu’elle puisse élargir ses compétences;\n     >	lui demander quelles sont ses attentes et quels sont les défis de son équipe afin que je puisse tenir compte de tous les facteurs pour déterminer comment son équipe pourrait profiter de l’expérience de Mary dans le domaine de la formation;\n     > l’informer que je travaille à recueillir plus d’information auprès de Mary, et que je lui ferai part de mes suggestions d’ici la fin de la semaine.\n- Organiser une réunion avec Mary pour discuter des objectifs de son affectation et veiller à ce qu’elle se sente motivée et à ce qu’elle sache ce qui est attendu d’elle.\n- Consulter les objectifs passés et actuels de Mary pour vérifier que ce que je propose est conforme à son plan de perfectionnement professionnel.",
            reasonsForAction:
              "Former Mary à la synthèse de l’information provenant de sources multiples serait avantageux pour mon équipe, qui a besoin de consolider l’information recueillie auprès de nombreuses sources. Demander à Geneviève quels sont ses attentes et ses défis m’aidera à mieux préparer Mary et à m’assurer que l’affectation sera avantageuse pour nos deux équipes."
          }
        },
        evaluation: {
          title: "Évaluation",
          bullet1:
            "Les mesures que vous prenez et les explications que vous donnez seront prises en compte dans l’évaluation de votre rendement pour chacune des compétences clés en leadership (décrites ci-dessous). On évaluera à quel point ces mesures et explications démontrent les compétences clés en leadership.",
          bullet2:
            "L’efficacité des mesures prises sera évaluée. Le niveau d’efficacité est déterminé par l’effet positif ou négatif que ces mesures auraient sur la résolution des situations présentées, et par l’étendue de cet effet.",
          bullet3:
            "Vos réponses seront également évaluées en fonction de leur contribution à l’atteinte des objectifs organisationnels présentés dans l’information contextuelle.",
          keyLeadershipCompetenciesSection: {
            title: "Compétences clés en leadership",
            para1Title: "Créer une vision et une stratégie\u00A0: ",
            para1:
              "Les gestionnaires contribuent à définir l’avenir et à tracer la voie à suivre. Pour ce faire, ils tiennent compte du contexte. Ils mettent à contribution leurs connaissances. Ils obtiennent et intègrent de l’information provenant de diverses sources pour la mise en œuvre d’activités concrètes. Ils considèrent divers points de vue et consultent d’autres personnes, au besoin. Les gestionnaires assurent l’équilibre entre les priorités organisationnelles et contribuent à améliorer les résultats.",
            para2Title: "Mobiliser les personnes\u00A0: ",
            para2:
              "Les gestionnaires inspirent et motivent les personnes qu’ils dirigent. Ils gèrent le rendement de leurs employés et leur offrent de la rétroaction constructive et respectueuse pour encourager et rendre possible l’excellence en matière de rendement. Ils dirigent en donnant l’exemple et se fixent des objectifs personnels qui sont plus exigeants que ceux qu’ils établissent pour les autres.",
            para3Title: "Préserver l’intégrité et le respect\u00A0: ",
            para3:
              "Les gestionnaires donnent l’exemple sur le plan des pratiques éthiques, du professionnalisme et de l’intégrité personnelle, en agissant dans l’intérêt du Canada, des Canadiens et des Canadiennes. Ils créent des environnements de travail inclusifs, empreints de respect et de confiance, où les conseils judicieux sont valorisés. Ils encouragent les autres à faire part de leurs points de vue, tout en encourageant la collégialité.",
            para4Title: "Collaborer avec les partenaires et les intervenants\u00A0: ",
            para4:
              "Les gestionnaires cherchent à obtenir, de façon délibérée et ingénieuse, un grand éventail de perspectives. Lorsqu’ils établissent des partenariats, ils gèrent les attentes et visent à atteindre un consensus. Ils font preuve d’ouverture et de souplesse afin d’améliorer les résultats et apportent une perspective globale de l’organisation à leurs interactions. Les gestionnaires reconnaissent le rôle des partenaires dans l’obtention des résultats.",
            para5Title: "Promouvoir l’innovation et orienter le changement\u00A0: ",
            para5:
              "Les gestionnaires créent un environnement propice aux idées audacieuses, à l’expérimentation et à la prise de risques en toute connaissance de cause. Lors de la mise en œuvre d’un changement, ils maintiennent l’élan, surmontent la résistance et anticipent les conséquences. Ils perçoivent les revers comme une bonne occasion de comprendre et d’apprendre.",
            para6Title: "Obtenir des résultats\u00A0: ",
            para6:
              "Les gestionnaires s’assurent de répondre aux objectifs de l’équipe en gérant les ressources. Ils prévoient, planifient et surveillent les progrès, et font des ajustements au besoin. Ils démontrent leur connaissance du contexte lors de la prise de décisions. Les gestionnaires assument la responsabilité personnelle à l’égard de leurs actions et des résultats de leurs décisions."
          }
        }
      },

      // Background Page
      background: {
        hiddenTabNameComplementary: "Sous «\u00A0Information contextuelle\u00A0»",
        orgCharts: {
          link: "Description de l'image",
          ariaLabel: "Description de l'image de l'Organigramme Équipe",
          treeViewInstructions:
            "Ci-dessous, vous trouverez la vue arborescente de l’organigramme. Une fois sélectionné, vous pouvez utiliser les touches fléchées pour la navigation, l’expansion et l’effondrement de l’information."
        }
      },

      // Inbox Page
      inboxPage: {
        tabName: "Sous «\u00A0Boîte de réception\u00A0»",
        emailId: " courriel ",
        subject: "Objet",
        to: "À",
        from: "De",
        date: "Date",
        addReply: "Ajouter une réponse par courriel",
        addTask: "Ajouter une réponse par liste de tâches",
        yourActions: `Vous avez répondu avec {0} courriel(s) et {1} liste(s) de tâches`,
        editActionDialog: {
          addEmail: "Ajouter une réponse par courriel",
          editEmail: "Modifier la réponse par courriel",
          addTask: "Ajouter à la liste de tâches",
          editTask: "Modifier la tâche",
          save: "Sauvegarder la réponse"
        },
        characterLimitReached: "(Limite atteinte)",
        emailCommons: {
          to: "À\u00A0:",
          toFieldSelected: "Champ À choisi",
          cc: "Cc\u00A0:",
          ccFieldSelected: "Champ CC choisi",
          currentSelectedPeople: "Les personnes choisies actuellement sont\u00A0: {0}",
          currentSelectedPeopleAreNone: "Aucunes",
          reply: "répondre",
          replyAll: "répondre à tous",
          forward: "transmettre",
          editButton: "Modifier la réponse",
          deleteButton: "Supprimer la réponse",
          originalEmail: "Courriel d’origine",
          toAndCcFieldsPlaceholder: "Sélectionnez à partir du carnet d’adresses",
          yourResponse: "Votre réponse"
        },
        addEmailResponse: {
          selectResponseType:
            "Veuillez choisir la manière dont vous souhaitez répondre au courriel d’origine\u00A0:",
          response: "Votre réponse par courriel\u00A0: limite de {0} caractères",
          reasonsForAction:
            "Justification des mesures prises (facultatif)\u00A0: limite de {0} caractères",
          emailResponseTooltip: "Rédiger une réponse au courriel que vous avez reçu.",
          reasonsForActionTooltip:
            "Dans cette section, vous pouvez expliquer pourquoi vous avez pris une certaine mesure en réponse à une situation, si vous souhaitez fournir des renseignements supplémentaires.",
          invalidToFieldError: "Ce champ ne peut être vide"
        },
        emailResponse: {
          title: "Réponse par courriel no. ",
          description: "Pour cette réponse, vous avez choisi de\u00A0:",
          response: "Votre réponse par courriel\u00A0:",
          reasonsForAction: "Votre justification des mesures prises (facultatif)\u00A0:"
        },
        addEmailTask: {
          header: "Courriel no {0}: {1}",
          task: "Votre réponse par liste de tâches\u00A0: limite de {0} caractères",
          reasonsForAction:
            "Justification des mesures prises (facultatif)\u00A0: limite de {0} caractères"
        },
        taskContent: {
          title: "Réponse par liste de tâches no. ",
          task: "Votre réponse par liste de tâches\u00A0:",
          taskTooltipPart1:
            "Une action que vous comptez prendre pour résoudre une situation dans les courriels.",
          taskTooltipPart2:
            "Exemple\u00A0: planifier une réunion, demander de l'information à un collègue.",
          reasonsForAction: "Votre justification des mesures prises (facultatif)\u00A0:",
          reasonsForActionTooltip:
            "Dans cette section, vous pouvez expliquer pourquoi vous avez pris une certaine mesure en réponse à une situation, si vous souhaitez fournir des renseignements supplémentaires."
        },
        deleteResponseConfirmation: {
          title: "Êtes-vous certain de vouloir annuler cette réponse ?",
          systemMessageDescription:
            "Votre réponse ne sera pas sauvegardée si vous continuez. Si vous souhaitez enregistrer votre réponse, vous pouvez y retourner. Toutes vos réponses peuvent être modifiées ou supprimées avant de soumettre le test.",
          description:
            "Si vous ne voulez pas sauvegarder la réponse, cliquez sur le bouton «\u00A0Supprimer la réponse\u00A0»."
        }
      },

      // Confirmation Page
      confirmationPage: {
        submissionConfirmedTitle: "Félicitations ! Votre test a été soumis.",
        feedbackSurvey:
          "Nous aimerions recevoir vos commentaires sur l’évaluation. Veuillez remplir ce {0} facultatif avant de vous déconnecter et de quitter.",
        optionalSurvey: "sondage de 15 minutes",
        surveyLink: "https://surveys-sondages.psc-cfp.gc.ca/s/se.ashx?s=25113745259E9113&c=fr-CA",
        logout:
          "Pour des raisons de sécurité, assurez-vous de fermer votre session dans le coin supérieur droit de cette page. Vous pouvez discrètement recueillir vos effets personnels et quitter la séance de test. Si vous avez des questions ou des préoccupations au sujet de votre test, veuillez communiquer avec {0}.",
        thankYou: "Nous vous remercions d’avoir terminé votre évaluation. Bonne chance !"
      },

      // Quit Confirmation Page
      quitConfirmationPage: {
        title: "Vous avez quitté le test",
        instructionsTestNotScored1: "Votre test ",
        instructionsTestNotScored2: "ne sera pas corrigé.",
        instructionsRaiseHand:
          "Veuillez lever la main. L’administrateur de tests viendra vous donner d’autres directives.",
        instructionsEmail:
          "Si vous avez des questions ou des préoccupations au sujet de votre test, veuillez communiquer avec {0}."
      },

      // Timeout Page
      timeoutConfirmation: {
        timeoutConfirmedTitle: "Votre temps de test est écoulé...",
        timeoutSaved:
          "Vos réponses ont été sauvegardées et soumises aux fins de notation. Veuillez prendre note que l’information dans le bloc-notes n’est pas sauvegardée.",
        timeoutIssue:
          "S’il y a eu un problème, veuillez en informer votre administrateur de tests. Cliquez sur «\u00A0Continuer\u00A0» pour quitter cette séance de test et pour recevoir d’autres instructions."
      },

      // Test tabs
      tabs: {
        instructionsTabTitle: "Instructions",
        backgroundTabTitle: "Information contextuelle",
        inboxTabTitle: "Boîte de réception",
        disabled: "Vous ne pouvez accéder à la boîte de courriel avant d'avoir commencé le test."
      },

      // Test Footer
      testFooter: {
        timer: {
          timer: "Chronomètre",
          timeLeft: "Temps restant dans la séance de test\u00A0:",
          timerHidden: "Minuterie cachée.",
          timerHide: "Masquer le chronomètre",
          timeRemaining: "Temps restant\u00A0: {0} heures, {1} minutes, {2} secondes"
        },
        pauseButton: "Mettre le test en pause",
        unpauseButton: "Reprendre le test",
        unpausePopup: {
          title: "Reprendre le test",
          description1: "Êtes-vous sûr de vouloir reprendre votre test ?",
          description2:
            "Le temps restant dans votre banque de pauses sera recalculé lorsque vous sélectionnerez Reprendre le test.",
          unpauseButton: "Reprendre le test"
        },
        submitTestPopupBox: {
          title: "Soumettre votre test",
          warning: {
            title: "Avertissement\u00A0: your notebook will not be saved.",
            message1:
              "Cette étape mettra fin à votre séance de test. Assurez-vous d'avoir répondu à toutes les questions avant de soumettre votre test. Une fois le test soumis, vous ne pourrez plus apporter de changements. Le contenu du bloc-notes ne sera ni transmis ni corrigé.",
            message2: "Souhaitez-vous poursuivre ?"
          },
          description:
            "Si vous êtes prêt(e) à soumettre votre test pour la notation, cliquez sur le bouton «\u00A0Soumettre le test\u00A0». La séance de test sera fermée et vous recevrez d’autres instructions."
        },
        quitTestPopupBox: {
          title: "Quitter le test",
          description1:
            "Êtes-vous certain de vouloir quitter ce test ? Toutes vos réponses seront perdues, vous ne pourrez les récupérer et vous perdrez l'accès au test.",
          description2: "Pour quitter, vous devez consentir aux énoncés suivants\u00A0:",
          checkboxOne: "Je choisis de quitter ce test;",
          checkboxTwo: "Je comprends que mon test ne sera pas noté;",
          checkboxThree:
            "Je comprends qu'il pourrait y avoir une période d'attente avant de pouvoir refaire ce test;"
        }
      }
    },

    // Screen Reader
    ariaLabel: {
      mainMenu: "Menu Principal",
      tabMenu: "Menu des onglets de la BRG-e",
      instructionsMenu: "Menu des instructions",
      languageToggleBtn: "bouton-de-langue-a-bascule",
      authenticationMenu: "Menu d'authentification",
      emailHeader: "en-tête du courriel",
      responseDetails: "détails de la réponse",
      reasonsForActionDetails: "motifs de l'action",
      taskDetails: "détails sur la ou les tâches",
      emailOptions: "options de messagerie",
      taskOptions: "options de tâche",
      taskTooltip: "infobulle de tâche",
      emailResponseTooltip: "Infobulle pour les réponses par courriel",
      reasonsForActionTooltip: "infobulle des motifs de l'action",
      passwordConfirmationRequirements: "Il doit correspondre à votre mot de passe",
      dobDayField: "Champ Journée sélectionné",
      dobMonthField: "Champ Mois sélectionné",
      dobYearField: "Champ Année sélectionné",
      emailsList: "Liste des courriels",
      questionList: "List des questions",
      topNavigationSection: "Barre de navigation du haut",
      sideNavigationSection: "Menu de navigation latérale",
      quitTest: "Quitter le test",
      selectedPermission: "Permission sélectionnée\u00A0:",
      description: "Description\u00A0:",
      pendingStatus: "Statut en attente\u00A0:",
      scorerPanel: "FR Scorer Panel",
      navExpandButton: "Navigation à bascule"
    },

    // Commons
    commons: {
      psc: "Commission de la fonction publique",
      loading: "Chargement",
      nextButton: "Suivant",
      backButton: "Retour",
      enterEmibSample: "Passer à l’échantillon du test de la BRG-e",
      enterEmibReal: "Passer aux instructions du test",
      resumeEmibReal: "Rentrez la BRG-e",
      startTest: "Commencer le test",
      resumeTest: "Reprendre le test",
      confirmStartTest: {
        aboutToStart: "Vous êtes sur le point de commencer le test.",
        breakBankTitle: "Banque de pauses\u00A0:",
        breakBankWarning: "Vous avez une banque de pauses de {0} pour ce test.",
        testSectionTimeTitle: "Durée totale\u00A0:",
        newtimerWarning: "Vous aurez un total de {0} pour compléter le test.",
        additionalTimeWarning: "Ceci inclut {0} de temps supplémentaire.",
        wishToProceed: "Souhaitez-vous poursuivre ?",
        timeUnlimited: "temps illimité",
        confirmProceed: "Oui, je souhaite poursuivre."
      },
      submitTestButton: "Soumettre votre test",
      quitTest: "Quitter le test",
      returnToTest: "Reprendre le test",
      returnToResponse: "Retourner à la réponse",
      passStatus: "Réussi",
      failStatus: "Échoue",
      error: "Erreur",
      success: "Succès",
      info: "Information",
      warning: "Avertissement !",
      enabled: "Activé",
      disabled: "Désactivé",
      backToTop: "Haut de la page",
      notepad: {
        title: "Bloc-notes",
        hideNotepad: "Masquer le bloc-notes",
        placeholder: "Mettez vos notes ici..."
      },
      calculator: {
        title: "Calculatrice",
        errorMessage: "Erreur",
        calculatorResultAccessibility: "La calculatrice affiche",
        backspaceButton: "Espace arrière",
        hideCalculator: "Masquer la calculatrice"
      },
      cancel: "Annuler",
      cancelChanges: "Annuler les modifications",
      cancelResponse: "Annuler la réponse",
      addButton: "Ajouter",
      deleteButton: "Supprimer",
      saveButton: "Sauvegarder",
      saveDraftButton: "Sauvegarder",
      applyButton: "Appliquer",
      duplicateButton: "Dupliquer",
      previewButton: "Aperçu",
      close: "Fermer",
      login: "Ouvrir une session",
      ok: "OK",
      deleteConfirmation: "Confirmer la suppression",
      continue: "Continuer",
      sendRequest: "Envoyer la requête",
      submit: "Soumettre",
      na: "S/O",
      valid: "Valide",
      invalid: "Invalidé",
      confirm: "Confirmer",
      english: "Anglais",
      french: "Français",
      bilingual: "Bilingue",
      status: {
        checkedIn: "Enregistré",
        ready: "Prêt",
        preTest: "Pré-test",
        active: "En test",
        locked: "Verrouillé",
        paused: "En pause",
        neverStarted: "Non commencé",
        inactivity: "Inactivité",
        timedOut: "Temps écoulé",
        quit: "Quitté",
        submitted: "Soumis",
        unassigned: "Non attribué",
        invalid: "Invalide"
      },
      seconds: "seconde(s)",
      minutes: "minute(s)",
      hours: "heure(s)",
      pleaseSelect: "Veuillez sélectionner une option",
      none: "Aucun",
      convertedScore: {
        invalidScoreConversion: "Conversion invalide du résultat",
        pass: "Réussite",
        fail: "Échec",
        none: "Aucun résultat",
        notScored: "Aucune correction",
        invalid: "Invalide"
      },
      yes: "Oui",
      no: "Non",
      preferNotToAnswer: "Préfère ne pas répondre",
      active: "Actif",
      inactive: "Inactif",
      moveUp: "FR Move Up",
      moveDown: "FR Move Down",
      default: "Par défaut",
      tools: "Outils",
      pagination: {
        nextPageButton: "Page suivante",
        previousPageButton: "Page précédente",
        skipToPagesLink: "Aller directement à la liste des pages",
        breakAriaLabel: "FR Move multiple pages"
      },
      saved: "Sauvegarder",
      fieldCannotBeEmptyErrorMessage: "Ce champ ne peut être vide",
      currentLanguage: "fr",
      true: "VRAI",
      false: "FAUX",
      next: "Suivant",
      previous: "Précédent",
      upload: "Téléverser",
      all: "Tous",
      generate: "Regénérer",
      unchanged: "FR (unchanged)",
      discard: "FR Discard"
    }
  }
};

const LOCALIZE = new LocalizedStrings(LOCALIZE_OBJ);

export default LOCALIZE;
export { LOCALIZE_OBJ };
