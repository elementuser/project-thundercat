import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import "./css/lib/aurora.min.css";
import "./css/cat-theme.css";
import Settings from "./components/commons/Settings";
import Translation from "./components/commons/Translation";
import LOCALIZE from "./text_resources";
import psc_logo_light_en from "./images/psc_logo_light_en.png";
import psc_logo_light_fr from "./images/psc_logo_light_fr.png";
import { Navbar, Nav, Row } from "react-bootstrap";
import QuitTest from "./components/commons/QuitTest";
import SelectLanguage from "./SelectLanguage";
import { LANGUAGES } from "./modules/LocalizeRedux";
import { PATH } from "./App";
import { setNavBarHeight } from "./modules/TestSectionRedux";
import ToolsButton from "./components/commons/ToolsButton";
import CustomButton, { THEME } from "./components/commons/CustomButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars } from "@fortawesome/free-solid-svg-icons";

const styles = {
  navBar: {
    position: "unset"
  },
  row: {
    padding: "10px 0 10px 0"
  },
  col: {
    padding: "0 10px 0 10px"
  }
};

class TestNavBar extends Component {
  static propTypes = {
    quitTestHidden: PropTypes.bool,
    // Props from Redux
    currentLanguage: PropTypes.string,
    isTestActive: PropTypes.bool.isRequired,
    authenticateAction: PropTypes.func,
    logoutAction: PropTypes.func,
    handleQuitTest: PropTypes.func,
    testNameId: PropTypes.string
  };

  constructor(props) {
    super(props);
    this.testNavBarHeightRef = React.createRef();
  }

  state = {
    setFocusOnQuitTestButton: false
  };

  componentDidMount = () => {
    // updating nav bar height redux state
    if (this.testNavBarHeightRef.current) {
      this.props.setNavBarHeight(this.testNavBarHeightRef.current.clientHeight);
    }
  };

  componentDidUpdate = () => {
    // updating nav bar height redux state
    if (this.testNavBarHeightRef.current) {
      this.props.setNavBarHeight(this.testNavBarHeightRef.current.clientHeight);
    }
  };

  render() {
    // Determine if user has already selected a language.
    const isLanguageSelected = this.props.currentLanguage !== "";
    return (
      <div>
        <a href="#main-content" className="visually-hidden" id="skip-to-main-content-link-id">
          {LOCALIZE.mainTabs.skipToMain}
        </a>
        {!isLanguageSelected && <SelectLanguage />}
        {isLanguageSelected && window.location.pathname.indexOf(PATH.sampleTests) === -1 && (
          <div>
            <div>
              <Navbar
                ref={this.testNavBarHeightRef}
                style={styles.navBar}
                bg="dark"
                fixed="top"
                variant="dark"
                id="unit-test-active-test-navbar"
                expand={"xl"}
              >
                <Navbar.Brand>
                  <img
                    alt=""
                    src={
                      this.props.currentLanguage === LANGUAGES.french
                        ? psc_logo_light_fr
                        : psc_logo_light_en
                    }
                    width="370px"
                    height="27.75px"
                    className="d-inline-block align-top"
                  />
                </Navbar.Brand>
                <Navbar.Toggle
                  label={LOCALIZE.ariaLabel.navExpandButton}
                  aria-controls="basic-navbar-nav"
                  ariaLabel={LOCALIZE.ariaLabel.navExpandButton}
                  style={{
                    fontSize: this.props.accommodations.fontSize
                  }}
                >
                  <CustomButton
                    label={
                      <div>
                        <FontAwesomeIcon icon={faBars} />
                      </div>
                    }
                    buttonTheme={
                      this.props.isTestActive || this.props.isTestInvalidated
                        ? THEME.PRIMARY_IN_TEST
                        : THEME.SECONDARY
                    }
                    customStyle={{
                      marginRight: "16px",
                      minWidth: 0
                    }}
                  />
                </Navbar.Toggle>
                <Navbar.Collapse id="basic-navbar-nav">
                  <Row className="justify-content-end w-100" style={styles.row}>
                    <Nav className="mr-auto" />
                    {!this.props.quitTestHidden && (
                      <QuitTest
                        id="unit-test-quit-test-navbar-button"
                        setFocusOnQuitTestButton={this.state.setFocusOnQuitTestButton}
                        handleQuitTest={this.props.handleQuitTest}
                      />
                    )}
                    <ToolsButton calledInRealTest={true} />
                    <Settings
                      isTestActive={this.props.isTestActive}
                      isTestInvalidated={this.props.isTestInvalidated}
                      id="unit-test-settings-navbar-button"
                    />
                    <Translation
                      isTestActive={this.props.isTestActive}
                      isTestInvalidated={this.props.isTestInvalidated}
                      id="unit-test-translation-navbar-button"
                    />
                  </Row>
                </Navbar.Collapse>
              </Navbar>
            </div>
          </div>
        )}
      </div>
    );
  }
}

export { TestNavBar as UnconnectedTestNavBar };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    isTestActive: state.testStatus.isTestActive,
    authenticated: state.login.authenticated,
    testNameId: state.testStatus.currentTestId,
    isTestInvalidated: state.testStatus.isTestInvalidated,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setNavBarHeight
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(TestNavBar);
