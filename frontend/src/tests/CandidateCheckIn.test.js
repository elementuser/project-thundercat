import React from "react";
import { mount, shallow } from "enzyme";
import { UnconnectedCandidateCheckIn } from "../CandidateCheckIn";
import LOCALIZE from "../text_resources";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";

const mockStore = configureStore();

const initialState = {
  localize: {
    language: "en"
  },
  accommodations: {
    fontFamily: "Nunito Sans"
  }
};

function getComponent(props) {
  return mount(
    shallow(
      <Provider store={mockStore(initialState)}>
        <UnconnectedCandidateCheckIn {...props} accommodations={{ fontSize: "16px" }} />
      </Provider>
    ).get(0)
  );
}
describe("renders right objects depending on the checkedIn state", () => {
  it("when checkedIn is set to false", () => {
    const wrapper = getComponent({ isCheckedIn: false, isLoaded: true });
    expect(wrapper.find("#unit-test-check-in-button").exists()).toEqual(true);
    expect(wrapper.find("#unit-test-check-in-loading-button").exists()).toEqual(false);
  });

  it("when checkedIn is set to false and is not loaded", () => {
    const wrapper = getComponent({ isCheckedIn: false, isLoaded: false });
    expect(wrapper.find("#unit-test-check-in-button").exists()).toEqual(false);
    expect(wrapper.find("#unit-test-check-in-loading-button").exists()).toEqual(true);
  });
});
