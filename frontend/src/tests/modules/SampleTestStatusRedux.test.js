import testStatus, {
  setCurrentTest,
  activateTest,
  startTest,
  initialState,
  deactivateTest,
  quitTest,
  timeoutTest,
  resetTestStatusState,
  PAGES
} from "../../modules/SampleTestStatusRedux";
describe("test activation state and moves to emib tabs", () => {
  it("test sent and end current test", () => {
    // "view" the test
    const currentTestAction = setCurrentTest(1, "emiPizzaTest", null);
    const currentTestState = testStatus(initialState, currentTestAction);
    expect(currentTestState).toEqual({
      isTestActive: false,
      isTestStarted: false,
      currentPage: PAGES.preTest,
      startTime: null,
      timeLimit: null,
      currentAssignedTestId: 1,
      currentTestId: "emiPizzaTest"
    });

    // submit, quit, and timeout the test
    const submitTestAction = deactivateTest();
    const submitTestState = testStatus(currentTestState, submitTestAction);
    expect(submitTestState).toEqual({
      isTestActive: false,
      isTestStarted: false,
      currentPage: PAGES.confirm,
      startTime: null,
      timeLimit: null,
      currentAssignedTestId: null,
      currentTestId: null
    });

    const quitTestAction = quitTest();
    const quitTestState = testStatus(currentTestState, quitTestAction);
    expect(quitTestState).toEqual({
      isTestActive: false,
      isTestStarted: false,
      currentPage: PAGES.quit,
      startTime: null,
      timeLimit: null,
      currentAssignedTestId: null,
      currentTestId: null
    });

    const timeoutTestAction = timeoutTest();
    const timeoutTestState = testStatus(currentTestState, timeoutTestAction);
    expect(timeoutTestState).toEqual({
      isTestActive: false,
      isTestStarted: false,
      currentPage: PAGES.timeout,
      startTime: null,
      timeLimit: null,
      currentAssignedTestId: null,
      currentTestId: null
    });
  });
  it("test activate test", () => {
    const action1 = activateTest();
    expect(testStatus(initialState, action1)).toEqual({
      isTestActive: true,
      isTestStarted: false,
      currentPage: PAGES.preTest,
      startTime: null,
      timeLimit: null,
      currentAssignedTestId: null,
      currentTestId: null
    });
  });
  it("test start test with undefined time", () => {
    const action1 = startTest(undefined, 0, new Date(Date.now()).toString());
    const newState = testStatus(initialState, action1);

    expect(newState.isTestStarted).toEqual(true);
    expect(newState.currentPage).toEqual(PAGES.background);
    expect(newState.startTime).not.toEqual(null);
    expect(new Date(newState.startTime).getTime()).toBeGreaterThan(1);
    expect(new Date(newState.startTime).getTime()).toBeLessThanOrEqual(Date.now());
    expect(newState.timeLimit).toEqual(null);
  });
  it("test start test with defined time", () => {
    const action1 = startTest(300, undefined, new Date(Date.now()).toString());
    const newState = testStatus(initialState, action1);

    expect(newState.isTestStarted).toEqual(true);
    expect(newState.currentPage).toEqual(PAGES.background);
    expect(newState.startTime).not.toEqual(null);
    expect(new Date(newState.startTime).getTime()).toBeGreaterThan(1);
    expect(new Date(newState.startTime).getTime()).toBeLessThanOrEqual(Date.now());
    expect(newState.timeLimit).toEqual(300);
  });
  it("test deactivate test and moves to confirm page", () => {
    const action2 = deactivateTest();
    expect(testStatus(initialState, action2)).toEqual({
      isTestActive: false,
      isTestStarted: false,
      currentPage: PAGES.confirm,
      startTime: null,
      timeLimit: null,
      currentAssignedTestId: null,
      currentTestId: null
    });
  });
  it("test quits the test and moves to quit page", () => {
    const action3 = quitTest();
    expect(testStatus(initialState, action3)).toEqual({
      isTestActive: false,
      isTestStarted: false,
      currentPage: PAGES.quit,
      startTime: null,
      timeLimit: null,
      currentAssignedTestId: null,
      currentTestId: null
    });
  });
  it("test times out the test and moves to timeout page", () => {
    const action3 = timeoutTest();
    expect(testStatus(initialState, action3)).toEqual({
      isTestActive: false,
      isTestStarted: false,
      currentPage: PAGES.timeout,
      startTime: null,
      timeLimit: null,
      currentAssignedTestId: null,
      currentTestId: null
    });
  });
});

describe("test status reset state", () => {
  it("test status should equal initial state", () => {
    const currentTestAction = setCurrentTest(1, "emiPizzaTest", null);
    const currentTestState = testStatus(initialState, currentTestAction);
    expect(currentTestState).toEqual({
      isTestActive: false,
      isTestStarted: false,
      currentPage: PAGES.preTest,
      startTime: null,
      timeLimit: null,
      currentAssignedTestId: 1,
      currentTestId: "emiPizzaTest"
    });
    const reset = resetTestStatusState();
    const state = testStatus(currentTestState, reset);
    expect(state).toEqual({ ...initialState });
  });
});
