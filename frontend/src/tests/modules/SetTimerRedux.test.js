import timer, {
  initialState,
  updateTimerState,
  setDefaultTimes,
  triggerTimeUpdate,
  updateValidTimerState
} from "../../modules/SetTimerRedux";

// updateTimerState
describe("update timer action", () => {
  const currentTimerArray = [];
  it("should get the updated currentTimer state (single object)", () => {
    currentTimerArray.push({ hours: "03", minutes: "30" });
    const action = updateTimerState(currentTimerArray);
    expect(timer(initialState, action)).toEqual({
      currentTimer: [{ hours: "03", minutes: "30" }],
      defaultTimes: [],
      validTimer: true,
      timeUpdated: false
    });
  });

  it("should get the updated currentTimer state (multiple objects)", () => {
    currentTimerArray.push({ hours: "01", minutes: "15" });
    const action = updateTimerState(currentTimerArray);
    expect(timer(initialState, action)).toEqual({
      currentTimer: [
        { hours: "03", minutes: "30" },
        { hours: "01", minutes: "15" }
      ],
      defaultTimes: [],
      validTimer: true,
      timeUpdated: false
    });
  });
});

// setDefaultTimes
describe("set default times action", () => {
  it("should get the updated defaultTimes state", () => {
    const action = setDefaultTimes([180, 210]);
    expect(timer(initialState, action)).toEqual({
      currentTimer: [],
      defaultTimes: [180, 210],
      validTimer: true,
      timeUpdated: false
    });
  });
});

// triggerTimeUpdate
describe("trigger time update action", () => {
  it("should get the timeUpdated state set to true", () => {
    const action = triggerTimeUpdate();
    expect(timer(initialState, action)).toEqual({
      currentTimer: [],
      defaultTimes: [],
      validTimer: true,
      timeUpdated: true
    });
  });
});

// updateValidTimerState
describe("update valid timer state action", () => {
  it("should get the validTimer state set to false", () => {
    const action = updateValidTimerState(false);
    expect(timer(initialState, action)).toEqual({
      currentTimer: [],
      defaultTimes: [],
      validTimer: false,
      timeUpdated: false
    });
  });
});
