import React from "react";
import { shallow } from "enzyme";
import { UnconnectedTestNavBar as TestNavBar } from "../TestNavBar";
import SelectLanguage from "../SelectLanguage";

it("renders select language page if not selected", () => {
  const wrapper = shallow(
    <TestNavBar
      currentLanguage={""}
      updateBackendTest={() => {}}
      isTestActive={false}
      accommodations={{
        fontSize: "16px"
      }}
    />
  );
  const selectLanguage = <SelectLanguage />;
  expect(wrapper.containsMatchingElement(selectLanguage)).toEqual(true);
});

it("does not render select language page if not selected", () => {
  const wrapper = shallow(
    <TestNavBar
      currentLanguage={"en"}
      updateBackendTest={() => {}}
      isTestActive={false}
      accommodations={{
        fontSize: "16px"
      }}
    />
  );
  const selectLanguage = <SelectLanguage />;
  expect(wrapper.containsMatchingElement(selectLanguage)).toEqual(false);
});

it("renders navbar on active test", () => {
  const wrapper = shallow(
    <TestNavBar
      currentLanguage={"en"}
      updateBackendTest={() => {}}
      isTestActive={true}
      accommodations={{
        fontSize: "16px"
      }}
    />
  );
  expect(wrapper.find("#unit-test-active-test-navbar").exists()).toEqual(true);
});
it("renders quit test button on active test", () => {
  const wrapper = shallow(
    <TestNavBar
      currentLanguage={"en"}
      updateBackendTest={() => {}}
      isTestActive={true}
      accommodations={{
        fontSize: "16px"
      }}
    />
  );
  expect(wrapper.find("#unit-test-quit-test-navbar-button").exists()).toEqual(true);
});
it("renders settings button on active test", () => {
  const wrapper = shallow(
    <TestNavBar
      currentLanguage={"en"}
      updateBackendTest={() => {}}
      isTestActive={true}
      accommodations={{
        fontSize: "16px"
      }}
    />
  );
  expect(wrapper.find("#unit-test-settings-navbar-button").exists()).toEqual(true);
});
it("renders translation button on active test", () => {
  const wrapper = shallow(
    <TestNavBar
      currentLanguage={"en"}
      updateBackendTest={() => {}}
      isTestActive={true}
      accommodations={{
        fontSize: "16px"
      }}
    />
  );
  expect(wrapper.find("#unit-test-translation-navbar-button").exists()).toEqual(true);
});
