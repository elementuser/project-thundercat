import React from "react";
import { shallow } from "enzyme";
import { UnconnectedMyTests as MyTests } from "../MyTests";
import LOCALIZE from "../text_resources";
import AlertMessage, { ALERT_TYPE } from "../components/commons/AlertMessage";

describe("renders component content", () => {
  const wrapper = shallow(<MyTests firstName="Testing" lastName="MyTests" />, {
    disableLifecycleMethods: true
  });
  it("renders page title", () => {
    const title = (
      <h1 id="user-welcome-message" className="green-divider">
        {LOCALIZE.formatString(LOCALIZE.myTests.title, "Testing", "MyTests")}
      </h1>
    );
    expect(wrapper.containsMatchingElement(title)).toEqual(true);
  });

  it("renders alert message", () => {
    const alertMessage = (
      <div>
        <AlertMessage
          alertType={ALERT_TYPE.light}
          message={
            <>
              <p>{LOCALIZE.myTests.alertCatTestsOnly1}</p>
              <p>{LOCALIZE.myTests.alertCatTestsOnly2}</p>
              <p>{LOCALIZE.myTests.alertCatTestsOnly3}</p>
            </>
          }
          usesMarkdownOrHtmlTags={false}
        />
      </div>
    );
    expect(wrapper.containsMatchingElement(alertMessage)).toEqual(true);
  });
});
