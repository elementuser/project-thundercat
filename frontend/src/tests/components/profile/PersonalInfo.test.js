import React from "react";
import { shallow } from "enzyme";
import { unconnectedPersonalInfo as PersonalInfo } from "../../../components/profile/PersonalInfo";
import LOCALIZE from "../../../text_resources";

describe("renders component content", () => {
  // mock data
  const firstName = "FirstName";
  const lastName = "LastName";
  const primaryEmail = "firstname.lastname@email.ca";
  const dateOfBirthDay = { value: 10, label: "10" };
  const dateOfBirthMonth = { value: 12, label: "12" };
  const dateOfBirthYear = { value: 1980, label: "1980" };
  const pri = "12345679";

  const mockGetUserExtendedProfile = jest.fn();
  mockGetUserExtendedProfile.mockImplementation(() =>
    Promise.resolve({
      payload: {
        data: {
          ok: true,
          body: {
            id: 0,
            current_employer_id: 0,
            organization_id: 0,
            group_id: 0,
            level_id: 0,
            residence_id: 0,
            education_id: 0,
            gender_id: 0,
            minority_id: 0,
            user: 0
          }
        }
      }
    })
  );

  const wrapper = shallow(
    <PersonalInfo
      firstName={""}
      lastName={""}
      psrsAppID={""}
      primaryEmail={""}
      dateOfBirth={""}
      pri={pri}
      militaryNbr={""}
      accommodations={{ fontSize: "16px" }}
      currentEmployer={0}
      organization={0}
      group={0}
      level={0}
      residence={0}
      education={0}
      gender={0}
      updateUserExtendedProfile={() => {}}
      setUserInformation={() => {}}
      getUserExtendedProfile={mockGetUserExtendedProfile}
      updateExtendedProfileState={() => {}}
      updateCurrentEmployer={() => {}}
      updateOrganization={() => {}}
      updateGroup={() => {}}
      updateLevel={() => {}}
      updateResidence={() => {}}
      updateEducation={() => {}}
      updateGender={() => {}}
      getUserInformation={() => {}}
      isExtendedProfileLoaded={false}
      employerOptions={undefined}
      organizationOptions={undefined}
      groupOptions={{
        groups: [
          { value: 1, label: "AA" },
          { value: 2, label: "BB" }
        ],
        groupLevels: [
          { AA: [{ classif_id: 1, level: "01" }] },
          { BB: [{ classif_id: 2, level: "01" }] }
        ]
      }}
      residenceOptions={undefined}
      educationOptions={undefined}
      genderOptions={undefined}
      minorityOptions={undefined}
    />,
    {
      disableLifecycleMethods: true
    }
  );
  wrapper.setState({
    firstNameContent: firstName,
    lastNameContent: lastName,
    primaryEmailContent: primaryEmail,
    dateOfBirthDaySelectedValue: dateOfBirthDay,
    dateOfBirthMonthSelectedValue: dateOfBirthMonth,
    dateOfBirthYearSelectedValue: dateOfBirthYear,
    priContent: pri,
    isLoading: false
  });

  it("renders page title", () => {
    const title = <h2>{LOCALIZE.profile.personalInfo.title}</h2>;
    expect(wrapper.containsMatchingElement(title)).toEqual(true);
  });

  it("renders first name content", () => {
    const firstNameInput = wrapper.find("#first-name-input").props();
    expect(firstNameInput.value).toEqual(firstName);
    const firstNameLabel = <label>{LOCALIZE.profile.personalInfo.nameSection.firstName}</label>;
    expect(wrapper.containsMatchingElement(firstNameLabel)).toEqual(true);
  });

  it("renders last name content", () => {
    const lastNameInput = wrapper.find("#last-name-input").props();
    expect(lastNameInput.value).toEqual(lastName);
    const lastNameLabel = <label>{LOCALIZE.profile.personalInfo.nameSection.lastName}</label>;
    expect(wrapper.containsMatchingElement(lastNameLabel)).toEqual(true);
  });

  it("renders email content", () => {
    const primaryEmailInput = wrapper.find("#primary-email-input").props();
    expect(primaryEmailInput.value).toEqual(primaryEmail);
    const primaryEmailLabel = (
      <label>{LOCALIZE.profile.personalInfo.emailAddressesSection.primary}</label>
    );
    expect(wrapper.containsMatchingElement(primaryEmailLabel)).toEqual(true);
  });

  it("renders pri content", () => {
    const priInput = wrapper.find("#pri-input").props();
    expect(priInput.value).toEqual(pri);
    const priTitleLabel = <label>{LOCALIZE.profile.personalInfo.pri.title}</label>;
    expect(wrapper.containsMatchingElement(priTitleLabel)).toEqual(true);
  });
});
