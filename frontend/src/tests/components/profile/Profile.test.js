import React from "react";
import { shallow } from "enzyme";
import { unconnectedProfile as Profile } from "../../../components/profile/Profile";
import LOCALIZE from "../../../text_resources";

describe("renders component content", () => {
  it("renders the page title", () => {
    const wrapper = shallow(
      <Profile
        firstName=""
        lastName=""
        username=""
        primaryEmail=""
        secondaryEmail=""
        dateOfBirth=""
        priOrMilitaryNbr=""
        accommodations={{ fontSize: "16px" }}
        currentSideTab={{ sideTab: "personal-info" }}
      />,
      { disableLifecycleMethods: true }
    );
    const title = <h1>{LOCALIZE.formatString(LOCALIZE.profile.title, "", "")}</h1>;
    expect(wrapper.containsMatchingElement(title)).toEqual(true);
  });
});
