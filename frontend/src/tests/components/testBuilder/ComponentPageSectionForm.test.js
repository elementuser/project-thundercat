import React from "react";
import { mount, shallow } from "enzyme";
import { unconnectedComponentPageSectionForm as ComponentPageSectionForm } from "../../../components/testBuilder/ComponentPageSectionForm";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import { PageSectionTypeMap } from "../../../components/testFactory/Constants";

const mockStore = configureStore();

const modifyTestDefinitionField = jest.fn();
// unit-test-markdown-page-section
// unit-test-sample-email-page-section
// unit-test-sample-email-response-page-section
// unit-test-sample-task-response-page-section
// unit-test-zoom-image-page-section
// unit-test-tree-desc-page-section

const initialState = {
  localize: {
    language: "en"
  },
  testBuilder: { test_definition: [{ test_language: "--" }] },
  accommodations: { fontFamily: "Nunito Sans" }
};

describe("It renders address book contact property", () => {
  it("order", () => {
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <ComponentPageSectionForm {...testProps} />
      </Provider>
    );
    expect(wrapper.find("#component-page-section-order-input").exists()).toEqual(true);
  });
  it("type", () => {
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <ComponentPageSectionForm {...testProps} />
      </Provider>
    );
    expect(wrapper.find("#page_section_type").exists()).toEqual(true);
  });
  it("save and delete buttons", () => {
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <ComponentPageSectionForm {...testProps} />
      </Provider>
    );
    expect(wrapper.find("#unit-test-delete-item-button").exists()).toEqual(true);
    expect(wrapper.find("#unit-test-save-item-button").exists()).toEqual(true);
  });

  it("delete popup", () => {
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <ComponentPageSectionForm {...testProps} />
      </Provider>
    );
    wrapper.find("#unit-test-delete-item-button").simulate("click");
    expect(wrapper.find("#unit-test-popup").exists()).toEqual(true);
  });
});

describe("It calls save redux", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  it("1 times for bilingual", () => {
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <ComponentPageSectionForm
          {...testProps}
          testLanguage={"--"}
          modifyTestDefinitionField={modifyTestDefinitionField}
        />
      </Provider>
    );
    wrapper.find("#unit-test-save-item-button").simulate("click");
    // expect to call 3 times because english and french contact details
    // and the main contact
    expect(modifyTestDefinitionField).toHaveBeenCalledTimes(1);
  });
  it("1 time for french", () => {
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <ComponentPageSectionForm
          {...testProps}
          testLanguage={"fr"}
          modifyTestDefinitionField={modifyTestDefinitionField}
        />
      </Provider>
    );
    wrapper.find("#unit-test-save-item-button").simulate("click");
    expect(modifyTestDefinitionField).toHaveBeenCalledTimes(1);
  });
  it("1 time for english", () => {
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <ComponentPageSectionForm
          {...testProps}
          testLanguage={"en"}
          modifyTestDefinitionField={modifyTestDefinitionField}
        />
      </Provider>
    );
    wrapper.find("#unit-test-save-item-button").simulate("click");
    expect(modifyTestDefinitionField).toHaveBeenCalledTimes(1);
  });
});

describe("It updates correctly", () => {
  it("page section type", () => {
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <ComponentPageSectionForm
          {...testProps}
          testLanguage={"en"}
          modifyTestDefinitionField={modifyTestDefinitionField}
        />
      </Provider>
    );
    expect(wrapper.find(".page_section_type-classname").exists()).toEqual(true);
    wrapper
      .find(".page_section_type-classname")
      .first()
      .simulate("keyDown", { key: "ArrowDown", keyCode: 40 });
    wrapper
      .find(".page_section_type-classname")
      .first()
      .simulate("keyDown", { key: "Enter", keyCode: 13 });
    expect(
      wrapper
        .find(".page_section_type-classname")
        .first()
        .text()
    ).toEqual(PageSectionTypeMap[1]);
  });
});

const testProps = {
  enPageDefinition: {
    id: 1,
    content: "## Temporary Filler Content",
    page_section: 346,
    language: "en",
    page_section_type: 1
  },
  frPageDefinition: {
    id: 2,
    content: "## Temporary Filler Content",
    page_section: 346,
    language: "fr",
    page_section_type: 1
  },
  componentPageSection: { id: 346, order: 1, page_section_type: 1, section_component_page: 333 },
  testLanguage: "en",
  INDEX: 0,
  currentLanguage: "en",
  expandItem: () => {}
};
