import React from "react";
import { mount } from "enzyme";
import { UnconnectedMarkdownPageSectionForm } from "../../../components/testBuilder/MarkdownPageSectionForm";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
const mockStore = configureStore();

const functions = {
  modifyTestDefinitionField: jest.fn(),
  handleSave: jest.fn(),
  handleDelete: jest.fn()
};
const initialState = {
  localize: {
    language: "en"
  },
  testBuilder: { test_definition: [{ test_language: "--" }] },
  accommodations: { fontFamily: "Nunito Sans" }
};

describe("It renders markdown page section property", () => {
  it("en content", () => {
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <UnconnectedMarkdownPageSectionForm {...testProps} {...functions} />
      </Provider>
    );
    expect(wrapper.find("#en-content-input").exists()).toEqual(true);
    expect(wrapper.find("#en-content-title").exists()).toEqual(true);
  });
  it("fr content", () => {
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <UnconnectedMarkdownPageSectionForm {...testProps} {...functions} />
      </Provider>
    );
    expect(wrapper.find("#fr-content-input").exists()).toEqual(true);
    expect(wrapper.find("#fr-content-title").exists()).toEqual(true);
  });
});

describe("It calls save redux", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  it("2 times for bilingual", () => {
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <UnconnectedMarkdownPageSectionForm {...testProps} testLanguage={"--"} {...functions} />
      </Provider>
    );
    wrapper
      .find("#unit-test-save-item-button")
      .hostNodes()
      .simulate("click");
    expect(functions.modifyTestDefinitionField).toHaveBeenCalledTimes(2);
    expect(functions.handleSave).toHaveBeenCalledTimes(1);
  });
  it("1 time for french", () => {
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <UnconnectedMarkdownPageSectionForm {...testProps} testLanguage={"fr"} {...functions} />
      </Provider>
    );
    wrapper
      .find("#unit-test-save-item-button")
      .hostNodes()
      .simulate("click");
    expect(functions.modifyTestDefinitionField).toHaveBeenCalledTimes(1);
    expect(functions.handleSave).toHaveBeenCalledTimes(1);
  });
  it("1 time for english", () => {
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <UnconnectedMarkdownPageSectionForm {...testProps} testLanguage={"en"} {...functions} />
      </Provider>
    );
    wrapper
      .find("#unit-test-save-item-button")
      .hostNodes()
      .simulate("click");
    expect(functions.modifyTestDefinitionField).toHaveBeenCalledTimes(1);
    expect(functions.handleSave).toHaveBeenCalledTimes(1);
  });
});

describe("It calls delete redux", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  it("popup and delete called", () => {
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <UnconnectedMarkdownPageSectionForm {...testProps} testLanguage={"--"} {...functions} />
      </Provider>
    );
    wrapper
      .find("#unit-test-delete-item-button")
      .hostNodes()
      .simulate("click");
    expect(wrapper.find("#unit-test-popup").exists()).toEqual(true);
    wrapper
      .find("#unit-test-right-btn")
      .hostNodes()
      .simulate("click");
    expect(functions.handleDelete).toHaveBeenCalledTimes(1);
  });
});

describe("It updates correctly", () => {
  it("en content", () => {
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <UnconnectedMarkdownPageSectionForm {...testProps} testLanguage={"en"} {...functions} />
      </Provider>
    );
    let actionObj = {
      target: { value: "test string !@#!$%^*&*(", name: "content" }
    };
    expect(wrapper.find("#en-content-input").exists()).toEqual(true);
    wrapper.find("#en-content-input").simulate("change", actionObj);

    expect(wrapper.find("#invalid-order-msg").exists()).toEqual(false);
    expect(
      wrapper.find(UnconnectedMarkdownPageSectionForm).state().enPageDefinition.content
    ).toEqual("test string !@#!$%^*&*(");
  });
  it("fr content", () => {
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <UnconnectedMarkdownPageSectionForm {...testProps} testLanguage={"fr"} {...functions} />
      </Provider>
    );
    let actionObj = {
      target: { value: "test string !@#!$%^*&*(", name: "content" }
    };
    expect(wrapper.find("#fr-content-input").exists()).toEqual(true);
    wrapper.find("#fr-content-input").simulate("change", actionObj);

    expect(wrapper.find("#invalid-order-msg").exists()).toEqual(false);
    expect(
      wrapper.find(UnconnectedMarkdownPageSectionForm).state().frPageDefinition.content
    ).toEqual("test string !@#!$%^*&*(");
  });
});

const testProps = {
  enPageDefinition: {
    id: 1,
    content: "## Temporary Filler Content",
    page_section: 346,
    language: "en",
    page_section_type: 1
  },
  frPageDefinition: {
    id: 2,
    content: "## Temporary Filler Content",
    page_section: 346,
    language: "fr",
    page_section_type: 1
  },
  componentPageSection: { id: 346, order: 1, page_section_type: 1, section_component_page: 333 },
  testLanguage: "--",
  expandItem: () => {}
};
