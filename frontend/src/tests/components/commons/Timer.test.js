import React from "react";
import { mount } from "enzyme";
import { UnconnectedTimer } from "../../../components/commons/Timer";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";

const mockStore = configureStore();

const initialState = {
  localize: {
    language: "en"
  },
  accommodations: {
    fontFamily: "Nunito Sans"
  }
};

const getServerTime = jest.fn().mockReturnValue(Promise.resolve("2023-08-24T17:53:22.520622Z"));

it("defaults to showing the time", () => {
  const wrapper = mount(
    <Provider store={mockStore(initialState)}>
      <UnconnectedTimer
        timeout={() => {}}
        timeRemaining={() => {}}
        testSection={{ is_public: true, count_up: true }}
        getServerTime={getServerTime}
        setCurrentTime={() => {}}
      />
    </Provider>
  );
  expect(wrapper.find("#unit-test-time-label").exists()).toEqual(true);
});

it("hides the time on click", () => {
  const wrapper = mount(
    <Provider store={mockStore(initialState)}>
      <UnconnectedTimer
        timeout={() => {}}
        timeRemaining={() => {}}
        testSection={{ is_public: false, count_up: false }}
        getServerTime={getServerTime}
        setCurrentTime={() => {}}
      />
    </Provider>
  );
  wrapper.find("#unit-test-toggle-timer").first().simulate("click");
  expect(wrapper.find("#unit-test-time-label").exists()).toEqual(false);
});
