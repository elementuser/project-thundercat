import React from "react";
import { shallow } from "enzyme";
import { unconnectedSetTimer as SetTimer } from "../../../components/commons/SetTimer";
import LOCALIZE from "../../../text_resources";

const CURRENT_TIMER = {
  hours: "03",
  minutes: "30"
};

describe("renders component content", () => {
  const wrapper = shallow(
    <SetTimer
      index={0}
      currentHours={CURRENT_TIMER.hours}
      currentMinutes={CURRENT_TIMER.minutes}
      minutesLeaps={5}
      updateTimerState={() => {}}
      updateValidTimerState={() => {}}
      resetTimerStates={() => {}}
      triggerTimeUpdate={() => {}}
      accommodations={{ fontSize: "16px" }}
    />,
    {
      disableLifecycleMethods: true
    }
  );

  it("renders current time", () => {
    const currentHours = <span>03</span>;
    const semicolon = <span>:</span>;
    const currentMinutes = <span>30</span>;
    expect(wrapper.containsMatchingElement(currentHours)).toEqual(true);
    expect(wrapper.containsMatchingElement(semicolon)).toEqual(true);
    expect(wrapper.containsMatchingElement(currentMinutes)).toEqual(true);
  });

  it("renders time labels", () => {
    const hoursLabel = (
      <span>{LOCALIZE.testAdministration.activeCandidates.editTimePopup.hours}</span>
    );
    const minutesLabel = (
      <span>{LOCALIZE.testAdministration.activeCandidates.editTimePopup.minutes}</span>
    );
    expect(wrapper.containsMatchingElement(hoursLabel)).toEqual(true);
    expect(wrapper.containsMatchingElement(minutesLabel)).toEqual(true);
  });
});
