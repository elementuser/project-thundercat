import React from "react";
import { mount } from "enzyme";
import {
  ERROR_MESSAGE,
  unconnectedDatePicker as DatePicker
} from "../../../components/commons/DatePicker";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import LOCALIZE from "../../../text_resources";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheckCircle } from "@fortawesome/free-solid-svg-icons";

const initialState = {
  state: {
    datePicker: {
      completeDatePicked: "2020-01-15"
    },
    localize: {
      language: "en"
    }
  },
  resetDatePickedStates: jest.fn(),
  accommodations: {
    fontSize: "16px",
    fontFamily: "Nunito Sans"
  }
};

const mockStore = configureStore();

describe("renders component content", () => {
  const wrapper = mount(
    <Provider store={mockStore(initialState)}>
      <DatePicker
        triggerValidation={false}
        displayValidIcon={true}
        completeDatePicked={initialState.state.datePicker.completeDatePicked}
        resetDatePickedStates={initialState.resetDatePickedStates}
        customInvalidDateErrorMessage={ERROR_MESSAGE.default}
        accommodations={{
          fontSize: "16px",
          fontFamily: "Nunito Sans"
        }}
      />
    </Provider>
  );

  it("renders field labels", () => {
    const dayFieldLabel = <label id="day-field-unit-test">{LOCALIZE.datePicker.dayField}</label>;
    const monthFieldLabel = (
      <label id="month-field-unit-test">{LOCALIZE.datePicker.monthField}</label>
    );
    const yearFieldLabel = <label id="year-field-unit-test">{LOCALIZE.datePicker.yearField}</label>;
    expect(wrapper.containsMatchingElement(dayFieldLabel)).toEqual(true);
    expect(wrapper.containsMatchingElement(monthFieldLabel)).toEqual(true);
    expect(wrapper.containsMatchingElement(yearFieldLabel)).toEqual(true);
  });

  it("renders valid icon if valid date picked", () => {
    wrapper.find(DatePicker).instance().setState({ validationTriggered: true });
    wrapper.update();
    const validDatePickedLabel = (
      <div>
        <FontAwesomeIcon icon={faCheckCircle} />
        {LOCALIZE.commons.valid}
      </div>
    );
    expect(wrapper.containsMatchingElement(validDatePickedLabel)).toEqual(true);
  });

  it("renders invalid label if not valid date picked", () => {
    wrapper.find(DatePicker).instance().setState({ isValidCompleteDatePicked: false });
    wrapper.update();
    const invalidDatePickedLabel = (
      <label id="date-error">{LOCALIZE.datePicker.datePickedError}</label>
    );
    expect(wrapper.containsMatchingElement(invalidDatePickedLabel)).toEqual(true);
  });

  it("renders invalid label if valid date picked but not valid future date picked", () => {
    wrapper
      .find(DatePicker)
      .instance()
      .setState({ isValidFutureDatePicked: false, isValidCompleteDatePicked: true });
    wrapper.update();
    const invalidFutureDatePickedLabel = (
      <label id="future-date-error">{LOCALIZE.datePicker.futureDatePickedError}</label>
    );
    expect(wrapper.containsMatchingElement(invalidFutureDatePickedLabel)).toEqual(true);
  });
});
