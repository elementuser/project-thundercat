import { calculateRemainingTime } from "../../../components/commons/TimerCalculation";
import moment from "moment";

const now = moment(new Date(Date.now()));

const oneMin = 60000;
const oneMinInFuture = moment(new Date(now).getTime() + oneMin);

const twoMin = oneMin * 2;
const twoMinInFuture = moment(new Date(now).getTime() + twoMin);

const threeMin = oneMin * 3;
const threeMinInFuture = moment(new Date(now).getTime() + threeMin);

const fourMin = oneMin * 4;
const fourMinInFuture = moment(new Date(now).getTime() + fourMin);

const fiveMin = oneMin * 5;
const fiveMinInFuture = moment(new Date(now).getTime() + fiveMin);

const sixMin = oneMin * 6;
const sixMinInFuture = moment(new Date(now).getTime() + sixMin);

it("returns 0 if startTime is null", () => {
  expect(calculateRemainingTime(null, null, null, () => {})).toEqual(0);
  expect(calculateRemainingTime(null, oneMin, null, () => {})).toEqual(0);
  expect(calculateRemainingTime(null, null, oneMin, () => {})).toEqual(0);
  expect(calculateRemainingTime(null, oneMin, oneMin, () => {})).toEqual(0);
});

it("returns 0 if now is null", () => {
  expect(calculateRemainingTime(null, null, null, () => {})).toEqual(0);
  expect(calculateRemainingTime(oneMin, null, null, () => {})).toEqual(0);
  expect(calculateRemainingTime(null, oneMin, null, () => {})).toEqual(0);
  expect(calculateRemainingTime(oneMin, oneMin, null, () => {})).toEqual(0);
});

it("returns increasing number if time limit is null or if isSample is true", () => {
  expect(calculateRemainingTime(now, null, now, () => {})).toEqual(0);
  expect(calculateRemainingTime(now, null, twoMinInFuture, () => {})).toEqual(twoMin);
  expect(calculateRemainingTime(oneMinInFuture, null, threeMinInFuture, () => {})).toEqual(twoMin);
  expect(calculateRemainingTime(oneMinInFuture, null, fourMinInFuture, () => {})).toEqual(threeMin);
  expect(calculateRemainingTime(oneMinInFuture, null, fiveMinInFuture, () => {})).toEqual(fourMin);
  expect(calculateRemainingTime(oneMinInFuture, null, sixMinInFuture, () => {})).toEqual(fiveMin);
  expect(calculateRemainingTime(oneMinInFuture, 90, sixMinInFuture, () => {}, true)).toEqual(
    fiveMin
  );
});

it("returns decreasing number if time limit is null", () => {
  expect(calculateRemainingTime(oneMin, 5, moment(oneMin), () => {})).toEqual(fiveMin);
  expect(calculateRemainingTime(oneMin, 5, moment(twoMin), () => {})).toEqual(fourMin);
  expect(calculateRemainingTime(oneMin, 5, moment(threeMin), () => {})).toEqual(threeMin);
  expect(calculateRemainingTime(oneMin, 5, moment(fourMin), () => {})).toEqual(twoMin);
  expect(calculateRemainingTime(oneMin, 5, moment(fiveMin), () => {})).toEqual(oneMin);
  expect(calculateRemainingTime(oneMin, 5, moment(sixMin), () => {})).toEqual(null);
});

it("timeout function is triggered when time is out", () => {
  const timeout = jest.fn();
  calculateRemainingTime(oneMin, 5, moment(oneMin), timeout);
  calculateRemainingTime(oneMin, 5, moment(twoMin), timeout);
  calculateRemainingTime(oneMin, 5, moment(threeMin), timeout);
  calculateRemainingTime(oneMin, 5, moment(fourMin), timeout);
  calculateRemainingTime(oneMin, 5, moment(fiveMin), timeout);
  expect(timeout).toHaveBeenCalledTimes(0);
  calculateRemainingTime(oneMin, 5, moment(sixMin), timeout);
  expect(timeout).toHaveBeenCalledTimes(1);
});
