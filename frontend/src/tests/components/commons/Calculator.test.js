import React from "react";
import { mount, shallow } from "enzyme";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import { UnconnectedCalculator as Calculator } from "../../../components/commons/calculator/Calculator";

const mockStore = configureStore();

const initialState = {
  localize: {
    language: "en"
  },
  accommodations: { fontSize: "16px", fontFamily: "Nunito Sans" }
};

describe("renders component content", () => {
  const wrapper = mount(
    shallow(
      <Provider store={mockStore(initialState)}>
        <Calculator accommodations={{ fontSize: "16px", fontFamily: "Nunito Sans" }} />
      </Provider>
    ).get(0)
  );

  it("renders calculator buttons", () => {
    expect(wrapper.find("#reset-calculator-button").exists()).toEqual(true);
    expect(wrapper.find("#open-parenthesis-calculator-button").exists()).toEqual(true);
    expect(wrapper.find("#close-parenthesis-calculator-button").exists()).toEqual(true);
    expect(wrapper.find("#division-calculator-button").exists()).toEqual(true);
    expect(wrapper.find("#multiplication-calculator-button").exists()).toEqual(true);
    expect(wrapper.find("#substraction-calculator-button").exists()).toEqual(true);
    expect(wrapper.find("#addition-calculator-button").exists()).toEqual(true);
    expect(wrapper.find("#number-0-calculator-button").exists()).toEqual(true);
    expect(wrapper.find("#number-1-calculator-button").exists()).toEqual(true);
    expect(wrapper.find("#number-2-calculator-button").exists()).toEqual(true);
    expect(wrapper.find("#number-3-calculator-button").exists()).toEqual(true);
    expect(wrapper.find("#number-4-calculator-button").exists()).toEqual(true);
    expect(wrapper.find("#number-5-calculator-button").exists()).toEqual(true);
    expect(wrapper.find("#number-6-calculator-button").exists()).toEqual(true);
    expect(wrapper.find("#number-7-calculator-button").exists()).toEqual(true);
    expect(wrapper.find("#number-8-calculator-button").exists()).toEqual(true);
    expect(wrapper.find("#number-9-calculator-button").exists()).toEqual(true);
    expect(wrapper.find("#dot-calculator-button").exists()).toEqual(true);
    expect(wrapper.find("#equal-calculator-button").exists()).toEqual(true);
    expect(wrapper.find("#backspace-calculator-button").exists()).toEqual(true);
  });

  it("renders calculator result window", () => {
    expect(wrapper.find("#calculator-result").exists()).toEqual(true);
    expect(wrapper.state().result).toEqual("0");
  });
});

describe("calculate equations", () => {
  const wrapper = mount(
    shallow(
      <Provider store={mockStore(initialState)}>
        <Calculator accommodations={{ fontSize: "16px", fontFamily: "Nunito Sans" }} />
      </Provider>
    ).get(0)
  );

  it("Addition", () => {
    wrapper.setState({ result: "2.25+3.5" });
    wrapper
      .find("#equal-calculator-button")
      .first()
      .simulate("click");
    expect(wrapper.state().result).toEqual("5.75");
  });

  it("Substraction", () => {
    wrapper.setState({ result: "10-7" });
    wrapper
      .find("#equal-calculator-button")
      .first()
      .simulate("click");
    expect(wrapper.state().result).toEqual("3");
  });

  it("Multiplication", () => {
    wrapper.setState({ result: "3*12" });
    wrapper
      .find("#equal-calculator-button")
      .first()
      .simulate("click");
    expect(wrapper.state().result).toEqual("36");
  });

  it("Division", () => {
    wrapper.setState({ result: "45/9" });
    wrapper
      .find("#equal-calculator-button")
      .first()
      .simulate("click");
    expect(wrapper.state().result).toEqual("5");
  });
  it("Complex Equation", () => {
    wrapper.setState({ result: "(5*(12+8)/4-(-5))/3" });
    wrapper
      .find("#equal-calculator-button")
      .first()
      .simulate("click");
    expect(wrapper.state().result).toEqual("10");
    wrapper
      .find("#backspace-calculator-button")
      .first()
      .simulate("click");
    expect(wrapper.state().result).toEqual("1");
    wrapper
      .find("#reset-calculator-button")
      .first()
      .simulate("click");
    expect(wrapper.state().result).toEqual("0");
  });
});
