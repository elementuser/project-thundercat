import React from "react";
import { shallow } from "enzyme";
import { unconnectedSearchBarWithDisplayOptions as SearchBarWithDisplayOptions } from "../../../components/commons/SearchBarWithDisplayOptions";
import LOCALIZE from "../../../text_resources";

describe("renders component content", () => {
  const wrapper = shallow(
    <SearchBarWithDisplayOptions
      idPrefix={"unit-test-example"}
      currentlyLoading={false}
      updateSearchStates={() => {}}
      updatePageState={() => {}}
      paginationPageSize={2}
      updatePaginationPageSize={() => {}}
      data={[]}
      resultsFound={10}
      accommodations={{ fontSize: "16px" }}
    />,
    {
      disableLifecycleMethods: true
    }
  );

  it("renders search bar label", () => {
    const searchBarLabel = (
      <label id="unit-test-example-search-bar-title">
        {LOCALIZE.SearchBarWithDisplayOptions.searchBarTitle}
      </label>
    );
    const resultsFoundlabel = (
      <label id="unit-test-example-results-found">
        {LOCALIZE.formatString(LOCALIZE.SearchBarWithDisplayOptions.resultsFound, 10)}
      </label>
    );
    const clearSearchButton = (
      <button aria-label={LOCALIZE.SearchBarWithDisplayOptions.clearSearch}>
        {LOCALIZE.SearchBarWithDisplayOptions.clearSearch}
      </button>
    );
    expect(wrapper.containsMatchingElement(searchBarLabel)).toEqual(true);
    expect(wrapper.containsMatchingElement(resultsFoundlabel)).toEqual(false);
    expect(wrapper.containsMatchingElement(clearSearchButton)).toEqual(false);
  });

  it("renders display option label", () => {
    const displayOptionsLabel = (
      <label id="unit-test-example-display-options-label">
        {LOCALIZE.SearchBarWithDisplayOptions.displayOptionLabel}
      </label>
    );
    const resultsFoundlabel = (
      <label id="unit-test-example-results-found">
        {LOCALIZE.formatString(LOCALIZE.SearchBarWithDisplayOptions.resultsFound, 10)}
      </label>
    );
    const clearSearchButton = (
      <button aria-label={LOCALIZE.SearchBarWithDisplayOptions.clearSearch}>
        {LOCALIZE.SearchBarWithDisplayOptions.clearSearch}
      </button>
    );
    expect(wrapper.containsMatchingElement(displayOptionsLabel)).toEqual(true);
    expect(wrapper.containsMatchingElement(resultsFoundlabel)).toEqual(false);
    expect(wrapper.containsMatchingElement(clearSearchButton)).toEqual(false);
  });

  it("renders results found label and clear search button", () => {
    wrapper.setState({ displayResultsFound: true });
    const resultsFoundlabel = (
      <label id="unit-test-example-results-found">
        {LOCALIZE.formatString(LOCALIZE.SearchBarWithDisplayOptions.resultsFound, 10)}
      </label>
    );
    const clearSearchButton = (
      <button aria-label={LOCALIZE.SearchBarWithDisplayOptions.clearSearch}>
        {LOCALIZE.SearchBarWithDisplayOptions.clearSearch}
      </button>
    );
    expect(wrapper.containsMatchingElement(resultsFoundlabel)).toEqual(true);
    expect(wrapper.containsMatchingElement(clearSearchButton)).toEqual(true);
  });
});
