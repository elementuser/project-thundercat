import React from "react";
import { shallow } from "enzyme";
import { unconnectedTestContentReport as TestContentReport } from "../../../../components/commons/reports/TestContentReport";
import LOCALIZE from "../../../../text_resources";

describe("renders component content", () => {
  const wrapper = shallow(<TestContentReport currentlyLoading={true} />, {
    disableLifecycleMethods: true
  });

  it("renders no dropdowns at all (initial states)", () => {
    const parentCode = <label id="parent-code-label">{LOCALIZE.reports.parentCodeLabel}</label>;
    const testCode = <label id="test-code-label">{LOCALIZE.reports.testCodeLabel}</label>;
    const testVersion = <label id="test-version-label">{LOCALIZE.reports.testVersionLabel}</label>;
    expect(wrapper.containsMatchingElement(parentCode)).toEqual(false);
    expect(wrapper.containsMatchingElement(testCode)).toEqual(false);
    expect(wrapper.containsMatchingElement(testVersion)).toEqual(false);
    expect(wrapper.find("#unit-test-parent-code-dropdown").exists()).toEqual(false);
    expect(wrapper.find("#unit-test-test-code-dropdown").exists()).toEqual(false);
    expect(wrapper.find("#unit-test-test-version-dropdown").exists()).toEqual(false);
  });

  it("renders parent code dropdown (when report type has been selected)", () => {
    // simulating that report type (option 1 - Individual Score Sheet) has been selected
    wrapper.setState({
      parentCodeVisible: true
    });

    const parentCode = <label id="parent-code-label">{LOCALIZE.reports.parentCodeLabel}</label>;
    const testCode = <label id="test-code-label">{LOCALIZE.reports.testCodeLabel}</label>;
    const testVersion = <label id="test-version-label">{LOCALIZE.reports.testVersionLabel}</label>;
    expect(wrapper.containsMatchingElement(parentCode)).toEqual(true);
    expect(wrapper.containsMatchingElement(testCode)).toEqual(false);
    expect(wrapper.containsMatchingElement(testVersion)).toEqual(false);
    expect(wrapper.find("#unit-test-parent-code-dropdown").exists()).toEqual(true);
    expect(wrapper.find("#unit-test-test-code-dropdown").exists()).toEqual(false);
    expect(wrapper.find("#unit-test-test-version-dropdown").exists()).toEqual(false);
  });

  it("renders parent code and test code dropdowns (when parent code has been selected)", () => {
    // simulating that report type (option 1 - Individual Score Sheet) has been selected
    wrapper.setState({
      testCodeVisible: true
    });

    const parentCode = <label id="parent-code-label">{LOCALIZE.reports.parentCodeLabel}</label>;
    const testCode = <label id="test-code-label">{LOCALIZE.reports.testCodeLabel}</label>;
    const testVersion = <label id="test-version-label">{LOCALIZE.reports.testVersionLabel}</label>;
    expect(wrapper.containsMatchingElement(parentCode)).toEqual(true);
    expect(wrapper.containsMatchingElement(testCode)).toEqual(true);
    expect(wrapper.containsMatchingElement(testVersion)).toEqual(false);
    expect(wrapper.find("#unit-test-parent-code-dropdown").exists()).toEqual(true);
    expect(wrapper.find("#unit-test-test-code-dropdown").exists()).toEqual(true);
    expect(wrapper.find("#unit-test-test-version-dropdown").exists()).toEqual(false);
  });

  it("renders parent code, test code and test version dropdowns (when test code has been selected)", () => {
    // simulating that report type (option 1 - Individual Score Sheet) has been selected
    wrapper.setState({
      testVersionVisible: true
    });

    const parentCode = <label id="parent-code-label">{LOCALIZE.reports.parentCodeLabel}</label>;
    const testCode = <label id="test-code-label">{LOCALIZE.reports.testCodeLabel}</label>;
    const testVersion = <label id="test-version-label">{LOCALIZE.reports.testVersionLabel}</label>;
    expect(wrapper.containsMatchingElement(parentCode)).toEqual(true);
    expect(wrapper.containsMatchingElement(testCode)).toEqual(true);
    expect(wrapper.containsMatchingElement(testVersion)).toEqual(true);
    expect(wrapper.find("#unit-test-parent-code-dropdown").exists()).toEqual(true);
    expect(wrapper.find("#unit-test-test-code-dropdown").exists()).toEqual(true);
    expect(wrapper.find("#unit-test-test-version-dropdown").exists()).toEqual(true);
  });
});
