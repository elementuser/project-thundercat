import React from "react";
import { shallow } from "enzyme";
import SampleEmail from "../../../components/eMIB/SampleEmail";
import LOCALIZE from "../../../text_resources";

const sampleEmail = {
  id: 1,
  to: "Tony Stark",
  from: "Doctor Strange",
  subject: "You stole my tech",
  date: "Friday November 4",
  body: "Please give me my tech back."
};

it("renders all email fields except CC", () => {
  const wrapper = shallow(<SampleEmail email={sampleEmail} />);
  // wrapper.setState({ buttonVisible: true });
  expect(wrapper.find("#unit-test-email-id").exists()).toEqual(true);
  expect(wrapper.find("#unit-test-email-to").exists()).toEqual(true);
  expect(wrapper.find("#unit-test-email-cc").exists()).toEqual(false);
  expect(wrapper.find("#unit-test-email-from").exists()).toEqual(true);
  expect(wrapper.find("#unit-test-email-subject").exists()).toEqual(true);
  expect(wrapper.find("#unit-test-email-date").exists()).toEqual(true);
  expect(wrapper.find("#unit-test-email-body").exists()).toEqual(true);
});

it("renders correct email number text", () => {
  const wrapper = shallow(<SampleEmail email={sampleEmail} />);
  let text = LOCALIZE.emibTest.inboxPage.emailId.toUpperCase() + sampleEmail.id;
  expect(wrapper.find("#unit-test-email-id").text()).toEqual(text);
});

it("renders correct to field", () => {
  const wrapper = shallow(<SampleEmail email={sampleEmail} />);
  let text = LOCALIZE.emibTest.inboxPage.to + ": " + sampleEmail.to;
  expect(wrapper.find("#unit-test-email-to").text()).toEqual(text);
});

it("renders correct from field", () => {
  const wrapper = shallow(<SampleEmail email={sampleEmail} />);
  let text = LOCALIZE.emibTest.inboxPage.from + ": " + sampleEmail.from;
  expect(wrapper.find("#unit-test-email-from").text()).toEqual(text);
});

it("renders correct subject field", () => {
  const wrapper = shallow(<SampleEmail email={sampleEmail} />);
  let text = LOCALIZE.emibTest.inboxPage.subject + ": " + sampleEmail.subject;
  expect(wrapper.find("#unit-test-email-subject").text()).toEqual(text);
});

it("renders correct date field", () => {
  const wrapper = shallow(<SampleEmail email={sampleEmail} />);
  let text = LOCALIZE.emibTest.inboxPage.date + ": " + sampleEmail.date;
  expect(wrapper.find("#unit-test-email-date").text()).toEqual(text);
});

it("renders correct body", () => {
  const wrapper = shallow(<SampleEmail email={sampleEmail} />);
  expect(wrapper.find("#unit-test-email-body").text()).toEqual(sampleEmail.body);
});
