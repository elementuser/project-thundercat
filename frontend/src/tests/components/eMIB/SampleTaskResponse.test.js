import React from "react";
import { shallow } from "enzyme";
import SampleTaskResponse from "../../../components/eMIB/SampleTaskResponse";
import LOCALIZE from "../../../text_resources";
import { ACTION_TYPE } from "../../../components/eMIB/constants";

const exampleTaskResponse = {
  actionType: ACTION_TYPE.task,
  task: "get the hulk to give up",
  reasonsForAction: "he is a menace"
};

it("renders all task fields", () => {
  const wrapper = shallow(<SampleTaskResponse action={exampleTaskResponse} />);
  // wrapper.setState({ buttonVisible: true });
  expect(wrapper.find("#unit-test-sample-task-header").exists()).toEqual(true);
  expect(wrapper.find("#unit-test-sample-task-content").exists()).toEqual(true);
  expect(wrapper.find("#unit-test-sample-task-reason-header").exists()).toEqual(true);
  expect(wrapper.find("#unit-test-sample-task-reason").exists()).toEqual(true);
});

it("renders task header", () => {
  const wrapper = shallow(<SampleTaskResponse action={exampleTaskResponse} />);
  let text = LOCALIZE.emibTest.inboxPage.taskContent.task;
  expect(wrapper.find("#unit-test-sample-task-header").text()).toEqual(text);
});

it("renders task content", () => {
  const wrapper = shallow(<SampleTaskResponse action={exampleTaskResponse} />);
  let text = exampleTaskResponse.task;
  expect(wrapper.find("#unit-test-sample-task-content").text()).toEqual(text);
});

it("renders task reason header", () => {
  const wrapper = shallow(<SampleTaskResponse action={exampleTaskResponse} />);
  let text = LOCALIZE.emibTest.inboxPage.emailResponse.reasonsForAction;
  expect(wrapper.find("#unit-test-sample-task-reason-header").text()).toEqual(text);
});

it("renders task reason content", () => {
  const wrapper = shallow(<SampleTaskResponse action={exampleTaskResponse} />);
  let text = exampleTaskResponse.reasonsForAction;
  expect(wrapper.find("#unit-test-sample-task-reason").text()).toEqual(text);
});
