import { contactNameFromId, recursivelyProcessTree } from "../../../helpers/transformations";
import { addressBook } from "./constants";

describe("Check contactNameFromId", () => {
  it("returns the name when id is in the address book", () => {
    expect(contactNameFromId(addressBook, 3)).toEqual(
      "Geneviève Bédard (Director, Research and Innovations Unit)"
    );
  });

  it("returns an empty string when the id is not in the address book", () => {
    expect(contactNameFromId(addressBook, 19)).toEqual("");
  });
});

describe("recursivelyProcessTree", () => {
  it("returns a processed array for single level tree", () => {
    const treeContent = [
      {
        id: 6,
        team_information_tree_child: null,
        text: {
          id: 6,
          parent: null,
          test_section_component: [4, 5],
          name: "Marc Sheridan",
          title: "Manager, Human Resources",
          label: "Marc Sheridan (Manager, Human Resources)",
          value: 6,
          language: "en",
          contact: 6,
          text: "Marc Sheridan (Manager, Human Resources)"
        },
        parent: 2,
        test_section_component: [4, 5]
      }
    ];
    const array = recursivelyProcessTree(treeContent, "team_information_tree_child", 1, 0);
    expect(array).toEqual([
      {
        id: 0,
        level: 1,
        name: "Marc Sheridan (Manager, Human Resources)",
        parent: undefined
      }
    ]);
  });

  it("returns a processed array for double level tree", () => {
    const treeContent = [
      {
        id: 6,
        team_information_tree_child: null,
        text: {
          id: 6,
          parent: 2,
          test_section_component: [4, 5],
          name: "Marc Sheridan",
          title: "Manager, Human Resources",
          label: "Marc Sheridan (Manager, Human Resources)",
          value: 6,
          language: "en",
          contact: 6,
          text: "Marc Sheridan (Manager, Human Resources)"
        },
        parent: 2,
        test_section_component: [4, 5]
      },
      {
        id: 7,
        team_information_tree_child: null,
        text: {
          id: 7,
          parent: 2,
          test_section_component: [4, 5],
          name: "Bob McNutt",
          title: "Manager, Finance",
          label: "Bob McNutt (Manager, Finance)",
          value: 7,
          language: "en",
          contact: 7,
          text: "Bob McNutt (Manager, Finance)"
        },
        parent: 2,
        test_section_component: [4, 5]
      },
      {
        id: 8,
        team_information_tree_child: null,
        text: {
          id: 8,
          parent: 2,
          test_section_component: [4, 5],
          name: "Lana Hussad",
          title: "Manager, Information Technology",
          label: "Lana Hussad (Manager, Information Technology)",
          value: 8,
          language: "en",
          contact: 8,
          text: "Lana Hussad (Manager, Information Technology)"
        },
        parent: 2,
        test_section_component: [4, 5]
      }
    ];
    const array = recursivelyProcessTree(treeContent, "team_information_tree_child", 1, 0);
    expect(array).toEqual([
      { id: 0, name: "Marc Sheridan (Manager, Human Resources)", level: 1, parent: undefined },
      { id: 1, name: "Bob McNutt (Manager, Finance)", level: 1, parent: undefined },
      { id: 2, name: "Lana Hussad (Manager, Information Technology)", level: 1, parent: undefined }
    ]);
  });
});
