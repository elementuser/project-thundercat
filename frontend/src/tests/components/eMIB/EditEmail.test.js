import React from "react";
import EditEmail from "../../../components/eMIB/EditEmail";
import { EDIT_MODE } from "../../../components/eMIB/constants";
import { mount } from "enzyme";
import { addressBook } from "./constants";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";

const mockStore = configureStore();

const initialState = {
  localize: {
    language: "en"
  },
  accommodations: {
    fontFamily: "Nunito Sans"
  }
};

describe("renders EditEmail component", () => {
  it("cc field is unpopulated when there is no original cc", () => {
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <EditEmail
          onChange={() => {}}
          action={null}
          originalFrom={[addressBook[0]]}
          originalTo={[addressBook[1]]}
          originalCC={[]}
          editMode={EDIT_MODE.update}
          toFieldValid={true}
          triggerPropsUpdate={true}
          addressBook={addressBook}
          emailType={"reply"}
        />
      </Provider>
    );

    wrapper.find("#unit-test-reply-all-button").simulate("click");
    expect(wrapper.find(EditEmail).instance().state).toEqual({
      emailBody: "",
      emailCc: [],
      emailCcSelectedValues: [],
      emailTo: [addressBook[1], addressBook[0]],
      emailToSelectedValues: [],
      emailType: "replyAll",
      reasonsForAction: ""
    });
  });

  it("cc field is populated when there is an original cc", () => {
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <EditEmail
          onChange={() => {}}
          action={null}
          originalFrom={[addressBook[0]]}
          originalTo={[addressBook[1]]}
          originalCC={[addressBook[2]]}
          editMode={EDIT_MODE.update}
          toFieldValid={true}
          triggerPropsUpdate={true}
          addressBook={addressBook}
          emailType={"reply"}
        />
      </Provider>
    );

    wrapper.find("#unit-test-reply-all-button").simulate("click");
    expect(wrapper.find(EditEmail).instance().state).toEqual({
      emailBody: "",
      emailCc: [addressBook[2]],
      emailCcSelectedValues: [],
      emailTo: [addressBook[1], addressBook[0]],
      emailToSelectedValues: [],
      emailType: "replyAll",
      reasonsForAction: ""
    });
  });
});

it("checks if the right icon button is coloured in", () => {
  const wrapper = mount(
    <Provider store={mockStore(initialState)}>
      <EditEmail
        onChange={() => {}}
        action={null}
        originalFrom={[addressBook[0]]}
        originalTo={[addressBook[1]]}
        originalCC={[addressBook[2]]}
        editMode={EDIT_MODE.update}
        toFieldValid={true}
        triggerPropsUpdate={true}
        addressBook={addressBook}
        emailType={"reply"}
      />
    </Provider>
  );

  const replyAllIcon = wrapper.find("#unit-test-reply-all-button > FontAwesomeIcon");
  replyAllIcon.simulate("click");

  expect(getComputedStyle(replyAllIcon.getDOMNode()).getPropertyValue("background-color")).toBe(
    "rgb(0, 86, 94)"
  );

  const replyIcon = wrapper.find("#unit-test-reply-button > FontAwesomeIcon");
  expect(getComputedStyle(replyIcon.getDOMNode()).getPropertyValue("background-color")).toBe("");
});

// describe("renders component's content", () => {
//   const addressBook = [
//     { label: "Sherlock Holmes (Consulting Detective)", value: 300 },
//     { label: "Jean Luc Picard (Captain, U.S.S. Enterpise, Starfleet)", value: 301 },
//     { label: "Arthur Pendragon (King of England)", value: 302 }
//   ];

//   it("returns empty list when empty", () => {
//     expect(generateToIds("", addressBook)).toEqual([]);
//   });

//   it("renders empty list when name does not exist in the book", () => {
//     expect(generateToIds("The Outsider", addressBook)).toEqual([]);
//     expect(generateToIds("Corvo Attano (Assassin, Dishonored)", addressBook)).toEqual([]);
//     expect(generateToIds("The Outsider; Corvo Attano (Assassin, Dishonored)", addressBook)).toEqual(
//       []
//     );
//   });

//   it("renders one name when there is one", () => {
//     const names = addressBook[0].label;
//     const ret = [addressBook[0].value];
//     expect(generateToIds(names, addressBook)).toEqual(ret);
//   });

//   it("renders two name when there are two", () => {
//     const names = addressBook[0].label + "; " + addressBook[1].label;
//     const ret = [addressBook[0].value, addressBook[1].value];
//     expect(generateToIds(names, addressBook)).toEqual(ret);
//   });

//   it("renders three name when there are three", () => {
//     const names = addressBook[0].label + "; " + addressBook[1].label + "; " + addressBook[2].label;
//     const ret = [addressBook[0].value, addressBook[1].value, addressBook[2].value];
//     expect(generateToIds(names, addressBook)).toEqual(ret);
//   });
// });
