import React from "react";
import { mount } from "enzyme";
import {
  styles,
  UnconnectedQuestionPreview as QuestionPreview
} from "../../../components/testFactory/QuestionPreview";
import LOCALIZE from "../../../text_resources";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";

const mockStore = configureStore();

const initialState = {
  localize: {
    language: "en"
  },
  accommodations: {
    fontFamily: "Nunito Sans"
  }
};

describe("It renders a", () => {
  it("question number", () => {
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <QuestionPreview
          index={0}
          questionId={0}
          isAnswered={false}
          isRead={false}
          isSelected={false}
          accommodations={{
            fontFamily: "Nunito Sans"
          }}
        />
      </Provider>
    );

    expect(wrapper.find("#unit-test-question-id-label-0").exists()).toEqual(true);
  });
});

describe("It renders the correct content", () => {
  it("question preview text", () => {
    const index = 0;
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <QuestionPreview
          index={index}
          questionId={0}
          isAnswered={false}
          isRead={false}
          isSelected={true}
          accommodations={{
            fontFamily: "Nunito Sans"
          }}
        />
      </Provider>
    );

    const questionPreview = wrapper.find("#unit-test-question-id-label-0");

    const questionTitle = LOCALIZE.formatString(
      LOCALIZE.mcTest.questionList.questionIdShort,
      index + 1
    );
    const markedForReviewTooltip = LOCALIZE.mcTest.questionList.reviewQuestion;
    const questionAnsweredTooltip = LOCALIZE.mcTest.questionList.answeredQuestion;
    const questionUnansweredTooltip = LOCALIZE.mcTest.questionList.unansweredQuestion;
    const questionSeenTooltip = LOCALIZE.mcTest.questionList.seenQuestion;
    const questionUnseenTooltip = LOCALIZE.mcTest.questionList.unseenQuestion;

    expect(questionPreview.containsMatchingElement(questionTitle)).toEqual(true);
    expect(questionPreview.containsMatchingElement(markedForReviewTooltip)).toEqual(false);
    expect(questionPreview.containsMatchingElement(questionAnsweredTooltip)).toEqual(false);
    expect(
      wrapper
        .find("#unit-test-question-id-label-0")
        .containsMatchingElement(questionUnansweredTooltip)
    ).toEqual(true);
    expect(questionPreview.containsMatchingElement(questionSeenTooltip)).toEqual(false);
    expect(questionPreview.containsMatchingElement(questionUnseenTooltip)).toEqual(true);
  });
});

describe("It renders the correct images", () => {
  it("unreadIcon", () => {
    testCore(false, true, false, false);
  });
  it("readIcon", () => {
    testCore(true, true, false, false);
  });
  it("answeredIcon", () => {
    testCore(true, true, true, false);
  });
  it("unansweredIcon", () => {
    testCore(true, true, false, false);
  });
  it("reviewIcon", () => {
    testCore(true, true, false, true);
  });
  it("no reviewIcon", () => {
    testCore(true, true, false, false);
  });
});

const testCore = function(isRead, isSelected, isAnswered, isReview) {
  // READ/UNREAD CHECK
  // defaults, or if unread
  let buttonBackgroundColor = styles.buttonUnreadBackground;
  if (isRead) {
    // if it is read
    buttonBackgroundColor = styles.buttonReadBackground;
  }

  // SELECTED/UNSELECTED CHECK
  // defaults, or unselected
  let buttonTextColor = styles.buttonUnselectedText;
  let imageStyle = styles.buttonUnselectedSymbol;
  if (isSelected) {
    // if it is selected
    buttonBackgroundColor = styles.buttonSelectedBackground;
    buttonTextColor = styles.buttonSelectedText;
    imageStyle = styles.buttonSelectedText;
  }

  const wrapper = mount(
    <Provider store={mockStore(initialState)}>
      <QuestionPreview
        index={0}
        questionId={0}
        isAnswered={isAnswered}
        isRead={isRead}
        isSelected={isSelected}
        isMarkedForReview={isReview}
        accommodations={{
          fontFamily: "Nunito Sans"
        }}
      />
    </Provider>
  );
  if (isRead) {
    expect(wrapper.find("#unit-test-seen-0").exists()).toEqual(true);
  }
  if (!isRead) {
    expect(wrapper.find("#unit-test-unseen-0").exists()).toEqual(true);
  }
  if (isAnswered) {
    expect(wrapper.find("#unit-test-answered-0").exists()).toEqual(true);
  }
  if (!isAnswered) {
    expect(wrapper.find("#unit-test-unanswered-0").exists()).toEqual(true);
  }
  if (isReview) {
    expect(wrapper.find("#unit-test-review-0").exists()).toEqual(true);
  }
  if (!isReview) {
    expect(wrapper.find("#unit-test-review").exists()).toEqual(false);
  }
};
