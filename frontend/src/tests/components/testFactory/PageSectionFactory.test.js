import React from "react";
import { shallow } from "enzyme";
import { UnconnectedPageSectionFactory as PageSectionFactory } from "../../../components/testFactory/PageSectionFactory";
import {
  MARKDOWN_SECTION,
  SAMPLE_EMAIL_SECTION,
  SAMPLE_EMAIL_RESPONSE_SECTION,
  SAMPLE_TASK_RESPONSE_SECTION,
  SAMPLE_IMAGE_SECTION,
  SAMPLE_TREE_DESC_SECTION,
  UNSUPPORTED_TYPE_SECTION,
  PRIVACY_NOTICE_SECTION
} from "./sampleData";

describe("Renders correct page section type", () => {
  it("Markdown", () => {
    const wrapper = shallow(
      <PageSectionFactory sectionComponentPage={MARKDOWN_SECTION} setNextButtonDisabled={false} />
    );
    expect(wrapper.find("#unit-test-page-section-factory-markdown").exists()).toEqual(true);
  });
  it("Image", () => {
    const wrapper = shallow(
      <PageSectionFactory
        sectionComponentPage={SAMPLE_IMAGE_SECTION}
        setNextButtonDisabled={false}
      />
    );
    expect(wrapper.find("#unit-test-page-section-factory-image").exists()).toEqual(true);
  });
  it("Tree desc", () => {
    const wrapper = shallow(
      <PageSectionFactory
        sectionComponentPage={SAMPLE_TREE_DESC_SECTION}
        setNextButtonDisabled={false}
      />
    );
    expect(wrapper.find("#unit-test-page-section-factory-tree-desc").exists()).toEqual(true);
  });
  it("sample email", () => {
    const wrapper = shallow(
      <PageSectionFactory
        sectionComponentPage={SAMPLE_EMAIL_SECTION}
        setNextButtonDisabled={false}
      />
    );
    expect(wrapper.find("#unit-test-page-section-factory-sample-email").exists()).toEqual(true);
  });
  it("sample email response", () => {
    const wrapper = shallow(
      <PageSectionFactory
        sectionComponentPage={SAMPLE_EMAIL_RESPONSE_SECTION}
        setNextButtonDisabled={false}
      />
    );
    expect(wrapper.find("#unit-test-page-section-factory-sample-email-response").exists()).toEqual(
      true
    );
  });
  it("sample task response", () => {
    const wrapper = shallow(
      <PageSectionFactory
        sectionComponentPage={SAMPLE_TASK_RESPONSE_SECTION}
        setNextButtonDisabled={false}
      />
    );
    expect(wrapper.find("#unit-test-page-section-factory-sample-task-response").exists()).toEqual(
      true
    );
  });
  it("privacy notice statement", () => {
    const wrapper = shallow(
      <PageSectionFactory
        sectionComponentPage={PRIVACY_NOTICE_SECTION}
        setNextButtonDisabled={false}
      />
    );
    expect(wrapper.find("#unit-test-page-section-factory-pns").exists()).toEqual(true);
  });
  it("unsupported type", () => {
    const wrapper = shallow(
      <PageSectionFactory
        sectionComponentPage={UNSUPPORTED_TYPE_SECTION}
        setNextButtonDisabled={false}
      />
    );
    expect(wrapper.find("#unit-test-page-section-factory-unsupported").exists()).toEqual(true);
  });
});
