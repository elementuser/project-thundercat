import React from "react";
import { shallow } from "enzyme";
import { UnconnectedTestSectionComponentFactory as TestSectionComponentFactory } from "../../../components/testFactory/TestSectionComponentFactory";
import {
  SINGLE_PAGE,
  TOP_TABS,
  FINISH,
  QUIT,
  UNSUPPORTED,
  PROCEED_BUTTON,
  POPUP_BUTTON
} from "./SampleTestSectionComponentFactoryData";

describe("Renders correct single component type", () => {
  it("single page", () => {
    const wrapper = shallow(
      <TestSectionComponentFactory
        testSection={SINGLE_PAGE.testSection}
        handleNext={() => {}}
        handleFinishTest={() => {}}
        timeLimit={SINGLE_PAGE.testSection.default_time}
        testSectionStartTime={SINGLE_PAGE.testSectionStartTime}
        timeout={() => {}}
        testSectionType={SINGLE_PAGE.testSection.type}
        resetAllRedux={() => {}}
        defaultTab={SINGLE_PAGE.testSection.default_tab}
        usesNotepad={SINGLE_PAGE.testSection.uses_notepad}
        usesCalculator={SINGLE_PAGE.testSection.uses_calculator}
        accommodations={{ fontSize: "16px", fontFamily: "Nunito Sans" }}
        newTestDefinition={{ test_definition: [{ is_public: false }] }}
        setContentUnLoaded={() => {}}
      />
    );
    expect(wrapper.find("#unit-test-single-component-factory").exists()).toEqual(true);
  });
  it("top tabs", () => {
    const wrapper = shallow(
      <TestSectionComponentFactory
        testSection={TOP_TABS.testSection}
        handleNext={() => {}}
        handleFinishTest={() => {}}
        timeLimit={TOP_TABS.testSection.default_time}
        testSectionStartTime={TOP_TABS.testSectionStartTime}
        timeout={() => {}}
        testSectionType={TOP_TABS.testSection.type}
        resetAllRedux={() => {}}
        defaultTab={TOP_TABS.testSection.default_tab}
        usesNotepad={TOP_TABS.testSection.uses_notepad}
        usesCalculator={SINGLE_PAGE.testSection.uses_calculator}
        accommodations={{ fontSize: "16px", fontFamily: "Nunito Sans" }}
        newTestDefinition={{ test_definition: [{ is_public: false }] }}
        setContentUnLoaded={() => {}}
      />
    );
    expect(wrapper.find("#unit-test-top-tabs-component").exists()).toEqual(true);
  });
  it("finish page", () => {
    const wrapper = shallow(
      <TestSectionComponentFactory
        testSection={FINISH.testSection}
        handleNext={() => {}}
        handleFinishTest={() => {}}
        timeLimit={FINISH.testSection.default_time}
        testSectionStartTime={FINISH.testSectionStartTime}
        timeout={() => {}}
        testSectionType={FINISH.testSection.type}
        resetAllRedux={() => {}}
        defaultTab={FINISH.testSection.default_tab}
        usesNotepad={FINISH.testSection.uses_notepad}
        usesCalculator={SINGLE_PAGE.testSection.uses_calculator}
        accommodations={{ fontSize: "16px", fontFamily: "Nunito Sans" }}
        newTestDefinition={{ test_definition: [{ is_public: false }] }}
        setContentUnLoaded={() => {}}
      />
    );
    expect(wrapper.find("#unit-test-single-component-factory").exists()).toEqual(true);
  });
  it("quit page", () => {
    const wrapper = shallow(
      <TestSectionComponentFactory
        testSection={QUIT.testSection}
        handleNext={() => {}}
        handleFinishTest={() => {}}
        timeLimit={QUIT.testSection.default_time}
        testSectionStartTime={QUIT.testSectionStartTime}
        timeout={() => {}}
        testSectionType={QUIT.testSection.type}
        resetAllRedux={() => {}}
        defaultTab={QUIT.testSection.default_tab}
        usesNotepad={QUIT.testSection.uses_notepad}
        usesCalculator={SINGLE_PAGE.testSection.uses_calculator}
        accommodations={{ fontSize: "16px", fontFamily: "Nunito Sans" }}
        newTestDefinition={{ test_definition: [{ is_public: false }] }}
        setContentUnLoaded={() => {}}
      />
    );
    expect(wrapper.find("#unit-test-single-component-factory").exists()).toEqual(true);
  });
  it("unsupported type", () => {
    const wrapper = shallow(
      <TestSectionComponentFactory
        testSection={UNSUPPORTED.testSection}
        handleNext={() => {}}
        handleFinishTest={() => {}}
        timeLimit={UNSUPPORTED.testSection.default_time}
        testSectionStartTime={UNSUPPORTED.testSectionStartTime}
        timeout={() => {}}
        testSectionType={UNSUPPORTED.testSection.type}
        resetAllRedux={() => {}}
        defaultTab={UNSUPPORTED.testSection.default_tab}
        usesNotepad={UNSUPPORTED.testSection.uses_notepad}
        usesCalculator={SINGLE_PAGE.testSection.uses_calculator}
        accommodations={{ fontSize: "16px", fontFamily: "Nunito Sans" }}
        newTestDefinition={{ test_definition: [{ is_public: false }] }}
        setContentUnLoaded={() => {}}
      />
    );
    expect(wrapper.find("#unit-test-unsupported-component").exists()).toEqual(true);
  });
});

describe("Renders correct button/footer type", () => {
  it("go home - finish", () => {
    const wrapper = shallow(
      <TestSectionComponentFactory
        testSection={FINISH.testSection}
        handleNext={() => {}}
        handleFinishTest={() => {}}
        timeLimit={FINISH.testSection.default_time}
        testSectionStartTime={FINISH.testSectionStartTime}
        timeout={() => {}}
        testSectionType={FINISH.testSection.type}
        resetAllRedux={() => {}}
        defaultTab={FINISH.testSection.default_tab}
        usesNotepad={FINISH.testSection.uses_notepad}
        usesCalculator={SINGLE_PAGE.testSection.uses_calculator}
        accommodations={{ fontSize: "16px", fontFamily: "Nunito Sans" }}
        newTestDefinition={{ test_definition: [{ is_public: false }] }}
        setContentUnLoaded={() => {}}
      />
    );
    expect(wrapper.find("#unit-test-go-home-button").exists()).toEqual(true);
  });
  it("go home -  quit", () => {
    const wrapper = shallow(
      <TestSectionComponentFactory
        testSection={QUIT.testSection}
        handleNext={() => {}}
        handleFinishTest={() => {}}
        timeLimit={QUIT.testSection.default_time}
        testSectionStartTime={QUIT.testSectionStartTime}
        timeout={() => {}}
        testSectionType={QUIT.testSection.type}
        resetAllRedux={() => {}}
        defaultTab={QUIT.testSection.default_tab}
        usesNotepad={QUIT.testSection.uses_notepad}
        usesCalculator={SINGLE_PAGE.testSection.uses_calculator}
        accommodations={{ fontSize: "16px", fontFamily: "Nunito Sans" }}
        newTestDefinition={{ test_definition: [{ is_public: false }] }}
        setContentUnLoaded={() => {}}
      />
    );
    expect(wrapper.find("#unit-test-go-home-button").exists()).toEqual(true);
  });
  it("proceed", () => {
    const wrapper = shallow(
      <TestSectionComponentFactory
        testSection={PROCEED_BUTTON.testSection}
        handleNext={() => {}}
        handleFinishTest={() => {}}
        timeLimit={PROCEED_BUTTON.testSection.default_time}
        testSectionStartTime={PROCEED_BUTTON.testSectionStartTime}
        timeout={() => {}}
        testSectionType={PROCEED_BUTTON.testSection.type}
        resetAllRedux={() => {}}
        defaultTab={PROCEED_BUTTON.testSection.default_tab}
        usesNotepad={PROCEED_BUTTON.testSection.uses_notepad}
        usesCalculator={SINGLE_PAGE.testSection.uses_calculator}
        accommodations={{ fontSize: "16px", fontFamily: "Nunito Sans" }}
        newTestDefinition={{ test_definition: [{ is_public: false }] }}
        setContentUnLoaded={() => {}}
      />
    );
    expect(wrapper.find("#unit-test-proceed-button").exists()).toEqual(true);
  });
  it("popup", () => {
    const wrapper = shallow(
      <TestSectionComponentFactory
        testSection={POPUP_BUTTON.testSection}
        handleNext={() => {}}
        handleFinishTest={() => {}}
        timeLimit={POPUP_BUTTON.testSection.default_time}
        testSectionStartTime={POPUP_BUTTON.testSectionStartTime}
        timeout={() => {}}
        testSectionType={POPUP_BUTTON.testSection.type}
        resetAllRedux={() => {}}
        defaultTab={POPUP_BUTTON.testSection.default_tab}
        usesNotepad={POPUP_BUTTON.testSection.uses_notepad}
        usesCalculator={SINGLE_PAGE.testSection.uses_calculator}
        accommodations={{ fontSize: "16px", fontFamily: "Nunito Sans" }}
        newTestDefinition={{ test_definition: [{ is_public: false }] }}
        setContentUnLoaded={() => {}}
      />
    );
    expect(wrapper.find("#unit-test-popup-button").exists()).toEqual(true);
  });
});
