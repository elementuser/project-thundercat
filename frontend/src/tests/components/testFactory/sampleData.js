export let MARKDOWN_SECTION = {
  page_sections: [
    {
      id: 19,
      page_section_content: {
        content:
          "# Test instructions\r\n\r\nWhen you start the test, first read the background information which describes your position and the fictitious organization you work for. We recommend you take approximately 10 minutes to read it. Then proceed to the in-box where you can read the emails you received and take action to respond to them as though you were a manager within the fictitious organization.\r\n\r\nWhile you are in the email in-box, you will have access to the following:\r\n\r\n-   The test instructions;\r\n-   The background information describing your job as the manager and the fictitious organization where you work;\r\n-   A notepad to serve as scrap paper.\r\n\r\n## Step 1 - Responding to emails\r\n\r\nYou can respond to the emails you received in two ways: by writing an email or by adding tasks to your task list. A description of both ways of responding is presented below, followed by examples."
      },
      order: 1,
      page_section_type: 1,
      section_component_page: 10
    }
  ]
};

export let SAMPLE_EMAIL_SECTION = {
  page_sections: [
    {
      id: 20,
      page_section_content: {
        email_id: 1,
        from_field: "Geneviève Bédard (Director, Research and Innovation Unit)",
        to_field: "T.C. Bernier (Manager, Quality Assurance Team)",
        date_field: "Friday, November 4",
        body:
          "Hello T.C.,\r\n\r\nI was pleased to hear that one of your quality assurance analysts, Mary Woodside, has accepted a six-month assignment with my team, starting on January 2. I understand she has experience in teaching and using modern teaching tools from her previous work as a college professor. My team needs help developing innovative teaching techniques that promote employee productivity and general well-being. Therefore, I think Mary’s experience will be a good asset for the team.\r\n\r\nAre there any areas in which you might want Mary to gain more experience and which could be of value when she returns to your team? I want to maximize the benefits of the assignment for both our teams.\r\n\r\nLooking forward to getting your input,\r\n\r\nGeneviève",
        subject_field: "Preparing Mary for her assignment"
      },
      order: 2,
      page_section_type: 3,
      section_component_page: 10
    }
  ]
};

export let SAMPLE_EMAIL_RESPONSE_SECTION = {
  page_sections: [
    {
      id: 22,
      page_section_content: {
        cc_field: "",
        to_field: "Geneviève Bédard",
        response:
          "Hi Geneviève,\r\n\r\nI agree that we should plan Mary’s assignment so both our teams can benefit from it. I suggest that we train Mary in the synthesis of data from multiple sources. Doing so could help her broaden her skill set and would be beneficial to my team when she comes back. Likewise, your team members could benefit from her past experience in teaching. I’ll consult her directly as I would like to have her input on this. I’ll get back to you later this week once I have more information to provide you on this matter.\r\n\r\nThat being said, what are your expectations? Are there any current challenges or specific team dynamics that should be taken into account? I’d like to consider all factors, such as the current needs of your team, challenges and team dynamics before I meet with Mary to discuss her assignment.\r\n\r\nThanks,\r\n\r\nT.C."
      },
      order: 4,
      page_section_type: 4,
      section_component_page: 10
    }
  ]
};

export let SAMPLE_TASK_RESPONSE_SECTION = {
  page_sections: [
    {
      id: 24,
      page_section_content: {
        response:
          "- Reply to Geneviève’s email:\r\n     > Suggest training Mary in the synthesis of information from multiple sources so that she can broaden her skill set.\r\n     > Ask what her expectations are and what the challenges are on her team so I can consider all factors when determining how her team could benefit from Mary’s experience in providing training.\r\n     > Inform her that I will get more information from Mary and will respond with suggestions by the end of the week.\r\n- Schedule a meeting with Mary to discuss her assignment objectives and ensure she feels engaged and knows what is expected of her.\r\n- Refer to Mary’s past and current objectives to ensure that what I propose is in line with her professional development plan.",
        reason: ""
      },
      order: 6,
      page_section_type: 5,
      section_component_page: 10
    }
  ]
};

export let SAMPLE_TREE_DESC_SECTION = {
  page_sections: [
    {
      id: 35,
      page_section_content: {
        address_book: [
          {
            id: 1,
            team_information_tree_child: [
              {
                id: 2,
                team_information_tree_child: [
                  {
                    id: 6,
                    team_information_tree_child: null,
                    text: {
                      id: 6,
                      parent: 2,
                      test_section_component: [10, 11],
                      name: "Marc Sheridan",
                      title: "Manager, Human Resources",
                      label: "Marc Sheridan (Manager, Human Resources)",
                      value: 6,
                      language: "en",
                      contact: 6,
                      text: "Marc Sheridan (Manager, Human Resources)"
                    },
                    parent: 2,
                    test_section_component: [10, 11]
                  },
                  {
                    id: 7,
                    team_information_tree_child: null,
                    text: {
                      id: 7,
                      parent: 2,
                      test_section_component: [10, 11],
                      name: "Bob McNutt",
                      title: "Manager, Finance",
                      label: "Bob McNutt (Manager, Finance)",
                      value: 7,
                      language: "en",
                      contact: 7,
                      text: "Bob McNutt (Manager, Finance)"
                    },
                    parent: 2,
                    test_section_component: [10, 11]
                  },
                  {
                    id: 8,
                    team_information_tree_child: null,
                    text: {
                      id: 8,
                      parent: 2,
                      test_section_component: [10, 11],
                      name: "Lana Hussad",
                      title: "Manager, Information Technology",
                      label: "Lana Hussad (Manager, Information Technology)",
                      value: 8,
                      language: "en",
                      contact: 8,
                      text: "Lana Hussad (Manager, Information Technology)"
                    },
                    parent: 2,
                    test_section_component: [10, 11]
                  }
                ],
                text: {
                  id: 2,
                  parent: 1,
                  test_section_component: [10, 11],
                  name: "Amari Kinsler",
                  title: "Director, Corporate Affairs Unit",
                  label: "Amari Kinsler (Director, Corporate Affairs Unit)",
                  value: 2,
                  language: "en",
                  contact: 2,
                  text: "Amari Kinsler (Director, Corporate Affairs Unit)"
                },
                parent: 1,
                test_section_component: [10, 11]
              },
              {
                id: 3,
                team_information_tree_child: null,
                text: {
                  id: 3,
                  parent: 1,
                  test_section_component: [10, 11],
                  name: "Geneviève Bédard",
                  title: "Director, Research and Innovations Unit",
                  label: "Geneviève Bédard (Director, Research and Innovations Unit)",
                  value: 3,
                  language: "en",
                  contact: 3,
                  text: "Geneviève Bédard (Director, Research and Innovations Unit)"
                },
                parent: 1,
                test_section_component: [10, 11]
              },
              {
                id: 4,
                team_information_tree_child: null,
                text: {
                  id: 4,
                  parent: 1,
                  test_section_component: [10, 11],
                  name: "Bartosz Greco",
                  title: "Director, Program Development Unit",
                  label: "Bartosz Greco (Director, Program Development Unit)",
                  value: 4,
                  language: "en",
                  contact: 4,
                  text: "Bartosz Greco (Director, Program Development Unit)"
                },
                parent: 1,
                test_section_component: [10, 11]
              },
              {
                id: 5,
                team_information_tree_child: [
                  {
                    id: 9,
                    team_information_tree_child: null,
                    text: {
                      id: 9,
                      parent: 5,
                      test_section_component: [10, 11],
                      name: "Lucy Trang",
                      title: "Manager, E-Training",
                      label: "Lucy Trang (Manager, E-Training)",
                      value: 9,
                      language: "en",
                      contact: 9,
                      text: "Lucy Trang (Manager, E-Training)"
                    },
                    parent: 5,
                    test_section_component: [10, 11]
                  },
                  {
                    id: 10,
                    team_information_tree_child: null,
                    text: {
                      id: 10,
                      parent: 5,
                      test_section_component: [10, 11],
                      name: "Geoffrey Hamma",
                      title: "Manager, Audits",
                      label: "Geoffrey Hamma (Manager, Audits)",
                      value: 10,
                      language: "en",
                      contact: 10,
                      text: "Geoffrey Hamma (Manager, Audits)"
                    },
                    parent: 5,
                    test_section_component: [10, 11]
                  },
                  {
                    id: 11,
                    team_information_tree_child: null,
                    text: {
                      id: 11,
                      parent: 5,
                      test_section_component: [10, 11],
                      name: "Haydar Kalil",
                      title: "Manager, Services and Support",
                      label: "Haydar Kalil (Manager, Services and Support)",
                      value: 11,
                      language: "en",
                      contact: 11,
                      text: "Haydar Kalil (Manager, Services and Support)"
                    },
                    parent: 5,
                    test_section_component: [10, 11]
                  },
                  {
                    id: 12,
                    team_information_tree_child: [
                      {
                        id: 13,
                        team_information_tree_child: null,
                        text: {
                          id: 13,
                          parent: 12,
                          test_section_component: [10, 11],
                          name: "Danny McBride",
                          title: "Quality Assurance Analyst, Quality Assurance Team",
                          label:
                            "Danny McBride (Quality Assurance Analyst, Quality Assurance Team)",
                          value: 13,
                          language: "en",
                          contact: 13,
                          text: "Danny McBride (Quality Assurance Analyst, Quality Assurance Team)"
                        },
                        parent: 12,
                        test_section_component: [10, 11]
                      },
                      {
                        id: 14,
                        team_information_tree_child: null,
                        text: {
                          id: 14,
                          parent: 12,
                          test_section_component: [10, 11],
                          name: "Serge Duplessis",
                          title: "Quality Assurance Analyst, Quality Assurance Team",
                          label:
                            "Serge Duplessis (Quality Assurance Analyst, Quality Assurance Team)",
                          value: 14,
                          language: "en",
                          contact: 14,
                          text:
                            "Serge Duplessis (Quality Assurance Analyst, Quality Assurance Team)"
                        },
                        parent: 12,
                        test_section_component: [10, 11]
                      },
                      {
                        id: 15,
                        team_information_tree_child: null,
                        text: {
                          id: 15,
                          parent: 12,
                          test_section_component: [10, 11],
                          name: "Marina Richter",
                          title: "Quality Assurance Analyst, Quality Assurance Team",
                          label:
                            "Marina Richter (Quality Assurance Analyst, Quality Assurance Team)",
                          value: 15,
                          language: "en",
                          contact: 15,
                          text: "Marina Richter (Quality Assurance Analyst, Quality Assurance Team)"
                        },
                        parent: 12,
                        test_section_component: [10, 11]
                      },
                      {
                        id: 16,
                        team_information_tree_child: null,
                        text: {
                          id: 16,
                          parent: 12,
                          test_section_component: [10, 11],
                          name: "Mary Woodside",
                          title: "Quality Assurance Analyst, Quality Assurance Team",
                          label:
                            "Mary Woodside (Quality Assurance Analyst, Quality Assurance Team)",
                          value: 16,
                          language: "en",
                          contact: 16,
                          text: "Mary Woodside (Quality Assurance Analyst, Quality Assurance Team)"
                        },
                        parent: 12,
                        test_section_component: [10, 11]
                      },
                      {
                        id: 17,
                        team_information_tree_child: null,
                        text: {
                          id: 17,
                          parent: 12,
                          test_section_component: [10, 11],
                          name: "Charlie Wang",
                          title: "Quality Assurance Analyst, Quality Assurance Team",
                          label: "Charlie Wang (Quality Assurance Analyst, Quality Assurance Team)",
                          value: 17,
                          language: "en",
                          contact: 17,
                          text: "Charlie Wang (Quality Assurance Analyst, Quality Assurance Team)"
                        },
                        parent: 12,
                        test_section_component: [10, 11]
                      },
                      {
                        id: 18,
                        team_information_tree_child: null,
                        text: {
                          id: 18,
                          parent: 12,
                          test_section_component: [10, 11],
                          name: "Jack Laurier",
                          title: "Quality Assurance Analyst, Quality Assurance Team",
                          label: "Jack Laurier (Quality Assurance Analyst, Quality Assurance Team)",
                          value: 18,
                          language: "en",
                          contact: 18,
                          text: "Jack Laurier (Quality Assurance Analyst, Quality Assurance Team)"
                        },
                        parent: 12,
                        test_section_component: [10, 11]
                      }
                    ],
                    text: {
                      id: 12,
                      parent: 5,
                      test_section_component: [10, 11],
                      name: "T.C. Bernier",
                      title: "Manager, Quality Assurance",
                      label: "T.C. Bernier (Manager, Quality Assurance)",
                      value: 12,
                      language: "en",
                      contact: 12,
                      text: "T.C. Bernier (Manager, Quality Assurance)"
                    },
                    parent: 5,
                    test_section_component: [10, 11]
                  }
                ],
                text: {
                  id: 5,
                  parent: 1,
                  test_section_component: [10, 11],
                  name: "Nancy Ward",
                  title: "Director, Services and Communications Unit",
                  label: "Nancy Ward (Director, Services and Communications Unit)",
                  value: 5,
                  language: "en",
                  contact: 5,
                  text: "Nancy Ward (Director, Services and Communications Unit)"
                },
                parent: 1,
                test_section_component: [10, 11]
              }
            ],
            text: {
              id: 1,
              parent: null,
              test_section_component: [10, 11],
              name: "Jenna Icard",
              title: "President, Organizational Development Council",
              label: "Jenna Icard (President, Organizational Development Council)",
              value: 1,
              language: "en",
              contact: 1,
              text: "Jenna Icard (President, Organizational Development Council)"
            },
            parent: null,
            test_section_component: [10, 11]
          }
        ]
      },
      order: 3,
      page_section_type: 6,
      section_component_page: 15
    }
  ]
};

export let SAMPLE_IMAGE_SECTION = {
  page_sections: [
    {
      id: 34,
      page_section_content: {
        id: 1,
        large_image: "org_chart_en.png",
        small_image: "org_chart_en.png",
        page_section: 34,
        language: "en"
      },
      order: 2,
      page_section_type: 2,
      section_component_page: 15
    }
  ]
};

export let PRIVACY_NOTICE_SECTION = {
  page_sections: [
    {
      id: 34,
      page_section_content: {
        id: 1,
        large_image: "org_chart_en.png",
        small_image: "org_chart_en.png",
        page_section: 34,
        language: "en"
      },
      order: 2,
      page_section_type: 7,
      section_component_page: 15
    }
  ]
};

export let UNSUPPORTED_TYPE_SECTION = {
  page_sections: [
    {
      id: 34,
      page_section_content: {
        id: 1,
        large_image: "org_chart_en.png",
        small_image: "org_chart_en.png",
        page_section: 34,
        language: "en"
      },
      order: 2,
      page_section_type: 20000,
      section_component_page: 15
    }
  ]
};
