export let SINGLE_PAGE_TYPE_SECTION = {
  order: 1,
  id: 7,
  title: "Overview TSC",
  component_type: 1,
  test_section_component: [
    {
      order: 1,
      id: 6,
      title: "Overview1",
      pages: {
        page_sections: [
          {
            id: 6,
            page_section_content: {
              content:
                '# Sample e-MIB\r\n\r\n## Overview\r\n\r\nThe electronic Managerial In-Box (e-MIB) simulates an email in-box containing a series of emails depicting situations typically encountered by managers in the federal public service. You must respond to these emails. The situations presented will provide you with the opportunity to demonstrate the Key Leadership Competencies. More information about these competencies is provided in the "Evaluation" section.\r\n\r\nThe next page will allow you to:\r\n\r\n-   read detailed instructions on how to complete the test;\r\n-   see examples of how to respond to emails within the inbox;\r\n-   explore the test environment before taking the real test.\r\n\r\n## About the sample test\r\n\r\nThe sample test has been designed to provide you with the opportunity to familiarize yourself with:\r\n\r\n-   the components of the test (e.g., instructions, background information, email in-box and notepad); and\r\n-   the features of the test interface (e.g., menu bars, buttons, etc.).\r\n\r\nThe background information includes a description of the organization and your role, as well as information on your employees, colleagues and the management team. The background information and the emails are only examples. They reflect neither the length nor the level of difficulty of the real test.\r\n\r\n## Differences between the sample test and the real test\r\n\r\n-   There is no time limit in the sample test. Take your time to familiarize yourself with the interface. Please note that the real test will have a timer with a time limit of 3 hours.\r\n-   The sample test consists of 3 emails which you may respond to. In the real test, there will be more emails and more background information.'
            },
            order: 1,
            page_section_type: 1,
            section_component_page: 6
          }
        ]
      }
    }
  ]
};
export let SIDE_NAV_TYPE_SECTION = {
  order: 1,
  id: 9,
  title: "Instructions",
  component_type: 2,
  test_section_component: [
    {
      order: 1,
      id: 10,
      title: "Test Instructions",
      pages: {
        page_sections: [
          {
            id: 19,
            page_section_content: {
              content:
                "# Test instructions\r\n\r\nWhen you start the test, first read the background information which describes your position and the fictitious organization you work for. We recommend you take approximately 10 minutes to read it. Then proceed to the in-box where you can read the emails you received and take action to respond to them as though you were a manager within the fictitious organization.\r\n\r\nWhile you are in the email in-box, you will have access to the following:\r\n\r\n-   The test instructions;\r\n-   The background information describing your job as the manager and the fictitious organization where you work;\r\n-   A notepad to serve as scrap paper.\r\n\r\n## Step 1 - Responding to emails\r\n\r\nYou can respond to the emails you received in two ways: by writing an email or by adding tasks to your task list. A description of both ways of responding is presented below, followed by examples."
            },
            order: 1,
            page_section_type: 1,
            section_component_page: 10
          }
        ]
      }
    },
    {
      order: 2,
      id: 11,
      title: "Tips on taking the e-MIB",
      pages: {
        page_sections: [
          {
            id: 29,
            page_section_content: {
              content:
                "# Tips on taking the e-MIB\r\n\r\nThe e-MIB presents you with situations that will give you the opportunity to demonstrate the Key Leadership Competencies. Here are some tips that will help you provide assessors with the information they need to evaluate your performance on the Key Leadership Competencies:\r\n\r\n-   Answer all the questions asked in the emails you received. Also, note that there may be situations that span several emails and for which no specific questions are asked, but that you can respond to. This will ensure that you take advantage of all the opportunities provided to demonstrate the competencies.\r\n-   Don’t hesitate to provide your recommendations where appropriate, even if they are only initial thoughts. If needed, you can then note other information you would need to reach a final decision.\r\n-   For situations in which you feel you need to speak with someone in person before you can make a decision, you should indicate what information you need and how this would affect your decision one way or the other.\r\n-   Use only the information provided in the emails and the background information. Do not make any inferences based on the culture of your own organization. Avoid making assumptions that are not reasonably supported by either the background information or the emails.\r\n-   The e-MIB is set within a specific industry in order to provide enough context for the situations presented. However, effective responses rely on the competency targeted and not on specific knowledge of the industry.\r\n\r\n## Other important information\r\n\r\n-   You will be assessed on the content of your emails, the information provided in your task list and the reasons listed for your actions. Information left on your notepad will not be evaluated.\r\n-   You will not be assessed on how you write. No points will be deducted for spelling, grammar, punctuation or incomplete sentences. However, your writing will need to be clear enough to ensure that the assessors understand which situation you are responding to and your main points.\r\n-   You can answer the emails in any order you want.\r\n-   You are responsible for managing your own time."
            },
            order: 1,
            page_section_type: 1,
            section_component_page: 11
          }
        ]
      }
    },
    {
      order: 3,
      id: 12,
      title: "Evaluation",
      pages: {
        page_sections: [
          {
            id: 30,
            page_section_content: {
              content:
                "# Evaluation\r\n\r\n-   Both the actions you take and the explanations you give will be considered when assessing your performance on each of the Key Leadership Competencies (described below). You will be assessed on the extent to which they demonstrate the Key Leadership Competencies.\r\n-   Your actions will be evaluated on effectiveness. Effectiveness is measured by whether your actions would have a positive or a negative impact on resolving the situations presented and by how widespread that impact would be.\r\n-   Your responses will also be evaluated on how well they meet the organizational objectives presented in the background information.\r\n\r\n## Key Leadership Competencies\r\n\r\n**Create Vision and Strategy**: Managers help to define the future and chart a path forward. To do so, they take into account context. They leverage their knowledge and seek and integrate information from diverse sources to implement concrete activities. They consider different perspectives and consult as needed. Managers balance organizational priorities and improve outcomes.\r\n\r\n**Mobilize People**: Managers inspire and motivate the people they lead. They manage the performance of their employees and provide constructive and respectful feedback to encourage and enable performance excellence. They lead by example, setting goals for themselves that are more demanding than those that they set for others.\r\n\r\n**Uphold Integrity and Respect**: Managers exemplify ethical practices, professionalism and personal integrity, acting in the interest of Canada and Canadians. They create respectful, inclusive and trusting work environments where sound advice is valued. They encourage the expression of diverse opinions and perspectives, while fostering collegiality.\r\n\r\n**Collaborate with Partners and Stakeholders**: Managers are deliberate and resourceful about seeking a wide spectrum of perspectives. In building partnerships, they manage expectations and aim to reach consensus. They demonstrate openness and flexibility to improve outcomes and bring a whole-of-organization perspective to their interactions. Managers acknowledge the role of partners in achieving outcomes.\r\n\r\n**Promote Innovation and Guide Change**: Managers create an environment that supports bold thinking, experimentation and intelligent risk taking. When implementing change, managers maintain momentum, address resistance and anticipate consequences. They use setbacks as a valuable source of insight and learning.\r\n\r\n**Achieve Results**: Managers ensure that they meet team objectives by managing resources. They anticipate, plan, monitor progress and adjust as needed. They demonstrate awareness of the context when making decisions. Managers take personal responsibility for their actions and the outcomes of their decisions."
            },
            order: 1,
            page_section_type: 1,
            section_component_page: 12
          }
        ]
      }
    }
  ]
};

export let EMAIL_QUESTION_LIST_TYPE_SECTION = {
  order: 3,
  id: 11,
  title: "Inbox",
  component_type: 3,
  test_section_component: {
    questions: [
      {
        id: 3,
        sections: [],
        question_type: 2,
        details: {},
        answers: [],
        email: [
          {
            id: 2,
            to_field: [
              {
                id: 12,
                parent: 5,
                test_section_component: [10, 11],
                name: "T.C. Bernier",
                title: "Manager, Quality Assurance",
                label: "T.C. Bernier (Manager, Quality Assurance)",
                value: 12,
                language: "en",
                contact: 12,
                text: "T.C. Bernier (Manager, Quality Assurance)"
              }
            ],
            from_field: {
              id: 14,
              parent: 12,
              test_section_component: [10, 11],
              name: "Serge Duplessis",
              title: "Quality Assurance Analyst, Quality Assurance Team",
              label: "Serge Duplessis (Quality Assurance Analyst, Quality Assurance Team)",
              value: 14,
              language: "en",
              contact: 14,
              text: "Serge Duplessis (Quality Assurance Analyst, Quality Assurance Team)"
            },
            cc_field: [],
            email_id: 1,
            subject_field: "Bad experience with Serv",
            date_field: "Thursday, November 3",
            body:
              "Hello T.C.,\r\n\r\nAs you are settling into this position, I was hoping to share with you some of my thoughts about the proposed changes to our service requests and documentation practices.\r\n\r\nI have been working on the Quality Assurance team for over 12 years. I feel that, overall, we are quite successful in understanding and processing service requests. Switching to an automated, computerized system would take a very long time to adapt to and could jeopardize the quality of our service. For example, having a face-to-face or telephone conversation with a client can help us better understand the client’s issues in more depth because it allows us to ask probing questions and receive important information related to each case. By buying this new technology, we risk having more IT problems and unexpected delays in the long run.\r\n\r\nI have voiced my opinion in previous meetings, but I do not feel that my opinion matters. Everyone else has been on the team for less than two years and I feel ignored because I’m the oldest member on the team. I urge you to consider my opinion so that we do not make a costly mistake.\r\n\r\nSerge",
            question: 3,
            language: "en"
          }
        ]
      }
    ],
    address_book: [
      {
        id: 1,
        parent: null,
        test_section_component: [10, 11],
        name: "Jenna Icard",
        title: "President, Organizational Development Council",
        label: "Jenna Icard (President, Organizational Development Council)",
        value: 1,
        language: "en",
        contact: 1,
        text: "Jenna Icard (President, Organizational Development Council)"
      },
      {
        id: 2,
        parent: 1,
        test_section_component: [10, 11],
        name: "Amari Kinsler",
        title: "Director, Corporate Affairs Unit",
        label: "Amari Kinsler (Director, Corporate Affairs Unit)",
        value: 2,
        language: "en",
        contact: 2,
        text: "Amari Kinsler (Director, Corporate Affairs Unit)"
      },
      {
        id: 3,
        parent: 1,
        test_section_component: [10, 11],
        name: "Geneviève Bédard",
        title: "Director, Research and Innovations Unit",
        label: "Geneviève Bédard (Director, Research and Innovations Unit)",
        value: 3,
        language: "en",
        contact: 3,
        text: "Geneviève Bédard (Director, Research and Innovations Unit)"
      },
      {
        id: 4,
        parent: 1,
        test_section_component: [10, 11],
        name: "Bartosz Greco",
        title: "Director, Program Development Unit",
        label: "Bartosz Greco (Director, Program Development Unit)",
        value: 4,
        language: "en",
        contact: 4,
        text: "Bartosz Greco (Director, Program Development Unit)"
      },
      {
        id: 5,
        parent: 1,
        test_section_component: [10, 11],
        name: "Nancy Ward",
        title: "Director, Services and Communications Unit",
        label: "Nancy Ward (Director, Services and Communications Unit)",
        value: 5,
        language: "en",
        contact: 5,
        text: "Nancy Ward (Director, Services and Communications Unit)"
      },
      {
        id: 6,
        parent: 2,
        test_section_component: [10, 11],
        name: "Marc Sheridan",
        title: "Manager, Human Resources",
        label: "Marc Sheridan (Manager, Human Resources)",
        value: 6,
        language: "en",
        contact: 6,
        text: "Marc Sheridan (Manager, Human Resources)"
      },
      {
        id: 7,
        parent: 2,
        test_section_component: [10, 11],
        name: "Bob McNutt",
        title: "Manager, Finance",
        label: "Bob McNutt (Manager, Finance)",
        value: 7,
        language: "en",
        contact: 7,
        text: "Bob McNutt (Manager, Finance)"
      },
      {
        id: 8,
        parent: 2,
        test_section_component: [10, 11],
        name: "Lana Hussad",
        title: "Manager, Information Technology",
        label: "Lana Hussad (Manager, Information Technology)",
        value: 8,
        language: "en",
        contact: 8,
        text: "Lana Hussad (Manager, Information Technology)"
      },
      {
        id: 9,
        parent: 5,
        test_section_component: [10, 11],
        name: "Lucy Trang",
        title: "Manager, E-Training",
        label: "Lucy Trang (Manager, E-Training)",
        value: 9,
        language: "en",
        contact: 9,
        text: "Lucy Trang (Manager, E-Training)"
      },
      {
        id: 10,
        parent: 5,
        test_section_component: [10, 11],
        name: "Geoffrey Hamma",
        title: "Manager, Audits",
        label: "Geoffrey Hamma (Manager, Audits)",
        value: 10,
        language: "en",
        contact: 10,
        text: "Geoffrey Hamma (Manager, Audits)"
      },
      {
        id: 11,
        parent: 5,
        test_section_component: [10, 11],
        name: "Haydar Kalil",
        title: "Manager, Services and Support",
        label: "Haydar Kalil (Manager, Services and Support)",
        value: 11,
        language: "en",
        contact: 11,
        text: "Haydar Kalil (Manager, Services and Support)"
      },
      {
        id: 12,
        parent: 5,
        test_section_component: [10, 11],
        name: "T.C. Bernier",
        title: "Manager, Quality Assurance",
        label: "T.C. Bernier (Manager, Quality Assurance)",
        value: 12,
        language: "en",
        contact: 12,
        text: "T.C. Bernier (Manager, Quality Assurance)"
      }
    ]
  }
};

export let QUESTION_LIST_TYPE_SECTION = {
  order: 2,
  id: 4,
  title: "Question List",
  component_type: 4,
  test_section_component: {
    questions: [
      {
        id: 2,
        sections: [
          {
            id: 2,
            question_section_content: { content: "### This question 2\r\ngo away" },
            order: 1,
            question_section_type: 1,
            question: 2
          }
        ],
        question_type: 1,
        details: { diffculty: 1 },
        answers: [
          {
            id: 5,
            content: "Answer 1 to question 2 english",
            scoring_value: 5,
            question: 2,
            language: "en"
          },
          {
            id: 6,
            content: "Answer 2 to question 2 english",
            scoring_value: 0,
            question: 2,
            language: "en"
          }
        ],
        email: []
      },
      {
        id: 1,
        sections: [
          {
            id: 1,
            question_section_content: {
              content: "### This question 1\r\nplease answer with one of the selected UPDATED:"
            },
            order: 1,
            question_section_type: 1,
            question: 1
          }
        ],
        question_type: 1,
        details: { diffculty: 1 },
        answers: [
          {
            id: 2,
            content: "Answer 2 to question 1 english",
            scoring_value: 0,
            question: 1,
            language: "en"
          },
          {
            id: 1,
            content: "Answer 1 to question 1 english",
            scoring_value: 5,
            question: 1,
            language: "en"
          }
        ],
        email: []
      }
    ]
  }
};
