import React from "react";
import { shallow } from "enzyme";
import { UnconnectedTestBuilderTestPage } from "../../../components/testFactory/TestBuilderTestPage";
import { BUILDER_PROPS as testProps } from "./SampleTestPageData";

const functions = {
  setLanguage: jest.fn(),
  getQuestionsList: jest.fn().mockReturnValue(Promise.resolve({ ...testProps.response })),
  updateTestSection: jest.fn(),
  setContentLoaded: jest.fn(),
  setStartTime: jest.fn()
};
const getQuestionsList = jest
  .fn()
  .mockReturnValue(Promise.resolve({ ...testProps.untimedResponse }));
const errorGetQuestionsList = jest
  .fn()
  .mockReturnValue(Promise.resolve({ ok: false, body: { error: "this is an error message" } }));

describe("It calls correct functions", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  it("on mount, timed", async () => {
    const wrapper = shallow(
      <UnconnectedTestBuilderTestPage
        {...testProps}
        {...functions}
        accommodations={{ fontSize: "12px" }}
      />
    );

    await functions.getQuestionsList();
    expect(functions.getQuestionsList).toHaveBeenCalledTimes(1);

    wrapper.setState({
      questionsList: testProps.response.body
    });
    wrapper.update();

    expect(functions.updateTestSection).toHaveBeenCalledTimes(1);
    expect(functions.setStartTime).toHaveBeenCalledTimes(1);
    expect(functions.setContentLoaded).toHaveBeenCalledTimes(1);
  });
  it("on mount, not timed", async () => {
    const wrapper = shallow(
      <UnconnectedTestBuilderTestPage
        {...testProps}
        {...functions}
        accommodations={{ fontSize: "12px" }}
      />
    );
    await getQuestionsList();
    expect(getQuestionsList).toHaveBeenCalledTimes(1);

    wrapper.setState({
      questionsList: testProps.untimedResponse.body
    });
    wrapper.update();

    expect(functions.updateTestSection).toHaveBeenCalledTimes(1);
    expect(functions.setStartTime).toHaveBeenCalledTimes(0);
    expect(functions.setContentLoaded).toHaveBeenCalledTimes(1);
  });
});
describe("It displays", () => {
  it("error message", async () => {
    const wrapper = shallow(
      <UnconnectedTestBuilderTestPage
        {...testProps}
        {...functions}
        accommodations={{ fontSize: "12px" }}
      />
    );
    await errorGetQuestionsList();
    expect(errorGetQuestionsList).toHaveBeenCalledTimes(1);

    wrapper.setState({
      error: true,
      errorMessage: "Some error message"
    });
    wrapper.update();

    expect(functions.updateTestSection).toHaveBeenCalledTimes(0);
    expect(functions.setStartTime).toHaveBeenCalledTimes(0);
    expect(functions.setContentLoaded).toHaveBeenCalledTimes(0);
    expect(wrapper.state("error")).toEqual(true);
  });
});
