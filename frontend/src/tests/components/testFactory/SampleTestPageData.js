export const TEST_PROPS = {
  currentLanguage: "en",
  testSection: {
    id: 6,
    redux: [],
    default_time: 5,
    localize: {
      en: {
        title: "5min",
        type: 1,
        next_section_button: {
          content: "Next",
          title: "Next",
          button_text: "Next",
          confirm_proceed: true,
          next_section_time_limit: 20,
          button_type: 2
        },
        components: [
          {
            order: 1,
            id: 1,
            title: "template",
            component_type: 2,
            test_section_component: [
              {
                order: 1,
                id: 1,
                title: "info",
                pages: {
                  page_sections: [
                    {
                      id: 1,
                      page_section_content: { content: "## Temporary Filler Content" },
                      order: 1,
                      page_section_type: 1,
                      section_component_page: 1
                    }
                  ]
                }
              }
            ]
          }
        ]
      },
      fr: {
        title: "5min",
        type: 1,
        next_section_button: {
          content: "FR Next",
          title: "FR Next",
          button_text: "FR Next",
          confirm_proceed: true,
          next_section_time_limit: 20,
          button_type: 2
        },
        components: [
          {
            order: 1,
            id: 1,
            title: "template",
            component_type: 2,
            test_section_component: [
              {
                order: 1,
                id: 1,
                title: "FR info",
                pages: {
                  page_sections: [
                    {
                      id: 1,
                      page_section_content: { content: "## Temporary Filler Content" },
                      order: 1,
                      page_section_type: 1,
                      section_component_page: 1
                    }
                  ]
                }
              }
            ]
          }
        ]
      }
    },
    is_public: true,
    order: 1,
    section_type: 1,
    en_title: "5min",
    fr_title: "5min",
    en_description: null,
    fr_description: null,
    next_section_button_type: 2,
    scoring_type: 0,
    uses_notepad: false,
    uses_calculator: false,
    block_cheating: true,
    default_tab: 1,
    test_definition: 2,
    start_time: ""
  },
  testSections: {
    en: {
      title: "5min",
      type: 1,
      next_section_button: {
        content: "Next",
        title: "Next",
        button_text: "Next",
        confirm_proceed: true,
        next_section_time_limit: 20,
        button_type: 2
      },
      components: [
        {
          order: 1,
          id: 1,
          title: "template",
          component_type: 2,
          test_section_component: [
            {
              order: 1,
              id: 1,
              title: "info",
              pages: {
                page_sections: [
                  {
                    id: 1,
                    page_section_content: { content: "## Temporary Filler Content" },
                    order: 1,
                    page_section_type: 1,
                    section_component_page: 1
                  }
                ]
              }
            }
          ]
        }
      ]
    }
  },
  testSectionLoaded: false,
  testSectionStartTime: "Thu May 28 2020 11:28:24 GMT-0400 (Eastern Daylight Time)",
  cheatingAttempts: 0,
  testId: 2,
  isTestActive: false,
  isTestLocked: false,
  isTestPaused: false,
  previousStatus: null,
  history: {
    length: 18,
    action: "PUSH",
    location: { pathname: "/sample-tests/test-factory", search: "", hash: "", key: "rzdr6u" }
  },
  location: { pathname: "/sample-tests/test-factory", search: "", hash: "", key: "rzdr6u" },
  match: {
    path: "/sample-tests/test-factory",
    url: "/sample-tests/test-factory",
    isExact: true,
    params: {}
  },
  sampleTest: true
};
export const TEST_PROPS_TIMED_SECTION = {
  currentLanguage: "en",
  testSection: {
    id: 6,
    redux: [],
    default_time: 5,
    localize: {
      en: {
        title: "5min",
        type: 1,
        default_time: 100,
        next_section_button: {
          content: "Next",
          title: "Next",
          button_text: "Next",
          confirm_proceed: true,
          next_section_time_limit: 20,
          button_type: 2
        },
        components: [
          {
            order: 1,
            id: 1,
            title: "template",
            component_type: 2,
            test_section_component: [
              {
                order: 1,
                id: 1,
                title: "info",
                pages: {
                  page_sections: [
                    {
                      id: 1,
                      page_section_content: { content: "## Temporary Filler Content" },
                      order: 1,
                      page_section_type: 1,
                      section_component_page: 1
                    }
                  ]
                }
              }
            ]
          }
        ]
      },
      fr: {
        title: "5min",
        type: 1,
        next_section_button: {
          content: "FR Next",
          title: "FR Next",
          button_text: "FR Next",
          confirm_proceed: true,
          next_section_time_limit: 20,
          button_type: 2
        },
        components: [
          {
            order: 1,
            id: 1,
            title: "template",
            component_type: 2,
            test_section_component: [
              {
                order: 1,
                id: 1,
                title: "FR info",
                pages: {
                  page_sections: [
                    {
                      id: 1,
                      page_section_content: { content: "## Temporary Filler Content" },
                      order: 1,
                      page_section_type: 1,
                      section_component_page: 1
                    }
                  ]
                }
              }
            ]
          }
        ]
      }
    },
    is_public: false,
    order: 1,
    section_type: 1,
    en_title: "5min",
    fr_title: "5min",
    en_description: null,
    fr_description: null,
    next_section_button_type: 2,
    scoring_type: 0,
    uses_notepad: false,
    uses_calculator: false,
    block_cheating: false,
    default_tab: 1,
    test_definition: 2,
    start_time: ""
  },
  testSections: {
    en: {
      title: "5min",
      type: 1,
      next_section_button: {
        content: "Next",
        title: "Next",
        button_text: "Next",
        confirm_proceed: true,
        next_section_time_limit: 20,
        button_type: 2
      },
      components: [
        {
          order: 1,
          id: 1,
          title: "template",
          component_type: 2,
          test_section_component: [
            {
              order: 1,
              id: 1,
              title: "info",
              pages: {
                page_sections: [
                  {
                    id: 1,
                    page_section_content: { content: "## Temporary Filler Content" },
                    order: 1,
                    page_section_type: 1,
                    section_component_page: 1
                  }
                ]
              }
            }
          ]
        }
      ]
    }
  },
  testSectionLoaded: false,
  testSectionStartTime: "Thu May 28 2020 11:28:24 GMT-0400 (Eastern Daylight Time)",
  cheatingAttempts: 0,
  testId: 2,
  isTestActive: true,
  isTestLocked: false,
  isTestPaused: false,
  previousStatus: null,
  history: {
    length: 18,
    action: "PUSH",
    location: { pathname: "/sample-tests/test-factory", search: "", hash: "", key: "rzdr6u" }
  },
  location: { pathname: "/sample-tests/test-factory", search: "", hash: "", key: "rzdr6u" },
  match: {
    path: "/sample-tests/test-factory",
    url: "/sample-tests/test-factory",
    isExact: true,
    params: {}
  },
  sampleTest: false
};
export const TEST_PROPS_NONE_BUTTON = {
  currentLanguage: "en",
  testSection: {
    id: 6,
    redux: [],
    default_time: 5,
    localize: {
      en: {
        title: "5min",
        type: 1,
        next_section_button: {
          content: "Next",
          title: "Next",
          button_text: "Next",
          confirm_proceed: true,
          next_section_time_limit: 20,
          button_type: 2
        },
        components: [
          {
            order: 1,
            id: 1,
            title: "template",
            component_type: 2,
            test_section_component: [
              {
                order: 1,
                id: 1,
                title: "info",
                pages: {
                  page_sections: [
                    {
                      id: 1,
                      page_section_content: { content: "## Temporary Filler Content" },
                      order: 1,
                      page_section_type: 1,
                      section_component_page: 1
                    }
                  ]
                }
              }
            ]
          }
        ]
      },
      fr: {
        title: "5min",
        type: 1,
        next_section_button: {
          content: "FR Next",
          title: "FR Next",
          button_text: "FR Next",
          confirm_proceed: true,
          next_section_time_limit: 20,
          button_type: 2
        },
        components: [
          {
            order: 1,
            id: 1,
            title: "template",
            component_type: 2,
            test_section_component: [
              {
                order: 1,
                id: 1,
                title: "FR info",
                pages: {
                  page_sections: [
                    {
                      id: 1,
                      page_section_content: { content: "## Temporary Filler Content" },
                      order: 1,
                      page_section_type: 1,
                      section_component_page: 1
                    }
                  ]
                }
              }
            ]
          }
        ]
      }
    },
    is_public: true,
    order: 1,
    section_type: 1,
    en_title: "5min",
    fr_title: "5min",
    en_description: null,
    fr_description: null,
    next_section_button_type: 0,
    scoring_type: 0,
    uses_notepad: false,
    uses_calculator: false,
    block_cheating: false,
    default_tab: 1,
    test_definition: 2,
    start_time: ""
  },
  testSectionLoaded: false,
  testSectionStartTime: "Thu May 28 2020 11:28:24 GMT-0400 (Eastern Daylight Time)",
  cheatingAttempts: 0,
  assignedTestId: null,
  testId: 2,
  isTestActive: false,
  isTestLocked: false,
  isTestPaused: false,
  previousStatus: null,
  history: {
    length: 18,
    action: "PUSH",
    location: { pathname: "/sample-tests/test-factory", search: "", hash: "", key: "rzdr6u" }
  },
  location: { pathname: "/sample-tests/test-factory", search: "", hash: "", key: "rzdr6u" },
  match: {
    path: "/sample-tests/test-factory",
    url: "/sample-tests/test-factory",
    isExact: true,
    params: {}
  },
  sampleTest: false
};

export const BUILDER_PROPS = {
  currentLanguage: "en",
  testSection: {},
  testSectionLoaded: false,
  testSectionStartTime: "",
  newTestDefinition: {
    test_definition: [
      {
        id: 2,
        test_code: "EGAT001",
        version: "2",
        en_name: "EGAT",
        fr_name: "FR EGAT",
        is_public: true,
        active: true,
        test_language: null
      }
    ],
    next_section_buttons: [
      {
        id: 1,
        content: "Next",
        title: "Next",
        button_text: "Next",
        confirm_proceed: true,
        test_section: 6,
        language: "en",
        next_section_button_type: 2
      },
      {
        id: 2,
        content: "FR Next",
        title: "FR Next",
        button_text: "FR Next",
        confirm_proceed: true,
        test_section: 6,
        language: "fr",
        next_section_button_type: 2
      },
      {
        id: 3,
        content: "Next",
        title: "Next",
        button_text: "Next",
        confirm_proceed: true,
        test_section: 7,
        language: "en",
        next_section_button_type: 2
      },
      {
        id: 4,
        content: "Next",
        title: "Next",
        button_text: "Next",
        confirm_proceed: true,
        test_section: 7,
        language: "fr",
        next_section_button_type: 2
      },
      {
        id: 5,
        content: "Next",
        title: "Next",
        button_text: "Next",
        confirm_proceed: true,
        test_section: 8,
        language: "en",
        next_section_button_type: 2
      },
      {
        id: 6,
        content: "Next",
        title: "Next",
        button_text: "Next",
        confirm_proceed: true,
        test_section: 8,
        language: "fr",
        next_section_button_type: 2
      },
      {
        id: 7,
        content: "Finish",
        title: "Finish",
        button_text: "Finish",
        confirm_proceed: true,
        test_section: 9,
        language: "en",
        next_section_button_type: 2
      },
      {
        id: 8,
        content: "Finish",
        title: "Finish",
        button_text: "Finish",
        confirm_proceed: true,
        test_section: 9,
        language: "fr",
        next_section_button_type: 2
      }
    ],
    test_sections: [
      {
        id: 6,
        order: 1,
        section_type: 1,
        default_time: 5,
        en_title: "5min",
        fr_title: "5min",
        en_description: null,
        fr_description: null,
        next_section_button_type: 2,
        scoring_type: 0,
        uses_notepad: false,
        uses_calculator: false,
        block_cheating: false,
        default_tab: 1,
        test_definition: 2
      },
      {
        id: 7,
        order: 2,
        section_type: 1,
        default_time: 20,
        en_title: "question list 1",
        fr_title: "question list 1",
        en_description: null,
        fr_description: null,
        next_section_button_type: 2,
        scoring_type: 1,
        uses_notepad: false,
        uses_calculator: false,
        block_cheating: false,
        default_tab: 1,
        test_definition: 2
      },
      {
        id: 8,
        order: 3,
        section_type: 1,
        default_time: 30,
        en_title: "question list 2",
        fr_title: "question list 2",
        en_description: null,
        fr_description: null,
        next_section_button_type: 2,
        scoring_type: 1,
        uses_notepad: false,
        uses_calculator: false,
        block_cheating: false,
        default_tab: 1,
        test_definition: 2
      },
      {
        id: 9,
        order: 4,
        section_type: 3,
        default_time: null,
        en_title: "Finish",
        fr_title: "Finish",
        en_description: null,
        fr_description: null,
        next_section_button_type: 2,
        scoring_type: 0,
        uses_notepad: false,
        uses_calculator: false,
        block_cheating: false,
        default_tab: null,
        test_definition: 2
      },
      {
        id: 10,
        order: 5,
        section_type: 4,
        default_time: null,
        en_title: "Quit",
        fr_title: "Quit",
        en_description: null,
        fr_description: null,
        next_section_button_type: 0,
        scoring_type: 0,
        uses_notepad: false,
        uses_calculator: false,
        block_cheating: false,
        default_tab: null,
        test_definition: 2
      }
    ],
    test_section_components: [
      {
        id: 1,
        order: 1,
        component_type: 2,
        en_title: "template",
        fr_title: "template",
        shuffle_all_questions: false,
        test_section: [6]
      },
      {
        id: 2,
        order: 1,
        component_type: 4,
        en_title: "questions",
        fr_title: "fr questions",
        shuffle_all_questions: false,
        test_section: [7]
      },
      {
        id: 3,
        order: 1,
        component_type: 4,
        en_title: "questions 2",
        fr_title: "fr questions 2",
        shuffle_all_questions: false,
        test_section: [8]
      },
      {
        id: 4,
        order: 4,
        component_type: 1,
        en_title: "template",
        fr_title: "template",
        shuffle_all_questions: false,
        test_section: [9]
      },
      {
        id: 5,
        order: 5,
        component_type: 1,
        en_title: "template",
        fr_title: "template",
        shuffle_all_questions: false,
        test_section: [10]
      }
    ],
    questions: [
      {
        id: 1,
        question_type: 1,
        pilot: false,
        order: 1,
        test_section_component: 2,
        question_block_type: 3,
        dependencies: [9]
      },
      {
        id: 2,
        question_type: 1,
        pilot: false,
        order: 1,
        test_section_component: 2,
        question_block_type: 4,
        dependencies: [10]
      },
      {
        id: 3,
        question_type: 1,
        pilot: false,
        order: 1,
        test_section_component: 2,
        question_block_type: 2,
        dependencies: []
      },
      {
        id: 4,
        question_type: 1,
        pilot: false,
        order: 1,
        test_section_component: 2,
        question_block_type: 2,
        dependencies: []
      },
      {
        id: 5,
        question_type: 1,
        pilot: false,
        order: 1,
        test_section_component: 2,
        question_block_type: 5,
        dependencies: []
      },
      {
        id: 6,
        question_type: 1,
        pilot: false,
        order: 1,
        test_section_component: 2,
        question_block_type: 5,
        dependencies: []
      },
      {
        id: 7,
        question_type: 1,
        pilot: false,
        order: 1,
        test_section_component: 2,
        question_block_type: 3,
        dependencies: []
      },
      {
        id: 8,
        question_type: 1,
        pilot: false,
        order: 1,
        test_section_component: 2,
        question_block_type: 3,
        dependencies: []
      },
      {
        id: 9,
        question_type: 1,
        pilot: false,
        order: 2,
        test_section_component: 2,
        question_block_type: 3,
        dependencies: [1]
      },
      {
        id: 10,
        question_type: 1,
        pilot: false,
        order: 2,
        test_section_component: 2,
        question_block_type: 4,
        dependencies: [2]
      },
      {
        id: 11,
        question_type: 1,
        pilot: false,
        order: 1,
        test_section_component: 3,
        question_block_type: null,
        dependencies: []
      }
    ],
    question_sections: [
      { id: 1, order: 1, question_section_type: 1, question: 1 },
      { id: 2, order: 1, question_section_type: 1, question: 2 },
      { id: 3, order: 1, question_section_type: 1, question: 3 },
      { id: 4, order: 1, question_section_type: 1, question: 4 },
      { id: 5, order: 1, question_section_type: 1, question: 5 },
      { id: 6, order: 1, question_section_type: 1, question: 6 },
      { id: 7, order: 1, question_section_type: 1, question: 7 },
      { id: 8, order: 1, question_section_type: 1, question: 8 },
      { id: 9, order: 1, question_section_type: 1, question: 9 },
      { id: 10, order: 1, question_section_type: 1, question: 10 },
      { id: 11, order: 1, question_section_type: 1, question: 11 }
    ],
    question_section_definitions: [
      {
        id: 1,
        content: "### A1\nTestlet question 1",
        question_section: 1,
        language: "en",
        question_section_type: 1
      },
      { id: 2, content: "FR Temp", question_section: 1, language: "fr", question_section_type: 1 },
      {
        id: 3,
        content: "### A2\nTestlet question 1",
        question_section: 2,
        language: "en",
        question_section_type: 1
      },
      { id: 4, content: "FR Temp", question_section: 2, language: "fr", question_section_type: 1 },
      {
        id: 5,
        content: "### B1\nTemp",
        question_section: 3,
        language: "en",
        question_section_type: 1
      },
      { id: 6, content: "FR Temp", question_section: 3, language: "fr", question_section_type: 1 },
      {
        id: 7,
        content: "### B1\nTemp",
        question_section: 4,
        language: "en",
        question_section_type: 1
      },
      { id: 8, content: "FR Temp", question_section: 4, language: "fr", question_section_type: 1 },
      {
        id: 9,
        content: "### B2\nTemp",
        question_section: 5,
        language: "en",
        question_section_type: 1
      },
      { id: 10, content: "FR Temp", question_section: 5, language: "fr", question_section_type: 1 },
      {
        id: 11,
        content: "### C\nTemp",
        question_section: 6,
        language: "en",
        question_section_type: 1
      },
      { id: 12, content: "FR Temp", question_section: 6, language: "fr", question_section_type: 1 },
      { id: 13, content: "FR Temp", question_section: 7, language: "fr", question_section_type: 1 },
      {
        id: 14,
        content: "### A1\nnot part of testlet",
        question_section: 7,
        language: "en",
        question_section_type: 1
      },
      { id: 15, content: "FR Temp", question_section: 8, language: "fr", question_section_type: 1 },
      {
        id: 16,
        content: "### A1\nnot part of testlet",
        question_section: 8,
        language: "en",
        question_section_type: 1
      },
      {
        id: 17,
        content: "### A1\nTestlet question 2",
        question_section: 9,
        language: "en",
        question_section_type: 1
      },
      { id: 18, content: "FR Temp", question_section: 9, language: "fr", question_section_type: 1 },
      {
        id: 19,
        content: "### A2\nTestlet Question 2",
        question_section: 10,
        language: "en",
        question_section_type: 1
      },
      {
        id: 20,
        content: "FR Temp",
        question_section: 10,
        language: "fr",
        question_section_type: 1
      },
      {
        id: 21,
        content: "question section 2 \nquestion 1",
        question_section: 11,
        language: "en",
        question_section_type: 1
      },
      { id: 22, content: "FR Temp", question_section: 11, language: "fr", question_section_type: 1 }
    ],
    emails: [],
    email_details: [],
    answers: [
      { id: 1, scoring_value: 0, question: 1 },
      { id: 2, scoring_value: 5, question: 1 },
      { id: 3, scoring_value: 0, question: 2 },
      { id: 4, scoring_value: 0, question: 2 },
      { id: 5, scoring_value: 0, question: 3 },
      { id: 6, scoring_value: 0, question: 3 },
      { id: 7, scoring_value: 0, question: 4 },
      { id: 8, scoring_value: 0, question: 4 },
      { id: 9, scoring_value: 0, question: 5 },
      { id: 10, scoring_value: 0, question: 5 },
      { id: 11, scoring_value: 0, question: 6 },
      { id: 12, scoring_value: 0, question: 6 },
      { id: 13, scoring_value: 0, question: 7 },
      { id: 14, scoring_value: 5, question: 7 },
      { id: 15, scoring_value: 5, question: 8 },
      { id: 16, scoring_value: 0, question: 8 },
      { id: 17, scoring_value: 5, question: 9 },
      { id: 18, scoring_value: 0, question: 9 },
      { id: 19, scoring_value: 5, question: 10 },
      { id: 20, scoring_value: 0, question: 10 },
      { id: 21, scoring_value: 55, question: 11 }
    ],
    answer_details: [
      { id: 1, content: "Temp", answer: 1, language: "fr" },
      { id: 2, content: "Temp", answer: 1, language: "en" },
      { id: 3, content: "Temp", answer: 2, language: "fr" },
      { id: 4, content: "5", answer: 2, language: "en" },
      { id: 5, content: "Temp", answer: 3, language: "fr" },
      { id: 6, content: "Temp", answer: 3, language: "en" },
      { id: 7, content: "Temp", answer: 4, language: "fr" },
      { id: 8, content: "Temp", answer: 4, language: "en" },
      { id: 9, content: "Temp", answer: 5, language: "fr" },
      { id: 10, content: "Temp", answer: 5, language: "en" },
      { id: 11, content: "Temp", answer: 6, language: "fr" },
      { id: 12, content: "Temp", answer: 6, language: "en" },
      { id: 13, content: "Temp", answer: 7, language: "fr" },
      { id: 14, content: "Temp", answer: 7, language: "en" },
      { id: 15, content: "Temp", answer: 8, language: "fr" },
      { id: 16, content: "Temp", answer: 8, language: "en" },
      { id: 17, content: "Temp", answer: 9, language: "fr" },
      { id: 18, content: "Temp", answer: 9, language: "en" },
      { id: 19, content: "Temp", answer: 10, language: "fr" },
      { id: 20, content: "Temp", answer: 10, language: "en" },
      { id: 21, content: "Temp", answer: 11, language: "fr" },
      { id: 22, content: "Temp", answer: 11, language: "en" },
      { id: 23, content: "Temp", answer: 12, language: "fr" },
      { id: 24, content: "Temp", answer: 12, language: "en" },
      { id: 25, content: "Temp", answer: 13, language: "fr" },
      { id: 26, content: "Temp", answer: 13, language: "en" },
      { id: 27, content: "Temp", answer: 14, language: "fr" },
      { id: 28, content: "5", answer: 14, language: "en" },
      { id: 29, content: "Temp", answer: 15, language: "fr" },
      { id: 30, content: "5", answer: 15, language: "en" },
      { id: 31, content: "Temp", answer: 16, language: "fr" },
      { id: 32, content: "Temp", answer: 16, language: "en" },
      { id: 33, content: "Temp", answer: 17, language: "fr" },
      { id: 34, content: "5", answer: 17, language: "en" },
      { id: 35, content: "Temp", answer: 18, language: "fr" },
      { id: 36, content: "Temp", answer: 18, language: "en" },
      { id: 37, content: "FR answer 1 worth 5", answer: 19, language: "fr" },
      { id: 38, content: "answer 1 worth 5", answer: 19, language: "en" },
      { id: 39, content: "Temp", answer: 20, language: "fr" },
      { id: 40, content: "Temp", answer: 20, language: "en" },
      { id: 41, content: "Temp", answer: 21, language: "fr" },
      { id: 42, content: "answer 1 worth 55", answer: 21, language: "en" }
    ],
    contact_details: [],
    address_book: [],
    multiple_choice_question_details: [
      { id: 1, question_difficulty_type: 1, question: 1 },
      { id: 2, question_difficulty_type: 1, question: 2 },
      { id: 3, question_difficulty_type: 1, question: 3 },
      { id: 4, question_difficulty_type: 1, question: 4 },
      { id: 5, question_difficulty_type: 1, question: 5 },
      { id: 6, question_difficulty_type: 1, question: 6 },
      { id: 7, question_difficulty_type: 1, question: 7 },
      { id: 8, question_difficulty_type: 1, question: 8 },
      { id: 9, question_difficulty_type: 1, question: 9 },
      { id: 10, question_difficulty_type: 1, question: 10 },
      { id: 11, question_difficulty_type: 1, question: 11 }
    ],
    section_component_pages: [
      { id: 1, order: 1, en_title: "info", fr_title: "FR info", test_section_component: 1 },
      { id: 2, order: 2, en_title: "1", fr_title: "1", test_section_component: 4 },
      { id: 3, order: 3, en_title: "quit", fr_title: "quit", test_section_component: 5 }
    ],
    page_sections: [
      { id: 1, order: 1, page_section_type: 1, section_component_page: 1 },
      { id: 2, order: 1, page_section_type: 1, section_component_page: 2 },
      { id: 3, order: 1, page_section_type: 1, section_component_page: 3 }
    ],
    page_section_definitions: [
      {
        id: 1,
        content: "## Temporary Filler Content",
        page_section: 1,
        language: "fr",
        page_section_type: 1
      },
      {
        id: 2,
        content: "## Temporary Filler Content",
        page_section: 1,
        language: "en",
        page_section_type: 1
      },
      { id: 3, content: "## You finished!", page_section: 2, language: "en", page_section_type: 1 },
      { id: 4, content: "## You finished!", page_section: 2, language: "fr", page_section_type: 1 },
      {
        id: 5,
        content: "## FR Quit  \nYou have quit the test.",
        page_section: 3,
        language: "fr",
        page_section_type: 1
      },
      {
        id: 6,
        content: "## Quit  \nYou have quit the test.",
        page_section: 3,
        language: "en",
        page_section_type: 1
      }
    ],
    question_block_types: [
      { id: 1, name: "C", test_definition: 2 },
      { id: 2, name: "B1", test_definition: 2 },
      { id: 3, name: "A1", test_definition: 2 },
      { id: 4, name: "A2", test_definition: 2 },
      { id: 5, name: "B2", test_definition: 2 }
    ],
    question_list_rules: [
      {
        id: 1,
        number_of_questions: 0,
        order: null,
        shuffle: true,
        test_section_component: 2,
        question_block_type: [3]
      },
      {
        id: 2,
        number_of_questions: 0,
        order: null,
        shuffle: false,
        test_section_component: 2,
        question_block_type: [2, 5]
      }
    ],
    viewTestSectionOrderNumber: 1
  },
  viewTestSectionOrderNumber: 1,
  history: {
    length: 23,
    action: "PUSH",
    location: { pathname: "/test-builder-test", search: "", hash: "", key: "wj2anm" }
  },
  location: { pathname: "/test-builder-test", search: "", hash: "", key: "wj2anm" },
  match: { path: "/test-builder-test", url: "/test-builder-test", isExact: true, params: {} },
  response: {
    body: {
      id: 16,
      redux: [],
      default_time: 5,
      localize: {
        en: {
          title: "5min",
          type: 1,
          next_section_button: {
            content: "Next",
            title: "Next",
            button_text: "Next",
            confirm_proceed: true,
            button_type: 2
          },
          components: [
            {
              order: 1,
              id: 11,
              title: "template",
              component_type: 2,
              test_section_component: [
                {
                  order: 1,
                  id: 7,
                  title: "info",
                  pages: {
                    page_sections: [
                      {
                        id: 7,
                        page_section_content: { content: "## Temporary Filler Content" },
                        order: 1,
                        page_section_type: 1,
                        section_component_page: 7
                      }
                    ]
                  }
                }
              ]
            }
          ]
        },
        fr: {
          title: "5min",
          type: 1,
          next_section_button: {
            content: "FR Next",
            title: "FR Next",
            button_text: "FR Next",
            confirm_proceed: true,
            button_type: 2
          },
          components: [
            {
              order: 1,
              id: 11,
              title: "template",
              component_type: 2,
              test_section_component: [
                {
                  order: 1,
                  id: 7,
                  title: "FR info",
                  pages: {
                    page_sections: [
                      {
                        id: 7,
                        page_section_content: { content: "## Temporary Filler Content" },
                        order: 1,
                        page_section_type: 1,
                        section_component_page: 7
                      }
                    ]
                  }
                }
              ]
            }
          ]
        }
      },
      is_public: true,
      order: 1,
      section_type: 1,
      en_title: "5min",
      fr_title: "5min",
      en_description: null,
      fr_description: null,
      next_section_button_type: 2,
      scoring_type: 0,
      uses_notepad: false,
      uses_calculator: false,
      block_cheating: false,
      default_tab: 1,
      test_definition: 4,
      start_time: ""
    },
    ok: true
  },
  untimedResponse: {
    body: {
      id: 16,
      redux: [],
      localize: {
        en: {
          title: "5min",
          type: 1,
          next_section_button: {
            content: "Next",
            title: "Next",
            button_text: "Next",
            confirm_proceed: true,
            button_type: 2
          },
          components: [
            {
              order: 1,
              id: 11,
              title: "template",
              component_type: 2,
              test_section_component: [
                {
                  order: 1,
                  id: 7,
                  title: "info",
                  pages: {
                    page_sections: [
                      {
                        id: 7,
                        page_section_content: { content: "## Temporary Filler Content" },
                        order: 1,
                        page_section_type: 1,
                        section_component_page: 7
                      }
                    ]
                  }
                }
              ]
            }
          ]
        },
        fr: {
          title: "5min",
          type: 1,
          next_section_button: {
            content: "FR Next",
            title: "FR Next",
            button_text: "FR Next",
            confirm_proceed: true,
            button_type: 2
          },
          components: [
            {
              order: 1,
              id: 11,
              title: "template",
              component_type: 2,
              test_section_component: [
                {
                  order: 1,
                  id: 7,
                  title: "FR info",
                  pages: {
                    page_sections: [
                      {
                        id: 7,
                        page_section_content: { content: "## Temporary Filler Content" },
                        order: 1,
                        page_section_type: 1,
                        section_component_page: 7
                      }
                    ]
                  }
                }
              ]
            }
          ]
        }
      },
      is_public: true,
      order: 1,
      section_type: 1,
      en_title: "5min",
      fr_title: "5min",
      en_description: null,
      fr_description: null,
      next_section_button_type: 2,
      scoring_type: 0,
      uses_notepad: false,
      uses_calculator: false,
      block_cheating: false,
      default_tab: 1,
      test_definition: 4,
      start_time: ""
    },
    ok: true
  }
};
