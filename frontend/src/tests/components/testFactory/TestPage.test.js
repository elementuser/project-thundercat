import React from "react";
import { shallow, mount } from "enzyme";
import { UnconnectedTestPage } from "../../../components/testFactory/TestPage";
import { TEST_PROPS_TIMED_SECTION, TEST_PROPS as testProps } from "./SampleTestPageData";
import LockScreen from "../../../components/commons/LockScreen";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import PauseScreen from "../../../components/commons/PauseScreen";
import { BrowserRouter } from "react-router-dom";

const mockStore = configureStore();

const initialState = {
  localize: {
    language: "en"
  },
  testSection: {
    testSection: testProps.testSection,
    testSectionLoaded: true
  },
  testStatus: { isTestActive: true },
  login: { authenticated: true },
  assignedTest: { assignedTestId: 1 },
  user: { username: "username@email.ca" },
  accommodations: { fontSize: "16px", fontFamily: "Nunito Sans" },
  notepad: { isNotepadHidden: false },
  calculator: { isCalculatorHidden: false }
};

const initialState_PausedTest = {
  localize: {
    language: "en"
  },
  testSection: {
    testSection: testProps.testSection,
    testSectionLoaded: true
  },
  testStatus: { isTestActive: true, isTestPaused: true },
  login: { authenticated: true },
  assignedTest: { assignedTestId: 1 },
  user: { username: "username@email.ca" },
  accommodations: { fontSize: "16px", fontFamily: "Nunito Sans" },
  notepad: { isNotepadHidden: false },
  calculator: { isCalculatorHidden: false }
};

// const modifyTestDefinitionField = jest.fn();
const functions = {
  setLanguage: jest.fn(),
  getTestSection: jest.fn().mockReturnValue(Promise.resolve({ ...testProps.testSection })),
  getTestSectionTimed: jest
    .fn()
    .mockReturnValue(Promise.resolve({ ...TEST_PROPS_TIMED_SECTION.testSection })),
  getCurrentTestSection: jest.fn().mockReturnValue(Promise.resolve({ ...testProps.testSections })),
  setTestHeight: jest.fn(),
  updateTestSection: jest.fn(),
  setContentLoaded: jest.fn().mockReturnValue(Promise.resolve({})),
  setContentUnLoaded: jest.fn(),
  setStartTime: jest.fn(),
  resetTestFactoryState: jest.fn(),
  resetAssignedTestState: jest.fn(),
  resetInboxState: jest.fn(),
  updateAssignedTestId: jest.fn(),
  resetNotepadState: jest.fn(),
  resetQuestionListState: jest.fn(),
  resetTopTabsState: jest.fn(),
  scorescoreUitTestSection: jest.fn(),
  scoreUitTest: jest.fn(),
  updateCheatingAttempts: jest.fn(),
  deactivateTest: jest.fn(),
  eraseOppositeLoadedLanguage: jest.fn(),
  quitTest: jest.fn(),
  getUpdatedTestStartTime: jest.fn().mockReturnValue(Promise.resolve({})),
  history: { push: jest.fn() },
  triggerFocusToMainNavLink: jest.fn(),
  getCurrentTestStatus: jest
    .fn()
    .mockReturnValue(Promise.resolve({ status: 13, is_invalid: false })),
  invalidateTest: jest.fn(),
  setCurrentTime: jest.fn(),
  getServerTime: jest.fn().mockReturnValue(Promise.resolve({})),
  validateTimeoutState: jest.fn().mockReturnValue(Promise.resolve({ status: 409 })),
  updateBackendAndDbStatusState: jest.fn()
};

describe("It calls correct functions", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  it("on mount, not timed", async () => {
    const testPropsLocal = { ...testProps, testSection: {} };
    const wrapper = shallow(
      <UnconnectedTestPage
        {...testPropsLocal}
        {...functions}
        languagesLoaded={[]}
        accommodations={{ fontSize: "12px" }}
      />
    );
    expect(functions.getTestSection).toHaveBeenCalledTimes(1);
    await functions.getTestSection();
    expect(functions.setTestHeight).toHaveBeenCalledTimes(1);
    expect(functions.updateTestSection).toHaveBeenCalledTimes(1);
    expect(functions.setContentLoaded).toHaveBeenCalledTimes(1);
  });
  // it("on mount, timed", async () => {
  //   const functionsLocal = { ...functions, getTestSection: functions.getTestSectionTimed };

  //   const wrapper = shallow(
  //     <UnconnectedTestPage
  //       {...TEST_PROPS_TIMED_SECTION}
  //       {...functionsLocal}
  //       default_time={100}
  //       assignedTestId={1}
  //       testSectionLoaded={true}
  //       languagesLoaded={["en"]}
  //     />
  //   );
  //   expect(functions.getUpdatedTestStartTime).toHaveBeenCalledTimes(1);
  // });
  it("get next test section", () => {
    const wrapper = shallow(
      <UnconnectedTestPage
        {...testProps}
        {...functions}
        languagesLoaded={[]}
        accommodations={{ fontSize: "12px" }}
      />,
      {
        disableLifecycleMethods: true
      }
    );
    wrapper.instance().handleNext();
    expect(functions.getCurrentTestStatus).toHaveBeenCalledTimes(0);
    expect(functions.getTestSection).toHaveBeenCalledTimes(1);
  });
  it("finish test", () => {
    const wrapper = shallow(
      <UnconnectedTestPage
        {...testProps}
        {...functions}
        languagesLoaded={[]}
        accommodations={{ fontSize: "12px" }}
      />,
      {
        disableLifecycleMethods: true
      }
    );
    wrapper.instance().handleFinishTest();
    expect(functions.triggerFocusToMainNavLink).toHaveBeenCalledTimes(1);
  });
  it("quit test", async () => {
    const wrapper = shallow(
      <UnconnectedTestPage
        {...TEST_PROPS_TIMED_SECTION}
        {...functions}
        languagesLoaded={[]}
        accommodations={{ fontSize: "12px" }}
      />,
      {
        disableLifecycleMethods: true
      }
    );
    wrapper.instance().handleQuitTest();
    expect(functions.getCurrentTestStatus).toHaveBeenCalledTimes(1);
    await functions.getCurrentTestStatus();
    expect(functions.quitTest).toHaveBeenCalledTimes(1);
  });
  it("timeout test", async () => {
    const wrapper = shallow(
      <UnconnectedTestPage
        {...TEST_PROPS_TIMED_SECTION}
        {...functions}
        languagesLoaded={[]}
        accommodations={{ fontSize: "12px" }}
      />,
      {
        disableLifecycleMethods: true
      }
    );
    wrapper.instance().timeout();
    await functions.validateTimeoutState();
    expect(functions.getCurrentTestStatus).toHaveBeenCalledTimes(1);
    await functions.getCurrentTestStatus();
    expect(functions.getTestSection).toHaveBeenCalledTimes(1);
  });
  // This test doesn't affect SLE
  // it("cheating popup", () => {
  //   const wrapper = mount(
  //     <Provider store={mockStore(initialState)}>
  //       <UnconnectedTestPage
  //         {...testProps}
  //         {...functions}
  //         isTestLocked={true}
  //         languagesLoaded={[]}
  //       />
  //     </Provider>
  //   );
  //   wrapper
  //     .find(UnconnectedTestPage)
  //     .instance()
  //     .openCheatingPopup();
  //   // expect(functions.updateCheatingAttempts).toHaveBeenCalledTimes(1);
  //   expect(wrapper.find(PopupBox).exists()).toEqual(true);
  // });
  // This test doesn't affect SLE
  // it("visibility change", () => {
  //   const wrapper = mount(
  //     <Provider store={mockStore(initialState)}>
  //       <UnconnectedTestPage
  //         {...testProps}
  //         {...functions}
  //         // isTestLocked={true}
  //         languagesLoaded={[]}
  //         testSectionLoaded={true}
  //       />
  //     </Provider>
  //   );
  //   wrapper
  //     .find(UnconnectedTestPage)
  //     .instance()
  //     .visibilityChange();
  //   // expect(functions.updateCheatingAttempts).toHaveBeenCalledTimes(1);
  //   expect(wrapper.find(PopupBox).exists()).toEqual(true);
  // });
});

describe("It displays correct", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  it("lock screen", () => {
    const wrapper = mount(
      <BrowserRouter>
        <Provider store={mockStore(initialState)}>
          <UnconnectedTestPage
            {...testProps}
            {...functions}
            isTestLocked={true}
            languagesLoaded={[]}
            testSectionLoaded={true}
            accommodations={{ fontSize: "12px" }}
          />
        </Provider>
      </BrowserRouter>
    );
    expect(wrapper.find(LockScreen).exists()).toEqual(true);
  });
  it("pause screen", () => {
    const wrapper = shallow(
      <UnconnectedTestPage
        {...testProps}
        {...functions}
        isTestPaused={true}
        languagesLoaded={[]}
        testSectionLoaded={true}
        accommodations={{ fontSize: "12px" }}
      />
    );
    expect(wrapper.find(PauseScreen).exists()).toEqual(true);
  });
  // not working for some reason, may come back to this
  // it("nav bar", () => {
  //   const wrapper = mount(
  //     <Provider store={mockStore(initialState)}>
  //       <UnconnectedTestPage {...testProps} {...functions} />
  //     </Provider>
  //   );
  //   expect(wrapper.find(TestNavBar).exists()).toEqual(true);
  // });
});
