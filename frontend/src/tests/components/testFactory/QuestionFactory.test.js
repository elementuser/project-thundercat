import React from "react";
import { shallow, mount } from "enzyme";
import { UnconnectedQuestionFactory as QuestionFactory } from "../../../components/testFactory/QuestionFactory";
import { QuestionSectionType } from "../../../components/testFactory/Constants";

const question = [
  {
    question_section_type: QuestionSectionType.MARKDOWN,
    question_section_content: { content: "Markdown content" }
  },
  { question_section_type: 10, question_section_content: { content: "" } }
];

describe("It renders a", () => {
  it("markdown section", () => {
    const wrapper = shallow(<QuestionFactory questionSections={question} index={0} />);
    expect(wrapper.find("#unit-test-question-content").exists()).toEqual(true);
    expect(wrapper.find("#unit-test-question-content-markdown").exists()).toEqual(true);
  });
  it("undefined type section", () => {
    const wrapper = shallow(<QuestionFactory questionSections={question} index={0} />);
    expect(wrapper.find("#unit-test-question-content").exists()).toEqual(true);
    expect(wrapper.find("#unit-test-question-content-undefined-section").exists()).toEqual(true);
  });
});

describe("It renders correct content in", () => {
  it("question content", () => {
    const wrapper = mount(<QuestionFactory questionSections={question} index={0} />);
    expect(wrapper.find("#unit-test-question-content-markdown").text()).toEqual(
      question[0].question_section_content.content
    );
  });
});
