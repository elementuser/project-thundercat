import { TestSectionType, NextSectionButtonType } from "../../../components/testFactory/Constants";

export const SINGLE_PAGE = {
  testSection: {
    title: "Overview TS",
    type: TestSectionType.SINGLE_COMPONENT,
    next_section_button: { button_type: 1, content: "Proceed to sample e-MIB test" },
    components: [{}]
  },
  timeLimit: null,
  testSectionStartTime: "",
  testSectionType: 2,
  defaultTab: null,
  usesNotepad: false,
  usesCalculator: false,
  currentLanguage: "en",
  startTime: ""
};

export const TOP_TABS = {
  testSection: {
    title: "Overview TS",
    type: TestSectionType.TOP_TABS,
    next_section_button: { button_type: 1, content: "Proceed to sample e-MIB test" },
    components: [{}]
  },
  timeLimit: null,
  testSectionStartTime: "",
  testSectionType: 2,
  defaultTab: null,
  usesNotepad: false,
  usesCalculator: false,
  currentLanguage: "en",
  startTime: ""
};

export const FINISH = {
  testSection: {
    title: "Overview TS",
    type: TestSectionType.FINISH,
    next_section_button: { button_type: 1, content: "Proceed to sample e-MIB test" },
    components: [{}]
  },
  timeLimit: null,
  testSectionStartTime: "",
  testSectionType: 2,
  defaultTab: null,
  usesNotepad: false,
  usesCalculator: false,
  currentLanguage: "en",
  startTime: ""
};

export const QUIT = {
  testSection: {
    title: "Overview TS",
    type: TestSectionType.QUIT,
    next_section_button: { button_type: 1, content: "Proceed to sample e-MIB test" },
    components: [{}]
  },
  timeLimit: null,
  testSectionStartTime: "",
  testSectionType: 2,
  defaultTab: null,
  usesNotepad: false,
  usesCalculator: false,
  currentLanguage: "en",
  startTime: ""
};

export const UNSUPPORTED = {
  testSection: {
    title: "Overview TS",
    type: 0,
    next_section_button: { button_type: 1, content: "Proceed to sample e-MIB test" },
    components: [{}]
  },
  timeLimit: null,
  testSectionStartTime: "",
  testSectionType: 2,
  defaultTab: null,
  usesNotepad: false,
  usesCalculator: false,
  currentLanguage: "en",
  startTime: ""
};

export const PROCEED_BUTTON = {
  testSection: {
    title: "Overview TS",
    type: TestSectionType.SINGLE_COMPONENT,
    next_section_button: {
      button_type: NextSectionButtonType.PROCEED,
      content: "Proceed to sample e-MIB test"
    },
    components: [{}]
  },
  timeLimit: null,
  testSectionStartTime: "",
  testSectionType: 2,
  defaultTab: null,
  usesNotepad: false,
  usesCalculator: false,
  currentLanguage: "en",
  startTime: ""
};

export const POPUP_BUTTON = {
  testSection: {
    title: "Overview TS",
    type: TestSectionType.SINGLE_COMPONENT,
    next_section_button: {
      button_type: NextSectionButtonType.POPUP,
      content: "Proceed to sample e-MIB test"
    },
    components: [{}]
  },
  timeLimit: null,
  testSectionStartTime: "",
  testSectionType: 2,
  defaultTab: null,
  usesNotepad: false,
  usesCalculator: false,
  currentLanguage: "en",
  startTime: ""
};
