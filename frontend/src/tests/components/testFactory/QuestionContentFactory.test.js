import React from "react";
import { shallow, mount } from "enzyme";
import { UnconnectedQuestionContentFactory as QuestionContentFactory } from "../../../components/testFactory/QuestionContentFactory";
import LOCALIZE from "../../../text_resources";
import { QuestionType } from "../../../components/testFactory/Constants";

const changeQuestion = jest.fn();
const emptyFunction = () => {};
const questions = [];
const question = { answers: [], email: [] };

describe("It renders a", () => {
  it("multiple choice question", () => {
    const wrapper = shallow(
      <QuestionContentFactory
        question={question}
        index={0}
        questionIndex={0}
        questions={questions}
        changeQuestion={emptyFunction}
        handleAnswerClick={emptyFunction}
        markForReview={emptyFunction}
        type={QuestionType.MULTIPLE_CHOICE}
        addressBook={[]}
        isAnswered={false}
        isRead={false}
        isSelected={true}
      />
    );
    expect(wrapper.find("#unit-test-question-content-factory-multiple-choice").exists()).toEqual(
      true
    );
  });
  it("email question", () => {
    const wrapper = shallow(
      <QuestionContentFactory
        question={question}
        index={0}
        questionIndex={0}
        questions={questions}
        changeQuestion={emptyFunction}
        handleAnswerClick={emptyFunction}
        markForReview={emptyFunction}
        type={QuestionType.EMAIL}
        addressBook={[]}
        isAnswered={false}
        isRead={false}
        isSelected={true}
        emailResponses={0}
        taskResponses={0}
      />
    );
    expect(wrapper.find("#unit-test-question-content-factory-email").exists()).toEqual(true);
  });
});
