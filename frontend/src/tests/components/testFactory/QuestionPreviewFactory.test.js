import React from "react";
import { shallow } from "enzyme";
import { UnconnectedQuestionPreviewFactory as QuestionPreviewFactory } from "../../../components/testFactory/QuestionPreviewFactory";
import { QuestionType } from "../../../components/testFactory/Constants";

describe("Renders correct question preview type", () => {
  it("unsupported", () => {
    const wrapper = shallow(
      <QuestionPreviewFactory
        index={0}
        question={{ email: {} }}
        questionId={1}
        type={0}
        isSelected={false}
      />
    );
    expect(wrapper.find("#unit-test-unsupported-question-preview-type").exists()).toEqual(true);
  });
  it("email", () => {
    const question = { email: { id: 0 } };
    const wrapper = shallow(
      <QuestionPreviewFactory
        index={0}
        question={question}
        questionId={1}
        type={QuestionType.EMAIL}
        isSelected={false}
        emailResponses={[]}
        taskResponses={[]}
        isRead={false}
      />
    );
    expect(wrapper.find("#unit-test-email-question-preview-type").exists()).toEqual(true);
  });
  it("multiple choice", () => {
    const wrapper = shallow(
      <QuestionPreviewFactory
        index={0}
        question={{}}
        questionId={1}
        type={QuestionType.MULTIPLE_CHOICE}
        isSelected={false}
        isAnswered={false}
        isMarkedForReview={false}
        isRead={false}
        isSelected={false}
      />
    );
    expect(wrapper.find("#unit-test-multiple-choice-question-preview-type").exists()).toEqual(true);
  });
});
