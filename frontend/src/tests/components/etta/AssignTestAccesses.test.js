import React from "react";
import { shallow } from "enzyme";
import { unconnectedAssignTestAccesses as AssignTestAccesses } from "../../../components/etta/test_accesses/AssignTestAccesses";
import LOCALIZE from "../../../text_resources";

const mockData = {
  testOrderNumberSelectedOption: "2020-123456",
  staffingProcessNumber: "A123B654",
  departmentMinistryCode: "1040",
  isOrg: "is org data",
  isRef: "is ref data",
  billingContact: "John Smith",
  billingContactInfo: "john.smith@canada.ca",
  usersSelectedOptions: "John Doe"
};

describe("renders component content", () => {
  const wrapper = shallow(<AssignTestAccesses accommodations={{ fontSize: "16px" }} />, {
    disableLifecycleMethods: true
  });

  // financialDataFieldsVisible is set to false (default state)
  it("renders test order number field - financialDataFieldsVisible: false", () => {
    const label = (
      <label id="test-order-number-label">
        {LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.testOrderNumberLabel}
      </label>
    );
    expect(wrapper.containsMatchingElement(label)).toEqual(true);
    const input = <input id="test-order-number" value={mockData.testOrderNumber}></input>;
    expect(wrapper.containsMatchingElement(input)).toEqual(true);
  });

  it("does not render staffing process number field - financialDataFieldsVisible: false", () => {
    const label = (
      <label id="staffing-process-number-label">
        {LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.staffingProcessNumber}
      </label>
    );
    expect(wrapper.contains(label)).toEqual(false);
  });

  it("does not render department/ministry field - financialDataFieldsVisible: false", () => {
    const label = (
      <label id="department-ministry-code-label">
        {LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.departmentMinistry}
      </label>
    );
    expect(wrapper.contains(label)).toEqual(false);
  });

  it("does not render is org field - financialDataFieldsVisible: false", () => {
    const label = (
      <label id="is-org-label">
        {LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.isOrg}
      </label>
    );
    expect(wrapper.contains(label)).toEqual(false);
  });

  it("does not render is ref field - financialDataFieldsVisible: false", () => {
    const label = (
      <label id="is-ref-label">
        {LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.isRef}
      </label>
    );
    expect(wrapper.contains(label)).toEqual(false);
  });

  it("does not render billing contact field - financialDataFieldsVisible: false", () => {
    const label = (
      <label id="billing-contact-label">
        {LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.billingContact}
      </label>
    );
    expect(wrapper.contains(label)).toEqual(false);
  });

  it("does not render billing contact info field - financialDataFieldsVisible: false", () => {
    const label = (
      <label id="billing-contact-info-label">
        {LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.billingContactInfo}
      </label>
    );
    expect(wrapper.contains(label)).toEqual(false);
  });

  wrapper.setState({
    testOrderNumberSelectedOption: mockData.testOrderNumberSelectedOption,
    staffingProcessNumber: mockData.staffingProcessNumber,
    departmentMinistryCode: mockData.departmentMinistryCode,
    isOrg: mockData.isOrg,
    isRef: mockData.isRef,
    billingContact: mockData.billingContact,
    billingContactInfo: mockData.billingContactInfo,
    financialDataFieldsVisible: true
  });

  // financialDataFieldsVisible is set to true
  it("renders test order number field - financialDataFieldsVisible: true", () => {
    const label = (
      <label id="test-order-number-label">
        {LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.testOrderNumberLabel}
      </label>
    );
    expect(wrapper.containsMatchingElement(label)).toEqual(true);
    const input = <input id="test-order-number" value={mockData.testOrderNumber}></input>;
    expect(wrapper.containsMatchingElement(input)).toEqual(true);
  });

  it("renders staffing process number field - financialDataFieldsVisible: true", () => {
    const label = (
      <label id="staffing-process-number-label">
        {LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.staffingProcessNumber}
      </label>
    );
    expect(wrapper.containsMatchingElement(label)).toEqual(true);
    const input = (
      <input id="staffing-process-number" value={mockData.staffingProcessNumber}></input>
    );
    expect(wrapper.containsMatchingElement(input)).toEqual(true);
  });

  it("renders department/ministry field - financialDataFieldsVisible: true", () => {
    const label = (
      <label id="department-ministry-code-label">
        {LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.departmentMinistry}
      </label>
    );
    expect(wrapper.containsMatchingElement(label)).toEqual(true);
    const input = (
      <input id="department-ministry-code" value={mockData.departmentMinistryCode}></input>
    );
    expect(wrapper.containsMatchingElement(input)).toEqual(true);
  });

  it("renders is org field - financialDataFieldsVisible: true", () => {
    const label = (
      <label id="is-org-label">
        {LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.isOrg}
      </label>
    );
    expect(wrapper.containsMatchingElement(label)).toEqual(true);
    const input = <input id="is-org" value={mockData.isOrg}></input>;
    expect(wrapper.containsMatchingElement(input)).toEqual(true);
  });

  it("renders is ref field - financialDataFieldsVisible: true", () => {
    const label = (
      <label id="is-ref-label">
        {LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.isRef}
      </label>
    );
    expect(wrapper.containsMatchingElement(label)).toEqual(true);
    const input = <input id="is-ref" value={mockData.isRef}></input>;
    expect(wrapper.containsMatchingElement(input)).toEqual(true);
  });

  it("renders billing contact field - financialDataFieldsVisible: true", () => {
    const label = (
      <label id="billing-contact-label">
        {LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.billingContact}
      </label>
    );
    expect(wrapper.containsMatchingElement(label)).toEqual(true);
    const input = <input id="billing-contact" value={mockData.billingContact}></input>;
    expect(wrapper.containsMatchingElement(input)).toEqual(true);
  });

  it("renders billing contact info field - financialDataFieldsVisible: true", () => {
    const label = (
      <label id="billing-contact-info-label">
        {LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.billingContactInfo}
      </label>
    );
    expect(wrapper.containsMatchingElement(label)).toEqual(true);
    const input = <input id="billing-contact-info" value={mockData.billingContactInfo}></input>;
    expect(wrapper.containsMatchingElement(input)).toEqual(true);
  });
});
