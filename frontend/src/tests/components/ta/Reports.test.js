import React from "react";
import { shallow } from "enzyme";
import { unconnectedReports as Reports } from "../../../components/ta/Reports";
import LOCALIZE from "../../../text_resources";

describe("renders component content", () => {
  const wrapper = shallow(<Reports />);

  it("renders title", () => {
    const title = <h2>{LOCALIZE.testAdministration.sideNavItems.reports}</h2>;
    expect(wrapper.containsMatchingElement(title)).toEqual(true);
  });

  it("renders description", () => {
    const description = <p>{LOCALIZE.testAdministration.reports.description}</p>;
    expect(wrapper.containsMatchingElement(description)).toEqual(true);
  });
});
