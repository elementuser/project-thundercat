import React from "react";
import { mount } from "enzyme";
import { unconnectedInvitations as Invitations } from "../../../components/ta/uit/Invitations";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import LOCALIZE from "../../../text_resources";
import DropdownSelect from "../../../components/commons/DropdownSelect";
import DatePicker from "../../../components/commons/DatePicker";

const mockStore = configureStore();

const initialState = {
  localize: {
    language: "en"
  },
  accommodations: { fontSize: "16px", fontFamily: "Nunito Sans" },
  datePicker: {
    completeDatePicked: "2026-1-1"
  },
  uit: {
    uitTestOrderNumber: "",
    uitTests: []
  }
};

describe("renders component content", () => {
  const wrapper = mount(
    <Provider store={mockStore(initialState)}>
      <Invitations
        accommodations={{ fontSize: "16px" }}
        resetUitInvitationsRelatedStates={() => {}}
        uitTestOrderNumberOptions={[]}
        uitTestsOptions={[]}
        uitTests={[]}
      />
    </Provider>
  );

  it("renders test order number label and dropdown", () => {
    const label = <label>{LOCALIZE.testAdministration.uit.tabs.invitations.testOrderNumber}</label>;
    const dropdown = (
      <DropdownSelect
        idPrefix="uit-test-order-number-dropdown"
        ariaLabelledBy="uit-test-order-number-label uit-test-order-number-error-message uit-test-order-number-current-value-accessibility"
        ariaRequired={true}
        hasPlaceholder={true}
      ></DropdownSelect>
    );
    expect(wrapper.containsMatchingElement(label)).toEqual(true);
    expect(wrapper.containsMatchingElement(dropdown)).toEqual(true);
  });

  it("renders tests label and dropdown", () => {
    const label = <label>{LOCALIZE.testAdministration.uit.tabs.invitations.test}</label>;
    const dropdown = (
      <DropdownSelect
        idPrefix="uit-tests-dropdown"
        ariaLabelledBy="uit-tests-label uit-tests-error-message uit-tests-current-value-accessibility"
        ariaRequired={true}
        hasPlaceholder={true}
      ></DropdownSelect>
    );
    expect(wrapper.containsMatchingElement(label)).toEqual(true);
    expect(wrapper.containsMatchingElement(dropdown)).toEqual(true);
  });

  // it("renders email template label and dropdown", () => {
  //   const label = <label>{LOCALIZE.testAdministration.uit.tabs.invitations.emailTemplate}</label>;
  //   const dropdown = (
  //     <DropdownSelect
  //       idPrefix="uit-email-template-dropdown"
  //       ariaLabelledBy="uit-email-template-dropdown uit-email-template-current-value-accessibility"
  //       hasPlaceholder={true}
  //     ></DropdownSelect>
  //   );
  //   expect(wrapper.containsMatchingElement(label)).toEqual(true);
  //   expect(wrapper.containsMatchingElement(dropdown)).toEqual(true);
  // });

  it("renders validity end date label and date picker", () => {
    const label = <label>{LOCALIZE.testAdministration.uit.tabs.invitations.validityEndDate}</label>;
    const datePicker = (
      <DatePicker
        dateLabelId={"uit-validity-end-date-label"}
        futureDateValidation={true}
        displayValidIcon={false}
        menuPlacement={"top"}
      />
    );
    expect(wrapper.containsMatchingElement(label)).toEqual(true);
    expect(wrapper.containsMatchingElement(datePicker)).toEqual(true);
  });

  it("renders send invites button", () => {
    expect(wrapper.find("#uit-send-invitations-button").exists()).toEqual(true);
  });
});
