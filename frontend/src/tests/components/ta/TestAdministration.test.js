import React from "react";
import { shallow } from "enzyme";
import { UnconnectedTestAdministration } from "../../../components/ta/TestAdministration";
import LOCALIZE from "../../../text_resources";

describe("renders component content", () => {
  const wrapper = shallow(
    <UnconnectedTestAdministration
      firstName="John"
      lastName="Smith"
      username="john.smith@canada.ca"
    />,
    {
      disableLifecycleMethods: true
    }
  );

  it("renders welcome message", () => {
    wrapper.setState({ checkedIn: false });
    const welcomeMessage = (
      <h1>{LOCALIZE.formatString(LOCALIZE.testAdministration.title, "John", "Smith")}</h1>
    );
    expect(wrapper.containsMatchingElement(welcomeMessage)).toEqual(true);
  });

  it("renders side nav container label", () => {
    wrapper.setState({ checkedIn: false });
    const label = (
      <label>
        {LOCALIZE.testAdministration.containerLabel}
        <span></span>
      </label>
    );
    expect(wrapper.containsMatchingElement(label)).toEqual(true);
  });
});
