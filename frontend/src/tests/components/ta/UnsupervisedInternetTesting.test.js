import React from "react";
import { shallow } from "enzyme";
import { unconnectedUnsupervisedInternetTesting as UnsupervisedInternetTesting } from "../../../components/ta/uit/UnsupervisedInternetTesting";
import LOCALIZE from "../../../text_resources";

describe("renders component content", () => {
  const wrapper = shallow(<UnsupervisedInternetTesting />, {
    disableLifecycleMethods: true
  });

  it("renders title", () => {
    const title = <h2>{LOCALIZE.testAdministration.uit.title}</h2>;
    expect(wrapper.containsMatchingElement(title)).toEqual(true);
  });

  it("renders description", () => {
    const description = <p>{LOCALIZE.testAdministration.uit.description}</p>;
    expect(wrapper.containsMatchingElement(description)).toEqual(true);
  });
});
