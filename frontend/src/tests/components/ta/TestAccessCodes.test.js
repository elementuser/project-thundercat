import React from "react";
import { shallow } from "enzyme";
import { unconnectedTestAccessCodes as TestAccessCodes } from "../../../components/ta/TestAccessCodes";
import LOCALIZE from "../../../text_resources";
import { LANGUAGES } from "../../../modules/LocalizeRedux";

const mockData = [
  {
    id: 1,
    en_test_name: "Pizza Test",
    fr_test_name: "FR Pizza Test",
    staffing_process_number: "2020-PSC-CFP-10-4045-01",
    department_ministry_code: "1040",
    is_org: "Org1",
    is_ref: "Ref1",
    billing_contact: "John Smith",
    billing_contact_info: "john.smith@canada.ca",
    test_access_code: "8OTiwoSqWa",
    test_order_number: "2020/01-1265",
    test: "emibPizzaTest",
    test_session_language: 1,
    ta_username: "ta1@email.ca"
  },
  {
    id: 1,
    en_test_name: "Pizza Test",
    fr_test_name: "FR Pizza Test",
    staffing_process_number: "2020-PSC-CFP-25-8745-09",
    department_ministry_code: "2575",
    is_org: "Org2",
    is_ref: "Ref2",
    billing_contact: "John dOE",
    billing_contact_info: "john.DOE@canada.ca",
    test_access_code: "4FyCQU1sB4",
    test_order_number: "2020/15-8524",
    test: "emibPizzaTest",
    test_session_language: 2,
    ta_username: "ta2@email.ca"
  }
];

describe("renders component content", () => {
  const wrapper = shallow(
    <TestAccessCodes
      activeTestAccessCodes={mockData}
      getActiveTestAccessCodes={() => {}}
      testOrderNumberOptions={[]}
      populateTestToAdministerOptions={() => {}}
      testToAdministerOptions={[]}
      populateTestSessionLanguageOptions={() => {}}
      testSessionLanguageOptions={[]}
      accommodations={{ spacing: false, fontSize: "16px" }}
    />
  );
  wrapper.setState({ activeTestAccessCodes: mockData });
  wrapper.setProps({ currentLanguage: LANGUAGES.english });
  it("renders title", () => {
    const title = <h2>{LOCALIZE.testAdministration.sideNavItems.testAccessCodes}</h2>;
    expect(wrapper.containsMatchingElement(title)).toEqual(true);
  });

  it("renders description", () => {
    const description = <p>{LOCALIZE.testAdministration.testAccessCodes.description}</p>;
    expect(wrapper.containsMatchingElement(description)).toEqual(true);
  });
});
