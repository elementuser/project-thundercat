import React from "react";
import { shallow } from "enzyme";
import { UnconnectedDashboard } from "../Dashboard";
import LOCALIZE from "../text_resources";
import { styles } from "../components/etta/permissions/ActivePermissions";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";

describe("renders title and loading symbol", () => {
  const wrapper = shallow(
    <UnconnectedDashboard firstName="" lastName="" username="" isUserLoading={true} />,
    {
      disableLifecycleMethods: true
    }
  );
  it("renders title", () => {
    const title = <h1>{LOCALIZE.formatString(LOCALIZE.dashboard.title, "", "")}</h1>;
    expect(wrapper.containsMatchingElement(title)).toEqual(true);
  });
});

describe("renders title and description", () => {
  const wrapper = shallow(
    <UnconnectedDashboard firstName="" lastName="" username="" isUserLoading={true} />,
    {
      disableLifecycleMethods: true
    }
  );
  wrapper.setState({
    isLoaded: true,
    isMounted: true
  });
  wrapper.setProps({ isUserLoading: false });
  wrapper.update();
  it("renders title", () => {
    const title = <h1>{LOCALIZE.formatString(LOCALIZE.dashboard.title, "", "")}</h1>;
    expect(wrapper.containsMatchingElement(title)).toEqual(true);
  });

  it("renders description", () => {
    const description = <p>{LOCALIZE.dashboard.description}</p>;
    expect(wrapper.contains(description)).toEqual(true);
  });
});
