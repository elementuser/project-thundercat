import React, { Component } from "react";
import PropTypes from "prop-types";
import LOCALIZE from "./text_resources";
import { Helmet } from "react-helmet";
import { Button, Row, Col } from "react-bootstrap";
import psc_logo_fr from "./images/psc_logo_fr.png";
import canada_logo from "./images/canada_logo.png";
import { setLanguage, LANGUAGES } from "./modules/LocalizeRedux";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import clearTestSessionStorage from "./helpers/localStorage";
import Splash from "./images/Splash.jpg";
import { triggerFocusToMainNavLink } from "./modules/AccommodationsRedux";

// Strings won't get translated because they will always appear in both
// languages because the user has not selected a language yet.
const strings = {
  cat: "Candidate Assessment Tool",
  oec: "Outil d'évaluation des candidats",
  en: "English",
  fr: "Français"
};

const styles = {
  logo: {
    padding: "10px 20px"
  },
  selectorContainer: {
    maxWidth: "600px",
    height: "fit-content",
    backgroundColor: "#FFFFFF",
    padding: 20,
    border: "1px solid #00565e",
    borderRadius: 5,
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    margin: "auto"
  },
  imageContainer: {
    margin: "10px 0px"
  },
  buttonsContainer: {
    margin: "10px 0px",
    borderTop: "1px solid #CECECE",
    paddingTop: "15px"
  },
  langButton: {
    margin: 0,
    minWidth: 90
  },
  splash: {
    width: "calc(100vw)",
    height: "calc(100vh)",
    backgroundImage: `url(${Splash})`,
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    backgroundPosition: "center"
  },
  noPaddingCol: {
    padding: 0
  },
  h2: {
    padding: 0,
    marginTop: 15,
    marginBottom: 15
  }
};

class SelectLanguage extends Component {
  static propTypes = {
    // this function initializes language so that we don't
    // always redirect to the choose english or french page.
    initializeLanguage: PropTypes.func.isRequired,
    // Props from Redux
    setLanguage: PropTypes.func.isRequired
  };

  setLanguageToEnglish = () => {
    this.props.initializeLanguage();
    this.props.setLanguage(LANGUAGES.english);
    // trigger focus to Home link
    this.props.triggerFocusToMainNavLink();
  };

  setLanguageToFrench = () => {
    this.props.initializeLanguage();
    this.props.setLanguage(LANGUAGES.french);
    // trigger focus to Home link
    this.props.triggerFocusToMainNavLink();
  };

  componentDidMount = () => {
    clearTestSessionStorage();
  };

  render() {
    return (
      <div className="notranslate">
        <div className="app">
          <Helmet>
            <title className="notranslate">{LOCALIZE.titles.CAT}</title>
          </Helmet>
          <div style={styles.splash}>
            <div style={styles.selectorContainer}>
              <Row className="justify-content-between" style={styles.imageContainer}>
                <Col style={styles.noPaddingCol}>
                  <img
                    alt={LOCALIZE.mainTabs.psc}
                    src={psc_logo_fr}
                    width="370px"
                    height="44.67px"
                    className="align-top"
                    style={styles.logo}
                  />
                </Col>
                <Col style={styles.noPaddingCol}>
                  <img
                    alt={LOCALIZE.mainTabs.canada}
                    src={canada_logo}
                    width="120px"
                    height="39.17px"
                    className="align-top"
                    style={styles.logo}
                  />
                </Col>
              </Row>
              <Row className="align-items-center">
                <Col className="align-self-center">
                  <h2 style={styles.h2} lang={LANGUAGES.french}>
                    {strings.oec}
                  </h2>
                </Col>
              </Row>
              <Row className="align-items-center">
                <Col className="align-self-center">
                  <h2 style={styles.h2}>{strings.cat}</h2>
                </Col>
              </Row>
              <Row
                className="align-items-center justify-content-center"
                style={styles.buttonsContainer}
              >
                <Col xs={6} className="text-right">
                  <Button
                    lang={LANGUAGES.french}
                    onClick={this.setLanguageToFrench}
                    style={styles.langButton}
                  >
                    {strings.fr}
                  </Button>
                </Col>
                <Col xs={6} className="text-left">
                  <Button onClick={this.setLanguageToEnglish} style={styles.langButton}>
                    {strings.en}
                  </Button>
                </Col>
              </Row>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setLanguage,
      triggerFocusToMainNavLink
    },
    dispatch
  );

export default connect(null, mapDispatchToProps)(SelectLanguage);
