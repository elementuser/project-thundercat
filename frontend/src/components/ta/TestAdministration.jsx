import React, { Component } from "react";
import PropTypes from "prop-types";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { isTokenStillValid, getUserInformation } from "../../modules/LoginRedux";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import ContentContainer from "../commons/ContentContainer";
import "../../css/test-administration.css";
import { styles as SystemAdministrationStyles } from "../etta/SystemAdministration";
import SideNavigation from "../eMIB/SideNavigation";
import TestAccessCodes from "./TestAccessCodes";
import ActiveCandidates from "./ActiveCandidates";
import UnsupervisedInternetTesting from "./uit/UnsupervisedInternetTesting";
import Reports from "./Reports";
import {
  getActiveTestAccessCodesRedux,
  getTestAdministratorAssignedCandidates,
  setTAUserSideNavState
} from "../../modules/TestAdministrationRedux";
import { getTestPermissions, getOrderlessTestOptions } from "../../modules/PermissionsRedux";
import { COMMON_STYLE } from "../commons/GenericTable";
import { getTimeInHoursMinutes } from "../../helpers/timeConversion";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faClock,
  faThumbsUp,
  faLock,
  faLockOpen,
  faTrashAlt,
  faPauseCircle
} from "@fortawesome/free-solid-svg-icons";
import {
  faThumbsUp as regularFaThumbsUp,
  faClock as regularFaClock,
  faPauseCircle as regularFaPauseCircle
} from "@fortawesome/free-regular-svg-icons";
import getFormattedTestStatus from "./TestStatus";
import TEST_STATUS from "./Constants";
import { PATH } from "../commons/Constants";
import StyledTooltip, { TYPE, EFFECT, TRIGGER_TYPE } from "../authentication/StyledTooltip";
import CustomButton, { THEME } from "../commons/CustomButton";
import LastLogin from "../authentication/LastLogin";
import orderByLabels from "../../helpers/orderingHelpers";

export const styles = {
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  disabledActionButton: {
    // default disabled color
    color: "#DDDDDD"
  },
  time: {
    padding: "6px 12px",
    border: "2px solid #00565e",
    borderRadius: 8,
    fontWeight: "bold",
    color: "#00565e"
  },
  noMargin: {
    margin: 0
  },
  allUnset: {
    all: "unset"
  },
  appPadding: {
    padding: "15px"
  }
};

export const taActions = {
  APPROVE: "APPROVE",
  UNASSIGN: "UN_ASSIGN",
  LOCK_UNLOCK: "LOCK_UNLOCK",
  LOCK_UNLOCK_ALL: "LOCK_UNLOCK_ALL"
};

class TestAdministration extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  static propTypes = {
    // Props from Redux
    isTokenStillValid: PropTypes.func,
    getActiveTestAccessCodesRedux: PropTypes.func,
    getTestPermissions: PropTypes.func,
    getTestAdministratorAssignedCandidates: PropTypes.func,
    getUserInformation: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.languageFieldRef = React.createRef();
    this.testFieldRef = React.createRef();
  }

  state = {
    isLoaded: false,
    testPermissions: [],
    activeTestAccessCodes: [],
    activeTestAccessCodesLoading: false,
    testOrderNumberOptions: [],
    testToAdministerOptions: [],
    testSessionLanguageOptions: [],
    testAdministratorAssignedCandidates: [],
    rowsDefinition: {},
    currentlyLoading: false,
    selectedCandidateData: {},
    triggerShowEditTimePopup: false,
    triggerShowAddEditBreakBankPopup: false,
    triggerShowLockPopup: false,
    triggerShowUnlockPopup: false,
    triggerApproveCandidate: false,
    triggerUnAssignCandidate: false,
    testAccessCodes: [],
    triggerReRender: false,
    isTaDashboard: false,
    pollingState: undefined
  };

  // calling redux state on page load to get the first name and last name
  componentDidMount = () => {
    this._isMounted = true;
    // check if the token is still valid before getting user information
    this.props.isTokenStillValid().then(bool => {
      // if the token is still valid
      if (this._isMounted && bool) {
        this.getActiveTestAccessCodes();
        this.populateTestPermissions();
        this.populateTestAdministratorAssignedCandidates();
        this.setState({
          isLoaded: true
        });
      }
    });

    if (this.props.currentHomePage === PATH.testAdministration) {
      this.setState({
        isTaDashboard: true
      });
    }
    // if there is at least one test access code or at least one active candidate
    if (
      this.state.activeTestAccessCodes.length > 0 ||
      this.state.testAdministratorAssignedCandidates.length > 0
    ) {
      // clearing existing polling interval(s) and creating new one (interval of 5 seconds)
      clearInterval(this.state.pollingState);
      const interval = setInterval(this.handleActiveCandidatesTableUpdates, 5000);
      this.setState({ pollingState: interval });
    }
  };

  componentDidUpdate(prevProps, prevState) {
    // check if the token is still valid before getting user information
    this.props.isTokenStillValid().then(bool => {
      // if the token is still valid
      if (this._isMounted && bool) {
        // if CAT language toggle button has been selected
        if (prevProps.currentLanguage !== this.props.currentLanguage) {
          this.getActiveTestAccessCodes();
          this.populateTestPermissions();
          this.populateTestAdministratorAssignedCandidates();
        }
      }
      // if activeTestAccessCodes or testAdministratorAssignedCandidates gets updated
      if (
        prevState.activeTestAccessCodes !== this.state.activeTestAccessCodes ||
        prevState.testAdministratorAssignedCandidates !==
          this.state.testAdministratorAssignedCandidates
      ) {
        // if there is at least one test access code or at least one active candidate
        if (
          this.state.activeTestAccessCodes.length > 0 ||
          this.state.testAdministratorAssignedCandidates.length > 0
        ) {
          // clearing existing polling interval(s) and creating new one (interval of 5 seconds)
          clearInterval(this.state.pollingState);
          const interval = setInterval(this.handleActiveCandidatesTableUpdates, 5000);
          this.setState({ pollingState: interval });
        } else {
          // clearing existing polling interval(s)
          clearInterval(this.state.pollingState);
        }
      }
    });
  }

  componentWillUnmount = () => {
    this._isMounted = false;
    clearInterval(this.state.pollingState);
  };

  handleActiveCandidatesTableUpdates = () => {
    // ==================== TEST ACCESS CODES LOGIC ====================
    const activeTestAccessCodes = [];
    this.props.getActiveTestAccessCodesRedux().then(response => {
      // looping in all active test access codes
      for (let i = 0; i < response.length; i++) {
        // pushing results in array
        activeTestAccessCodes.push(response[i]);
      }
      // saving array in activeTestAccessCodes state
      this.setState({ activeTestAccessCodes: activeTestAccessCodes });
    });
    // ==================== TEST ACCESS CODES LOGIC (END) ====================

    // ==================== ACTIVE CANDIDATES LOGIC ====================
    // initializing needed object and array for rowsDefinition props (needed for GenericTable component)
    let rowsDefinition = {};
    const data = [];
    // initializing testAdministratorAssignedCandidates array
    let testAdministratorAssignedCandidates = [];
    this.props.getTestAdministratorAssignedCandidates().then(response => {
      // looping in response given
      for (let i = 0; i < response.length; i++) {
        // populating data object with provided data
        data.push({
          column_1: `${response[i].candidate_last_name}, ${response[i].candidate_first_name} (${response[i].candidate_email})`,
          column_2: response[i].candidate_dob,
          column_3: response[i].test.test_code,
          column_4: getFormattedTestStatus(Number(response[i].status)),
          column_5: this.getTotalTestTime(response[i].assigned_test_sections),
          column_6: this.getTestTimeRemaining(response[i].test_time_remaining),
          column_7: this.populateColumnSix(i, response)
        });
      }

      // updating testAdministratorAssignedCandidates array with provided data
      testAdministratorAssignedCandidates = response;

      // updating rowsDefinition object with provided data and needed style
      rowsDefinition = {
        column_1_style: COMMON_STYLE.LEFT_TEXT,
        column_2_style: COMMON_STYLE.CENTERED_TEXT,
        column_3_style: COMMON_STYLE.CENTERED_TEXT,
        column_4_style: COMMON_STYLE.CENTERED_TEXT,
        column_5_style: COMMON_STYLE.CENTERED_TEXT,
        column_6_style: COMMON_STYLE.CENTERED_TEXT,
        column_7_style: COMMON_STYLE.CENTERED_TEXT,
        data: data
      };

      // saving results in state
      this.setState({
        testAdministratorAssignedCandidates: testAdministratorAssignedCandidates,
        rowsDefinition: rowsDefinition
      });
    });
    // ==================== ACTIVE CANDIDATES LOGIC (END) ====================
  };

  // getting all active test access codes for the current TA
  getActiveTestAccessCodes = () => {
    this.setState({ activeTestAccessCodesLoading: true }, () => {
      const activeTestAccessCodes = [];
      this.props
        .getActiveTestAccessCodesRedux()
        .then(response => {
          // looping in all active test access codes
          for (let i = 0; i < response.length; i++) {
            // pushing results in array
            activeTestAccessCodes.push(response[i]);
          }
          // saving array in activeTestAccessCodes state
          this.setState({ activeTestAccessCodes: activeTestAccessCodes });
        })
        .then(() => {
          this.setState({ activeTestAccessCodesLoading: false });
        });
    });
  };

  // populating user' test permissions object
  populateTestPermissions = () => {
    this.props.getTestPermissions(false).then(response => {
      this.setState({ testPermissions: response }, () => {
        // populating test order number options
        this.populateTestOrderNumberOptions();
      });
    });
  };

  // populating test order number options
  populateTestOrderNumberOptions = () => {
    const testOrderNumberOptionsDuplicatesRemoval = [];
    const testOrderNumberOptionsArray = [];
    const testOrderNumberOptions = [];
    // looping in test permissions
    for (let i = 0; i < this.state.testPermissions.length; i++) {
      // if test order number does not already exist in testOrderNumberOptionsArray push it
      if (
        testOrderNumberOptionsDuplicatesRemoval.indexOf(
          this.state.testPermissions[i].test_order_number
        ) < 0
      ) {
        testOrderNumberOptionsArray.push(
          `${this.state.testPermissions[i].test_order_number} (${this.state.testPermissions[i].staffing_process_number})`
        );
        testOrderNumberOptionsDuplicatesRemoval.push(
          this.state.testPermissions[i].test_order_number
        );
      }
    }
    // looping in testOrderNumberOptionsArray in order to create the testOrderNumberOptions array (including values and labels)
    for (let i = 0; i < testOrderNumberOptionsArray.length; i++) {
      testOrderNumberOptions.push({
        label: testOrderNumberOptionsArray[i],
        // getting only the first part of the test order number - without (<staffing_process_number>)
        value: testOrderNumberOptionsArray[i].split(" ")[0]
      });
    }
    // sorting options
    testOrderNumberOptions.sort(orderByLabels);
    // adding orderless test option (as first option)
    testOrderNumberOptions.unshift({
      label:
        LOCALIZE.testAdministration.testAccessCodes.generateTestAccessCodePopup.orderlessTestOption,
      value: 0
    });
    // saving results in state
    this.setState({ testOrderNumberOptions: testOrderNumberOptions });
  };

  // populating test to administer options
  populateTestToAdministerOptions = testOrderNumber => {
    const testToAdministerOptionsArray = [];
    const testToAdministerOptions = [];
    const testToAdministerTestInternalNames = [];

    // orderless test order option has been selected
    if (testOrderNumber.value === 0) {
      this.props.getOrderlessTestOptions().then(response => {
        // looping in response
        for (let i = 0; i < response.length; i++) {
          // populating testToAdministerOptions
          testToAdministerOptions.push({
            label: `${response[i][`${this.props.currentLanguage}_name`]} - v${response[i].version}`,
            value: response[i].id
          });
        }
      });
      // regular test access codes has been selected
    } else {
      // looping in test permissions
      for (let i = 0; i < this.state.testPermissions.length; i++) {
        // getting all object attributes where test order number is the same as the one provided
        if (testOrderNumber.value === this.state.testPermissions[i].test_order_number) {
          // if test to administer does not already exist in testToAdministerOptionsArray push it
          if (
            testToAdministerOptionsArray.indexOf(
              this.state.testPermissions[i].test[`${this.props.currentLanguage}_name`]
            ) < 0
          ) {
            testToAdministerOptionsArray.push(
              this.state.testPermissions[i].test[`${this.props.currentLanguage}_name`]
            );
            testToAdministerTestInternalNames.push(this.state.testPermissions[i].test);
          }
        }
      }
      // looping in testToAdministerOptionsArray in order to create the testToAdministerOptions array (including values and labels)
      for (let i = 0; i < testToAdministerOptionsArray.length; i++) {
        testToAdministerOptions.push({
          label: testToAdministerOptionsArray[i],
          value: testToAdministerTestInternalNames[i].id
        });
      }
    }
    // saving results in state
    this.setState({ testToAdministerOptions: testToAdministerOptions });
  };

  // populating test session language options
  populateTestSessionLanguageOptions = () => {
    const testSessionLanguageOptions = [];
    // english option
    testSessionLanguageOptions.push({
      label: LOCALIZE.commons.english,
      value: LOCALIZE.commons.english
    });
    // french option
    testSessionLanguageOptions.push({
      label: LOCALIZE.commons.french,
      value: LOCALIZE.commons.french
    });
    // saving results in state
    this.setState({ testSessionLanguageOptions: testSessionLanguageOptions });
  };

  // populating assigned candidates of the current test administrator
  populateTestAdministratorAssignedCandidates = () => {
    this.setState({ currentlyLoading: true }, () => {
      // initializing needed object and array for rowsDefinition props (needed for GenericTable component)
      let rowsDefinition = {};
      const data = [];
      // initializing testAdministratorAssignedCandidates array
      let testAdministratorAssignedCandidates = [];
      this.props
        .getTestAdministratorAssignedCandidates()
        .then(response => {
          // looping in response given
          for (let i = 0; i < response.length; i++) {
            // populating data object with provided data
            data.push({
              column_1: `${response[i].candidate_last_name}, ${response[i].candidate_first_name} (${response[i].candidate_email})`,
              column_2: response[i].candidate_dob,
              column_3: response[i].test.test_code,
              column_4: getFormattedTestStatus(Number(response[i].status)),
              column_5: this.getTotalTestTime(response[i].assigned_test_sections),
              column_6: this.getTestTimeRemaining(response[i].test_time_remaining),
              column_7: this.populateColumnSix(i, response)
            });
          }

          // updating testAdministratorAssignedCandidates array with provided data
          testAdministratorAssignedCandidates = response;

          // updating rowsDefinition object with provided data and needed style
          rowsDefinition = {
            column_1_style: COMMON_STYLE.LEFT_TEXT,
            column_2_style: COMMON_STYLE.CENTERED_TEXT,
            column_3_style: COMMON_STYLE.CENTERED_TEXT,
            column_4_style: COMMON_STYLE.CENTERED_TEXT,
            column_5_style: COMMON_STYLE.CENTERED_TEXT,
            column_6_style: COMMON_STYLE.CENTERED_TEXT,
            column_7_style: COMMON_STYLE.CENTERED_TEXT,
            data: data
          };

          // saving results in state
          this.setState({
            testAdministratorAssignedCandidates: testAdministratorAssignedCandidates,
            rowsDefinition: rowsDefinition
          });
        })
        .then(() => {
          this.setState({ currentlyLoading: false });
        });
    });
  };

  getTestTimeRemaining = timeRemaining => {
    // divided by 60, since we're receiving time in seconds
    return `${getTimeInHoursMinutes(Number(timeRemaining / 60)).formattedHours} : ${
      getTimeInHoursMinutes(Number(timeRemaining / 60)).formattedMinutes
    }`;
  };

  populateColumnSix = (i, response) => {
    // user has AAE permission
    if (this.props.isAae) {
      return (
        <div className="d-flex justify-content-center flex-nowrap">
          <StyledTooltip
            id={`update-test-time-tooltip-${i}`}
            place="top"
            type={TYPE.light}
            effect={EFFECT.solid}
            triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
            disabled={response[i].status !== TEST_STATUS.ASSIGNED}
            tooltipElement={
              <div style={styles.allUnset}>
                <CustomButton
                  dataTip=""
                  dataFor={`update-test-time-tooltip-${i}`}
                  label={
                    <FontAwesomeIcon
                      icon={response[i].test_time_updated ? faClock : regularFaClock}
                    />
                  }
                  action={() => {
                    this.editCandidateTime(i);
                  }}
                  customStyle={
                    response[i].status === TEST_STATUS.ASSIGNED
                      ? styles.actionButton
                      : { ...styles.actionButton, ...styles.disabledActionButton }
                  }
                  buttonTheme={THEME.SECONDARY}
                  disabled={response[i].status !== TEST_STATUS.ASSIGNED}
                  ariaLabel={LOCALIZE.formatString(
                    LOCALIZE.testAdministration.activeCandidates.table.updateTestTimerAriaLabel,
                    response[i].candidate_first_name,
                    `${response[i].candidate_last_name} (${response[i].candidate_email})`,
                    response[i].candidate_dob,
                    response[i].test.test_code,
                    getFormattedTestStatus(Number(response[i].status)),
                    getTimeInHoursMinutes(
                      Number(this.getTotalTestTimeInMinutes(response[i].assigned_test_sections))
                    ).formattedHours,
                    getTimeInHoursMinutes(
                      Number(this.getTotalTestTimeInMinutes(response[i].assigned_test_sections))
                    ).formattedMinutes
                  )}
                />
              </div>
            }
            tooltipContent={
              <div>
                <p>
                  {
                    LOCALIZE.testAdministration.activeCandidates.table.actionTooltips
                      .updateTestTimer
                  }
                </p>
              </div>
            }
          />
          <StyledTooltip
            id={`add-edit-break-bank-tooltip-${i}`}
            place="top"
            type={TYPE.light}
            effect={EFFECT.solid}
            triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
            disabled={response[i].status !== TEST_STATUS.ASSIGNED}
            tooltipElement={
              <div style={styles.allUnset}>
                <CustomButton
                  dataTip=""
                  dataFor={`add-edit-break-bank-tooltip-${i}`}
                  label={
                    <FontAwesomeIcon
                      icon={response[i].pause_test_time > 0 ? faPauseCircle : regularFaPauseCircle}
                    />
                  }
                  action={() => {
                    this.addEditCandidateBreakBank(i);
                  }}
                  customStyle={
                    response[i].status === TEST_STATUS.ASSIGNED
                      ? styles.actionButton
                      : { ...styles.actionButton, ...styles.disabledActionButton }
                  }
                  buttonTheme={THEME.SECONDARY}
                  disabled={response[i].status !== TEST_STATUS.ASSIGNED}
                  ariaLabel={LOCALIZE.formatString(
                    LOCALIZE.testAdministration.activeCandidates.table.addEditBreakBankAriaLabel,
                    response[i].candidate_first_name,
                    `${response[i].candidate_last_name} (${response[i].candidate_email})`,
                    response[i].candidate_dob,
                    response[i].test.test_code,
                    getFormattedTestStatus(Number(response[i].status)),
                    getTimeInHoursMinutes(
                      Number(this.getTotalTestTimeInMinutes(response[i].assigned_test_sections))
                    ).formattedHours,
                    getTimeInHoursMinutes(
                      Number(this.getTotalTestTimeInMinutes(response[i].assigned_test_sections))
                    ).formattedMinutes
                  )}
                />
              </div>
            }
            tooltipContent={
              <div>
                <p>
                  {
                    LOCALIZE.testAdministration.activeCandidates.table.actionTooltips
                      .addEditBreakBank
                  }
                </p>
              </div>
            }
          />
          <StyledTooltip
            id={`approve-candidate-tooltip-${i}`}
            place="top"
            type={TYPE.light}
            effect={EFFECT.solid}
            triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
            disabled={response[i].status !== TEST_STATUS.ASSIGNED}
            tooltipElement={
              <div style={styles.allUnset}>
                <CustomButton
                  dataTip=""
                  dataFor={`approve-candidate-tooltip-${i}`}
                  label={
                    <FontAwesomeIcon
                      icon={
                        response[i].status === TEST_STATUS.ASSIGNED ? regularFaThumbsUp : faThumbsUp
                      }
                    />
                  }
                  action={() => {
                    this.taAction(i, taActions.APPROVE);
                  }}
                  customStyle={
                    response[i].status === TEST_STATUS.ASSIGNED
                      ? styles.actionButton
                      : { ...styles.actionButton, ...styles.disabledActionButton }
                  }
                  buttonTheme={THEME.SECONDARY}
                  disabled={response[i].status !== TEST_STATUS.ASSIGNED}
                  ariaLabel={LOCALIZE.formatString(
                    LOCALIZE.testAdministration.activeCandidates.table.approveAriaLabel,
                    response[i].candidate_first_name,
                    `${response[i].candidate_last_name} (${response[i].candidate_email})`,
                    response[i].candidate_dob,
                    response[i].test.test_code,
                    getFormattedTestStatus(Number(response[i].status)),
                    getTimeInHoursMinutes(
                      Number(this.getTotalTestTimeInMinutes(response[i].assigned_test_sections))
                    ).formattedHours,
                    getTimeInHoursMinutes(
                      Number(this.getTotalTestTimeInMinutes(response[i].assigned_test_sections))
                    ).formattedMinutes
                  )}
                />
              </div>
            }
            tooltipContent={
              <div>
                <p>{LOCALIZE.testAdministration.activeCandidates.table.actionTooltips.approve}</p>
              </div>
            }
          />
          <StyledTooltip
            id={`unassign-candidate-tooltip-${i}`}
            place="top"
            type={TYPE.light}
            effect={EFFECT.solid}
            triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
            disabled={
              response[i].status !== TEST_STATUS.ASSIGNED &&
              response[i].status !== TEST_STATUS.READY &&
              response[i].status !== TEST_STATUS.PRE_TEST
            }
            tooltipElement={
              <div style={styles.allUnset}>
                <CustomButton
                  dataTip=""
                  dataFor={`unassign-candidate-tooltip-${i}`}
                  label={<FontAwesomeIcon icon={faTrashAlt} />}
                  action={() => {
                    this.taAction(i, taActions.UNASSIGN);
                  }}
                  customStyle={
                    response[i].status === TEST_STATUS.ASSIGNED ||
                    response[i].status === TEST_STATUS.READY ||
                    response[i].status === TEST_STATUS.PRE_TEST
                      ? styles.actionButton
                      : { ...styles.actionButton, ...styles.disabledActionButton }
                  }
                  buttonTheme={THEME.SECONDARY}
                  disabled={
                    response[i].status !== TEST_STATUS.ASSIGNED &&
                    response[i].status !== TEST_STATUS.READY &&
                    response[i].status !== TEST_STATUS.PRE_TEST
                  }
                  ariaLabel={LOCALIZE.formatString(
                    LOCALIZE.testAdministration.activeCandidates.table.unAssignAriaLabel,
                    response[i].candidate_first_name,
                    `${response[i].candidate_last_name} (${response[i].candidate_email})`,
                    response[i].candidate_dob,
                    response[i].test.test_code,
                    getFormattedTestStatus(Number(response[i].status)),
                    getTimeInHoursMinutes(
                      Number(this.getTotalTestTimeInMinutes(response[i].assigned_test_sections))
                    ).formattedHours,
                    getTimeInHoursMinutes(
                      Number(this.getTotalTestTimeInMinutes(response[i].assigned_test_sections))
                    ).formattedMinutes
                  )}
                />
              </div>
            }
            tooltipContent={
              <div>
                <p>{LOCALIZE.testAdministration.activeCandidates.table.actionTooltips.unAssign}</p>
              </div>
            }
          />
          <StyledTooltip
            id={`lock-candidate-tooltip-${i}`}
            place="top"
            type={TYPE.light}
            effect={EFFECT.solid}
            triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
            disabled={response[i].status === TEST_STATUS.ASSIGNED}
            tooltipElement={
              <div style={styles.allUnset}>
                <CustomButton
                  dataTip=""
                  dataFor={`lock-candidate-tooltip-${i}`}
                  label={
                    <FontAwesomeIcon
                      icon={response[i].status === TEST_STATUS.LOCKED ? faLockOpen : faLock}
                    />
                  }
                  action={() => {
                    this.taAction(i, taActions.LOCK_UNLOCK);
                  }}
                  customStyle={
                    response[i].status === TEST_STATUS.ASSIGNED
                      ? { ...styles.actionButton, ...styles.disabledActionButton }
                      : styles.actionButton
                  }
                  buttonTheme={THEME.SECONDARY}
                  disabled={response[i].status === TEST_STATUS.ASSIGNED}
                  ariaLabel={LOCALIZE.formatString(
                    response[i].status === TEST_STATUS.LOCKED
                      ? LOCALIZE.testAdministration.activeCandidates.table.unlockAriaLabel
                      : LOCALIZE.testAdministration.activeCandidates.table.lockAriaLabel,
                    response[i].candidate_first_name,
                    `${response[i].candidate_last_name} (${response[i].candidate_email})`,
                    response[i].candidate_dob,
                    response[i].test.test_code,
                    getFormattedTestStatus(Number(response[i].status)),
                    getTimeInHoursMinutes(
                      Number(this.getTotalTestTimeInMinutes(response[i].assigned_test_sections))
                    ).formattedHours,
                    getTimeInHoursMinutes(
                      Number(this.getTotalTestTimeInMinutes(response[i].assigned_test_sections))
                    ).formattedMinutes
                  )}
                />
              </div>
            }
            tooltipContent={
              <div>
                <p>
                  {response[i].status === TEST_STATUS.LOCKED
                    ? LOCALIZE.testAdministration.activeCandidates.table.actionTooltips.unlock
                    : LOCALIZE.testAdministration.activeCandidates.table.actionTooltips.lock}
                </p>
              </div>
            }
          />
          {/* <StyledTooltip
            trigger={["hover", "focus"]}
            placement="top"
            overlayClassName={"tooltip"}
            overlay={
              <div>
                <p>{LOCALIZE.testAdministration.activeCandidates.table.actionTooltips.report}</p>
              </div>
            }
          >
            <div style={styles.allUnset}>
              <CustomButton
                label={<FontAwesomeIcon icon={faEdit} />}
                action={() => {
                  this.reportCandidate(i);
                }}
                customStyle={styles.actionButton}
                buttonTheme={THEME.SECONDARY}
                ariaLabel={LOCALIZE.formatString(
                  LOCALIZE.testAdministration.activeCandidates.table.reportAriaLabel,
                  response[i].candidate_first_name,
                  `${response[i].candidate_last_name} (${response[i].candidate_email})`
                )}
              />
            </div>
          </StyledTooltip> */}
        </div>
      );
    }
    // user is a regular TA
    return (
      <div className="d-flex justify-content-center flex-nowrap">
        <StyledTooltip
          id={`approve-candidate-tooltip-${i}`}
          place="top"
          type={TYPE.light}
          effect={EFFECT.solid}
          triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
          disabled={response[i].status !== TEST_STATUS.ASSIGNED}
          tooltipElement={
            <div style={styles.allUnset}>
              <CustomButton
                dataTip=""
                dataFor={`approve-candidate-tooltip-${i}`}
                label={
                  <FontAwesomeIcon
                    icon={
                      response[i].status === TEST_STATUS.ASSIGNED ? regularFaThumbsUp : faThumbsUp
                    }
                  />
                }
                action={() => {
                  this.taAction(i, taActions.APPROVE);
                }}
                customStyle={
                  response[i].status === TEST_STATUS.ASSIGNED
                    ? styles.actionButton
                    : { ...styles.actionButton, ...styles.disabledActionButton }
                }
                buttonTheme={THEME.SECONDARY}
                disabled={response[i].status !== TEST_STATUS.ASSIGNED}
                ariaLabel={LOCALIZE.formatString(
                  LOCALIZE.testAdministration.activeCandidates.table.approveAriaLabel,
                  response[i].candidate_first_name,
                  `${response[i].candidate_last_name} (${response[i].candidate_email})`,
                  response[i].candidate_dob,
                  response[i].test.test_code,
                  getFormattedTestStatus(Number(response[i].status)),
                  getTimeInHoursMinutes(
                    Number(this.getTotalTestTimeInMinutes(response[i].assigned_test_sections))
                  ).formattedHours,
                  getTimeInHoursMinutes(
                    Number(this.getTotalTestTimeInMinutes(response[i].assigned_test_sections))
                  ).formattedMinutes
                )}
              />
            </div>
          }
          tooltipContent={
            <div>
              <p>{LOCALIZE.testAdministration.activeCandidates.table.actionTooltips.approve}</p>
            </div>
          }
        />
        <StyledTooltip
          id={`unassign-candidate-tooltip-${i}`}
          place="top"
          type={TYPE.light}
          effect={EFFECT.solid}
          triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
          disabled={
            response[i].status !== TEST_STATUS.ASSIGNED &&
            response[i].status !== TEST_STATUS.READY &&
            response[i].status !== TEST_STATUS.PRE_TEST
          }
          tooltipElement={
            <div style={styles.allUnset}>
              <CustomButton
                dataTip=""
                dataFor={`unassign-candidate-tooltip-${i}`}
                label={<FontAwesomeIcon icon={faTrashAlt} />}
                action={() => {
                  this.taAction(i, taActions.UNASSIGN);
                }}
                customStyle={
                  response[i].status === TEST_STATUS.ASSIGNED ||
                  response[i].status === TEST_STATUS.READY ||
                  response[i].status === TEST_STATUS.PRE_TEST
                    ? styles.actionButton
                    : { ...styles.actionButton, ...styles.disabledActionButton }
                }
                buttonTheme={THEME.SECONDARY}
                disabled={
                  response[i].status !== TEST_STATUS.ASSIGNED &&
                  response[i].status !== TEST_STATUS.READY &&
                  response[i].status !== TEST_STATUS.PRE_TEST
                }
                ariaLabel={LOCALIZE.formatString(
                  LOCALIZE.testAdministration.activeCandidates.table.unAssignAriaLabel,
                  response[i].candidate_first_name,
                  `${response[i].candidate_last_name} (${response[i].candidate_email})`,
                  response[i].candidate_dob,
                  response[i].test.test_code,
                  getFormattedTestStatus(Number(response[i].status)),
                  getTimeInHoursMinutes(
                    Number(this.getTotalTestTimeInMinutes(response[i].assigned_test_sections))
                  ).formattedHours,
                  getTimeInHoursMinutes(
                    Number(this.getTotalTestTimeInMinutes(response[i].assigned_test_sections))
                  ).formattedMinutes
                )}
              />
            </div>
          }
          tooltipContent={
            <div>
              <p>{LOCALIZE.testAdministration.activeCandidates.table.actionTooltips.unAssign}</p>
            </div>
          }
        />
        <StyledTooltip
          id={`lock-candidate-tooltip-${i}`}
          place="top"
          type={TYPE.light}
          effect={EFFECT.solid}
          triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
          disabled={response[i].status === TEST_STATUS.ASSIGNED}
          tooltipElement={
            <div style={styles.allUnset}>
              <CustomButton
                dataTip=""
                dataFor={`lock-candidate-tooltip-${i}`}
                label={
                  <FontAwesomeIcon
                    icon={response[i].status === TEST_STATUS.LOCKED ? faLockOpen : faLock}
                  />
                }
                action={() => {
                  this.taAction(i, taActions.LOCK_UNLOCK);
                }}
                customStyle={
                  response[i].status === TEST_STATUS.ASSIGNED
                    ? { ...styles.actionButton, ...styles.disabledActionButton }
                    : styles.actionButton
                }
                buttonTheme={THEME.SECONDARY}
                disabled={response[i].status === TEST_STATUS.ASSIGNED}
                ariaLabel={LOCALIZE.formatString(
                  response[i].status === TEST_STATUS.LOCKED
                    ? LOCALIZE.testAdministration.activeCandidates.table.unlockAriaLabel
                    : LOCALIZE.testAdministration.activeCandidates.table.lockAriaLabel,
                  response[i].candidate_first_name,
                  `${response[i].candidate_last_name} (${response[i].candidate_email})`,
                  response[i].candidate_dob,
                  response[i].test.test_code,
                  getFormattedTestStatus(Number(response[i].status)),
                  getTimeInHoursMinutes(
                    Number(this.getTotalTestTimeInMinutes(response[i].assigned_test_sections))
                  ).formattedHours,
                  getTimeInHoursMinutes(
                    Number(this.getTotalTestTimeInMinutes(response[i].assigned_test_sections))
                  ).formattedMinutes
                )}
              />
            </div>
          }
          tooltipContent={
            <div>
              <p>
                {response[i].status === TEST_STATUS.LOCKED
                  ? LOCALIZE.testAdministration.activeCandidates.table.actionTooltips.unlock
                  : LOCALIZE.testAdministration.activeCandidates.table.actionTooltips.lock}
              </p>
            </div>
          }
        />
        {/* <StyledTooltip
            trigger={["hover", "focus"]}
            placement="top"
            overlayClassName={"tooltip"}
            overlay={
              <div>
                <p>{LOCALIZE.testAdministration.activeCandidates.table.actionTooltips.report}</p>
              </div>
            }
          >
            <div style={styles.allUnset}>
              <CustomButton
                label={<FontAwesomeIcon icon={faEdit} />}
                action={() => {
                  this.reportCandidate(i);
                }}
                customStyle={styles.actionButton}
                buttonTheme={THEME.SECONDARY}
                ariaLabel={LOCALIZE.formatString(
                  LOCALIZE.testAdministration.activeCandidates.table.reportAriaLabel,
                  response[i].candidate_first_name,
                  `${response[i].candidate_last_name} (${response[i].candidate_email})`
                )}
              />
            </div>
          </StyledTooltip> */}
      </div>
    );
  };

  // getting total test time based on all timed test sections
  getTotalTestTime = assignedTestSections => {
    // initializing totalTestTime
    let totalTestTime = 0;
    // looping in all assigned test sections
    for (let i = 0; i < assignedTestSections.length; i++) {
      // test_section_time is not null
      if (assignedTestSections[i].test_section_time !== null) {
        // incrementing totalTestTime
        totalTestTime += assignedTestSections[i].test_section_time;
      }
    }
    // returning formatted test time
    return `${getTimeInHoursMinutes(Number(totalTestTime)).formattedHours} : ${
      getTimeInHoursMinutes(Number(totalTestTime)).formattedMinutes
    }`;
  };

  // getting total test time based on all timed test sections
  getTotalTestTimeInMinutes = assignedTestSections => {
    // initializing totalTestTime
    let totalTestTime = 0;
    // looping in all assigned test sections
    for (let i = 0; i < assignedTestSections.length; i++) {
      // test_section_time is not null
      if (assignedTestSections[i].test_section_time !== null) {
        // incrementing totalTestTime
        totalTestTime += assignedTestSections[i].test_section_time;
      }
    }
    // returning total test time in minutes
    return totalTestTime;
  };

  taAction = (i, action) => {
    switch (action) {
      case taActions.APPROVE:
        this.approveCandidate(i);
        break;
      case taActions.UNASSIGN:
        this.unAssignCandidate(i);
        break;
      case taActions.LOCK_UNLOCK:
        this.lockUnlockCandidate(i);
        break;
      // no action needed for now
      case taActions.LOCK_UNLOCK_ALL:
        break;
      default:
        break;
    }
  };

  // edit candidate time
  editCandidateTime = i => {
    this.setState({
      triggerShowEditTimePopup: !this.state.triggerShowEditTimePopup,
      selectedCandidateData: this.state.testAdministratorAssignedCandidates[i]
    });
  };

  // add candidate pause time
  addEditCandidateBreakBank = i => {
    this.setState({
      triggerShowAddEditBreakBankPopup: !this.state.triggerShowAddEditBreakBankPopup,
      selectedCandidateData: this.state.testAdministratorAssignedCandidates[i]
    });
  };

  // approve candidate
  approveCandidate = i => {
    this.setState({
      triggerApproveCandidate: !this.state.triggerApproveCandidate,
      selectedCandidateData: this.state.testAdministratorAssignedCandidates[i]
    });
  };

  // un-assign candidate's test
  unAssignCandidate = i => {
    this.setState({
      triggerUnAssignCandidate: !this.state.triggerUnAssignCandidate,
      selectedCandidateData: this.state.testAdministratorAssignedCandidates[i]
    });
  };

  // lock/unlock candidate
  lockUnlockCandidate = i => {
    // test is locked
    if (this.state.testAdministratorAssignedCandidates[i].status === TEST_STATUS.LOCKED) {
      this.setState({
        triggerShowUnlockPopup: !this.state.triggerShowUnlockPopup,
        selectedCandidateData: this.state.testAdministratorAssignedCandidates[i]
      });
      // test is active
    } else {
      this.setState({
        triggerShowLockPopup: !this.state.triggerShowLockPopup,
        selectedCandidateData: this.state.testAdministratorAssignedCandidates[i]
      });
    }
  };

  // TODO (fnormand): implement report candidate functionality
  reportCandidate = i => {
    console.log("Report candidate #", i);
  };

  // Returns array where each item indicates specifications related to How To Page including the title and the body
  getTestAdministrationSections = () => {
    return [
      {
        menuString: LOCALIZE.testAdministration.sideNavItems.testAccessCodes,
        body: (
          <TestAccessCodes
            activeTestAccessCodes={this.state.activeTestAccessCodes}
            activeTestAccessCodesLoading={this.state.activeTestAccessCodesLoading}
            getActiveTestAccessCodes={this.getActiveTestAccessCodes}
            testOrderNumberOptions={this.state.testOrderNumberOptions}
            populateTestToAdministerOptions={this.populateTestToAdministerOptions}
            testToAdministerOptions={this.state.testToAdministerOptions}
            populateTestSessionLanguageOptions={this.populateTestSessionLanguageOptions}
            testSessionLanguageOptions={this.state.testSessionLanguageOptions}
          />
        )
      },
      {
        menuString: LOCALIZE.testAdministration.sideNavItems.activeCandidates,
        body: (
          <ActiveCandidates
            testAdministratorAssignedCandidates={this.state.testAdministratorAssignedCandidates}
            rowsDefinition={this.state.rowsDefinition}
            currentlyLoading={this.state.currentlyLoading}
            triggerShowEditTimePopup={this.state.triggerShowEditTimePopup}
            triggerShowAddEditBreakBankPopup={this.state.triggerShowAddEditBreakBankPopup}
            triggerShowLockPopup={this.state.triggerShowLockPopup}
            triggerShowUnlockPopup={this.state.triggerShowUnlockPopup}
            triggerApproveCandidate={this.state.triggerApproveCandidate}
            triggerUnAssignCandidate={this.state.triggerUnAssignCandidate}
            updateAssignedCandidatesTable={this.updateAssignedCandidatesTable}
            selectedCandidateData={this.state.selectedCandidateData}
            populateTestAdministratorAssignedCandidates={
              this.populateTestAdministratorAssignedCandidates
            }
            triggerReRender={this.state.triggerReRender}
            taAction={this.taAction}
          />
        )
      },
      {
        menuString: LOCALIZE.testAdministration.sideNavItems.uat,
        body: <UnsupervisedInternetTesting triggerReRender={this.state.triggerReRender} />
      },
      {
        menuString: LOCALIZE.testAdministration.sideNavItems.reports,
        body: <Reports />
      }
    ];
  };

  updateAssignedCandidatesTable = () => {
    // initializing variables
    const testAdministratorAssignedCandidates = [];
    const newData = [];
    const { rowsDefinition } = this.state;
    // getting assigned candidates
    this.props.getTestAdministratorAssignedCandidates().then(response => {
      // populating table data
      for (let i = 0; i < response.length; i++) {
        testAdministratorAssignedCandidates.push(response[i]);
        newData.push({
          column_1: `${response[i].candidate_last_name}, ${response[i].candidate_first_name} (${response[i].candidate_email})`,
          column_2: response[i].candidate_dob,
          column_3: response[i].test.test_code,
          column_4: getFormattedTestStatus(Number(response[i].status)),
          column_5: this.getTotalTestTime(response[i].assigned_test_sections),
          column_6: this.getTestTimeRemaining(response[i].test_time_remaining),
          column_7: this.populateColumnSix(i, response)
        });
      }
      // rebuilding rowsDefinition state
      const newRowsDefinition = {
        column_1_style: rowsDefinition.column_1_style,
        column_2_style: rowsDefinition.column_2_style,
        column_3_style: rowsDefinition.column_3_style,
        column_4_style: rowsDefinition.column_4_style,
        column_5_style: rowsDefinition.column_5_style,
        column_6_style: rowsDefinition.column_6_style,
        column_7_style: COMMON_STYLE.CENTERED_TEXT,
        data: newData
      };
      // saving new data in state + triggering re-render
      this.setState({
        rowsDefinition: newRowsDefinition,
        testAdministratorAssignedCandidates: testAdministratorAssignedCandidates,
        triggerReRender: !this.state.triggerReRender
      });
    });
  };

  render() {
    const specs = this.getTestAdministrationSections();
    return (
      <div>
        {this.state.isTaDashboard && <LastLogin lastLoginDate={this.props.lastLogin} />}
        <div className="app" style={styles.appPadding}>
          <Helmet>
            <html lang={this.props.currentLanguage} />
            <title className="notranslate">{LOCALIZE.titles.testAdministration}</title>
          </Helmet>
          <ContentContainer>
            <div id="main-content" role="main">
              <div
                id="user-welcome-message-div"
                style={SystemAdministrationStyles.header}
                aria-labelledby="user-welcome-message"
                className="notranslate"
              >
                <h1 id="user-welcome-message" className="green-divider">
                  {LOCALIZE.formatString(
                    LOCALIZE.testAdministration.title,
                    this.props.firstName,
                    this.props.lastName
                  )}
                </h1>
              </div>
              <div>
                <div style={SystemAdministrationStyles.sectionContainerLabelDiv}>
                  <div>
                    <label style={SystemAdministrationStyles.sectionContainerLabel}>
                      {LOCALIZE.testAdministration.containerLabel}
                      <span style={SystemAdministrationStyles.tabStyleBorder}></span>
                    </label>
                  </div>
                </div>
                <section aria-label={LOCALIZE.ariaLabel.sideNavigationSection}>
                  <div style={SystemAdministrationStyles.sectionContainer}>
                    <SideNavigation
                      specs={specs}
                      startIndex={this.props.selectedSideNavItem}
                      loadOnClick={true}
                      displayNextPreviousButton={false}
                      isMain={true}
                      tabContainerStyle={SystemAdministrationStyles.tabContainer}
                      tabContentStyle={SystemAdministrationStyles.tabContent}
                      navStyle={SystemAdministrationStyles.nav}
                      bodyContentCustomStyle={SystemAdministrationStyles.sideNavBodyContent}
                      updateSelectedSideNavItem={this.props.setTAUserSideNavState}
                    />
                  </div>
                </section>
              </div>
            </div>
          </ContentContainer>
        </div>
      </div>
    );
  }
}

export { TestAdministration as UnconnectedTestAdministration };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    firstName: state.user.firstName,
    lastName: state.user.lastName,
    username: state.user.username,
    lastLogin: state.user.lastLogin,
    currentHomePage: state.userPermissions.currentHomePage,
    testOrderNumber: state.testAdministration.testOrderNumber,
    selectedSideNavItem: state.testAdministration.selectedSideNavItem,
    isAae: state.userPermissions.isAae
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      isTokenStillValid,
      getActiveTestAccessCodesRedux,
      getTestPermissions,
      getTestAdministratorAssignedCandidates,
      getUserInformation,
      getOrderlessTestOptions,
      setTAUserSideNavState
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(TestAdministration);
