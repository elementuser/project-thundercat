import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { LANGUAGES } from "../../modules/LocalizeRedux";
import { bindActionCreators } from "redux";
import { styles as ActivePermissionsStyles } from "../etta/permissions/ActivePermissions";
import { alternateColorsStyle } from "../commons/Constants";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMagic, faTimes, faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import {
  deleteTestAccessCode,
  resetTestAdministrationStates,
  getNewTestAccessCode
} from "../../modules/TestAdministrationRedux";
import { getTestPermissionFinancialData } from "../../modules/PermissionsRedux";

import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import SystemMessage, { MESSAGE_TYPE } from "../commons/SystemMessage";
import TestSessionInformationPopup from "./TestSessionInformationPopup";
import CustomButton, { THEME } from "../commons/CustomButton";
import GenericTable from "../commons/GenericTable";

export const styles = {
  mainContainer: {
    width: "100%"
  },
  table: {
    width: "100%",
    borderWidth: 1,
    borderStyle: "solid",
    borderColor: "#CECECE",
    borderTop: "1px solid #00565e"
  },
  tableHead: {
    height: 60,
    backgroundColor: "#00565e",
    color: "white",
    fontWeight: "bold"
  },
  tableLayout: {
    tableLayout: "fixed"
  },
  tableHeadTestAccessCode: {
    paddingLeft: 12
  },
  tableHeadTest: {
    textAlign: "center"
  },
  tableHeadStaffingProcessNumber: {
    textAlign: "center"
  },
  tableHeadAction: {
    textAlign: "center"
  },
  centeredText: {
    textAlign: "center"
  },
  testAccessCodeColumn: {
    paddingLeft: 12,
    wordBreak: "break-word"
  },
  staffingProcessNumberColumn: {
    wordBreak: "break-word"
  },
  staffingProcessNumberLabel: {
    width: "100%",
    wordWrap: "break-word",
    padding: "6px 0"
  },
  buttonLabel: {
    marginLeft: 6
  },
  disableButton: {
    minWidth: 100,
    margin: "6px 0"
  },
  generateCodeButton: {
    background: "transparent",
    padding: 12,
    border: "none",
    color: " #00565e"
  },
  generateCodeButtonIcon: {
    marginRight: 12,
    color: "#00565e"
  },
  boldText: {
    fontWeight: "bold"
  }
};

export const COMMON_STYLE = {
  CENTERED_TEXT: {
    textAlign: "center",
    padding: "12px"
  },
  LEFT_TEXT: {
    textAlign: "left",
    padding: "12px"
  }
};

class TestAccessCodes extends Component {
  static propTypes = {
    activeTestAccessCodes: PropTypes.array.isRequired,
    activeTestAccessCodesLoading: PropTypes.bool,
    getActiveTestAccessCodes: PropTypes.func.isRequired,
    testOrderNumberOptions: PropTypes.array.isRequired,
    populateTestToAdministerOptions: PropTypes.func.isRequired,
    testToAdministerOptions: PropTypes.array.isRequired,
    populateTestSessionLanguageOptions: PropTypes.func.isRequired,
    testSessionLanguageOptions: PropTypes.array.isRequired,
    // provided by redux
    deleteTestAccessCode: PropTypes.func,
    getTestPermissionFinancialData: PropTypes.func,
    resetTestAdministrationStates: PropTypes.func,
    getNewTestAccessCode: PropTypes.func
  };

  state = {
    activeTestAccessCodes: [],
    showGenerateTestAccessCodePopup: false,
    showDisableConfirmationPopup: false,
    testAccessCodeToDisable: "",
    rowsDefinition: {}
  };

  componentDidMount = () => {
    // populating table
    this.props.getActiveTestAccessCodes();
  };

  componentDidUpdate = prevProps => {
    // if activeTestAccessCodes gets updated
    if (prevProps.activeTestAccessCodes !== this.props.activeTestAccessCodes) {
      this.setState({ activeTestAccessCodes: this.props.activeTestAccessCodes }, () => {
        // trigger populate table rows
        this.populateTableRows();
      });
    }
  };

  openGenerateTestAccessCodePopup = () => {
    this.setState({
      showGenerateTestAccessCodePopup: true
    });
  };

  closeGenerateTestAccessCodePopup = id => {
    this.setState({ showGenerateTestAccessCodePopup: false });
  };

  openDisableConfirmationPopup = id => {
    this.setState({
      showDisableConfirmationPopup: true,
      testAccessCodeToDisable: this.props.activeTestAccessCodes[id].test_access_code
    });
  };

  closeDisableConfirmationPopup = () => {
    this.setState({ showDisableConfirmationPopup: false, testAccessCodeToDisable: "" });
  };

  // resetting test administration redux states
  resetInputs = () => {
    this.props.resetTestAdministrationStates();
  };

  // handle disable selected test access code
  handleDisableTestAccessCode = () => {
    this.props.deleteTestAccessCode(this.state.testAccessCodeToDisable).then(response => {
      // if request succeeded
      if (response.status === 200) {
        // re-populating active test access codes table
        this.props.getActiveTestAccessCodes();
        // reseting testAccessCodeToDisable state
        this.setState({ testAccessCodeToDisable: "" });
        // should never happen
      } else {
        throw new Error("Something went wrong during disable test access code process");
      }
    });
  };

  // handling generate new test access code functionality
  handleGenerateTestAccessCode = () => {
    const testSessionLanguageId =
      this.props.testSessionLanguage.value === LOCALIZE.commons.english ? 1 : 2;
    // creating body
    let body = {};
    // orderless option selected
    if (this.props.testOrderNumber.value === 0) {
      body = {
        test_order_number: this.props.testOrderNumber.value,
        test_id: this.props.testToAdminister.value,
        test_session_language: testSessionLanguageId,
        orderless_request: true,
        reference_number: this.props.orderlessFinancialData.referenceNumber,
        department_ministry_id: this.props.orderlessFinancialData.departmentMinistryId,
        fis_organisation_code: this.props.orderlessFinancialData.fisOrganisationCode,
        fis_reference_code: this.props.orderlessFinancialData.fisReferenceCode,
        billing_contact_name: this.props.orderlessFinancialData.billingContactName,
        billing_contact_info: this.props.orderlessFinancialData.billingContactInfo
      };
      // test order number option selected
    } else {
      body = {
        test_order_number: this.props.testOrderNumber.value,
        test_id: this.props.testToAdminister.value,
        test_session_language: testSessionLanguageId,
        orderless_request: false
      };
    }

    // getting new test access code based on selected parameters
    this.props.getNewTestAccessCode(body).then(response => {
      // request successful
      if (response.status === 200) {
        // re-populating active test access codes table
        this.props.getActiveTestAccessCodes();
        // closing popup
        this.closeGenerateTestAccessCodePopup();
        // resetting inputs
        this.resetInputs();
        // should never happen
      } else {
        throw new Error("Something went wrong during generate new test access code process");
      }
    });
  };

  // populating table rows for the GenericTable component
  populateTableRows = () => {
    const { activeTestAccessCodes } = this.state;

    // initializing needed object and array for rowsDefinition state (needed for GenericTable component)
    let rowsDefinition = {};
    const data = [];

    activeTestAccessCodes.map((testAccessCode, id) => {
      return data.push({
        column_1: testAccessCode.test_access_code,
        column_2:
          this.props.currentLanguage === LANGUAGES.english
            ? testAccessCode.en_test_name
            : testAccessCode.fr_test_name,
        column_3: testAccessCode.staffing_process_number,
        column_4: (
          <CustomButton
            label={
              <div>
                <FontAwesomeIcon icon={faTrashAlt} />
                <span style={styles.buttonLabel}>
                  {LOCALIZE.testAdministration.testAccessCodes.table.actionButton}
                </span>
              </div>
            }
            action={() => this.openDisableConfirmationPopup(id)}
            customStyle={styles.disableButton}
            buttonTheme={THEME.SECONDARY}
          />
        )
      });
    });

    // updating rowsDefinition object with provided data and needed style
    rowsDefinition = {
      column_1_style: COMMON_STYLE.LEFT_TEXT,
      column_2_style: COMMON_STYLE.LEFT_TEXT,
      column_3_style: COMMON_STYLE.LEFT_TEXT,
      column_4_style: COMMON_STYLE.CENTERED_TEXT,
      data: data
    };

    this.setState({
      rowsDefinition: rowsDefinition
    });
  };

  // Populate Last Row Content
  populateLastRowHtml = () => {
    return (
      <td
        id="generate-test-access-code-button"
        colSpan="4"
        style={alternateColorsStyle(this.state.activeTestAccessCodes.length, 60)}
      >
        <CustomButton
          label={
            <>
              <span style={styles.generateCodeButtonIcon}>
                <FontAwesomeIcon icon={faMagic} />
              </span>
              <span>{LOCALIZE.testAdministration.testAccessCodes.table.generateNewCode}</span>
            </>
          }
          action={this.openGenerateTestAccessCodePopup}
          customStyle={styles.generateCodeButton}
        />
      </td>
    );
  };

  // Populate Empty Table with Generate Button
  populateEmptyTable = () => {
    return (
      <CustomButton
        label={
          <>
            <span style={styles.generateCodeButtonIcon}>
              <FontAwesomeIcon icon={faMagic} />
            </span>
            <span>{LOCALIZE.testAdministration.testAccessCodes.table.generateNewCode}</span>
          </>
        }
        action={this.openGenerateTestAccessCodePopup}
        customStyle={styles.generateCodeButton}
      />
    );
  };

  render() {
    // setting the popup overflow visible variable
    let popupOverflowVisible = true;

    // GenericTable Columns
    const columnsDefinition = [
      {
        label: LOCALIZE.testAdministration.testAccessCodes.table.testAccessCode,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label: LOCALIZE.testAdministration.testAccessCodes.table.test,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label: LOCALIZE.testAdministration.testAccessCodes.table.staffingProcessNumber,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label: LOCALIZE.testAdministration.testAccessCodes.table.action,
        style: COMMON_STYLE.CENTERED_TEXT
      }
    ];

    // if spacing is enabled
    if (this.props.accommodations.spacing) {
      popupOverflowVisible = false;
      // if font size is greater than 40px
    } else if (parseInt(this.props.accommodations.fontSize.split("px")[0]) > 40) {
      popupOverflowVisible = false;
      // if test order number, test to administer and test session language are defined/selected
    } else if (
      this.props.testOrderNumber !== "" &&
      this.props.testToAdminister !== "" &&
      this.props.testSessionLanguage !== ""
    ) {
      popupOverflowVisible = false;
      // orderless option selected
    } else if (
      typeof this.props.testOrderNumber !== "undefined" &&
      this.props.testOrderNumber.value === 0
    ) {
      popupOverflowVisible = false;
    }

    return (
      <div style={styles.mainContainer}>
        <div>
          <h2>{LOCALIZE.testAdministration.sideNavItems.testAccessCodes}</h2>
          <p>{LOCALIZE.testAdministration.testAccessCodes.description}</p>
        </div>
        <div>
          <GenericTable
            classnamePrefix="test-access-codes"
            columnsDefinition={columnsDefinition}
            rowsDefinition={this.state.rowsDefinition}
            currentlyLoading={this.props.activeTestAccessCodesLoading}
            emptyTableMessage={this.populateEmptyTable()}
            hasLastRowHtml={
              this.state.activeTestAccessCodes.length > 0 &&
              !this.props.activeTestAccessCodesLoading
            }
            lastRowHtml={this.populateLastRowHtml()}
          />
        </div>
        <PopupBox
          show={this.state.showGenerateTestAccessCodePopup}
          handleClose={() => {}}
          isBackdropStatic={true}
          shouldCloseOnEsc={false}
          onPopupClose={this.resetInputs}
          overflowVisible={popupOverflowVisible}
          title={LOCALIZE.testAdministration.testAccessCodes.generateTestAccessCodePopup.title}
          description={
            <TestSessionInformationPopup
              testOrderNumberOptions={this.props.testOrderNumberOptions}
              populateTestToAdministerOptions={this.props.populateTestToAdministerOptions}
              testToAdministerOptions={this.props.testToAdministerOptions}
              populateTestSessionLanguageOptions={this.props.populateTestSessionLanguageOptions}
              testSessionLanguageOptions={this.props.testSessionLanguageOptions}
            />
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeGenerateTestAccessCodePopup}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={
            LOCALIZE.testAdministration.testAccessCodes.generateTestAccessCodePopup.generateButton
          }
          rightButtonIcon={faMagic}
          rightButtonState={this.props.generateButtonDisabled}
          rightButtonAction={this.handleGenerateTestAccessCode}
          customModalStyle={{ width: "100%" }}
        />
        <PopupBox
          show={this.state.showDisableConfirmationPopup}
          handleClose={this.closeDisableConfirmationPopup}
          isBackdropStatic={false}
          shouldCloseOnEsc={true}
          size={"lg"}
          title={
            LOCALIZE.testAdministration.testAccessCodes.disableTestAccessCodeConfirmationPopup.title
          }
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.error}
                title={LOCALIZE.commons.warning}
                message={
                  <p>
                    {LOCALIZE.formatString(
                      LOCALIZE.testAdministration.testAccessCodes
                        .disableTestAccessCodeConfirmationPopup.description,
                      <span style={ActivePermissionsStyles.boldText}>
                        {this.state.testAccessCodeToDisable}
                      </span>
                    )}
                  </p>
                }
              />
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeDisableConfirmationPopup}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={LOCALIZE.testAdministration.testAccessCodes.table.actionButton}
          rightButtonIcon={faTrashAlt}
          rightButtonIconCustomStyle={styles.customPopupButtonStyle}
          rightButtonAction={this.handleDisableTestAccessCode}
        />
      </div>
    );
  }
}

export { TestAccessCodes as unconnectedTestAccessCodes };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    username: state.user.username,
    generateButtonDisabled: state.testAdministration.generateButtonDisabled,
    testOrderNumber: state.testAdministration.testOrderNumber,
    testSessionLanguage: state.testAdministration.testSessionLanguage,
    testToAdminister: state.testAdministration.testToAdminister,
    orderlessFinancialData: state.testAdministration.orderlessFinancialData,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      deleteTestAccessCode,
      getTestPermissionFinancialData,
      resetTestAdministrationStates,
      getNewTestAccessCode
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(TestAccessCodes);
