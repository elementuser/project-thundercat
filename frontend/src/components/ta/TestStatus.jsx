import LOCALIZE from "../../text_resources";
import TEST_STATUS from "./Constants";

// getting formatted test statuses
function getFormattedTestStatus(statusId) {
  // see 'assigned_test_status.py' for references
  if (statusId === TEST_STATUS.ASSIGNED) {
    return LOCALIZE.commons.status.checkedIn;
  }
  if (statusId === TEST_STATUS.READY) {
    return LOCALIZE.commons.status.ready;
  }
  if (statusId === TEST_STATUS.PRE_TEST) {
    return LOCALIZE.commons.status.preTest;
  }
  if (statusId === TEST_STATUS.ACTIVE) {
    return LOCALIZE.commons.status.active;
  }
  if (statusId === TEST_STATUS.LOCKED) {
    return LOCALIZE.commons.status.locked;
  }
  if (statusId === TEST_STATUS.PAUSED) {
    return LOCALIZE.commons.status.paused;
  }
  if (statusId === TEST_STATUS.NEVER_STARTED) {
    return LOCALIZE.commons.status.neverStarted;
  }
  if (statusId === TEST_STATUS.INACTIVITY) {
    return LOCALIZE.commons.status.inactivity;
  }
  if (statusId === TEST_STATUS.TIMED_OUT) {
    return LOCALIZE.commons.status.timedOut;
  }
  if (statusId === TEST_STATUS.QUIT) {
    return LOCALIZE.commons.status.quit;
  }
  if (statusId === TEST_STATUS.SUBMITTED) {
    return LOCALIZE.commons.status.submitted;
  }
  if (statusId === TEST_STATUS.UNASSIGNED) {
    return LOCALIZE.commons.status.unassigned;
  }
  // should never happen
  return "non defined status";
}

export default getFormattedTestStatus;
