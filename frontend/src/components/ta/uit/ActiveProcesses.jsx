/* eslint-disable no-nested-ternary */
import React, { Component } from "react";
import PropTypes from "prop-types";
import LOCALIZE from "../../../text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import GenericTable, { COMMON_STYLE } from "../../commons/GenericTable";
import {
  getAllActiveUITProcessesForTA,
  getSelectedUITActiveProcessesDetails,
  deactivateSingleUitTest,
  triggerTablesRerender,
  deactivateUitProcess,
  updateSelectedUITActiveProcessValidityEndDate,
  updateUITActiveProcessesPaginationPage,
  updateUITActiveProcessesPaginationPageSize,
  updateSearchUitActiveProcessesStates,
  getFoundActiveUITProcessesForTA,
  resetUitStates
} from "../../../modules/UitRedux";
import StyledTooltip, { TYPE, EFFECT, TRIGGER_TYPE } from "../../authentication/StyledTooltip";
import CustomButton, { THEME } from "../../commons/CustomButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faTrashAlt,
  faBinoculars,
  faTimes,
  faSpinner,
  faCalendarAlt,
  faSave
} from "@fortawesome/free-solid-svg-icons";
import PopupBox, { BUTTON_STATE, BUTTON_TYPE } from "../../commons/PopupBox";
import { UIT_TEST_STATUS, getUitTestStatus } from "../Constants";
import getFormattedDate from "../../../helpers/getFormattedDate";
import SystemMessage, { MESSAGE_TYPE } from "../../commons/SystemMessage";
import DropdownSelect from "../../commons/DropdownSelect";
import DatePicker from "../../commons/DatePicker";
import populateCustomFutureYearsDateOptions from "../../../helpers/populateCustomDatePickerOptions";
import SearchBarWithDisplayOptions from "../../commons/SearchBarWithDisplayOptions";
import Pagination from "../../commons/Pagination";
import { Row, Col } from "react-bootstrap";
import { LANGUAGES } from "../../commons/Translation";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 6,
    xl: 6
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 6,
    xl: 6
  }
};

const styles = {
  allUnset: {
    all: "unset"
  },
  tooltipIcon: {
    width: "100%",
    minHeight: 20,
    textAlign: "center"
  },
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  actionButtonPopup: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  popupFinancialDataContainer: {
    border: "1px solid #CECECE",
    backgroundColor: "#F3F3F3",
    padding: 12
  },
  labelContainer: {
    display: "table-cell",
    verticalAlign: "middle"
  },
  datePickerContainer: {
    display: "table",
    width: "100%",
    padding: "24px 12px 12px 12px"
  },
  reasonForModificationContainer: {
    display: "table",
    width: "93%",
    padding: "12px 12px 24px 12px"
  },
  updateProcessDatePickerLabelContainer: {
    display: "table-cell",
    verticalAlign: "middle",
    width: "35%",
    paddingRight: 12
  },
  updateProcessDatePickerContainer: {
    width: "65%",
    display: "table-cell",
    verticalAlign: "middle"
  },
  updateProcessReasonForModificationContainer: {
    display: "table-cell",
    verticalAlign: "middle"
  },
  updateProcessReasonForModifLabelContainer: {
    display: "table-cell",
    verticalAlign: "middle",
    width: "37.5%",
    paddingRight: 12
  },
  popupLabel: {
    fontWeight: "bold",
    paddingRight: 12
  },
  popupTableContainer: {
    margin: "24px 0"
  },
  popupTableLabel: {
    fontWeight: "bold",
    paddingBottom: 24,
    paddingTop: 24
  },
  bold: {
    fontWeight: "bold"
  },
  dropdownContainer: {
    width: "100%",
    margin: "18px 0 8px 0"
  },
  dropdownLabel: {
    paddingRight: 12
  }
};
class ActiveProcesses extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  validityEndDateRef = React.createRef();

  static propTypes = {
    getAllActiveUITProcessesForTA: PropTypes.func,
    getSelectedUITActiveProcessesDetails: PropTypes.func,
    updateUITActiveProcessesPaginationPage: PropTypes.func,
    updateUITActiveProcessesPaginationPageSize: PropTypes.func,
    updateSearchUITActiveProcessesStates: PropTypes.func
  };

  state = {
    rowsDefinition: {},
    currentlyLoading: false,
    allActiveProcesses: [],
    viewSelectedProcess: {},
    viewSelectedProcessPopup: false,
    showUpdateSelectedProcessExpiryDatePopup: false,
    updateSelectedProcessExpiryDate: {},
    deleteSelectedProcess: {},
    showDeleteSelectedProcessPopup: false,
    deleteProcessCurrentlyLoading: false,
    currentlyLoadingForPopUp: false,
    rowsDefinitionPopup: {},
    selectedActiveProcesses: [],
    showDeleteSingleUitTestPopup: false,
    deleteSingleUitTestApiBody: {},
    deleteSingleUitTestApiIndex: null,
    deleteSingleUitTestCurrentlyLoading: false,
    showUitTestAlreadyStartedPopupContent: false,
    uitTestAlreadyStartedErrorMessage: "",
    deleteSingleUitSelectedReasonOptions: [],
    deleteWholeUitProcessSelectedReasonOptions: [],
    deleteUitSelectedReason: {},
    deleteWholeUitProcessSuccessfullyDone: false,
    reasonsForModificationOptions: [],
    updateUitValidityEndDateCurrentlyLoading: false,
    updateUitValidityEndDateSuccssfullyDone: false,
    uitSelectedReasonForModification: {},
    triggerDateValidation: false,
    triggerResetFieldValues: false,
    nextPageNumber: 0,
    previousPageNumber: 0,
    numberOfPages: 1,
    resultsFound: 0
  };

  componentDidMount = () => {
    // resetting redux states
    this.props.resetUitStates();
    // initialize pagination page redux state
    this.props.updateUITActiveProcessesPaginationPage(1);
    this.populateActiveProcesses();
  };

  componentDidUpdate = (prevProps, prevState) => {
    // if currentLanguage gets updated
    if (prevProps.currentLanguage !== this.props.currentLanguage) {
      this.populateUitActiveProcessesBasedOnActiveSearch();
    }
    // if uitActiveProcessesPaginationPage gets updated
    if (
      prevProps.uitActiveProcessesPaginationPage !== this.props.uitActiveProcessesPaginationPage
    ) {
      this.populateUitActiveProcessesBasedOnActiveSearch();
    }
    // if uitActiveProcessesPaginationPageSize get updated
    if (
      prevProps.uitActiveProcessesPaginationPageSize !==
      this.props.uitActiveProcessesPaginationPageSize
    ) {
      this.populateUitActiveProcessesBasedOnActiveSearch();
    }
    // if search uitActiveProcessesKeyword gets updated
    if (prevProps.uitActiveProcessesKeyword !== this.props.uitActiveProcessesKeyword) {
      // if uitActiveProcessesKeyword redux state is empty
      if (this.props.uitActiveProcessesKeyword !== "") {
        this.populateUitActiveProcessesBasedOnActiveSearch();
      } else if (
        this.props.uitActiveProcessesKeyword === "" &&
        this.props.uitActiveProcessesActiveSearch
      ) {
        this.populateUitActiveProcessesBasedOnActiveSearch();
      }
    }
    // if uitActiveProcessesActiveSearch gets updated
    if (prevProps.uitActiveProcessesActiveSearch !== this.props.uitActiveProcessesActiveSearch) {
      this.populateUitActiveProcessesBasedOnActiveSearch();
    }
    // if triggerUitTablesRerender gets updated
    if (prevProps.triggerUitTablesRerender !== this.props.triggerUitTablesRerender) {
      // populating table
      this.populateUitActiveProcessesBasedOnActiveSearch();
    }
    // if triggerDateValidation state gets updated
    if (prevState.triggerDateValidation !== this.state.triggerDateValidation) {
      // making sure that the update selected process expiry date popup is opened
      if (this.state.showUpdateSelectedProcessExpiryDatePopup) {
        // validate update process popup content
        this.validateAndUpdateActiveProcessValidityEndDate();
      }
    }
  };

  componentWillUnmount = () => {
    this._isMounted = false;
  };

  populateActiveProcesses = () => {
    this._isMounted = true;
    // initializing needed object and array for rowsDefinition props (needed for GenericTable component)
    let rowsDefinition = {};
    const data = [];
    const allActiveProcessesArray = [];
    this.setState({ currentlyLoading: true }, () => {
      this.props
        .getAllActiveUITProcessesForTA(
          this.props.uitActiveProcessesPaginationPage,
          this.props.uitActiveProcessesPaginationPageSize
        )
        .then(response => {
          if (this._isMounted) {
            for (let i = 0; i < response.results.length; i++) {
              allActiveProcessesArray.push({
                id: response.results[i].id,
                test_permission_data: response.results[i].test_permission_data,
                total_candidates: response.results[i].total_candidates,
                total_tests_taken: response.results[i].total_tests_taken,
                invite_date: response.results[i].invite_date,
                validity_end_date: response.results[i].validity_end_date
              });
              // pushing needed data in data array (needed for GenericTable component)
              data.push({
                column_1: response.results[i].test_permission_data.staffing_process_number,
                column_2: response.results[i].test_permission_data.test.test_code,
                column_3: `${response.results[i].total_tests_taken}/${response.results[i].total_candidates}`,
                column_4:
                  this.props.currentLanguage === LANGUAGES.english
                    ? `${response.results[i].test_permission_data.department_ministry_data.edesc} (${response.results[i].test_permission_data.department_ministry_data.eabrv})`
                    : `${response.results[i].test_permission_data.department_ministry_data.fdesc} (${response.results[i].test_permission_data.department_ministry_data.fabrv})`,
                column_5: response.results[i].invite_date,
                column_6: response.results[i].validity_end_date,
                column_7: this.populateColumnSix(i, response.results[i])
              });

              // updating rowsDefinition object with provided data and needed style
              rowsDefinition = {
                column_1_style: COMMON_STYLE.LEFT_TEXT,
                column_2_style: COMMON_STYLE.LEFT_TEXT,
                column_3_style: COMMON_STYLE.CENTERED_TEXT,
                column_4_style: COMMON_STYLE.CENTERED_TEXT,
                column_5_style: COMMON_STYLE.LEFT_TEXT,
                column_6_style: COMMON_STYLE.LEFT_TEXT,
                column_7_style: COMMON_STYLE.CENTERED_TEXT,
                data: data
              };
            }
            // populating reasons for deletion (single candidate) options
            const deleteSingleUitSelectedReasonOptions = [];
            for (let i = 0; i < response.reasons_for_deletion_single_candidate.length; i++) {
              deleteSingleUitSelectedReasonOptions.push({
                value: response.reasons_for_deletion_single_candidate[i].id,
                label:
                  response.reasons_for_deletion_single_candidate[i][
                    `${this.props.currentLanguage}_name`
                  ]
              });
            }
            // populating reasons for deletion (whole process) options
            const deleteWholeUitProcessSelectedReasonOptions = [];
            for (let i = 0; i < response.reasons_for_deletion_whole_process.length; i++) {
              deleteWholeUitProcessSelectedReasonOptions.push({
                value: response.reasons_for_deletion_whole_process[i].id,
                label:
                  response.reasons_for_deletion_whole_process[i][
                    `${this.props.currentLanguage}_name`
                  ]
              });
            }
            // populating reasons for modification options
            const reasonsForModificationOptions = [];
            for (let i = 0; i < response.reasons_for_modification.length; i++) {
              reasonsForModificationOptions.push({
                value: response.reasons_for_modification[i].id,
                label: response.reasons_for_modification[i][`${this.props.currentLanguage}_name`]
              });
            }
            // saving results in state
            this.setState({
              rowsDefinition: rowsDefinition,
              allActiveProcesses: allActiveProcessesArray,
              deleteSingleUitSelectedReasonOptions: deleteSingleUitSelectedReasonOptions,
              deleteWholeUitProcessSelectedReasonOptions:
                deleteWholeUitProcessSelectedReasonOptions,
              reasonsForModificationOptions: reasonsForModificationOptions,
              nextPageNumber: response.next_page_number,
              previousPageNumber: response.previous_page_number,
              numberOfPages: Math.ceil(
                response.count / this.props.uitActiveProcessesPaginationPageSize
              )
            });
          }
        })
        .then(() => {
          if (this._isMounted) {
            this.setState({ currentlyLoading: false });
          }
        });
    });
  };

  populateColumnSix = (i, response) => {
    return (
      <div className="d-flex justify-content-center flex-nowrap">
        <StyledTooltip
          id={`view-selected-active-processes-${i}`}
          place="top"
          type={TYPE.light}
          effect={EFFECT.solid}
          triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
          tooltipElement={
            <div style={styles.allUnset}>
              <CustomButton
                dataTip=""
                dataFor={`view-selected-active-processes-${i}`}
                label={<FontAwesomeIcon icon={faBinoculars} style={styles.tooltipIcon} />}
                action={() => {
                  this.viewSelectedProcessPopup(i);
                }}
                customStyle={styles.actionButton}
                buttonTheme={THEME.SECONDARY}
                ariaLabel={LOCALIZE.formatString(
                  LOCALIZE.testAdministration.uit.tabs.activeProcessess.table
                    .viewButtonAccessibility,
                  response.test_permission_data.staffing_process_number
                )}
              />
            </div>
          }
          tooltipContent={
            <div>
              <p>
                {
                  LOCALIZE.testAdministration.uit.tabs.activeProcessess.table
                    .viewSelectedProcessTooltip
                }
              </p>
            </div>
          }
        />
        <StyledTooltip
          id={`update-selected-active-process-expiry-date-${i}`}
          place="top"
          type={TYPE.light}
          effect={EFFECT.solid}
          triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
          tooltipElement={
            <div style={styles.allUnset}>
              <CustomButton
                dataTip=""
                dataFor={`update-selected-active-process-expiry-date-${i}`}
                label={<FontAwesomeIcon icon={faCalendarAlt} style={styles.tooltipIcon} />}
                action={() => {
                  this.updateSelectedProcessExpiryDatePopup(i);
                }}
                customStyle={styles.actionButton}
                buttonTheme={THEME.SECONDARY}
                ariaLabel={LOCALIZE.formatString(
                  LOCALIZE.testAdministration.uit.tabs.activeProcessess.table
                    .updateSelectedProcessExpiryDateAccessibility,
                  response.test_permission_data.staffing_process_number
                )}
              />
            </div>
          }
          tooltipContent={
            <div>
              <p>
                {
                  LOCALIZE.testAdministration.uit.tabs.activeProcessess.table
                    .updateSelectedProcessExpiryDate
                }
              </p>
            </div>
          }
        />
        <StyledTooltip
          id={`delete-selected-processes-${i}`}
          place="top"
          type={TYPE.light}
          effect={EFFECT.solid}
          triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
          tooltipElement={
            <div style={styles.allUnset}>
              <CustomButton
                dataTip=""
                dataFor={`delete-selected-processes-${i}`}
                label={<FontAwesomeIcon icon={faTrashAlt} style={styles.tooltipIcon} />}
                action={() => {
                  this.deleteSelectedProcessPopup(i);
                }}
                buttonTheme={THEME.DANGER}
                customStyle={styles.actionButton}
                ariaLabel={LOCALIZE.formatString(
                  LOCALIZE.testAdministration.uit.tabs.activeProcessess.table
                    .deleteButtonAccessibility,
                  response.test_permission_data.staffing_process_number
                )}
              />
            </div>
          }
          tooltipContent={
            <div>
              <p>
                {
                  LOCALIZE.testAdministration.uit.tabs.activeProcessess.table
                    .deleteSelectedProcessTooltip
                }
              </p>
            </div>
          }
        />
      </div>
    );
  };

  populateInvitedCandidatesTable = () => {
    let rowsDefinition = {};
    const data = [];
    const selectedActiveProcessesArray = [];
    this.setState({ currentlyLoadingForPopUp: true }, () => {
      this.props
        .getSelectedUITActiveProcessesDetails(this.state.viewSelectedProcess.id)
        .then(response => {
          for (let i = 0; i < response.candidates_data.length; i++) {
            selectedActiveProcessesArray.push({
              candidate_email: response.candidates_data[i].email,
              test_status: response.candidates_data[i].test_status
            });
            // pushing needed data in data array (needed for GenericTable component)
            data.push({
              column_1: response.candidates_data[i].email,
              column_2: getUitTestStatus(
                response.candidates_data[i].test_status,
                response.candidates_data[i].code_deactivated
              ),
              column_3: (
                <StyledTooltip
                  id={`delete-selected-candidate-${i}`}
                  place="top"
                  type={TYPE.light}
                  effect={EFFECT.solid}
                  triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
                  tooltipElement={
                    <div style={styles.allUnset}>
                      <CustomButton
                        dataTip=""
                        dataFor={`delete-selected-candidate-${i}`}
                        label={<FontAwesomeIcon icon={faTrashAlt} style={styles.tooltipIcon} />}
                        action={() => this.openDeleteSingelUitTestPopup(i)}
                        buttonTheme={THEME.DANGER}
                        disabled={
                          response.candidates_data[i].test_status !== UIT_TEST_STATUS.NOT_TAKEN
                            ? true
                            : !!response.candidates_data[i].code_deactivated
                        }
                        customStyle={styles.actionButtonPopup}
                        ariaLabel={LOCALIZE.formatString(
                          LOCALIZE.testAdministration.uit.tabs.activeProcessess.selectedProcessPopup
                            .deleteCandidateAccessibility,
                          response.candidates_data[i].email
                        )}
                      />
                    </div>
                  }
                  tooltipContent={
                    <div>
                      <p>
                        {
                          LOCALIZE.testAdministration.uit.tabs.activeProcessess.selectedProcessPopup
                            .deleteCandidateTooltip
                        }
                      </p>
                    </div>
                  }
                />
              )
            });

            // updating rowsDefinition object with provided data and needed style
            rowsDefinition = {
              column_1_style: COMMON_STYLE.LEFT_TEXT,
              column_2_style: COMMON_STYLE.LEFT_TEXT,
              column_3_style: COMMON_STYLE.CENTERED_TEXT,
              data: data
            };

            // saving results in state
            this.setState({
              rowsDefinitionPopup: rowsDefinition,
              selectedActiveProcesses: selectedActiveProcessesArray
            });
          }
        })
        .then(() => {
          this.setState({ currentlyLoadingForPopUp: false });
        });
    });
  };

  updateInvitedCandidatesTable = newTestStatus => {
    // initializing needed variables
    const newSelectedActiveProcesses = this.state.selectedActiveProcesses;
    const newRowsDefinitionPopup = this.state.rowsDefinitionPopup;
    // updating test status of selected candidate (selectedActiveProcesses and rowsDefinitionPopup states)
    newSelectedActiveProcesses[this.state.deleteSingleUitTestApiIndex].test_status = newTestStatus;
    newRowsDefinitionPopup.data[this.state.deleteSingleUitTestApiIndex].column_2 =
      newTestStatus === UIT_TEST_STATUS.UNASSIGNED
        ? LOCALIZE.testAdministration.uit.tabs.activeProcessess.selectedProcessPopup
            .testCodeDeactivated
        : LOCALIZE.testAdministration.uit.tabs.activeProcessess.selectedProcessPopup.testInProgress;
    newRowsDefinitionPopup.data[this.state.deleteSingleUitTestApiIndex].column_3 = (
      <StyledTooltip
        id={`delete-selected-candidate-${this.state.deleteSingleUitTestApiIndex}`}
        place="top"
        type={TYPE.light}
        effect={EFFECT.solid}
        triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
        tooltipElement={
          <div style={styles.allUnset}>
            <CustomButton
              dataTip=""
              dataFor={`delete-selected-candidate-${this.state.deleteSingleUitTestApiIndex}`}
              label={<FontAwesomeIcon icon={faTrashAlt} style={styles.tooltipIcon} />}
              action={() => {}}
              buttonTheme={THEME.DANGER}
              disabled={true}
              customStyle={styles.actionButtonPopup}
              ariaLabel={LOCALIZE.formatString(
                LOCALIZE.testAdministration.uit.tabs.activeProcessess.selectedProcessPopup
                  .deleteCandidateAccessibility,
                this.state.rowsDefinitionPopup.data[this.state.deleteSingleUitTestApiIndex].column_1
              )}
            />
          </div>
        }
        tooltipContent={
          <div>
            <p>
              {
                LOCALIZE.testAdministration.uit.tabs.activeProcessess.selectedProcessPopup
                  .deleteCandidateTooltip
              }
            </p>
          </div>
        }
      />
    );
    this.setState({
      selectedActiveProcesses: newSelectedActiveProcesses,
      rowsDefinitionPopup: newRowsDefinitionPopup
    });
  };

  // to view the details of the selected process
  viewSelectedProcessPopup = id => {
    this.setState({
      viewSelectedProcessPopup: true,
      viewSelectedProcess: this.state.allActiveProcesses[id]
    });
    this.populateInvitedCandidatesTable();
  };

  // to view the details of the selected process
  updateSelectedProcessExpiryDatePopup = id => {
    this.setState({
      showUpdateSelectedProcessExpiryDatePopup: true,
      updateSelectedProcessExpiryDate: this.state.allActiveProcesses[id],
      updateUitValidityEndDateSuccssfullyDone: false
    });
  };

  closeUpdateSelectedProcessExpiryDatePopup = () => {
    this.setState({
      showUpdateSelectedProcessExpiryDatePopup: false,
      updateSelectedProcessExpiryDate: {},
      triggerResetFieldValues: !this.state.triggerResetFieldValues,
      uitSelectedReasonForModification: {},
      updateUitValidityEndDateSuccssfullyDone: false
    });
  };

  handleUpdateSelectedProcessExpiryDate = () => {
    this.setState({
      triggerDateValidation: !this.state.triggerDateValidation
    });
  };

  validateAndUpdateActiveProcessValidityEndDate = () => {
    this.setState({ updateUitValidityEndDateCurrentlyLoading: true }, () => {
      // if selected date is valid
      if (this.props.completeDateValidState) {
        const body = {
          id: this.state.updateSelectedProcessExpiryDate.id,
          validity_end_date: this.props.completeDatePicked,
          reason_for_modification_id: this.state.uitSelectedReasonForModification.value
        };
        this.props.updateSelectedUITActiveProcessValidityEndDate(body).then(response => {
          if (response.ok) {
            this.setState(
              {
                updateUitValidityEndDateCurrentlyLoading: false,
                updateUitValidityEndDateSuccssfullyDone: true
              },
              () => {
                // triggering active/completed processes tables rerender
                this.props.triggerTablesRerender();
              }
            );
            // should never happen
          } else {
            throw new Error(
              "Something went wrong during the UIT active process validity end date update process"
            );
          }
        });
      } else {
        this.setState(
          {
            updateUitValidityEndDateCurrentlyLoading: false,
            updateUitValidityEndDateSuccssfullyDone: false
          },
          () => {
            // focusing on validity end date day field
            this.validityEndDateRef.current.focus();
          }
        );
      }
    });
  };

  // to close the pop up
  closeSelectedProcessPopup = () => {
    this.setState(
      {
        viewSelectedProcessPopup: false,
        viewSelectedProcess: {},
        showDeleteSingleUitTestPopup: false,
        showUitTestAlreadyStartedPopupContent: false,
        deleteSingleUitTestApiBody: {},
        deleteSingleUitTestApiIndex: null,
        uitTestAlreadyStartedErrorMessage: ""
      },
      () => {
        // triggering active/completed processes tables rerender
        this.props.triggerTablesRerender();
      }
    );
  };

  // to delete the selected process
  deleteSelectedProcessPopup = id => {
    this.setState({
      showDeleteSelectedProcessPopup: true,
      deleteSelectedProcess: this.state.allActiveProcesses[id],
      deleteWholeUitProcessSuccessfullyDone: false
    });
  };

  // close delete the selected process pop up
  closeDeleteSelectedProcessPopup = () => {
    this.setState({
      showDeleteSelectedProcessPopup: false,
      deleteSelectedProcess: {},
      deleteUitSelectedReason: {},
      deleteSingleUitTestApiIndex: null,
      deleteWholeUitProcessSuccessfullyDone: false
    });
  };

  handleDeleteSelectedProcess = () => {
    this.setState({ deleteProcessCurrentlyLoading: true }, () => {
      const body = {
        uit_invite_id: this.state.deleteSelectedProcess.id,
        reason_for_deletion: this.state.deleteUitSelectedReason.value
      };
      this.props.deactivateUitProcess(body).then(response => {
        if (response.ok) {
          // closing popup and reinitializing deleteSingleUitTestApiBody and deleteSingleUitTestApiIndex
          this.setState(
            {
              deleteProcessCurrentlyLoading: false,
              deleteWholeUitProcessSuccessfullyDone: true
            },
            () => {
              // triggering active/completed processes tables rerender
              this.props.triggerTablesRerender();
            }
          );
          // should never happen
        } else {
          throw new Error("Something went wrong during the delete process action");
        }
      });
    });
  };

  openDeleteSingelUitTestPopup = i => {
    // getting needed data for the delete single UIT test action
    const deleteSingleUitTestApiBody = {
      uit_invite_id: this.state.viewSelectedProcess.id,
      invited_candidate_email: this.state.selectedActiveProcesses[i].candidate_email
    };
    this.setState({
      showDeleteSingleUitTestPopup: true,
      deleteSingleUitTestApiBody: deleteSingleUitTestApiBody,
      deleteSingleUitTestApiIndex: i
    });
  };

  closeDeleteSingelUitTestPopup = () => {
    this.setState({ showDeleteSingleUitTestPopup: false, deleteUitSelectedReason: {} });
  };

  handleDeactivateSingleUitTest = () => {
    const deleteSingleUitTestApiBody = { ...this.state.deleteSingleUitTestApiBody };
    deleteSingleUitTestApiBody.reason_for_deletion = this.state.deleteUitSelectedReason.value;
    this.setState(
      {
        deleteSingleUitTestCurrentlyLoading: true,
        deleteSingleUitTestApiBody: deleteSingleUitTestApiBody
      },
      () => {
        this.props.deactivateSingleUitTest(this.state.deleteSingleUitTestApiBody).then(response => {
          // successfully deleted/deactivated
          if (response.ok) {
            // refreshing popup table
            this.updateInvitedCandidatesTable(UIT_TEST_STATUS.UNASSIGNED);
            // triggering active/completed processes tables rerender
            this.props.triggerTablesRerender();
            // closing popup and reinitializing deleteSingleUitTestApiBody and deleteSingleUitTestApiIndex
            this.setState({
              showDeleteSingleUitTestPopup: false,
              deleteSingleUitTestApiBody: {},
              deleteSingleUitTestApiIndex: null,
              deleteSingleUitTestCurrentlyLoading: false,
              deleteUitSelectedReason: {}
            });
            // test already started (or other error - unlikely to happen)
          } else {
            // refreshing popup table
            this.updateInvitedCandidatesTable(UIT_TEST_STATUS.IN_PROGRESS);
            // triggering active/completed processes tables rerender
            this.props.triggerTablesRerender();
            // opening already started test popup
            this.setState({
              showUitTestAlreadyStartedPopupContent: true,
              uitTestAlreadyStartedErrorMessage: response.error[`${this.props.currentLanguage}`],
              deleteSingleUitTestCurrentlyLoading: false
            });
          }
        });
      }
    );
  };

  closeUitTestAlreadyStartedPopup = () => {
    this.setState({
      showDeleteSingleUitTestPopup: false,
      deleteSingleUitTestApiBody: {},
      deleteSingleUitTestApiIndex: null,
      showUitTestAlreadyStartedPopupContent: false,
      uitTestAlreadyStartedErrorMessage: "",
      deleteUitSelectedReason: {}
    });
  };

  getSelectedReasonForDeletion = selectedOption => {
    this.setState({ deleteUitSelectedReason: selectedOption });
  };

  getSelectedReasonForModification = selectedOption => {
    this.setState({ uitSelectedReasonForModification: selectedOption });
  };

  populateUitActiveProcessesBasedOnActiveSearch = () => {
    // current search
    if (this.props.uitActiveProcessesActiveSearch) {
      this.populateFoundActiveProcesses();
      // no search
    } else {
      this.populateActiveProcesses();
    }
  };

  populateActiveProcessesObject = (allActiveProcessesArray, response) => {
    // initializing needed object and array for rowsDefinition props (needed for GenericTable component)
    let rowsDefinition = {};
    const data = [];
    // making sure that there are some results
    if (typeof response.results !== "undefined") {
      // looping in response given
      for (let i = 0; i < response.results.length; i++) {
        const currentResult = response.results[i];
        // pushing needed data in allActiveProcessesArray
        allActiveProcessesArray.push({
          id: currentResult.id,
          process_number: currentResult.test_permission_data.staffing_process_number,
          test: currentResult.test_permission_data.test.test_code,
          total_candidates: currentResult.total_candidates,
          total_tests_taken: currentResult.total_tests_taken,
          validity_end_date: currentResult.validity_end_date,
          test_permission_data: currentResult.test_permission_data
        });
        // pushing needed data in data array (needed for GenericTable component)
        data.push({
          column_1: currentResult.test_permission_data.staffing_process_number,
          column_2: currentResult.test_permission_data.test.test_code,
          column_3: `${currentResult.total_tests_taken}/${currentResult.total_candidates}`,
          column_4:
            this.props.currentLanguage === LANGUAGES.english
              ? `${currentResult.test_permission_data.department_ministry_data.edesc} (${currentResult.test_permission_data.department_ministry_data.eabrv})`
              : `${currentResult.test_permission_data.department_ministry_data.fdesc} (${currentResult.test_permission_data.department_ministry_data.fabrv})`,
          column_5: currentResult.invite_date,
          column_6: currentResult.validity_end_date,
          column_7: this.populateColumnSix(i, response.results[i])
        });
      }

      // updating rowsDefinition object with provided data and needed style
      rowsDefinition = {
        column_1_style: COMMON_STYLE.LEFT_TEXT,
        column_2_style: COMMON_STYLE.CENTERED_TEXT,
        column_3_style: COMMON_STYLE.CENTERED_TEXT,
        column_4_style: COMMON_STYLE.CENTERED_TEXT,
        column_5_style: COMMON_STYLE.CENTERED_TEXT,
        column_6_style: COMMON_STYLE.CENTERED_TEXT,
        column_7_style: COMMON_STYLE.CENTERED_TEXT,
        data: data
      };

      // saving results in state
      this.setState({
        rowsDefinition: rowsDefinition,
        allActiveProcesses: allActiveProcessesArray,
        nextPageNumber: response.next_page_number,
        previousPageNumber: response.previous_page_number
      });
    }
    this.setState({
      numberOfPages: Math.ceil(response.count / this.props.uitActiveProcessesPaginationPageSize)
    });
  };

  populateFoundActiveProcesses = () => {
    this.setState({ currentlyLoading: true }, () => {
      // adding small delay to avoi calling that API multiple times
      setTimeout(() => {
        // if callingGetFoundActiveProcessesUIT is set to false
        if (!this.state.callingGetFoundActiveProcessesUIT) {
          // updating callingGetFoundActiveProcessesUIT to true (while the API is being called)
          this.setState({ callingGetFoundActiveProcessesUIT: true });
          const allActiveProcessesArray = [];
          this.props
            .getFoundActiveUITProcessesForTA(
              this.props.currentLanguage,
              this.props.uitActiveProcessesKeyword,
              this.props.uitActiveProcessesPaginationPage,
              this.props.uitActiveProcessesPaginationPageSize
            )
            .then(response => {
              // there are no results found
              if (response[0] === "no results found") {
                this.setState({
                  allActiveProcesses: [],
                  rowsDefinition: {},
                  numberOfPages: 1,
                  nextPageNumber: 0,
                  previousPageNumber: 0,
                  resultsFound: 0
                });
                // there is at least one result found
              } else {
                this.populateActiveProcessesObject(allActiveProcessesArray, response);

                // populating reasons for deletion (single candidate) options
                const deleteSingleUitSelectedReasonOptions = [];
                for (let i = 0; i < response.reasons_for_deletion_single_candidate.length; i++) {
                  deleteSingleUitSelectedReasonOptions.push({
                    value: response.reasons_for_deletion_single_candidate[i].id,
                    label:
                      response.reasons_for_deletion_single_candidate[i][
                        `${this.props.currentLanguage}_name`
                      ]
                  });
                }
                // populating reasons for deletion (whole process) options
                const deleteWholeUitProcessSelectedReasonOptions = [];
                for (let i = 0; i < response.reasons_for_deletion_whole_process.length; i++) {
                  deleteWholeUitProcessSelectedReasonOptions.push({
                    value: response.reasons_for_deletion_whole_process[i].id,
                    label:
                      response.reasons_for_deletion_whole_process[i][
                        `${this.props.currentLanguage}_name`
                      ]
                  });
                }
                // populating reasons for modification options
                const reasonsForModificationOptions = [];
                for (let i = 0; i < response.reasons_for_modification.length; i++) {
                  reasonsForModificationOptions.push({
                    value: response.reasons_for_modification[i].id,
                    label:
                      response.reasons_for_modification[i][`${this.props.currentLanguage}_name`]
                  });
                }

                this.setState({
                  resultsFound: response.count,
                  deleteSingleUitSelectedReasonOptions: deleteSingleUitSelectedReasonOptions,
                  deleteWholeUitProcessSelectedReasonOptions:
                    deleteWholeUitProcessSelectedReasonOptions,
                  reasonsForModificationOptions: reasonsForModificationOptions
                });
              }
            })
            .then(() => {
              this.setState(
                {
                  currentlyLoading: false,
                  displayResultsFound: true,
                  callingGetFoundActiveProcessesUIT: false
                },
                () => {
                  // if there is at least one result found
                  if (allActiveProcessesArray.length > 0) {
                    // make sure that this element exsits to avoid any error
                    if (document.getElementById("active-processes-results-found")) {
                      document.getElementById("active-processes-results-found").focus();
                    }
                    // no results found
                  } else {
                    // make sure that this element exsits to avoid any error
                    if (document.getElementById("active-processes-no-data")) {
                      // focusing on no results found row
                      document.getElementById("active-processes-no-data").focus();
                    }
                  }
                }
              );
            });
        }
      }, 100);
    });
  };

  render() {
    const columnsDefinition = [
      {
        label: LOCALIZE.testAdministration.uit.tabs.activeProcessess.table.columnOne,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label: LOCALIZE.testAdministration.uit.tabs.activeProcessess.table.columnTwo,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.testAdministration.uit.tabs.activeProcessess.table.columnThree,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.testAdministration.uit.tabs.activeProcessess.table.columnFour,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.testAdministration.uit.tabs.activeProcessess.table.columnFive,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.testAdministration.uit.tabs.activeProcessess.table.columnSix,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.testAdministration.uit.tabs.activeProcessess.table.columnSeven,
        style: COMMON_STYLE.CENTERED_TEXT
      }
    ];

    const columnsDefinitionPopup = [
      {
        label: LOCALIZE.testAdministration.uit.tabs.activeProcessess.selectedProcessPopup.columnOne,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label: LOCALIZE.testAdministration.uit.tabs.activeProcessess.selectedProcessPopup.columnTwo,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label:
          LOCALIZE.testAdministration.uit.tabs.activeProcessess.selectedProcessPopup.columnThree,
        style: COMMON_STYLE.CENTERED_TEXT
      }
    ];

    // setting the popup overflow visible variable
    let popupOverflowVisible = true;

    // if spacing is enabled
    if (this.props.accommodations.spacing) {
      popupOverflowVisible = false;
      // if font size is greater than 25px
    } else if (parseInt(this.props.accommodations.fontSize.split("px")[0]) > 25) {
      popupOverflowVisible = false;
    }

    return (
      <div>
        <div>
          <SearchBarWithDisplayOptions
            idPrefix={"active-processes"}
            currentlyLoading={this.state.currentlyLoading}
            updateSearchStates={this.props.updateSearchUitActiveProcessesStates}
            updatePageState={this.props.updateUITActiveProcessesPaginationPage}
            paginationPageSize={Number(this.props.uitActiveProcessesPaginationPageSize)}
            updatePaginationPageSize={this.props.updateUITActiveProcessesPaginationPageSize}
            data={this.state.allActiveProcesses}
            resultsFound={this.state.resultsFound}
          />
          <div>
            <GenericTable
              classnamePrefix="active-processes"
              columnsDefinition={columnsDefinition}
              rowsDefinition={this.state.rowsDefinition}
              emptyTableMessage={
                LOCALIZE.testAdministration.uit.tabs.activeProcessess.table.noActiveProcessess
              }
              currentlyLoading={this.state.currentlyLoading}
            />
          </div>
          <Pagination
            paginationContainerId={"active-processes-pagination-pages-link"}
            pageCount={this.state.numberOfPages}
            paginationPage={this.props.uitActiveProcessesPaginationPage}
            updatePaginationPageState={this.props.updateUITActiveProcessesPaginationPage}
            firstTableRowId={"active-processes-table-row-0"}
          />
        </div>
        {Object.entries(this.state.deleteSelectedProcess).length > 0 && (
          <PopupBox
            show={this.state.showDeleteSelectedProcessPopup}
            handleClose={() => {}}
            isBackdropStatic={true}
            shouldCloseOnEsc={false}
            overflowVisible={false}
            size={this.state.deleteWholeUitProcessSuccessfullyDone ? "lg" : "xl"}
            title={
              !this.state.deleteWholeUitProcessSuccessfullyDone
                ? LOCALIZE.formatString(
                    LOCALIZE.testAdministration.uit.tabs.activeProcessess.deleteProcessPopup.title,
                    this.state.deleteSelectedProcess.test_permission_data.staffing_process_number
                  )
                : LOCALIZE.formatString(
                    LOCALIZE.testAdministration.uit.tabs.activeProcessess.deleteProcessPopup
                      .titleSuccess,
                    this.state.deleteSelectedProcess.test_permission_data.staffing_process_number
                  )
            }
            description={
              <div>
                {!this.state.deleteWholeUitProcessSuccessfullyDone ? (
                  <div>
                    <div>
                      <SystemMessage
                        messageType={MESSAGE_TYPE.error}
                        title={LOCALIZE.commons.warning}
                        message={
                          <p>
                            {LOCALIZE.formatString(
                              LOCALIZE.testAdministration.uit.tabs.activeProcessess
                                .deleteProcessPopup.systemMessageDescription,
                              <span style={styles.bold}>
                                {parseInt(this.state.deleteSelectedProcess.total_candidates) -
                                  parseInt(this.state.deleteSelectedProcess.total_tests_taken)}
                              </span>
                            )}
                          </p>
                        }
                      />
                    </div>
                    <p style={styles.popupTableLabel}>
                      {
                        LOCALIZE.testAdministration.uit.tabs.activeProcessess.deleteProcessPopup
                          .description
                      }
                    </p>
                    <div style={styles.popupFinancialDataContainer}>
                      <div>
                        <div style={styles.labelContainer}>
                          <label>
                            <span style={styles.popupLabel}>
                              {
                                LOCALIZE.testAdministration.uit.tabs.activeProcessess
                                  .selectedProcessPopup.financialData.processNumber
                              }
                            </span>
                            <span>
                              {
                                this.state.deleteSelectedProcess.test_permission_data
                                  .staffing_process_number
                              }
                            </span>
                          </label>
                        </div>
                      </div>
                      <div>
                        <div style={styles.labelContainer}>
                          <label>
                            <span style={styles.popupLabel}>
                              {
                                LOCALIZE.testAdministration.uit.tabs.activeProcessess
                                  .selectedProcessPopup.financialData.requestingDepartment
                              }
                            </span>
                            <span>
                              {
                                this.state.deleteSelectedProcess.test_permission_data
                                  .department_ministry_code
                              }
                            </span>
                          </label>
                        </div>
                      </div>
                      <div>
                        <div style={styles.labelContainer}>
                          <label>
                            <span style={styles.popupLabel}>
                              {
                                LOCALIZE.testAdministration.uit.tabs.activeProcessess
                                  .selectedProcessPopup.financialData.HrCoordinator
                              }
                            </span>
                            <span>{`${this.state.deleteSelectedProcess.test_permission_data.billing_contact} (${this.state.deleteSelectedProcess.test_permission_data.billing_contact_info})`}</span>
                          </label>
                        </div>
                      </div>
                      <div>
                        <div style={styles.labelContainer}>
                          <label>
                            <span style={styles.popupLabel}>
                              {
                                LOCALIZE.testAdministration.uit.tabs.activeProcessess
                                  .selectedProcessPopup.financialData.validityEndDate
                              }
                            </span>
                            <span>
                              {getFormattedDate(this.state.deleteSelectedProcess.validity_end_date)}
                            </span>
                          </label>
                        </div>
                      </div>
                      <div>
                        <div style={styles.labelContainer}>
                          <label>
                            <span style={styles.popupLabel}>
                              {
                                LOCALIZE.testAdministration.uit.tabs.activeProcessess
                                  .selectedProcessPopup.financialData.test
                              }
                            </span>
                            <span>{`${
                              this.state.deleteSelectedProcess.test_permission_data.test.test_code
                            } (${
                              this.state.deleteSelectedProcess.test_permission_data.test[
                                `${this.props.currentLanguage}_name`
                              ]
                            })`}</span>
                          </label>
                        </div>
                      </div>
                    </div>
                    <Row
                      className="align-items-center justify-content-center"
                      style={styles.dropdownContainer}
                    >
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                      >
                        <label id="reason-for-uit-delete-label" style={styles.dropdownLabel}>
                          {
                            LOCALIZE.testAdministration.uit.tabs.activeProcessess
                              .deleteSingleUitTestPopup.selectReason
                          }
                        </label>
                      </Col>
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                        style={styles.dropdownCustomStyle}
                      >
                        <DropdownSelect
                          idPrefix="reason-for-uit-delete-dropdown"
                          ariaLabelledBy="reason-for-uit-delete-label"
                          options={this.state.deleteWholeUitProcessSelectedReasonOptions}
                          onChange={this.getSelectedReasonForDeletion}
                          defaultValue={this.state.deleteUitSelectedReason}
                          hasPlaceholder={false}
                          menuPlacement={"top"}
                        ></DropdownSelect>
                      </Col>
                    </Row>
                  </div>
                ) : (
                  <div>
                    <SystemMessage
                      messageType={MESSAGE_TYPE.success}
                      title={LOCALIZE.commons.success}
                      message={
                        <p>
                          {
                            LOCALIZE.testAdministration.uit.tabs.activeProcessess.deleteProcessPopup
                              .successMessage
                          }
                        </p>
                      }
                    />
                  </div>
                )}
              </div>
            }
            rightButtonType={
              !this.state.deleteWholeUitProcessSuccessfullyDone
                ? BUTTON_TYPE.danger
                : BUTTON_TYPE.primary
            }
            rightButtonTitle={
              this.state.deleteWholeUitProcessSuccessfullyDone ? (
                LOCALIZE.commons.ok
              ) : this.state.deleteProcessCurrentlyLoading ? (
                // eslint-disable-next-line jsx-a11y/label-has-associated-control
                <label className="fa fa-spinner fa-spin">
                  <FontAwesomeIcon icon={faSpinner} />
                </label>
              ) : (
                LOCALIZE.testAdministration.uit.tabs.activeProcessess.deleteProcessPopup
                  .deleteButton
              )
            }
            rightButtonIcon={
              this.state.deleteWholeUitProcessSuccessfullyDone
                ? ""
                : !this.state.deleteProcessCurrentlyLoading
                ? faTrashAlt
                : ""
            }
            rightButtonState={
              this.state.deleteWholeUitProcessSuccessfullyDone
                ? BUTTON_STATE.enabled
                : this.state.deleteProcessCurrentlyLoading ||
                  Object.entries(this.state.deleteUitSelectedReason).length <= 0
                ? BUTTON_STATE.disabled
                : BUTTON_STATE.enabled
            }
            rightButtonAction={
              !this.state.deleteWholeUitProcessSuccessfullyDone
                ? this.handleDeleteSelectedProcess
                : this.closeDeleteSelectedProcessPopup
            }
            leftButtonType={
              !this.state.deleteWholeUitProcessSuccessfullyDone
                ? BUTTON_TYPE.primary
                : BUTTON_TYPE.none
            }
            leftButtonTitle={LOCALIZE.commons.cancel}
            leftButtonIcon={faTimes}
            leftButtonState={
              this.state.deleteProcessCurrentlyLoading
                ? BUTTON_STATE.disabled
                : BUTTON_STATE.enabled
            }
            leftButtonAction={this.closeDeleteSelectedProcessPopup}
          />
        )}
        {Object.entries(this.state.viewSelectedProcess).length > 0 && (
          <PopupBox
            show={this.state.viewSelectedProcessPopup}
            handleClose={() => {}}
            isBackdropStatic={false}
            shouldCloseOnEsc={true}
            title={LOCALIZE.testAdministration.uit.tabs.activeProcessess.selectedProcessPopup.title}
            description={
              <div>
                <div style={styles.popupFinancialDataContainer}>
                  <div>
                    <div style={styles.labelContainer}>
                      <label>
                        <span style={styles.popupLabel}>
                          {
                            LOCALIZE.testAdministration.uit.tabs.activeProcessess
                              .selectedProcessPopup.financialData.processNumber
                          }
                        </span>
                        <span>
                          {
                            this.state.viewSelectedProcess.test_permission_data
                              .staffing_process_number
                          }
                        </span>
                      </label>
                    </div>
                  </div>
                  <div>
                    <div style={styles.labelContainer}>
                      <label>
                        <span style={styles.popupLabel}>
                          {
                            LOCALIZE.testAdministration.uit.tabs.activeProcessess
                              .selectedProcessPopup.financialData.requestingDepartment
                          }
                        </span>
                        <span>
                          {this.props.currentLanguage === LANGUAGES.english
                            ? `${this.state.viewSelectedProcess.test_permission_data.department_ministry_data.edesc} (${this.state.viewSelectedProcess.test_permission_data.department_ministry_data.eabrv})`
                            : `${this.state.viewSelectedProcess.test_permission_data.department_ministry_data.fdesc} (${this.state.viewSelectedProcess.test_permission_data.department_ministry_data.fabrv})`}
                        </span>
                      </label>
                    </div>
                  </div>
                  <div>
                    <div style={styles.labelContainer}>
                      <label>
                        <span style={styles.popupLabel}>
                          {
                            LOCALIZE.testAdministration.uit.tabs.activeProcessess
                              .selectedProcessPopup.financialData.HrCoordinator
                          }
                        </span>
                        <span>{`${this.state.viewSelectedProcess.test_permission_data.billing_contact} (${this.state.viewSelectedProcess.test_permission_data.billing_contact_info})`}</span>
                      </label>
                    </div>
                  </div>
                  <div>
                    <div style={styles.labelContainer}>
                      <label>
                        <span style={styles.popupLabel}>
                          {
                            LOCALIZE.testAdministration.uit.tabs.activeProcessess
                              .selectedProcessPopup.financialData.validityEndDate
                          }
                        </span>
                        <span>
                          {getFormattedDate(this.state.viewSelectedProcess.validity_end_date)}
                        </span>
                      </label>
                    </div>
                  </div>
                  <div>
                    <div style={styles.labelContainer}>
                      <label>
                        <span style={styles.popupLabel}>
                          {
                            LOCALIZE.testAdministration.uit.tabs.activeProcessess
                              .selectedProcessPopup.financialData.test
                          }
                        </span>
                        <span>{`${
                          this.state.viewSelectedProcess.test_permission_data.test.test_code
                        } (${
                          this.state.viewSelectedProcess.test_permission_data.test[
                            `${this.props.currentLanguage}_name`
                          ]
                        })`}</span>
                      </label>
                    </div>
                  </div>
                </div>

                <div style={styles.popupTableContainer}>
                  <p style={styles.popupTableLabel}>
                    {LOCALIZE.formatString(
                      LOCALIZE.testAdministration.uit.tabs.activeProcessess.selectedProcessPopup
                        .description,
                      `${
                        this.state.currentlyLoadingForPopUp
                          ? LOCALIZE.commons.na
                          : typeof this.state.rowsDefinitionPopup.data !== "undefined" &&
                            this.state.rowsDefinitionPopup.data.length
                      }`
                    )}
                  </p>
                  <GenericTable
                    classnamePrefix="view-selected-active-processes"
                    columnsDefinition={columnsDefinitionPopup}
                    rowsDefinition={this.state.rowsDefinitionPopup}
                    emptyTableMessage={""}
                    currentlyLoading={this.state.currentlyLoadingForPopUp}
                  />
                </div>
              </div>
            }
            rightButtonType={BUTTON_TYPE.primary}
            rightButtonTitle={LOCALIZE.commons.close}
            rightButtonIcon={faTimes}
            rightButtonAction={this.closeSelectedProcessPopup}
          />
        )}
        {Object.entries(this.state.updateSelectedProcessExpiryDate).length > 0 && (
          <PopupBox
            show={this.state.showUpdateSelectedProcessExpiryDatePopup}
            handleClose={() => {}}
            isBackdropStatic={true}
            shouldCloseOnEsc={false}
            overflowVisible={false}
            size={this.state.updateUitValidityEndDateSuccssfullyDone ? "lg" : "xl"}
            title={
              !this.state.updateUitValidityEndDateSuccssfullyDone
                ? LOCALIZE.testAdministration.uit.tabs.activeProcessess.updateProcessExpiryDatePopup
                    .title
                : LOCALIZE.formatString(
                    LOCALIZE.testAdministration.uit.tabs.activeProcessess
                      .updateProcessExpiryDatePopup.titleSuccess,
                    this.state.updateSelectedProcessExpiryDate.test_permission_data
                      .staffing_process_number
                  )
            }
            description={
              <div>
                {!this.state.updateUitValidityEndDateSuccssfullyDone ? (
                  <div>
                    <div className="row" style={styles.popupFinancialDataContainer}>
                      <div className="col">
                        <div>
                          <div className="row" style={styles.labelContainer}>
                            <label>
                              <span style={styles.popupLabel}>
                                {
                                  LOCALIZE.testAdministration.uit.tabs.activeProcessess
                                    .selectedProcessPopup.financialData.processNumber
                                }
                              </span>
                              <span>
                                {
                                  this.state.updateSelectedProcessExpiryDate.test_permission_data
                                    .staffing_process_number
                                }
                              </span>
                            </label>
                          </div>
                        </div>
                        <div>
                          <div className="row" style={styles.labelContainer}>
                            <label>
                              <span style={styles.popupLabel}>
                                {
                                  LOCALIZE.testAdministration.uit.tabs.activeProcessess
                                    .selectedProcessPopup.financialData.requestingDepartment
                                }
                              </span>
                              <span>
                                {
                                  this.state.updateSelectedProcessExpiryDate.test_permission_data
                                    .department_ministry_code
                                }
                              </span>
                            </label>
                          </div>
                        </div>
                        <div>
                          <div className="row" style={styles.labelContainer}>
                            <label>
                              <span style={styles.popupLabel}>
                                {
                                  LOCALIZE.testAdministration.uit.tabs.activeProcessess
                                    .selectedProcessPopup.financialData.HrCoordinator
                                }
                              </span>
                              <span>{`${this.state.updateSelectedProcessExpiryDate.test_permission_data.billing_contact} (${this.state.updateSelectedProcessExpiryDate.test_permission_data.billing_contact_info})`}</span>
                            </label>
                          </div>
                        </div>
                        <div>
                          <div className="row" style={styles.labelContainer}>
                            <label>
                              <span style={styles.popupLabel}>
                                {
                                  LOCALIZE.testAdministration.uit.tabs.activeProcessess
                                    .selectedProcessPopup.financialData.validityEndDate
                                }
                              </span>
                              <span>
                                {getFormattedDate(
                                  this.state.updateSelectedProcessExpiryDate.validity_end_date
                                )}
                              </span>
                            </label>
                          </div>
                        </div>
                        <div>
                          <div className="row" style={styles.labelContainer}>
                            <label>
                              <span style={styles.popupLabel}>
                                {
                                  LOCALIZE.testAdministration.uit.tabs.activeProcessess
                                    .selectedProcessPopup.financialData.test
                                }
                              </span>
                              <span>{`${
                                this.state.updateSelectedProcessExpiryDate.test_permission_data.test
                                  .test_code
                              } (${
                                this.state.updateSelectedProcessExpiryDate.test_permission_data
                                  .test[`${this.props.currentLanguage}_name`]
                              })`}</span>
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="row align-items-start row-padding-top-bottom">
                      <div className="col-xl-4">
                        <label id="uit-active-process-validity-end-date-label" style={styles.label}>
                          {
                            LOCALIZE.testAdministration.uit.tabs.activeProcessess
                              .updateProcessExpiryDatePopup.validityEndDate
                          }
                        </label>
                      </div>
                      <div className="col-xl-8">
                        <DatePicker
                          dateDayFieldRef={this.validityEndDateRef}
                          dateLabelId={"uit-active-process-validity-end-date-label"}
                          triggerResetFieldValues={this.state.triggerResetFieldValues}
                          initialDateDayValue={{
                            label: `${
                              this.state.updateSelectedProcessExpiryDate.validity_end_date.split(
                                "-"
                              )[2]
                            }`,
                            value:
                              this.state.updateSelectedProcessExpiryDate.validity_end_date.split(
                                "-"
                              )[2]
                          }}
                          initialDateMonthValue={{
                            label: `${
                              this.state.updateSelectedProcessExpiryDate.validity_end_date.split(
                                "-"
                              )[1]
                            }`,
                            value:
                              this.state.updateSelectedProcessExpiryDate.validity_end_date.split(
                                "-"
                              )[1]
                          }}
                          initialDateYearValue={{
                            label: `${
                              this.state.updateSelectedProcessExpiryDate.validity_end_date.split(
                                "-"
                              )[0]
                            }`,
                            value:
                              this.state.updateSelectedProcessExpiryDate.validity_end_date.split(
                                "-"
                              )[0]
                          }}
                          customYearOptions={populateCustomFutureYearsDateOptions(1)}
                          triggerValidation={this.state.triggerDateValidation}
                          futureDateValidation={true}
                          displayValidIcon={false}
                          maxMenuHeight="125px"
                          menuPlacement="bottom"
                        />
                      </div>
                    </div>
                    <div className="row align-items-start row-padding-top-bottom">
                      <div className="col-xl-4">
                        <label
                          id="uit-active-process-reason-for-modification-label"
                          style={styles.label}
                        >
                          {
                            LOCALIZE.testAdministration.uit.tabs.activeProcessess
                              .updateProcessExpiryDatePopup.reasonForModification
                          }
                        </label>
                      </div>
                      <div className="col-xl-8">
                        <div>
                          <DropdownSelect
                            idPrefix="reason-for-uit-modification-dropdown"
                            ariaLabelledBy="reason-for-uit-modification-label"
                            options={this.state.reasonsForModificationOptions}
                            onChange={this.getSelectedReasonForModification}
                            defaultValue={this.state.uitSelectedReasonForModification}
                            hasPlaceholder={false}
                            menuPlacement={"top"}
                          ></DropdownSelect>
                        </div>
                      </div>
                    </div>
                  </div>
                ) : (
                  <div>
                    <SystemMessage
                      messageType={MESSAGE_TYPE.success}
                      title={LOCALIZE.commons.success}
                      message={
                        <p>
                          {
                            LOCALIZE.testAdministration.uit.tabs.activeProcessess
                              .updateProcessExpiryDatePopup.successMessage
                          }
                        </p>
                      }
                    />
                  </div>
                )}
              </div>
            }
            leftButtonType={
              !this.state.updateUitValidityEndDateSuccssfullyDone
                ? BUTTON_TYPE.secondary
                : BUTTON_TYPE.none
            }
            leftButtonTitle={LOCALIZE.commons.cancel}
            leftButtonIcon={faTimes}
            leftButtonAction={this.closeUpdateSelectedProcessExpiryDatePopup}
            leftButtonState={
              this.state.updateUitValidityEndDateCurrentlyLoading
                ? BUTTON_STATE.disabled
                : BUTTON_STATE.enabled
            }
            rightButtonType={BUTTON_TYPE.primary}
            rightButtonTitle={
              this.state.updateUitValidityEndDateSuccssfullyDone ? (
                LOCALIZE.commons.ok
              ) : this.state.updateUitValidityEndDateCurrentlyLoading ? (
                // eslint-disable-next-line jsx-a11y/label-has-associated-control
                <label className="fa fa-spinner fa-spin">
                  <FontAwesomeIcon icon={faSpinner} />
                </label>
              ) : (
                LOCALIZE.testAdministration.uit.tabs.activeProcessess.updateProcessExpiryDatePopup
                  .rightButton
              )
            }
            rightButtonIcon={
              this.state.updateUitValidityEndDateSuccssfullyDone
                ? ""
                : this.state.updateUitValidityEndDateCurrentlyLoading
                ? ""
                : faSave
            }
            rightButtonAction={
              this.state.updateUitValidityEndDateSuccssfullyDone
                ? this.closeUpdateSelectedProcessExpiryDatePopup
                : this.handleUpdateSelectedProcessExpiryDate
            }
            rightButtonState={
              this.state.updateUitValidityEndDateSuccssfullyDone
                ? BUTTON_STATE.enabled
                : this.state.updateUitValidityEndDateCurrentlyLoading ||
                  Object.entries(this.state.uitSelectedReasonForModification).length <= 0
                ? BUTTON_STATE.disabled
                : BUTTON_STATE.enabled
            }
          />
        )}
        {Object.entries(this.state.deleteSingleUitTestApiBody).length > 0 && (
          <PopupBox
            show={this.state.showDeleteSingleUitTestPopup}
            handleClose={() => {}}
            isBackdropStatic={true}
            shouldCloseOnEsc={false}
            overflowVisible={popupOverflowVisible}
            title={
              this.state.showUitTestAlreadyStartedPopupContent
                ? LOCALIZE.testAdministration.uit.tabs.activeProcessess.uitTestAlreadyStartedPopup
                    .title
                : LOCALIZE.testAdministration.uit.tabs.activeProcessess.deleteSingleUitTestPopup
                    .title
            }
            description={
              this.state.showUitTestAlreadyStartedPopupContent ? (
                <SystemMessage
                  messageType={MESSAGE_TYPE.info}
                  title={LOCALIZE.commons.info}
                  message={<p>{this.state.uitTestAlreadyStartedErrorMessage}</p>}
                />
              ) : (
                <div>
                  <SystemMessage
                    messageType={MESSAGE_TYPE.error}
                    title={LOCALIZE.commons.warning}
                    message={
                      <p>
                        {LOCALIZE.formatString(
                          LOCALIZE.testAdministration.uit.tabs.activeProcessess
                            .deleteSingleUitTestPopup.description,
                          <span style={styles.bold}>
                            {this.state.deleteSingleUitTestApiBody.invited_candidate_email}
                          </span>
                        )}
                      </p>
                    }
                  />
                  <Row
                    className="align-items-center justify-content-center"
                    style={styles.dropdownContainer}
                  >
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                    >
                      <label id="reason-for-uit-delete-label" style={styles.dropdownLabel}>
                        {
                          LOCALIZE.testAdministration.uit.tabs.activeProcessess
                            .deleteSingleUitTestPopup.selectReason
                        }
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                    >
                      <DropdownSelect
                        idPrefix="reason-for-uit-delete-dropdown"
                        ariaLabelledBy="reason-for-uit-delete-label"
                        options={this.state.deleteSingleUitSelectedReasonOptions}
                        onChange={this.getSelectedReasonForDeletion}
                        defaultValue={this.state.deleteUitSelectedReason}
                        hasPlaceholder={false}
                        menuPlacement={"top"}
                      ></DropdownSelect>
                    </Col>
                  </Row>
                </div>
              )
            }
            rightButtonType={
              !this.state.showUitTestAlreadyStartedPopupContent
                ? BUTTON_TYPE.danger
                : BUTTON_TYPE.primary
            }
            rightButtonTitle={
              !this.state.showUitTestAlreadyStartedPopupContent ? (
                this.state.deleteSingleUitTestCurrentlyLoading ? ( // eslint-disable-next-line jsx-a11y/label-has-associated-control
                  <label className="fa fa-spinner fa-spin">
                    <FontAwesomeIcon icon={faSpinner} />
                  </label>
                ) : (
                  LOCALIZE.testAdministration.uit.tabs.activeProcessess.deleteSingleUitTestPopup
                    .deleteButton
                )
              ) : (
                LOCALIZE.commons.ok
              )
            }
            rightButtonIcon={
              !this.state.showUitTestAlreadyStartedPopupContent
                ? this.state.deleteSingleUitTestCurrentlyLoading
                  ? ""
                  : faTrashAlt
                : ""
            }
            rightButtonAction={
              !this.state.showUitTestAlreadyStartedPopupContent
                ? this.handleDeactivateSingleUitTest
                : this.closeUitTestAlreadyStartedPopup
            }
            rightButtonState={
              this.state.deleteSingleUitTestCurrentlyLoading ||
              Object.entries(this.state.deleteUitSelectedReason).length <= 0
                ? BUTTON_STATE.disabled
                : BUTTON_STATE.enabled
            }
            leftButtonType={
              !this.state.showUitTestAlreadyStartedPopupContent
                ? BUTTON_TYPE.primary
                : BUTTON_TYPE.none
            }
            leftButtonTitle={
              !this.state.showUitTestAlreadyStartedPopupContent ? LOCALIZE.commons.cancel : ""
            }
            leftButtonIcon={!this.state.showUitTestAlreadyStartedPopupContent ? faTimes : ""}
            leftButtonAction={
              !this.state.showUitTestAlreadyStartedPopupContent &&
              this.closeDeleteSingelUitTestPopup
            }
            leftButtonState={
              this.state.deleteSingleUitTestCurrentlyLoading
                ? BUTTON_STATE.disabled
                : BUTTON_STATE.enabled
            }
            size={"lg"}
            backdropClassName={"popup-box-on-top"}
          />
        )}
      </div>
    );
  }
}

export { ActiveProcesses as unconnectedActiveProcesses };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    triggerUitTablesRerender: state.uit.triggerUitTablesRerender,
    accommodations: state.accommodations,
    completeDatePicked: state.datePicker.completeDatePicked,
    completeDateValidState: state.datePicker.completeDateValidState,
    uitActiveProcessesPaginationPageSize: state.uit.uitActiveProcessesPaginationPageSize,
    uitActiveProcessesPaginationPage: state.uit.uitActiveProcessesPaginationPage,
    uitActiveProcessesKeyword: state.uit.uitActiveProcessesKeyword,
    uitActiveProcessesActiveSearch: state.uit.uitActiveProcessesActiveSearch
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAllActiveUITProcessesForTA,
      getSelectedUITActiveProcessesDetails,
      deactivateSingleUitTest,
      triggerTablesRerender,
      deactivateUitProcess,
      updateSelectedUITActiveProcessValidityEndDate,
      updateUITActiveProcessesPaginationPage,
      updateUITActiveProcessesPaginationPageSize,
      updateSearchUitActiveProcessesStates,
      getFoundActiveUITProcessesForTA,
      resetUitStates
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ActiveProcesses);
