import React, { Component } from "react";
import LOCALIZE from "../../../text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Tabs, Tab, Row, Col } from "react-bootstrap";
import Invitations from "./Invitations";
import ActiveProcesses from "./ActiveProcesses";
import CompletedProcesses from "./CompletedProcesses";
import {
  getTestPermissions,
  getOrderlessUitTestOptions,
  updateOrderlessTestOrderOptionSelected
} from "../../../modules/PermissionsRedux";

const styles = {
  mainContainer: {
    width: "100%"
  },
  tabNavigation: {
    borderBottom: "none"
  },
  tabContainer: {
    marginTop: 2,
    borderTop: "3px solid #00565e",
    backgroundColor: "#F8F8F8"
  },
  viewEditDetailsBtn: {
    minWidth: 100
  },
  buttonIcon: {
    marginRight: 6
  },
  viewPermissionRequestButton: {
    marginLeft: 6
  }
};

class UnsupervisedInternetTesting extends Component {
  static propTypes = {};

  state = {
    uitTestPermissions: [],
    uitTestOrderNumberOptions: [],
    uitTestsOptions: []
  };

  componentDidMount = () => {
    this.props.updateOrderlessTestOrderOptionSelected(false);
    this.populateTestPermissions();
  };

  componentDidUpdate = prevProps => {
    // if uitTestOrderNumber gets updated
    if (prevProps.uitTestOrderNumber !== this.props.uitTestOrderNumber) {
      // if uit test order number is already defined
      if (this.props.uitTestOrderNumber !== "") {
        // populating tests options
        this.populateTestsOptions(this.props.uitTestOrderNumber);
      }
    }
  };

  // populating user' test permissions object
  populateTestPermissions = () => {
    this.props.getTestPermissions(true).then(response => {
      this.setState({ uitTestPermissions: response }, () => {
        // populating test order number options
        this.populateTestOrderNumberOptions();

        // if uit test order number is already defined
        if (this.props.uitTestOrderNumber !== "") {
          // populating tests options
          this.populateTestsOptions(this.props.uitTestOrderNumber);
        }
      });
    });
  };

  // populating test order number options
  // TODO (fnormand): need to consider the new UIT flag (future changes)
  populateTestOrderNumberOptions = () => {
    const testOrderNumberOptionsDuplicatesRemoval = [];
    const testOrderNumberOptionsArray = [];
    const uitTestOrderNumberOptions = [];

    // adding orderless test option
    uitTestOrderNumberOptions.push({
      label: LOCALIZE.testAdministration.uit.tabs.invitations.orderlessTestOption,
      value: 0
    });

    // looping in test permissions
    for (let i = 0; i < this.state.uitTestPermissions.length; i++) {
      // if test order number does not already exist in testOrderNumberOptionsArray push it
      if (
        testOrderNumberOptionsDuplicatesRemoval.indexOf(
          this.state.uitTestPermissions[i].test_order_number
        ) < 0
      ) {
        testOrderNumberOptionsArray.push(
          `${this.state.uitTestPermissions[i].test_order_number} (${this.state.uitTestPermissions[i].staffing_process_number})`
        );
        testOrderNumberOptionsDuplicatesRemoval.push(
          this.state.uitTestPermissions[i].test_order_number
        );
      }
    }
    // looping in testOrderNumberOptionsArray in order to create the uitTestOrderNumberOptions array (including values and labels)
    for (let i = 0; i < testOrderNumberOptionsArray.length; i++) {
      uitTestOrderNumberOptions.push({
        label: testOrderNumberOptionsArray[i],
        // getting only the first part of the test order number - without (<staffing_process_number>)
        value: testOrderNumberOptionsArray[i].split(" ")[0]
      });
    }
    // saving results in state
    this.setState({ uitTestOrderNumberOptions: uitTestOrderNumberOptions });
  };

  // populating tests options
  populateTestsOptions = testOrderNumber => {
    const testsOptionsArray = [];
    const uitTestsOptions = [];
    const testsInternalNames = [];

    // "No Test Order" (orderless test) option has been selected
    if (testOrderNumber.value === 0) {
      // getting orderless test options
      this.props.getOrderlessUitTestOptions().then(response => {
        // looping in response
        for (let i = 0; i < response.length; i++) {
          uitTestsOptions.push({
            label: `${response[i][`${this.props.currentLanguage}_name`]} - v${response[i].version}`,
            value: response[i].id
          });
        }
        // updating redux state
        this.props.updateOrderlessTestOrderOptionSelected(true);
        // saving results in state
        this.setState({ uitTestsOptions: uitTestsOptions });
      });
      // regular test order number option has been selected
    } else {
      // looping in test permissions
      for (let i = 0; i < this.state.uitTestPermissions.length; i++) {
        // getting all object attributes where test order number is the same as the one provided
        if (testOrderNumber.value === this.state.uitTestPermissions[i].test_order_number) {
          // if test does not already exist in testsOptionsArray push it
          if (
            testsOptionsArray.indexOf(
              this.state.uitTestPermissions[i].test[`${this.props.currentLanguage}_name`]
            ) < 0
          ) {
            testsOptionsArray.push(
              this.state.uitTestPermissions[i].test[`${this.props.currentLanguage}_name`]
            );
            testsInternalNames.push(this.state.uitTestPermissions[i].test);
          }
        }
      }
      // looping in testsOptionsArray in order to create the uitTestsOptions array (including values and labels)
      for (let i = 0; i < testsOptionsArray.length; i++) {
        uitTestsOptions.push({
          label: testsOptionsArray[i],
          value: testsInternalNames[i].id
        });
      }
      // updating redux state
      this.props.updateOrderlessTestOrderOptionSelected(false);
      // saving results in state
      this.setState({ uitTestsOptions: uitTestsOptions });
    }
  };

  render() {
    const TABS = [
      {
        key: "invitations",
        tabName: LOCALIZE.testAdministration.uit.tabs.invitations.title,
        body: (
          <Invitations
            uitTestOrderNumberOptions={this.state.uitTestOrderNumberOptions}
            uitTestsOptions={this.state.uitTestsOptions}
          />
        )
      },
      {
        key: "active-processes",
        tabName: LOCALIZE.testAdministration.uit.tabs.activeProcessess.title,
        body: <ActiveProcesses />
      },
      {
        key: "completed-processes",
        tabName: LOCALIZE.testAdministration.uit.tabs.completedProcessess.title,
        body: <CompletedProcesses />
      }
    ];
    return (
      <div style={styles.mainContainer}>
        <div>
          <h2>{LOCALIZE.testAdministration.uit.title}</h2>
          <p>{LOCALIZE.testAdministration.uit.description}</p>
        </div>
        <div>
          <Row>
            <Col>
              <Tabs defaultActiveKey="invitations" id="uat-tabs" style={styles.tabNavigation}>
                {TABS.map((tab, index) => {
                  return (
                    <Tab
                      key={index}
                      eventKey={tab.key}
                      title={tab.tabName}
                      style={styles.tabContainer}
                    >
                      {tab.body}
                    </Tab>
                  );
                })}
              </Tabs>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

export { UnsupervisedInternetTesting as unconnectedUnsupervisedInternetTesting };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    uitTestOrderNumber: state.uit.uitTestOrderNumber
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    { getTestPermissions, getOrderlessUitTestOptions, updateOrderlessTestOrderOptionSelected },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(UnsupervisedInternetTesting);
