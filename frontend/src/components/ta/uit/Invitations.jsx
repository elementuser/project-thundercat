/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Component } from "react";
import PropTypes from "prop-types";
import LOCALIZE, { LOCALIZE_OBJ } from "../../../text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import DropdownSelect from "../../commons/DropdownSelect";
import DatePicker, { ERROR_MESSAGE } from "../../commons/DatePicker";
import CustomButton, { THEME } from "../../commons/CustomButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faClock,
  faDownload,
  faEnvelope,
  faFolderOpen,
  faPauseCircle,
  faSpinner,
  faTimes,
  faUndo
} from "@fortawesome/free-solid-svg-icons";
import {
  faClock as regularFaClock,
  faPauseCircle as regularFaPauseCircle
} from "@fortawesome/free-regular-svg-icons";
import populateCustomFutureYearsDateOptions from "../../../helpers/populateCustomDatePickerOptions";
import StyledTooltip, { TYPE, EFFECT, TRIGGER_TYPE } from "../../authentication/StyledTooltip";
import { getLineSpacingCSS } from "../../../modules/AccommodationsRedux";
import {
  updateUitTestOrderNumberState,
  updateUitTestsState,
  resetUitInvitationsRelatedStates,
  getReasonsForTesting,
  sendUitInvitations,
  triggerTablesRerender,
  uitInvitationEndDateValidation
} from "../../../modules/UitRedux";
import Papa from "papaparse";
import "../../../css/uit.css";
import { validateNameWithParentheses, validateEmail } from "../../../helpers/regexValidator";
import PopupBox, { BUTTON_TYPE, BUTTON_STATE } from "../../commons/PopupBox";
import {
  getTestPermissionFinancialData,
  updateOrderlessTestOrderOptionSelected
} from "../../../modules/PermissionsRedux";
import getFormattedDate from "../../../helpers/getFormattedDate";
import GenericTable, { COMMON_STYLE } from "../../commons/GenericTable";
import SystemMessage, { MESSAGE_TYPE } from "../../commons/SystemMessage";
import { getOrganization } from "../../../modules/ExtendedProfileOptionsRedux";
import { getTaExtendedProfile } from "../../../modules/UserProfileRedux";
import { getSpecifiedTestScoringMethodData } from "../../../modules/TestBuilderRedux";
import { getTestData } from "../../../modules/TestRedux";
import { LANGUAGES } from "../../commons/Translation";
import { SCORING_METHOD_TYPE } from "../../testBuilder/helpers";
import { Col, Row } from "react-bootstrap";
import Switch from "react-switch";
import getSwitchTransformScale from "../../../helpers/switchTransformScale";
import orderByLabels from "../../../helpers/orderingHelpers";
import UitTemplate from "../../../templates/uit_template.xlsx";
import { getTimeInHoursMinutes } from "../../../helpers/timeConversion";
import SetTimer from "../../commons/SetTimer";
import {
  resetTimerStates,
  updateTimerState,
  setDefaultTimes,
  updateValidTimerState
} from "../../../modules/SetTimerRedux";
import { updateTestTime } from "../../../modules/TestAdministrationRedux";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 6,
    xl: 5
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 6,
    xl: 7
  },
  spacedFirstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 12,
    xl: 12
  },
  spacedSecondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 12,
    xl: 12
  }
};

const styles = {
  mainContainer: {
    margin: "24px 60px"
  },
  subDataContainer: {
    paddingLeft: 48
  },
  subDataContainerLoading: {
    textAlign: "center"
  },
  elementContainer: {
    padding: "12px 0"
  },
  labelContainer: {
    display: "table-cell",
    verticalAlign: "middle"
  },
  label: {
    overflowWrap: "break-word",
    margin: "0 12px 0 0"
  },
  popupLabel: {
    fontWeight: "bold",
    paddingRight: 12
  },
  popupFinancialDataContainer: {
    border: "1px solid #CECECE",
    backgroundColor: "#F3F3F3",
    padding: 12
  },
  testsDivInPopup: {
    marginLeft: 24
  },
  popupTableContainer: {
    margin: "24px 0"
  },
  popupTableLabel: {
    fontWeight: "bold",
    paddingBottom: 24
  },
  dropdown: {
    width: "100%",
    display: "table-cell",
    verticalAlign: "middle",
    position: "relative"
  },
  candidatesUploadDownloadContainer: {
    position: "relative",
    width: "100%",
    display: "table-cell",
    paddingLeft: 0,
    verticalAlign: "middle",
    textAlign: "center"
  },
  candidatesUploadDownloadInputContainer: {
    width: "100%",
    float: "left"
  },
  input: {
    backgroundColor: "white",
    minHeight: 38,
    border: "1px solid #00565e",
    width: "100%",
    padding: "3px 6px 3px 6px"
  },
  inputWithButtons: {
    backgroundColor: "white",
    minHeight: 38,
    border: "1px solid #00565e",
    height: "100%"
  },
  candidatesUploadTextField: {
    textOverflow: "ellipsis"
  },
  uploadDownloadIconsContainer: {
    position: "absolute",
    right: 0
  },
  uploadDownloadIconButton: {
    padding: "3px 12px",
    minWidth: 25,
    minHeight: 38,
    borderLeft: "1px solid #00565e",
    borderRadius: 0,
    height: "100%"
  },
  noBorderRight: {
    borderRight: "none"
  },
  borderRadius: {
    borderRadius: "0 4px 4px 0"
  },
  browseButton: {
    borderRadius: "0 0.35rem 0.35rem 0"
  },
  buttonContainer: {
    textAlign: "center",
    margin: 24
  },
  buttonLabel: {
    margin: 0,
    paddingLeft: 6
  },
  sendInvitesButton: {
    minWidth: 175,
    margin: 10
  },
  resetAllFieldsButton: {
    minWidth: 175,
    margin: 10
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6,
    width: "100%"
  },
  errorMessageTextAlign: {
    textAlign: "left"
  },
  noPadding: {
    padding: 0
  },
  loading: {
    height: 60,
    textAlign: "center",
    fontSize: "32px",
    padding: 6
  },
  centerText: {
    textAlign: "center"
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  reuseDataLabel: {
    paddingLeft: 63
  },
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  bold: {
    fontWeight: "bold"
  },
  testSectionTimerMainContainer: {
    margin: "12px 0"
  },
  testSectionTimerDescriptionContainer: {
    paddingLeft: 33,
    verticalAlign: "middle"
  },
  testSectionTimerTimeContainer: {
    verticalAlign: "middle"
  },
  allUnset: {
    all: "unset"
  },
  opacity: {
    opacity: "0.5"
  }
};

class Invitations extends Component {
  uitTestOrderNumberRef = React.createRef();

  uitTestsRef = React.createRef();

  reasonForTestingRef = React.createRef();

  levelRequiredRef = React.createRef();

  validityEndDateRef = React.createRef();

  static propTypes = {
    uitTestOrderNumberOptions: PropTypes.array.isRequired,
    uitTestsOptions: PropTypes.array.isRequired
  };

  state = {
    isLoading: false,

    // field related states
    isValidUitTestOrderNumber: true,
    isValidUitTests: true,
    testsDropdownDisabled: this.props.uitTestOrderNumber === "",
    uitTestsAccessibilityWording: LOCALIZE.commons.none,
    triggerDateValidation: false,
    triggerResetDatePickerFieldValues: false,
    reuseData: false,
    referenceNumber: "",
    isLoadingDepartmentMinistryOptions: true,
    departmentMinistryOptions: [],
    departmentMinistry: [],
    fisOrgCode: "",
    fisRefCode: "",
    billingContactName: "",
    billingContactInfo: "",
    reasonForTestingOptionsEn: [],
    reasonForTestingOptionsFr: [],
    reasonForTesting: [],
    isLoadingLevelRequiredOptions: true,
    levelRequiredOptionsEn: [],
    levelRequiredOptionsFr: [],
    levelRequired: [],
    showLevelRequiredRawScoreTextBox: false,
    levelRequiredMaxPossibleScore: 0,
    levelRequiredScoringMethodNone: false,

    // file data related states
    selectedFileDetails: {
      name: ""
    },
    selectedFileData: [],
    csvErrorsArray: [],

    // file validation related states
    displayNoCsvFileSelectedError: false,
    displayNoDataInCsvFileError: false,
    displayInvalidFileTypeError: false,
    displayDuplicateEmailsError: false,
    isValidFile: true,

    // form validation related states
    isValidForm: false,
    IsValidReferenceNumber: true,
    isValidFisOrgCode: true,
    isValidFisRefCode: true,
    isValidBillingContactName: true,
    isValidBillingContactInfo: true,
    isValidReasonForTesting: true,
    isValidLevelRequired: true,
    isValidLevelRequiredRawScore: true,
    isValidNumberOfCandidates: true,
    isValidNumberOfCandidatesBasedOnEndDate: true,

    // popup related states
    showInvitesConfirmationPopup: false,
    financialData: {},
    rowsDefinition: {},
    triggerPopupTableRerender: false,
    isLoadingPopupTableData: true,
    isSendingInvitations: false,
    displaySentInvitationsSuccessMessage: false,
    displaySentInvitationsFailMessage: false,
    showResetAllFieldsPopup: false,
    showEditTestTimePopup: false,
    selectedCandidateData: {},
    isRightPopupButtonDisabled: false,
    currentHours: [],
    totalHours: 0,
    currentMinutes: [],
    totalMinutes: 0,
    currentHoursBreakBank: ["00"],
    currentMinutesBreakBank: ["00"],
    showAddEditBreakBankPopup: false,

    // default values (switch style)
    isLoadingSwitch: false,
    switchHeight: 25,
    switchWidth: 50
  };

  componentDidMount = () => {
    // initializing redux states
    this.props.resetUitInvitationsRelatedStates();
    this.getUpdatedSwitchDimensions();
  };

  componentDidUpdate = (prevProps, prevState) => {
    if (prevProps.accommodations.fontSize !== this.props.accommodations.fontSize) {
      this.getUpdatedSwitchDimensions();
    }
    // if uitTestOrderNumber gets updated
    if (prevProps.uitTestOrderNumber !== this.props.uitTestOrderNumber) {
      // updating testsDropdownDisabled state
      this.setState({ testsDropdownDisabled: this.props.uitTestOrderNumber === "" });
    }
    // if uitTests gets updated
    if (prevProps.uitTests !== this.props.uitTests) {
      if (Object.entries(this.props.uitTests).length > 0) {
        // populating level required options based on current selected test
        this.populateLevelRequiredOptions(this.props.uitTests.value);
        // no test is selected
      } else {
        // updating uitTestsAccessibilityWording state
        this.setState({ uitTestsAccessibilityWording: LOCALIZE.commons.none });
      }
    }
    // if triggerDateValidation state gets updated
    if (prevState.triggerDateValidation !== this.state.triggerDateValidation) {
      // adding small delay, so date validation (props) has time to happen
      setTimeout(() => {
        // validating form
        this.validateForm();
      }, 100);
    }
    // if isValidForm state gets updated
    if (prevState.isValidForm !== this.state.isValidForm) {
      // valid form
      if (this.state.isValidForm) {
        this.openPopup();
      }
    }
    // if orderlessTestOrderOptionSelected gets updated
    if (
      prevProps.orderlessTestOrderOptionSelected !== this.props.orderlessTestOrderOptionSelected
    ) {
      if (this.props.orderlessTestOrderOptionSelected) {
        // populating department/ministry options
        this.populateDepartmentMinistryOptions();
        // populating reasons for testing
        this.populateReasonsForTesting();
      }
    }
    // if timeUpdated gets updated
    if (prevProps.timeUpdated !== this.props.timeUpdated) {
      this.calculateTotalTestTime();
    }
  };

  calculateTotalTestTime = () => {
    let totalMinutes = 0;
    // calculating the total time in minutes
    for (let i = 0; i < this.props.currentTimer.length; i++) {
      totalMinutes += Number(this.props.currentTimer[i].hours) * 60;
      totalMinutes += Number(this.props.currentTimer[i].minutes);
    }
    // saving total time in hours and minutes
    this.setState({
      totalHours: getTimeInHoursMinutes(totalMinutes).hours,
      totalMinutes: getTimeInHoursMinutes(totalMinutes).minutes
    });
  };

  onPopupClose = () => {
    // initializing currentTimer redux state to []
    this.props.updateTimerState([]);
  };

  populateDepartmentMinistryOptions = () => {
    this.setState({ isLoadingDepartmentMinistryOptions: true }, () => {
      const departmentMinistryOptions = [];
      this.props
        .getOrganization()
        .then(response => {
          for (let i = 0; i < response.body.length; i++) {
            // Interface is in English
            if (this.props.currentLanguage === LANGUAGES.english) {
              departmentMinistryOptions.push({
                label: `${response.body[i].edesc} (${response.body[i].eabrv})`,
                value: `${response.body[i].dept_id}`
              });
              // Interface is in French
            } else {
              departmentMinistryOptions.push({
                label: `${response.body[i].fdesc} (${response.body[i].fabrv})`,
                value: `${response.body[i].dept_id}`
              });
            }
          }
          this.setState({ departmentMinistryOptions: departmentMinistryOptions });
        })
        .then(() => {
          // getting TA extended profile data
          this.props.getTaExtendedProfile().then(response => {
            let initialDepartmentMinistrySelectedOption = [];
            // associated department/ministry found
            if (Object.entries(response.body).length > 0) {
              initialDepartmentMinistrySelectedOption = {
                label: `${response.body[`department_name_${this.props.currentLanguage}`]} (${
                  response.body[`department_abrv_${this.props.currentLanguage}`]
                })`,
                value: `${response.body.department_id}`
              };
            }
            this.setState({
              departmentMinistry: initialDepartmentMinistrySelectedOption,
              isLoadingDepartmentMinistryOptions: false
            });
          });
        });
    });
  };

  populateReasonsForTesting = () => {
    this.props.getReasonsForTesting().then(response => {
      const reasonForTestingOptionsEn = [];
      const reasonForTestingOptionsFr = [];
      // looping in response
      for (let i = 0; i < response.length; i++) {
        // populating reasonForTestingOptions (english)
        reasonForTestingOptionsEn.push({
          label: response[i].description_en,
          value: response[i].id
        });
        // populating reasonForTestingOptions (french)
        reasonForTestingOptionsFr.push({
          label: response[i].description_fr,
          value: response[i].id
        });
      }
      // sorting options
      reasonForTestingOptionsEn.sort(orderByLabels);
      reasonForTestingOptionsFr.sort(orderByLabels);
      // adding provided in CSV file option (as first option)
      reasonForTestingOptionsEn.unshift({
        label: LOCALIZE_OBJ.en.testAdministration.uit.tabs.invitations.providedInCsvOption,
        value: "CSV"
      });
      reasonForTestingOptionsFr.unshift({
        label: LOCALIZE_OBJ.fr.testAdministration.uit.tabs.invitations.providedInCsvOption,
        value: "CSV"
      });
      // updating needed states
      this.setState({
        reasonForTestingOptionsEn: reasonForTestingOptionsEn,
        reasonForTestingOptionsFr: reasonForTestingOptionsFr
      });
    });
  };

  populateLevelRequiredOptions = testId => {
    this.setState({ isLoadingLevelRequiredOptions: true }, () => {
      // getting test definition scoring method data
      this.props.getSpecifiedTestScoringMethodData(testId).then(response => {
        // initializing needed variables
        const levelRequiredOptionsEn = [];
        const levelRequiredOptionsFr = [];
        let showLevelRequiredRawScoreTextBox = false;
        let levelRequiredMaxPossibleScore = 0;
        let levelRequiredScoringMethodNone = false;

        // getting/setting scoring method type
        const scoringMethodType = response.body.scoring_method_type;
        // THRESHOLD
        if (scoringMethodType === SCORING_METHOD_TYPE.THRESHOLD) {
          // looping in data
          for (let i = 0; i < response.body.data.length; i++) {
            // populating level required options
            levelRequiredOptionsEn.push({
              label: `${response.body.data[i].en_conversion_value}`,
              value: response.body.data[i].id
            });
            levelRequiredOptionsFr.push({
              label: `${response.body.data[i].fr_conversion_value}`,
              value: response.body.data[i].id
            });
          }
          // sorting options
          levelRequiredOptionsEn.sort(orderByLabels);
          levelRequiredOptionsFr.sort(orderByLabels);
          // adding provided in CSV file option (as first option)
          levelRequiredOptionsEn.unshift({
            label: LOCALIZE_OBJ.en.testAdministration.uit.tabs.invitations.providedInCsvOption,
            value: "CSV"
          });
          levelRequiredOptionsFr.unshift({
            label: LOCALIZE_OBJ.fr.testAdministration.uit.tabs.invitations.providedInCsvOption,
            value: "CSV"
          });
        }
        // PASS_FAIL
        else if (scoringMethodType === SCORING_METHOD_TYPE.PASS_FAIL) {
          // populating level required options
          // Pass
          levelRequiredOptionsEn.push({
            label: LOCALIZE_OBJ.en.commons.passStatus,
            value: LOCALIZE_OBJ.en.commons.passStatus
          });
          levelRequiredOptionsFr.push({
            label: LOCALIZE_OBJ.fr.commons.passStatus,
            value: LOCALIZE_OBJ.fr.commons.passStatus
          });
          // Fail
          levelRequiredOptionsEn.push({
            label: LOCALIZE_OBJ.en.commons.failStatus,
            value: LOCALIZE_OBJ.en.commons.failStatus
          });
          levelRequiredOptionsFr.push({
            label: LOCALIZE_OBJ.fr.commons.failStatus,
            value: LOCALIZE_OBJ.fr.commons.failStatus
          });
          // sorting options
          levelRequiredOptionsEn.sort(orderByLabels);
          levelRequiredOptionsFr.sort(orderByLabels);
          // adding provided in CSV file option (as first option)
          levelRequiredOptionsEn.unshift({
            label: LOCALIZE_OBJ.en.testAdministration.uit.tabs.invitations.providedInCsvOption,
            value: "CSV"
          });
          levelRequiredOptionsFr.unshift({
            label: LOCALIZE_OBJ.fr.testAdministration.uit.tabs.invitations.providedInCsvOption,
            value: "CSV"
          });
        }
        // RAW
        else if (scoringMethodType === SCORING_METHOD_TYPE.RAW) {
          showLevelRequiredRawScoreTextBox = true;
          levelRequiredMaxPossibleScore = response.body.data.maximum_possible_score;
        }
        // PERCENTAGE
        else if (scoringMethodType === SCORING_METHOD_TYPE.PERCENTAGE) {
          // TODO: TO BE COMPLETED
        }
        // NONE
        else if (scoringMethodType === SCORING_METHOD_TYPE.NONE) {
          levelRequiredScoringMethodNone = true;
          this.setState({
            levelRequired: { label: LOCALIZE.commons.none, value: LOCALIZE.commons.none }
          });
        }

        // updating needed states
        this.setState({
          levelRequiredOptionsEn: levelRequiredOptionsEn,
          levelRequiredOptionsFr: levelRequiredOptionsFr,
          isLoadingLevelRequiredOptions: false,
          showLevelRequiredRawScoreTextBox: showLevelRequiredRawScoreTextBox,
          levelRequiredMaxPossibleScore: levelRequiredMaxPossibleScore,
          levelRequiredScoringMethodNone: levelRequiredScoringMethodNone
        });
      });
    });
  };

  handleUploadFile = () => {
    // open file explorer
    this.upload.click();
  };

  handleDownloadTemplate = () => {
    // creating csv template file a tag
    // reference: https://stackoverflow.com/questions/50694881/how-to-download-file-in-react-js
    const link = document.createElement("a");
    link.href = UitTemplate;
    link.download = LOCALIZE.testAdministration.uit.tabs.invitations.csvTemplateFileName;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  };

  // get selected test order number option + setting needed states
  onSelectedTestOrderNumberOptionChange = selectedOption => {
    this.props.updateUitTestOrderNumberState(selectedOption);
    this.props.updateUitTestsState("");
    this.setState({ isValidUitTestOrderNumber: true, isValidUitTests: true, financialData: {} });
  };

  // get selected tests options + setting needed states
  onSelectedDepartmentMinistryOptionChange = selectedOption => {
    this.setState({ departmentMinistry: selectedOption });
  };

  // get selected tests options + setting needed states
  onSelectedTestsOptionChange = selectedOption => {
    this.props.updateUitTestsState(selectedOption);
    this.setState({
      selectedFileDetails: {
        name: ""
      },
      selectedFileData: [],
      csvErrorsArray: [],
      isValidUitTests: true,
      reasonForTesting: [],
      isValidReasonForTesting: true,
      levelRequired: [],
      isValidLevelRequired: true,
      isValidLevelRequiredRawScore: true
    });
  };

  // get selected reason for testing option + setting needed states
  onSelectedReasonForTestingOptionChange = selectedOption => {
    this.setState(
      {
        reasonForTesting: selectedOption,
        isValidReasonForTesting: true
      },
      () => {
        // validating CSV file
        const validatedCsvFile = this.validateCsvFile(this.state.selectedFileData);
        this.setState({
          isValidFile: validatedCsvFile.validState,
          csvErrorsArray: validatedCsvFile.csvErrorsArray
        });
      }
    );
  };

  // get selected level required option + setting needed states
  onSelectedLevelRequiredOptionChange = selectedOption => {
    this.setState(
      {
        levelRequired: selectedOption,
        isValidLevelRequired: true
      },
      () => {
        // validating CSV file
        const validatedCsvFile = this.validateCsvFile(this.state.selectedFileData);
        this.setState({
          isValidFile: validatedCsvFile.validState,
          csvErrorsArray: validatedCsvFile.csvErrorsArray
        });
      }
    );
  };

  openPopup = () => {
    // initializing needed variables
    let financialData = null;
    let rowsDefinition = {};
    // orderless test order option has been selected
    if (this.props.orderlessTestOrderOptionSelected) {
      // setting financial data
      const financialData = {
        referenceNumber: this.state.referenceNumber,
        departmentMinistry: this.state.departmentMinistry.label,
        fisOrgCode: this.state.fisOrgCode,
        fisRefCode: this.state.fisRefCode,
        billingContactName: this.state.billingContactName,
        billingContactInfo: this.state.billingContactInfo,
        test: this.props.uitTests.label,
        reasonForTesting: this.state.reasonForTesting.label,
        levelRequired: this.state.levelRequired
      };
      // populating table rows
      rowsDefinition = this.populateTableRows();

      // updating needed states
      this.setState(
        {
          financialData: financialData,
          rowsDefinition: rowsDefinition,
          showInvitesConfirmationPopup: true,
          isLoadingPopupTableData: true
        },
        () => {
          this.setState({
            triggerPopupTableRerender: !this.state.triggerPopupTableRerender,
            isLoadingPopupTableData: false
          });
        }
      );

      // regular test access codes has been selected
    } else {
      // getting financial data based on the provided test order number and test
      this.props
        .getTestPermissionFinancialData(
          this.props.uitTestOrderNumber.value,
          this.props.uitTests.value
        )
        // eslint-disable-next-line no-loop-func
        .then(response => {
          // updating financialData based on first test
          // eslint-disable-next-line prefer-destructuring
          financialData = response[0];
          financialData.test = [response[0].test];

          // populating table rows
          rowsDefinition = this.populateTableRows();

          // updating needed states
          this.setState({
            financialData: financialData,
            rowsDefinition: rowsDefinition,
            showInvitesConfirmationPopup: true,
            isLoadingPopupTableData: true
          });
        })
        .then(() => {
          this.setState({
            triggerPopupTableRerender: !this.state.triggerPopupTableRerender,
            isLoadingPopupTableData: false
          });
        });
    }
  };

  closePopup = () => {
    this.setState({
      showInvitesConfirmationPopup: false,
      isValidForm: false
    });
  };

  triggerFormValidation = () => {
    this.setState(
      {
        triggerDateValidation: !this.state.triggerDateValidation
      },
      () => {
        // validating CSV file
        const validatedCsvFile = this.validateCsvFile(this.state.selectedFileData);
        this.setState({
          isValidFile: validatedCsvFile.validState,
          csvErrorsArray: validatedCsvFile.csvErrorsArray
        });
      }
    );
  };

  validateForm = () => {
    // initializing needed variables
    let isValidUitTestOrderNumber = true;
    let isValidUitTests = true;
    let displayNoCsvFileSelectedError = false;
    let IsValidReferenceNumber = true;
    let isValidFisOrgCode = true;
    let isValidFisRefCode = true;
    let isValidBillingContactName = true;
    let isValidBillingContactInfo = true;
    let isValidReasonForTesting = true;
    let isValidLevelRequired = true;
    let isValidNumberOfCandidates = true;
    let isValidNumberOfCandidatesBasedOnEndDate = true;

    // getting totalNumberOfCandidates (considering multi-tests requests)
    const totalNumberOfCandidates = this.state.selectedFileData.length;

    // validating the number of candidate based on the end date (maximum of 10 000 candidates per ending date/expiry date)
    this.props
      .uitInvitationEndDateValidation(this.props.completeDatePicked, totalNumberOfCandidates)
      .then(response => {
        isValidNumberOfCandidatesBasedOnEndDate = response.is_valid;

        // validating the uit test order number
        if (this.props.uitTestOrderNumber === "") {
          isValidUitTestOrderNumber = false;
        }

        // validating the uit tests
        if (this.props.uitTests === "") {
          isValidUitTests = false;
        }

        // making sure that a csv file has been selected
        if (this.state.selectedFileData.length <= 0 && this.state.selectedFileDetails.name === "") {
          displayNoCsvFileSelectedError = true;
        }

        // if orderless test number option has been selected
        if (this.props.orderlessTestOrderOptionSelected) {
          // validating the reference number
          if (this.state.referenceNumber === "") {
            IsValidReferenceNumber = false;
          }
          // validating the FIS Organisation Code
          if (this.state.fisOrgCode === "") {
            isValidFisOrgCode = false;
          }
          // validating the FIS Reference Code
          if (this.state.fisRefCode === "") {
            isValidFisRefCode = false;
          }
          // validating the Billing Contact Name
          if (this.state.billingContactName === "") {
            isValidBillingContactName = false;
          }
          // validating the Billing Contact Info
          isValidBillingContactInfo = validateEmail(this.state.billingContactInfo);

          // validating the Reason for Testing
          if (this.state.reasonForTesting.length <= 0) {
            isValidReasonForTesting = false;
          }
          // validating the Level Required
          if (
            (!this.state.showLevelRequiredRawScoreTextBox &&
              this.state.levelRequired.length <= 0) ||
            !this.state.isValidLevelRequiredRawScore
          ) {
            isValidLevelRequired = false;
          }
        }

        // validating the number of candidates (maximum of 500 invitations in total including multi-tests requests)
        if (totalNumberOfCandidates > 500) {
          isValidNumberOfCandidates = false;
        }

        // all states are valid + no errors + isValidFile state is set to true
        if (
          isValidUitTestOrderNumber &&
          isValidUitTests &&
          this.props.completeDateValidState &&
          !displayNoCsvFileSelectedError &&
          this.state.isValidFile &&
          IsValidReferenceNumber &&
          isValidFisOrgCode &&
          isValidFisRefCode &&
          isValidBillingContactName &&
          isValidBillingContactInfo &&
          isValidReasonForTesting &&
          isValidLevelRequired &&
          isValidNumberOfCandidates &&
          isValidNumberOfCandidatesBasedOnEndDate &&
          this.state.csvErrorsArray.length <= 0
        ) {
          this.setState({
            isValidForm: true,
            isValidUitTestOrderNumber: true,
            isValidUitTests: true,
            // removing csv file errors
            displayNoCsvFileSelectedError: false,
            displayInvalidFileTypeError: false,
            // removing date error
            isValidNumberOfCandidatesBasedOnEndDate: isValidNumberOfCandidatesBasedOnEndDate
          });
          // there is at least on state invalid
        } else {
          this.setState(
            {
              isValidForm: false,
              isValidUitTestOrderNumber: isValidUitTestOrderNumber,
              isValidUitTests: isValidUitTests,
              displayNoCsvFileSelectedError: displayNoCsvFileSelectedError,
              // removing invalid csv file type errors
              displayInvalidFileTypeError: false,
              IsValidReferenceNumber: IsValidReferenceNumber,
              isValidFisOrgCode: isValidFisOrgCode,
              isValidFisRefCode: isValidFisRefCode,
              isValidBillingContactName: isValidBillingContactName,
              isValidBillingContactInfo: isValidBillingContactInfo,
              isValidReasonForTesting: isValidReasonForTesting,
              isValidLevelRequired: isValidLevelRequired,
              isValidNumberOfCandidates: isValidNumberOfCandidates,
              isValidNumberOfCandidatesBasedOnEndDate: isValidNumberOfCandidatesBasedOnEndDate
            },
            () => {
              this.focusOnHighestErrorField();
            }
          );
        }
      });
  };

  // analyses field by field and focus on the highest error field
  focusOnHighestErrorField = () => {
    if (!this.state.isValidUitTestOrderNumber) {
      this.uitTestOrderNumberRef.current.focus();
    } else if (!this.state.IsValidReferenceNumber && this.props.orderlessTestOrderOptionSelected) {
      document.getElementById("reference-number-input").focus();
    } else if (!this.state.isValidFisOrgCode && this.props.orderlessTestOrderOptionSelected) {
      document.getElementById("fis-organisation-code-input").focus();
    } else if (!this.state.isValidFisRefCode && this.props.orderlessTestOrderOptionSelected) {
      document.getElementById("fis-reference-code-input").focus();
    } else if (
      !this.state.isValidBillingContactName &&
      this.props.orderlessTestOrderOptionSelected
    ) {
      document.getElementById("billing-contact-name-input").focus();
    } else if (
      !this.state.isValidBillingContactInfo &&
      this.props.orderlessTestOrderOptionSelected
    ) {
      document.getElementById("billing-contact-info-input").focus();
    } else if (!this.state.isValidUitTests) {
      this.uitTestsRef.current.focus();
    } else if (!this.state.isValidReasonForTesting && this.props.orderlessTestOrderOptionSelected) {
      this.reasonForTestingRef.current.focus();
    } else if (
      !this.state.isValidLevelRequired &&
      !this.state.showLevelRequiredRawScoreTextBox &&
      this.props.orderlessTestOrderOptionSelected
    ) {
      this.levelRequiredRef.current.focus();
    } else if (
      (!this.state.isValidLevelRequired || !this.state.isValidLevelRequiredRawScore) &&
      this.state.showLevelRequiredRawScoreTextBox &&
      this.props.orderlessTestOrderOptionSelected
    ) {
      document.getElementById("level-required-input").focus();
    } else if (!this.state.isValidFile || this.state.displayNoCsvFileSelectedError) {
      document.getElementById("uit-candidates-upload-text-field").focus();
    } else if (
      !this.props.completeDateValidState ||
      !this.state.isValidNumberOfCandidatesBasedOnEndDate
    ) {
      this.validityEndDateRef.current.focus();
    }
  };

  openResetAllFieldsPopup = () => {
    this.setState({ showResetAllFieldsPopup: true });
  };

  closeResetAllFieldsPopup = () => {
    this.setState({ showResetAllFieldsPopup: false });
  };

  resetAllFields = resetReuseDataState => {
    // resetReuseDataState is set to false + current reuseData state is set to true
    if (!resetReuseDataState && this.state.reuseData) {
      this.setState({
        isValidForm: false,
        showInvitesConfirmationPopup: false,
        reasonForTesting: [],
        levelRequired: this.state.levelRequiredScoringMethodNone
          ? { label: LOCALIZE.commons.none, value: LOCALIZE.commons.none }
          : [],
        selectedFileDetails: {
          name: ""
        },
        selectedFileData: [],
        csvErrorsArray: [],
        financialData: {},
        rowsDefinition: {},
        triggerPopupTableRerender: false,
        isLoadingPopupTableData: true,
        isSendingInvitations: false,
        displaySentInvitationsSuccessMessage: false,
        displaySentInvitationsFailMessage: false
      });
    } else {
      this.props.updateUitTestOrderNumberState("");
      this.props.updateUitTestsState("");
      this.props.updateOrderlessTestOrderOptionSelected(false);
      this.props.resetUitInvitationsRelatedStates();
      this.setState({
        isLoading: false,

        // field related states
        isValidUitTestOrderNumber: true,
        isValidUitTests: true,
        testsDropdownDisabled: this.props.uitTestOrderNumber === "",
        uitTestsAccessibilityWording: LOCALIZE.commons.none,
        triggerResetDatePickerFieldValues: !this.state.triggerResetDatePickerFieldValues,
        reuseData: resetReuseDataState ? false : this.state.reuseData,
        referenceNumber: "",
        isLoadingDepartmentMinistryOptions: true,
        departmentMinistryOptions: [],
        departmentMinistry: [],
        fisOrgCode: "",
        fisRefCode: "",
        billingContactName: "",
        billingContactInfo: "",
        reasonForTestingOptionsEn: [],
        reasonForTestingOptionsFr: [],
        reasonForTesting: [],
        isLoadingLevelRequiredOptions: true,
        levelRequiredOptionsEn: [],
        levelRequiredOptionsFr: [],
        levelRequired: [],
        showLevelRequiredRawScoreTextBox: false,
        levelRequiredMaxPossibleScore: 0,
        levelRequiredScoringMethodNone: false,
        isValidNumberOfCandidates: true,

        // file data related states
        selectedFileDetails: {
          name: ""
        },
        selectedFileData: [],
        csvErrorsArray: [],

        // file validation related states
        displayNoCsvFileSelectedError: false,
        displayNoDataInCsvFileError: false,
        displayInvalidFileTypeError: false,
        displayDuplicateEmailsError: false,
        isValidFile: true,

        // form validation related states
        isValidForm: false,
        IsValidReferenceNumber: true,
        isValidFisOrgCode: true,
        isValidFisRefCode: true,
        isValidBillingContactName: true,
        isValidBillingContactInfo: true,
        isValidReasonForTesting: true,
        isValidLevelRequired: true,
        isValidLevelRequiredRawScore: true,
        isValidNumberOfCandidatesBasedOnEndDate: true,

        // popup related states
        showInvitesConfirmationPopup: false,
        financialData: {},
        rowsDefinition: {},
        triggerPopupTableRerender: false,
        isLoadingPopupTableData: true,
        isSendingInvitations: false,
        displaySentInvitationsSuccessMessage: false,
        displaySentInvitationsFailMessage: false,
        showResetAllFieldsPopup: false
      });
    }
  };

  onFileSelection = event => {
    // updating states (including the selected file details)
    this.setState(
      {
        isLoading: true,
        selectedFileDetails: event.target.files[0],
        isValidNumberOfCandidates: true
      },
      () => {
        event.stopPropagation();
        event.preventDefault();

        // making sure that the selected file is an excel file
        // valid file type
        if (
          this.state.selectedFileDetails.type === "application/vnd.ms-excel" ||
          this.state.selectedFileDetails.type === "text/csv"
        ) {
          // parsing file data
          Papa.parse(this.state.selectedFileDetails, { complete: this.updateData, header: true });
          // updating displayInvalidFileTypeError state
          this.setState({
            displayInvalidFileTypeError: false,
            selectedFileDetails: this.state.selectedFileDetails
          });
          // invalid file type
        } else {
          // updating needed state (including reinitializing the selectedFileDetails and selectedFileData states)
          this.setState(
            {
              selectedFileDetails: { name: "" },
              selectedFileData: [],
              displayInvalidFileTypeError: true,
              isValidFile: false,
              displayNoCsvFileSelectedError: false,
              displayNoDataInCsvFileError: false,
              displayDuplicateEmailsError: false,
              isLoading: false
            },
            () => {
              // focusing on candidates upload input field
              document.getElementById("uit-candidates-upload-text-field").focus();
            }
          );
        }
      }
    );
  };

  updateData = result => {
    const { data } = result;
    // calling csv validation function
    const isValidCsvFile = this.validateCsvFile(data);
    const selectedFileData = isValidCsvFile.finalDataArray;
    // user has AAE permission
    if (this.props.isAae) {
      // getting default test time
      this.props.getTestData(this.props.uitTests.value).then(response => {
        const defaultTestTime = response.body.default_test_time;
        // looping in selectedFileData
        for (let i = 0; i < selectedFileData.length; i++) {
          // pushing default values for the accommodation columns
          selectedFileData[i].test_sections = this.populateTestSections(
            response.body.test_sections
          );
          selectedFileData[i].total_time = `${
            getTimeInHoursMinutes(Number(defaultTestTime)).formattedHours
          } : ${getTimeInHoursMinutes(Number(defaultTestTime)).formattedMinutes}`;
          selectedFileData[i].break_bank = 0;
        }
        // updating needed states
        this.setState(
          {
            selectedFileData: selectedFileData,
            isValidFile: isValidCsvFile.validState,
            displayNoDataInCsvFileError: isValidCsvFile.displayNoDataInCsvFileError,
            displayNoCsvFileSelectedError: false,
            displayDuplicateEmailsError: isValidCsvFile.containsDuplicateEmail,
            isLoading: false,
            csvErrorsArray: isValidCsvFile.csvErrorsArray
          },
          () => {
            // focusing on candidates upload input field
            document.getElementById("uit-candidates-upload-text-field").focus();
          }
        );
      });
      // user is a regular TA
    } else {
      // looping in selectedFileData
      for (let i = 0; i < selectedFileData.length; i++) {
        // pushing non-accommodation values for the accommodation columns
        selectedFileData[i].test_sections = [];
        selectedFileData[i].total_time = null;
        selectedFileData[i].break_bank = 0;
      }
      // updating needed states
      this.setState(
        {
          selectedFileData: selectedFileData,
          isValidFile: isValidCsvFile.validState,
          displayNoDataInCsvFileError: isValidCsvFile.displayNoDataInCsvFileError,
          displayNoCsvFileSelectedError: false,
          displayDuplicateEmailsError: isValidCsvFile.containsDuplicateEmail,
          isLoading: false,
          csvErrorsArray: isValidCsvFile.csvErrorsArray
        },
        () => {
          // focusing on candidates upload input field
          document.getElementById("uit-candidates-upload-text-field").focus();
        }
      );
    }
  };

  populateTestSections = testSections => {
    // creating a copy of testSections
    const copyOfTestSections = Array.from(testSections);
    // looping in copyOfTestSections
    for (let i = 0; i < copyOfTestSections.length; i++) {
      // adding time attribute with the same value as the default time
      copyOfTestSections[i].time = copyOfTestSections[i].default_time;
    }
    return copyOfTestSections;
  };

  formatBreakBank = seconds => {
    // divided by 60, since we're receiving time in seconds
    return `${getTimeInHoursMinutes(Number(seconds / 60)).formattedHours} : ${
      getTimeInHoursMinutes(Number(seconds / 60)).formattedMinutes
    }`;
  };

  validateCsvFile = data => {
    let isValidCsvFile = true;
    const csvErrorsArray = [];
    const finalDataArray = [];
    let displayNoDataInCsvFileError = false;
    let containsDuplicateEmail = false;
    // looping in data array
    for (let i = 0; i < data.length; i++) {
      // if last row of the csv file
      if (
        (data[i].Prenom_First_Name === "" &&
          typeof data[i].Nom_de_famille_Last_Name === "undefined") ||
        (data[i].Prenom_First_Name === "" &&
          data[i].Nom_de_famille_Last_Name === "" &&
          data[i].Courriel_Email === "")
      ) {
        break;
      }
      // validating Prenom_First_Name
      if (!validateNameWithParentheses(data[i].Prenom_First_Name)) {
        isValidCsvFile = false;
        // + 2 because of the header + index that starts at 0
        csvErrorsArray.push(`A${i + 2}`);
      }

      // validating Nom_de_famille_Last_Name
      if (!validateNameWithParentheses(data[i].Nom_de_famille_Last_Name)) {
        isValidCsvFile = false;
        // + 2 because of the header + index that starts at 0
        csvErrorsArray.push(`B${i + 2}`);
      }

      // validating email
      if (!validateEmail(data[i].Courriel_Email)) {
        isValidCsvFile = false;
        // + 2 because of the header + index that starts at 0
        csvErrorsArray.push(`C${i + 2}`);
      }

      // provided in CSV option selected (reason for testing - orderless request)
      if (Object.entries(this.state.reasonForTesting).length > 0) {
        if (this.state.reasonForTesting.value === "CSV") {
          // creating a copy of reasonForTestingOptions
          const copyOfReasonForTestingOptionsEn = Array.from(this.state.reasonForTestingOptionsEn);
          const copyOfReasonForTestingOptionsFr = Array.from(this.state.reasonForTestingOptionsFr);
          // checking if "Provided in CSV" option is part of the options
          const indexOfProvidedInCsvEn = copyOfReasonForTestingOptionsEn.findIndex(obj => {
            return obj.value === "CSV";
          });
          const indexOfProvidedInCsvFr = copyOfReasonForTestingOptionsFr.findIndex(obj => {
            return obj.value === "CSV";
          });
          // "Provided in CSV" option has been found in reasonForTestingOptions
          if (indexOfProvidedInCsvEn >= 0) {
            // removing "Provided in CSV" option from reasonForTestingOptions
            copyOfReasonForTestingOptionsEn.splice(indexOfProvidedInCsvEn, 1);
          }
          if (indexOfProvidedInCsvFr >= 0) {
            // removing "Provided in CSV" option from reasonForTestingOptions
            copyOfReasonForTestingOptionsFr.splice(indexOfProvidedInCsvFr, 1);
          }
          // checking if reason for testing of current iterration is part of the reasonForTestingOptions arrays (in both languages)
          if (
            copyOfReasonForTestingOptionsEn.filter(
              obj =>
                obj.label.toUpperCase() ===
                data[i].Motif_pour_evaluation_Reason_for_Testing.toUpperCase()
            ).length <= 0 &&
            copyOfReasonForTestingOptionsFr.filter(
              obj =>
                obj.label.toUpperCase() ===
                data[i].Motif_pour_evaluation_Reason_for_Testing.toUpperCase()
            ).length <= 0
          ) {
            isValidCsvFile = false;
            // + 2 because of the header + index that starts at 0
            csvErrorsArray.push(`D${i + 2}`);
          }
        }
      }

      // provided in CSV option selected (level required - orderless request)
      // only possible for Threshold and Pass-Fail scoring methods
      if (Object.entries(this.state.levelRequired).length > 0) {
        if (this.state.levelRequired.value === "CSV") {
          // creating a copy of levelRequiredOptions
          const copyOfLevelRequiredOptionsEn = Array.from(this.state.levelRequiredOptionsEn);
          const copyOfLevelRequiredOptionsFr = Array.from(this.state.levelRequiredOptionsFr);
          // checking if "Provided in CSV" option is part of the options
          const indexOfProvidedInCsvEn = copyOfLevelRequiredOptionsEn.findIndex(obj => {
            return obj.value === "CSV";
          });
          const indexOfProvidedInCsvFr = copyOfLevelRequiredOptionsFr.findIndex(obj => {
            return obj.value === "CSV";
          });
          // "Provided in CSV" option has been found in levelRequiredOptions
          if (indexOfProvidedInCsvEn >= 0) {
            // removing "Provided in CSV" option from levelRequiredOptions
            copyOfLevelRequiredOptionsEn.splice(indexOfProvidedInCsvEn, 1);
          }
          if (indexOfProvidedInCsvFr >= 0) {
            // removing "Provided in CSV" option from levelRequiredOptions
            copyOfLevelRequiredOptionsFr.splice(indexOfProvidedInCsvFr, 1);
          }
          // checking if level required of current iterration is part of the levelRequiredOptions array
          if (
            copyOfLevelRequiredOptionsEn.filter(
              obj => obj.label.toUpperCase() === data[i].Niveau_requis_Level_Required.toUpperCase()
            ).length <= 0 &&
            copyOfLevelRequiredOptionsFr.filter(
              obj => obj.label.toUpperCase() === data[i].Niveau_requis_Level_Required.toUpperCase()
            ).length <= 0
          ) {
            isValidCsvFile = false;
            // + 2 because of the header + index that starts at 0
            csvErrorsArray.push(`E${i + 2}`);
          }
        }
      }

      // provided in CSV option selected (level required - orderless request)
      // only possible for RAW Score scoring method
      // if level required/raw score is not defined ==> provided in CSV
      if (
        this.state.showLevelRequiredRawScoreTextBox &&
        (this.state.levelRequired === "" ||
          (Array.isArray(this.state.levelRequired) && this.state.levelRequired.length <= 0))
      ) {
        // getting current value
        const currentValue = parseInt(data[i].Niveau_requis_Level_Required);
        // if NaN (not a number)
        // eslint-disable-next-line no-restricted-globals
        if (isNaN(currentValue)) {
          isValidCsvFile = false;
          // + 2 because of the header + index that starts at 0
          csvErrorsArray.push(`E${i + 2}`);
        } else {
          // provided number <= levelRequiredMaxPossibleScore
          if (currentValue > this.state.levelRequiredMaxPossibleScore) {
            isValidCsvFile = false;
            // + 2 because of the header + index that starts at 0
            csvErrorsArray.push(`E${i + 2}`);
          }
        }
      }

      finalDataArray.push(data[i]);
    }

    // if valid CSV file
    if (isValidCsvFile) {
      // making sure that the csv file contains data
      if (finalDataArray.length <= 0) {
        displayNoDataInCsvFileError = true;
      }
      // making sure that there is no duplicate emails
      // looping in finalDataArray
      for (let i = 0; i < finalDataArray.length; i++) {
        // filtering finalDataArray by current email (email of the current iteration)
        const tempArray = finalDataArray.filter(
          obj => obj.Courriel_Email.toLowerCase() === finalDataArray[i].Courriel_Email.toLowerCase()
        );
        // verifying if the tempArray size is greater than 1 (meaning that the same email is used more than once)
        if (tempArray.length > 1) {
          // getting index of first element
          const indexOfFirst = finalDataArray.findIndex(obj => {
            return obj.Courriel_Email.toLowerCase() === tempArray[0].Courriel_Email.toLowerCase();
          });
          // getting index of second element
          const indexOfSecond = finalDataArray.findIndex((obj, index) => {
            return (
              obj.Courriel_Email.toLowerCase() === tempArray[1].Courriel_Email.toLowerCase() &&
              index !== indexOfFirst
            );
          });
          containsDuplicateEmail = true;
          // updating csvErrorsArray
          // + 2 because of the header + index that starts at 0
          csvErrorsArray.push(`C${indexOfFirst + 2}`);
          csvErrorsArray.push(`C${indexOfSecond + 2}`);
          break;
        }
      }
    }

    return {
      validState: isValidCsvFile,
      finalDataArray: finalDataArray,
      csvErrorsArray: csvErrorsArray,
      displayNoDataInCsvFileError: displayNoDataInCsvFileError,
      containsDuplicateEmail: containsDuplicateEmail
    };
  };

  // populating table rows for the GenericTable component
  populateTableRows = () => {
    const { selectedFileData } = this.state;

    // initializing needed object and array for rowsDefinition state (needed for GenericTable component)
    let rowsDefinition = {};
    const data = [];

    for (let i = 0; i < selectedFileData.length; i++) {
      // user has AAE permission
      if (this.props.isAae) {
        // pushing needed data in data array (needed for GenericTable component)
        data.push({
          column_1: selectedFileData[i].Courriel_Email,
          column_2: selectedFileData[i].Prenom_First_Name,
          column_3: selectedFileData[i].Nom_de_famille_Last_Name,
          column_4: this.getTotalTestTime(selectedFileData[i]),
          column_5: this.formatBreakBank(selectedFileData[i].break_bank),
          column_6: this.populateColumnSix(i)
        });
        // user is a regular TA
      } else {
        // pushing needed data in data array (needed for GenericTable component)
        data.push({
          column_1: selectedFileData[i].Courriel_Email,
          column_2: selectedFileData[i].Prenom_First_Name,
          column_3: selectedFileData[i].Nom_de_famille_Last_Name
        });
      }
    }

    // user has AAE permission
    if (this.props.isAae) {
      // updating rowsDefinition object with provided data and needed style
      rowsDefinition = {
        column_1_style: COMMON_STYLE.LEFT_TEXT,
        column_2_style: COMMON_STYLE.LEFT_TEXT,
        column_3_style: COMMON_STYLE.LEFT_TEXT,
        column_4_style: COMMON_STYLE.CENTERED_TEXT,
        column_5_style: COMMON_STYLE.CENTERED_TEXT,
        column_6_style: COMMON_STYLE.CENTERED_TEXT,
        data: data
      };
      // user is a regular TA
    } else {
      // updating rowsDefinition object with provided data and needed style
      rowsDefinition = {
        column_1_style: COMMON_STYLE.LEFT_TEXT,
        column_2_style: COMMON_STYLE.LEFT_TEXT,
        column_3_style: COMMON_STYLE.LEFT_TEXT,
        data: data
      };
    }

    return rowsDefinition;
  };

  populateColumnSix = i => {
    return (
      <div className="d-flex justify-content-center flex-nowrap">
        <StyledTooltip
          id={`edit-test-time-tooltip-${i}`}
          place="top"
          type={TYPE.light}
          effect={EFFECT.solid}
          triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
          tooltipElement={
            <div style={styles.allUnset}>
              <CustomButton
                dataTip=""
                dataFor={`edit-test-time-tooltip-${i}`}
                label={
                  <FontAwesomeIcon icon={this.containsAddedTime(i) ? faClock : regularFaClock} />
                }
                action={() => {
                  this.editTestTime(i);
                }}
                customStyle={styles.actionButton}
                buttonTheme={THEME.SECONDARY}
              />
            </div>
          }
          tooltipContent={
            <div>
              <p>
                {
                  LOCALIZE.testAdministration.uit.tabs.invitations.invitesConfirmationPopup
                    .inviteData.table.editTestTimeTooltip
                }
              </p>
            </div>
          }
        />
        <StyledTooltip
          id={`edit-break-bank-tooltip-${i}`}
          place="top"
          type={TYPE.light}
          effect={EFFECT.solid}
          triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
          tooltipElement={
            <div style={styles.allUnset}>
              <CustomButton
                dataTip=""
                dataFor={`edit-break-bank-tooltip-${i}`}
                label={
                  <FontAwesomeIcon
                    icon={
                      this.state.selectedFileData[i].break_bank > 0
                        ? faPauseCircle
                        : regularFaPauseCircle
                    }
                  />
                }
                action={() => {
                  this.editBreakBank(i);
                }}
                customStyle={styles.actionButton}
                buttonTheme={THEME.SECONDARY}
              />
            </div>
          }
          tooltipContent={
            <div>
              <p>
                {
                  LOCALIZE.testAdministration.uit.tabs.invitations.invitesConfirmationPopup
                    .inviteData.table.editBreakBankTooltip
                }
              </p>
            </div>
          }
        />
      </div>
    );
  };

  containsAddedTime = index => {
    // initializing containsAddedTime
    let containsAddedTime = false;
    // getting current candidate's data
    const currentCandidateData = this.state.selectedFileData[index];
    // looping in test sections of current candidate data
    for (let i = 0; i < currentCandidateData.test_sections.length; i++) {
      // time > default time
      if (
        currentCandidateData.test_sections[i].time >
        currentCandidateData.test_sections[i].default_time
      ) {
        // updating containsAddedTime
        containsAddedTime = true;
        break;
      }
    }
    return containsAddedTime;
  };

  getTotalTestTime = candidateData => {
    // initializing totalTestTime
    let totalTestTime = 0;
    // looping in test sections
    for (let i = 0; i < candidateData.test_sections.length; i++) {
      totalTestTime += candidateData.test_sections[i].time;
    }
    return `${getTimeInHoursMinutes(Number(totalTestTime)).formattedHours} : ${
      getTimeInHoursMinutes(Number(totalTestTime)).formattedMinutes
    }`;
  };

  editTestTime = index => {
    const { selectedFileData } = this.state;
    const currentHours = [];
    const currentMinutes = [];
    const defaultTimes = [];
    for (let i = 0; i < selectedFileData[index].test_sections.length; i++) {
      currentHours.push(
        getTimeInHoursMinutes(selectedFileData[index].test_sections[i].time).formattedHours
      );
      currentMinutes.push(
        getTimeInHoursMinutes(selectedFileData[index].test_sections[i].time).formattedMinutes
      );
      defaultTimes.push(selectedFileData[index].test_sections[i].default_time);
    }
    this.props.setDefaultTimes(defaultTimes);
    this.setState({
      showEditTestTimePopup: true,
      selectedCandidateData: this.state.selectedFileData[index],
      currentHours: currentHours,
      currentMinutes: currentMinutes
    });
  };

  closeEditTestTimePopup = () => {
    this.setState({
      showEditTestTimePopup: false,
      selectedCandidateData: {},
      isRightPopupButtonDisabled: false
    });
  };

  handleSetTimer = () => {
    // disabling right popup button (to avoid multiple clicks)
    this.setState({ isRightPopupButtonDisabled: true }, () => {
      const { currentTimer } = this.props;
      // getting index of current selected candidate
      const index = this.getCurrentTableIndex();
      // const currentIndex = this.getCurrentTableIndex();
      const copyOfSelectedFileData = Array.from(this.state.selectedFileData);
      for (let i = 0; i < currentTimer.length; i++) {
        let timeInMinutes = 0;
        // const firstIteration = i === 0;
        // const lastIteration = i === currentTimer.length - 1;
        timeInMinutes = Number(currentTimer[i].hours) * 60 + Number(currentTimer[i].minutes);

        // need to rebuild the test_section array for whaterver reason. Otherwise, all fields on the table will be updated
        // initializing newTestSectionArray
        const newTestSectionArray = {};
        // adding needed attributes
        newTestSectionArray.en_title = copyOfSelectedFileData[index].test_sections[i].en_title;
        newTestSectionArray.fr_title = copyOfSelectedFileData[index].test_sections[i].fr_title;
        newTestSectionArray.id = copyOfSelectedFileData[index].test_sections[i].id;
        newTestSectionArray.test_definition =
          copyOfSelectedFileData[index].test_sections[i].test_definition;
        newTestSectionArray.default_time =
          copyOfSelectedFileData[index].test_sections[i].default_time;
        newTestSectionArray.time = timeInMinutes;
        // updating the test sections array (based on the current index of the for loop)
        copyOfSelectedFileData[index].test_sections[i] = newTestSectionArray;
      }
      this.setState({ selectedFileData: copyOfSelectedFileData }, () => {
        // updating table
        const updatedRowsDefinition = this.populateTableRows();
        this.setState({ rowsDefinition: updatedRowsDefinition }, () => {
          // closing edit test time popup
          this.closeEditTestTimePopup();
        });
      });
    });
  };

  getCurrentTableIndex = () => {
    // initializing currentIndex
    let currentIndex = null;
    // looping in TA assigned candidates table to get the index of the current selected candidate
    for (let i = 0; i < this.state.selectedFileData.length; i++) {
      // using the email to find the index, since the email must be unique to be a valid CSV file
      if (
        this.state.selectedCandidateData.Courriel_Email ===
        this.state.selectedFileData[i].Courriel_Email
      ) {
        // updating currentIndex once found
        currentIndex = i;
        break;
      }
    }
    return currentIndex;
  };

  editBreakBank = index => {
    const currentHoursBreakBank = [];
    const currentMinutesBreakBank = [];
    const selectedCandidateData = this.state.selectedFileData[index];
    // setting timer default time
    currentHoursBreakBank.push(
      getTimeInHoursMinutes(Number(selectedCandidateData.break_bank / 60)).formattedHours
    );
    currentMinutesBreakBank.push(
      getTimeInHoursMinutes(Number(selectedCandidateData.break_bank / 60)).formattedMinutes
    );
    this.props.setDefaultTimes([selectedCandidateData.break_bank]);
    // break bank time is not defined
    this.setState({
      showAddEditBreakBankPopup: true,
      currentHoursBreakBank: currentHoursBreakBank,
      currentMinutesBreakBank: currentMinutesBreakBank,
      selectedCandidateData: selectedCandidateData
    });
    this.setState({ showAddEditBreakBankPopup: true });
  };

  handleAddEditBreakBank = () => {
    // disabling right popup button (to avoid multiple clicks)
    this.setState({ isRightPopupButtonDisabled: true }, () => {
      // getting index of current selected candidate
      const index = this.getCurrentTableIndex();
      // converting selected time in seconds
      let totalSeconds = 0;
      totalSeconds += Number(this.props.currentTimer[0].hours) * 60 * 60;
      totalSeconds += Number(this.props.currentTimer[0].minutes) * 60;

      // creating a copy of the selectedFileData
      const copyOfSelectedFileData = this.state.selectedFileData;
      copyOfSelectedFileData[index].break_bank = totalSeconds;

      this.setState({ selectedFileData: copyOfSelectedFileData }, () => {
        // updating table
        const updatedRowsDefinition = this.populateTableRows();
        this.setState({ rowsDefinition: updatedRowsDefinition }, () => {
          // closing edit test time popup
          this.closeAddEditBreakBankPopup();
        });
      });
    });
  };

  closeAddEditBreakBankPopup = () => {
    this.props.updateValidTimerState(true);
    this.setState({ showAddEditBreakBankPopup: false, isRightPopupButtonDisabled: false });
  };

  handleSendInvitations = () => {
    this.setState({ isSendingInvitations: true }, () => {
      // sending invitations
      let body = {};
      // orderless test order option has been selected
      if (this.props.orderlessTestOrderOptionSelected) {
        // checking if additional info has been provided in CSV (reason for testing and/or level required)
        let additionalInfoProvidedInCsv = false;
        // reason for testing
        if (this.state.reasonForTesting.value === "CSV") {
          // setting additionalInfoProvidedInCsv to true
          additionalInfoProvidedInCsv = true;
          // level required (threshold or pass/fail)
        } else if (Object.entries(this.state.levelRequired).length > 0) {
          if (this.state.levelRequired.value === "CSV") {
            // setting additionalInfoProvidedInCsv to true
            additionalInfoProvidedInCsv = true;
          }
          // level required (RAW Score)
        } else if (
          this.state.showLevelRequiredRawScoreTextBox &&
          (this.state.levelRequired === "" ||
            (Array.isArray(this.state.levelRequired) && this.state.levelRequired.length <= 0))
        ) {
          // setting additionalInfoProvidedInCsv to true
          additionalInfoProvidedInCsv = true;
        }
        body = {
          candidates_list: this.state.selectedFileData,
          validity_end_date: getFormattedDate(this.props.completeDatePicked),
          tests: this.props.uitTests.value,
          reference_number: this.state.referenceNumber,
          department_ministry_id: this.state.departmentMinistry.value,
          fis_organisation_code: this.state.fisOrgCode,
          fis_reference_code: this.state.fisRefCode,
          billing_contact_name: this.state.billingContactName,
          billing_contact_info: this.state.billingContactInfo,
          reason_for_testing_id: this.state.reasonForTesting.value,
          level_required: this.state.showLevelRequiredRawScoreTextBox
            ? this.state.levelRequired
            : this.state.levelRequired.value === "CSV"
            ? this.state.levelRequired.value
            : this.state.levelRequired.label,
          orderless_request: true,
          additional_info_provided_in_csv: additionalInfoProvidedInCsv
        };
        // regular test access codes has been selected
      } else {
        const testsArray = [this.props.uitTests.value];
        body = {
          candidates_list: this.state.selectedFileData,
          validity_end_date: getFormattedDate(this.props.completeDatePicked),
          test_order_number: this.props.uitTestOrderNumber.value,
          tests: testsArray,
          orderless_request: false
        };
      }
      this.props.sendUitInvitations(body).then(response => {
        if (response.ok) {
          // Updating the states - Success
          this.setState({
            isSendingInvitations: false,
            displaySentInvitationsSuccessMessage: true,
            displaySentInvitationsFailMessage: false
          });
          // triggering active/completed processes tables rerender
          this.props.triggerTablesRerender();
        } else {
          // Updating the states - Fail
          this.setState({
            isSendingInvitations: false,
            displaySentInvitationsSuccessMessage: false,
            displaySentInvitationsFailMessage: true
          });
        }
      });
    });
  };

  // get reference number content
  getReferenceNumberContent = event => {
    const referenceNumber = event.target.value;
    // allow only alphanumeric, slash and dash (0 to 25 chars)
    const regexExpression = /^([a-zA-zÀ-ÿ0-9-/]{0,25})$/;
    if (regexExpression.test(referenceNumber)) {
      this.setState({ referenceNumber: referenceNumber, IsValidReferenceNumber: true });
    }
    if (referenceNumber.length <= 0) {
      this.setState({ IsValidReferenceNumber: false });
    }
  };

  // get FIS organisation code content
  getFisOrgCodeContent = event => {
    const fisOrgCode = event.target.value;
    // allow maximum of 16 chars (prevent use of comma)
    const regexExpression = /^([^,]{0,16})$/;
    if (regexExpression.test(fisOrgCode)) {
      this.setState({ fisOrgCode: fisOrgCode, isValidFisOrgCode: true });
    }
    if (fisOrgCode.length <= 0) {
      this.setState({ isValidFisOrgCode: false });
    }
  };

  // get FIS reference code content
  getFisRefCodeContent = event => {
    const fisRefCode = event.target.value;
    // allow maximum of 20 chars (prevent use of comma)
    const regexExpression = /^([^,]{0,20})$/;
    if (regexExpression.test(fisRefCode)) {
      this.setState({ fisRefCode: fisRefCode, isValidFisRefCode: true });
    }
    if (fisRefCode.length <= 0) {
      this.setState({ isValidFisRefCode: false });
    }
  };

  // get Billing contact name content
  getBillingContactNameContent = event => {
    const billingContactName = event.target.value;
    // allow maximum of 180 chars (prevent use of comma)
    const regexExpression = /^([^,]{0,180})$/;
    if (regexExpression.test(billingContactName)) {
      this.setState({ billingContactName: billingContactName, isValidBillingContactName: true });
    }
    if (billingContactName.length <= 0) {
      this.setState({ isValidBillingContactName: false });
    }
  };

  // get Billing contact information content
  getBillingContactInfoContent = event => {
    const billingContactInfo = event.target.value;
    // allow maximum of 255 chars
    const regexExpression = /^(.{0,255})$/;
    if (regexExpression.test(billingContactInfo)) {
      this.setState({ billingContactInfo: billingContactInfo, isValidBillingContactInfo: true });
    }
    if (billingContactInfo.length <= 0) {
      this.setState({ isValidBillingContactInfo: false });
    }
  };

  // get Level Required content (expected raw score)
  getLevelRequiredContent = event => {
    const levelRequired = event.target.value;
    // allow only numeric entries
    const regexExpression = /^[0-9]*$/;
    if (regexExpression.test(levelRequired)) {
      this.setState({
        levelRequired: levelRequired,
        isValidLevelRequired: true,
        isValidLevelRequiredRawScore: true
      });
    }
    if (levelRequired > this.state.levelRequiredMaxPossibleScore) {
      this.setState({ isValidLevelRequiredRawScore: false });
    }
    // adding small delay to allow states to be updated
    setTimeout(() => {
      // validating CSV file
      const validatedCsvFile = this.validateCsvFile(this.state.selectedFileData);
      this.setState({
        isValidFile: validatedCsvFile.validState,
        csvErrorsArray: validatedCsvFile.csvErrorsArray
      });
    }, 250);
  };

  onSwitchToggle = event => {
    this.setState({ reuseData: event });
  };

  // getting switch dimensions and updating states
  getUpdatedSwitchDimensions = () => {
    // need this loading state, otherwise the switch diameter is not calculated on state change
    this.setState({ isLoadingSwitch: true }, () => {
      this.setState(
        {
          switchHeight: getSwitchTransformScale(this.props.accommodations.fontSize).height,
          switchWidth: getSwitchTransformScale(this.props.accommodations.fontSize).width
        },
        () => {
          this.setState({ isLoadingSwitch: false });
        }
      );
    });
  };

  getFormattedErrorsString = () => {
    // initializing finalFormattedErrorsString
    let finalFormattedErrorsString = "";
    // looping in csvErrorsArray
    for (let i = 0; i < this.state.csvErrorsArray.length; i++) {
      // not the last index
      if (i < this.state.csvErrorsArray.length - 1) {
        finalFormattedErrorsString = `${
          finalFormattedErrorsString + this.state.csvErrorsArray[i]
        }, `;
      } else {
        finalFormattedErrorsString += this.state.csvErrorsArray[i];
      }
    }

    return finalFormattedErrorsString;
  };

  render() {
    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }
    const columnsDefinition = [
      {
        label:
          LOCALIZE.testAdministration.uit.tabs.invitations.invitesConfirmationPopup.inviteData.table
            .emailAddress,
        style: {}
      },
      {
        label:
          LOCALIZE.testAdministration.uit.tabs.invitations.invitesConfirmationPopup.inviteData.table
            .firstName,
        style: {}
      },
      {
        label:
          LOCALIZE.testAdministration.uit.tabs.invitations.invitesConfirmationPopup.inviteData.table
            .lastName,
        style: {}
      }
    ];
    // user has AAE permission
    if (this.props.isAae) {
      columnsDefinition.push(
        {
          label:
            LOCALIZE.testAdministration.uit.tabs.invitations.invitesConfirmationPopup.inviteData
              .table.totalTime,
          style: COMMON_STYLE.CENTERED_TEXT
        },
        {
          label:
            LOCALIZE.testAdministration.uit.tabs.invitations.invitesConfirmationPopup.inviteData
              .table.breakBank,
          style: COMMON_STYLE.CENTERED_TEXT
        },
        {
          label:
            LOCALIZE.testAdministration.uit.tabs.invitations.invitesConfirmationPopup.inviteData
              .table.actions,
          style: COMMON_STYLE.CENTERED_TEXT
        }
      );
    }
    return (
      <div style={styles.mainContainer} role="presentation">
        <Row className="justify-content-end align-items-center" style={styles.elementContainer}>
          <Col
            xl={columnSizes.firstColumn.xl}
            lg={columnSizes.firstColumn.lg}
            md={columnSizes.firstColumn.md}
            sm={columnSizes.firstColumn.sm}
            xs={columnSizes.firstColumn.xs}
            style={styles.labelContainer}
          >
            <label id="uit-test-order-number-label" style={styles.label}>
              {LOCALIZE.testAdministration.uit.tabs.invitations.testOrderNumber}
            </label>
          </Col>
          <Col
            xl={columnSizes.secondColumn.xl}
            lg={columnSizes.secondColumn.lg}
            md={columnSizes.secondColumn.md}
            sm={columnSizes.secondColumn.sm}
            xs={columnSizes.secondColumn.xs}
            style={styles.dropdown}
          >
            <DropdownSelect
              idPrefix="uit-test-order-number-dropdown"
              ariaLabelledBy="uit-test-order-number-label uit-test-order-number-error-message uit-test-order-number-current-value-accessibility"
              ariaRequired={true}
              options={this.props.uitTestOrderNumberOptions}
              onChange={this.onSelectedTestOrderNumberOptionChange}
              defaultValue={this.props.uitTestOrderNumber}
              hasPlaceholder={true}
              isValid={this.state.isValidUitTestOrderNumber}
              customRef={this.uitTestOrderNumberRef}
              orderByLabels={false}
            ></DropdownSelect>
            <label id="uit-test-order-number-current-value-accessibility" style={styles.hiddenText}>
              {
                LOCALIZE.testAdministration.uit.tabs.invitations
                  .testOrderNumberCurrentValueAccessibility
              }
            </label>
          </Col>
          {!this.state.isValidUitTestOrderNumber && (
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
            >
              <label
                className="text-left"
                id="uit-test-order-number-error-message"
                style={styles.errorMessage}
              >
                {LOCALIZE.testAdministration.uit.tabs.invitations.testOrderNumberErrorMessage}
              </label>
            </Col>
          )}
        </Row>
        {this.props.orderlessTestOrderOptionSelected && (
          <>
            <Row className="justify-content-end align-items-center" style={styles.elementContainer}>
              <Col
                xl={columnSizes.firstColumn.xl}
                lg={columnSizes.firstColumn.lg}
                md={7}
                sm={7}
                xs={7}
                style={{ ...styles.labelContainer, ...styles.reuseDataLabel }}
              >
                <label id="reuse-data-label" style={styles.label}>
                  {LOCALIZE.testAdministration.uit.tabs.invitations.reuseData}
                </label>
              </Col>
              <Col
                xl={columnSizes.secondColumn.xl}
                lg={columnSizes.secondColumn.lg}
                md={5}
                sm={5}
                xs={5}
                style={styles.dropdown}
              >
                {!this.state.isLoadingSwitch && (
                  <Switch
                    onChange={this.onSwitchToggle}
                    checked={this.state.reuseData}
                    aria-labelledby="reuse-data-label"
                    height={this.state.switchHeight}
                    width={this.state.switchWidth}
                  />
                )}
              </Col>
            </Row>

            <div style={styles.subDataContainer}>
              <Row
                className="justify-content-end align-items-center"
                style={styles.elementContainer}
              >
                <Col
                  xl={columnSizes.spacedFirstColumn.xl}
                  lg={columnSizes.spacedFirstColumn.lg}
                  md={columnSizes.spacedFirstColumn.md}
                  sm={columnSizes.spacedFirstColumn.sm}
                  xs={columnSizes.spacedFirstColumn.xs}
                  style={styles.labelContainer}
                >
                  <label
                    id="reference-number-label"
                    htmlFor="reference-number-input"
                    style={styles.label}
                  >
                    {LOCALIZE.testAdministration.uit.tabs.invitations.referenceNumber}
                  </label>
                </Col>
                <Col
                  xl={columnSizes.spacedSecondColumn.xl}
                  lg={columnSizes.spacedSecondColumn.lg}
                  md={columnSizes.spacedSecondColumn.md}
                  sm={columnSizes.spacedSecondColumn.sm}
                  xs={columnSizes.spacedSecondColumn.xs}
                  style={styles.dropdown}
                >
                  <input
                    id="reference-number-input"
                    className={this.state.IsValidReferenceNumber ? "valid-field" : "invalid-field"}
                    aria-required={true}
                    type="text"
                    style={{ ...styles.input, ...accommodationsStyle }}
                    value={this.state.referenceNumber}
                    onChange={this.getReferenceNumberContent}
                  ></input>
                </Col>
                {!this.state.IsValidReferenceNumber && (
                  <Col
                    xl={columnSizes.spacedSecondColumn.xl}
                    lg={columnSizes.spacedSecondColumn.lg}
                    md={columnSizes.spacedSecondColumn.md}
                    sm={columnSizes.spacedSecondColumn.sm}
                    xs={columnSizes.spacedSecondColumn.xs}
                  >
                    <label
                      htmlFor="reference-number-input"
                      style={{ ...styles.errorMessage, ...styles.errorMessagePadding }}
                    >
                      {LOCALIZE.testAdministration.uit.tabs.invitations.referenceNumberError}
                    </label>
                  </Col>
                )}
              </Row>
              <Row
                className="justify-content-end align-items-center"
                style={styles.elementContainer}
              >
                <Col
                  xl={columnSizes.spacedFirstColumn.xl}
                  lg={columnSizes.spacedFirstColumn.lg}
                  md={columnSizes.spacedFirstColumn.md}
                  sm={columnSizes.spacedFirstColumn.sm}
                  xs={columnSizes.spacedFirstColumn.xs}
                  style={styles.labelContainer}
                >
                  <label id="department-ministry-label" style={styles.label}>
                    {LOCALIZE.testAdministration.uit.tabs.invitations.departmentMinistry}
                  </label>
                </Col>
                <Col
                  xl={columnSizes.spacedSecondColumn.xl}
                  lg={columnSizes.spacedSecondColumn.lg}
                  md={columnSizes.spacedSecondColumn.md}
                  sm={columnSizes.spacedSecondColumn.sm}
                  xs={columnSizes.spacedSecondColumn.xs}
                  style={
                    this.state.isLoadingDepartmentMinistryOptions
                      ? { ...styles.dropdown, ...styles.centerText }
                      : styles.dropdown
                  }
                >
                  {this.state.isLoadingDepartmentMinistryOptions ? (
                    <label className="fa fa-spinner fa-spin">
                      <FontAwesomeIcon icon={faSpinner} />
                    </label>
                  ) : (
                    <DropdownSelect
                      idPrefix="department-ministry-dropdown"
                      ariaLabelledBy="department-ministry-label"
                      ariaRequired={true}
                      isMulti={false}
                      options={this.state.departmentMinistryOptions}
                      onChange={this.onSelectedDepartmentMinistryOptionChange}
                      defaultValue={this.state.departmentMinistry}
                      hasPlaceholder={true}
                      isValid={true}
                    ></DropdownSelect>
                  )}
                </Col>
              </Row>
              <Row
                className="justify-content-end align-items-center"
                style={styles.elementContainer}
              >
                <Col
                  xl={columnSizes.spacedFirstColumn.xl}
                  lg={columnSizes.spacedFirstColumn.lg}
                  md={columnSizes.spacedFirstColumn.md}
                  sm={columnSizes.spacedFirstColumn.sm}
                  xs={columnSizes.spacedFirstColumn.xs}
                  style={styles.labelContainer}
                >
                  <label
                    id="fis-organisation-code-label"
                    htmlFor="fis-organisation-code-input"
                    style={styles.label}
                  >
                    {LOCALIZE.testAdministration.uit.tabs.invitations.fisOrganisationCode}
                  </label>
                </Col>
                <Col
                  xl={columnSizes.spacedSecondColumn.xl}
                  lg={columnSizes.spacedSecondColumn.lg}
                  md={columnSizes.spacedSecondColumn.md}
                  sm={columnSizes.spacedSecondColumn.sm}
                  xs={columnSizes.spacedSecondColumn.xs}
                  style={styles.dropdown}
                >
                  <input
                    id="fis-organisation-code-input"
                    className={this.state.isValidFisOrgCode ? "valid-field" : "invalid-field"}
                    aria-required={true}
                    type="text"
                    style={{ ...styles.input, ...accommodationsStyle }}
                    value={this.state.fisOrgCode}
                    onChange={this.getFisOrgCodeContent}
                  ></input>
                </Col>
                {!this.state.isValidFisOrgCode && (
                  <Col
                    xl={columnSizes.spacedSecondColumn.xl}
                    lg={columnSizes.spacedSecondColumn.lg}
                    md={columnSizes.spacedSecondColumn.md}
                    sm={columnSizes.spacedSecondColumn.sm}
                    xs={columnSizes.spacedSecondColumn.xs}
                  >
                    <label
                      htmlFor="fis-organisation-code-input"
                      style={{ ...styles.errorMessage, ...styles.errorMessagePadding }}
                    >
                      {LOCALIZE.testAdministration.uit.tabs.invitations.fisOrganisationCodeError}
                    </label>
                  </Col>
                )}
              </Row>
              <Row
                className="justify-content-end align-items-center"
                style={styles.elementContainer}
              >
                <Col
                  xl={columnSizes.spacedFirstColumn.xl}
                  lg={columnSizes.spacedFirstColumn.lg}
                  md={columnSizes.spacedFirstColumn.md}
                  sm={columnSizes.spacedFirstColumn.sm}
                  xs={columnSizes.spacedFirstColumn.xs}
                  style={styles.labelContainer}
                >
                  <label
                    id="fis-reference-code-label"
                    htmlFor="fis-reference-code-input"
                    style={styles.label}
                  >
                    {LOCALIZE.testAdministration.uit.tabs.invitations.fisReferenceCode}
                  </label>
                </Col>
                <Col
                  xl={columnSizes.spacedSecondColumn.xl}
                  lg={columnSizes.spacedSecondColumn.lg}
                  md={columnSizes.spacedSecondColumn.md}
                  sm={columnSizes.spacedSecondColumn.sm}
                  xs={columnSizes.spacedSecondColumn.xs}
                  style={styles.dropdown}
                >
                  <input
                    id="fis-reference-code-input"
                    className={this.state.isValidFisRefCode ? "valid-field" : "invalid-field"}
                    aria-required={true}
                    type="text"
                    style={{ ...styles.input, ...accommodationsStyle }}
                    value={this.state.fisRefCode}
                    onChange={this.getFisRefCodeContent}
                  ></input>
                </Col>
                {!this.state.isValidFisRefCode && (
                  <Col
                    xl={columnSizes.spacedSecondColumn.xl}
                    lg={columnSizes.spacedSecondColumn.lg}
                    md={columnSizes.spacedSecondColumn.md}
                    sm={columnSizes.spacedSecondColumn.sm}
                    xs={columnSizes.spacedSecondColumn.xs}
                  >
                    <label
                      htmlFor="fis-reference-code-input"
                      style={{ ...styles.errorMessage, ...styles.errorMessagePadding }}
                    >
                      {LOCALIZE.testAdministration.uit.tabs.invitations.fisReferenceCodeError}
                    </label>
                  </Col>
                )}
              </Row>
              <Row
                className="justify-content-end align-items-center"
                style={styles.elementContainer}
              >
                <Col
                  xl={columnSizes.spacedFirstColumn.xl}
                  lg={columnSizes.spacedFirstColumn.lg}
                  md={columnSizes.spacedFirstColumn.md}
                  sm={columnSizes.spacedFirstColumn.sm}
                  xs={columnSizes.spacedFirstColumn.xs}
                  style={styles.labelContainer}
                >
                  <label
                    id="billing-contact-name-label"
                    htmlFor="billing-contact-name-input"
                    style={styles.label}
                  >
                    {LOCALIZE.testAdministration.uit.tabs.invitations.billingContactName}
                  </label>
                </Col>
                <Col
                  xl={columnSizes.spacedSecondColumn.xl}
                  lg={columnSizes.spacedSecondColumn.lg}
                  md={columnSizes.spacedSecondColumn.md}
                  sm={columnSizes.spacedSecondColumn.sm}
                  xs={columnSizes.spacedSecondColumn.xs}
                  style={styles.dropdown}
                >
                  <input
                    id="billing-contact-name-input"
                    className={
                      this.state.isValidBillingContactName ? "valid-field" : "invalid-field"
                    }
                    aria-required={true}
                    type="text"
                    style={{ ...styles.input, ...accommodationsStyle }}
                    value={this.state.billingContactName}
                    onChange={this.getBillingContactNameContent}
                  ></input>
                </Col>
                {!this.state.isValidBillingContactName && (
                  <Col
                    xl={columnSizes.spacedSecondColumn.xl}
                    lg={columnSizes.spacedSecondColumn.lg}
                    md={columnSizes.spacedSecondColumn.md}
                    sm={columnSizes.spacedSecondColumn.sm}
                    xs={columnSizes.spacedSecondColumn.xs}
                  >
                    <label
                      htmlFor="billing-contact-name-input"
                      style={{ ...styles.errorMessage, ...styles.errorMessagePadding }}
                    >
                      {LOCALIZE.testAdministration.uit.tabs.invitations.billingContactNameError}
                    </label>
                  </Col>
                )}
              </Row>
              <Row
                className="justify-content-end align-items-center"
                style={styles.elementContainer}
              >
                <Col
                  xl={columnSizes.spacedFirstColumn.xl}
                  lg={columnSizes.spacedFirstColumn.lg}
                  md={columnSizes.spacedFirstColumn.md}
                  sm={columnSizes.spacedFirstColumn.sm}
                  xs={columnSizes.spacedFirstColumn.xs}
                  style={styles.labelContainer}
                >
                  <label
                    id="billing-contact-info-label"
                    htmlFor="billing-contact-info-input"
                    style={styles.label}
                  >
                    {LOCALIZE.testAdministration.uit.tabs.invitations.billingInformation}
                  </label>
                </Col>
                <Col
                  xl={columnSizes.spacedSecondColumn.xl}
                  lg={columnSizes.spacedSecondColumn.lg}
                  md={columnSizes.spacedSecondColumn.md}
                  sm={columnSizes.spacedSecondColumn.sm}
                  xs={columnSizes.spacedSecondColumn.xs}
                  style={styles.dropdown}
                >
                  <input
                    id="billing-contact-info-input"
                    className={
                      this.state.isValidBillingContactInfo ? "valid-field" : "invalid-field"
                    }
                    aria-required={true}
                    type="text"
                    style={{ ...styles.input, ...accommodationsStyle }}
                    value={this.state.billingContactInfo}
                    onChange={this.getBillingContactInfoContent}
                  ></input>
                </Col>
                {!this.state.isValidBillingContactInfo && (
                  <Col
                    xl={columnSizes.spacedSecondColumn.xl}
                    lg={columnSizes.spacedSecondColumn.lg}
                    md={columnSizes.spacedSecondColumn.md}
                    sm={columnSizes.spacedSecondColumn.sm}
                    xs={columnSizes.spacedSecondColumn.xs}
                  >
                    <label
                      htmlFor="billing-contact-info-input"
                      style={{ ...styles.errorMessage, ...styles.errorMessagePadding }}
                    >
                      {LOCALIZE.testAdministration.uit.tabs.invitations.billingInformationError}
                    </label>
                  </Col>
                )}
              </Row>
            </div>
          </>
        )}
        <Row className="justify-content-end align-items-center" style={styles.elementContainer}>
          <Col
            xl={columnSizes.firstColumn.xl}
            lg={columnSizes.firstColumn.lg}
            md={columnSizes.firstColumn.md}
            sm={columnSizes.firstColumn.sm}
            xs={columnSizes.firstColumn.xs}
            style={styles.labelContainer}
          >
            <label id="uit-tests-label" style={styles.label}>
              {LOCALIZE.testAdministration.uit.tabs.invitations.test}
            </label>
          </Col>
          <Col
            xl={columnSizes.secondColumn.xl}
            lg={columnSizes.secondColumn.lg}
            md={columnSizes.secondColumn.md}
            sm={columnSizes.secondColumn.sm}
            xs={columnSizes.secondColumn.xs}
            style={styles.dropdown}
          >
            <DropdownSelect
              idPrefix="uit-tests-dropdown"
              ariaLabelledBy="uit-tests-label uit-tests-error-message uit-tests-current-value-accessibility"
              ariaRequired={true}
              isMulti={false}
              options={this.props.uitTestsOptions}
              onChange={this.onSelectedTestsOptionChange}
              defaultValue={this.props.uitTests}
              hasPlaceholder={true}
              isValid={this.state.testsDropdownDisabled ? true : !!this.state.isValidUitTests}
              isDisabled={this.state.testsDropdownDisabled}
              customRef={this.uitTestsRef}
              menuPlacement={this.props.orderlessTestOrderOptionSelected ? "top" : "auto"}
            ></DropdownSelect>
            <label id="uit-tests-current-value-accessibility" style={styles.hiddenText}>
              {LOCALIZE.formatString(
                LOCALIZE.testAdministration.uit.tabs.invitations.testCurrentValueAccessibility,
                this.state.uitTestsAccessibilityWording
              )}
            </label>
          </Col>
          {!this.state.testsDropdownDisabled && !this.state.isValidUitTests && (
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
            >
              <label
                id="uit-tests-error-message"
                style={{ ...styles.errorMessage, ...styles.errorMessagePadding }}
              >
                {LOCALIZE.testAdministration.uit.tabs.invitations.testsErrorMessage}
              </label>
            </Col>
          )}
        </Row>
        {Object.entries(this.props.uitTests).length > 0 &&
          this.props.orderlessTestOrderOptionSelected && (
            <div style={styles.subDataContainer}>
              <Row
                className="justify-content-end align-items-center"
                style={styles.elementContainer}
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={styles.labelContainer}
                >
                  <label id="reason-for-testing-label" style={styles.label}>
                    {LOCALIZE.testAdministration.uit.tabs.invitations.reasonForTesting}
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={
                    this.state.isLoadingLevelRequiredOptions
                      ? { ...styles.dropdown, ...styles.centerText }
                      : styles.dropdown
                  }
                >
                  {this.state.isLoadingLevelRequiredOptions ? (
                    <label className="fa fa-spinner fa-spin" style={styles.loading}>
                      <FontAwesomeIcon icon={faSpinner} />
                    </label>
                  ) : (
                    <DropdownSelect
                      idPrefix="reason-for-testing-dropdown"
                      ariaLabelledBy="reason-for-testing-label reason-for-testing-error-message"
                      ariaRequired={true}
                      isMulti={false}
                      options={
                        this.props.currentLanguage === LANGUAGES.english
                          ? this.state.reasonForTestingOptionsEn
                          : this.state.reasonForTestingOptionsFr
                      }
                      onChange={this.onSelectedReasonForTestingOptionChange}
                      defaultValue={this.state.reasonForTesting}
                      hasPlaceholder={true}
                      isValid={this.state.isValidReasonForTesting}
                      customRef={this.reasonForTestingRef}
                      orderByLabels={false}
                    ></DropdownSelect>
                  )}
                </Col>
                {!this.state.isValidReasonForTesting && (
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    <label
                      id="reason-for-testing-error-message"
                      style={{ ...styles.errorMessage, ...styles.errorMessagePadding }}
                    >
                      {LOCALIZE.testAdministration.uit.tabs.invitations.invalidReasonForTesting}
                    </label>
                  </Col>
                )}
              </Row>
              <Row
                className="justify-content-end align-items-center"
                style={styles.elementContainer}
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={styles.labelContainer}
                >
                  <label
                    id="level-required-label"
                    htmlFor={
                      this.state.showLevelRequiredRawScoreTextBox ? "level-required-input" : ""
                    }
                    style={styles.label}
                  >
                    {this.state.showLevelRequiredRawScoreTextBox
                      ? LOCALIZE.testAdministration.uit.tabs.invitations.levelRequiredRawScore
                      : LOCALIZE.testAdministration.uit.tabs.invitations.levelRequired}
                  </label>
                </Col>
                {this.state.showLevelRequiredRawScoreTextBox ? (
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={
                      this.state.isLoadingLevelRequiredOptions
                        ? { ...styles.dropdown, ...styles.centerText }
                        : styles.dropdown
                    }
                  >
                    {this.state.isLoadingLevelRequiredOptions ? (
                      <label className="fa fa-spinner fa-spin" style={styles.loading}>
                        <FontAwesomeIcon icon={faSpinner} />
                      </label>
                    ) : (
                      <input
                        id="level-required-input"
                        className={
                          this.state.isValidLevelRequired && this.state.isValidLevelRequiredRawScore
                            ? "valid-field"
                            : "invalid-field"
                        }
                        aria-required={true}
                        type="text"
                        style={{ ...styles.input, ...accommodationsStyle }}
                        value={this.state.levelRequired}
                        onChange={this.getLevelRequiredContent}
                      ></input>
                    )}
                  </Col>
                ) : (
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={
                      this.state.isLoadingLevelRequiredOptions
                        ? { ...styles.dropdown, ...styles.centerText }
                        : styles.dropdown
                    }
                  >
                    {this.state.isLoadingLevelRequiredOptions ? (
                      <label className="fa fa-spinner fa-spin" style={styles.loading}>
                        <FontAwesomeIcon icon={faSpinner} />
                      </label>
                    ) : (
                      <DropdownSelect
                        idPrefix="level-required-dropdown"
                        ariaLabelledBy="level-required-label level-required-error-message"
                        ariaRequired={true}
                        isMulti={false}
                        options={
                          this.props.currentLanguage === LANGUAGES.english
                            ? this.state.levelRequiredOptionsEn
                            : this.state.levelRequiredOptionsFr
                        }
                        onChange={this.onSelectedLevelRequiredOptionChange}
                        defaultValue={this.state.levelRequired}
                        isDisabled={this.state.levelRequiredScoringMethodNone}
                        hasPlaceholder={true}
                        isValid={this.state.isValidLevelRequired}
                        customRef={this.levelRequiredRef}
                        orderByLabels={false}
                      ></DropdownSelect>
                    )}
                  </Col>
                )}
                {!this.state.isValidLevelRequired && this.state.isValidLevelRequiredRawScore && (
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    <label
                      id="level-required-error-message"
                      htmlFor={
                        this.state.showLevelRequiredRawScoreTextBox ? "level-required-input" : ""
                      }
                      style={{ ...styles.errorMessage, ...styles.errorMessagePadding }}
                    >
                      {LOCALIZE.testAdministration.uit.tabs.invitations.invalidLevelRequired}
                    </label>
                  </Col>
                )}
                {!this.state.isValidLevelRequiredRawScore && (
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    <label
                      id="level-required-error-message"
                      htmlFor={"level-required-input"}
                      style={{ ...styles.errorMessage, ...styles.errorMessagePadding }}
                    >
                      {
                        LOCALIZE.testAdministration.uit.tabs.invitations
                          .invalidLevelRequiredRawScore
                      }
                    </label>
                  </Col>
                )}
              </Row>
            </div>
          )}
        {/* <Row className="justify-content-end align-items-center" style={styles.elementContainer}>
          <Col
            xl={columnSizes.firstColumn.xl}
            lg={columnSizes.firstColumn.lg}
            md={columnSizes.firstColumn.md}
            sm={columnSizes.firstColumn.sm}
            xs={columnSizes.firstColumn.xs}
            style={styles.labelContainer}
          >
            <label id="uit-email-template-label" style={styles.label}>
              {LOCALIZE.testAdministration.uit.tabs.invitations.emailTemplate}
            </label>
          </Col>
          <Col
            xl={columnSizes.secondColumn.xl}
            lg={columnSizes.secondColumn.lg}
            md={columnSizes.secondColumn.md}
            sm={columnSizes.secondColumn.sm}
            xs={columnSizes.secondColumn.xs}
            style={styles.dropdown}
          >
            <DropdownSelect
              idPrefix="uit-email-template-dropdown"
              ariaLabelledBy="uit-email-template-dropdown uit-email-template-current-value-accessibility"
              options={[]}
              onChange={() => {}}
              defaultValue={{ label: LOCALIZE.commons.default, value: null }}
              hasPlaceholder={true}
              isDisabled={true}
              dateDayFieldRef={this.validityEndDateRef}
            ></DropdownSelect>
            <label id="uit-email-template-current-value-accessibility" style={styles.hiddenText}>
              {
                LOCALIZE.testAdministration.uit.tabs.invitations
                  .emailTemplateCurrentValueAccessibility
              }
            </label>
          </Col>
        </Row> */}
        <Row className="justify-content-end align-items-center" style={styles.elementContainer}>
          <Col
            xl={columnSizes.firstColumn.xl}
            lg={columnSizes.firstColumn.lg}
            md={columnSizes.firstColumn.md}
            sm={columnSizes.firstColumn.sm}
            xs={columnSizes.firstColumn.xs}
            style={styles.labelContainer}
          >
            <label id="uit-candidates-label" style={styles.label}>
              {LOCALIZE.testAdministration.uit.tabs.invitations.candidates}
            </label>
          </Col>
          <Col
            xl={columnSizes.secondColumn.xl}
            lg={columnSizes.secondColumn.lg}
            md={columnSizes.secondColumn.md}
            sm={columnSizes.secondColumn.sm}
            xs={columnSizes.secondColumn.xs}
          >
            {this.state.isLoading && (
              <div style={styles.centerText}>
                <label className="fa fa-spinner fa-spin" style={styles.loading}>
                  <FontAwesomeIcon icon={faSpinner} />
                </label>
              </div>
            )}
            {!this.state.isLoading && (
              <>
                <div className="input-group">
                  <input
                    id="uit-candidates-upload-text-field"
                    className={
                      !Object.entries(this.props.uitTests).length > 0
                        ? "valid-field form-control transparent-borders"
                        : this.state.isValidFile &&
                          this.state.csvErrorsArray.length <= 0 &&
                          !this.state.displayNoDataInCsvFileError &&
                          !this.state.displayNoCsvFileSelectedError &&
                          !this.displayDuplicateEmailsError &&
                          this.state.isValidNumberOfCandidates
                        ? "valid-field form-control"
                        : "invalid-field form-control"
                    }
                    aria-invalid={
                      !(
                        this.state.isValidFile &&
                        this.state.csvErrorsArray.length <= 0 &&
                        !this.state.displayNoDataInCsvFileError &&
                        !this.state.displayNoCsvFileSelectedError &&
                        !this.displayDuplicateEmailsError &&
                        this.state.isValidNumberOfCandidates
                      )
                    }
                    type="text"
                    style={
                      Object.entries(this.props.uitTests).length > 0
                        ? {
                            ...styles.inputWithButtons,
                            ...styles.candidatesUploadTextField,
                            ...accommodationsStyle
                          }
                        : {
                            ...styles.inputWithButtons,
                            ...styles.candidatesUploadTextField,
                            ...accommodationsStyle,
                            ...styles.opacity
                          }
                    }
                    value={this.state.selectedFileDetails.name}
                    onChange={() => {}}
                    readOnly={true}
                    disabled={!Object.entries(this.props.uitTests).length > 0}
                  ></input>

                  <label htmlFor="uit-candidates-upload-text-field" style={styles.hiddenText}>
                    {LOCALIZE.formatString(
                      LOCALIZE.testAdministration.uit.tabs.invitations.fileSelectedAccessibility,
                      this.state.selectedFileDetails.name
                    )}
                  </label>

                  <div className="input-group-append">
                    <StyledTooltip
                      id="uit-candidates-upload-button-tooltip"
                      place="top"
                      type={TYPE.light}
                      effect={EFFECT.solid}
                      triggerType={[TRIGGER_TYPE.focus, TRIGGER_TYPE.hover]}
                      tooltipElement={
                        <CustomButton
                          dataTip=""
                          dataFor="uit-candidates-upload-button-tooltip"
                          label={
                            <>
                              <FontAwesomeIcon icon={faFolderOpen} />
                              <label style={styles.hiddenText}>
                                {LOCALIZE.testAdministration.uit.tabs.invitations.browseButton}
                              </label>
                            </>
                          }
                          action={this.handleUploadFile}
                          customStyle={{
                            ...styles.uploadDownloadIconButton,
                            ...styles.noBorderRight
                          }}
                          buttonTheme={`${THEME.SECONDARY} ${
                            this.state.isValidFile &&
                            !this.state.displayNoDataInCsvFileError &&
                            !this.state.displayNoCsvFileSelectedError
                              ? ""
                              : "custom-browse-button-error"
                          }`}
                          disabled={!Object.entries(this.props.uitTests).length > 0}
                        />
                      }
                      tooltipContent={
                        <div>
                          <p>{LOCALIZE.testAdministration.uit.tabs.invitations.browseButton}</p>
                        </div>
                      }
                    />
                    <StyledTooltip
                      id="uit-candidates-download-button-tooltip"
                      place="top"
                      type={TYPE.light}
                      effect={EFFECT.solid}
                      triggerType={[TRIGGER_TYPE.focus, TRIGGER_TYPE.hover]}
                      tooltipElement={
                        <CustomButton
                          dataTip=""
                          dataFor="uit-candidates-download-button-tooltip"
                          label={
                            <>
                              <FontAwesomeIcon icon={faDownload} />
                              <label style={styles.hiddenText}>
                                {
                                  LOCALIZE.testAdministration.uit.tabs.invitations
                                    .downloadTemplateButton
                                }
                              </label>
                            </>
                          }
                          action={this.handleDownloadTemplate}
                          customStyle={{
                            ...styles.uploadDownloadIconButton,
                            ...styles.borderRadius
                          }}
                          buttonTheme={`${THEME.SECONDARY} ${
                            this.state.isValidFile &&
                            !this.state.displayNoDataInCsvFileError &&
                            !this.state.displayNoCsvFileSelectedError
                              ? ""
                              : "custom-download-template-button-error"
                          }`}
                        />
                      }
                      tooltipContent={
                        <div>
                          <p>
                            {
                              LOCALIZE.testAdministration.uit.tabs.invitations
                                .downloadTemplateButton
                            }
                          </p>
                        </div>
                      }
                    />
                    <input
                      id="file-explorer-handler"
                      type="file"
                      tabIndex={-1}
                      // eslint-disable-next-line no-return-assign
                      ref={ref => (this.upload = ref)}
                      style={styles.hiddenText}
                      onChange={this.onFileSelection.bind(this)}
                    />
                  </div>
                </div>
              </>
            )}
          </Col>
          <>
            {" "}
            {!this.state.isLoading && (
              <>
                {this.state.displayInvalidFileTypeError && (
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    <label
                      htmlFor="uit-candidates-upload-text-field"
                      style={{ ...styles.errorMessage, ...styles.errorMessageTextAlign }}
                    >
                      {LOCALIZE.testAdministration.uit.tabs.invitations.wrongFileTypeError}
                    </label>
                  </Col>
                )}
                {!this.state.isValidFile &&
                  this.state.csvErrorsArray.length > 0 &&
                  !this.state.displayDuplicateEmailsError &&
                  !this.state.displayInvalidFileTypeError && (
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                    >
                      <label
                        htmlFor="uit-candidates-upload-text-field"
                        style={{ ...styles.errorMessage, ...styles.errorMessageTextAlign }}
                      >
                        {LOCALIZE.formatString(
                          LOCALIZE.testAdministration.uit.tabs.invitations.invalidFileError,
                          this.getFormattedErrorsString()
                        )}
                      </label>
                    </Col>
                  )}
                {this.state.isValidFile && this.state.displayNoDataInCsvFileError && (
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    <label
                      htmlFor="uit-candidates-upload-text-field"
                      style={{ ...styles.errorMessage, ...styles.errorMessageTextAlign }}
                    >
                      {LOCALIZE.testAdministration.uit.tabs.invitations.noDataInCsvFileError}
                    </label>
                  </Col>
                )}
                {this.state.displayNoCsvFileSelectedError && (
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    <label
                      htmlFor="uit-candidates-upload-text-field"
                      style={{ ...styles.errorMessage, ...styles.errorMessageTextAlign }}
                    >
                      {LOCALIZE.testAdministration.uit.tabs.invitations.noCsvFileSelectedError}
                    </label>
                  </Col>
                )}
                {this.state.isValidFile &&
                  !this.state.isValidNumberOfCandidates &&
                  !this.state.displayNoCsvFileSelectedError &&
                  !this.state.displayNoDataInCsvFileError &&
                  !this.state.displayInvalidFileTypeError && (
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                    >
                      <label
                        htmlFor="uit-candidates-upload-text-field"
                        style={{ ...styles.errorMessage, ...styles.errorMessageTextAlign }}
                      >
                        {
                          LOCALIZE.testAdministration.uit.tabs.invitations
                            .maximumInvitedCandideReachedError
                        }
                      </label>
                    </Col>
                  )}
                {this.state.displayDuplicateEmailsError && (
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    <label
                      htmlFor="uit-candidates-upload-text-field"
                      style={{ ...styles.errorMessage, ...styles.errorMessageTextAlign }}
                    >
                      {LOCALIZE.formatString(
                        LOCALIZE.testAdministration.uit.tabs.invitations
                          .duplicateEmailsFoundInCsvError,
                        this.state.csvErrorsArray[0],
                        this.state.csvErrorsArray[1]
                      )}
                    </label>
                  </Col>
                )}
              </>
            )}
          </>
        </Row>
        <Row style={styles.elementContainer}>
          <Col
            xl={12}
            lg={12}
            md={columnSizes.firstColumn.md}
            sm={columnSizes.firstColumn.sm}
            xs={columnSizes.firstColumn.xs}
            style={styles.labelContainer}
          >
            <label id="uit-validity-end-date-label" style={styles.label}>
              {LOCALIZE.testAdministration.uit.tabs.invitations.validityEndDate}
            </label>
          </Col>
          <Col
            xl={12}
            lg={12}
            md={columnSizes.secondColumn.md}
            sm={columnSizes.secondColumn.sm}
            xs={columnSizes.secondColumn.xs}
            style={styles.dropdown}
          >
            <DatePicker
              dateDayFieldRef={this.validityEndDateRef}
              dateLabelId={"uit-validity-end-date-label"}
              triggerValidation={this.state.triggerDateValidation}
              triggerResetFieldValues={this.state.triggerResetDatePickerFieldValues}
              customYearOptions={populateCustomFutureYearsDateOptions(1)}
              futureDateValidation={true}
              displayValidIcon={false}
              menuPlacement={"top"}
              isValidCompleteDatePicked={this.state.isValidNumberOfCandidatesBasedOnEndDate}
              customInvalidDateErrorMessage={
                !this.state.isValidNumberOfCandidatesBasedOnEndDate
                  ? LOCALIZE.testAdministration.uit.tabs.invitations
                      .numberOfCandidatesBasedOnEndDateExceededError
                  : ERROR_MESSAGE.default
              }
            />
          </Col>
        </Row>
        <Row className="justify-content-center align-items-center" style={styles.buttonContainer}>
          <Col xl={6} lg={6} md={6} sm={12} xs={12}>
            <CustomButton
              buttonId="uit-clear-all-fields-button"
              label={
                <>
                  <FontAwesomeIcon icon={faUndo} />
                  <span style={styles.buttonLabel}>
                    {LOCALIZE.testAdministration.uit.tabs.invitations.resetAllFields}
                  </span>
                </>
              }
              action={() => this.openResetAllFieldsPopup()}
              customStyle={styles.resetAllFieldsButton}
              buttonTheme={THEME.SECONDARY}
            />
          </Col>
          <Col xl={6} lg={6} md={6} sm={12} xs={12}>
            <CustomButton
              buttonId="uit-send-invitations-button"
              label={
                <>
                  <FontAwesomeIcon icon={faEnvelope} />
                  <span style={styles.buttonLabel}>
                    {LOCALIZE.testAdministration.uit.tabs.invitations.sendInvitesButton}
                  </span>
                </>
              }
              action={() => this.triggerFormValidation()}
              customStyle={styles.sendInvitesButton}
              type={"submit"}
              buttonTheme={THEME.PRIMARY}
            />
          </Col>
        </Row>
        {Object.entries(this.state.financialData).length > 0 && (
          <PopupBox
            show={this.state.showInvitesConfirmationPopup}
            size={this.state.displaySentInvitationsSuccessMessage ? "lg" : "xl"}
            title={
              this.state.displaySentInvitationsSuccessMessage
                ? LOCALIZE.testAdministration.uit.tabs.invitations.invitesConfirmationPopup
                    .titleInvitationsSentSuccessfull
                : this.state.displaySentInvitationsFailMessage
                ? LOCALIZE.testAdministration.uit.tabs.invitations.invitesConfirmationPopup
                    .titleInvitationsSentFail
                : LOCALIZE.testAdministration.uit.tabs.invitations.invitesConfirmationPopup.title
            }
            handleClose={() => {}}
            description={
              <div>
                {!this.state.displaySentInvitationsSuccessMessage &&
                  !this.state.displaySentInvitationsFailMessage && (
                    <div>
                      {this.props.orderlessTestOrderOptionSelected ? (
                        <div style={styles.popupFinancialDataContainer}>
                          <div>
                            <div style={styles.labelContainer}>
                              <label>
                                <span style={styles.popupLabel}>
                                  {
                                    LOCALIZE.testAdministration.uit.tabs.invitations
                                      .invitesConfirmationPopup.inviteData.referenceNumber
                                  }
                                </span>
                                <span>{this.state.financialData.referenceNumber}</span>
                              </label>
                            </div>
                          </div>
                          <div>
                            <div style={styles.labelContainer}>
                              <label>
                                <span style={styles.popupLabel}>
                                  {
                                    LOCALIZE.testAdministration.uit.tabs.invitations
                                      .invitesConfirmationPopup.inviteData.requestingDepartment
                                  }
                                </span>
                                <span>{this.state.financialData.departmentMinistry}</span>
                              </label>
                            </div>
                          </div>
                          <div>
                            <div style={styles.labelContainer}>
                              <label>
                                <span style={styles.popupLabel}>
                                  {
                                    LOCALIZE.testAdministration.uit.tabs.invitations
                                      .invitesConfirmationPopup.inviteData.fisOrganisationCode
                                  }
                                </span>
                                <span>{this.state.financialData.fisOrgCode}</span>
                              </label>
                            </div>
                          </div>
                          <div>
                            <div style={styles.labelContainer}>
                              <label>
                                <span style={styles.popupLabel}>
                                  {
                                    LOCALIZE.testAdministration.uit.tabs.invitations
                                      .invitesConfirmationPopup.inviteData.fisReferenceCode
                                  }
                                </span>
                                <span>{this.state.financialData.fisRefCode}</span>
                              </label>
                            </div>
                          </div>
                          <div>
                            <div style={styles.labelContainer}>
                              <label>
                                <span style={styles.popupLabel}>
                                  {
                                    LOCALIZE.testAdministration.uit.tabs.invitations
                                      .invitesConfirmationPopup.inviteData.HrCoordinator
                                  }
                                </span>
                                <span>{this.state.financialData.billingContactName}</span>
                              </label>
                            </div>
                          </div>
                          <div>
                            <div style={styles.labelContainer}>
                              <label>
                                <span style={styles.popupLabel}>
                                  {
                                    LOCALIZE.testAdministration.uit.tabs.invitations
                                      .invitesConfirmationPopup.inviteData.billingContactInfo
                                  }
                                </span>
                                <span>{this.state.financialData.billingContactInfo}</span>
                              </label>
                            </div>
                          </div>
                          <div>
                            <div style={styles.labelContainer}>
                              <label>
                                <span style={styles.popupLabel}>
                                  {
                                    LOCALIZE.testAdministration.uit.tabs.invitations
                                      .invitesConfirmationPopup.inviteData.validityEndDate
                                  }
                                </span>
                                <span>{getFormattedDate(this.props.completeDatePicked)}</span>
                              </label>
                            </div>
                          </div>
                          <div>
                            <div style={styles.labelContainer}>
                              <label>
                                <span style={styles.popupLabel}>
                                  {
                                    LOCALIZE.testAdministration.uit.tabs.invitations
                                      .invitesConfirmationPopup.inviteData.test
                                  }
                                </span>
                                <span>{this.state.financialData.test}</span>
                              </label>
                            </div>
                          </div>
                          <div>
                            <div style={styles.labelContainer}>
                              <label>
                                <span style={styles.popupLabel}>
                                  {
                                    LOCALIZE.testAdministration.uit.tabs.invitations
                                      .invitesConfirmationPopup.inviteData.reasonForTesting
                                  }
                                </span>
                                <span>{this.state.financialData.reasonForTesting}</span>
                              </label>
                            </div>
                          </div>
                          {this.state.showLevelRequiredRawScoreTextBox ? (
                            <div>
                              <div style={styles.labelContainer}>
                                <label>
                                  <span style={styles.popupLabel}>
                                    {
                                      LOCALIZE.testAdministration.uit.tabs.invitations
                                        .invitesConfirmationPopup.inviteData.levelRequiredRawScore
                                    }
                                  </span>
                                  <span>
                                    {this.state.levelRequired.length <= 0
                                      ? LOCALIZE.testAdministration.uit.tabs.invitations
                                          .providedInCsvOption
                                      : this.state.financialData.levelRequired}
                                  </span>
                                </label>
                              </div>
                            </div>
                          ) : (
                            <div>
                              <div style={styles.labelContainer}>
                                <label>
                                  <span style={styles.popupLabel}>
                                    {
                                      LOCALIZE.testAdministration.uit.tabs.invitations
                                        .invitesConfirmationPopup.inviteData.levelRequired
                                    }
                                  </span>
                                  <span>{this.state.financialData.levelRequired.label}</span>
                                </label>
                              </div>
                            </div>
                          )}
                        </div>
                      ) : (
                        <div style={styles.popupFinancialDataContainer}>
                          <div>
                            <div style={styles.labelContainer}>
                              <label>
                                <span style={styles.popupLabel}>
                                  {
                                    LOCALIZE.testAdministration.uit.tabs.invitations
                                      .invitesConfirmationPopup.inviteData.processNumber
                                  }
                                </span>
                                <span>{this.props.uitTestOrderNumber.label}</span>
                              </label>
                            </div>
                          </div>
                          <div>
                            <div style={styles.labelContainer}>
                              <label>
                                <span style={styles.popupLabel}>
                                  {
                                    LOCALIZE.testAdministration.uit.tabs.invitations
                                      .invitesConfirmationPopup.inviteData.requestingDepartment
                                  }
                                </span>
                                <span>{this.state.financialData.department_ministry_code}</span>
                              </label>
                            </div>
                          </div>
                          <div>
                            <div style={styles.labelContainer}>
                              <label>
                                <span style={styles.popupLabel}>
                                  {
                                    LOCALIZE.testAdministration.uit.tabs.invitations
                                      .invitesConfirmationPopup.inviteData.HrCoordinator
                                  }
                                </span>
                                <span>{`${this.state.financialData.billing_contact} (${this.state.financialData.billing_contact_info})`}</span>
                              </label>
                            </div>
                          </div>
                          <div>
                            <div style={styles.labelContainer}>
                              <label>
                                <span style={styles.popupLabel}>
                                  {
                                    LOCALIZE.testAdministration.uit.tabs.invitations
                                      .invitesConfirmationPopup.inviteData.validityEndDate
                                  }
                                </span>
                                <span>{getFormattedDate(this.props.completeDatePicked)}</span>
                              </label>
                            </div>
                          </div>
                          <div>
                            <div style={styles.labelContainer}>
                              <label>
                                <span style={styles.popupLabel}>
                                  {
                                    LOCALIZE.testAdministration.uit.tabs.invitations
                                      .invitesConfirmationPopup.inviteData.test
                                  }
                                </span>
                                <div style={styles.testsDivInPopup}>
                                  {this.state.financialData.test.map((test, index) => {
                                    return (
                                      <p key={index} style={styles.noPadding}>{`- ${
                                        test.test_code
                                      } (${test[`${this.props.currentLanguage}_name`]})`}</p>
                                    );
                                  })}
                                </div>
                              </label>
                            </div>
                          </div>
                        </div>
                      )}
                      <div style={styles.popupTableContainer}>
                        <p style={styles.popupTableLabel}>
                          {LOCALIZE.formatString(
                            LOCALIZE.testAdministration.uit.tabs.invitations
                              .invitesConfirmationPopup.inviteData.table.title,
                            `${this.state.rowsDefinition.data.length}`
                          )}
                        </p>
                        <GenericTable
                          classnamePrefix="uit-confirm-invited-candidates-popup"
                          columnsDefinition={columnsDefinition}
                          rowsDefinition={this.state.rowsDefinition}
                          emptyTableMessage={""}
                          currentlyLoading={this.state.isLoadingPopupTableData}
                          tableWithButtons={false}
                          triggerReRender={this.state.triggerPopupTableRerender}
                        />
                      </div>
                    </div>
                  )}
                {this.state.displaySentInvitationsSuccessMessage && (
                  <div>
                    <p>
                      {
                        <SystemMessage
                          messageType={MESSAGE_TYPE.success}
                          title={LOCALIZE.commons.success}
                          message={
                            LOCALIZE.testAdministration.uit.tabs.invitations
                              .invitesConfirmationPopup.inviteData.invitationSentSuccessfullMessage
                          }
                        />
                      }
                    </p>
                  </div>
                )}
                {this.state.displaySentInvitationsFailMessage && (
                  <div>
                    <p>
                      {
                        <SystemMessage
                          messageType={MESSAGE_TYPE.error}
                          title={LOCALIZE.commons.error}
                          message={
                            LOCALIZE.testAdministration.uit.tabs.invitations
                              .invitesConfirmationPopup.inviteData.invitationSentFailMessage
                          }
                        />
                      }
                    </p>
                  </div>
                )}
              </div>
            }
            leftButtonType={
              this.state.displaySentInvitationsFailMessage ||
              this.state.displaySentInvitationsSuccessMessage
                ? BUTTON_TYPE.none
                : BUTTON_TYPE.secondary
            }
            leftButtonTitle={LOCALIZE.commons.cancel}
            leftButtonLabel={LOCALIZE.commons.cancel}
            leftButtonIcon={faTimes}
            leftButtonAction={() => this.closePopup()}
            leftButtonState={
              this.state.isSendingInvitations ? BUTTON_STATE.disabled : BUTTON_STATE.enabled
            }
            rightButtonType={BUTTON_TYPE.primary}
            rightButtonTitle={
              this.state.isSendingInvitations ? (
                // eslint-disable-next-line jsx-a11y/label-has-associated-control
                <label className="fa fa-spinner fa-spin">
                  <FontAwesomeIcon icon={faSpinner} />
                </label>
              ) : this.state.displaySentInvitationsSuccessMessage ||
                this.state.displaySentInvitationsFailMessage ? (
                LOCALIZE.commons.ok
              ) : (
                LOCALIZE.testAdministration.uit.tabs.invitations.invitesConfirmationPopup.sendButton
              )
            }
            rightButtonLabel={
              this.state.displaySentInvitationsSuccessMessage ||
              this.state.displaySentInvitationsFailMessage
                ? LOCALIZE.commons.ok
                : LOCALIZE.testAdministration.uit.tabs.invitations.invitesConfirmationPopup
                    .sendButton
            }
            rightButtonIcon={
              this.state.isSendingInvitations ||
              this.state.displaySentInvitationsSuccessMessage ||
              this.state.displaySentInvitationsFailMessage
                ? ""
                : faEnvelope
            }
            rightButtonAction={
              this.state.displaySentInvitationsSuccessMessage ||
              this.state.displaySentInvitationsFailMessage
                ? () => this.resetAllFields(false)
                : () => this.handleSendInvitations()
            }
            rightButtonState={
              this.state.isSendingInvitations ? BUTTON_STATE.disabled : BUTTON_STATE.enabled
            }
          />
        )}
        <PopupBox
          show={this.state.showResetAllFieldsPopup}
          title={LOCALIZE.testAdministration.uit.tabs.invitations.resetAllFieldsPopup.title}
          handleClose={() => {}}
          size={"lg"}
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.warning}
                title={LOCALIZE.commons.warning}
                message={
                  <p>
                    {
                      LOCALIZE.testAdministration.uit.tabs.invitations.resetAllFieldsPopup
                        .description
                    }
                  </p>
                }
              />
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonIcon={faTimes}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonLabel={LOCALIZE.commons.cancel}
          leftButtonAction={() => this.closeResetAllFieldsPopup()}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonIcon={faUndo}
          rightButtonTitle={
            LOCALIZE.testAdministration.uit.tabs.invitations.resetAllFieldsPopup.resetButton
          }
          rightButtonLabel={
            LOCALIZE.testAdministration.uit.tabs.invitations.resetAllFieldsPopup.resetButton
          }
          rightButtonAction={() => this.resetAllFields(true)}
        />
        {/* using same labels as the supervised edit time popup */}
        <PopupBox
          show={this.state.showEditTestTimePopup}
          title={LOCALIZE.testAdministration.activeCandidates.editTimePopup.title}
          handleClose={() => {}}
          size={"lg"}
          onPopupOpen={this.calculateTotalTestTime}
          onPopupClose={this.onPopupClose}
          description={
            <div>
              <div>
                <p>
                  <SystemMessage
                    messageType={MESSAGE_TYPE.warning}
                    title={LOCALIZE.commons.warning}
                    message={
                      <>
                        <span style={styles.bold}>
                          {LOCALIZE.testAdministration.activeCandidates.editTimePopup.description1}
                        </span>
                      </>
                    }
                  />
                  <br />
                  <span>
                    {LOCALIZE.formatString(
                      LOCALIZE.testAdministration.activeCandidates.editTimePopup.description2,
                      <span style={styles.bold}>
                        {this.state.selectedCandidateData.Prenom_First_Name}
                      </span>,
                      <span style={styles.bold}>
                        {this.state.selectedCandidateData.Nom_de_famille_Last_Name}
                      </span>
                    )}
                  </span>
                </p>
              </div>
              {typeof this.state.selectedCandidateData.test_sections !== "undefined" &&
                this.state.selectedCandidateData.test_sections.map(
                  (assigned_test_section, index) => {
                    return (
                      <div key={index} style={styles.testSectionTimerMainContainer}>
                        <Row className="align-items-center justify-content-center">
                          <Col
                            xl={columnSizes.firstColumn.xl}
                            lg={columnSizes.firstColumn.lg}
                            md={columnSizes.firstColumn.md}
                            sm={columnSizes.firstColumn.sm}
                            xs={columnSizes.firstColumn.xs}
                            style={styles.testSectionTimerDescriptionContainer}
                          >
                            <p style={{ ...styles.bold, ...styles.centerText }}>
                              {assigned_test_section[`${this.props.currentLanguage}_title`]}
                            </p>
                            <p>
                              {assigned_test_section[`${this.props.currentLanguage}_description`]}
                            </p>
                          </Col>
                          <Col
                            xl={columnSizes.secondColumn.xl}
                            lg={columnSizes.secondColumn.lg}
                            md={columnSizes.secondColumn.md}
                            sm={columnSizes.secondColumn.sm}
                            xs={columnSizes.secondColumn.xs}
                            style={styles.testSectionTimerTimeContainer}
                          >
                            <SetTimer
                              index={index}
                              currentHours={this.state.currentHours[index]}
                              currentMinutes={this.state.currentMinutes[index]}
                            />
                          </Col>
                        </Row>
                      </div>
                    );
                  }
                )}
              <div>
                <p>{LOCALIZE.testAdministration.activeCandidates.editTimePopup.description3}</p>
                <p style={styles.bold}>
                  {LOCALIZE.formatString(
                    LOCALIZE.testAdministration.activeCandidates.editTimePopup.description4,
                    <span>{this.state.totalHours}</span>,
                    <span>{this.state.totalMinutes}</span>
                  )}
                </p>
              </div>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonCustomStyle={{ minWidth: 150 }}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeEditTestTimePopup}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={
            LOCALIZE.testAdministration.activeCandidates.editTimePopup.setTimerButton
          }
          rightButtonState={
            this.props.validTimer && !this.state.isRightPopupButtonDisabled
              ? BUTTON_STATE.enabled
              : BUTTON_STATE.disabled
          }
          rightButtonIcon={faClock}
          rightButtonIconCustomStyle={{ transform: "scale(1.25)" }}
          rightButtonAction={this.handleSetTimer}
        />
        {/* using same labels as the supervised edit time popup */}
        <PopupBox
          show={this.state.showAddEditBreakBankPopup}
          handleClose={() => {}}
          isBackdropStatic={true}
          shouldCloseOnEsc={false}
          onPopupClose={this.onPopupClose}
          title={LOCALIZE.testAdministration.activeCandidates.addEditBreakBankPopup.title}
          size="lg"
          description={
            <div>
              <div>
                <p>
                  <SystemMessage
                    messageType={MESSAGE_TYPE.warning}
                    title={LOCALIZE.commons.warning}
                    message={
                      <>
                        <span style={styles.bold}>
                          {LOCALIZE.testAdministration.activeCandidates.editTimePopup.description1}
                        </span>
                      </>
                    }
                  />
                  <br />
                  <span>
                    {LOCALIZE.formatString(
                      LOCALIZE.testAdministration.activeCandidates.addEditBreakBankPopup
                        .description,
                      <span style={styles.bold}>
                        {this.state.selectedCandidateData.candidate_first_name}
                      </span>,
                      <span style={styles.bold}>
                        {this.state.selectedCandidateData.candidate_last_name}
                      </span>
                    )}
                  </span>
                </p>
              </div>
              <div style={styles.breakBankMainContainer}>
                <Row className="align-items-center justify-content-center">
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.breakBankDescriptionContainer}
                  >
                    <p style={{ ...styles.bold, ...styles.centerText }}>
                      {
                        LOCALIZE.testAdministration.activeCandidates.addEditBreakBankPopup
                          .setTimeTitle
                      }
                    </p>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.breakBankMainContainer}
                  >
                    <SetTimer
                      index={0}
                      currentHours={this.state.currentHoursBreakBank[0]}
                      currentMinutes={this.state.currentMinutesBreakBank[0]}
                      customTimeValidationState={true}
                    />
                  </Col>
                </Row>
              </div>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonCustomStyle={{ minWidth: 150 }}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeAddEditBreakBankPopup}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={
            LOCALIZE.testAdministration.activeCandidates.addEditBreakBankPopup.setBreakBankButton
          }
          rightButtonState={
            this.props.validTimer && !this.state.isRightPopupButtonDisabled
              ? BUTTON_STATE.enabled
              : BUTTON_STATE.disabled
          }
          rightButtonIcon={faPauseCircle}
          rightButtonIconCustomStyle={{ transform: "scale(1.25)" }}
          rightButtonAction={this.handleAddEditBreakBank}
        />
      </div>
    );
  }
}

export { Invitations as unconnectedInvitations };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    uitTestOrderNumber: state.uit.uitTestOrderNumber,
    uitTests: state.uit.uitTests,
    completeDatePicked: state.datePicker.completeDatePicked,
    completeDateValidState: state.datePicker.completeDateValidState,
    orderlessTestOrderOptionSelected: state.userPermissions.orderlessTestOrderOptionSelected,
    isAae: state.userPermissions.isAae,
    currentTimer: state.timer.currentTimer,
    validTimer: state.timer.validTimer,
    timeUpdated: state.timer.timeUpdated
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateUitTestOrderNumberState,
      updateUitTestsState,
      resetUitInvitationsRelatedStates,
      getReasonsForTesting,
      sendUitInvitations,
      getTestPermissionFinancialData,
      triggerTablesRerender,
      getOrganization,
      getTaExtendedProfile,
      getSpecifiedTestScoringMethodData,
      updateOrderlessTestOrderOptionSelected,
      uitInvitationEndDateValidation,
      getTestData,
      updateTestTime,
      resetTimerStates,
      updateTimerState,
      setDefaultTimes,
      updateValidTimerState
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Invitations);
