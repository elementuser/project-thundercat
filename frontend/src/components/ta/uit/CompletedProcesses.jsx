import React, { Component } from "react";
import PropTypes from "prop-types";
import LOCALIZE from "../../../text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import GenericTable, { COMMON_STYLE } from "../../commons/GenericTable";
import {
  getAllCompletedUITProcessesForTA,
  updateUITCompletedPaginationPage,
  updateUITCompletedPaginationPageSize,
  updateSearchUitCompletedStates,
  getFoundCompletedUITProcessesForTA,
  getSelectedUITCompletedProcessesDetails,
  resetUitStates
} from "../../../modules/UitRedux";
import StyledTooltip, { TYPE, EFFECT, TRIGGER_TYPE } from "../../authentication/StyledTooltip";
import CustomButton, { THEME } from "../../commons/CustomButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBinoculars, faTimes } from "@fortawesome/free-solid-svg-icons";
import SearchBarWithDisplayOptions from "../../commons/SearchBarWithDisplayOptions";
import Pagination from "../../commons/Pagination";
import PopupBox, { BUTTON_TYPE } from "../../commons/PopupBox";
import { getUitTestStatus } from "../Constants";
import { LANGUAGES } from "../../commons/Translation";

const styles = {
  allUnset: {
    all: "unset"
  },
  faBinoculars: {
    width: "100%",
    minHeight: 20,
    textAlign: "center"
  },
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  popupTableLabel: {
    padding: "12px 0 24px 0"
  }
};

class CompletedProcesses extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  static propTypes = {
    getAllCompletedUITProcessesForTA: PropTypes.func,
    updateUITCompletedPaginationPage: PropTypes.func,
    updateUITCompletedPaginationPageSize: PropTypes.func,
    updateSearchUitCompletedStates: PropTypes.func
  };

  state = {
    rowsDefinition: {},
    currentlyLoading: false,
    allCompletedProcesses: [],
    nextPageNumber: 0,
    previousPageNumber: 0,
    numberOfPages: 1,
    resultsFound: 0,
    callingGetFoundCompletedUIT: false,
    displayResultsFound: false,
    showCompletedProcessDetailsPopup: false,
    completedProcessDetails: {},
    selectedCompletedProcessesDetails: {},
    rowsDefinitionPopup: {},
    currentlyLoadingForPopUp: false
  };

  componentDidMount = () => {
    // resetting redux states
    this.props.resetUitStates();
    // initialize pagination page redux state
    this.props.updateUITCompletedPaginationPage(1);
    this.populateCompletedProcesses();
  };

  componentDidUpdate = prevProps => {
    // if currentLanguage gets updated
    if (prevProps.currentLanguage !== this.props.currentLanguage) {
      this.populateUitCompletedBasedOnActiveSearch();
    }
    // if uitCompletedPaginationPage gets updated
    if (prevProps.uitCompletedPaginationPage !== this.props.uitCompletedPaginationPage) {
      this.populateUitCompletedBasedOnActiveSearch();
    }
    // if uitCompletedPaginationPageSize get updated
    if (prevProps.uitCompletedPaginationPageSize !== this.props.uitCompletedPaginationPageSize) {
      this.populateUitCompletedBasedOnActiveSearch();
    }
    // if search uitCompletedKeyword gets updated
    if (prevProps.uitCompletedKeyword !== this.props.uitCompletedKeyword) {
      // if uitCompletedKeyword redux state is empty
      if (this.props.uitCompletedKeyword !== "") {
        this.populateUitCompletedBasedOnActiveSearch();
      } else if (this.props.uitCompletedKeyword === "" && this.props.uitCompletedActiveSearch) {
        this.populateUitCompletedBasedOnActiveSearch();
      }
    }
    // if uitCompletedActiveSearch gets updated
    if (prevProps.uitCompletedActiveSearch !== this.props.uitCompletedActiveSearch) {
      this.populateUitCompletedBasedOnActiveSearch();
    }
    // if triggerUitTablesRerender gets updated
    if (prevProps.triggerUitTablesRerender !== this.props.triggerUitTablesRerender) {
      this.populateUitCompletedBasedOnActiveSearch();
    }
  };

  componentWillUnmount = () => {
    this._isMounted = false;
  };

  populateCompletedProcesses = () => {
    this._isMounted = true;
    const allCompletedProcessesArray = [];
    this.setState({ currentlyLoading: true }, () => {
      this.props
        .getAllCompletedUITProcessesForTA(
          this.props.uitCompletedPaginationPage,
          this.props.uitCompletedPaginationPageSize
        )
        .then(response => {
          if (this._isMounted) {
            this.populateCompletedProcessesObject(allCompletedProcessesArray, response);
          }
        })
        .then(() => {
          if (this._isMounted) {
            this.setState({
              currentlyLoading: false
            });
          }
        });
    });
  };

  populateCompletedProcessesObject = (allCompletedProcessesArray, response) => {
    // initializing needed object and array for rowsDefinition props (needed for GenericTable component)
    let rowsDefinition = {};
    const data = [];
    // making sure that there are some results
    if (typeof response.results !== "undefined") {
      // looping in response given
      for (let i = 0; i < response.results.length; i++) {
        const currentResult = response.results[i];
        // pushing needed data in allCompletedProcessesArray
        allCompletedProcessesArray.push({
          id: currentResult.id,
          process_number: currentResult.test_permission_data.staffing_process_number,
          test: currentResult.test_permission_data.test.test_code,
          total_candidates: currentResult.total_candidates,
          total_tests_taken: currentResult.total_tests_taken,
          validity_end_date: currentResult.validity_end_date
        });
        // pushing needed data in data array (needed for GenericTable component)
        data.push({
          column_1: currentResult.test_permission_data.staffing_process_number,
          column_2: currentResult.test_permission_data.test.test_code,
          column_3: `${currentResult.total_tests_taken}/${currentResult.total_candidates}`,
          column_4:
            this.props.currentLanguage === LANGUAGES.english
              ? `${currentResult.test_permission_data.department_ministry_data.edesc} (${currentResult.test_permission_data.department_ministry_data.eabrv})`
              : `${currentResult.test_permission_data.department_ministry_data.fdesc} (${currentResult.test_permission_data.department_ministry_data.fabrv})`,
          column_5: currentResult.validity_end_date,
          column_6: this.populateColumnSix(i, currentResult)
        });

        // updating rowsDefinition object with provided data and needed style
        rowsDefinition = {
          column_1_style: COMMON_STYLE.LEFT_TEXT,
          column_2_style: COMMON_STYLE.CENTERED_TEXT,
          column_3_style: COMMON_STYLE.CENTERED_TEXT,
          column_4_style: COMMON_STYLE.CENTERED_TEXT,
          column_5_style: COMMON_STYLE.CENTERED_TEXT,
          column_6_style: COMMON_STYLE.CENTERED_TEXT,
          data: data
        };

        // saving results in state
        this.setState({
          rowsDefinition: rowsDefinition,
          allCompletedProcesses: allCompletedProcessesArray,
          nextPageNumber: response.next_page_number,
          previousPageNumber: response.previous_page_number,
          results_found: response.results.count
        });
      }
    }
    this.setState({
      numberOfPages: Math.ceil(response.count / this.props.uitCompletedPaginationPageSize)
    });
  };

  populateColumnSix = (i, response) => {
    return (
      <div>
        <StyledTooltip
          id={`view-selected-completed-processes-${i}`}
          place="top"
          type={TYPE.light}
          effect={EFFECT.solid}
          triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
          tooltipElement={
            <div style={styles.allUnset}>
              <CustomButton
                dataTip=""
                dataFor={`view-selected-completed-processes-${i}`}
                label={<FontAwesomeIcon icon={faBinoculars} style={styles.faBinoculars} />}
                action={() => this.openCompletedProcessDetailsPopup(i)}
                customStyle={styles.actionButton}
                buttonTheme={THEME.SECONDARY}
                ariaLabel={LOCALIZE.formatString(
                  LOCALIZE.testAdministration.uit.tabs.completedProcessess.table
                    .viewButtonAccessibility,
                  response.test_permission_data.staffing_process_number
                )}
              />
            </div>
          }
          tooltipContent={
            <div>
              <p>{LOCALIZE.testAdministration.uit.tabs.completedProcessess.table.actionTooltip}</p>
            </div>
          }
        />
      </div>
    );
  };

  populateUitCompletedBasedOnActiveSearch = () => {
    // current search
    if (this.props.uitCompletedActiveSearch) {
      this.populateFoundCompletedProcesses();
      // no search
    } else {
      this.populateCompletedProcesses();
    }
  };

  populateFoundCompletedProcesses = () => {
    this.setState(
      { currentlyLoading: true },
      () => {
        // adding small delay to avoi calling that API multiple times
        setTimeout(() => {
          // if callingGetFoundCompletedUIT is set to false
          if (!this.state.callingGetFoundCompletedUIT) {
            // updating callingGetFoundCompletedUIT to true (while the API is being called)
            this.setState({ callingGetFoundCompletedUIT: true });
            const allCompletedProcessesArray = [];
            this.props
              .getFoundCompletedUITProcessesForTA(
                this.props.currentLanguage,
                this.props.uitCompletedKeyword,
                this.props.uitCompletedPaginationPage,
                this.props.uitCompletedPaginationPageSize
              )
              .then(response => {
                // there are no results found
                if (response[0] === "no results found" || response.results.length <= 0) {
                  this.setState({
                    allCompletedProcesses: [],
                    rowsDefinition: {},
                    numberOfPages: 1,
                    nextPageNumber: 0,
                    previousPageNumber: 0,
                    resultsFound: 0
                  });
                  // there is at least one result found
                } else {
                  this.populateCompletedProcessesObject(allCompletedProcessesArray, response);
                  this.setState({ resultsFound: response.count });
                }
              })
              .then(() => {
                this.setState(
                  {
                    currentlyLoading: false,
                    displayResultsFound: true,
                    callingGetFoundCompletedUIT: false
                  },
                  () => {
                    // if there is at least one result found
                    if (allCompletedProcessesArray.length > 0) {
                      // make sure that this element exsits to avoid any error
                      if (document.getElementById("completed-processes-results-found")) {
                        document.getElementById("completed-processes-results-found").focus();
                      }
                      // no results found
                    } else {
                      // make sure that this element exsits to avoid any error
                      if (document.getElementById("completed-processes-no-data")) {
                        // focusing on no results found row
                        document.getElementById("completed-processes-no-data").focus();
                      }
                    }
                  }
                );
              });
          }
        });
      },
      100
    );
  };

  populateSelectedCompletedProcessDetailsTable = () => {
    let rowsDefinitionPopup = {};
    const data = [];
    const selectedCompletedProcessesArray = [];
    this.setState({ currentlyLoadingForPopUp: true }, () => {
      this.props
        .getSelectedUITCompletedProcessesDetails(this.state.completedProcessDetails.id)
        .then(response => {
          for (let i = 0; i < response.candidates_data.length; i++) {
            selectedCompletedProcessesArray.push({
              candidate_email: response.candidates_data[i].email,
              test_status: response.candidates_data[i].test_status
            });
            // pushing needed data in data array (needed for GenericTable component)
            data.push({
              column_1: response.candidates_data[i].email,
              column_2: getUitTestStatus(
                response.candidates_data[i].test_status,
                response.candidates_data[i].code_deactivated
              )
            });

            // updating rowsDefinition object with provided data and needed style
            rowsDefinitionPopup = {
              column_1_style: COMMON_STYLE.LEFT_TEXT,
              column_2_style: COMMON_STYLE.LEFT_TEXT,
              data: data
            };

            // saving results in state
            this.setState({
              rowsDefinitionPopup: rowsDefinitionPopup,
              selectedCompletedProcessesDetails: selectedCompletedProcessesArray
            });
          }
        })
        .then(() => {
          this.setState({ currentlyLoadingForPopUp: false });
        });
    });
  };

  openCompletedProcessDetailsPopup = index => {
    this.setState(
      {
        showCompletedProcessDetailsPopup: true,
        completedProcessDetails: this.state.allCompletedProcesses[index]
      },
      () => {
        this.populateSelectedCompletedProcessDetailsTable();
      }
    );
  };

  closeCompletedProcessDetailsPopup = () => {
    this.setState({ showCompletedProcessDetailsPopup: false, completedProcessDetails: {} });
  };

  render() {
    const columnsDefinition = [
      {
        label: LOCALIZE.testAdministration.uit.tabs.completedProcessess.table.process,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label: LOCALIZE.testAdministration.uit.tabs.completedProcessess.table.test,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.testAdministration.uit.tabs.completedProcessess.table.totalCandidates,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.testAdministration.uit.tabs.completedProcessess.table.testsTaken,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.testAdministration.uit.tabs.completedProcessess.table.endDate,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.testAdministration.uit.tabs.completedProcessess.table.actions,
        style: COMMON_STYLE.CENTERED_TEXT
      }
    ];

    const columnsDefinitionPopup = [
      {
        label: LOCALIZE.testAdministration.uit.tabs.activeProcessess.selectedProcessPopup.columnOne,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label: LOCALIZE.testAdministration.uit.tabs.activeProcessess.selectedProcessPopup.columnTwo,
        style: COMMON_STYLE.LEFT_TEXT
      }
    ];

    return (
      <div>
        <div>
          <SearchBarWithDisplayOptions
            idPrefix={"completed-processes"}
            currentlyLoading={this.state.currentlyLoading}
            updateSearchStates={this.props.updateSearchUitCompletedStates}
            updatePageState={this.props.updateUITCompletedPaginationPage}
            paginationPageSize={Number(this.props.uitCompletedPaginationPageSize)}
            updatePaginationPageSize={this.props.updateUITCompletedPaginationPageSize}
            data={this.state.allCompletedProcesses}
            resultsFound={this.state.resultsFound}
          />
          <div>
            <GenericTable
              classnamePrefix="completed-processes"
              columnsDefinition={columnsDefinition}
              rowsDefinition={this.state.rowsDefinition}
              emptyTableMessage={
                LOCALIZE.testAdministration.uit.tabs.completedProcessess.noCompletedProcessess
              }
              currentlyLoading={this.state.currentlyLoading}
            />
          </div>
          <Pagination
            paginationContainerId={"completed-processes-pagination-pages-link"}
            pageCount={this.state.numberOfPages}
            paginationPage={this.props.uitCompletedPaginationPage}
            updatePaginationPageState={this.props.updateUITCompletedPaginationPage}
            firstTableRowId={"completed-processes-table-row-0"}
          />
        </div>
        {Object.entries(this.state.completedProcessDetails).length > 0 && (
          <PopupBox
            show={this.state.showCompletedProcessDetailsPopup}
            title={LOCALIZE.formatString(
              LOCALIZE.testAdministration.uit.tabs.completedProcessess.popup.title,
              this.state.completedProcessDetails.test,
              this.state.completedProcessDetails.process_number
            )}
            handleClose={() => {}}
            description={
              <div>
                <p style={styles.popupTableLabel}>
                  {LOCALIZE.formatString(
                    LOCALIZE.testAdministration.uit.tabs.completedProcessess.popup.description,
                    `${
                      this.state.currentlyLoadingForPopUp
                        ? LOCALIZE.commons.na
                        : typeof this.state.rowsDefinitionPopup.data !== "undefined" &&
                          this.state.rowsDefinitionPopup.data.length
                    }`
                  )}
                </p>
                <GenericTable
                  classnamePrefix="view-selected-active-processes"
                  columnsDefinition={columnsDefinitionPopup}
                  rowsDefinition={this.state.rowsDefinitionPopup}
                  emptyTableMessage={""}
                  currentlyLoading={this.state.currentlyLoadingForPopUp}
                />
              </div>
            }
            rightButtonType={BUTTON_TYPE.primary}
            rightButtonTitle={LOCALIZE.commons.close}
            rightButtonLabel={LOCALIZE.commons.close}
            rightButtonIcon={faTimes}
            rightButtonAction={this.closeCompletedProcessDetailsPopup}
          />
        )}
      </div>
    );
  }
}

export { CompletedProcesses as unconnectedCompletedProcesses };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    uitCompletedPaginationPageSize: state.uit.uitCompletedPaginationPageSize,
    uitCompletedPaginationPage: state.uit.uitCompletedPaginationPage,
    uitCompletedKeyword: state.uit.uitCompletedKeyword,
    uitCompletedActiveSearch: state.uit.uitCompletedActiveSearch,
    triggerUitTablesRerender: state.uit.triggerUitTablesRerender
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAllCompletedUITProcessesForTA,
      updateUITCompletedPaginationPage,
      updateUITCompletedPaginationPageSize,
      updateSearchUitCompletedStates,
      getFoundCompletedUITProcessesForTA,
      getSelectedUITCompletedProcessesDetails,
      resetUitStates
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(CompletedProcesses);
