import LOCALIZE from "../../text_resources";

// reference: assigned_test_status.py
const TEST_STATUS = {
  ASSIGNED: 11,
  READY: 12,
  ACTIVE: 13,
  LOCKED: 14,
  PAUSED: 15,
  QUIT: 19,
  SUBMITTED: 20,
  UNASSIGNED: 21,
  PRE_TEST: 22
};

// UIT test status
const UIT_TEST_STATUS = {
  TAKEN: 91,
  NOT_TAKEN: 92,
  IN_PROGRESS: 93,
  UNASSIGNED: 94
};

function getUitTestStatus(uitTestStatus, codeDeactivated) {
  switch (uitTestStatus) {
    case UIT_TEST_STATUS.IN_PROGRESS:
      return LOCALIZE.testAdministration.uit.tabs.activeProcessess.selectedProcessPopup
        .testInProgress;
    case UIT_TEST_STATUS.TAKEN:
      return LOCALIZE.testAdministration.uit.tabs.activeProcessess.selectedProcessPopup.testTaken;
    case UIT_TEST_STATUS.UNASSIGNED:
      return LOCALIZE.testAdministration.uit.tabs.activeProcessess.selectedProcessPopup
        .testUnassigned;
    default:
      if (codeDeactivated) {
        return LOCALIZE.testAdministration.uit.tabs.activeProcessess.selectedProcessPopup
          .testCodeDeactivated;
      }
      return LOCALIZE.testAdministration.uit.tabs.activeProcessess.selectedProcessPopup
        .testNotTaken;
  }
}

export default TEST_STATUS;
export { UIT_TEST_STATUS, getUitTestStatus };
