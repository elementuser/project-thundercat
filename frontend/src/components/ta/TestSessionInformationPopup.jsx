import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { styles as AssignTestAccessesStyle } from "../etta/test_accesses/AssignTestAccesses";
import { getTestPermissionFinancialData } from "../../modules/PermissionsRedux";
import DropdownSelect from "../commons/DropdownSelect";
import {
  updateTestOrderNumberState,
  updateTestToAdministerState,
  updateTestToAdministerDropdownDisabledState,
  updateTestSessionLanguageState,
  updateTestSessionLanguageDropdownDisabledState,
  updateGenerateButtonDisabledState,
  resetTestAdministrationStates,
  updateOrderlessFinancialDataState
} from "../../modules/TestAdministrationRedux";
import { BUTTON_STATE } from "../commons/PopupBox";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";
import { LANGUAGES } from "../commons/Translation";
import { getOrganization } from "../../modules/ExtendedProfileOptionsRedux";
import { getTaExtendedProfile } from "../../modules/UserProfileRedux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import { Col, Row } from "react-bootstrap";
import { validateEmail } from "../../helpers/regexValidator";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 5,
    xl: 5
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 7,
    xl: 7
  }
};

const styles = {
  mainPopupContainer: {
    padding: "10px 15px",
    width: "100%"
  },
  billingInfoContainer: {
    width: "90%",
    margin: "8px auto",
    border: "1px solid #CECECE",
    backgroundColor: "#F3F3F3",
    padding: 6
  },
  noPadding: {
    padding: 0
  },
  boldText: {
    fontWeight: "bold"
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  col: {
    width: "100%"
  },
  row: {
    marginBottom: "15px"
  },
  orderlessDataContainer: {
    paddingLeft: 48
  },
  input: {
    backgroundColor: "white",
    minHeight: 38,
    border: "1px solid #00565e",
    width: "100%",
    padding: "3px 6px 3px 6px"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6,
    width: "100%"
  },
  centerText: {
    textAlign: "center"
  }
};

class TestSessionInformationPopup extends Component {
  static propTypes = {
    testOrderNumberOptions: PropTypes.array.isRequired,
    populateTestToAdministerOptions: PropTypes.func.isRequired,
    testToAdministerOptions: PropTypes.array.isRequired,
    populateTestSessionLanguageOptions: PropTypes.func.isRequired,
    testSessionLanguageOptions: PropTypes.array.isRequired,
    // provided by redux
    getTestPermissionFinancialData: PropTypes.func,
    updateTestOrderNumberState: PropTypes.func,
    updateTestToAdministerState: PropTypes.func,
    updateTestToAdministerDropdownDisabledState: PropTypes.func,
    updateTestSessionLanguageState: PropTypes.func,
    updateTestSessionLanguageDropdownDisabledState: PropTypes.func,
    updateGenerateButtonDisabledState: PropTypes.func,
    resetTestAdministrationStates: PropTypes.func
  };

  state = {
    financialDataVisible: false,
    financialData: {},
    referenceNumber: "",
    IsValidReferenceNumber: true,
    isLoadingDepartmentMinistryOptions: true,
    departmentMinistryOptions: [],
    departmentMinistry: [],
    fisOrgCode: "",
    isValidFisOrgCode: true,
    fisRefCode: "",
    isValidFisRefCode: true,
    billingContactName: "",
    isValidBillingContactName: true,
    billingContactInfo: "",
    isValidBillingContactInfo: true
  };

  componentDidMount = () => {
    // resetting redux states
    this.props.resetTestAdministrationStates();
  };

  componentDidUpdate = prevProps => {
    // if testOrderNumber gets updated
    if (prevProps.testOrderNumber !== this.props.testOrderNumber) {
      // orderless option selected
      if (this.props.testOrderNumber.value === 0) {
        this.populateDepartmentMinistryOptions();
      }
    } // if testToAdminister and/or testSessionLanguage gets updated
    if (
      prevProps.testToAdminister !== this.props.testToAdminister ||
      prevProps.testSessionLanguage !== this.props.testSessionLanguage
    ) {
      // validating orderless financial data
      this.validateOrderlessFinancialData();
    }
  };

  // get selected test order number option + setting needed states
  onSelectedTestOrderNumberOptionChange = selectedOption => {
    // updating redux states
    this.props.updateTestOrderNumberState(selectedOption);
    this.props.updateTestToAdministerDropdownDisabledState(false);
    this.props.updateTestToAdministerState("");
    this.props.updateTestSessionLanguageDropdownDisabledState(true);
    this.props.updateTestSessionLanguageState("");
    this.props.updateGenerateButtonDisabledState(BUTTON_STATE.disabled);

    // populating test to administer options
    this.props.populateTestToAdministerOptions(selectedOption);

    // disabling generate button
    this.props.updateGenerateButtonDisabledState(BUTTON_STATE.disabled);
    // resetting financial data state
    this.setState({ financialData: {}, financialDataVisible: false });
  };

  // get selected test to administer option + setting needed states
  onSelectedTestToAdministerOptionChange = selectedOption => {
    // updating redux states
    this.props.updateTestToAdministerState(selectedOption);
    this.props.updateTestSessionLanguageDropdownDisabledState(false);
    this.props.updateTestSessionLanguageState("");
    this.props.updateGenerateButtonDisabledState(BUTTON_STATE.disabled);

    // populating test session language options
    this.props.populateTestSessionLanguageOptions();
    // resetting financial data state
    this.setState({ financialData: {}, financialDataVisible: false });
  };

  // get selected test session language option + setting needed states + getting and setting financial data + enabling generate button
  onSelectedTestSessionLanguageOptionChange = selectedOption => {
    this.props.updateTestSessionLanguageState(selectedOption);
    // test order number option selected (not orderless)
    if (this.props.testOrderNumber.value !== 0) {
      // getting test permission's financial data
      this.props
        .getTestPermissionFinancialData(
          this.props.testOrderNumber.value,
          this.props.testToAdminister.value
        )
        .then(response => {
          // getting all needed financial data
          const staffingProcessNumber = response[0].staffing_process_number;
          const departmentMinistry = response[0].department_ministry_code;
          const contact = `${response[0].billing_contact} (${response[0].billing_contact_info})`;
          const isOrg = response[0].is_org;
          const isRef = response[0].is_ref;
          // saving data in state
          this.setState({
            financialData: {
              staffingProcessNumber: staffingProcessNumber,
              departmentMinistry: departmentMinistry,
              contact: contact,
              isOrg: isOrg,
              isRef: isRef
            },
            financialDataVisible: true
          });
          // enabling generate button
          this.props.updateGenerateButtonDisabledState(BUTTON_STATE.enabled);
        });
    }
  };

  // get reference number content
  getReferenceNumberContent = event => {
    const referenceNumber = event.target.value;
    // allow only alphanumeric, slash and dash (0 to 25 chars)
    const regexExpression = /^([a-zA-zÀ-ÿ0-9-/]{0,25})$/;
    if (regexExpression.test(referenceNumber)) {
      this.setState({ referenceNumber: referenceNumber, IsValidReferenceNumber: true });
    }
    if (referenceNumber.length <= 0) {
      this.setState({ IsValidReferenceNumber: false });
    }
    // adding small delay to make sure that the states are updated
    setTimeout(() => {
      // validating orderless financial data
      this.validateOrderlessFinancialData();
    }, 100);
  };

  // get FIS organisation code content
  getFisOrgCodeContent = event => {
    const fisOrgCode = event.target.value;
    // allow maximum of 16 chars (prevent use of comma)
    const regexExpression = /^([^,]{0,16})$/;
    if (regexExpression.test(fisOrgCode)) {
      this.setState({ fisOrgCode: fisOrgCode, isValidFisOrgCode: true });
    }
    if (fisOrgCode.length <= 0) {
      this.setState({ isValidFisOrgCode: false });
    }
    // adding small delay to make sure that the states are updated
    setTimeout(() => {
      // validating orderless financial data
      this.validateOrderlessFinancialData();
    }, 100);
  };

  // get FIS reference code content
  getFisRefCodeContent = event => {
    const fisRefCode = event.target.value;
    // allow maximum of 20 chars (prevent use of comma)
    const regexExpression = /^([^,]{0,20})$/;
    if (regexExpression.test(fisRefCode)) {
      this.setState({ fisRefCode: fisRefCode, isValidFisRefCode: true });
    }
    if (fisRefCode.length <= 0) {
      this.setState({ isValidFisRefCode: false });
    }
    // adding small delay to make sure that the states are updated
    setTimeout(() => {
      // validating orderless financial data
      this.validateOrderlessFinancialData();
    }, 100);
  };

  // get Billing contact name content
  getBillingContactNameContent = event => {
    const billingContactName = event.target.value;
    // allow maximum of 180 chars (prevent use of comma)
    const regexExpression = /^([^,]{0,180})$/;
    if (regexExpression.test(billingContactName)) {
      this.setState({ billingContactName: billingContactName, isValidBillingContactName: true });
    }
    if (billingContactName.length <= 0) {
      this.setState({ isValidBillingContactName: false });
    }
    // adding small delay to make sure that the states are updated
    setTimeout(() => {
      // validating orderless financial data
      this.validateOrderlessFinancialData();
    }, 100);
  };

  // get Billing contact information content
  getBillingContactInfoContent = event => {
    const billingContactInfo = event.target.value;
    // allow maximum of 255 chars
    const regexExpression = /^(.{0,255})$/;
    if (regexExpression.test(billingContactInfo)) {
      this.setState({ billingContactInfo: billingContactInfo, isValidBillingContactInfo: true });
    }
    if (!validateEmail(billingContactInfo)) {
      this.setState({ isValidBillingContactInfo: false });
    }
    // adding small delay to make sure that the states are updated
    setTimeout(() => {
      // validating orderless financial data
      this.validateOrderlessFinancialData();
    }, 100);
  };

  // get selected tests options + setting needed states
  onSelectedDepartmentMinistryOptionChange = selectedOption => {
    this.setState({ departmentMinistry: selectedOption });
  };

  populateDepartmentMinistryOptions = () => {
    this.setState({ isLoadingDepartmentMinistryOptions: true }, () => {
      const departmentMinistryOptions = [];
      this.props
        .getOrganization()
        .then(response => {
          for (let i = 0; i < response.body.length; i++) {
            // Interface is in English
            if (this.props.currentLanguage === LANGUAGES.english) {
              departmentMinistryOptions.push({
                label: `${response.body[i].edesc} (${response.body[i].eabrv})`,
                value: `${response.body[i].dept_id}`
              });
              // Interface is in French
            } else {
              departmentMinistryOptions.push({
                label: `${response.body[i].fdesc} (${response.body[i].fabrv})`,
                value: `${response.body[i].dept_id}`
              });
            }
          }
          this.setState({ departmentMinistryOptions: departmentMinistryOptions });
        })
        .then(() => {
          // getting TA extended profile data
          this.props.getTaExtendedProfile().then(response => {
            let initialDepartmentMinistrySelectedOption = [];
            // associated department/ministry found
            if (Object.entries(response.body).length > 0) {
              initialDepartmentMinistrySelectedOption = {
                label: `${response.body[`department_name_${this.props.currentLanguage}`]} (${
                  response.body[`department_abrv_${this.props.currentLanguage}`]
                })`,
                value: `${response.body.department_id}`
              };
            }
            this.setState({
              departmentMinistry: initialDepartmentMinistrySelectedOption,
              isLoadingDepartmentMinistryOptions: false
            });
          });
        });
    });
  };

  validateOrderlessFinancialData = () => {
    // updating orderless financial data redux state
    this.props.updateOrderlessFinancialDataState({
      referenceNumber: this.state.referenceNumber,
      departmentMinistryId: this.state.departmentMinistry.value,
      fisOrganisationCode: this.state.fisOrgCode,
      fisReferenceCode: this.state.fisRefCode,
      billingContactName: this.state.billingContactName,
      billingContactInfo: this.state.billingContactInfo
    });
    // all fields are filled and valid
    if (
      this.state.referenceNumber !== "" &&
      this.state.IsValidReferenceNumber &&
      this.state.fisOrgCode !== "" &&
      this.state.isValidFisOrgCode &&
      this.state.fisRefCode !== "" &&
      this.state.isValidFisRefCode &&
      this.state.billingContactInfo !== "" &&
      this.state.isValidBillingContactInfo &&
      this.state.billingContactName !== "" &&
      this.state.isValidBillingContactName &&
      Object.entries(this.props.testToAdminister).length > 0 &&
      Object.entries(this.props.testSessionLanguage).length > 0
    ) {
      // enabling generate button
      this.props.updateGenerateButtonDisabledState(BUTTON_STATE.enabled);
      // one or more fields are empty or invalid
    } else {
      // disabling generate button
      this.props.updateGenerateButtonDisabledState(BUTTON_STATE.disabled);
    }
  };

  render() {
    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }
    return (
      <div style={styles.mainPopupContainer}>
        <Row className="align-items-center" style={styles.row}>
          <Col style={styles.col}>
            <p>
              {LOCALIZE.testAdministration.testAccessCodes.generateTestAccessCodePopup.description}
            </p>
          </Col>
        </Row>
        <Row className="align-items-center" style={styles.row}>
          <Col
            xl={columnSizes.firstColumn.xl}
            lg={columnSizes.firstColumn.lg}
            md={columnSizes.firstColumn.md}
            sm={columnSizes.firstColumn.sm}
            xs={columnSizes.firstColumn.xs}
            style={styles.col}
          >
            <label id="test-order-number-label" style={AssignTestAccessesStyle.label}>
              {
                LOCALIZE.testAdministration.testAccessCodes.generateTestAccessCodePopup
                  .testOrderNumber
              }
            </label>
          </Col>
          <Col
            xl={columnSizes.secondColumn.xl}
            lg={columnSizes.secondColumn.lg}
            md={columnSizes.secondColumn.md}
            sm={columnSizes.secondColumn.sm}
            xs={columnSizes.secondColumn.xs}
            style={styles.col}
          >
            <DropdownSelect
              idPrefix="generate-test-access-code-popup-test-order-number-dropdown"
              ariaLabelledBy="test-order-number-label test-order-number-current-value-accessibility"
              options={this.props.testOrderNumberOptions}
              onChange={this.onSelectedTestOrderNumberOptionChange}
              defaultValue={this.props.testOrderNumber}
              hasPlaceholder={true}
              orderByLabels={false}
            ></DropdownSelect>
            <label id="test-order-number-current-value-accessibility" style={styles.hiddenText}>
              {
                LOCALIZE.testAdministration.testAccessCodes.generateTestAccessCodePopup
                  .testOrderNumberCurrentValueAccessibility
              }
            </label>
          </Col>
        </Row>
        {Object.entries(this.props.testOrderNumber).length > 0 &&
          this.props.testOrderNumber.value === 0 && (
            <div style={styles.orderlessDataContainer}>
              <Row className="align-items-center justify-content-end" style={styles.row}>
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={styles.col}
                >
                  <label
                    id="generate-test-access-code-reference-number-label"
                    htmlFor="generate-test-access-code-reference-number-input"
                    style={AssignTestAccessesStyle.label}
                  >
                    {
                      LOCALIZE.testAdministration.testAccessCodes.generateTestAccessCodePopup
                        .referenceNumber
                    }
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={styles.col}
                >
                  <input
                    id="generate-test-access-code-reference-number-input"
                    className={this.state.IsValidReferenceNumber ? "valid-field" : "invalid-field"}
                    aria-required={true}
                    type="text"
                    style={{ ...styles.input, ...accommodationsStyle }}
                    value={this.state.referenceNumber}
                    onChange={this.getReferenceNumberContent}
                  ></input>
                </Col>
                {!this.state.IsValidReferenceNumber && (
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.col}
                  >
                    <label
                      htmlFor="generate-test-access-code-reference-number-input"
                      style={{ ...styles.errorMessage, ...styles.errorMessagePadding }}
                    >
                      {LOCALIZE.commons.fieldCannotBeEmptyErrorMessage}
                    </label>
                  </Col>
                )}
              </Row>
              <Row className="align-items-center justify-content-end" style={styles.row}>
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={styles.col}
                >
                  <label
                    id="generate-test-access-code-department-ministry-label"
                    htmlFor="generate-test-access-code-department-ministry-input"
                    style={AssignTestAccessesStyle.label}
                  >
                    {
                      LOCALIZE.testAdministration.testAccessCodes.generateTestAccessCodePopup
                        .departmentMinistry
                    }
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={
                    this.state.isLoadingDepartmentMinistryOptions
                      ? { ...styles.col, ...styles.centerText }
                      : styles.col
                  }
                >
                  {this.state.isLoadingDepartmentMinistryOptions ? (
                    // eslint-disable-next-line jsx-a11y/label-has-associated-control
                    <label className="fa fa-spinner fa-spin">
                      <FontAwesomeIcon icon={faSpinner} />
                    </label>
                  ) : (
                    <DropdownSelect
                      idPrefix="generate-test-access-code-department-ministry"
                      ariaLabelledBy="generate-test-access-code-department-ministry-label"
                      options={this.state.departmentMinistryOptions}
                      onChange={this.onSelectedDepartmentMinistryOptionChange}
                      defaultValue={this.state.departmentMinistry}
                      hasPlaceholder={true}
                      orderByLabels={false}
                    ></DropdownSelect>
                  )}
                </Col>
              </Row>
              <Row className="align-items-center justify-content-end" style={styles.row}>
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={styles.col}
                >
                  <label
                    id="generate-test-access-code-fis-org-code-label"
                    htmlFor="generate-test-access-code-fis-org-code-input"
                    style={AssignTestAccessesStyle.label}
                  >
                    {
                      LOCALIZE.testAdministration.testAccessCodes.generateTestAccessCodePopup
                        .fisOrganisationCode
                    }
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={styles.col}
                >
                  <input
                    id="generate-test-access-code-fis-org-code-input"
                    className={this.state.isValidFisOrgCode ? "valid-field" : "invalid-field"}
                    aria-required={true}
                    type="text"
                    style={{ ...styles.input, ...accommodationsStyle }}
                    value={this.state.fisOrgCode}
                    onChange={this.getFisOrgCodeContent}
                  ></input>
                </Col>
                {!this.state.isValidFisOrgCode && (
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.col}
                  >
                    <label
                      htmlFor="generate-test-access-code-fis-org-code-input"
                      style={{ ...styles.errorMessage, ...styles.errorMessagePadding }}
                    >
                      {LOCALIZE.commons.fieldCannotBeEmptyErrorMessage}
                    </label>
                  </Col>
                )}
              </Row>
              <Row className="align-items-center justify-content-end" style={styles.row}>
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={styles.col}
                >
                  <label
                    id="generate-test-access-code-fis-ref-code-label"
                    htmlFor="generate-test-access-code-fis-ref-code-input"
                    style={AssignTestAccessesStyle.label}
                  >
                    {
                      LOCALIZE.testAdministration.testAccessCodes.generateTestAccessCodePopup
                        .fisReferenceCode
                    }
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={styles.col}
                >
                  <input
                    id="generate-test-access-code-fis-ref-code-input"
                    className={this.state.isValidFisRefCode ? "valid-field" : "invalid-field"}
                    aria-required={true}
                    type="text"
                    style={{ ...styles.input, ...accommodationsStyle }}
                    value={this.state.fisRefCode}
                    onChange={this.getFisRefCodeContent}
                  ></input>
                </Col>
                {!this.state.isValidFisRefCode && (
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.col}
                  >
                    <label
                      htmlFor="generate-test-access-code-fis-ref-code-input"
                      style={{ ...styles.errorMessage, ...styles.errorMessagePadding }}
                    >
                      {LOCALIZE.commons.fieldCannotBeEmptyErrorMessage}
                    </label>
                  </Col>
                )}
              </Row>
              <Row className="align-items-center justify-content-end" style={styles.row}>
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={styles.col}
                >
                  <label
                    id="generate-test-access-code-billing-contact-name-label"
                    htmlFor="generate-test-access-code-billing-contact-name-input"
                    style={AssignTestAccessesStyle.label}
                  >
                    {
                      LOCALIZE.testAdministration.testAccessCodes.generateTestAccessCodePopup
                        .billingContactName
                    }
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={styles.col}
                >
                  <input
                    id="generate-test-access-code-billing-contact-name-input"
                    className={
                      this.state.isValidBillingContactName ? "valid-field" : "invalid-field"
                    }
                    aria-required={true}
                    type="text"
                    style={{ ...styles.input, ...accommodationsStyle }}
                    value={this.state.billingContactName}
                    onChange={this.getBillingContactNameContent}
                  ></input>
                </Col>
                {!this.state.isValidBillingContactName && (
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.col}
                  >
                    <label
                      htmlFor="generate-test-access-code-billing-contact-name-input"
                      style={{ ...styles.errorMessage, ...styles.errorMessagePadding }}
                    >
                      {LOCALIZE.commons.fieldCannotBeEmptyErrorMessage}
                    </label>
                  </Col>
                )}
              </Row>
              <Row className="align-items-center justify-content-end" style={styles.row}>
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={styles.col}
                >
                  <label
                    id="generate-test-access-code-billing-contact-info-label"
                    htmlFor="generate-test-access-code-billing-contact-info-input"
                    style={AssignTestAccessesStyle.label}
                  >
                    {
                      LOCALIZE.testAdministration.testAccessCodes.generateTestAccessCodePopup
                        .billingInfo
                    }
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={styles.col}
                >
                  <input
                    id="generate-test-access-code-billing-contact-info-input"
                    className={
                      this.state.isValidBillingContactInfo ? "valid-field" : "invalid-field"
                    }
                    aria-required={true}
                    type="text"
                    style={{ ...styles.input, ...accommodationsStyle }}
                    value={this.state.billingContactInfo}
                    onChange={this.getBillingContactInfoContent}
                  ></input>
                </Col>
                {!this.state.isValidBillingContactInfo && (
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.col}
                  >
                    <label
                      htmlFor="generate-test-access-code-billing-contact-info-input"
                      style={{ ...styles.errorMessage, ...styles.errorMessagePadding }}
                    >
                      {
                        LOCALIZE.testAdministration.testAccessCodes.generateTestAccessCodePopup
                          .mustBeAValidEmailErrorMessage
                      }
                    </label>
                  </Col>
                )}
              </Row>
            </div>
          )}
        <Row className="align-items-center" style={styles.row}>
          <Col
            xl={columnSizes.firstColumn.xl}
            lg={columnSizes.firstColumn.lg}
            md={columnSizes.firstColumn.md}
            sm={columnSizes.firstColumn.sm}
            xs={columnSizes.firstColumn.xs}
            style={styles.col}
          >
            <label id="test-to-administer-label" style={AssignTestAccessesStyle.label}>
              {
                LOCALIZE.testAdministration.testAccessCodes.generateTestAccessCodePopup
                  .testToAdminister
              }
            </label>
          </Col>
          <Col
            xl={columnSizes.secondColumn.xl}
            lg={columnSizes.secondColumn.lg}
            md={columnSizes.secondColumn.md}
            sm={columnSizes.secondColumn.sm}
            xs={columnSizes.secondColumn.xs}
            style={styles.col}
          >
            <DropdownSelect
              idPrefix="generate-test-access-code-popup-test-dropdown"
              ariaLabelledBy="test-to-administer-label test-to-administer-current-value-accessibility"
              options={this.props.testToAdministerOptions}
              onChange={this.onSelectedTestToAdministerOptionChange}
              defaultValue={this.props.testToAdminister}
              hasPlaceholder={true}
              isDisabled={this.props.testToAdministerDropdownDisabled}
            ></DropdownSelect>
            <label id="test-to-administer-current-value-accessibility" style={styles.hiddenText}>
              {
                LOCALIZE.testAdministration.testAccessCodes.generateTestAccessCodePopup
                  .testToAdministerCurrentValueAccessibility
              }
            </label>
          </Col>
        </Row>
        <Row className="align-items-center">
          <Col
            xl={columnSizes.firstColumn.xl}
            lg={columnSizes.firstColumn.lg}
            md={columnSizes.firstColumn.md}
            sm={columnSizes.firstColumn.sm}
            xs={columnSizes.firstColumn.xs}
            style={styles.col}
          >
            <label id="test-session-language-label" style={AssignTestAccessesStyle.label}>
              {
                LOCALIZE.testAdministration.testAccessCodes.generateTestAccessCodePopup
                  .testSessionLanguage
              }
            </label>
          </Col>
          <Col
            xl={columnSizes.secondColumn.xl}
            lg={columnSizes.secondColumn.lg}
            md={columnSizes.secondColumn.md}
            sm={columnSizes.secondColumn.sm}
            xs={columnSizes.secondColumn.xs}
            style={styles.col}
          >
            <DropdownSelect
              idPrefix="generate-test-access-code-popup-test-session-language-dropdown"
              ariaLabelledBy="test-session-language-label test-session-language-current-value-accessibility"
              options={this.props.testSessionLanguageOptions}
              onChange={this.onSelectedTestSessionLanguageOptionChange}
              defaultValue={this.props.testSessionLanguage}
              hasPlaceholder={true}
              isDisabled={this.props.testSessionLanguageDropdownDisabled}
              menuPlacement={"top"}
            ></DropdownSelect>
            <label id="test-session-language-current-value-accessibility" style={styles.hiddenText}>
              {
                LOCALIZE.testAdministration.testAccessCodes.generateTestAccessCodePopup
                  .testSessionLanguageCurrentValueAccessibility
              }
            </label>
          </Col>
        </Row>
        {this.state.financialDataVisible &&
          this.props.testToAdminister.value !== "" &&
          this.props.testSessionLanguage.value !== "" && (
            <Col style={AssignTestAccessesStyle.fieldSeparator}>
              <Row className="align-items-center" style={AssignTestAccessesStyle.labelContainer}>
                <label id="billing-info-label" style={AssignTestAccessesStyle.label}>
                  {
                    LOCALIZE.testAdministration.testAccessCodes.generateTestAccessCodePopup
                      .billingInformation.title
                  }
                </label>
              </Row>
              <Row className="align-items-center" style={styles.billingInfoContainer}>
                <Col>
                  <Row className="align-items-center">
                    <p className="col" style={styles.noPadding}>
                      <span style={styles.boldText}>
                        {
                          LOCALIZE.testAdministration.testAccessCodes.generateTestAccessCodePopup
                            .billingInformation.staffingProcessNumber
                        }
                      </span>{" "}
                      <span>{this.state.financialData.staffingProcessNumber}</span>
                    </p>
                  </Row>
                  <Row className="align-items-center">
                    <p className="col" style={styles.noPadding}>
                      <span style={styles.boldText}>
                        {
                          LOCALIZE.testAdministration.testAccessCodes.generateTestAccessCodePopup
                            .billingInformation.departmentMinistry
                        }
                      </span>{" "}
                      <span>{this.state.financialData.departmentMinistry}</span>
                    </p>
                  </Row>
                  <Row className="align-items-center">
                    <p className="col" style={styles.noPadding}>
                      <span style={styles.boldText}>
                        {
                          LOCALIZE.testAdministration.testAccessCodes.generateTestAccessCodePopup
                            .billingInformation.contact
                        }
                      </span>{" "}
                      <span>{this.state.financialData.contact}</span>
                    </p>
                  </Row>
                  <Row className="align-items-center">
                    <p className="col" style={styles.noPadding}>
                      <span style={styles.boldText}>
                        {
                          LOCALIZE.testAdministration.testAccessCodes.generateTestAccessCodePopup
                            .billingInformation.isOrg
                        }
                      </span>{" "}
                      <span>{this.state.financialData.isOrg}</span>
                    </p>
                  </Row>
                  <Row className="align-items-center">
                    <p className="col" style={styles.noPadding}>
                      <span style={styles.boldText}>
                        {
                          LOCALIZE.testAdministration.testAccessCodes.generateTestAccessCodePopup
                            .billingInformation.isRef
                        }
                      </span>{" "}
                      <span>{this.state.financialData.isRef}</span>
                    </p>
                  </Row>
                </Col>
              </Row>
            </Col>
          )}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    accommodations: state.accommodations,
    currentLanguage: state.localize.language,
    username: state.user.username,
    testOrderNumber: state.testAdministration.testOrderNumber,
    testToAdminister: state.testAdministration.testToAdminister,
    testToAdministerDropdownDisabled: state.testAdministration.testToAdministerDropdownDisabled,
    testSessionLanguage: state.testAdministration.testSessionLanguage,
    testSessionLanguageDropdownDisabled:
      state.testAdministration.testSessionLanguageDropdownDisabled
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getTestPermissionFinancialData,
      updateTestOrderNumberState,
      updateTestToAdministerState,
      updateTestToAdministerDropdownDisabledState,
      updateTestSessionLanguageState,
      updateTestSessionLanguageDropdownDisabledState,
      updateGenerateButtonDisabledState,
      resetTestAdministrationStates,
      getOrganization,
      getTaExtendedProfile,
      updateOrderlessFinancialDataState
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(TestSessionInformationPopup);
