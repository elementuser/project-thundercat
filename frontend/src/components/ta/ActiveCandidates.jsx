import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { styles as TestAccessCodesStyle } from "./TestAccessCodes";
import GenericTable, { COMMON_STYLE } from "../commons/GenericTable";
import PopupBox, { BUTTON_TYPE, BUTTON_STATE } from "../commons/PopupBox";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faTimes,
  faClock,
  faLock,
  faLockOpen,
  faSpinner,
  faThumbsUp,
  // faEdit,
  faTrashAlt,
  faPauseCircle
} from "@fortawesome/free-solid-svg-icons";
import { getTimeInHoursMinutes } from "../../helpers/timeConversion";
import SetTimer from "../commons/SetTimer";
import {
  resetTimerStates,
  updateTimerState,
  setDefaultTimes,
  updateValidTimerState
} from "../../modules/SetTimerRedux";
import { getAssignedTests } from "../../modules/AssignedTestsRedux";
import {
  approveCandidate,
  unAssignCandidate,
  updateTestTime,
  lockCandidateTest,
  unlockCandidateTest,
  lockAllCandidatesTest,
  unlockAllCandidatesTest,
  updateBreakBank
} from "../../modules/TestAdministrationRedux";
import TEST_STATUS from "./Constants";
import { styles as TestAdministrationStyle } from "./TestAdministration";
import CustomButton, { THEME } from "../commons/CustomButton";
import { Row, Col } from "react-bootstrap";
import SystemMessage, { MESSAGE_TYPE } from "../commons/SystemMessage";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 4,
    xl: 4
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 8,
    xl: 8
  }
};

const styles = {
  bold: {
    fontWeight: "bold"
  },
  floatLeft: {
    float: "left"
  },
  floatRight: {
    float: "right"
  },
  lockUnlockAllButton: {
    minWidth: 150,
    margin: "0 24px 12px"
  },
  buttonIcon: {
    transform: "scale(1.5)",
    padding: "1.5px",
    marginRight: 12
  },
  testSectionTimerMainContainer: {
    margin: "12px 0"
  },
  testSectionTimerDescriptionContainer: {
    paddingLeft: 33,
    verticalAlign: "middle"
  },
  breakBankMainContainer: {
    margin: "12px 0"
  },
  breakBankDescriptionContainer: {
    paddingLeft: 33,
    verticalAlign: "middle"
  },
  testSectionTimerTimeContainer: {
    verticalAlign: "middle"
  },
  centerText: {
    textAlign: "center"
  }
};

class ActiveCandidates extends Component {
  static propTypes = {
    testAdministratorAssignedCandidates: PropTypes.array.isRequired,
    rowsDefinition: PropTypes.object.isRequired,
    currentlyLoading: PropTypes.bool.isRequired,
    selectedCandidateData: PropTypes.object.isRequired,
    triggerShowEditTimePopup: PropTypes.bool.isRequired,
    triggerShowAddEditBreakBankPopup: PropTypes.bool.isRequired,
    triggerShowLockPopup: PropTypes.bool.isRequired,
    triggerShowUnlockPopup: PropTypes.bool.isRequired,
    triggerApproveCandidate: PropTypes.bool.isRequired,
    triggerUnAssignCandidate: PropTypes.bool.isRequired,
    populateTestAdministratorAssignedCandidates: PropTypes.func.isRequired,
    updateAssignedCandidatesTable: PropTypes.func.isRequired,
    triggerReRender: PropTypes.bool,
    taAction: PropTypes.func.isRequired,
    // provided by redux
    updateTestTime: PropTypes.func,
    updateTimerState: PropTypes.func,
    setDefaultTimes: PropTypes.func,
    resetTimerStates: PropTypes.func,
    approveCandidate: PropTypes.func,
    lockCandidateTest: PropTypes.func,
    unlockCandidateTest: PropTypes.func,
    lockAllCandidatesTest: PropTypes.func,
    unlockAllCandidatesTest: PropTypes.func,
    getAssignedTests: PropTypes.func
  };

  state = {
    testAdministratorAssignedCandidates: [],
    rowsDefinition: {},
    selectedCandidateData: {},
    showEditTimePopup: false,
    showAddEditBreakBankPopup: false,
    currentHours: [],
    currentHoursBreakBank: ["00"],
    totalHours: 0,
    currentMinutes: [],
    currentMinutesBreakBank: ["00"],
    totalMinutes: 0,
    timedAssignedTestSection: [],
    showLockPopup: false,
    showUnlockPopup: false,
    allTestsLockedFlag: false,
    atLeastOneTestToLockUnlock: false,
    showLockAllPopup: false,
    showUnlockAllPopup: false,
    showUnAssignPopup: false,
    isRightPopupButtonDisabled: false,
    testTimeRemaining: undefined
  };

  componentDidMount = () => {
    this.props.resetTimerStates();
  };

  componentDidUpdate = prevProps => {
    // if testAdministratorAssignedCandidates gets updated
    if (
      prevProps.testAdministratorAssignedCandidates !==
      this.props.testAdministratorAssignedCandidates
    ) {
      // initializing allTestsLockedFlag to true
      let allTestsLockedFlag = true;
      let atLeastOneTestToLockUnlock = false;
      // looping in testAdministratorAssignedCandidates array
      for (let i = 0; i < this.props.testAdministratorAssignedCandidates.length; i++) {
        // if these is at least one test that is ready, pre-test, active or paused
        if (
          this.props.testAdministratorAssignedCandidates[i].status === TEST_STATUS.READY ||
          this.props.testAdministratorAssignedCandidates[i].status === TEST_STATUS.PRE_TEST ||
          this.props.testAdministratorAssignedCandidates[i].status === TEST_STATUS.ACTIVE ||
          this.props.testAdministratorAssignedCandidates[i].status === TEST_STATUS.PAUSED
        ) {
          // set allTestsLockedFlag to false
          allTestsLockedFlag = false;
        }
        // if there is at least one test to lock/unlock
        if (
          this.props.testAdministratorAssignedCandidates[i].status === TEST_STATUS.READY ||
          this.props.testAdministratorAssignedCandidates[i].status === TEST_STATUS.PRE_TEST ||
          this.props.testAdministratorAssignedCandidates[i].status === TEST_STATUS.ACTIVE ||
          this.props.testAdministratorAssignedCandidates[i].status === TEST_STATUS.LOCKED ||
          this.props.testAdministratorAssignedCandidates[i].status === TEST_STATUS.PAUSED
        ) {
          // set allTestsLockedFlag to false
          atLeastOneTestToLockUnlock = true;
        }
      }
      this.setState({
        testAdministratorAssignedCandidates: this.props.testAdministratorAssignedCandidates,
        allTestsLockedFlag: allTestsLockedFlag,
        atLeastOneTestToLockUnlock: atLeastOneTestToLockUnlock
      });

      if (this.props.testAdministratorAssignedCandidates.length !== 0) {
        let activeTestStatus = false;
        for (let i = 0; i < this.props.testAdministratorAssignedCandidates.length; i++) {
          if (this.props.testAdministratorAssignedCandidates[i].status === TEST_STATUS.ACTIVE) {
            activeTestStatus = true;
          }
        }
        if (activeTestStatus) {
          if (this.state.testTimeRemaining) {
            clearInterval(this.state.testTimeRemaining);
          }
          // call update assigned candidates table every 1 minute to update remaining time left on candidate test
          const interval = setInterval(this.props.updateAssignedCandidatesTable, 60 * 1000);

          this.setState({ testTimeRemaining: interval });
        }
      }
    }
    // if rowsDefinition gets updated
    if (prevProps.rowsDefinition !== this.props.rowsDefinition) {
      this.setState({ rowsDefinition: this.props.rowsDefinition });
    }
    // if triggerShowEditTimePopup gets updated
    if (prevProps.triggerShowEditTimePopup !== this.props.triggerShowEditTimePopup) {
      this.setState({ showEditTimePopup: true });
    }
    // if triggerShowAddEditBreakBankPopup gets updated
    if (
      prevProps.triggerShowAddEditBreakBankPopup !== this.props.triggerShowAddEditBreakBankPopup
    ) {
      const currentHoursBreakBank = [];
      const currentMinutesBreakBank = [];
      const { selectedCandidateData } = this.props;
      // setting timer default time
      // break bank time is defined
      if (selectedCandidateData.pause_test_time !== null) {
        currentHoursBreakBank.push(
          getTimeInHoursMinutes(Number(selectedCandidateData.pause_test_time)).formattedHours
        );
        currentMinutesBreakBank.push(
          getTimeInHoursMinutes(Number(selectedCandidateData.pause_test_time)).formattedMinutes
        );
        this.props.setDefaultTimes([selectedCandidateData.pause_test_time]);
        // break bank time is not defined
      } else {
        currentHoursBreakBank.push("00");
        currentMinutesBreakBank.push("00");
        this.props.setDefaultTimes([0]);
      }
      this.setState({
        showAddEditBreakBankPopup: true,
        currentHoursBreakBank: currentHoursBreakBank,
        currentMinutesBreakBank: currentMinutesBreakBank
      });
    }
    // if triggerShowLockPopup gets updated
    if (prevProps.triggerShowLockPopup !== this.props.triggerShowLockPopup) {
      this.setState({ showLockPopup: true });
    }
    // if triggerShowUnlockPopup gets updated
    if (prevProps.triggerShowUnlockPopup !== this.props.triggerShowUnlockPopup) {
      this.setState({ showUnlockPopup: true });
    }
    // if triggerApproveCandidate gets updated
    if (prevProps.triggerApproveCandidate !== this.props.triggerApproveCandidate) {
      this.setState(
        {
          testAdministratorAssignedCandidates: this.props.testAdministratorAssignedCandidates,
          rowsDefinition: this.props.rowsDefinition
        },
        () => {
          this.handleApproveCandidate();
        }
      );
    }
    // if triggerUnAssignCandidate gets updated
    if (prevProps.triggerUnAssignCandidate !== this.props.triggerUnAssignCandidate) {
      this.setState(
        {
          testAdministratorAssignedCandidates: this.props.testAdministratorAssignedCandidates
        },
        () => {
          this.setState({ showUnAssignPopup: true });
        }
      );
    }
    // if selectedCandidateData gets updated
    if (prevProps.selectedCandidateData !== this.props.selectedCandidateData) {
      const { assigned_test_sections } = this.props.selectedCandidateData;
      const currentHours = [];
      const currentMinutes = [];
      const timedAssignedTestSection = [];
      const defaultTimes = [];
      for (let i = 0; i < assigned_test_sections.length; i++) {
        if (assigned_test_sections[i].test_section_time !== null) {
          currentHours.push(
            getTimeInHoursMinutes(assigned_test_sections[i].test_section_time).formattedHours
          );
          currentMinutes.push(
            getTimeInHoursMinutes(assigned_test_sections[i].test_section_time).formattedMinutes
          );
          timedAssignedTestSection.push(assigned_test_sections[i]);
          defaultTimes.push(assigned_test_sections[i].test_section.default_time);
        }
      }
      this.props.setDefaultTimes(defaultTimes);
      this.setState({
        selectedCandidateData: this.props.selectedCandidateData,
        currentHours: currentHours,
        currentMinutes: currentMinutes,
        timedAssignedTestSection: timedAssignedTestSection
      });
    }
    // if timeUpdated gets updated
    if (prevProps.timeUpdated !== this.props.timeUpdated) {
      this.calculateTotalTestTime();
    }
  };

  componentWillUnmount = () => {
    clearInterval(this.state.testTimeRemaining);
  };

  closeEditTimePopup = () => {
    this.props.updateValidTimerState(true);
    this.setState({ showEditTimePopup: false, isRightPopupButtonDisabled: false });
  };

  closeAddEditBreakBankPopup = () => {
    this.props.updateValidTimerState(true);
    this.setState({ showAddEditBreakBankPopup: false, isRightPopupButtonDisabled: false });
  };

  closeLockPopup = () => {
    this.setState({
      showLockPopup: false,
      isRightPopupButtonDisabled: false
    });
  };

  closeUnlockPopup = () => {
    this.setState({
      showUnlockPopup: false,
      isRightPopupButtonDisabled: false
    });
  };

  openLockAllPopup = () => {
    this.setState({ showLockAllPopup: true });
  };

  closeLockAllPopup = () => {
    this.setState({ showLockAllPopup: false, isRightPopupButtonDisabled: false });
  };

  openUnlockAllPopup = () => {
    this.setState({ showUnlockAllPopup: true });
  };

  closeUnlockAllPopup = () => {
    this.setState({ showUnlockAllPopup: false, isRightPopupButtonDisabled: false });
  };

  handleSetTimer = () => {
    // disabling right popup button (to avoid multiple clicks)
    this.setState({ isRightPopupButtonDisabled: true }, () => {
      const { currentTimer } = this.props;
      const currentIndex = this.getCurrentTableIndex();
      for (let i = 0; i < currentTimer.length; i++) {
        let timeInMinutes = 0;
        const firstIteration = i === 0;
        const lastIteration = i === currentTimer.length - 1;
        timeInMinutes = Number(currentTimer[i].hours) * 60 + Number(currentTimer[i].minutes);
        this.props
          .updateTestTime(
            this.state.timedAssignedTestSection[i].id,
            timeInMinutes,
            firstIteration,
            lastIteration
          )
          .then(() => {
            // if last element of the loop
            if (i === currentTimer.length - 1) {
              // closing edit time popup
              this.closeEditTimePopup();
              this.triggerTaActionLoading(currentIndex);
              // updating TA's assigned candidates table
              this.props.updateAssignedCandidatesTable();
            }
          });
      }
    });
  };

  handleAddEditBreakBank = () => {
    // disabling right popup button (to avoid multiple clicks)
    this.setState({ isRightPopupButtonDisabled: true }, () => {
      // converting selected time in seconds
      let totalSeconds = 0;
      totalSeconds += Number(this.props.currentTimer[0].hours) * 60 * 60;
      totalSeconds += Number(this.props.currentTimer[0].minutes) * 60;
      const currentIndex = this.getCurrentTableIndex();

      // updating break bank
      this.props
        .updateBreakBank(this.state.selectedCandidateData.id, totalSeconds)
        .then(response => {
          // successful request
          if (response.ok) {
            // triggering TA table reload
            this.triggerTaActionLoading(currentIndex);
            // updating TA's assigned candidates table
            this.props.updateAssignedCandidatesTable();
            this.closeAddEditBreakBankPopup();
            // should never happen
          } else {
            throw new Error("An error occurred during the update break bank request process");
          }
        });
    });
  };

  getCurrentTableIndex = () => {
    // initializing currentIndex
    let currentIndex = null;
    // looping in TA assigned candidates table to get the index of the current selected candidate
    for (let i = 0; i < this.state.testAdministratorAssignedCandidates.length; i++) {
      if (
        this.state.selectedCandidateData.id === this.state.testAdministratorAssignedCandidates[i].id
      ) {
        // updating currentIndex once found
        currentIndex = i;
        break;
      }
    }
    return currentIndex;
  };

  triggerTaActionLoading = index => {
    const { testAdministratorAssignedCandidates, rowsDefinition } = this.state;
    const tempRowsDefinition = rowsDefinition;
    for (let i = 0; i < rowsDefinition.data.length; i++) {
      // provided index is the same as the current index
      if (i === index) {
        tempRowsDefinition.data[i].column_7 = (
          // eslint-disable-next-line jsx-a11y/label-has-associated-control
          <label className="fa fa-spinner fa-spin">
            <FontAwesomeIcon icon={faSpinner} />
          </label>
        );
        // other candidates than the one selected
      } else {
        tempRowsDefinition.data[i].column_7 = (
          <div>
            <CustomButton
              label={<FontAwesomeIcon icon={faClock} />}
              customStyle={{
                ...TestAdministrationStyle.actionButton,
                ...TestAdministrationStyle.disabledActionButton
              }}
              disabled={true}
            />
            <CustomButton
              label={<FontAwesomeIcon icon={faThumbsUp} />}
              customStyle={{
                ...TestAdministrationStyle.actionButton,
                ...TestAdministrationStyle.disabledActionButton
              }}
              disabled={true}
            />
            <CustomButton
              label={<FontAwesomeIcon icon={faTrashAlt} />}
              customStyle={{
                ...TestAdministrationStyle.actionButton,
                ...TestAdministrationStyle.disabledActionButton
              }}
              disabled={true}
            />
            <CustomButton
              label={
                <FontAwesomeIcon
                  icon={
                    testAdministratorAssignedCandidates[i].status === TEST_STATUS.LOCKED
                      ? faLockOpen
                      : faLock
                  }
                />
              }
              customStyle={{
                ...TestAdministrationStyle.actionButton,
                ...TestAdministrationStyle.disabledActionButton
              }}
              disabled={true}
            />
            {/* <CustomButton
              label={<FontAwesomeIcon icon={faEdit} />}
              customStyle={{
                ...TestAdministrationStyle.actionButton,
                ...TestAdministrationStyle.disabledActionButton
              }}
              disabled={true}
            /> */}
          </div>
        );
      }
    }
    this.setState({ rowsDefinition: tempRowsDefinition });
  };

  handleApproveCandidate = () => {
    const currentIndex = this.getCurrentTableIndex();

    const { username } = this.state.testAdministratorAssignedCandidates[currentIndex];
    const test = this.state.testAdministratorAssignedCandidates[currentIndex].test.id;

    // approving candidate (updating candidate's test status)
    this.props.approveCandidate(username, test).then(response => {
      // request successful
      if (response.status === 200) {
        this.triggerTaActionLoading(currentIndex);
        // updating TA's assigned candidates table
        this.props.updateAssignedCandidatesTable();
        // should never happen
      } else {
        throw new Error("Something went wrong during approve candidate process");
      }
    });
  };

  handleLockUnlock = () => {
    // disabling right popup button (to avoid multiple clicks)
    this.setState({ isRightPopupButtonDisabled: true }, () => {
      // getting candidate's assigned tests data to get the updated test section id
      this.props.getAssignedTests(this.state.selectedCandidateData.id).then(assignedTest => {
        const { username } = this.state.selectedCandidateData;
        const test = this.state.selectedCandidateData.test.id;
        const currentTestStatus = this.state.selectedCandidateData.status;
        const testSectionId = assignedTest[0].test_section;

        const currentIndex = this.getCurrentTableIndex();

        if (
          currentTestStatus === TEST_STATUS.PRE_TEST ||
          currentTestStatus === TEST_STATUS.ACTIVE ||
          currentTestStatus === TEST_STATUS.READY ||
          currentTestStatus === TEST_STATUS.PAUSED
        ) {
          this.props.lockCandidateTest(username, test, testSectionId).then(response => {
            // request successful
            if (response.status === 200) {
              this.triggerTaActionLoading(currentIndex);
              // closing popup
              this.closeLockPopup();
              this.props.updateAssignedCandidatesTable();
              // should never happen
            } else {
              throw new Error("Something went wrong during lock candidate process");
            }
          });
        } else if (currentTestStatus === TEST_STATUS.LOCKED) {
          this.props.unlockCandidateTest(username, test, testSectionId).then(response => {
            // request successful
            if (response.status === 200) {
              this.triggerTaActionLoading(currentIndex);
              // closing popup
              this.closeUnlockPopup();
              this.props.updateAssignedCandidatesTable();
              // should never happen
            } else {
              throw new Error("Something went wrong during unlock candidate process");
            }
          });
        }
      });
    });
  };

  handleLockUnlockAll = () => {
    // disabling right popup button (to avoid multiple clicks)
    this.setState({ isRightPopupButtonDisabled: true }, () => {
      // unlock all candidates
      if (this.state.allTestsLockedFlag) {
        this.props.unlockAllCandidatesTest().then(response => {
          // request successful
          if (response.status === 200) {
            // closing popup
            this.closeUnlockAllPopup();
            // re-populating active candidates table
            this.props.populateTestAdministratorAssignedCandidates();
            // should never happen
          } else {
            throw new Error("Something went wrong during unlock all candidates process");
          }
        });
        // lock all candidates
      } else {
        this.props.lockAllCandidatesTest().then(response => {
          // request successful
          if (response.status === 200) {
            // closing popup
            this.closeLockAllPopup();
            // re-populating active candidates table
            this.props.populateTestAdministratorAssignedCandidates();
            // should never happen
          } else {
            throw new Error("Something went wrong during lock all candidates process");
          }
        });
      }
    });
  };

  handleUnAssign = () => {
    // disabling right popup button (to avoid multiple clicks)
    this.setState({ isRightPopupButtonDisabled: true }, () => {
      const currentIndex = this.getCurrentTableIndex();
      const { username } = this.state.testAdministratorAssignedCandidates[currentIndex];
      const test = this.state.testAdministratorAssignedCandidates[currentIndex].test.id;

      this.props.unAssignCandidate(username, test).then(response => {
        // request successful
        if (response.status === 200) {
          this.triggerTaActionLoading(currentIndex);
          // close un-assign popup
          this.handleCloseUnAssignPopup();
          // updating TA's assigned candidates table
          this.props.updateAssignedCandidatesTable();
          // should never happen
        } else {
          throw new Error("Something went wrong during unassign candidate process");
        }
      });
    });
  };

  // reset lock popup states + timer state
  resetNeededLockPopupStates = () => {
    this.props.updateTimerState([]);
  };

  onPopupClose = () => {
    // initializing currentTimer redux state to []
    this.props.updateTimerState([]);
  };

  calculateTotalTestTime = () => {
    let totalMinutes = 0;
    // calculating the total time in minutes
    for (let i = 0; i < this.props.currentTimer.length; i++) {
      totalMinutes += Number(this.props.currentTimer[i].hours) * 60;
      totalMinutes += Number(this.props.currentTimer[i].minutes);
    }
    // saving total time in hours and minutes
    this.setState({
      totalHours: getTimeInHoursMinutes(totalMinutes).hours,
      totalMinutes: getTimeInHoursMinutes(totalMinutes).minutes
    });
  };

  handleCloseUnAssignPopup = () => {
    this.setState({ showUnAssignPopup: false, isRightPopupButtonDisabled: false });
  };

  render() {
    const columnsDefinition = [
      {
        label: LOCALIZE.testAdministration.activeCandidates.table.candidate,
        style: { width: "30%" }
      },
      {
        label: LOCALIZE.testAdministration.activeCandidates.table.dob,
        style: { ...{ width: "11%" }, ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label: LOCALIZE.testAdministration.activeCandidates.table.testCode,
        style: { ...{ width: "11%" }, ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label: LOCALIZE.testAdministration.activeCandidates.table.status,
        style: { ...{ width: "10%" }, ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label: LOCALIZE.testAdministration.activeCandidates.table.timer,
        style: { ...{ width: "10%" }, ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label: LOCALIZE.testAdministration.activeCandidates.table.timeRemaining,
        style: { ...{ width: "10%" }, ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label: LOCALIZE.testAdministration.activeCandidates.table.actions,
        style: { ...{ width: "18%" }, ...COMMON_STYLE.CENTERED_TEXT }
      }
    ];

    return (
      <div style={TestAccessCodesStyle.mainContainer}>
        <Row className="align-items-center justify-content-start">
          <Col>
            <h2>{LOCALIZE.testAdministration.sideNavItems.activeCandidates}</h2>
          </Col>
        </Row>
        <Row className="align-items-center justify-content-start">
          <Col>
            <p>{LOCALIZE.testAdministration.activeCandidates.description}</p>
          </Col>
        </Row>
        <Row className="align-items-center justify-content-end">
          <Col xl={"auto"} lg={"auto"} md={"auto"} sm={"auto"} xs={"auto"}>
            <CustomButton
              buttonId="lock-unlock-all-button-id"
              label={
                <>
                  <span>
                    <FontAwesomeIcon
                      icon={this.state.allTestsLockedFlag ? faLockOpen : faLock}
                      style={styles.buttonIcon}
                    />
                  </span>
                  <span>
                    {!this.state.atLeastOneTestToLockUnlock
                      ? LOCALIZE.testAdministration.activeCandidates.lockAllButton
                      : this.state.allTestsLockedFlag
                      ? LOCALIZE.testAdministration.activeCandidates.unlockAllButton
                      : LOCALIZE.testAdministration.activeCandidates.lockAllButton}
                  </span>
                </>
              }
              action={
                this.state.allTestsLockedFlag ? this.openUnlockAllPopup : this.openLockAllPopup
              }
              customStyle={styles.lockUnlockAllButton}
              buttonTheme={THEME.SECONDARY}
              disabled={this.props.currentlyLoading || !this.state.atLeastOneTestToLockUnlock}
            />
          </Col>
        </Row>
        <GenericTable
          classnamePrefix="active-candidates"
          columnsDefinition={columnsDefinition}
          rowsDefinition={this.state.rowsDefinition}
          emptyTableMessage={LOCALIZE.testAdministration.activeCandidates.noActiveCandidates}
          currentlyLoading={this.props.currentlyLoading}
          triggerReRender={this.props.triggerReRender}
        />
        {Object.keys(this.state.selectedCandidateData).length > 0 && (
          <div>
            <PopupBox
              show={this.state.showEditTimePopup}
              handleClose={() => {}}
              isBackdropStatic={true}
              shouldCloseOnEsc={false}
              onPopupClose={this.onPopupClose}
              onPopupOpen={this.calculateTotalTestTime}
              title={LOCALIZE.testAdministration.activeCandidates.editTimePopup.title}
              size="lg"
              description={
                <div>
                  <div>
                    <p>
                      <SystemMessage
                        messageType={MESSAGE_TYPE.warning}
                        title={LOCALIZE.commons.warning}
                        message={
                          <>
                            <span style={styles.bold}>
                              {
                                LOCALIZE.testAdministration.activeCandidates.editTimePopup
                                  .description1
                              }
                            </span>
                          </>
                        }
                      />
                      <br />
                      <span>
                        {LOCALIZE.formatString(
                          LOCALIZE.testAdministration.activeCandidates.editTimePopup.description2,
                          <span style={styles.bold}>
                            {this.state.selectedCandidateData.candidate_first_name}
                          </span>,
                          <span style={styles.bold}>
                            {this.state.selectedCandidateData.candidate_last_name}
                          </span>
                        )}
                      </span>
                    </p>
                  </div>
                  {this.state.timedAssignedTestSection.map((assigned_test_section, index) => {
                    return (
                      <div key={index} style={styles.testSectionTimerMainContainer}>
                        <Row className="align-items-center justify-content-center">
                          <Col
                            xl={columnSizes.firstColumn.xl}
                            lg={columnSizes.firstColumn.lg}
                            md={columnSizes.firstColumn.md}
                            sm={columnSizes.firstColumn.sm}
                            xs={columnSizes.firstColumn.xs}
                            style={styles.testSectionTimerDescriptionContainer}
                          >
                            <p style={{ ...styles.bold, ...styles.centerText }}>
                              {
                                assigned_test_section.test_section[
                                  `${this.props.currentLanguage}_title`
                                ]
                              }
                            </p>
                            <p>
                              {
                                assigned_test_section.test_section[
                                  `${this.props.currentLanguage}_description`
                                ]
                              }
                            </p>
                          </Col>
                          <Col
                            xl={columnSizes.secondColumn.xl}
                            lg={columnSizes.secondColumn.lg}
                            md={columnSizes.secondColumn.md}
                            sm={columnSizes.secondColumn.sm}
                            xs={columnSizes.secondColumn.xs}
                            style={styles.testSectionTimerTimeContainer}
                          >
                            <SetTimer
                              index={index}
                              currentHours={this.state.currentHours[index]}
                              currentMinutes={this.state.currentMinutes[index]}
                            />
                          </Col>
                        </Row>
                      </div>
                    );
                  })}
                  <div>
                    <p>{LOCALIZE.testAdministration.activeCandidates.editTimePopup.description3}</p>
                    <p style={styles.bold}>
                      {LOCALIZE.formatString(
                        LOCALIZE.testAdministration.activeCandidates.editTimePopup.description4,
                        <span>{this.state.totalHours}</span>,
                        <span>{this.state.totalMinutes}</span>
                      )}
                    </p>
                  </div>
                </div>
              }
              leftButtonType={BUTTON_TYPE.secondary}
              leftButtonTitle={LOCALIZE.commons.cancel}
              leftButtonCustomStyle={{ minWidth: 150 }}
              leftButtonIcon={faTimes}
              leftButtonAction={this.closeEditTimePopup}
              rightButtonType={BUTTON_TYPE.primary}
              rightButtonTitle={
                LOCALIZE.testAdministration.activeCandidates.editTimePopup.setTimerButton
              }
              rightButtonState={
                this.props.validTimer && !this.state.isRightPopupButtonDisabled
                  ? BUTTON_STATE.enabled
                  : BUTTON_STATE.disabled
              }
              rightButtonIcon={faClock}
              rightButtonIconCustomStyle={{ transform: "scale(1.25)" }}
              rightButtonAction={this.handleSetTimer}
            />
            <PopupBox
              show={this.state.showAddEditBreakBankPopup}
              handleClose={() => {}}
              isBackdropStatic={true}
              shouldCloseOnEsc={false}
              onPopupClose={this.onPopupClose}
              title={LOCALIZE.testAdministration.activeCandidates.addEditBreakBankPopup.title}
              size="lg"
              description={
                <div>
                  <div>
                    <p>
                      <SystemMessage
                        messageType={MESSAGE_TYPE.warning}
                        title={LOCALIZE.commons.warning}
                        message={
                          <>
                            <span style={styles.bold}>
                              {
                                LOCALIZE.testAdministration.activeCandidates.editTimePopup
                                  .description1
                              }
                            </span>
                          </>
                        }
                      />
                      <br />
                      <span>
                        {LOCALIZE.formatString(
                          LOCALIZE.testAdministration.activeCandidates.addEditBreakBankPopup
                            .description,
                          <span style={styles.bold}>
                            {this.state.selectedCandidateData.candidate_first_name}
                          </span>,
                          <span style={styles.bold}>
                            {this.state.selectedCandidateData.candidate_last_name}
                          </span>
                        )}
                      </span>
                    </p>
                  </div>
                  <div style={styles.breakBankMainContainer}>
                    <Row className="align-items-center justify-content-center">
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        style={styles.breakBankDescriptionContainer}
                      >
                        <p style={{ ...styles.bold, ...styles.centerText }}>
                          {
                            LOCALIZE.testAdministration.activeCandidates.addEditBreakBankPopup
                              .setTimeTitle
                          }
                        </p>
                      </Col>
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                        style={styles.breakBankMainContainer}
                      >
                        <SetTimer
                          index={0}
                          currentHours={this.state.currentHoursBreakBank[0]}
                          currentMinutes={this.state.currentMinutesBreakBank[0]}
                          customTimeValidationState={true}
                        />
                      </Col>
                    </Row>
                  </div>
                </div>
              }
              leftButtonType={BUTTON_TYPE.secondary}
              leftButtonTitle={LOCALIZE.commons.cancel}
              leftButtonCustomStyle={{ minWidth: 150 }}
              leftButtonIcon={faTimes}
              leftButtonAction={this.closeAddEditBreakBankPopup}
              rightButtonType={BUTTON_TYPE.primary}
              rightButtonTitle={
                LOCALIZE.testAdministration.activeCandidates.addEditBreakBankPopup
                  .setBreakBankButton
              }
              rightButtonState={
                this.props.validTimer && !this.state.isRightPopupButtonDisabled
                  ? BUTTON_STATE.enabled
                  : BUTTON_STATE.disabled
              }
              rightButtonIcon={faPauseCircle}
              rightButtonIconCustomStyle={{ transform: "scale(1.25)" }}
              rightButtonAction={this.handleAddEditBreakBank}
            />
            <PopupBox
              show={this.state.showLockPopup}
              handleClose={() => {}}
              isBackdropStatic={true}
              shouldCloseOnEsc={false}
              onPopupClose={this.resetNeededLockPopupStates}
              title={LOCALIZE.testAdministration.activeCandidates.lockPopup.title}
              size="lg"
              description={
                <div>
                  <SystemMessage
                    messageType={MESSAGE_TYPE.warning}
                    title={LOCALIZE.commons.warning}
                    message={
                      <p>
                        {LOCALIZE.formatString(
                          LOCALIZE.testAdministration.activeCandidates.lockPopup.description1,
                          <span style={styles.bold}>
                            {this.state.selectedCandidateData.candidate_first_name}
                          </span>,
                          <span style={styles.bold}>
                            {this.state.selectedCandidateData.candidate_last_name}
                          </span>
                        )}
                      </p>
                    }
                  />
                </div>
              }
              leftButtonType={BUTTON_TYPE.secondary}
              leftButtonTitle={LOCALIZE.commons.cancel}
              leftButtonLabel={LOCALIZE.commons.cancel}
              leftButtonCustomStyle={{ minWidth: 150 }}
              leftButtonIcon={faTimes}
              leftButtonAction={this.closeLockPopup}
              rightButtonType={BUTTON_TYPE.primary}
              rightButtonTitle={LOCALIZE.testAdministration.activeCandidates.lockPopup.lockButton}
              rightButtonLabel={LOCALIZE.testAdministration.activeCandidates.lockPopup.lockButton}
              rightButtonIcon={faLock}
              rightButtonIconCustomStyle={{ transform: "scale(1.25)" }}
              rightButtonAction={this.handleLockUnlock}
            />
            <PopupBox
              show={this.state.showUnlockPopup}
              handleClose={() => {}}
              isBackdropStatic={true}
              shouldCloseOnEsc={false}
              title={LOCALIZE.testAdministration.activeCandidates.unlockPopup.title}
              size="lg"
              description={
                <div>
                  <SystemMessage
                    messageType={MESSAGE_TYPE.warning}
                    title={LOCALIZE.commons.warning}
                    message={
                      <>
                        <p>
                          {LOCALIZE.formatString(
                            LOCALIZE.testAdministration.activeCandidates.unlockPopup.description1,
                            <span style={styles.bold}>
                              {this.state.selectedCandidateData.candidate_first_name}
                            </span>,
                            <span style={styles.bold}>
                              {this.state.selectedCandidateData.candidate_last_name}
                            </span>
                          )}
                        </p>
                        <p>
                          {LOCALIZE.testAdministration.activeCandidates.unlockPopup.description2}
                        </p>
                        <p>
                          {LOCALIZE.testAdministration.activeCandidates.unlockPopup.description3}
                        </p>
                      </>
                    }
                  />
                </div>
              }
              leftButtonType={BUTTON_TYPE.secondary}
              leftButtonTitle={LOCALIZE.commons.cancel}
              leftButtonCustomStyle={{ minWidth: 150 }}
              leftButtonIcon={faTimes}
              leftButtonAction={this.closeUnlockPopup}
              rightButtonType={BUTTON_TYPE.primary}
              rightButtonTitle={
                LOCALIZE.testAdministration.activeCandidates.unlockPopup.unlockButton
              }
              rightButtonIcon={faLockOpen}
              rightButtonIconCustomStyle={{ transform: "scale(1.25)" }}
              rightButtonAction={this.handleLockUnlock}
              rightButtonState={
                this.state.isRightPopupButtonDisabled ? BUTTON_STATE.disabled : BUTTON_STATE.enabled
              }
            />
          </div>
        )}
        <PopupBox
          show={this.state.showLockAllPopup}
          handleClose={() => {}}
          isBackdropStatic={true}
          shouldCloseOnEsc={false}
          title={LOCALIZE.testAdministration.activeCandidates.lockAllPopup.title}
          size="lg"
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.warning}
                title={LOCALIZE.commons.warning}
                message={
                  <>
                    <p>{LOCALIZE.testAdministration.activeCandidates.lockAllPopup.description1}</p>
                    <p>{LOCALIZE.testAdministration.activeCandidates.lockAllPopup.description2}</p>
                    <p>{LOCALIZE.testAdministration.activeCandidates.lockAllPopup.description3}</p>
                  </>
                }
              />
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonCustomStyle={{ minWidth: 150 }}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeLockAllPopup}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={
            LOCALIZE.testAdministration.activeCandidates.lockAllPopup.lockTestsButton
          }
          rightButtonIcon={faLock}
          rightButtonIconCustomStyle={{ transform: "scale(1.25)" }}
          rightButtonAction={this.handleLockUnlockAll}
          rightButtonState={
            this.state.isRightPopupButtonDisabled ? BUTTON_STATE.disabled : BUTTON_STATE.enabled
          }
        />
        <PopupBox
          show={this.state.showUnlockAllPopup}
          handleClose={() => {}}
          isBackdropStatic={true}
          shouldCloseOnEsc={false}
          title={LOCALIZE.testAdministration.activeCandidates.unlockAllPopup.title}
          size="lg"
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.warning}
                title={LOCALIZE.commons.warning}
                message={
                  <>
                    <p>
                      {LOCALIZE.testAdministration.activeCandidates.unlockAllPopup.description1}
                    </p>
                    <p>
                      {LOCALIZE.testAdministration.activeCandidates.unlockAllPopup.description2}
                    </p>
                    <p>
                      {LOCALIZE.testAdministration.activeCandidates.unlockAllPopup.description3}
                    </p>
                  </>
                }
              />
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonCustomStyle={{ minWidth: 150 }}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeUnlockAllPopup}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={
            LOCALIZE.testAdministration.activeCandidates.unlockAllPopup.unlockTestsButton
          }
          rightButtonIcon={faLockOpen}
          rightButtonIconCustomStyle={{ transform: "scale(1.25)" }}
          rightButtonAction={this.handleLockUnlockAll}
          rightButtonState={
            this.state.isRightPopupButtonDisabled ? BUTTON_STATE.disabled : BUTTON_STATE.enabled
          }
        />
        {Object.keys(this.state.selectedCandidateData).length > 0 && (
          <PopupBox
            show={this.state.showUnAssignPopup}
            handleClose={() => {}}
            isBackdropStatic={true}
            shouldCloseOnEsc={false}
            title={LOCALIZE.testAdministration.activeCandidates.unAssignPopup.title}
            size="lg"
            description={
              <div>
                <SystemMessage
                  messageType={MESSAGE_TYPE.error}
                  title={LOCALIZE.commons.warning}
                  message={
                    <p>
                      {LOCALIZE.formatString(
                        LOCALIZE.testAdministration.activeCandidates.unAssignPopup.description,
                        <span style={styles.bold}>
                          {this.state.selectedCandidateData.candidate_first_name}
                        </span>,
                        <span style={styles.bold}>
                          {this.state.selectedCandidateData.candidate_last_name}
                        </span>
                      )}
                    </p>
                  }
                />
              </div>
            }
            leftButtonType={BUTTON_TYPE.secondary}
            leftButtonTitle={LOCALIZE.commons.cancel}
            leftButtonIcon={faTimes}
            leftButtonAction={this.handleCloseUnAssignPopup}
            rightButtonType={BUTTON_TYPE.danger}
            rightButtonTitle={LOCALIZE.testAdministration.activeCandidates.unAssignPopup.confirm}
            rightButtonIcon={faTrashAlt}
            rightButtonAction={this.handleUnAssign}
            rightButtonState={
              this.state.isRightPopupButtonDisabled ? BUTTON_STATE.disabled : BUTTON_STATE.enabled
            }
          />
        )}
      </div>
    );
  }
}

export { ActiveCandidates as unconnectedActiveCandidates };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    currentTimer: state.timer.currentTimer,
    validTimer: state.timer.validTimer,
    timeUpdated: state.timer.timeUpdated,
    defaultTimes: state.timer.defaultTimes,
    username: state.user.username,
    isTestLocked: state.testStatus.isTestLocked,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateTestTime,
      updateTimerState,
      setDefaultTimes,
      resetTimerStates,
      updateValidTimerState,
      approveCandidate,
      unAssignCandidate,
      lockCandidateTest,
      unlockCandidateTest,
      lockAllCandidatesTest,
      unlockAllCandidatesTest,
      getAssignedTests,
      updateBreakBank
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ActiveCandidates);
