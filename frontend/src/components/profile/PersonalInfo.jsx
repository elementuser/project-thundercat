/* eslint-disable no-param-reassign */
import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faQuestionCircle, faTrashAlt } from "@fortawesome/free-regular-svg-icons";
import {
  faSpinner,
  faSave,
  faPencilAlt,
  faTimes,
  faShare,
  faBinoculars
} from "@fortawesome/free-solid-svg-icons";
import { Button, Row, Col } from "react-bootstrap";
import {
  updateUserPersonalInfo,
  updateUserExtendedProfile,
  getUserExtendedProfile,
  updateExtendedProfileState,
  updateCurrentEmployer,
  updateOrganization,
  updateGroup,
  updateSubclassification,
  updateLevel,
  updateResidence,
  updateEducation,
  updateGender,
  sendUserProfileChangeRequest,
  getUserProfileChangeRequest,
  deleteUserProfileChangeRequest
} from "../../modules/UserProfileRedux";
import {
  getAboriginal,
  getVisibleMinority,
  getDisability,
  getGroup,
  setGroupOptions,
  getSubClassifications,
  setSubClassificationOptions
} from "../../modules/ExtendedProfileOptionsRedux";
import validateName, {
  validatePri,
  validateMilitaryNbr,
  validateEmail,
  validatePSRSAppID
} from "../../helpers/regexValidator";
import { setUserInformation } from "../../modules/UserRedux";
import StyledTooltip, { TYPE, EFFECT, TRIGGER_TYPE } from "../authentication/StyledTooltip";
import CustomButton, { THEME } from "../commons/CustomButton";
import DatePicker, { validateDatePicked } from "../commons/DatePicker";
import PopupBox, { BUTTON_STATE, BUTTON_TYPE } from "../commons/PopupBox";
import DropdownSelect from "../commons/DropdownSelect";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";
import SystemMessage, { MESSAGE_TYPE } from "../commons/SystemMessage";
import EXTENDED_PROFILE_EE_INFO, { VISIBLE_MINORITY_ID, DISABILITY_ID } from "./Constants";
import { refreshGroupSubgroupLevelDropdownLanguage } from "./ExtendedProfileUtility";
// CSS
import "../../css/personal-info.css";

// Constants
import LOCALIZE, { LOCALIZE_OBJ } from "../../text_resources";
import { LANGUAGES } from "../../modules/LocalizeRedux";
import getCheckboxTransformScale from "../../helpers/checkboxTransformScale";
import AlertMessage, { ALERT_TYPE } from "../commons/AlertMessage";
import PrivacyNoticeStatement from "../commons/PrivacyNoticeStatement";
import { updateDatePicked, updateDatePickedValidState } from "../../modules/DatePickerRedux";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 5,
    lg: 5,
    xl: 5
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 6,
    lg: 6,
    xl: 6
  },
  thirdColumn: {
    xs: 12,
    sm: 12,
    md: 1,
    lg: 1,
    xl: 1
  },
  radioButtonChoicesColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 12,
    xl: 12
  },
  popupFirstColumn: {
    xs: 12,
    sm: 12,
    md: 2,
    lg: 2,
    xl: 2
  },
  popupSecondColumn: {
    xs: 12,
    sm: 12,
    md: 5,
    lg: 5,
    xl: 5
  },
  popupThirdColumn: {
    xs: 12,
    sm: 12,
    md: 5,
    lg: 5,
    xl: 5
  }
};

const styles = {
  mainContent: {
    marginBottom: 24
  },
  inputsContainer: {
    padding: "12px 24px 0 24px"
  },
  itemContainer: {
    width: "100%",
    margin: "12px 0"
  },
  radioButtonItemContainer: {
    width: "100%",
    margin: "12px 0",
    paddingBottom: 24
  },
  radioButtonSubItemContainer: {
    width: "100%",
    margin: "12px 0",
    padding: "0 24px 24px 24px"
  },
  subGroupOtherOptionTextAreaContainer: {
    width: "100%",
    margin: "-24px 0 12px 0",
    padding: "0 24px 24px 24px"
  },
  dobItemContainer: {
    width: "100%",
    margin: "12px 0"
  },
  psrsItemContainer: {
    width: "100%",
    margin: "12px 0"
  },
  labelContainer: {
    padding: "0 12px 0 0"
  },
  inputContainer: {
    paddingLeft: 0
  },
  radioButtonsContainer: {
    verticalAlign: "middle",
    paddingLeft: 24
  },
  input: {
    width: "100%",
    minHeight: 38,
    padding: "3px 6px 3px 6px",
    borderRadius: 4
  },
  additionalInfoDropdownContainer: {
    width: "75%"
  },
  additionalInfoDropdown: {
    width: "100%"
  },
  errorContainer: {
    margin: "6px 0 6px 3px"
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  tooltipIconContainer: {
    padding: "0 6px"
  },
  tooltipMarginTop: {
    marginTop: 1
  },
  tooltipIcon: {
    color: "#00565e"
  },
  hrStyle: {
    marginTop: 36
  },
  updateProfileButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  updateButtonContainer: {
    float: "right"
  },
  updateButton: {
    margin: "24px 24px 0 0",
    minWidth: 100
  },
  saveButtonLabel: {
    margin: 0,
    paddingLeft: 6
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6
  },
  noMargin: {
    margin: 0
  },
  dropdown: {
    padding: 0,
    width: 250
  },
  loadingContainer: {
    margin: "0 auto"
  },
  loading: {
    fontSize: "32px",
    padding: 6,
    textAlign: "center"
  },
  privacyNoticeLink: {
    textDecoration: "underline",
    color: "#0278A4",
    cursor: "pointer",
    backgroundColor: "transparent",
    border: "none",
    padding: 0
  },
  radioButtonLabel: {
    paddingLeft: 12
  },
  radioButton: {
    marginLeft: 15,
    verticalAlign: "middle"
  },
  checkbox: {
    verticalAlign: "middle",
    margin: "0 16px"
  },
  textArea: {
    padding: 10,
    resize: "none",
    width: "100%",
    minHeight: 100
  },
  boldText: {
    fontWeight: "bold"
  },
  noPadding: {
    padding: 0
  },
  popupInputsContainer: {
    padding: "24px 12px"
  },
  popupRow: {
    padding: "12px 0"
  },
  columnTitle: {
    display: "block",
    textAlign: "center",
    fontWeight: "bold"
  },
  textAreaInput: {
    width: "100%",
    height: 85,
    padding: "3px 6px 3px 6px",
    borderRadius: 4,
    display: "block",
    resize: "none"
  },
  allUnset: {
    all: "unset"
  }
};

function addNAOption(list) {
  // if not initialized return
  if (!(list instanceof Array)) {
    return list;
  }
  if (list[0] === undefined) {
    return list;
  }
  const NA = { value: 0, label: LOCALIZE.commons.na };
  // if the first element is N/A, pop and replace
  // this is needed to prevent duplicates and allow language toggle
  if (list[0].value !== 0) {
    list.unshift(NA);
  } else {
    list[0] = NA;
  }
  return list;
}

function hasNoValidSubgroups(subClassificationOptions) {
  return (
    subClassificationOptions !== undefined &&
    subClassificationOptions.length === 2 && // If there are no valid subgroups, subClassificationOptions contains only the two options "N/A" and "None"
    subClassificationOptions[0].value === 0 && // "N/A" or "S/O"
    subClassificationOptions[1].label === LOCALIZE.commons.none
  );
}

class PersonalInfo extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  static propTypes = {
    // provided by redux
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    psrsAppID: PropTypes.string,
    primaryEmail: PropTypes.string.isRequired,
    secondaryEmail: PropTypes.string,
    dateOfBirth: PropTypes.string.isRequired,
    pri: PropTypes.string,
    militaryNbr: PropTypes.string,
    currentLanguage: PropTypes.string,
    currentEmployer: PropTypes.number,
    organization: PropTypes.number,
    group: PropTypes.number,
    subclassification: PropTypes.number,
    level: PropTypes.number,
    residence: PropTypes.number,
    education: PropTypes.number,
    gender: PropTypes.number,
    updateUserPersonalInfo: PropTypes.func,
    updateUserExtendedProfile: PropTypes.func,
    setUserInformation: PropTypes.func,
    getUserExtendedProfile: PropTypes.func,
    updateExtendedProfileState: PropTypes.func,
    updateCurrentEmployer: PropTypes.func,
    updateOrganization: PropTypes.func,
    getGroup: PropTypes.func,
    setGroupOptions: PropTypes.func,
    getSubClassifications: PropTypes.func,
    setSubClassificationOptions: PropTypes.func,
    updateGroup: PropTypes.func,
    updateSubclassification: PropTypes.func,
    updateLevel: PropTypes.func,
    updateResidence: PropTypes.func,
    updateEducation: PropTypes.func,
    updateGender: PropTypes.func,
    isExtendedProfileLoaded: PropTypes.bool,
    employerOptions: PropTypes.array,
    organizationOptions: PropTypes.array,
    groupOptions: PropTypes.object,
    subClassificationOptions: PropTypes.object,
    residenceOptions: PropTypes.array,
    educationOptions: PropTypes.array,
    genderOptions: PropTypes.array
  };

  constructor(props) {
    super(props);
    this.dateOfBirthDayFieldRef = React.createRef();

    // current DOB
    this.popupCurrentOfBirthDayFieldRef = React.createRef();
    this.popupCurrentOfBirthMonthFieldRef = React.createRef();
    this.popupCurrentOfBirthYearFieldRef = React.createRef();
    // new DOB
    this.popupNewOfBirthDayFieldRef = React.createRef();
    this.popupNewOfBirthMonthFieldRef = React.createRef();
    this.popupNewOfBirthYearFieldRef = React.createRef();

    this.state = {
      firstNameContent: this.props.firstName,
      lastNameContent: this.props.lastName,
      primaryEmailContent: this.props.primaryEmail,
      usernameContent: this.props.username,
      psrsAppIdContent: this.props.psrsAppID === null ? "" : this.props.psrsAppID,
      currentEmployerContent: this.createSelectionObject(
        this.props.currentEmployer,
        this.props.employerOptions
      ),
      organizationContent: this.createSelectionObject(
        this.props.organization,
        this.props.organizationOptions
      ),
      groupContent: this.createSelectionObject(this.props.group, this.props.groupOptions.groups),

      subClassificationOptions: this.generateSubClassificationOptions(
        undefined,
        undefined,
        this.props.groupOptions
      ),
      subClassificationContent: [],
      levelOptions: [],
      levelContent: [],
      residenceContent: this.createSelectionObject(
        this.props.residence,
        this.props.residenceOptions
      ),
      educationContent: this.createSelectionObject(
        this.props.education,
        this.props.educationOptions
      ),
      genderContent: this.createSelectionObject(this.props.gender, this.props.genderOptions),
      secondaryEmailContent: this.props.secondaryEmail === null ? "" : this.props.secondaryEmail,
      isValidSecondaryEmail: true,
      dateOfBirthYearOptions: [],
      dateOfBirthYearSelectedValue: {
        value: parseInt(this.props.dateOfBirth.split("-")[0]),
        label: `${this.props.dateOfBirth.split("-")[0]}`
      },
      dateOfBirthMonthOptions: [],
      dateOfBirthMonthSelectedValue: {
        value: parseInt(this.props.dateOfBirth.split("-")[1]),
        label: `${this.props.dateOfBirth.split("-")[1]}`
      },
      dateOfBirthDayOptions: [],
      dateOfBirthDaySelectedValue: {
        value: parseInt(this.props.dateOfBirth.split("-")[2]),
        label: `${this.props.dateOfBirth.split("-")[2]}`
      },
      triggerDobValidation: true,
      priContent: this.props.pri,
      militaryNbrContent: this.props.militaryNbr,
      isValidPri: true,
      isValidMilitaryNbr: true,
      isValidPSRSApplicantID: true,
      displayDataUpdatedConfirmationMsgPopup: false,
      triggerRerender: false,
      isLoading: true,
      showPrivacyNoticeDialog: false,
      isLoadingEEInfo: false,
      identifyAsWoman: "",
      isValidIdentifyAsWoman: true,
      aboriginal: "",
      isValidAboriginal: true,
      subAboriginalOptions: [],
      subAboriginalCheckboxStates: [],
      isValidSubAboriginal: true,
      visibleMinority: "",
      isValidVisibleMinority: true,
      subVisibleMinorityOptions: [],
      subVisibleMinorityCheckboxStates: [],
      subVisibleMinorityOtherOptionSelected: false,
      subVisibleMinorityOtherContent: "",
      isValidSubVisibleMinority: true,
      disability: "",
      isValidDisability: true,
      subDisabilityOptions: [],
      subDisabilityCheckboxStates: [],
      subDisabilityOtherOptionSelected: false,
      subDisabilityOtherContent: "",
      isValidSubDisability: true,
      showProfileChangePopup: false,
      popupNewFirstNameContent: "",
      isValidPopupNewFirstName: true,
      popupNewLastNameContent: "",
      isValidPopupNewLastName: true,
      triggerDateOfBirthValidation: false,
      hideDobErrorMessage: true,
      popupCommentsContent: "",
      popupRightButtonDisabled: true,
      showProfileChangeRequestSentConfirmationPopup: false,
      existingProfileChangeRequest: false,
      birthDateFromExistingRequest: null,
      showDeleteConfirmationProfileChangePopup: false
    };
  }

  createSelectionObject = (value, definition) => {
    let val = value;
    if (value === null || value === undefined) {
      val = 0;
    }
    if (definition === null || definition === undefined) {
      return { value: 0, label: "" };
    }
    return definition.filter(obj => obj.value === val)[0];
  };

  selectionObjectToValue = obj => {
    if (obj === null || obj === undefined) {
      return 0;
    }
    return obj.value;
  };

  selectionMinorityObjectToArrayOfValues = obj => {
    if (obj === []) {
      return "";
    }
    let formattedObj = "";
    for (let i = 0; i < obj.length; i++) {
      // if this is the last item of the obj
      if (i === obj.length - 1) {
        formattedObj += `${obj[i].value}`;
      } else {
        formattedObj += `${obj[i].value},`;
      }
    }
    return formattedObj;
  };

  handleCloseSaveConfirmationPopup = () => {
    this.setState({ displayDataUpdatedConfirmationMsgPopup: false });

    // Reload page to show the right states
    window.location.reload();
  };

  componentDidMount = () => {
    this._isMounted = true;
    refreshGroupSubgroupLevelDropdownLanguage(
      this.props.getGroup,
      this.props.setGroupOptions,
      this.props.getSubClassifications,
      this.props.setSubClassificationOptions
    );
    this.populateDateOfBirthYearOptions();
    this.populateDateOfBirthMonthOptions();
    this.populateDateOfBirthDayOptions();
    this.populateSubAboriginalOptionsData().then(() => {
      this.populateSubVisibleMinorityOptionsData().then(() => {
        this.populateSubDisabilityOptionsData().then(() => {
          this.setState({ isLoadingEEInfo: true }, () => {
            this.props
              .getUserExtendedProfile()
              .then(response => {
                // default, in case something is not returned
                let currentEmployerTemp = null;
                let organizationTemp = null;
                let groupTemp = null;
                let subClassificationTemp = null;
                let levelTemp = null;
                let residenceTemp = null;
                let educationTemp = null;
                let genderTemp = null;
                let identifyAsWoman = "";
                let aboriginal = "";
                let { subAboriginalCheckboxStates } = this.state;
                let visibleMinority = "";
                let { subVisibleMinorityCheckboxStates } = this.state;
                let { subVisibleMinorityOtherOptionSelected } = this.state;
                let subVisibleMinorityOtherContent = "";
                let disability = "";
                let { subDisabilityCheckboxStates } = this.state;
                let { subDisabilityOtherOptionSelected } = this.state;
                let subDisabilityOtherContent = "";

                // if the response is ok, get the values
                if (response.ok) {
                  currentEmployerTemp = response.body.current_employer_id;
                  organizationTemp = response.body.organization_id;
                  groupTemp = response.body.group_id;
                  subClassificationTemp = response.body.subgroup_id;
                  levelTemp = response.body.level_id;
                  residenceTemp = response.body.residence_id;
                  educationTemp = response.body.education_id;
                  genderTemp = response.body.gender_id;

                  // setting identify_as_woman
                  identifyAsWoman = this.getEEGroupRadioButtonState(
                    "identify-as-woman",
                    response.body.identify_as_woman
                  );
                  aboriginal = this.getEEGroupRadioButtonState(
                    "aboriginal",
                    response.body.aboriginal
                  );
                  subAboriginalCheckboxStates = this.getEESubGroupCheckboxStates(
                    "aboriginal",
                    response.body.aboriginal_data
                  ).subGroupCheckboxStates;
                  visibleMinority = this.getEEGroupRadioButtonState(
                    "visible-minority",
                    response.body.visible_minority
                  );
                  subVisibleMinorityCheckboxStates = this.getEESubGroupCheckboxStates(
                    "visible-minority",
                    response.body.visible_minority_data
                  ).subGroupCheckboxStates;
                  subVisibleMinorityOtherOptionSelected = this.getEESubGroupCheckboxStates(
                    "visible-minority",
                    response.body.visible_minority_data
                  ).subGroupOtherOptionSelected;
                  subVisibleMinorityOtherContent = this.getEESubGroupCheckboxStates(
                    "visible-minority",
                    response.body.visible_minority_data
                  ).subGroupOtherContent;
                  disability = this.getEEGroupRadioButtonState(
                    "disability",
                    response.body.disability
                  );
                  subDisabilityCheckboxStates = this.getEESubGroupCheckboxStates(
                    "disability",
                    response.body.disability_data
                  ).subGroupCheckboxStates;
                  subDisabilityOtherOptionSelected = this.getEESubGroupCheckboxStates(
                    "disability",
                    response.body.disability_data
                  ).subGroupOtherOptionSelected;
                  subDisabilityOtherContent = this.getEESubGroupCheckboxStates(
                    "disability",
                    response.body.disability_data
                  ).subGroupOtherContent;
                }

                // set EE info (local states)
                this.setState({
                  identifyAsWoman: identifyAsWoman,
                  aboriginal: aboriginal,
                  subAboriginalCheckboxStates: subAboriginalCheckboxStates,
                  visibleMinority: visibleMinority,
                  subVisibleMinorityCheckboxStates: subVisibleMinorityCheckboxStates,
                  subVisibleMinorityOtherOptionSelected: subVisibleMinorityOtherOptionSelected,
                  subVisibleMinorityOtherContent: subVisibleMinorityOtherContent,
                  disability: disability,
                  subDisabilityCheckboxStates: subDisabilityCheckboxStates,
                  subDisabilityOtherOptionSelected: subDisabilityOtherOptionSelected,
                  subDisabilityOtherContent: subDisabilityOtherContent
                });

                // set the redux state
                this.props.updateExtendedProfileState(
                  currentEmployerTemp,
                  organizationTemp,
                  groupTemp,
                  subClassificationTemp,
                  levelTemp,
                  residenceTemp,
                  educationTemp,
                  genderTemp
                );
              })
              .then(() => {
                this.setState({ isLoadingEEInfo: false }, () => {
                  this.props.getUserProfileChangeRequest(this.props.username).then(response => {
                    // existing profile change request
                    if (typeof response.body.info === "undefined") {
                      // updating popup states
                      this.setState({
                        popupNewFirstNameContent: response.body.new_first_name,
                        isValidPopupNewFirstName: true,
                        popupNewLastNameContent: response.body.new_last_name,
                        isValidPopupNewLastName: true,
                        popupCommentsContent: response.body.comments,
                        popupRightButtonDisabled: false,
                        existingProfileChangeRequest: true,
                        birthDateFromExistingRequest: response.body.new_birth_date
                      });
                    }
                    this.setState({ isLoading: false });
                  });
                });
              });
          });
        });
      });
    });
  };

  componentWillUnmount = () => {
    this._isMounted = false;
  };

  componentDidUpdate = prevProps => {
    // update first name field based on the prop value

    if (prevProps.firstName !== this.props.firstName) {
      this.setState({ firstNameContent: this.props.firstName });
    }
    // update last name field based on the prop value
    if (prevProps.lastName !== this.props.lastName) {
      this.setState({ lastNameContent: this.props.lastName });
    }
    // update primary email field based on the prop value
    if (prevProps.primaryEmail !== this.props.primaryEmail) {
      this.setState({ primaryEmailContent: this.props.primaryEmail });
    }
    // update psrs field based on the prop value
    if (prevProps.psrsAppID !== this.props.psrsAppID) {
      this.setState({ psrsAppIdContent: this.props.psrsAppID });
    }
    // update secondary email field based on the prop value
    if (prevProps.secondaryEmail !== this.props.secondaryEmail) {
      this.setState({
        secondaryEmailContent: this.props.secondaryEmail === null ? "" : this.props.secondaryEmail
      });
    }
    // update date of birth selected value field based on the prop value
    if (prevProps.dateOfBirth !== this.props.dateOfBirth) {
      this.setState({
        // year
        dateOfBirthYearSelectedValue: {
          value: parseInt(this.props.dateOfBirth.split("-")[0]),
          label: `${this.props.dateOfBirth.split("-")[0]}`
        },
        // month
        dateOfBirthMonthSelectedValue: {
          value: parseInt(this.props.dateOfBirth.split("-")[1]),
          label: `${this.props.dateOfBirth.split("-")[1]}`
        },
        // day
        dateOfBirthDaySelectedValue: {
          value: parseInt(this.props.dateOfBirth.split("-")[2]),
          label: `${this.props.dateOfBirth.split("-")[2]}`
        }
      });
    }
    // update pri field based on the prop value
    if (prevProps.pri !== this.props.pri) {
      this.setState({ priContent: this.props.pri });
    }
    // update military number field based on the prop value
    if (prevProps.militaryNbr !== this.props.militaryNbr) {
      this.setState({ militaryNbrContent: this.props.militaryNbr });
    }
    // update currentEmployer field based on the prop value
    if (
      prevProps.currentEmployer !== this.props.currentEmployer ||
      prevProps.employerOptions !== this.props.employerOptions
    ) {
      this.setState({
        currentEmployerContent: this.createSelectionObject(
          this.props.currentEmployer,
          this.props.employerOptions
        )
      });
    }

    // update organization field based on the prop value
    if (
      prevProps.organization !== this.props.organization ||
      prevProps.organizationOptions !== this.props.organizationOptions
    ) {
      this.setState({
        organizationContent: this.createSelectionObject(
          this.props.organization,
          this.props.organizationOptions
        )
      });
    }

    // update group field based on the prop value
    if (
      prevProps.group !== this.props.group ||
      prevProps.groupOptions !== this.props.groupOptions ||
      // the bracketed condition below ensures "N/A" toggles to "S/O" when no group selection has been made
      // AND language is updated from English to French (and vice versa)
      (this.state.groupContent !== undefined &&
        this.state.groupContent.value === 0 && // 0 === "N/A" or "S/O"
        this.state.groupContent.label !== this.props.groupOptions.groups[0].label)
    ) {
      const groupContent = this.createSelectionObject(
        this.props.group,
        this.props.groupOptions.groups
      );
      let subClassificationOptions = [];
      if (groupContent !== undefined) {
        subClassificationOptions = this.generateSubClassificationOptions(
          groupContent.label,
          groupContent.value,
          this.props.groupOptions
        );
      }
      let subClassificationContent = [];

      // this should always be the case when the subgroup does not actually exist (i.e. only direct group->level mappings)
      if (hasNoValidSubgroups(subClassificationOptions)) {
        subClassificationContent = this.createSelectionObject(
          subClassificationOptions[1].value,
          subClassificationOptions
        );
        this.updateNoneSubclassification(subClassificationOptions[1]);
      } else {
        subClassificationContent = this.createSelectionObject(
          this.props.subclassification,
          subClassificationOptions
        );
      }

      this.setState({
        subClassificationOptions: subClassificationOptions,
        groupContent: groupContent,
        subClassificationContent: subClassificationContent
      });
    }

    if (
      (this.props.subClassificationOptions !== undefined &&
        this.props.subClassificationOptions.subClassifications !== undefined &&
        prevProps.subclassification !== this.props.subclassification) ||
      prevProps.subClassificationOptions !== this.props.subClassificationOptions
    ) {
      const subClassificationContent = this.createSelectionObject(
        this.props.subclassification,
        this.props.subClassificationOptions.subClassifications
      );
      let levelOptions = [];
      if (subClassificationContent !== undefined) {
        levelOptions = this.generateLevelOptions(
          subClassificationContent.label,
          subClassificationContent.value,
          this.props.subClassificationOptions
        );
      }
      const levelContent = this.createSelectionObject(this.props.level, levelOptions);
      this.setState({
        levelOptions: levelOptions,
        subClassificationContent: subClassificationContent,
        levelContent: levelContent
      });
    }

    // update level field based on the prop value
    if (prevProps.level !== this.props.level) {
      // levelOptions is not defined yet
      if (this.state.levelOptions.length <= 0) {
        // adding small delay, so the levelOptions state gets updated
        setTimeout(() => {
          this.setState({
            levelContent: this.createSelectionObject(this.props.level, this.state.levelOptions)
          });
        }, 250);
        // levelOptions is defined
      } else {
        this.setState({
          levelContent: this.createSelectionObject(this.props.level, this.state.levelOptions)
        });
      }
    }

    // update residence field based on the prop value
    if (
      prevProps.residence !== this.props.residence ||
      prevProps.residenceOptions !== this.props.residenceOptions
    ) {
      this.setState({
        residenceContent: this.createSelectionObject(
          this.props.residence,
          this.props.residenceOptions
        )
      });
    }

    // update education field based on the prop value
    if (
      prevProps.education !== this.props.education ||
      prevProps.educationOptions !== this.props.educationOptions
    ) {
      this.setState({
        educationContent: this.createSelectionObject(
          this.props.education,
          this.props.educationOptions
        )
      });
    }

    // update gender field based on the prop value
    if (
      prevProps.gender !== this.props.gender ||
      prevProps.genderOptions !== this.props.genderOptions
    ) {
      this.setState({
        genderContent: this.createSelectionObject(this.props.gender, this.props.genderOptions)
      });
    }
  };

  // populate year (DOB) from 1900 to current year
  populateDateOfBirthYearOptions = () => {
    const yearOptionsArray = [];
    // loop from 1900 to current year
    for (let i = 1900; i <= new Date().getFullYear(); i++) {
      // push each value in yearOptionsArray
      yearOptionsArray.push({ value: i, label: `${i}` });
    }
    // sorting (descending order)
    yearOptionsArray.reverse();
    // saving result in state
    if (this._isMounted) {
      this.setState({ dateOfBirthYearOptions: yearOptionsArray });
    }
  };

  // populate month (DOB)
  populateDateOfBirthMonthOptions = () => {
    const monthOptionsArray = [];
    // loop from 1 to 12
    for (let i = 1; i <= 12; i++) {
      // push each value in monthOptionsArray
      monthOptionsArray.push({ value: i, label: `${i}` });
    }
    // saving result in state
    if (this._isMounted) {
      this.setState({ dateOfBirthMonthOptions: monthOptionsArray });
    }
  };

  // populate day (DOB)
  populateDateOfBirthDayOptions = () => {
    const dayOptionsArray = [];
    // loop from 1 to 31
    for (let i = 1; i <= 31; i++) {
      // push each value in dayOptionsArray
      dayOptionsArray.push({ value: i, label: `${i}` });
    }
    // saving result in state
    if (this._isMounted) {
      this.setState({ dateOfBirthDayOptions: dayOptionsArray });
    }
  };

  handleSubGroupsDataFormatting = subGroupResponseData => {
    // initializing needed variables
    const subGroupOptions = [];
    const subGroupCheckboxStates = [];
    // looping in response.body
    for (let i = 0; i < subGroupResponseData.body.length; i++) {
      subGroupOptions.push(subGroupResponseData.body[i]);
      subGroupCheckboxStates.push({ checked: false });
    }
    // handling "prefer not to say" data
    subGroupOptions.push({
      id: 0,
      edesc: LOCALIZE_OBJ.en.commons.preferNotToAnswer,
      fdesc: LOCALIZE_OBJ.fr.commons.preferNotToAnswer
    });
    subGroupCheckboxStates.push({ checked: false });

    return { subGroupOptions: subGroupOptions, subGroupCheckboxStates: subGroupCheckboxStates };
  };

  populateSubAboriginalOptionsData = async () => {
    this.props.getAboriginal().then(response => {
      const subGroupData = this.handleSubGroupsDataFormatting(response);
      // updating needed states
      this.setState({
        subAboriginalOptions: subGroupData.subGroupOptions,
        subAboriginalCheckboxStates: subGroupData.subGroupCheckboxStates
      });
    });
  };

  populateSubVisibleMinorityOptionsData = async () => {
    this.props.getVisibleMinority().then(response => {
      const subGroupData = this.handleSubGroupsDataFormatting(response);
      // updating needed states
      this.setState({
        subVisibleMinorityOptions: subGroupData.subGroupOptions,
        subVisibleMinorityCheckboxStates: subGroupData.subGroupCheckboxStates
      });
    });
  };

  populateSubDisabilityOptionsData = async () => {
    this.props.getDisability().then(response => {
      const subGroupData = this.handleSubGroupsDataFormatting(response);
      // updating needed states
      this.setState({
        subDisabilityOptions: subGroupData.subGroupOptions,
        subDisabilityCheckboxStates: subGroupData.subGroupCheckboxStates
      });
    });
  };

  // update PSRS App ID
  updatePsrsAppID = event => {
    const psrsAppIdContent = event.target.value;
    const regex = /^(([A-Za-z]{1})([0-9]{0,6}))$/;
    // string must contains 7 characters (1 letter and 6 numbers)
    if (psrsAppIdContent === "" || regex.test(psrsAppIdContent)) {
      this.setState({
        psrsAppIdContent
      });
    }
  };

  // update secondary email value
  updateSecondaryEmailValue = event => {
    const secondaryEmailContent = event.target.value;
    this.setState({
      secondaryEmailContent
    });
  };

  // update Current Employer
  updateCurrentEmployer = selectedOption => {
    const currentEmployer = parseInt(selectedOption.value);
    this.props.updateCurrentEmployer(currentEmployer);
  };

  // update Organization
  updateOrganization = selectedOption => {
    const organization = parseInt(selectedOption.value);
    this.props.updateOrganization(organization);
  };

  // update Group
  updateGroup = selectedOption => {
    const groupID = parseInt(selectedOption.value);
    this.props.updateGroup(groupID);
    const subclassificationOptions = this.generateSubClassificationOptions(
      selectedOption.label,
      selectedOption.value,
      this.props.groupOptions
    );
    this.props.updateSubclassification(
      subclassificationOptions.length > 0 ? subclassificationOptions[0].value : []
    );
    this.setState({ subclassificationOptions: subclassificationOptions });
  };

  // update subClassification
  updateSubclassification = selectedOption => {
    const subClassification = parseInt(selectedOption.value);
    this.props.updateSubclassification(subClassification);
    const levelOptions = this.generateLevelOptions(
      selectedOption.label,
      selectedOption.value,
      this.props.subClassificationOptions
    );
    this.props.updateLevel(levelOptions.length > 0 ? levelOptions[0].value : []);
    this.setState({ levelOptions: levelOptions });
  };

  updateNoneSubclassification = selectedOption => {
    const subClassification = parseInt(selectedOption.value);
    this.props.updateSubclassification(subClassification);
    const levelOptions = this.generateLevelOptions(
      selectedOption.label,
      selectedOption.value,
      this.props.subClassificationOptions
    );
    this.setState({
      levelOptions: levelOptions,
      levelContent: this.createSelectionObject(this.props.level, this.state.levelOptions)
    });
  };

  // update Level
  updateLevel = selectedOption => {
    const level = parseInt(selectedOption.value);
    this.props.updateLevel(level);
  };

  // update Residence
  updateResidence = selectedOption => {
    const residence = parseInt(selectedOption.value);
    this.props.updateResidence(residence);
  };

  // update Education
  updateEducation = selectedOption => {
    const education = parseInt(selectedOption.value);
    this.props.updateEducation(education);
  };

  // update Gender
  updateGender = selectedOption => {
    const gender = parseInt(selectedOption.value);
    this.props.updateGender(gender);
  };

  // get selected year (DOB)
  getSelectedDateOfBirthYear = selectedOption => {
    this.setState({ dateOfBirthYearSelectedValue: selectedOption });
  };

  // get selected month (DOB)
  getSelectedDateOfBirthMonth = selectedOption => {
    this.setState({ dateOfBirthMonthSelectedValue: selectedOption });
  };

  // get selected day (DOB)
  getSelectedDateOfBirthDay = selectedOption => {
    this.setState({ dateOfBirthDaySelectedValue: selectedOption });
  };

  // update pri value
  updatePriValue = event => {
    const priContent = event.target.value;
    /* only the following can be inserted into this field:
          - 0 to 9 numbers
    */
    const regex = /^([0-9]{0,9})$/;
    if (event.target.value === "" || regex.test(event.target.value)) {
      this.setState({
        priContent
      });
    }
  };

  // update pri or military number value
  updateMilitaryNbrValue = event => {
    const militaryNbrContent = event.target.value;
    /* only the following can be inserted into this field:
            - 1 letter followed by 0 to 6 numbers
      */
    const regex = /^(([A-Za-z]{1})([0-9]{0,6}))$/;
    if (event.target.value === "" || regex.test(event.target.value)) {
      this.setState({
        militaryNbrContent
      });
    }
  };

  handleRadioButtonClick = (event, idPrefix) => {
    switch (idPrefix) {
      // identify as a woman
      case "identify-as-woman":
        this.setState({ identifyAsWoman: event.target.id, isValidIdentifyAsWoman: true });
        break;
      // aboriginal
      case "aboriginal":
        this.setState({ aboriginal: event.target.id, isValidAboriginal: true });
        break;
      // visible-minority
      case "visible-minority":
        this.setState({ visibleMinority: event.target.id, isValidVisibleMinority: true });
        break;
      // disability
      case "disability":
        this.setState({ disability: event.target.id, isValidDisability: true });
        break;
      default:
        break;
    }
  };

  // getting formatted group data (YES, NO, PREFER NOT TO SAY)
  getFormattedEEGroupData = groupName => {
    // initializing formattedData
    let formattedData = null;
    switch (groupName) {
      // identify as a woman
      case "identify-as-woman":
        if (this.state.identifyAsWoman === "identify-as-woman-yes-radio-button-id") {
          formattedData = EXTENDED_PROFILE_EE_INFO.yes;
        } else if (this.state.identifyAsWoman === "identify-as-woman-no-radio-button-id") {
          formattedData = EXTENDED_PROFILE_EE_INFO.no;
        } else {
          formattedData = EXTENDED_PROFILE_EE_INFO.preferNotToSay;
        }
        break;
      // aboriginal
      case "aboriginal":
        if (this.state.aboriginal === "aboriginal-yes-radio-button-id") {
          formattedData = EXTENDED_PROFILE_EE_INFO.yes;
        } else if (this.state.aboriginal === "aboriginal-no-radio-button-id") {
          formattedData = EXTENDED_PROFILE_EE_INFO.no;
        } else {
          formattedData = EXTENDED_PROFILE_EE_INFO.preferNotToSay;
        }
        break;
      // visible minority
      case "visible-minority":
        if (this.state.visibleMinority === "visible-minority-yes-radio-button-id") {
          formattedData = EXTENDED_PROFILE_EE_INFO.yes;
        } else if (this.state.visibleMinority === "visible-minority-no-radio-button-id") {
          formattedData = EXTENDED_PROFILE_EE_INFO.no;
        } else {
          formattedData = EXTENDED_PROFILE_EE_INFO.preferNotToSay;
        }
        break;
      // disability
      case "disability":
        if (this.state.disability === "disability-yes-radio-button-id") {
          formattedData = EXTENDED_PROFILE_EE_INFO.yes;
        } else if (this.state.disability === "disability-no-radio-button-id") {
          formattedData = EXTENDED_PROFILE_EE_INFO.no;
        } else {
          formattedData = EXTENDED_PROFILE_EE_INFO.preferNotToSay;
        }
        break;
      default:
        break;
    }
    return formattedData;
  };

  // getting formatted sub group data (checkbox options)
  getFormattedEESubGroupData = groupName => {
    // initializing formattedData
    const formattedData = [];
    switch (groupName) {
      // aboriginal
      case "aboriginal":
        // looping in subAboriginalCheckboxStates
        for (let i = 0; i < this.state.subAboriginalCheckboxStates.length; i++) {
          // if checkbox enabled
          if (this.state.subAboriginalCheckboxStates[i].checked === true) {
            formattedData.push(
              typeof this.state.subAboriginalOptions[i].abrg_id !== "undefined"
                ? this.state.subAboriginalOptions[i].abrg_id
                : 0
            );
          }
        }
        break;
      // visible minority
      case "visible-minority":
        // looping in subVisibleMinorityCheckboxStates
        for (let i = 0; i < this.state.subVisibleMinorityCheckboxStates.length; i++) {
          // if checkbox enabled
          if (this.state.subVisibleMinorityCheckboxStates[i].checked === true) {
            formattedData.push(
              typeof this.state.subVisibleMinorityOptions[i].vismin_id !== "undefined"
                ? this.state.subVisibleMinorityOptions[i].vismin_id
                : 0
            );
          }
        }
        break;
      // disability
      case "disability":
        // looping in subDisabilityCheckboxStates
        for (let i = 0; i < this.state.subDisabilityCheckboxStates.length; i++) {
          // if checkbox enabled
          if (this.state.subDisabilityCheckboxStates[i].checked === true) {
            formattedData.push(
              typeof this.state.subDisabilityOptions[i].dsbl_id !== "undefined"
                ? this.state.subDisabilityOptions[i].dsbl_id
                : 0
            );
          }
        }
        break;
      default:
        break;
    }
    return formattedData;
  };

  // getting radio button state
  getEEGroupRadioButtonState = (groupName, value) => {
    // initializing radioButtonState
    let radioButtonState = null;
    switch (groupName) {
      // identify as a woman
      case "identify-as-woman":
        if (value === EXTENDED_PROFILE_EE_INFO.yes) {
          radioButtonState = "identify-as-woman-yes-radio-button-id";
        } else if (value === EXTENDED_PROFILE_EE_INFO.no) {
          radioButtonState = "identify-as-woman-no-radio-button-id";
        } else {
          radioButtonState = "identify-as-woman-prefer-not-to-answer-radio-button-id";
        }
        break;
      // aboriginal
      case "aboriginal":
        if (value === EXTENDED_PROFILE_EE_INFO.yes) {
          radioButtonState = "aboriginal-yes-radio-button-id";
        } else if (value === EXTENDED_PROFILE_EE_INFO.no) {
          radioButtonState = "aboriginal-no-radio-button-id";
        } else {
          radioButtonState = "aboriginal-prefer-not-to-answer-radio-button-id";
        }
        break;
      // visible minority
      case "visible-minority":
        if (value === EXTENDED_PROFILE_EE_INFO.yes) {
          radioButtonState = "visible-minority-yes-radio-button-id";
        } else if (value === EXTENDED_PROFILE_EE_INFO.no) {
          radioButtonState = "visible-minority-no-radio-button-id";
        } else {
          radioButtonState = "visible-minority-prefer-not-to-answer-radio-button-id";
        }
        break;
      // disability
      case "disability":
        if (value === EXTENDED_PROFILE_EE_INFO.yes) {
          radioButtonState = "disability-yes-radio-button-id";
        } else if (value === EXTENDED_PROFILE_EE_INFO.no) {
          radioButtonState = "disability-no-radio-button-id";
        } else {
          radioButtonState = "disability-prefer-not-to-answer-radio-button-id";
        }
        break;
      default:
        break;
    }
    return radioButtonState;
  };

  // getting chackbox states
  getEESubGroupCheckboxStates = (groupName, subGroupdata) => {
    // initializing needed variables
    let subGroupCheckboxStates = [];
    let subGroupOtherOptionSelected = false;
    let subGroupOtherContent = "";
    const indexesArray = [];
    switch (groupName) {
      // aboriginal
      case "aboriginal":
        // looping in subAboriginalOptions
        for (let i = 0; i < this.state.subAboriginalOptions.length; i++) {
          // looping in subGroupData
          for (let j = 0; j < subGroupdata.length; j++) {
            // if subGroupData id is matching the current option
            if (this.state.subAboriginalOptions[i].abrg_id === subGroupdata[j].abrg_id) {
              indexesArray.push(i);
            }
            // if prefer not to say is selected
            if (subGroupdata[j].abrg_id === 0) {
              indexesArray.push(this.state.subAboriginalOptions.length - 1);
            }
          }
        }
        // creating copy of subGroupCheckboxStates
        subGroupCheckboxStates = this.state.subAboriginalCheckboxStates;
        // looping in indexesArray
        for (let i = 0; i < indexesArray.length; i++) {
          subGroupCheckboxStates[indexesArray[i]].checked = true;
        }
        break;
      // visible minority
      case "visible-minority":
        // looping in subVisibleMinorityOptions
        for (let i = 0; i < this.state.subVisibleMinorityOptions.length; i++) {
          // looping in subGroupData
          for (let j = 0; j < subGroupdata.length; j++) {
            // if subGroupData id is matching the current option
            if (this.state.subVisibleMinorityOptions[i].vismin_id === subGroupdata[j].vismin_id) {
              indexesArray.push(i);
            }
            // checking if other option has been selected
            if (subGroupdata[j].vismin_id === VISIBLE_MINORITY_ID.other) {
              subGroupOtherOptionSelected = true;
              subGroupOtherContent = subGroupdata[j].other_specification;
            }
            // if prefer not to say is selected
            if (subGroupdata[j].vismin_id === 0) {
              indexesArray.push(this.state.subVisibleMinorityOptions.length - 1);
            }
          }
        }
        // creating copy of subGroupCheckboxStates
        subGroupCheckboxStates = this.state.subVisibleMinorityCheckboxStates;
        // looping in indexesArray
        for (let i = 0; i < indexesArray.length; i++) {
          subGroupCheckboxStates[indexesArray[i]].checked = true;
        }
        break;
      // disability
      case "disability":
        // looping in subDisabilityOptions
        for (let i = 0; i < this.state.subDisabilityOptions.length; i++) {
          // looping in subGroupData
          for (let j = 0; j < subGroupdata.length; j++) {
            // if subGroupData id is matching the current option
            if (this.state.subDisabilityOptions[i].dsbl_id === subGroupdata[j].dsbl_id) {
              indexesArray.push(i);
            }
            // checking if other option has been selected
            if (subGroupdata[j].dsbl_id === DISABILITY_ID.other) {
              subGroupOtherOptionSelected = true;
              subGroupOtherContent = subGroupdata[j].other_specification;
            }
            // if prefer not to say is selected
            if (subGroupdata[j].dsbl_id === 0) {
              indexesArray.push(this.state.subDisabilityOptions.length - 1);
            }
          }
        }
        // creating copy of subGroupCheckboxStates
        subGroupCheckboxStates = this.state.subDisabilityCheckboxStates;
        // looping in indexesArray
        for (let i = 0; i < indexesArray.length; i++) {
          subGroupCheckboxStates[indexesArray[i]].checked = true;
        }
        break;
      default:
        break;
    }
    return {
      subGroupCheckboxStates: subGroupCheckboxStates,
      subGroupOtherOptionSelected: subGroupOtherOptionSelected,
      subGroupOtherContent: subGroupOtherContent
    };
  };

  // save user personal info in the database
  handleSaveData = () => {
    // triggering DOB validation
    this.triggerDobValidation().then(() => {
      // data is valid
      if (this.validatePersonalInfoData()) {
        const data = {
          username: this.state.usernameContent,
          psrsAppID: this.state.psrsAppIdContent,
          secondaryEmail: this.state.secondaryEmailContent,
          pri: this.state.priContent.toUpperCase(),
          military_service_nbr: this.state.militaryNbrContent.toUpperCase()
        };
        const extendedData = {
          username: data.username,
          current_employer_id: this.selectionObjectToValue(this.state.currentEmployerContent),
          organization_id: this.selectionObjectToValue(this.state.organizationContent),
          group_id: this.selectionObjectToValue(this.state.groupContent),
          subclassification_id: this.selectionObjectToValue(this.state.subClassificationContent),
          level_id: this.selectionObjectToValue(this.state.levelContent),
          residence_id: this.selectionObjectToValue(this.state.residenceContent),
          education_id: this.selectionObjectToValue(this.state.educationContent),
          gender_id: this.selectionObjectToValue(this.state.genderContent),
          identify_as_woman: this.getFormattedEEGroupData("identify-as-woman"),
          aboriginal: this.getFormattedEEGroupData("aboriginal"),
          aboriginal_ids: this.getFormattedEESubGroupData("aboriginal"),
          visible_minority: this.getFormattedEEGroupData("visible-minority"),
          visible_minority_ids: this.getFormattedEESubGroupData("visible-minority"),
          visible_minority_other_specification_content: this.state.subVisibleMinorityOtherContent,
          disability: this.getFormattedEEGroupData("disability"),
          disability_ids: this.getFormattedEESubGroupData("disability"),
          disability_other_specification_content: this.state.subDisabilityOtherContent
        };
        this.props.updateUserPersonalInfo(data).then(response => {
          // request succeeded
          if (response.ok) {
            // setting user profile data
            this.props.setUserInformation(
              this.props.firstName,
              this.props.lastName,
              data.username,
              this.props.isSuperUser,
              this.props.lastPasswordChange,
              data.psrsAppID,
              this.props.primaryEmail,
              data.secondaryEmail,
              `${this.props.completeDatePicked.split("-")[0]}-${
                this.props.completeDatePicked.split("-")[1]
              }-${this.props.completeDatePicked.split("-")[2]}`,
              data.pri,
              data.military_service_nbr,
              // set profileCompleted to true
              true
            );
            this.setState({ displayDataUpdatedConfirmationMsgPopup: true });
            // should never happen
          } else {
            this.setState({ displayDataUpdatedConfirmationMsgPopup: false });
            throw new Error("An error has occurred during the data update");
          }
        });
        this.props.updateUserExtendedProfile(extendedData).then(response => {
          // request succeeded
          if (response.ok) {
            this.setState({ displayDataUpdatedConfirmationMsgPopup: true });
            // should never happen
          } else {
            this.setState({ displayDataUpdatedConfirmationMsgPopup: false });
            // setting user profile data
            this.props.setUserInformation(
              this.props.firstName,
              this.props.lastName,
              data.username,
              this.props.isSuperUser,
              this.props.lastPasswordChange,
              data.psrsAppID,
              this.props.primaryEmail,
              data.secondaryEmail,
              `${this.props.completeDatePicked.split("-")[0]}-${
                this.props.completeDatePicked.split("-")[1]
              }-${this.props.completeDatePicked.split("-")[2]}`,
              data.pri,
              data.militaryNbr,
              // set profileCompleted to false
              false
            );
            throw new Error("An error has occurred during the extended profile update");
          }
        });
      }
    });
  };

  // validate personal info data that is editable only (needs to be done before sending data in the database)
  validatePersonalInfoData = () => {
    // validating fields
    const isValidPri = validatePri(this.state.priContent);
    const isValidMilitaryNbr = validateMilitaryNbr(this.state.militaryNbrContent);

    const isValidPSRSApplicantID = validatePSRSAppID(this.state.psrsAppIdContent);
    const isValidIdentifyAsWoman = this.state.identifyAsWoman !== "";
    const isValidAboriginal = this.state.aboriginal !== "";
    let isValidSubAboriginal = false;
    const isValidVisibleMinority = this.state.visibleMinority !== "";
    let isValidSubVisibleMinority = false;
    const isValidDisability = this.state.disability !== "";
    let isValidSubDisability = false;

    // validating secondary email field
    let isValidSecondaryEmail = false;
    // if secondary email string is not empty, validate it using the regex validator
    if (this.state.secondaryEmailContent !== "") {
      isValidSecondaryEmail = validateEmail(this.state.secondaryEmailContent);
      // else the field is empty, so valid since this is not a mandatory field
    } else {
      isValidSecondaryEmail = true;
    }

    // validating sub aboriginal groups
    // aboriginal option is unselected, no or prefer not to say
    if (this.state.aboriginal !== "aboriginal-yes-radio-button-id") {
      isValidSubAboriginal = true;
      // aboriginal option is yes
    } else {
      // checking if at least one checkbox is checked
      // looping in subAboriginalCheckboxStates
      for (let i = 0; i < this.state.subAboriginalCheckboxStates.length; i++) {
        // checkbox checked
        if (this.state.subAboriginalCheckboxStates[i].checked === true) {
          isValidSubAboriginal = true;
          break;
        }
      }
    }

    // validating sub visible minority groups
    // visible minority option is unselected, no or prefer not to say
    if (this.state.visibleMinority !== "visible-minority-yes-radio-button-id") {
      isValidSubVisibleMinority = true;
      // visible minority option is yes
    } else {
      // checking if at least one checkbox is checked
      // looping in subVisibleMinorityCheckboxStates
      for (let i = 0; i < this.state.subVisibleMinorityCheckboxStates.length; i++) {
        // checkbox checked
        if (this.state.subVisibleMinorityCheckboxStates[i].checked === true) {
          isValidSubVisibleMinority = true;
          break;
        }
      }
    }

    // validating sub disability groups
    // disability option is unselected, no or prefer not to say
    if (this.state.disability !== "disability-yes-radio-button-id") {
      isValidSubDisability = true;
      // disability option is yes
    } else {
      // checking if at least one checkbox is checked
      // looping in subDisabilityCheckboxStates
      for (let i = 0; i < this.state.subDisabilityCheckboxStates.length; i++) {
        // checkbox checked
        if (this.state.subDisabilityCheckboxStates[i].checked === true) {
          isValidSubDisability = true;
          break;
        }
      }
    }

    // updating fields state
    this.setState(
      {
        isValidPri,
        isValidMilitaryNbr,
        isValidPSRSApplicantID,
        isValidSecondaryEmail,
        isValidIdentifyAsWoman: isValidIdentifyAsWoman,
        isValidAboriginal: isValidAboriginal,
        isValidSubAboriginal: isValidSubAboriginal,
        isValidVisibleMinority: isValidVisibleMinority,
        isValidSubVisibleMinority: isValidSubVisibleMinority,
        isValidDisability: isValidDisability,
        isValidSubDisability: isValidSubDisability
      },
      () => {
        // focusing on highest invalid field
        this.focusOnHighestInvalidField();
      }
    );
    // making sure that all needed fields are valid
    if (
      isValidPri &&
      isValidMilitaryNbr &&
      isValidSecondaryEmail &&
      this.props.completeDateValidState &&
      isValidPSRSApplicantID &&
      isValidIdentifyAsWoman &&
      isValidAboriginal &&
      isValidSubAboriginal &&
      isValidVisibleMinority &&
      isValidSubVisibleMinority &&
      isValidDisability &&
      isValidSubDisability
    ) {
      // whole data is valid
      return true;
      // there is at least one invalid field
    }
    return false;
  };

  // focus on highest invalid field
  focusOnHighestInvalidField = () => {
    if (!this.state.isValidSecondaryEmail) {
      document.getElementById("secondary-email-input").focus();
    } else if (!this.props.completeDateValidState) {
      this.dateOfBirthDayFieldRef.current.focus();
    } else if (!this.state.isValidPSRSApplicantID) {
      document.getElementById("psrs-applicant-id-input").focus();
    } else if (!this.state.isValidPri) {
      document.getElementById("pri-input").focus();
    } else if (!this.state.isValidMilitaryNbr) {
      document.getElementById("military-number-input").focus();
    } else if (!this.state.isValidIdentifyAsWoman) {
      document.getElementById("identify-as-woman-radio-buttons-error-window").focus();
    } else if (!this.state.isValidAboriginal) {
      document.getElementById("aboriginal-radio-buttons-error-window").focus();
    } else if (!this.state.isValidSubAboriginal) {
      document.getElementById("aboriginal-checkboxes-error-window").focus();
    } else if (!this.state.isValidVisibleMinority) {
      document.getElementById("visible-minority-radio-buttons-error-window").focus();
    } else if (!this.state.isValidSubVisibleMinority) {
      document.getElementById("visible-minority-checkboxes-error-window").focus();
    } else if (!this.state.isValidDisability) {
      document.getElementById("disability-radio-buttons-error-window").focus();
    } else if (!this.state.isValidSubDisability) {
      document.getElementById("disability-checkboxes-error-window").focus();
    }
  };

  generateSubClassificationOptions = (groupN, classifId, groupOptions) => {
    let groupName = groupN;
    let groupValue = {};
    if (groupN === undefined) {
      if (groupOptions.groups.length !== 0) {
        // if undefined, get the first group name and value
        groupName = groupOptions.groups[0].label;
        groupValue = groupOptions.groups[0].value;
      }
    }
    let sub_classifications = [];
    // if groupName is defined (not N/A) and groupSubClassifications contains options:
    if (
      groupName !== LOCALIZE.commons.na &&
      groupValue !== 0 &&
      groupOptions.groupSubClassifications !== undefined &&
      groupOptions.groupSubClassifications.length > 0
    ) {
      // getting subClassification data
      const sub_classification_data = groupOptions.groupSubClassifications.filter(
        obj => obj[`${groupName}`]
      )[0][`${groupName}`];
      // looping in subClassification
      for (let i = 0; i < sub_classification_data.length; i++) {
        // populating subClassification array
        sub_classifications.push({
          label: sub_classification_data[i].subclassification,
          value: sub_classification_data[i].classif_id
        });
      }
    } else if (groupName === LOCALIZE.commons.none) {
      const sub_classification_data = groupOptions.groupSubClassifications.filter(
        obj => obj[`${classifId}`]
      )[`${classifId}`][0];
      // looping in subClassification
      for (let i = 0; i < sub_classification_data.length; i++) {
        // populating subClassification array
        sub_classifications.push({
          label: sub_classification_data[i].subclassification,
          value: sub_classification_data[i].classif_id
        });
      }
    }
    sub_classifications = addNAOption(sub_classifications);
    return sub_classifications;
  };

  generateLevelOptions = (subclassifName, classifId, subClassificationOptions) => {
    let subclassificationName = subclassifName;
    let subclassificationValue = {};
    if (subclassifName === undefined) {
      if (subClassificationOptions.subClassifications.length !== 0) {
        // if undefined, get the first group name and value
        subclassificationName = subClassificationOptions.subClassifications[0].label;
        subclassificationValue = subClassificationOptions.subClassifications[0].value;
      }
    }
    let levels = [];
    // if subclassificationName is defined (not N/A) and groupLevels contains options
    if (
      subclassificationName !== LOCALIZE.commons.na &&
      subclassificationName !== LOCALIZE.commons.none &&
      subclassificationValue !== 0 &&
      subClassificationOptions.subClassificationLevels.length > 0
    ) {
      // getting level data
      let level_data = [];
      level_data = subClassificationOptions.subClassificationLevels.filter(
        obj => obj[`${subclassificationName}`]
      )[0][`${subclassificationName}`];
      // looping in level_data
      for (let i = 0; i < level_data.length; i++) {
        // populating levels array
        levels.push({ label: level_data[i].level, value: level_data[i].classif_id });
      }
    } else if (
      subclassificationName === LOCALIZE.commons.none &&
      subclassificationName !== undefined &&
      subClassificationOptions.subClassificationLevels.length > 0
    ) {
      let none_group_index = 0;

      for (let i = 0; i < subClassificationOptions.subClassifications.length; i++) {
        if (subClassificationOptions.subClassifications[i].value === classifId) {
          none_group_index = i;
          break;
        }
      }
      let level_data = [];
      level_data = subClassificationOptions.subClassificationLevels[none_group_index];

      const level_data_array = level_data[`${subclassificationName}`];
      // looping in level_data

      if (level_data_array !== undefined) {
        for (let i = 0; i < level_data_array.length; i++) {
          // populating levels array
          levels.push({ label: level_data_array[i].level, value: level_data_array[i].classif_id });
        }
      }
    }

    levels = addNAOption(levels);
    return levels;
  };

  showPrivacyNoticePopup = () => {
    this.setState({ showPrivacyNoticeDialog: true });
  };

  closePrivacyNoticePopup = () => {
    this.setState({ showPrivacyNoticeDialog: false });
  };

  generateRadioButtonChoices = (idPrefix, radioButtonStyle, state, isValid) => {
    // creating all needed radio button options (Yes, No and Prefer not to say)
    return (
      <Col
        id={`${idPrefix}-radio-buttons-error-window`}
        xl={columnSizes.radioButtonChoicesColumn.xl}
        lg={columnSizes.radioButtonChoicesColumn.lg}
        md={columnSizes.radioButtonChoicesColumn.md}
        sm={columnSizes.radioButtonChoicesColumn.sm}
        xs={columnSizes.radioButtonChoicesColumn.xs}
        className={isValid ? "" : "invalid-field"}
        style={styles.radioButtonsContainer}
        tabIndex={!isValid && "0"}
      >
        <fieldset id={`${idPrefix}-radio-buttons-group`}>
          <Row className="align-items-center">
            <div className="d-flex">
              <div>
                <input
                  id={`${idPrefix}-yes-radio-button-id`}
                  type="radio"
                  name={`${idPrefix}-radio-buttons`}
                  aria-labelledby={
                    isValid
                      ? `${idPrefix} ${idPrefix}-yes-radio-button-label`
                      : `${idPrefix} ${idPrefix}-error-message ${idPrefix}-yes-radio-button-label`
                  }
                  style={{ ...styles.radioButton, ...radioButtonStyle }}
                  onChange={e => this.handleRadioButtonClick(e, idPrefix)}
                  checked={state === `${idPrefix}-yes-radio-button-id`}
                />
              </div>
              <div>
                <label
                  id={`${idPrefix}-yes-radio-button-label`}
                  htmlFor={`${idPrefix}-yes-radio-button-id`}
                  style={styles.radioButtonLabel}
                >
                  {LOCALIZE.commons.yes}
                </label>
              </div>
            </div>
          </Row>
          <Row className="align-items-center">
            <div className="d-flex">
              <div>
                <input
                  id={`${idPrefix}-no-radio-button-id`}
                  type="radio"
                  name={`${idPrefix}-radio-buttons`}
                  aria-labelledby={
                    isValid
                      ? `${idPrefix} ${idPrefix}-no-radio-button-label`
                      : `${idPrefix} ${idPrefix}-error-message ${idPrefix}-no-radio-button-label`
                  }
                  style={{ ...styles.radioButton, ...radioButtonStyle }}
                  onChange={e => this.handleRadioButtonClick(e, idPrefix)}
                  checked={state === `${idPrefix}-no-radio-button-id`}
                />
              </div>
              <div>
                <label
                  id={`${idPrefix}-no-radio-button-label`}
                  htmlFor={`${idPrefix}-no-radio-button-id`}
                  style={styles.radioButtonLabel}
                >
                  {LOCALIZE.commons.no}
                </label>
              </div>
            </div>
          </Row>
          <Row className="align-items-center">
            <div className="d-flex">
              <div>
                <input
                  id={`${idPrefix}-prefer-not-to-answer-radio-button-id`}
                  type="radio"
                  name={`${idPrefix}-radio-buttons`}
                  aria-labelledby={
                    isValid
                      ? `${idPrefix} ${idPrefix}-prefer-not-to-answer-radio-button-label`
                      : `${idPrefix} ${idPrefix}-error-message ${idPrefix}-prefer-not-to-answer-radio-button-label`
                  }
                  style={{ ...styles.radioButton, ...radioButtonStyle }}
                  onChange={e => this.handleRadioButtonClick(e, idPrefix)}
                  checked={state === `${idPrefix}-prefer-not-to-answer-radio-button-id`}
                />
              </div>
              <div>
                <label
                  id={`${idPrefix}-prefer-not-to-answer-radio-button-label`}
                  htmlFor={`${idPrefix}-prefer-not-to-answer-radio-button-id`}
                  style={styles.radioButtonLabel}
                >
                  {LOCALIZE.commons.preferNotToAnswer}
                </label>
              </div>
            </div>
          </Row>
        </fieldset>
      </Col>
    );
  };

  generateCheckBoxesChoices = (
    idPrefix,
    subGroupOptions,
    subGroupCheckboxStates,
    isValid,
    checkboxTransformScale
  ) => {
    return (
      <Col
        id={`${idPrefix}-checkboxes-error-window`}
        xl={columnSizes.radioButtonChoicesColumn.xl}
        lg={columnSizes.radioButtonChoicesColumn.lg}
        md={columnSizes.radioButtonChoicesColumn.md}
        sm={columnSizes.radioButtonChoicesColumn.sm}
        xs={columnSizes.radioButtonChoicesColumn.xs}
        className={isValid ? "" : "invalid-field"}
        style={styles.inputContainer}
        tabIndex={!isValid && "0"}
        aria-labelledby={!isValid && `${idPrefix}-sub-group-error-message`}
      >
        {subGroupOptions.map((subGroupData, index) => {
          return (
            <div key={`${idPrefix}-key-${index}`} className="d-flex">
              <div>
                <input
                  id={`${idPrefix}-checkbox-${index}`}
                  aria-labelledby={
                    // if first or last checkbox item
                    index === 0 || index === subGroupCheckboxStates.length - 1
                      ? // read the sub group label + checkbox label
                        `${idPrefix}-sub-groups ${idPrefix}-checkbox-label-${index}`
                      : // only read the checkbox label
                        `${idPrefix}-checkbox-label-${index}`
                  }
                  type="checkbox"
                  style={{ ...styles.checkbox, ...{ transform: checkboxTransformScale } }}
                  onChange={() =>
                    this.changeCheckboxStatus(index, idPrefix, subGroupOptions.length)
                  }
                  checked={subGroupCheckboxStates[index].checked}
                />
              </div>
              <div>
                <label
                  id={`${idPrefix}-checkbox-label-${index}`}
                  htmlFor={`${idPrefix}-checkbox-${index}`}
                >
                  {this.props.currentLanguage === LANGUAGES.english
                    ? `${subGroupData.edesc} ${
                        subGroupData.full_edesc !== null &&
                        typeof subGroupData.full_edesc !== "undefined"
                          ? subGroupData.full_edesc
                          : ""
                      }`
                    : `${subGroupData.fdesc} ${
                        subGroupData.full_fdesc !== null &&
                        typeof subGroupData.full_fdesc !== "undefined"
                          ? subGroupData.full_fdesc
                          : ""
                      }`}
                </label>
              </div>
            </div>
          );
        })}
      </Col>
    );
  };

  changeCheckboxStatus = (index, idPrefix, arrayLength) => {
    switch (idPrefix) {
      // aboriginal
      case "aboriginal":
        // creating a copy of subAboriginalCheckboxStates
        let copyOfSubAboriginalCheckboxStates = Array.from(this.state.subAboriginalCheckboxStates);
        // handling state updates
        copyOfSubAboriginalCheckboxStates = this.handlePreferNotToSayOptionSelection(
          index,
          arrayLength,
          copyOfSubAboriginalCheckboxStates
        );
        // updating needed states
        this.setState({
          subAboriginalCheckboxStates: copyOfSubAboriginalCheckboxStates,
          isValidSubAboriginal: true
        });
        break;
      // visible-minority
      case "visible-minority":
        // creating a copy of subAboriginalCheckboxStates
        let copyOfSubVisibleMinorityCheckboxStates = Array.from(
          this.state.subVisibleMinorityCheckboxStates
        );
        // handling state updates
        copyOfSubVisibleMinorityCheckboxStates = this.handlePreferNotToSayOptionSelection(
          index,
          arrayLength,
          copyOfSubVisibleMinorityCheckboxStates
        );
        // checking if "other" option has been selected
        let subVisibleMinorityOtherOptionSelected = false;
        // getting index of "other" option (VISMIN_ID: 22)
        const indexOfOtherVisibleMinorityOption = this.state.subVisibleMinorityOptions.findIndex(
          obj => {
            return obj.vismin_id === VISIBLE_MINORITY_ID.other;
          }
        );
        // getting state of "other" option checkbox
        const otherVisibleMinorityCheckboxState =
          copyOfSubVisibleMinorityCheckboxStates[indexOfOtherVisibleMinorityOption].checked;
        if (otherVisibleMinorityCheckboxState === true) {
          subVisibleMinorityOtherOptionSelected = true;
        }
        // updating needed states
        this.setState({
          subVisibleMinorityCheckboxStates: copyOfSubVisibleMinorityCheckboxStates,
          subVisibleMinorityOtherOptionSelected: subVisibleMinorityOtherOptionSelected,
          isValidSubVisibleMinority: true
        });
        break;
      // visible-minority
      case "disability":
        // creating a copy of subAboriginalCheckboxStates
        let copyOfSubDisabilityCheckboxStates = Array.from(this.state.subDisabilityCheckboxStates);
        // handling state updates
        copyOfSubDisabilityCheckboxStates = this.handlePreferNotToSayOptionSelection(
          index,
          arrayLength,
          copyOfSubDisabilityCheckboxStates
        );
        // checking if "other" option has been selected
        let subDisabilityOtherOptionSelected = false;
        // getting index of "other" option (DSBL_ID: 6)
        const indexOfOtherDisabilityOption = this.state.subDisabilityOptions.findIndex(obj => {
          return obj.dsbl_id === 6;
        });
        // getting state of "other" option checkbox
        const otherDisabilityCheckboxState =
          copyOfSubDisabilityCheckboxStates[indexOfOtherDisabilityOption].checked;
        if (otherDisabilityCheckboxState === true) {
          subDisabilityOtherOptionSelected = true;
        }
        // updating needed states
        this.setState({
          subDisabilityCheckboxStates: copyOfSubDisabilityCheckboxStates,
          subDisabilityOtherOptionSelected: subDisabilityOtherOptionSelected,
          isValidSubDisability: true
        });
        break;
      default:
        break;
    }
  };

  handlePreferNotToSayOptionSelection = (index, arrayLength, checkboxStatesArray) => {
    // if "Prefer not to say" option is selected
    if (index === arrayLength - 1) {
      // "Prefer not to say" option is checked
      if (checkboxStatesArray[index].checked === false) {
        // looping in checkboxStatesArray
        for (let i = 0; i < checkboxStatesArray.length; i++) {
          // not last iteration
          if (i < checkboxStatesArray.length - 1) {
            checkboxStatesArray[i].checked = false;
          } else {
            checkboxStatesArray[i].checked = true;
          }
        }
        // "Prefer not to say" option is unchecked
      } else {
        checkboxStatesArray[arrayLength - 1].checked = false;
      }
      // if other option is selected
    } else {
      // updating needed checkbox
      checkboxStatesArray[index].checked = !checkboxStatesArray[index].checked;
      // unchecking last option (prefer not to say)
      checkboxStatesArray[arrayLength - 1].checked = false;
    }
    return checkboxStatesArray;
  };

  // update sub visible minority other specification content
  updateSubVisibleMinorityOtherContent = event => {
    const subVisibleMinorityOtherContent = event.target.value;
    this.setState({
      subVisibleMinorityOtherContent: subVisibleMinorityOtherContent
    });
  };

  // update sub disability other specification content
  updateSubDisabilityOtherContent = event => {
    const subDisabilityOtherContent = event.target.value;
    this.setState({
      subDisabilityOtherContent: subDisabilityOtherContent
    });
  };

  openProfileChangePopup = () => {
    this.setState({
      showProfileChangePopup: true,
      showProfileChangeRequestSentConfirmationPopup: false,
      showDeleteConfirmationProfileChangePopup: false
    });
  };

  openDeleteConfirmationProfileChangeRequestPopup = () => {
    this.setState({
      showDeleteConfirmationProfileChangePopup: true
    });
  };

  closeDeleteConfirmationProfileChangeRequestPopup = () => {
    this.setState({
      showDeleteConfirmationProfileChangePopup: false
    });
  };

  handleDeleteProfileChangeRequest = () => {
    const body = {
      username: this.props.username
    };
    this.props.deleteUserProfileChangeRequest(body).then(response => {
      // successful request
      if (response.ok) {
        this.setState({ existingProfileChangeRequest: false }, () => {
          // closing popups
          this.closeDeleteConfirmationProfileChangeRequestPopup();
          this.closeProfileChangePopup();
        });
        // should never happen
      } else {
        throw new Error("An error occurred during the delete user profile change request process");
      }
    });
  };

  closeProfileChangePopup = () => {
    this.setState(
      {
        showProfileChangePopup: false
      },
      () => {
        // adding small delay here to avoid seeing glitching default popup view on close
        setTimeout(() => {
          // there is no existing request
          if (!this.state.existingProfileChangeRequest) {
            this.setState({
              popupNewFirstNameContent: "",
              isValidPopupNewFirstName: true,
              popupNewLastNameContent: "",
              isValidPopupNewLastName: true,
              popupRightButtonDisabled: true,
              hideDobErrorMessage: true,
              popupCommentsContent: ""
            });
          }
          this.setState({
            showProfileChangeRequestSentConfirmationPopup: false,
            showDeleteConfirmationProfileChangePopup: false
          });
        }, 150);
      }
    );
  };

  handleSendProfileChangeRequest = () => {
    // DOB has been fully provided
    if (
      this.props.completeDatePicked.split("-")[0] !== null &&
      this.props.completeDatePicked.split("-")[1] !== null &&
      this.props.completeDatePicked.split("-")[2] !== null
    ) {
      // triggering DOB validation and displaying DOB error message if needed
      this.setState({
        triggerDateOfBirthValidation: !this.state.triggerDateOfBirthValidation,
        hideDobErrorMessage: false
      });
      // DOB has not been fully provided
    } else {
      this.props.updateDatePickedValidState(true);
    }

    // adding small delay to make sure that the DOB validation is completed
    setTimeout(() => {
      const isValidForm = this.validateForm();

      // valid form
      if (isValidForm) {
        // sending request
        const body = {
          username: this.props.username,
          current_first_name: this.props.firstName,
          current_last_name: this.props.lastName,
          current_birth_date: this.props.dateOfBirth,
          new_first_name: this.state.popupNewFirstNameContent,
          new_last_name: this.state.popupNewLastNameContent,
          new_birth_date: this.props.completeDatePicked,
          comments: this.state.popupCommentsContent
        };
        this.props.sendUserProfileChangeRequest(body).then(response => {
          // successful request
          if (response.ok) {
            // getting updated user profile change request data
            this.props.getUserProfileChangeRequest(this.props.username).then(response => {
              // updating popup states
              this.setState({
                popupNewFirstNameContent: response.body.new_first_name,
                isValidPopupNewFirstName: true,
                popupNewLastNameContent: response.body.new_last_name,
                isValidPopupNewLastName: true,
                popupCommentsContent: response.body.comments,
                popupRightButtonDisabled: false,
                existingProfileChangeRequest: true,
                birthDateFromExistingRequest: response.body.new_birth_date,
                showProfileChangeRequestSentConfirmationPopup: true,
                hideDobErrorMessage: true
              });
            });
            // should never happen
          } else {
            throw new Error(
              "An error occurred during the send user profile change request process"
            );
          }
        });
      }
    }, 100);
  };

  validateForm = () => {
    // validating needed fields
    let isValidFirstName = validateName(this.state.popupNewFirstNameContent);
    // considering "" as valid
    if (!isValidFirstName && this.state.popupNewFirstNameContent === "") {
      isValidFirstName = true;
    }
    let isValidLastName = validateName(this.state.popupNewLastNameContent) || "";
    // considering "" as valid
    if (!isValidLastName && this.state.popupNewLastNameContent === "") {
      isValidLastName = true;
    }

    this.setState(
      {
        isValidPopupNewFirstName: isValidFirstName,
        isValidPopupNewLastName: isValidLastName
      },
      () => {
        // focusing on highest error fiels
        this.focusOnHighestErrorField();
      }
    );

    // form is valid
    if (isValidFirstName && isValidLastName && this.props.completeDateValidState) {
      return true;
    }

    // form contains invalid fields
    return false;
  };

  // update popup first name value
  updatePopupFirstNameValue = event => {
    const popupNewFirstNameContent = event.target.value;
    this.setState(
      {
        popupNewFirstNameContent: popupNewFirstNameContent,
        isValidPopupNewFirstName: true
      },
      () => {
        if (
          this.state.popupNewFirstNameContent !== "" ||
          this.state.popupNewLastNameContent !== ""
        ) {
          this.setState({ popupRightButtonDisabled: false });
        } else {
          this.setState({ popupRightButtonDisabled: true });
        }
      }
    );
  };

  // update popup last name value
  updatePopupLastNameValue = event => {
    const popupNewLastNameContent = event.target.value;
    this.setState(
      {
        popupNewLastNameContent: popupNewLastNameContent,
        isValidPopupNewLastName: true
      },
      () => {
        if (
          this.state.popupNewFirstNameContent !== "" ||
          this.state.popupNewLastNameContent !== ""
        ) {
          this.setState({ popupRightButtonDisabled: false });
        } else {
          this.setState({ popupRightButtonDisabled: true });
        }
      }
    );
  };

  // handling date updates
  handleDOBUpdates = () => {
    let popupNewDOB = null;

    // resetting valid state to true
    this.props.updateDatePickedValidState(true);

    // disabling submit button and hiding DOB error message
    this.setState({
      popupRightButtonDisabled: true,
      hideDobErrorMessage: true
    });

    // updating date picked props
    this.props.updateDatePicked(
      `${
        this.popupNewOfBirthYearFieldRef.current.props.value !== ""
          ? this.popupNewOfBirthYearFieldRef.current.props.value.value
          : null
      }-${
        this.popupNewOfBirthMonthFieldRef.current.props.value !== ""
          ? this.popupNewOfBirthMonthFieldRef.current.props.value.value
          : null
      }-${
        this.popupNewOfBirthDayFieldRef.current.props.value !== ""
          ? this.popupNewOfBirthDayFieldRef.current.props.value.value
          : null
      }`
    );

    // verify if date from date has been fully provided
    if (
      this.popupNewOfBirthDayFieldRef.current.props.value !== "" &&
      this.popupNewOfBirthMonthFieldRef.current.props.value !== "" &&
      this.popupNewOfBirthYearFieldRef.current.props.value !== ""
    ) {
      // create the date format
      popupNewDOB = `${this.popupNewOfBirthYearFieldRef.current.props.value.value}-${this.popupNewOfBirthMonthFieldRef.current.props.value.value}-${this.popupNewOfBirthDayFieldRef.current.props.value.value}`;

      // at least one field (not null) has been provided in the DatePicker or new first and/or last names are defined
      if (
        popupNewDOB !== "null-null-null" ||
        this.state.popupNewFirstNameContent !== "" ||
        this.state.popupNewLastNameContent !== ""
      ) {
        // enabling submit button
        this.setState({ popupRightButtonDisabled: false });
      }

      // verify if the date is valid
      const isValidPopupNewDOB = validateDatePicked(popupNewDOB);
      this.props.updateDatePickedValidState(isValidPopupNewDOB);
    }
  };

  updatePopupCommentsContent = event => {
    const popupCommentsContent = event.target.value;
    this.setState({
      popupCommentsContent: popupCommentsContent
    });
  };

  // analyses field by field and focus on the highest error field
  focusOnHighestErrorField = () => {
    if (!this.state.isValidPopupNewFirstName) {
      document.getElementById("popup-new-first-name-input").focus();
    } else if (!this.state.isValidPopupNewLastName) {
      document.getElementById("popup-new-last-name-input").focus();
    } else if (!this.props.completeDateValidState) {
      this.popupNewOfBirthDayFieldRef.current.focus();
    }
  };

  // async function that triggers the DOB validation
  async triggerDobValidation() {
    this.setState({ triggerDobValidation: !this.state.triggerDobValidation });
  }

  render() {
    const {
      firstNameContent,
      lastNameContent,
      primaryEmailContent,
      psrsAppIdContent,
      secondaryEmailContent,
      isValidSecondaryEmail,
      dateOfBirthYearSelectedValue,
      dateOfBirthMonthSelectedValue,
      dateOfBirthDaySelectedValue,
      triggerDobValidation,
      priContent,
      militaryNbrContent,
      isValidPri,
      isValidMilitaryNbr,
      isValidPSRSApplicantID,
      currentEmployerContent,
      organizationContent,
      groupContent,
      subClassificationOptions,
      subClassificationContent,
      levelOptions,
      levelContent,
      residenceContent,
      educationContent,
      genderContent,
      isLoading,
      showProfileChangePopup,
      popupNewFirstNameContent,
      isValidPopupNewFirstName,
      popupNewLastNameContent,
      isValidPopupNewLastName,
      triggerDateOfBirthValidation,
      hideDobErrorMessage,
      popupCommentsContent,
      popupRightButtonDisabled,
      showProfileChangeRequestSentConfirmationPopup,
      existingProfileChangeRequest,
      birthDateFromExistingRequest,
      showDeleteConfirmationProfileChangePopup
    } = this.state;

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    const groupOptions = addNAOption(this.props.groupOptions.groups);

    const radioButtonStyle = {
      width: this.props.accommodations.fontSize,
      height: this.props.accommodations.fontSize
    };

    const checkboxTransformScale = getCheckboxTransformScale(this.props.accommodations.fontSize);

    return (
      <Col style={isLoading ? styles.loadingContainer : styles.mainContent}>
        {!isLoading && (
          <div>
            {!this.props.profileCompleted && (
              <AlertMessage
                alertType={ALERT_TYPE.light}
                message={LOCALIZE.profile.completeProfilePrompt}
              />
            )}
            <Row>
              <h2>{LOCALIZE.profile.personalInfo.title}</h2>
            </Row>
            <div style={styles.inputsContainer}>
              <Row
                id="personal-info-first-name"
                className="align-items-center"
                style={styles.itemContainer}
                role="presentation"
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={styles.labelContainer}
                >
                  <label id="first-name" htmlFor="first-name-input">
                    {LOCALIZE.profile.personalInfo.nameSection.firstName}
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={styles.inputContainer}
                >
                  <input
                    id="first-name-input"
                    aria-disabled={true}
                    tabIndex={0}
                    className={"valid-field input-disabled-look"}
                    aria-labelledby="name-title first-name"
                    aria-required
                    style={{ ...styles.input, ...accommodationsStyle }}
                    type="text"
                    value={firstNameContent}
                  />
                </Col>
                <Col
                  xl={columnSizes.thirdColumn.xl}
                  lg={columnSizes.thirdColumn.lg}
                  md={columnSizes.thirdColumn.md}
                  sm={columnSizes.thirdColumn.sm}
                  xs={columnSizes.thirdColumn.xs}
                  style={styles.inputContainer}
                >
                  <div style={styles.allUnset}>
                    <StyledTooltip
                      id="change-profile-request-tooltip-first-name"
                      place="top"
                      type={TYPE.light}
                      effect={EFFECT.solid}
                      triggerType={[TRIGGER_TYPE.hover]}
                      tooltipElement={
                        <CustomButton
                          dataTip=""
                          dataFor="change-profile-request-tooltip-first-name"
                          label={
                            <>
                              <span>
                                <FontAwesomeIcon
                                  icon={
                                    !this.state.existingProfileChangeRequest
                                      ? faPencilAlt
                                      : faBinoculars
                                  }
                                />
                              </span>
                            </>
                          }
                          action={this.openProfileChangePopup}
                          customStyle={styles.updateProfileButton}
                          buttonTheme={THEME.PRIMARY}
                          ariaLabel={LOCALIZE.profile.personalInfo.profileChangeRequest.tooltip}
                        />
                      }
                      tooltipContent={
                        <p>
                          {!this.state.existingProfileChangeRequest
                            ? LOCALIZE.profile.personalInfo.profileChangeRequest.tooltip
                            : LOCALIZE.profile.personalInfo.profileChangeRequest
                                .existingRequestTooltip}
                        </p>
                      }
                    />
                  </div>
                </Col>
              </Row>
              <Row
                id="personal-info-last-name"
                className="align-items-center"
                style={styles.itemContainer}
                role="presentation"
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={styles.labelContainer}
                >
                  <label id="last-name" htmlFor="last-name-input">
                    {LOCALIZE.profile.personalInfo.nameSection.lastName}
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={styles.inputContainer}
                >
                  <input
                    id="last-name-input"
                    aria-disabled={true}
                    className={"valid-field input-disabled-look"}
                    aria-labelledby="name-title last-name"
                    aria-required
                    style={{ ...styles.input, ...accommodationsStyle }}
                    type="text"
                    value={lastNameContent}
                  />
                </Col>
                <Col
                  xl={columnSizes.thirdColumn.xl}
                  lg={columnSizes.thirdColumn.lg}
                  md={columnSizes.thirdColumn.md}
                  sm={columnSizes.thirdColumn.sm}
                  xs={columnSizes.thirdColumn.xs}
                  style={styles.inputContainer}
                >
                  <div style={styles.allUnset}>
                    <StyledTooltip
                      id="change-profile-request-tooltip-last-name"
                      place="top"
                      type={TYPE.light}
                      effect={EFFECT.solid}
                      triggerType={[TRIGGER_TYPE.hover]}
                      tooltipElement={
                        <CustomButton
                          dataTip=""
                          dataFor="change-profile-request-tooltip-last-name"
                          label={
                            <>
                              <span>
                                <FontAwesomeIcon
                                  icon={
                                    !this.state.existingProfileChangeRequest
                                      ? faPencilAlt
                                      : faBinoculars
                                  }
                                />
                              </span>
                            </>
                          }
                          action={this.openProfileChangePopup}
                          customStyle={styles.updateProfileButton}
                          buttonTheme={THEME.PRIMARY}
                          ariaLabel={LOCALIZE.profile.personalInfo.profileChangeRequest.tooltip}
                        />
                      }
                      tooltipContent={
                        <p>
                          {!this.state.existingProfileChangeRequest
                            ? LOCALIZE.profile.personalInfo.profileChangeRequest.tooltip
                            : LOCALIZE.profile.personalInfo.profileChangeRequest
                                .existingRequestTooltip}
                        </p>
                      }
                    />
                  </div>
                </Col>
              </Row>
              <Row
                id="personal-info-primary-email-address"
                className="align-items-center"
                style={styles.itemContainer}
                role="presentation"
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={styles.labelContainer}
                >
                  <label id="primary-email" htmlFor="primary-email-input">
                    {LOCALIZE.profile.personalInfo.emailAddressesSection.primary}
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={styles.inputContainer}
                >
                  <input
                    id="primary-email-input"
                    aria-disabled={true}
                    className="valid-field input-disabled-look"
                    aria-labelledby="email-title primary-email"
                    aria-required
                    style={{ ...styles.input, ...accommodationsStyle }}
                    type="text"
                    value={primaryEmailContent}
                  />
                </Col>
              </Row>
              <Row
                id="personal-info-secondary-email-address"
                className="align-items-center"
                style={styles.itemContainer}
                role="presentation"
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={styles.labelContainer}
                >
                  <label id="secondary-email" htmlFor="secondary-email-input">
                    {LOCALIZE.profile.personalInfo.emailAddressesSection.secondary}
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={styles.inputContainer}
                >
                  <input
                    id="secondary-email-input"
                    className={isValidSecondaryEmail ? "valid-field" : "invalid-field"}
                    aria-labelledby="email-title secondary-email secondary-email-tooltip-for-accessibility invalid-secondary-email-msg"
                    aria-required={false}
                    aria-invalid={!isValidSecondaryEmail}
                    style={{ ...styles.input, ...accommodationsStyle }}
                    type="text"
                    value={secondaryEmailContent}
                    onChange={this.updateSecondaryEmailValue}
                  />
                  {!isValidSecondaryEmail && (
                    <div style={styles.errorContainer}>
                      <label
                        id="invalid-secondary-email-msg"
                        style={{ ...styles.errorMessage, ...styles.noMargin }}
                        className="notranslate"
                      >
                        {LOCALIZE.profile.personalInfo.emailAddressesSection.emailError}
                      </label>
                    </div>
                  )}
                </Col>
              </Row>
              <Row
                id="personal-info-date-of-birth"
                className="align-items-center"
                style={styles.dobItemContainer}
                role="presentation"
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={styles.labelContainer}
                  id="dob-title"
                  role="presentation"
                >
                  <span>
                    <label id="date-of-birth">
                      {LOCALIZE.profile.personalInfo.dateOfBirth.title}
                    </label>
                    <StyledTooltip
                      id="date-of-birth-tooltip"
                      place="top"
                      type={TYPE.light}
                      effect={EFFECT.solid}
                      triggerType={[TRIGGER_TYPE.hover]}
                      tooltipElement={
                        <Button
                          data-tip=""
                          data-for="date-of-birth-tooltip"
                          tabIndex="-1"
                          variant="link"
                          style={{
                            ...styles.tooltipIconContainer,
                            ...{
                              fontSize:
                                parseInt(this.props.accommodations.fontSize.split("px")[0]) * 1.25
                            }
                          }}
                        >
                          <FontAwesomeIcon icon={faQuestionCircle} style={styles.tooltipIcon} />
                        </Button>
                      }
                      tooltipContent={
                        <div>
                          <p>{LOCALIZE.profile.personalInfo.dateOfBirth.titleTooltip}</p>
                        </div>
                      }
                    />
                  </span>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={styles.inputContainer}
                  role="presentation"
                >
                  <label
                    id="date-of-birth-tooltip-for-accessibility"
                    style={styles.hiddenText}
                    role="tooltip"
                  >
                    {LOCALIZE.profile.personalInfo.dateOfBirth.titleTooltip}
                  </label>
                  <DatePicker
                    ariaLabelledBy={"date-of-birth-tooltip-for-accessibility"}
                    dateLabelId={"date-of-birth"}
                    initialDateDayValue={dateOfBirthDaySelectedValue}
                    initialDateMonthValue={dateOfBirthMonthSelectedValue}
                    initialDateYearValue={dateOfBirthYearSelectedValue}
                    disabledDropdowns={true}
                    displayValidIcon={false}
                    triggerValidation={triggerDobValidation}
                    dateDayFieldRef={this.dateOfBirthDayFieldRef}
                  />
                </Col>
                <Col
                  xl={columnSizes.thirdColumn.xl}
                  lg={columnSizes.thirdColumn.lg}
                  md={columnSizes.thirdColumn.md}
                  sm={columnSizes.thirdColumn.sm}
                  xs={columnSizes.thirdColumn.xs}
                  style={styles.inputContainer}
                >
                  <div style={styles.allUnset}>
                    <StyledTooltip
                      id="change-profile-request-tooltip-first-name"
                      place="top"
                      type={TYPE.light}
                      effect={EFFECT.solid}
                      triggerType={[TRIGGER_TYPE.hover]}
                      tooltipElement={
                        <CustomButton
                          dataTip=""
                          dataFor="change-profile-request-tooltip-first-name"
                          label={
                            <>
                              <span>
                                <FontAwesomeIcon
                                  icon={
                                    !this.state.existingProfileChangeRequest
                                      ? faPencilAlt
                                      : faBinoculars
                                  }
                                />
                              </span>
                            </>
                          }
                          action={this.openProfileChangePopup}
                          customStyle={styles.updateProfileButton}
                          buttonTheme={THEME.PRIMARY}
                          ariaLabel={LOCALIZE.profile.personalInfo.profileChangeRequest.tooltip}
                        />
                      }
                      tooltipContent={
                        <p>
                          {!this.state.existingProfileChangeRequest
                            ? LOCALIZE.profile.personalInfo.profileChangeRequest.tooltip
                            : LOCALIZE.profile.personalInfo.profileChangeRequest
                                .existingRequestTooltip}
                        </p>
                      }
                    />
                  </div>
                </Col>
              </Row>
              <Row
                id="psrs-applicant-id-title"
                className="align-items-center"
                style={styles.psrsItemContainer}
                role="presentation"
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={styles.labelContainer}
                >
                  <label id="psrs-applicant-id" htmlFor="psrs-applicant-id-input">
                    {LOCALIZE.profile.personalInfo.psrsAppId.title}
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={styles.inputContainer}
                >
                  <input
                    id="psrs-applicant-id-input"
                    className={isValidPSRSApplicantID ? "valid-field" : "invalid-field"}
                    aria-labelledby="psrs-applicant-id invalid-psrs-applicant-id-msg"
                    aria-required={false}
                    aria-invalid={!isValidPSRSApplicantID}
                    style={{ ...styles.input, ...accommodationsStyle }}
                    type="text"
                    value={psrsAppIdContent}
                    onChange={this.updatePsrsAppID}
                  />
                  {!isValidPSRSApplicantID && (
                    <div style={styles.errorContainer}>
                      <label
                        id="invalid-psrs-applicant-id-msg"
                        style={{ ...styles.errorMessage, ...styles.noMargin }}
                        className="notranslate"
                      >
                        {LOCALIZE.profile.personalInfo.psrsAppId.psrsAppIdError}
                      </label>
                    </div>
                  )}
                </Col>
              </Row>
              <Row
                id="personal-info-pri"
                className="align-items-center"
                style={styles.itemContainer}
                role="presentation"
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  id="pri-title"
                  style={styles.labelContainer}
                >
                  <label id="pri" htmlFor="pri-input">
                    {LOCALIZE.profile.personalInfo.pri.title}
                  </label>
                  <StyledTooltip
                    id="pri-tooltip"
                    place="top"
                    type={TYPE.light}
                    effect={EFFECT.solid}
                    triggerType={[TRIGGER_TYPE.hover]}
                    tooltipElement={
                      <Button
                        data-tip=""
                        data-for="pri-tooltip"
                        tabIndex="-1"
                        variant="link"
                        style={{
                          ...styles.tooltipIconContainer,
                          ...styles.tooltipMarginTop,
                          ...{
                            fontSize:
                              parseInt(this.props.accommodations.fontSize.split("px")[0]) * 1.25
                          }
                        }}
                      >
                        <FontAwesomeIcon icon={faQuestionCircle} style={styles.tooltipIcon} />
                      </Button>
                    }
                    tooltipContent={
                      <div>
                        <p>{LOCALIZE.profile.personalInfo.pri.titleTooltip}</p>
                      </div>
                    }
                  />
                  <label id="pri-tooltip-for-accessibility" style={styles.hiddenText}>
                    {LOCALIZE.profile.personalInfo.pri.titleTooltip}
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={styles.inputContainer}
                >
                  <input
                    id="pri-input"
                    className={isValidPri ? "valid-field" : "invalid-field"}
                    aria-labelledby="pri pri-tooltip-for-accessibility invalid-pri-msg"
                    aria-required={false}
                    aria-invalid={!isValidPri}
                    style={{ ...styles.input, ...accommodationsStyle }}
                    type="text"
                    value={priContent}
                    onChange={this.updatePriValue}
                  />
                  {!isValidPri && (
                    <label
                      id="invalid-pri-msg"
                      style={{ ...styles.errorMessage, ...styles.noMargin }}
                    >
                      {LOCALIZE.profile.personalInfo.pri.priError}
                    </label>
                  )}
                </Col>
              </Row>
              <Row id="personal-info-military-nbr" style={styles.itemContainer} role="presentation">
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  id="military-nbr-title"
                  style={styles.labelContainer}
                >
                  <label id="military-number" htmlFor="military-number-input">
                    {LOCALIZE.profile.personalInfo.militaryNbr.title}
                  </label>
                  <StyledTooltip
                    id="military-number-tooltip"
                    place="top"
                    type={TYPE.light}
                    effect={EFFECT.solid}
                    triggerType={[TRIGGER_TYPE.hover]}
                    tooltipElement={
                      <Button
                        data-tip=""
                        data-for="military-number-tooltip"
                        tabIndex="-1"
                        variant="link"
                        style={{
                          ...styles.tooltipIconContainer,
                          ...styles.tooltipMarginTop,
                          ...{
                            fontSize:
                              parseInt(this.props.accommodations.fontSize.split("px")[0]) * 1.25
                          }
                        }}
                      >
                        <FontAwesomeIcon icon={faQuestionCircle} style={styles.tooltipIcon} />
                      </Button>
                    }
                    tooltipContent={
                      <div>
                        <p>{LOCALIZE.profile.personalInfo.militaryNbr.titleTooltip}</p>
                      </div>
                    }
                  />
                  <label id="military-number-tooltip-for-accessibility" style={styles.hiddenText}>
                    {LOCALIZE.profile.personalInfo.militaryNbr.titleTooltip}
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={styles.inputContainer}
                >
                  <input
                    id="military-number-input"
                    className={isValidMilitaryNbr ? "valid-field" : "invalid-field"}
                    aria-labelledby="military-number military-number-tooltip-for-accessibility invalid-military-number-msg"
                    aria-required={false}
                    aria-invalid={!isValidMilitaryNbr}
                    style={{ ...styles.input, ...accommodationsStyle }}
                    type="text"
                    value={militaryNbrContent}
                    onChange={this.updateMilitaryNbrValue}
                  />
                  {!isValidMilitaryNbr && (
                    <label
                      id="invalid-military-number-msg"
                      style={{ ...styles.errorMessage, ...styles.noMargin }}
                    >
                      {LOCALIZE.profile.personalInfo.militaryNbr.militaryNbrError}
                    </label>
                  )}
                </Col>
              </Row>
            </div>
            <hr style={styles.hrStyle}></hr>
            <div>
              <Row>
                <h2>{LOCALIZE.profile.eeInformation.title}</h2>
                <p>{LOCALIZE.profile.eeInformation.description1}</p>
                <p>
                  {LOCALIZE.formatString(
                    LOCALIZE.profile.eeInformation.description2,
                    <button
                      aria-label={LOCALIZE.profile.eeInformation.privacyNoticeLink}
                      tabIndex="0"
                      onClick={this.showPrivacyNoticePopup}
                      style={styles.privacyNoticeLink}
                    >
                      {LOCALIZE.profile.eeInformation.privacyNoticeLink}
                    </button>
                  )}
                </p>
              </Row>
              {!this.state.isLoadingEEInfo && (
                <div style={styles.inputsContainer}>
                  <Row
                    id="personal-info-identify-as-woman"
                    style={styles.radioButtonItemContainer}
                    role="presentation"
                  >
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                      style={styles.labelContainer}
                    >
                      <label id="identify-as-woman" style={styles.boldText}>
                        {LOCALIZE.profile.eeInformation.identifyAsWoman}
                      </label>
                    </Col>
                    {this.generateRadioButtonChoices(
                      "identify-as-woman",
                      radioButtonStyle,
                      this.state.identifyAsWoman,
                      this.state.isValidIdentifyAsWoman
                    )}
                    {!this.state.isValidIdentifyAsWoman && (
                      <Row>
                        <Col
                          xl={columnSizes.radioButtonChoicesColumn.xl}
                          lg={columnSizes.radioButtonChoicesColumn.lg}
                          md={columnSizes.radioButtonChoicesColumn.md}
                          sm={columnSizes.radioButtonChoicesColumn.sm}
                          xs={columnSizes.radioButtonChoicesColumn.xs}
                        >
                          <label
                            id="identify-as-woman-error-message"
                            htmlFor="identify-as-woman-radio-buttons-group"
                            style={styles.errorMessage}
                            className="notranslate"
                          >
                            {LOCALIZE.profile.eeInformation.invalidIdentifyAsWomanErrorMessage}
                          </label>
                        </Col>
                      </Row>
                    )}
                  </Row>
                  <Row
                    id="personal-info-aboriginal"
                    style={
                      this.state.aboriginal === "aboriginal-yes-radio-button-id"
                        ? { ...styles.radioButtonItemContainer, ...styles.noPadding }
                        : styles.radioButtonItemContainer
                    }
                    role="presentation"
                  >
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                      style={styles.labelContainer}
                    >
                      <label id="aboriginal" style={styles.boldText}>
                        {LOCALIZE.profile.eeInformation.aboriginal}
                      </label>
                    </Col>
                    {this.generateRadioButtonChoices(
                      "aboriginal",
                      radioButtonStyle,
                      this.state.aboriginal,
                      this.state.isValidAboriginal
                    )}
                    {!this.state.isValidAboriginal && (
                      <Row>
                        <Col
                          xl={columnSizes.radioButtonChoicesColumn.xl}
                          lg={columnSizes.radioButtonChoicesColumn.lg}
                          md={columnSizes.radioButtonChoicesColumn.md}
                          sm={columnSizes.radioButtonChoicesColumn.sm}
                          xs={columnSizes.radioButtonChoicesColumn.xs}
                        >
                          <label
                            id="aboriginal-error-message"
                            htmlFor="aboriginal-radio-buttons-group"
                            style={styles.errorMessage}
                            className="notranslate"
                          >
                            {LOCALIZE.profile.eeInformation.invalidAboriginalErrorMessage}
                          </label>
                        </Col>
                      </Row>
                    )}
                  </Row>
                  {this.state.aboriginal === "aboriginal-yes-radio-button-id" && (
                    <Row
                      id="personal-info-aboriginal"
                      style={styles.radioButtonSubItemContainer}
                      role="presentation"
                    >
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        style={styles.labelContainer}
                      >
                        <label id="aboriginal-sub-groups" style={styles.boldText}>
                          {LOCALIZE.profile.eeInformation.subAboriginal}
                        </label>
                      </Col>
                      {this.generateCheckBoxesChoices(
                        "aboriginal",
                        this.state.subAboriginalOptions,
                        this.state.subAboriginalCheckboxStates,
                        this.state.isValidSubAboriginal,
                        checkboxTransformScale
                      )}
                      {!this.state.isValidSubAboriginal && (
                        <Row>
                          <Col
                            xl={columnSizes.radioButtonChoicesColumn.xl}
                            lg={columnSizes.radioButtonChoicesColumn.lg}
                            md={columnSizes.radioButtonChoicesColumn.md}
                            sm={columnSizes.radioButtonChoicesColumn.sm}
                            xs={columnSizes.radioButtonChoicesColumn.xs}
                          >
                            <label
                              id="aboriginal-sub-group-error-message"
                              style={styles.errorMessage}
                              className="notranslate"
                            >
                              {LOCALIZE.profile.eeInformation.invalidSubAboriginalErrorMessage}
                            </label>
                          </Col>
                        </Row>
                      )}
                    </Row>
                  )}
                  <Row
                    id="personal-info-visible-minority"
                    style={
                      this.state.visibleMinority === "visible-minority-yes-radio-button-id"
                        ? { ...styles.radioButtonItemContainer, ...styles.noPadding }
                        : styles.radioButtonItemContainer
                    }
                    role="presentation"
                  >
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                      style={styles.labelContainer}
                    >
                      <label id="visible-minority" style={styles.boldText}>
                        {LOCALIZE.profile.eeInformation.visibleMinority}
                      </label>
                    </Col>
                    {this.generateRadioButtonChoices(
                      "visible-minority",
                      radioButtonStyle,
                      this.state.visibleMinority,
                      this.state.isValidVisibleMinority
                    )}
                    {!this.state.isValidVisibleMinority && (
                      <Row>
                        <Col
                          xl={columnSizes.radioButtonChoicesColumn.xl}
                          lg={columnSizes.radioButtonChoicesColumn.lg}
                          md={columnSizes.radioButtonChoicesColumn.md}
                          sm={columnSizes.radioButtonChoicesColumn.sm}
                          xs={columnSizes.radioButtonChoicesColumn.xs}
                        >
                          <label
                            id="visible-minority-error-message"
                            htmlFor="visible-minority-radio-buttons-group"
                            style={styles.errorMessage}
                            className="notranslate"
                          >
                            {LOCALIZE.profile.eeInformation.invalidVisibleMinorityErrorMessage}
                          </label>
                        </Col>
                      </Row>
                    )}
                  </Row>
                  {this.state.visibleMinority === "visible-minority-yes-radio-button-id" && (
                    <Row
                      id="personal-info-visible-minority"
                      style={styles.radioButtonSubItemContainer}
                      role="presentation"
                    >
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        style={styles.labelContainer}
                      >
                        <label id="visible-minority-sub-groups" style={styles.boldText}>
                          {LOCALIZE.profile.eeInformation.subVisibleMinority}
                        </label>
                      </Col>
                      {this.generateCheckBoxesChoices(
                        "visible-minority",
                        this.state.subVisibleMinorityOptions,
                        this.state.subVisibleMinorityCheckboxStates,
                        this.state.isValidSubVisibleMinority,
                        checkboxTransformScale
                      )}
                      {!this.state.isValidSubVisibleMinority && (
                        <Row>
                          <Col
                            xl={columnSizes.radioButtonChoicesColumn.xl}
                            lg={columnSizes.radioButtonChoicesColumn.lg}
                            md={columnSizes.radioButtonChoicesColumn.md}
                            sm={columnSizes.radioButtonChoicesColumn.sm}
                            xs={columnSizes.radioButtonChoicesColumn.xs}
                          >
                            <label
                              id="visible-minority-sub-group-error-message"
                              style={styles.errorMessage}
                              className="notranslate"
                            >
                              {LOCALIZE.profile.eeInformation.invalidSubVisibleMinorityErrorMessage}
                            </label>
                          </Col>
                        </Row>
                      )}
                    </Row>
                  )}
                  {this.state.subVisibleMinorityOtherOptionSelected &&
                    this.state.visibleMinority === "visible-minority-yes-radio-button-id" && (
                      <Row
                        id="personal-info-visible-minority-other-text-area"
                        style={styles.subGroupOtherOptionTextAreaContainer}
                        role="presentation"
                      >
                        <Col
                          xl={columnSizes.firstColumn.xl}
                          lg={columnSizes.firstColumn.lg}
                          md={columnSizes.firstColumn.md}
                          sm={columnSizes.firstColumn.sm}
                          xs={columnSizes.firstColumn.xs}
                          style={styles.labelContainer}
                        >
                          <label
                            id="visible-minority-other-option-specification"
                            htmlFor="visible-minority-other-option-specification-text-area"
                            style={styles.boldText}
                          >
                            {LOCALIZE.profile.eeInformation.subVisibleMinorityOtherTextAreaLabel}
                          </label>
                        </Col>
                        <Col
                          xl={columnSizes.radioButtonChoicesColumn.xl}
                          lg={columnSizes.radioButtonChoicesColumn.lg}
                          md={columnSizes.radioButtonChoicesColumn.md}
                          sm={columnSizes.radioButtonChoicesColumn.sm}
                          xs={columnSizes.radioButtonChoicesColumn.xs}
                        >
                          <textarea
                            id="visible-minority-other-option-specification-text-area"
                            className={"valid-field"}
                            value={this.state.subVisibleMinorityOtherContent}
                            style={styles.textArea}
                            onChange={this.updateSubVisibleMinorityOtherContent}
                            maxLength="200"
                          ></textarea>
                        </Col>
                      </Row>
                    )}
                  <Row
                    id="personal-info-disability"
                    style={{ ...styles.radioButtonItemContainer, ...styles.noPadding }}
                    role="presentation"
                  >
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                      style={styles.labelContainer}
                    >
                      <label id="disability" style={styles.boldText}>
                        {LOCALIZE.profile.eeInformation.disability}
                      </label>
                    </Col>
                    {this.generateRadioButtonChoices(
                      "disability",
                      radioButtonStyle,
                      this.state.disability,
                      this.state.isValidDisability
                    )}
                    {!this.state.isValidDisability && (
                      <Row>
                        <Col
                          xl={columnSizes.radioButtonChoicesColumn.xl}
                          lg={columnSizes.radioButtonChoicesColumn.lg}
                          md={columnSizes.radioButtonChoicesColumn.md}
                          sm={columnSizes.radioButtonChoicesColumn.sm}
                          xs={columnSizes.radioButtonChoicesColumn.xs}
                        >
                          <label
                            id="disability-error-message"
                            htmlFor="disability-radio-buttons-group"
                            style={styles.errorMessage}
                            className="notranslate"
                          >
                            {LOCALIZE.profile.eeInformation.invalidDisabilityErrorMessage}
                          </label>
                        </Col>
                      </Row>
                    )}
                  </Row>
                  {this.state.disability === "disability-yes-radio-button-id" && (
                    <Row
                      id="personal-info-disability"
                      style={styles.radioButtonSubItemContainer}
                      role="presentation"
                    >
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        style={styles.labelContainer}
                      >
                        <label id="disability-sub-groups" style={styles.boldText}>
                          {LOCALIZE.profile.eeInformation.subDisability}
                        </label>
                      </Col>
                      {this.generateCheckBoxesChoices(
                        "disability",
                        this.state.subDisabilityOptions,
                        this.state.subDisabilityCheckboxStates,
                        this.state.isValidSubDisability,
                        checkboxTransformScale
                      )}
                      {!this.state.isValidSubDisability && (
                        <Row>
                          <Col
                            xl={columnSizes.radioButtonChoicesColumn.xl}
                            lg={columnSizes.radioButtonChoicesColumn.lg}
                            md={columnSizes.radioButtonChoicesColumn.md}
                            sm={columnSizes.radioButtonChoicesColumn.sm}
                            xs={columnSizes.radioButtonChoicesColumn.xs}
                          >
                            <label
                              id="disability-sub-group-error-message"
                              style={styles.errorMessage}
                              className="notranslate"
                            >
                              {LOCALIZE.profile.eeInformation.invalidSubDisabilityErrorMessage}
                            </label>
                          </Col>
                        </Row>
                      )}
                    </Row>
                  )}
                  {this.state.subDisabilityOtherOptionSelected &&
                    this.state.disability === "disability-yes-radio-button-id" && (
                      <Row
                        id="personal-info-disability-other-text-area"
                        style={styles.subGroupOtherOptionTextAreaContainer}
                        role="presentation"
                      >
                        <Col
                          xl={columnSizes.firstColumn.xl}
                          lg={columnSizes.firstColumn.lg}
                          md={columnSizes.firstColumn.md}
                          sm={columnSizes.firstColumn.sm}
                          xs={columnSizes.firstColumn.xs}
                          style={styles.labelContainer}
                        >
                          <label
                            id="disability-other-option-specification"
                            htmlFor="disability-other-option-specification-text-area"
                            style={styles.boldText}
                          >
                            {LOCALIZE.profile.eeInformation.subDisabilityOtherTextAreaLabel}
                          </label>
                        </Col>
                        <Col
                          xl={columnSizes.radioButtonChoicesColumn.xl}
                          lg={columnSizes.radioButtonChoicesColumn.lg}
                          md={columnSizes.radioButtonChoicesColumn.md}
                          sm={columnSizes.radioButtonChoicesColumn.sm}
                          xs={columnSizes.radioButtonChoicesColumn.xs}
                        >
                          <textarea
                            id="disability-other-option-specification-text-area"
                            className={"valid-field"}
                            value={this.state.subDisabilityOtherContent}
                            style={styles.textArea}
                            onChange={this.updateSubDisabilityOtherContent}
                            maxLength="200"
                          ></textarea>
                        </Col>
                      </Row>
                    )}
                </div>
              )}
            </div>
            <hr style={styles.hrStyle}></hr>
            <div>
              <Row>
                <h2>{LOCALIZE.profile.additionalInfo.title}</h2>
              </Row>
              <div style={styles.inputsContainer}>
                <Row
                  id="personal-info-current-employer"
                  className="align-items-center"
                  style={styles.itemContainer}
                  role="presentation"
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.labelContainer}
                  >
                    <label id="current-employer">
                      {LOCALIZE.profile.additionalInfo.current_employer.title}
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.inputContainer}
                  >
                    <DropdownSelect
                      idPrefix="personal-info-current-employer-dropdown"
                      customStyle={styles.additionalInfoDropdown}
                      onChange={this.updateCurrentEmployer}
                      defaultValue={currentEmployerContent}
                      options={this.props.employerOptions}
                      ariaLabelledBy="current-employer"
                      orderByValues={true}
                    />
                  </Col>
                </Row>
                <Row
                  id="personal-info-organization"
                  className="align-items-center"
                  style={styles.itemContainer}
                  role="presentation"
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.labelContainer}
                  >
                    <label id="organization">
                      {LOCALIZE.profile.additionalInfo.organization.title}
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.inputContainer}
                  >
                    <DropdownSelect
                      idPrefix={"personal-info-organization-dropdown"}
                      customStyle={styles.additionalInfoDropdown}
                      onChange={this.updateOrganization}
                      defaultValue={organizationContent}
                      options={this.props.organizationOptions}
                      ariaLabelledBy="organization"
                      orderByValues={true}
                    />
                  </Col>
                </Row>
                <Row
                  id="personal-info-group"
                  className="align-items-center"
                  style={styles.itemContainer}
                  role="presentation"
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.labelContainer}
                  >
                    <label id="group">{LOCALIZE.profile.additionalInfo.group.title}</label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.inputContainer}
                  >
                    <DropdownSelect
                      idPrefix={"personal-info-group-dropdown"}
                      customStyle={styles.additionalInfoDropdown}
                      onChange={this.updateGroup}
                      defaultValue={groupContent}
                      options={groupOptions}
                      ariaLabelledBy="group"
                      orderByValues={true}
                    />
                  </Col>
                </Row>
                {this.props.group !== null &&
                  this.props.group !== 0 &&
                  !hasNoValidSubgroups(subClassificationOptions) && (
                    <Row
                      id="personal-info-subclassification"
                      className="align-items-center"
                      style={styles.itemContainer}
                      role="presentation"
                    >
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        style={styles.labelContainer}
                      >
                        <label id="subclassification">
                          {LOCALIZE.profile.additionalInfo.subclassification.title}
                        </label>
                      </Col>
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                        style={styles.inputContainer}
                      >
                        <DropdownSelect
                          idPrefix={"personal-info-subclassification-dropdown"}
                          customStyle={styles.additionalInfoDropdown}
                          onChange={this.updateSubclassification}
                          defaultValue={subClassificationContent}
                          options={subClassificationOptions}
                          ariaLabelledBy="subclassification"
                          orderByValues={true}
                          hasPlaceholder={true}
                          customPlaceholderWording={LOCALIZE.commons.na}
                        />
                      </Col>
                    </Row>
                  )}
                {this.props.group !== null &&
                  this.props.group !== 0 &&
                  this.props.subclassification !== 0 && (
                    <Row
                      id="personal-info-level"
                      className="align-items-center"
                      style={styles.itemContainer}
                      role="presentation"
                    >
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        style={styles.labelContainer}
                      >
                        <label id="level">{LOCALIZE.profile.additionalInfo.level.title}</label>
                      </Col>
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                        style={styles.inputContainer}
                      >
                        <DropdownSelect
                          idPrefix={"personal-info-level-dropdown"}
                          customStyle={styles.additionalInfoDropdown}
                          onChange={this.updateLevel}
                          defaultValue={levelContent}
                          options={levelOptions}
                          ariaLabelledBy="level"
                          orderByValues={true}
                          hasPlaceholder={true}
                          customPlaceholderWording={LOCALIZE.commons.na}
                        />
                      </Col>
                    </Row>
                  )}
                <Row
                  id="personal-info-residence"
                  className="align-items-center"
                  style={styles.itemContainer}
                  role="presentation"
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.labelContainer}
                  >
                    <label id="residence">{LOCALIZE.profile.additionalInfo.residence.title}</label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.inputContainer}
                  >
                    <DropdownSelect
                      idPrefix={"personal-info-residence-dropdown"}
                      customStyle={styles.additionalInfoDropdown}
                      onChange={this.updateResidence}
                      defaultValue={residenceContent}
                      options={this.props.residenceOptions}
                      ariaLabelledBy="residence"
                      orderByValues={true}
                    />
                  </Col>
                </Row>
                <Row
                  id="personal-info-education"
                  className="align-items-center"
                  style={styles.itemContainer}
                  role="presentation"
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.labelContainer}
                  >
                    <label id="education">{LOCALIZE.profile.additionalInfo.education.title}</label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.inputContainer}
                  >
                    <DropdownSelect
                      idPrefix={"personal-info-education-dropdown"}
                      customStyle={styles.additionalInfoDropdown}
                      onChange={this.updateEducation}
                      defaultValue={educationContent}
                      options={this.props.educationOptions}
                      ariaLabelledBy="education"
                      orderByValues={true}
                    />
                  </Col>
                </Row>
                <Row
                  id="personal-info-gender"
                  className="align-items-center"
                  style={styles.itemContainer}
                  role="presentation"
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.labelContainer}
                  >
                    <label id="gender">{LOCALIZE.profile.additionalInfo.gender.title}</label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.inputContainer}
                  >
                    <DropdownSelect
                      idPrefix={"personal-info-gender-dropdown"}
                      customStyle={styles.additionalInfoDropdown}
                      onChange={this.updateGender}
                      defaultValue={genderContent}
                      options={this.props.genderOptions}
                      ariaLabelledBy="gender"
                      orderByValues={true}
                    />
                  </Col>
                </Row>
              </div>
            </div>
            <div id="data-saved-successfully" style={styles.updateButtonContainer}>
              <CustomButton
                label={
                  <>
                    <span>
                      <FontAwesomeIcon icon={faSave} />
                    </span>
                    <span style={styles.saveButtonLabel}>{LOCALIZE.commons.saveButton}</span>
                  </>
                }
                action={this.handleSaveData}
                customStyle={styles.updateButton}
                buttonTheme={THEME.PRIMARY}
                ariaLabel={LOCALIZE.profile.saveButton}
                ariaDescribedBy="data-saved-successfully"
              />
            </div>
          </div>
        )}
        {isLoading && (
          <div style={styles.loading}>
            {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
            <label className="fa fa-spinner fa-spin">
              <FontAwesomeIcon icon={faSpinner} />
            </label>
          </div>
        )}
        <PopupBox
          show={this.state.displayDataUpdatedConfirmationMsgPopup}
          title={LOCALIZE.profile.personalInfo.saveConfirmationPopup.title}
          handleClose={() => {}}
          size={"lg"}
          description={
            <div>
              <p>
                {
                  <SystemMessage
                    messageType={MESSAGE_TYPE.success}
                    title={LOCALIZE.commons.success}
                    message={LOCALIZE.profile.personalInfo.saveConfirmationPopup.description}
                  />
                }
              </p>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.ok}
          rightButtonAction={this.handleCloseSaveConfirmationPopup}
        />
        <PrivacyNoticeStatement
          showPopup={this.state.showPrivacyNoticeDialog}
          handleClose={this.closePrivacyNoticePopup}
        />
        <PopupBox
          show={showProfileChangePopup}
          title={
            !showProfileChangeRequestSentConfirmationPopup
              ? LOCALIZE.profile.personalInfo.profileChangeRequest.requestPopup.title
              : LOCALIZE.profile.personalInfo.profileChangeRequest.requestPopup.successTitle
          }
          handleClose={() => {}}
          size={!showProfileChangeRequestSentConfirmationPopup ? "xl" : "lg"}
          description={
            <div>
              {!showProfileChangeRequestSentConfirmationPopup ? (
                <div>
                  <p>
                    {LOCALIZE.profile.personalInfo.profileChangeRequest.requestPopup.description}
                  </p>
                  <div style={styles.popupInputsContainer}>
                    <Row
                      className="align-items-center justify-content-center"
                      style={styles.popupRow}
                    >
                      <Col
                        xl={columnSizes.popupFirstColumn.xl}
                        lg={columnSizes.popupFirstColumn.lg}
                        md={columnSizes.popupFirstColumn.md}
                        sm={columnSizes.popupFirstColumn.sm}
                        xs={columnSizes.popupFirstColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.popupSecondColumn.xl}
                        lg={columnSizes.popupSecondColumn.lg}
                        md={columnSizes.popupSecondColumn.md}
                        sm={columnSizes.popupSecondColumn.sm}
                        xs={columnSizes.popupSecondColumn.xs}
                      >
                        <label style={styles.columnTitle} id="popup-current-label">
                          {LOCALIZE.profile.personalInfo.profileChangeRequest.requestPopup.current}
                        </label>
                      </Col>
                      <Col
                        xl={columnSizes.popupThirdColumn.xl}
                        lg={columnSizes.popupThirdColumn.lg}
                        md={columnSizes.popupThirdColumn.md}
                        sm={columnSizes.popupThirdColumn.sm}
                        xs={columnSizes.popupThirdColumn.xs}
                      >
                        <label style={styles.columnTitle} id="popup-new-label">
                          {LOCALIZE.profile.personalInfo.profileChangeRequest.requestPopup.new}
                        </label>
                      </Col>
                    </Row>
                    <Row
                      className="align-items-center justify-content-center"
                      style={isValidPopupNewFirstName ? styles.popupRow : {}}
                    >
                      <Col
                        xl={columnSizes.popupFirstColumn.xl}
                        lg={columnSizes.popupFirstColumn.lg}
                        md={columnSizes.popupFirstColumn.md}
                        sm={columnSizes.popupFirstColumn.sm}
                        xs={columnSizes.popupFirstColumn.xs}
                      >
                        <label
                          id="popup-first-name-label"
                          htmlFor="popup-new-first-name-input"
                          style={styles.boldText}
                        >
                          {
                            LOCALIZE.profile.personalInfo.profileChangeRequest.requestPopup
                              .firstNameLabel
                          }
                        </label>
                      </Col>
                      <Col
                        xl={columnSizes.popupSecondColumn.xl}
                        lg={columnSizes.popupSecondColumn.lg}
                        md={columnSizes.popupSecondColumn.md}
                        sm={columnSizes.popupSecondColumn.sm}
                        xs={columnSizes.popupSecondColumn.xs}
                      >
                        <input
                          id="popup-current-first-name-input"
                          tabIndex={0}
                          className={"valid-field input-disabled-look"}
                          aria-labelledby="popup-current-label popup-first-name-label"
                          style={{
                            ...styles.input,
                            ...accommodationsStyle
                          }}
                          type="text"
                          value={this.props.firstName}
                          aria-disabled={true}
                        />
                      </Col>
                      <Col
                        xl={columnSizes.popupThirdColumn.xl}
                        lg={columnSizes.popupThirdColumn.lg}
                        md={columnSizes.popupThirdColumn.md}
                        sm={columnSizes.popupThirdColumn.sm}
                        xs={columnSizes.popupThirdColumn.xs}
                      >
                        <div>
                          <input
                            id="popup-new-first-name-input"
                            className={
                              existingProfileChangeRequest
                                ? "valid-field input-disabled-look"
                                : isValidPopupNewFirstName
                                ? "valid-field"
                                : "invalid-field"
                            }
                            aria-labelledby="popup-new-label popup-first-name-label popup-new-first-name-error"
                            style={{ ...styles.input, ...accommodationsStyle }}
                            type="text"
                            placeholder={LOCALIZE.commons.unchanged}
                            value={popupNewFirstNameContent}
                            onChange={
                              !existingProfileChangeRequest
                                ? this.updatePopupFirstNameValue
                                : () => {}
                            }
                            aria-disabled={existingProfileChangeRequest}
                          />
                        </div>
                      </Col>
                    </Row>
                    {!isValidPopupNewFirstName && (
                      <Row className="align-items-center justify-content-center">
                        <Col
                          xl={columnSizes.popupFirstColumn.xl}
                          lg={columnSizes.popupFirstColumn.lg}
                          md={columnSizes.popupFirstColumn.md}
                          sm={columnSizes.popupFirstColumn.sm}
                          xs={columnSizes.popupFirstColumn.xs}
                        ></Col>
                        <Col
                          xl={columnSizes.popupSecondColumn.xl}
                          lg={columnSizes.popupSecondColumn.lg}
                          md={columnSizes.popupSecondColumn.md}
                          sm={columnSizes.popupSecondColumn.sm}
                          xs={columnSizes.popupSecondColumn.xs}
                        ></Col>
                        <Col
                          xl={columnSizes.popupThirdColumn.xl}
                          lg={columnSizes.popupThirdColumn.lg}
                          md={columnSizes.popupThirdColumn.md}
                          sm={columnSizes.popupThirdColumn.sm}
                          xs={columnSizes.popupThirdColumn.xs}
                        >
                          <label
                            id="popup-new-first-name-error"
                            htmlFor="popup-new-first-name-input"
                            style={styles.errorMessage}
                            className="notranslate"
                          >
                            {
                              LOCALIZE.profile.personalInfo.profileChangeRequest.requestPopup
                                .firstNameError
                            }
                          </label>
                        </Col>
                      </Row>
                    )}
                    <Row
                      className="align-items-center justify-content-center"
                      style={styles.popupRow}
                    >
                      <Col
                        xl={columnSizes.popupFirstColumn.xl}
                        lg={columnSizes.popupFirstColumn.lg}
                        md={columnSizes.popupFirstColumn.md}
                        sm={columnSizes.popupFirstColumn.sm}
                        xs={columnSizes.popupFirstColumn.xs}
                      >
                        <label
                          id="popup-last-name-label"
                          htmlFor="popup-new-last-name-input"
                          style={styles.boldText}
                        >
                          {
                            LOCALIZE.profile.personalInfo.profileChangeRequest.requestPopup
                              .lastNameLabel
                          }
                        </label>
                      </Col>
                      <Col
                        xl={columnSizes.popupSecondColumn.xl}
                        lg={columnSizes.popupSecondColumn.lg}
                        md={columnSizes.popupSecondColumn.md}
                        sm={columnSizes.popupSecondColumn.sm}
                        xs={columnSizes.popupSecondColumn.xs}
                      >
                        <input
                          id="popup-current-last-name-input"
                          tabIndex={0}
                          className={"valid-field input-disabled-look"}
                          aria-labelledby="popup-current-label popup-last-name-label"
                          style={{
                            ...styles.input,
                            ...accommodationsStyle
                          }}
                          type="text"
                          value={this.props.lastName}
                          aria-disabled={true}
                        />
                      </Col>
                      <Col
                        xl={columnSizes.popupThirdColumn.xl}
                        lg={columnSizes.popupThirdColumn.lg}
                        md={columnSizes.popupThirdColumn.md}
                        sm={columnSizes.popupThirdColumn.sm}
                        xs={columnSizes.popupThirdColumn.xs}
                      >
                        <input
                          id="popup-new-last-name-input"
                          className={
                            existingProfileChangeRequest
                              ? "valid-field input-disabled-look"
                              : isValidPopupNewLastName
                              ? "valid-field"
                              : "invalid-field"
                          }
                          aria-labelledby="popup-new-label popup-last-name-label popup-new-last-name-error"
                          style={{ ...styles.input, ...accommodationsStyle }}
                          type="text"
                          placeholder={LOCALIZE.commons.unchanged}
                          value={popupNewLastNameContent}
                          onChange={
                            !existingProfileChangeRequest ? this.updatePopupLastNameValue : () => {}
                          }
                          aria-disabled={existingProfileChangeRequest}
                        />
                      </Col>
                    </Row>
                    {!isValidPopupNewLastName && (
                      <Row className="align-items-center justify-content-center">
                        <Col
                          xl={columnSizes.popupFirstColumn.xl}
                          lg={columnSizes.popupFirstColumn.lg}
                          md={columnSizes.popupFirstColumn.md}
                          sm={columnSizes.popupFirstColumn.sm}
                          xs={columnSizes.popupFirstColumn.xs}
                        ></Col>
                        <Col
                          xl={columnSizes.popupSecondColumn.xl}
                          lg={columnSizes.popupSecondColumn.lg}
                          md={columnSizes.popupSecondColumn.md}
                          sm={columnSizes.popupSecondColumn.sm}
                          xs={columnSizes.popupSecondColumn.xs}
                        ></Col>
                        <Col
                          xl={columnSizes.popupThirdColumn.xl}
                          lg={columnSizes.popupThirdColumn.lg}
                          md={columnSizes.popupThirdColumn.md}
                          sm={columnSizes.popupThirdColumn.sm}
                          xs={columnSizes.popupThirdColumn.xs}
                        >
                          <label
                            id="popup-new-last-name-error"
                            htmlFor="popup-new-last-name-input"
                            style={styles.errorMessage}
                            className="notranslate"
                          >
                            {
                              LOCALIZE.profile.personalInfo.profileChangeRequest.requestPopup
                                .lastNameError
                            }
                          </label>
                        </Col>
                      </Row>
                    )}
                    <Row
                      className="align-items-center justify-content-center"
                      style={styles.popupRow}
                    >
                      <Col
                        xl={columnSizes.popupFirstColumn.xl}
                        lg={columnSizes.popupFirstColumn.lg}
                        md={columnSizes.popupFirstColumn.md}
                        sm={columnSizes.popupFirstColumn.sm}
                        xs={columnSizes.popupFirstColumn.xs}
                      >
                        <label
                          id="popup-dob-label"
                          htmlFor="popup-new-dob-input"
                          style={styles.boldText}
                        >
                          {LOCALIZE.profile.personalInfo.profileChangeRequest.requestPopup.dobLabel}
                        </label>
                      </Col>
                      <Col
                        xl={columnSizes.popupSecondColumn.xl}
                        lg={columnSizes.popupSecondColumn.lg}
                        md={columnSizes.popupSecondColumn.md}
                        sm={columnSizes.popupSecondColumn.sm}
                        xs={columnSizes.popupSecondColumn.xs}
                      >
                        <DatePicker
                          dateLabelId={"popup-current-dob-fields"}
                          dateDayFieldRef={this.popupCurrentOfBirthDayFieldRef}
                          dateMonthFieldRef={this.popupCurrentOfBirthMonthFieldRef}
                          dateYearFieldRef={this.popupCurrentOfBirthYearFieldRef}
                          initialDateDayValue={{
                            value: Number(this.props.dateOfBirth.split("-")[2]),
                            label: `${this.props.dateOfBirth.split("-")[2]}`
                          }}
                          initialDateMonthValue={{
                            value: Number(this.props.dateOfBirth.split("-")[1]),
                            label: `${this.props.dateOfBirth.split("-")[1]}`
                          }}
                          initialDateYearValue={{
                            value: Number(this.props.dateOfBirth.split("-")[0]),
                            label: `${this.props.dateOfBirth.split("-")[0]}`
                          }}
                          disabledDropdowns={true}
                        />
                      </Col>
                      <Col
                        xl={columnSizes.popupThirdColumn.xl}
                        lg={columnSizes.popupThirdColumn.lg}
                        md={columnSizes.popupThirdColumn.md}
                        sm={columnSizes.popupThirdColumn.sm}
                        xs={columnSizes.popupThirdColumn.xs}
                      >
                        {/* TODO: fix accessibility issues (screenreader is reading the current DOB values values) */}
                        <DatePicker
                          dateDayFieldRef={this.popupNewOfBirthDayFieldRef}
                          dateMonthFieldRef={this.popupNewOfBirthMonthFieldRef}
                          dateYearFieldRef={this.popupNewOfBirthYearFieldRef}
                          ariaLabelledBy={`popup-new-label popup-dob-label ${
                            !this.props.completeDateValidState ? "popup-new-dob-error" : ""
                          }`}
                          dateLabelId={"popup-new-dob-fields"}
                          onInputChange={this.handleDOBUpdates}
                          triggerValidation={triggerDateOfBirthValidation}
                          isValidCompleteDatePicked={
                            !(!hideDobErrorMessage && !this.props.completeDateValidState)
                          }
                          // do not show the default invalid date error message here for style purposes (error message needs to be in his own Row)
                          customInvalidDateErrorMessage={""}
                          customPlaceholder={LOCALIZE.commons.unchanged}
                          includeDeselectOption={true}
                          initialDateDayValue={
                            existingProfileChangeRequest && birthDateFromExistingRequest !== null
                              ? {
                                  value: Number(birthDateFromExistingRequest.split("-")[2]),
                                  label: birthDateFromExistingRequest.split("-")[2]
                                }
                              : null
                          }
                          initialDateMonthValue={
                            existingProfileChangeRequest && birthDateFromExistingRequest !== null
                              ? {
                                  value: Number(birthDateFromExistingRequest.split("-")[1]),
                                  label: birthDateFromExistingRequest.split("-")[1]
                                }
                              : null
                          }
                          initialDateYearValue={
                            existingProfileChangeRequest && birthDateFromExistingRequest !== null
                              ? {
                                  value: Number(birthDateFromExistingRequest.split("-")[0]),
                                  label: birthDateFromExistingRequest.split("-")[0]
                                }
                              : null
                          }
                          disabledDropdowns={existingProfileChangeRequest}
                        />
                      </Col>
                    </Row>
                    {!this.props.completeDateValidState &&
                      // at least one field has been provided in the DatePicker
                      this.props.completeDatePicked !== "null-null-null" &&
                      !hideDobErrorMessage && (
                        <Row className="align-items-center justify-content-center">
                          <Col
                            xl={columnSizes.popupFirstColumn.xl}
                            lg={columnSizes.popupFirstColumn.lg}
                            md={columnSizes.popupFirstColumn.md}
                            sm={columnSizes.popupFirstColumn.sm}
                            xs={columnSizes.popupFirstColumn.xs}
                          ></Col>
                          <Col
                            xl={columnSizes.popupSecondColumn.xl}
                            lg={columnSizes.popupSecondColumn.lg}
                            md={columnSizes.popupSecondColumn.md}
                            sm={columnSizes.popupSecondColumn.sm}
                            xs={columnSizes.popupSecondColumn.xs}
                          ></Col>
                          <Col
                            xl={columnSizes.popupThirdColumn.xl}
                            lg={columnSizes.popupThirdColumn.lg}
                            md={columnSizes.popupThirdColumn.md}
                            sm={columnSizes.popupThirdColumn.sm}
                            xs={columnSizes.popupThirdColumn.xs}
                          >
                            <label
                              id="popup-new-dob-error"
                              style={styles.errorMessage}
                              className="notranslate"
                            >
                              {LOCALIZE.datePicker.datePickedError}
                            </label>
                          </Col>
                        </Row>
                      )}
                    <Row
                      className="align-items-center justify-content-center"
                      style={styles.popupRow}
                    >
                      <Col
                        xl={columnSizes.popupFirstColumn.xl}
                        lg={columnSizes.popupFirstColumn.lg}
                        md={columnSizes.popupFirstColumn.md}
                        sm={columnSizes.popupFirstColumn.sm}
                        xs={columnSizes.popupFirstColumn.xs}
                      >
                        <label
                          id="popup-comments-label"
                          htmlFor="popup-comments-input"
                          style={styles.boldText}
                        >
                          {LOCALIZE.profile.personalInfo.profileChangeRequest.requestPopup.comments}
                        </label>
                      </Col>
                      <Col
                        xl={columnSizes.popupSecondColumn.xl + columnSizes.popupThirdColumn.xl}
                        lg={columnSizes.popupSecondColumn.lg + columnSizes.popupThirdColumn.xl}
                        md={columnSizes.popupSecondColumn.md + columnSizes.popupThirdColumn.xl}
                        sm={columnSizes.popupSecondColumn.sm}
                        xs={columnSizes.popupSecondColumn.xs}
                      >
                        <div
                          tabIndex={0}
                          role="navigation"
                          aria-label={`${LOCALIZE.profile.personalInfo.profileChangeRequest.requestPopup.current} ${LOCALIZE.profile.personalInfo.profileChangeRequest.requestPopup.dobLabel}`}
                        >
                          <textarea
                            id="popup-comments-input"
                            className={
                              existingProfileChangeRequest
                                ? "valid-field input-disabled-look"
                                : "valid-field"
                            }
                            aria-required={true}
                            style={{ ...styles.textAreaInput, ...accommodationsStyle }}
                            value={popupCommentsContent}
                            onChange={
                              !existingProfileChangeRequest
                                ? this.updatePopupCommentsContent
                                : () => {}
                            }
                            maxLength="1000"
                            aria-disabled={existingProfileChangeRequest}
                          ></textarea>
                        </div>
                      </Col>
                    </Row>
                  </div>
                </div>
              ) : (
                <div>
                  {
                    <SystemMessage
                      messageType={MESSAGE_TYPE.success}
                      title={LOCALIZE.commons.success}
                      message={
                        LOCALIZE.profile.personalInfo.profileChangeRequest.requestPopup
                          .successDescription
                      }
                    />
                  }
                </div>
              )}
            </div>
          }
          leftButtonType={
            existingProfileChangeRequest && !showProfileChangeRequestSentConfirmationPopup
              ? BUTTON_TYPE.danger
              : !showProfileChangeRequestSentConfirmationPopup
              ? BUTTON_TYPE.secondary
              : BUTTON_TYPE.none
          }
          leftButtonIcon={
            existingProfileChangeRequest && !showProfileChangeRequestSentConfirmationPopup
              ? faTrashAlt
              : !showProfileChangeRequestSentConfirmationPopup
              ? faTimes
              : ""
          }
          leftButtonTitle={
            existingProfileChangeRequest ? LOCALIZE.commons.discard : LOCALIZE.commons.cancel
          }
          leftButtonLabel={
            existingProfileChangeRequest ? LOCALIZE.commons.discard : LOCALIZE.commons.cancel
          }
          leftButtonAction={
            existingProfileChangeRequest
              ? this.openDeleteConfirmationProfileChangeRequestPopup
              : this.closeProfileChangePopup
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonIcon={
            existingProfileChangeRequest
              ? faTimes
              : !showProfileChangeRequestSentConfirmationPopup
              ? faShare
              : ""
          }
          rightButtonTitle={
            existingProfileChangeRequest
              ? LOCALIZE.commons.close
              : !showProfileChangeRequestSentConfirmationPopup
              ? LOCALIZE.commons.submit
              : LOCALIZE.commons.ok
          }
          rightButtonLabel={
            existingProfileChangeRequest
              ? LOCALIZE.commons.close
              : !showProfileChangeRequestSentConfirmationPopup
              ? LOCALIZE.commons.submit
              : LOCALIZE.commons.ok
          }
          rightButtonAction={
            existingProfileChangeRequest
              ? this.closeProfileChangePopup
              : !showProfileChangeRequestSentConfirmationPopup
              ? this.handleSendProfileChangeRequest
              : this.closeProfileChangePopup
          }
          rightButtonState={popupRightButtonDisabled ? BUTTON_STATE.disabled : BUTTON_STATE.enabled}
        />
        <PopupBox
          show={showDeleteConfirmationProfileChangePopup}
          title={LOCALIZE.profile.personalInfo.profileChangeRequest.requestPopup.deleteTitle}
          handleClose={() => {}}
          size={"lg"}
          description={
            <div>
              {
                <SystemMessage
                  messageType={MESSAGE_TYPE.error}
                  title={LOCALIZE.commons.warning}
                  message={
                    LOCALIZE.profile.personalInfo.profileChangeRequest.requestPopup
                      .deleteConfirmationSystemMessage
                  }
                />
              }
              <p>
                {
                  LOCALIZE.profile.personalInfo.profileChangeRequest.requestPopup
                    .deleteConfirmationDescription
                }
              </p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonIcon={faTimes}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonLabel={LOCALIZE.commons.cancel}
          leftButtonAction={this.closeDeleteConfirmationProfileChangeRequestPopup}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonIcon={faTrashAlt}
          rightButtonTitle={LOCALIZE.commons.discard}
          rightButtonLabel={LOCALIZE.commons.discard}
          rightButtonAction={this.handleDeleteProfileChangeRequest}
        />
      </Col>
    );
  }
}

export { PersonalInfo as unconnectedPersonalInfo };

const mapStateToProps = (state, ownProps) => {
  return {
    firstName: state.user.firstName,
    lastName: state.user.lastName,
    username: state.user.username,
    psrsAppID: state.user.psrsAppID,
    primaryEmail: state.user.primaryEmail,
    secondaryEmail: state.user.secondaryEmail,
    dateOfBirth: state.user.dateOfBirth,
    pri: state.user.pri,
    militaryNbr: state.user.militaryNbr,
    lastPasswordChange: state.user.lastPasswordChange,
    isSuperUser: state.user.isSuperUser,
    profileCompleted: state.user.profileCompleted,
    triggerRerender: state.userProfile.triggerRerender,
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    currentEmployer: state.userProfile.extendedProfile.currentEmployer,
    organization: state.userProfile.extendedProfile.organization,
    group: state.userProfile.extendedProfile.group,
    subclassification: state.userProfile.extendedProfile.subclassification,
    level: state.userProfile.extendedProfile.level,
    residence: state.userProfile.extendedProfile.residence,
    education: state.userProfile.extendedProfile.education,
    gender: state.userProfile.extendedProfile.gender,
    isExtendedProfileLoaded: state.extendedProfileOptions.loaded,
    employerOptions: state.extendedProfileOptions.employerOptions[state.localize.language],
    organizationOptions: state.extendedProfileOptions.organizationOptions[state.localize.language],
    groupOptions: state.extendedProfileOptions.groupOptions,
    subClassificationOptions: state.extendedProfileOptions.subClassificationOptions,
    residenceOptions: state.extendedProfileOptions.residenceOptions[state.localize.language],
    educationOptions: state.extendedProfileOptions.educationOptions[state.localize.language],
    genderOptions: state.extendedProfileOptions.genderOptions[state.localize.language],
    completeDateValidState: state.datePicker.completeDateValidState,
    completeDatePicked: state.datePicker.completeDatePicked
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateUserPersonalInfo,
      updateUserExtendedProfile,
      setUserInformation,
      getUserExtendedProfile,
      updateExtendedProfileState,
      updateCurrentEmployer,
      updateOrganization,
      getGroup,
      setGroupOptions,
      getSubClassifications,
      setSubClassificationOptions,
      updateGroup,
      updateSubclassification,
      updateLevel,
      updateResidence,
      updateEducation,
      updateGender,
      getAboriginal,
      getVisibleMinority,
      getDisability,
      updateDatePicked,
      updateDatePickedValidState,
      sendUserProfileChangeRequest,
      getUserProfileChangeRequest,
      deleteUserProfileChangeRequest
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(PersonalInfo);
