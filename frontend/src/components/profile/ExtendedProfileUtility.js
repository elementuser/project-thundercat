import LOCALIZE, { LOCALIZE_OBJ } from "../../text_resources";

const please_select_en = { value: 0, label: LOCALIZE_OBJ.en.commons.pleaseSelect };
const please_select_fr = { value: 0, label: LOCALIZE_OBJ.fr.commons.pleaseSelect };

function compare(a, b) {
  if (a.label < b.label) {
    return -1;
  }
  if (a.label > b.label) {
    return 1;
  }
  return 0;
}

function formatResidence(data) {
  const english = [];
  const french = [];
  data.forEach(element => {
    english.push({ value: element.prov_id, label: element.edesc });
    french.push({ value: element.prov_id, label: element.fdesc });
  });
  english.sort(compare); // sort en/fr alphabetically
  french.sort(compare);
  english.unshift(please_select_en); // pre-pend the "please select" option
  french.unshift(please_select_fr);
  return { en: english, fr: french };
}

function formatEducation(data) {
  const english = [];
  const french = [];
  data.forEach(element => {
    english.push({ value: element.education_id, label: element.edesc });
    french.push({ value: element.education_id, label: element.fdesc });
  });
  english.sort(compare); // sort en/fr alphabetically
  french.sort(compare);
  english.unshift(please_select_en); // pre-pend the "please select" option
  french.unshift(please_select_fr);
  return { en: english, fr: french };
}

function formatGender(data) {
  const english = [];
  const french = [];
  data.forEach(element => {
    english.push({ value: element.gnd_id, label: element.edesc });
    french.push({ value: element.gnd_id, label: element.fdesc });
  });
  english.sort(compare); // sort en/fr alphabetically
  french.sort(compare);
  english.unshift(please_select_en); // pre-pend the "please select" option
  french.unshift(please_select_fr);
  return { en: english, fr: french };
}

function formatMinority(data) {
  const english = [];
  const french = [];
  data.forEach(element => {
    english.push({ value: element.minority_id, label: element.edesc });
    french.push({ value: element.minority_id, label: element.fdesc });
  });
  english.sort(compare); // sort en/fr alphabetically
  french.sort(compare);
  return { en: english, fr: french };
}

function formatEmployer(data) {
  const english = [];
  const french = [];
  data.forEach(element => {
    english.push({ value: element.empsts_id, label: element.edesc });
    french.push({ value: element.empsts_id, label: element.fdesc });
  });
  english.sort(compare); // sort en/fr alphabetically
  french.sort(compare);
  english.unshift(please_select_en); // pre-pend the "please select" option
  french.unshift(please_select_fr);
  return { en: english, fr: french };
}

function formatOrganization(data) {
  const english = [];
  const french = [];
  data.forEach(element => {
    english.push({ value: element.dept_id, label: element.edesc });
    french.push({ value: element.dept_id, label: element.fdesc });
  });
  english.sort(compare); // sort en/fr alphabetically
  french.sort(compare);
  english.unshift(please_select_en); // pre-pend the "please select" option
  french.unshift(please_select_fr);
  return { en: english, fr: french };
}

function formatGroup(data) {
  const groups = [];
  const groupSubClassifications = data.group_subclassifications;

  // looping in data
  for (let i = 0; i < data.group_options.length; i++) {
    const group_code = data.group_options[i].group;
    const group_subclassifications = groupSubClassifications.filter(obj => obj[`${group_code}`])[0][
      `${group_code}`
    ];

    // subclassification codes that match their parent's group_code indicate a direct "group->level" set of mappings
    // (as opposed to "group->subclassification->level" mappings)
    for (let j = 0; j < group_subclassifications.length; j++) {
      if (group_subclassifications[j].subclassification === group_code) {
        group_subclassifications[j].subclassification = `${LOCALIZE.commons.none}`;
      }
    }
    // populating groups
    groups.push({ label: data.group_options[i].group, value: data.group_options[i].classif_id });
  }

  return { groups: groups, groupSubClassifications: groupSubClassifications };
}

function formatSubClassification(data) {
  const subClassifications = [];
  const subClassificationLevels = data.subclassification_levels;

  // looping in data
  for (let i = 0; i < data.subclassification_options.length; i++) {
    // populating groups
    const { parent_group } = data.subclassification_options[i];

    // defined parent_group means the subclassification is actually a group
    // (i.e. group has its own level mapping in addition to subgroups containing their own distinct level mappings)
    if (parent_group !== null) {
      const parent_levels = data.subclassification_levels.filter(obj => obj[`${parent_group}`])[0][
        `${parent_group}`
      ];
      subClassifications.push({
        label: LOCALIZE.commons.none,
        value: data.subclassification_options[i].classif_id
      });
      subClassificationLevels[i] = {
        [LOCALIZE.commons.none]: parent_levels
      };
    } else {
      subClassifications.push({
        label: data.subclassification_options[i].subclassification,
        value: data.subclassification_options[i].classif_id
      });
    }
  }
  return {
    subClassifications: subClassifications,
    subClassificationLevels: subClassificationLevels
  };
}

function refreshGroupSubgroupLevelDropdownLanguage(
  getGroup,
  setGroupOptions,
  getSubClassifications,
  setSubClassificationOptions
) {
  getGroup().then(group => {
    setGroupOptions(formatGroup(group.body));
  });
  getSubClassifications().then(subclassification => {
    setSubClassificationOptions(formatSubClassification(subclassification.body));
  });
}

function populateExtendedProfileDropdowns(
  isExtendedProfileLoaded,
  getEmployer,
  setEmployerOptions,
  getOrganization,
  setOrganizationOptions,
  getGroup,
  setGroupOptions,
  getSubClassifications,
  setSubClassificationOptions,
  getResidence,
  setResidenceOptions,
  getEducation,
  setEducationOptions,
  getGender,
  setGenderOptions
) {
  if (!isExtendedProfileLoaded) {
    getEmployer().then(employer => {
      setEmployerOptions(formatEmployer(employer.body));
    });
    getOrganization().then(organization => {
      setOrganizationOptions(formatOrganization(organization.body));
    });
    getGroup().then(group => {
      setGroupOptions(formatGroup(group.body));
    });
    getSubClassifications().then(subclassification => {
      setSubClassificationOptions(formatSubClassification(subclassification.body));
    });
    getResidence().then(residence => {
      setResidenceOptions(formatResidence(residence.body));
    });
    getEducation().then(education => {
      setEducationOptions(formatEducation(education.body));
    });
    getGender().then(gender => {
      setGenderOptions(formatGender(gender.body));
    });
  }
}
export { refreshGroupSubgroupLevelDropdownLanguage, populateExtendedProfileDropdowns };
