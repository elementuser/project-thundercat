// ==================== ** IMPORTANT ** ====================
// if you update this file, you also need to update the backend file called extended_profile.py (...\project-thundercat\backend\user_management\static\extended_profile.py)

const EXTENDED_PROFILE_EE_INFO = {
  yes: 1,
  no: 2,
  preferNotToSay: 3
};

export const ABORIGINAL_ID = {
  inuit: 1,
  metis: 2,
  naIndianFirstNation: 3
};

export const VISIBLE_MINORITY_ID = {
  black: 1,
  nonWhiteLatinAmerican: 2,
  mixedOrigin: 3,
  chinese: 4,
  japanese: 5,
  korean: 6,
  filipino: 7,
  southAsianEastIndian: 8,
  nonWhiteMiddleEastern: 9,
  southeastAsian: 10,
  other: 22
};

export const DISABILITY_ID = {
  dexterity: 1,
  mobility: 2,
  speech: 3,
  visual: 4,
  hearing: 5,
  other: 6
};

export const PERMISSION = {
  testAdministrator: "is_test_administrator",
  systemAdministrator: "is_etta",
  ppcAdministrator: "is_ppc",
  scorer: "is_scorer",
  testBuilder: "is_tb",
  aae: "is_aae",
  rdOperations: "is_rd_operations",
  testDeveloper: "is_td"
};

export default EXTENDED_PROFILE_EE_INFO;
