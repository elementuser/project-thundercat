import React, { Component } from "react";
import PropTypes from "prop-types";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import "../../css/profile-permissions.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faQuestionCircle } from "@fortawesome/free-regular-svg-icons";
import { Button, Row, Col } from "react-bootstrap";
import { updatePermissionRequestDataState } from "../../modules/UserProfileRedux";
import { updatePri, updateMilitaryNbr } from "../../modules/UserRedux";
import validateName, {
  validateEmail,
  validatePri,
  validateMilitaryNbr
} from "../../helpers/regexValidator";
import getCheckboxTransformScale from "../../helpers/checkboxTransformScale";
import StyledTooltip, { TYPE, EFFECT, TRIGGER_TYPE } from "../authentication/StyledTooltip";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 6,
    xl: 6
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 6,
    xl: 6
  }
};

const styles = {
  container: {
    width: "100%",
    position: "relative"
  },
  formContainer: {
    padding: "24px 6px"
  },
  itemContainer: {
    margin: "12px 0"
  },
  label: {
    paddingRight: 12,
    verticalAlign: "middle"
  },
  labelHeight: {
    minHeight: 36
  },
  rationaleLabel: {
    verticalAlign: "middle",
    display: "table-cell"
  },
  gocEmailTooltipContainer: {
    verticalAlign: "middle"
  },
  gocEmailTooltipLabel: {
    verticalAlign: "middle"
  },
  inputContainer: {
    verticalAlign: "middle"
  },
  input: {
    width: "100%",
    minHeight: 38,
    padding: "3px 6px 3px 6px",
    borderRadius: 4
  },
  rationaleInput: {
    minHeight: 85,
    overflowY: "scroll",
    resize: "none",
    display: "table-cell",
    width: "100%"
  },
  permissionsContainer: {
    paddingTop: 24
  },
  permissionChoicesContainer: {
    width: "96%",
    margin: "0 auto",
    paddingBottom: 12
  },
  permissionContainer: {
    width: "90%",
    margin: "12px auto 0 auto",
    border: "1px solid #00565e",
    borderRadius: 4
  },
  clickableZone: {
    width: "100%",
    display: "table",
    minHeight: 40
  },
  permissionCheckboxAndLabelContainer: {
    width: "90%",
    display: "table-cell",
    verticalAlign: "middle"
  },
  checkbox: {
    margin: "0 16px",
    verticalAlign: "middle"
  },
  permissionLabel: {
    wordBreak: "break-word",
    padding: 6,
    margin: 0,
    verticalAlign: "middle"
  },
  permissionsTooltipContainer: {
    display: "table-cell",
    textAlign: "center",
    verticalAlign: "middle"
  },
  iconContainer: {
    padding: 0,
    verticalAlign: "middle"
  },
  icon: {
    color: "#00565e",
    marginTop: "-1px",
    verticalAlign: "middle"
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  gocEmailgocEmailTooltipIconContainer: {
    padding: "0 6px"
  },
  gocEmailTooltipIcon: {
    color: "#00565e"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    margin: "4px 0 0 4px"
  },
  rationalErrorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginLeft: 4
  },
  customMarginForPermissionsRequestedError: {
    margin: "6px 0 0 20px"
  },
  columnOneTogether: {
    paddingRight: 0
  },
  columnTwoTogether: {
    paddingLeft: 0
  }
};

class RequestPermissionForm extends Component {
  static propTypes = {
    permissionsDefinition: PropTypes.array.isRequired,
    triggerValidation: PropTypes.bool.isRequired,
    // provided by redux
    updatePermissionRequestDataState: PropTypes.func,
    updatePri: PropTypes.func,
    updateMilitaryNbr: PropTypes.func
  };

  state = {
    gocEmailContent: "",
    isValidGocEmail: true,
    priContent: this.props.pri,
    militaryNbrContent: this.props.militaryNbr,
    isValidPri: true,
    isValidMilitaryNbr: true,
    supervisorContent: "",
    isValidSupervisor: true,
    supervisorEmailContent: "",
    isValidSupervisorEmail: true,
    rationaleContent: "",
    isValidRational: true,
    permissionsRequestedArray: [],
    isValidPermissionsRequestedArray: true
  };

  componentDidMount = () => {
    // updating permission request data (pri attribute) based on initial value
    this.props.updatePermissionRequestDataState(
      (this.props.permissionRequestData.pri = this.props.pri.toUpperCase())
    );
    // updating permission request data (militaryNbr attribute) based on initial value
    this.props.updatePermissionRequestDataState(
      (this.props.permissionRequestData.militaryNbr = this.props.militaryNbr.toUpperCase())
    );
  };

  componentDidUpdate = prevProps => {
    // initialize permissionsRequestedArray state on permissionsDefinition props load
    if (prevProps.permissionsDefinition !== this.props.permissionsDefinition) {
      // get initial permissions checkboxes states
      this.setState({ permissionsRequestedArray: this.props.permissionsDefinition });
    }

    // when form validation gets called
    if (prevProps.triggerValidation !== this.props.triggerValidation) {
      // populating requested permissions (IDs)
      const permissionRequestedIds = [];
      // looping in existing permissions
      for (let i = 0; i < this.state.permissionsRequestedArray.length; i++) {
        // permission item is checked
        if (this.state.permissionsRequestedArray[i].checked) {
          // pushing result in permissionRequestedIds array
          permissionRequestedIds.push(this.state.permissionsRequestedArray[i].permission_id);
        }
      }
      // saving the array in redux state
      this.props.updatePermissionRequestDataState(
        (this.props.permissionRequestData.permissions = permissionRequestedIds)
      );
      // validating permission request form
      this.validateForm();
    }
  };

  // update GoC email content
  updateGocEmailContent = event => {
    const gocEmailContent = event.target.value;
    this.setState({
      gocEmailContent: gocEmailContent
    });
    // updating permission request data redux state
    this.props.updatePermissionRequestDataState(
      (this.props.permissionRequestData.gocEmail = gocEmailContent.toLowerCase())
    );
  };

  // update pri content
  updatePriContent = event => {
    const priContent = event.target.value;
    /* only the following can be inserted into this field:
          - 0 to 9 numbers
    */
    const regex = /^([0-9]{0,9})$/;
    if (event.target.value === "" || regex.test(event.target.value)) {
      this.setState({
        priContent: priContent
      });
      // updating pri redux state
      this.props.updatePri(priContent);
    }
  };

  // update military number content
  updateMilitaryNbrContent = event => {
    const militaryNbrContent = event.target.value;
    /* only the following can be inserted into this field:
            - 1 letter followed by 0 to 6 numbers
      */
    const regex = /^(([A-Za-z]{1})([0-9]{0,6}))$/;
    if (event.target.value === "" || regex.test(event.target.value)) {
      this.setState({
        militaryNbrContent: militaryNbrContent
      });
      // updating militaryNbr redux state
      this.props.updateMilitaryNbr(militaryNbrContent);
    }
  };

  // update supervisor content
  updateSupervisorContent = event => {
    const supervisorContent = event.target.value;
    this.setState({
      supervisorContent: supervisorContent
    });
    // updating permission request data redux state
    this.props.updatePermissionRequestDataState(
      (this.props.permissionRequestData.supervisor = supervisorContent)
    );
  };

  // update supervisor email content
  updateSupervisorEmailContent = event => {
    const supervisorEmailContent = event.target.value;
    this.setState({
      supervisorEmailContent: supervisorEmailContent
    });
    // updating permission request data redux state
    this.props.updatePermissionRequestDataState(
      (this.props.permissionRequestData.supervisorEmail = supervisorEmailContent.toLowerCase())
    );
  };

  // update rationale content
  updateRationaleContent = event => {
    const rationaleContent = event.target.value;
    this.setState({
      rationaleContent: rationaleContent
    });
    // updating permission request data redux state
    this.props.updatePermissionRequestDataState(
      (this.props.permissionRequestData.rationale = rationaleContent)
    );
  };

  // update permissions requested checkboxes status
  togglePermissionCheckbox = id => {
    const updatedPermissionsRequestedArray = Array.from(this.state.permissionsRequestedArray);
    updatedPermissionsRequestedArray[id].checked = !updatedPermissionsRequestedArray[id].checked;
    this.setState({ permissionsRequestedArray: updatedPermissionsRequestedArray });
  };

  // handle form validation
  validateForm = () => {
    const {
      gocEmailContent,
      priContent,
      militaryNbrContent,
      supervisorContent,
      supervisorEmailContent,
      rationaleContent,
      permissionsRequestedArray
    } = this.state;

    // goc email validation
    const isValidGocEmail = validateEmail(gocEmailContent);

    // pri validation
    const isValidPri = priContent !== "" ? validatePri(priContent) : false;

    // military number validation
    const isValidMilitaryNbr =
      militaryNbrContent !== "" ? validateMilitaryNbr(militaryNbrContent) : false;

    // supervisor name validation
    const isValidSupervisor = validateName(supervisorContent);

    // supervisor email validation
    const isValidSupervisorEmail = validateEmail(supervisorEmailContent);

    // rationale validation
    // must contain at least one letter to be valid
    const regexExpression = /^(.*[A-Za-z].*)$/;
    const isValidRational = !!regexExpression.test(rationaleContent);

    // permissions choice validation
    let isValidPermissionsRequestedArray = false;
    // looping in permissionsRequestedArray
    for (let i = 0; i < permissionsRequestedArray.length; i++) {
      // if at least one permission is requested, this section is valid
      if (permissionsRequestedArray[i].checked) {
        isValidPermissionsRequestedArray = true;
      }
    }

    // saving all validation results in states
    this.setState(
      {
        isValidGocEmail: isValidGocEmail,
        isValidPri: isValidPri,
        isValidMilitaryNbr: isValidMilitaryNbr,
        isValidSupervisor: isValidSupervisor,
        isValidSupervisorEmail: isValidSupervisorEmail,
        isValidRational: isValidRational,
        isValidPermissionsRequestedArray: isValidPermissionsRequestedArray
      },
      () => {
        this.focusOnHighestErrorField();
      }
    );

    // checking if all validations are met
    if (
      isValidGocEmail &&
      isValidPri &&
      isValidMilitaryNbr &&
      isValidSupervisor &&
      isValidSupervisorEmail &&
      isValidRational &&
      isValidPermissionsRequestedArray
    ) {
      // updating permission request data redux state
      this.props.updatePermissionRequestDataState(
        (this.props.permissionRequestData.isValidForm = true)
      );
    } else {
      // updating permission request data redux state
      this.props.updatePermissionRequestDataState(
        (this.props.permissionRequestData.isValidForm = false)
      );
    }
  };

  // analysing field by field and focusing on the highest error field
  focusOnHighestErrorField = () => {
    if (!this.state.isValidGocEmail) {
      document.getElementById("goc-email").focus();
    } else if (!this.state.isValidPri) {
      document.getElementById("pri").focus();
    } else if (!this.state.isValidMilitaryNbr) {
      document.getElementById("military-nbr").focus();
    } else if (!this.state.isValidSupervisor) {
      document.getElementById("supervisor").focus();
    } else if (!this.state.isValidSupervisorEmail) {
      document.getElementById("supervisor-email").focus();
    } else if (!this.state.isValidRational) {
      document.getElementById("rationale").focus();
    } else if (!this.state.isValidPermissionsRequestedArray) {
      document.getElementById("permissions-error").focus();
    }
  };

  render() {
    const {
      gocEmailContent,
      isValidGocEmail,
      priContent,
      militaryNbrContent,
      isValidPri,
      isValidMilitaryNbr,
      supervisorContent,
      isValidSupervisor,
      supervisorEmailContent,
      isValidSupervisorEmail,
      rationaleContent,
      isValidRational,
      isValidPermissionsRequestedArray
    } = this.state;

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    const checkboxTransformScale = getCheckboxTransformScale(this.props.accommodations.fontSize);

    return (
      <div style={styles.container}>
        <Row>
          <p>{LOCALIZE.profile.permissions.addPermissionPopup.description}</p>
        </Row>
        <div style={styles.formContainer}>
          <Row style={styles.itemContainer}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label id="goc-email-title" htmlFor="goc-email" style={styles.gocEmailTooltipLabel}>
                {LOCALIZE.profile.permissions.addPermissionPopup.gocEmail}
              </label>
              <StyledTooltip
                id="goc-email-title-tooltip"
                place="top"
                type={TYPE.light}
                effect={EFFECT.solid}
                triggerType={[TRIGGER_TYPE.hover]}
                tooltipElement={
                  <Button
                    data-tip=""
                    data-for="goc-email-title-tooltip"
                    tabIndex="-1"
                    variant="link"
                    style={{
                      ...styles.gocEmailgocEmailTooltipIconContainer,
                      ...{
                        fontSize: parseInt(this.props.accommodations.fontSize.split("px")[0]) * 1.25
                      }
                    }}
                  >
                    <FontAwesomeIcon
                      icon={faQuestionCircle}
                      style={styles.gocEmailTooltipIcon}
                    ></FontAwesomeIcon>
                  </Button>
                }
                tooltipContent={
                  <div>
                    <p>{LOCALIZE.profile.permissions.addPermissionPopup.gocEmailTooltip}</p>
                  </div>
                }
              />
              <label id="goc-email-tooltip-for-accessibility" style={styles.hiddenText}>
                , {LOCALIZE.profile.permissions.addPermissionPopup.gocEmailTooltip}
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={styles.inputContainer}
            >
              <input
                id="goc-email"
                className={isValidGocEmail ? "valid-field" : "invalid-field"}
                aria-labelledby="goc-email-title goc-email-error goc-email-tooltip-for-accessibility"
                aria-required={true}
                aria-invalid={!isValidGocEmail}
                style={{ ...styles.input, ...accommodationsStyle }}
                type="text"
                value={gocEmailContent}
                onChange={this.updateGocEmailContent}
              ></input>
              {!isValidGocEmail && (
                <label
                  id="goc-email-error"
                  htmlFor="goc-email"
                  style={styles.errorMessage}
                  className="notranslate"
                >
                  {LOCALIZE.profile.permissions.addPermissionPopup.gocEmailError}
                </label>
              )}
            </Col>
          </Row>
          <Row style={styles.itemContainer}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label id="pri-title" htmlFor="pri" style={styles.label}>
                {LOCALIZE.profile.permissions.addPermissionPopup.pri}
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={styles.inputContainer}
            >
              <input
                id="pri"
                className={isValidPri ? "valid-field" : "invalid-field"}
                aria-labelledby="pri-title pri-error"
                aria-required={true}
                aria-invalid={!isValidPri}
                style={{ ...styles.input, ...accommodationsStyle }}
                type="text"
                value={priContent}
                onChange={this.updatePriContent}
              ></input>
              {!isValidPri && (
                <label
                  id="pri-error"
                  htmlFor="pri"
                  style={styles.errorMessage}
                  className="notranslate"
                >
                  {LOCALIZE.profile.permissions.addPermissionPopup.priError}
                </label>
              )}
            </Col>
          </Row>
          <Row style={styles.itemContainer}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label id="military-number-title" htmlFor="military-nbr" style={styles.label}>
                {LOCALIZE.profile.permissions.addPermissionPopup.militaryNbr}
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={styles.inputContainer}
            >
              <input
                id="military-nbr"
                className={isValidMilitaryNbr ? "valid-field" : "invalid-field"}
                aria-labelledby="military-number-title military-number-error"
                aria-required={true}
                aria-invalid={!isValidMilitaryNbr}
                style={{ ...styles.input, ...accommodationsStyle }}
                type="text"
                value={militaryNbrContent}
                onChange={this.updateMilitaryNbrContent}
              ></input>
              {!isValidMilitaryNbr && (
                <label
                  id="military-number-error"
                  htmlFor="military-nbr"
                  style={styles.errorMessage}
                  className="notranslate"
                >
                  {LOCALIZE.profile.permissions.addPermissionPopup.militaryNbrError}
                </label>
              )}
            </Col>
          </Row>
          <Row style={styles.itemContainer}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label id="supervisor-title" htmlFor="supervisor" style={styles.label}>
                {LOCALIZE.profile.permissions.addPermissionPopup.supervisor}
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={styles.inputContainer}
            >
              <input
                id="supervisor"
                className={isValidSupervisor ? "valid-field" : "invalid-field"}
                aria-labelledby="supervisor-title supervisor-error"
                aria-required={true}
                aria-invalid={!isValidSupervisor}
                style={{ ...styles.input, ...accommodationsStyle }}
                type="text"
                value={supervisorContent}
                onChange={this.updateSupervisorContent}
              ></input>
              {!isValidSupervisor && (
                <label
                  id="supervisor-error"
                  htmlFor="supervisor"
                  style={styles.errorMessage}
                  className="notranslate"
                >
                  {LOCALIZE.profile.permissions.addPermissionPopup.supervisorError}
                </label>
              )}
            </Col>
          </Row>
          <Row style={styles.itemContainer}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label id="supervisor-email-title" htmlFor="supervisor-email" style={styles.label}>
                {LOCALIZE.profile.permissions.addPermissionPopup.supervisorEmail}
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={styles.inputContainer}
            >
              <input
                id="supervisor-email"
                className={isValidSupervisorEmail ? "valid-field" : "invalid-field"}
                aria-labelledby="supervisor-email-title supervisor-email-error"
                aria-required={true}
                aria-invalid={!isValidSupervisorEmail}
                style={{ ...styles.input, ...accommodationsStyle }}
                type="text"
                value={supervisorEmailContent}
                onChange={this.updateSupervisorEmailContent}
              ></input>
              {!isValidSupervisorEmail && (
                <label
                  id="supervisor-email-error"
                  htmlFor="supervisor-email"
                  style={styles.errorMessage}
                  className="notranslate"
                >
                  {LOCALIZE.profile.permissions.addPermissionPopup.supervisorEmailError}
                </label>
              )}
            </Col>
          </Row>
          <Row style={styles.itemContainer}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label
                id="rationale-title"
                htmlFor="rationale"
                style={{ ...styles.label, ...styles.rationaleLabel }}
              >
                {LOCALIZE.profile.permissions.addPermissionPopup.rationale}
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={styles.inputContainer}
            >
              <textarea
                id="rationale"
                className={isValidRational ? "valid-field" : "invalid-field"}
                aria-labelledby="rationale-title rationale-error"
                aria-required={true}
                aria-invalid={!isValidRational}
                style={{ ...styles.input, ...styles.rationaleInput, ...accommodationsStyle }}
                value={rationaleContent}
                onChange={this.updateRationaleContent}
                maxLength="300"
              ></textarea>
              {!isValidRational && (
                <label id="rationale-error" htmlFor="rationale" style={styles.rationalErrorMessage}>
                  {LOCALIZE.profile.permissions.addPermissionPopup.rationaleError}
                </label>
              )}
            </Col>
          </Row>
        </div>
        <div style={styles.permissionsContainer}>
          <Row>
            <label id="permissions-title">
              {LOCALIZE.profile.permissions.addPermissionPopup.permissions}
            </label>
          </Row>
          <div
            className={!isValidPermissionsRequestedArray ? "invalid-field" : ""}
            style={styles.permissionChoicesContainer}
          >
            {this.props.permissionsDefinition.map((permission, id) => {
              return (
                <Row key={id} style={styles.permissionContainer}>
                  <div
                    style={styles.clickableZone}
                    onClick={() => {
                      this.togglePermissionCheckbox(id);
                    }}
                  >
                    <div style={styles.permissionCheckboxAndLabelContainer}>
                      <input
                        id={`permission-checkbox-${id}`}
                        aria-describedby={`permissions-title permission-name-${id} permission-description-${id}`}
                        aria-invalid={!isValidPermissionsRequestedArray}
                        type="checkbox"
                        style={{ ...styles.checkbox, ...{ transform: checkboxTransformScale } }}
                        checked={permission.checked}
                        onChange={() => {}}
                      ></input>
                      <label
                        htmlFor={`permission-checkbox-${id}`}
                        id={`permission-name-${id}`}
                        style={styles.permissionLabel}
                        onClick={() => {
                          this.togglePermissionCheckbox(id);
                        }}
                      >
                        {permission.name}
                      </label>
                    </div>
                    <div style={styles.permissionsTooltipContainer}>
                      <StyledTooltip
                        id={`permission-description-tooltip-${id}`}
                        place="top"
                        type={TYPE.light}
                        effect={EFFECT.solid}
                        triggerType={[TRIGGER_TYPE.hover]}
                        tooltipElement={
                          <div>
                            <Button
                              data-tip=""
                              data-for={`permission-description-tooltip-${id}`}
                              tabIndex="-1"
                              variant="link"
                              style={{
                                ...styles.iconContainer,
                                ...{
                                  fontSize:
                                    parseInt(this.props.accommodations.fontSize.split("px")[0]) *
                                    1.25
                                }
                              }}
                            >
                              <FontAwesomeIcon
                                icon={faQuestionCircle}
                                style={styles.icon}
                              ></FontAwesomeIcon>
                            </Button>
                          </div>
                        }
                        tooltipContent={
                          <div>
                            <p>{permission.description}</p>
                          </div>
                        }
                      />
                    </div>
                    <label id={`permission-description-${id}`} style={styles.hiddenText}>
                      {permission.description}
                    </label>
                  </div>
                </Row>
              );
            })}
          </div>
          {!isValidPermissionsRequestedArray && (
            <label
              id="permissions-error"
              tabIndex={0}
              style={{
                ...styles.errorMessage,
                ...styles.customMarginForPermissionsRequestedError
              }}
            >
              {LOCALIZE.profile.permissions.addPermissionPopup.permissionsError}
            </label>
          )}
        </div>
      </div>
    );
  }
}

export { RequestPermissionForm as unconnectedRequestPermissionForm };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    permissionRequestData: state.userProfile.permissionRequestData,
    pri: state.user.pri,
    militaryNbr: state.user.militaryNbr,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updatePermissionRequestDataState,
      updatePri,
      updateMilitaryNbr
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(RequestPermissionForm);
