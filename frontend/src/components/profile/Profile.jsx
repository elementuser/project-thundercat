import React, { Component } from "react";
import { Row, Col } from "react-bootstrap";
import PropTypes from "prop-types";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { isTokenStillValid } from "../../modules/LoginRedux";
import { bindActionCreators } from "redux";
import ContentContainer from "../commons/ContentContainer";
import { Helmet } from "react-helmet";
import SideNavigation from "../eMIB/SideNavigation";
import PersonalInfo from "./PersonalInfo";
import Password from "./Password";
import Preferences from "./Preferences";
import Permissions from "./Permissions";
import {
  getEmployer,
  getOrganization,
  getGroup,
  getSubClassifications,
  getResidence,
  getEducation,
  getGender,
  setEmployerOptions,
  setOrganizationOptions,
  setGroupOptions,
  setSubClassificationOptions,
  setResidenceOptions,
  setEducationOptions,
  setGenderOptions
} from "../../modules/ExtendedProfileOptionsRedux";
import { switchSideTab, switchTopTab } from "../../modules/NavTabsRedux";
import { populateExtendedProfileDropdowns } from "./ExtendedProfileUtility";
import "../../css/profile-permissions.css";
// import ProfileMerge from "./ProfileMerge";

const styles = {
  tabContainer: {
    zIndex: 1,
    padding: 0
  },
  tabContent: {
    backgroundColor: "transparent"
  },
  nav: {
    marginTop: 10,
    marginLeft: 10,
    maxWidth: 210,
    textAlign: "center"
  },
  body: {
    paddingLeft: 12
  },
  test: {
    textAlign: "center",
    alignItems: "center"
  },
  container: {
    display: "flex"
  },
  appPadding: {
    padding: "15px"
  }
};

class Profile extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  static propTypes = {
    // Props from Redux
    isTokenStillValid: PropTypes.func,
    getEmployer: PropTypes.func,
    getOrganization: PropTypes.func,
    getGroup: PropTypes.func,
    getSubClassifications: PropTypes.func,
    getResidence: PropTypes.func,
    getEducation: PropTypes.func,
    getGender: PropTypes.func,
    setEmployerOptions: PropTypes.func,
    setOrganizationOptions: PropTypes.func,
    setGroupOptions: PropTypes.func,
    setSubClassificationOptions: PropTypes.func,
    setResidenceOptions: PropTypes.func,
    setEducationOptions: PropTypes.func,
    setGenderOptions: PropTypes.func,
    isExtendedProfileLoaded: PropTypes.bool,
    employerOptions: PropTypes.object,
    organizationOptions: PropTypes.object,
    groupOptions: PropTypes.object,
    residenceOptions: PropTypes.object,
    educationOptions: PropTypes.object,
    genderOptions: PropTypes.object,
    minorityOptions: PropTypes.object
  };

  state = {
    isLoaded: false
  };

  componentDidMount = () => {
    this.props.switchTopTab("profile");
    this._isMounted = true;
    // check if the token is still valid before getting user information
    this.props.isTokenStillValid().then(bool => {
      // if the token is still valid
      if (this._isMounted && bool) {
        this.setState({
          isLoaded: true
        });
      }
    });
    populateExtendedProfileDropdowns(
      this.props.isExtendedProfileLoaded,
      this.props.getEmployer,
      this.props.setEmployerOptions,
      this.props.getOrganization,
      this.props.setOrganizationOptions,
      this.props.getGroup,
      this.props.setGroupOptions,
      this.props.getSubClassifications,
      this.props.setSubClassificationOptions,
      this.props.getResidence,
      this.props.setResidenceOptions,
      this.props.getEducation,
      this.props.setEducationOptions,
      this.props.getGender,
      this.props.setGenderOptions
    );
  };

  componentWillUnmount = () => {
    this._isMounted = false;
  };

  // Returns array where each item indicates specifications related to How To Page including the title and the body
  getProfileSections = () => {
    return [
      {
        menuString: LOCALIZE.profile.sideNavItems.personalInfo,
        body: <PersonalInfo />,
        key: "profile-personal-info"
      },
      {
        menuString: LOCALIZE.profile.sideNavItems.password,
        body: <Password username={this.props.username} />,
        key: "profile-password"
      },
      {
        menuString: LOCALIZE.profile.sideNavItems.preferences,
        body: <Preferences />,
        key: "profile-preferences"
      },
      {
        menuString: LOCALIZE.profile.sideNavItems.permissions,
        body: <Permissions username={this.props.username} />,
        key: "profile-permissions"
      }
      // {
      //   menuString: LOCALIZE.profile.sideNavItems.profileMerge,
      //   body: <ProfileMerge />
      // }
    ];
  };

  switchTab = key => {
    if (key !== this.props.currentSideTab.sideTab) {
      this.props.switchSideTab(key);
    }
  };

  render() {
    const specs = this.getProfileSections();

    return (
      <div className="app" style={styles.appPadding}>
        <Helmet>
          <html lang={this.props.currentLanguage} />
          <title className="notranslate">{LOCALIZE.titles.profile}</title>
        </Helmet>
        <ContentContainer>
          <div id="main-content" role="main">
            <div
              id="user-welcome-message-div"
              aria-labelledby="user-welcome-message"
              className="notranslate"
            >
              <h1 id="user-welcome-message" className="green-divider">
                {LOCALIZE.formatString(
                  LOCALIZE.profile.title,
                  this.props.firstName,
                  this.props.lastName
                )}
              </h1>
            </div>
            <section aria-label={LOCALIZE.ariaLabel.sideNavigationSection}>
              <SideNavigation
                specs={specs}
                activeKey={this.props.currentSideTab.sideTab}
                switchTab={() => this.switchTab}
                startIndex={0}
                displayNextPreviousButton={false}
                isMain={true}
                tabContainerStyle={styles.tabContainer}
                tabContentStyle={styles.tabContent}
                navStyle={styles.nav}
                accommodations={this.props.accommodations}
                showUserIcon={true}
              />
            </section>
          </div>
        </ContentContainer>
      </div>
    );
  }
}

export { Profile as unconnectedProfile };

const mapStateToProps = (state, ownProps) => {
  const tab = state.navTabs.currentSideTab.profile || {};
  return {
    currentLanguage: state.localize.language,
    firstName: state.user.firstName,
    lastName: state.user.lastName,
    username: state.user.username,
    primaryEmail: state.user.primaryEmail,
    secondaryEmail: state.user.secondaryEmail,
    dateOfBirth: state.user.dateOfBirth,
    accommodations: state.accommodations,
    isExtendedProfileLoaded: state.extendedProfileOptions.loaded,
    employerOptions: state.extendedProfileOptions.employerOptions,
    organizationOptions: state.extendedProfileOptions.organizationOptions,
    groupOptions: state.extendedProfileOptions.groupOptions,
    subclassificationOptions: state.extendedProfileOptions.subclassificationOptions,
    residenceOptions: state.extendedProfileOptions.residenceOptions,
    educationOptions: state.extendedProfileOptions.educationOptions,
    genderOptions: state.extendedProfileOptions.genderOptions,
    minorityOptions: state.extendedProfileOptions.minorityOptions,
    currentSideTab: tab
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      isTokenStillValid,
      getEmployer,
      getOrganization,
      getGroup,
      getSubClassifications,
      getResidence,
      getEducation,
      getGender,
      setEmployerOptions,
      setOrganizationOptions,
      setGroupOptions,
      setSubClassificationOptions,
      setResidenceOptions,
      setEducationOptions,
      setGenderOptions,
      switchSideTab,
      switchTopTab
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
