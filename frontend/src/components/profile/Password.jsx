import React, { Component } from "react";
import PropTypes from "prop-types";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import "../../css/password.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye, faEyeSlash, faSave } from "@fortawesome/free-solid-svg-icons";
import { faQuestionCircle } from "@fortawesome/free-regular-svg-icons";
import { Button, Row, Col } from "react-bootstrap";
import {
  updateUserPassword,
  validateUserCredentials,
  updateUserLastPasswordChangeTime
} from "../../modules/UserProfileRedux";
import { setLastPasswordChange } from "../../modules/UserRedux";
import { validatePassword } from "../../helpers/regexValidator";
import PasswordMinimumRequirements from "../authentication/PasswordMinimumRequirements";
import PASSWORD_REQUIREMENTS from "../../helpers/passwordRequirementsDefinition";
import { getUserInformation } from "../../modules/LoginRedux";
import SessionStorage, { ACTION, ITEM } from "../../SessionStorage";
import StyledTooltip, { TYPE, EFFECT, TRIGGER_TYPE } from "../authentication/StyledTooltip";
import CustomButton, { THEME } from "../commons/CustomButton";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";
import SystemMessage, { MESSAGE_TYPE } from "../commons/SystemMessage";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 6,
    lg: 6,
    xl: 6
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 6,
    lg: 6,
    xl: 6
  }
};

const styles = {
  mainContainer: {
    width: "100%",
    paddingBottom: 24
  },
  container: {
    padding: "12px 0 0 24px"
  },
  passwordRecoveryMargin: {
    marginTop: 62
  },
  passwordInput: {
    width: "100%",
    minHeight: 38,
    padding: "3px 6px 3px 6px",
    borderRadius: "4px 0 0 4px"
  },
  input: {
    width: 270,
    minHeight: 38,
    padding: "3px 6px",
    borderRadius: 4
  },
  label: {
    marginTop: 6
  },
  contentContainer: {
    margin: "24px 0",
    width: "100%"
  },
  dropdown: {
    width: 550
  },
  passwordVisilibity: {
    color: "#00565e",
    border: "1px solid #00565e",
    borderRadius: "0 4px 4px 0",
    background: "white",
    borderLeft: "none",
    borderWidth: 1,
    borderColor: "#00565e",
    minWidth: "inherite",
    padding: "3px 8px",
    minHeight: 38
  },
  passwordVisilibityInvalid: {
    borderColor: "#923534",
    borderWidth: 3
  },
  validInputIcon: {
    display: "table-cell",
    color: "#00565e",
    border: "1px solid #00565e",
    borderRadius: "0 4px 4px 0",
    background: "white",
    borderLeft: "none"
  },
  tooltipIconContainer: {
    padding: "0 6px"
  },
  tooltipIcon: {
    color: "#00565e"
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  updateButtonContainer: {
    marginTop: 24,
    float: "right"
  },
  updateButton: {
    marginRight: 50,
    minWidth: 100
  },
  saveButtonLabel: {
    margin: 0,
    paddingLeft: 6
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6
  },
  columnOneTogether: {
    paddingRight: 0
  },
  columnTwoTogether: {
    paddingLeft: 0
  }
};

class Password extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  static propTypes = {
    username: PropTypes.string.isRequired,
    // provided by redux
    updateUserPassword: PropTypes.func,
    validateUserCredentials: PropTypes.func,
    getUserInformation: PropTypes.func,
    updateUserLastPasswordChangeTime: PropTypes.func,
    setLastPasswordChange: PropTypes.func
  };

  state = {
    currentPasswordContent: "",
    viewCurrentPassword: false,
    currentPasswordInputType: "password",
    newPasswordContent: "",
    viewNewPassword: false,
    newPasswordInputType: "password",
    newPasswordConfirmationContent: "",
    viewNewPasswordConfirmation: false,
    newPasswordConfirmationInputType: "password",
    invalidCredentialsError: false,
    newPasswordConfirmationMustMatchError: false,
    minimumNewPasswordRequirementsError: false,
    newPasswordTooCommonError: false,
    newPasswordTooSimilarToUsernameError: false,
    newPasswordTooSimilarToFirstNameError: false,
    newPasswordTooSimilarToLastNameError: false,
    newPasswordTooSimilarToEmailError: false,
    displayPasswordUpdatedSuccessfullyMsgPopup: false,
    secretQuestionOptions: [],
    secretQuestionSelectedValue: null,
    secretAnswerContent: "",
    isValidSecretQuestionSection: true,
    displaySecretQuestionUpdatedSuccessfullyMsg: false
  };

  componentDidMount = () => {
    this._isMounted = true;
    this.populateSecretQuestionOptions();
    this.getLastPasswordChangeTime();
  };

  componentWillUnmount = () => {
    this._isMounted = false;
  };

  // TODO (fnormand): create new model with secret questions + create new view to get the questions and populate them using this function here
  // populate secret question options
  populateSecretQuestionOptions = () => {
    const secretQuestionsArray = [];
    // 5 temporary questions
    secretQuestionsArray.push({ value: 0, label: "Question 1..." });
    secretQuestionsArray.push({ value: 1, label: "Question 2..." });
    secretQuestionsArray.push({ value: 2, label: "Question 3..." });
    secretQuestionsArray.push({ value: 3, label: "Question 4..." });
    secretQuestionsArray.push({ value: 4, label: "Question 5..." });
    // save result in state
    this.setState({ secretQuestionOptions: secretQuestionsArray });
  };

  // update current password content
  updateCurrentPasswordContent = event => {
    const currentPasswordContent = event.target.value;
    this.setState({
      currentPasswordContent: currentPasswordContent
    });
  };

  // update new password content
  updateNewPasswordContent = event => {
    const newPasswordContent = event.target.value;
    this.setState({
      newPasswordContent: newPasswordContent
    });
  };

  // update confirm password content
  updatenewPasswordConfirmationContent = event => {
    const newPasswordConfirmationContent = event.target.value;
    this.setState({
      newPasswordConfirmationContent: newPasswordConfirmationContent
    });
  };

  // update secret answer content
  updateSecretAnswerContent = event => {
    const secretAnswerContent = event.target.value;
    this.setState({
      secretAnswerContent: secretAnswerContent
    });
  };

  // get selected secret question
  getSelectedSecretQuestion = selectedOption => {
    this.setState({ secretQuestionSelectedValue: selectedOption });
  };

  // toggle state to display or not the current password
  toggleCurrentPasswordView = () => {
    this.setState({ viewCurrentPassword: !this.state.viewCurrentPassword }, () => {
      if (this.state.viewCurrentPassword) {
        this.setState({ currentPasswordInputType: "text" });
      } else {
        this.setState({ currentPasswordInputType: "password" });
      }
    });
  };

  // toggle state to display or not the new password
  toggleNewPasswordView = () => {
    this.setState({ viewNewPassword: !this.state.viewNewPassword }, () => {
      if (this.state.viewNewPassword) {
        this.setState({ newPasswordInputType: "text" });
      } else {
        this.setState({ newPasswordInputType: "password" });
      }
    });
  };

  // toggle state to display or not the confirm password
  toggleConfirmPasswordView = () => {
    this.setState({ viewNewPasswordConfirmation: !this.state.viewNewPasswordConfirmation }, () => {
      if (this.state.viewNewPasswordConfirmation) {
        this.setState({ newPasswordConfirmationInputType: "text" });
      } else {
        this.setState({ newPasswordConfirmationInputType: "password" });
      }
    });
  };

  // save new user password/security question in the database
  handleSaveNewPasswordData = () => {
    // reseting all error fields before each validation
    this.resetPasswordErrorFields();
    // checks if we need user's credentials validation
    const passwordChangeRequested =
      this.state.currentPasswordContent !== "" ||
      this.state.newPasswordContent !== "" ||
      this.state.newPasswordConfirmationContent !== "";
    // if at least one password/new password field has been filled
    if (passwordChangeRequested) {
      // password/new password validation
      // validating new password with regex password validator
      const isValidNewPassword = validatePassword(this.state.newPasswordContent).length === 0;
      // validating that new password and new password confirmation are the same
      const isValidNewPasswordConfirmation =
        this.state.newPasswordContent === this.state.newPasswordConfirmationContent;

      // checking if both (new pasword and new password confirmation) are valid
      if (isValidNewPassword && isValidNewPasswordConfirmation) {
        // validating user's credentials
        this.props
          .validateUserCredentials({
            username: this.props.username,
            password: this.state.currentPasswordContent
          })
          .then(response => {
            // if user's credentials are valid
            if (response.status === 200) {
              // updated user's password
              this.props
                .updateUserPassword({
                  current_password: this.state.currentPasswordContent,
                  new_password: this.state.newPasswordContent,
                  re_new_password: this.state.newPasswordConfirmationContent
                })
                .then(response => {
                  // password updated successfully
                  if (response.status === 204) {
                    // remove password/new password data from all fields + update displayPasswordUpdatedSuccessfullyMsgPopup state to true
                    this.setState({
                      currentPasswordContent: "",
                      newPasswordContent: "",
                      newPasswordConfirmationContent: "",
                      displayPasswordUpdatedSuccessfullyMsgPopup: true
                    });
                    // updating last password change time
                    this.props
                      .updateUserLastPasswordChangeTime(
                        SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
                        this.props.username
                      )
                      .then(() => {
                        // getting new latest password change time
                        this.getLastPasswordChangeTime();
                      });
                    // there are new password errors
                  } else if (response.new_password.length > 0) {
                    this.handlePasswordErrors(response);
                    this.focusOnHighestPasswordErrorField();
                    // should never happen
                  } else {
                    throw new Error("An error occurred during the password change process");
                  }
                });

              // user's credentials are not valid, so password section is invalid
            } else {
              this.setState({ invalidCredentialsError: true }, () => {
                this.focusOnHighestPasswordErrorField();
              });
            }
          });
        // new password and/or new password confirmation are invalid
      } else {
        // invalid new password based on minimum requirements
        if (!isValidNewPassword) {
          this.setState({ minimumNewPasswordRequirementsError: true }, () => {
            this.focusOnHighestPasswordErrorField();
          });
        }
        // new password does not match new password confirmation
        if (!isValidNewPasswordConfirmation) {
          this.setState(
            {
              newPasswordConfirmationMustMatchError: true
            },
            () => {
              this.focusOnHighestPasswordErrorField();
            }
          );
        }
      }
    }
  };

  // handle password errors, such as password too similar to username, first name, last name and email and password is too common
  handlePasswordErrors = response => {
    // checking for password too common error
    if (response.new_password.indexOf(PASSWORD_REQUIREMENTS.passwordTooCommon) >= 0) {
      this.setState({ newPasswordTooCommonError: true });
    }

    // checking for password too similar to username error
    if (response.new_password.indexOf(PASSWORD_REQUIREMENTS.passwordTooSimilarToUsername) >= 0) {
      this.setState({ newPasswordTooSimilarToUsernameError: true });
    }

    // checking for password too similar to first name error
    if (response.new_password.indexOf(PASSWORD_REQUIREMENTS.passwordTooSimilarToFirstName) >= 0) {
      this.setState({ newPasswordTooSimilarToFirstNameError: true });
    }

    // checking for password too similar to last name error
    if (response.new_password.indexOf(PASSWORD_REQUIREMENTS.passwordTooSimilarToLastName) >= 0) {
      this.setState({ newPasswordTooSimilarToLastNameError: true });
    }

    // checking for password too similar to email error
    if (response.new_password.indexOf(PASSWORD_REQUIREMENTS.passwordTooSimilarToEmail) >= 0) {
      this.setState({ newPasswordTooSimilarToEmailError: true });
    }
  };

  // reseting all password/new password error fields
  resetPasswordErrorFields = () => {
    this.setState({
      invalidCredentialsError: false,
      newPasswordConfirmationMustMatchError: false,
      minimumNewPasswordRequirementsError: false,
      newPasswordTooCommonError: false,
      newPasswordTooSimilarToUsernameError: false,
      newPasswordTooSimilarToFirstNameError: false,
      newPasswordTooSimilarToLastNameError: false,
      newPasswordTooSimilarToEmailError: false
    });
  };

  // getting last_password_change time
  getLastPasswordChangeTime = () => {
    if (this._isMounted) {
      this.props.getUserInformation().then(response => {
        // saving result in state
        this.props.setLastPasswordChange(response.last_password_change);
      });
    }
  };

  // analyses field by field and focus on the highest error field (password section only)
  focusOnHighestPasswordErrorField = () => {
    if (this.state.invalidCredentialsError) {
      document.getElementById("current-password-input").focus();
    } else if (
      this.state.minimumNewPasswordRequirementsError ||
      this.state.newPasswordTooCommonError ||
      this.state.newPasswordTooSimilarToUsernameError ||
      this.state.newPasswordTooSimilarToFirstNameError ||
      this.state.newPasswordTooSimilarToLastNameError ||
      this.state.newPasswordTooSimilarToEmailError
    ) {
      document.getElementById("new-password-input").focus();
    } else if (this.state.newPasswordConfirmationMustMatchError) {
      document.getElementById("confirm-password-input").focus();
    }
  };

  handleCloseSaveConfirmationPopup = () => {
    this.setState({ displayPasswordUpdatedSuccessfullyMsgPopup: false });
  };

  // TODO
  handleSaveSecretQuestionData = () => {
    console.log("HANDLE SAVE SECRET QUESTION DATA");
  };

  render() {
    const {
      currentPasswordContent,
      currentPasswordInputType,
      viewCurrentPassword,
      newPasswordContent,
      newPasswordInputType,
      viewNewPassword,
      newPasswordConfirmationContent,
      newPasswordConfirmationInputType,
      viewNewPasswordConfirmation,
      invalidCredentialsError,
      newPasswordConfirmationMustMatchError,
      minimumNewPasswordRequirementsError,
      newPasswordTooCommonError,
      newPasswordTooSimilarToUsernameError,
      newPasswordTooSimilarToFirstNameError,
      newPasswordTooSimilarToLastNameError,
      newPasswordTooSimilarToEmailError
      // secretAnswerContent,
      // secretQuestionOptions,
      // secretQuestionSelectedValue,
      // displaySecretQuestionUpdatedSuccessfullyMsg
    } = this.state;
    let lastPasswordChangeTime = this.props.lastPasswordChange;
    // catches an odd case where it may be null; set to "" if that happens
    if (lastPasswordChangeTime === undefined || lastPasswordChangeTime === null) {
      lastPasswordChangeTime = "";
    }

    let isNewPasswordFieldValid = true;
    if (
      newPasswordTooCommonError ||
      newPasswordTooSimilarToUsernameError ||
      newPasswordTooSimilarToFirstNameError ||
      newPasswordTooSimilarToLastNameError ||
      newPasswordTooSimilarToEmailError ||
      minimumNewPasswordRequirementsError
    ) {
      isNewPasswordFieldValid = false;
    }

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <div style={styles.mainContainer}>
        <div>
          <Row>
            <h2>{LOCALIZE.profile.password.newPassword.title}</h2>
          </Row>
          <div style={styles.container}>
            <Row>
              <p id="password-last-updated-date-id">
                {LOCALIZE.formatString(
                  LOCALIZE.profile.password.newPassword.updatedDate,
                  lastPasswordChangeTime !== ""
                    ? lastPasswordChangeTime
                    : LOCALIZE.profile.password.newPassword.updatedDateNever
                )}
              </p>
            </Row>
            <Row style={styles.contentContainer} role="presentation">
              <Col
                xl={columnSizes.firstColumn.xl}
                lg={columnSizes.firstColumn.lg}
                md={columnSizes.firstColumn.md}
                sm={columnSizes.firstColumn.sm}
                xs={columnSizes.firstColumn.xs}
              >
                <label id="current-password" htmlFor="current-password-input" style={styles.label}>
                  {LOCALIZE.profile.password.newPassword.currentPassword}
                </label>
              </Col>
              <Col
                xl={columnSizes.secondColumn.xl}
                lg={columnSizes.secondColumn.lg}
                md={columnSizes.secondColumn.md}
                sm={columnSizes.secondColumn.sm}
                xs={columnSizes.secondColumn.xs}
              >
                <Row className="justify-content-start">
                  <Col xs={10} style={styles.columnOneTogether}>
                    <input
                      id="current-password-input"
                      className={
                        invalidCredentialsError
                          ? "invalid-field invalid-password-field"
                          : "valid-field"
                      }
                      aria-labelledby="current-password current-password-error-labels"
                      aria-required={true}
                      aria-invalid={invalidCredentialsError}
                      style={{ ...styles.passwordInput, ...accommodationsStyle }}
                      type={currentPasswordInputType}
                      value={currentPasswordContent}
                      onChange={this.updateCurrentPasswordContent}
                    ></input>
                  </Col>
                  <Col xs={2} style={styles.columnTwoTogether}>
                    <CustomButton
                      label={<FontAwesomeIcon icon={viewCurrentPassword ? faEye : faEyeSlash} />}
                      ariaLabel={LOCALIZE.profile.password.newPassword.showCurrentPassword}
                      ariaPressed={viewCurrentPassword}
                      action={this.toggleCurrentPasswordView}
                      type={"button"}
                      customStyle={
                        invalidCredentialsError
                          ? { ...styles.passwordVisilibity, ...styles.passwordVisilibityInvalid }
                          : styles.passwordVisilibity
                      }
                      buttonTheme={THEME.secondary}
                    />
                  </Col>
                </Row>
                <div id="current-password-error-labels">
                  {invalidCredentialsError && (
                    <label
                      htmlFor="current-password-input"
                      style={styles.errorMessage}
                      className="notranslate"
                    >
                      {LOCALIZE.profile.password.newPassword.invalidPasswordError}
                    </label>
                  )}
                </div>
              </Col>
            </Row>
            <Row style={styles.contentContainer} role="presentation">
              <Col
                xl={columnSizes.firstColumn.xl}
                lg={columnSizes.firstColumn.lg}
                md={columnSizes.firstColumn.md}
                sm={columnSizes.firstColumn.sm}
                xs={columnSizes.firstColumn.xs}
              >
                <label id="new-password" htmlFor="new-password-input" style={styles.label}>
                  {LOCALIZE.profile.password.newPassword.newPassword}
                </label>
                <StyledTooltip
                  id="new-password-tooltip"
                  place="top"
                  type={TYPE.light}
                  effect={EFFECT.solid}
                  triggerType={[TRIGGER_TYPE.hover]}
                  tooltipElement={
                    <Button
                      data-tip=""
                      data-for="new-password-tooltip"
                      tabIndex="-1"
                      variant="link"
                      style={{
                        ...styles.tooltipIconContainer,
                        ...accommodationsStyle,
                        ...{
                          fontSize:
                            parseInt(this.props.accommodations.fontSize.split("px")[0]) * 1.25
                        }
                      }}
                    >
                      <FontAwesomeIcon
                        icon={faQuestionCircle}
                        style={styles.tooltipIcon}
                      ></FontAwesomeIcon>
                    </Button>
                  }
                  tooltipContent={<PasswordMinimumRequirements />}
                />
                {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
                <label id="new-password-tooltip-for-accessibility" style={styles.hiddenText}>
                  <PasswordMinimumRequirements />
                </label>
              </Col>
              <Col
                xl={columnSizes.secondColumn.xl}
                lg={columnSizes.secondColumn.lg}
                md={columnSizes.secondColumn.md}
                sm={columnSizes.secondColumn.sm}
                xs={columnSizes.secondColumn.xs}
              >
                <Row className="justify-content-start">
                  <Col xs={10} style={styles.columnOneTogether}>
                    <input
                      id="new-password-input"
                      className={
                        isNewPasswordFieldValid
                          ? "valid-field"
                          : "invalid-field invalid-password-field"
                      }
                      aria-labelledby={`new-password ${
                        minimumNewPasswordRequirementsError
                          ? ""
                          : "new-password-tooltip-for-accessibility"
                      } new-password-error-labels`}
                      aria-required={true}
                      aria-invalid={!isNewPasswordFieldValid}
                      style={{ ...styles.passwordInput, ...accommodationsStyle }}
                      type={newPasswordInputType}
                      value={newPasswordContent}
                      onChange={this.updateNewPasswordContent}
                    ></input>
                  </Col>
                  <Col xs={2} style={styles.columnTwoTogether}>
                    <CustomButton
                      label={<FontAwesomeIcon icon={viewNewPassword ? faEye : faEyeSlash} />}
                      ariaLabel={LOCALIZE.profile.password.newPassword.showNewPassword}
                      ariaPressed={viewNewPassword}
                      action={this.toggleNewPasswordView}
                      type={"button"}
                      customStyle={
                        isNewPasswordFieldValid
                          ? styles.passwordVisilibity
                          : { ...styles.passwordVisilibity, ...styles.passwordVisilibityInvalid }
                      }
                      buttonTheme={THEME.secondary}
                    />
                  </Col>
                </Row>
                <div id="new-password-error-labels">
                  {newPasswordTooCommonError && (
                    <label
                      htmlFor="new-password-input"
                      style={styles.errorMessage}
                      className="notranslate"
                    >
                      {LOCALIZE.authentication.createAccount.passwordTooCommonError}
                    </label>
                  )}
                  {newPasswordTooSimilarToUsernameError && (
                    <label
                      htmlFor="new-password-input"
                      style={styles.errorMessage}
                      className="notranslate"
                    >
                      {LOCALIZE.authentication.createAccount.passwordTooSimilarToUsernameError}
                    </label>
                  )}
                  {newPasswordTooSimilarToFirstNameError && (
                    <label
                      htmlFor="new-password-input"
                      style={styles.errorMessage}
                      className="notranslate"
                    >
                      {LOCALIZE.authentication.createAccount.passwordTooSimilarToFirstNameError}
                    </label>
                  )}
                  {newPasswordTooSimilarToLastNameError && (
                    <label
                      htmlFor="new-password-input"
                      style={styles.errorMessage}
                      className="notranslate"
                    >
                      {LOCALIZE.authentication.createAccount.passwordTooSimilarToLastNameError}
                    </label>
                  )}
                  {newPasswordTooSimilarToEmailError && (
                    <label
                      htmlFor="new-password-input"
                      style={styles.errorMessage}
                      className="notranslate"
                    >
                      {LOCALIZE.authentication.createAccount.passwordTooSimilarToEmailError}
                    </label>
                  )}
                  {minimumNewPasswordRequirementsError && (
                    <label
                      htmlFor="new-password-input"
                      style={styles.errorMessage}
                      className="notranslate"
                    >
                      <PasswordMinimumRequirements />
                    </label>
                  )}
                </div>
              </Col>
            </Row>
            <Row style={styles.contentContainer} role="presentation">
              <Col
                xl={columnSizes.firstColumn.xl}
                lg={columnSizes.firstColumn.lg}
                md={columnSizes.firstColumn.md}
                sm={columnSizes.firstColumn.sm}
                xs={columnSizes.firstColumn.xs}
              >
                <label id="confirm-password" htmlFor="confirm-password-input" style={styles.label}>
                  {LOCALIZE.profile.password.newPassword.confirmPassword}
                </label>
              </Col>
              <Col
                xl={columnSizes.secondColumn.xl}
                lg={columnSizes.secondColumn.lg}
                md={columnSizes.secondColumn.md}
                sm={columnSizes.secondColumn.sm}
                xs={columnSizes.secondColumn.xs}
              >
                <Row className="justify-content-start">
                  <Col xs={10} style={styles.columnOneTogether}>
                    <input
                      id="confirm-password-input"
                      className={
                        newPasswordConfirmationMustMatchError
                          ? "invalid-field invalid-password-field"
                          : "valid-field"
                      }
                      aria-labelledby="confirm-password new-password-confirmation-error-labels"
                      aria-required={true}
                      aria-invalid={newPasswordConfirmationMustMatchError}
                      style={{ ...styles.passwordInput, ...accommodationsStyle }}
                      type={newPasswordConfirmationInputType}
                      value={newPasswordConfirmationContent}
                      onChange={this.updatenewPasswordConfirmationContent}
                    ></input>
                  </Col>
                  <Col xs={2} style={styles.columnTwoTogether}>
                    <CustomButton
                      label={
                        <FontAwesomeIcon icon={viewNewPasswordConfirmation ? faEye : faEyeSlash} />
                      }
                      ariaLabel={LOCALIZE.profile.password.newPassword.showConfirmPassword}
                      ariaPressed={viewNewPasswordConfirmation}
                      action={this.toggleConfirmPasswordView}
                      type={"button"}
                      customStyle={
                        newPasswordConfirmationMustMatchError
                          ? { ...styles.passwordVisilibity, ...styles.passwordVisilibityInvalid }
                          : styles.passwordVisilibity
                      }
                      buttonTheme={THEME.secondary}
                    />
                  </Col>
                </Row>
                <div id="new-password-confirmation-error-labels">
                  {newPasswordConfirmationMustMatchError && (
                    <label
                      htmlFor="confirm-password-input"
                      style={styles.errorMessage}
                      className="notranslate"
                    >
                      {
                        LOCALIZE.authentication.createAccount.content.inputs
                          .passwordConfirmationError
                      }
                    </label>
                  )}
                </div>
              </Col>
            </Row>
          </div>
          <div style={styles.updateButtonContainer}>
            <CustomButton
              buttonId="my-password-save-button-id"
              label={
                <>
                  <span>
                    <FontAwesomeIcon icon={faSave} />
                  </span>
                  <span style={styles.saveButtonLabel}>{LOCALIZE.commons.saveButton}</span>
                </>
              }
              action={this.handleSaveNewPasswordData}
              customStyle={styles.updateButton}
              buttonTheme={THEME.PRIMARY}
              ariaLabel={LOCALIZE.profile.saveButton}
            />
          </div>
        </div>
        {/* TO BE COMPLETED */}
        {/* <div style={styles.passwordRecoveryMargin}>
          <h2>{LOCALIZE.profile.password.passwordRecovery.title}</h2>
          <div style={styles.container}>
            <div style={styles.contentContainer}>
              <div style={styles.firstColumn}>
                <label id="secret-question" style={styles.label}>
                  <p>{LOCALIZE.profile.password.passwordRecovery.secretQuestion}</p>
                </label>
              </div>
              <div style={{ ...styles.secondColumn, ...styles.dropdown }}>
                <Select
                  id="secret-question-select"
                  className="valid-field"
                  name="secret-question-name"
                  aria-labelledby={"secret-question"}
                  aria-required={true}
                  options={secretQuestionOptions}
                  onChange={this.getSelectedSecretQuestion}
                  clearable={false}
                  value={secretQuestionSelectedValue}
                ></Select>
              </div>
            </div>
            <div style={styles.contentContainer}>
              <div style={styles.firstColumn}>
                <label id="secret-answer" htmlFor="secret-answer-input" style={styles.label}>
                  {LOCALIZE.profile.password.passwordRecovery.secretAnswer}
                </label>
              </div>
              <div style={styles.secondColumn}>
                <input
                  id="secret-answer-input"
                  className="valid-field"
                  aria-labelledby="secret-answer"
                  aria-required={true}
                  style={styles.input}
                  type="text"
                  value={secretAnswerContent}
                  onChange={this.updateSecretAnswerContent}
                ></input>
              </div>
            </div>
          </div>
        </div>
        <div id="secret-question-saved-successfully" style={styles.updateButtonContainer}>
          {displaySecretQuestionUpdatedSuccessfullyMsg && (
            <p style={styles.updateButtonConfirmationMsg}>
              {LOCALIZE.profile.password.passwordRecovery.secretQuestionUpdatedConfirmation}
            </p>
          )}
          <button
            className="btn btn-primary"
            aria-describedby="secret-question-saved-successfully"
            style={styles.updateButton}
            aria-label={LOCALIZE.profile.saveButton}
            onClick={this.handleSaveSecretQuestionData}
          >
            <FontAwesomeIcon icon={faSave} style={styles.buttonIcon} />
          </button>
        </div> */}
        <PopupBox
          show={this.state.displayPasswordUpdatedSuccessfullyMsgPopup}
          title={LOCALIZE.profile.password.newPassword.popup.title}
          handleClose={() => {}}
          description={
            <div>
              <p>
                {
                  <SystemMessage
                    messageType={MESSAGE_TYPE.success}
                    title={LOCALIZE.commons.success}
                    message={LOCALIZE.profile.password.newPassword.popup.description}
                  />
                }
              </p>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.ok}
          rightButtonAction={this.handleCloseSaveConfirmationPopup}
        />
      </div>
    );
  }
}

export { Password as unconnectedPassword };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    lastPasswordChange: state.user.lastPasswordChange,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateUserPassword,
      validateUserCredentials,
      getUserInformation,
      updateUserLastPasswordChangeTime,
      setLastPasswordChange
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Password);
