import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Col, Navbar, Row } from "react-bootstrap";
import Timer from "../commons/Timer";
import CustomButton, { THEME } from "../commons/CustomButton";
import LOCALIZE from "../../text_resources";
import { setTestFooterHeight, getServerTime } from "../../modules/TestSectionRedux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPauseCircle } from "@fortawesome/free-solid-svg-icons";
import { triggerBreakBankAction } from "../../modules/BreakBankRedux";
import { BreakBankActions } from "./Constants";
import { pauseTest } from "../../modules/TestStatusRedux";

const columnSizes = {
  firstColumn: {
    xs: 4,
    sm: 4,
    md: 4,
    lg: 4,
    xl: 4
  },
  firstColumnWithoutPauseButton: {
    xs: 6,
    sm: 6,
    md: 6,
    lg: 6,
    xl: 6
  },
  secondColumn: {
    xs: 4,
    sm: 4,
    md: 4,
    lg: 4,
    xl: 4
  },
  thirdColumn: {
    xs: 4,
    sm: 4,
    md: 4,
    lg: 4,
    xl: 4
  },
  thirdColumnWithoutPauseButton: {
    xs: 6,
    sm: 6,
    md: 6,
    lg: 6,
    xl: 6
  }
};

const styles = {
  footer: {
    borderTop: "1px solid #96a8b2",
    backgroundColor: "#D5DEE0"
  },
  content: {
    width: 1400,
    margin: "0 auto",
    padding: "0px 20px"
  },
  buttonLabel: {
    margin: 0,
    paddingLeft: 6
  },
  footerIndex: {
    zIndex: 1
  },
  pauseCol: {
    textAlign: "center"
  },
  submitOrNextCol: {
    textAlign: "right"
  },
  boldText: {
    fontWeight: "bold"
  }
};

class TestFooter extends Component {
  constructor(props, context) {
    super(props, context);
    this.PropTypes = {
      submitTest: PropTypes.func,
      timeout: PropTypes.func,
      startTime: PropTypes.string,
      displayPauseButton: PropTypes.bool,
      breakBankData: PropTypes.object
    };
    this.testFooterHeightRef = React.createRef();

    TestFooter.defaultProps = {
      isSampleTest: false,
      displayPauseButton: false,
      breakBankData: {}
    };
  }

  state = {
    breakBankTimeRemaining: undefined
  };

  componentDidMount = () => {
    // updating footer height redux state
    if (this.testFooterHeightRef.current) {
      this.props.setTestFooterHeight(this.testFooterHeightRef.current.clientHeight);
    }
  };

  componentDidUpdate = prevProps => {
    // updating footer height redux state
    if (this.testFooterHeightRef.current) {
      this.props.setTestFooterHeight(this.testFooterHeightRef.current.clientHeight);
    }

    // update on font size change
    if (prevProps.accommodations !== this.props.accommodations) {
      this.props.setTestFooterHeight(this.testFooterHeightRef.current.clientHeight);
    }
  };

  handlePause = () => {
    const data = {
      accommodation_request_id: this.props.accommodationRequestId,
      action_type: BreakBankActions.PAUSE,
      test_section_id: this.props.testSection.id
    };
    this.props.triggerBreakBankAction(data).then(response => {
      // successful request
      if (response.ok) {
        // getting server time
        this.props.getServerTime().then(response => {
          // pausing test
          // divided by 60 to get the time in minutes (provided in seconds)
          this.props.pauseTest(this.props.breakBankData.time_remaining / 60, response);
        });
        // should never happen
      } else {
        throw new Error("An error occurred during the pause request process");
      }
    });
  };

  render() {
    return (
      <div role="contentinfo" className="fixed-bottom" style={styles.footerIndex}>
        <Navbar ref={this.testFooterHeightRef} fixed="bottom" style={styles.footer}>
          <div style={styles.content} id="unit-test-timer">
            <Row role="presentation">
              {this.props.timeLimit && (
                <Col
                  xl={
                    this.props.displayPauseButton
                      ? columnSizes.firstColumn.xl
                      : columnSizes.firstColumnWithoutPauseButton.xl
                  }
                  lg={
                    this.props.displayPauseButton
                      ? columnSizes.firstColumn.lg
                      : columnSizes.firstColumnWithoutPauseButton.lg
                  }
                  md={
                    this.props.displayPauseButton
                      ? columnSizes.firstColumn.md
                      : columnSizes.firstColumnWithoutPauseButton.md
                  }
                  sm={
                    this.props.displayPauseButton
                      ? columnSizes.firstColumn.sm
                      : columnSizes.firstColumnWithoutPauseButton.sm
                  }
                  xs={
                    this.props.displayPauseButton
                      ? columnSizes.firstColumn.xs
                      : columnSizes.firstColumnWithoutPauseButton.xs
                  }
                >
                  <Timer
                    timeout={this.props.timeout}
                    startTime={this.props.startTime}
                    timeLimit={this.props.timeLimit}
                    isSampleTest={this.props.isSampleTest}
                  />
                </Col>
              )}
              {this.props.displayPauseButton && (
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={styles.pauseCol}
                >
                  <CustomButton
                    buttonId="unit-test-pause-btn"
                    label={
                      <>
                        <FontAwesomeIcon icon={faPauseCircle} />
                        <span style={styles.buttonLabel} className="notranslate">
                          {LOCALIZE.emibTest.testFooter.pauseButton}
                        </span>
                      </>
                    }
                    action={this.handlePause}
                    buttonTheme={THEME.SECONDARY}
                    ariaLabel={LOCALIZE.emibTest.testFooter.pauseButton}
                    disabled={this.props.breakBankData.time_remaining < 1}
                  ></CustomButton>
                </Col>
              )}
              <Col
                id="unit-test-submit-btn-container"
                xl={
                  !this.props.timeLimit && !this.props.displayPauseButton
                    ? 12
                    : this.props.displayPauseButton
                    ? columnSizes.thirdColumn.xl
                    : columnSizes.thirdColumnWithoutPauseButton.xl
                }
                lg={
                  !this.props.timeLimit && !this.props.displayPauseButton
                    ? 12
                    : this.props.displayPauseButton
                    ? columnSizes.thirdColumn.lg
                    : columnSizes.thirdColumnWithoutPauseButton.lg
                }
                md={
                  !this.props.timeLimit && !this.props.displayPauseButton
                    ? 12
                    : this.props.displayPauseButton
                    ? columnSizes.thirdColumn.md
                    : columnSizes.thirdColumnWithoutPauseButton.md
                }
                sm={
                  !this.props.timeLimit && !this.props.displayPauseButton
                    ? 12
                    : this.props.displayPauseButton
                    ? columnSizes.thirdColumn.sm
                    : columnSizes.thirdColumnWithoutPauseButton.sm
                }
                xs={
                  !this.props.timeLimit && !this.props.displayPauseButton
                    ? 12
                    : this.props.displayPauseButton
                    ? columnSizes.thirdColumn.xs
                    : columnSizes.thirdColumnWithoutPauseButton.xs
                }
                style={styles.submitOrNextCol}
              >
                <CustomButton
                  buttonId="unit-test-submit-btn"
                  label={<span className="notranslate">{this.props.buttonText}</span>}
                  action={this.props.submitTest}
                  buttonTheme={THEME.SUCCESS}
                  ariaLabel={this.props.buttonText}
                ></CustomButton>
              </Col>
            </Row>
          </div>
        </Navbar>
      </div>
    );
  }
}

export { TestFooter as unconnectedTestFooter };

const mapStateToProps = (state, ownProps) => {
  return {
    accommodations: state.accommodations,
    accommodationRequestId: state.assignedTest.accommodationRequestId,
    testSection: state.testSection.testSection
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setTestFooterHeight,
      triggerBreakBankAction,
      pauseTest,
      getServerTime
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(TestFooter);
