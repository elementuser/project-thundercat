/* eslint-disable react/no-children-prop */
import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import LOCALIZE from "../../text_resources";
import TopTabs from "../eMIB/TopTabs";
import SingleComponentFactory from "./SingleComponentFactory";
import TestFooter from "./TestFooter";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import { NextSectionButtonType, TestSectionComponentType, TestSectionType } from "./Constants";
import ReactMarkdown from "react-markdown";
import rehypeRaw from "rehype-raw";
import CustomButton, { THEME } from "../commons/CustomButton";
import getCheckboxTransformScale from "../../helpers/checkboxTransformScale";
import { getFormattedTestTime } from "../../helpers/timeConversion";
import {
  updateTestSection,
  setContentLoaded,
  setStartTime,
  setContentUnLoaded
} from "../../modules/TestSectionRedux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import getBreakBankRemainingTime from "../../modules/BreakBankRedux";

const styles = {
  startTestBtn: {
    textAlign: "center",
    margin: "32px 0",
    width: "auto",
    minWidth: 300
  },
  checkboxZone: {
    paddingBottom: 12,
    display: "table"
  },
  checkboxContainer: {
    display: "table-cell",
    verticalAlign: "middle"
  },
  checkboxLabelContainer: {
    display: "table-cell"
  },
  checkbox: {
    margin: "0 16px",
    verticalAlign: "middle",
    display: "block"
  },
  checkboxLabel: {
    margin: 0
  },
  paddingTop: {
    // if you update the padding top, don't forget to update the value in ".../helpers/inTestHeightCalculations.js" (getInTestHeightCalculations function)
    paddingTop: 24
  },
  loadingContainer: {
    textAlign: "center"
  },
  wishToProceedContainer: {
    paddingTop: 24
  },
  boldText: {
    fontWeight: "bold"
  }
};

class TestSectionComponentFactory extends Component {
  constructor(props, context) {
    super(props, context);

    this.PropTypes = {
      testSection: PropTypes.object,
      calledInTestBuilder: PropTypes.bool,
      error: PropTypes.bool,
      errorMessage: PropTypes.string
    };

    TestSectionComponentFactory.defaultProps = {
      isContentLoaded: false,
      calledInTestBuilder: false
    };
  }

  state = {
    showSubmitTestPopup: false,
    showStartTestPopup: false,
    proceedCheckbox: false,
    popupVisible: false,
    isNextButtonDisabled: false,
    error: false,
    errorMessage: "",
    isLoading: true,
    displayPauseButtonInFooter: false,
    breakBankData: {}
  };

  componentDidMount = () => {
    // not called in test builder
    if (!this.props.calledInTestBuilder) {
      // set loading to false
      this.setState({ isLoading: false });
    }

    // test section is loaded and the test section is defined
    if (this.props.testSectionLoaded && typeof this.props.testSection !== "undefined") {
      // handling break bank logic
      this.handleBreakBankLogic();
      // test section props is undefined (that means that the language has been changed)
    } else {
      // update testSectionLoaded to false
      this.props.setContentUnLoaded();
    }

    // if there is an API error
    if (this.props.error) {
      // updating error states
      this.setState({
        error: this.props.error,
        errorMessage: this.props.errorMessage,
        isLoading: false
      });
    }
  };

  componentDidUpdate = prevProps => {
    // if testSectionLoaded gets updated
    if (prevProps.testSectionLoaded !== this.props.testSectionLoaded) {
      // make sure that the testSection props is defined
      if (this.props.testSection) {
        // set loading to false
        this.setState({ isLoading: false });
      }
    }
    // if testSection gets updated
    if (prevProps.testSection !== this.props.testSection) {
      // make sure that testSectionLoaded is set to true
      if (this.props.testSectionLoaded) {
        // set loading to false
        this.setState({ isLoading: false });
      }
    }
  };

  handleBreakBankLogic = () => {
    // making sure that there is an existing question list in this current test section
    let containsQuestionList = false;
    for (let i = 0; i < this.props.testSection.components.length; i++) {
      // contains question list
      if (
        this.props.testSection.components[i].component_type ===
        TestSectionComponentType.QUESTION_LIST || this.props.testSection.components[i].component_type ===
        TestSectionComponentType.ITEM_BANK
      ) {
        // updating containsQuestionList
        containsQuestionList = true;
        break;
      }
    }
    // if question list not in a sample test
    if (
      containsQuestionList &&
      typeof this.props.assignedTestId !== "undefined" &&
      this.props.assignedTestId !== null
    ) {
      // checking if we need to display the pause button or not (in the test footer)
      // getting break bank remaining time (if accommodation request + accommodationRequestId exist)
      if (
        typeof this.props.accommodationRequestId !== "undefined" &&
        this.props.accommodationRequestId !== null
      ) {
        this.props.getBreakBankRemainingTime(this.props.accommodationRequestId).then(response => {
          // accommodation request exists
          if (response.existing_break_bank_accommodation_request) {
            // updating displayPauseButtonInFooter and breakBankData states
            this.setState({
              displayPauseButtonInFooter: true,
              breakBankData: response,
              isLoading: false
            });
          } else {
            // updating displayPauseButtonInFooter and breakBankData states
            this.setState({
              displayPauseButtonInFooter: false,
              breakBankData: {},
              isLoading: false
            });
          }
        });
      } else {
        this.setState({ isLoading: false });
      }
    } else {
      // updating displayPauseButtonInFooter state
      this.setState({ displayPauseButtonInFooter: false, isLoading: false });
    }
  };

  isTimedSection = defaultTime => {
    if (defaultTime || defaultTime === 0) return true;
    return false;
  };

  setNextButtonDisabled = state => {
    this.setState({ isNextButtonDisabled: state });
  };

  openStartTestPopup = () => {
    this.setState({ showStartTestPopup: true });
  };

  closeStartTestPopup = () => {
    this.setState({ showStartTestPopup: false });
  };

  openSubmitTestPopup = () => {
    this.setState({ showSubmitTestPopup: true });
  };

  closeSubmitTestPopup = () => {
    this.setState({ showSubmitTestPopup: false });
  };

  factory = () => {
    const { testSection } = this.props;
    if (!testSection || testSection.components[0] === undefined) {
      return (
        <>
          <h1>Error</h1>
          <h3>Malformed Test Definition</h3>
          <h4>missing Test Section Component child</h4>
        </>
      );
    }
    switch (testSection.type) {
      case TestSectionType.SINGLE_COMPONENT:
        return (
          <div id="unit-test-single-component-factory">
            <SingleComponentFactory
              testSection={testSection.components[0]}
              type={testSection.components[0].component_type}
              setNextButtonDisabled={this.setNextButtonDisabled}
              calledInTestBuilder={this.props.calledInTestBuilder}
            />
          </div>
        );
      case TestSectionType.TOP_TABS:
        return (
          <div id="unit-test-top-tabs-component">
            <TopTabs
              testSection={testSection}
              currentLanguage={this.props.currentLanguage}
              usesNotepad={this.props.usesNotepad}
              usesCalculator={this.props.usesCalculator}
              defaultTab={this.props.defaultTab}
              calledInTestBuilder={this.props.calledInTestBuilder}
            />
          </div>
        );
      case TestSectionType.FINISH:
        return (
          <div id="unit-test-single-component-factory">
            <SingleComponentFactory
              testSection={testSection.components[0]}
              type={testSection.components[0].component_type}
              setNextButtonDisabled={this.setNextButtonDisabled}
              calledInTestBuilder={this.props.calledInTestBuilder}
            />
          </div>
        );
      case TestSectionType.QUIT:
        return (
          <div id="unit-test-single-component-factory">
            <SingleComponentFactory
              testSection={this.props.testSection.components[0]}
              type={this.props.testSection.components[0].component_type}
              calledInTestBuilder={this.props.calledInTestBuilder}
            />
          </div>
        );
      default:
        return (
          <div id="unit-test-unsupported-component">
            <h1>Unsupported Page Type: {testSection.type}</h1>
          </div>
        );
    }
  };

  submitTest = () => {
    this.setState({ popupVisible: !this.state.popupVisible });
  };

  buttonFactory = () => {
    if (!this.props.testSection) return;
    const button = this.props.testSection.next_section_button;
    if (this.props.testSectionType === TestSectionType.FINISH) {
      return (
        <div style={styles.startTestBtn} id="unit-test-go-home-button" className="notranslate">
          <CustomButton
            label={LOCALIZE.mcTest.finishPage.homeButton}
            action={this.props.handleFinishTest}
            customStyle={styles.startTestBtn}
            buttonTheme={THEME.PRIMARY_WIDE}
          />
        </div>
      );
    }
    if (this.props.testSectionType === TestSectionType.QUIT) {
      return (
        <div style={styles.startTestBtn} id="unit-test-go-home-button" className="notranslate">
          <CustomButton
            label={LOCALIZE.mcTest.finishPage.homeButton}
            action={this.props.handleFinishTest}
            customStyle={styles.startTestBtn}
            buttonTheme={THEME.PRIMARY_WIDE}
          />
        </div>
      );
    }
    if (button.button_type === NextSectionButtonType.POPUP) {
      const rightButtonState = button.confirm_proceed
        ? !this.state.proceedCheckbox
        : this.state.proceedCheckbox;
      return (
        <div id="unit-test-popup-button">
          <TestFooter
            submitTest={this.submitTest}
            timeout={this.props.timeout}
            timeLimit={this.props.timeLimit}
            startTime={this.props.testSectionStartTime}
            buttonText={button.button_text}
            isSampleTest={this.props.newTestDefinition.test_definition[0].is_public}
            displayPauseButton={this.state.displayPauseButtonInFooter}
            breakBankData={this.state.breakBankData}
          />
          <PopupBox
            show={this.state.popupVisible}
            handleClose={this.closePopup}
            title={button.title}
            description={this.popupDescription(button)}
            leftButtonType={BUTTON_TYPE.secondary}
            leftButtonTitle={LOCALIZE.commons.cancel}
            rightButtonState={rightButtonState}
            rightButtonType={BUTTON_TYPE.primary}
            rightButtonTitle={button.button_text}
            rightButtonAction={this.props.handleNext}
          />
        </div>
      );
    }
    if (button.button_type === NextSectionButtonType.PROCEED) {
      return (
        <div style={styles.startTestBtn} id="unit-test-proceed-button" className="notranslate">
          <CustomButton
            label={button.button_text}
            action={this.props.handleNext}
            customStyle={styles.startTestBtn}
            buttonTheme={THEME.PRIMARY_WIDE}
            ariaLabel={LOCALIZE.ariaLabel.startTestBtn}
            disabled={this.state.isNextButtonDisabled}
          ></CustomButton>
        </div>
      );
    }
  };

  closePopup = () => {
    this.setState({ popupVisible: !this.state.popupVisible, proceedCheckbox: false });
    document.getElementById("unit-test-popup-button").focus();
  };

  popupDescription = button => {
    const timeSectionWording = (
      <>
        {button.break_bank !== null && (
          <>
            <p style={styles.boldText}>{LOCALIZE.commons.confirmStartTest.breakBankTitle}</p>
            <p>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              {LOCALIZE.formatString(
                LOCALIZE.commons.confirmStartTest.breakBankWarning,
                <b>{getFormattedTestTime(button.break_bank)}</b>
              )}
            </p>
          </>
        )}
        <>
          <p style={styles.boldText}>{LOCALIZE.commons.confirmStartTest.testSectionTimeTitle}</p>
          <p>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            {LOCALIZE.formatString(
              LOCALIZE.commons.confirmStartTest.newtimerWarning,
              <b>
                {button.next_section_time_limit
                  ? getFormattedTestTime(button.next_section_time_limit)
                  : LOCALIZE.commons.confirmStartTest.timeUnlimited}
              </b>
            )}
          </p>
        </>
        {button.additional_time !== null && (
          <p>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            {LOCALIZE.formatString(
              LOCALIZE.commons.confirmStartTest.additionalTimeWarning,
              <b>{getFormattedTestTime(button.additional_time)}</b>
            )}
          </p>
        )}
        <div style={styles.wishToProceedContainer}>
          <p>{LOCALIZE.commons.confirmStartTest.wishToProceed}</p>
        </div>
      </>
    );

    const checkboxTransformScale = getCheckboxTransformScale(this.props.accommodations.fontSize);

    return (
      <>
        <div id="unit-test-popup-description">
          <ReactMarkdown
            rehypePlugins={[rehypeRaw]}
            children={button.content}
            // add className="notranslate" to 'p' tags to prevent Microsoft Edge from translating
            components={{
              p: ({ node, ...props }) => <p className="notranslate" {...props} />
            }}
          />
        </div>
        {button.next_section_time_limit && (
          <div id="unit-test-popup-next-section-time-limit">{timeSectionWording}</div>
        )}
        {button.confirm_proceed && (
          <div
            id="unit-test-popup-confirm-procced"
            className="custom-control custom-checkbox"
            style={styles.checkboxZone}
          >
            <div style={styles.checkboxContainer}>
              <input
                type="checkbox"
                id={"procced-to-next-section-checkbox"}
                checked={this.state.proceedCheckbox}
                style={{ ...styles.checkbox, ...{ transform: checkboxTransformScale } }}
                onChange={() => {
                  this.setState({ proceedCheckbox: !this.state.proceedCheckbox });
                }}
              />
            </div>
            <div style={styles.checkboxLabelContainer}>
              <label htmlFor={"procced-to-next-section-checkbox"} style={styles.checkboxLabel}>
                {LOCALIZE.commons.confirmStartTest.confirmProceed}
              </label>
            </div>
          </div>
        )}
      </>
    );
  };

  render() {
    const noSelect = !this.props.readOnly;
    const customFontSize = {
      fontSize: `${parseInt(this.props.accommodations.fontSize.split("px")[0]) + 12}px`
    };
    return (
      <div>
        {this.state.isLoading && (
          <div>
            <h2 style={{ width: "900px", margin: "auto", paddingTop: "20px" }}>
              {LOCALIZE.commons.loading}
            </h2>
            <div style={{ ...styles.loadingContainer, ...customFontSize }}>
              {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
              <label className="fa fa-spinner fa-spin">
                <FontAwesomeIcon icon={faSpinner} />
              </label>
            </div>
          </div>
        )}
        {!this.state.isLoading && (
          <div>
            {this.state.error && (
              <div style={styles.container}>
                <div
                  style={{ width: "900px", margin: "auto", paddingTop: "20px", color: "red" }}
                  id="test-page-error-message"
                >
                  <h1>{LOCALIZE.commons.error}</h1>
                  {this.state.errorMessage.message &&
                    this.state.errorMessage.message.map((message, index) => {
                      return <h2 key={index}>{message[`${this.props.currentLanguage}`]}</h2>;
                    })}
                  {!this.state.errorMessage.message && (
                    <h2>{LOCALIZE.testBuilder.errors.nondescriptError}</h2>
                  )}
                  <p>{this.state.errorMessage.error}</p>
                </div>
              </div>
            )}

            {!this.state.error && (
              <div
                className={noSelect ? "no-select" : ""}
                id="main-content"
                tabIndex={0}
                style={styles.paddingTop}
              >
                {this.factory()}
                {!this.props.readOnly && this.buttonFactory()}
              </div>
            )}
          </div>
        )}
      </div>
    );
  }
}

export { TestSectionComponentFactory as UnconnectedTestSectionComponentFactory };
const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    testSectionLoaded: state.testSection.isLoaded,
    newTestDefinition: state.testBuilder,
    accommodations: state.accommodations,
    viewTestSectionOrderNumber: state.testBuilder.viewTestSectionOrderNumber,
    accommodationRequestId: state.assignedTest.accommodationRequestId
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateTestSection,
      setContentLoaded,
      setStartTime,
      setContentUnLoaded,
      getBreakBankRemainingTime
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(TestSectionComponentFactory);
