import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { QuestionType } from "./Constants";
import QuestionFactory from "./QuestionFactory";
import MarkdownButtonAnswer from "./MarkdownButtonAnswer";
import NextPreviousButtonNav from "../commons/NextPreviousButtonNav";
import Email from "../eMIB/Email";
import QuestionContentScorerHeaderFactory from "./QuestionContentScorerHeaderFactory";
import { updateRatings } from "../../modules/ScorerRedux";
import { LANGUAGES } from "../commons/Translation";

const styles = {
  mcPadding: {
    padding: 16
  }
};
class QuestionContentFactory extends Component {
  static propTypes = {
    question: PropTypes.object,
    changeQuestion: PropTypes.func,
    calledInTestBuilder: PropTypes.bool
  };

  componentDidMount = () => {
    // scorer mode
    if (this.props.readOnly) {
      this.props.updateRatings(this.props.question);
    }
  };

  contentFactory = () => {
    const { question, questions, questionIndex, questionId } = this.props;
    switch (this.props.type) {
      case QuestionType.MULTIPLE_CHOICE:
        // initializing isBilingualQuestion to false
        let isBilingualQuestion = false;
        // called in the test builder
        // if it's not called in the test builder, the serializer will take care of that translation, so we can keep the isBilingualQuestion set to false
        if (this.props.calledInTestBuilder) {
          // checking if this is a bilingual question (if the answers are bilingual, that means that the whole question is bilingual as well)
          if (
            question.answers.filter(obj => obj.language === LANGUAGES.english).length > 0 &&
            question.answers.filter(obj => obj.language === LANGUAGES.french).length > 0
          ) {
            // set isBilingualQuestion to true
            isBilingualQuestion = true;
          }
        }
        return (
          <div
            id="unit-test-question-content-factory-multiple-choice"
            style={styles.mcPadding}
            lang={this.props.lang}
            tabIndex={0}
          >
            <QuestionFactory
              questionSections={question.sections}
              index={questionIndex}
              lang={this.props.lang}
              isBilingualQuestion={isBilingualQuestion}
            />
            {/* this could be turned into a factory for different answer types
       but for now, only markdown multiple choice */}
            {isBilingualQuestion
              ? question.answers.map(
                  (answer, answerIndex) =>
                    // getting answers in the right language
                    answer.language === this.props.currentLanguage && (
                      <div key={answerIndex}>
                        <MarkdownButtonAnswer
                          key={answerIndex}
                          answer={answer}
                          answerId={answer.id}
                          questionId={question.id}
                          handleClick={this.props.handleAnswerClick}
                          lang={this.props.lang}
                        />
                      </div>
                    )
                )
              : question.answers.map((answer, answerIndex) => (
                  <div key={answerIndex}>
                    <MarkdownButtonAnswer
                      key={answerIndex}
                      answer={answer}
                      answerId={answer.id}
                      questionId={question.id}
                      handleClick={this.props.handleAnswerClick}
                      lang={this.props.lang}
                    />
                  </div>
                ))}
            {/* next button mark for review and prev button */}
            <hr></hr>
            <NextPreviousButtonNav
              lang={this.props.currentLanguage}
              showNext={typeof questions[questionIndex + 1] !== "undefined"}
              showPrevious={typeof questions[questionIndex - 1] !== "undefined"}
              showReview={true}
              isMarkedForReview={this.props.isMarkedForReview}
              questionId={questionId}
              onChangeToPrevious={() => {
                this.props.changeQuestion(
                  typeof questions[questionIndex - 1] !== "undefined"
                    ? questions[questionIndex - 1].id
                    : undefined
                );
              }}
              onChangeToNext={() => {
                this.props.changeQuestion(
                  typeof questions[questionIndex + 1] !== "undefined"
                    ? questions[questionIndex + 1].id
                    : undefined
                );
              }}
              onMarkForReview={() => {
                this.props.markForReview(question.id);
              }}
            />
          </div>
        );

      case QuestionType.EMAIL:
        return (
          <div id="unit-test-question-content-factory-email">
            <Email
              questionId={question.id}
              email={question.email}
              emailResponses={this.props.emailResponses}
              taskResponses={this.props.taskResponses}
              emailCount={this.props.emailResponses.length}
              taskCount={this.props.taskResponses.length}
              addressBook={this.props.addressBook}
              disabled={this.props.readOnly}
            />
          </div>
        );

      default:
        return <h1>Unsupported Question Type: {this.props.type}</h1>;
    }
  };

  render() {
    return (
      <>
        <QuestionContentScorerHeaderFactory {...this.props} />
        {this.contentFactory()}
      </>
    );
  }
}

export { QuestionContentFactory as UnconnectedQuestionContentFactory };
const mapStateToProps = (state, ownProps) => {
  let isMarkedForReview = false;
  if (typeof state.questionList.questionSummaries[`${ownProps.questionId}`] !== "undefined") {
    isMarkedForReview =
      state.questionList.questionSummaries[`${ownProps.questionId}`].review || false;
  }
  return {
    currentLanguage: state.localize.language,
    testContentHeight: state.testSection.testHeight,
    testSectionId: state.testSection.testSection.id,
    questionSummaries: state.questionList.questionSummaries,
    emailResponses: [...state.emailInbox.emailResponses].filter(
      x => x.questionId === ownProps.question.id
    ),
    taskResponses: [...state.emailInbox.taskResponses].filter(
      x => x.questionId === ownProps.question.id
    ),
    readOnly: state.testSection.readOnly,
    isMarkedForReview: isMarkedForReview
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateRatings
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(QuestionContentFactory);
