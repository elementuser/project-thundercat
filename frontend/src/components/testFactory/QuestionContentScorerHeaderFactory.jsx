import React, { Component } from "react";
import { QuestionType } from "./Constants";
import CollapsingItemHeader from "./CollapsingItemHeader";
import LOCALIZE from "../../text_resources";

class QuestionContentScorerHeaderFactory extends Component {
  render() {
    if (!this.props.readOnly) {
      return null;
    }
    const { question } = this.props;
    let situation = "";
    if (question.situation) {
      situation = question.situation.situation;
    }

    if (this.props.type === QuestionType.EMAIL) {
      return (
        <CollapsingItemHeader
          title={<label>{LOCALIZE.scorer.situationTitle}</label>}
          body={<>{situation}</>}
        />
      );
    }
    return null;
  }
}

export default QuestionContentScorerHeaderFactory;
