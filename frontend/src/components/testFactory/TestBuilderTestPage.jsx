/* eslint-disable no-param-reassign */
/* eslint-disable no-await-in-loop */
/* eslint-disable no-loop-func */
import React, { Component } from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { setLanguage, LANGUAGES } from "../../modules/LocalizeRedux";
import TestSectionComponentFactory from "./TestSectionComponentFactory";
import {
  getTestSection,
  updateTestSection,
  setStartTime,
  setContentLoaded,
  setContentUnLoaded,
  getQuestionsList
} from "../../modules/TestSectionRedux";
import { Helmet } from "react-helmet";
import LOCALIZE from "../../text_resources";
import { tryTestSection } from "../../modules/TestBuilderRedux";
import { PATH } from "../commons/Constants";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowAltCircleLeft, faSpinner } from "@fortawesome/free-solid-svg-icons";
import CustomButton, { THEME } from "../commons/CustomButton";
import { TestSectionComponentType, TestSectionType } from "./Constants";
import { getGeneratedItemsListFromBundle } from "../../modules/ItemBankRedux";
import shuffleArrayOfObjects from "../../helpers/shuffleArray";

const styles = {
  container: {
    maxWidth: 1400,
    paddingTop: 20,
    paddingRight: 20,
    paddingLeft: 20,
    margin: "0px auto",
    textAlign: "left"
  },
  buttonLabel: {
    marginLeft: 6
  },
  loadingContainer: {
    textAlign: "center"
  }
};

class TestBuilderTestPage extends Component {
  constructor(props, context) {
    super(props, context);

    this.PropTypes = {
      setLanguage: PropTypes.func,
      currentLanguage: PropTypes.string,
      sampleTest: PropTypes.bool
    };

    TestBuilderTestPage.defaultProps = {
      sampleTest: false
    };
  }

  state = {
    isLoading: true,
    questionsList: {},
    error: false,
    errorMessage: "",
    calledInTestBuilder: true
  };

  componentDidMount = () => {
    // test section is not loaded
    if (!this.props.testSectionLoaded) {
      // getting test section
      this.getTestSection();
      // test section is loaded
    } else {
      // set loading to false
      this.setState({ isLoading: false });
    }
  };

  componentDidUpdate = (prevProps, prevState) => {
    // if testSectionLoaded gets updated
    if (prevProps.testSectionLoaded !== this.props.testSectionLoaded) {
      this.setState({ isLoading: false });
    }
    // if questionsList state gets updated
    if (prevState.questionsList !== this.state.questionsList) {
      this.updateTestSectionData();
    }
  };

  getTestSection = () => {
    // set loading to true
    this.setState(
      { isLoading: true },
      () => {
        // adding small delay, so the loading icon is displayed
        setTimeout(() => {
          this.convertReduxStatesToJson();
        });
      },
      100
    );
  };

  updateTestSectionData = () => {
    this.props.updateTestSection(this.state.questionsList, this.props.currentLanguage);

    if (this.isTimedSection(this.state.questionsList.default_time)) {
      // store the start time in redux
      if (!this.state.questionsList.start_time) {
        this.props.setStartTime(Date());
      } else {
        this.props.setStartTime(new Date(this.state.questionsList.start_time).toString());
      }
    }

    this.props.setContentLoaded();
  };

  // async function that allows us to get all related item bank list objects
  getItemBankItemsListObjs = async () => {
    const itemBankItemsLists = [];

    // if at least one item bank rule has been defined
    if (this.props.newTestDefinition.item_bank_rules.length > 0) {
      const shuffleRules = this.props.newTestDefinition.test_section_components.filter(
        obj => obj.id === this.props.newTestDefinition.item_bank_rules[0].test_section_component
      )[0].shuffle_question_blocks;
      const body = {
        item_bank_rules: this.props.newTestDefinition.item_bank_rules,
        shuffle_rules: shuffleRules
      };
      // getting items list from the bundle of the current iteration
      await this.props.getGeneratedItemsListFromBundle(body).then(response => {
        // updating itemBankItemsLists
        itemBankItemsLists.push({
          test_section_component:
            this.props.newTestDefinition.item_bank_rules[0].test_section_component,
          items_list: response
        });
      });
    }

    // returning itemBankItemsLists
    return itemBankItemsLists;
  };

  // this function is converting the local redux states in compatible formatted json (improves performance for view test section locally in the test builder)
  convertReduxStatesToJson = () => {
    // initializing variables
    const currentPageSectionsObj = [];
    const questionsData = [];
    const itemBankItemsLists = [];
    const currentTestSectionComponentsObj = [];
    let hasQuestionsListSection = false;
    let hasItemBankItemsListSection = false;
    let isFirstLoad = true;

    // setting first load to false if language has been changed (if the testSection object is not empty)
    if (Object.entries(this.props.testSection).length > 0) {
      isFirstLoad = false;
    }

    // viewTestSectionOrderNumber is undefined (happends when you first load a test, then view the questions list and then switch language)
    let orderToView = this.props.viewTestSectionOrderNumber;
    if (typeof this.props.viewTestSectionOrderNumber === "undefined") {
      orderToView = this.props.testSection.order;
    }

    // getting current test section
    const currentTestSection = this.props.newTestDefinition.test_sections.filter(
      obj => obj.order === orderToView
    )[0];
    // getting current test section components
    const currentTestSectionComponents =
      this.props.newTestDefinition.test_section_components.filter(
        obj => obj.test_section.indexOf(currentTestSection.id) >= 0
      );
    // getting current questions
    const currentQuestions = this.props.newTestDefinition.questions.filter(obj =>
      currentTestSectionComponents.some(
        testSectionComponent => testSectionComponent.id === obj.test_section_component
      )
    );
    this.getItemBankItemsListObjs().then(itemBankItemsLists => {
      // getting section component pages
      const currentSectionComponentPages =
        this.props.newTestDefinition.section_component_pages.filter(obj =>
          currentTestSectionComponents.some(
            testSectionComponent => testSectionComponent.id === obj.test_section_component
          )
        );
      // getting current page sections
      const currentPageSections = this.props.newTestDefinition.page_sections.filter(obj =>
        currentSectionComponentPages.some(
          sectionComponentPage => sectionComponentPage.id === obj.section_component_page
        )
      );
      // getting current page section definitions
      const currentPageSectionDefinitions =
        this.props.newTestDefinition.page_section_definitions.filter(obj =>
          currentPageSections.some(pageSection => pageSection.id === obj.page_section)
        );
      // getting current next section button
      const currentNextSectionButton = this.props.newTestDefinition.next_section_buttons.filter(
        obj => obj.test_section === currentTestSection.id
      );

      // looping in currentPageSections in order to create the currentPageSectionsObj array
      for (let i = 0; i < currentPageSections.length; i++) {
        currentPageSectionsObj.push({
          id: currentPageSections[i].id,
          order: currentPageSections[i].order,
          page_section_content: {
            content: currentPageSectionDefinitions.filter(
              obj =>
                obj.language === this.props.currentLanguage &&
                obj.page_section === currentPageSections[i].id
            )[0].content
          },
          page_section_type: currentPageSections[i].page_section_type,
          section_component_page: currentPageSections[i].section_component_page
        });
      }

      // looping in currentQuestions in order to create the currentPageSectionsObj array
      for (let i = 0; i < currentQuestions.length; i++) {
        // getting related answers
        const answers = this.props.newTestDefinition.answers.filter(
          obj => obj.question === currentQuestions[i].id
        );

        // getting multiple choice question details
        const multipleChoiceQuestionDetails =
          this.props.newTestDefinition.multiple_choice_question_details.filter(
            obj => obj.question === currentQuestions[i].id
          )[0];

        // getting question sections
        const questionSections = this.props.newTestDefinition.question_sections.filter(
          obj => obj.question === currentQuestions[i].id
        );

        // initializing answersData
        const answersData = [];

        // looping in answers
        for (let j = 0; j < answers.length; j++) {
          // getting answer details
          const answerDetails = this.props.newTestDefinition.answer_details.filter(
            obj => obj.answer === answers[j].id
          )[0];

          // populating answersData array
          answersData.push({
            answer: answerDetails.answer,
            content: answerDetails.content,
            id: answerDetails.id,
            language: answerDetails.language,
            order: answers[j].order,
            question: answers[j].question
          });
        }

        // initializing questionSectionsData
        const questionSectionsData = [];

        // looping in questionSections
        for (let j = 0; j < questionSections.length; j++) {
          // populating questionSectionsData array
          questionSectionsData.push({
            id: questionSections[j].id,
            order: questionSections[j].order,
            question: questionSections[j].question,
            question_section_content: {
              content: this.props.newTestDefinition.question_section_definitions.filter(
                obj => obj.question_section === questionSections[j].id
              )[0].content
            },
            question_section_type: questionSections[j].question_section_type
          });
        }

        // populating questionsData array
        questionsData.push({
          answers: answersData,
          details: {
            difficulty: multipleChoiceQuestionDetails.question_difficulty_type
          },
          // TODO: EMIB/INBOX
          email: {
            competency_types: [],
            email_id: null,
            question: null
          },
          // TODO: EMIB/INBOX
          example_rating: [],
          id: currentQuestions[i].id,
          order: currentQuestions[i].order,
          question_type: currentQuestions[i].question_type,
          sections: questionSectionsData,
          // TODO: EMIB/INBOX
          situation: {}
        });
      }

      // looping in currentTestSectionComponents in order to create the currentTestSectionComponentsObj array
      for (let i = 0; i < currentTestSectionComponents.length; i++) {
        // initializing testSectionComponentObj
        const testSectionComponentObj = [];
        // QUESITON LIST
        if (
          currentTestSectionComponents[i].component_type === TestSectionComponentType.QUESTION_LIST
        ) {
          // set hasQuestionsListSection to true
          hasQuestionsListSection = true;
          // populating currentTestSectionComponentsObj array
          currentTestSectionComponentsObj.push({
            component_type: currentTestSectionComponents[i].component_type,
            id: currentTestSectionComponents[i].id,
            language: currentTestSectionComponents[i].language,
            order: currentTestSectionComponents[i].order,
            shuffle_question_blocks: currentTestSectionComponents[i].shuffle_question_blocks,
            test_section_component: {
              questions: questionsData
            },
            title: {
              en: currentTestSectionComponents[i].en_title,
              fr: currentTestSectionComponents[i].fr_title
            }
          });
        }
        // ITEM BANK
        else if (
          currentTestSectionComponents[i].component_type === TestSectionComponentType.ITEM_BANK
        ) {
          // set let hasItemBankItemsListSection = false; to true
          hasItemBankItemsListSection = true;
          // populating currentTestSectionComponentsObj array
          currentTestSectionComponentsObj.push({
            component_type: currentTestSectionComponents[i].component_type,
            id: currentTestSectionComponents[i].id,
            language: currentTestSectionComponents[i].language,
            order: currentTestSectionComponents[i].order,
            shuffle_question_blocks: currentTestSectionComponents[i].shuffle_question_blocks,
            test_section_component: {
              questions: itemBankItemsLists.filter(
                obj => obj.test_section_component === currentTestSectionComponents[i].id
              )
            },
            title: {
              en: currentTestSectionComponents[i].en_title,
              fr: currentTestSectionComponents[i].fr_title
            }
          });
        }
        // TODO: EMIB/INBOX
        // else if (currentTestSectionComponents[i].component_type === TestSectionComponentType.INBOX) {}
        // SINGLE PAGE / SIDE NAVIGATION
        else {
          // looping in currentSectionComponentPages in order to create the testSectionComponentObj array
          for (let j = 0; j < currentSectionComponentPages.length; j++) {
            testSectionComponentObj.push({
              id: currentSectionComponentPages[j].id,
              order: currentSectionComponentPages[j].order,
              pages: {
                page_sections: currentPageSectionsObj.filter(
                  obj => obj.section_component_page === currentSectionComponentPages[j].id
                )
              },
              title: {
                en: currentSectionComponentPages[j].en_title,
                fr: currentSectionComponentPages[j].fr_title
              }
            });
          }

          // populating currentTestSectionComponentsObj array
          currentTestSectionComponentsObj.push({
            component_type: currentTestSectionComponents[i].component_type,
            id: currentTestSectionComponents[i].id,
            language: currentTestSectionComponents[i].language,
            order: currentTestSectionComponents[i].order,
            shuffle_question_blocks: currentTestSectionComponents[i].shuffle_question_blocks,
            test_section_component: testSectionComponentObj,
            title: {
              en: currentTestSectionComponents[i].en_title,
              fr: currentTestSectionComponents[i].fr_title
            }
          });
        }
      }

      // reviewing section that contains at least one questions list
      if (hasQuestionsListSection || hasItemBankItemsListSection) {
        if (hasQuestionsListSection) {
          // TEMP (NEED TO BE IMPROVED)
          const currentQuestionsListTestSectionComponent = currentTestSectionComponents.filter(
            obj => obj.component_type === TestSectionComponentType.QUESTION_LIST
          )[0];

          const body = {
            local_testing: true,
            assigned_test_id: null,
            test_section_component_id: currentQuestionsListTestSectionComponent.id,
            shuffle_all: currentQuestionsListTestSectionComponent.shuffle_all_questions,
            shuffle_question_blocks:
              currentQuestionsListTestSectionComponent.shuffle_question_blocks,
            test_definition: {
              new_test: this.props.newTestDefinition
            }
          };

          // if first load
          if (isFirstLoad) {
            // getting questions list
            this.props.getQuestionsList(body).then(response => {
              // response is successfully received
              if (response.ok) {
                // getting index of test section where component_type is QUESTION_LIST
                const testSectionComponentOfTypeQuestionsList = testSectionType =>
                  testSectionType.component_type === TestSectionComponentType.QUESTION_LIST;

                const index = currentTestSectionComponentsObj.findIndex(
                  testSectionComponentOfTypeQuestionsList
                );

                // updating questions data based on getQuestionsList response
                currentTestSectionComponentsObj[index].test_section_component.questions =
                  response.body;

                // getting formatted json data
                const jsonResponse = this.getFormattedJson(
                  currentTestSectionComponentsObj,
                  currentTestSection,
                  currentNextSectionButton
                );

                // updating needed states
                this.setState({ questionsList: jsonResponse });
                // error
              } else {
                this.setState({ error: true, errorMessage: response.body.error, isLoading: false });
              }
            });
            // language change detected
          } else {
            // getting index of test section where component_type is QUESTION_LIST
            const testSectionComponentOfTypeQuestionsList = testSectionType =>
              testSectionType.component_type === TestSectionComponentType.QUESTION_LIST;

            const index = currentTestSectionComponentsObj.findIndex(
              testSectionComponentOfTypeQuestionsList
            );

            // updating questions data based on existing test section state
            currentTestSectionComponentsObj[index].test_section_component.questions =
              this.props.testSection.localize[
                `${
                  this.props.currentLanguage === LANGUAGES.english
                    ? LANGUAGES.french
                    : LANGUAGES.english
                }`
              ].components[index].test_section_component.questions;
          }

          // has ITEM BANK component type
        }
        if (hasItemBankItemsListSection) {
          // TEMP (NEED TO BE IMPROVED)
          const currentQuestionsListTestSectionComponent = currentTestSectionComponents.filter(
            obj => obj.component_type === TestSectionComponentType.ITEM_BANK
          )[0];

          // getting index of test section where component_type is ITEM_BANK
          const testSectionComponentOfTypeQuestionsList = testSectionType =>
            testSectionType.component_type === TestSectionComponentType.ITEM_BANK;

          const index = currentTestSectionComponentsObj.findIndex(
            testSectionComponentOfTypeQuestionsList
          );

          // formatting item bank items
          const formattedItemBankItems = [];

          // getting related items lists
          let related_items_list_objs = itemBankItemsLists.filter(
            obj => obj.test_section_component === currentQuestionsListTestSectionComponent.id
          );

          // if shuffle item bank rules (term used in the backend and in redux states is shuffle_question_blocks)
          if (currentQuestionsListTestSectionComponent.shuffle_question_blocks) {
            // ref: https://flaviocopes.com/how-to-shuffle-array-javascript/
            related_items_list_objs = shuffleArrayOfObjects(related_items_list_objs);
          }

          // looping in related_items_list_objs
          for (let i = 0; i < related_items_list_objs.length; i++) {
            for (let j = 0; j < related_items_list_objs[i].items_list.length; j++)
              formattedItemBankItems.push(related_items_list_objs[i].items_list[j]);
          }

          // updating questions data based on getQuestionsList response
          currentTestSectionComponentsObj[index].test_section_component.questions =
            formattedItemBankItems;

          // getting formatted json data
          const jsonResponse = this.getFormattedJson(
            currentTestSectionComponentsObj,
            currentTestSection,
            currentNextSectionButton
          );

          // updating needed states
          this.setState({ questionsList: jsonResponse });
        }
        // getting formatted json data
        const jsonResponse = this.getFormattedJson(
          currentTestSectionComponentsObj,
          currentTestSection,
          currentNextSectionButton
        );

        // updating needed states
        this.setState({ questionsList: jsonResponse });
      } else {
        // getting formatted json data
        const jsonResponse = this.getFormattedJson(
          currentTestSectionComponentsObj,
          currentTestSection,
          currentNextSectionButton
        );

        // updating needed states
        this.setState({ questionsList: jsonResponse });
      }
    });
  };

  getFormattedJson = (
    currentTestSectionComponentsObj,
    currentTestSection,
    currentNextSectionButton
  ) => {
    // formatting localize to match the backend serializer
    // initializing localize
    let localize = {};
    // current language is set to English
    if (this.props.currentLanguage === LANGUAGES.english) {
      localize = {
        en: {
          components: currentTestSectionComponentsObj,
          next_section_button:
            currentTestSection.section_type === TestSectionType.FINISH ||
            currentTestSection.section_type === TestSectionType.QUIT
              ? { button_text: {}, button_type: {} }
              : {
                  button_text: currentNextSectionButton.filter(
                    obj => obj.language === this.props.currentLanguage
                  )[0].button_text,
                  button_type: currentNextSectionButton.filter(
                    obj => obj.language === this.props.currentLanguage
                  )[0].next_section_button_type,
                  confirm_proceed: currentNextSectionButton.filter(
                    obj => obj.language === this.props.currentLanguage
                  )[0].confirm_proceed,
                  content: currentNextSectionButton.filter(
                    obj => obj.language === this.props.currentLanguage
                  )[0].content,
                  title: currentNextSectionButton.filter(
                    obj => obj.language === this.props.currentLanguage
                  )[0].title
                },
          title: currentTestSection[`${this.props.currentLanguage}_title`],
          type: currentTestSection.section_type
        }
      };
      // current language is set to French
    } else {
      localize = {
        fr: {
          components: currentTestSectionComponentsObj,
          next_section_button:
            currentTestSection.section_type === TestSectionType.FINISH ||
            currentTestSection.section_type === TestSectionType.QUIT
              ? { button_text: {}, button_type: {} }
              : {
                  button_text: currentNextSectionButton.filter(
                    obj => obj.language === this.props.currentLanguage
                  )[0].button_text,
                  button_type: currentNextSectionButton.filter(
                    obj => obj.language === this.props.currentLanguage
                  )[0].next_section_button_type,
                  confirm_proceed: currentNextSectionButton.filter(
                    obj => obj.language === this.props.currentLanguage
                  )[0].confirm_proceed,
                  content: currentNextSectionButton.filter(
                    obj => obj.language === this.props.currentLanguage
                  )[0].content,
                  title: currentNextSectionButton.filter(
                    obj => obj.language === this.props.currentLanguage
                  )[0].title
                },
          title: currentTestSection[`${this.props.currentLanguage}_title`],
          type: currentTestSection.section_type
        }
      };
    }

    const jsonResponse = {
      block_cheating: currentTestSection.block_cheating,
      count_up: this.props.newTestDefinition.test_definition[0].count_up,
      default_tab: currentTestSection.default_tab,
      default_time: currentTestSection.default_time,
      en_title: currentTestSection.en_title,
      fr_title: currentTestSection.fr_title,
      id: currentTestSection.id,
      is_public: this.props.newTestDefinition.test_definition[0].is_public,
      localize: localize,
      minimum_score: currentTestSection.minimum_score,
      next_section_button_type: currentTestSection.next_section_button_type,
      order: currentTestSection.order,
      redux: [],
      scorer: null,
      scoring_type: currentTestSection.scoring_type,
      section_type: currentTestSection.section_type,
      start_time: "",
      test_definition: this.props.newTestDefinition.test_definition[0].id,
      uses_notepad: currentTestSection.uses_notepad,
      uses_calculator: currentTestSection.uses_calculator,
      item_exposure: currentTestSection.item_exposure
    };

    return jsonResponse;
  };

  handleNext = () => {};

  handleFinishTest = () => {};

  isTimedSection = defaultTime => {
    if (defaultTime || defaultTime === 0) return true;
    return false;
  };

  timeout = () => {};

  handleGoBack = () => {
    this.props.setContentUnLoaded();
    this.props.history.push(PATH.testBuilder);
  };

  render() {
    const customFontSize = {
      fontSize: `${parseInt(this.props.accommodations.fontSize.split("px")[0]) + 12}px`
    };

    if (this.state.isLoading) {
      return (
        <div>
          <h2 style={{ width: "900px", margin: "auto", paddingTop: "20px" }}>
            {LOCALIZE.commons.loading}
          </h2>
          <div style={{ ...styles.loadingContainer, ...customFontSize }}>
            {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
            <label className="fa fa-spinner fa-spin">
              <FontAwesomeIcon icon={faSpinner} />
            </label>
          </div>
        </div>
      );
    }
    const { testSection } = this.props;

    return (
      <div style={styles.appPadding}>
        <Helmet>
          <html lang={this.props.currentLanguage} />
          <title className="notranslate">{LOCALIZE.titles.uitTest}</title>
        </Helmet>
        <div style={styles.container}>
          <CustomButton
            buttonId={"back-to-test-builder-button"}
            label={
              <>
                <FontAwesomeIcon icon={faArrowAltCircleLeft} />
                <span style={styles.buttonLabel}>{LOCALIZE.testBuilder.goBackButton}</span>
              </>
            }
            action={this.handleGoBack}
            buttonTheme={THEME.SECONDARY}
          />

          <TestSectionComponentFactory
            testSection={!this.state.error ? testSection.localize[this.props.currentLanguage] : {}}
            handleNext={this.handleNext}
            handleFinishTest={this.handleFinishTest}
            timeLimit={testSection.default_time}
            testSectionStartTime={this.props.testSectionStartTime}
            timeout={this.timeout}
            testSectionType={testSection.section_type}
            resetAllRedux={this.resetAllRedux}
            defaultTab={testSection.default_tab}
            usesNotepad={testSection.uses_notepad}
            usesCalculator={testSection.uses_calculator}
            accommodations={this.props.accommodations}
            calledInTestBuilder={true}
            error={this.state.error}
            errorMessage={this.state.errorMessage}
          />
        </div>
      </div>
    );
  }
}

export { TestBuilderTestPage as UnconnectedTestBuilderTestPage };
const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    testSection: state.testSection.testSection,
    testSectionLoaded: state.testSection.isLoaded,
    testSectionStartTime: state.testSection.startTime,
    newTestDefinition: state.testBuilder,
    viewTestSectionOrderNumber: state.testBuilder.viewTestSectionOrderNumber,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setLanguage,
      getTestSection,
      updateTestSection,
      setContentLoaded,
      setContentUnLoaded,
      setStartTime,
      tryTestSection,
      getQuestionsList,
      getGeneratedItemsListFromBundle
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(TestBuilderTestPage));
