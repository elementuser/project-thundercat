import React, { Component } from "react";
import PropTypes from "prop-types";
import LOCALIZE from "../../text_resources";
import "../../css/inbox.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faEye,
  faEyeSlash,
  faExclamationCircle,
  faCircle as faCircleSolid
} from "@fortawesome/free-solid-svg-icons";
import { Row, Col } from "react-bootstrap";
import { faCircle } from "@fortawesome/free-regular-svg-icons";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";
import { connect } from "react-redux";
import StyledTooltip, { TYPE, EFFECT, TRIGGER_TYPE } from "../authentication/StyledTooltip";

export const styles = {
  // buttons
  button: {
    textAlign: "left",
    padding: 5,
    borderWidth: "0px 1px 1px 1px",
    borderStyle: "solid",
    borderColor: "#00565E",
    cursor: "pointer",
    fontSize: 14,
    width: "100%",
    margin: "0px 0px 0px 0px"
  },
  buttonSelectedBackground: {
    backgroundColor: "#00565E"
  },
  buttonReadBackground: {
    backgroundColor: "#F5F5F5"
  },
  buttonUnreadBackground: {
    backgroundColor: "white"
  },
  buttonSelectedText: {
    color: "#D3FCFF",
    position: "relative"
  },
  buttonSelectedImage: {
    color: "#D3FCFF",
    position: "relative",
    margin: 2
  },
  buttonUnselectedText: {
    color: "black"
  },
  buttonUnselectedSymbol: {
    color: "#00565E",
    position: "relative",
    margin: 2
  },
  emptyIcon: {
    textAlign: "left",
    cursor: "pointer",
    fontSize: 14,
    width: "100%"
  },
  questionPreviewLabelContainer: {
    display: "table-cell",
    paddingRight: 12,
    verticalAlign: "middle"
  },
  questionPreviewIconsContainer: {
    display: "table-cell",
    width: "100%",
    textAlign: "right",
    verticalAlign: "middle"
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  }
};

class QuestionPreview extends Component {
  static propTypes = {
    isRead: PropTypes.bool.isRequired,
    isAnswered: PropTypes.bool.isRequired,
    isSelected: PropTypes.bool.isRequired
  };

  render() {
    let style = {
      fontFamily: this.props.accommodations.fontFamily,
      fontSize: this.props.accommodations.fontSize
    };
    if (this.props.accommodations.spacing) {
      style = { ...style, ...getLineSpacingCSS() };
    }

    // READ/UNREAD CHECK
    // defaults, or if unread
    let buttonBackgroundColor = styles.buttonUnreadBackground;
    if (this.props.isRead) {
      // if it is read
      buttonBackgroundColor = styles.buttonReadBackground;
    }

    // SELECTED/UNSELECTED CHECK
    // defaults, or unselected
    let buttonTextColor = styles.buttonUnselectedText;
    let imageStyle = styles.buttonUnselectedSymbol;
    if (this.props.isSelected) {
      // if it is selected
      buttonBackgroundColor = styles.buttonSelectedBackground;
      buttonTextColor = styles.buttonSelectedText;
      imageStyle = styles.buttonSelectedImage;
    }

    const buttonStyle = { ...styles.button, ...buttonTextColor, ...buttonBackgroundColor };
    const emptyIcon = { ...styles.emptyIcon, ...buttonTextColor, ...buttonBackgroundColor };

    const { index } = this.props;
    return (
      <Row
        id={
          this.props.isSelected
            ? `selected-question-preview-${index}`
            : `unselected-question-preview-${index}`
        }
        style={{ ...buttonStyle, ...style }}
      >
        <Col id={`unit-test-question-id-label-${index}`}>
          <div>
            <div style={styles.questionPreviewLabelContainer}>
              <label
                aria-labelledby={`question-number-label-for-accessibility-${index} ${
                  this.props.isMarkedForReview && `question-marked-for-review-tooltip-${index}`
                } ${
                  this.props.isAnswered
                    ? `question-answered-tooltip-${index}`
                    : `question-unanswered-tooltip-${index}`
                }
                ${
                  this.props.isRead
                    ? `question-seen-tooltip-${index}`
                    : `question-unseen-tooltip-${index}`
                } `}
                className="notranslate"
              >
                {LOCALIZE.formatString(LOCALIZE.mcTest.questionList.questionIdShort, index + 1)}
              </label>
              <label
                id={`question-number-label-for-accessibility-${index}`}
                style={styles.hiddenText}
              >
                {LOCALIZE.formatString(LOCALIZE.mcTest.questionList.questionIdLong, index + 1)}
              </label>
            </div>
            <div style={styles.questionPreviewIconsContainer}>
              {this.props.isMarkedForReview ? (
                <StyledTooltip
                  id={`question-marked-for-review-tooltip-${index}`}
                  place="top"
                  type={TYPE.light}
                  effect={EFFECT.solid}
                  triggerType={[TRIGGER_TYPE.hover]}
                  tooltipElement={
                    <FontAwesomeIcon
                      data-tip=""
                      data-for={`question-marked-for-review-tooltip-${index}`}
                      id={`unit-test-review-${index}`}
                      icon={faExclamationCircle}
                      style={{ ...imageStyle }}
                    />
                  }
                  tooltipContent={
                    <div>
                      <p>{LOCALIZE.mcTest.questionList.reviewQuestion}</p>
                    </div>
                  }
                />
              ) : (
                <div style={emptyIcon}></div>
              )}
              {this.props.isAnswered ? (
                <StyledTooltip
                  id={`question-answered-tooltip-${index}`}
                  place="top"
                  type={TYPE.light}
                  effect={EFFECT.solid}
                  triggerType={[TRIGGER_TYPE.hover]}
                  tooltipElement={
                    <FontAwesomeIcon
                      data-tip=""
                      data-for={`question-answered-tooltip-${index}`}
                      id={`unit-test-answered-${index}`}
                      icon={faCircleSolid}
                      style={{ ...imageStyle }}
                    />
                  }
                  tooltipContent={
                    <div>
                      <p>{LOCALIZE.mcTest.questionList.answeredQuestion}</p>
                    </div>
                  }
                />
              ) : (
                <StyledTooltip
                  id={`question-unanswered-tooltip-${index}`}
                  place="top"
                  type={TYPE.light}
                  effect={EFFECT.solid}
                  triggerType={[TRIGGER_TYPE.hover]}
                  tooltipElement={
                    <FontAwesomeIcon
                      data-tip=""
                      data-for={`question-unanswered-tooltip-${index}`}
                      id={`unit-test-unanswered-${index}`}
                      icon={faCircle}
                      style={{ ...imageStyle }}
                    />
                  }
                  tooltipContent={
                    <div>
                      <p>{LOCALIZE.mcTest.questionList.unansweredQuestion}</p>
                    </div>
                  }
                />
              )}
              {this.props.isRead ? (
                <StyledTooltip
                  id={`question-seen-tooltip-${index}`}
                  place="top"
                  type={TYPE.light}
                  effect={EFFECT.solid}
                  triggerType={[TRIGGER_TYPE.hover]}
                  tooltipElement={
                    <FontAwesomeIcon
                      data-tip=""
                      data-for={`question-seen-tooltip-${index}`}
                      id={`unit-test-seen-${index}`}
                      icon={faEye}
                      style={{ ...imageStyle }}
                    />
                  }
                  tooltipContent={
                    <div>
                      <p>{LOCALIZE.mcTest.questionList.seenQuestion}</p>
                    </div>
                  }
                />
              ) : (
                <StyledTooltip
                  id={`question-unseen-tooltip-${index}`}
                  place="top"
                  type={TYPE.light}
                  effect={EFFECT.solid}
                  triggerType={[TRIGGER_TYPE.hover]}
                  tooltipElement={
                    <FontAwesomeIcon
                      data-tip=""
                      data-for={`question-unseen-tooltip-${index}`}
                      id={`unit-test-unseen-${index}`}
                      icon={faEyeSlash}
                      style={{ ...imageStyle }}
                    />
                  }
                  tooltipContent={
                    <div>
                      <p>{LOCALIZE.mcTest.questionList.unseenQuestion}</p>
                    </div>
                  }
                />
              )}
            </div>
          </div>
        </Col>
      </Row>
    );
  }
}
export { QuestionPreview as UnconnectedQuestionPreview };

const mapStateToProps = (state, ownProps) => {
  return {
    accommodations: state.accommodations
  };
};

export default connect(mapStateToProps, null)(QuestionPreview);
