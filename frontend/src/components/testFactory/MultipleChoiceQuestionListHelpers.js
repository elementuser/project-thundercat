export async function changeQuestion(questionId, props) {
  // don't call the APIs if we have already done it
  if (questionId === props.previousQuestionId) {
    return;
  }

  // calling prop functions asynchronously
  setTimeout(() => {
    // updating needed redux states
    props.setPreviousQuestionId(questionId);
    props.readQuestion(questionId);
    props.changeCurrentQuestion(questionId);
    props.setSeenQuestionLoaded();

    // if it's a real test
    if (props.assignedTestId) {
      // DB Calls
      // adding current question as seen with empty answer (new row in CandidateAnswers and CandidateMultipleChoiceAnswers tables)
      props.seenUitQuestion(props.assignedTestId, questionId, props.testSectionComponentId);
      // writing history to database
      props.updateCandidateAnswerModifyDate(
        props.assignedTestId,
        questionId,
        props.testSectionComponentId
      );
    }
  }, 0);
}

export const handleAnswerClick = async (answerId, questionId, props) => {
  const { assignedTestId, currentLanguage } = props;
  props.answerQuestion(questionId, answerId);
  if (props.assignedTestId) {
    props
      .saveUitTestResponse(
        assignedTestId,
        questionId,
        answerId,
        props.testSectionComponentId,
        currentLanguage
      )
      .then(response => {
        let isBackendAndDbUp = true;
        // service unavailable
        if (response.status === 503) {
          isBackendAndDbUp = false;
        }
        // updating redux states
        props.updateBackendAndDbStatusState(isBackendAndDbUp);
      });
  }
};
