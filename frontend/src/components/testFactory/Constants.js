import LOCALIZE from "../../text_resources";

export const TestSectionType = {
  TOP_TABS: 1,
  SINGLE_COMPONENT: 2,
  FINISH: 3,
  QUIT: 4
};

export const NextSectionButtonType = {
  NONE: 0,
  PROCEED: 1,
  POPUP: 2
};
export const NextSectionButtonTypeObject = {
  NONE: { value: 0, label: "NONE" },
  PROCEED: {
    value: 1,
    label: LOCALIZE.testBuilder.testDefinition.constants.nextSectionButtonTypeObject.proceed
  },
  POPUP: {
    value: 2,
    label: LOCALIZE.testBuilder.testDefinition.constants.nextSectionButtonTypeObject.popup
  }
};

export const ReducerType = {
  INBOX: 1,
  QUESTION_LIST: 2,
  NOTEPAD: 3
};

export const TestSectionComponentType = {
  SINGLE_PAGE: 1,
  SIDE_NAVIGATION: 2,
  INBOX: 3,
  QUESTION_LIST: 4,
  ITEM_BANK: 5
};
export const TestSectionComponentTypeMap = {
  1: "SINGLE_PAGE",
  2: "SIDE_NAVIGATION",
  3: "INBOX",
  4: "QUESTION_LIST",
  5: "ITEM_BANK"
};

export const PageSectionType = {
  MARKDOWN: 1,
  ZOOM_IMAGE: 2,
  SAMPLE_EMAIL: 3,
  SAMPLE_EMAIL_RESPONSE: 4,
  SAMPLE_TASK_RESPONSE: 5,
  TREE_DESCRIPTION: 6,
  PRIVACY_NOTICE: 7
};
export const PageSectionTypeMap = {
  1: "MARKDOWN",
  2: "ZOOM_IMAGE",
  3: "SAMPLE_EMAIL",
  4: "SAMPLE_EMAIL_RESPONSE",
  5: "SAMPLE_TASK_RESPONSE",
  6: "TREE_DESCRIPTION",
  7: "PRIVACY_NOTICE"
};
export const QuestionType = {
  MULTIPLE_CHOICE: 1,
  EMAIL: 2
};

export const QuestionSectionType = {
  MARKDOWN: 1
};
export const QuestionSectionTypeObject = {
  MARKDOWN: { value: 1, label: "MARKDOWN" }
};

export const QuestionDifficultyType = {
  EASY: 1,
  MEDIUM: 2,
  HARD: 3
};

export const TestSectionScoringTypeObject = {
  NOT_SCORABLE: {
    value: 0,
    label: LOCALIZE.testBuilder.testDefinition.constants.testSectionScoringTypeObject.not_scorable
  },
  AUTO_SCORE: {
    value: 1,
    label: LOCALIZE.testBuilder.testDefinition.constants.testSectionScoringTypeObject.auto_score
  },
  COMPETENCY: {
    value: 2,
    label: LOCALIZE.testBuilder.testDefinition.constants.testSectionScoringTypeObject.competency
  }
};

// mirror of BreakBankActionsConstants under ...\backend\cms\views\test_break_bank.py
export const BreakBankActions = {
  PAUSE: "PAUSE",
  UNPAUSE: "UNPAUSE"
};

// mirror of ItemBankAccessType under ...\backend\cms\static\item_bank_access_types.py
export const ItemBankAccessType = {
  NONE: "is_none",
  OWNER: "is_owner",
  CONTRIBUTOR: "is_contributor"
};
