import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import SideNavigation from "./SideNavigation";
import PageSectionFactory from "./PageSectionFactory";
import { switchSideTab } from "../../modules/NavTabsRedux";

const styles = {
  iconContainer: {
    paddingLeft: 13
  },
  icon: {
    fontSize: 200
  },
  tabContainer: {
    zIndex: 1,
    padding: 0
  },
  tabContent: {
    backgroundColor: "transparent"
  },
  nav: {
    marginTop: 10,
    textAlign: "center"
  },
  body: {
    paddingLeft: 12
  }
};

class SideNavigationPage extends Component {
  static propTypes = {
    testSection: PropTypes.array
    // Props from Redux
  };

  switchTab = key => {
    this.props.switchSideTab(key);
  };

  render() {
    return (
      <SideNavigation
        specs={this.props.testSection.map(page => {
          return {
            menuString: page.title[this.props.currentLanguage],
            body: <PageSectionFactory sectionComponentPage={page.pages} />
          };
        })}
        startIndex={0}
        activeKey={
          typeof this.props.currentTab[this.props.currentTopTab] !== "undefined"
            ? this.props.currentTab[this.props.currentTopTab].sideTab
            : 1
        }
        switchTab={this.switchTab}
        displayNextPreviousButton={false}
        isMain={true}
        tabContainerStyle={styles.tabContainer}
        tabContentStyle={styles.tabContent}
        navStyle={styles.nav}
      />
    );
  }
}

export { SideNavigationPage as UnconnectedSideNavigationPage };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    currentTab: state.navTabs.currentSideTab,
    currentTopTab: state.navTabs.currentTopTab
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({ switchSideTab }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SideNavigationPage);
