import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { PATH } from "../commons/Constants";
import TestPage from "./TestPage";
import { Route, Redirect } from "react-router-dom";
import {
  getAssignedTests,
  updateAssignedTestId,
  resetAssignedTestState
} from "../../modules/AssignedTestsRedux";
import {
  activateTest,
  deactivateTest,
  lockTest,
  unlockTest,
  pauseTest,
  unpauseTest,
  invalidateTest,
  setPreviousTestStatus,
  getPreviousTestStatus,
  resetTestStatusState,
  getCurrentTestStatus
} from "../../modules/TestStatusRedux";
import { resetTestFactoryState } from "../../modules/TestSectionRedux";
import { resetInboxState } from "../../modules/EmailInboxRedux";
import { resetNotepadState } from "../../modules/NotepadRedux";
import { resetQuestionListState } from "../../modules/QuestionListRedux";
import { resetTopTabsState } from "../../modules/NavTabsRedux";
import TEST_STATUS from "../ta/Constants";
import { setErrorStatus } from "../../modules/ErrorStatusRedux";

class TestLanding extends Component {
  state = {
    isLoading: true,
    assignedTestId: null,
    testAccessCode: null,
    taActionTriggered: true,
    pollingState: undefined
  };

  componentDidMount = () => {
    this.checkForActiveAssignedTest();
    // setting up the polling function
    if (this.state.pollingState) {
      clearInterval(this.state.pollingState);
    }
    // getting assigned tests
    this.props.getAssignedTests(this.state.assignedTestId).then(assignedTests => {
      // looping in assigned tests
      for (let i = 0; i < assignedTests.length; i++) {
        // if current assigned test is PRE_TEST, ACTIVE, LOCKED or PAUSED
        if (
          assignedTests[i].status === TEST_STATUS.PRE_TEST ||
          assignedTests[i].status === TEST_STATUS.ACTIVE ||
          assignedTests[i].status === TEST_STATUS.LOCKED ||
          assignedTests[i].status === TEST_STATUS.PAUSED
        ) {
          let interval;
          // clearing existing polling interval(s)
          clearInterval(this.state.pollingState);
          // if supervised test
          if (
            assignedTests[i].uit_invite_id === null ||
            typeof assignedTests[i].uit_invite_id === "undefined"
          ) {
            // creating new polling interval (interval of 5 seconds)
            interval = setInterval(this.handleTestStatusUpdates, 5000);

            // if unsupervised test
          } else {
            // creating new polling interval (interval of 30 seconds)
            interval = setInterval(this.handleTestStatusUpdates, 30000);
          }
          this.setState({ pollingState: interval });
        }
      }
    });
  };

  componentWillUnmount = () => {
    clearInterval(this.state.pollingState);
  };

  handleCandidateKickout = () => {
    // invalidating test + redirecting to invalidated test page
    this.props.invalidateTest();
    this.resetAllRedux();
    this.props.history.push(PATH.invalidateTest);
  };

  // resetting all needed redux states
  resetAllRedux = () => {
    this.props.resetTestFactoryState();
    this.props.resetInboxState();
    this.props.resetNotepadState();
    this.props.resetAssignedTestState();
    this.props.resetQuestionListState();
    this.props.resetTopTabsState();
  };

  handleTestStatusUpdates = () => {
    // getting current test status
    this.props.getCurrentTestStatus(this.state.assignedTestId).then(testStatusData => {
      // test status is LOCKED, PAUSED, READY or ACTIVE
      if (
        (testStatusData.status === TEST_STATUS.LOCKED && !this.props.isTestLocked) ||
        (testStatusData.status === TEST_STATUS.PAUSED && !this.props.isTestPaused) ||
        testStatusData.status === TEST_STATUS.READY ||
        (testStatusData.status === TEST_STATUS.ACTIVE &&
          (this.props.isTestLocked || this.props.isTestPaused)) ||
        (testStatusData.status === TEST_STATUS.PRE_TEST &&
          (this.props.isTestLocked || this.props.isTestPaused))
      ) {
        // reloading the page
        window.location.reload();
        // test status is UNASSIGNED or QUIT
      } else if (
        testStatusData.status === TEST_STATUS.UNASSIGNED ||
        testStatusData.status === TEST_STATUS.QUIT
      ) {
        // if test has been invalidated
        if (testStatusData.is_invalid) {
          this.handleCandidateKickout();
        }
      }
    });
  };

  // checking for active assigned test (active or locked)
  checkForActiveAssignedTest = () => {
    this.setState({ isLoading: true }, () => {
      let nonStartedTestIndex = null;
      this.props
        .getAssignedTests()
        .then(response => {
          // initializing assignedTestsObj
          const assignedTestsObj = {
            active: false,
            activeAssignedTest: {},
            locked: false,
            lockedAssignedTest: {},
            paused: false,
            pausedAssignedTest: {},
            isUit: false
          };
          // checking for assigned tests
          for (let i = 0; i < response.length; i++) {
            // if test is active
            if (
              response[i].status === TEST_STATUS.PRE_TEST ||
              response[i].status === TEST_STATUS.ACTIVE
            ) {
              assignedTestsObj.active = true;
              assignedTestsObj.activeAssignedTest = response[i];
              // if test is locked
            } else if (response[i].status === TEST_STATUS.LOCKED) {
              assignedTestsObj.locked = true;
              assignedTestsObj.lockedAssignedTest = response[i];
              // if test is paused
            } else if (response[i].status === TEST_STATUS.PAUSED) {
              assignedTestsObj.paused = true;
              assignedTestsObj.pausedAssignedTest = response[i];
            }
            assignedTestsObj.isUit = response[i].uit_invite_id !== null;
            if (response[i].status === TEST_STATUS.READY) {
              // previous status is READY (meaning that the test was locked before the candidate started the test)
              if (this.props.previousStatus === TEST_STATUS.READY) {
                // setting nonStartedTestIndex
                nonStartedTestIndex = i;
              }
            }
            // that is the last assigned test + nonStartedTestIndex has been set
            if (i === response.length - 1 && nonStartedTestIndex >= 0) {
              // reset test status redux states
              this.props.resetTestStatusState();
            }
          }
          // there is an active/locked/paused test
          if (assignedTestsObj.active || assignedTestsObj.locked || assignedTestsObj.paused) {
            // getting previous status
            this.props
              .getPreviousTestStatus(
                assignedTestsObj.activeAssignedTest.id ||
                  assignedTestsObj.pausedAssignedTest.id ||
                  assignedTestsObj.lockedAssignedTest.id
              )
              .then(previousTestStatus => {
                // setting previous status
                this.props.setPreviousTestStatus(previousTestStatus);
              });
          }
          // there is an active test
          if (assignedTestsObj.active) {
            // activating test
            this.props.activateTest();
            // updating assigned test redux states
            this.props.updateAssignedTestId(
              assignedTestsObj.activeAssignedTest.id,
              assignedTestsObj.activeAssignedTest.test.id,
              assignedTestsObj.activeAssignedTest.accommodation_request
            );
            this.setState({
              assignedTestId: assignedTestsObj.activeAssignedTest.id,
              testAccessCode: assignedTestsObj.activeAssignedTest.test_access_code
            });

            // there is no active test, but a locked test
          } else if (assignedTestsObj.locked) {
            // locking test
            this.props.lockTest();
            // updating assigned test redux states
            this.props.updateAssignedTestId(
              assignedTestsObj.lockedAssignedTest.id,
              assignedTestsObj.lockedAssignedTest.test.id,
              assignedTestsObj.lockedAssignedTest.accommodation_request
            );
            this.setState({
              assignedTestId: assignedTestsObj.lockedAssignedTest.id,
              testAccessCode: assignedTestsObj.lockedAssignedTest.test_access_code
            });

            // there is no active or locked test, but a paused test
          } else if (assignedTestsObj.paused) {
            // pausing test
            this.props.pauseTest(
              assignedTestsObj.pausedAssignedTest.pause_test_time,
              assignedTestsObj.pausedAssignedTest.pause_start_date
            );
            // updating assigned test redux states
            this.props.updateAssignedTestId(
              assignedTestsObj.pausedAssignedTest.id,
              assignedTestsObj.pausedAssignedTest.test.id,
              assignedTestsObj.pausedAssignedTest.accommodation_request
            );
            this.setState({
              assignedTestId: assignedTestsObj.pausedAssignedTest.id,
              testAccessCode: assignedTestsObj.pausedAssignedTest.test_access_code
            });
          }
        })
        .then(() => {
          this.setState({ isLoading: false });
        });
    });
  };

  render() {
    return (
      <>
        {!this.state.isLoading && (
          <Route
            path={PATH.testBase}
            render={() =>
              this.props.isTestActive ? (
                <TestPage
                  assignedTestId={this.state.assignedTestId}
                  sampleTest={false}
                  testAccessCode={this.state.testAccessCode}
                  taActionTriggered={this.state.taActionTriggered}
                />
              ) : (
                <Redirect to={PATH.dashboard} />
              )
            }
          />
        )}
      </>
    );
  }
}
export { TestLanding as UnconnectedUITLanding };
const mapStateToProps = (state, ownProps) => {
  return {
    isTestActive: state.testStatus.isTestActive,
    isTestLocked: state.testStatus.isTestLocked,
    isTestPaused: state.testStatus.isTestPaused,
    previousStatus: state.testStatus.previousStatus,
    username: state.user.username,
    testSection: state.testSection.testSection
  };
};
const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAssignedTests,
      activateTest,
      deactivateTest,
      lockTest,
      unlockTest,
      pauseTest,
      unpauseTest,
      invalidateTest,
      setPreviousTestStatus,
      getPreviousTestStatus,
      resetTestStatusState,
      setErrorStatus,
      updateAssignedTestId,
      getCurrentTestStatus,
      resetTestFactoryState,
      resetInboxState,
      resetNotepadState,
      resetAssignedTestState,
      resetQuestionListState,
      resetTopTabsState
    },
    dispatch
  );
export default connect(mapStateToProps, mapDispatchToProps)(TestLanding);
