import React, { Component, useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import "../../css/inbox.css";
import {
  readQuestion,
  changeCurrentQuestion,
  answerQuestion,
  setStartTime,
  loadAnswers,
  setPreviousQuestionId,
  setSeenQuestionLoaded,
  markForReview,
  markForReviewDB,
  updateMultipleChoiceAnswersModifyDateDB,
  resetQuestionListState
} from "../../modules/QuestionListRedux";
import { loadInboxAnswers, resetInboxState } from "../../modules/EmailInboxRedux";
import LOCALIZE from "../../text_resources";
import {
  saveUitTestResponse,
  seenUitQuestion,
  getTestResponses,
  updateCandidateAnswerModifyDate,
  updateBackendAndDbStatusState
} from "../../modules/UpdateResponseRedux";

import QuestionPreviewFactory from "./QuestionPreviewFactory";
import QuestionContentFactory from "./QuestionContentFactory";
import { TestSectionComponentType } from "./Constants";
import { useTabState, Tab, TabList, TabPanel } from "reakit/Tab";
import { changeQuestion, handleAnswerClick } from "./MultipleChoiceQuestionListHelpers";
import { Col, Container, Row } from "react-bootstrap";
import getInTestHeightCalculations from "../../helpers/inTestHeightCalculations";
import {
  invalidateTest,
  getCurrentTestStatus,
  lockTest,
  unlockTest,
  pauseTest,
  unpauseTest
} from "../../modules/TestStatusRedux";
import { resetTestFactoryState, getUpdatedTestStartTime } from "../../modules/TestSectionRedux";
import { resetNotepadState } from "../../modules/NotepadRedux";
import { resetAssignedTestState } from "../../modules/AssignedTestsRedux";
import { resetTopTabsState } from "../../modules/NavTabsRedux";
import TEST_STATUS from "../ta/Constants";
import { PATH } from "../commons/Constants";
import ItemComponent from "../testBuilder/itemBank/items/ItemComponent";
import getItemContentToPreview, {
  getItemOptionsToPreview
} from "../../helpers/itemBankItemPreview";
import { getLanguageData, setLanguageData } from "../../modules/LocalizeRedux";
import ItemOptions from "../testBuilder/itemBank/items/ItemOptions";
import NextPreviousButtonNav from "../commons/NextPreviousButtonNav";

const styles = {
  rowContainer: {
    width: "100%"
  },
  menuTabStyle: {
    display: "block",
    width: "100%",
    backgroundColor: "white",
    padding: 0,
    border: "none"
  },
  questionListContainer: {
    paddingBottom: 12,
    paddingLeft: 0
  },
  questionContentContainer: {
    width: "auto",
    overflowY: "scroll"
  },
  containerWidth: {
    minWidth: "100%"
  },
  questionListStyle: {
    flexWrap: "wrap",
    width: "100%"
  },
  titlePadding: {
    padding: "16px 16px 0 16px"
  }
};

function getTablist(props, tab) {
  const { questions } = props;
  function changeQuestionCheck(question) {
    // if question has been changed
    if (Number(question.id) !== Number(props.currentQuestion)) {
      // changeQuestion(question.id, props);
      changeQuestion(question.id, props);
      // scrolling to top on question change action
      // getting number of top tabs elements
      const topTabsButtons = document.querySelectorAll("#navigation-items-section button");
      // looping in each buttons (top tabs elements)
      for (let i = 0; i < topTabsButtons.length; i++) {
        // getting top tab component div (scrollable div)
        const questionContentDiv = document.getElementById(`question-content-column`);
        // if defined
        if (questionContentDiv !== null) {
          // scroll to top
          questionContentDiv.scrollTop = 0;
        }
      }
    }
  }

  return (
    <>
      {questions.map((question, index) => {
        return (
          <Tab
            lang={props.currentLanguage}
            key={question.id}
            style={styles.menuTabStyle}
            {...tab}
            id={question.id}
            onFocus={() => {
              changeQuestionCheck(question);
            }}
          >
            <QuestionPreviewFactory
              index={index}
              question={question}
              questionId={question.id}
              currentQuestion={props.currentQuestion}
              type={props.questionType}
              isSelected={Number(tab.currentId) === Number(question.id)}
            />
          </Tab>
        );
      })}
    </>
  );
}

function getTabPanels(props, tab, componentFunctions) {
  const { questions } = props;

  async function answerClickHandler(questionId, answerId) {
    // calling async function
    setTimeout(() => {
      handleAnswerClick(answerId, questionId, props);
    }, 0);
  }

  function changeQuestionHandler(questionId) {
    changeQuestion(questionId, props);
    tab.move(questionId);
  }

  // getting current selected question
  const currentSelectedQuestion = questions.filter(obj => obj.id === tab.currentId)[0];
  // getting index of current selected question
  const questionIndex = questions.findIndex(obj => {
    return obj.id === tab.currentId;
  });

  // making sure that the currentSelectedQuestion is defined
  if (typeof currentSelectedQuestion !== "undefined") {
    // QUESTION LIST
    if (props.source === TestSectionComponentType.QUESTION_LIST) {
      // rendering only selected question data (way better performance wise)
      return (
        <>
          <QuestionContentFactory
            question={currentSelectedQuestion}
            questionId={currentSelectedQuestion.id}
            index={questionIndex}
            questionIndex={questionIndex}
            questions={questions}
            changeQuestion={changeQuestionHandler}
            type={props.questionType}
            addressBook={props.addressBook}
            handleAnswerClick={answerClickHandler}
            markForReview={componentFunctions.markForReview}
            lang={props.lang}
            calledInTestBuilder={props.calledInTestBuilder}
          />
        </>
      );
    }
    // getting and setting isMarkedForReview
    let isMarkedForReview = false;
    if (typeof props.questionSummaries[currentSelectedQuestion.id] !== "undefined") {
      isMarkedForReview = props.questionSummaries[currentSelectedQuestion.id].review || false;
    }

    // ITEM BANK ITEM
    return (
      <>
        <div
          id="unit-test-item-content-header"
          lang={props.lang}
          tabIndex={0}
          style={styles.titlePadding}
        >
          <h1 id={`question-${questionIndex + 1}`} lang={props.lang} className="notranslate">
            {LOCALIZE.formatString(LOCALIZE.mcTest.questionList.questionIdLong, questionIndex + 1)}
          </h1>
        </div>
        <div>
          {/* making sure that the language data has been loaded and set before rendering the item content */}
          {Object.entries(props.languageData).length > 0 && (
            <ItemComponent
              itemContent={getItemContentToPreview(
                questions[questionIndex],
                props.languageData,
                // hardcoding content-1 tab, since we only need to provide the regular content for now
                [{ key: "content-1" }],
                "content-1"
              )}
              index={questionIndex}
              languageCode={
                currentSelectedQuestion.item_bank_language_id === null
                  ? props.currentLanguage
                  : props.languageData.filter(
                      obj => obj.language_id === currentSelectedQuestion.item_bank_language_id
                    )[0].ISO_Code_1
              }
              calledFromQuestionPreview={false}
            />
          )}
        </div>
        <div>
          {/* making sure that the language data has been loaded and set before rendering the item content */}
          {Object.entries(props.languageData).length > 0 && (
            <ItemOptions
              itemOptions={getItemOptionsToPreview(
                questions[questionIndex],
                props.languageData,
                // hardcoding content-1 tab, since we only need to provide the regular content for now
                [{ key: "content-1" }],
                "content-1"
              )}
              languageCode={
                currentSelectedQuestion.item_bank_language_id === null
                  ? props.currentLanguage
                  : props.languageData.filter(
                      obj => obj.language_id === currentSelectedQuestion.item_bank_language_id
                    )[0].ISO_Code_1
              }
              handleAnswerClick={answerClickHandler}
              calledFromQuestionPreview={false}
            />
          )}
        </div>
        <div>
          <hr></hr>
          <NextPreviousButtonNav
            lang={props.lang}
            showNext={typeof questions[questionIndex + 1] !== "undefined"}
            showPrevious={typeof questions[questionIndex - 1] !== "undefined"}
            showReview={true}
            isMarkedForReview={isMarkedForReview}
            questionId={currentSelectedQuestion.id}
            onChangeToPrevious={() =>
              changeQuestionHandler(
                typeof questions[questionIndex - 1] !== "undefined"
                  ? questions[questionIndex - 1].id
                  : undefined
              )
            }
            onChangeToNext={() =>
              changeQuestionHandler(
                typeof questions[questionIndex + 1] !== "undefined"
                  ? questions[questionIndex + 1].id
                  : undefined
              )
            }
            onMarkForReview={() => componentFunctions.markForReview(currentSelectedQuestion.id)}
          />
        </div>
      </>
    );
  }
}

function QuestionListHooks(props) {
  const tab = useTabState({ orientation: "vertical", selectedId: props.currentQuestion });

  return function renderReakit(componentFunctions) {
    let { topTabsHeight } = componentFunctions.props;

    // add extra height if in test builder
    if (componentFunctions.props.calledInTestBuilder) {
      let heightOfBackToTestBuilderButton = 0;
      if (document.getElementById("back-to-test-builder-button") != null) {
        heightOfBackToTestBuilderButton = document.getElementById(
          "back-to-test-builder-button"
        ).offsetHeight;
      }
      topTabsHeight += heightOfBackToTestBuilderButton + 20; // height of "back to test builder button" + height of padding under button
    }

    const customHeight = getInTestHeightCalculations(
      componentFunctions.props.accommodations.fontSize,
      componentFunctions.props.accommodations.spacing,
      componentFunctions.props.testNavBarHeight,
      topTabsHeight,
      componentFunctions.props.testFooterHeight
    );

    let listStyle = {};
    let heightOfQuestionList = 0;
    if (document.getElementById("question-list") != null) {
      const questionListDiv = document.getElementById("question-list");
      // making sure that the "button" tag exists
      if (typeof questionListDiv.getElementsByTagName("button")[0] !== "undefined") {
        heightOfQuestionList =
          questionListDiv.getElementsByTagName("button")[0].offsetHeight *
          questionListDiv.getElementsByTagName("button").length;
      }
    }

    const heightOfTopTabsWindow =
      // 100vh - <calculated_value>
      // customHeight ==> { height: calc(100vh - <calculated_value>) }
      window.innerHeight - parseInt(customHeight.height.split("- ")[1].split("px")[0]);

    // Add scroll if the height of the question list is greater than the full height of the visible section (height of the top tabs window)
    if (heightOfQuestionList <= heightOfTopTabsWindow) {
      listStyle = {
        ...{
          height: heightOfQuestionList
        }
      };
    } else {
      listStyle = {
        ...{
          overflowY: "scroll"
        },
        ...customHeight
      };
    }

    return (
      <Container style={styles.containerWidth}>
        <Row>
          <Col sm={3} style={styles.questionListContainer}>
            <TabList
              {...tab}
              aria-label={LOCALIZE.ariaLabel.questionList}
              style={{ ...listStyle, ...styles.questionListStyle }}
              id={"question-list"}
            >
              {getTablist(props, tab)}
            </TabList>
          </Col>
          <Col
            id={"question-content-column"}
            sm={9}
            style={{ ...styles.questionContentContainer, ...customHeight }}
          >
            {getTabPanels(props, tab, componentFunctions)}
          </Col>
        </Row>
      </Container>
    );
  };
}

function withMyHook(Component) {
  return function WrappedComponent(props) {
    const myHookValue = QuestionListHooks(props);
    useEffect(() => {
      // make sure that the currentQuestion and the assignedTestId is defined (in order to avoid API call errors)
      if (props.currentQuestion !== null && props.assignedTestId) {
        // updating modify date of candidate multiple choice answers
        props.updateMultipleChoiceAnswersModifyDateDB(
          props.assignedTestId,
          props.currentQuestion,
          props.testSectionComponentId
        );
      }
      // disabling missing dependency warning
    }, [props.currentQuestion, props.initialSelectedQuestion]);
    return <Component {...props} myHookValue={myHookValue} />;
  };
}

class MultipleChoiceQuestionList extends Component {
  static propTypes = {
    calledInTestBuilder: PropTypes.bool
  };

  state = {
    previousQuestionId: null,
    seenQuestionLoaded: false,
    pollingState: undefined
  };

  // temporary comment to see effectiveness over time
  INBOX_HEIGHT = `800px`;

  componentDidMount = () => {
    // if assignedTestId is defined (real test)
    if (this.props.assignedTestId) {
      this.props
        .getTestResponses(this.props.assignedTestId, this.props.testSectionComponentId)
        .then(response => {
          if (response.ok) {
            this.loadAnswers(response);
          }
        });
      // if assignedTestId is not defined (sample test)
    } else {
      // getting current question based on currentQuestion redux state value (if undefined or null, currentQuestion is the defaultActiveKey)
      const currentQuestion =
        typeof this.props.currentQuestion === "undefined" || this.props.currentQuestion === null
          ? this.props.defaultActiveKey
          : this.props.currentQuestion;
      // getting index of initial question
      const indexOfInitialQuestion = this.props.questions.findIndex(obj => {
        return obj.id === currentQuestion;
      });
      changeQuestion(currentQuestion, this.props).then(() => {
        // making sure that the id exists to avoid errors
        if (document.getElementById(`unselected-question-preview-${indexOfInitialQuestion}`)) {
          // triggering click action on currentQuestion
          document.getElementById(`unselected-question-preview-${indexOfInitialQuestion}`).click();
        }
      });
    }
    this.props
      .getUpdatedTestStartTime(this.props.assignedTestId, this.props.testSection.id)
      .then(response => {
        // initializing startTime
        this.props.setStartTime(response);
      });

    // setting up the polling function
    if (this.state.pollingState) {
      clearInterval(this.state.pollingState);
    }
    // creating new polling interval that will call handleTestStatusValidation function every minute
    const interval = setInterval(this.handleTestStatusValidation, 60000);
    this.setState({ pollingState: interval });
    // languageData props is not defined yet
    if (Object.entries(this.props.languageData).length <= 0) {
      // getting language data
      this.props.getLanguageData().then(response => {
        // setting redux states
        this.props.setLanguageData(response);
      });
    }
  };

  componentWillUnmount = () => {
    clearInterval(this.state.pollingState);
  };

  // making sure that the current test status is the right one (if not, redirect the user accordingly)
  handleTestStatusValidation = () => {
    // if it's a real test
    if (this.props.assignedTestId !== null && this.props.assignedTestId !== undefined) {
      // getting current test status
      this.props.getCurrentTestStatus(this.props.assignedTestId).then(testStatusData => {
        // current section is a pre-test section
        if (this.props.testSection.default_time === null) {
          // expected test status = PRE_TEST
          if (
            testStatusData.status !== TEST_STATUS.PRE_TEST &&
            testStatusData.status !== TEST_STATUS.LOCKED &&
            testStatusData.status !== TEST_STATUS.PAUSED
          ) {
            // kicking candidate out
            // invalidating test + redirecting to invalidated test page
            this.props.invalidateTest();
            this.props.resetTestFactoryState();
            this.props.resetInboxState();
            this.props.resetNotepadState();
            this.props.resetAssignedTestState();
            this.props.resetQuestionListState();
            this.props.resetTopTabsState();
            window.location.href = PATH.invalidateTest;
            // current section is LOCKED
          } else if (testStatusData.status === TEST_STATUS.LOCKED) {
            // locking test (redux state)
            this.props.lockTest();
            // current section is PAUSED
          } else if (testStatusData.status === TEST_STATUS.PAUSED) {
            // pausing test (redux state)
            this.props.pauseTest();
            // reloading page (in order for the timer to be updated)
            window.location.reload();
          } else if (testStatusData.status === TEST_STATUS.PRE_TEST) {
            // unlocking/unpausing test (redux states)
            this.props.unlockTest();
            this.props.unpauseTest();
          }
        } // current section is a timed section
        else if (this.props.testSection.default_time !== null) {
          // expected test status = ACTIVE
          if (
            testStatusData.status !== TEST_STATUS.ACTIVE &&
            testStatusData.status !== TEST_STATUS.LOCKED &&
            testStatusData.status !== TEST_STATUS.PAUSED
          ) {
            // kicking candidate out
            // invalidating test + redirecting to invalidated test page
            this.props.invalidateTest();
            this.props.resetTestFactoryState();
            this.props.resetInboxState();
            this.props.resetNotepadState();
            this.props.resetAssignedTestState();
            this.props.resetQuestionListState();
            this.props.resetTopTabsState();
            window.location.href = PATH.invalidateTest;
            // current section is LOCKED
          } else if (testStatusData.status === TEST_STATUS.LOCKED) {
            // locking test (redux state)
            this.props.lockTest();
            // current section is PAUSED
          } else if (testStatusData.status === TEST_STATUS.PAUSED) {
            // pausing test (redux state)
            this.props.pauseTest();
            // reloading page (in order for the timer to be updated)
            window.location.reload();
          } else if (testStatusData.status === TEST_STATUS.ACTIVE) {
            // unlocking/unpausing test (redux states)
            this.props.unlockTest();
            this.props.unpauseTest();
          }
        }
      });
    }
  };

  loadAnswers = response => {
    switch (this.props.componentType) {
      case TestSectionComponentType.QUESTION_LIST || TestSectionComponentType.ITEM_BANK:
        this.props.loadAnswers(response.body[0].answers);
        break;
      case TestSectionComponentType.INBOX:
        this.props.loadInboxAnswers(response.body[0].answers);
        break;
      default:
        break;
    }
  };

  componentDidUpdate = prevProps => {
    // if seenQuestionLoaded gets updated
    if (prevProps.seenQuestionLoaded !== this.props.seenQuestionLoaded) {
      // getting test responses
      if (this.props.assignedTestId) {
        this.props
          .getTestResponses(this.props.assignedTestId, this.props.testSectionComponentId)
          .then(response => {
            // preventing errors (this condition should always be true)
            if (response.ok) {
              this.loadAnswers(response);
              // should never happen
            } else {
              // throw new Error("Something went wrong during getting answers process");
            }
          });
      }
    }
    if (prevProps.initialSelectedQuestion !== this.props.initialSelectedQuestion) {
      changeQuestion(this.props.initialSelectedQuestion, this.props).then(() => {
        // getting index of initial question
        const indexOfInitialQuestion = this.props.questions.findIndex(obj => {
          return obj.id === this.props.initialSelectedQuestion;
        });
        // making sure that the id exists to avoid errors
        if (document.getElementById(`unselected-question-preview-${indexOfInitialQuestion}`)) {
          // triggering click action on currentQuestion
          document.getElementById(`unselected-question-preview-${indexOfInitialQuestion}`).click();
        }
      });
    }
  };

  markForReview = async questionId => {
    const { assignedTestId } = this.props;
    const { testSectionComponentId } = this.props;

    // calling async function
    setTimeout(() => {
      this.props.markForReview(questionId);
      // if it's a real test
      if (this.props.assignedTestId !== null && this.props.assignedTestId !== undefined) {
        this.props.markForReviewDB(assignedTestId, questionId, testSectionComponentId);
      }
    }, 0);
  };

  render() {
    const { questionSummaries } = this.props;

    if (typeof questionSummaries === "undefined") return null;

    const { myHookValue } = this.props;

    const componentFunctions = {
      markForReview: this.markForReview,
      props: this.props
    };

    return <>{myHookValue(componentFunctions)}</>;
  }
}

export { MultipleChoiceQuestionList as UnconnectedMultipleChoiceQuestionList };
const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    languageData: state.localize.languageData,
    questionSummaries: state.questionList.questionSummaries,
    previousQuestionId: state.questionList.previousQuestionId,
    currentQuestion: state.questionList.currentQuestion,
    initialSelectedQuestion: state.questionList.initialSelectedQuestion,
    assignedTestId: state.assignedTest.assignedTestId,
    startTime: state.questionList.startTime,
    readOnly: state.testSection.readOnly,
    accommodations: state.accommodations,
    triggerClosingDialog: state.accommodations.triggerClosingDialog,
    triggerRerender: state.accommodations.triggerRerender,
    isNotepadHidden: state.notepad.isNotepadHidden,
    topTabsHeight: state.testSection.topTabsHeight,
    testNavBarHeight: state.testSection.testNavBarHeight,
    testFooterHeight: state.testSection.testFooterHeight,
    currentTopTab: state.navTabs.currentTopTab,
    testSection: state.testSection.testSection,
    newTestDefinition: state.testBuilder
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      readQuestion,
      changeCurrentQuestion,
      answerQuestion,
      saveUitTestResponse,
      getTestResponses,
      loadAnswers,
      setStartTime,
      seenUitQuestion,
      loadInboxAnswers,
      setPreviousQuestionId,
      setSeenQuestionLoaded,
      markForReview,
      markForReviewDB,
      updateMultipleChoiceAnswersModifyDateDB,
      getCurrentTestStatus,
      invalidateTest,
      resetTestFactoryState,
      resetInboxState,
      resetNotepadState,
      resetAssignedTestState,
      resetQuestionListState,
      resetTopTabsState,
      updateCandidateAnswerModifyDate,
      lockTest,
      unlockTest,
      pauseTest,
      unpauseTest,
      getUpdatedTestStartTime,
      getLanguageData,
      setLanguageData,
      updateBackendAndDbStatusState
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(withMyHook(MultipleChoiceQuestionList));
