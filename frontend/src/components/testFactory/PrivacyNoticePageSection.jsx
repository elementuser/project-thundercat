import React, { Component } from "react";
import PropTypes from "prop-types";
import LOCALIZE from "../../text_resources";

const styles = {
  privacyNoticeZone: {
    marginTop: 24
  },
  checkbox: {
    width: 18,
    height: 18,
    marginTop: 3
  }
};

class PrivacyNoticePageSection extends Component {
  static propTypes = {
    sectionComponentPage: PropTypes.object
  };

  state = {
    isButtonDisabled: false
  };

  componentDidMount = () => {
    this.props.setNextButtonDisabled(true);
    this.setState({ isButtonDisabled: true });
  };

  render() {
    const { setNextButtonDisabled } = this.props;

    return (
      <div>
        <div className="privacy-notice-grid" style={styles.privacyNoticeZone}>
          <div className="privacy-notice-grid-checkbox">
            <input
              // aria-invalid={!isValidPrivacyNotice && !isFirstLoad}
              aria-labelledby={"privacy-notice-error privacy-notice-description"}
              id="unit-test-privacy-notice-checkbox"
              type="checkbox"
              style={styles.checkbox}
              onChange={() => {
                setNextButtonDisabled(!this.state.isButtonDisabled);
                this.setState({ isButtonDisabled: !this.state.isButtonDisabled });
              }}
            />
          </div>
          <div className="privacy-notice-grid-description">
            <label id="unit-test-privacy-notice-description" htmlFor="privacy-notice-checkbox">
              {LOCALIZE.formatString(
                LOCALIZE.authentication.createAccount.privacyNotice,
                LOCALIZE.authentication.createAccount.privacyNoticeLink
              )}
            </label>
          </div>
        </div>
      </div>
    );
  }
}

export default PrivacyNoticePageSection;
