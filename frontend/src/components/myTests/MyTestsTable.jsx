import React, { Component } from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import GenericTable, { COMMON_STYLE } from "../commons/GenericTable";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import getRetestDate from "../../helpers/retestPeriodCalculations";
import getConvertedScore from "../../helpers/scoreConversion";
import { Row, Col } from "react-bootstrap";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 6,
    xl: 6
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 6,
    xl: 6
  }
};

const styles = {
  mainDiv: {
    marginTop: 12,
    textAlign: "center"
  },
  popupStyles: {
    itemContainer: {
      width: "100%",
      padding: "6px 0"
    },
    label: {
      verticalAlign: "middle",
      paddingRight: 18
    },
    value: {
      verticalAlign: "middle"
    },
    inputDisabled: {
      width: "100%",
      minHeight: 38,
      padding: "3px 6px 3px 6px",
      borderRadius: 4,
      wordBreak: "break-word"
    }
  }
};

class MyTestsTable extends Component {
  static propTypes = {
    currentlyLoading: PropTypes.bool.isRequired,
    rowsDefinition: PropTypes.object.isRequired,
    selectedTestDetails: PropTypes.object.isRequired,
    triggerPopup: PropTypes.bool.isRequired
  };

  state = {
    displayResultsDetailsPopup: false
  };

  componentDidUpdate = prevProps => {
    // if triggerPopup gets updated
    if (prevProps.triggerPopup !== this.props.triggerPopup) {
      this.handlePopupDisplay();
    }
  };

  // open popup
  handlePopupDisplay = () => {
    this.setState({ displayResultsDetailsPopup: true });
  };

  // close popup
  closePopup = () => {
    this.setState({ displayResultsDetailsPopup: false });
  };

  render() {
    // column definition
    const columnsDefinition = [
      {
        label: LOCALIZE.myTests.table.nameOfTest,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label: LOCALIZE.myTests.table.testDate,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.myTests.table.score,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.myTests.table.results,
        style: COMMON_STYLE.CENTERED_TEXT
      }
    ];

    return (
      <div style={styles.mainDiv}>
        <GenericTable
          classnamePrefix="my-tests"
          columnsDefinition={columnsDefinition}
          rowsDefinition={this.props.rowsDefinition}
          emptyTableMessage={LOCALIZE.myTests.table.noDataMessage}
          currentlyLoading={this.props.currentlyLoading}
        />
        {Object.keys(this.props.selectedTestDetails).length !== 0 && (
          <PopupBox
            show={this.state.displayResultsDetailsPopup}
            handleClose={this.closePopup}
            isBackdropStatic={true}
            shouldCloseOnEsc={true}
            title={LOCALIZE.myTests.table.viewResultsPopup.title}
            description={
              <div>
                <Row style={styles.popupStyles.itemContainer}>
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.popupStyles.label}
                  >
                    {LOCALIZE.myTests.table.viewResultsPopup.testLabel}
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.popupStyles.value}
                  >
                    <input
                      style={styles.popupStyles.inputDisabled}
                      className={"valid-field"}
                      disabled={true}
                      type="text"
                      value={this.props.selectedTestDetails.test.test_code}
                    />
                  </Col>
                </Row>
                <Row style={styles.popupStyles.itemContainer}>
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.popupStyles.label}
                  >
                    {LOCALIZE.myTests.table.viewResultsPopup.testDescriptionLabel}
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.popupStyles.value}
                  >
                    <textarea
                      style={styles.popupStyles.inputDisabled}
                      className={"valid-field"}
                      disabled={true}
                      type="text"
                      value={
                        this.props.selectedTestDetails.test[`${this.props.currentLanguage}_name`]
                      }
                    />
                  </Col>
                </Row>
                {/* <Row style={styles.popupStyles.itemContainer}>
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.popupStyles.label}
                  >
                    {LOCALIZE.myTests.table.viewResultsPopup.languageLabel}
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.popupStyles.value}
                  >
                    <input
                      style={styles.popupStyles.inputDisabled}
                      className={"valid-field"}
                      disabled={true}
                      type="text"
                      value={
                        this.props.selectedTestDetails.test_session_language === 1
                          ? LOCALIZE.commons.english
                          : LOCALIZE.commons.french
                      }
                    />
                  </Col>
                </Row> */}
                <Row style={styles.popupStyles.itemContainer}>
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.popupStyles.label}
                  >
                    {LOCALIZE.myTests.table.viewResultsPopup.testDateLabel}
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.popupStyles.value}
                  >
                    <input
                      style={styles.popupStyles.inputDisabled}
                      className={"valid-field"}
                      disabled={true}
                      type="text"
                      value={
                        this.props.selectedTestDetails.start_date !== null
                          ? this.props.selectedTestDetails.start_date.split("T")[0]
                          : LOCALIZE.commons.na
                      }
                    />
                  </Col>
                </Row>
                {/* <Row style={styles.popupStyles.itemContainer}>
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.popupStyles.label}
                  >
                    {LOCALIZE.myTests.table.viewResultsPopup.retestDateLabel}
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.popupStyles.value}
                  >
                    <input
                      style={styles.popupStyles.inputDisabled}
                      className={"valid-field"}
                      disabled={true}
                      type="text"
                      value={
                        this.props.selectedTestDetails.start_date !== null &&
                        parseInt(this.props.selectedTestDetails.test.retest_period) !== 0
                          ? getRetestDate(
                              this.props.selectedTestDetails.start_date,
                              parseInt(this.props.selectedTestDetails.test.retest_period)
                            )
                          : LOCALIZE.commons.na
                      }
                    />
                  </Col>
                </Row> */}
                {/* <Row style={styles.popupStyles.itemContainer}>
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.popupStyles.label}
                  >
                    {LOCALIZE.myTests.table.viewResultsPopup.retestPeriodLabel}
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.popupStyles.value}
                  >
                    <input
                      style={styles.popupStyles.inputDisabled}
                      className={"valid-field"}
                      disabled={true}
                      type="text"
                      value={
                        parseInt(this.props.selectedTestDetails.test.retest_period) !== 0
                          ? `${this.props.selectedTestDetails.test.retest_period} ${LOCALIZE.myTests.table.viewResultsPopup.retestPeriodUnits}`
                          : LOCALIZE.commons.na
                      }
                    />
                  </Col>
                </Row> */}
                <Row style={styles.popupStyles.itemContainer}>
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.popupStyles.label}
                  >
                    {LOCALIZE.myTests.table.viewResultsPopup.scoreLabel}
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.popupStyles.value}
                  >
                    <input
                      style={styles.popupStyles.inputDisabled}
                      className={"valid-field"}
                      disabled={true}
                      type="text"
                      value={
                        this.props.selectedTestDetails.is_invalid
                          ? LOCALIZE.commons.status.invalid
                          : getConvertedScore(
                              this.props.selectedTestDetails[
                                `${this.props.currentLanguage}_converted_score`
                              ],
                              this.props.selectedTestDetails.status
                            )
                      }
                    />
                  </Col>
                </Row>
              </div>
            }
            rightButtonType={BUTTON_TYPE.primary}
            rightButtonTitle={LOCALIZE.commons.ok}
            rightButtonAction={this.closePopup}
          />
        )}
      </div>
    );
  }
}

export { MyTestsTable as UnconnectedMyTestsTable };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(MyTestsTable));

export { styles as TEST_TABLE_STYLES };
