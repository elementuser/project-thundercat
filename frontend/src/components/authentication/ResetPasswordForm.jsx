import React, { Component } from "react";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import LOCALIZE from "../../text_resources";
import PasswordMinimumRequirements from "./PasswordMinimumRequirements";
import StyledTooltip, { TYPE, EFFECT, TRIGGER_TYPE } from "./StyledTooltip";
import { Button } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faQuestionCircle } from "@fortawesome/free-regular-svg-icons";
import CustomButton, { THEME } from "../commons/CustomButton";
import PASSWORD_REQUIREMENTS from "../../helpers/passwordRequirementsDefinition";
import { validatePassword, PASSWORD_REQUIREMENTS_REGEX } from "../../helpers/regexValidator";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";
import { setNewPassword } from "../../modules/PasswordResetRedux";
import { connect } from "react-redux";
import { faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import { PATH } from "../../SiteNavBar";
import { history } from "../../store-index";
import SystemMessage, { MESSAGE_TYPE } from "../commons/SystemMessage";

const styles = {
  resetPasswordContent: {
    padding: "12px 32px 0 32px",
    border: "1px solid #cdcdcd"
  },
  inputContainer: {
    position: "relative",
    display: "table-cell",
    width: "100%"
  },
  inputTitle: {
    padding: "12px 0 6px 0",
    fontWeight: "bold"
  },
  inputs: {
    width: "100%",
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565E",
    borderRadius: "4px 0 0 4px"
  },
  resetBtn: {
    display: "block",
    margin: "24px auto"
  },
  validIconForInputField: {
    color: "#278400",
    position: "absolute",
    right: 0,
    top: "12%"
  },
  validIconMarginForOtherField: {
    marginRight: 9
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6
  },
  passwordRequirementsError: {
    color: "#923534",
    marginTop: 6,
    fontWeight: "bold"
  },
  passwordRequirementsForScreenReader: {
    display: "none"
  },
  tooltipIconContainer: {
    padding: "0 6px"
  },
  tooltipIcon: {
    color: "#00565e"
  },
  passwordVisilibityContainer: {
    display: "table-cell"
  },
  passwordVisilibity: {
    color: "#00565e",
    border: "1px solid #00565e",
    borderRadius: "0 4px 4px 0",
    background: "white",
    borderLeft: "none",
    borderWidth: 1,
    borderColor: "#00565e",
    minWidth: "inherite",
    padding: "3px 6px"
  },
  passwordVisilibityInvalid: {
    borderColor: "#923534",
    borderWidth: 3
  }
};
class ResetPasswordForm extends Component {
  static props = {
    passwordResetToken: PropTypes.string.isRequired,
    // provided by redux
    setNewPassword: PropTypes.func
  };

  state = {
    // Ensures no errors are shown on page load
    isFirstLoad: true,

    // Field Content States
    passwordContent: "",
    passwordConfirmationContent: "",

    // Field Validation States
    isValidPassword: false,
    isValidPasswordConfirmation: false,
    isFirstPasswordLoad: true,
    viewNewPassword: false,
    newPasswordInputType: "password",
    viewNewPasswordConfirmation: false,
    newPasswordConfirmationInputType: "password",
    showPasswordResetSuccessfulConfirmationPopup: false,

    // Password Requirements States
    atLeastOneUppercase: false,
    atLeastOneLowercase: false,
    atLeastOneDigit: false,
    atLeastOneSpecialChar: false,
    betweenMinAndMaxChar: false,

    // API Errors Handler States
    passwordTooCommonError: false,
    passwordTooSimilarToUsernameError: false,
    passwordTooSimilarToFirstNameError: false,
    passwordTooSimilarToLastNameError: false,
    passwordTooSimilarToEmailError: false
  };

  getPasswordContent = event => {
    const passwordContent = event.target.value;
    this.setState({
      passwordContent: passwordContent
    });
  };

  getPasswordConfirmationContent = event => {
    const passwordConfirmationContent = event.target.value;
    this.setState({
      passwordConfirmationContent: passwordConfirmationContent
    });
  };

  // returns password requirements on password field selection for the screen reader users
  getPasswordRequirements = () => {
    // only on first load, since the dynamic password requirements are handling that after the first page load
    if (this.state.isFirstLoad) {
      return (
        <span id="password-requirements" style={styles.passwordRequirementsForScreenReader}>
          {LOCALIZE.authentication.createAccount.content.inputs.passwordErrors.description +
            LOCALIZE.authentication.createAccount.content.inputs.passwordErrors.upperCase +
            LOCALIZE.authentication.createAccount.content.inputs.passwordErrors.lowerCase +
            LOCALIZE.authentication.createAccount.content.inputs.passwordErrors.digit +
            LOCALIZE.authentication.createAccount.content.inputs.passwordErrors.specialCharacter +
            LOCALIZE.authentication.createAccount.content.inputs.passwordErrors.length}
        </span>
      );
    }
  };

  handlePasswordErrors = password => {
    // password too common error
    if (password.indexOf(PASSWORD_REQUIREMENTS.passwordTooCommon) >= 0) {
      this.setState({ passwordTooCommonError: true, isValidPassword: false });
    }

    // password too similar to first name error
    if (password.indexOf(PASSWORD_REQUIREMENTS.passwordTooSimilarToFirstName) >= 0) {
      this.setState({ passwordTooSimilarToFirstNameError: true, isValidPassword: false });
    }

    // password too similar to last name error
    if (password.indexOf(PASSWORD_REQUIREMENTS.passwordTooSimilarToLastName) >= 0) {
      this.setState({ passwordTooSimilarToLastNameError: true, isValidPassword: false });
    }

    // password too similar to email error (SHOULD NEVER HAPPEN, SINCE THE USERNAME IS THE EMAIL)
    if (password.indexOf(PASSWORD_REQUIREMENTS.passwordTooSimilarToEmail) >= 0) {
      this.setState({ passwordTooSimilarToEmailError: true, isValidPassword: false });
    }

    // password too similar to username error
    if (password.indexOf(PASSWORD_REQUIREMENTS.passwordTooSimilarToUsername) >= 0) {
      this.setState({ passwordTooSimilarToUsernameError: true, isValidPassword: false });
    }

    // focus on password field
    document.getElementById("password-field").focus();
  };

  validateForm = () => {
    this.resetPasswordRequirementsStates();
    const { passwordContent, passwordConfirmationContent } = this.state;

    const passwordErrorsArray = validatePassword(this.state.passwordContent);
    let isValidPassword = false;

    // checking the password validity
    if (passwordErrorsArray.length === 0) {
      isValidPassword = true;
    } else {
      this.findMissingPasswordRequirements(passwordErrorsArray);
      isValidPassword = false;
    }

    this.setState({
      isFirstLoad: false,
      isFirstPasswordLoad: false,
      passwordTooCommonError: false,
      passwordTooSimilarToUsernameError: false,
      passwordTooSimilarToFirstNameError: false,
      passwordTooSimilarToLastNameError: false,
      passwordTooSimilarToEmailError: false,
      isValidPassword: isValidPassword,
      isValidPasswordConfirmation: passwordContent === passwordConfirmationContent
    });
  };

  // resetting all password requirements states to true
  resetPasswordRequirementsStates = () => {
    this.setState({
      atLeastOneUppercase: true,
      atLeastOneLowercase: true,
      atLeastOneDigit: true,
      atLeastOneSpecialChar: true,
      betweenMinAndMaxChar: true
    });
  };

  // checking password requirements that are not satisfied
  findMissingPasswordRequirements = passwordErrorsArray => {
    // using indexOf instead of includes, since IE is not compatible with it
    const indexOfUppercase = passwordErrorsArray.indexOf(PASSWORD_REQUIREMENTS_REGEX.UPPERCASE);
    if (indexOfUppercase >= 0) {
      this.setState({ atLeastOneUppercase: false });
    }
    const indexOfLowercase = passwordErrorsArray.indexOf(PASSWORD_REQUIREMENTS_REGEX.LOWERCASE);
    if (indexOfLowercase >= 0) {
      this.setState({ atLeastOneLowercase: false });
    }
    const indexOfDigit = passwordErrorsArray.indexOf(PASSWORD_REQUIREMENTS_REGEX.DIGIT);
    if (indexOfDigit >= 0) {
      this.setState({ atLeastOneDigit: false });
    }
    const indexOfSpecialChar = passwordErrorsArray.indexOf(
      PASSWORD_REQUIREMENTS_REGEX.SPECIAL_CHARS
    );
    if (indexOfSpecialChar >= 0) {
      this.setState({ atLeastOneSpecialChar: false });
    }
    const indexOfNumberOfChars = passwordErrorsArray.indexOf(
      PASSWORD_REQUIREMENTS_REGEX.NUMBER_OF_CHARS
    );
    if (indexOfNumberOfChars >= 0) {
      this.setState({ betweenMinAndMaxChar: false });
    }
  };

  // checks if all fields are valid
  isFormValid = () => {
    return this.state.isValidPassword && this.state.isValidPasswordConfirmation;
  };

  // analyses field by field and focus on the highest error field
  focusOnHighestErrorField = () => {
    if (!this.state.isValidPassword) {
      document.getElementById("password-field").focus();
    } else if (!this.state.isValidPasswordConfirmation) {
      document.getElementById("password-confirmation-field").focus();
    }
  };

  // toggle state to display or not the new password
  toggleNewPasswordView = () => {
    this.setState({ viewNewPassword: !this.state.viewNewPassword }, () => {
      if (this.state.viewNewPassword) {
        this.setState({ newPasswordInputType: "text" });
      } else {
        this.setState({ newPasswordInputType: "password" });
      }
    });
  };

  // toggle state to display or not the confirm password
  toggleConfirmPasswordView = () => {
    this.setState({ viewNewPasswordConfirmation: !this.state.viewNewPasswordConfirmation }, () => {
      if (this.state.viewNewPasswordConfirmation) {
        this.setState({ newPasswordConfirmationInputType: "text" });
      } else {
        this.setState({ newPasswordConfirmationInputType: "password" });
      }
    });
  };

  // openPasswordResetSuccessfulConfirmationPopup = () => {
  //   this.setState({ showPasswordResetSuccessfulConfirmationPopup: true });
  // };

  handleSubmit = event => {
    const validForm = this.isFormValid();

    // TODO: This code has to updated once backend calls are updated
    if (validForm) {
      // update user's password
      const data = {
        token: this.props.passwordResetToken,
        password: this.state.passwordContent
      };
      this.props.setNewPassword(data).then(response => {
        // if response is OK
        if (response.status === "OK") {
          // open confirmation popup on successful update and set the state of password fields to empty
          this.setState({
            passwordContent: "",
            passwordConfirmationContent: "",
            showPasswordResetSuccessfulConfirmationPopup: true
          });
          // there is a password error
        } else if (response.password) {
          // update respective password error state and focusing on password field
          this.handlePasswordErrors(response.password);
          // should never happen
        } else {
          throw new Error("Something went wrong during the password update process");
        }
      });
    } else {
      this.focusOnHighestErrorField();
    }
    event.preventDefault();
  };

  handleGoBackToLogin = () => {
    history.push(PATH.login);
  };

  render() {
    const {
      isFirstLoad,
      isFirstPasswordLoad,
      passwordContent,
      viewNewPassword,
      passwordConfirmationContent,
      viewNewPasswordConfirmation,
      newPasswordInputType,
      newPasswordConfirmationInputType,
      isValidPassword,
      isValidPasswordConfirmation,
      atLeastOneUppercase,
      atLeastOneLowercase,
      atLeastOneDigit,
      atLeastOneSpecialChar,
      betweenMinAndMaxChar,
      passwordTooCommonError,
      passwordTooSimilarToUsernameError,
      passwordTooSimilarToFirstNameError,
      passwordTooSimilarToLastNameError,
      passwordTooSimilarToEmailError
    } = this.state;

    const validFieldClass = "valid-field";
    const invalidFieldClass = "invalid-field invalid-password-field";

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <div role="form">
        <div style={styles.resetPasswordContent}>
          <h2>{LOCALIZE.authentication.resetPassword.content.title}</h2>
          <form onSubmit={this.handleSubmit}>
            <div>
              <div style={styles.inputTitle}>
                <label id="password-title">
                  {LOCALIZE.authentication.resetPassword.content.newPassword}
                </label>
                {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
                <label className="visually-hidden" id="password-tooltip-text">
                  <PasswordMinimumRequirements />
                </label>
                <StyledTooltip
                  id="password-tooltip"
                  place="right"
                  type={TYPE.light}
                  effect={EFFECT.solid}
                  triggerType={[TRIGGER_TYPE.hover]}
                  tooltipElement={
                    <Button
                      data-tip=""
                      data-for="password-tooltip"
                      tabIndex="-1"
                      style={{
                        ...styles.tooltipIconContainer,
                        ...accommodationsStyle,
                        fontSize: parseInt(this.props.accommodations.fontSize.split("px")[0]) * 1.25
                      }}
                      variant="link"
                    >
                      <FontAwesomeIcon
                        icon={faQuestionCircle}
                        style={styles.tooltipIcon}
                      ></FontAwesomeIcon>
                    </Button>
                  }
                  tooltipContent={
                    <span>
                      <PasswordMinimumRequirements />
                    </span>
                  }
                />
              </div>

              <div style={styles.inputContainer}>
                {isValidPassword && (
                  <div
                    style={{
                      ...styles.validIconForInputField,
                      ...styles.validIconMarginForOtherField
                    }}
                  ></div>
                )}
                <input
                  className={
                    (isValidPassword &&
                      !passwordTooCommonError &&
                      !passwordTooSimilarToUsernameError &&
                      !passwordTooSimilarToFirstNameError &&
                      !passwordTooSimilarToLastNameError &&
                      !passwordTooSimilarToEmailError) ||
                    isFirstLoad
                      ? validFieldClass
                      : invalidFieldClass
                  }
                  aria-labelledby={
                    "password-title password-errors password-too-common-error password-too-similar-to-username password-too-similar-to-first-name password-too-similar-to-last-name password-too-similar-to-email password-tooltip-text"
                  }
                  aria-invalid={!isValidPassword && !isFirstLoad}
                  aria-required={"true"}
                  id="password-field"
                  type={newPasswordInputType}
                  value={passwordContent}
                  style={{ ...styles.inputs, ...accommodationsStyle }}
                  onChange={this.getPasswordContent}
                />
              </div>
              <div style={styles.passwordVisilibityContainer}>
                <CustomButton
                  label={<FontAwesomeIcon icon={viewNewPassword ? faEye : faEyeSlash} />}
                  ariaLabel={LOCALIZE.authentication.resetPassword.content.showNewPassword}
                  ariaPressed={viewNewPassword}
                  action={this.toggleNewPasswordView}
                  type={"button"}
                  customStyle={
                    (isValidPassword &&
                      !passwordTooCommonError &&
                      !passwordTooSimilarToUsernameError &&
                      !passwordTooSimilarToFirstNameError &&
                      !passwordTooSimilarToLastNameError &&
                      !passwordTooSimilarToEmailError) ||
                    isFirstLoad
                      ? styles.passwordVisilibity
                      : { ...styles.passwordVisilibity, ...styles.passwordVisilibityInvalid }
                  }
                  buttonTheme={THEME.secondary}
                />
              </div>
              {this.getPasswordRequirements()}
              {!isValidPassword &&
                !isFirstPasswordLoad &&
                !passwordTooCommonError &&
                !passwordTooSimilarToUsernameError &&
                !passwordTooSimilarToFirstNameError &&
                !passwordTooSimilarToLastNameError &&
                !passwordTooSimilarToEmailError && (
                  <label id="password-errors" htmlFor={"password-field"} className="notranslate">
                    <p style={styles.errorMessage}>
                      {
                        LOCALIZE.authentication.createAccount.content.inputs.passwordErrors
                          .description
                      }
                    </p>
                    <ul style={styles.passwordRequirementsError}>
                      {!atLeastOneUppercase && (
                        <li>
                          {
                            LOCALIZE.authentication.createAccount.content.inputs.passwordErrors
                              .upperCase
                          }
                        </li>
                      )}
                      {!atLeastOneLowercase && (
                        <li>
                          {
                            LOCALIZE.authentication.createAccount.content.inputs.passwordErrors
                              .lowerCase
                          }
                        </li>
                      )}
                      {!atLeastOneDigit && (
                        <li>
                          {
                            LOCALIZE.authentication.createAccount.content.inputs.passwordErrors
                              .digit
                          }
                        </li>
                      )}
                      {!atLeastOneSpecialChar && (
                        <li>
                          {
                            LOCALIZE.authentication.createAccount.content.inputs.passwordErrors
                              .specialCharacter
                          }
                        </li>
                      )}
                      {!betweenMinAndMaxChar && (
                        <li>
                          {
                            LOCALIZE.authentication.createAccount.content.inputs.passwordErrors
                              .length
                          }
                        </li>
                      )}
                    </ul>
                  </label>
                )}
              {passwordTooCommonError && (
                <label
                  id="password-too-common-error"
                  style={styles.errorMessage}
                  className="notranslate"
                >
                  {LOCALIZE.authentication.createAccount.passwordTooCommonError}
                </label>
              )}
              {passwordTooSimilarToUsernameError && (
                <label
                  id="password-too-similar-to-username"
                  style={styles.errorMessage}
                  className="notranslate"
                >
                  {LOCALIZE.authentication.createAccount.passwordTooSimilarToUsernameError}
                </label>
              )}
              {passwordTooSimilarToFirstNameError && (
                <label
                  id="password-too-similar-to-first-name"
                  style={styles.errorMessage}
                  className="notranslate"
                >
                  {LOCALIZE.authentication.createAccount.passwordTooSimilarToFirstNameError}
                </label>
              )}
              {passwordTooSimilarToLastNameError && (
                <label
                  id="password-too-similar-to-last-name"
                  style={styles.errorMessage}
                  className="notranslate"
                >
                  {LOCALIZE.authentication.createAccount.passwordTooSimilarToLastNameError}
                </label>
              )}
              {passwordTooSimilarToEmailError && (
                <label
                  id="password-too-similar-to-email"
                  style={styles.errorMessage}
                  className="notranslate"
                >
                  {LOCALIZE.authentication.createAccount.passwordTooSimilarToEmailError}
                </label>
              )}
            </div>
            <div>
              <div style={styles.inputTitle}>
                <label id="password-confirmation-title" className="notranslate">
                  {LOCALIZE.authentication.resetPassword.content.confirmNewPassword}
                </label>
              </div>
              <div style={styles.inputContainer}>
                {isValidPasswordConfirmation && (
                  <div
                    style={{
                      ...styles.validIconForInputField,
                      ...styles.validIconMarginForOtherField
                    }}
                  ></div>
                )}
                <input
                  className={
                    isValidPasswordConfirmation || isFirstLoad ? validFieldClass : invalidFieldClass
                  }
                  aria-invalid={!isValidPasswordConfirmation && !isFirstLoad}
                  aria-required={"true"}
                  aria-labelledby={"password-confirmation-title password-confirmation-error"}
                  id="password-confirmation-field"
                  type={newPasswordConfirmationInputType}
                  value={passwordConfirmationContent}
                  style={{ ...styles.inputs, ...accommodationsStyle }}
                  onChange={this.getPasswordConfirmationContent}
                />
              </div>
              <div style={styles.passwordVisilibityContainer}>
                <CustomButton
                  label={
                    <FontAwesomeIcon icon={viewNewPasswordConfirmation ? faEye : faEyeSlash} />
                  }
                  ariaLabel={LOCALIZE.authentication.resetPassword.content.showConfirmNewPassword}
                  ariaPressed={viewNewPasswordConfirmation}
                  action={this.toggleConfirmPasswordView}
                  type={"button"}
                  customStyle={
                    !isValidPasswordConfirmation && !isFirstPasswordLoad
                      ? { ...styles.passwordVisilibity, ...styles.passwordVisilibityInvalid }
                      : styles.passwordVisilibity
                  }
                  buttonTheme={THEME.secondary}
                />
              </div>
              {!isValidPasswordConfirmation && !isFirstPasswordLoad && (
                <label
                  id="password-confirmation-error"
                  style={styles.errorMessage}
                  className="notranslate"
                >
                  {LOCALIZE.authentication.createAccount.content.inputs.passwordConfirmationError}
                </label>
              )}
            </div>

            <CustomButton
              label={LOCALIZE.authentication.resetPassword.content.button}
              action={this.validateForm}
              type={"submit"}
              customStyle={styles.resetBtn}
              buttonTheme={THEME.PRIMARY}
            />
          </form>
        </div>

        <PopupBox
          show={this.state.showPasswordResetSuccessfulConfirmationPopup}
          handleClose={() => {}}
          isBackdropStatic={true}
          shouldCloseOnEsc={false}
          title={LOCALIZE.authentication.resetPassword.popUpBox.title}
          description={
            <div>
              <p>
                {
                  <SystemMessage
                    messageType={MESSAGE_TYPE.success}
                    title={LOCALIZE.commons.success}
                    message={LOCALIZE.authentication.resetPassword.popUpBox.description}
                  />
                }
              </p>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.ok}
          rightButtonAction={this.handleGoBackToLogin}
        />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setNewPassword
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ResetPasswordForm);
