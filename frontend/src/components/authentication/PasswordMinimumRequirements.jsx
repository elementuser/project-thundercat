import React, { Component } from "react";
import LOCALIZE from "../../text_resources";

const styles = {
  list: {
    marginBottom: 6
  }
};

class PasswordMinimumRequirements extends Component {
  render() {
    return (
      <div>
        <p>{LOCALIZE.authentication.createAccount.content.inputs.passwordErrors.description}</p>
        <ul style={styles.list}>
          <li>{LOCALIZE.authentication.createAccount.content.inputs.passwordErrors.upperCase}</li>

          <li>{LOCALIZE.authentication.createAccount.content.inputs.passwordErrors.lowerCase}</li>

          <li>{LOCALIZE.authentication.createAccount.content.inputs.passwordErrors.digit}</li>

          <li>
            {LOCALIZE.authentication.createAccount.content.inputs.passwordErrors.specialCharacter}
          </li>

          <li>{LOCALIZE.authentication.createAccount.content.inputs.passwordErrors.length}</li>
        </ul>
      </div>
    );
  }
}

export default PasswordMinimumRequirements;
