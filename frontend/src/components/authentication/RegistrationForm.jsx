import React, { Component } from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import LOCALIZE from "../../text_resources";
import validateName, {
  validateEmail,
  validatePSRSAppID,
  validatePassword,
  validatePri,
  validateMilitaryNbr,
  PASSWORD_REQUIREMENTS_REGEX
} from "../../helpers/regexValidator";
import PASSWORD_REQUIREMENTS from "../../helpers/passwordRequirementsDefinition";
import "../../css/registration-form.css";
import {
  registerAction,
  handleAuthResponseAndState,
  loginAction,
  updatePageHasErrorState,
  authenticateAction
} from "../../modules/LoginRedux";
import { connect } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheckCircle, faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons";
import { faQuestionCircle } from "@fortawesome/free-regular-svg-icons";
import { Button } from "react-bootstrap";
import PasswordMinimumRequirements from "./PasswordMinimumRequirements";
import DatePicker from "../commons/DatePicker";
import { setUserInformation, setIsUserLoading } from "../../modules/UserRedux";
import StyledTooltip, { TYPE, EFFECT, TRIGGER_TYPE } from "./StyledTooltip";
import CustomButton, { THEME } from "../commons/CustomButton";
import getCheckboxTransformScale from "../../helpers/checkboxTransformScale";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";
import PrivacyNoticeStatement from "../commons/PrivacyNoticeStatement";
import usernameFormatter from "../../helpers/usernameFormatter";

const styles = {
  createAccountContent: {
    padding: "12px 32px 0 32px",
    border: "1px solid #cdcdcd"
  },
  inputContainer: {
    position: "relative"
  },
  inputContainerForPassword: {
    position: "relative",
    display: "table-cell",
    width: "100%"
  },
  inputTitle: {
    padding: "12px 0 6px 0",
    fontWeight: "bold"
  },
  passwordLabel: {
    padding: "5px 0 0 0",
    fontWeight: "bold"
  },
  inputs: {
    width: "100%",
    padding: "3px 6px 3px 6px",
    borderRadius: 4
  },
  inputForNames: {
    width: 240,
    padding: "3px 6px 3px 6px",
    borderRadius: 4
  },
  inputForPasswords: {
    width: "100%",
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565E",
    borderRadius: "4px 0 0 4px"
  },
  validInputPadding: {
    padding: "3px 75px 3px 6px"
  },
  dobContainer: {
    display: "inline-block"
  },
  monthAndDayField: {
    width: 77,
    marginRight: 36,
    float: "left"
  },
  yearField: {
    width: 97,
    marginRight: 36,
    float: "left"
  },
  dobLabel: {
    padding: "3px 0 0 4px",
    margin: 0
  },
  checkMark: {
    marginRight: 6
  },
  validIconForInputField: {
    color: "#278400",
    position: "absolute",
    right: 0,
    top: "12%"
  },
  validIconMarginForFirstName: {
    marginRight: 20
  },
  validIconMarginForOtherField: {
    marginRight: 9
  },
  loginBtn: {
    display: "block",
    margin: "24px auto"
  },
  passwordRequirementsError: {
    color: "#923534",
    marginTop: 6,
    fontWeight: "bold"
  },
  passwordRequirementsForScreenReader: {
    display: "none"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6
  },
  privacyNoticeZone: {
    marginTop: 24
  },
  checkboxContainer: {
    textAlign: "center"
  },
  checkbox: {
    verticalAlign: "middle"
  },
  privacyNoticeLink: {
    textDecoration: "underline",
    color: "#0278A4",
    cursor: "pointer",
    backgroundColor: "transparent",
    border: "none",
    padding: 0
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  tooltipIcon: {
    color: "#00565e"
  },
  tooltipButton: {
    padding: "0 6px"
  },
  passwordVisilibityContainer: {
    display: "table-cell"
  },
  passwordVisilibity: {
    color: "#00565e",
    border: "1px solid #00565e",
    borderRadius: "0 4px 4px 0",
    background: "white",
    borderLeft: "none",
    borderWidth: 1,
    borderColor: "#00565e",
    minWidth: "inherite",
    padding: "3px 6px"
  },
  passwordVisilibityInvalid: {
    borderColor: "#923534",
    borderWidth: 3
  }
};

class RegistrationForm extends Component {
  static propTypes = {
    // Props from Redux
    registerAction: PropTypes.func,
    handleAuthResponseAndState: PropTypes.func,
    loginAction: PropTypes.func,
    updatePageHasErrorState: PropTypes.func,
    setUserInformation: PropTypes.func,
    setIsUserLoading: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.dateOfBirthDayFieldRef = React.createRef();
  }

  state = {
    // Ensures no errors are shown on page load
    isFirstLoad: true,

    // Field Content States
    firstNameContent: "",
    lastNameContent: "",
    psrsAppID: "",
    priContent: "",
    militaryNbrContent: "",
    emailContent: "",
    passwordContent: "",
    passwordConfirmationContent: "",

    // Field Validation States
    isValidFirstName: false,
    isValidLastName: false,
    triggerDateOfBirthValidation: false,
    isValidPSRSApplicantID: false,
    isValidEmail: false,
    isValidPri: false,
    isValidMilitaryNbr: false,
    isValidPassword: false,
    isFirstPasswordLoad: true,
    isValidPasswordConfirmation: false,
    isCheckboxChecked: false,
    isValidPrivacyNotice: false,
    viewPasswordContent: false,
    passwordInputType: "password",
    viewPasswordConfirmation: false,
    passwordConfirmationInputType: "password",

    // Password Requirements States
    atLeastOneUppercase: false,
    atLeastOneLowercase: false,
    atLeastOneDigit: false,
    atLeastOneSpecialChar: false,
    betweenMinAndMaxChar: false,

    // PopupBox States
    showPrivacyNoticeDialog: false,

    // API Errors Handler States
    accountExistsError: false,
    passwordTooCommonError: false,
    passwordTooSimilarToUsernameError: false,
    passwordTooSimilarToFirstNameError: false,
    passwordTooSimilarToLastNameError: false,
    passwordTooSimilarToEmailError: false
  };

  getFirstNameContent = event => {
    const firstNameContent = event.target.value;
    // allow maximum of 30 chars
    const regexExpression = /^(.{0,30})$/;
    if (regexExpression.test(firstNameContent)) {
      this.setState({
        firstNameContent: firstNameContent
      });
    }
  };

  getLastNameContent = event => {
    const lastNameContent = event.target.value;
    // allow maximum of 150 chars
    const regexExpression = /^(.{0,150})$/;
    if (regexExpression.test(lastNameContent)) {
      this.setState({
        lastNameContent: lastNameContent
      });
    }
  };

  getEmailContent = event => {
    const emailContent = event.target.value;
    this.setState({ emailContent: emailContent });
  };

  getPsrsAppID = event => {
    const psrsAppID = event.target.value;

    const regex = /^(([A-Za-z]{1})([0-9]{0,6}))$/;
    // string must contains 7 characters (1 letter and 6 numbers)
    if (psrsAppID === "" || regex.test(psrsAppID)) {
      this.setState({ psrsAppID: psrsAppID });
    }
  };

  getPriContent = event => {
    const priContent = event.target.value;

    /* only the following can be inserted into this field:
          - 0 to 9 numbers
    */
    const regex = /^([0-9]{0,9})$/;

    if (event.target.value === "" || regex.test(event.target.value)) {
      this.setState({
        priContent: priContent
      });
    }
  };

  getMilitaryNbrContent = event => {
    const militaryNbrContent = event.target.value;

    /* only the following can be inserted into this field:
          - 1 letter followed by 0 to 6 numbers
    */
    const regex = /^(([A-Za-z]{1})([0-9]{0,6}))$/;

    if (event.target.value === "" || regex.test(event.target.value)) {
      this.setState({
        militaryNbrContent: militaryNbrContent
      });
    }
  };

  getPasswordContent = event => {
    const passwordContent = event.target.value;
    this.setState({
      passwordContent: passwordContent
    });
  };

  getPasswordConfirmationContent = event => {
    const passwordConfirmationContent = event.target.value;
    this.setState({
      passwordConfirmationContent: passwordConfirmationContent
    });
  };

  // returns password requirements on password field selection for the screen reader users
  getPasswordRequirements = () => {
    // only on first load, since the dynamic password requirements are handling that after the first page load
    if (this.state.isFirstLoad) {
      return (
        <span id="password-requirements" style={styles.passwordRequirementsForScreenReader}>
          {LOCALIZE.authentication.createAccount.content.inputs.passwordErrors.description +
            LOCALIZE.authentication.createAccount.content.inputs.passwordErrors.upperCase +
            LOCALIZE.authentication.createAccount.content.inputs.passwordErrors.lowerCase +
            LOCALIZE.authentication.createAccount.content.inputs.passwordErrors.digit +
            LOCALIZE.authentication.createAccount.content.inputs.passwordErrors.specialCharacter +
            LOCALIZE.authentication.createAccount.content.inputs.passwordErrors.length}
        </span>
      );
    }
  };

  handleAccountAlreadyExistsError = response => {
    if (response.username[0] === "A user with that username already exists.") {
      this.setState({ accountExistsError: true, isValidEmail: false });
      // focus on email address field
      document.getElementById("email-address-field").focus();
    }
  };

  handlePasswordErrors = response => {
    // password too common error
    if (response.password.indexOf(PASSWORD_REQUIREMENTS.passwordTooCommon) >= 0) {
      this.setState({ passwordTooCommonError: true, isValidPassword: false });
    }

    // password too similar to first name error
    if (response.password.indexOf(PASSWORD_REQUIREMENTS.passwordTooSimilarToFirstName) >= 0) {
      this.setState({ passwordTooSimilarToFirstNameError: true, isValidPassword: false });
    }

    // password too similar to last name error
    if (response.password.indexOf(PASSWORD_REQUIREMENTS.passwordTooSimilarToLastName) >= 0) {
      this.setState({ passwordTooSimilarToLastNameError: true, isValidPassword: false });
    }

    // password too similar to email error (SHOULD NEVER HAPPEN, SINCE THE USERNAME IS THE EMAIL)
    if (response.password.indexOf(PASSWORD_REQUIREMENTS.passwordTooSimilarToEmail) >= 0) {
      this.setState({ passwordTooSimilarToEmailError: true, isValidPassword: false });
    }

    // password too similar to username error
    if (response.password.indexOf(PASSWORD_REQUIREMENTS.passwordTooSimilarToUsername) >= 0) {
      this.setState({ passwordTooSimilarToUsernameError: true, isValidPassword: false });
    }

    // focus on password field
    document.getElementById("password-field").focus();
  };

  validateForm = () => {
    this.resetPasswordRequirementsStates();
    const isValidFirstName = validateName(this.state.firstNameContent);
    const isValidLastName = validateName(this.state.lastNameContent);
    const isValidPSRSApplicantID = validatePSRSAppID(this.state.psrsAppID);
    const isValidEmail = validateEmail(this.state.emailContent);
    const isValidPri = validatePri(this.state.priContent);
    const isValidMilitaryNbr = validateMilitaryNbr(this.state.militaryNbrContent);
    const { passwordContent, passwordConfirmationContent } = this.state;
    const isValidPrivacyNotice = this.state.isCheckboxChecked;
    const passwordErrorsArray = validatePassword(this.state.passwordContent);
    let isValidPassword = false;

    // checking the password validity
    if (passwordErrorsArray.length === 0) {
      isValidPassword = true;
    } else {
      this.findMissingPasswordRequirements(passwordErrorsArray);
      isValidPassword = false;
    }

    this.setState({
      triggerDateOfBirthValidation: !this.state.triggerDateOfBirthValidation,
      isFirstLoad: false,
      isFirstPasswordLoad: false,
      accountExistsError: false,
      passwordTooCommonError: false,
      passwordTooSimilarToUsernameError: false,
      passwordTooSimilarToFirstNameError: false,
      passwordTooSimilarToLastNameError: false,
      passwordTooSimilarToEmailError: false,
      isValidFirstName: isValidFirstName,
      isValidLastName: isValidLastName,
      isValidPSRSApplicantID: isValidPSRSApplicantID,
      isValidEmail: isValidEmail,
      isValidPri: isValidPri,
      isValidMilitaryNbr: isValidMilitaryNbr,
      isValidPassword: isValidPassword,
      isValidPasswordConfirmation: passwordContent === passwordConfirmationContent,
      isValidPrivacyNotice: isValidPrivacyNotice
    });
  };

  // resetting all password requirements states to true
  resetPasswordRequirementsStates = () => {
    this.setState({
      atLeastOneUppercase: true,
      atLeastOneLowercase: true,
      atLeastOneDigit: true,
      atLeastOneSpecialChar: true,
      betweenMinAndMaxChar: true
    });
  };

  // checking password requirements that are not satisfied
  findMissingPasswordRequirements = passwordErrorsArray => {
    // using indexOf instead of includes, since IE is not compatible with it
    const indexOfUppercase = passwordErrorsArray.indexOf(PASSWORD_REQUIREMENTS_REGEX.UPPERCASE);
    if (indexOfUppercase >= 0) {
      this.setState({ atLeastOneUppercase: false });
    }
    const indexOfLowercase = passwordErrorsArray.indexOf(PASSWORD_REQUIREMENTS_REGEX.LOWERCASE);
    if (indexOfLowercase >= 0) {
      this.setState({ atLeastOneLowercase: false });
    }
    const indexOfDigit = passwordErrorsArray.indexOf(PASSWORD_REQUIREMENTS_REGEX.DIGIT);
    if (indexOfDigit >= 0) {
      this.setState({ atLeastOneDigit: false });
    }
    const indexOfSpecialChar = passwordErrorsArray.indexOf(
      PASSWORD_REQUIREMENTS_REGEX.SPECIAL_CHARS
    );
    if (indexOfSpecialChar >= 0) {
      this.setState({ atLeastOneSpecialChar: false });
    }
    const indexOfNumberOfChars = passwordErrorsArray.indexOf(
      PASSWORD_REQUIREMENTS_REGEX.NUMBER_OF_CHARS
    );
    if (indexOfNumberOfChars >= 0) {
      this.setState({ betweenMinAndMaxChar: false });
    }
  };

  // checks if all fields are valid
  isFormValid = () => {
    return (
      this.state.isValidFirstName &&
      this.state.isValidLastName &&
      this.props.completeDateValidState &&
      this.state.isValidPSRSApplicantID &&
      this.state.isValidEmail &&
      this.state.isValidPri &&
      this.state.isValidMilitaryNbr &&
      this.state.isValidPassword &&
      this.state.isValidPasswordConfirmation &&
      this.state.isValidPrivacyNotice
    );
  };

  // analyses field by field and focus on the highest error field
  focusOnHighestErrorField = () => {
    if (!this.state.isValidFirstName) {
      document.getElementById("first-name-field").focus();
    } else if (!this.state.isValidLastName) {
      document.getElementById("last-name-field").focus();
    } else if (!this.props.completeDateValidState) {
      this.dateOfBirthDayFieldRef.current.focus();
    } else if (!this.state.isValidPSRSApplicantID) {
      document.getElementById("psrs-applicant-id").focus();
    } else if (!this.state.isValidPri) {
      document.getElementById("pri-field").focus();
    } else if (!this.state.isValidMilitaryNbr) {
      document.getElementById("military-nbr-field").focus();
    } else if (!this.state.isValidEmail) {
      document.getElementById("email-address-field").focus();
    } else if (!this.state.isValidPassword) {
      document.getElementById("password-field").focus();
    } else if (!this.state.isValidPasswordConfirmation) {
      document.getElementById("password-confirmation-field").focus();
    } else if (!this.state.isValidPrivacyNotice) {
      document.getElementById("privacy-notice-checkbox").focus();
    }
  };

  // toggle state to display or not the password
  togglePasswordView = () => {
    this.setState({ viewPasswordContent: !this.state.viewPasswordContent }, () => {
      if (this.state.viewPasswordContent) {
        this.setState({ passwordInputType: "text" });
      } else {
        this.setState({ passwordInputType: "password" });
      }
    });
  };

  // toggle state to display or not the confirm password
  toggleConfirmPasswordView = () => {
    this.setState({ viewPasswordConfirmation: !this.state.viewPasswordConfirmation }, () => {
      if (this.state.viewPasswordConfirmation) {
        this.setState({ passwordConfirmationInputType: "text" });
      } else {
        this.setState({ passwordConfirmationInputType: "password" });
      }
    });
  };

  handleSubmit = event => {
    const validForm = this.isFormValid();
    let psrsAppID = null;
    if (
      this.state.psrsAppID !== null &&
      this.state.psrsAppID !== "" &&
      this.state.psrsAppID !== undefined
    ) {
      psrsAppID = this.state.psrsAppID.toUpperCase();
    }
    // if all fields are valid, execute API errors validation
    if (validForm) {
      this.props.setIsUserLoading(true);
      this.props
        .registerAction({
          first_name: this.state.firstNameContent,
          last_name: this.state.lastNameContent,
          birth_date: this.props.completeDatePicked,
          psrs_applicant_id: psrsAppID,
          email: this.state.emailContent.toLowerCase(),
          pri: this.state.priContent.toUpperCase(),
          military_service_nbr: this.state.militaryNbrContent.toUpperCase(),
          password: this.state.passwordContent,
          username: usernameFormatter(this.state.emailContent.toLowerCase())
        })
        // API errors validation
        .then(response => {
          // response returns email and username
          if (
            response.first_name === this.state.firstNameContent &&
            response.last_name === this.state.lastNameContent &&
            response.birth_date.length > 0 &&
            response.psrs_applicant_id === psrsAppID &&
            response.email === this.state.emailContent.toLowerCase() &&
            response.username === usernameFormatter(this.state.emailContent.toLowerCase())
          ) {
            // account successfully created
            this.setState({ accountExistsError: false });
            // login the user by using its credentials
            this.props.setUserInformation(
              response.first_name,
              response.last_name,
              usernameFormatter(response.username),
              false, // is staff
              "", // last password change
              response.psrs_applicant_id,
              response.email, // --
              "", // secondary email
              response.birth_date,
              this.state.priContent.toUpperCase(),
              this.state.militaryNbrContent.toUpperCase(),
              // set profileCompleted to false
              false
            );
            this.props
              .loginAction({
                username: usernameFormatter(this.state.emailContent.toLowerCase()),
                password: this.state.passwordContent
              })
              .then(response => {
                this.props.handleAuthResponseAndState(response).then(_ => {
                  this.props.updatePageHasErrorState(false);
                  this.props.authenticateAction(true);
                });
              });
          } else {
            this.props.updatePageHasErrorState(true);
          }
          // response gets username error(s)
          if (typeof response.username !== "undefined") {
            // account already exists error
            this.handleAccountAlreadyExistsError(response);
          }
          // response gets password error(s)
          if (typeof response.password !== "undefined") {
            // password too common error
            this.handlePasswordErrors(response);
          }
        });
    } else {
      this.props.updatePageHasErrorState(true);
      this.focusOnHighestErrorField();
    }
    event.preventDefault();
  };

  /*
    we need that 'event' to avoid checkbox status updates if clicking on
    the link in the privacy notice description
  */
  showPrivacyNoticePopup = event => {
    this.setState({ showPrivacyNoticeDialog: true });
    event.preventDefault();
  };

  closePrivacyNoticePopup = () => {
    this.setState({ showPrivacyNoticeDialog: false });
  };

  changeCheckboxStatus = () => {
    this.setState({ isCheckboxChecked: !this.state.isCheckboxChecked });
  };

  render() {
    const {
      isFirstLoad,
      firstNameContent,
      lastNameContent,
      psrsAppID,
      emailContent,
      priContent,
      militaryNbrContent,
      passwordContent,
      viewPasswordContent,
      passwordConfirmationContent,
      viewPasswordConfirmation,
      passwordInputType,
      passwordConfirmationInputType,
      isValidFirstName,
      isValidLastName,
      triggerDateOfBirthValidation,
      isValidPSRSApplicantID,
      isValidEmail,
      isValidPri,
      isValidMilitaryNbr,
      isValidPassword,
      isFirstPasswordLoad,
      isValidPasswordConfirmation,
      isValidPrivacyNotice,
      atLeastOneUppercase,
      atLeastOneLowercase,
      atLeastOneDigit,
      atLeastOneSpecialChar,
      betweenMinAndMaxChar,
      accountExistsError,
      passwordTooCommonError,
      passwordTooSimilarToUsernameError,
      passwordTooSimilarToFirstNameError,
      passwordTooSimilarToLastNameError,
      passwordTooSimilarToEmailError
    } = this.state;

    const validFieldClass = "valid-field";
    const invalidFieldClass = "invalid-field";
    const invalidFieldClassPassword = "invalid-field invalid-password-field";

    const checkboxTransformScale = getCheckboxTransformScale(this.props.accommodations.fontSize);

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <div>
        <div role="form">
          <div style={styles.createAccountContent}>
            <h2>{LOCALIZE.authentication.createAccount.content.title}</h2>
            <span>{LOCALIZE.authentication.createAccount.content.description}</span>
            <form onSubmit={this.handleSubmit}>
              <div>
                <div style={styles.inputTitle}>
                  <label id="first-name-title">
                    {LOCALIZE.authentication.createAccount.content.inputs.firstNameTitle}
                  </label>
                </div>
                <div style={styles.inputContainer}>
                  {isValidFirstName && (
                    <div
                      style={{
                        ...styles.validIconForInputField,
                        ...styles.validIconMarginForFirstName
                      }}
                    >
                      <FontAwesomeIcon style={styles.checkMark} icon={faCheckCircle} />
                      {LOCALIZE.authentication.createAccount.content.inputs.valid}
                    </div>
                  )}
                  <input
                    className={
                      isValidFirstName || isFirstLoad ? validFieldClass : invalidFieldClass
                    }
                    aria-labelledby={"first-name-title first-name-error"}
                    aria-invalid={!this.state.isValidFirstName && !isFirstLoad}
                    aria-required={"true"}
                    id="first-name-field"
                    type="text"
                    value={firstNameContent}
                    style={
                      isValidFirstName
                        ? {
                            ...styles.inputs,
                            ...styles.validInputPadding,
                            ...accommodationsStyle
                          }
                        : { ...styles.inputs, ...accommodationsStyle }
                    }
                    onChange={this.getFirstNameContent}
                  />
                </div>
                {!isValidFirstName && !isFirstLoad && (
                  <label id="first-name-error" style={styles.errorMessage} className="notranslate">
                    {LOCALIZE.authentication.createAccount.content.inputs.firstNameError}
                  </label>
                )}
              </div>
              <div>
                <div style={styles.inputTitle}>
                  <label id="last-name-title">
                    {LOCALIZE.authentication.createAccount.content.inputs.lastNameTitle}
                  </label>
                </div>
                <div style={styles.inputContainer}>
                  {isValidLastName && (
                    <div
                      style={{
                        ...styles.validIconForInputField,
                        ...styles.validIconMarginForOtherField
                      }}
                    >
                      <FontAwesomeIcon style={styles.checkMark} icon={faCheckCircle} />
                      {LOCALIZE.authentication.createAccount.content.inputs.valid}
                    </div>
                  )}
                  <input
                    className={isValidLastName || isFirstLoad ? validFieldClass : invalidFieldClass}
                    aria-labelledby={"last-name-title last-name-error"}
                    aria-invalid={!this.state.isValidLastName && !isFirstLoad}
                    aria-required={"true"}
                    id="last-name-field"
                    type="text"
                    value={lastNameContent}
                    style={
                      isValidLastName
                        ? {
                            ...styles.inputs,
                            ...styles.validInputPadding,
                            ...accommodationsStyle
                          }
                        : { ...styles.inputs, ...accommodationsStyle }
                    }
                    onChange={this.getLastNameContent}
                  />
                </div>
                {!isValidLastName && !isFirstLoad && (
                  <label id="last-name-error" style={styles.errorMessage} className="notranslate">
                    {LOCALIZE.authentication.createAccount.content.inputs.lastNameError}
                  </label>
                )}
              </div>
              <div>
                <div style={styles.inputTitle}>
                  <label id="dob-title">
                    {LOCALIZE.authentication.createAccount.content.inputs.dobDayTitle}
                  </label>
                </div>
                <DatePicker
                  dateDayFieldRef={this.dateOfBirthDayFieldRef}
                  dateLabelId={"dob-title"}
                  triggerValidation={triggerDateOfBirthValidation}
                />
              </div>
              <div>
                <div style={styles.inputTitle}>
                  <label htmlFor={"psrs-applicant-id"}>
                    {LOCALIZE.authentication.createAccount.content.inputs.psrsAppIdTitle}
                  </label>
                </div>
                <div style={styles.inputContainer}>
                  {isValidPSRSApplicantID && (
                    <div
                      style={{
                        ...styles.validIconForInputField,
                        ...styles.validIconMarginForOtherField
                      }}
                    >
                      <FontAwesomeIcon style={styles.checkMark} icon={faCheckCircle} />
                      {LOCALIZE.authentication.createAccount.content.inputs.valid}
                    </div>
                  )}
                  <input
                    className={
                      isValidPSRSApplicantID || isFirstLoad ? validFieldClass : invalidFieldClass
                    }
                    aria-invalid={!this.state.psrsAppID && !isFirstLoad}
                    aria-required={"false"}
                    id="psrs-applicant-id"
                    type="text"
                    value={psrsAppID}
                    style={{ ...styles.inputs, ...accommodationsStyle }}
                    onChange={this.getPsrsAppID}
                  />
                </div>
                {!isValidPSRSApplicantID && !isFirstLoad && (
                  <label
                    htmlFor={"psrs-applicant-id"}
                    style={styles.errorMessage}
                    className="notranslate"
                  >
                    {LOCALIZE.authentication.createAccount.content.inputs.psrsAppIdError}
                  </label>
                )}
              </div>
              <div>
                <div style={styles.inputTitle}>
                  <label htmlFor={"pri-field"}>
                    {LOCALIZE.authentication.createAccount.content.inputs.priTitle}
                  </label>
                </div>
                <div style={styles.inputContainer}>
                  {isValidPri && (
                    <div
                      style={{
                        ...styles.validIconForInputField,
                        ...styles.validIconMarginForOtherField
                      }}
                    >
                      <FontAwesomeIcon style={styles.checkMark} icon={faCheckCircle} />
                      {LOCALIZE.authentication.createAccount.content.inputs.valid}
                    </div>
                  )}
                  <input
                    className={isValidPri || isFirstLoad ? validFieldClass : invalidFieldClass}
                    aria-invalid={!this.state.isValidPri && !isFirstLoad}
                    aria-required={"false"}
                    id="pri-field"
                    type="text"
                    value={priContent}
                    style={{ ...styles.inputs, ...accommodationsStyle }}
                    onChange={this.getPriContent}
                  />
                </div>
                {!isValidPri && !isFirstLoad && (
                  <label htmlFor={"pri-field"} style={styles.errorMessage} className="notranslate">
                    {LOCALIZE.authentication.createAccount.content.inputs.priError}
                  </label>
                )}
              </div>
              <div>
                <div style={styles.inputTitle}>
                  <label htmlFor={"military-nbr-field"}>
                    {LOCALIZE.authentication.createAccount.content.inputs.militaryNbrTitle}
                  </label>
                </div>
                <div style={styles.inputContainer}>
                  {isValidMilitaryNbr && (
                    <div
                      style={{
                        ...styles.validIconForInputField,
                        ...styles.validIconMarginForOtherField
                      }}
                    >
                      <FontAwesomeIcon style={styles.checkMark} icon={faCheckCircle} />
                      {LOCALIZE.authentication.createAccount.content.inputs.valid}
                    </div>
                  )}
                  <input
                    className={
                      isValidMilitaryNbr || isFirstLoad ? validFieldClass : invalidFieldClass
                    }
                    aria-invalid={!this.state.isValidMilitaryNbr && !isFirstLoad}
                    aria-required={"false"}
                    id="military-nbr-field"
                    type="text"
                    value={militaryNbrContent}
                    style={{ ...styles.inputs, ...accommodationsStyle }}
                    onChange={this.getMilitaryNbrContent}
                  />
                </div>
                {!isValidMilitaryNbr && !isFirstLoad && (
                  <label
                    htmlFor={"military-nbr-field"}
                    style={styles.errorMessage}
                    className="notranslate"
                  >
                    {LOCALIZE.authentication.createAccount.content.inputs.militaryNbrError}
                  </label>
                )}
              </div>
              <div>
                <div style={styles.inputTitle}>
                  <label id="email-address-title">
                    {LOCALIZE.authentication.createAccount.content.inputs.emailTitle}
                  </label>
                </div>
                <div style={styles.inputContainer}>
                  {isValidEmail && (
                    <div
                      style={{
                        ...styles.validIconForInputField,
                        ...styles.validIconMarginForOtherField
                      }}
                    >
                      <FontAwesomeIcon style={styles.checkMark} icon={faCheckCircle} />
                      {LOCALIZE.authentication.createAccount.content.inputs.valid}
                    </div>
                  )}
                  <input
                    className={
                      (isValidEmail && !accountExistsError) || isFirstLoad
                        ? validFieldClass
                        : invalidFieldClass
                    }
                    aria-labelledby={
                      "email-address-title email-address-error email-address-account-exists-error"
                    }
                    aria-invalid={!this.state.isValidEmail && !isFirstLoad}
                    aria-required={"true"}
                    id="email-address-field"
                    type="text"
                    value={emailContent}
                    style={
                      isValidEmail
                        ? { ...styles.inputs, ...styles.validInputPadding, ...accommodationsStyle }
                        : { ...styles.inputs, ...accommodationsStyle }
                    }
                    onChange={this.getEmailContent}
                  />
                </div>
                {!isValidEmail && !isFirstLoad && !accountExistsError && (
                  <label
                    id="email-address-error"
                    style={styles.errorMessage}
                    className="notranslate"
                  >
                    {LOCALIZE.authentication.createAccount.content.inputs.emailError}
                  </label>
                )}
              </div>
              {accountExistsError && (
                <label
                  id="email-address-account-exists-error"
                  style={styles.errorMessage}
                  className="notranslate"
                >
                  {LOCALIZE.authentication.createAccount.accountAlreadyExistsError}
                </label>
              )}
              <div>
                <div style={styles.passwordLabel}>
                  <label id="password-title">
                    {LOCALIZE.authentication.createAccount.content.inputs.passwordTitle}
                  </label>
                  {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
                  <label className="visually-hidden" id="password-tooltip-text">
                    <PasswordMinimumRequirements />
                  </label>
                  <StyledTooltip
                    id="password-tooltip"
                    place="right"
                    type={TYPE.light}
                    effect={EFFECT.solid}
                    triggerType={[TRIGGER_TYPE.hover]}
                    tooltipElement={
                      <Button
                        data-tip=""
                        data-for="password-tooltip"
                        tabIndex="-1"
                        style={{
                          ...styles.tooltipButton,
                          ...{
                            fontSize:
                              parseInt(this.props.accommodations.fontSize.split("px")[0]) * 1.25
                          }
                        }}
                        variant="link"
                      >
                        <FontAwesomeIcon
                          icon={faQuestionCircle}
                          style={styles.tooltipIcon}
                        ></FontAwesomeIcon>
                      </Button>
                    }
                    tooltipContent={
                      <span>
                        <PasswordMinimumRequirements />
                      </span>
                    }
                  />
                </div>
                <div style={styles.inputContainerForPassword}>
                  {isValidPassword && (
                    <div
                      style={{
                        ...styles.validIconForInputField,
                        ...styles.validIconMarginForOtherField
                      }}
                    >
                      <FontAwesomeIcon style={styles.checkMark} icon={faCheckCircle} />
                      {LOCALIZE.authentication.createAccount.content.inputs.valid}
                    </div>
                  )}
                  <input
                    className={
                      (isValidPassword &&
                        !passwordTooCommonError &&
                        !passwordTooSimilarToUsernameError &&
                        !passwordTooSimilarToFirstNameError &&
                        !passwordTooSimilarToLastNameError &&
                        !passwordTooSimilarToEmailError) ||
                      isFirstLoad
                        ? validFieldClass
                        : invalidFieldClassPassword
                    }
                    aria-labelledby={
                      "password-title password-errors password-too-common-error password-too-similar-to-username password-too-similar-to-first-name password-too-similar-to-last-name password-too-similar-to-email password-tooltip-text"
                    }
                    aria-invalid={!isValidPassword && !isFirstLoad}
                    aria-required={"true"}
                    id="password-field"
                    type={passwordInputType}
                    value={passwordContent}
                    style={{ ...styles.inputForPasswords, ...accommodationsStyle }}
                    onChange={this.getPasswordContent}
                  />
                </div>
                <div style={styles.passwordVisilibityContainer}>
                  <CustomButton
                    label={<FontAwesomeIcon icon={viewPasswordContent ? faEye : faEyeSlash} />}
                    ariaLabel={LOCALIZE.authentication.createAccount.content.inputs.showPassword}
                    ariaPressed={viewPasswordContent}
                    action={this.togglePasswordView}
                    type={"button"}
                    customStyle={
                      (isValidPassword &&
                        !passwordTooCommonError &&
                        !passwordTooSimilarToUsernameError &&
                        !passwordTooSimilarToFirstNameError &&
                        !passwordTooSimilarToLastNameError &&
                        !passwordTooSimilarToEmailError) ||
                      isFirstLoad
                        ? styles.passwordVisilibity
                        : { ...styles.passwordVisilibity, ...styles.passwordVisilibityInvalid }
                    }
                    buttonTheme={THEME.secondary}
                  />
                </div>
                {this.getPasswordRequirements()}
                {!isValidPassword &&
                  !isFirstPasswordLoad &&
                  !passwordTooCommonError &&
                  !passwordTooSimilarToUsernameError &&
                  !passwordTooSimilarToFirstNameError &&
                  !passwordTooSimilarToLastNameError &&
                  !passwordTooSimilarToEmailError && (
                    <label id="password-errors" className="notranslate">
                      <p style={styles.errorMessage}>
                        {
                          LOCALIZE.authentication.createAccount.content.inputs.passwordErrors
                            .description
                        }
                      </p>
                      <ul style={styles.passwordRequirementsError}>
                        {!atLeastOneUppercase && (
                          <li>
                            {
                              LOCALIZE.authentication.createAccount.content.inputs.passwordErrors
                                .upperCase
                            }
                          </li>
                        )}
                        {!atLeastOneLowercase && (
                          <li>
                            {
                              LOCALIZE.authentication.createAccount.content.inputs.passwordErrors
                                .lowerCase
                            }
                          </li>
                        )}
                        {!atLeastOneDigit && (
                          <li>
                            {
                              LOCALIZE.authentication.createAccount.content.inputs.passwordErrors
                                .digit
                            }
                          </li>
                        )}
                        {!atLeastOneSpecialChar && (
                          <li>
                            {
                              LOCALIZE.authentication.createAccount.content.inputs.passwordErrors
                                .specialCharacter
                            }
                          </li>
                        )}
                        {!betweenMinAndMaxChar && (
                          <li>
                            {
                              LOCALIZE.authentication.createAccount.content.inputs.passwordErrors
                                .length
                            }
                          </li>
                        )}
                      </ul>
                    </label>
                  )}
                {passwordTooCommonError && (
                  <label
                    id="password-too-common-error"
                    style={styles.errorMessage}
                    className="notranslate"
                  >
                    {LOCALIZE.authentication.createAccount.passwordTooCommonError}
                  </label>
                )}
                {passwordTooSimilarToUsernameError && (
                  <label
                    id="password-too-similar-to-username"
                    style={styles.errorMessage}
                    className="notranslate"
                  >
                    {LOCALIZE.authentication.createAccount.passwordTooSimilarToUsernameError}
                  </label>
                )}
                {passwordTooSimilarToFirstNameError && (
                  <label
                    id="password-too-similar-to-first-name"
                    style={styles.errorMessage}
                    className="notranslate"
                  >
                    {LOCALIZE.authentication.createAccount.passwordTooSimilarToFirstNameError}
                  </label>
                )}
                {passwordTooSimilarToLastNameError && (
                  <label
                    id="password-too-similar-to-last-name"
                    style={styles.errorMessage}
                    className="notranslate"
                  >
                    {LOCALIZE.authentication.createAccount.passwordTooSimilarToLastNameError}
                  </label>
                )}
                {passwordTooSimilarToEmailError && (
                  <label
                    id="password-too-similar-to-email"
                    style={styles.errorMessage}
                    className="notranslate"
                  >
                    {LOCALIZE.authentication.createAccount.passwordTooSimilarToEmailError}
                  </label>
                )}
              </div>
              <div>
                <div style={styles.inputTitle}>
                  <label id="password-confirmation-title" className="notranslate">
                    {LOCALIZE.authentication.createAccount.content.inputs.passwordConfirmationTitle}
                  </label>
                </div>
                <div style={styles.inputContainerForPassword}>
                  {isValidPasswordConfirmation && (
                    <div
                      style={{
                        ...styles.validIconForInputField,
                        ...styles.validIconMarginForOtherField
                      }}
                    >
                      <FontAwesomeIcon style={styles.checkMark} icon={faCheckCircle} />
                      {LOCALIZE.authentication.createAccount.content.inputs.valid}
                    </div>
                  )}
                  <input
                    className={
                      isValidPasswordConfirmation || isFirstLoad
                        ? validFieldClass
                        : invalidFieldClassPassword
                    }
                    aria-invalid={!isValidPasswordConfirmation && !isFirstLoad}
                    aria-required={"true"}
                    aria-labelledby={"password-confirmation-title password-confirmation-error"}
                    id="password-confirmation-field"
                    type={passwordConfirmationInputType}
                    value={passwordConfirmationContent}
                    style={{ ...styles.inputForPasswords, ...accommodationsStyle }}
                    onChange={this.getPasswordConfirmationContent}
                  />
                </div>
                <div style={styles.passwordVisilibityContainer}>
                  <CustomButton
                    label={<FontAwesomeIcon icon={viewPasswordConfirmation ? faEye : faEyeSlash} />}
                    ariaLabel={
                      LOCALIZE.authentication.createAccount.content.inputs.showPasswordConfirmation
                    }
                    ariaPressed={viewPasswordConfirmation}
                    action={this.toggleConfirmPasswordView}
                    type={"button"}
                    customStyle={
                      !isValidPasswordConfirmation && !isFirstPasswordLoad
                        ? { ...styles.passwordVisilibity, ...styles.passwordVisilibityInvalid }
                        : styles.passwordVisilibity
                    }
                    buttonTheme={THEME.secondary}
                  />
                </div>
                {!isValidPasswordConfirmation && !isFirstPasswordLoad && (
                  <label
                    id="password-confirmation-error"
                    style={styles.errorMessage}
                    className="notranslate"
                  >
                    {LOCALIZE.authentication.createAccount.content.inputs.passwordConfirmationError}
                  </label>
                )}
              </div>
              <div className="privacy-notice-grid" style={styles.privacyNoticeZone}>
                <div className="privacy-notice-grid-checkbox" style={styles.checkboxContainer}>
                  <input
                    aria-invalid={!isValidPrivacyNotice && !isFirstLoad}
                    aria-labelledby={"privacy-notice-error privacy-notice-description"}
                    id="privacy-notice-checkbox"
                    type="checkbox"
                    style={{ ...styles.checkbox, ...{ transform: checkboxTransformScale } }}
                    onChange={this.changeCheckboxStatus}
                  />
                </div>
                <div className="privacy-notice-grid-description">
                  <label id="privacy-notice-description" htmlFor="privacy-notice-checkbox">
                    {LOCALIZE.formatString(
                      LOCALIZE.authentication.createAccount.privacyNotice,
                      <button
                        aria-label={LOCALIZE.authentication.createAccount.privacyNoticeLink}
                        tabIndex="0"
                        onClick={this.showPrivacyNoticePopup}
                        style={styles.privacyNoticeLink}
                      >
                        {LOCALIZE.authentication.createAccount.privacyNoticeLink}
                      </button>
                    )}
                  </label>
                </div>
              </div>
              {!isValidPrivacyNotice && !isFirstLoad && (
                <label
                  id="privacy-notice-error"
                  style={styles.errorMessage}
                  className="notranslate"
                >
                  {LOCALIZE.authentication.createAccount.privacyNoticeError}
                </label>
              )}
              <CustomButton
                label={LOCALIZE.authentication.createAccount.button}
                action={this.validateForm}
                customStyle={styles.loginBtn}
                type={"submit"}
                buttonTheme={THEME.PRIMARY}
              />
            </form>
          </div>
        </div>
        <PrivacyNoticeStatement
          showPopup={this.state.showPrivacyNoticeDialog}
          handleClose={this.closePrivacyNoticePopup}
        />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    completeDatePicked: state.datePicker.completeDatePicked,
    completeDateValidState: state.datePicker.completeDateValidState,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch => ({
  registerAction: data => dispatch(registerAction(data)),
  loginAction: data => dispatch(loginAction(data)),
  handleAuthResponseAndState: userData => dispatch(handleAuthResponseAndState(userData)),
  updatePageHasErrorState: bool => dispatch(updatePageHasErrorState(bool)),
  setUserInformation: (
    firstName,
    lastName,
    username,
    isSuperUser,
    lastPasswordChange,
    psrsAppID,
    primaryEmail,
    secondaryEmail,
    dateOfBirth,
    pri,
    militaryNbr
  ) =>
    dispatch(
      setUserInformation(
        firstName,
        lastName,
        username,
        isSuperUser,
        lastPasswordChange,
        psrsAppID,
        primaryEmail,
        secondaryEmail,
        dateOfBirth,
        pri,
        militaryNbr
      )
    ),
  setIsUserLoading: bool => dispatch(setIsUserLoading(bool)),
  authenticateAction: authentication => dispatch(authenticateAction(authentication)),
  dispatch
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(RegistrationForm));
