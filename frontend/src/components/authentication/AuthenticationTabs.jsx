import React, { Component } from "react";
import LoginForm from "./LoginForm";
import RegistrationForm from "./RegistrationForm";
import LOCALIZE from "../../text_resources";
import { Container } from "react-bootstrap";
import { useTabState, Tab, TabList, TabPanel } from "reakit/Tab";
import { connect } from "react-redux";

const styles = {
  row: {
    position: "relative"
  },
  bottomMargin: {
    marginBottom: 32
  },
  menuTabStyle: {
    backgroundColor: "#fafafa",
    padding: 8,
    border: "2px solid transparent",
    borderTopLeftRadius: "0.35rem",
    borderTopRightRadius: "0.35rem",
    borderBottomWidth: "4px",
    color: "#00565e",
    borderColor: "transparent transparent #00565e",
    marginBottom: "-2px"
  },
  menuTabInactive: {
    backgroundColor: "#fafafa",
    padding: 8,
    border: "none"
  },
  mainContainer: {
    maxWidth: "800px"
  }
};

function AuthenticationTabsHooks(props) {
  const tab = useTabState();
  return (
    <Container
      style={
        parseInt(props.accommodations.fontSize.split("px")[0]) < 30 ? styles.mainContainer : {}
      }
    >
      <TabList {...tab}>
        <Tab
          {...tab}
          id={"tab1"}
          style={tab.selectedId === "tab1" ? styles.menuTabStyle : styles.menuTabInactive}
        >
          {LOCALIZE.authentication.login.title}
        </Tab>
        <Tab
          {...tab}
          id={"tab2"}
          style={tab.selectedId === "tab2" ? styles.menuTabStyle : styles.menuTabInactive}
        >
          {LOCALIZE.authentication.createAccount.title}
        </Tab>
      </TabList>
      <TabPanel {...tab}>
        <LoginForm />
      </TabPanel>
      <TabPanel {...tab}>
        <RegistrationForm />
      </TabPanel>
    </Container>
  );
}

function withMyHook(Component) {
  return function WrappedComponent(props) {
    const myHookValue = AuthenticationTabsHooks(props);
    return <Component {...props} myHookValue={myHookValue} />;
  };
}

const mapStateToProps = (state, ownProps) => {
  return {
    accommodations: state.accommodations
  };
};

class AuthenticationTabs extends Component {
  render() {
    const { myHookValue } = this.props;

    return <>{myHookValue}</>;
  }
}

export default connect(mapStateToProps)(withMyHook(AuthenticationTabs));
