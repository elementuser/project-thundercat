import React, { Component } from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import LOCALIZE from "../../text_resources";
import {
  loginAction,
  handleAuthResponseAndState,
  updatePageHasErrorState,
  updateLastLoginField,
  getUserInformation,
  authenticateAction
} from "../../modules/LoginRedux";

import { setUserInformation, setIsUserLoading, setLastLoginDate } from "../../modules/UserRedux";
import { connect } from "react-redux";
import CustomButton, { THEME } from "../commons/CustomButton";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";
import { setTestDefinitionSelectionSideNavState } from "../../modules/TestBuilderRedux";
import { setBOUserSideNavState } from "../../modules/UserLookUpRedux";
import doesThisEmailExist, { sendPasswordResetEmail } from "../../modules/PasswordResetRedux";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes, faUndo, faEye, faEyeSlash, faSpinner } from "@fortawesome/free-solid-svg-icons";
import { validateEmail } from "../../helpers/regexValidator";
import { PATH } from "../commons/Constants";
import {
  getUserPermissions,
  updatePermissionsState,
  updateCurrentHomePageState
} from "../../modules/PermissionsRedux";
import { Row, Col } from "react-bootstrap";
import { PERMISSION } from "../profile/Constants";
import usernameFormatter from "../../helpers/usernameFormatter";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 6,
    xl: 6
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 6,
    xl: 6
  }
};

const styles = {
  loginContent: {
    padding: "12px 32px 0 32px",
    border: "1px solid #cdcdcd"
  },
  inputTitles: {
    fontWeight: "bold"
  },
  passwordContainer: {
    width: "100%",
    display: "table-cell"
  },
  passwordInput: {
    width: "100%",
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565E",
    borderRadius: "4px 0 0 4px"
  },
  inputs: {
    width: "100%",
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565E",
    borderRadius: 4
  },
  passwordVisilibityContainer: {
    display: "table-cell"
  },
  passwordVisilibity: {
    color: "#00565e",
    border: "1px solid #00565e",
    borderRadius: "0 4px 4px 0",
    background: "white",
    borderLeft: "none",
    minWidth: "inherite",
    padding: "3px 6px"
  },
  wrongCredentialsContainer: {
    display: "table-cell",
    verticalAlign: "top"
  },
  forgotPasswordContainer: {
    textAlign: "right",
    display: "table-cell",
    paddingTop: 2,
    verticalAlign: "top"
  },
  forgotPasswordButton: {
    background: "transparent",
    border: "none",
    color: "#0278A4",
    fontWeight: "inherit",
    padding: 4
  },
  loginBtn: {
    display: "block",
    margin: "24px auto"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6
  },
  successMessage: {
    color: "#749F41",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6
  },
  resetPasswordEmailAddressContainer: {
    width: "100%",
    padding: 12,
    margin: "0 auto"
  },
  resetPasswordEmailAddressLabel: {
    display: "table-cell",
    width: "40%",
    verticalAlign: "middle"
  },
  resetPasswordEmailAddressInput: {
    display: "table-cell",
    width: "60%",
    verticalAlign: "middle"
  },
  formRow: {
    padding: "15px 0 15px 0"
  }
};

class LoginForm extends Component {
  static propTypes = {
    // Props from Redux
    loginAction: PropTypes.func,
    handleAuthResponseAndState: PropTypes.func,
    setLoginState: PropTypes.func,
    authenticated: PropTypes.bool,
    updatePageHasErrorState: PropTypes.func,
    updateLastLoginField: PropTypes.func,
    getUserInformation: PropTypes.func,
    setUserInformation: PropTypes.func,
    setIsUserLoading: PropTypes.func,
    doesThisEmailExist: PropTypes.func,
    sendPasswordResetEmail: PropTypes.func
  };

  state = {
    username: "",
    password: "",
    viewPassword: false,
    passwordInputType: "password",
    wrongCredentials: false,
    APIError: {},
    showForgotPasswordPopup: false,
    resetPasswordEmailAddressContent: "",
    isValidForgotPasswordEmailAddress: true,
    isAnExistingEmailAddress: true,
    backendError: false,
    backendErrorContent: "",
    passwordResetSuccessful: false,
    currentlyLoading: false
  };

  handleGetUserPermissionsLogic = () => {
    // getting user permissions
    this.props
      .getUserPermissions()
      .then(response => {
        // initializing permission flags
        const { isSuperUser } = this.props;
        let isEtta = false;
        let isPpc = false;
        let isTa = false;
        let isScorer = false;
        let isTb = false;
        let isAae = false;
        let isRdOperations = false;
        let isTd = false;
        // specified user is a super user
        if (isSuperUser) {
          // setting all permission flags to true
          isEtta = true;
          isPpc = true;
          isTa = true;
          isScorer = true;
          isTb = true;
          isAae = true;
          isRdOperations = true;
          isTd = true;
          // // specified user is not a super user
        } else {
          // looping in user permissions to update the needed permission flags
          for (let i = 0; i < response.length; i++) {
            if (response[i].codename === PERMISSION.systemAdministrator) {
              isEtta = true;
            } else if (response[i].codename === PERMISSION.ppcAdministrator) {
              isPpc = true;
            } else if (response[i].codename === PERMISSION.scorer) {
              isScorer = true;
            } else if (response[i].codename === PERMISSION.testAdministrator) {
              isTa = true;
            } else if (response[i].codename === PERMISSION.testBuilder) {
              isTb = true;
            } else if (response[i].codename === PERMISSION.aae) {
              isAae = true;
            } else if (response[i].codename === PERMISSION.rdOperations) {
              isRdOperations = true;
            } else if (response[i].codename === PERMISSION.testDeveloper) {
              isTd = true;
            }
          }
        }
        // update permissions states based on the user permission flags
        this.props.updatePermissionsState({
          isEtta: isEtta,
          isPpc: isPpc,
          isTa: isTa,
          isScorer: isScorer,
          isTb: isTb,
          isAae: isAae,
          isRdOperations: isRdOperations,
          isTd: isTd
        });
        // update the current home page state based on the permissions
        // is super user or ETTA
        if (isEtta || isRdOperations || isSuperUser) {
          this.props.updateCurrentHomePageState(PATH.systemAdministration);
          // is PPC
        } else if (isPpc) {
          this.props.updateCurrentHomePageState(PATH.ppcAdministration);
          // is scorer
        } else if (isScorer) {
          this.props.updateCurrentHomePageState(PATH.scorerBase);
          // is TA
        } else if (isTa) {
          this.props.updateCurrentHomePageState(PATH.testAdministration);
          // is Test Builder
        } else if (isTb) {
          this.props.updateCurrentHomePageState(PATH.testBuilder);
          // is Test Developer
        } else if (isTd) {
          this.props.updateCurrentHomePageState(PATH.testBuilder);
          // is candidate
        } else {
          this.props.updateCurrentHomePageState(PATH.dashboard);
        }
      })
      .then(() => {
        // will also trigger the redirect to the dynamic home page
        this.props.authenticateAction(true);
      });
  };

  handleSubmit = event => {
    event.preventDefault();
    // set currentlyLoading to true (will disable the login button)
    this.setState({ currentlyLoading: true }, () => {
      // calling login action
      this.props
        .loginAction({
          username: this.state.username.toLowerCase(),
          password: this.state.password
        })
        .then(response => {
          // credentials are wrong
          if (response.non_field_errors || typeof response.access === "undefined") {
            this.setState({
              password: "",
              wrongCredentials: true,
              APIError: response,
              currentlyLoading: false
            });
            this.props.updatePageHasErrorState(true);
            // focus on password field
            document.getElementById("password").focus();
            // right authentication
          } else {
            // updating needed redux states
            this.props.setIsUserLoading(true);
            this.props.setLastLoginDate(response.last_login);
            this.props.updatePageHasErrorState(false);
            this.props.updateLastLoginField({
              username: usernameFormatter(this.state.username),
              password: this.state.password
            });
            // resetting side nav selected tab props
            this.props.setTestDefinitionSelectionSideNavState(0);
            this.props.setBOUserSideNavState(0);
            // removing wrongCredentials error message
            this.setState({ wrongCredentials: false });
            // handling local storage token updates
            this.props.handleAuthResponseAndState(response).then(_ => {
              // ==================== USER DATA ====================
              // getting user data
              this.props
                .getUserInformation()
                .then(response => {
                  // setting user data
                  this.props.setUserInformation(
                    response.first_name,
                    response.last_name,
                    usernameFormatter(response.username),
                    response.is_staff,
                    response.last_password_change,
                    response.psrs_applicant_id,
                    response.email,
                    response.secondary_email,
                    response.birth_date,
                    response.pri,
                    response.military_service_nbr,
                    response.is_profile_complete
                  );
                  return response;
                })
                .then(responseFromFetUserInformation => {
                  // ==================== USER PERMISSIONS ====================
                  // username and email are not the same (can happen for super users or users/emails with apostrophes)
                  if (
                    responseFromFetUserInformation.username !== responseFromFetUserInformation.email
                  ) {
                    // calling login action
                    this.props
                      .loginAction({
                        username: responseFromFetUserInformation.username,
                        password: this.state.password
                      })
                      .then(response => {
                        // handling local storage token updates
                        this.props.handleAuthResponseAndState(response).then(_ => {
                          this.handleGetUserPermissionsLogic();
                        });
                      });
                  } else {
                    this.handleGetUserPermissionsLogic();
                  }
                  // ==================== USER PERMISSIONS (END) ====================
                });
              // ==================== USER DATA (END) ====================
            });
          }
        });
    });
  };

  togglePasswordView = () => {
    this.setState({ viewPassword: !this.state.viewPassword }, () => {
      if (this.state.viewPassword) {
        this.setState({ passwordInputType: "text" });
      } else {
        this.setState({ passwordInputType: "password" });
      }
    });
  };

  handleUsernameChange = event => {
    this.setState({ username: event.target.value });
  };

  handlePasswordChange = event => {
    this.setState({ password: event.target.value });
  };

  handleOpenPopup = () => {
    this.setState({ showForgotPasswordPopup: true });
  };

  handleClosePopup = () => {
    this.setState({
      showForgotPasswordPopup: false,
      resetPasswordEmailAddressContent: "",
      isValidForgotPasswordEmailAddress: true,
      isAnExistingEmailAddress: true,
      backendError: false,
      backendErrorContent: "",
      passwordResetSuccessful: false
    });
  };

  getResetPasswordEmailContent = event => {
    const emailContent = event.target.value;
    this.setState({ resetPasswordEmailAddressContent: emailContent });
  };

  handleSendEmail = () => {
    // email validation (wording only)
    const isValidForgotPasswordEmailAddress = validateEmail(
      this.state.resetPasswordEmailAddressContent
    );

    // if invalid email address
    if (!isValidForgotPasswordEmailAddress) {
      // updating states
      this.setState(
        { isValidForgotPasswordEmailAddress: false, isAnExistingEmailAddress: true },
        () => {
          // focus on input field
          document.getElementById("reset-password-email-address").focus();
        }
      );
      // if valid email address
    } else {
      // making sure that the provided email address exists
      this.props
        .doesThisEmailExist(this.state.resetPasswordEmailAddressContent)
        .then(usernameExists => {
          if (!usernameExists) {
            // updating states
            this.setState(
              {
                isValidForgotPasswordEmailAddress: true,
                isAnExistingEmailAddress: false
              },
              () => {
                // focus on input field
                document.getElementById("reset-password-email-address").focus();
              }
            );
          } else {
            const data = {
              email: this.state.resetPasswordEmailAddressContent,
              current_language: this.props.currentLanguage
            };
            this.props.sendPasswordResetEmail(data).then(response => {
              // make sure that the response is OK
              if (response.status === "OK") {
                // update popup content (successfully sent email)
                this.setState(
                  {
                    isValidForgotPasswordEmailAddress: true,
                    isAnExistingEmailAddress: true,
                    backendError: false,
                    backendErrorContent: "",
                    passwordResetSuccessful: true
                  },
                  () => {
                    // disable input field
                    document.getElementById("reset-password-email-address").disabled = true;
                    // focus on success message
                    document.getElementById("email-sent-successfully").focus();
                  }
                );
                // handle error from the backend
              } else {
                this.setState({ backendError: true, backendErrorContent: response }, () => {
                  document.getElementById("maximum-amout-of-email-sent-error-message").focus();
                });
              }
            });
          }
        });
    }
  };

  render() {
    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }
    return (
      <div>
        {!this.props.authenticated && (
          <div role="form">
            <div style={styles.loginContent}>
              <Row>
                <Col xl={12} lg={12} md={12} sm={12}>
                  <h2>{LOCALIZE.authentication.login.content.title}</h2>
                </Col>
              </Row>
              <form onSubmit={this.handleSubmit}>
                <Row style={styles.formRow}>
                  <Col xl={12} lg={12} md={12} sm={12} style={styles.inputTitles}>
                    <label htmlFor={"username"}>
                      {LOCALIZE.authentication.login.content.inputs.emailTitle}
                    </label>
                  </Col>
                  <Col xl={12} lg={12} md={12} sm={12}>
                    <input
                      className="valid-field"
                      aria-required={"true"}
                      type="text"
                      id="username"
                      style={{ ...styles.inputs, ...accommodationsStyle }}
                      onChange={this.handleUsernameChange}
                      value={this.state.username}
                    />
                  </Col>
                </Row>
                <Row style={styles.formRow}>
                  <Col xl={12} lg={12} md={12} sm={12} style={styles.inputTitles}>
                    <label htmlFor={"password"}>
                      {LOCALIZE.authentication.login.content.inputs.passwordTitle}
                    </label>
                  </Col>
                  <Col xl={12} lg={12} md={12} sm={12}>
                    <div style={styles.passwordContainer}>
                      <input
                        className="valid-field"
                        aria-label={
                          this.state.wrongCredentials
                            ? LOCALIZE.authentication.login.invalidCredentials +
                              LOCALIZE.authentication.login.passwordFieldSelected
                            : LOCALIZE.authentication.login.content.inputs.passwordTitle
                        }
                        aria-invalid={this.state.wrongCredentials}
                        aria-required={"true"}
                        type={this.state.passwordInputType}
                        id="password"
                        style={{ ...styles.passwordInput, ...accommodationsStyle }}
                        onChange={this.handlePasswordChange}
                        value={this.state.password}
                      />
                    </div>
                    <div style={styles.passwordVisilibityContainer}>
                      <CustomButton
                        label={
                          <FontAwesomeIcon icon={this.state.viewPassword ? faEye : faEyeSlash} />
                        }
                        ariaLabel={LOCALIZE.authentication.login.content.inputs.showPassword}
                        ariaPressed={this.state.viewPassword}
                        action={this.togglePasswordView}
                        type={"button"}
                        customStyle={styles.passwordVisilibity}
                        buttonTheme={THEME.secondary}
                      />
                    </div>
                  </Col>
                </Row>
                <Row role="presentation">
                  <Col xl={6} lg={6} md={6} sm={6}>
                    {this.state.wrongCredentials && (
                      <label
                        id="invalid-credentials-error-label"
                        htmlFor={"password"}
                        style={styles.errorMessage}
                        className="notranslate"
                      >
                        {this.state.APIError[`${this.props.currentLanguage}`]}
                      </label>
                    )}
                  </Col>
                  <Col xl={6} lg={6} md={6} sm={6} className="align-top text-right">
                    <CustomButton
                      label={LOCALIZE.authentication.login.forgotPassword}
                      action={this.handleOpenPopup}
                      type={"button"}
                      customStyle={styles.forgotPasswordButton}
                      buttonTheme={THEME.secondary}
                      className="text-right"
                    />
                  </Col>
                </Row>
                <Row>
                  <CustomButton
                    label={
                      this.state.currentlyLoading ? (
                        // eslint-disable-next-line jsx-a11y/label-has-associated-control
                        <label className="fa fa-spinner fa-spin">
                          <FontAwesomeIcon icon={faSpinner} />
                        </label>
                      ) : (
                        LOCALIZE.authentication.login.button
                      )
                    }
                    type={"submit"}
                    customStyle={styles.loginBtn}
                    buttonTheme={THEME.PRIMARY}
                    disabled={this.state.currentlyLoading}
                  />
                </Row>
              </form>
            </div>
          </div>
        )}
        <PopupBox
          show={this.state.showForgotPasswordPopup}
          title={LOCALIZE.authentication.login.forgotPasswordPopup.title}
          handleClose={() => {}}
          shouldCloseOnEsc={false}
          size={"lg"}
          description={
            <div>
              <Row>
                <Col>
                  <p>{LOCALIZE.authentication.login.forgotPasswordPopup.description}</p>
                </Col>
              </Row>
              <Row role="presentation" style={styles.resetPasswordEmailAddressContainer}>
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={styles.resetPasswordEmailAddressLabel}
                >
                  <label htmlFor="reset-password-email-address" style={styles.inputTitles}>
                    {LOCALIZE.authentication.login.forgotPasswordPopup.emailAddressLabel}
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={styles.resetPasswordEmailAddressInput}
                >
                  <input
                    id="reset-password-email-address"
                    className={
                      this.state.isValidForgotPasswordEmailAddress &&
                      this.state.isAnExistingEmailAddress
                        ? "valid-field"
                        : "invalid-field"
                    }
                    aria-invalid={
                      !this.state.isValidResetPasswordEmailAddress ||
                      !this.state.isAnExistingEmailAddress
                    }
                    aria-required={"true"}
                    type="text"
                    value={this.state.resetPasswordEmailAddressContent}
                    style={{
                      ...styles.inputs,
                      ...accommodationsStyle
                    }}
                    onChange={this.getResetPasswordEmailContent}
                  />
                  {!this.state.isValidForgotPasswordEmailAddress && (
                    <label
                      htmlFor="reset-password-email-address"
                      style={styles.errorMessage}
                      className="notranslate"
                    >
                      {
                        LOCALIZE.authentication.login.forgotPasswordPopup
                          .invalidEmailAddressErrorMessage
                      }
                    </label>
                  )}
                  {!this.state.isAnExistingEmailAddress && (
                    <label
                      htmlFor="reset-password-email-address"
                      style={styles.errorMessage}
                      className="notranslate"
                    >
                      {
                        LOCALIZE.authentication.login.forgotPasswordPopup
                          .usernameDoesNotExistErrorMessage
                      }
                    </label>
                  )}
                  {this.state.passwordResetSuccessful && (
                    <label id="email-sent-successfully" style={styles.successMessage} tabIndex={0}>
                      {LOCALIZE.authentication.login.forgotPasswordPopup.emailSentSuccessfully}
                    </label>
                  )}
                </Col>
              </Row>
              {this.state.passwordResetSuccessful && (
                <Row>
                  <Col>
                    <label id="email-sent-successfully-message">
                      <p>
                        {
                          LOCALIZE.authentication.login.forgotPasswordPopup
                            .emailSentSuccessfullMessage
                        }
                      </p>
                    </label>
                  </Col>
                </Row>
              )}
              {this.state.backendError && (
                <Row>
                  <Col>
                    <label
                      id="maximum-amout-of-email-sent-error-message"
                      style={styles.errorMessage}
                      tabIndex={0}
                      className="notranslate"
                    >
                      {this.state.backendErrorContent[this.props.currentLanguage]}
                    </label>
                  </Col>
                </Row>
              )}
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={
            this.state.passwordResetSuccessful
              ? LOCALIZE.authentication.login.forgotPasswordPopup.resendEmail
              : LOCALIZE.commons.cancel
          }
          leftButtonIcon={this.state.passwordResetSuccessful ? faUndo : faTimes}
          leftButtonAction={
            this.state.passwordResetSuccessful
              ? () => this.handleSendEmail()
              : () => this.handleClosePopup()
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={
            this.state.passwordResetSuccessful
              ? LOCALIZE.commons.ok
              : LOCALIZE.authentication.login.forgotPasswordPopup.sendResetLinkButton
          }
          rightButtonIcon={this.state.passwordResetSuccessful ? "" : faUndo}
          rightButtonAction={
            this.state.passwordResetSuccessful
              ? () => this.handleClosePopup()
              : () => this.handleSendEmail()
          }
        />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    authenticated: state.login.authenticated,
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    isSuperUser: state.user.isSuperUser
  };
};

const mapDispatchToProps = dispatch => ({
  loginAction: data => dispatch(loginAction(data)),
  handleAuthResponseAndState: userData => dispatch(handleAuthResponseAndState(userData)),
  updatePageHasErrorState: bool => dispatch(updatePageHasErrorState(bool)),
  updateLastLoginField: data => dispatch(updateLastLoginField(data)),
  setLastLoginDate: data => dispatch(setLastLoginDate(data)),
  getUserInformation: token => dispatch(getUserInformation(token)),
  setUserInformation: (
    firstName,
    lastName,
    username,
    isSuperUser,
    lastPasswordChange,
    psrsAppID,
    primaryEmail,
    secondaryEmail,
    dateOfBirth,
    pri,
    militaryNbr,
    lastLogin
  ) =>
    dispatch(
      setUserInformation(
        firstName,
        lastName,
        username,
        isSuperUser,
        lastPasswordChange,
        psrsAppID,
        primaryEmail,
        secondaryEmail,
        dateOfBirth,
        pri,
        militaryNbr,
        lastLogin
      )
    ),
  setIsUserLoading: bool => dispatch(setIsUserLoading(bool)),
  doesThisEmailExist: username => dispatch(doesThisEmailExist(username)),
  sendPasswordResetEmail: data => dispatch(sendPasswordResetEmail(data)),
  setTestDefinitionSelectionSideNavState: index =>
    dispatch(setTestDefinitionSelectionSideNavState(index)),
  setBOUserSideNavState: index => dispatch(setBOUserSideNavState(index)),
  getUserPermissions: () => dispatch(getUserPermissions()),
  updatePermissionsState: permissions => dispatch(updatePermissionsState(permissions)),
  updateCurrentHomePageState: currentHomePage =>
    dispatch(updateCurrentHomePageState(currentHomePage)),
  authenticateAction: authenticated => dispatch(authenticateAction(authenticated)),
  dispatch
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(LoginForm));
