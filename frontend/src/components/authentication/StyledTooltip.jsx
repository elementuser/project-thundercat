import React, { Component } from "react";
import PropTypes from "prop-types";
import ReactTooltip from "react-tooltip";
import { connect } from "react-redux";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";
import "../../css/styled-tooltip.css";

export const TYPE = {
  dark: "dark",
  error: "error",
  info: "info",
  light: "light",
  success: "success",
  warning: "warning"
};

export const EFFECT = {
  float: "float",
  solid: "solid"
};

export const TRIGGER_TYPE = {
  hover: "hover",
  click: "click",
  focus: "focus"
};

class StyledTooltip extends Component {
  static propTypes = {
    tooltipElement: PropTypes.object.isRequired,
    tooltipContent: PropTypes.object.isRequired,
    triggerType: PropTypes.array.isRequired,
    customStyle: PropTypes.object
  };

  render() {
    let style = {
      fontFamily: this.props.accommodations.fontFamily,
      fontSize: this.props.accommodations.fontSize
    };
    if (this.props.accommodations.spacing) {
      style = { ...style, ...getLineSpacingCSS() };
    }

    // generating eventString and eventOffString based on provided trigger types
    let eventString = "";
    let eventOffString = "";
    // making sure that the triggerType props is defined
    if (typeof this.props.triggerType !== "undefined") {
      // hover
      if (this.props.triggerType.indexOf(TRIGGER_TYPE.hover) >= 0) {
        eventString += " mouseover";
        eventOffString += " mouseout";
      }
      // focus
      if (this.props.triggerType.indexOf(TRIGGER_TYPE.focus) >= 0) {
        eventString += " focusin";
        eventOffString += " focusout";
      }
      // click
      if (this.props.triggerType.indexOf(TRIGGER_TYPE.click) >= 0) {
        eventString += " click";
        eventOffString += " focusout";
      }
    }

    const tooltipContent = (
      <div style={{ ...style, ...this.props.customStyle }}>{this.props.tooltipContent}</div>
    );
    return (
      <>
        <span>{this.props.tooltipElement}</span>
        <ReactTooltip
          {...this.props}
          className="react-tooltip-custom-class"
          border={true}
          event={eventString}
          eventOff={eventOffString}
        >
          {tooltipContent}
        </ReactTooltip>
      </>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    accommodations: state.accommodations
  };
};

export default connect(mapStateToProps, null)(StyledTooltip);
