import React, { Component } from "react";
import PropTypes from "prop-types";
import LOCALIZE from "../../text_resources";
import "../../css/collapsing-item.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faAngleDown,
  faAngleUp,
  faArrowDown,
  faArrowUp,
  faEnvelope,
  faTasks
} from "@fortawesome/free-solid-svg-icons";
import CustomButton, { THEME } from "../commons/CustomButton";
import { BUTTON_TYPE } from "../commons/PopupBox";
import StyledTooltip, { TYPE, EFFECT, TRIGGER_TYPE } from "../authentication/StyledTooltip";

const styles = {
  container: {
    position: "relative",
    margin: "16px 0 16px 0",
    wordWrap: "break-word",
    width: "100%"
  },
  containerCollapsed: {
    display: "table",
    width: "100%"
  },
  collapsingItemContainer: {
    display: "table-cell",
    verticalAlign: "middle"
  },
  moveUpAndDownButtonsMainContainer: {
    display: "table-cell",
    verticalAlign: "middle",
    padding: "0 6px",
    width: "1%"
  },
  moveUpAndDownButtonsContainer: {
    display: "table-cell",
    verticalAlign: "middle"
  },
  moveUpAndDownButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  button: {
    width: "100%",
    textAlign: "left",
    minHeight: 48
  },
  emailAndTaskIcon: {
    margin: 8
  },
  titleContainer: {
    float: "left",
    paddingTop: 4
  },
  expandIcon: {
    float: "right",
    margin: "auto 8px",
    pointerEvents: "none"
  },
  iconStyle: {
    marginTop: 6
  },
  contentContainer: {
    margin: 12
  },
  allUnset: {
    all: "unset"
  }
};

export const ICON_TYPE = {
  email: faEnvelope,
  task: faTasks
};

class CollapsingItemContainer extends Component {
  constructor(props, context) {
    super(props, context);

    this.PropTypes = {
      id: PropTypes.string,
      iconType: PropTypes.object,
      title: PropTypes.object.isRequired,
      body: PropTypes.object.isRequired,
      // used so we only pass props for editing when it makes sense
      // this prevents an error from occurring in AssignTestAccess.jsx
      bodyIsEditable: PropTypes.bool,
      triggerStateReset: PropTypes.bool,
      usesMoveUpAndDownFunctionalities: PropTypes.bool,
      moveUpAction: PropTypes.func,
      moveUpButtonDisabled: PropTypes.bool,
      moveDownAction: PropTypes.func,
      moveDownButtonDisabled: PropTypes.bool,
      draggableSwitch: PropTypes.func,
      triggerCollapse: PropTypes.bool
    };

    CollapsingItemContainer.defaultProps = {
      usesMoveUpAndDownFunctionalities: false,
      moveUpButtonDisabled: false,
      moveDownButtonDisabled: false,
      triggerCollapse: true
    };
  }

  state = {
    isCollapsed: true,
    // used by containers that contain an editable response (Sample e-MIB)
    userIsEditing: false
  };

  componentDidUpdate = prevProps => {
    // if triggerStateReset gets updated
    if (prevProps.triggerStateReset !== this.props.triggerStateReset) {
      // resetting isCollapsed local state
      this.setState({ isCollapsed: true });
      if (this.props.draggableSwitch) {
        this.props.draggableSwitch();
      }
    }
    // if triggerCollapse gets updated, collapse the item
    if (prevProps.triggerCollapse !== this.props.triggerCollapse) {
      this.collapseItem();
    }
  };

  collapseItem = () => {
    this.setState({ isCollapsed: true });
  };

  expandItem = () => {
    if (!this.state.userIsEditing) {
      this.setState({ isCollapsed: !this.state.isCollapsed });
      if (this.props.draggableSwitch) {
        this.props.draggableSwitch();
      }
    }
  };

  showEditDialog = () => {
    this.setState({ userIsEditing: true });
  };

  closeEditDialog = () => {
    this.setState({ userIsEditing: false });
  };

  render() {
    const { iconType, title, body, bodyIsEditable, usesMoveUpAndDownFunctionalities } = this.props;
    const { isCollapsed, userIsEditing } = this.state;
    let { buttonClass, iconClass, containerClass } = "";

    if (isCollapsed) {
      buttonClass = `${THEME.SECONDARY}`;
      iconClass = faAngleDown;
      containerClass = "";
    } else if (!isCollapsed) {
      buttonClass = `${THEME.PRIMARY} expanded-button-style`;
      iconClass = faAngleUp;
      containerClass = "expanded-container-style";
    }

    return (
      <div
        className={`${containerClass}`}
        style={
          usesMoveUpAndDownFunctionalities && isCollapsed
            ? { ...styles.container, ...styles.containerCollapsed }
            : styles.container
        }
      >
        <div
          style={
            usesMoveUpAndDownFunctionalities && isCollapsed ? styles.collapsingItemContainer : {}
          }
        >
          <CustomButton
            buttonId={this.props.id}
            label={
              <>
                {this.props.iconType && (
                  <FontAwesomeIcon icon={iconType} style={styles.emailAndTaskIcon} />
                )}
                <div style={styles.titleContainer}>
                  <span>{title}</span>
                </div>
                <div style={styles.expandIcon}>
                  <FontAwesomeIcon
                    icon={iconClass}
                    style={styles.iconStyle}
                    aria-hidden="true"
                    focusable="false"
                  />
                </div>
              </>
            }
            action={this.expandItem}
            customStyle={styles.button}
            buttonTheme={buttonClass}
            ariaExpanded={!this.state.isCollapsed}
          />
        </div>
        {this.props.usesMoveUpAndDownFunctionalities && isCollapsed && (
          <div style={styles.moveUpAndDownButtonsMainContainer}>
            <div style={styles.moveUpAndDownButtonsContainer}>
              <StyledTooltip
                id={`move-up-collapsing-item`}
                place="top"
                type={TYPE.light}
                effect={EFFECT.solid}
                triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
                disabled={false}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      dataTip=""
                      dataFor="move-up-collapsing-item"
                      label={<FontAwesomeIcon icon={faArrowUp} />}
                      ariaLabel={LOCALIZE.commons.moveUp}
                      action={this.props.moveUpAction}
                      disabled={this.props.moveUpButtonDisabled}
                      customStyle={styles.moveUpAndDownButton}
                      buttonTheme={BUTTON_TYPE.secondary}
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>{LOCALIZE.commons.moveUp}</p>
                  </div>
                }
              />
            </div>
            <div style={styles.moveUpAndDownButtonsContainer}>
              <StyledTooltip
                id={`move-down-collapsing-item`}
                place="top"
                type={TYPE.light}
                effect={EFFECT.solid}
                triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
                disabled={false}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      dataTip=""
                      dataFor="move-down-collapsing-item"
                      label={<FontAwesomeIcon icon={faArrowDown} />}
                      ariaLabel={LOCALIZE.commons.moveDown}
                      action={this.props.moveDownAction}
                      disabled={this.props.moveDownButtonDisabled}
                      customStyle={styles.moveUpAndDownButton}
                      buttonTheme={BUTTON_TYPE.secondary}
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>{LOCALIZE.commons.moveDown}</p>
                  </div>
                }
              />
            </div>
          </div>
        )}
        {!isCollapsed && (
          <div style={styles.contentContainer}>
            {React.cloneElement(body, {
              expandItem: this.expandItem,
              ...(bodyIsEditable
                ? {
                    userIsEditing,
                    showEditDialog: this.showEditDialog,
                    closeEditDialog: this.closeEditDialog
                  }
                : {})
            })}
          </div>
        )}
      </div>
    );
  }
}

export default CollapsingItemContainer;
