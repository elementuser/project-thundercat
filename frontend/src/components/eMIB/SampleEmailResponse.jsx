import React, { Component } from "react";
import "../../css/collapsing-item.css";
import LOCALIZE from "../../text_resources";
import { EMAIL_TYPE, actionShape, emailShape } from "./constants";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faReply, faReplyAll, faShareSquare } from "@fortawesome/free-solid-svg-icons";

const styles = {
  type: {
    minHeight: 35
  },
  replyAndUser: {
    color: "#00565E"
  },
  headings: {
    fontWeight: "bold"
  },
  responseType: {
    icon: {
      color: "white",
      margin: "0 8px",
      padding: 3,
      backgroundColor: "#00565E",
      border: "3px solid #009FAE",
      borderRadius: 4,
      fontSize: 24
    },
    attribute: {
      color: "#00565E",
      textDecoration: "underline"
    }
  },
  preWrap: {
    whiteSpace: "pre-wrap"
  },
  topDivider: {
    marginTop: 16,
    paddingTop: 16,
    borderTop: "1px solid rgba(0, 0, 0, 0.1)"
  },
  disabledExampleComponent: {
    border: "1px solid #00565e",
    borderRadius: 4,
    marginTop: 4,
    marginBottom: 4,
    padding: 16
  }
};

class SampleEmailResponse extends Component {
  constructor(props, context) {
    super(props, context);

    this.PropTypes = {
      action: actionShape,
      email: emailShape
    };
  }

  render() {
    const { action } = this.props;
    const visibleToNames = action.emailTo;

    const visibleCcNames = action.emailCc;
    return (
      <div style={styles.disabledExampleComponent}>
        <div aria-label={LOCALIZE.ariaLabel.responseDetails}>
          <div>
            <div style={styles.type} id="unit-test-email-response-type">
              <span style={styles.headings}>
                {LOCALIZE.emibTest.inboxPage.emailResponse.description}
              </span>
              {action.emailType === EMAIL_TYPE.reply && (
                <>
                  <FontAwesomeIcon icon={faReply} style={styles.responseType.icon} />
                  <span style={styles.responseType.attribute}>
                    {LOCALIZE.emibTest.inboxPage.emailCommons.reply}
                  </span>
                </>
              )}

              {action.emailType === EMAIL_TYPE.replyAll && (
                <>
                  <FontAwesomeIcon icon={faReplyAll} style={styles.responseType.icon} />
                  <span style={styles.responseType.attribute}>
                    {LOCALIZE.emibTest.inboxPage.emailCommons.replyAll}
                  </span>
                </>
              )}
              {action.emailType === EMAIL_TYPE.forward && (
                <>
                  <FontAwesomeIcon icon={faShareSquare} style={styles.responseType.icon} />
                  <span style={styles.responseType.attribute}>
                    {LOCALIZE.emibTest.inboxPage.emailCommons.forward}
                  </span>
                </>
              )}
            </div>
            <div id="unit-test-response-to">
              {LOCALIZE.emibTest.inboxPage.emailCommons.to}{" "}
              <span style={styles.replyAndUser}>{visibleToNames}</span>
            </div>
            <div id="unit-test-response-cc">
              {LOCALIZE.emibTest.inboxPage.emailCommons.cc}{" "}
              <span style={styles.replyAndUser}>{visibleCcNames}</span>
            </div>
          </div>
          <div style={styles.topDivider} id="unit-test-response-body-header">
            <div style={styles.headings}>{LOCALIZE.emibTest.inboxPage.emailResponse.response}</div>
            <p style={styles.preWrap} id="unit-test-response-body">
              {action.emailBody}
            </p>
          </div>
          <div style={styles.topDivider} id="unit-test-response-reasons-header">
            <div style={styles.headings}>
              {LOCALIZE.emibTest.inboxPage.emailResponse.reasonsForAction}
            </div>
            <p style={styles.preWrap} id="unit-test-response-reasons">
              {action.reasonsForAction}
            </p>
          </div>
        </div>
      </div>
    );
  }
}

export default SampleEmailResponse;
