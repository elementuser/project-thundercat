import React, { Component } from "react";
import PropTypes from "prop-types";
import LOCALIZE from "../../text_resources";
import { EMAIL_TYPE, actionShape, EDIT_MODE } from "./constants";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faReply, faReplyAll, faShareSquare } from "@fortawesome/free-solid-svg-icons";
import { faQuestionCircle } from "@fortawesome/free-regular-svg-icons";
import { Button } from "react-bootstrap";
import StyledTooltip, { TYPE, EFFECT, TRIGGER_TYPE } from "../authentication/StyledTooltip";
import Select from "react-select";
import "../../css/lib/react-super-select.css";
import "../../css/edit-email.css";

// These two consts limit the number of characters
// that can be entered into two text areas
// and are used to display <x>/<MAX>
// under the text areas
const MAX_RESPONSE = "3000";
const MAX_REASON = "650";

const styles = {
  header: {
    responseTypeIcons: {
      marginRight: 10,
      padding: 6,
      border: "1px solid #00565E",
      borderRadius: 4,
      cursor: "pointer",
      fontSize: 24
    },
    responseTypeIconsSelected: {
      backgroundColor: "#00565E",
      color: "white"
    },
    radioButtonZone: {
      marginBottom: 12
    },
    responseTypeRadio: {
      all: "unset",
      color: "#00565E",
      cursor: "pointer"
    },
    radioPadding: {
      marginBottom: 16
    },
    radioTextUnselected: {
      fontWeight: "normal",
      cursor: "pointer",
      paddingRight: 20,
      color: "#00565E"
    },
    radioTextSelected: {
      fontWeight: "bold",
      cursor: "pointer",
      paddingRight: 20,
      color: "#00565E"
    },
    fieldsetLegend: {
      fontSize: 16,
      marginBottom: 12,
      marginTop: 12,
      paddingTop: 12
    },
    titleStyle: {
      flex: "0 0 36px",
      height: 32,
      lineHeight: "2.1em",
      paddingRight: 4,
      marginTop: 5,
      marginBottom: 5
    }
  },
  invalidToFieldBorder: {
    border: "3px solid #923534"
  },
  invalidToFieldLabel: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6
  },
  response: {
    textArea: {
      padding: "6px 12px",
      border: "1px solid #00565E",
      borderRadius: 4,
      width: "100%",
      height: 225,
      resize: "none"
    }
  },
  textCounter: {
    width: "100%",
    textAlign: "right",
    paddingRight: 12
  },
  reasonsForAction: {
    textArea: {
      padding: "6px 12px",
      border: "1px solid #00565E",
      borderRadius: 4,
      width: "100%",
      height: 150,
      resize: "none"
    }
  },
  charLimitReached: {
    marginLeft: 5,
    color: "#923534"
  },
  tooltipButton: {
    float: "right",
    textDecoration: "underline",
    padding: "0 6px"
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  popover: {
    zIndex: 9999,
    padding: "0 12px"
  },
  toAndCcFieldContainer: {
    display: "flex",
    marginBottom: 10
  },
  toAndCcFieldInput: {
    flexGrow: 1,
    minWidth: 0
  },
  toFieldMargin: {
    marginTop: 16
  },
  responseIconsRow: {
    display: "flex",
    width: "100%"
  },
  responseButton: {
    cursor: "pointer"
  },
  responseTypeIcons: {
    marginRight: 10,
    padding: 9,
    border: "1px solid #00565E",
    borderRadius: 4,
    width: 40,
    height: 40,
    verticalAlign: "middle"
  },
  responseTypeIconsSelected: {
    backgroundColor: "#00565E",
    color: "white"
  },
  responseTypeEmailText: {
    marginRight: 10
  },
  tooltipIcon: {
    minHeight: 36,
    minWidth: 40,
    color: "#00565e"
  }
};

class EditEmail extends Component {
  static propTypes = {
    onChange: PropTypes.func.isRequired,
    action: actionShape,
    originalFrom: PropTypes.array,
    originalTo: PropTypes.array,
    originalCC: PropTypes.array,
    editMode: PropTypes.oneOf(Object.keys(EDIT_MODE)).isRequired,
    toFieldValid: PropTypes.bool.isRequired,
    triggerPropsUpdate: PropTypes.bool,
    emailType: PropTypes.string // only passed in on 'Create'

    // Provided by redux
    // addressBook: PropTypes.arrayOf(addressBookOptionShape)
  };

  constructor(props) {
    super(props);
    this.toFieldRef = React.createRef();
  }

  state = {
    emailType: !this.props.action ? this.props.emailType : this.props.action.emailType,
    emailBody: !this.props.action
      ? ""
      : !this.props.action.emailBody
      ? ""
      : this.props.action.emailBody,
    emailTo: !this.props.action ? this.setEmailToField() : this.props.action.emailTo,
    emailToSelectedValues: [],
    emailCc: !this.props.action ? this.setEmailCCField() : this.props.action.emailCc,
    emailCcSelectedValues: [],
    reasonsForAction: !this.props.action
      ? ""
      : !this.props.action.reasonsForAction
      ? ""
      : this.props.action.reasonsForAction
  };

  componentDidMount() {
    // After generating the initial state, update the parent with it.
    // This allows defaults to be set.
    this.props.onChange(this.state);
  }

  componentDidUpdate = (prevProps, prevState, snapshot) => {
    // if toFieldValid props is updating
    if (prevProps.toFieldValid !== this.props.toFieldValid) {
      // focusing on 'To' field selection box
      this.toFieldRef.current.focus();
      // if triggerPropsUpdate props is updating
    } else if (prevProps.triggerPropsUpdate !== this.props.triggerPropsUpdate) {
      // focusing on 'To' field selection box
      this.toFieldRef.current.focus();
    }

    // update the 'To' field when the emailType prop is changed
    if (prevProps.emailType !== this.props.emailType) {
      this.onEmailTypeChange(this.props.emailType);
    }
  };

  setEmailToField() {
    const { emailType, originalTo, originalFrom } = this.props;

    switch (emailType) {
      case EMAIL_TYPE.reply:
        return originalFrom;
      case EMAIL_TYPE.replyAll:
        return [...originalTo, ...originalFrom];
      default:
        return []; // catches EMAIL_TYPE.forward
    }
  }

  setEmailCCField() {
    const { emailType, originalCC } = this.props;

    switch (emailType) {
      case EMAIL_TYPE.reply:
      case EMAIL_TYPE.replyAll:
        return originalCC;
      default:
        return []; // catches EMAIL_TYPE.forward
    }
  }

  // reset 'To' and 'CC' fields to default
  onEmailTypeChange = newEmailType => {
    // Names sent in the original email.
    const { originalTo, originalFrom, originalCC } = this.props;

    // forwarding should have a blank to field.
    let replyList = [];
    let ccList = [];
    if (newEmailType === EMAIL_TYPE.reply) {
      // Reply to the person that sent you this email.
      replyList = originalFrom;
    } else if (newEmailType === EMAIL_TYPE.replyAll) {
      // Reply all to everyone this email was from and sent to.
      const toList = originalTo;
      const fromList = originalFrom;
      ccList = originalCC;

      replyList = toList.concat(fromList);
    }

    this.setState({ emailType: newEmailType, emailTo: replyList, emailCc: ccList });
    this.props.onChange({
      ...this.state,
      emailType: newEmailType,
      emailTo: replyList,
      emailCc: ccList
    });
  };

  onEmailToChange = options => {
    const opts = options !== null ? options : [];
    this.setState({ emailTo: opts });
    this.props.onChange({ ...this.state, emailTo: opts });
  };

  onEmailCcChange = options => {
    const opts = options !== null ? options : [];
    this.setState({ emailCc: opts });
    this.props.onChange({ ...this.state, emailCc: opts });
  };

  onEmailBodyChange = event => {
    const newEmailBody = event.target.value;
    this.setState({ emailBody: newEmailBody });
    this.props.onChange({ ...this.state, emailBody: newEmailBody });
  };

  onReasonsForActionChange = event => {
    const newreasonsForAction = event.target.value;
    this.setState({ reasonsForAction: newreasonsForAction });
    this.props.onChange({ ...this.state, reasonsForAction: newreasonsForAction });
  };

  // getting the selected values from To field + updating the needed state using them
  // this is used for hidden name values
  getToFieldSelectedValues = emailTo => {
    const values = [];
    for (let i = 0; i < emailTo.length; i++) {
      values.push(emailTo[i].label);
    }
    this.setState({ emailToSelectedValues: values });
  };

  // getting the selected values from Cc field + updating the needed state using them
  getCcFieldSelectedValues = emailCc => {
    const values = [];
    for (let i = 0; i < emailCc.length; i++) {
      values.push(emailCc[i].label);
    }
    this.setState({ emailCcSelectedValues: values });
  };

  getContactFromId = contactArray => {
    const array = [];
    for (const contact of contactArray) {
      for (const user of this.props.addressBook) {
        if (user.value === contact.value || user.value === contact.id) array.push(user);
      }
    }
    return array;
  };

  render() {
    const { emailBody, reasonsForAction, emailType } = this.state;
    let { emailTo, emailCc } = this.state;

    // Get localized to/cc options from the address book.
    const options = this.props.addressBook;

    // Convert emailTo and emailCC from array of indexes to options.

    // These two constants are used by 2 seperate labels:
    // 1 is a popover (which does not exist until clicked on;
    // thus cannot be used for aria-labelled-by)
    // 2 is a visual hidden label which aria-labelled by can use
    emailTo = this.getContactFromId(emailTo);
    emailCc = this.getContactFromId(emailCc);
    const yourResponseTooltipText =
      LOCALIZE.emibTest.inboxPage.addEmailResponse.emailResponseTooltip;
    const reasonsTooltipText = LOCALIZE.emibTest.inboxPage.addEmailResponse.reasonsForActionTooltip;
    return (
      <div style={styles.container}>
        <form>
          {this.props.editMode === EDIT_MODE.update && (
            <div style={styles.responseIconsRow}>
              <div
                onClick={this.onEmailTypeChange.bind(this, EMAIL_TYPE.reply)}
                style={styles.responseButton}
                id="unit-test-reply-button"
              >
                <FontAwesomeIcon
                  icon={faReply}
                  style={{
                    ...styles.responseTypeIcons,
                    ...(emailType === EMAIL_TYPE.reply ? styles.responseTypeIconsSelected : {})
                  }}
                />
                <span style={styles.responseTypeEmailText}>
                  {LOCALIZE.emibTest.inboxPage.emailCommons.reply}
                </span>
              </div>
              <div
                onClick={this.onEmailTypeChange.bind(this, EMAIL_TYPE.replyAll)}
                style={styles.responseButton}
                id="unit-test-reply-all-button"
              >
                <FontAwesomeIcon
                  icon={faReplyAll}
                  style={{
                    ...styles.responseTypeIcons,
                    ...(emailType === EMAIL_TYPE.replyAll ? styles.responseTypeIconsSelected : {})
                  }}
                />
                <span style={styles.responseTypeEmailText}>
                  {LOCALIZE.emibTest.inboxPage.emailCommons.replyAll}
                </span>
              </div>
              <div
                onClick={this.onEmailTypeChange.bind(this, EMAIL_TYPE.forward)}
                style={styles.responseButton}
              >
                <FontAwesomeIcon
                  icon={faShareSquare}
                  style={{
                    ...styles.responseTypeIcons,
                    ...(emailType === EMAIL_TYPE.forward ? styles.responseTypeIconsSelected : {})
                  }}
                />
                <span style={styles.responseTypeEmailText}>
                  {LOCALIZE.emibTest.inboxPage.emailCommons.forward}
                </span>
              </div>
            </div>
          )}
          <div
            className="font-weight-bold"
            style={{ ...styles.toAndCcFieldContainer, ...styles.toFieldMargin }}
          >
            <label style={styles.header.titleStyle}>
              {LOCALIZE.emibTest.inboxPage.emailCommons.to}
            </label>
            <label id="to-field-selected" style={styles.hiddenText}>
              {LOCALIZE.emibTest.inboxPage.emailCommons.toFieldSelected}{" "}
              {this.state.emailToSelectedValues.length === 0
                ? `${LOCALIZE.formatString(
                    LOCALIZE.emibTest.inboxPage.emailCommons.currentSelectedPeople,
                    LOCALIZE.emibTest.inboxPage.emailCommons.currentSelectedPeopleAreNone
                  )}.`
                : `${LOCALIZE.formatString(
                    LOCALIZE.emibTest.inboxPage.emailCommons.currentSelectedPeople,
                    this.state.emailToSelectedValues
                  )}.`}
            </label>
            <label id="to-field-placeholder" style={styles.hiddenText}>
              {LOCALIZE.emibTest.inboxPage.emailCommons.toAndCcFieldsPlaceholder}
            </label>
            <div style={styles.toAndCcFieldInput}>
              <Select
                ref={this.toFieldRef}
                className={!this.props.toFieldValid ? "invalid-to-field" : ""}
                aria-labelledby="to-field-selected invalid-to-field to-field-placeholder"
                placeholder={LOCALIZE.emibTest.inboxPage.emailCommons.toAndCcFieldsPlaceholder}
                value={emailTo}
                onChange={this.onEmailToChange}
                options={options}
                isMulti="true"
                onFocus={() => this.getToFieldSelectedValues(emailTo)}
              />
            </div>
          </div>
          {!this.props.toFieldValid && (
            <label id="invalid-to-field" style={styles.invalidToFieldLabel}>
              {LOCALIZE.emibTest.inboxPage.addEmailResponse.invalidToFieldError}
            </label>
          )}
          <div className="font-weight-bold" style={styles.toAndCcFieldContainer}>
            <label style={styles.header.titleStyle}>
              {LOCALIZE.emibTest.inboxPage.emailCommons.cc}
            </label>
            <label id="cc-field-selected" style={styles.hiddenText}>
              {LOCALIZE.emibTest.inboxPage.emailCommons.ccFieldSelected}{" "}
              {this.state.emailCcSelectedValues.length === 0
                ? `${LOCALIZE.formatString(
                    LOCALIZE.emibTest.inboxPage.emailCommons.currentSelectedPeople,
                    LOCALIZE.emibTest.inboxPage.emailCommons.currentSelectedPeopleAreNone
                  )}.`
                : `${LOCALIZE.formatString(
                    LOCALIZE.emibTest.inboxPage.emailCommons.currentSelectedPeople,
                    this.state.emailCcSelectedValues
                  )}.`}
            </label>
            <label id="cc-field-placeholder" style={styles.hiddenText}>
              {LOCALIZE.emibTest.inboxPage.emailCommons.toAndCcFieldsPlaceholder}
            </label>
            <div style={styles.toAndCcFieldInput}>
              <Select
                aria-labelledby="cc-field-selected cc-field-placeholder"
                placeholder={LOCALIZE.emibTest.inboxPage.emailCommons.toAndCcFieldsPlaceholder}
                value={emailCc}
                onChange={this.onEmailCcChange}
                options={options}
                isMulti="true"
                onFocus={() => this.getCcFieldSelectedValues(emailCc)}
              />
            </div>
          </div>
          <div>
            <div className="font-weight-bold form-group">
              <label id="your-response-text-label">
                <p>
                  {LOCALIZE.formatString(
                    LOCALIZE.emibTest.inboxPage.addEmailResponse.response,
                    MAX_RESPONSE
                  )}
                </p>
                {this.state.emailBody.length >= MAX_RESPONSE && (
                  <p style={styles.charLimitReached} aria-live="assertive" role="alert">
                    {LOCALIZE.emibTest.inboxPage.characterLimitReached}
                  </p>
                )}
              </label>
              <StyledTooltip
                id="your-response-tooltip"
                place="left"
                type={TYPE.light}
                effect={EFFECT.solid}
                triggerType={[TRIGGER_TYPE.hover]}
                tooltipElement={
                  <Button
                    data-tip=""
                    data-for="your-response-tooltip"
                    tabIndex="-1"
                    aria-label={LOCALIZE.ariaLabel.emailResponseTooltip}
                    style={styles.tooltipButton}
                    variant="link"
                  >
                    <FontAwesomeIcon
                      icon={faQuestionCircle}
                      style={styles.tooltipIcon}
                    ></FontAwesomeIcon>
                  </Button>
                }
                tooltipContent={
                  <div>
                    <p>{yourResponseTooltipText}</p>
                  </div>
                }
              />
              <div>
                <label className="visually-hidden" id="your-response-tooltip-text">
                  {yourResponseTooltipText}
                </label>
                <textarea
                  id="your-response-text-area"
                  maxLength={MAX_RESPONSE}
                  aria-labelledby="your-response-text-label your-response-tooltip-text"
                  style={styles.response.textArea}
                  value={emailBody}
                  onChange={this.onEmailBodyChange}
                />
              </div>
              <div style={styles.textCounter}>
                {this.state.emailBody === undefined ? 0 : this.state.emailBody.length}/
                {MAX_RESPONSE}
              </div>
            </div>
          </div>
          <div>
            <div className="font-weight-bold form-group">
              <label id="reasons-for-action-text-label">
                <p>
                  {LOCALIZE.formatString(
                    LOCALIZE.emibTest.inboxPage.addEmailResponse.reasonsForAction,
                    MAX_REASON
                  )}
                </p>
                {this.state.reasonsForAction.length >= MAX_REASON && (
                  <p style={styles.charLimitReached} aria-live="assertive" role="alert">
                    {LOCALIZE.formatString(
                      LOCALIZE.emibTest.inboxPage.characterLimitReached,
                      MAX_REASON
                    )}
                  </p>
                )}
              </label>
              <StyledTooltip
                id="reasons-for-action-tooltip"
                place="left"
                type={TYPE.light}
                effect={EFFECT.solid}
                triggerType={[TRIGGER_TYPE.hover]}
                tooltipElement={
                  <Button
                    data-tip=""
                    data-for="reasons-for-action-tooltip"
                    tabIndex="-1"
                    aria-label={LOCALIZE.ariaLabel.reasonsForActionTooltip}
                    style={styles.tooltipButton}
                    variant="link"
                  >
                    <FontAwesomeIcon
                      icon={faQuestionCircle}
                      style={styles.tooltipIcon}
                    ></FontAwesomeIcon>
                  </Button>
                }
                tooltipContent={
                  <div>
                    <p>{reasonsTooltipText}</p>
                  </div>
                }
              />
              <div>
                <label className="visually-hidden" id="reasons-for-action-tooltip-text">
                  {reasonsTooltipText}
                </label>
                <textarea
                  id="reasons-for-action-text-area"
                  maxLength={MAX_REASON}
                  aria-labelledby="reasons-for-action-text-label reasons-for-action-tooltip-text"
                  style={styles.reasonsForAction.textArea}
                  value={reasonsForAction}
                  onChange={this.onReasonsForActionChange}
                />
              </div>
              <div style={styles.textCounter}>
                {this.state.reasonsForAction === undefined ? 0 : this.state.reasonsForAction.length}
                /{MAX_REASON}
              </div>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

export default EditEmail;
