import React, { Component } from "react";
import "../../css/collapsing-item.css";
import LOCALIZE from "../../text_resources";
import { actionShape, emailShape } from "./constants";

const styles = {
  headings: {
    fontWeight: "bold"
  },
  preWrap: {
    whiteSpace: "pre-wrap"
  },
  topDivider: {
    marginTop: 16,
    paddingTop: 16,
    borderTop: "1px solid rgba(0, 0, 0, 0.1)"
  },
  disabledExampleComponent: {
    border: "1px solid #00565e",
    borderRadius: 4,
    marginTop: 4,
    marginBottom: 4,
    padding: 16
  }
};

class SampleTaskResponse extends Component {
  constructor(props, context) {
    super(props, context);

    this.PropTypes = {
      action: actionShape,
      email: emailShape.isRequired
    };
  }

  render() {
    const { action } = this.props;
    return (
      <div style={styles.disabledExampleComponent}>
        <div aria-label={LOCALIZE.ariaLabel.taskDetails}>
          <div>
            <div style={styles.headings} id="unit-test-sample-task-header">
              {LOCALIZE.emibTest.inboxPage.taskContent.task}
            </div>
            <p style={styles.preWrap} id="unit-test-sample-task-content">
              {action.task}
            </p>
          </div>
          <div style={styles.topDivider}>
            <div style={styles.headings} id="unit-test-sample-task-reason-header">
              {LOCALIZE.emibTest.inboxPage.emailResponse.reasonsForAction}
            </div>
            <p style={styles.preWrap} id="unit-test-sample-task-reason">
              {action.reasonsForAction}
            </p>
          </div>
        </div>
      </div>
    );
  }
}

export default SampleTaskResponse;
