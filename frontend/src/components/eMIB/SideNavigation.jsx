import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import NextPreviousButtonNav from "../commons/NextPreviousButtonNav";
import { useTabState, Tab, TabList, TabPanel } from "reakit/Tab";
import accommodations, { getLineSpacingCSS } from "../../modules/AccommodationsRedux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUserCircle } from "@fortawesome/free-solid-svg-icons";
import { Row, Col } from "react-bootstrap";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 2,
    xl: 2
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 10,
    xl: 10
  }
};
const styles = {
  container: {
    display: "flex"
  },
  col: {
    paddingBottom: "30px"
  },
  sideNaveRow: {
    height: "auto",
    padding: "0 30px 0 30px",
    wordBreak: "break-word"
  },
  mainTabPanel: {
    padding: "5px 30px 5px 30px"
  }
};
function getTabs(props, tab, componentFunctions) {
  function switchTab(key) {
    if (tab.currentId === key && props.switchTab) {
      props.switchTab(key);
    }
    // if updated selected side nav item is defined then call it
    if (props.updateSelectedSideNavItem) {
      // creating a copy of selectedItemsArray
      const copyOfSelectedItemsArray = componentFunctions.state.selectedItemsArray;
      // copyOfSelectedItemsArray does not already contain the startIndex value
      if (!copyOfSelectedItemsArray.includes(props.startIndex)) {
        // add startIndex value to copyOfSelectedItemsArray
        copyOfSelectedItemsArray.push(props.startIndex);
      }
      // copyOfSelectedItemsArray is not matching the selectedItemsArray (need to update the state)
      if (copyOfSelectedItemsArray !== componentFunctions.state.selectedItemsArray) {
        // setting selectedItemsArray state
        componentFunctions.setState({ selectedItemsArray: copyOfSelectedItemsArray });
      }
      props.updateSelectedSideNavItem(tab.currentId);
    }
  }

  let menuTabStyle = {
    display: "block",
    backgroundColor: "#00565e",
    color: "#fff",
    borderRadius: "0.35rem",
    width: "100%"
  };

  let menuTabStyleError = {
    display: "block",
    backgroundColor: "#923534",
    color: "#fff",
    border: "3px solid #923534",
    borderRadius: "0.35rem",
    width: "100%"
  };

  let menuTabInactive = {
    display: "block",
    backgroundColor: "#fafafa",
    border: "none",
    width: "100%",
    color: "#0278a4"
  };

  let menuTabInactiveError = {
    display: "block",
    backgroundColor: "#fafafa",
    border: "3px solid #923534",
    borderRadius: "0.35rem",
    width: "100%",
    color: "#923534",
    fontWeight: "bold"
  };

  if (props.accommodations.spacing) {
    menuTabStyle = {
      ...menuTabStyle,
      ...getLineSpacingCSS()
    };
    menuTabStyleError = {
      ...menuTabStyleError,
      ...getLineSpacingCSS()
    };
    menuTabInactive = {
      ...menuTabInactive,
      ...getLineSpacingCSS()
    };
    menuTabInactiveError = {
      ...menuTabInactiveError,
      ...getLineSpacingCSS()
    };
  } else {
    menuTabStyle = {
      ...menuTabStyle,
      ...{ padding: "0.5rem 1rem" }
    };
    menuTabStyleError = {
      ...menuTabStyleError,
      ...{ padding: "0.5rem 1rem" }
    };
    menuTabInactive = {
      ...menuTabInactive,
      ...{ padding: "0.5rem 1rem" }
    };
    menuTabInactiveError = {
      ...menuTabInactiveError,
      ...{ padding: "0.5rem 1rem" }
    };
  }

  return (
    <>
      {props.specs.map((item, index) => {
        let itemWithError = false;
        if (typeof item.error !== "undefined") {
          itemWithError = item.error;
        }

        return (
          <Tab
            {...tab}
            id={item.key || index + 1}
            key={item.key || index + 1}
            style={
              tab.selectedId === (item.key || index + 1)
                ? !itemWithError
                  ? menuTabStyle
                  : menuTabStyleError
                : !itemWithError
                ? menuTabInactive
                : menuTabInactiveError
            }
            onFocus={switchTab(item.key)}
            className="col-xl-12 col-lg-12 col-md-12 col-sm-12"
          >
            {item.menuString}
            <span style={componentFunctions.styles.hiddenText}>{props.parentTabName}</span>
          </Tab>
        );
      })}
    </>
  );
}

function getTabPanels(props, tab, componentFunctions) {
  const { specs } = props;
  return (
    <>
      {specs.map((item, index) => {
        let renderItem = true;
        // startIndex is defined (> 0) + loadOnClick is set to true
        // ==> using the loadOnClick logic
        if (typeof props.startIndex !== "undefined" && props.startIndex > 0 && props.loadOnClick) {
          // current index is part of the selectedItemsArray
          if (componentFunctions.state.selectedItemsArray.includes(index + 1)) {
            // redering this item
            renderItem = true;
          } else {
            // not rendering this item
            renderItem = false;
          }
        }
        return (
          <TabPanel key={index + 1} {...tab} style={props.tabContentStyle}>
            <div
              style={{
                ...(props.bodyContentCustomStyle
                  ? { ...componentFunctions.styles.bodyContent, ...props.bodyContentCustomStyle }
                  : componentFunctions.styles.bodyContent),
                ...styles.mainTabPanel
              }}
            >
              {renderItem && (
                <div>
                  {item.body}
                  {props.displayNextPreviousButton && (
                    <NextPreviousButtonNav
                      showNext={index + 1 < specs.length - 1}
                      showPrevious={index + 1 > 0}
                      onChangeToPrevious={() => {
                        componentFunctions.onChangeToPrevious(index + 1 - 1);
                      }}
                      onChangeToNext={() => {
                        componentFunctions.onChangeToNext(index + 1 + 1);
                      }}
                    />
                  )}
                </div>
              )}
            </div>
          </TabPanel>
        );
      })}
    </>
  );
}

function SideNavigationHooks(props) {
  const tab = useTabState({
    orientation: "vertical",
    selectedId: props.startIndex > 0 ? props.startIndex : props.activeKey
  });

  // converting current font size in Int
  const fontSizeInt = parseInt(props.accommodations.fontSize.substring(0, 2));

  // converting profile icon font size
  const iconStyle = {
    fontSize: `${fontSizeInt + 200}px`,
    width: "100%"
  };

  return function func(componentFunctions) {
    return (
      <Row style={styles.container} className="notranslate">
        <Col
          xl={fontSizeInt > 29 ? 12 : columnSizes.firstColumn.xl}
          lg={fontSizeInt > 20 ? 12 : columnSizes.firstColumn.lg}
          md={columnSizes.firstColumn.md}
          sm={columnSizes.firstColumn.sm}
          xs={columnSizes.firstColumn.xs}
        >
          <TabList
            {...tab}
            aria-label={LOCALIZE.ariaLabel.sideNavigationSection}
            id="navigation-items-section"
            style={{
              ...props.tabContentStyle,
              ...componentFunctions.styles.sideNavPadding,
              ...styles.sideNaveRow
            }}
            className="row"
          >
            {props.showUserIcon ? <FontAwesomeIcon style={iconStyle} icon={faUserCircle} /> : ""}
            {getTabs(props, tab, componentFunctions)}
          </TabList>
        </Col>
        <Col
          xl={fontSizeInt > 29 ? 12 : columnSizes.secondColumn.xl}
          lg={fontSizeInt > 20 ? 12 : columnSizes.secondColumn.lg}
          md={columnSizes.secondColumn.md}
          sm={columnSizes.secondColumn.sm}
          xs={columnSizes.secondColumn.xs}
          style={{ ...props.tabContainerStyle, ...styles.col }}
        >
          {getTabPanels(props, tab, componentFunctions)}
        </Col>
      </Row>
    );
  };
}

class SideNavigation extends Component {
  EVENTKEYSTRING = `event_key_`;

  EVENT_KEYS = [];

  styles = {
    rowContainer: {
      margin: 0
    },
    sideNavPadding: {
      padding: "0 10px",
      paddingTop: 10
    },
    bodyContent: {
      paddingRight: 20,
      paddingTop: 10,
      paddingBottom: 10,
      minHeight: 500,
      textAlign: "left"
    },
    hiddenText: {
      position: "absolute",
      left: -10000,
      top: "auto",
      width: 1,
      height: 1,
      overflow: "hidden"
    },
    noStyle: {}
  };

  constructor(props, context) {
    super(props, context);

    this.PropTypes = {
      startIndex: PropTypes.number.isRequired,
      loadOnClick: PropTypes.bool,
      specs: PropTypes.arrayOf(
        PropTypes.shape({
          menuString: PropTypes.string,
          body: PropTypes.object
        })
      ).isRequired,
      isMain: PropTypes.bool,
      parentTabName: PropTypes.string,
      // the buttons are not working and has to be fixed when required
      displayNextPreviousButton: PropTypes.bool,
      tabContainerStyle: PropTypes.object,
      tabContentStyle: PropTypes.object,
      navStyle: PropTypes.object,
      bodyContentCustomStyle: PropTypes.object,
      updateSelectedSideNavItem: PropTypes.func,
      accommodations: PropTypes.object,
      showUserIcon: PropTypes.bool
    };
    SideNavigation.defaultProps = {
      tabContainerStyle: this.styles.noStyle,
      tabContentStyle: this.styles.noStyle,
      navStyle: this.styles.noStyle,
      showUserIcon: false,
      loadOnClick: false
    };
  }

  state = {
    currentIndex: this.EVENTKEYSTRING + (this.props.startIndex + 1),
    selectedItemsArray: []
  };

  componentDidMount = () => {
    // if background tab is rendered
    if (this.props.startIndex === 10) {
      // focusing on side navigation items of background tab
      document.getElementById("navigation-items-section").focus();
    }
  };

  // the buttons are not working and has to be fixed when required
  onChangeToNext = index => {
    this.setState({ currentIndex: this.EVENT_KEYS[index + this.props.startIndex] });
    document.getElementById("main-content").scrollTop = 0;
  };

  // the buttons are not working and has to be fixed when required
  onChangeToPrevious = index => {
    this.setState({ currentIndex: this.EVENT_KEYS[index + this.props.startIndex] });
    document.getElementById("main-content").scrollTop = 0;
  };

  onSelect = eventKey => {
    this.setState({ currentIndex: eventKey });
    document.getElementById("main-content").scrollTop = 0;
  };

  render() {
    const { myHookValue } = this.props;

    return <>{myHookValue(this)}</>;
  }
}

function withMyHook(Component) {
  return function WrappedComponent(props) {
    const myHookValue = SideNavigationHooks(props);
    return <Component {...props} myHookValue={myHookValue} />;
  };
}

const mapStateToProps = (state, ownProps) => {
  return {
    accommodations: state.accommodations,
    validationErrors: state.testBuilder.validation_errors
  };
};

export default connect(mapStateToProps, null)(withMyHook(SideNavigation));
