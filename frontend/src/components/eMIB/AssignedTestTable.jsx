import React, { Component } from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import {
  getAssignedTests,
  updateAssignedTestId,
  resetAssignedTestState
} from "../../modules/AssignedTestsRedux";
import { PATH } from "../commons/Constants";
import CandidateCheckIn from "../../CandidateCheckIn";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlayCircle } from "@fortawesome/free-solid-svg-icons";
import SessionStorage, { ACTION, ITEM } from "../../SessionStorage";
import TEST_STATUS from "../ta/Constants";
import {
  activateTest,
  deactivateTest,
  updateTestStatus,
  getCurrentTestStatus,
  invalidateTest
} from "../../modules/TestStatusRedux";
import { resetTestFactoryState, setContentUnLoaded } from "../../modules/TestSectionRedux";

import { resetNotepadState } from "../../modules/NotepadRedux";
import { resetQuestionListState } from "../../modules/QuestionListRedux";
import { resetTopTabsState } from "../../modules/NavTabsRedux";
import { setErrorStatus } from "../../modules/ErrorStatusRedux";
import CustomButton, { THEME } from "../commons/CustomButton";
import GenericTable, { COMMON_STYLE } from "../commons/GenericTable";

const styles = {
  table: {
    width: "75%",
    margin: "24px auto"
  },
  viewButton: {
    minWidth: 150
  },
  buttonLabel: {
    margin: 0,
    paddingLeft: 6
  }
};

class AssignedTestTable extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  static propTypes = {
    username: PropTypes.string,
    isUserLoading: PropTypes.bool,
    // Props from Redux
    getAssignedTests: PropTypes.func,
    updateAssignedTestId: PropTypes.func,
    activateTest: PropTypes.func,
    deactivateTest: PropTypes.func,
    setErrorStatus: PropTypes.func
  };

  state = {
    username: "",
    assigned_tests: [],
    rowsDefinition: {},
    activeUitTest: false,
    id: null,
    testId: null,
    startTime: null,
    isCheckedIn: false,
    isLoading: true,
    pollingState: undefined
  };

  componentDidMount = () => {
    this._isMounted = true;
    // remove active test variable if it exists
    SessionStorage(ACTION.REMOVE, ITEM.TEST_ACTIVE);

    if (this._isMounted) {
      // will be defined except in tests
      if (typeof this.props.getAssignedTests !== "undefined") {
        this.setState({ isLoading: true }, () => {
          this.populateTable();
        });
      }
    }
  };

  componentDidUpdate = prevProps => {
    // if language gets updated
    if (prevProps.currentLanguage !== this.props.currentLanguage) {
      // updating assigned tests table
      this.props.getAssignedTests().then(response => {
        this.setState({ assigned_tests: response });
      });
    }
  };

  componentWillUnmount = () => {
    this._isMounted = false;
    clearInterval(this.state.pollingState);
  };

  handleTestStatusUpdates = () => {
    // looping in assigned tests
    for (let i = 0; i < this.state.assigned_tests.length; i++) {
      // getting current test status
      this.props.getCurrentTestStatus(this.state.assigned_tests[i].id).then(testStatusData => {
        // test status is LOCKED, PAUSED or UNASSIGNED
        if (
          testStatusData.status === TEST_STATUS.LOCKED ||
          testStatusData.status === TEST_STATUS.PAUSED ||
          testStatusData.status === TEST_STATUS.UNASSIGNED
        ) {
          // reloading the page
          window.location.reload();
          // test status is READY
        } else if (testStatusData.status === TEST_STATUS.READY) {
          // updating respective assigned_tests status state
          const copyOfAssignedTest = Array.from(this.state.assigned_tests);
          copyOfAssignedTest[i].status = TEST_STATUS.READY;
          this.setState({ assigned_tests: copyOfAssignedTest }, () => {
            // populating table rows
            this.populateTableRows();
          });
        }
      });
    }
  };

  // populating assigned tests table
  populateTable = () => {
    let assignedTests = null;
    let activeTestObj = {};

    /* displayTableContent state is used here to avoid unwanted table content display, such as
        the flashing view button when refreshing the page */
    if (this._isMounted) {
      // getting assigned tests
      this.props
        .getAssignedTests()
        .then(assigned_tests => {
          // verifying if there is an active test
          activeTestObj = this.checkForActiveTest(assigned_tests);
          assignedTests = assigned_tests;
        })
        .then(() => {
          // there is an active test
          if (activeTestObj.activeTest && !activeTestObj.unsupervisedTest) {
            // redirecting user to test page
            this.props.history.push(PATH.testBase);
            // there is no active test
          } else {
            // initializing pollingFunctionNeeded
            let pollingFunctionNeeded = false;
            // looping in assigned tests
            for (let i = 0; i < assignedTests.length; i++) {
              // making sure that this is a supervised test before calling the polling function
              if (assignedTests[i].uit_invite_id === null) {
                pollingFunctionNeeded = true;
              }
            }
            if (pollingFunctionNeeded) {
              // clearing existing polling interval(s) and creating new one (interval of 5 seconds)
              clearInterval(this.state.pollingState);
              const interval = setInterval(this.handleTestStatusUpdates, 5000);
              this.setState({ pollingState: interval });
            }
          }
        })
        .then(() => {
          this.setState(
            {
              assigned_tests: assignedTests,
              isLoading: false
            },
            () => {
              // populating table rows
              this.populateTableRows();
            }
          );
        });
    }
  };

  // populating table rows for the GenericTable component
  populateTableRows = () => {
    const { assigned_tests } = this.state;

    // initializing needed object and array for rowsDefinition state (needed for GenericTable component)
    let rowsDefinition = {};
    const data = [];
    for (let i = 0; i < assigned_tests.length; i++) {
      // pushing needed data in data array (needed for GenericTable component)
      data.push({
        column_1: assigned_tests[i].test[`${this.props.currentLanguage}_name`],
        column_2: (
          <CustomButton
            buttonId={`unit-test-button-${i}`}
            label={
              <>
                <FontAwesomeIcon icon={faPlayCircle} />
                <span style={styles.buttonLabel}>{LOCALIZE.dashboard.table.viewButton}</span>
              </>
            }
            action={() =>
              this.viewTest(
                assigned_tests[i].id,
                assigned_tests[i].test.id,
                assigned_tests[i].test_access_code,
                assigned_tests[i].uit_invite_id,
                assigned_tests[i].accommodation_request
              )
            }
            customStyle={styles.viewButton}
            buttonTheme={THEME.SECONDARY}
            disabled={
              this.props.isTestActive ? true : assigned_tests[i].status === TEST_STATUS.ASSIGNED
            }
            ariaLabel={LOCALIZE.formatString(
              LOCALIZE.dashboard.table.viewButtonAccessibilityLabel,
              assigned_tests[i].test[`${this.props.currentLanguage}_name`]
            )}
          />
        )
      });
    }

    // updating rowsDefinition object with provided data and needed style
    rowsDefinition = {
      column_1_style: {},
      column_2_style: COMMON_STYLE.CENTERED_TEXT,
      data: data
    };

    this.setState({
      rowsDefinition: rowsDefinition
    });
  };

  viewTest = (currentAssignedTestId, testId, testAccessCode, uitInviteId, accommodationRequest) => {
    // getting current test status
    this.props.getCurrentTestStatus(currentAssignedTestId).then(testStatusData => {
      // at this point, the current test status should be READY
      if (testStatusData.status === TEST_STATUS.READY) {
        this.props.updateTestStatus(currentAssignedTestId, TEST_STATUS.PRE_TEST);
        this.props.setContentUnLoaded();
        this.resetAllRedux();
        this.props.history.push(PATH.testBase);
        this.props.updateAssignedTestId(currentAssignedTestId, testId, accommodationRequest);
        this.props.activateTest();
      }
      // current test status is LOCKED, PAUSED or UNASSIGNED
      else if (
        testStatusData.status === TEST_STATUS.LOCKED ||
        testStatusData.status === TEST_STATUS.PAUSED ||
        testStatusData.status === TEST_STATUS.UNASSIGNED
      ) {
        // reloading the page
        window.location.reload();
        // current test status is not the right one (not READY/LOCKED/PAUSED)
      } else {
        // invalidating test + redirecting to invalidated test page
        this.props.invalidateTest();
        this.resetAllRedux();
        this.props.history.push(PATH.invalidateTest);
      }
    });
  };

  resetAllRedux = () => {
    this.props.resetTestFactoryState();
    this.props.resetNotepadState();
    this.props.resetQuestionListState();
    this.props.resetTopTabsState();
  };

  /* checking in the assigned tests list if there is an active test (active, locked or paused)
  return true if that's the case */
  checkForActiveTest = assigned_tests => {
    let activeTest = false;
    let testAccessCode = null;
    let unsupervisedTest = false;
    for (let i = 0; i < assigned_tests.length; i++) {
      // if test is active
      if (
        assigned_tests[i].status === TEST_STATUS.PRE_TEST ||
        assigned_tests[i].status === TEST_STATUS.ACTIVE ||
        assigned_tests[i].status === TEST_STATUS.LOCKED ||
        assigned_tests[i].status === TEST_STATUS.PAUSED
      ) {
        this.props.updateAssignedTestId(
          assigned_tests[i].id.toString(),
          assigned_tests[i].test.id,
          assigned_tests[i].accommodation_request
        );
        testAccessCode = assigned_tests[i].test_access_code;
        activeTest = true;
        if (assigned_tests[i].uit_invite_id !== null) {
          unsupervisedTest = true;
        }
      }
    }
    return {
      activeTest: activeTest,
      testAccessCode: testAccessCode,
      unsupervisedTest: unsupervisedTest
    };
  };

  handleCheckIn = () => {
    this.populateTable();
    this.setState({ ...this.state, isCheckedIn: true });
  };

  render() {
    const columnsDefinition = [
      {
        label: LOCALIZE.dashboard.table.columnOne,
        style: { width: "50%" }
      },
      {
        label: LOCALIZE.dashboard.table.columnTwo,
        style: { ...{ width: "25%" }, ...COMMON_STYLE.CENTERED_TEXT }
      }
    ];
    return (
      <div className="notranslate">
        <div id="candidate-check-in">
          <CandidateCheckIn
            username={this.props.username}
            handleCheckIn={this.handleCheckIn}
            isCheckedIn={this.state.isCheckedIn}
            isLoaded={!this.props.isUserLoading}
          />
        </div>
        <div role="region" aria-labelledby="test-assignment-table">
          <div style={styles.table}>
            <GenericTable
              classnamePrefix="assigned-tests"
              columnsDefinition={columnsDefinition}
              rowsDefinition={this.state.rowsDefinition}
              emptyTableMessage={LOCALIZE.dashboard.table.noTests}
              currentlyLoading={this.props.isUserLoading || this.state.isLoading}
              tableWithButtons={true}
            />
          </div>
        </div>
      </div>
    );
  }
}

export { AssignedTestTable as UnconnectedAssignedTestTable };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    isTestActive: state.testStatus.isTestActive
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAssignedTests,
      updateAssignedTestId,
      activateTest,
      deactivateTest,
      resetTestFactoryState,
      setContentUnLoaded,
      resetAssignedTestState,
      resetNotepadState,
      resetQuestionListState,
      resetTopTabsState,
      updateTestStatus,
      setErrorStatus,
      getCurrentTestStatus,
      invalidateTest
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(AssignedTestTable));

export { styles as TEST_TABLE_STYLES };
