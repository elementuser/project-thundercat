import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import LOCALIZE from "../../../text_resources";
import { Container, Row } from "react-bootstrap";
import { makeLabel, makeTextBoxField } from "../helpers";
import CustomButton, { THEME } from "../../commons/CustomButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck } from "@fortawesome/free-solid-svg-icons";
import {
  modifyItemBankField,
  updateItemBankData,
  setItemBankData,
  setItemBankInitialData
} from "../../../modules/ItemBankRedux";
import ELEMENT_TO_UPDATE from "./Constants";
import { getItemBankHeightCalculations } from "../../../helpers/inTestHeightCalculations";
import * as _ from "lodash";
import { LANGUAGES } from "../../commons/Translation";

const styles = {
  mainContainer: {
    width: "100%",
    padding: "24px 12px",
    border: "1px solid #cdcdcd",
    borderTop: "none",
    overflowY: "scroll"
  },
  rowStyle: {
    margin: "12px 24px"
  },
  applyButtonContainer: {
    textAlign: "center",
    marginTop: 36
  },
  applyButton: {
    minWidth: 200
  },
  buttonIcon: {
    marginRight: 6
  }
};

class ItemBankDefinition extends Component {
  state = {
    // Main object keeping all fields' values
    item_bank_definition: { ...this.props.item_bank_definition },
    // Validity status of Item Bank ID
    isValidItemBankId: true,
    // Validity status of English Name
    isValidEnglishName: true,
    // Validity status of French Name
    isValidFrenchName: true,
    // Stores the timer used to know when a user is still typing
    typingTimer: 0
  };

  /*--------------------------*/
  //                          //
  //      Field Changes       //
  //                          //
  /*--------------------------*/

  /*---------------------------*/
  // Modify Item Bank ID Field //
  /*---------------------------*/
  onItemBankIdChange = (name, value) => {
    // allow only alphanumeric, slash and dash (0 to 15 chars)
    const regexExpression = /^([a-zA-Z0-9-/]{0,15})$/;
    if (regexExpression.test(value)) {
      this.setState({
        item_bank_definition: {
          ...this.state.item_bank_definition,
          custom_item_bank_id: value.toUpperCase()
        }
      });

      // checking field validity
      if (value === "") {
        this.setState({ isValidItemBankId: false });
      } else {
        this.setState({ isValidItemBankId: true });
      }

      // Create a buffer before updating the redux state
      // Clear the old typingTimer
      clearTimeout(this.state.typingTimer);

      // Change the typingTimer to a timeout of 200 millisec
      // When user stops writing, this will go through after that time
      // Otherwise, the timer will be reset to that time
      const newTimer = setTimeout(() => {
        this.updateRedux();
      }, 200);

      this.setState({ typingTimer: newTimer });
    }
  };

  /*--------------------------*/
  //    Modify Name Fields    //
  /*--------------------------*/
  onNameChange = (name, value) => {
    // Removing _name to get the language
    const language = name.replace("_name", "");

    // allow maximum of 150 characters
    const regexExpression = /^(.{0,150})$/;

    // allow maximum of 150 characters
    if (regexExpression.test(value)) {
      // Update the local states
      if (language === LANGUAGES.english) {
        this.setState({
          item_bank_definition: { ...this.state.item_bank_definition, en_name: value }
        });

        // checking field validity
        if (value === "") {
          this.setState({ isValidEnglishName: false });
        } else {
          this.setState({ isValidEnglishName: true });
        }
      } else if (language === LANGUAGES.french) {
        this.setState({
          item_bank_definition: { ...this.state.item_bank_definition, fr_name: value }
        });

        // checking field validity
        if (value === "") {
          this.setState({ isValidFrenchName: false });
        } else {
          this.setState({ isValidFrenchName: true });
        }
      }

      // Create a buffer before updating the redux state
      // Clear the old typingTimer
      clearTimeout(this.state.typingTimer);

      // Change the typingTimer to a timeout of 200 millisec
      // When user stops writing, this will go through after that time
      // Otherwise, the timer will be reset to that time
      const newTimer = setTimeout(() => {
        this.updateRedux();
      }, 200);

      this.setState({ typingTimer: newTimer });
    }
  };

  /*--------------------------*/
  //                          //
  //       Data Changes       //
  //                          //
  /*--------------------------*/

  /*--------------------------*/
  //    Update Redux Fields   //
  /*--------------------------*/
  updateRedux = () => {
    const newObj = { ...this.state.item_bank_definition };

    // new redux state values
    const temp_item_bank_data = _.cloneDeep(this.props.item_bank_data);
    temp_item_bank_data.item_bank_definition = [newObj];

    // updating redux states
    this.props.setItemBankData(temp_item_bank_data);
  };

  /*--------------------------*/
  //     Modify Database      //
  /*--------------------------*/
  updateDatabase = () => {
    // setting API data parameter
    const data = this.props.item_bank_data;

    data.element_to_update = ELEMENT_TO_UPDATE.itemBankDefinition;
    this.props.updateItemBankData(data).then(response => {
      if (response.ok) {
        // Set the value for the initial data of the Item Bank
        // This is used to detect changes on multiple components of Item Bank
        this.props.setItemBankInitialData(_.cloneDeep(data));

        // updating needed states
        this.setState({
          isValidItemBankId: true
        });
        // should never happen (except if you did not provide correctly all needed parameters)
      } else {
        throw new Error("Something went wrong during the update item bank data process");
      }
    });
  };

  /*--------------------------*/
  //       Apply Button       //
  /*--------------------------*/

  // Update the database with the current data if all field are valid
  // Otherwise, we focus and show the highest error field
  handleApplyChanges = () => {
    // if all fields are valid
    if (
      this.state.isValidEnglishName &&
      this.state.isValidFrenchName &&
      this.state.isValidItemBankId
    ) {
      // saving data
      this.updateDatabase();
    } else {
      // adding small delay to make sure that validation errors are displayed before focusing on respective field
      setTimeout(() => {
        // focus on highest invalid field
        this.focusOnHighestErrorField();
      }, 100);
    }
  };

  /*--------------------------*/
  //                          //
  //   Validation - Related   //
  //                          //
  /*--------------------------*/

  /*--------------------------*/
  //    Errors Fields Focus   //
  /*--------------------------*/

  // Should never happen since we are doing realtime validation
  // analyses field by field and focus on the highest error field
  focusOnHighestErrorField = () => {
    if (!this.state.isValidItemBankId) {
      document.getElementById("item-bank-definition-custom-item-bank-id-input").focus();
    } else if (!this.state.isValidEnglishName) {
      document.getElementById("item-bank-definition-en-name-input").focus();
    } else if (!this.state.isValidFrenchName) {
      document.getElementById("item-bank-definition-fr-name-input").focus();
    }
  };

  /*------------------------------------*/
  //   Compare New VS Old Redux States  //
  /*------------------------------------*/

  // will return true if changes are detected
  changesDetected = () => {
    if (
      _.isEqual(
        this.props.item_bank_definition,
        this.props.itemBankInitialData.item_bank_definition[0]
      )
    ) {
      return false;
    }
    return true;
  };

  render() {
    // calculating the height of the top tabs and above (without considering the nav bar)
    // =========================================================================================
    let heightOfBackToItemBankSelectionButton = 0;
    let heightOfTopTabs = 0;
    let topPageHeight = 0;
    if (document.getElementById("back-to-item-bank-selection-btn") != null) {
      heightOfBackToItemBankSelectionButton = document.getElementById(
        "back-to-item-bank-selection-btn"
      ).offsetHeight;
      if (document.getElementsByClassName("nav nav-tabs").length > 0) {
        heightOfTopTabs = document.getElementsByClassName("nav nav-tabs")[0].offsetHeight;
      }
    }

    topPageHeight += heightOfBackToItemBankSelectionButton + heightOfTopTabs + 24; // height of "back to item bank selection button" + height of margin under button

    const customHeight = getItemBankHeightCalculations(
      this.props.accommodations.fontSize,
      this.props.accommodations.spacing,
      this.props.testNavBarHeight,
      topPageHeight,
      0
    );
    // =========================================================================================

    return (
      <div style={{ ...styles.mainContainer, ...customHeight.maxHeight }}>
        <Container>
          {/* Item Bank ID */}
          <Row style={styles.rowStyle}>
            {makeLabel(
              "custom_item_bank_id",
              LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                .itemBankDefinition,
              "item-bank-definition-custom-item-bank-id",
              {},
              false
            )}
            {makeTextBoxField(
              "custom_item_bank_id",
              LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                .itemBankDefinition,
              this.state.item_bank_definition.custom_item_bank_id,
              this.state.isValidItemBankId,
              this.onItemBankIdChange,
              "item-bank-definition-custom-item-bank-id",
              true
            )}
          </Row>
          {/* English Name of Item Bank */}
          <Row style={styles.rowStyle}>
            {makeLabel(
              "en_name",
              LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                .itemBankDefinition,
              "item-bank-definition-en-name",
              {},
              false
            )}
            {makeTextBoxField(
              "en_name",
              LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                .itemBankDefinition,
              this.state.item_bank_definition.en_name,
              this.state.isValidEnglishName,
              this.onNameChange,
              "item-bank-definition-en-name"
            )}
          </Row>
          {/* French Name of Item Bank */}
          <Row style={styles.rowStyle}>
            {makeLabel(
              "fr_name",
              LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                .itemBankDefinition,
              "item-bank-definition-fr-name",
              {},
              false
            )}
            {makeTextBoxField(
              "fr_name",
              LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                .itemBankDefinition,
              this.state.item_bank_definition.fr_name,
              this.state.isValidFrenchName,
              this.onNameChange,
              "item-bank-definition-fr-name"
            )}
          </Row>
          <div style={styles.applyButtonContainer}>
            <CustomButton
              label={
                <>
                  <FontAwesomeIcon icon={faCheck} style={styles.buttonIcon} />
                  <span>
                    {this.changesDetected() ? LOCALIZE.commons.applyButton : LOCALIZE.commons.saved}
                  </span>
                </>
              }
              action={this.handleApplyChanges}
              customStyle={styles.applyButton}
              buttonTheme={this.changesDetected() ? THEME.PRIMARY : THEME.SUCCESS}
              disabled={
                !this.state.isValidEnglishName ||
                !this.state.isValidFrenchName ||
                !this.state.isValidItemBankId ||
                !this.changesDetected()
              }
            />
          </div>
        </Container>
      </div>
    );
  }
}

export { ItemBankDefinition as unconnectedItemBankDefinition };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    item_bank_data: state.itemBank.item_bank_data,
    item_bank_definition: state.itemBank.item_bank_data.item_bank_definition[0],
    itemBankInitialData: state.itemBank.itemBankInitialData,
    testNavBarHeight: state.testSection.testNavBarHeight,
    currentTab: state.navTabs.currentTopTab
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      modifyItemBankField,
      updateItemBankData,
      setItemBankData,
      setItemBankInitialData
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ItemBankDefinition);
