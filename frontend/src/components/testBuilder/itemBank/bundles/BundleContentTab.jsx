import React, { Component } from "react";
import * as _ from "lodash";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getItemBankHeightCalculations } from "../../../../helpers/inTestHeightCalculations";
import { getLineSpacingCSS } from "../../../../modules/AccommodationsRedux";
import { DragDropContext, Droppable } from "react-beautiful-dnd";
import CustomDraggable from "../../../commons/CustomDraggable/CustomDraggable";
import { setValidBundleBundlesRuleState, setBundleData } from "../../../../modules/ItemBankRedux";
import CustomButton, { THEME } from "../../../commons/CustomButton";
import { faPlusCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import LOCALIZE from "../../../../text_resources";
import SystemMessage, { MESSAGE_TYPE } from "../../../commons/SystemMessage";

export const BUNDLE_RULE_DROPPABLE_ID = {
  bundleContentBundleDroppable: "bundle-content-bundle-droppable",
  bundleContentBundleRuleDroppable: "bundle-content-bundle-rule-droppable",
  bundleContentBundleRuleContentDroppable: "bundle-content-bundle-rule-content-droppable"
};

const styles = {
  mainContainer: {
    width: "100%",
    padding: "24px 12px",
    border: "1px solid #cdcdcd",
    borderTop: "none",
    borderBottom: "none",
    overflowY: "auto",
    overflowX: "hidden"
  },
  createNewBundleRuleButtonContainer: {
    margin: "12px 24px"
  },
  buttonIcon: {
    marginRight: 6
  },
  systemMessageContainer: {
    margin: "12px 48px",
    fontWeight: "bold"
  }
};

// Reordering of a list after onDragEnd
const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

class BundleContentTab extends Component {
  state = {
    showCreateBundleRulePopup: false,
    disabledBundleContentBundleDroppable: false,
    disabledBundleContentBundleRuleDroppable: false,
    disabledBundleContentBundleRuleContentDroppable: false
  };

  componentDidMount = () => {
    // validating bundle rules
    this.validatingBundleRules();
  };

  componentDidUpdate = prevProps => {
    // if bundle_associations or bundle_bundles_rule_data gets updated
    if (
      prevProps.bundleData.bundle_associations !== this.props.bundleData.bundle_associations ||
      prevProps.bundleData.bundle_bundles_rule_data !==
        this.props.bundleData.bundle_bundles_rule_data
    ) {
      // validating bundle rules
      this.validatingBundleRules();
    }
  };

  // onDragEnd - Bundles
  onDragEndBundle(result) {
    // dropped outside the list
    if (!result.destination) {
      return;
    }

    // creating deep copy of bundleData
    const copyOfBundleData = _.cloneDeep(this.props.bundleData);

    // initializing needed variables
    let newBundles = [];
    let newBundleRules = [];
    let newBundleRuleAssociations = [];
    let filteredBundleItemsAssociationContent = [];
    let filteredBundleBundlesAssociationContent = [];
    let copyOfBundleBundlesRuleData = copyOfBundleData.bundle_bundles_rule_data;

    // getting respective content
    filteredBundleItemsAssociationContent = copyOfBundleData.bundle_associations.filter(obj => {
      return obj.system_id_link !== null;
    });
    filteredBundleBundlesAssociationContent = copyOfBundleData.bundle_associations.filter(obj => {
      return obj.system_id_link === null;
    });

    // source ID and destination ID are different (moving bundle between different sections)
    if (result.source.droppableId !== result.destination.droppableId) {
      // bundleContentBundleDroppable (bundle associations)
      if (result.source.droppableId === BUNDLE_RULE_DROPPABLE_ID.bundleContentBundleDroppable) {
        // initializing currentObj
        const currentObj = filteredBundleBundlesAssociationContent[result.source.index];
        // getting index of the rule destination
        const indexOfRuleDestination = result.destination.droppableId
          .split(BUNDLE_RULE_DROPPABLE_ID.bundleContentBundleRuleContentDroppable)[1]
          .split("-")[1];
        copyOfBundleBundlesRuleData[indexOfRuleDestination].bundle_bundles_rule_associations.splice(
          result.destination.index,
          0,
          currentObj
        );
        // removing selected bundle from the bundle associations
        filteredBundleBundlesAssociationContent.splice(result.source.index, 1);
      }
      // bundleContentBundleRuleContentDroppable (bundle rule associations)
      else if (
        result.source.droppableId.includes(
          BUNDLE_RULE_DROPPABLE_ID.bundleContentBundleRuleContentDroppable
        )
      ) {
        // destination is in the bundle associations
        if (
          result.destination.droppableId === BUNDLE_RULE_DROPPABLE_ID.bundleContentBundleDroppable
        ) {
          // getting index of the rule source
          const indexOfRuleSource = result.source.droppableId
            .split(BUNDLE_RULE_DROPPABLE_ID.bundleContentBundleRuleContentDroppable)[1]
            .split("-")[1];
          // initializing currentObj
          const currentObj =
            copyOfBundleBundlesRuleData[indexOfRuleSource].bundle_bundles_rule_associations[
              result.source.index
            ];
          // removing selected bundle from the bundle rule associations
          copyOfBundleBundlesRuleData[indexOfRuleSource].bundle_bundles_rule_associations.splice(
            result.source.index,
            1
          );
          // adding selected bundle to the bundle associations
          filteredBundleBundlesAssociationContent.splice(result.destination.index, 0, currentObj);
          // destination is in another bundle rule (as part of some bundle rule associations)
        } else {
          // getting index of the rule source
          const indexOfRuleSource = result.source.droppableId
            .split(BUNDLE_RULE_DROPPABLE_ID.bundleContentBundleRuleContentDroppable)[1]
            .split("-")[1];
          // initializing currentObj
          const currentObj =
            copyOfBundleBundlesRuleData[indexOfRuleSource].bundle_bundles_rule_associations[
              result.source.index
            ];
          // removing selected bundle from the bundle rule associations
          copyOfBundleBundlesRuleData[indexOfRuleSource].bundle_bundles_rule_associations.splice(
            result.source.index,
            1
          );
          // getting index of the rule destination
          const indexOfRuleDestination = result.destination.droppableId
            .split(BUNDLE_RULE_DROPPABLE_ID.bundleContentBundleRuleContentDroppable)[1]
            .split("-")[1];
          // adding selected bundle to the other bundle rule associations
          copyOfBundleBundlesRuleData[
            indexOfRuleDestination
          ].bundle_bundles_rule_associations.splice(result.destination.index, 0, currentObj);
        }
      }

      // updating display orders of copyOfBundleBundlesRuleData and bundle_bundles_rule_associations
      for (let i = 0; i < copyOfBundleBundlesRuleData.length; i++) {
        copyOfBundleBundlesRuleData[i].display_order = i + 1;
        // looping in bundle_bundles_rule_associations
        for (
          let j = 0;
          j < copyOfBundleBundlesRuleData[i].bundle_bundles_rule_associations.length;
          j++
        ) {
          copyOfBundleBundlesRuleData[i].bundle_bundles_rule_associations[j].display_order = j + 1;
        }
      }

      const mergedArrays = filteredBundleBundlesAssociationContent.concat(
        filteredBundleItemsAssociationContent
      );

      // updating the redux states
      // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
      const new_obj = {
        ...this.props.bundleData,
        bundle_associations: mergedArrays,
        bundle_bundles_rule_data: copyOfBundleBundlesRuleData
      };
      // setting new bundleData redux state
      this.props.setBundleData(new_obj);
      // source ID and destination ID are the same (moving bundle within the same section)
    } else {
      // source is bundleContentBundleDroppable (bundle assocations)
      if (result.source.droppableId === BUNDLE_RULE_DROPPABLE_ID.bundleContentBundleDroppable) {
        // reordering content
        newBundles = reorder(
          filteredBundleBundlesAssociationContent,
          result.source.index,
          result.destination.index
        );
        // updating display orders
        for (let i = 0; i < newBundles.length; i++) {
          newBundles[i].display_order = i + 1;
        }
        // updating bundle items associations content
        filteredBundleBundlesAssociationContent = newBundles;
        // source is bundleContentBundleRuleDroppable (bundle rules)
      } else if (
        result.source.droppableId === BUNDLE_RULE_DROPPABLE_ID.bundleContentBundleRuleDroppable
      ) {
        // reordering content
        newBundleRules = reorder(
          copyOfBundleBundlesRuleData,
          result.source.index,
          result.destination.index
        );
        // updating display orders
        for (let i = 0; i < newBundleRules.length; i++) {
          newBundleRules[i].display_order = i + 1;
        }
        // updating copyOfBundleBundlesRuleData
        copyOfBundleBundlesRuleData = newBundleRules;
        // source is bundleContentBundleRuleContentDroppable (bundle rule associations)
      } else if (
        result.source.droppableId.includes(
          BUNDLE_RULE_DROPPABLE_ID.bundleContentBundleRuleContentDroppable
        )
      ) {
        // getting index of the rule source
        const indexOfRuleSource = result.source.droppableId
          .split(BUNDLE_RULE_DROPPABLE_ID.bundleContentBundleRuleContentDroppable)[1]
          .split("-")[1];

        // reordering content
        newBundleRuleAssociations = reorder(
          copyOfBundleBundlesRuleData[indexOfRuleSource].bundle_bundles_rule_associations,
          result.source.index,
          result.destination.index
        );
        // updating display orders
        for (let i = 0; i < newBundleRuleAssociations.length; i++) {
          newBundleRuleAssociations[i].display_order = i + 1;
        }
        // updating copyOfBundleBundlesRuleData
        copyOfBundleBundlesRuleData[indexOfRuleSource].bundle_bundles_rule_associations =
          newBundleRuleAssociations;
        // should never happen
      } else {
        throw new Error("Unsupported draggable action");
      }

      const mergedArrays = filteredBundleBundlesAssociationContent.concat(
        filteredBundleItemsAssociationContent
      );
      // updating the redux states
      // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
      const new_obj = {
        ...this.props.bundleData,
        bundle_associations: mergedArrays,
        bundle_bundles_rule_data: copyOfBundleBundlesRuleData
      };
      // setting new bundleData redux state
      this.props.setBundleData(new_obj);
    }
  }

  onDragStartBundle(result) {
    // bundleContentBundleDroppable (bundle associations)
    if (result.source.droppableId === BUNDLE_RULE_DROPPABLE_ID.bundleContentBundleDroppable) {
      this.setState({
        disabledBundleContentBundleDroppable: false,
        disabledBundleContentBundleRuleDroppable: true,
        disabledBundleContentBundleRuleContentDroppable: false
      });
    }
    // bundleContentBundleRuleDroppable (bundle rules)
    else if (
      result.source.droppableId === BUNDLE_RULE_DROPPABLE_ID.bundleContentBundleRuleDroppable
    ) {
      this.setState({
        disabledBundleContentBundleDroppable: true,
        disabledBundleContentBundleRuleDroppable: false,
        disabledBundleContentBundleRuleContentDroppable: true
      });
    }
    // bundleContentBundleRuleContentDroppable (bundle rule associations)
    else if (
      result.source.droppableId.includes(
        BUNDLE_RULE_DROPPABLE_ID.bundleContentBundleRuleContentDroppable
      )
    ) {
      this.setState({
        disabledBundleContentBundleDroppable: false,
        disabledBundleContentBundleRuleDroppable: true,
        disabledBundleContentBundleRuleContentDroppable: false
      });
    }
  }

  // Remove bundle from bundle_associations
  removeBundle = index => {
    // creating deep copy of bundleData
    const copyOfBundleData = _.cloneDeep(this.props.bundleData);

    // initializing needed variables
    const newBundles = [];
    let filteredBundleItemsAssociationContent = [];
    let filteredBundleBundlesAssociationContent = [];

    // getting respective content
    filteredBundleItemsAssociationContent = copyOfBundleData.bundle_associations.filter(obj => {
      return obj.system_id_link !== null;
    });
    filteredBundleBundlesAssociationContent = copyOfBundleData.bundle_associations.filter(obj => {
      return obj.system_id_link === null;
    });

    // deleting respective item
    filteredBundleBundlesAssociationContent.splice(index, 1);

    // re-ordering display order
    for (let i = 0; i < filteredBundleBundlesAssociationContent.length; i++) {
      const data = filteredBundleBundlesAssociationContent[i];
      data.display_order = i + 1;
      newBundles.push(data);
    }

    // updating bundle items associations content
    filteredBundleBundlesAssociationContent = newBundles;

    const mergedArrays = filteredBundleBundlesAssociationContent.concat(
      filteredBundleItemsAssociationContent
    );

    // updating the redux states
    // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
    const new_obj = {
      ...this.props.bundleData,
      bundle_associations: mergedArrays
    };
    // setting new bundleData redux state
    this.props.setBundleData(new_obj);
  };

  // Remove bundle rule
  removeBundleRule = index => {
    // creating deep copy of bundleData
    const copyOfBundleData = _.cloneDeep(this.props.bundleData);

    // initializing needed variables
    const copyOfBundleBundlesRuleData = copyOfBundleData.bundle_bundles_rule_data;
    const newBundles = [];
    let filteredBundleItemsAssociationContent = [];
    let filteredBundleBundlesAssociationContent = [];

    // looping in respective bundle bundles rule associations
    for (
      let i = 0;
      i < copyOfBundleBundlesRuleData[index].bundle_bundles_rule_associations.length;
      i++
    ) {
      // adding current rule association to the bundle associations list
      copyOfBundleData.bundle_associations.push(
        copyOfBundleBundlesRuleData[index].bundle_bundles_rule_associations[i]
      );
    }

    // deleting respective rule
    copyOfBundleBundlesRuleData.splice(index, 1);

    // getting respective content
    filteredBundleItemsAssociationContent = copyOfBundleData.bundle_associations.filter(obj => {
      return obj.system_id_link !== null;
    });
    filteredBundleBundlesAssociationContent = copyOfBundleData.bundle_associations.filter(obj => {
      return obj.system_id_link === null;
    });

    // re-ordering display order
    for (let i = 0; i < filteredBundleBundlesAssociationContent.length; i++) {
      const data = filteredBundleBundlesAssociationContent[i];
      data.display_order = i + 1;
      newBundles.push(data);
    }

    // updating bundle items associations content
    filteredBundleBundlesAssociationContent = newBundles;

    const mergedArrays = filteredBundleBundlesAssociationContent.concat(
      filteredBundleItemsAssociationContent
    );

    // updating the redux states
    // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
    const new_obj = {
      ...this.props.bundleData,
      bundle_associations: mergedArrays,
      bundle_bundles_rule_data: copyOfBundleBundlesRuleData
    };
    // setting new bundleData redux state
    this.props.setBundleData(new_obj);
  };

  validatingBundleRules = () => {
    // initializing validBundleRules
    let validBundleRules = true;

    // looping in bundle_bundles_rule_data
    for (let i = 0; i < this.props.bundleData.bundle_bundles_rule_data.length; i++) {
      // bundle_bundles_rule_associations is empty
      if (
        this.props.bundleData.bundle_bundles_rule_data[i].bundle_bundles_rule_associations.length <=
        0
      ) {
        // updating validBundleRules
        validBundleRules = false;
        // breaking the loop
        break;
      }
      // number of bundles is 0 or greater than number of bundles in bundle_bundles_rule_associations
      if (
        this.props.bundleData.bundle_bundles_rule_data[i].number_of_bundles === 0 ||
        this.props.bundleData.bundle_bundles_rule_data[i].number_of_bundles >
          this.props.bundleData.bundle_bundles_rule_data[i].bundle_bundles_rule_associations.length
      ) {
        // updating validBundleRules
        validBundleRules = false;
        // breaking the loop
        break;
      }
    }

    this.props.setValidBundleBundlesRuleState(validBundleRules);
  };

  // Draggable Styling
  getItemStyle = (isDragging, draggableStyle) => ({
    // some basic styles to make the items look a bit nicer
    userSelect: "none",

    // change background colour if dragging
    background: isDragging ? "#0099A8" : "white",
    color: "#00565e",

    // styles we need to apply on draggables
    ...draggableStyle
  });

  // Droppable Styling
  getListStyle = isDraggingOver => ({
    background: isDraggingOver ? "#cdcdcd" : "white",
    padding: "12px 24px 12px 24px"
  });

  handleCreateNewBundleRule = () => {
    // creating deep copy of bundleData
    const copyOfBundleData = _.cloneDeep(this.props.bundleData);

    // initializing new_bundle_bundles_rule_data
    let new_bundle_bundles_rule_data = [];

    // bundle_bundles_rule_data is empty (meaning that there are no bundle rules yet)
    if (copyOfBundleData.bundle_bundles_rule_data !== "") {
      // setting new_bundle_bundles_rule_data
      new_bundle_bundles_rule_data = copyOfBundleData.bundle_bundles_rule_data;
    }

    new_bundle_bundles_rule_data.push({
      id: `bundle-rule-${copyOfBundleData.bundle_bundles_rule_data.length + 1}`,
      number_of_bundles: null,
      shuffle_bundles: false,
      keep_items_together: false,
      bundle_rule_id: null,
      bundle_bundles_rule_associations: [],
      display_order: copyOfBundleData.bundle_bundles_rule_data.length + 1
    });

    // updating the redux states
    // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
    const new_obj = {
      ...this.props.bundleData,
      bundle_bundles_rule_data: new_bundle_bundles_rule_data
    };
    // setting new bundleData redux state
    this.props.setBundleData(new_obj);
  };

  render() {
    // calculating the height of the top tabs and above (without considering the nav bar)
    // =========================================================================================
    let heightOfBackToItemBankSelectionButton = 0;
    let heightOfTopTabs = 0;
    let heightOfFooter = 0;
    let topPageHeight = 0;
    if (document.getElementById("back-to-item-bank-editor-btn") != null) {
      heightOfBackToItemBankSelectionButton = document.getElementById(
        "back-to-item-bank-editor-btn"
      ).offsetHeight;
      if (document.getElementsByClassName("nav nav-tabs").length > 0) {
        heightOfTopTabs = document.getElementsByClassName("nav nav-tabs")[0].offsetHeight;
      }
    } else {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }

    topPageHeight += heightOfBackToItemBankSelectionButton + heightOfTopTabs + 12; // height of "back to item bank selection button" + height of margin under button

    if (document.getElementById("item-bank-footer-main-div") !== null) {
      heightOfFooter = document.getElementById("item-bank-footer-main-div").offsetHeight;
    } else {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }

    const customHeight = getItemBankHeightCalculations(
      this.props.accommodations.fontSize,
      this.props.accommodations.spacing,
      this.props.testNavBarHeight,
      topPageHeight,
      heightOfFooter
    );
    // =========================================================================================

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <div style={{ ...styles.mainContainer, ...customHeight.height }}>
        <div>
          <div style={styles.createNewBundleRuleButtonContainer}>
            <CustomButton
              buttonId={"item-bank-bundles-create-new-bundle-rule-btn"}
              label={
                <>
                  <FontAwesomeIcon icon={faPlusCircle} style={styles.buttonIcon} />
                  <span>
                    {
                      LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                        .bundles.bundleContent.createNewBundleRuleButton
                    }
                  </span>
                </>
              }
              action={this.handleCreateNewBundleRule}
              buttonTheme={THEME.PRIMARY}
            />
          </div>
          <div>
            <DragDropContext
              onDragStart={result => {
                this.onDragStartBundle(result);
              }}
              onDragEnd={result => {
                this.onDragEndBundle(result);
              }}
            >
              <Droppable
                droppableId={BUNDLE_RULE_DROPPABLE_ID.bundleContentBundleRuleDroppable}
                isDropDisabled={this.state.disabledBundleContentBundleRuleDroppable}
              >
                {(provided, snapshot) => (
                  <div
                    {...provided.droppableProps}
                    ref={provided.innerRef}
                    style={this.getListStyle(snapshot.isDraggingOver)}
                  >
                    {this.props.bundleData.bundle_bundles_rule_data.map((bundleRule, index) => (
                      <CustomDraggable
                        key={`bundle-content-bundle-rule-key-${index}`}
                        bundleBundlesRuleData={bundleRule}
                        index={index}
                        getItemStyle={this.getItemStyle}
                        deleteItem={() => this.removeBundleRule(index)}
                        customDeleteButtonClass="align-items-center"
                        disabledBundleContentBundleRuleContentDroppable={
                          this.state.disabledBundleContentBundleRuleContentDroppable
                        }
                      />
                    ))}
                    {provided.placeholder}
                  </div>
                )}
              </Droppable>
              {this.props.bundleData.bundle_bundles_rule_data.length > 0 &&
                this.props.bundleData.bundle_associations.filter(obj => obj.system_id_link === null)
                  .length > 0 && (
                  <div style={styles.systemMessageContainer}>
                    <SystemMessage
                      messageType={MESSAGE_TYPE.info}
                      message={
                        <p className="notranslate">
                          {
                            LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                              .bundles.bundleContent.bundleAssociationsSystemMessage
                          }
                        </p>
                      }
                    />
                  </div>
                )}
              <Droppable
                droppableId={BUNDLE_RULE_DROPPABLE_ID.bundleContentBundleDroppable}
                isDropDisabled={this.state.disabledBundleContentBundleDroppable}
              >
                {(provided, snapshot) => (
                  <div
                    {...provided.droppableProps}
                    ref={provided.innerRef}
                    style={this.getListStyle(snapshot.isDraggingOver)}
                  >
                    {this.props.bundleData.bundle_associations
                      .filter(obj => {
                        return obj.system_id_link === null;
                      })
                      .map((bundle, index) => (
                        <CustomDraggable
                          key={`bundle-content-bundle-key-${index}`}
                          bundleAssociatedBundleData={bundle}
                          index={index}
                          getItemStyle={this.getItemStyle}
                          deleteItem={() => this.removeBundle(index)}
                          customDeleteButtonClass="align-items-center"
                        />
                      ))}
                    {provided.placeholder}
                  </div>
                )}
              </Droppable>
            </DragDropContext>
          </div>
        </div>
      </div>
    );
  }
}

export { BundleContentTab as unconnectedBundleContentTab };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    testNavBarHeight: state.testSection.testNavBarHeight,
    bundleData: state.itemBank.bundleData
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setBundleData,
      setValidBundleBundlesRuleState
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(BundleContentTab);
