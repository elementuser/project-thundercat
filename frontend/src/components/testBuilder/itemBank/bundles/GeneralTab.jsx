import React, { Component } from "react";
import * as _ from "lodash";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getItemBankHeightCalculations } from "../../../../helpers/inTestHeightCalculations";
import LOCALIZE from "../../../../text_resources";
import { getLineSpacingCSS } from "../../../../modules/AccommodationsRedux";
import DropdownSelect from "../../../commons/DropdownSelect";
import PopupBox, { BUTTON_STATE, BUTTON_TYPE } from "../../../commons/PopupBox";
import SystemMessage, { MESSAGE_TYPE } from "../../../commons/SystemMessage";
import { faCheck, faPencilAlt, faTimes } from "@fortawesome/free-solid-svg-icons";
import {
  setBundleData,
  setDataReadyForUpload,
  setItemBankData,
  getSpecificBundle,
  setBundleInitialData
} from "../../../../modules/ItemBankRedux";
import Switch from "react-switch";
import getSwitchTransformScale from "../../../../helpers/switchTransformScale";
import { Col, Row } from "react-bootstrap";
import objectEqualsCheck from "../../../../helpers/objectHelpers";
import StyledTooltip, { EFFECT, TRIGGER_TYPE, TYPE } from "../../../authentication/StyledTooltip";
import CustomButton, { THEME } from "../../../commons/CustomButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const styles = {
  mainContainer: {
    width: "100%",
    padding: "24px 12px",
    border: "1px solid #cdcdcd",
    borderTop: "none",
    borderBottom: "none",
    overflowY: "auto",
    overflowX: "hidden"
  },
  dropdownDiv: {
    marginBottom: 12
  },
  label: {
    fontWeight: "bold",
    padding: "0 0 2px 2px"
  },
  historicalIdLabel: {
    padding: 0
  },
  labelFloatContainer: {
    width: "100%"
  },
  actionLabel: {
    marginRight: 6
  },
  actionButtons: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "0px 6px",
    color: "#00565e",
    fontSize: 20,
    float: "right"
  },
  allUnset: {
    all: "unset"
  },
  bundleVersionInput: {
    minHeight: 38,
    border: "1px solid #00565e",
    padding: "3px 6px 3px 6px",
    borderRadius: 4,
    height: "100%",
    width: "100%"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6,
    width: "100%"
  },
  switchesSection: {
    marginTop: 200
  },
  bundleNameInputContainer: {
    padding: "15px 40px 15px 40px"
  },
  bundleNameInput: {
    minHeight: 38,
    border: "1px solid #00565e",
    padding: "3px 6px 3px 6px",
    borderRadius: 4,
    height: "100%",
    width: "100%"
  }
};

class GeneralTab extends Component {
  state = {
    triggerRerender: false,
    isLoading: false,
    bundleOptions: [],
    showChangeBundleConfirmationPopup: false,
    selectedBundleData: {},
    bundleVersionContent: this.props.bundleData.version_text,
    typingTimer: 0,
    showEditBundleNamePopup: false,
    bundleNameContent: this.props.bundleData.name,
    displayInvalidBundleNameError: false,
    // default values
    switchHeight: 25,
    switchWidth: 50,
    disabledShuffleItems: false,
    disabledShuffleBundlesSwitches: false
  };

  componentDidMount = () => {
    // populating bundle options
    this.populateBundleOptions();
    this.getUpdatedSwitchDimensions();
    // checking for changes
    this.changesDetected();
    // getting disabled shuffle items switch state
    this.getShuffleItemsSwitchDisabledState();
    // getting disabled shuffle bundles switch state
    this.getShuffleBundlesSwitchDisabledState();
  };

  componentDidUpdate = (prevProps, prevState) => {
    // if bundleData gets updated
    if (prevProps.bundleData !== this.props.bundleData) {
      // checking for changes
      this.changesDetected();
      // getting disabled shuffle items switch state
      this.getShuffleItemsSwitchDisabledState();
      // getting disabled shuffle bundles switch state
      this.getShuffleBundlesSwitchDisabledState();
      this.setState({
        bundleVersionContent: this.props.bundleData.version_text,
        bundleNameContent: this.props.bundleData.name
      });
    }
    // if item_bank_data gets updated
    if (prevProps.item_bank_data !== this.props.item_bank_data) {
      // populating bundle options
      this.populateBundleOptions();
    }
    // if fontSize gets updated
    if (prevProps.accommodations.fontSize !== this.props.accommodations.fontSize) {
      this.getUpdatedSwitchDimensions();
    }
    // if disabledShuffleItems state gets updated
    if (prevState.disabledShuffleItems !== this.state.disabledShuffleItems) {
      // if switch is being disabled
      if (this.state.disabledShuffleItems) {
        // updating redux states
        // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
        const new_obj = {
          ...this.props.bundleData,
          shuffle_items: false
        };

        // setting new bundleData redux state
        this.props.setBundleData(new_obj);
      }
    }
    // if disabledShuffleBundlesSwitches state gets updated
    if (prevState.disabledShuffleBundlesSwitches !== this.state.disabledShuffleBundlesSwitches) {
      // if switch is being disabled
      if (this.state.disabledShuffleBundlesSwitches) {
        // updating redux states
        // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
        const new_obj = {
          ...this.props.bundleData,
          shuffle_bundles: false,
          shuffle_between_bundles: false
        };

        // setting new bundleData redux state
        this.props.setBundleData(new_obj);
      }
    }
  };

  // getting switch dimensions and updating states
  getUpdatedSwitchDimensions = () => {
    // need this loading state, otherwise the switch diameter is not calculated on state change
    this.setState({ isLoading: true }, () => {
      this.setState(
        {
          switchHeight: getSwitchTransformScale(this.props.accommodations.fontSize).height,
          switchWidth: getSwitchTransformScale(this.props.accommodations.fontSize).width
        },
        () => {
          this.setState({ isLoading: false });
        }
      );
    });
  };

  populateBundleOptions = () => {
    const bundleOptions = [];

    // looping in item bank bundles
    for (let i = 0; i < this.props.item_bank_data.list_of_bundles.length; i++) {
      // populating array
      bundleOptions.push({
        label: this.props.item_bank_data.list_of_bundles[i].bundle_name,
        value: this.props.item_bank_data.list_of_bundles[i].id,
        id: this.props.item_bank_data.list_of_bundles[i].id
      });
      // }
    }

    // properly ordering the bundles
    bundleOptions.sort((a, b) => a.name - b.name);

    this.setState({ bundleOptions: bundleOptions });
  };

  changeBundle = selectedOption => {
    if (typeof selectedOption !== "undefined") {
      // getting and setting new selected item
      this.props.getSpecificBundle(selectedOption.id).then(response => {
        const next_bundle = response;

        this.props.setBundleData(next_bundle);
        this.props.setBundleInitialData(next_bundle);

        // updating ready for upload redux states
        this.props.setDataReadyForUpload(false);
      });
    }
  };

  openChangeBundleConfirmationPopup = selectedOption => {
    this.setState({
      showChangeBundleConfirmationPopup: true,
      selectedBundleData: selectedOption
    });
  };

  closeChangeBundleConfirmationPopup = () => {
    this.setState({ showChangeBundleConfirmationPopup: false });
  };

  updateBundleVersionContent = event => {
    const bundleVersionContent = event.target.value;
    // allow only alphanumeric, slash, underscore and dash (0 to 50 chars)
    const regexExpression = /^([a-zA-z0-9-/_]{0,50})$/;
    if (regexExpression.test(bundleVersionContent)) {
      this.setState({
        bundleVersionContent: bundleVersionContent.toUpperCase()
      });
    }

    // Create a buffer before updating the redux state
    // Clear the old typingTimer
    clearTimeout(this.state.typingTimer);

    // Change the typingTimer to a timeout of 300 millisec
    // When user stops writing, this will go through after that time
    // Otherwise, the timer will be reset to that time
    const newTimer = setTimeout(() => {
      // updating redux states
      // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
      const new_obj = {
        ...this.props.bundleData,
        version_text: this.state.bundleVersionContent
      };

      // setting new bundleData redux state
      this.props.setBundleData(new_obj);
    }, 200);

    this.setState({ typingTimer: newTimer });
  };

  changeShuffleItems = event => {
    // updating redux states
    // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
    const new_obj = {
      ...this.props.bundleData,
      shuffle_items: event
    };

    // setting new bundleData redux state
    this.props.setBundleData(new_obj);
  };

  getShuffleItemsSwitchDisabledState = () => {
    // initializing disabledShuffleItems
    let disabledShuffleItems = false;

    // if there is no associated items
    if (
      this.props.bundleData.bundle_associations.filter(obj => obj.system_id_link !== null).length <=
      0
    ) {
      // setting disabledShuffleItems to true
      disabledShuffleItems = true;
    }

    // updating state
    this.setState({ disabledShuffleItems: disabledShuffleItems });
  };

  changeShuffleBundles = event => {
    // updating redux states
    // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
    const new_obj = {
      ...this.props.bundleData,
      // mutually exclusive fields
      shuffle_bundles: event,
      shuffle_between_bundles: event ? false : this.props.bundleData.shuffle_between_bundles
    };

    // setting new bundleData redux state
    this.props.setBundleData(new_obj);
  };

  changeShuffleBetweenBundles = event => {
    // updating redux states
    // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
    const new_obj = {
      ...this.props.bundleData,
      // mutually exclusive fields
      shuffle_bundles: event ? false : this.props.bundleData.shuffle_bundles,
      shuffle_between_bundles: event
    };

    // setting new bundleData redux state
    this.props.setBundleData(new_obj);
  };

  getShuffleBundlesSwitchDisabledState = () => {
    // initializing disabledShuffleBundlesSwitches
    let disabledShuffleBundlesSwitches = false;

    // if there is no associated bundles and no rule
    if (
      this.props.bundleData.bundle_associations.filter(obj => obj.system_id_link === null).length <=
        0 &&
      this.props.bundleData.bundle_bundles_rule_data.length <= 0
    ) {
      // setting disabledShuffleBundlesSwitches to true
      disabledShuffleBundlesSwitches = true;
    }

    // updating state
    this.setState({ disabledShuffleBundlesSwitches: disabledShuffleBundlesSwitches });
  };

  changeActiveState = event => {
    // updating redux states
    // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
    const new_obj = {
      ...this.props.bundleData,
      active: event
    };

    // setting new bundleData redux state
    this.props.setBundleData(new_obj);
  };

  changesDetected = () => {
    // removing needed attributes from the comparison
    const copyOfBundleData = _.cloneDeep(this.props.bundleData);
    // looping in bundle_associations
    for (let i = 0; i < copyOfBundleData.bundle_associations.length; i++) {
      // removing non useful comparison attributes
      delete copyOfBundleData.bundle_associations[i].bundle_items_association_id;
      delete copyOfBundleData.bundle_associations[i].bundle_bundles_association_id;
      delete copyOfBundleData.bundle_associations[i].bundle_link_id;
      delete copyOfBundleData.bundle_associations[i].version;
      if (copyOfBundleData.bundle_associations[i].bundle_data !== null) {
        delete copyOfBundleData.bundle_associations[i].bundle_data.bundle_associations;
        delete copyOfBundleData.bundle_associations[i].bundle_data.bundle_count;
        delete copyOfBundleData.bundle_associations[i].bundle_data.item_count;
      }
    }
    const copyOfBundleInitialData = _.cloneDeep(this.props.bundleInitialData);
    // looping in bundle_associations
    for (let i = 0; i < copyOfBundleInitialData.bundle_associations.length; i++) {
      // removing non useful comparison attributes
      delete copyOfBundleInitialData.bundle_associations[i].bundle_items_association_id;
      delete copyOfBundleInitialData.bundle_associations[i].bundle_bundles_association_id;
      delete copyOfBundleInitialData.bundle_associations[i].bundle_link_id;
      delete copyOfBundleInitialData.bundle_associations[i].version;
      if (copyOfBundleInitialData.bundle_associations[i].bundle_data !== null) {
        delete copyOfBundleInitialData.bundle_associations[i].bundle_data.bundle_associations;
        delete copyOfBundleInitialData.bundle_associations[i].bundle_data.bundle_count;
        delete copyOfBundleInitialData.bundle_associations[i].bundle_data.item_count;
      }
    }

    let changesDetected = false;
    if (!objectEqualsCheck(copyOfBundleInitialData, copyOfBundleData)) {
      changesDetected = true;
    }
    this.props.setDataReadyForUpload(changesDetected);
    this.setState({ triggerRerender: !this.state.triggerRerender });
  };

  openEditBundleNamePopup = () => {
    this.setState({ showEditBundleNamePopup: true });
  };

  closeEditBundleNamePopup = () => {
    this.setState({
      showEditBundleNamePopup: false,
      displayInvalidBundleNameError: false,
      bundleNameContent: this.props.bundleData.name
    });
  };

  updateBundleNameContent = event => {
    const bundleNameContent = event.target.value;
    // allow only alphanumeric, slash, underscore and dash (0 to 50 chars)
    const regexExpression = /^([a-zA-z0-9-/_]{0,50})$/;
    if (regexExpression.test(bundleNameContent)) {
      let displayInvalidBundleNameError = false;
      // bundleNameContent is empty
      if (bundleNameContent === "") {
        // set displayInvalidBundleNameError to true
        displayInvalidBundleNameError = true;
      }
      this.setState(
        {
          bundleNameContent: bundleNameContent.toUpperCase(),
          displayInvalidBundleNameError: displayInvalidBundleNameError
        },
        () => {
          // Create a buffer before updating the redux state
          // Clear the old typingTimer
          clearTimeout(this.state.typingTimer);

          // Change the typingTimer to a timeout of 300 millisec
          // When user stops writing, this will go through after that time
          // Otherwise, the timer will be reset to that time
          const newTimer = setTimeout(() => {
            // bundle name is valid (not empty)
            if (!this.state.displayInvalidBundleNameError) {
              // updating redux states
              // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
              const new_obj = {
                ...this.props.bundleData,
                name: this.state.bundleNameContent
              };

              // setting new bundleData redux state
              this.props.setBundleData(new_obj);

              setTimeout(() => {
                // populating bundle options
                this.populateBundleOptions();
              }, 250);
            }
          }, 200);

          this.setState({ typingTimer: newTimer });
        }
      );
    }
  };

  render() {
    // calculating the height of the top tabs and above (without considering the nav bar)
    // =========================================================================================
    let heightOfBackToItemBankSelectionButton = 0;
    let heightOfTopTabs = 0;
    let heightOfFooter = 0;
    let topPageHeight = 0;
    if (document.getElementById("back-to-item-bank-editor-btn") != null) {
      heightOfBackToItemBankSelectionButton = document.getElementById(
        "back-to-item-bank-editor-btn"
      ).offsetHeight;
      if (document.getElementsByClassName("nav nav-tabs").length > 0) {
        heightOfTopTabs = document.getElementsByClassName("nav nav-tabs")[0].offsetHeight;
      }
    } else {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }

    topPageHeight += heightOfBackToItemBankSelectionButton + heightOfTopTabs + 12; // height of "back to item bank selection button" + height of margin under button

    if (document.getElementById("item-bank-footer-main-div") !== null) {
      heightOfFooter = document.getElementById("item-bank-footer-main-div").offsetHeight;
    } else {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }

    const customHeight = getItemBankHeightCalculations(
      this.props.accommodations.fontSize,
      this.props.accommodations.spacing,
      this.props.testNavBarHeight,
      topPageHeight,
      heightOfFooter
    );
    // =========================================================================================

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <div style={{ ...styles.mainContainer, ...customHeight.height }}>
        <div style={styles.dropdownDiv}>
          <label
            id="item-bank-selected-bundle-general-bundle-name-label"
            style={{ ...styles.label, ...styles.labelFloatContainer }}
          >
            <span style={styles.actionLabel}>
              {
                LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles
                  .general.bundleNameLabel
              }
            </span>
            <span>
              <StyledTooltip
                id="edit-bundle-name-tooltip"
                place="top"
                type={TYPE.light}
                effect={EFFECT.solid}
                triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      dataTip=""
                      dataFor="edit-bundle-name-tooltip"
                      label={
                        <>
                          <FontAwesomeIcon icon={faPencilAlt} />
                        </>
                      }
                      customStyle={styles.actionButtons}
                      action={this.openEditBundleNamePopup}
                      buttonTheme={THEME.PRIMARY}
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>
                      {
                        LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                          .bundles.general.editBundleNameTooltip
                      }
                    </p>
                  </div>
                }
              />
            </span>
          </label>
          <DropdownSelect
            idPrefix="item-bank-selected-bundle-general-bundle-name"
            ariaLabelledBy="item-bank-selected-bundle-general-bundle-name-label"
            options={this.state.bundleOptions}
            onChange={
              this.props.dataReadyForUpload
                ? this.openChangeBundleConfirmationPopup
                : this.changeBundle
            }
            defaultValue={{
              label: this.props.bundleData.name,
              value: this.props.bundleData.id
            }}
            hasPlaceholder={false}
            orderByLabels={false}
            orderByValues={false}
          ></DropdownSelect>
        </div>
        <div style={styles.dropdownDiv}>
          <label id="item-bank-selected-bundle-general-bundle-version-label" style={styles.label}>
            {
              LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles
                .general.bundleVersionLabel
            }
          </label>
          <input
            id="item-bank-selected-bundle-general-bundle-version-input"
            className={"valid-field"}
            ariaLabelledBy="item-bank-selected-bundle-general-bundle-version-label"
            style={{ ...styles.bundleVersionInput, ...accommodationsStyle }}
            type="text"
            value={this.state.bundleVersionContent}
            onChange={this.updateBundleVersionContent}
          ></input>
        </div>
        <div style={styles.switchesSection}>
          <div style={styles.dropdownDiv}>
            <Row className="align-items-center">
              <Col xl={7} lg={7} md={6} sm={6} xs={6}>
                <label
                  id="item-bank-selected-bundle-general-shuffle-items-label"
                  style={styles.label}
                >
                  {
                    LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles
                      .general.shuffleItemsLabel
                  }
                </label>
              </Col>
              <Col className="text-right" xl={5} lg={5} md={6} sm={6} xs={6}>
                {!this.state.isLoading && (
                  <Switch
                    onChange={this.changeShuffleItems}
                    checked={this.props.bundleData.shuffle_items}
                    aria-labelledby="item-bank-selected-bundle-general-shuffle-items-label"
                    height={this.state.switchHeight}
                    width={this.state.switchWidth}
                    disabled={this.state.disabledShuffleItems}
                  />
                )}
              </Col>
            </Row>
          </div>
          <div style={styles.dropdownDiv}>
            <Row className="align-items-center">
              <Col xl={7} lg={7} md={6} sm={6} xs={6}>
                <label
                  id="item-bank-selected-bundle-general-shuffle-bundles-label"
                  style={styles.label}
                >
                  {
                    LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles
                      .general.shuffleBundlesLabel
                  }
                </label>
              </Col>
              <Col className="text-right" xl={5} lg={5} md={6} sm={6} xs={6}>
                {!this.state.isLoading && (
                  <Switch
                    onChange={this.changeShuffleBundles}
                    checked={this.props.bundleData.shuffle_bundles}
                    aria-labelledby="item-bank-selected-bundle-general-shuffle-bundles-label"
                    height={this.state.switchHeight}
                    width={this.state.switchWidth}
                    disabled={this.state.disabledShuffleBundlesSwitches}
                  />
                )}
              </Col>
            </Row>
          </div>
          <div style={styles.dropdownDiv}>
            <Row className="align-items-center">
              <Col xl={7} lg={7} md={6} sm={6} xs={6}>
                <label
                  id="item-bank-selected-bundle-general-shuffle-between-bundles-label"
                  style={styles.label}
                >
                  {
                    LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles
                      .general.shuffleBetweenBundlesLabel
                  }
                </label>
              </Col>
              <Col className="text-right" xl={5} lg={5} md={6} sm={6} xs={6}>
                {!this.state.isLoading && (
                  <Switch
                    onChange={this.changeShuffleBetweenBundles}
                    checked={this.props.bundleData.shuffle_between_bundles}
                    aria-labelledby="item-bank-selected-bundle-general-shuffle-between-bundles-label"
                    height={this.state.switchHeight}
                    width={this.state.switchWidth}
                    // disabled={this.state.disabledShuffleBundlesSwitches}
                    // TODO: put back the disabled logic once the shuffle between bundles logic will be implemented
                    disabled={true}
                  />
                )}
              </Col>
            </Row>
          </div>
          <div style={styles.dropdownDiv}>
            <Row className="align-items-center">
              <Col xl={7} lg={7} md={6} sm={6} xs={6}>
                <label id="item-bank-selected-bundle-general-active-label" style={styles.label}>
                  {
                    LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles
                      .general.activeLabel
                  }
                </label>
              </Col>
              <Col className="text-right" xl={5} lg={5} md={6} sm={6} xs={6}>
                {!this.state.isLoading && (
                  <Switch
                    onChange={this.changeActiveState}
                    checked={this.props.bundleData.active}
                    aria-labelledby="item-bank-selected-bundle-general-active-label"
                    height={this.state.switchHeight}
                    width={this.state.switchWidth}
                  />
                )}
              </Col>
            </Row>
          </div>
        </div>
        <PopupBox
          show={this.state.showChangeBundleConfirmationPopup}
          handleClose={this.closeChangeBundleConfirmationPopup}
          title={
            LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles
              .changeBundleConfirmationPopup.changeBundleTitle
          }
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.warning}
                title={LOCALIZE.commons.warning}
                message={
                  <p>
                    {
                      LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                        .bundles.changeBundleConfirmationPopup.systemMessageDescription
                    }
                  </p>
                }
              />
              <p>
                {
                  LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles
                    .changeBundleConfirmationPopup.description
                }
              </p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.confirm}
          rightButtonAction={() => this.changeBundle(this.state.selectedBundleData)}
          rightButtonIcon={faCheck}
          size="lg"
        />

        <PopupBox
          show={this.state.showEditBundleNamePopup}
          handleClose={() => {}}
          title={
            LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles.general
              .editBundleNamePopup.title
          }
          description={
            <div>
              <p>
                {
                  LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles
                    .general.editBundleNamePopup.description
                }
              </p>
              <div>
                <Row role="presentation" style={styles.bundleNameInputContainer}>
                  <Col className="align-self-center" xl={4} lg={4} md={12} sm={12}>
                    <label
                      id="bundle-name-label"
                      htmlFor="bundle-name-label-input"
                      style={styles.historicalIdLabel}
                    >
                      {
                        LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                          .bundles.general.editBundleNamePopup.bundleNameLabel
                      }
                    </label>
                  </Col>
                  <Col className="align-self-center" xl={8} lg={8} md={12} sm={12}>
                    <input
                      id="bundle-name-label-input"
                      className={
                        !this.state.displayInvalidBundleNameError ? "valid-field" : "invalid-field"
                      }
                      ariaLabelledBy="bundle-name-label invalid-bundle-name-error"
                      style={{ ...styles.bundleNameInput, ...accommodationsStyle }}
                      type="text"
                      value={this.state.bundleNameContent}
                      onChange={this.updateBundleNameContent}
                    ></input>
                    {this.state.displayInvalidBundleNameError && (
                      <label
                        id="invalid-bundle-name-error"
                        htmlFor="bundle-name-label-input"
                        style={styles.errorMessage}
                        className="notranslate"
                      >
                        {
                          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                            .bundles.general.editBundleNamePopup.bundleNameMustBeValid
                        }
                      </label>
                    )}
                  </Col>
                </Row>
              </div>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.confirm}
          rightButtonAction={this.closeEditBundleNamePopup}
          rightButtonIcon={faCheck}
          rightButtonState={
            !this.state.displayInvalidBundleNameError ? BUTTON_STATE.enabled : BUTTON_STATE.disabled
          }
          size="lg"
        />
      </div>
    );
  }
}

export { GeneralTab as unconnectedGeneralTab };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    testNavBarHeight: state.testSection.testNavBarHeight,
    item_bank_data: state.itemBank.item_bank_data,
    bundleData: state.itemBank.bundleData,
    bundleInitialData: state.itemBank.bundleInitialData,
    dataReadyForUpload: state.itemBank.dataReadyForUpload
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setBundleData,
      setDataReadyForUpload,
      setItemBankData,
      getSpecificBundle,
      setBundleInitialData
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(GeneralTab);
