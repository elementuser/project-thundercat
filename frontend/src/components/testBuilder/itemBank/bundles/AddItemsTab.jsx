import React, { Component } from "react";
import * as _ from "lodash";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getItemBankHeightCalculations } from "../../../../helpers/inTestHeightCalculations";
import LOCALIZE from "../../../../text_resources";
import { getLineSpacingCSS } from "../../../../modules/AccommodationsRedux";
import SearchBarWithDisplayOptions from "../../../commons/SearchBarWithDisplayOptions";
import {
  updateSearchAddItemsBundlesStates,
  updateBundlesAddItemsPageState,
  updateBundlesAddItemsPageSizeState,
  getItemsReadyToBeAddedToBundle,
  getFoundItemsReadyToBeAddedToBundle,
  setBundleData,
  getDevelopmentStatuses,
  setBundleLocalUnlinkedSystemIds
} from "../../../../modules/ItemBankRedux";
import { faCaretLeft, faCaretRight, faShareSquare } from "@fortawesome/free-solid-svg-icons";
import GenericTable, { COMMON_STYLE } from "../../../commons/GenericTable";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import ReactPaginate from "react-paginate";
import getCheckboxTransformScale from "../../../../helpers/checkboxTransformScale";
import CustomButton, { THEME } from "../../../commons/CustomButton";
import { BUTTON_STATE } from "../../../commons/PopupBox";
import DropdownSelect from "../../../commons/DropdownSelect";
import { ITEM_DEVELOPMENT_STATUS } from "../items/Constants";
import { Col, Row } from "react-bootstrap";

const styles = {
  mainContainer: {
    width: "100%",
    padding: "24px 12px",
    border: "1px solid #cdcdcd",
    borderTop: "none",
    borderBottom: "none",
    overflowY: "auto",
    overflowX: "hidden"
  },
  searchDropdownsContainer: {
    margin: "12px 48px"
  },
  dropdownContainer: {
    marginBottom: 24
  },
  tableContainer: {
    margin: "12px 0 0 0"
  },
  label: {
    fontWeight: "bold",
    padding: "0 0 2px 2px"
  },
  paginationContainer: {
    display: "flex",
    position: "relative"
  },
  paginationIcon: {
    padding: "0 6px"
  },
  overflow: {
    overflowY: "auto",
    borderTop: "1px solid #00565e",
    borderBottom: "1px solid #00565e",
    borderRight: "1px solid #00565e",
    borderRadius: "4px"
  },
  noBorder: {
    borderTop: "none",
    borderBottom: "none",
    borderRight: "1px solid #00565e"
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  checkbox: {
    margin: "0 16px",
    verticalAlign: "middle"
  },
  buttonIcon: {
    marginRight: 6
  },
  addItemsToBundleButtonContainer: {
    textAlign: "center",
    marginTop: 24
  }
};

class AddItemsTab extends Component {
  constructor(props) {
    super(props);
    this.addItemsPaginationDivRef = React.createRef();
  }

  state = {
    currentlyLoading: true,
    rowsDefinition: {},
    resultsFound: 0,
    numberOfItemsPages: 1,
    checkAllItems: false,
    checkedItems: [],
    isLoadingDevelopmentStatusOptions: true,
    developmentStatusOptions: [],
    developmentStatusSelectedOptions: [],
    activeStateOptions: [],
    activeStateSelectedOptions: []
  };

  componentDidMount = () => {
    this.props.updateBundlesAddItemsPageState(1);
    this.populateDevelopmentStatusOptions();
    this.populateActiveStateOptions();
    this.populateCustomAttributeValueOptions();
  };

  componentDidUpdate = (prevProps, prevState) => {
    // if currentLanguage gets updated
    if (prevProps.currentLanguage !== this.props.currentLanguage) {
      this.getAllItemsBasedOnActiveSearch();
    }
    // if length of bundle_associations (bundleData) gets updated
    if (
      prevProps.bundleData.bundle_associations.length !==
      this.props.bundleData.bundle_associations.length
    ) {
      this.getAllItemsBasedOnActiveSearch();
    }
    // if bundle name gets updated
    if (prevProps.bundleData.name !== this.props.bundleData.name) {
      this.resetDropdownSelectedOptionsStates();
    }
    // if bundlesAddItemsPaginationPageSize get updated
    if (
      prevProps.bundlesAddItemsPaginationPageSize !== this.props.bundlesAddItemsPaginationPageSize
    ) {
      this.getAllItemsBasedOnActiveSearch();
    }
    // if bundlesAddItemsPaginationPage get updated
    if (prevProps.bundlesAddItemsPaginationPage !== this.props.bundlesAddItemsPaginationPage) {
      this.getAllItemsBasedOnActiveSearch();
    }
    // if search bundlesAddItemsKeyword gets updated
    if (prevProps.bundlesAddItemsKeyword !== this.props.bundlesAddItemsKeyword) {
      // if bundlesAddItemsKeyword redux state is empty
      if (this.props.bundlesAddItemsKeyword !== "") {
        this.getAllItemsBasedOnActiveSearch();
      } else if (
        this.props.bundlesAddItemsKeyword === "" &&
        this.props.bundlesAddItemsActiveSearch
      ) {
        this.getAllItemsBasedOnActiveSearch();
      }
    }
    // if bundlesAddItemsActiveSearch gets updated
    if (prevProps.bundlesAddItemsActiveSearch !== this.props.bundlesAddItemsActiveSearch) {
      // there is no current active search (on clear search action)
      if (!this.props.bundlesAddItemsActiveSearch) {
        this.getAllItems();
        // there is a current active search (on search action)
      } else {
        this.getFoundItems();
      }
    }
    // if isLoadingDevelopmentStatusOptions gets updated
    if (
      prevState.isLoadingDevelopmentStatusOptions !== this.state.isLoadingDevelopmentStatusOptions
    ) {
      this.getAllItemsBasedOnActiveSearch();
    }
  };

  resetDropdownSelectedOptionsStates = () => {
    // resetting default search criterias states
    this.setState({ developmentStatusSelectedOptions: [], activeStateSelectedOptions: [] }, () => {
      // setting back to the initial selected options
      this.populateDevelopmentStatusOptions();
      this.populateActiveStateOptions();
    });

    // resetting custom search criterias states
    for (let i = 0; i < this.props.item_bank_data.item_bank_attributes.length; i++) {
      this.setState({
        [`${this.props.item_bank_data.item_bank_attributes[
          i
        ].item_bank_attribute_text.en[0].text.replaceAll(" ", "_")}_selected_options`]: []
      });
    }
  };

  populateDevelopmentStatusOptions = () => {
    this.setState({ isLoadingDevelopmentStatusOptions: true }, () => {
      const developmentStatusOptions = [];
      this.props
        .getDevelopmentStatuses()
        .then(response => {
          for (let i = 0; i < response.length; i++) {
            developmentStatusOptions.push({
              label: `${response[i][`${this.props.currentLanguage}_name`]}`,
              value: response[i].id,
              development_status_codename: response[i].codename,
              development_status_name_en: response[i].en_name,
              development_status_name_fr: response[i].fr_name
            });
          }
          // setting respective state
          this.setState({
            developmentStatusOptions: developmentStatusOptions,

            // leaving blank as a default value is required, but no filters are desired
            developmentStatusSelectedOptions: []
          });
        })
        .then(() => {
          this.setState({ isLoadingDevelopmentStatusOptions: false });
        });
    });
  };

  populateActiveStateOptions = () => {
    const activeStateOptions = [];

    // adding active state
    activeStateOptions.push({ label: LOCALIZE.commons.active, value: 1 });

    // adding inactive state
    activeStateOptions.push({ label: LOCALIZE.commons.inactive, value: 0 });

    // leaving activeStateSelectedOptions blank as a default value is required, but no filters are desired
    this.setState({
      activeStateOptions: activeStateOptions,
      activeStateSelectedOptions: []
    });
  };

  // get item banks data based on the bundlesAddItemsActiveSearch redux state
  getAllItemsBasedOnActiveSearch = () => {
    // current search
    if (this.props.bundlesAddItemsActiveSearch) {
      this.getFoundItems();
      // no current search
    } else {
      this.getAllItems();
    }
  };

  getAllItems = () => {
    this.setState({ currentlyLoading: true }, () => {
      // initializing needed object and array for rowsDefinition (needed for GenericTable component)
      let rowsDefinition = {};
      const data = [];
      // getting all linked items (from the redux state)
      const allLinkedItems = this.props.bundleData.bundle_associations.filter(
        obj => obj.system_id_link !== null
      );
      const allLinkedItemSystemIds = [];
      for (let i = 0; i < allLinkedItems.length; i++) {
        allLinkedItemSystemIds.push(allLinkedItems[i].system_id_link);
      }

      // getting all local unlinked items (from the redux state)
      const allLocalUnlinkedItemSystemIds =
        typeof this.props.bundleLocalUnlinkedSystemIds !== "undefined"
          ? this.props.bundleLocalUnlinkedSystemIds
          : [];

      // formatting default search criterias
      const defaultSearchCriterias = {
        development_status_data: this.state.developmentStatusSelectedOptions,
        active_state_data: this.state.activeStateSelectedOptions
      };

      // formatting custom search criterias
      const customSearchCriterias = [];
      for (let i = 0; i < this.props.item_bank_data.item_bank_attributes.length; i++) {
        customSearchCriterias.push(
          this.state[
            `${this.props.item_bank_data.item_bank_attributes[
              i
            ].item_bank_attribute_text.en[0].text.replaceAll(" ", "_")}_selected_options`
          ]
        );
      }

      this.props
        .getItemsReadyToBeAddedToBundle(
          this.props.item_bank_data.item_bank_definition[0].id,
          this.props.bundleData.id,
          allLinkedItemSystemIds.length > 0 ? allLinkedItemSystemIds : " ",
          allLocalUnlinkedItemSystemIds.length > 0 ? allLocalUnlinkedItemSystemIds : " ",
          JSON.stringify(defaultSearchCriterias),
          JSON.stringify(customSearchCriterias),
          this.props.bundlesAddItemsPaginationPage,
          this.props.bundlesAddItemsPaginationPageSize
        )
        .then(response => {
          // setting checked item statuses
          const checkedItems = [];

          // looping in all related items
          for (let i = 0; i < response.results.length; i++) {
            checkedItems.push(false);
          }

          this.setState({ checkedItems: checkedItems }, () => {
            // initializing custom index
            let customIndex = 0;
            for (let i = 0; i < response.results.length; i++) {
              // making sure that system ID of the current iteration is not part of the bundle_associations state
              if (
                this.props.bundleData.bundle_associations.filter(
                  obj => obj.system_id_link === response.results[i].system_id
                ).length <= 0
              ) {
                // populating data object with provided data
                data.push({
                  column_1: this.populateColumnOne(customIndex),
                  column_2: response.results[i].system_id,
                  column_3: response.results[i].historical_id,
                  column_4:
                    response.results[i][`response_format_name_${this.props.currentLanguage}`],
                  column_5:
                    response.results[i][`development_status_name_${this.props.currentLanguage}`],
                  column_6: response.results[i].version
                });
                // already part of the bundle associations
              } else {
                checkedItems.splice(i, 1);
              }
              // incrementing custom index
              customIndex += 1;
            }

            // updating rowsDefinition object with provided data and needed style
            rowsDefinition = {
              column_1_style: COMMON_STYLE.CENTERED_TEXT,
              column_2_style: COMMON_STYLE.CENTERED_TEXT,
              column_3_style: COMMON_STYLE.CENTERED_TEXT,
              column_4_style: COMMON_STYLE.CENTERED_TEXT,
              column_5_style: COMMON_STYLE.CENTERED_TEXT,
              column_6_style: COMMON_STYLE.CENTERED_TEXT,
              data: data
            };
            // saving results in state
            this.setState({
              rowsDefinition: rowsDefinition,
              currentlyLoading: false,
              numberOfItemsPages: Math.ceil(
                parseInt(response.count) / this.props.bundlesAddItemsPaginationPageSize
              ),
              checkedItems: checkedItems,
              checkAllItems: false
            });
          });
        });
    });
  };

  getFoundItems = () => {
    this.setState({ currentlyLoading: true }, () => {
      // initializing needed object and array for rowsDefinition (needed for GenericTable component)
      let rowsDefinition = {};
      const data = [];
      // getting all linked items (from the redux state)
      const allLinkedItems = this.props.bundleData.bundle_associations.filter(
        obj => obj.system_id_link !== null
      );
      const allLinkedItemSystemIds = [];
      for (let i = 0; i < allLinkedItems.length; i++) {
        allLinkedItemSystemIds.push(allLinkedItems[i].system_id_link);
      }

      // getting all local unlinked items (from the redux state)
      const allLocalUnlinkedItemSystemIds =
        typeof this.props.bundleLocalUnlinkedSystemIds !== "undefined"
          ? this.props.bundleLocalUnlinkedSystemIds
          : [];

      // formatting default search criterias
      const defaultSearchCriterias = {
        development_status_data: this.state.developmentStatusSelectedOptions,
        active_state_data: this.state.activeStateSelectedOptions
      };

      // formatting custom search criterias
      const customSearchCriterias = [];
      for (let i = 0; i < this.props.item_bank_data.item_bank_attributes.length; i++) {
        customSearchCriterias.push(
          this.state[
            `${this.props.item_bank_data.item_bank_attributes[
              i
            ].item_bank_attribute_text.en[0].text.replaceAll(" ", "_")}_selected_options`
          ]
        );
      }

      this.props
        .getFoundItemsReadyToBeAddedToBundle(
          this.props.item_bank_data.item_bank_definition[0].id,
          this.props.bundleData.id,
          allLinkedItemSystemIds.length > 0 ? allLinkedItemSystemIds : " ",
          allLocalUnlinkedItemSystemIds.length > 0 ? allLocalUnlinkedItemSystemIds : " ",
          JSON.stringify(defaultSearchCriterias),
          JSON.stringify(customSearchCriterias),
          this.props.bundlesAddItemsKeyword,
          this.props.currentLanguage,
          this.props.bundlesAddItemsPaginationPage,
          this.props.bundlesAddItemsPaginationPageSize
        )
        .then(response => {
          // there are no results found
          if (response[0] === "no results found" || response.error) {
            // resetting all checkedItems values to false (if defined)
            const checkedItems = [];
            if (this.state.checkedItems.length > 0) {
              for (let i = 0; i < this.state.checkedItems.length; i++) {
                checkedItems.push(false);
              }
            }

            this.setState(
              {
                rowsDefinition: {},
                numberOfItemsPages: 1,
                resultsFound: 0,
                checkAllItems: false,
                checkedItems: checkedItems
              },
              () => {
                this.setState({ currentlyLoading: false }, () => {
                  // make sure that this element exsits to avoid any error
                  if (document.getElementById("item-bank-bundles-add-items-results-found")) {
                    document.getElementById("item-bank-bundles-add-items-results-found").focus();
                  }
                });
              }
            );
            // there is at least one result found
          } else {
            // setting checked item statuses
            const checkedItems = [];

            // looping in all related items
            for (let i = 0; i < response.results.length; i++) {
              checkedItems.push(false);
            }

            this.setState({ checkedItems: checkedItems }, () => {
              // initializing custom index
              let customIndex = 0;
              for (let i = 0; i < response.results.length; i++) {
                // making sure that system ID of the current iteration is not part of the bundle_associations state
                if (
                  this.props.bundleData.bundle_associations.filter(
                    obj => obj.system_id_link === response.results[i].system_id
                  ).length <= 0
                ) {
                  // populating data object with provided data
                  data.push({
                    column_1: this.populateColumnOne(customIndex),
                    column_2: response.results[i].system_id,
                    column_3: response.results[i].historical_id,
                    column_4:
                      response.results[i][`response_format_name_${this.props.currentLanguage}`],
                    column_5:
                      response.results[i][`development_status_name_${this.props.currentLanguage}`],
                    column_6: response.results[i].version
                  });
                  // already part of the bundle associations
                } else {
                  checkedItems.splice(i, 1);
                }
                // incrementing custom index
                customIndex += 1;
              }

              // updating rowsDefinition object with provided data and needed style
              rowsDefinition = {
                column_1_style: COMMON_STYLE.CENTERED_TEXT,
                column_2_style: COMMON_STYLE.CENTERED_TEXT,
                column_3_style: COMMON_STYLE.CENTERED_TEXT,
                column_4_style: COMMON_STYLE.CENTERED_TEXT,
                column_5_style: COMMON_STYLE.CENTERED_TEXT,
                column_6_style: COMMON_STYLE.CENTERED_TEXT,
                data: data
              };
              // saving results in state
              this.setState(
                {
                  rowsDefinition: rowsDefinition,
                  currentlyLoading: false,
                  numberOfItemsPages: Math.ceil(
                    parseInt(response.count) / this.props.bundlesAddItemsPaginationPageSize
                  ),
                  resultsFound: response.count,
                  checkedItems: checkedItems,
                  checkAllItems: false
                },
                () => {
                  // make sure that this element exsits to avoid any error
                  if (document.getElementById("item-bank-bundles-add-items-results-found")) {
                    document.getElementById("item-bank-bundles-add-items-results-found").focus();
                  }
                }
              );
            });
          }
        });
    });
  };

  populateColumnOne = i => {
    const checkboxTransformScale = getCheckboxTransformScale(this.props.accommodations.fontSize);

    return (
      <input
        type="checkbox"
        style={{
          ...styles.checkbox,
          ...{ transform: checkboxTransformScale }
        }}
        id={`add-items-check-item-${i}`}
        checked={this.state.checkedItems[i]}
        onChange={event => this.toggleCheckbox(i, event)}
      />
    );
  };

  handlePageChange = id => {
    // "+1" because redux bundlesAddItemsPaginationPage uses index 1 and react-paginate (id) uses index 0
    const selectedPage = id.selected + 1;
    // update current page redux state
    this.props.updateBundlesAddItemsPageState(selectedPage);
    // focusing on the first table's row (make sure that the row exists before trying to focus to avoid errors)
    if (document.getElementById("item-bank-bundles-add-items-table-row-0")) {
      document.getElementById("item-bank-bundles-add-items-table-row-0").focus();
    }
  };

  toggleCheckAll = event => {
    const checkboxTransformScale = getCheckboxTransformScale(this.props.accommodations.fontSize);

    // initializing checkedItems
    const checkedItems = [];

    // looping in all related items
    for (let i = 0; i < this.state.checkedItems.length; i++) {
      // populating checkedItems
      checkedItems.push(event.target.checked);
    }

    this.setState({ checkAllItems: event.target.checked, checkedItems: checkedItems }, () => {
      const copyOfRowsDefinition = this.state.rowsDefinition;

      // looping in copyOfRowsDefinition
      for (let i = 0; i < copyOfRowsDefinition.data.length; i++) {
        // updating column_1 input
        copyOfRowsDefinition.data[i].column_1 = (
          <input
            type="checkbox"
            style={{
              ...styles.checkbox,
              ...{ transform: checkboxTransformScale }
            }}
            id={`add-items-check-item-${i}`}
            checked={this.state.checkedItems[i]}
            onChange={event => this.toggleCheckbox(i, event)}
          />
        );
      }
      this.setState({ rowsDefinition: copyOfRowsDefinition });
    });
  };

  toggleCheckbox = (index, event) => {
    const checkboxTransformScale = getCheckboxTransformScale(this.props.accommodations.fontSize);

    // creating a copy of checked items
    const copyOfCheckedItems = this.state.checkedItems;
    // updating checkbox
    copyOfCheckedItems[index] = event.target.checked;

    this.setState({ checkedItems: copyOfCheckedItems }, () => {
      const copyOfRowsDefinition = this.state.rowsDefinition;

      // looping in copyOfRowsDefinition
      for (let i = 0; i < copyOfRowsDefinition.data.length; i++) {
        // updating column_1 input
        copyOfRowsDefinition.data[i].column_1 = (
          <input
            type="checkbox"
            style={{
              ...styles.checkbox,
              ...{ transform: checkboxTransformScale }
            }}
            id={`add-items-check-item-${i}`}
            checked={this.state.checkedItems[i]}
            onChange={event => this.toggleCheckbox(i, event)}
          />
        );
      }

      // checking if we need to check/uncheck the check all checkbox
      // initializing checkAllItems
      let checkAllItems = true;

      // looping in checkedItems
      for (let i = 0; i < this.state.checkedItems.length; i++) {
        if (this.state.checkedItems[i] === false) {
          checkAllItems = false;
          break;
        }
      }

      // updating needed states
      this.setState({ rowsDefinition: copyOfRowsDefinition, checkAllItems: checkAllItems });
    });
  };

  handleAddToBundle = () => {
    // creating a copy of bundleData
    const copyOfBundleData = _.cloneDeep(this.props.bundleData);
    // creating copy of bundleLocalUnlinkedSystemIds
    const copyOfBundleLocalUnlinkedSystemIds = _.cloneDeep(this.props.bundleLocalUnlinkedSystemIds);

    // looping in rowsDefinition
    for (let i = 0; i < this.state.rowsDefinition.data.length; i++) {
      // checking if checkbox of current iteration is checked
      if (this.state.checkedItems[i]) {
        // getting item data of respective item
        const itemData = this.props.item_bank_data.item_bank_items.filter(
          obj => obj.system_id === this.state.rowsDefinition.data[i].column_2
        )[0];
        // adding item to bundle associtaions
        copyOfBundleData.bundle_associations.push({
          bundle_bundles_association_id: null,
          item_data: itemData,
          bundle_data: null,
          bundle_items_association_id: null,
          bundle_link_id: null,
          bundle_source_id: this.props.bundleData.id,
          system_id_link: this.state.rowsDefinition.data[i].column_2,
          display_order:
            copyOfBundleData.bundle_associations.filter(obj => obj.system_id_link !== null).length +
            1,
          version: null
        });

        // removing item from bundleLocalUnlinkedSystemIds redux state (if needed)
        const indexOfcurrentInteration = copyOfBundleLocalUnlinkedSystemIds.findIndex(obj => {
          return obj === this.state.rowsDefinition.data[i].column_2;
        });

        // item exists in list
        if (indexOfcurrentInteration >= 0) {
          copyOfBundleLocalUnlinkedSystemIds.splice(indexOfcurrentInteration, 1);
        }
      }
    }

    // updating redux states
    this.props.setBundleData(copyOfBundleData);
    this.props.setBundleLocalUnlinkedSystemIds(copyOfBundleLocalUnlinkedSystemIds);

    // unchecking all checkboxes
    // setting checked item statuses
    const checkedItems = [];

    // looping in all related items
    for (let i = 0; i < this.state.rowsDefinition.data.length; i++) {
      checkedItems.push(false);
    }

    this.setState({ checkedItems: checkedItems, checkAllItems: false }, () => {
      this.getAllItemsBasedOnActiveSearch();
    });
  };

  changeDevelopmentStatus = selectedOptions => {
    this.setState({ developmentStatusSelectedOptions: selectedOptions }, () => {
      this.getAllItemsBasedOnActiveSearch();
    });
  };

  changeActiveState = selectedOptions => {
    this.setState({ activeStateSelectedOptions: selectedOptions }, () => {
      this.getAllItemsBasedOnActiveSearch();
    });
  };

  populateCustomAttributeValueOptions = () => {
    // looping in attribute values
    for (let i = 0; i < this.props.item_bank_data.item_bank_attributes.length; i++) {
      // initializing options
      const options = [];
      // looping in values
      for (
        let j = 0;
        j < this.props.item_bank_data.item_bank_attributes[i].item_bank_attribute_values.length;
        j++
      ) {
        options.push({
          label:
            this.props.item_bank_data.item_bank_attributes[i].item_bank_attribute_values[j].value
              .text,
          value: this.props.item_bank_data.item_bank_attributes[i].item_bank_attribute_values[j].id
        });
      }
      this.setState({
        [`${this.props.item_bank_data.item_bank_attributes[
          i
        ].item_bank_attribute_text.en[0].text.replaceAll(" ", "_")}_options`]: options
      });
    }
  };

  changeAttributeValue = (variableName, selectedOptions) => {
    this.setState({ [variableName]: selectedOptions }, () => {
      this.getAllItemsBasedOnActiveSearch();
    });
  };

  render() {
    const checkboxTransformScale = getCheckboxTransformScale(this.props.accommodations.fontSize);

    const columnsDefinition = [
      {
        label: (
          <input
            type="checkbox"
            style={{
              ...styles.checkbox,
              ...{ transform: checkboxTransformScale }
            }}
            id="add-items-check-all-checkbox"
            checked={this.state.checkAllItems}
            onChange={this.toggleCheckAll}
            disabled={
              Object.entries(this.state.rowsDefinition).length <= 0 ||
              this.state.rowsDefinition.data.length <= 0
            }
          />
        ),
        style: { ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label:
          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles.addItems
            .table.systemId,
        style: { ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label:
          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles.addItems
            .table.rndItemId,
        style: { ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label:
          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles.addItems
            .table.responseFormat,
        style: { ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label:
          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles.addItems
            .table.itemStatus,
        style: { ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label:
          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles.addItems
            .table.version,
        style: { ...COMMON_STYLE.CENTERED_TEXT }
      }
    ];

    // calculating the height of the top tabs and above (without considering the nav bar)
    // =========================================================================================
    let heightOfBackToItemBankSelectionButton = 0;
    let heightOfTopTabs = 0;
    let heightOfFooter = 0;
    let topPageHeight = 0;
    if (document.getElementById("back-to-item-bank-editor-btn") != null) {
      heightOfBackToItemBankSelectionButton = document.getElementById(
        "back-to-item-bank-editor-btn"
      ).offsetHeight;
      if (document.getElementsByClassName("nav nav-tabs").length > 0) {
        heightOfTopTabs = document.getElementsByClassName("nav nav-tabs")[0].offsetHeight;
      }
    } else {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }

    topPageHeight += heightOfBackToItemBankSelectionButton + heightOfTopTabs + 12; // height of "back to item bank selection button" + height of margin under button

    if (document.getElementById("item-bank-footer-main-div") !== null) {
      heightOfFooter = document.getElementById("item-bank-footer-main-div").offsetHeight;
    } else {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }

    const customHeight = getItemBankHeightCalculations(
      this.props.accommodations.fontSize,
      this.props.accommodations.spacing,
      this.props.testNavBarHeight,
      topPageHeight,
      heightOfFooter
    );
    // =========================================================================================

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <div style={{ ...styles.mainContainer, ...customHeight.height }}>
        <div style={styles.searchDropdownsContainer}>
          <div style={styles.dropdownContainer}>
            <Row className="align-items-center">
              <Col xl={4} lg={4} md={12} sm={12} xs={12}>
                <label
                  id="item-bank-selected-bundle-add-items-development-status-label"
                  style={styles.label}
                >
                  {
                    LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles
                      .addItems.searchDropdowns.developmentStatusLabel
                  }
                </label>
              </Col>
              <Col xl={8} lg={8} md={12} sm={12} xs={12}>
                <DropdownSelect
                  idPrefix="item-bank-selected-bundle-add-items-development-status"
                  ariaLabelledBy="item-bank-selected-bundle-add-items-development-status-label"
                  options={this.state.developmentStatusOptions}
                  onChange={this.changeDevelopmentStatus}
                  defaultValue={this.state.developmentStatusSelectedOptions}
                  isMulti={true}
                  hasPlaceholder={false}
                  orderByLabels={false}
                  orderByValues={false}
                />
              </Col>
            </Row>
          </div>
          <div style={styles.dropdownContainer}>
            <Row className="align-items-center">
              <Col xl={4} lg={4} md={12} sm={12} xs={12}>
                <label
                  id="item-bank-selected-bundle-add-items-active-state-label"
                  style={styles.label}
                >
                  {
                    LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles
                      .addItems.searchDropdowns.activeStateLabel
                  }
                </label>
              </Col>
              <Col xl={8} lg={8} md={12} sm={12} xs={12}>
                <DropdownSelect
                  idPrefix="item-bank-selected-bundle-add-items-active-state"
                  ariaLabelledBy="item-bank-selected-bundle-add-items-active-state-label"
                  options={this.state.activeStateOptions}
                  onChange={this.changeActiveState}
                  defaultValue={this.state.activeStateSelectedOptions}
                  isMulti={true}
                  hasPlaceholder={false}
                  orderByLabels={false}
                  orderByValues={false}
                />
              </Col>
            </Row>
          </div>
          {this.props.item_bank_data.item_bank_attributes.map(attribute_data => {
            return (
              <div style={styles.dropdownContainer}>
                <Row className="align-items-center">
                  <Col xl={4} lg={4} md={12} sm={12} xs={12}>
                    <label
                      id={`item-bank-selected-bundle-add-items-${attribute_data.item_bank_attribute_text.en[0].text}-label`}
                      style={styles.label}
                    >
                      {`${
                        attribute_data.item_bank_attribute_text[this.props.currentLanguage][0].text
                      }:`}
                    </label>
                  </Col>
                  <Col xl={8} lg={8} md={12} sm={12} xs={12}>
                    <DropdownSelect
                      idPrefix={`item-bank-selected-bundle-add-items-${attribute_data.item_bank_attribute_text.en[0].text}`}
                      ariaLabelledBy={`item-bank-selected-bundle-add-items-${attribute_data.item_bank_attribute_text.en[0].text}-label`}
                      options={
                        this.state[
                          `${attribute_data.item_bank_attribute_text.en[0].text.replaceAll(
                            " ",
                            "_"
                          )}_options`
                        ]
                      }
                      onChange={selectedOptions =>
                        this.changeAttributeValue(
                          `${attribute_data.item_bank_attribute_text.en[0].text.replaceAll(
                            " ",
                            "_"
                          )}_selected_options`,
                          selectedOptions
                        )
                      }
                      defaultValue={
                        this.state[
                          `${attribute_data.item_bank_attribute_text.en[0].text.replaceAll(
                            " ",
                            "_"
                          )}_selected_options`
                        ]
                      }
                      isMulti={true}
                      hasPlaceholder={false}
                      orderByLabels={false}
                      orderByValues={false}
                    />
                  </Col>
                </Row>
              </div>
            );
          })}
        </div>
        <div style={styles.tableContainer}>
          <SearchBarWithDisplayOptions
            idPrefix={"item-bank-bundles-add-items"}
            currentlyLoading={this.state.currentlyLoading}
            updateSearchStates={this.props.updateSearchAddItemsBundlesStates}
            updatePageState={this.props.updateBundlesAddItemsPageState}
            paginationPageSize={this.props.bundlesAddItemsPaginationPageSize}
            updatePaginationPageSize={this.props.updateBundlesAddItemsPageSizeState}
            data={this.state.rowsDefinition || []}
            resultsFound={this.state.resultsFound}
            searchBarContent={this.props.bundlesAddItemsKeyword}
          />
          <div style={styles.overflow}>
            <GenericTable
              classnamePrefix="item-bank-bundles-add-items"
              columnsDefinition={columnsDefinition}
              rowsDefinition={this.state.rowsDefinition}
              emptyTableMessage={
                LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles
                  .addItems.table.noData
              }
              style={styles.noBorder}
              currentlyLoading={this.state.currentlyLoading}
            />
          </div>
          <div style={styles.paginationContainer}>
            <ReactPaginate
              ref={this.addItemsPaginationDivRef}
              pageCount={this.state.numberOfItemsPages}
              containerClassName={"pagination"}
              breakClassName={"break"}
              activeClassName={"active-page"}
              marginPagesDisplayed={3}
              // "-1" because react-paginate uses index 0 and bundlesAddItemsPaginationPage redux state uses index 1
              forcePage={this.props.bundlesAddItemsPaginationPage - 1}
              onPageChange={page => {
                this.handlePageChange(page);
              }}
              previousLabel={
                <div style={styles.paginationIcon}>
                  <label style={styles.hiddenText}>
                    {LOCALIZE.commons.pagination.previousPageButton}
                  </label>
                  <FontAwesomeIcon icon={faCaretLeft} />
                </div>
              }
              nextLabel={
                <div style={styles.paginationIcon}>
                  <label style={styles.hiddenText}>
                    {LOCALIZE.commons.pagination.nextPageButton}
                  </label>
                  <FontAwesomeIcon icon={faCaretRight} />
                </div>
              }
            />
          </div>
        </div>
        <div style={styles.addItemsToBundleButtonContainer}>
          <CustomButton
            buttonId={"item-bank-bundles-add-items-to-bundle-btn"}
            label={
              <>
                <FontAwesomeIcon icon={faShareSquare} style={styles.buttonIcon} />
                <span>
                  {
                    LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles
                      .addItems.addItemsToBundleButton
                  }
                </span>
              </>
            }
            action={this.handleAddToBundle}
            buttonTheme={THEME.PRIMARY}
            disabled={
              this.state.checkedItems.filter(obj => obj === true).length > 0
                ? BUTTON_STATE.enabled
                : BUTTON_STATE.disabled
            }
          />
        </div>
      </div>
    );
  }
}

export { AddItemsTab as unconnectedAddItemsTab };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    testNavBarHeight: state.testSection.testNavBarHeight,
    bundlesAddItemsPaginationPage: state.itemBank.bundlesAddItemsPaginationPage,
    bundlesAddItemsPaginationPageSize: state.itemBank.bundlesAddItemsPaginationPageSize,
    bundlesAddItemsActiveSearch: state.itemBank.bundlesAddItemsActiveSearch,
    bundlesAddItemsKeyword: state.itemBank.bundlesAddItemsKeyword,
    bundleData: state.itemBank.bundleData,
    item_bank_data: state.itemBank.item_bank_data,
    bundleLocalUnlinkedSystemIds: state.itemBank.bundleLocalUnlinkedSystemIds
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateSearchAddItemsBundlesStates,
      updateBundlesAddItemsPageState,
      updateBundlesAddItemsPageSizeState,
      getItemsReadyToBeAddedToBundle,
      getFoundItemsReadyToBeAddedToBundle,
      setBundleData,
      getDevelopmentStatuses,
      setBundleLocalUnlinkedSystemIds
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(AddItemsTab);
