import React, { Component } from "react";
import * as _ from "lodash";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getItemBankHeightCalculations } from "../../../../helpers/inTestHeightCalculations";
import LOCALIZE from "../../../../text_resources";
import { getLineSpacingCSS } from "../../../../modules/AccommodationsRedux";
import SearchBarWithDisplayOptions from "../../../commons/SearchBarWithDisplayOptions";
import GenericTable, { COMMON_STYLE } from "../../../commons/GenericTable";
import { faCaretLeft, faCaretRight, faShareSquare } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import ReactPaginate from "react-paginate";
import {
  getBundlesReadyToBeAddedToBundle,
  getFoundBundlesReadyToBeAddedToBundle,
  setBundleData,
  updateBundlesAddBundlesPageSizeState,
  updateBundlesAddBundlesPageState,
  updateSearchAddBundlesBundlesStates,
  getSpecificBundle
} from "../../../../modules/ItemBankRedux";
import getCheckboxTransformScale from "../../../../helpers/checkboxTransformScale";
import CustomButton, { THEME } from "../../../commons/CustomButton";
import { BUTTON_STATE } from "../../../commons/PopupBox";

const styles = {
  mainContainer: {
    width: "100%",
    padding: "24px 12px",
    border: "1px solid #cdcdcd",
    borderTop: "none",
    borderBottom: "none",
    overflowY: "auto",
    overflowX: "hidden"
  },
  tableContainer: {
    margin: "12px 0 0 0"
  },
  overflow: {
    overflowY: "auto",
    borderTop: "1px solid #00565e",
    borderBottom: "1px solid #00565e",
    borderRight: "1px solid #00565e",
    borderRadius: "4px"
  },
  noBorder: {
    borderTop: "none",
    borderBottom: "none",
    borderRight: "1px solid #00565e"
  },
  paginationContainer: {
    display: "flex",
    position: "relative"
  },
  paginationIcon: {
    padding: "0 6px"
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  checkbox: {
    margin: "0 16px",
    verticalAlign: "middle"
  },
  buttonIcon: {
    marginRight: 6
  },
  addBundlesToBundleButtonContainer: {
    textAlign: "center",
    marginTop: 24
  }
};

class AddBundlesTab extends Component {
  constructor(props) {
    super(props);
    this.addBundlesPaginationDivRef = React.createRef();
  }

  state = {
    currentlyLoading: true,
    rowsDefinition: {},
    resultsFound: 0,
    numberOfItemsPages: 1,
    checkAllBundles: false,
    checkedBundles: []
  };

  componentDidMount = () => {
    this.props.updateBundlesAddBundlesPageState(1);
    this.getAllBundlesBasedOnActiveSearch();
  };

  componentDidUpdate = prevProps => {
    // if currentLanguage gets updated
    if (prevProps.currentLanguage !== this.props.currentLanguage) {
      this.getAllBundlesBasedOnActiveSearch();
    }
    // if triggerBundlesTableRerender gets updated
    if (prevProps.triggerBundlesTableRerender !== this.props.triggerBundlesTableRerender) {
      this.getAllBundlesBasedOnActiveSearch();
    }
    // if length of bundle_associations (bundleData) gets updated
    if (
      prevProps.bundleData.bundle_associations.length !==
      this.props.bundleData.bundle_associations.length
    ) {
      this.getAllBundlesBasedOnActiveSearch();
    }
    // if bundle ID (bundleData) gets updated
    if (prevProps.bundleData.id !== this.props.bundleData.id) {
      this.getAllBundlesBasedOnActiveSearch();
    }
    // if bundlesAddBundlesPaginationPageSize get updated
    if (
      prevProps.bundlesAddBundlesPaginationPageSize !==
      this.props.bundlesAddBundlesPaginationPageSize
    ) {
      this.getAllBundlesBasedOnActiveSearch();
    }
    // if bundlesAddBundlesPaginationPage get updated
    if (prevProps.bundlesAddBundlesPaginationPage !== this.props.bundlesAddBundlesPaginationPage) {
      this.getAllBundlesBasedOnActiveSearch();
    }
    // if search bundlesAddBundlesKeyword gets updated
    if (prevProps.bundlesAddBundlesKeyword !== this.props.bundlesAddBundlesKeyword) {
      // if bundlesAddBundlesKeyword redux state is empty
      if (this.props.bundlesAddBundlesKeyword !== "") {
        this.getAllBundlesBasedOnActiveSearch();
      } else if (
        this.props.bundlesAddBundlesKeyword === "" &&
        this.props.bundlesAddBundlesActiveSearch
      ) {
        this.getAllBundlesBasedOnActiveSearch();
      }
    }
    // if bundlesAddBundlesActiveSearch gets updated
    if (prevProps.bundlesAddBundlesActiveSearch !== this.props.bundlesAddBundlesActiveSearch) {
      // there is no current active search (on clear search action)
      if (!this.props.bundlesAddBundlesActiveSearch) {
        this.getAllBundles();
        // there is a current active search (on search action)
      } else {
        this.getFoundBundles();
      }
    }
  };

  // get item banks data based on the bundlesAddBundlesActiveSearch redux state
  getAllBundlesBasedOnActiveSearch = () => {
    // current search
    if (this.props.bundlesAddBundlesActiveSearch) {
      this.getFoundBundles();
      // no current search
    } else {
      this.getAllBundles();
    }
  };

  getAllBundles = () => {
    this.setState({ currentlyLoading: true }, () => {
      // initializing needed object and array for rowsDefinition (needed for GenericTable component)
      let rowsDefinition = {};
      const data = [];

      const body = {
        bundle_provided_data: this.props.bundleData,
        page: this.props.bundlesAddBundlesPaginationPage,
        page_size: this.props.bundlesAddBundlesPaginationPageSize
      };

      this.props.getBundlesReadyToBeAddedToBundle(body).then(response => {
        // setting checked bundles statuses
        const checkedBundles = [];

        // looping in all related items
        for (let i = 0; i < response.results.length; i++) {
          checkedBundles.push(false);
        }

        this.setState({ checkedBundles: checkedBundles }, () => {
          // initializing custom index
          let customIndex = 0;
          for (let i = 0; i < response.results.length; i++) {
            // making sure that bundle link ID of the current iteration is not part of the bundle_associations state
            if (
              this.props.bundleData.bundle_associations.filter(
                obj => obj.bundle_link_id === response.results[i].bundle_link_id
              ).length <= 0
            ) {
              // populating data object with provided data
              data.push({
                column_1: this.populateColumnOne(customIndex),
                column_2: response.results[i].name,
                column_3: response.results[i].version_text,
                column_4: response.results[i].item_count,
                column_5: response.results[i].bundle_count,
                column_6: response.results[i].active
                  ? LOCALIZE.commons.active
                  : LOCALIZE.commons.inactive,
                bundle_id: response.results[i].id,
                bundle_data: response.results[i]
              });
              // already part of the bundle associations
            } else {
              checkedBundles.splice(i, 1);
            }
            // incrementing custom index
            customIndex += 1;
          }

          // updating rowsDefinition object with provided data and needed style
          rowsDefinition = {
            column_1_style: COMMON_STYLE.CENTERED_TEXT,
            column_2_style: COMMON_STYLE.CENTERED_TEXT,
            column_3_style: COMMON_STYLE.CENTERED_TEXT,
            column_4_style: COMMON_STYLE.CENTERED_TEXT,
            column_5_style: COMMON_STYLE.CENTERED_TEXT,
            column_6_style: COMMON_STYLE.CENTERED_TEXT,
            data: data
          };
          // saving results in state
          this.setState({
            rowsDefinition: rowsDefinition,
            currentlyLoading: false,
            numberOfItemsPages: Math.ceil(
              parseInt(response.count) / this.props.bundlesAddBundlesPaginationPageSize
            ),
            checkedBundles: checkedBundles,
            checkAllBundles: false
          });
        });
      });
    });
  };

  getFoundBundles = () => {
    this.setState({ currentlyLoading: true }, () => {
      // initializing needed object and array for rowsDefinition (needed for GenericTable component)
      let rowsDefinition = {};
      const data = [];

      const body = {
        bundle_provided_data: this.props.bundleData,
        keyword: this.props.bundlesAddBundlesKeyword,
        page: this.props.bundlesAddBundlesPaginationPage,
        page_size: this.props.bundlesAddBundlesPaginationPageSize
      };

      this.props.getFoundBundlesReadyToBeAddedToBundle(body).then(response => {
        // there are no results found
        if (response[0] === "no results found" || response.error) {
          // resetting all checkedBundles values to false (if defined)
          const checkedBundles = [];
          if (this.state.checkedBundles.length > 0) {
            for (let i = 0; i < this.state.checkedBundles.length; i++) {
              checkedBundles.push(false);
            }
          }

          this.setState(
            {
              rowsDefinition: {},
              numberOfItemsPages: 1,
              resultsFound: 0,
              checkAllBundles: false,
              checkedBundles: checkedBundles
            },
            () => {
              this.setState({ currentlyLoading: false }, () => {
                // make sure that this element exsits to avoid any error
                if (document.getElementById("item-bank-bundles-add-bundles-results-found")) {
                  document.getElementById("item-bank-bundles-add-bundles-results-found").focus();
                }
              });
            }
          );
          // there is at least one result found
        } else {
          // setting checked bundles statuses
          const checkedBundles = [];

          // looping in all related items
          for (let i = 0; i < response.results.length; i++) {
            checkedBundles.push(false);
          }

          this.setState({ checkedBundles: checkedBundles }, () => {
            // initializing custom index
            let customIndex = 0;
            for (let i = 0; i < response.results.length; i++) {
              // making sure that bundle link ID of the current iteration is not part of the bundle_associations state
              if (
                this.props.bundleData.bundle_associations.filter(
                  obj => obj.bundle_link_id === response.results[i].bundle_link_id
                ).length <= 0
              ) {
                // populating data object with provided data
                data.push({
                  column_1: this.populateColumnOne(customIndex),
                  column_2: response.results[i].name,
                  column_3: response.results[i].version_text,
                  column_4: response.results[i].item_count,
                  column_5: response.results[i].bundle_count,
                  column_6: response.results[i].active
                    ? LOCALIZE.commons.active
                    : LOCALIZE.commons.inactive,
                  bundle_id: response.results[i].id,
                  bundle_data: response.results[i]
                });
                // already part of the bundle associations
              } else {
                checkedBundles.splice(i, 1);
              }
              // incrementing custom index
              customIndex += 1;
            }

            // updating rowsDefinition object with provided data and needed style
            rowsDefinition = {
              column_1_style: COMMON_STYLE.CENTERED_TEXT,
              column_2_style: COMMON_STYLE.CENTERED_TEXT,
              column_3_style: COMMON_STYLE.CENTERED_TEXT,
              column_4_style: COMMON_STYLE.CENTERED_TEXT,
              column_5_style: COMMON_STYLE.CENTERED_TEXT,
              column_6_style: COMMON_STYLE.CENTERED_TEXT,
              data: data
            };
            // saving results in state
            this.setState({
              rowsDefinition: rowsDefinition,
              currentlyLoading: false,
              numberOfItemsPages: Math.ceil(
                parseInt(response.count) / this.props.bundlesAddBundlesPaginationPageSize
              ),
              resultsFound: response.count,
              checkedBundles: checkedBundles,
              checkAllBundles: false
            });
          });
        }
      });
    });
  };

  populateColumnOne = i => {
    const checkboxTransformScale = getCheckboxTransformScale(this.props.accommodations.fontSize);

    return (
      <input
        type="checkbox"
        style={{
          ...styles.checkbox,
          ...{ transform: checkboxTransformScale }
        }}
        id={`add-bundles-check-item-${i}`}
        checked={this.state.checkedBundles[i]}
        onChange={event => this.toggleCheckbox(i, event)}
      />
    );
  };

  toggleCheckAll = event => {
    const checkboxTransformScale = getCheckboxTransformScale(this.props.accommodations.fontSize);

    // initializing checkedBundles
    const checkedBundles = [];

    // looping in all related items
    for (let i = 0; i < this.state.checkedBundles.length; i++) {
      // populating checkedBundles
      checkedBundles.push(event.target.checked);
    }

    this.setState({ checkAllBundles: event.target.checked, checkedBundles: checkedBundles }, () => {
      const copyOfRowsDefinition = this.state.rowsDefinition;

      // looping in copyOfRowsDefinition
      for (let i = 0; i < copyOfRowsDefinition.data.length; i++) {
        // updating column_1 input
        copyOfRowsDefinition.data[i].column_1 = (
          <input
            type="checkbox"
            style={{
              ...styles.checkbox,
              ...{ transform: checkboxTransformScale }
            }}
            id={`add-bundles-check-item-${i}`}
            checked={this.state.checkedBundles[i]}
            onChange={event => this.toggleCheckbox(i, event)}
          />
        );
      }
      this.setState({ rowsDefinition: copyOfRowsDefinition });
    });
  };

  toggleCheckbox = (index, event) => {
    const checkboxTransformScale = getCheckboxTransformScale(this.props.accommodations.fontSize);

    // creating a copy of checked items
    const copyOfCheckedBundles = this.state.checkedBundles;
    // updating checkbox
    copyOfCheckedBundles[index] = event.target.checked;

    this.setState({ checkedBundles: copyOfCheckedBundles }, () => {
      const copyOfRowsDefinition = this.state.rowsDefinition;

      // looping in copyOfRowsDefinition
      for (let i = 0; i < copyOfRowsDefinition.data.length; i++) {
        // updating column_1 input
        copyOfRowsDefinition.data[i].column_1 = (
          <input
            type="checkbox"
            style={{
              ...styles.checkbox,
              ...{ transform: checkboxTransformScale }
            }}
            id={`add-bundles-check-item-${i}`}
            checked={this.state.checkedBundles[i]}
            onChange={event => this.toggleCheckbox(i, event)}
          />
        );
      }

      // checking if we need to check/uncheck the check all checkbox
      // initializing checkAllBundles
      let checkAllBundles = true;

      // looping in checkedBundles
      for (let i = 0; i < this.state.checkedBundles.length; i++) {
        if (this.state.checkedBundles[i] === false) {
          checkAllBundles = false;
          break;
        }
      }

      // updating needed states
      this.setState({ rowsDefinition: copyOfRowsDefinition, checkAllBundles: checkAllBundles });
    });
  };

  handlePageChange = id => {
    // "+1" because redux bundlesAddBundlesPaginationPage uses index 1 and react-paginate (id) uses index 0
    const selectedPage = id.selected + 1;
    // update current page redux state
    this.props.updateBundlesAddBundlesPageState(selectedPage);
    // focusing on the first table's row (make sure that the row exists before trying to focus to avoid errors)
    if (document.getElementById("item-bank-bundles-add-bundles-table-row-0")) {
      document.getElementById("item-bank-bundles-add-bundles-table-row-0").focus();
    }
  };

  handleAddToBundle = () => {
    // creating a copy of bundleData
    const copyOfBundleData = _.cloneDeep(this.props.bundleData);

    // looping in rowsDefinition
    for (let i = 0; i < this.state.rowsDefinition.data.length; i++) {
      // checking if checkbox of current iteration is checked
      if (this.state.checkedBundles[i]) {
        // getting item data of respective item
        const bundleData = this.state.rowsDefinition.data[i].bundle_data;
        // adding item to bundle associtaions
        copyOfBundleData.bundle_associations.push({
          bundle_bundles_association_id: null,
          item_data: null,
          bundle_data: bundleData,
          bundle_items_association_id: null,
          bundle_link_id: this.state.rowsDefinition.data[i].bundle_id,
          bundle_source_id: this.props.bundleData.id,
          system_id_link: null,
          display_order:
            copyOfBundleData.bundle_associations.filter(obj => obj.system_id_link === null).length +
            1,
          version: null
        });
      }
    }
    // updating redux states
    this.props.setBundleData(copyOfBundleData);

    // unchecking all checkboxes
    // setting checked item statuses
    const checkedBundles = [];

    // looping in all related items
    for (let i = 0; i < this.state.rowsDefinition.data.length; i++) {
      checkedBundles.push(false);
    }

    this.setState({ checkedBundles: checkedBundles, checkAllItems: false }, () => {
      this.getAllBundlesBasedOnActiveSearch();
    });
  };

  render() {
    const checkboxTransformScale = getCheckboxTransformScale(this.props.accommodations.fontSize);

    const columnsDefinition = [
      {
        label: (
          <input
            type="checkbox"
            style={{
              ...styles.checkbox,
              ...{ transform: checkboxTransformScale }
            }}
            id="add-bundles-check-all-checkbox"
            checked={this.state.checkAllBundles}
            onChange={this.toggleCheckAll}
            disabled={
              Object.entries(this.state.rowsDefinition).length <= 0 ||
              this.state.rowsDefinition.data.length <= 0
            }
          />
        ),
        style: { ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label:
          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles.addBundles
            .table.bundleName,
        style: { ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label:
          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles.addBundles
            .table.bundleVersion,
        style: { ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label:
          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles.addBundles
            .table.itemCount,
        style: { ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label:
          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles.addBundles
            .table.bundleCount,
        style: { ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label:
          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles.addBundles
            .table.status,
        style: { ...COMMON_STYLE.CENTERED_TEXT }
      }
    ];

    // calculating the height of the top tabs and above (without considering the nav bar)
    // =========================================================================================
    let heightOfBackToItemBankSelectionButton = 0;
    let heightOfTopTabs = 0;
    let heightOfFooter = 0;
    let topPageHeight = 0;
    if (document.getElementById("back-to-item-bank-editor-btn") != null) {
      heightOfBackToItemBankSelectionButton = document.getElementById(
        "back-to-item-bank-editor-btn"
      ).offsetHeight;
      if (document.getElementsByClassName("nav nav-tabs").length > 0) {
        heightOfTopTabs = document.getElementsByClassName("nav nav-tabs")[0].offsetHeight;
      }
    } else {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }

    topPageHeight += heightOfBackToItemBankSelectionButton + heightOfTopTabs + 12; // height of "back to item bank selection button" + height of margin under button

    if (document.getElementById("item-bank-footer-main-div") !== null) {
      heightOfFooter = document.getElementById("item-bank-footer-main-div").offsetHeight;
    } else {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }

    const customHeight = getItemBankHeightCalculations(
      this.props.accommodations.fontSize,
      this.props.accommodations.spacing,
      this.props.testNavBarHeight,
      topPageHeight,
      heightOfFooter
    );
    // =========================================================================================

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <div style={{ ...styles.mainContainer, ...customHeight.height }}>
        <div style={styles.tableContainer}>
          <SearchBarWithDisplayOptions
            idPrefix={"item-bank-bundles-add-bundles"}
            currentlyLoading={this.state.currentlyLoading}
            updateSearchStates={this.props.updateSearchAddBundlesBundlesStates}
            updatePageState={this.props.updateBundlesAddBundlesPageState}
            paginationPageSize={this.props.bundlesAddBundlesPaginationPageSize}
            updatePaginationPageSize={this.props.updateBundlesAddBundlesPageSizeState}
            data={this.state.rowsDefinition || []}
            resultsFound={this.state.resultsFound}
            searchBarContent={this.props.bundlesAddBundlesKeyword}
          />
          <div style={styles.overflow}>
            <GenericTable
              classnamePrefix="item-bank-bundles-add-bundles"
              columnsDefinition={columnsDefinition}
              rowsDefinition={this.state.rowsDefinition}
              emptyTableMessage={
                LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles
                  .addBundles.table.noData
              }
              style={styles.noBorder}
              currentlyLoading={this.state.currentlyLoading}
            />
          </div>
          <div style={styles.paginationContainer}>
            <ReactPaginate
              ref={this.addBundlesPaginationDivRef}
              pageCount={this.state.numberOfItemsPages}
              containerClassName={"pagination"}
              breakClassName={"break"}
              activeClassName={"active-page"}
              marginPagesDisplayed={3}
              // "-1" because react-paginate uses index 0 and bundlesAddBundlesPaginationPage redux state uses index 1
              forcePage={this.props.bundlesAddBundlesPaginationPage - 1}
              onPageChange={page => {
                this.handlePageChange(page);
              }}
              previousLabel={
                <div style={styles.paginationIcon}>
                  <label style={styles.hiddenText}>
                    {LOCALIZE.commons.pagination.previousPageButton}
                  </label>
                  <FontAwesomeIcon icon={faCaretLeft} />
                </div>
              }
              nextLabel={
                <div style={styles.paginationIcon}>
                  <label style={styles.hiddenText}>
                    {LOCALIZE.commons.pagination.nextPageButton}
                  </label>
                  <FontAwesomeIcon icon={faCaretRight} />
                </div>
              }
            />
          </div>
        </div>
        <div style={styles.addBundlesToBundleButtonContainer}>
          <CustomButton
            buttonId={"item-bank-bundles-add-bundles-to-bundle-btn"}
            label={
              <>
                <FontAwesomeIcon icon={faShareSquare} style={styles.buttonIcon} />
                <span>
                  {
                    LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles
                      .addBundles.addBundlesToBundleButton
                  }
                </span>
              </>
            }
            action={this.handleAddToBundle}
            buttonTheme={THEME.PRIMARY}
            disabled={
              this.state.checkedBundles.filter(obj => obj === true).length > 0
                ? BUTTON_STATE.enabled
                : BUTTON_STATE.disabled
            }
          />
        </div>
      </div>
    );
  }
}

export { AddBundlesTab as unconnectedAddBundlesTab };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    testNavBarHeight: state.testSection.testNavBarHeight,
    bundlesAddBundlesPaginationPage: state.itemBank.bundlesAddBundlesPaginationPage,
    bundlesAddBundlesPaginationPageSize: state.itemBank.bundlesAddBundlesPaginationPageSize,
    bundlesAddBundlesActiveSearch: state.itemBank.bundlesAddBundlesActiveSearch,
    bundlesAddBundlesKeyword: state.itemBank.bundlesAddBundlesKeyword,
    bundleData: state.itemBank.bundleData,
    item_bank_data: state.itemBank.item_bank_data,
    triggerBundlesTableRerender: state.itemBank.triggerBundlesTableRerender
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateSearchAddBundlesBundlesStates,
      updateBundlesAddBundlesPageState,
      updateBundlesAddBundlesPageSizeState,
      getBundlesReadyToBeAddedToBundle,
      getFoundBundlesReadyToBeAddedToBundle,
      setBundleData,
      getSpecificBundle
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(AddBundlesTab);
