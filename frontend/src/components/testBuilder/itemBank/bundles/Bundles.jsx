import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getItemBankHeightCalculations } from "../../../../helpers/inTestHeightCalculations";
import CustomButton, { THEME } from "../../../commons/CustomButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCaretLeft,
  faCaretRight,
  faClone,
  faPen,
  faPlusCircle,
  faTimes
} from "@fortawesome/free-solid-svg-icons";
import LOCALIZE from "../../../../text_resources";
import PopupBox, { BUTTON_STATE, BUTTON_TYPE } from "../../../commons/PopupBox";
import SearchBarWithDisplayOptions from "../../../commons/SearchBarWithDisplayOptions";
import GenericTable, { COMMON_STYLE } from "../../../commons/GenericTable";
import ReactPaginate from "react-paginate";
import {
  updateSearchBundlesStates,
  updateBundlesPageState,
  updateBundlesPageSizeState,
  getAllBundles,
  getFoundBundles,
  setItemBankData,
  createNewBundle,
  resetTopTabsStates,
  setBundleData,
  setBundleInitialData,
  setBundleLocalUnlinkedSystemIds,
  setItemBankInitialData,
  getSpecificBundle
} from "../../../../modules/ItemBankRedux";
import StyledTooltip, { EFFECT, TRIGGER_TYPE, TYPE } from "../../../authentication/StyledTooltip";
import { Col, Row } from "react-bootstrap";
import { getLineSpacingCSS } from "../../../../modules/AccommodationsRedux";
import { PATH } from "../../../commons/Constants";
import { history } from "../../../../store-index";
import * as _ from "lodash";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 5,
    xl: 5
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 7,
    xl: 7
  }
};

const styles = {
  mainContainer: {
    width: "100%",
    padding: "24px 12px",
    border: "1px solid #cdcdcd",
    borderTop: "none"
  },
  createButton: {
    margin: "0 0 12px 0"
  },
  buttonIcon: {
    marginRight: 6
  },
  tableContainer: {
    margin: "12px 0 0 0"
  },
  paginationContainer: {
    display: "flex",
    position: "relative"
  },
  paginationIcon: {
    padding: "0 6px"
  },
  overflow: {
    overflowY: "auto",
    borderTop: "1px solid #00565e",
    borderBottom: "1px solid #00565e",
    borderRight: "1px solid #00565e",
    borderRadius: "4px"
  },
  noBorder: {
    borderTop: "none",
    borderBottom: "none",
    borderRight: "1px solid #00565e"
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  allUnset: {
    all: "unset"
  },
  bundleContainer: {
    width: "100%",
    margin: "12px 0"
  },
  labelContainer: {
    verticalAlign: "middle"
  },
  label: {
    paddingRight: 12,
    margin: 0
  },
  inputContainer: {
    verticalAlign: "middle"
  },
  input: {
    width: "100%",
    minHeight: 38,
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565e",
    borderRadius: 4
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    margin: "4px 0 0 4px"
  }
};

class Bundles extends Component {
  constructor(props) {
    super(props);
    this.paginationDivRef = React.createRef();
  }

  state = {
    triggerRerender: false,
    currentlyLoading: true,
    resultsFound: 0,
    numberOfItemsPages: 1,
    rowsDefinition: {},
    showCreateNewBundlePopup: false,
    bundleNameContent: "",
    bundleNameAlreadyUsed: false,
    bundleVersionContent: "",
    isValidNewBundleForm: false
  };

  componentDidMount = () => {
    this.props.updateBundlesPageState(1);
    this.getAllBundlesBasedOnActiveSearch();
  };

  componentDidUpdate = prevProps => {
    // if currentLanguage gets updated
    if (prevProps.currentLanguage !== this.props.currentLanguage) {
      this.getAllBundlesBasedOnActiveSearch();
    }
    // if bundlesPaginationPageSize get updated
    if (prevProps.bundlesPaginationPageSize !== this.props.bundlesPaginationPageSize) {
      this.getAllBundlesBasedOnActiveSearch();
    }
    // if bundlesPaginationPage get updated
    if (prevProps.bundlesPaginationPage !== this.props.bundlesPaginationPage) {
      this.getAllBundlesBasedOnActiveSearch();
    }
    // if search bundlesKeyword gets updated
    if (prevProps.bundlesKeyword !== this.props.bundlesKeyword) {
      // if bundlesKeyword redux state is empty
      if (this.props.bundlesKeyword !== "") {
        this.getAllBundlesBasedOnActiveSearch();
      } else if (this.props.bundlesKeyword === "" && this.props.bundlesActiveSearch) {
        this.getAllBundlesBasedOnActiveSearch();
      }
    }
    // if bundlesActiveSearch gets updated
    if (prevProps.bundlesActiveSearch !== this.props.bundlesActiveSearch) {
      // there is no current active search (on clear search action)
      if (!this.props.bundlesActiveSearch) {
        this.getAllBundles();
        // there is a current active search (on search action)
      } else {
        this.getFoundBundles();
      }
    }
    // if currentTab gets updated
    if (prevProps.currentTab !== this.props.currentTab) {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }
  };

  // get item banks data based on the bundlesActiveSearch redux state
  getAllBundlesBasedOnActiveSearch = () => {
    // current search
    if (this.props.bundlesActiveSearch) {
      this.getFoundBundles();
      // no current search
    } else {
      this.getAllBundles();
    }
  };

  getAllBundles = () => {
    this.setState({ currentlyLoading: true }, () => {
      // initializing needed object and array for rowsDefinition (needed for GenericTable component)
      let rowsDefinition = {};
      const data = [];
      this.props
        .getAllBundles(
          this.props.item_bank_definition.id,
          this.props.bundlesPaginationPage,
          this.props.bundlesPaginationPageSize
        )
        .then(response => {
          // updating redux states
          // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
          const new_obj = {
            ..._.cloneDeep(this.props.item_bank_data),
            list_of_bundles: response.list_of_bundles
          };
          this.props.setItemBankData(new_obj);

          // Set the value for the initial data of the Item Bank
          // This is used to detect changes on multiple components of Item Bank
          const new_initial_obj = {
            ..._.cloneDeep(this.props.itemBankInitialData),
            list_of_bundles: response.list_of_bundles
          };
          this.props.setItemBankInitialData(new_initial_obj);

          for (let i = 0; i < response.results.length; i++) {
            // populating data object with provided data
            data.push({
              column_1: response.results[i].name,
              column_2: response.results[i].version_text,
              column_3: response.results[i].item_count,
              column_4: response.results[i].bundle_count,
              column_5: response.results[i].active
                ? LOCALIZE.commons.active
                : LOCALIZE.commons.inactive,
              column_6: this.populateColumnSix(i, response)
            });
          }

          // updating rowsDefinition object with provided data and needed style
          rowsDefinition = {
            column_1_style: COMMON_STYLE.LEFT_TEXT,
            column_2_style: COMMON_STYLE.CENTERED_TEXT,
            column_3_style: COMMON_STYLE.CENTERED_TEXT,
            column_4_style: COMMON_STYLE.CENTERED_TEXT,
            column_5_style: COMMON_STYLE.CENTERED_TEXT,
            column_6_style: COMMON_STYLE.CENTERED_TEXT,
            data: data
          };
          // saving results in state
          this.setState({
            rowsDefinition: rowsDefinition,
            currentlyLoading: false,
            numberOfItemsPages: Math.ceil(
              parseInt(response.count) / this.props.bundlesPaginationPageSize
            )
          });
        });
    });
  };

  getFoundBundles = () => {
    this.setState({ currentlyLoading: true }, () => {
      // initializing needed object and array for rowsDefinition (needed for GenericTable component)
      let rowsDefinition = {};
      const data = [];
      setTimeout(() => {
        this.props
          .getFoundBundles(
            this.props.item_bank_definition.id,
            this.props.bundlesKeyword,
            this.props.bundlesPaginationPage,
            this.props.bundlesPaginationPageSize
          )
          .then(response => {
            // results are found
            if (typeof response.results !== "undefined") {
              // updating redux states
              // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
              const new_obj = {
                ..._.cloneDeep(this.props.item_bank_data),
                list_of_bundles: response.list_of_bundles
              };
              this.props.setItemBankData(new_obj);

              // Set the value for the initial data of the Item Bank
              // This is used to detect changes on multiple components of Item Bank
              const new_initial_obj = {
                ..._.cloneDeep(this.props.itemBankInitialData),
                list_of_bundles: response.list_of_bundles
              };
              this.props.setItemBankInitialData(new_initial_obj);
            }

            // there are no results found
            if (response[0] === "no results found") {
              this.setState(
                {
                  rowsDefinition: {},
                  numberOfItemsPages: 1,
                  resultsFound: 0
                },
                () => {
                  this.setState({ currentlyLoading: false }, () => {
                    // make sure that this element exsits to avoid any error
                    if (document.getElementById("item-bank-bundles-results-found")) {
                      document.getElementById("item-bank-bundles-results-found").focus();
                    }
                  });
                }
              );
              // there is at least one result found
            } else {
              // making sure that the results object is defined (avoiding page break)
              if (typeof response.results !== "undefined") {
                for (let i = 0; i < response.results.length; i++) {
                  // populating data object with provided data
                  data.push({
                    column_1: response.results[i].name,
                    column_2: response.results[i].version_text,
                    column_3: response.results[i].item_count,
                    column_4: response.results[i].bundle_count,
                    column_5: response.results[i].active
                      ? LOCALIZE.commons.active
                      : LOCALIZE.commons.inactive,
                    column_6: this.populateColumnSix(i, response)
                  });
                }
              }

              // updating rowsDefinition object with provided data and needed style
              rowsDefinition = {
                column_1_style: COMMON_STYLE.LEFT_TEXT,
                column_2_style: COMMON_STYLE.CENTERED_TEXT,
                column_3_style: COMMON_STYLE.CENTERED_TEXT,
                column_4_style: COMMON_STYLE.CENTERED_TEXT,
                column_5_style: COMMON_STYLE.CENTERED_TEXT,
                column_6_style: COMMON_STYLE.CENTERED_TEXT,
                data: data
              };
              // saving results in state
              this.setState(
                {
                  rowsDefinition: rowsDefinition,
                  currentlyLoading: false,
                  numberOfItemsPages: Math.ceil(
                    parseInt(response.count) / this.props.bundlesPaginationPageSize
                  ),
                  resultsFound: response.count
                },
                () => {
                  // make sure that this element exsits to avoid any error
                  if (document.getElementById("item-bank-bundles-search-results-found")) {
                    document.getElementById("item-bank-bundles-search-results-found").focus();
                  }
                }
              );
            }
          });
      }, 100);
    });
  };

  populateColumnSix = (i, response) => {
    return (
      <div className="d-flex justify-content-center flex-nowrap">
        <StyledTooltip
          id={`item-bank-bundle-edit-button-${i}`}
          place="top"
          type={TYPE.light}
          effect={EFFECT.solid}
          triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
          tooltipElement={
            <div style={styles.allUnset}>
              <CustomButton
                dataTip=""
                dataFor={`item-bank-bundle-edit-button-${i}`}
                label={<FontAwesomeIcon icon={faPen} />}
                ariaLabel={LOCALIZE.formatString(
                  LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles
                    .table.editActionAccessibilityLabel,
                  response.results[i].name
                )}
                action={() => {
                  this.editSelectedBundle(response.results[i]);
                }}
                customStyle={styles.actionButton}
                buttonTheme={THEME.SECONDARY}
              />
            </div>
          }
          tooltipContent={
            <div>
              <p>
                {
                  LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles
                    .table.editAction
                }
              </p>
            </div>
          }
        />
        <StyledTooltip
          id={`item-bank-bundle-duplicate-button-${i}`}
          place="top"
          type={TYPE.light}
          effect={EFFECT.solid}
          triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
          tooltipElement={
            <div style={styles.allUnset}>
              <CustomButton
                dataTip=""
                dataFor={`item-bank-bundle-duplicate-button-${i}`}
                label={<FontAwesomeIcon icon={faClone} />}
                ariaLabel={LOCALIZE.formatString(
                  LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles
                    .table.duplicateActionAccessibilityLabel,
                  response.results[i].name
                )}
                action={() => {
                  this.duplicateSelectedBundle(response.results[i]);
                }}
                customStyle={styles.actionButton}
                buttonTheme={THEME.SECONDARY}
              />
            </div>
          }
          tooltipContent={
            <div>
              <p>
                {
                  LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles
                    .table.duplicateAction
                }
              </p>
            </div>
          }
        />
      </div>
    );
  };

  editSelectedBundle = currentSelectedBundle => {
    this.props.resetTopTabsStates();
    this.props.setBundleLocalUnlinkedSystemIds([]);

    this.props
      .getSpecificBundle(currentSelectedBundle.id)
      .then(response => {
        this.props.setBundleData(response);
        this.props.setBundleInitialData(response);
      })
      .then(() => {
        history.push(PATH.bundleEditor);
      });
  };

  duplicateSelectedBundle = currentSelectedBundle => {
    console.log("DUPLICATE BUNDLE: ", currentSelectedBundle);
  };

  openCreateNewBundlePopup = () => {
    this.setState({ showCreateNewBundlePopup: true });
  };

  closeCreateNewBundlePopup = () => {
    this.setState({
      showCreateNewBundlePopup: false,
      bundleNameContent: "",
      bundleNameAlreadyUsed: false,
      bundleVersionContent: "",
      isValidNewBundleForm: false
    });
  };

  handlePageChange = id => {
    // "+1" because redux bundlesPaginationPage uses index 1 and react-paginate (id) uses index 0
    const selectedPage = id.selected + 1;
    // update current page redux state
    this.props.updateBundlesPageState(selectedPage);
    // focusing on the first table's row (make sure that the row exists before trying to focus to avoid errors)
    if (document.getElementById("item-bank-bundles-table-row-0")) {
      document.getElementById("item-bank-bundles-table-row-0").focus();
    }
  };

  updateBundleNameContent = event => {
    const bundleNameContent = event.target.value;
    // allow only alphanumeric, slash, underscore and dash (0 to 50 chars)
    const regexExpression = /^([a-zA-z0-9-/_]{0,50})$/;
    if (regexExpression.test(bundleNameContent)) {
      this.setState(
        {
          bundleNameContent: bundleNameContent.toUpperCase(),
          bundleNameAlreadyUsed: false
        },
        () => {
          this.validateNewBundleForm();
        }
      );
    }
  };

  updateBundleVersionContent = event => {
    const bundleVersionContent = event.target.value;
    // allow only alphanumeric, slash, underscore and dash (0 to 50 chars)
    const regexExpression = /^([a-zA-z0-9-/_]{0,50})$/;
    if (regexExpression.test(bundleVersionContent)) {
      this.setState(
        {
          bundleVersionContent: bundleVersionContent.toUpperCase()
        },
        () => {
          this.validateNewBundleForm();
        }
      );
    }
  };

  validateNewBundleForm = () => {
    // initializing needed variables
    let isValidForm = false;
    const isValidBundleName = this.state.bundleNameContent !== "";

    // everything is valid
    if (isValidBundleName) {
      isValidForm = true;
    }

    this.setState({ isValidNewBundleForm: isValidForm });
  };

  handleCreateNewBundle = () => {
    const body = {
      name: this.state.bundleNameContent,
      version_text: this.state.bundleVersionContent,
      item_bank_id: this.props.item_bank_definition.id
    };
    this.props.createNewBundle(body).then(response => {
      if (response.ok) {
        this.setState({
          showCreateNewBundlePopup: false,
          bundleNameContent: "",
          bundleNameAlreadyUsed: false,
          bundleVersionContent: "",
          isValidNewBundleForm: false
        });
        // Redirect to the created bundle
        this.props.resetTopTabsStates();
        this.props.setBundleLocalUnlinkedSystemIds([]);

        const copyOfListOfBundles = _.cloneDeep(this.props.item_bank_data.list_of_bundles);

        copyOfListOfBundles.push({ id: response.id, bundle_name: response.name });

        this.props.setItemBankData({
          ...this.props.item_bank_data,
          list_of_bundles: copyOfListOfBundles
        });

        this.props.setBundleData(response);
        this.props.setBundleInitialData(response);
        history.push(PATH.bundleEditor);

        // if there is a validation error
      } else if (response.status === 409) {
        this.setState({ bundleNameAlreadyUsed: true }, () => {
          // focusing on respective input
          document.getElementById("item-bank-bundle-name").focus();
        });
        // should never happen
      } else {
        throw new Error("An error occurred during the create new item process");
      }
    });
  };

  render() {
    const columnsDefinition = [
      {
        label:
          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles.table
            .bundleName,
        style: { ...COMMON_STYLE.LEFT_TEXT }
      },
      {
        label:
          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles.table
            .bundleVersion,
        style: { ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label:
          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles.table
            .itemCount,
        style: { ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label:
          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles.table
            .bundleCount,
        style: { ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label:
          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles.table
            .status,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label:
          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles.table
            .actions,
        style: { ...{ width: "10%" }, ...COMMON_STYLE.CENTERED_TEXT }
      }
    ];

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    // calculating the height of the top tabs and above (without considering the nav bar)
    // =========================================================================================
    let heightOfBackToItemBankSelectionButton = 0;
    let heightOfTopTabs = 0;
    let heightOfNewBtnAndSearchBar = 0;
    let heightOfPagination = 0;
    let heightOfResultsFoundDiv = 0;
    let topPageHeight = 0;
    if (document.getElementById("back-to-item-bank-selection-btn") != null) {
      heightOfBackToItemBankSelectionButton = document.getElementById(
        "back-to-item-bank-selection-btn"
      ).offsetHeight;
      if (document.getElementsByClassName("nav nav-tabs").length > 0) {
        heightOfTopTabs = document.getElementsByClassName("nav nav-tabs")[0].offsetHeight;
      }
      if (
        document.getElementById("item-bank-bundles-create-new-bundle-btn") !== null &&
        document.getElementById(
          "item-bank-bundles-search-display-options-dropdown-react-select-dropdown"
        ) !== null
      ) {
        heightOfNewBtnAndSearchBar =
          24 + // height of padding above the new item button
          // height of the create new item button
          document.getElementById("item-bank-bundles-create-new-bundle-btn").offsetHeight +
          18 + // height of marging above the search bar
          // height of the display options dropdown
          document.getElementById(
            "item-bank-bundles-search-display-options-dropdown-react-select-dropdown"
          ).offsetHeight +
          12; // height of marging below the search bar;
        if (document.getElementsByClassName("pagination").length > 0) {
          heightOfPagination =
            // [1] since it's the second time we're calling the pagination class (first one is called in Items.jsx component)
            document.getElementsByClassName("pagination")[1].offsetHeight +
            24 + // height of marging above the pagination section
            12; // height of padding below the pagination section
        }
        if (
          this.props.bundlesActiveSearch &&
          document.getElementById("search-bar-results-found-main-div") !== null
        ) {
          heightOfResultsFoundDiv = document.getElementById(
            "search-bar-results-found-main-div"
          ).offsetHeight;
        }
      }
    } else {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }

    topPageHeight +=
      heightOfBackToItemBankSelectionButton + heightOfTopTabs + heightOfResultsFoundDiv + 24; // height of "back to item bank selection button" + height of margin under button

    const customHeight = getItemBankHeightCalculations(
      this.props.accommodations.fontSize,
      this.props.accommodations.spacing,
      this.props.testNavBarHeight,
      topPageHeight - heightOfResultsFoundDiv,
      0
    );

    const customTableHeight = getItemBankHeightCalculations(
      this.props.accommodations.fontSize,
      this.props.accommodations.spacing,
      this.props.testNavBarHeight,
      topPageHeight + heightOfNewBtnAndSearchBar + heightOfPagination,
      0
    );
    // =========================================================================================

    return (
      <div style={{ ...styles.mainContainer, ...customHeight.maxHeight }}>
        <div style={styles.createButton}>
          <CustomButton
            buttonId={"item-bank-bundles-create-new-bundle-btn"}
            label={
              <>
                <FontAwesomeIcon icon={faPlusCircle} style={styles.buttonIcon} />
                <span>
                  {
                    LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles
                      .createNewBundleButton
                  }
                </span>
              </>
            }
            action={this.openCreateNewBundlePopup}
            buttonTheme={THEME.PRIMARY}
          />
        </div>
        <div style={styles.tableContainer}>
          <SearchBarWithDisplayOptions
            idPrefix={"item-bank-bundles-search"}
            currentlyLoading={this.state.currentlyLoading}
            updateSearchStates={this.props.updateSearchBundlesStates}
            updatePageState={this.props.updateBundlesPageState}
            paginationPageSize={this.props.bundlesPaginationPageSize}
            updatePaginationPageSize={this.props.updateBundlesPageSizeState}
            data={this.state.rowsDefinition || []}
            resultsFound={this.state.resultsFound}
            searchBarContent={this.props.bundlesKeyword}
          />
          <div style={{ ...styles.overflow, ...customTableHeight.maxHeight }}>
            <GenericTable
              classnamePrefix="item-bank-bundles"
              columnsDefinition={columnsDefinition}
              rowsDefinition={this.state.rowsDefinition}
              emptyTableMessage={
                LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles
                  .table.noData
              }
              style={styles.noBorder}
              currentlyLoading={this.state.currentlyLoading}
            />
          </div>
          <div style={styles.paginationContainer}>
            <ReactPaginate
              ref={this.paginationDivRef}
              pageCount={this.state.numberOfItemsPages}
              containerClassName={"pagination"}
              breakClassName={"break"}
              activeClassName={"active-page"}
              marginPagesDisplayed={3}
              // "-1" because react-paginate uses index 0 and bundlesPaginationPage redux state uses index 1
              forcePage={this.props.bundlesPaginationPage - 1}
              onPageChange={page => {
                this.handlePageChange(page);
              }}
              previousLabel={
                <div style={styles.paginationIcon}>
                  <label style={styles.hiddenText}>
                    {LOCALIZE.commons.pagination.previousPageButton}
                  </label>
                  <FontAwesomeIcon icon={faCaretLeft} />
                </div>
              }
              nextLabel={
                <div style={styles.paginationIcon}>
                  <label style={styles.hiddenText}>
                    {LOCALIZE.commons.pagination.nextPageButton}
                  </label>
                  <FontAwesomeIcon icon={faCaretRight} />
                </div>
              }
            />
          </div>
        </div>
        <PopupBox
          show={this.state.showCreateNewBundlePopup}
          handleClose={() => {}}
          title={
            LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles
              .createBundlePopup.title
          }
          description={
            <div>
              <p>
                {
                  LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles
                    .createBundlePopup.description
                }
              </p>
              <Row
                className="align-items-center justify-content-start"
                style={styles.bundleContainer}
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={styles.labelContainer}
                >
                  <label style={styles.label}>
                    <label htmlFor="item-bank-bundle-name">
                      {
                        LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                          .bundles.createBundlePopup.bundleNameLabel
                      }
                    </label>
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={styles.inputContainer}
                >
                  <input
                    id="item-bank-bundle-name"
                    className={!this.state.bundleNameAlreadyUsed ? "valid-field" : "invalid-field"}
                    aria-required={true}
                    aria-invalid={this.state.bundleNameAlreadyUsed}
                    style={{ ...styles.input, ...accommodationsStyle }}
                    type="text"
                    value={this.state.bundleNameContent}
                    onChange={this.updateBundleNameContent}
                  ></input>
                  {this.state.bundleNameAlreadyUsed && (
                    <label
                      id="item-bank-bundle-name-error"
                      htmlFor="item-bank-bundle-name"
                      style={styles.errorMessage}
                    >
                      {
                        LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                          .bundles.createBundlePopup.bundleNameAlreadyBeenUsedError
                      }
                    </label>
                  )}
                </Col>
              </Row>
              <Row
                className="align-items-center justify-content-start"
                style={styles.bundleContainer}
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={styles.labelContainer}
                >
                  <label style={styles.label}>
                    <label htmlFor="item-bank-bundle-version-text">
                      {
                        LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                          .bundles.createBundlePopup.bundleVersionLabel
                      }
                    </label>
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={styles.inputContainer}
                >
                  <input
                    id="item-bank-bundle-version-text"
                    className="valid-field"
                    aria-required={false}
                    style={{ ...styles.input, ...accommodationsStyle }}
                    type="text"
                    value={this.state.bundleVersionContent}
                    onChange={this.updateBundleVersionContent}
                  ></input>
                </Col>
              </Row>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonLabel={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeCreateNewBundlePopup}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={
            LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles
              .createBundlePopup.rightButton
          }
          rightButtonLabel={
            LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles
              .createBundlePopup.rightButton
          }
          rightButtonIcon={faPlusCircle}
          rightButtonAction={this.handleCreateNewBundle}
          rightButtonState={
            this.state.isValidNewBundleForm ? BUTTON_STATE.enabled : BUTTON_STATE.disabled
          }
        />
      </div>
    );
  }
}

export { Bundles as unconnectedBundles };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    testNavBarHeight: state.testSection.testNavBarHeight,
    currentTab: state.navTabs.currentTopTab,
    item_bank_definition: state.itemBank.item_bank_data.item_bank_definition[0],
    item_bank_data: state.itemBank.item_bank_data,
    itemBankInitialData: state.itemBank.itemBankInitialData,
    bundleData: state.itemBank.bundleData,
    bundlesPaginationPage: state.itemBank.bundlesPaginationPage,
    bundlesPaginationPageSize: state.itemBank.bundlesPaginationPageSize,
    bundlesActiveSearch: state.itemBank.bundlesActiveSearch,
    bundlesKeyword: state.itemBank.bundlesKeyword
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateSearchBundlesStates,
      updateBundlesPageState,
      updateBundlesPageSizeState,
      getAllBundles,
      getFoundBundles,
      setItemBankData,
      createNewBundle,
      resetTopTabsStates,
      setBundleData,
      setBundleInitialData,
      setBundleLocalUnlinkedSystemIds,
      setItemBankInitialData,
      getSpecificBundle
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Bundles);
