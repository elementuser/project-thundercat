/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Component } from "react";
import { connect } from "react-redux";
import * as _ from "lodash";
import LOCALIZE from "../../../../text_resources";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import ContentContainer, { maxWidthUtils } from "../../../commons/ContentContainer";
import CustomButton, { THEME } from "../../../commons/CustomButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faArrowAltCircleLeft,
  faCheck,
  faSpinner,
  faTimes,
  faMagic
} from "@fortawesome/free-solid-svg-icons";
import { history } from "../../../../store-index";
import { PATH } from "../../../commons/Constants";
import {
  setItemData,
  setFirstColumnTab,
  setSecondColumnTab,
  setThirdColumnTab,
  setBundleData,
  setBundleInitialData,
  uploadBundle,
  setDataReadyForUpload,
  setItemBankData,
  resetBundlesAddItemsStates,
  setBundleLocalUnlinkedSystemIds,
  getSpecificBundle,
  resetBundlesAddBundlesStates,
  triggerBundlesTableRerender,
  setValidBundleItemsBasicRuleState,
  previewGeneratedItemsListFromBundle
} from "../../../../modules/ItemBankRedux";
import PopupBox, { BUTTON_STATE, BUTTON_TYPE } from "../../../commons/PopupBox";
import SystemMessage, { MESSAGE_TYPE } from "../../../commons/SystemMessage";
import TopTabs from "../../../commons/TopTabs";
import { Col, Row } from "react-bootstrap";
import GeneralTab from "./GeneralTab";
import ItemBankFooter from "../ItemBankFooter";
import AddItemsTab from "./AddItemsTab";
import ItemContentTab from "./ItemContentTab";
import AddBundlesTab from "./AddBundlesTab";
import BundleContentTab from "./BundleContentTab";
import { triggerAppRerender } from "../../../../modules/AppRedux";
import ItemComponent from "../items/ItemComponent";
import ItemOptions from "../items/ItemOptions";
import getItemContentToPreview, {
  getItemOptionsToPreview
} from "../../../../helpers/itemBankItemPreview";

export const SECTIONS = {
  general: "general",
  add: "add",
  content: "content"
};

const styles = {
  loadingContainer: {
    margin: 24,
    textAlign: "center"
  },
  loading: {
    fontSize: "32px",
    padding: 6
  },
  backButtonAndTitleContainer: {
    marginBottom: 12
  },
  backButtonContainer: {
    position: "absolute"
  },
  titleContainer: {
    textAlign: "center"
  },
  h2: {
    margin: 0,
    padding: 0
  },
  colOneStyle: {
    padding: 0,
    maxWidth: "15%"
  },
  colTwoStyle: {
    padding: 0,
    maxWidth: "45%"
  },
  colThreeStyle: {
    padding: 0,
    maxWidth: "40%"
  },
  generalTab: {
    paddingRight: 3
  },
  addTab: {
    paddingLeft: 3,
    paddingRight: 3
  },
  contentTab: {
    paddingLeft: 3
  },
  buttonLabel: {
    marginLeft: 6
  },
  appPadding: {
    padding: "15px 15px 0 15px"
  },
  previewBundleDataLoadingContainer: {
    textAlign: "center"
  },
  itemContentMainContainer: {
    border: "1px solid grey",
    padding: 6
  },
  itemContentContainer: {
    borderBottom: "1px solid grey",
    marginBottom: 12,
    paddingBottom: 12
  },
  customH1Style: {
    margin: 0
  }
};

class BundleEditor extends Component {
  state = {
    isLoading: true,
    showBackToItemBankEditorPopup: false,
    showPreviewBundlePopup: false,
    isLoadingPreviewBundleData: true,
    isLoadingPreviewNewBundleData: false,
    previewBundleData: [],
    showGoNextItemConfirmationPopup: false,
    showGoPreviousItemConfirmationPopup: false,
    showUploadSuccessfulPopup: false,
    bundleNameAlreadyUsed: false,
    showBundleStructuralConflictError: false,
    showDuplicateSystemIdsFoundInBundleAssociationsError: false
  };

  componentDidMount = () => {
    // making sure that the item data ID is defined (otherwise, redirect the user to the item bank editor page)
    // can happen if the URL is manually entered or if the back browser button has been pressed
    if (typeof this.props.bundleData.id === "undefined") {
      setTimeout(() => {
        if (typeof this.props.bundleData.id === "undefined") {
          history.push(PATH.itemBankEditor);
        } else {
          this.setState({ isLoading: false });
        }
      }, 3000);
    } else {
      this.setState({ isLoading: false });
    }
    // triggering app rerender to make sure that the special page height and overflow is properly set
    this.props.triggerAppRerender();
  };

  componentDidUpdate = prevProps => {
    // if secondColumnTab gets updated
    if (prevProps.secondColumnTab !== this.props.secondColumnTab) {
      // updating third tab redux state
      this.props.setThirdColumnTab(`content-${this.props.secondColumnTab.split("-")[1]}`);
    }
    // if thirdColumnTab gets updated
    if (prevProps.thirdColumnTab !== this.props.thirdColumnTab) {
      // updating second tab redux state
      this.props.setSecondColumnTab(`add-${this.props.thirdColumnTab.split("-")[1]}`);
    }
  };

  openBackToItemBankEditorPopup = () => {
    this.setState({ showBackToItemBankEditorPopup: true });
  };

  closeBackToItemBankEditorPopup = () => {
    this.setState({ showBackToItemBankEditorPopup: false });
  };

  handleBackToItemBankEditor = () => {
    // resetting needed redux states
    this.props.setBundleData({});
    this.props.setBundleInitialData({});
    this.props.resetBundlesAddItemsStates();
    this.props.resetBundlesAddBundlesStates();
    history.push(PATH.itemBankEditor);
  };

  getTotalAmountOfAssociatedBundles = () => {
    // initializing totalAmountOfAssociatedBundles
    let totalAmountOfAssociatedBundles = 0;

    // adding associated bundles amount
    totalAmountOfAssociatedBundles += this.props.bundleData.bundle_associations.filter(
      obj => obj.system_id_link === null
    ).length;

    // looping in bundle rules
    for (let i = 0; i < this.props.bundleData.bundle_bundles_rule_data.length; i++) {
      // adding bundle rule associations amount
      totalAmountOfAssociatedBundles +=
        this.props.bundleData.bundle_bundles_rule_data[i].bundle_bundles_rule_associations.length;
    }

    return totalAmountOfAssociatedBundles;
  };

  getTABS = section => {
    switch (section) {
      case SECTIONS.general:
        return [
          {
            key: `${SECTIONS.general}-1`,
            tabName:
              LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles
                .topTabs.generalTabs.general,
            body: <GeneralTab />
          }
        ];
      case SECTIONS.add:
        return [
          {
            key: `${SECTIONS.add}-1`,
            tabName:
              LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles
                .topTabs.addTabs.addItems,
            body: <AddItemsTab />
          },
          {
            key: `${SECTIONS.add}-2`,
            tabName:
              LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles
                .topTabs.addTabs.addBundles,
            body: <AddBundlesTab />
          }
        ];
      case SECTIONS.content:
        return [
          {
            key: `${SECTIONS.content}-1`,
            tabName: LOCALIZE.formatString(
              LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles
                .topTabs.contentTabs.itemContent,
              this.props.bundleData.bundle_associations.filter(obj => obj.system_id_link !== null)
                .length
            ),
            body: <ItemContentTab />
          },
          {
            key: `${SECTIONS.content}-2`,
            tabName: LOCALIZE.formatString(
              LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles
                .topTabs.contentTabs.bundleContent,
              this.getTotalAmountOfAssociatedBundles()
            ),
            body: <BundleContentTab />
          }
        ];
      default:
        return null;
    }
  };

  openGoNextConfirmationPopup = () => {
    this.setState({ showGoNextItemConfirmationPopup: true });
  };

  closeGoNextConfirmationPopup = () => {
    this.setState({ showGoNextItemConfirmationPopup: false });
  };

  openGoPreviousConfirmationPopup = () => {
    this.setState({ showGoPreviousItemConfirmationPopup: true });
  };

  closeGoPreviousConfirmationPopup = () => {
    this.setState({ showGoPreviousItemConfirmationPopup: false });
  };

  getPreviousButtonDisableState = () => {
    // initializing disabled
    let disabled = false;
    // no more than one question
    if (this.props.item_bank_data.list_of_bundles.length <= 1) {
      disabled = true;
    }

    // getting index of current question
    const indexOfCurrentBundle = this.props.item_bank_data.list_of_bundles.findIndex(obj => {
      return obj.id === this.props.bundleData.id;
    });
    // first element of the array
    if (indexOfCurrentBundle === 0) {
      disabled = true;
    }

    return disabled;
  };

  getNextButtonDisableState = () => {
    // initializing disabled
    let disabled = false;
    // no more than one question
    if (this.props.item_bank_data.list_of_bundles.length <= 1) {
      disabled = true;
    }

    // getting index of current question
    const indexOfCurrentBundle = this.props.item_bank_data.list_of_bundles.findIndex(obj => {
      return obj.id === this.props.bundleData.id;
    });
    // last element of the array
    if (indexOfCurrentBundle === this.props.item_bank_data.list_of_bundles.length - 1) {
      disabled = true;
    }

    return disabled;
  };

  handlePreviousAction = () => {
    // getting index of current question
    const indexOfCurrentBundle = this.props.item_bank_data.list_of_bundles.findIndex(obj => {
      return obj.id === this.props.bundleData.id;
    });

    // updating redux states
    this.props
      .getSpecificBundle(this.props.item_bank_data.list_of_bundles[indexOfCurrentBundle - 1].id)
      .then(response => {
        const previous_bundle = response;

        this.props.setBundleData(previous_bundle);
        this.props.setBundleInitialData(previous_bundle);
        // updating local unlinked system IDs redux state
        this.props.setBundleLocalUnlinkedSystemIds([]);
        // closing popup
        this.closeGoPreviousConfirmationPopup();
        // updating ready for upload redux states
        this.props.setDataReadyForUpload(false);
      });
  };

  handleNextAction = () => {
    // getting index of current question
    const indexOfCurrentBundle = this.props.item_bank_data.list_of_bundles.findIndex(obj => {
      return obj.id === this.props.bundleData.id;
    });
    // updating redux states
    this.props
      .getSpecificBundle(this.props.item_bank_data.list_of_bundles[indexOfCurrentBundle + 1].id)
      .then(response => {
        const next_bundle = response;

        this.props.setBundleData(next_bundle);
        this.props.setBundleInitialData(next_bundle);
        // updating local unlinked system IDs redux state
        this.props.setBundleLocalUnlinkedSystemIds([]);
        // closing popup
        this.closeGoPreviousConfirmationPopup();
        // updating ready for upload redux states
        this.props.setDataReadyForUpload(false);
      });
  };

  handleUpload = () => {
    // creating a copy of bundleData
    const copyOfBundleData = _.cloneDeep(this.props.bundleData);

    // there are bundle rules
    if (copyOfBundleData.bundle_bundles_rule_data.length > 0) {
      // removing all bundle bundle associations
      copyOfBundleData.bundle_associations = copyOfBundleData.bundle_associations.filter(obj => {
        return obj.system_id_link !== null;
      });
    }

    this.props.uploadBundle(copyOfBundleData).then(response => {
      // there is no error
      if (!response.error) {
        // updating bundle relatd redux states
        this.props.setBundleData(response);
        this.props.setBundleInitialData(response);

        const copyOfItemBankData = _.cloneDeep(this.props.item_bank_data);
        copyOfItemBankData.list_of_bundles = response.list_of_bundles;
        this.props.setItemBankData(copyOfItemBankData);
        // updating ready for upload redux states
        this.props.setDataReadyForUpload(false);
        this.props.setValidBundleItemsBasicRuleState(true);
        // updating local unlinked system IDs redux state
        this.props.setBundleLocalUnlinkedSystemIds([]);
        // open upload successful popup
        this.openUploadSuccessfulPopup();
        // triggering bundles table rerender
        this.props.triggerBundlesTableRerender();
        // if there is a validation error
      } else if (response.error) {
        // bundle name already exists error
        if (response.error_code === 1) {
          // setting bundleNameAlreadyUsed to true
          this.setState({ bundleNameAlreadyUsed: true }, () => {
            this.openUploadSuccessfulPopup();
          });
          // bundle structural conflict error
        } else if (response.error_code === 2) {
          // setting showBundleStructuralConflictError to true
          this.setState({ showBundleStructuralConflictError: true }, () => {
            this.openUploadSuccessfulPopup();
          });
          // duplicate found in bundle associations error
        } else if (response.error_code === 3) {
          // setting showDuplicateSystemIdsFoundInBundleAssociationsError to true
          this.setState({ showDuplicateSystemIdsFoundInBundleAssociationsError: true }, () => {
            this.openUploadSuccessfulPopup();
          });
          // should never happen
        } else {
          throw new Error("An error occurred during the upload bundle process");
        }

        // should never happen
      } else {
        throw new Error("An error occurred during the upload bundle process");
      }
    });
  };

  openPreviewBundlePopup = () => {
    this.setState({ showPreviewBundlePopup: true, isLoadingPreviewBundleData: true }, () => {
      this.props.previewGeneratedItemsListFromBundle(this.props.bundleData).then(response => {
        // there is no error or there is the bundle name already exists error (we don't care about this error for the preview functionality)
        if (!response.error || response.error_code === 1) {
          this.setState({ previewBundleData: response }, () => {
            this.setState({ isLoadingPreviewBundleData: false });
          });
        } else if (response.error) {
          // bundle structural conflict error
          if (response.error_code === 2) {
            // setting showBundleStructuralConflictError to true
            this.setState({
              showBundleStructuralConflictError: true,
              isLoadingPreviewBundleData: false
            });
            // duplicate found in bundle associations error
          } else if (response.error_code === 3) {
            // setting showDuplicateSystemIdsFoundInBundleAssociationsError to true
            this.setState({
              showDuplicateSystemIdsFoundInBundleAssociationsError: true,
              isLoadingPreviewBundleData: false
            });
            // should never happen
          } else {
            throw new Error("An error occurred during the preview bundle process");
          }
        }
      });
    });
  };

  generateNewItemsListFromBundle = () => {
    this.setState({ isLoadingPreviewNewBundleData: true }, () => {
      this.props.previewGeneratedItemsListFromBundle(this.props.bundleData).then(response => {
        this.setState({ previewBundleData: response }, () => {
          this.setState({ isLoadingPreviewNewBundleData: false });
        });
      });
    });
  };

  closePreviewBundlePopup = () => {
    this.setState({
      showPreviewBundlePopup: false,
      previewBundleData: [],
      isLoadingPreviewNewBundleData: false,
      showBundleStructuralConflictError: false,
      showDuplicateSystemIdsFoundInBundleAssociationsError: false
    });
  };

  openUploadSuccessfulPopup = () => {
    this.setState({ showUploadSuccessfulPopup: true });
  };

  closeUploadSuccessfulPopup = () => {
    this.setState({
      showUploadSuccessfulPopup: false,
      bundleNameAlreadyUsed: false,
      showBundleStructuralConflictError: false,
      showDuplicateSystemIdsFoundInBundleAssociationsError: false
    });
  };

  render() {
    const generalTabs = this.getTABS(SECTIONS.general);
    const addTabs = this.getTABS(SECTIONS.add);
    const contentTabs = this.getTABS(SECTIONS.content);

    return (
      <div>
        {this.state.isLoading ? (
          <div style={styles.loadingContainer}>
            <label className="fa fa-spinner fa-spin">
              <FontAwesomeIcon icon={faSpinner} style={styles.loading} />
            </label>
          </div>
        ) : (
          <div className="app" style={styles.appPadding}>
            <Helmet>
              <html lang={this.props.currentLanguage} />
              <title>{LOCALIZE.titles.itemEditor}</title>
            </Helmet>
            <ContentContainer customMaxWidth={maxWidthUtils.wide}>
              <div>
                <div style={styles.backButtonAndTitleContainer}>
                  <div style={styles.backButtonContainer}>
                    <CustomButton
                      buttonId="back-to-item-bank-editor-btn"
                      label={
                        <>
                          <FontAwesomeIcon icon={faArrowAltCircleLeft} />
                          <span style={styles.buttonLabel}>
                            {
                              LOCALIZE.testBuilder.testDefinition.selectTestDefinition
                                .itemBankEditor.bundles.backToItemBankEditorButton
                            }
                          </span>
                        </>
                      }
                      action={
                        this.props.dataReadyForUpload
                          ? this.openBackToItemBankEditorPopup
                          : this.handleBackToItemBankEditor
                      }
                      buttonTheme={THEME.SECONDARY}
                    />
                  </div>
                  <div style={styles.titleContainer}>
                    <h2 style={styles.h2}>
                      {LOCALIZE.formatString(
                        LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                          .bundles.bundleTitle,
                        this.props.bundleData.name
                      )}
                    </h2>
                  </div>
                </div>
                <Row role="presentation">
                  <Col style={styles.colOneStyle}>
                    <TopTabs
                      TABS={generalTabs}
                      defaultTab={`${SECTIONS.general}-1`}
                      customStyle={styles.generalTab}
                      setTab={this.props.setFirstColumnTab}
                      customCurrentTabProp={this.props.firstColumnTab}
                    />
                  </Col>
                  <Col style={styles.colTwoStyle}>
                    <TopTabs
                      TABS={addTabs}
                      defaultTab={`${SECTIONS.add}-1`}
                      customStyle={styles.addTab}
                      setTab={this.props.setSecondColumnTab}
                      customCurrentTabProp={this.props.secondColumnTab}
                      addTabButtonAction={this.openAddAlternativeContentTabPopup}
                    />
                  </Col>
                  <Col style={styles.colThreeStyle}>
                    <TopTabs
                      TABS={contentTabs}
                      defaultTab={`${SECTIONS.content}-1`}
                      customStyle={styles.contentTab}
                      setTab={this.props.setThirdColumnTab}
                      customCurrentTabProp={this.props.thirdColumnTab}
                    />
                  </Col>
                </Row>
              </div>
              <ItemBankFooter
                previewQuestion={this.openPreviewBundlePopup}
                previousAction={
                  this.props.dataReadyForUpload
                    ? this.openGoPreviousConfirmationPopup
                    : this.handlePreviousAction
                }
                nextAction={
                  this.props.dataReadyForUpload
                    ? this.openGoNextConfirmationPopup
                    : this.handleNextAction
                }
                hideSaveButton={true}
                triggerUploadPopup={false}
                uploadAction={this.handleUpload}
                disabledPreviousButton={this.getPreviousButtonDisableState()}
                disabledNextButton={this.getNextButtonDisableState()}
                disabledPreviousAndNextBtns={this.props.item_bank_data.list_of_bundles.length <= 1}
              />
            </ContentContainer>
            <PopupBox
              show={this.state.showBackToItemBankEditorPopup}
              handleClose={this.closeBackToItemBankEditorPopup}
              title={
                LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                  .backToItemBankEditorPopup.title
              }
              description={
                <div>
                  <SystemMessage
                    messageType={MESSAGE_TYPE.warning}
                    title={LOCALIZE.commons.warning}
                    message={
                      <p>
                        {
                          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                            .items.backToItemBankEditorPopup.systemMessageDescription
                        }
                      </p>
                    }
                  />
                  <p>
                    {
                      LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                        .backToItemBankEditorPopup.description
                    }
                  </p>
                </div>
              }
              leftButtonType={BUTTON_TYPE.secondary}
              leftButtonTitle={LOCALIZE.commons.cancel}
              leftButtonIcon={faTimes}
              rightButtonType={BUTTON_TYPE.primary}
              rightButtonTitle={LOCALIZE.commons.confirm}
              rightButtonAction={this.handleBackToItemBankEditor}
              rightButtonIcon={faCheck}
              size="lg"
            />
          </div>
        )}
        {this.state.showPreviewBundlePopup && (
          <PopupBox
            show={this.state.showPreviewBundlePopup}
            handleClose={() => {}}
            title={LOCALIZE.formatString(
              LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles
                .previewBundlePopup.title
            )}
            description={
              <div
                style={
                  !this.state.isLoadingPreviewBundleData &&
                  !this.state.showBundleStructuralConflictError &&
                  !this.state.showDuplicateSystemIdsFoundInBundleAssociationsError
                    ? styles.itemContentMainContainer
                    : {}
                }
              >
                {this.state.isLoadingPreviewBundleData ? (
                  <div style={styles.previewBundleDataLoadingContainer}>
                    <label className="fa fa-spinner fa-spin">
                      <FontAwesomeIcon icon={faSpinner} style={styles.loading} />
                    </label>
                  </div>
                ) : this.state.showBundleStructuralConflictError ? (
                  <SystemMessage
                    messageType={MESSAGE_TYPE.error}
                    title={LOCALIZE.commons.error}
                    message={
                      <p>
                        {
                          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                            .bundles.previewBundlePopup
                            .systemMessageErrorBundleStructuralConflictError
                        }
                      </p>
                    }
                  />
                ) : this.state.showDuplicateSystemIdsFoundInBundleAssociationsError ? (
                  <SystemMessage
                    messageType={MESSAGE_TYPE.error}
                    title={LOCALIZE.commons.error}
                    message={
                      <p>
                        {
                          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                            .bundles.previewBundlePopup
                            .systemMessageErrorDuplicateSystemIdsFoundInBundleAssociationsError
                        }
                      </p>
                    }
                  />
                ) : (
                  this.state.previewBundleData.map((itemData, index) => {
                    return (
                      <div
                        style={
                          index !== this.state.previewBundleData.length - 1
                            ? styles.itemContentContainer
                            : {}
                        }
                      >
                        <h1 style={styles.customH1Style}>
                          {LOCALIZE.formatString(
                            LOCALIZE.mcTest.questionList.questionIdLong,
                            index + 1
                          )}
                        </h1>
                        <div>
                          <ItemComponent
                            itemContent={getItemContentToPreview(
                              itemData,
                              this.props.languageData,
                              // hardcoding content-1 tab, since we only need to provide the regular content for now
                              [{ key: "content-1" }],
                              "content-1"
                            )}
                            index={index}
                            // getting right language (if item bank language is bilingual, use the currentLanguage props, else use the item bank language)
                            languageCode={
                              this.props.item_bank_data.item_bank_definition[0].language_id === null
                                ? this.props.currentLanguage
                                : this.props.languageData.filter(
                                    obj =>
                                      obj.language_id ===
                                      this.props.item_bank_data.item_bank_definition[0].language_id
                                  )[0].ISO_Code_1
                            }
                            calledFromQuestionPreview={true}
                          />
                        </div>
                        <div>
                          <ItemOptions
                            itemOptions={getItemOptionsToPreview(
                              itemData,
                              this.props.languageData,
                              // hardcoding content-1 tab, since we only need to provide the regular content for now
                              [{ key: "content-1" }],
                              "content-1"
                            )}
                            // getting right language (if item bank language is bilingual, use the currentLanguage props, else use the item bank language)
                            languageCode={
                              this.props.item_bank_data.item_bank_definition[0].language_id === null
                                ? this.props.currentLanguage
                                : this.props.languageData.filter(
                                    obj =>
                                      obj.language_id ===
                                      this.props.item_bank_data.item_bank_definition[0].language_id
                                  )[0].ISO_Code_1
                            }
                            calledFromQuestionPreview={true}
                          />
                        </div>
                      </div>
                    );
                  })
                )}
              </div>
            }
            leftButtonType={BUTTON_TYPE.secondary}
            leftButtonTitle={
              !this.state.isLoadingPreviewNewBundleData ? (
                LOCALIZE.commons.generate
              ) : (
                // eslint-disable-next-line jsx-a11y/label-has-associated-control
                <label className="fa fa-spinner fa-spin">
                  <FontAwesomeIcon icon={faSpinner} />
                </label>
              )
            }
            leftButtonAction={this.generateNewItemsListFromBundle}
            leftButtonIcon={!this.state.isLoadingPreviewNewBundleData ? faMagic : ""}
            leftButtonState={
              this.state.isLoadingPreviewNewBundleData
                ? BUTTON_STATE.disabled
                : this.state.bundleNameAlreadyUsed ||
                  this.state.showBundleStructuralConflictError ||
                  this.state.showDuplicateSystemIdsFoundInBundleAssociationsError
                ? BUTTON_STATE.disabled
                : BUTTON_STATE.enabled
            }
            rightButtonType={BUTTON_TYPE.primary}
            rightButtonTitle={LOCALIZE.commons.close}
            rightButtonAction={this.closePreviewBundlePopup}
            rightButtonIcon={faTimes}
            rightButtonState={
              this.state.isLoadingPreviewNewBundleData
                ? BUTTON_STATE.disabled
                : BUTTON_STATE.enabled
            }
          />
        )}
        <PopupBox
          show={this.state.showGoNextItemConfirmationPopup}
          handleClose={this.closeGoNextConfirmationPopup}
          title={
            LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles
              .changeBundleConfirmationPopup.goNextTitle
          }
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.warning}
                title={LOCALIZE.commons.warning}
                message={
                  <p>
                    {
                      LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                        .bundles.changeBundleConfirmationPopup.systemMessageDescription
                    }
                  </p>
                }
              />
              <p>
                {
                  LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles
                    .changeBundleConfirmationPopup.description
                }
              </p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.confirm}
          rightButtonAction={this.handleNextAction}
          rightButtonIcon={faCheck}
          size="lg"
        />
        <PopupBox
          show={this.state.showGoPreviousItemConfirmationPopup}
          handleClose={this.closeGoPreviousConfirmationPopup}
          title={
            LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles
              .changeBundleConfirmationPopup.goPreviousTitle
          }
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.warning}
                title={LOCALIZE.commons.warning}
                message={
                  <p>
                    {
                      LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                        .bundles.changeBundleConfirmationPopup.systemMessageDescription
                    }
                  </p>
                }
              />
              <p>
                {
                  LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles
                    .changeBundleConfirmationPopup.description
                }
              </p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.confirm}
          rightButtonAction={this.handlePreviousAction}
          rightButtonIcon={faCheck}
          size="lg"
        />
        <PopupBox
          show={this.state.showUploadSuccessfulPopup}
          handleClose={this.closeUploadSuccessfulPopup}
          title={
            this.state.bundleNameAlreadyUsed ||
            this.state.showBundleStructuralConflictError ||
            this.state.showDuplicateSystemIdsFoundInBundleAssociationsError
              ? LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles
                  .uploadSuccessfulPopup.titleWithError
              : LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles
                  .uploadSuccessfulPopup.title
          }
          description={
            <div>
              {this.state.bundleNameAlreadyUsed ? (
                <SystemMessage
                  messageType={MESSAGE_TYPE.error}
                  title={LOCALIZE.commons.error}
                  message={
                    <p>
                      {
                        LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                          .bundles.uploadSuccessfulPopup.systemMessageErrorBundleNameAlreadyUsed
                      }
                    </p>
                  }
                />
              ) : this.state.showBundleStructuralConflictError ? (
                <SystemMessage
                  messageType={MESSAGE_TYPE.error}
                  title={LOCALIZE.commons.error}
                  message={
                    <p>
                      {
                        LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                          .bundles.uploadSuccessfulPopup
                          .systemMessageErrorBundleStructuralConflictError
                      }
                    </p>
                  }
                />
              ) : this.state.showDuplicateSystemIdsFoundInBundleAssociationsError ? (
                <SystemMessage
                  messageType={MESSAGE_TYPE.error}
                  title={LOCALIZE.commons.error}
                  message={
                    <p>
                      {
                        LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                          .bundles.uploadSuccessfulPopup
                          .systemMessageErrorDuplicateSystemIdsFoundInBundleAssociationsError
                      }
                    </p>
                  }
                />
              ) : (
                <SystemMessage
                  messageType={MESSAGE_TYPE.success}
                  title={LOCALIZE.commons.success}
                  message={
                    <p>
                      {
                        LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                          .bundles.uploadSuccessfulPopup.systemMessageDescription
                      }
                    </p>
                  }
                />
              )}
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.ok}
          rightButtonAction={this.closeUploadSuccessfulPopup}
          size="lg"
        />
      </div>
    );
  }
}

export { BundleEditor as unconnectedBundleEditor };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    item_bank_data: state.itemBank.item_bank_data,
    itemData: state.itemBank.itemData,
    bundleData: state.itemBank.bundleData,
    firstColumnTab: state.itemBank.firstColumnTab,
    secondColumnTab: state.itemBank.secondColumnTab,
    thirdColumnTab: state.itemBank.thirdColumnTab,
    dataReadyForUpload: state.itemBank.dataReadyForUpload,
    languageData: state.localize.languageData
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setItemData,
      setFirstColumnTab,
      setSecondColumnTab,
      setThirdColumnTab,
      setBundleData,
      setBundleInitialData,
      triggerAppRerender,
      uploadBundle,
      setDataReadyForUpload,
      setItemBankData,
      resetBundlesAddItemsStates,
      setBundleLocalUnlinkedSystemIds,
      getSpecificBundle,
      resetBundlesAddBundlesStates,
      triggerBundlesTableRerender,
      setValidBundleItemsBasicRuleState,
      previewGeneratedItemsListFromBundle
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(BundleEditor);
