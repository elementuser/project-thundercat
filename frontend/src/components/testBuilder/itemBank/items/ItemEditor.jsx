/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../../../text_resources";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import ContentContainer, { maxWidthUtils } from "../../../commons/ContentContainer";
import CustomButton, { THEME } from "../../../commons/CustomButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faArrowAltCircleLeft,
  faCheck,
  faPlusCircle,
  faSpinner,
  faTimes,
  faTrashAlt
} from "@fortawesome/free-solid-svg-icons";
import { history } from "../../../../store-index";
import { PATH } from "../../../commons/Constants";
import {
  setItemData,
  setItemInitialData,
  resetItemsValidationErrorStates,
  setFirstColumnTab,
  setSecondColumnTab,
  setThirdColumnTab,
  setConditionalContentTabsData,
  getItemData,
  updateItemData,
  setDataChangesDetected,
  setDataReadyForUpload,
  setItemBankData,
  setItemBankInitialData
} from "../../../../modules/ItemBankRedux";
import PopupBox, { BUTTON_STATE, BUTTON_TYPE } from "../../../commons/PopupBox";
import SystemMessage, { MESSAGE_TYPE } from "../../../commons/SystemMessage";
import TopTabs from "../../../commons/TopTabs";
import { Col, Row } from "react-bootstrap";
import GeneralTab from "./GeneralTab";
import ContentTab from "./ContentTab";
import CategoriesTab from "./CategoriesTab";
import CommentsTab from "./CommentsTab";
import TranslationsTab from "./TranslationsTab";
import ItemBankFooter, { VERSION_CONTROL_RADIO_BUTTON } from "../ItemBankFooter";
import { ITEM_ACTION_TYPE } from "../Constants";
import ItemComponent from "./ItemComponent";
import ItemOptions from "./ItemOptions";
import DropdownSelect from "../../../commons/DropdownSelect";
import { ITEM_CONTENT_TYPE, ITEM_OPTION_TYPE } from "./Constants";
import ScreenReaderTab from "./ScreenReaderTab";
import StyledTooltip, { EFFECT, TRIGGER_TYPE, TYPE } from "../../../authentication/StyledTooltip";
import _ from "lodash";
import getItemContentToPreview, {
  getItemOptionsToPreview
} from "../../../../helpers/itemBankItemPreview";
import { triggerAppRerender } from "../../../../modules/AppRedux";

export const SECTIONS = {
  general: "general",
  content: "content",
  other: "other"
};

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 6,
    xl: 6
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 6,
    xl: 6
  }
};

const styles = {
  loadingContainer: {
    margin: 24,
    textAlign: "center"
  },
  loading: {
    fontSize: "32px",
    padding: 6
  },
  backButtonAndTitleContainer: {
    marginBottom: 12
  },
  backButtonContainer: {
    position: "absolute"
  },
  titleContainer: {
    textAlign: "center"
  },
  h2: {
    margin: 0,
    padding: 0
  },
  colOneStyle: {
    padding: 0,
    maxWidth: "15%"
  },
  colTwoStyle: {
    padding: 0,
    maxWidth: "45%"
  },
  colThreeStyle: {
    padding: 0,
    maxWidth: "40%"
  },
  generalTab: {
    paddingRight: 3
  },
  contentTab: {
    paddingLeft: 3,
    paddingRight: 3
  },
  otherTab: {
    paddingLeft: 3
  },
  buttonLabel: {
    marginLeft: 6
  },
  sectionContainer: {
    borderStyle: "solid",
    borderWidth: "3px 1px 1px 1px",
    borderColor: "#CECECE",
    minHeight: 400,
    padding: "12px 0",
    marginBottom: 24
  },
  body: {
    paddingLeft: 12
  },
  appPadding: {
    padding: "15px 15px 0 15px"
  },
  alternativeTypeContainer: {
    width: "80%",
    margin: "24px auto"
  },
  allUnset: {
    all: "unset"
  },
  alternativeContentTabLabel: {
    paddingRight: 6
  },
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    color: "#00565e",
    padding: "0 3px",
    margin: 0
  }
};

class ItemEditor extends Component {
  state = {
    isLoading: true,
    showBackToItemBankEditorPopup: false,
    showPreviewQuestionPopup: false,
    showGoNextItemConfirmationPopup: false,
    showGoPreviousItemConfirmationPopup: false,
    itemBankLanguageData: "",
    showAddContentTabPopup: false,
    alternativeContentTypeOptions: [],
    selectedAlternativeContentType: "",
    showDeleteContentTabPopup: false,
    tabKeyToBeDeleted: null
  };

  componentDidMount = () => {
    // making sure that the item data ID is defined (otherwise, redirect the user to the item bank editor page)
    // can happen if the URL is manually entered or if the back browser button has been pressed
    if (typeof this.props.itemData.item_id === "undefined") {
      setTimeout(() => {
        if (typeof this.props.itemData.item_id === "undefined") {
          history.push(PATH.itemBankEditor);
        } else {
          this.setState({ isLoading: false });
        }
      }, 3000);
    } else {
      this.setState({ isLoading: false });
    }
    // checking if we're in a draft version
    if (typeof this.props.itemData.version === "undefined") {
      this.props.setDataReadyForUpload(true);
    } else {
      this.props.setDataReadyForUpload(false);
    }
    // getting item bank language data
    this.getItemBankLanguageData();
    // getting content tabs
    this.getConditionalContentTabs();
    // populating alternative content type options
    this.populateAlternativeContentTypeOptions();
    // triggering app rerender to make sure that the special page height and overflow is properly set
    this.props.triggerAppRerender();
  };

  componentDidUpdate = prevProps => {
    // if itemData gets updates
    if (prevProps.itemData !== this.props.itemData) {
      // checking if we're in a draft version
      if (typeof this.props.itemData.version === "undefined") {
        this.props.setDataReadyForUpload(true);
      } else {
        this.props.setDataReadyForUpload(false);
      }
      // getting content tabs
      this.getConditionalContentTabs();
    }
  };

  getConditionalContentTabs = () => {
    // initializing conditionalContentTabsData
    const conditionalContentTabsData = [];
    // checking if Item Data contains alternative content
    // looping in languageData
    for (let i = 0; i < this.props.languageData.length; i++) {
      // item content in language of current iteration is defined
      if (
        typeof this.props.itemData.item_content[this.props.languageData[i].ISO_Code_1] !==
        "undefined"
      ) {
        // looping in item content in language of current iteration
        for (
          let j = 0;
          j < this.props.itemData.item_content[this.props.languageData[i].ISO_Code_1].length;
          j++
        ) {
          if (
            this.props.itemData.item_content[this.props.languageData[i].ISO_Code_1][j]
              .content_type !== ITEM_CONTENT_TYPE.markdown &&
            !conditionalContentTabsData.includes(
              this.props.itemData.item_content[this.props.languageData[i].ISO_Code_1][j]
                .content_type
            )
          ) {
            conditionalContentTabsData.push(
              this.props.itemData.item_content[this.props.languageData[i].ISO_Code_1][j]
                .content_type
            );
          }
        }
      }
      // item options in language of current iteration is defined
      if (
        typeof this.props.itemData.item_options[this.props.languageData[i].ISO_Code_1] !==
        "undefined"
      ) {
        // looping in item content in language of current iteration
        for (
          let j = 0;
          j < this.props.itemData.item_options[this.props.languageData[i].ISO_Code_1].length;
          j++
        ) {
          if (
            this.props.itemData.item_options[this.props.languageData[i].ISO_Code_1][j]
              .option_type !== ITEM_OPTION_TYPE.multipleChoice &&
            !conditionalContentTabsData.includes(
              this.props.itemData.item_options[this.props.languageData[i].ISO_Code_1][j].option_type
            )
          ) {
            conditionalContentTabsData.push(
              this.props.itemData.item_options[this.props.languageData[i].ISO_Code_1][j].option_type
            );
          }
        }
      }
    }
    this.props.setConditionalContentTabsData(conditionalContentTabsData);
    // adding small delay, so the redux states are up to date
    setTimeout(() => {
      // repopulating content type options
      this.populateAlternativeContentTypeOptions();
    }, 100);
  };

  populateAlternativeContentTypeOptions = () => {
    const alternativeContentTypeOptions = [];
    // screenReader content type had not been selected yet
    if (this.props.conditionalContentTabsData.indexOf(ITEM_CONTENT_TYPE.screenReader) < 0) {
      // add screen reader option
      alternativeContentTypeOptions.push({
        label:
          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items.content
            .addAlternativeContentPopup.alternativeContentTypeOptions.screenReader,
        value: ITEM_CONTENT_TYPE.screenReader
      });
    }
    this.setState({ alternativeContentTypeOptions: alternativeContentTypeOptions });
  };

  getItemBankLanguageData = () => {
    // initializing itemBankLanguageData
    let itemBankLanguageData = "";
    // looping in languageData
    for (let i = 0; i < this.props.languageData.length; i++) {
      if (
        this.props.languageData[i].language_id ===
        this.props.item_bank_data.item_bank_definition[0].language_id
      ) {
        itemBankLanguageData =
          this.props.languageData[i].language_text[this.props.currentLanguage][0].text;
        break;
      }
    }

    // itemBankLanguageData is still "" ==> that's a bilingual item bank
    if (itemBankLanguageData === "") {
      itemBankLanguageData = LOCALIZE.commons.bilingual;
    }

    // setting state
    this.setState({
      itemBankLanguageData: itemBankLanguageData
    });
  };

  openBackToItemBankEditorPopup = () => {
    this.setState({ showBackToItemBankEditorPopup: true });
  };

  closeBackToItemBankEditorPopup = () => {
    this.setState({ showBackToItemBankEditorPopup: false });
  };

  handleBackToItemBankEditor = () => {
    // resetting needed redux states
    this.props.setItemData({});
    this.props.setItemInitialData({});
    this.props.resetItemsValidationErrorStates();
    history.push(PATH.itemBankEditor);
  };

  openGoNextConfirmationPopup = () => {
    this.setState({ showGoNextItemConfirmationPopup: true });
  };

  closeGoNextConfirmationPopup = () => {
    this.setState({ showGoNextItemConfirmationPopup: false });
  };

  openGoPreviousConfirmationPopup = () => {
    this.setState({ showGoPreviousItemConfirmationPopup: true });
  };

  closeGoPreviousConfirmationPopup = () => {
    this.setState({ showGoPreviousItemConfirmationPopup: false });
  };

  openAddAlternativeContentTabPopup = () => {
    this.setState({ showAddContentTabPopup: true });
  };

  closeAddAlternativeContentTabPopup = () => {
    this.setState({ showAddContentTabPopup: false, selectedAlternativeContentType: "" });
  };

  onAlternativeContentTypeChange = selectedOption => {
    this.setState({ selectedAlternativeContentType: selectedOption });
  };

  handleAddAlternativeContentTab = () => {
    // initializing copyOfConditionalContentTabsData
    let copyOfConditionalContentTabsData = [];
    if (this.props.conditionalContentTabsData) {
      copyOfConditionalContentTabsData = this.props.conditionalContentTabsData;
    }
    copyOfConditionalContentTabsData.push(this.state.selectedAlternativeContentType.value);
    // updating redux states
    this.props.setConditionalContentTabsData(copyOfConditionalContentTabsData);
    // adding small delay, so the redux states are up to date
    setTimeout(() => {
      // closing popup
      this.closeAddAlternativeContentTabPopup();
      // repopulating content type options
      this.populateAlternativeContentTypeOptions();
    }, 100);
  };

  openDeleteAlternativeContentTabPopup = tabKey => {
    this.setState({ showDeleteContentTabPopup: true, tabKeyToBeDeleted: tabKey });
  };

  closeDeleteAlternativeContentTabPopup = () => {
    this.setState({ showDeleteContentTabPopup: false, tabKeyToBeDeleted: null });
  };

  handleDeleteAlternativeContentTab = () => {
    // initializing copyOfConditionalContentTabsData
    let copyOfConditionalContentTabsData = this.props.conditionalContentTabsData;
    // getting content type based on tab key to be deleted
    const contentTypeFromTabKey = parseInt(this.state.tabKeyToBeDeleted.split("-")[1]);
    // deleting respective item in array
    copyOfConditionalContentTabsData = copyOfConditionalContentTabsData.filter(value => {
      return value !== contentTypeFromTabKey;
    });
    // deleting respective content
    switch (contentTypeFromTabKey) {
      // Screen reader Content/Options
      case ITEM_CONTENT_TYPE.screenReader:
        // creating a copy of item content (screen reader)
        const copyOfItemContent = _.cloneDeep(this.props.itemData.item_content);
        // creating a copy of item options (multiple choice screen reader)
        const copyOfItemOptions = _.cloneDeep(this.props.itemData.item_options);
        // looping in languageData
        for (let i = 0; i < this.props.languageData.length; i++) {
          // copyOfItemContent in language of current iteration is defined
          if (typeof copyOfItemContent[this.props.languageData[i].ISO_Code_1] !== "undefined") {
            copyOfItemContent[this.props.languageData[i].ISO_Code_1] = copyOfItemContent[
              this.props.languageData[i].ISO_Code_1
            ].filter(obj => {
              return obj.content_type !== ITEM_CONTENT_TYPE.screenReader;
            });
          }
          // copyOfItemOptions in language of current iteration is defined
          if (typeof copyOfItemOptions[this.props.languageData[i].ISO_Code_1] !== "undefined") {
            copyOfItemOptions[this.props.languageData[i].ISO_Code_1] = copyOfItemOptions[
              this.props.languageData[i].ISO_Code_1
            ].filter(obj => {
              return obj.option_type !== ITEM_OPTION_TYPE.multipleChoiceScreenReader;
            });
          }
        }
        // updating redux states
        // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
        const new_obj = {
          ...this.props.itemData,
          item_content: copyOfItemContent,
          item_options: copyOfItemOptions
        };

        // setting new itemData redux state
        this.props.setItemData(new_obj, this.props.languageData);
        break;
      default:
        // do nothing
        break;
    }
    // updating redux states
    this.props.setConditionalContentTabsData(copyOfConditionalContentTabsData);
    // adding small delay, so the redux states are up to date
    setTimeout(() => {
      // closing popup
      this.closeDeleteAlternativeContentTabPopup();
      // repopulating content type options
      this.populateAlternativeContentTypeOptions();
    }, 100);
  };

  getTABS = section => {
    switch (section) {
      case SECTIONS.general:
        return [
          {
            key: `${SECTIONS.general}-1`,
            tabName:
              LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items.topTabs
                .generalTabs.generaL,
            body: <GeneralTab />
          }
        ];
      case SECTIONS.content:
        const conditionalContentTabs = [
          {
            key: `${SECTIONS.content}-1`,
            tabName:
              LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items.topTabs
                .contentTabs.content,
            body: <ContentTab />
          }
        ];
        // conditionalContentTabsData is defined
        if (typeof this.props.conditionalContentTabsData !== "undefined") {
          // looping in conditionalContentTabsData array
          for (let i = 0; i < this.props.conditionalContentTabsData.length; i++) {
            switch (this.props.conditionalContentTabsData[i]) {
              case ITEM_CONTENT_TYPE.screenReader:
                conditionalContentTabs.push({
                  key: `${SECTIONS.content}-${ITEM_CONTENT_TYPE.screenReader}`,
                  tabName: (
                    <>
                      <span style={styles.alternativeContentTabLabel}>
                        {
                          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                            .items.topTabs.contentTabs.screenReader
                        }
                      </span>
                      <StyledTooltip
                        id={`delete-${SECTIONS.content}-${i + 2}-tooltip`}
                        place="top"
                        type={TYPE.light}
                        effect={EFFECT.solid}
                        triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
                        tooltipElement={
                          <div style={styles.allUnset}>
                            <CustomButton
                              dataTip=""
                              dataFor={`delete-${SECTIONS.content}-${i + 2}-tooltip`}
                              label={
                                <>
                                  <FontAwesomeIcon icon={faTrashAlt} />
                                </>
                              }
                              action={() =>
                                this.openDeleteAlternativeContentTabPopup(
                                  `${SECTIONS.content}-${ITEM_CONTENT_TYPE.screenReader}`
                                )
                              }
                              customStyle={styles.actionButton}
                              buttonTheme={THEME.SECONDARY}
                            />
                          </div>
                        }
                        tooltipContent={
                          <div>
                            <p>{LOCALIZE.commons.deleteButton}</p>
                          </div>
                        }
                      />
                    </>
                  ),
                  body: <ScreenReaderTab />
                });
                break;
              default:
                // do nothing
                break;
            }
          }
        }
        return conditionalContentTabs;
      case SECTIONS.other:
        const conditionalOtherTabs = [
          {
            key: `${SECTIONS.other}-1`,
            tabName:
              LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items.topTabs
                .otherTabs.categories,
            body: <CategoriesTab />
          },
          {
            key: `${SECTIONS.other}-2`,
            tabName:
              LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items.topTabs
                .otherTabs.comments,
            body: <CommentsTab />
          }
        ];
        // bilingual item bank
        if (this.props.item_bank_data.item_bank_definition[0].language_id === null) {
          conditionalOtherTabs.push({
            key: `${SECTIONS.other}-3`,
            tabName:
              LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items.topTabs
                .otherTabs.translations,
            body: <TranslationsTab />
          });
        }

        return conditionalOtherTabs;
      default:
        return null;
    }
  };

  openPreviewQuestionPopup = () => {
    this.setState({ showPreviewQuestionPopup: true });
  };

  closePreviewQuestionPopup = () => {
    this.setState({ showPreviewQuestionPopup: false });
  };

  getPreviousButtonDisableState = () => {
    // initializing disabled
    let disabled = false;
    // no more than one question
    if (this.props.item_bank_data.item_bank_items.length <= 1) {
      disabled = true;
    }

    // getting index of current question
    const indexOfCurrentQuestion = this.props.item_bank_data.item_bank_items.findIndex(obj => {
      return obj.system_id === this.props.itemData.system_id;
    });
    // first element of the array
    if (indexOfCurrentQuestion === 0) {
      disabled = true;
    }

    return disabled;
  };

  getNextButtonDisableState = () => {
    // initializing disabled
    let disabled = false;
    // no more than one question
    if (this.props.item_bank_data.item_bank_items.length <= 1) {
      disabled = true;
    }

    // getting index of current question
    const indexOfCurrentQuestion = this.props.item_bank_data.item_bank_items.findIndex(obj => {
      return obj.system_id === this.props.itemData.system_id;
    });
    // last element of the array
    if (indexOfCurrentQuestion === this.props.item_bank_data.item_bank_items.length - 1) {
      disabled = true;
    }

    return disabled;
  };

  handlePreviousAction = () => {
    // getting index of current question
    const indexOfCurrentQuestion = this.props.item_bank_data.item_bank_items.findIndex(obj => {
      return obj.system_id === this.props.itemData.system_id;
    });
    // updating redux states
    this.props.setItemData(
      this.props.item_bank_data.item_bank_items[indexOfCurrentQuestion - 1],
      this.props.languageData
    );
    this.props.setItemInitialData(
      this.props.item_bank_data.item_bank_items[indexOfCurrentQuestion - 1],
      this.props.languageData
    );
    // closing popup
    this.closeGoPreviousConfirmationPopup();
  };

  handleNextAction = () => {
    // getting index of current question
    const indexOfCurrentQuestion = this.props.item_bank_data.item_bank_items.findIndex(obj => {
      return obj.system_id === this.props.itemData.system_id;
    });
    // updating redux states
    this.props.setItemData(
      this.props.item_bank_data.item_bank_items[indexOfCurrentQuestion + 1],
      this.props.languageData
    );
    this.props.setItemInitialData(
      this.props.item_bank_data.item_bank_items[indexOfCurrentQuestion + 1],
      this.props.languageData
    );
    // closing popup
    this.closeGoNextConfirmationPopup();
  };

  handleSaveDraft = () => {
    // validating scores
    this.validateScores();

    // adding timeout in order to make sure that the redux props have been properly updated
    setTimeout(() => {
      const data = {
        section_to_update: ITEM_ACTION_TYPE.saveDraft,
        data: this.props.itemData
      };
      // getting and setting needed redux states
      this.props.updateItemData(data).then(response => {
        if (response.ok) {
          this.props.setItemData(response, this.props.languageData);
          this.props.setItemInitialData(response, this.props.languageData);
          this.props.setDataChangesDetected(false);
          this.props.setDataReadyForUpload(true);

          // getting index of current item
          const indexOfCurrentItem = this.props.item_bank_data.item_bank_items.findIndex(obj => {
            return obj.system_id === this.props.itemData.system_id;
          });
          const copyOfItemBankData = this.props.item_bank_data;
          copyOfItemBankData.item_bank_items[indexOfCurrentItem] = response;

          this.props.setItemBankData(copyOfItemBankData);

          // Set the value for the initial data of the Item Bank
          // This is used to detect changes on multiple components of Item Bank
          this.props.setItemBankInitialData(_.cloneDeep(copyOfItemBankData));
          // should never happen
        } else {
          throw new Error("An error occurred during the save draft process");
        }
      });
    }, 100);
  };

  validateScores = () => {
    // making sure that no item option has a score of "-"
    // bilingual item bank
    if (this.props.item_bank_data.item_bank_definition[0].language_id === null) {
      // creating deep copy of item_options
      const copyOfItemOptions = _.cloneDeep(this.props.itemData.item_options);
      // looping in languageData
      for (let i = 0; i < this.props.languageData.length; i++) {
        // copyOfItemOptions in language of current iteration is defined
        if (typeof copyOfItemOptions[this.props.languageData[i].ISO_Code_1] !== "undefined") {
          for (
            let j = 0;
            j < copyOfItemOptions[this.props.languageData[i].ISO_Code_1].length;
            j++
          ) {
            // score finishing with "."
            if (
              copyOfItemOptions[this.props.languageData[i].ISO_Code_1][j].score.charAt(
                copyOfItemOptions[this.props.languageData[i].ISO_Code_1][j].score.length - 1
              ) === "."
            ) {
              copyOfItemOptions[this.props.languageData[i].ISO_Code_1][j].score = copyOfItemOptions[
                this.props.languageData[i].ISO_Code_1
              ][j].score.slice(0, -1);
            }
            // score is "-"
            if (copyOfItemOptions[this.props.languageData[i].ISO_Code_1][j].score === "-") {
              copyOfItemOptions[this.props.languageData[i].ISO_Code_1][j].score = "0";
            }
            // score is ""
            if (copyOfItemOptions[this.props.languageData[i].ISO_Code_1][j].score === "") {
              copyOfItemOptions[this.props.languageData[i].ISO_Code_1][j].score = "0";
            }
            // converting score to float
            copyOfItemOptions[this.props.languageData[i].ISO_Code_1][j].score = parseFloat(
              copyOfItemOptions[this.props.languageData[i].ISO_Code_1][j].score
            );
          }
        }
      }
      // updating redux states
      // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
      const new_obj = {
        ...this.props.itemData,
        item_options: copyOfItemOptions
      };

      // setting new itemData redux state
      this.props.setItemData(new_obj, this.props.languageData);

      // unilangual item bank
    } else {
      // getting language code
      let languageCode = null;
      for (let i = 0; i < this.props.languageData.length; i++) {
        if (
          this.props.languageData[i].language_id ===
          this.props.item_bank_data.item_bank_definition[0].language_id
        ) {
          languageCode = this.props.languageData[i].ISO_Code_1;
          break;
        }
      }
      // creating deep copy of item_options
      const copyOfItemOptions = _.cloneDeep(this.props.itemData.item_options);
      // copyOfItemOptions in language of item bank is defined
      if (typeof copyOfItemOptions[languageCode] !== "undefined") {
        for (let i = 0; i < copyOfItemOptions[languageCode].length; i++) {
          // score finishing with "."
          if (
            copyOfItemOptions[languageCode][i].score.charAt(
              copyOfItemOptions[languageCode][i].score.length - 1
            ) === "."
          ) {
            copyOfItemOptions[languageCode][i].score = copyOfItemOptions[languageCode][
              i
            ].score.slice(0, -1);
          }
          // score is "-"
          if (copyOfItemOptions[languageCode][i].score === "-") {
            copyOfItemOptions[languageCode][i].score = "0";
          }
          // score is ""
          if (copyOfItemOptions[languageCode][i].score === "") {
            copyOfItemOptions[languageCode][i].score = "0";
          }
          // converting score to float
          copyOfItemOptions[languageCode][i].score = parseFloat(
            copyOfItemOptions[languageCode][i].score
          );
        }
      }

      // updating redux states
      // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
      const new_obj = {
        ...this.props.itemData,
        item_options: copyOfItemOptions
      };

      // setting new itemData redux state
      this.props.setItemData(new_obj, this.props.languageData);
    }
  };

  handleUpload = () => {
    // getting section_to_update parameter
    let section_to_update = null;
    if (
      this.props.uploadItemDataParameters.versionControlParameter ===
      VERSION_CONTROL_RADIO_BUTTON.uploadNewVersion
    ) {
      section_to_update = ITEM_ACTION_TYPE.upload;
    } else if (
      this.props.uploadItemDataParameters.versionControlParameter ===
      VERSION_CONTROL_RADIO_BUTTON.overwriteVersion
    ) {
      section_to_update = ITEM_ACTION_TYPE.uploadOverwrite;
    }

    const data = {
      section_to_update: section_to_update,
      data: this.props.itemData
    };
    // getting and setting needed redux states
    this.props.updateItemData(data).then(response => {
      if (response.ok) {
        this.props.setItemData(response, this.props.languageData);
        this.props.setItemInitialData(response, this.props.languageData);
        this.props.setDataChangesDetected(false);
        this.props.setDataReadyForUpload(false);

        // getting index of current item
        const indexOfCurrentItem = this.props.item_bank_data.item_bank_items.findIndex(obj => {
          return obj.system_id === this.props.itemData.system_id;
        });
        const copyOfItemBankData = this.props.item_bank_data;
        copyOfItemBankData.item_bank_items[indexOfCurrentItem] = response;

        this.props.setItemBankData(copyOfItemBankData);

        // Set the value for the initial data of the Item Bank
        // This is used to detect changes on multiple components of Item Bank
        this.props.setItemBankInitialData(_.cloneDeep(copyOfItemBankData));
        // should never happen
      } else {
        throw new Error("An error occurred during the upload item process");
      }
    });
  };

  render() {
    const generalTabs = this.getTABS(SECTIONS.general);
    const contentTabs = this.getTABS(SECTIONS.content);
    const otherTabs = this.getTABS(SECTIONS.other);

    // getting language code
    let languageCode = null;
    // unilangual item bank
    if (this.props.item_bank_data.item_bank_definition[0].language_id !== null) {
      for (let i = 0; i < this.props.languageData.length; i++) {
        if (
          this.props.languageData[i].language_id ===
          this.props.item_bank_data.item_bank_definition[0].language_id
        ) {
          languageCode = this.props.languageData[i].ISO_Code_1;
          break;
        }
      }
      // bilingual item bank
    } else {
      // getting current selected tab
      const currentSelectedTab = contentTabs.filter(obj => {
        return obj.key === this.props.secondColumnTab;
      })[0];

      // current selected tab is undefined or tab key is content-1 (regular content tab)
      if (typeof currentSelectedTab === "undefined" || currentSelectedTab.key === "content-1") {
        languageCode = this.props.selectedContentLanguage.languageCode;
        // alternative content
      } else {
        // getting alternative tab name
        const alternativeTabName = currentSelectedTab.tabName.props.children[0].props.children;

        switch (alternativeTabName) {
          // SCREEN READER
          case LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items.topTabs
            .contentTabs.screenReader:
            languageCode = this.props.selectedContentScreenReaderLanguage.languageCode;
            break;
          default:
            languageCode = this.props.selectedContentLanguage.languageCode;
            break;
        }
      }
    }

    return (
      <div>
        {this.state.isLoading ? (
          <div style={styles.loadingContainer}>
            <label className="fa fa-spinner fa-spin">
              <FontAwesomeIcon icon={faSpinner} style={styles.loading} />
            </label>
          </div>
        ) : (
          <div className="app" style={styles.appPadding}>
            <Helmet>
              <html lang={this.props.currentLanguage} />
              <title>{LOCALIZE.titles.itemEditor}</title>
            </Helmet>
            <ContentContainer customMaxWidth={maxWidthUtils.wide}>
              <div>
                <div style={styles.backButtonAndTitleContainer}>
                  <div style={styles.backButtonContainer}>
                    <CustomButton
                      buttonId="back-to-item-bank-editor-btn"
                      label={
                        <>
                          <FontAwesomeIcon icon={faArrowAltCircleLeft} />
                          <span style={styles.buttonLabel}>
                            {
                              LOCALIZE.testBuilder.testDefinition.selectTestDefinition
                                .itemBankEditor.items.backToItemBankEditorButton
                            }
                          </span>
                        </>
                      }
                      action={
                        this.props.dataChangesDetected
                          ? this.openBackToItemBankEditorPopup
                          : this.handleBackToItemBankEditor
                      }
                      buttonTheme={THEME.SECONDARY}
                    />
                  </div>
                  <div style={styles.titleContainer}>
                    <h2 style={styles.h2}>
                      {LOCALIZE.formatString(
                        LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                          .items.itemTitle,
                        this.props.item_bank_data.item_bank_definition[0].custom_item_bank_id,
                        this.state.itemBankLanguageData,
                        this.props.item_bank_data.item_bank_definition[0][
                          `${this.props.currentLanguage}_name`
                        ],
                        this.props.itemData.system_id
                      )}
                      {typeof this.props.itemData.version === "undefined" &&
                        LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                          .items.general.draftVersionLabel}
                    </h2>
                  </div>
                </div>
                <Row role="presentation">
                  <Col style={styles.colOneStyle}>
                    <TopTabs
                      TABS={generalTabs}
                      defaultTab={`${SECTIONS.general}-1`}
                      customStyle={styles.generalTab}
                      setTab={this.props.setFirstColumnTab}
                      customCurrentTabProp={this.props.firstColumnTab}
                    />
                  </Col>
                  <Col style={styles.colTwoStyle}>
                    <TopTabs
                      TABS={contentTabs}
                      defaultTab={`${SECTIONS.content}-1`}
                      customStyle={styles.contentTab}
                      setTab={this.props.setSecondColumnTab}
                      customCurrentTabProp={this.props.secondColumnTab}
                      containsAddTabButton={this.state.alternativeContentTypeOptions.length > 0}
                      addTabButtonAction={this.openAddAlternativeContentTabPopup}
                    />
                  </Col>
                  <Col style={styles.colThreeStyle}>
                    <TopTabs
                      TABS={otherTabs}
                      defaultTab={`${SECTIONS.other}-1`}
                      customStyle={styles.otherTab}
                      setTab={this.props.setThirdColumnTab}
                      customCurrentTabProp={this.props.thirdColumnTab}
                    />
                  </Col>
                </Row>
              </div>
              <ItemBankFooter
                previewQuestion={this.openPreviewQuestionPopup}
                previousAction={
                  this.props.dataChangesDetected
                    ? this.openGoPreviousConfirmationPopup
                    : this.handlePreviousAction
                }
                nextAction={
                  this.props.dataChangesDetected
                    ? this.openGoNextConfirmationPopup
                    : this.handleNextAction
                }
                saveAction={this.handleSaveDraft}
                triggerUploadPopup={true}
                uploadAction={this.handleUpload}
                disabledPreviousButton={this.getPreviousButtonDisableState()}
                disabledNextButton={this.getNextButtonDisableState()}
                disabledPreviousAndNextBtns={this.props.item_bank_data.item_bank_items.length <= 1}
              />
            </ContentContainer>
            <PopupBox
              show={this.state.showBackToItemBankEditorPopup}
              handleClose={this.closeBackToItemBankEditorPopup}
              title={
                LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                  .backToItemBankEditorPopup.title
              }
              description={
                <div>
                  <SystemMessage
                    messageType={MESSAGE_TYPE.warning}
                    title={LOCALIZE.commons.warning}
                    message={
                      <p>
                        {
                          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                            .items.backToItemBankEditorPopup.systemMessageDescription
                        }
                      </p>
                    }
                  />
                  <p>
                    {
                      LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                        .backToItemBankEditorPopup.description
                    }
                  </p>
                </div>
              }
              leftButtonType={BUTTON_TYPE.secondary}
              leftButtonTitle={LOCALIZE.commons.cancel}
              leftButtonIcon={faTimes}
              rightButtonType={BUTTON_TYPE.primary}
              rightButtonTitle={LOCALIZE.commons.confirm}
              rightButtonAction={this.handleBackToItemBankEditor}
              rightButtonIcon={faCheck}
              size="lg"
            />
            <PopupBox
              show={this.state.showGoNextItemConfirmationPopup}
              handleClose={this.closeGoNextConfirmationPopup}
              title={
                LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                  .changeItemConfirmationPopup.goNextTitle
              }
              description={
                <div>
                  <SystemMessage
                    messageType={MESSAGE_TYPE.warning}
                    title={LOCALIZE.commons.warning}
                    message={
                      <p>
                        {
                          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                            .items.changeItemConfirmationPopup.systemMessageDescription
                        }
                      </p>
                    }
                  />
                  <p>
                    {
                      LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                        .changeItemConfirmationPopup.description
                    }
                  </p>
                </div>
              }
              leftButtonType={BUTTON_TYPE.secondary}
              leftButtonTitle={LOCALIZE.commons.cancel}
              leftButtonIcon={faTimes}
              rightButtonType={BUTTON_TYPE.primary}
              rightButtonTitle={LOCALIZE.commons.confirm}
              rightButtonAction={this.handleNextAction}
              rightButtonIcon={faCheck}
              size="lg"
            />
            <PopupBox
              show={this.state.showGoPreviousItemConfirmationPopup}
              handleClose={this.closeGoPreviousConfirmationPopup}
              title={
                LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                  .changeItemConfirmationPopup.goPreviousTitle
              }
              description={
                <div>
                  <SystemMessage
                    messageType={MESSAGE_TYPE.warning}
                    title={LOCALIZE.commons.warning}
                    message={
                      <p>
                        {
                          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                            .items.changeItemConfirmationPopup.systemMessageDescription
                        }
                      </p>
                    }
                  />
                  <p>
                    {
                      LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                        .changeItemConfirmationPopup.description
                    }
                  </p>
                </div>
              }
              leftButtonType={BUTTON_TYPE.secondary}
              leftButtonTitle={LOCALIZE.commons.cancel}
              leftButtonIcon={faTimes}
              rightButtonType={BUTTON_TYPE.primary}
              rightButtonTitle={LOCALIZE.commons.confirm}
              rightButtonAction={this.handlePreviousAction}
              rightButtonIcon={faCheck}
              size="lg"
            />
            {this.state.showPreviewQuestionPopup && (
              <PopupBox
                show={this.state.showPreviewQuestionPopup}
                handleClose={this.closePreviewQuestionPopup}
                title={LOCALIZE.formatString(
                  LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                    .previewItemPopup.title,
                  this.props.itemData.system_id
                )}
                description={
                  <div>
                    <div>
                      <ItemComponent
                        itemContent={getItemContentToPreview(
                          this.props.itemData,
                          this.props.languageData,
                          contentTabs,
                          this.props.secondColumnTab
                        )}
                        index={this.props.item_bank_data.item_bank_items.findIndex(obj => {
                          return obj.item_id === this.props.itemData.item_id;
                        })}
                        languageCode={languageCode}
                        calledFromQuestionPreview={true}
                      />
                    </div>
                    <div>
                      <ItemOptions
                        itemOptions={getItemOptionsToPreview(
                          this.props.itemData,
                          this.props.languageData,
                          contentTabs,
                          this.props.secondColumnTab,
                          // true since this is called for the preview of an item
                          true
                        )}
                        languageCode={languageCode}
                        calledFromQuestionPreview={true}
                      />
                    </div>
                  </div>
                }
                rightButtonType={BUTTON_TYPE.primary}
                rightButtonTitle={LOCALIZE.commons.close}
                rightButtonAction={this.closePreviewQuestionPopup}
                rightButtonIcon={faTimes}
              />
            )}
            <PopupBox
              show={this.state.showAddContentTabPopup}
              handleClose={() => {}}
              size="lg"
              title={
                LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                  .content.addAlternativeContentPopup.title
              }
              description={
                <div>
                  <p>
                    {
                      LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                        .content.addAlternativeContentPopup.description
                    }
                  </p>
                  <Row
                    className="justify-content-between align-items-center"
                    style={styles.alternativeTypeContainer}
                  >
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                    >
                      <label id="alternative-content-type-label" htmlFor="alternative-content-type">
                        {
                          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                            .items.content.addAlternativeContentPopup.alternativeContentTypeLabel
                        }
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                    >
                      <DropdownSelect
                        idPrefix="alternative-content-type-react-select-dropdown"
                        onChange={this.onAlternativeContentTypeChange}
                        defaultValue={this.state.selectedAlternativeContentType}
                        options={this.state.alternativeContentTypeOptions}
                        ariaLabelledBy="alternative-content-type-label"
                        orderByLabels={true}
                        menuPlacement={"bottom"}
                        hasPlaceholder={true}
                      />
                    </Col>
                  </Row>
                </div>
              }
              leftButtonType={BUTTON_TYPE.secondary}
              leftButtonTitle={LOCALIZE.commons.cancel}
              leftButtonIcon={faTimes}
              leftButtonAction={this.closeAddAlternativeContentTabPopup}
              rightButtonType={BUTTON_TYPE.primary}
              rightButtonTitle={
                LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                  .content.addAlternativeContentPopup.rightButton
              }
              rightButtonAction={this.handleAddAlternativeContentTab}
              rightButtonIcon={faPlusCircle}
              rightButtonState={
                Object.entries(this.state.selectedAlternativeContentType).length > 0
                  ? BUTTON_STATE.enabled
                  : BUTTON_STATE.disabled
              }
            />
            <PopupBox
              show={this.state.showDeleteContentTabPopup}
              handleClose={() => {}}
              size="lg"
              title={
                LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                  .content.deleteAlternativeContentPopup.title
              }
              description={
                <div>
                  <SystemMessage
                    messageType={MESSAGE_TYPE.warning}
                    title={LOCALIZE.commons.warning}
                    message={
                      <p>
                        {
                          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                            .items.content.deleteAlternativeContentPopup.systemMessageDescription
                        }
                      </p>
                    }
                  />
                  <p>
                    {
                      LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                        .content.deleteAlternativeContentPopup.description
                    }
                  </p>
                </div>
              }
              leftButtonType={BUTTON_TYPE.secondary}
              leftButtonTitle={LOCALIZE.commons.cancel}
              leftButtonIcon={faTimes}
              leftButtonAction={this.closeDeleteAlternativeContentTabPopup}
              rightButtonType={BUTTON_TYPE.primary}
              rightButtonTitle={LOCALIZE.commons.deleteButton}
              rightButtonAction={this.handleDeleteAlternativeContentTab}
              rightButtonIcon={faTrashAlt}
            />
          </div>
        )}
      </div>
    );
  }
}

export { ItemEditor as unconnectedItemEditor };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    languageData: state.localize.languageData,
    accommodations: state.accommodations,
    item_bank_data: state.itemBank.item_bank_data,
    itemData: state.itemBank.itemData,
    itemValidationErrors: state.itemBank.itemValidationErrors,
    firstColumnTab: state.itemBank.firstColumnTab,
    secondColumnTab: state.itemBank.secondColumnTab,
    thirdColumnTab: state.itemBank.thirdColumnTab,
    dataChangesDetected: state.itemBank.dataChangesDetected,
    selectedContentLanguage: state.itemBank.selectedContentLanguage,
    selectedContentScreenReaderLanguage: state.itemBank.selectedContentScreenReaderLanguage,
    uploadItemDataParameters: state.itemBank.uploadItemDataParameters,
    conditionalContentTabsData: state.itemBank.conditionalContentTabsData
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setItemData,
      setItemInitialData,
      resetItemsValidationErrorStates,
      setFirstColumnTab,
      setSecondColumnTab,
      setThirdColumnTab,
      setConditionalContentTabsData,
      getItemData,
      updateItemData,
      setDataChangesDetected,
      setDataReadyForUpload,
      setItemBankData,
      setItemBankInitialData,
      triggerAppRerender
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ItemEditor);
