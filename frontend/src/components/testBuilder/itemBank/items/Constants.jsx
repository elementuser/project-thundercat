// reference: items_utils.py (../backend/cms/static/items_utils.py)
const ITEM_RESPONSE_FORMAT = {
  multipleChoice: "is_mc"
};

export const ITEM_DEVELOPMENT_STATUS = {
  pilot: "is_pilot",
  regular: "is_regular",
  retired: "is_retired",
  draft: "is_draft"
};

export const ITEM_CONTENT_TYPE = {
  markdown: 1,
  screenReader: 2
};

export const ITEM_OPTION_TYPE = {
  multipleChoice: 1,
  multipleChoiceScreenReader: 2
};

export const ATTRIBUTE_VALUE_TYPE = {
  text: 1
};

export default ITEM_RESPONSE_FORMAT;
