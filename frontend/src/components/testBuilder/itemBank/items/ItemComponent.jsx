/* eslint-disable react/no-children-prop */
import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import ReactMarkdown from "react-markdown";
import rehypeRaw from "rehype-raw";
import LOCALIZE from "../../../../text_resources";
import { ITEM_CONTENT_TYPE } from "./Constants";

const styles = {
  stemPadding: {
    padding: "0 16px"
  }
};

class ItemComponent extends Component {
  constructor(props, context) {
    super(props, context);

    this.PropTypes = {
      itemContent: PropTypes.object.isRequired,
      index: PropTypes.number.isRequired,
      calledFromQuestionPreview: PropTypes.bool
    };
    ItemComponent.defaultProps = {
      calledFromQuestionPreview: false
    };
  }

  factory = (itemContent, languageCode) => {
    return (
      <div id="unit-test-item-content">
        {typeof itemContent[languageCode] !== "undefined"
          ? itemContent[languageCode].map((data, index) => {
              switch (data.content_type) {
                case ITEM_CONTENT_TYPE.markdown:
                  // calledFromQuestionPreview is defined and set to true
                  if (this.props.calledFromQuestionPreview) {
                    return (
                      <div key={`${data.id}-${index}`} id="unit-test-item-content-markdown">
                        <ReactMarkdown
                          rehypePlugins={[rehypeRaw]}
                          children={data.text}
                          // add className="notranslate" to 'p' tags to prevent Microsoft Edge from translating
                          components={{
                            p: ({ node, ...props }) => <p className="notranslate" {...props} />
                          }}
                        />
                      </div>
                    );
                  }

                  // called from sample or real test (need the question header)
                  return (
                    <div>
                      <div
                        key={`${data.id}-${index}`}
                        id="unit-test-item-content-markdown"
                        style={styles.stemPadding}
                      >
                        <ReactMarkdown
                          rehypePlugins={[rehypeRaw]}
                          children={data.text}
                          // add className="notranslate" to 'p' tags to prevent Microsoft Edge from translating
                          components={{
                            p: ({ node, ...props }) => <p className="notranslate" {...props} />
                          }}
                        />
                      </div>
                    </div>
                  );

                case ITEM_CONTENT_TYPE.screenReader:
                  // calledFromQuestionPreview is defined and set to true
                  if (this.props.calledFromQuestionPreview) {
                    return (
                      <div key={`${data.id}-${index}`} id="unit-test-item-content-markdown">
                        <ReactMarkdown
                          rehypePlugins={[rehypeRaw]}
                          children={data.text}
                          // add className="notranslate" to 'p' tags to prevent Microsoft Edge from translating
                          components={{
                            p: ({ node, ...props }) => <p className="notranslate" {...props} />
                          }}
                        />
                      </div>
                    );
                  }
                  // TODO
                  return null;

                default:
                  return (
                    <div key={index} id="unit-test-item-content-undefined-section">
                      <h1>Unsupported Item Content Type: {data.content_type}</h1>
                    </div>
                  );
              }
            })
          : []}
      </div>
    );
  };

  render() {
    const { itemContent, languageCode } = this.props;
    return <>{this.factory(itemContent, languageCode)}</>;
  }
}

export { ItemComponent as UnconnectedItemComponent };
const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language
  };
};

export default connect(mapStateToProps, null)(ItemComponent);
