/* eslint-disable jsx-a11y/label-has-associated-control */
import {
  faBinoculars,
  faCaretLeft,
  faCaretRight,
  faPen,
  faPlusCircle,
  faSpinner,
  faTimes
} from "@fortawesome/free-solid-svg-icons";
import * as _ from "lodash";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { Component } from "react";
import ReactPaginate from "react-paginate";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import LOCALIZE from "../../../../text_resources";
import CustomButton, { THEME } from "../../../commons/CustomButton";
import GenericTable, { COMMON_STYLE } from "../../../commons/GenericTable";
import PopupBox, { BUTTON_STATE, BUTTON_TYPE } from "../../../commons/PopupBox";
import SearchBarWithDisplayOptions from "../../../commons/SearchBarWithDisplayOptions";
import {
  getAllItems,
  getFoundItems,
  updateSearchItemsStates,
  updateItemsPageState,
  updateItemsPageSizeState,
  getResponseFormats,
  createNewItem,
  setItemData,
  setItemInitialData,
  resetTopTabsStates,
  setItemBankData,
  setItemBankInitialData,
  setDataChangesDetected,
  getAndSetSelectedItem,
  setItemsTableRendering
} from "../../../../modules/ItemBankRedux";
import StyledTooltip, { EFFECT, TRIGGER_TYPE, TYPE } from "../../../authentication/StyledTooltip";
import { Col, Row } from "react-bootstrap";
import DropdownSelect from "../../../commons/DropdownSelect";
import { LANGUAGES } from "../../../commons/Translation";
import { getLineSpacingCSS } from "../../../../modules/AccommodationsRedux";
import { history } from "../../../../store-index";
import { PATH } from "../../../commons/Constants";
import { getItemBankHeightCalculations } from "../../../../helpers/inTestHeightCalculations";
import ItemComponent from "./ItemComponent";
import ItemOptions from "./ItemOptions";
import { getLanguageData, setLanguageData } from "../../../../modules/LocalizeRedux";
import getItemContentToPreview, {
  getItemOptionsToPreview
} from "../../../../helpers/itemBankItemPreview";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 5,
    xl: 5
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 7,
    xl: 7
  }
};

const styles = {
  mainContainer: {
    width: "100%",
    padding: "24px 12px",
    border: "1px solid #cdcdcd",
    borderTop: "none"
  },
  createButton: {
    margin: "0 0 12px 0"
  },
  buttonIcon: {
    marginRight: 6
  },
  tableContainer: {
    margin: "12px 0 0 0"
  },
  paginationContainer: {
    display: "flex",
    position: "relative"
  },
  paginationIcon: {
    padding: "0 6px"
  },
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  allUnset: {
    all: "unset"
  },
  itemContainer: {
    width: "100%",
    margin: "12px 0"
  },
  labelContainer: {
    verticalAlign: "middle"
  },
  label: {
    paddingRight: 12,
    margin: 0
  },
  inputContainer: {
    verticalAlign: "middle"
  },
  input: {
    width: "100%",
    minHeight: 38,
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565e",
    borderRadius: 4
  },
  customLoadingContainer: {
    display: "grid",
    gridTemplateColumns: "1fr"
  },
  loadingOverlappingStyle: {
    gridRowStart: "1",
    gridColumnStart: "1"
  },
  transparentText: {
    color: "transparent"
  },
  icon: {
    paddingRight: 12,
    verticalAlign: "middle"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    margin: "4px 0 0 4px"
  },
  overflow: {
    overflowY: "auto",
    borderTop: "1px solid #00565e",
    borderBottom: "1px solid #00565e",
    borderRight: "1px solid #00565e",
    borderRadius: "4px"
  },
  noBorder: {
    borderTop: "none",
    borderBottom: "none",
    borderRight: "1px solid #00565e"
  }
};

class Items extends Component {
  constructor(props) {
    super(props);
    this.paginationDivRef = React.createRef();
  }

  state = {
    showCreateNewItemPopup: false,
    rowsDefinition: {},
    resultsFound: 0,
    numberOfItemsPages: 1,
    newItemResponseFormatOptions: [],
    newItemResponseFormatSelectedOption: [],
    isValidNewItemForm: false,
    newItemHistoricalId: "",
    itemHistoricalIdAlreadyUsed: false,
    currentlyCreatingNewItem: false,
    triggerRerender: false,
    showViewItemPopup: false,
    currentSelectedItemData: {},
    itemBankLanguageData: {}
  };

  componentDidMount = () => {
    this.props.updateItemsPageState(1);
    this.populateItemResponseFormatOptions();
    this.getAllItemsBasedOnActiveSearch();
    this.getItemBankLangageData();
  };

  componentDidUpdate = prevProps => {
    // if itemBankInitialData gets updated
    // This comparison is to identify when new custom variables are created
    // updating the items (based on the new custom variables)
    // _.isEqual is required to compare these objects
    if (
      !_.isEqual(
        prevProps.itemBankInitialData.item_bank_attributes,
        this.props.itemBankInitialData.item_bank_attributes
      )
    ) {
      // perform your action
      this.getAllItemsBasedOnActiveSearch();
    }
    // if currentLanguage gets updated
    if (prevProps.currentLanguage !== this.props.currentLanguage) {
      this.getAllItemsBasedOnActiveSearch();
    }
    // if itemsPaginationPageSize get updated
    if (prevProps.itemsPaginationPageSize !== this.props.itemsPaginationPageSize) {
      this.getAllItemsBasedOnActiveSearch();
    }
    // if itemsPaginationPage get updated
    if (prevProps.itemsPaginationPage !== this.props.itemsPaginationPage) {
      this.getAllItemsBasedOnActiveSearch();
    }
    // if search itemsKeyword gets updated
    if (prevProps.itemsKeyword !== this.props.itemsKeyword) {
      // if itemsKeyword redux state is empty
      if (this.props.itemsKeyword !== "") {
        this.getAllItemsBasedOnActiveSearch();
      } else if (this.props.itemsKeyword === "" && this.props.itemsActiveSearch) {
        this.getAllItemsBasedOnActiveSearch();
      }
    }
    // if itemsActiveSearch gets updated
    if (prevProps.itemsActiveSearch !== this.props.itemsActiveSearch) {
      // there is no current active search (on clear search action)
      if (!this.props.itemsActiveSearch) {
        this.getAllItems();
        // there is a current active search (on search action)
      } else {
        this.getFoundItems();
      }
    }
    // if currentTab gets updated
    if (prevProps.currentTab !== this.props.currentTab) {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }
  };

  getItemBankLangageData = () => {
    // initializing itemBankLanguageData (defaulting to English)
    let itemBankLanguageData = { ISO_Code_1: this.props.currentLanguage };
    this.props.getLanguageData().then(response => {
      // setting redux states
      this.props.setLanguageData(response);
      for (let i = 0; i < response.length; i++) {
        if (
          response[i].language_id === this.props.item_bank_data.item_bank_definition[0].language_id
        ) {
          itemBankLanguageData = response[i];
          break;
        }
      }
      this.setState({ itemBankLanguageData: itemBankLanguageData });
    });
  };

  // get item banks data based on the itemsActiveSearch redux state
  getAllItemsBasedOnActiveSearch = () => {
    // current search
    if (this.props.itemsActiveSearch) {
      this.getFoundItems();
      // no current search
    } else {
      this.getAllItems();
    }
  };

  populateItemResponseFormatOptions = () => {
    const responseFormatOptions = [];
    this.props.getResponseFormats().then(response => {
      for (let i = 0; i < response.length; i++) {
        // Interface is in English
        if (this.props.currentLanguage === LANGUAGES.english) {
          responseFormatOptions.push({
            label: `${response[i].en_name}`,
            value: `${response[i].codename}`
          });
          // Interface is in French
        } else {
          responseFormatOptions.push({
            label: `${response[i].fr_name}`,
            value: `${response[i].codename}`
          });
        }
      }
      this.setState({ newItemResponseFormatOptions: responseFormatOptions });
    });
  };

  openCreateNewItemPopup = () => {
    this.setState({ showCreateNewItemPopup: true });
  };

  closeCreateNewItemPopup = () => {
    this.setState({
      showCreateNewItemPopup: false,
      isValidNewItemForm: false,
      newItemResponseFormatSelectedOption: [],
      newItemHistoricalId: "",
      itemHistoricalIdAlreadyUsed: false
    });
  };

  handleCreateNewItem = () => {
    this.setState({ currentlyCreatingNewItem: true }, () => {
      const body = {
        item_bank_id: this.props.item_bank_definition.id,
        response_format: this.state.newItemResponseFormatSelectedOption.value,
        historical_id: this.state.newItemHistoricalId
      };
      this.props.createNewItem(body).then(response => {
        if (response.ok) {
          this.setState(
            {
              newItemResponseFormatSelectedOption: [],
              isValidNewItemForm: false,
              newItemHistoricalId: "",
              itemHistoricalIdAlreadyUsed: false,
              showCreateNewItemPopup: false
            },
            () => {
              this.getAllItemsBasedOnActiveSearch();
              setTimeout(() => {
                this.setState({ currentlyCreatingNewItem: false });
                this.props.setItemData(response, this.props.languageData);
                this.props.setItemInitialData(response, this.props.languageData);
                history.push(PATH.itemEditor);
              }, 250);
            }
          );
          // if there is a validation error
        } else if (response.status === 409) {
          this.setState({ itemHistoricalIdAlreadyUsed: true, currentlyCreatingNewItem: false });
          // should never happen
        } else {
          throw new Error("An error occurred during the create new item process");
        }
      });
    });
  };

  getAllItems = () => {
    // update redux state that controls the items' loading
    this.props.setItemsTableRendering(true);

    // initializing needed object and array for rowsDefinition (needed for GenericTable component)
    let rowsDefinition = {};
    const data = [];
    this.props
      .getAllItems(
        this.props.item_bank_definition.id,
        this.props.itemsPaginationPage,
        this.props.itemsPaginationPageSize
      )
      .then(response => {
        // updating redux states
        // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
        const new_obj = {
          ..._.cloneDeep(this.props.item_bank_data),
          item_bank_items: response.all_items,
          item_bank_number_of_found_items: response.all_items.length
        };

        this.props.setItemBankData(_.cloneDeep(new_obj));

        const new_initial_obj = {
          ..._.cloneDeep(this.props.itemBankInitialData),
          item_bank_items: response.all_items,
          item_bank_number_of_found_items:
            response[0] !== "no results found" ? response.results.length : 0
        };

        // Set the value for the initial data of the Item Bank
        // This is used to detect changes on multiple components of Item Bank
        this.props.setItemBankInitialData(_.cloneDeep(new_initial_obj));

        const NumberOfVariables = this.props.itemBankInitialData.item_bank_attributes.length;
        const columns = [];

        // Creating array to hold column names
        for (let j = 0; j < NumberOfVariables; j++) {
          columns[j] = `column_${j + 5}`;
        }

        for (let i = 0; i < response.results.length; i++) {
          // populating data object with provided data
          data.push({
            column_1: response.results[i].system_id,
            column_2: response.results[i].historical_id,
            column_3: response.results[i][`development_status_name_${this.props.currentLanguage}`],
            column_4: response.results[i].version
          });

          // Loop for number of variables to show
          for (let j = 0; j < NumberOfVariables; j++) {
            if (this.props.itemBankInitialData.item_bank_items[i]) {
              // Set to empty string if there are no values
              let tempVal =
                this.props.itemBankInitialData.item_bank_items[i].item_attribute_values[j].text;
              if (tempVal === "N/A") {
                tempVal = "";
              }

              data[i] = { ...data[i], [columns[j]]: tempVal };
            }
          }

          const actionsColumn = `column_${NumberOfVariables + 5}`;
          data[i] = { ...data[i], [actionsColumn]: this.populateColumnTwo(i, response) };

          // updating rowsDefinition object with provided data and needed style
          rowsDefinition = {
            column_1_style: COMMON_STYLE.LEFT_TEXT,
            column_2_style: COMMON_STYLE.CENTERED_TEXT,
            column_3_style: COMMON_STYLE.CENTERED_TEXT,
            column_4_style: COMMON_STYLE.CENTERED_TEXT
          };

          for (let j = 5; j < NumberOfVariables + 5; j++) {
            const tempCol = `column_${j}_style`;

            rowsDefinition = { ...rowsDefinition, [tempCol]: COMMON_STYLE.CENTERED_TEXT };
          }

          rowsDefinition = { ...rowsDefinition, data: data };

          // update redux state that controls the items' loading
          this.props.setItemsTableRendering(false);

          // saving results in state
          this.setState({
            rowsDefinition: rowsDefinition,
            // currentlyLoading: false,
            numberOfItemsPages: Math.ceil(
              parseInt(response.count) / this.props.itemsPaginationPageSize
            )
          });
        }
      });
  };

  getFoundItems = () => {
    // update redux state that controls the items' loading
    this.props.setItemsTableRendering(true);

    // initializing needed object and array for rowsDefinition (needed for GenericTable component)
    let rowsDefinition = {};
    const data = [];
    setTimeout(() => {
      this.props
        .getFoundItems(
          this.props.item_bank_definition.id,
          this.props.currentLanguage,
          this.props.itemsKeyword,
          this.props.itemsPaginationPage,
          this.props.itemsPaginationPageSize
        )
        .then(response => {
          // updating redux states
          // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
          const new_obj = {
            ..._.cloneDeep(this.props.item_bank_data),
            item_bank_items: response.all_items,
            item_bank_number_of_found_items:
              response[0] !== "no results found" ? response.results.length : 0
          };

          this.props.setItemBankData(_.cloneDeep(new_obj));

          const new_initial_obj = {
            ..._.cloneDeep(this.props.itemBankInitialData),
            item_bank_items: response.all_items,
            item_bank_number_of_found_items:
              response[0] !== "no results found" ? response.results.length : 0
          };

          // Set the value for the initial data of the Item Bank
          // This is used to detect changes on multiple components of Item Bank
          this.props.setItemBankInitialData(_.cloneDeep(new_initial_obj));

          const NumberOfVariables = this.props.itemBankInitialData.item_bank_attributes.length;

          // there are no results found
          if (response[0] === "no results found") {
            this.setState(
              {
                rowsDefinition: {},
                numberOfItemsPages: 1,
                resultsFound: 0
              },
              () => {
                this.setState({ currentlyLoading: false }, () => {
                  // make sure that this element exsits to avoid any error
                  if (document.getElementById("item-bank-items-results-found")) {
                    document.getElementById("item-bank-items-results-found").focus();
                  }
                });
              }
            );
            // there is at least one result found
          } else {
            // making sure that the results object is defined (avoiding page break)
            if (typeof response.results !== "undefined") {
              const columns = [];

              // Creating array to hold column names
              for (let j = 0; j < NumberOfVariables; j++) {
                columns[j] = `column_${j + 5}`;
              }

              for (let i = 0; i < response.results.length; i++) {
                // populating data object with provided data
                data.push({
                  column_1: response.results[i].system_id,
                  column_2: response.results[i].historical_id,
                  column_3:
                    response.results[i][`development_status_name_${this.props.currentLanguage}`],
                  column_4: response.results[i].version
                });

                // Loop for number of variables to show
                for (let j = 0; j < NumberOfVariables; j++) {
                  // Set to empty string if there are no values
                  let tempVal =
                    this.props.itemBankInitialData.item_bank_items[i].item_attribute_values[j].text;
                  if (tempVal === "N/A") {
                    tempVal = "";
                  }
                  data[i] = { ...data[i], [columns[j]]: tempVal };
                }

                const actionsColumn = `column_${NumberOfVariables + 5}`;
                data[i] = { ...data[i], [actionsColumn]: this.populateColumnTwo(i, response) };
              }
            }

            // updating rowsDefinition object with provided data and needed style
            rowsDefinition = {
              column_1_style: COMMON_STYLE.LEFT_TEXT,
              column_2_style: COMMON_STYLE.CENTERED_TEXT,
              column_3_style: COMMON_STYLE.CENTERED_TEXT,
              column_4_style: COMMON_STYLE.CENTERED_TEXT
            };

            for (let j = 5; j < NumberOfVariables + 5; j++) {
              const tempCol = `column_${j}_style`;
              rowsDefinition = { ...rowsDefinition, [tempCol]: COMMON_STYLE.CENTERED_TEXT };
            }

            rowsDefinition = { ...rowsDefinition, data: data };

            // update redux state that controls the items' loading
            this.props.setItemsTableRendering(false);

            // saving results in state
            this.setState(
              {
                rowsDefinition: rowsDefinition,
                // currentlyLoading: false,
                numberOfItemsPages: Math.ceil(
                  parseInt(response.count) / this.props.itemsPaginationPageSize
                ),
                resultsFound: response.count
              },
              () => {
                // make sure that this element exsits to avoid any error
                if (document.getElementById("item-bank-items-search-results-found")) {
                  document.getElementById("item-bank-items-search-results-found").focus();
                }
              }
            );
          }
        });
    }, 100);
  };

  populateColumnTwo = (i, response) => {
    return (
      <div className="d-flex justify-content-center flex-nowrap">
        <StyledTooltip
          id={`item-bank-item-view-button-${i}`}
          place="top"
          type={TYPE.light}
          effect={EFFECT.solid}
          triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
          tooltipElement={
            <div style={styles.allUnset}>
              <CustomButton
                dataTip=""
                dataFor={`item-bank-item-view-button-${i}`}
                label={<FontAwesomeIcon icon={faBinoculars} />}
                ariaLabel={LOCALIZE.formatString(
                  LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                    .table.viewActionAccessibilityLabel,
                  response.results[i].system_id
                )}
                action={() => {
                  this.viewSelectedItem(response.results[i]);
                }}
                customStyle={styles.actionButton}
                buttonTheme={THEME.SECONDARY}
              />
            </div>
          }
          tooltipContent={
            <div>
              <p>
                {
                  LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                    .table.viewAction
                }
              </p>
            </div>
          }
        />
        <StyledTooltip
          id={`item-bank-item-edit-button-${i}`}
          place="top"
          type={TYPE.light}
          effect={EFFECT.solid}
          triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
          tooltipElement={
            <div style={styles.allUnset}>
              <CustomButton
                dataTip=""
                dataFor={`item-bank-item-edit-button-${i}`}
                label={<FontAwesomeIcon icon={faPen} />}
                ariaLabel={LOCALIZE.formatString(
                  LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                    .table.editActionAccessibilityLabel,
                  response.results[i].system_id
                )}
                action={() => {
                  this.editSelectedItem(response.results[i]);
                }}
                customStyle={styles.actionButton}
                buttonTheme={THEME.SECONDARY}
              />
            </div>
          }
          tooltipContent={
            <div>
              <p>
                {
                  LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                    .table.editAction
                }
              </p>
            </div>
          }
        />
      </div>
    );
  };

  viewSelectedItem = currentSelectedItemData => {
    this.setState({ showViewItemPopup: true, currentSelectedItemData: currentSelectedItemData });
  };

  closeViewItemPopup = () => {
    this.setState({ showViewItemPopup: false, currentSelectedItemData: {} });
  };

  editSelectedItem = currentSelectedItem => {
    this.props.resetTopTabsStates();
    this.props.getAndSetSelectedItem(currentSelectedItem);
    this.props.setDataChangesDetected(false);

    setTimeout(() => {
      this.props.setItemInitialData(this.props.itemData, this.props.languageData);
      history.push(PATH.itemEditor);
    }, 100);
  };

  handlePageChange = id => {
    // "+1" because redux itemsPaginationPage uses index 1 and react-paginate (id) uses index 0
    const selectedPage = id.selected + 1;
    // update current page redux state
    this.props.updateItemsPageState(selectedPage);
    // focusing on the first table's row (make sure that the row exists before trying to focus to avoid errors)
    if (document.getElementById("item-bank-items-table-row-0")) {
      document.getElementById("item-bank-items-table-row-0").focus();
    }
  };

  getSelectedNewItemResponseFormatOption = option => {
    this.setState(
      {
        newItemResponseFormatSelectedOption: option
      },
      () => {
        this.validateNewItemForm();
      }
    );
  };

  updateNewItemHistoricalIdContent = event => {
    const newItemHistoricalId = event.target.value;
    // allow only alphanumeric, slash, underscore and dash (0 to 50 chars)
    const regexExpression = /^([a-zA-z0-9-/_]{0,50})$/;
    if (regexExpression.test(newItemHistoricalId)) {
      this.setState(
        {
          newItemHistoricalId: newItemHistoricalId.toUpperCase(),
          itemHistoricalIdAlreadyUsed: false
        },
        () => {
          this.validateNewItemForm();
        }
      );
    }
  };

  validateNewItemForm = () => {
    // initializing needed variables
    let isValidForm = false;
    const isValidResponseFormat =
      typeof this.state.newItemResponseFormatSelectedOption.value !== "undefined";

    // everything is valid
    if (isValidResponseFormat) {
      isValidForm = true;
    }

    this.setState({ isValidNewItemForm: isValidForm });
  };

  render() {
    const NumberOfVariables = this.props.itemBankInitialData.item_bank_attributes.length;
    const columnsDefinition = [
      {
        label:
          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items.table
            .itemId,
        style: { ...COMMON_STYLE.LEFT_TEXT }
      },
      {
        label:
          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items.table
            .rndItemId,
        style: { ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label:
          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items.table
            .itemType,
        style: { ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label:
          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items.table
            .version,
        style: { ...COMMON_STYLE.CENTERED_TEXT }
      }
    ];

    // Adding labels for variables dynamically
    for (let j = 0; j < NumberOfVariables; j++) {
      // Need to do this because the text doesnt exist in LOCALIZE as they are variable
      if (this.props.currentLanguage === LANGUAGES.english) {
        columnsDefinition[j + 4] = {
          label:
            this.props.itemBankInitialData.item_bank_attributes[j].item_bank_attribute_text.en[0]
              .text,
          style: { ...{ width: "10%" }, ...COMMON_STYLE.CENTERED_TEXT }
        };
      } else {
        columnsDefinition[j + 4] = {
          label:
            this.props.itemBankInitialData.item_bank_attributes[j].item_bank_attribute_text.fr[0]
              .text,
          style: { ...{ width: "10%" }, ...COMMON_STYLE.CENTERED_TEXT }
        };
      }
    }

    // adding actions column
    columnsDefinition[NumberOfVariables + 4] = {
      label:
        LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items.table.actions,
      style: { ...{ width: "10%" }, ...COMMON_STYLE.CENTERED_TEXT }
    };

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    // calculating the height of the top tabs and above (without considering the nav bar)
    // =========================================================================================
    let heightOfBackToItemBankSelectionButton = 0;
    let heightOfTopTabs = 0;
    let heightOfNewBtnAndSearchBar = 0;
    let heightOfPagination = 0;
    let heightOfResultsFoundDiv = 0;
    let topPageHeight = 0;
    if (document.getElementById("back-to-item-bank-selection-btn") != null) {
      heightOfBackToItemBankSelectionButton = document.getElementById(
        "back-to-item-bank-selection-btn"
      ).offsetHeight;
      if (document.getElementsByClassName("nav nav-tabs").length > 0) {
        heightOfTopTabs = document.getElementsByClassName("nav nav-tabs")[0].offsetHeight;
      }
      if (
        document.getElementById("item-bank-items-create-new-item-btn") !== null &&
        document.getElementById(
          "item-bank-items-search-display-options-dropdown-react-select-dropdown"
        ) !== null
      ) {
        heightOfNewBtnAndSearchBar =
          24 + // height of padding above the new item button
          // height of the create new item button
          document.getElementById("item-bank-items-create-new-item-btn").offsetHeight +
          18 + // height of marging above the search bar
          // height of the display options dropdown
          document.getElementById(
            "item-bank-items-search-display-options-dropdown-react-select-dropdown"
          ).offsetHeight +
          12; // height of marging below the search bar;
        if (document.getElementsByClassName("pagination").length > 0) {
          heightOfPagination =
            document.getElementsByClassName("pagination")[0].offsetHeight +
            24 + // height of marging above the pagination section
            12; // height of padding below the pagination section
        }
        if (
          this.props.itemsActiveSearch &&
          document.getElementById("search-bar-results-found-main-div") !== null
        ) {
          heightOfResultsFoundDiv = document.getElementById(
            "search-bar-results-found-main-div"
          ).offsetHeight;
        }
      }
    } else {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }

    topPageHeight +=
      heightOfBackToItemBankSelectionButton + heightOfTopTabs + heightOfResultsFoundDiv + 24; // height of "back to item bank selection button" + height of margin under button

    const customHeight = getItemBankHeightCalculations(
      this.props.accommodations.fontSize,
      this.props.accommodations.spacing,
      this.props.testNavBarHeight,
      topPageHeight - heightOfResultsFoundDiv,
      0
    );

    const customTableHeight = getItemBankHeightCalculations(
      this.props.accommodations.fontSize,
      this.props.accommodations.spacing,
      this.props.testNavBarHeight,
      topPageHeight + heightOfNewBtnAndSearchBar + heightOfPagination,
      0
    );
    // =========================================================================================

    return (
      <div style={{ ...styles.mainContainer, ...customHeight.maxHeight }}>
        <div style={styles.createButton}>
          <CustomButton
            buttonId={"item-bank-items-create-new-item-btn"}
            label={
              <>
                <FontAwesomeIcon icon={faPlusCircle} style={styles.buttonIcon} />
                <span>
                  {
                    LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                      .createNewItemButton
                  }
                </span>
              </>
            }
            action={this.openCreateNewItemPopup}
            buttonTheme={THEME.PRIMARY}
          />
        </div>
        <div style={styles.tableContainer}>
          <SearchBarWithDisplayOptions
            idPrefix={"item-bank-items-search"}
            currentlyLoading={this.props.itemsTableRendering}
            updateSearchStates={this.props.updateSearchItemsStates}
            updatePageState={this.props.updateItemsPageState}
            paginationPageSize={this.props.itemsPaginationPageSize}
            updatePaginationPageSize={this.props.updateItemsPageSizeState}
            data={this.state.rowsDefinition || []}
            resultsFound={this.state.resultsFound}
            searchBarContent={this.props.itemsKeyword}
          />
          <div style={{ ...styles.overflow, ...customTableHeight.maxHeight }}>
            <GenericTable
              classnamePrefix="item-bank-items"
              columnsDefinition={columnsDefinition}
              rowsDefinition={this.state.rowsDefinition}
              emptyTableMessage={
                LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items.table
                  .noData
              }
              style={styles.noBorder}
              currentlyLoading={this.props.itemsTableRendering}
            />
          </div>
          <div style={styles.paginationContainer}>
            <ReactPaginate
              ref={this.paginationDivRef}
              pageCount={this.state.numberOfItemsPages}
              containerClassName={"pagination"}
              breakClassName={"break"}
              activeClassName={"active-page"}
              marginPagesDisplayed={3}
              // "-1" because react-paginate uses index 0 and itemsPaginationPage redux state uses index 1
              forcePage={this.props.itemsPaginationPage - 1}
              onPageChange={page => {
                this.handlePageChange(page);
              }}
              previousLabel={
                <div style={styles.paginationIcon}>
                  <label style={styles.hiddenText}>
                    {LOCALIZE.commons.pagination.previousPageButton}
                  </label>
                  <FontAwesomeIcon icon={faCaretLeft} />
                </div>
              }
              nextLabel={
                <div style={styles.paginationIcon}>
                  <label style={styles.hiddenText}>
                    {LOCALIZE.commons.pagination.nextPageButton}
                  </label>
                  <FontAwesomeIcon icon={faCaretRight} />
                </div>
              }
            />
          </div>
        </div>
        <PopupBox
          show={this.state.showCreateNewItemPopup}
          handleClose={() => {}}
          title={
            LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
              .createItemPopup.title
          }
          description={
            <div>
              <p>
                {
                  LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                    .createItemPopup.description
                }
              </p>
              <Row
                className="align-items-center justify-content-start"
                style={styles.itemContainer}
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={styles.labelContainer}
                >
                  <label style={styles.label}>
                    <label id="item-bank-item-response-format-title">
                      {
                        LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                          .items.createItemPopup.responseFormatLabel
                      }
                    </label>
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={styles.inputContainer}
                >
                  <DropdownSelect
                    idPrefix="item-bank-item-response-format"
                    isValid={true}
                    ariaRequired={true}
                    ariaLabelledBy="item-bank-item-response-format-title"
                    hasPlaceholder={true}
                    options={this.state.newItemResponseFormatOptions}
                    onChange={this.getSelectedNewItemResponseFormatOption}
                    defaultValue={this.state.newItemResponseFormatSelectedOption}
                    isMulti={false}
                  />
                </Col>
              </Row>
              <Row
                className="align-items-center justify-content-start"
                style={styles.itemContainer}
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={styles.labelContainer}
                >
                  <label style={styles.label}>
                    <label id="item-bank-item-historical-id-title">
                      {
                        LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                          .items.createItemPopup.historicalIdLabel
                      }
                    </label>
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={styles.inputContainer}
                >
                  <input
                    id="item-bank-item-historical-id"
                    className={
                      !this.state.itemHistoricalIdAlreadyUsed ? "valid-field" : "invalid-field"
                    }
                    aria-required={false}
                    aria-invalid={this.state.itemHistoricalIdAlreadyUsed}
                    style={{ ...styles.input, ...accommodationsStyle }}
                    type="text"
                    value={this.state.newItemHistoricalId}
                    onChange={this.updateNewItemHistoricalIdContent}
                  ></input>
                  {this.state.itemHistoricalIdAlreadyUsed && (
                    <label
                      id="item-bank-item-historical-id-error"
                      htmlFor="item-bank-item-historical-id"
                      style={styles.errorMessage}
                    >
                      {
                        LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                          .items.createItemPopup.historicalIdAlreadyBeenUsedError
                      }
                    </label>
                  )}
                </Col>
              </Row>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonLabel={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeCreateNewItemPopup}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={
            !this.state.currentlyCreatingNewItem ? (
              LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                .createItemPopup.rightButton
            ) : (
              // this div is useful to get the right size of the button while loading
              <div style={styles.customLoadingContainer}>
                {/* this div is useful to get the right size of the button while loading */}
                <div style={{ ...styles.loadingOverlappingStyle, ...styles.transparentText }}>
                  <FontAwesomeIcon icon={faTimes} style={styles.icon} />
                  {
                    LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                      .createItemPopup.rightButton
                  }
                </div>
                <div style={styles.loadingOverlappingStyle}>
                  <label className="fa fa-spinner fa-spin">
                    <FontAwesomeIcon icon={faSpinner} />
                  </label>
                </div>
              </div>
            )
          }
          rightButtonLabel={
            LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
              .createItemPopup.rightButton
          }
          rightButtonIcon={!this.state.currentlyCreatingNewItem ? faPlusCircle : ""}
          rightButtonAction={this.handleCreateNewItem}
          rightButtonState={
            this.state.currentlyCreatingNewItem
              ? BUTTON_STATE.disabled
              : this.state.isValidNewItemForm
              ? BUTTON_STATE.enabled
              : BUTTON_STATE.disabled
          }
        />
        {Object.entries(this.state.currentSelectedItemData).length > 0 && (
          <PopupBox
            show={this.state.showViewItemPopup}
            handleClose={this.closeViewItemPopup}
            title={LOCALIZE.formatString(
              LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                .viewSelectedItemPopup.title,
              this.state.currentSelectedItemData.system_id
            )}
            description={
              <div>
                <div>
                  <ItemComponent
                    itemContent={getItemContentToPreview(
                      this.state.currentSelectedItemData,
                      this.props.languageData,
                      // hardcoding content-1 tab, since we only need to provide the regular content for now
                      [{ key: "content-1" }],
                      "content-1"
                    )}
                    index={this.props.item_bank_data.item_bank_items.findIndex(obj => {
                      return obj.item_id === this.state.currentSelectedItemData.item_id;
                    })}
                    languageCode={this.state.itemBankLanguageData.ISO_Code_1}
                    calledFromQuestionPreview={true}
                  />
                </div>
                <div>
                  <ItemOptions
                    itemOptions={getItemOptionsToPreview(
                      this.state.currentSelectedItemData,
                      this.props.languageData,
                      // hardcoding content-1 tab, since we only need to provide the regular content for now
                      [{ key: "content-1" }],
                      "content-1"
                    )}
                    languageCode={this.state.itemBankLanguageData.ISO_Code_1}
                    calledFromQuestionPreview={true}
                  />
                </div>
              </div>
            }
            rightButtonType={BUTTON_TYPE.primary}
            rightButtonTitle={LOCALIZE.commons.close}
            rightButtonAction={this.closeViewItemPopup}
            rightButtonIcon={faTimes}
          />
        )}
      </div>
    );
  }
}

export { Items as unconnectedItems };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    languageData: state.localize.languageData,
    accommodations: state.accommodations,
    item_bank_definition: state.itemBank.item_bank_data.item_bank_definition[0],
    item_bank_data: state.itemBank.item_bank_data,
    itemData: state.itemBank.itemData,
    itemsPaginationPage: state.itemBank.itemsPaginationPage,
    itemsPaginationPageSize: state.itemBank.itemsPaginationPageSize,
    itemsActiveSearch: state.itemBank.itemsActiveSearch,
    itemsKeyword: state.itemBank.itemsKeyword,
    testNavBarHeight: state.testSection.testNavBarHeight,
    currentTab: state.navTabs.currentTopTab,
    itemBankInitialData: state.itemBank.itemBankInitialData,
    itemsTableRendering: state.itemBank.itemsTableRendering
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAllItems,
      getFoundItems,
      updateSearchItemsStates,
      updateItemsPageState,
      updateItemsPageSizeState,
      getResponseFormats,
      createNewItem,
      setItemData,
      setItemInitialData,
      resetTopTabsStates,
      setItemBankData,
      setItemBankInitialData,
      setDataChangesDetected,
      getAndSetSelectedItem,
      getLanguageData,
      setLanguageData,
      setItemsTableRendering
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Items);
