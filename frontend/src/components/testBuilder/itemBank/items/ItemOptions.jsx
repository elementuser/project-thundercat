/* eslint-disable react/no-children-prop */
import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { ITEM_OPTION_TYPE } from "./Constants";
import MarkdownButtonAnswer from "../../../testFactory/MarkdownButtonAnswer";
import { TestSectionComponentType } from "../../../testFactory/Constants";

class ItemOptions extends Component {
  constructor(props, context) {
    super(props, context);

    this.PropTypes = {
      itemOptions: PropTypes.object.isRequired,
      calledFromQuestionPreview: PropTypes.bool,
      handleAnswerClick: PropTypes.func.isRequired
    };
    ItemOptions.defaultProps = {
      calledFromQuestionPreview: false
    };
  }

  factory = (itemOptions, languageCode) => {
    return (
      <div id="unit-test-item-options">
        {typeof itemOptions[languageCode] !== "undefined"
          ? itemOptions[languageCode].map((data, index) => {
              switch (data.option_type) {
                case ITEM_OPTION_TYPE.multipleChoice:
                  // calledFromQuestionPreview is defined and set to true
                  if (this.props.calledFromQuestionPreview) {
                    return (
                      <MarkdownButtonAnswer
                        key={`${data.id}-${index}`}
                        // sending a formatted object so it is compatible with the format used in MarkdownButtonAnswer
                        answer={{ content: data.text }}
                        answerId={data.item_option_id}
                        questionId={data.item_id}
                        handleClick={() => {}}
                        languageCode={languageCode}
                      />
                    );
                  }
                  // called from sample or real test (need the question header)
                  return (
                    <MarkdownButtonAnswer
                      key={`${data.id}-${index}`}
                      // sending a formatted object so it is compatible with the format used in MarkdownButtonAnswer
                      answer={{ content: data.text }}
                      source={TestSectionComponentType.ITEM_BANK}
                      answerId={data.item_option_id}
                      questionId={data.item_id}
                      handleClick={this.props.handleAnswerClick}
                      languageCode={languageCode}
                    />
                  );

                case ITEM_OPTION_TYPE.multipleChoiceScreenReader:
                  // calledFromQuestionPreview is defined and set to true
                  if (this.props.calledFromQuestionPreview) {
                    return (
                      <MarkdownButtonAnswer
                        key={`${data.id}-${index}`}
                        // sending a formatted object so it is compatible with the format used in MarkdownButtonAnswer
                        answer={{ content: data.text }}
                        source={TestSectionComponentType.ITEM_BANK}
                        answerId={data.id}
                        questionId={data.item_id}
                        handleClick={this.props.handleAnswerClick}
                        languageCode={languageCode}
                      />
                    );
                  }
                  // TODO
                  return null;

                default:
                  return (
                    <div key={index} id="unit-test-item-option-undefined-section">
                      <h1>Unsupported Item Option Type: {data.option_type}</h1>
                    </div>
                  );
              }
            })
          : []}
      </div>
    );
  };

  render() {
    const { itemOptions, languageCode } = this.props;
    return <>{this.factory(itemOptions, languageCode)}</>;
  }
}

export { ItemOptions as UnconnectedItemOptions };
const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language
  };
};

export default connect(mapStateToProps, null)(ItemOptions);
