import React, { Component } from "react";
import * as _ from "lodash";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getItemBankHeightCalculations } from "../../../../helpers/inTestHeightCalculations";
import LOCALIZE from "../../../../text_resources";
import DropdownSelect from "../../../commons/DropdownSelect";
import {
  getResponseFormats,
  getDevelopmentStatuses,
  getAndSetSelectedItem,
  getSelectedItemVersionData,
  setItemData,
  setItemInitialData,
  setDataChangesDetected,
  setDataReadyForUpload,
  deleteItemDraft,
  getAllItems,
  setItemBankData,
  setItemBankInitialData,
  updateItemData
} from "../../../../modules/ItemBankRedux";
import { LANGUAGES } from "../../../commons/Translation";
import { DRAFT_VERSION, ITEM_ACTION_TYPE } from "../Constants";
import PopupBox, { BUTTON_TYPE } from "../../../commons/PopupBox";
import SystemMessage, { MESSAGE_TYPE } from "../../../commons/SystemMessage";
import {
  faCheck,
  faPencilAlt,
  faPlusCircle,
  faTimes,
  faTrashAlt
} from "@fortawesome/free-solid-svg-icons";
import CustomButton, { THEME } from "../../../commons/CustomButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import StyledTooltip, { EFFECT, TRIGGER_TYPE, TYPE } from "../../../authentication/StyledTooltip";
import { Col, Row } from "react-bootstrap";
import { getLineSpacingCSS } from "../../../../modules/AccommodationsRedux";
import Switch from "react-switch";
import getSwitchTransformScale from "../../../../helpers/switchTransformScale";
import objectEqualsCheck from "../../../../helpers/objectHelpers";
import { ITEM_DEVELOPMENT_STATUS } from "./Constants";

const FUNCTION_SOURCE = {
  systemId: "system_id",
  historicalId: "historical_id",
  itemVersion: "item_version"
};

const styles = {
  mainContainer: {
    width: "100%",
    padding: "24px 12px",
    border: "1px solid #cdcdcd",
    borderTop: "none",
    borderBottom: "none",
    overflowY: "auto",
    overflowX: "hidden"
  },
  dropdownDiv: {
    marginBottom: 12
  },
  label: {
    fontWeight: "bold",
    padding: "0 0 2px 2px"
  },
  historicalIdLabel: {
    padding: 0
  },
  labelFloatContainer: {
    width: "100%"
  },
  actionLabel: {
    marginRight: 6
  },
  actionButtons: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "0px 6px",
    color: "#00565e",
    float: "right"
  },
  allUnset: {
    all: "unset"
  },
  historicalIdInputContainer: {
    padding: "15px 40px 15px 40px"
  },
  historicalIdInput: {
    minHeight: 38,
    border: "1px solid #00565e",
    padding: "3px 6px 3px 6px",
    borderRadius: 4,
    height: "100%",
    width: "100%"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6,
    width: "100%"
  }
};

class GeneralTab extends Component {
  state = {
    isLoading: false,
    triggerRerender: false,
    systemIdOptions: [],
    historicalIdOptions: [],
    developmentStatusOptions: [],
    versionOptions: [],
    responseFormatOptions: [],
    scoringFormatOptions: [],
    showChangeItemConfirmationPopup: false,
    selectedOptionData: {},
    functionSource: null,
    showDeleteDraftConfirmationPopup: false,
    showAddEditHistoricalIdPopup: false,
    historicalIdContent:
      this.props.itemData.historical_id !== null ? this.props.itemData.historical_id : "",
    displayInvalidHistoricalIdError: false,
    // default values
    switchHeight: 25,
    switchWidth: 50
  };

  componentDidMount = () => {
    // populating dropdown options
    this.populateDropdownOptions();
    this.getUpdatedSwitchDimensions();
    // checking for changes
    this.changesDetected();
  };

  componentDidUpdate = prevProps => {
    // if currentTab gets updated
    if (prevProps.currentTab !== this.props.currentTab) {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }
    // if itemData gets updated
    if (prevProps.itemData !== this.props.itemData) {
      // populating dropdown options
      this.populateDropdownOptions();
      // checking for changes
      this.changesDetected();
    }
    // if fontSize gets updated
    if (prevProps.accommodations.fontSize !== this.props.accommodations.fontSize) {
      this.getUpdatedSwitchDimensions();
    }
  };

  populateDropdownOptions = () => {
    // populating dropdown options
    this.populateSystemIdOptions();
    this.populateHistoricalIdOptions();
    this.populateDevelopmentStatusOptions();
    this.populateVersionOptions();
    this.populateResponseFormatOptions();
    this.populateScoringFormatOptions();
  };

  populateSystemIdOptions = () => {
    const systemIdOptions = [];
    // looping in item_bank_items
    for (let i = 0; i < this.props.item_bank_data.item_bank_items.length; i++) {
      systemIdOptions.push({
        label: this.props.item_bank_data.item_bank_items[i].system_id,
        value: this.props.item_bank_data.item_bank_items[i].item_id,
        // order needed for the sorting
        order: parseInt(this.props.item_bank_data.item_bank_items[i].system_id.split("_")[1]),
        system_id: this.props.item_bank_data.item_bank_items[i].system_id
      });
    }
    // properly ordering the items
    systemIdOptions.sort((a, b) => a.order - b.order);
    // setting respective state
    this.setState({ systemIdOptions: systemIdOptions });
  };

  populateHistoricalIdOptions = () => {
    const historicalIdOptions = [];
    // looping in item_bank_items
    for (let i = 0; i < this.props.item_bank_data.item_bank_items.length; i++) {
      // if the historical ID is defined
      if (
        this.props.item_bank_data.item_bank_items[i].historical_id !== "" &&
        this.props.item_bank_data.item_bank_items[i].historical_id !== null
      ) {
        historicalIdOptions.push({
          label: this.props.item_bank_data.item_bank_items[i].historical_id,
          value: this.props.item_bank_data.item_bank_items[i].item_id,
          system_id: this.props.item_bank_data.item_bank_items[i].system_id
        });
      }
    }
    // setting respective state
    this.setState({ historicalIdOptions: historicalIdOptions });
  };

  populateDevelopmentStatusOptions = () => {
    const developmentStatusOptions = [];
    this.props.getDevelopmentStatuses().then(response => {
      for (let i = 0; i < response.length; i++) {
        // Interface is in English
        if (this.props.currentLanguage === LANGUAGES.english) {
          developmentStatusOptions.push({
            label: `${response[i].en_name}`,
            value: response[i].id,
            development_status_codename: response[i].codename,
            development_status_name_en: response[i].en_name,
            development_status_name_fr: response[i].fr_name
          });
          // Interface is in French
        } else {
          developmentStatusOptions.push({
            label: `${response[i].fr_name}`,
            value: response[i].id,
            development_status_codename: response[i].codename,
            development_status_name_en: response[i].en_name,
            development_status_name_fr: response[i].fr_name
          });
        }
      }
      // setting respective state
      this.setState({ developmentStatusOptions: developmentStatusOptions });
    });
  };

  populateVersionOptions = () => {
    const versionOptions = [];
    // looping in item_bank_items
    for (let i = 0; i < this.props.itemData.available_versions.length; i++) {
      if (this.props.itemData.available_versions[i].version !== DRAFT_VERSION) {
        versionOptions.push({
          label: this.props.itemData.available_versions[i].version,
          value: this.props.itemData.available_versions[i].item_id
        });
      }
    }
    // ordering by version descending
    versionOptions.sort((a, b) => b.label - a.label);
    // adding draft version as first element of the table (if draft exists)
    if (
      this.props.itemData.available_versions.filter(obj => obj.version === DRAFT_VERSION).length > 0
    ) {
      versionOptions.unshift({
        label:
          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items.general
            .draftVersionLabel,
        value: DRAFT_VERSION
      });
    }
    // setting respective state
    this.setState({ versionOptions: versionOptions });
  };

  populateResponseFormatOptions = () => {
    const responseFormatOptions = [];
    this.props.getResponseFormats().then(response => {
      for (let i = 0; i < response.length; i++) {
        // Interface is in English
        if (this.props.currentLanguage === LANGUAGES.english) {
          responseFormatOptions.push({
            label: `${response[i].en_name}`,
            value: response[i].id,
            response_format_codename: response[i].codename,
            response_format_name_en: response[i].en_name,
            response_format_name_fr: response[i].fr_name
          });
          // Interface is in French
        } else {
          responseFormatOptions.push({
            label: `${response[i].fr_name}`,
            value: response[i].id,
            response_format_codename: response[i].codename,
            response_format_name_en: response[i].en_name,
            response_format_name_fr: response[i].fr_name
          });
        }
      }
      // setting respective state
      this.setState({ responseFormatOptions: responseFormatOptions });
    });
  };

  // TODO
  populateScoringFormatOptions = () => {
    this.setState({ scoringFormatOptions: [] });
  };

  // getting switch dimensions and updating states
  getUpdatedSwitchDimensions = () => {
    // need this loading state, otherwise the switch diameter is not calculated on state change
    this.setState({ isLoading: true }, () => {
      this.setState(
        {
          switchHeight: getSwitchTransformScale(this.props.accommodations.fontSize).height,
          switchWidth: getSwitchTransformScale(this.props.accommodations.fontSize).width
        },
        () => {
          this.setState({ isLoading: false });
        }
      );
    });
  };

  changeItem = selectedOption => {
    // getting and setting new selected item
    this.props.getAndSetSelectedItem(selectedOption);
  };

  changeItemVersion = selectedOption => {
    this.props
      .getSelectedItemVersionData(selectedOption.value, this.props.itemData.system_id)
      .then(response => {
        this.props.setItemData(response, this.props.languageData);
        this.props.setItemInitialData(response, this.props.languageData);
        this.props.setDataChangesDetected(false);

        // checking if we're in a draft version
        if (typeof response.version === "undefined") {
          this.props.setDataReadyForUpload(true);
        }
      });
  };

  changeItemStatus = selectedOption => {
    // updating redux states
    // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
    const new_obj = {
      ...this.props.itemData,
      development_status_id: selectedOption.value,
      development_status_codename: selectedOption.development_status_codename,
      development_status_name_en: selectedOption.development_status_name_en,
      development_status_name_fr: selectedOption.development_status_name_fr
    };

    // setting new itemData redux state
    this.props.setItemData(new_obj, this.props.languageData);
  };

  changeResponseFormat = selectedOption => {
    // updating redux states
    // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
    const new_obj = {
      ...this.props.itemData,
      response_format_id: selectedOption.value,
      response_format_codename: selectedOption.response_format_codename,
      response_format_name_en: selectedOption.response_format_name_en,
      response_format_name_fr: selectedOption.response_format_name_fr
    };

    // setting new itemData redux state
    this.props.setItemData(new_obj, this.props.languageData);
  };

  // TODO (does not exist yet)
  changeScoringFormat = selectedOption => {
    // updating redux states
    // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
    const new_obj = {
      ...this.props.itemData,
      scoring_format_id: selectedOption.value,
      scoring_format_codename: selectedOption.scoring_format_codename,
      scoring_format_name_en: selectedOption.scoring_format_name_en,
      scoring_format_name_fr: selectedOption.scoring_format_name_fr
    };

    // setting new itemData redux state
    this.props.setItemData(new_obj, this.props.languageData);
  };

  changeShuffleOptions = event => {
    // updating redux states
    // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
    const new_obj = {
      ...this.props.itemData,
      shuffle_options: event
    };

    // setting new itemData redux state
    this.props.setItemData(new_obj, this.props.languageData);
  };

  changeActiveStatus = event => {
    // updating redux states
    // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
    const new_obj = {
      ...this.props.itemData,
      active_status: event
    };

    // setting new itemData redux state
    this.props.setItemData(new_obj, this.props.languageData);

    // adding small delay, just to make sure that the itemData redux state is up to date when calling this logic
    setTimeout(() => {
      const data = {
        section_to_update: ITEM_ACTION_TYPE.activateDeactivate,
        data: this.props.itemData
      };
      // updating item active_status (directly in the DB)
      this.props.updateItemData(data);

      // getting index of current item
      const indexOfCurrentItem = this.props.item_bank_data.item_bank_items.findIndex(obj => {
        return obj.system_id === this.props.itemData.system_id;
      });
      const copyOfItemBankData = this.props.item_bank_data;
      copyOfItemBankData.item_bank_items[indexOfCurrentItem] = this.props.itemData;
      this.props.setItemBankData(copyOfItemBankData);
    }, 100);
  };

  getActiveStatusDisabledState = () => {
    let disabled = false;
    // currently in a draft or item status is ARCHIVED or DRAFT
    if (
      typeof this.props.itemData.version === "undefined" ||
      this.props.itemData.development_status_codename === ITEM_DEVELOPMENT_STATUS.retired ||
      this.props.itemData.development_status_codename === ITEM_DEVELOPMENT_STATUS.draft
    ) {
      disabled = true;
      // changes are detected
    } else if (this.props.dataChangesDetected) {
      disabled = true;
    }
    return disabled;
  };

  changesDetected = () => {
    // removing needed attributes from the comparison
    const copyOfItemData = _.cloneDeep(this.props.itemData);
    delete copyOfItemData.item_comments;
    delete copyOfItemData.active_status;
    const copyOfItemInitialData = _.cloneDeep(this.props.itemInitialData);
    delete copyOfItemInitialData.item_comments;
    delete copyOfItemInitialData.active_status;

    let changesDetected = false;
    if (!objectEqualsCheck(copyOfItemInitialData, copyOfItemData)) {
      changesDetected = true;
    }
    this.props.setDataChangesDetected(changesDetected);
    // if changesDetected is true
    if (changesDetected) {
      this.props.setDataReadyForUpload(false);
    }
    this.setState({ triggerRerender: !this.state.triggerRerender });
  };

  openChangeItemConfirmationPopup = (selectedOption, functionSource) => {
    this.setState({
      showChangeItemConfirmationPopup: true,
      selectedOptionData: selectedOption,
      functionSource: functionSource
    });
  };

  closeChangeItemConfirmationPopup = () => {
    this.setState({ showChangeItemConfirmationPopup: false });
  };

  openDeleteDraftConfirmationPopup = () => {
    this.setState({ showDeleteDraftConfirmationPopup: true });
  };

  closeDeleteDraftConfirmationPopup = () => {
    this.setState({ showDeleteDraftConfirmationPopup: false });
  };

  handleDeleteDraft = () => {
    const body = {
      system_id: this.props.itemData.system_id
    };
    this.props.deleteItemDraft(body).then(response => {
      if (response.ok) {
        this.props
          .getAllItems(
            this.props.item_bank_definition.id,
            this.props.itemsPaginationPage,
            this.props.itemsPaginationPageSize
          )
          .then(response => {
            // updating redux states
            // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
            const new_obj = {
              ...this.props.item_bank_data,
              item_bank_items: response.all_items,
              item_bank_number_of_found_items: response.all_items.length
            };
            this.props.setItemBankData(new_obj);

            // Set the value for the initial data of the Item Bank
            // This is used to detect changes on multiple components of Item Bank
            this.props.setItemBankInitialData(_.cloneDeep(new_obj));

            // getting index of selected item
            const indexOfSelectedItem = response.all_items.findIndex(obj => {
              return obj.system_id === this.props.itemData.system_id;
            });

            this.props.setItemData(
              response.all_items[indexOfSelectedItem],
              this.props.languageData
            );
            this.props.setItemInitialData(
              response.all_items[indexOfSelectedItem],
              this.props.languageData
            );
            this.props.setDataReadyForUpload(false);
            this.props.setDataChangesDetected(false);
            // closing popup
            this.closeDeleteDraftConfirmationPopup();
          });
        // should never happen
      } else {
        throw new Error("An error occurred during the delete item draft process");
      }
    });
  };

  openAddEditHistoricalIdPopup = () => {
    this.setState({
      showAddEditHistoricalIdPopup: true,
      historicalIdContent:
        this.props.itemData.historical_id !== null ? this.props.itemData.historical_id : ""
    });
  };

  closeAddEditHistoricalIdPopup = () => {
    this.setState({ showAddEditHistoricalIdPopup: false, displayInvalidHistoricalIdError: false });
  };

  updateHistoricalIdContent = event => {
    const historicalIdContent = event.target.value;
    // allow only alphanumeric, slash, underscore and dash (0 to 50 chars)
    const regexExpression = /^([a-zA-z0-9-/_]{0,50})$/;
    if (regexExpression.test(historicalIdContent)) {
      this.setState({
        historicalIdContent: historicalIdContent.toUpperCase()
      });
    }
  };

  handleAddEditHistoricalId = () => {
    // initializing historicalIdAlreadyExists
    let historicalIdAlreadyExists = false;

    // historical ID is defined (not "")
    if (this.state.historicalIdContent !== "") {
      // looping in all items
      for (let i = 0; i < this.props.item_bank_data.item_bank_items.length; i++) {
        // if there is another matching historical ID (except for the current selected item)
        if (
          this.props.item_bank_data.item_bank_items[i].item_id !== this.props.itemData.item_id &&
          this.props.item_bank_data.item_bank_items[i].historical_id ===
            this.state.historicalIdContent
        ) {
          // set historicalIdAlreadyExists to true
          historicalIdAlreadyExists = true;
          break;
        }
      }
    }

    // valid historical ID
    if (!historicalIdAlreadyExists) {
      // updating redux states
      // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
      const new_obj = {
        ...this.props.itemData,
        historical_id: this.state.historicalIdContent
      };
      // setting new itemData redux state
      this.props.setItemData(new_obj, this.props.languageData);
      // closing popup
      this.closeAddEditHistoricalIdPopup();
      // historical ID already exists
    } else {
      this.setState({ displayInvalidHistoricalIdError: true }, () => {
        // focusing on input
        document.getElementById("historical-id=label-input").focus();
      });
    }
  };

  render() {
    // calculating the height of the top tabs and above (without considering the nav bar)
    // =========================================================================================
    let heightOfBackToItemBankSelectionButton = 0;
    let heightOfTopTabs = 0;
    let heightOfFooter = 0;
    let topPageHeight = 0;
    if (document.getElementById("back-to-item-bank-editor-btn") != null) {
      heightOfBackToItemBankSelectionButton = document.getElementById(
        "back-to-item-bank-editor-btn"
      ).offsetHeight;
      if (document.getElementsByClassName("nav nav-tabs").length > 0) {
        heightOfTopTabs = document.getElementsByClassName("nav nav-tabs")[0].offsetHeight;
      }
    } else {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }

    topPageHeight += heightOfBackToItemBankSelectionButton + heightOfTopTabs + 12; // height of "back to item bank selection button" + height of margin under button

    if (document.getElementById("item-bank-footer-main-div") !== null) {
      heightOfFooter = document.getElementById("item-bank-footer-main-div").offsetHeight;
    } else {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }

    const customHeight = getItemBankHeightCalculations(
      this.props.accommodations.fontSize,
      this.props.accommodations.spacing,
      this.props.testNavBarHeight,
      topPageHeight,
      heightOfFooter
    );
    // =========================================================================================

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <div style={{ ...styles.mainContainer, ...customHeight.height }}>
        <div style={styles.dropdownDiv}>
          <label id="item-bank-selected-item-general-system-id-label" style={styles.label}>
            {
              LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items.general
                .systemIdLabel
            }
          </label>
          <DropdownSelect
            idPrefix="item-bank-selected-item-general-system-id"
            ariaLabelledBy="item-bank-selected-item-general-system-id-label"
            options={this.state.systemIdOptions}
            onChange={
              this.props.dataChangesDetected
                ? e => this.openChangeItemConfirmationPopup(e, FUNCTION_SOURCE.systemId)
                : this.changeItem
            }
            defaultValue={{
              label: this.props.itemData.system_id,
              value: this.props.itemData.item_id
            }}
            hasPlaceholder={false}
            orderByLabels={false}
            orderByValues={false}
          ></DropdownSelect>
        </div>
        <div style={styles.dropdownDiv}>
          <label
            id="item-bank-selected-item-general-historical-id-label"
            style={{ ...styles.label, ...styles.labelFloatContainer }}
          >
            <span style={styles.actionLabel}>
              {
                LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                  .general.historicalIdLabel
              }
            </span>
            <span>
              <StyledTooltip
                id="add-edit-historical-id-tooltip"
                place="top"
                type={TYPE.light}
                effect={EFFECT.solid}
                triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      dataTip=""
                      dataFor="add-edit-historical-id-tooltip"
                      label={
                        <>
                          <FontAwesomeIcon
                            icon={
                              this.props.itemData.historical_id !== "" &&
                              this.props.itemData.historical_id !== null
                                ? faPencilAlt
                                : faPlusCircle
                            }
                          />
                        </>
                      }
                      customStyle={styles.actionButtons}
                      action={this.openAddEditHistoricalIdPopup}
                      buttonTheme={THEME.PRIMARY}
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>
                      {this.props.itemData.historical_id !== "" &&
                      this.props.itemData.historical_id !== null
                        ? LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                            .items.general.editHistoricalIdTooltip
                        : LOCALIZE.commons.addButton}
                    </p>
                  </div>
                }
              />
            </span>
          </label>
          <DropdownSelect
            idPrefix="item-bank-selected-item-general-historical-id"
            ariaLabelledBy="item-bank-selected-item-general-historical-id-label"
            options={this.state.historicalIdOptions}
            onChange={
              this.props.dataChangesDetected
                ? e => this.openChangeItemConfirmationPopup(e, FUNCTION_SOURCE.historicalId)
                : this.changeItem
            }
            defaultValue={{
              label: this.props.itemData.historical_id,
              value: this.props.itemData.item_id
            }}
            hasPlaceholder={false}
            orderByLabels={true}
            orderByValues={false}
          ></DropdownSelect>
        </div>
        <div style={styles.dropdownDiv}>
          <label id="item-bank-selected-item-general-item-status-id-label" style={styles.label}>
            {
              LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items.general
                .developmentStatusLabel
            }
          </label>
          <DropdownSelect
            idPrefix="item-bank-selected-item-general-item-status-id"
            ariaLabelledBy="item-bank-selected-item-general-item-status-id-label"
            options={this.state.developmentStatusOptions}
            onChange={this.changeItemStatus}
            defaultValue={{
              label: this.props.itemData[`development_status_name_${this.props.currentLanguage}`],
              value: this.props.itemData.development_status_id,
              development_status_codename: this.props.itemData.development_status_codename,
              development_status_name_en: this.props.itemData.development_status_name_en,
              development_status_name_fr: this.props.itemData.development_status_name_fr
            }}
            hasPlaceholder={false}
            orderByLabels={true}
            orderByValues={false}
          ></DropdownSelect>
        </div>
        <div style={styles.dropdownDiv}>
          <label
            id="item-bank-selected-item-general-version-id-label"
            style={{ ...styles.label, ...styles.labelFloatContainer }}
          >
            <span style={styles.actionLabel}>
              {
                LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                  .general.versionLabel
              }
            </span>
            {/* we're in a draft version */}
            {typeof this.props.itemData.version === "undefined" && (
              <span>
                <StyledTooltip
                  id="delete-draft-tooltip"
                  place="top"
                  type={TYPE.light}
                  effect={EFFECT.solid}
                  triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
                  tooltipElement={
                    <div style={styles.allUnset}>
                      <CustomButton
                        dataTip=""
                        dataFor="delete-draft-tooltip"
                        label={
                          <>
                            <FontAwesomeIcon icon={faTrashAlt} />
                          </>
                        }
                        customStyle={styles.actionButtons}
                        action={this.openDeleteDraftConfirmationPopup}
                        buttonTheme={THEME.PRIMARY}
                      />
                    </div>
                  }
                  tooltipContent={
                    <div>
                      <p>
                        {
                          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                            .items.general.deleteDraftTooltip
                        }
                      </p>
                    </div>
                  }
                />
              </span>
            )}
          </label>
          <DropdownSelect
            idPrefix="item-bank-selected-item-general-version-id"
            ariaLabelledBy="item-bank-selected-item-general-version-id-label"
            options={this.state.versionOptions}
            onChange={
              this.props.dataChangesDetected
                ? e => this.openChangeItemConfirmationPopup(e, FUNCTION_SOURCE.itemVersion)
                : this.changeItemVersion
            }
            defaultValue={
              typeof this.props.itemData.version === "undefined"
                ? {
                    label:
                      LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                        .general.draftVersionLabel,
                    value: DRAFT_VERSION
                  }
                : {
                    label: this.props.itemData.version,
                    value: this.props.itemData.item_id
                  }
            }
            hasPlaceholder={false}
            orderByLabels={false}
            orderByValues={false}
          ></DropdownSelect>
        </div>
        <div style={styles.dropdownDiv}>
          <label id="item-bank-selected-item-general-response-format-id-label" style={styles.label}>
            {
              LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items.general
                .responseFormatLabel
            }
          </label>
          <DropdownSelect
            idPrefix="item-bank-selected-item-general-response-format-id"
            ariaLabelledBy="item-bank-selected-item-general-response-format-id-label"
            options={this.state.responseFormatOptions}
            onChange={this.changeResponseFormat}
            defaultValue={{
              label: this.props.itemData[`response_format_name_${this.props.currentLanguage}`],
              value: this.props.itemData.response_format_id,
              response_format_codename: this.props.itemData.response_format_codename,
              response_format_name_en: this.props.itemData.response_format_name_en,
              response_format_name_fr: this.props.itemData.response_format_name_fr
            }}
            hasPlaceholder={false}
            orderByLabels={false}
            orderByValues={false}
          ></DropdownSelect>
        </div>
        <div style={styles.dropdownDiv}>
          <label id="item-bank-selected-item-general-scoring-format-id-label" style={styles.label}>
            {
              LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items.general
                .scoringFormatLabel
            }
          </label>
          {/* TODO */}
          <DropdownSelect
            idPrefix="item-bank-selected-item-general-scoring-format-id"
            ariaLabelledBy="item-bank-selected-item-general-scoring-format-id-label"
            options={this.state.scoringFormatOptions}
            onChange={this.changeScoringFormat}
            menuPlacement="top"
            // defaultValue={{
            //   label: this.props.itemData[`scoring_format_name_${this.props.currentLanguage}`],
            //   value: this.props.itemData.scoring_format_id,
            //   scoring_format_codename: this.props.itemData.scoring_format_codename,
            //   scoring_format_name_en: this.props.itemData.scoring_format_name_en,
            //   scoring_format_name_fr: this.props.itemData.scoring_format_name_fr
            // }}
            hasPlaceholder={false}
            orderByLabels={false}
            orderByValues={false}
          ></DropdownSelect>
        </div>
        <div style={styles.dropdownDiv}>
          <Row className="align-items-center">
            <Col xl={7} lg={7} md={6} sm={6} xs={6}>
              <label
                id="item-bank-selected-item-general-shuffle-options-label"
                style={styles.label}
              >
                {
                  LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                    .general.shuffleOptionsLabel
                }
              </label>
            </Col>
            <Col className="text-right" xl={5} lg={5} md={6} sm={6} xs={6}>
              {!this.state.isLoading && (
                <Switch
                  onChange={this.changeShuffleOptions}
                  checked={this.props.itemData.shuffle_options}
                  aria-labelledby="item-bank-selected-item-general-shuffle-options-label"
                  height={this.state.switchHeight}
                  width={this.state.switchWidth}
                />
              )}
            </Col>
          </Row>
        </div>
        <div style={styles.dropdownDiv}>
          <Row className="align-items-center">
            <Col xl={6} lg={6} md={6} sm={6} xs={6} id="text-spacing">
              <label id="item-bank-selected-item-general-active-status-label" style={styles.label}>
                {
                  LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                    .general.activeStatusLabel
                }
              </label>
            </Col>
            <Col className="text-right" xl={6} lg={6} md={6} sm={6} xs={6}>
              <Switch
                onChange={this.changeActiveStatus}
                checked={
                  this.getActiveStatusDisabledState() ? false : this.props.itemData.active_status
                }
                disabled={this.getActiveStatusDisabledState()}
                aria-labelledby="item-bank-selected-item-general-active-status-label"
                height={this.state.switchHeight}
                width={this.state.switchWidth}
              />
            </Col>
          </Row>
        </div>
        <PopupBox
          show={this.state.showChangeItemConfirmationPopup}
          handleClose={this.closeChangeItemConfirmationPopup}
          title={
            LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
              .changeItemConfirmationPopup.changeItemTitle
          }
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.warning}
                title={LOCALIZE.commons.warning}
                message={
                  <p>
                    {
                      LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                        .changeItemConfirmationPopup.systemMessageDescription
                    }
                  </p>
                }
              />
              <p>
                {
                  LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                    .changeItemConfirmationPopup.description
                }
              </p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.confirm}
          rightButtonAction={
            this.state.functionSource === FUNCTION_SOURCE.systemId ||
            this.state.functionSource === FUNCTION_SOURCE.historicalId
              ? () => this.changeItem(this.state.selectedOptionData)
              : this.state.functionSource === FUNCTION_SOURCE.itemVersion
              ? () => this.changeItemVersion(this.state.selectedOptionData)
              : () => {}
          }
          rightButtonIcon={faCheck}
          size="lg"
        />
        <PopupBox
          show={this.state.showDeleteDraftConfirmationPopup}
          handleClose={() => {}}
          title={
            LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items.general
              .deleteDraftConfirmationPopup.title
          }
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.warning}
                title={LOCALIZE.commons.warning}
                message={
                  <p>
                    {
                      LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                        .general.deleteDraftConfirmationPopup.systemMessageDescription
                    }
                  </p>
                }
              />
              <p>
                {
                  LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                    .general.deleteDraftConfirmationPopup.description
                }
              </p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeDeleteDraftConfirmationPopup}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.deleteButton}
          rightButtonAction={this.handleDeleteDraft}
          rightButtonIcon={faCheck}
          size="lg"
        />
        <PopupBox
          show={this.state.showAddEditHistoricalIdPopup}
          handleClose={() => {}}
          title={
            LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items.general
              .addEditHistoricalIdPopup.title
          }
          description={
            <div>
              <p>
                {
                  LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                    .general.addEditHistoricalIdPopup.description
                }
              </p>
              <div>
                <Row role="presentation" style={styles.historicalIdInputContainer}>
                  <Col className="align-self-center" xl={4} lg={4} md={12} sm={12}>
                    <label
                      id="historical-id=label"
                      htmlFor="historical-id=label-input"
                      style={styles.historicalIdLabel}
                    >
                      {
                        LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                          .items.general.addEditHistoricalIdPopup.historicalIdLabel
                      }
                    </label>
                  </Col>
                  <Col className="align-self-center" xl={8} lg={8} md={12} sm={12}>
                    <input
                      id="historical-id=label-input"
                      className={
                        !this.state.displayInvalidHistoricalIdError
                          ? "valid-field"
                          : "invalid-field"
                      }
                      ariaLabelledBy="historical-id=label invalid-historical-id-error"
                      style={{ ...styles.historicalIdInput, ...accommodationsStyle }}
                      type="text"
                      value={this.state.historicalIdContent}
                      onChange={this.updateHistoricalIdContent}
                    ></input>
                    {this.state.displayInvalidHistoricalIdError && (
                      <label
                        id="invalid-historical-id-error"
                        htmlFor="historical-id=label-input"
                        style={styles.errorMessage}
                        className="notranslate"
                      >
                        {
                          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                            .items.general.historicalIdAlreadyExistsError
                        }
                      </label>
                    )}
                  </Col>
                </Row>
              </div>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeAddEditHistoricalIdPopup}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.saveButton}
          rightButtonAction={this.handleAddEditHistoricalId}
          rightButtonIcon={faCheck}
          size="lg"
        />
      </div>
    );
  }
}

export { GeneralTab as unconnectedGeneralTab };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    item_bank_data: state.itemBank.item_bank_data,
    item_bank_definition: state.itemBank.item_bank_data.item_bank_definition[0],
    itemsPaginationPage: state.itemBank.itemsPaginationPage,
    itemsPaginationPageSize: state.itemBank.itemsPaginationPageSize,
    itemData: state.itemBank.itemData,
    itemInitialData: state.itemBank.itemInitialData,
    testNavBarHeight: state.testSection.testNavBarHeight,
    currentTab: state.navTabs.currentTopTab,
    dataChangesDetected: state.itemBank.dataChangesDetected,
    languageData: state.localize.languageData
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getResponseFormats,
      getDevelopmentStatuses,
      getAndSetSelectedItem,
      getSelectedItemVersionData,
      setItemData,
      setItemInitialData,
      setDataChangesDetected,
      setDataReadyForUpload,
      deleteItemDraft,
      getAllItems,
      setItemBankData,
      setItemBankInitialData,
      updateItemData
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(GeneralTab);
