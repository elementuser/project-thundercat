import { faPlusCircle, faSpinner, faTimes, faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { Component } from "react";
import { Col, Row } from "react-bootstrap";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getItemBankHeightCalculations } from "../../../../helpers/inTestHeightCalculations";
import { getLineSpacingCSS } from "../../../../modules/AccommodationsRedux";
import {
  createItemComment,
  getItemData,
  setItemData,
  setItemBankData,
  deleteItemComment
} from "../../../../modules/ItemBankRedux";
import LOCALIZE from "../../../../text_resources";
import StyledTooltip, { EFFECT, TRIGGER_TYPE, TYPE } from "../../../authentication/StyledTooltip";
import CustomButton, { THEME } from "../../../commons/CustomButton";
import PopupBox, { BUTTON_STATE, BUTTON_TYPE } from "../../../commons/PopupBox";
import SystemMessage, { MESSAGE_TYPE } from "../../../commons/SystemMessage";

const columnSizes = {
  commentSectionFirstColumn: {
    xs: 11,
    sm: 11,
    md: 11,
    lg: 11,
    xl: 11
  },
  commentSectionSecondColumn: {
    xs: 1,
    sm: 1,
    md: 1,
    lg: 1,
    xl: 1
  },
  popupFirstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 12,
    xl: 12
  },
  popupSecondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 12,
    xl: 12
  }
};

const styles = {
  mainContainer: {
    width: "100%",
    padding: "24px 12px",
    border: "1px solid #cdcdcd",
    borderTop: "none",
    borderBottom: "none",
    overflowY: "auto"
  },
  buttonLabel: {
    margin: 0,
    paddingLeft: 6
  },
  addCommentBtnContainer: {
    marginLeft: 12
  },
  deleteCommentButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  allUnset: {
    all: "unset"
  },
  italic: {
    fontStyle: "italic"
  },
  commentsDeleteButtonTableRow: {
    padding: 0,
    textAlign: "center"
  },
  commentsContainer: {
    margin: "24px 0"
  },
  itemContainer: {
    width: "100%",
    margin: "12px 0"
  },
  labelContainer: {
    verticalAlign: "middle"
  },
  inputContainer: {
    verticalAlign: "middle"
  },
  label: {
    paddingRight: 12,
    margin: 0
  },
  input: {
    width: "100%",
    minHeight: 38,
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565e",
    borderRadius: 4
  },
  textAreaInput: {
    minHeight: 80,
    resize: "none",
    width: "100%"
  },
  bold: {
    fontWeight: "bold"
  }
};

class CommentsTab extends Component {
  state = {
    triggerRerender: false,
    showAddCommentPopup: false,
    commentContent: "",
    addCommentLoading: false,
    showDeleteCommentPopup: false,
    commentDataToBeDeleted: {},
    deleteCommentLoading: false
  };

  componentDidUpdate = prevProps => {
    // if currentTab gets updated
    if (prevProps.currentTab !== this.props.currentTab) {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }
  };

  openAddCommentPopup = () => {
    this.setState({ showAddCommentPopup: true });
  };

  closeAddCommentPopup = () => {
    this.setState({ showAddCommentPopup: false, commentContent: "" });
  };

  updateCommentContent = event => {
    const commentContent = event.target.value;
    // allow maximum of 255 characters
    if (commentContent.length <= 255) {
      this.setState({
        commentContent: commentContent
      });
    }
  };

  handleAddComment = () => {
    this.setState({ addCommentLoading: true }, () => {
      const body = {
        system_id: this.props.itemData.system_id,
        comment: this.state.commentContent,
        // if in draft mode (version is undefined) send empty string to backend
        version:
          typeof this.props.itemData.version !== "undefined" ? this.props.itemData.version : " "
      };
      this.props.createItemComment(body).then(response => {
        if (response.ok) {
          // updating the redux states
          // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
          const new_obj = {
            ...this.props.itemData,
            item_comments: response.item_comments
          };

          // setting new itemData redux state
          this.props.setItemData(new_obj, this.props.languageData);

          // getting index of selected item
          const indexOfSelectedItem = this.props.item_bank_data.item_bank_items.findIndex(obj => {
            return obj.system_id === this.props.itemData.system_id;
          });

          // creating a copy of item_bank_data and updating the needed index of item_bank_items
          const copyOfItemBankData = this.props.item_bank_data;
          copyOfItemBankData.item_bank_items[indexOfSelectedItem].item_comments =
            response.item_comments;

          this.props.setItemBankData(copyOfItemBankData);

          this.setState(
            {
              showAddCommentPopup: false,
              commentContent: ""
            },
            () => {
              setTimeout(() => {
                this.setState({ addCommentLoading: false });
              }, 100);
            }
          );
          // should never happen
        } else {
          throw new Error("An error occurred during the create item comment process");
        }
      });
    });
  };

  openDeleteCommentPopup = data => {
    this.setState({ showDeleteCommentPopup: true, commentDataToBeDeleted: data });
  };

  closeDeleteCommentPopup = () => {
    this.setState({ showDeleteCommentPopup: false, commentDataToBeDeleted: {} });
  };

  handleDeleteComment = () => {
    this.setState({ deleteCommentLoading: true });
    const body = {
      system_id: this.props.itemData.system_id,
      comment_id: this.state.commentDataToBeDeleted.id
    };
    this.props.deleteItemComment(body).then(response => {
      if (response.ok) {
        // updating the redux states
        // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
        const new_obj = {
          ...this.props.itemData,
          item_comments: response.item_comments
        };

        // setting new itemData redux state
        this.props.setItemData(new_obj, this.props.languageData);

        // getting index of selected item
        const indexOfSelectedItem = this.props.item_bank_data.item_bank_items.findIndex(obj => {
          return obj.system_id === this.props.itemData.system_id;
        });

        // creating a copy of item_bank_data and updating the needed index of item_bank_items
        const copyOfItemBankData = this.props.item_bank_data;
        copyOfItemBankData.item_bank_items[indexOfSelectedItem].item_comments =
          response.item_comments;

        this.props.setItemBankData(copyOfItemBankData);

        this.setState(
          {
            showDeleteCommentPopup: false,
            commentDataToBeDeleted: {}
          },
          () => {
            setTimeout(() => {
              this.setState({ deleteCommentLoading: false });
            }, 100);
          }
        );

        // should never happen
      } else {
        throw new Error("An error occurred during the delete item comment process");
      }
    });
  };

  render() {
    // calculating the height of the top tabs and above (without considering the nav bar)
    // =========================================================================================
    let heightOfBackToItemBankSelectionButton = 0;
    let heightOfTopTabs = 0;
    let heightOfFooter = 0;
    let topPageHeight = 0;
    if (document.getElementById("back-to-item-bank-editor-btn") != null) {
      heightOfBackToItemBankSelectionButton = document.getElementById(
        "back-to-item-bank-editor-btn"
      ).offsetHeight;
      if (document.getElementsByClassName("nav nav-tabs").length > 0) {
        heightOfTopTabs = document.getElementsByClassName("nav nav-tabs")[0].offsetHeight;
      }
    } else {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }

    topPageHeight += heightOfBackToItemBankSelectionButton + heightOfTopTabs + 12; // height of "back to item bank selection button" + height of margin under button

    if (document.getElementById("item-bank-footer-main-div") !== null) {
      heightOfFooter = document.getElementById("item-bank-footer-main-div").offsetHeight;
    } else {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }

    const customHeight = getItemBankHeightCalculations(
      this.props.accommodations.fontSize,
      this.props.accommodations.spacing,
      this.props.testNavBarHeight,
      topPageHeight,
      heightOfFooter
    );
    // =========================================================================================

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <div style={{ ...styles.mainContainer, ...customHeight.height }}>
        <div style={styles.addCommentBtnContainer}>
          <CustomButton
            label={
              <>
                <FontAwesomeIcon icon={faPlusCircle} />
                <span id="add-comment-btn" style={styles.buttonLabel} className="notranslate">
                  {
                    LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                      .comments.addCommentButton
                  }
                </span>
              </>
            }
            action={this.openAddCommentPopup}
            buttonTheme={THEME.PRIMARY}
          />
        </div>
        <div style={styles.commentsContainer}>
          {this.props.itemData.item_comments.map(data => {
            return (
              <div style={styles.allUnset}>
                <Row
                  className="align-items-center justify-content-start"
                  style={styles.itemContainer}
                >
                  <Col
                    xl={columnSizes.commentSectionFirstColumn.xl}
                    lg={columnSizes.commentSectionFirstColumn.lg}
                    md={columnSizes.commentSectionFirstColumn.md}
                    sm={columnSizes.commentSectionFirstColumn.sm}
                    xs={columnSizes.commentSectionFirstColumn.xs}
                    style={styles.commentsTableRow}
                  >
                    <p>
                      <span style={styles.bold}>{`${
                        data.modify_date.split(".")[0].split("T")[0]
                      } (v${data.version}) - [${data.user_last_name}, ${
                        data.user_first_name
                      }]:`}</span>
                      <br />
                      <span style={styles.italic}>{data.comment}</span>
                    </p>
                  </Col>
                  <Col
                    xl={columnSizes.commentSectionSecondColumn.xl}
                    lg={columnSizes.commentSectionSecondColumn.lg}
                    md={columnSizes.commentSectionSecondColumn.md}
                    sm={columnSizes.commentSectionSecondColumn.sm}
                    xs={columnSizes.commentSectionSecondColumn.xs}
                    style={styles.commentsDeleteButtonTableRow}
                  >
                    <StyledTooltip
                      id={`delete-item-comment-tooltip-${data.id}`}
                      place="top"
                      type={TYPE.light}
                      effect={EFFECT.solid}
                      triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
                      disabled={data.username !== this.props.username}
                      tooltipElement={
                        <CustomButton
                          dataTip=""
                          dataFor={`delete-item-comment-tooltip-${data.id}`}
                          label={
                            <>
                              <FontAwesomeIcon icon={faTrashAlt} style={styles.buttonIcon} />
                            </>
                          }
                          customStyle={styles.deleteCommentButton}
                          action={() => this.openDeleteCommentPopup(data)}
                          buttonTheme={THEME.PRIMARY}
                          disabled={
                            this.props.isSuperUser ? false : data.username !== this.props.username
                          }
                        />
                      }
                      tooltipContent={
                        <div>
                          <p>{LOCALIZE.commons.deleteButton}</p>
                        </div>
                      }
                    />
                  </Col>
                </Row>
              </div>
            );
          })}
        </div>
        <PopupBox
          show={this.state.showAddCommentPopup}
          handleClose={() => {}}
          size="lg"
          title={
            LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items.comments
              .addCommentPopup.title
          }
          description={
            <div>
              <Row
                className="align-items-center justify-content-start"
                style={styles.itemContainer}
              >
                <Col
                  xl={columnSizes.popupFirstColumn.xl}
                  lg={columnSizes.popupFirstColumn.lg}
                  md={columnSizes.popupFirstColumn.md}
                  sm={columnSizes.popupFirstColumn.sm}
                  xs={columnSizes.popupFirstColumn.xs}
                  style={styles.labelContainer}
                >
                  <label style={styles.label}>
                    <label id="item-bank-item-comment">
                      {
                        LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                          .items.comments.addCommentPopup.commentLabel
                      }
                    </label>
                  </label>
                </Col>
                <Col
                  xl={columnSizes.popupSecondColumn.xl}
                  lg={columnSizes.popupSecondColumn.lg}
                  md={columnSizes.popupSecondColumn.md}
                  sm={columnSizes.popupSecondColumn.sm}
                  xs={columnSizes.popupSecondColumn.xs}
                  style={styles.inputContainer}
                >
                  <textarea
                    id="item-comment"
                    className={"valid-field"}
                    aria-labelledby="item-bank-item-comment"
                    aria-required={true}
                    style={{ ...styles.input, ...styles.textAreaInput, ...accommodationsStyle }}
                    value={this.state.commentContent}
                    onChange={this.updateCommentContent}
                    maxLength="255"
                  ></textarea>
                </Col>
              </Row>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeAddCommentPopup}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={
            !this.state.addCommentLoading ? (
              LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items.comments
                .addCommentPopup.addButton
            ) : (
              // eslint-disable-next-line jsx-a11y/label-has-associated-control
              <label className="fa fa-spinner fa-spin">
                <FontAwesomeIcon icon={faSpinner} />
              </label>
            )
          }
          rightButtonAction={this.handleAddComment}
          rightButtonIcon={!this.state.addCommentLoading ? faPlusCircle : ""}
          rightButtonState={
            this.state.addCommentLoading
              ? BUTTON_STATE.disabled
              : this.state.commentContent !== ""
              ? BUTTON_STATE.enabled
              : BUTTON_STATE.disabled
          }
        />
        {Object.entries(this.state.commentDataToBeDeleted).length > 0 && (
          <PopupBox
            show={this.state.showDeleteCommentPopup}
            handleClose={() => {}}
            size="lg"
            title={
              LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items.comments
                .deleteCommentPopup.title
            }
            description={
              <div>
                <SystemMessage
                  messageType={MESSAGE_TYPE.error}
                  title={LOCALIZE.commons.warning}
                  message={
                    <>
                      <p>
                        {
                          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                            .items.comments.deleteCommentPopup.systemMessageDescription1
                        }
                      </p>
                      <p style={{ ...styles.leftMargin, ...styles.italic }}>
                        {LOCALIZE.formatString(
                          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                            .items.comments.deleteCommentPopup.systemMessageDescription2,
                          this.state.commentDataToBeDeleted.comment
                        )}
                      </p>
                    </>
                  }
                />
              </div>
            }
            leftButtonType={BUTTON_TYPE.primary}
            leftButtonTitle={LOCALIZE.commons.cancel}
            leftButtonIcon={faTimes}
            leftButtonAction={this.closeDeleteCommentPopup}
            rightButtonType={BUTTON_TYPE.danger}
            rightButtonTitle={
              !this.state.deleteCommentLoading ? (
                LOCALIZE.commons.deleteButton
              ) : (
                // eslint-disable-next-line jsx-a11y/label-has-associated-control
                <label className="fa fa-spinner fa-spin">
                  <FontAwesomeIcon icon={faSpinner} />
                </label>
              )
            }
            rightButtonAction={this.handleDeleteComment}
            rightButtonIcon={!this.state.deleteCommentLoading ? faTrashAlt : ""}
            rightButtonState={
              this.state.deleteCommentLoading ? BUTTON_STATE.disabled : BUTTON_STATE.enabled
            }
          />
        )}
      </div>
    );
  }
}

export { CommentsTab as unconnectedCommentsTab };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    languageData: state.localize.languageData,
    accommodations: state.accommodations,
    item_bank_data: state.itemBank.item_bank_data,
    itemData: state.itemBank.itemData,
    testNavBarHeight: state.testSection.testNavBarHeight,
    currentTab: state.navTabs.currentTopTab,
    isSuperUser: state.user.isSuperUser,
    username: state.user.username
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      createItemComment,
      getItemData,
      setItemData,
      setItemBankData,
      deleteItemComment
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(CommentsTab);
