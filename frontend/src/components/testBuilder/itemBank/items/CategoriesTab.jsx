import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getItemBankHeightCalculations } from "../../../../helpers/inTestHeightCalculations";
import DropdownSelect from "../../../commons/DropdownSelect";
import LOCALIZE, { LOCALIZE_OBJ } from "../../../../text_resources";
import { setItemData } from "../../../../modules/ItemBankRedux";

const styles = {
  mainContainer: {
    width: "100%",
    padding: "24px 12px 12px 24px",
    border: "1px solid #cdcdcd",
    borderTop: "none",
    borderBottom: "none",
    overflowY: "auto"
  },
  dropdownLabel: {
    fontWeight: "bold"
  },
  dropdownDiv: {
    padding: "15px 0 15px 0"
  },
  customDropdown: {
    maxWidth: "250px"
  }
};

class CategoriesTab extends Component {
  state = {
    triggerRerender: false
  };

  componentDidUpdate = prevProps => {
    // if currentTab gets updated
    if (prevProps.currentTab !== this.props.currentTab) {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }
  };

  getDropdownSelectValues = index => {
    const options = [];
    const attribute_values =
      this.props.item_bank_data.item_bank_attributes[index].item_bank_attribute_values;

    options.push({
      label: LOCALIZE.commons.na,
      value: LOCALIZE.commons.na
    });

    Object.entries(attribute_values).map(attribute_value =>
      options.push({
        label: attribute_value[1].value.text,
        value: attribute_value[1].id
      })
    );

    return options;
  };

  getInitialValueForDropdown = index => {
    // Default value selected in the dropdown
    const value = LOCALIZE.commons.na;

    // If we have a selected value from the database, make it the default value
    if (
      this.props.itemData.item_attribute_values &&
      this.props.itemData.item_attribute_values.length > 0 &&
      this.props.itemData.item_attribute_values[index].item_bank_attribute_value_id !==
        LOCALIZE_OBJ.en.commons.na
    ) {
      return {
        label: this.props.itemData.item_attribute_values[index].text,
        value: this.props.itemData.item_attribute_values[index].item_bank_attribute_value_id
      };
    }

    // Otherwise, put N/A as the default value
    return { label: value, value: value };
  };

  getSelectedDropdownValue = (index, event) => {
    if (
      this.props.itemData.item_attribute_values &&
      this.props.itemData.item_attribute_values.length > 0
    ) {
      // Create a temporary new itemData
      const new_item_data = { ...this.props.itemData };

      // Look if the selected value isn't N/A
      const item_attribute_values_temp = [...this.props.itemData.item_attribute_values];

      // Create a temporary item_attribute_value[item_att_index]
      const item_attribute_value_temp = {
        ...item_attribute_values_temp[index]
      };

      // Update data depending on the chosen value
      if (event.value !== LOCALIZE.commons.na) {
        // Change it's value with the event.value
        item_attribute_value_temp.item_bank_attribute_value_id = event.value;
        item_attribute_value_temp.text = event.label;
      } else {
        // Change it's value with N/A
        item_attribute_value_temp.item_bank_attribute_value_id = LOCALIZE_OBJ.en.commons.na;
        item_attribute_value_temp.text = LOCALIZE_OBJ.en.commons.na;
      }

      // Replace the array item with the new one
      item_attribute_values_temp[index] = item_attribute_value_temp;

      // Modify the final new_item_data that will replace itemData in the redux states
      new_item_data.item_attribute_values = item_attribute_values_temp;

      // Save&Replace in the redux states
      this.props.setItemData(new_item_data);
    }
  };

  render() {
    // calculating the height of the top tabs and above (without considering the nav bar)
    // =========================================================================================
    let heightOfBackToItemBankSelectionButton = 0;
    let heightOfTopTabs = 0;
    let heightOfFooter = 0;
    let topPageHeight = 0;
    if (document.getElementById("back-to-item-bank-editor-btn") != null) {
      heightOfBackToItemBankSelectionButton = document.getElementById(
        "back-to-item-bank-editor-btn"
      ).offsetHeight;
      if (document.getElementsByClassName("nav nav-tabs").length > 0) {
        heightOfTopTabs = document.getElementsByClassName("nav nav-tabs")[0].offsetHeight;
      }
    } else {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }

    topPageHeight += heightOfBackToItemBankSelectionButton + heightOfTopTabs + 12; // height of "back to item bank selection button" + height of margin under button

    if (document.getElementById("item-bank-footer-main-div") !== null) {
      heightOfFooter = document.getElementById("item-bank-footer-main-div").offsetHeight;
    } else {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }

    const customHeight = getItemBankHeightCalculations(
      this.props.accommodations.fontSize,
      this.props.accommodations.spacing,
      this.props.testNavBarHeight,
      topPageHeight,
      heightOfFooter
    );
    // =========================================================================================
    return (
      <div style={{ ...styles.mainContainer, ...customHeight.height }}>
        {this.props.item_bank_data.item_bank_attributes.map((item_attribute, index) => (
          <div style={styles.dropdownDiv}>
            <label
              style={styles.dropdownLabel}
              id={"attribute-dropdown-".concat(item_attribute.id)}
            >
              {item_attribute.item_bank_attribute_text[this.props.currentLanguage][0].text}
            </label>

            <div style={styles.customDropdown}>
              <DropdownSelect
                idPrefix={"attribute-".concat(item_attribute.id)}
                ariaLabelledBy={"attribute-dropdown-".concat(item_attribute.id)}
                options={this.getDropdownSelectValues(index)}
                defaultValue={this.getInitialValueForDropdown(index)}
                onChange={event => this.getSelectedDropdownValue(index, event)}
                orderByLabels={false}
              ></DropdownSelect>
            </div>
          </div>
        ))}
      </div>
    );
  }
}

export { CategoriesTab as unconnectedCategoriesTab };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    item_bank_data: state.itemBank.item_bank_data,
    itemData: state.itemBank.itemData,
    testNavBarHeight: state.testSection.testNavBarHeight,
    currentTab: state.navTabs.currentTopTab
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({ setItemData }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(CategoriesTab);
