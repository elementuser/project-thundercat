import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../../text_resources";
import { bindActionCreators } from "redux";
import GenericTable, { COMMON_STYLE } from "../../commons/GenericTable";
import SearchBarWithDisplayOptions from "../../commons/SearchBarWithDisplayOptions";
import ReactPaginate from "react-paginate";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faBinoculars,
  faCaretLeft,
  faCaretRight,
  faPoll,
  faSpinner
} from "@fortawesome/free-solid-svg-icons";
import CustomButton, { THEME } from "../../commons/CustomButton";
import StyledTooltip, { TYPE, EFFECT, TRIGGER_TYPE } from "../../authentication/StyledTooltip";
import {
  getAllItemBanks,
  getFoundItemBanks,
  getSelectedItemBankData,
  updateItemBanksPageState,
  updateItemBanksPageSizeState,
  updateSearchItemBanksStates,
  setItemBankData,
  setItemBankSideNavState,
  getAllItems,
  setItemBankAttributesInitialData,
  setItemBankInitialData
} from "../../../modules/ItemBankRedux";
import { history } from "../../../store-index";
import { PATH } from "../../commons/Constants";
import { PERMISSION } from "../../profile/Constants";
import { CSVLink } from "react-csv";
import generateItemBankReport from "../../commons/reports/GenerateItemBankReport";
import { getLanguageData } from "../../../modules/LocalizeRedux";
import * as _ from "lodash";
import { triggerAppRerender } from "../../../modules/AppRedux";

const styles = {
  tableContainer: {
    margin: 24
  },
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  disabledActionButton: {
    // default disabled color
    color: "#DDDDDD"
  },
  paginationContainer: {
    display: "flex"
  },
  paginationIcon: {
    padding: "0 6px"
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  allUnset: {
    all: "unset"
  }
};

class AllItemBanks extends Component {
  constructor(props) {
    super(props);
    this.createItemBankReportLinkButtonRef = React.createRef();
  }

  state = {
    currentlyLoading: false,
    resultsFound: 0,
    rowsDefinition: {},
    numberOfItemBanksPages: 1,
    formattedItemBankReportData: [],
    itemBankReportName: ""
  };

  componentDidMount = () => {
    this.props.updateItemBanksPageState(1);
    this.getAllItemBanksBasedOnActiveSearch();
    // triggering app rerender to make sure that the special page height and overflow is properly set
    this.props.triggerAppRerender();
  };

  componentDidUpdate = prevProps => {
    // if currentLanguage gets updated
    if (prevProps.currentLanguage !== this.props.currentLanguage) {
      this.getAllItemBanksBasedOnActiveSearch();
    }
    // if itemBanksPaginationPageSize get updated
    if (prevProps.itemBanksPaginationPageSize !== this.props.itemBanksPaginationPageSize) {
      this.getAllItemBanksBasedOnActiveSearch();
    }
    // if testDefinitionAllTestsPaginationPage get updated
    if (prevProps.itemBanksPaginationPage !== this.props.itemBanksPaginationPage) {
      this.getAllItemBanksBasedOnActiveSearch();
    }
    // if search itemBanksKeyword gets updated
    if (prevProps.itemBanksKeyword !== this.props.itemBanksKeyword) {
      // if itemBanksKeyword redux state is empty
      if (this.props.itemBanksKeyword !== "") {
        this.getAllItemBanksBasedOnActiveSearch();
      } else if (this.props.itemBanksKeyword === "" && this.props.itemBanksActiveSearch) {
        this.getAllItemBanksBasedOnActiveSearch();
      }
    }
    // if itemBanksActiveSearch gets updated
    if (prevProps.itemBanksActiveSearch !== this.props.itemBanksActiveSearch) {
      // there is no current active search (on clear search action)
      if (!this.props.itemBanksActiveSearch) {
        this.getAllItemBanksData();
        // there is a current active search (on search action)
      } else {
        this.getFoundItemBanksData();
      }
    }
  };

  // get item banks data based on the itemBanksActiveSearch redux state
  getAllItemBanksBasedOnActiveSearch = (triggerLoading = true) => {
    // current search
    if (this.props.itemBanksActiveSearch) {
      this.getFoundItemBanksData(triggerLoading);
      // no current search
    } else {
      this.getAllItemBanksData(triggerLoading);
    }
  };

  getAllItemBanksData = triggerLoading => {
    this.setState({ currentlyLoading: triggerLoading }, () => {
      // initializing needed object and array for rowsDefinition (needed for GenericTable component)
      let rowsDefinition = {};
      const data = [];
      this.props
        .getAllItemBanks(
          PERMISSION.testDeveloper,
          this.props.itemBanksPaginationPage,
          this.props.itemBanksPaginationPageSize
        )
        .then(response => {
          for (let i = 0; i < response.results.length; i++) {
            // populating data object with provided data
            data.push({
              column_1: response.results[i].custom_item_bank_id,
              column_2: response.results[i][`${this.props.currentLanguage}_name`],
              column_3: this.populateColumnThree(i, response)
            });
          }

          // updating rowsDefinition object with provided data and needed style
          rowsDefinition = {
            column_1_style: COMMON_STYLE.LEFT_TEXT,
            column_2_style: COMMON_STYLE.LEFT_TEXT,
            column_3_style: COMMON_STYLE.CENTERED_TEXT,
            data: data
          };
          // saving results in state
          this.setState({
            rowsDefinition: rowsDefinition,
            currentlyLoading: false,
            numberOfItemBanksPages: Math.ceil(
              parseInt(response.count) / this.props.itemBanksPaginationPageSize
            )
          });
        });
    });
  };

  getFoundItemBanksData = triggerLoading => {
    this.setState({ currentlyLoading: triggerLoading }, () => {
      // initializing needed object and array for rowsDefinition (needed for GenericTable component)
      let rowsDefinition = {};
      const data = [];
      setTimeout(() => {
        this.props
          .getFoundItemBanks(
            this.props.itemBanksKeyword,
            this.props.currentLanguage,
            PERMISSION.testDeveloper,
            this.props.itemBanksPaginationPage,
            this.props.itemBanksPaginationPageSize
          )
          .then(response => {
            // there are no results found
            if (response[0] === "no results found") {
              this.setState(
                {
                  rowsDefinition: {},
                  numberOfItemBanksPages: 1,
                  resultsFound: 0
                },
                () => {
                  this.setState({ currentlyLoading: false }, () => {
                    // make sure that this element exsits to avoid any error
                    if (document.getElementById("select-item-bank-results-found")) {
                      document.getElementById("select-item-bank-results-found").focus();
                    }
                  });
                }
              );
              // there is at least one result found
            } else {
              for (let i = 0; i < response.results.length; i++) {
                // populating data object with provided data
                data.push({
                  column_1: response.results[i].custom_item_bank_id,
                  column_2: response.results[i][`${this.props.currentLanguage}_name`],
                  column_3: this.populateColumnThree(i, response)
                });
              }

              // updating rowsDefinition object with provided data and needed style
              rowsDefinition = {
                column_1_style: COMMON_STYLE.LEFT_TEXT,
                column_2_style: COMMON_STYLE.LEFT_TEXT,
                column_3_style: COMMON_STYLE.CENTERED_TEXT,
                data: data
              };
              // saving results in state
              this.setState(
                {
                  rowsDefinition: rowsDefinition,
                  currentlyLoading: false,
                  numberOfItemBanksPages: Math.ceil(
                    parseInt(response.count) / this.props.itemBanksPaginationPageSize
                  ),
                  resultsFound: response.count
                },
                () => {
                  // make sure that this element exsits to avoid any error
                  if (document.getElementById("select-item-bank-results-found")) {
                    document.getElementById("select-item-bank-results-found").focus();
                  }
                }
              );
            }
          });
      }, 100);
    });
  };

  populateColumnThree = (i, response) => {
    return (
      <div className="d-flex justify-content-center flex-nowrap">
        <StyledTooltip
          id={`item-bank-view-button-${i}`}
          place="top"
          type={TYPE.light}
          effect={EFFECT.solid}
          triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
          tooltipElement={
            <div style={styles.allUnset}>
              <CustomButton
                dataTip=""
                dataFor={`item-bank-view-button-${i}`}
                label={<FontAwesomeIcon icon={faBinoculars} />}
                action={() => {
                  this.viewSelectedItemBank(response.results[i]);
                }}
                customStyle={styles.actionButton}
                buttonTheme={THEME.SECONDARY}
                disabled={response.results[i].action_buttons_disabled}
              />
            </div>
          }
          tooltipContent={
            <div>
              <p>
                {
                  LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBanks.table
                    .viewButtonTooltip
                }
              </p>
            </div>
          }
        />
        <StyledTooltip
          id={`item-bank-download-report-button-${i}`}
          place="top"
          type={TYPE.light}
          effect={EFFECT.solid}
          triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
          tooltipElement={
            <div style={styles.allUnset}>
              <CustomButton
                dataTip=""
                dataFor={`item-bank-download-report-button-${i}`}
                label={<FontAwesomeIcon icon={faPoll} />}
                action={() => {
                  this.downloadItemBankReport(i, response.results[i]);
                }}
                customStyle={styles.actionButton}
                buttonTheme={THEME.SECONDARY}
                disabled={response.results[i].action_buttons_disabled}
              />
            </div>
          }
          tooltipContent={
            <div>
              <p>
                {
                  LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBanks.table
                    .downloadReportButtonTooltip
                }
              </p>
            </div>
          }
        />
      </div>
    );
  };

  triggerItemBankActionLoading = index => {
    const { rowsDefinition } = this.state;
    const tempRowsDefinition = rowsDefinition;
    for (let i = 0; i < rowsDefinition.data.length; i++) {
      // provided index is the same as the current index
      if (i === index) {
        tempRowsDefinition.data[i].column_3 = (
          // eslint-disable-next-line jsx-a11y/label-has-associated-control
          <label className="fa fa-spinner fa-spin">
            <FontAwesomeIcon icon={faSpinner} />
          </label>
        );
        // other item bank than the one selected
      } else {
        tempRowsDefinition.data[i].column_3 = (
          <div>
            <CustomButton
              label={<FontAwesomeIcon icon={faBinoculars} />}
              customStyle={{
                ...styles.actionButton,
                ...styles.disabledActionButton
              }}
              disabled={true}
            />
            <CustomButton
              label={<FontAwesomeIcon icon={faPoll} />}
              customStyle={{
                ...styles.actionButton,
                ...styles.disabledActionButton
              }}
              disabled={true}
            />
          </div>
        );
      }
    }
    this.setState({ rowsDefinition: tempRowsDefinition });
  };

  viewSelectedItemBank = currentSelectedItemBank => {
    this.props
      .getSelectedItemBankData(currentSelectedItemBank.custom_item_bank_id)
      .then(response => {
        const item_bank_data = {
          item_bank_definition: response.item_bank_definition,
          item_bank_attributes: response.item_bank_attributes,
          item_bank_items: [],
          item_bank_number_of_found_items: 0,
          list_of_bundles: []
        };
        this.props.setItemBankData(_.cloneDeep(item_bank_data));

        // Set the values for the initial data of the Item Bank + Item Bank Attributes
        // These are used to detect changes on multiple components of Item Bank
        this.props.setItemBankAttributesInitialData(
          _.cloneDeep(item_bank_data.item_bank_attributes)
        );

        // Set the value for the initial data of the Item Bank
        // This is used to detect changes on multiple components of Item Bank
        this.props.setItemBankInitialData(_.cloneDeep(item_bank_data));

        // resetting side nav redux state
        this.props.setItemBankSideNavState(0);
      })
      .then(() => {
        history.push(PATH.itemBankEditor);
      });
  };

  downloadItemBankReport = (index, currentSelectedItemBank) => {
    // initializing formattedItemBankReportData
    let formattedItemBankReportData = [];
    // getting item bank language for the report's name
    let itemBankLanguage = "B";
    // unilangual item bank
    if (currentSelectedItemBank.language !== null) {
      // getting first letter of language in English (example: E, F, ...)
      itemBankLanguage = currentSelectedItemBank.language_data.language_text.en[0].text.charAt(0);
    }
    // trigger action loading
    this.triggerItemBankActionLoading(index);
    // getting all item bank items
    this.props
      // page and page size here doesn't matter, since we're interested on the "all_items" part from the response
      // last parameter set to true, so we're ignoring drafts
      .getAllItems(currentSelectedItemBank.id, 1, 25, true)
      .then(response => {
        // getting language data
        this.props.getLanguageData().then(languageDataResonse => {
          // getting formatted data
          formattedItemBankReportData = generateItemBankReport(
            response.all_items,
            languageDataResonse
          );
          this.setState(
            {
              formattedItemBankReportData: formattedItemBankReportData,
              itemBankReportName: LOCALIZE.formatString(
                LOCALIZE.reports.itemBankReport.name,
                itemBankLanguage,
                currentSelectedItemBank.custom_item_bank_id
              )
            },
            () => {
              // once all data has been created, simulate click on hidden create report link to generate the downloadable csv file
              this.createItemBankReportLinkButtonRef.current.link.click();
            }
          );
        });
      })
      .then(() => {
        // repopulating table data (without loading logic)
        this.getAllItemBanksBasedOnActiveSearch(false);
      });
  };

  handlePageChange = id => {
    // "+1" because redux itemBanksPaginationPage uses index 1 and react-paginate (id) uses index 0
    const selectedPage = id.selected + 1;
    // update current page redux state
    this.props.updateItemBanksPageState(selectedPage);
    // focusing on the first table's row (make sure that the row exists before trying to focus to avoid errors)
    if (document.getElementById("all-item-banks-table-row-0")) {
      document.getElementById("all-item-banks-table-row-0").focus();
    }
  };

  render() {
    const columnsDefinition = [
      {
        label: LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBanks.table.column1,
        style: { ...{ width: "30%" }, ...COMMON_STYLE.LEFT_TEXT }
      },
      {
        label: LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBanks.table.column2,
        style: { ...{ width: "50%" }, ...COMMON_STYLE.LEFT_TEXT }
      },
      {
        label: LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBanks.table.column3,
        style: { ...{ width: "20%" }, ...COMMON_STYLE.CENTERED_TEXT }
      }
    ];

    return (
      <div>
        <h2>{LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBanks.title}</h2>
        <div style={styles.tableContainer}>
          <div>
            <SearchBarWithDisplayOptions
              idPrefix={"select-item-bank"}
              currentlyLoading={this.state.currentlyLoading}
              updateSearchStates={this.props.updateSearchItemBanksStates}
              updatePageState={this.props.updateItemBanksPageState}
              paginationPageSize={this.props.itemBanksPaginationPageSize}
              updatePaginationPageSize={this.props.updateItemBanksPageSizeState}
              data={this.state.rowsDefinition || []}
              resultsFound={this.state.resultsFound}
              searchBarContent={this.props.itemBanksKeyword}
            />
            <GenericTable
              classnamePrefix="all-item-banks"
              columnsDefinition={columnsDefinition}
              rowsDefinition={this.state.rowsDefinition}
              emptyTableMessage={
                LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBanks.table.noData
              }
              currentlyLoading={this.state.currentlyLoading}
            />
            <div style={styles.paginationContainer}>
              <ReactPaginate
                pageCount={this.state.numberOfItemBanksPages}
                containerClassName={"pagination"}
                breakClassName={"break"}
                activeClassName={"active-page"}
                marginPagesDisplayed={3}
                // "-1" because react-paginate uses index 0 and itemBanksPaginationPage redux state uses index 1
                forcePage={this.props.itemBanksPaginationPage - 1}
                onPageChange={page => {
                  this.handlePageChange(page);
                }}
                previousLabel={
                  <div style={styles.paginationIcon}>
                    <label style={styles.hiddenText}>
                      {LOCALIZE.commons.pagination.previousPageButton}
                    </label>
                    <FontAwesomeIcon icon={faCaretLeft} />
                  </div>
                }
                nextLabel={
                  <div style={styles.paginationIcon}>
                    <label style={styles.hiddenText}>
                      {LOCALIZE.commons.pagination.nextPageButton}
                    </label>
                    <FontAwesomeIcon icon={faCaretRight} />
                  </div>
                }
              />
            </div>
          </div>
        </div>
        <CSVLink
          ref={this.createItemBankReportLinkButtonRef}
          asyncOnClick={true}
          data={this.state.formattedItemBankReportData}
          filename={this.state.itemBankReportName}
          enclosingCharacter=""
        ></CSVLink>
      </div>
    );
  }
}

export { AllItemBanks as unconnectedAllItemBanks };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    itemBanksPaginationPage: state.itemBank.itemBanksPaginationPage,
    itemBanksPaginationPageSize: state.itemBank.itemBanksPaginationPageSize,
    itemBanksKeyword: state.itemBank.itemBanksKeyword,
    itemBanksActiveSearch: state.itemBank.itemBanksActiveSearch
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAllItemBanks,
      getFoundItemBanks,
      getSelectedItemBankData,
      updateItemBanksPageState,
      updateItemBanksPageSizeState,
      updateSearchItemBanksStates,
      setItemBankData,
      setItemBankInitialData,
      setItemBankAttributesInitialData,
      setItemBankSideNavState,
      getAllItems,
      getLanguageData,
      triggerAppRerender
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(AllItemBanks);
