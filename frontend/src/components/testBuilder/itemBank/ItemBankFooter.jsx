/* eslint-disable no-undef */
import React, { Component } from "react";
import * as _ from "lodash";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Button, Navbar } from "react-bootstrap";
import CustomButton, { THEME } from "../../commons/CustomButton";
import LOCALIZE from "../../../text_resources";
import {
  faBinoculars,
  faCheck,
  faExclamationTriangle,
  faPlay,
  faSave,
  faSpinner,
  faTimes,
  faUpload
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import PopupBox, { BUTTON_STATE, BUTTON_TYPE } from "../../commons/PopupBox";
import SystemMessage, { MESSAGE_TYPE } from "../../commons/SystemMessage";
import GenericTable, { COMMON_STYLE } from "../../commons/GenericTable";
import objectEqualsCheck, { arrayEqualsCheck } from "../../../helpers/objectHelpers";
import { getItemData, setUploadItemDataParameters } from "../../../modules/ItemBankRedux";
import { LANGUAGES } from "../../commons/Translation";
import StyledTooltip, { EFFECT, TRIGGER_TYPE, TYPE } from "../../authentication/StyledTooltip";
import getCheckboxTransformScale from "../../../helpers/checkboxTransformScale";

export const VERSION_CONTROL_RADIO_BUTTON = {
  uploadNewVersion: "item-modifications-new-version-radio-button",
  overwriteVersion: "item-modifications-overwrite-version-radio-button"
};

const styles = {
  footer: {
    borderTop: "1px solid #96a8b2",
    backgroundColor: "#D5DEE0"
  },
  content: {
    margin: "0 auto"
  },
  footerIndex: {
    zIndex: 1
  },
  span: {
    paddingLeft: 5,
    paddingRight: 5
  },
  saveButtonContainer: {
    left: 0,
    position: "absolute",
    marginLeft: 24
  },
  saveButton: {
    minWidth: 150
  },
  previousButton: {
    minWidth: 50,
    padding: 6,
    marginRight: 48
  },
  nextButton: {
    minWidth: 50,
    padding: 6,
    marginLeft: 48
  },
  floatLeft: {
    float: "left"
  },
  uploadButtonContainer: {
    right: 0,
    position: "absolute",
    marginRight: 24
  },
  uploadButton: {
    minWidth: 150
  },
  bold: {
    fontWeight: "bold"
  },
  popupDescriptionContainer: {
    margin: "24px auto",
    width: "60%"
  },
  changedIcon: {
    color: "#278400",
    transform: "scale(1.5)"
  },
  warningsIcon: {
    color: "#CC0000",
    transform: "scale(1.5)"
  },
  tooltipButton: {
    padding: "0 6px"
  },
  ul: {
    textAlign: "left",
    margin: 6,
    paddingLeft: 12
  },
  li: {
    padding: 0,
    margin: 0
  },
  radioButtonsContainer: {
    width: "80%",
    margin: "24px auto 0 auto"
  },
  radioButtonLabel: {
    paddingLeft: 12
  },
  radioButton: {
    marginLeft: 15,
    verticalAlign: "middle"
  },
  checkboxContainer: {
    paddingTop: 24,
    display: "table-cell"
  },
  tableCellMiddle: {
    display: "table-cell",
    verticalAlign: "middle"
  },
  checkboxLabel: {
    paddingLeft: 12
  },
  checkbox: {
    marginLeft: 15
  }
};

class ItemBankFooter extends Component {
  constructor(props, context) {
    super(props, context);
    this.PropTypes = {
      previewQuestion: PropTypes.func,
      previousAction: PropTypes.func,
      nextAction: PropTypes.func,
      hideSaveButton: PropTypes.bool,
      saveAction: PropTypes.func,
      uploadAction: PropTypes.func,
      triggerUploadPopup: PropTypes.func,
      disabledPreviousButton: PropTypes.bool,
      disabledNextButton: PropTypes.bool
    };

    ItemBankFooter.defaultProps = {
      hideSaveButton: false
    };
  }

  state = {
    dataChangesDetected: this.props.dataChangesDetected,
    dataReadyForUpload: this.props.dataReadyForUpload,
    validBundleItemsBasicRuleState: this.props.validBundleItemsBasicRuleState,
    validBundleBundlesRuleState: this.props.validBundleBundlesRuleState,
    showUploadConfirmationPopup: false,
    showUploadedSuccessfullyPopup: false,
    uploadLoading: false,
    newVersionUploaded: "",
    rowsDefinition: {},
    currentlyLoading: true,
    itemBankLanguageData: { langauge_id: null, language_code: null },
    selectedRadioButtonOption: null,
    containsWarnings: false,
    reviewedWarningsCheckboxChecked: true
  };

  componentDidMount = () => {
    this.getItemBankLanguageData();
  };

  componentDidUpdate = prevProps => {
    // if dataChangesDetected gets updated
    if (prevProps.dataChangesDetected !== this.props.dataChangesDetected) {
      this.setState({ dataChangesDetected: this.props.dataChangesDetected });
    }
    // if dataReadyForUpload gets updated
    if (prevProps.dataReadyForUpload !== this.props.dataReadyForUpload) {
      this.setState({ dataReadyForUpload: this.props.dataReadyForUpload });
    }
    // if validBundleItemsBasicRuleState gets updated
    if (prevProps.validBundleItemsBasicRuleState !== this.props.validBundleItemsBasicRuleState) {
      this.setState({ validBundleItemsBasicRuleState: this.props.validBundleItemsBasicRuleState });
    }
    // if validBundleBundlesRuleState gets updated
    if (prevProps.validBundleBundlesRuleState !== this.props.validBundleBundlesRuleState) {
      this.setState({ validBundleBundlesRuleState: this.props.validBundleBundlesRuleState });
    }
    // if itemData gets updated and popup is opened
    if (prevProps.itemData !== this.props.itemData && this.state.showUploadConfirmationPopup) {
      this.setState({
        showUploadedSuccessfullyPopup: true,
        uploadLoading: false,
        newVersionUploaded: this.props.itemData.version
      });
    }
  };

  getItemBankLanguageData = () => {
    // getting item bank language data
    const itemBankLanguageId = this.props.item_bank_data.item_bank_definition[0].language_id;
    let itemBankLanguageData = { language_id: null, language_code: null };
    // looping in languageData
    for (let i = 0; i < this.props.languageData.length; i++) {
      // language_id of current iteration matched the itemBankLanguageId
      if (this.props.languageData[i].language_id === itemBankLanguageId) {
        itemBankLanguageData = {
          language_id: itemBankLanguageId,
          language_code: this.props.languageData[i].ISO_Code_1
        };
        break;
      }
    }
    this.setState({ itemBankLanguageData: itemBankLanguageData });
  };

  openUploadConfirmationPopup = () => {
    this.populateTableRows();
    this.setState({ showUploadConfirmationPopup: true, newVersionUploaded: "" });
  };

  closeUploadConfirmationPopup = () => {
    this.setState({ showUploadConfirmationPopup: false, newVersionUploaded: "" }, () => {
      setTimeout(() => {
        this.setState({
          showUploadedSuccessfullyPopup: false,
          uploadLoading: false,
          rowsDefinition: {},
          selectedRadioButtonOption: null,
          containsWarnings: false,
          reviewedWarningsCheckboxChecked: true
        });
      }, 250);
    });
  };

  // populating table rows for the GenericTable component
  populateTableRows = () => {
    this.setState({ currentlyLoading: true }, () => {
      // creating item elements array
      const itemElements = [];
      itemElements.push(
        LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
          .uploadConfirmationPopup.itemModificationsTable.itemElements.stemSections
      );
      itemElements.push(
        LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
          .uploadConfirmationPopup.itemModificationsTable.itemElements.options
      );
      itemElements.push(
        LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
          .uploadConfirmationPopup.itemModificationsTable.itemElements.categories
      );
      itemElements.push(
        LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
          .uploadConfirmationPopup.itemModificationsTable.itemElements.historicalId
      );
      itemElements.push(
        LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
          .uploadConfirmationPopup.itemModificationsTable.itemElements.itemStatus
      );
      itemElements.push(
        LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
          .uploadConfirmationPopup.itemModificationsTable.itemElements.responseFormat
      );
      itemElements.push(
        LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
          .uploadConfirmationPopup.itemModificationsTable.itemElements.scoringFormat
      );
      itemElements.push(
        LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
          .uploadConfirmationPopup.itemModificationsTable.itemElements.shuffleOptions
      );

      // getting item data based on itemData item_id (item where the draft has been based on)
      this.props.getItemData(this.props.itemData.item_id, true).then(response => {
        // initializing needed object and array for rowsDefinition state (needed for GenericTable component)
        let rowsDefinition = {};
        const data = [];
        for (let i = 0; i < itemElements.length; i++) {
          // pushing needed data in data array (needed for GenericTable component)
          data.push({
            column_1: itemElements[i],
            column_2: this.getItemElementChangesStatus(i, response),
            column_3: this.getItemElementWarningsStatus(i)
          });
        }

        // updating rowsDefinition object with provided data and needed style
        rowsDefinition = {
          column_1_style: {},
          column_2_style: COMMON_STYLE.CENTERED_TEXT,
          column_3_style: COMMON_STYLE.CENTERED_TEXT,
          data: data
        };

        this.setState({
          rowsDefinition: rowsDefinition,
          currentlyLoading: false
        });
      });
    });
  };

  getItemElementChangesStatus = (i, itemDataFromDB) => {
    switch (i) {
      // ref: itemElements array from populateTableRows function
      // Stem Sections
      case 0:
        // creating deep copy of copyOfItemData and copyOfItemDataFromDb
        const copyOfItemDataStems = _.cloneDeep(this.props.itemData.item_content);
        const copyOfItemDataStemsFromDb = _.cloneDeep(itemDataFromDB.item_content);
        // looping in all available languages
        for (let i = 0; i < Object.entries(LANGUAGES).length; i++) {
          // ITEM DATA
          // item_content is defined in language of current iteration
          if (
            typeof copyOfItemDataStems[Object.entries(LANGUAGES)[i][1]] !== "undefined" &&
            copyOfItemDataStems[Object.entries(LANGUAGES)[i][1]].length > 0
          ) {
            for (let j = 0; j < copyOfItemDataStems[Object.entries(LANGUAGES)[i][1]].length; j++) {
              // removing unwanted attributes for the comparison
              delete copyOfItemDataStems[Object.entries(LANGUAGES)[i][1]][j].id;
              delete copyOfItemDataStems[Object.entries(LANGUAGES)[i][1]][j].item_content_id;
              delete copyOfItemDataStems[Object.entries(LANGUAGES)[i][1]][j]
                .item_content_modify_date;
              delete copyOfItemDataStems[Object.entries(LANGUAGES)[i][1]][j].item_content_text_id;
              delete copyOfItemDataStems[Object.entries(LANGUAGES)[i][1]][j]
                .item_content_text_modify_date;
              delete copyOfItemDataStems[Object.entries(LANGUAGES)[i][1]][j].system_id;
              delete copyOfItemDataStems[Object.entries(LANGUAGES)[i][1]][j].username_id;
            }
          }
          // ITEM DATA FROM DB
          // item_content is defined in language of current iteration
          if (
            typeof copyOfItemDataStemsFromDb[Object.entries(LANGUAGES)[i][1]] !== "undefined" &&
            copyOfItemDataStemsFromDb[Object.entries(LANGUAGES)[i][1]].length > 0
          ) {
            for (
              let j = 0;
              j < copyOfItemDataStemsFromDb[Object.entries(LANGUAGES)[i][1]].length;
              j++
            ) {
              // removing unwanted attributes for the comparison
              delete copyOfItemDataStemsFromDb[Object.entries(LANGUAGES)[i][1]][j].id;
              delete copyOfItemDataStemsFromDb[Object.entries(LANGUAGES)[i][1]][j].item_content_id;
              delete copyOfItemDataStemsFromDb[Object.entries(LANGUAGES)[i][1]][j]
                .item_content_modify_date;
              delete copyOfItemDataStemsFromDb[Object.entries(LANGUAGES)[i][1]][j]
                .item_content_text_id;
              delete copyOfItemDataStemsFromDb[Object.entries(LANGUAGES)[i][1]][j]
                .item_content_text_modify_date;
            }
          }
        }
        return !objectEqualsCheck(copyOfItemDataStems, copyOfItemDataStemsFromDb) ? (
          <FontAwesomeIcon icon={faCheck} style={styles.changedIcon} />
        ) : (
          ""
        );
      // Options
      case 1:
        // creating deep copy of copyOfItemData and copyOfItemDataFromDb
        const copyOfItemDataOptions = _.cloneDeep(this.props.itemData.item_options);
        const copyOfItemDataOptionsFromDb = _.cloneDeep(itemDataFromDB.item_options);
        // looping in all available languages
        for (let i = 0; i < Object.entries(LANGUAGES).length; i++) {
          // ITEM DATA
          // item_options is defined in language of current iteration
          if (
            typeof copyOfItemDataOptions[Object.entries(LANGUAGES)[i][1]] !== "undefined" &&
            copyOfItemDataOptions[Object.entries(LANGUAGES)[i][1]].length > 0
          ) {
            for (
              let j = 0;
              j < copyOfItemDataOptions[Object.entries(LANGUAGES)[i][1]].length;
              j++
            ) {
              // removing unwanted attributes for the comparison
              delete copyOfItemDataOptions[Object.entries(LANGUAGES)[i][1]][j].id;
              delete copyOfItemDataOptions[Object.entries(LANGUAGES)[i][1]][j].item_option_id;
              delete copyOfItemDataOptions[Object.entries(LANGUAGES)[i][1]][j]
                .item_option_modify_date;
              delete copyOfItemDataOptions[Object.entries(LANGUAGES)[i][1]][j].item_option_text_id;
              delete copyOfItemDataOptions[Object.entries(LANGUAGES)[i][1]][j]
                .item_option_text_modify_date;
              delete copyOfItemDataOptions[Object.entries(LANGUAGES)[i][1]][j].system_id;
              delete copyOfItemDataOptions[Object.entries(LANGUAGES)[i][1]][j].username_id;
            }
          }
          // ITEM DATA FROM DB
          // item_options is defined in language of current iteration
          if (
            typeof copyOfItemDataOptionsFromDb[Object.entries(LANGUAGES)[i][1]] !== "undefined" &&
            copyOfItemDataOptionsFromDb[Object.entries(LANGUAGES)[i][1]].length > 0
          ) {
            for (
              let j = 0;
              j < copyOfItemDataOptionsFromDb[Object.entries(LANGUAGES)[i][1]].length;
              j++
            ) {
              // removing unwanted attributes for the comparison
              delete copyOfItemDataOptionsFromDb[Object.entries(LANGUAGES)[i][1]][j].id;
              delete copyOfItemDataOptionsFromDb[Object.entries(LANGUAGES)[i][1]][j].item_option_id;
              delete copyOfItemDataOptionsFromDb[Object.entries(LANGUAGES)[i][1]][j]
                .item_option_modify_date;
              delete copyOfItemDataOptionsFromDb[Object.entries(LANGUAGES)[i][1]][j]
                .item_option_text_id;
              delete copyOfItemDataOptionsFromDb[Object.entries(LANGUAGES)[i][1]][j]
                .item_option_text_modify_date;
            }
          }
        }
        return !objectEqualsCheck(copyOfItemDataOptions, copyOfItemDataOptionsFromDb) ? (
          <FontAwesomeIcon icon={faCheck} style={styles.changedIcon} />
        ) : (
          ""
        );
      // Categories
      case 2:
        // creating deep copy of copyOfItemData and copyOfItemDataFromDb
        const copyOfItemDataCategories = _.cloneDeep(this.props.itemData.item_attribute_values);
        const copyOfItemDataCategoriesFromDb = _.cloneDeep(itemDataFromDB.item_attribute_values);

        // Reference: https://stackoverflow.com/questions/48639336/check-if-two-arrays-contain-identical-objects-react-componentdidupdate
        return _.isEqual(copyOfItemDataCategories, copyOfItemDataCategoriesFromDb) ? (
          ""
        ) : (
          <FontAwesomeIcon icon={faCheck} style={styles.changedIcon} />
        );

      // Historical ID
      case 3:
        const copyOfItemDataForHistoricalId = { ...this.props.itemData };
        const copyOfItemDataForHistoricalIdFromDb = { ...itemDataFromDB };
        // initializing historical ID from DB
        let historicalIdFromDb = copyOfItemDataForHistoricalIdFromDb.historical_id;
        // historical ID from DB is null
        if (copyOfItemDataForHistoricalIdFromDb.historical_id === null) {
          // setting historicalIdFromDb to "", so the comparison is equivalent to the frontend value
          historicalIdFromDb = "";
        }
        return copyOfItemDataForHistoricalId.historical_id !== historicalIdFromDb ? (
          <FontAwesomeIcon icon={faCheck} style={styles.changedIcon} />
        ) : (
          ""
        );
      // Item Status
      case 4:
        const copyOfItemDataForItemStatus = { ...this.props.itemData };
        const copyOfItemDataForItemStatusFromDb = { ...itemDataFromDB };
        return copyOfItemDataForItemStatus.development_status_id !==
          copyOfItemDataForItemStatusFromDb.development_status_id ? (
          <FontAwesomeIcon icon={faCheck} style={styles.changedIcon} />
        ) : (
          ""
        );
      // Response Format
      case 5:
        const copyOfItemDataForResponseFormat = { ...this.props.itemData };
        const copyOfItemDataForResponseFormatFromDb = { ...itemDataFromDB };
        return copyOfItemDataForResponseFormat.response_format_id !==
          copyOfItemDataForResponseFormatFromDb.response_format_id ? (
          <FontAwesomeIcon icon={faCheck} style={styles.changedIcon} />
        ) : (
          ""
        );

      // Scoring Format (state does not exist yet --> TODO)
      // case 6:
      //   const copyOfItemDataForScoringFormat = { ...this.props.itemData };
      //   const copyOfItemDataForScoringFormatFromDb = { ...itemDataFromDB };
      //   return copyOfItemDataForScoringFormat.scoring_format_id !==
      //     copyOfItemDataForScoringFormatFromDb.scoring_format_id ? (
      //     <FontAwesomeIcon icon={faCheck} style={styles.changedIcon} />
      //   ) : (
      //     ""
      //   );

      // Shuffle Options
      case 7:
        const copyOfItemDataForShuffleOptions = { ...this.props.itemData };
        const copyOfItemDataForShuffleOptionsFromDb = { ...itemDataFromDB };
        return copyOfItemDataForShuffleOptions.shuffle_options !==
          copyOfItemDataForShuffleOptionsFromDb.shuffle_options ? (
          <FontAwesomeIcon icon={faCheck} style={styles.changedIcon} />
        ) : (
          ""
        );
      default:
        return false;
    }
  };

  getItemElementWarningsStatus = i => {
    switch (i) {
      // Stem Sections
      case 0:
        // initializing warning variables
        let stemsNotDefined = false;
        let containsBlankStems = false;

        // creating deep copy of copyOfItemData
        const copyOfItemDataStems = _.cloneDeep(this.props.itemData.item_content);

        // ========== STEMS VALIDATION ====================
        // item bank language is defined (not bilingual)
        if (this.state.itemBankLanguageData.language_code !== null) {
          // looping in all available languages
          for (let i = 0; i < Object.entries(LANGUAGES).length; i++) {
            // language_code is the same as the current iteration
            if (Object.entries(LANGUAGES)[i][1] === this.state.itemBankLanguageData.language_code) {
              // stems not defined in item bank language
              if (
                typeof copyOfItemDataStems[this.state.itemBankLanguageData.language_code] ===
                  "undefined" ||
                copyOfItemDataStems[this.state.itemBankLanguageData.language_code].length <= 0
              ) {
                stemsNotDefined = true;
                break;
              } else {
                // looping in item stems
                for (
                  let j = 0;
                  j < copyOfItemDataStems[this.state.itemBankLanguageData.language_code].length;
                  j++
                ) {
                  // blank stem
                  if (
                    copyOfItemDataStems[this.state.itemBankLanguageData.language_code][j].text ===
                    ""
                  ) {
                    containsBlankStems = true;
                    break;
                  }
                }
              }
            }
          }
          // item bank language is not defined (bilingual/multilingual)
        } else {
          // looping in all available languages
          for (let i = 0; i < Object.entries(LANGUAGES).length; i++) {
            // stems not defined in current iteration language
            if (
              typeof copyOfItemDataStems[Object.entries(LANGUAGES)[i][1]] === "undefined" ||
              copyOfItemDataStems[Object.entries(LANGUAGES)[i][1]].length <= 0
            ) {
              stemsNotDefined = true;
              break;
            } else {
              // looping in item stems
              for (
                let j = 0;
                j < copyOfItemDataStems[Object.entries(LANGUAGES)[i][1]].length;
                j++
              ) {
                // blank stem
                if (copyOfItemDataStems[Object.entries(LANGUAGES)[i][1]][j].text === "") {
                  containsBlankStems = true;
                  break;
                }
              }
            }
          }
        }
        // ========== STEMS VALIDATION (END) ====================

        if (stemsNotDefined || containsBlankStems) {
          this.setState({ containsWarnings: true, reviewedWarningsCheckboxChecked: false });
          return (
            <StyledTooltip
              id={`stem-warning-tooltip-${i}`}
              place="top"
              type={TYPE.light}
              effect={EFFECT.solid}
              triggerType={[TRIGGER_TYPE.hover]}
              tooltipElement={
                <Button
                  data-tip=""
                  data-for={`stem-warning-tooltip-${i}`}
                  style={styles.tooltipButton}
                  variant="link"
                >
                  <FontAwesomeIcon
                    icon={faExclamationTriangle}
                    style={styles.warningsIcon}
                  ></FontAwesomeIcon>
                </Button>
              }
              tooltipContent={
                <ul style={styles.ul}>
                  {stemsNotDefined && (
                    <li style={styles.li}>
                      {
                        LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                          .items.uploadConfirmationPopup.itemModificationsTable.warnings
                          .stemsNotDefined
                      }
                    </li>
                  )}
                  {containsBlankStems && (
                    <li style={styles.li}>
                      {
                        LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                          .items.uploadConfirmationPopup.itemModificationsTable.warnings
                          .containsBlankStems
                      }
                    </li>
                  )}
                </ul>
              }
            />
          );
        }
        return "";
      // Options
      case 1:
        // initializing warning variables
        let optionsNotDefined = false;
        let containsBlankOptions = false;

        // creating deep copy of copyOfItemData
        const copyOfItemDataOptions = _.cloneDeep(this.props.itemData.item_options);

        // ========== OPTIONS VALIDATION ====================
        // item bank language is defined (not bilingual)
        if (this.state.itemBankLanguageData.language_code !== null) {
          // looping in all available languages
          for (let i = 0; i < Object.entries(LANGUAGES).length; i++) {
            // language_code is the same as the current iteration
            if (Object.entries(LANGUAGES)[i][1] === this.state.itemBankLanguageData.language_code) {
              // options not defined in item bank language
              if (
                typeof copyOfItemDataOptions[this.state.itemBankLanguageData.language_code] ===
                  "undefined" ||
                copyOfItemDataOptions[this.state.itemBankLanguageData.language_code].length <= 0
              ) {
                optionsNotDefined = true;
                break;
              } else {
                // looping in item options
                for (
                  let j = 0;
                  j < copyOfItemDataOptions[this.state.itemBankLanguageData.language_code].length;
                  j++
                ) {
                  // blank option
                  if (
                    copyOfItemDataOptions[this.state.itemBankLanguageData.language_code][j].text ===
                    ""
                  ) {
                    containsBlankOptions = true;
                    break;
                  }
                }
              }
            }
          }
          // item bank language is not defined (bilingual/multilingual)
        } else {
          // looping in all available languages
          for (let i = 0; i < Object.entries(LANGUAGES).length; i++) {
            // options not defined in current iteration language
            if (
              typeof copyOfItemDataOptions[Object.entries(LANGUAGES)[i][1]] === "undefined" ||
              copyOfItemDataOptions[Object.entries(LANGUAGES)[i][1]].length <= 0
            ) {
              optionsNotDefined = true;
              break;
            } else {
              // looping in item options
              for (
                let j = 0;
                j < copyOfItemDataOptions[Object.entries(LANGUAGES)[i][1]].length;
                j++
              ) {
                // blank option
                if (copyOfItemDataOptions[Object.entries(LANGUAGES)[i][1]][j].text === "") {
                  containsBlankOptions = true;
                  break;
                }
              }
            }
          }
        }
        // ========== OPTIONS VALIDATION (END) ====================

        if (optionsNotDefined || containsBlankOptions) {
          this.setState({ containsWarnings: true, reviewedWarningsCheckboxChecked: false });
          return (
            <StyledTooltip
              id={`options-warning-tooltip-${i}`}
              place="top"
              type={TYPE.light}
              effect={EFFECT.solid}
              triggerType={[TRIGGER_TYPE.hover]}
              tooltipElement={
                <Button
                  data-tip=""
                  data-for={`options-warning-tooltip-${i}`}
                  style={styles.tooltipButton}
                  variant="link"
                >
                  <FontAwesomeIcon
                    icon={faExclamationTriangle}
                    style={styles.warningsIcon}
                  ></FontAwesomeIcon>
                </Button>
              }
              tooltipContent={
                <ul style={styles.ul}>
                  {optionsNotDefined && (
                    <li style={styles.li}>
                      {
                        LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                          .items.uploadConfirmationPopup.itemModificationsTable.warnings
                          .optionsNotDefined
                      }
                    </li>
                  )}
                  {containsBlankOptions && (
                    <li style={styles.li}>
                      {
                        LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                          .items.uploadConfirmationPopup.itemModificationsTable.warnings
                          .containsBlankOptions
                      }
                    </li>
                  )}
                </ul>
              }
            />
          );
        }
        return "";

      default:
        return "";
    }
  };

  handleRadioButtonClick = event => {
    // creating a copy of uploadItemDataParameters
    const copyOfUploadItemDataParameters = { ...this.props.uploadItemDataParameters };
    // updating versionControlParameter
    copyOfUploadItemDataParameters.versionControlParameter = event.target.id;
    // updating redux states
    this.props.setUploadItemDataParameters(copyOfUploadItemDataParameters);
    this.setState({ selectedRadioButtonOption: event.target.id });
  };

  handleUpload = () => {
    this.setState({ uploadLoading: true }, () => {
      this.props.uploadAction();
    });
  };

  render() {
    const columnsDefinition = [
      {
        label:
          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
            .uploadConfirmationPopup.itemModificationsTable.column1,
        style: { width: "50%" }
      },
      {
        label:
          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
            .uploadConfirmationPopup.itemModificationsTable.column2,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label:
          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
            .uploadConfirmationPopup.itemModificationsTable.column3,
        style: COMMON_STYLE.CENTERED_TEXT
      }
    ];

    const checkboxTransformScale = getCheckboxTransformScale(this.props.accommodations.fontSize);

    return (
      <div role="contentinfo" className="fixed-bottom" style={styles.footerIndex}>
        <Navbar id="item-bank-footer-main-div" fixed="bottom" style={styles.footer}>
          <div style={styles.content} id="unit-test-timer">
            <div style={styles.saveButtonContainer}>
              {!this.props.hideSaveButton && (
                <CustomButton
                  label={
                    <>
                      <FontAwesomeIcon icon={faSave} />
                      <span style={styles.span} className="notranslate">
                        {LOCALIZE.commons.saveDraftButton}
                      </span>
                    </>
                  }
                  action={this.props.saveAction}
                  buttonTheme={THEME.PRIMARY}
                  customStyle={styles.saveButton}
                  ariaLabel={LOCALIZE.commons.saveDraftButton}
                  disabled={!this.props.dataChangesDetected}
                ></CustomButton>
              )}
            </div>
            <div style={styles.floatLeft}>
              <CustomButton
                label={<FontAwesomeIcon icon={faPlay} rotation={180} />}
                action={this.props.previousAction}
                buttonTheme={THEME.SECONDARY}
                customStyle={styles.previousButton}
                ariaLabel={LOCALIZE.commons.previous}
                disabled={this.props.disabledPreviousButton}
              ></CustomButton>
              <CustomButton
                label={
                  <>
                    <FontAwesomeIcon icon={faBinoculars} />
                    <span style={styles.span} className="notranslate">
                      {LOCALIZE.commons.previewButton}
                    </span>
                  </>
                }
                action={this.props.previewQuestion}
                buttonTheme={THEME.SECONDARY}
                ariaLabel={LOCALIZE.commons.previewButton}
                disabled={
                  !this.state.validBundleItemsBasicRuleState ||
                  !this.state.validBundleBundlesRuleState
                }
              ></CustomButton>
              <CustomButton
                label={<FontAwesomeIcon icon={faPlay} />}
                action={this.props.nextAction}
                buttonTheme={THEME.SECONDARY}
                customStyle={styles.nextButton}
                ariaLabel={LOCALIZE.commons.next}
                disabled={this.props.disabledNextButton}
              ></CustomButton>
            </div>
            <div style={styles.uploadButtonContainer}>
              <CustomButton
                label={
                  <>
                    <FontAwesomeIcon icon={faUpload} />
                    <span style={styles.span} className="notranslate">
                      {LOCALIZE.commons.upload}
                    </span>
                  </>
                }
                action={
                  this.props.triggerUploadPopup
                    ? this.openUploadConfirmationPopup
                    : this.props.uploadAction
                }
                buttonTheme={THEME.SUCCESS}
                customStyle={styles.uploadButton}
                ariaLabel={LOCALIZE.commons.upload}
                disabled={
                  !this.state.dataReadyForUpload ||
                  !this.state.validBundleItemsBasicRuleState ||
                  !this.state.validBundleBundlesRuleState
                }
              ></CustomButton>
            </div>
          </div>
        </Navbar>
        <PopupBox
          show={this.state.showUploadConfirmationPopup}
          handleClose={() => {}}
          size={!this.state.showUploadedSuccessfullyPopup ? "xl" : "lg"}
          title={
            !this.state.showUploadedSuccessfullyPopup
              ? LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                  .uploadConfirmationPopup.title
              : LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                  .uploadConfirmationPopup.titleSuccessfully
          }
          description={
            <div>
              <SystemMessage
                messageType={
                  !this.state.showUploadedSuccessfullyPopup
                    ? MESSAGE_TYPE.warning
                    : MESSAGE_TYPE.success
                }
                title={
                  !this.state.showUploadedSuccessfullyPopup
                    ? LOCALIZE.commons.warning
                    : LOCALIZE.commons.success
                }
                message={
                  <p>
                    {!this.state.showUploadedSuccessfullyPopup
                      ? LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                          .items.uploadConfirmationPopup.systemMessageDescription
                      : this.props.uploadItemDataParameters.versionControlParameter ===
                        VERSION_CONTROL_RADIO_BUTTON.overwriteVersion
                      ? LOCALIZE.formatString(
                          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                            .items.uploadConfirmationPopup
                            .systemMessageDescriptionSuccessfullyOverwrite,
                          <span style={styles.bold}>{this.state.newVersionUploaded}</span>
                        )
                      : LOCALIZE.formatString(
                          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                            .items.uploadConfirmationPopup.systemMessageDescriptionSuccessfully,
                          <span style={styles.bold}>{this.state.newVersionUploaded}</span>
                        )}
                  </p>
                }
              />
              {!this.state.showUploadedSuccessfullyPopup && (
                <div>
                  <div style={styles.popupDescriptionContainer}>
                    <div>
                      <GenericTable
                        classnamePrefix="item-modifications"
                        columnsDefinition={columnsDefinition}
                        rowsDefinition={this.state.rowsDefinition}
                        currentlyLoading={this.state.currentlyLoading}
                        tableWithButtons={true}
                      />
                    </div>
                  </div>
                  <div style={styles.radioButtonsContainer}>
                    <label id="item-modifications-radio-buttons-label" style={styles.bold}>
                      {
                        LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                          .items.uploadConfirmationPopup.whatDoYouWantToDo
                      }
                    </label>
                    <div>
                      <input
                        id={VERSION_CONTROL_RADIO_BUTTON.uploadNewVersion}
                        name="item-modifications-radio-buttons"
                        type={"radio"}
                        aria-labelledby={`item-modifications-radio-buttons-label ${VERSION_CONTROL_RADIO_BUTTON.uploadNewVersion}`}
                        checked={
                          this.state.selectedRadioButtonOption ===
                          VERSION_CONTROL_RADIO_BUTTON.uploadNewVersion
                        }
                        style={styles.radioButton}
                        onChange={this.handleRadioButtonClick}
                        lang={this.props.selectedContentLanguage}
                      />
                      <label
                        htmlFor={VERSION_CONTROL_RADIO_BUTTON.uploadNewVersion}
                        style={styles.radioButtonLabel}
                      >
                        {
                          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                            .items.uploadConfirmationPopup.createNewVersionRadio
                        }
                      </label>
                    </div>
                    <div>
                      <input
                        id={VERSION_CONTROL_RADIO_BUTTON.overwriteVersion}
                        name="item-modifications-radio-buttons"
                        type={"radio"}
                        aria-labelledby={`item-modifications-radio-buttons-label ${VERSION_CONTROL_RADIO_BUTTON.overwriteVersion}`}
                        checked={
                          this.state.selectedRadioButtonOption ===
                          VERSION_CONTROL_RADIO_BUTTON.overwriteVersion
                        }
                        style={styles.radioButton}
                        onChange={this.handleRadioButtonClick}
                        lang={this.props.selectedContentLanguage}
                      />
                      <label
                        htmlFor={VERSION_CONTROL_RADIO_BUTTON.overwriteVersion}
                        style={styles.radioButtonLabel}
                      >
                        {
                          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                            .items.uploadConfirmationPopup.overwriteCurrentVersionRadio
                        }
                      </label>
                    </div>
                    {this.state.containsWarnings && (
                      <div style={styles.checkboxContainer}>
                        <div style={styles.tableCellMiddle}>
                          <input
                            id="item-modifications-reviewed-warnings-checkbox"
                            type="checkbox"
                            value={this.state.reviewedWarningsCheckboxChecked}
                            style={{ ...styles.checkbox, ...{ transform: checkboxTransformScale } }}
                            onChange={() => {
                              this.setState({
                                reviewedWarningsCheckboxChecked:
                                  !this.state.reviewedWarningsCheckboxChecked
                              });
                            }}
                          />
                        </div>
                        <div style={styles.tableCellMiddle}>
                          <label
                            htmlFor={"item-modifications-reviewed-warnings-checkbox"}
                            style={styles.checkboxLabel}
                          >
                            {
                              LOCALIZE.testBuilder.testDefinition.selectTestDefinition
                                .itemBankEditor.items.uploadConfirmationPopup
                                .reviewedWarningsCheckboxLabel
                            }
                          </label>
                        </div>
                      </div>
                    )}
                  </div>
                </div>
              )}
            </div>
          }
          leftButtonType={
            !this.state.showUploadedSuccessfullyPopup ? BUTTON_TYPE.secondary : BUTTON_TYPE.none
          }
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonLabel={LOCALIZE.commons.cancel}
          leftButtonAction={this.closeUploadConfirmationPopup}
          leftButtonIcon={faTimes}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={
            this.state.uploadLoading ? (
              // eslint-disable-next-line jsx-a11y/label-has-associated-control
              <label className="fa fa-spinner fa-spin">
                <FontAwesomeIcon icon={faSpinner} />
              </label>
            ) : !this.state.showUploadedSuccessfullyPopup ? (
              LOCALIZE.commons.upload
            ) : (
              LOCALIZE.commons.close
            )
          }
          rightButtonLabel={
            this.state.uploadLoading ? (
              // eslint-disable-next-line jsx-a11y/label-has-associated-control
              <label className="fa fa-spinner fa-spin">
                <FontAwesomeIcon icon={faSpinner} />
              </label>
            ) : !this.state.showUploadedSuccessfullyPopup ? (
              LOCALIZE.commons.upload
            ) : (
              LOCALIZE.commons.close
            )
          }
          rightButtonAction={
            !this.state.showUploadedSuccessfullyPopup
              ? this.handleUpload
              : this.closeUploadConfirmationPopup
          }
          rightButtonIcon={
            this.state.uploadLoading
              ? ""
              : !this.state.showUploadedSuccessfullyPopup
              ? faUpload
              : faTimes
          }
          rightButtonState={
            this.state.uploadLoading
              ? BUTTON_STATE.disabled
              : this.state.selectedRadioButtonOption === null ||
                !this.state.reviewedWarningsCheckboxChecked
              ? BUTTON_STATE.disabled
              : BUTTON_STATE.enabled
          }
        />
      </div>
    );
  }
}

export { ItemBankFooter as unconnectedItemBankFooter };

const mapStateToProps = (state, ownProps) => {
  return {
    accommodations: state.accommodations,
    accommodationRequestId: state.assignedTest.accommodationRequestId,
    dataChangesDetected: state.itemBank.dataChangesDetected,
    dataReadyForUpload: state.itemBank.dataReadyForUpload,
    validBundleItemsBasicRuleState: state.itemBank.validBundleItemsBasicRuleState,
    validBundleBundlesRuleState: state.itemBank.validBundleBundlesRuleState,
    itemData: state.itemBank.itemData,
    item_bank_data: state.itemBank.item_bank_data,
    selectedContentLanguage: state.itemBank.selectedContentLanguage,
    uploadItemDataParameters: state.itemBank.uploadItemDataParameters,
    languageData: state.localize.languageData
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getItemData,
      setUploadItemDataParameters
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ItemBankFooter);
