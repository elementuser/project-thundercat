/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../../text_resources";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import ContentContainer, { maxWidthUtils } from "../../commons/ContentContainer";
import ItemBankDefinition from "./ItemBankDefinition";
import ItemAttributes from "./ItemAttributes";
import Items from "./items/Items";
import CustomButton, { THEME } from "../../commons/CustomButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faArrowAltCircleLeft,
  faCheck,
  faSpinner,
  faTimes
} from "@fortawesome/free-solid-svg-icons";
import { history } from "../../../store-index";
import { PATH } from "../../commons/Constants";
import {
  resetItemBanksStates,
  resetItemsStates,
  resetBundlesStates
} from "../../../modules/ItemBankRedux";
import PopupBox, { BUTTON_TYPE } from "../../commons/PopupBox";
import SystemMessage, { MESSAGE_TYPE } from "../../commons/SystemMessage";
import TopTabs from "../../commons/TopTabs";
import * as _ from "lodash";
import Bundles from "./bundles/Bundles";
import { triggerAppRerender } from "../../../modules/AppRedux";

const styles = {
  loadingContainer: {
    margin: 24,
    textAlign: "center"
  },
  loading: {
    fontSize: "32px",
    padding: 6
  },
  backButtonStyle: {
    marginBottom: 24
  },
  buttonLabel: {
    marginLeft: 6
  },
  sectionContainer: {
    borderStyle: "solid",
    borderWidth: "3px 1px 1px 1px",
    borderColor: "#CECECE",
    minHeight: 400,
    padding: "12px 0",
    marginBottom: 24
  },
  body: {
    paddingLeft: 12
  },
  appPadding: {
    padding: "15px 15px 0 15px",
    marginBottom: "-100px"
  }
};

class ItemBankEditor extends Component {
  state = {
    isLoading: true,
    showBackToItemBankSelectionPopup: false,
    triggerRerender: false
  };

  constructor(props) {
    super(props);
    this.topTabsHeightRef = React.createRef();
  }

  componentDidMount = () => {
    // making sure that the item_bank_definition is defined (otherwise, redirect the user to the test builder page)
    // can happen if the URL is manually entered or if the back browser button has been pressed
    if (this.props.item_bank_definition.id === null) {
      setTimeout(() => {
        if (this.props.item_bank_definition.id === null) {
          history.push(PATH.testBuilder);
        } else {
          this.setState({ isLoading: false });
        }
      }, 3000);
    } else {
      this.setState({ isLoading: false });
    }
    // triggering app rerender to make sure that the special page height and overflow is properly set
    this.props.triggerAppRerender();
  };

  componentDidUpdate = prevProps => {
    // if accommodations are updated
    if (
      prevProps.accommodations.fontSize !== this.props.accommodations.fontSize ||
      prevProps.accommodations.spacing !== this.props.accommodations.spacing ||
      prevProps.accommodations.fontFamily !== this.props.accommodations.fontFamily
    ) {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }
  };

  openBackToItemBankSelectionPopup = () => {
    this.setState({ showBackToItemBankSelectionPopup: true });
  };

  closeBackToItemBankSelectionPopup = () => {
    this.setState({ showBackToItemBankSelectionPopup: false });
  };

  handleBackToItemBankSelection = () => {
    // resetting item bank and items redux states
    this.props.resetItemBanksStates();
    this.props.resetItemsStates();
    this.props.resetBundlesStates();
    history.push(PATH.testBuilder);
  };

  // will return true if changes are detected in redux state item_bank_data
  changesDetected = () => {
    if (_.isEqual(this.props.item_bank_data, this.props.itemBankInitialData)) {
      this.handleBackToItemBankSelection();
    }
    this.openBackToItemBankSelectionPopup();
  };

  getTABS = () => {
    return [
      {
        key: "1",
        tabName:
          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
            .sideNavigationItems.itemBankDefinition,
        body: <ItemBankDefinition />
      },
      {
        key: "2",
        tabName:
          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
            .sideNavigationItems.itemAttributes,
        body: <ItemAttributes />
      },
      {
        key: "3",
        tabName: LOCALIZE.formatString(
          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
            .sideNavigationItems.items,
          this.props.item_bank_data.item_bank_number_of_found_items
        ),
        body: <Items />
      },
      {
        key: "4",
        tabName: LOCALIZE.formatString(
          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
            .sideNavigationItems.bundles,
          this.props.item_bank_data.list_of_bundles.length
        ),
        body: <Bundles />
      }
    ];
  };

  render() {
    const TABS = this.getTABS();
    return (
      <div>
        {this.state.isLoading ? (
          <div style={styles.loadingContainer}>
            <label className="fa fa-spinner fa-spin">
              <FontAwesomeIcon icon={faSpinner} style={styles.loading} />
            </label>
          </div>
        ) : (
          <div className="app" style={styles.appPadding}>
            <Helmet>
              <html lang={this.props.currentLanguage} />
              <title>{LOCALIZE.titles.itemBankEditor}</title>
            </Helmet>
            <ContentContainer customMaxWidth={maxWidthUtils.wide}>
              <div ref={this.topTabsHeightRef}>
                <div style={styles.backButtonStyle}>
                  <CustomButton
                    buttonId="back-to-item-bank-selection-btn"
                    label={
                      <>
                        <FontAwesomeIcon icon={faArrowAltCircleLeft} />
                        <span style={styles.buttonLabel}>
                          {
                            LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                              .backToItemBankSelectionButton
                          }
                        </span>
                      </>
                    }
                    action={this.changesDetected}
                    buttonTheme={THEME.SECONDARY}
                  />
                </div>
                <TopTabs TABS={TABS} defaultTab={"1"} />
              </div>
            </ContentContainer>
            <PopupBox
              show={this.state.showBackToItemBankSelectionPopup}
              handleClose={this.closeBackToItemBankSelectionPopup}
              title={
                LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                  .backToItemBankSelectionPopup.title
              }
              description={
                <div>
                  <SystemMessage
                    messageType={MESSAGE_TYPE.warning}
                    title={LOCALIZE.commons.warning}
                    message={
                      <p>
                        {
                          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                            .backToItemBankSelectionPopup.systemMessageDescription
                        }
                      </p>
                    }
                  />
                  <p>
                    {
                      LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                        .backToItemBankSelectionPopup.description
                    }
                  </p>
                </div>
              }
              leftButtonType={BUTTON_TYPE.secondary}
              leftButtonTitle={LOCALIZE.commons.cancel}
              leftButtonIcon={faTimes}
              rightButtonType={BUTTON_TYPE.primary}
              rightButtonTitle={LOCALIZE.commons.confirm}
              rightButtonAction={this.handleBackToItemBankSelection}
              rightButtonIcon={faCheck}
              size="lg"
            />
          </div>
        )}
      </div>
    );
  }
}

export { ItemBankEditor as unconnectedItemBankEditor };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    item_bank_data: state.itemBank.item_bank_data,
    item_bank_definition: state.itemBank.item_bank_data.item_bank_definition[0],
    item_bank_items: state.itemBank.item_bank_data.item_bank_items,
    itemBankAttributesInitialData: state.itemBank.itemBankAttributesInitialData,
    itemBankInitialData: state.itemBank.itemBankInitialData,
    item_bank_attributes: state.itemBank.item_bank_data.item_bank_attributes
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      resetItemBanksStates,
      resetItemsStates,
      resetBundlesStates,
      triggerAppRerender
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ItemBankEditor);
