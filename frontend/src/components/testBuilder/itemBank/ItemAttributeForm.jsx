import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import LOCALIZE from "../../../text_resources";
import { Col, Row } from "react-bootstrap";
import { makeLabel, makeTextBoxField, getNextTempInNumberSeries, orderByOrder } from "../helpers";
import CustomButton, { THEME } from "../../commons/CustomButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlusCircle, faLanguage } from "@fortawesome/free-solid-svg-icons";
import {
  ITEM_BANK_ATTRIBUTES,
  modifyItemBankField,
  replaceItemBankField,
  setItemBankData
} from "../../../modules/ItemBankRedux";
import { ATTRIBUTE_VALUE_TYPE } from "./items/Constants";
import PropTypes from "prop-types";
import CustomDraggable from "../../commons/CustomDraggable/CustomDraggable";
import { DragDropContext, Droppable } from "react-beautiful-dnd";
import * as _ from "lodash";
import StyledTooltip, { EFFECT, TRIGGER_TYPE, TYPE } from "../../authentication/StyledTooltip";
import PopupBox, { BUTTON_TYPE } from "../../commons/PopupBox";

const styles = {
  mainContainer: {
    width: "95%",
    margin: "auto"
  },
  variableNameRow: {
    paddingRight: "24px",
    margin: "5px"
  },
  rowStyle: {
    margin: "5px"
  },
  buttonRowStyle: {
    margin: "15px"
  },
  attributeValueTitle: {
    fontWeight: "bold",
    color: "#00565e"
  },
  allUnset: {
    all: "unset"
  },
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  translateButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: 0,
    margin: "0 3px",
    color: "#00565e"
  },
  labelContainer: {
    marginTop: "auto",
    marginBottom: "auto",
    textAlign: "left"
  },
  translationsAttributeNameFields: {
    margin: "24px 0px"
  }
};

// Resetting the order value by using the array's current order
const adjustOrders = list => {
  const newList = list;
  for (let i = 0; i < list.length; i++) {
    newList[i].order = i + 1;
  }
  return newList;
};

// Moving an item in a list
// Specify:
// - list: an array
// - startIndex: the item at the specified index that we want to move
// - endIndex: the final index where the item will be moved
const reorderList = (list, startIndex, endIndex) => {
  const result = Array.from(list);

  // Reordering in the list
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

class ItemAttributeForm extends Component {
  constructor(props, context) {
    super(props, context);

    this.PropTypes = {
      // Item Bank Attribute
      item_bank_attribute: PropTypes.object,
      // Item Bank Attribute Index in Attributes Array
      INDEX: PropTypes.number,
      // Delete Attribute Action
      deleteItem: PropTypes.func
    };

    ItemAttributeForm.defaultProps = {};

    // Initializing all the states
    this.state = {
      // Stores the timer used to know when a user is still typing
      typingTimer: 0,
      // Stores this.props.item_bank_attribute.item_bank_attribute_text for real-time validation
      attributeNames: this.getInitialAttributeNames(),
      // Stores real-time validity of each attribute name (multilingual)
      attributeNamesValidity: this.getInitialAttributeNamesValidity(),
      // Stores this.props.item_bank_attribute.item_bank_attribute_values for real-time validation
      itemBankAttributeValues: this.getInitialItemBankAttributeValues(),
      // Stores real-time validity of each attribute value
      itemBankAttributeValuesValidity: this.getInitialItemBankAttributeValuesValidity(),
      // State to open the translation popup
      showTranslationPopup: false
    };
  }

  componentDidMount = () => {
    // Shows the popup if the Attribute Names aren't valid when opening the collapsible
    this.setState({ showTranslationPopup: !this.areAttributeNamesValid() });
  };

  /*--------------------------*/
  //                          //
  //     Attribute's Values   //
  //                          //
  /*--------------------------*/

  /*--------------------------*/
  //  Drag&Drop Value Section //
  /*--------------------------*/
  onDragEndItemBankAttributeValuesSection(result) {
    // dropped outside the list
    if (!result.destination) {
      return;
    }

    // ***IMPORTANT***
    // Since we are doing real-time validation with our states,
    // make sure to update the states at the same time as the redux states
    /*------------------------------------------------------------------*/
    // reordering of the state: item_bank_attributes_values
    const temp_itemBankAttributeValues = reorderList(
      this.state.itemBankAttributeValues,
      result.source.index,
      result.destination.index
    );

    // reordering of the state: item_bank_attributes_values_validity
    const temp_itemBankAttributeValuesValidity = reorderList(
      this.state.itemBankAttributeValuesValidity,
      result.source.index,
      result.destination.index
    );
    /*------------------------------------------------------------------*/

    // creating deep copy of item_bank_attribute_values of the current attribute
    const copyOfItemBankAttributeValues = _.cloneDeep(
      this.props.item_bank_attribute.item_bank_attribute_values
    );

    // reordering of the prop: item_bank_attributes_values
    let newItemBankAttributeValues = reorderList(
      copyOfItemBankAttributeValues,
      result.source.index,
      result.destination.index
    );
    // Changing the order values
    newItemBankAttributeValues = adjustOrders(newItemBankAttributeValues);

    const item_bank_data = _.cloneDeep(this.props.item_bank_data);

    const item_bank_attributes = [...item_bank_data.item_bank_attributes];
    let item_bank_attributes_current = item_bank_attributes[this.props.INDEX];

    item_bank_attributes_current = {
      ...item_bank_attributes_current,
      item_bank_attribute_values: [...newItemBankAttributeValues]
    };

    item_bank_attributes[this.props.INDEX] = item_bank_attributes_current;

    const new_item_bank_data = {
      ...item_bank_data,
      item_bank_attributes: [...item_bank_attributes]
    };

    // Update the local states
    this.setState({
      itemBankAttributeValues: [...temp_itemBankAttributeValues],
      itemBankAttributeValuesValidity: [...temp_itemBankAttributeValuesValidity]
    });

    this.props.setItemBankData(new_item_bank_data);
  }

  /*--------------------------*/
  //    Modify Value Field    //
  /*--------------------------*/
  onValueTextAreaComponentChange = (event, value_index) => {
    // allow maximum of 50 chars
    const regexExpression = /^(.{0,50})$/;

    const { value } = event.target;

    // realtime validation
    if (regexExpression.test(value)) {
      const itemBankAttributeValues = [...this.state.itemBankAttributeValues];

      // Verify if we currently have something for the text in that language
      if (itemBankAttributeValues[value_index]) {
        itemBankAttributeValues[value_index].value.text = value;
      }
      // If not, add the missing object parts
      else {
        itemBankAttributeValues[value_index] = {
          value: {
            text: value
          }
        };
      }

      // Set validity for current value to True
      const itemBankAttributeValuesValidity = [...this.state.itemBankAttributeValuesValidity];
      if (value === "") {
        itemBankAttributeValuesValidity[value_index] = false;
      } else {
        itemBankAttributeValuesValidity[value_index] = true;
      }

      // Update the local states
      this.setState({
        itemBankAttributeValues: [...itemBankAttributeValues],
        itemBankAttributeValuesValidity: [...itemBankAttributeValuesValidity]
      });

      // Create a buffer before updating the redux state
      // Clear the old typingTimer
      clearTimeout(this.state.typingTimer);

      // Change the typingTimer to a timeout of 200 millisec
      // When user stops writing, this will go through after that time
      // Otherwise, the timer will be reset to that time
      const newTimer = setTimeout(() => {
        this.modifyAttributeValue(value, value_index);
      }, 200);

      this.setState({ typingTimer: newTimer });
    }
  };

  // Function used when modifying an Attribute's Value Field
  modifyAttributeValue = (value, value_index) => {
    // Deep clone of Item_Bank_Data
    const item_bank_data = _.cloneDeep(this.props.item_bank_data);

    // Item Bank Attributes
    const item_bank_data_attributes = item_bank_data.item_bank_attributes;

    // Current Item Bank Attribute
    let item_bank_data_attribute_current = item_bank_data.item_bank_attributes[this.props.INDEX];

    // Item Bank Attribute's Values
    const item_bank_data_attribute_values =
      item_bank_data_attribute_current.item_bank_attribute_values;

    // Item Bank Attribute's Current Value Being Modified
    const item_bank_data_attribute_value_current = item_bank_data_attribute_values[value_index];
    // Modify the value text
    item_bank_data_attribute_value_current.value.text = value;

    item_bank_data_attribute_values[value_index] = item_bank_data_attribute_value_current;

    item_bank_data_attribute_current = {
      ...item_bank_data_attribute_current,
      item_bank_attribute_values: [...item_bank_data_attribute_values]
    };

    item_bank_data_attributes[this.props.INDEX] = item_bank_data_attribute_current;

    const new_item_bank_data = {
      ...item_bank_data,
      item_bank_attributes: [...item_bank_data_attributes]
    };

    // setting new itemData redux state
    this.props.setItemBankData(new_item_bank_data);
  };

  /*--------------------------*/
  //    Add New Value Field   //
  /*--------------------------*/
  handleCreateNewItemAttributeValue = () => {
    const currentItemBankAttribute = this.props.item_bank_attribute;

    // TODO: Create different objects depending on type of value
    // **Wait until we know what types of values we can have**
    const newObj = {
      id: getNextTempInNumberSeries(
        this.props.item_bank_attribute.item_bank_attribute_values,
        "id"
      ),
      order: currentItemBankAttribute.item_bank_attribute_values.length + 1,
      attribute_value_type: ATTRIBUTE_VALUE_TYPE.text,
      value: {
        text: ""
      }
    };
    currentItemBankAttribute.item_bank_attribute_values.push(newObj);
    // reordering array of item bank attribute values
    currentItemBankAttribute.item_bank_attribute_values.sort(orderByOrder);
    this.props.modifyItemBankField(
      ITEM_BANK_ATTRIBUTES,
      currentItemBankAttribute,
      currentItemBankAttribute.id,
      "order"
    );

    const temp_itemBankAttributeValuesValidity = [...this.state.itemBankAttributeValuesValidity];

    temp_itemBankAttributeValuesValidity[temp_itemBankAttributeValuesValidity.length] = true;

    this.setState({
      itemBankAttributeValues: this.getInitialItemBankAttributeValues(),
      itemBankAttributeValuesValidity: temp_itemBankAttributeValuesValidity
    });
  };

  /*--------------------------*/
  //    Delete Value Field    //
  /*--------------------------*/
  handleDeleteAttributeValue = (id, value_index) => {
    // delete the item attribute value
    const objArray = [...this.props.item_bank_attributes];
    let currentItemBankAttributeValues = this.props.item_bank_attribute.item_bank_attribute_values;
    currentItemBankAttributeValues = currentItemBankAttributeValues.filter(obj => obj.id !== id);
    // reordering array of item bank attribute values + updating order values
    currentItemBankAttributeValues.sort(orderByOrder);
    for (let i = 0; i < currentItemBankAttributeValues.length; i++) {
      currentItemBankAttributeValues[i].order = i + 1;
    }
    objArray[this.props.INDEX].item_bank_attribute_values = currentItemBankAttributeValues;
    this.props.replaceItemBankField(ITEM_BANK_ATTRIBUTES, objArray, "order");

    // remove the value validity of itemBankAttributeValuesValidity
    const temp_itemBankAttributeValuesValidity = [...this.state.itemBankAttributeValuesValidity];
    temp_itemBankAttributeValuesValidity.splice(value_index, 1);

    this.setState({
      itemBankAttributeValues: this.getInitialItemBankAttributeValues(),
      itemBankAttributeValuesValidity: temp_itemBankAttributeValuesValidity
    });
  };

  /*--------------------------*/
  //   Validate Value Field   //
  /*--------------------------*/
  isValueValid = item_bank_attribute_value => {
    if (item_bank_attribute_value.value.text === "") {
      return false;
    }
    return true;
  };

  /*--------------------------*/
  //                          //
  //        Attribute         //
  //                          //
  /*--------------------------*/

  /*--------------------------*/
  // Modify Attribute Fields  //
  /*--------------------------*/
  modifyAttributeFields = () => {
    // Creating a clone of item_bank_data
    const item_bank_data = _.cloneDeep(this.props.item_bank_data);

    const item_bank_attributes = [...item_bank_data.item_bank_attributes];

    const item_bank_attributes_current = item_bank_attributes[this.props.INDEX];

    // Updating the item_bank_attribute_text of the current attribute with our AttributeNames state
    item_bank_attributes_current.item_bank_attribute_text = this.state.attributeNames;

    item_bank_attributes[this.props.INDEX] = item_bank_attributes_current;

    const new_item_bank_data = {
      ...item_bank_data,
      item_bank_attributes: [...item_bank_attributes]
    };

    this.props.setItemBankData(new_item_bank_data);
  };

  /*--------------------------*/
  // Validate Attribute Field //
  /*--------------------------*/
  attributeNameValidation = (name, value) => {
    // Removing _name to get the language
    const language = name !== "" ? name.replace("_name", "") : this.props.currentLanguage;

    // allow maximum of 150 chars
    const regexExpression = /^(.{0,150})$/;

    // realtime validation
    if (regexExpression.test(value)) {
      // Change the validity state
      if (value !== "") {
        // Since the test passed, change the validity of the current langauge to "True"
        this.handleAttributeValidityChange(language, true);
      } else {
        // If the test passed, but it is empty, put the validity to "False"
        this.handleAttributeValidityChange(language, false);
      }

      const tempAttributeNames = { ...this.state.attributeNames };

      // Verify if we currently have something for the text in that language
      if (tempAttributeNames[language]) {
        tempAttributeNames[language][0].text = value;
      }
      // If not, add the missing object parts
      else {
        tempAttributeNames[language] = [];
        tempAttributeNames[language][0] = {
          text: value
        };
      }

      // Update the local states
      this.setState({
        attributeNames: {
          ...tempAttributeNames
        }
      });

      // Create a buffer before updating the redux state
      // Clear the old typingTimer
      clearTimeout(this.state.typingTimer);

      // Change the typingTimer to a timeout of 200 millisec
      // When user stops writing, this will go through after that time
      // Otherwise, the timer will be reset to that time
      const newTimer = setTimeout(() => {
        this.modifyAttributeFields();
      }, 200);

      this.setState({ typingTimer: newTimer });
    }
  };

  // Helper to change efficiently to True/False the validify of an Attribute Name
  handleAttributeValidityChange = (language, value) => {
    const newIsAttributeNameValid = { ...this.state.attributeNamesValidity };
    newIsAttributeNameValid[language] = value;
    this.setState({ attributeNamesValidity: newIsAttributeNameValid });
  };

  // Function used to activate/deactivate the "Add Value" button
  areAttributeNamesValid = () => {
    const languagesArray = LOCALIZE.getAvailableLanguages();

    // For each language, verify the validity of attribute's name
    for (let i = 0; i < languagesArray.length; i++) {
      if (
        !this.state.attributeNamesValidity[languagesArray[i]] ||
        !this.props.item_bank_attribute.item_bank_attribute_text[languagesArray[i]] ||
        this.props.item_bank_attribute.item_bank_attribute_text[languagesArray[i]][0].text === ""
      ) {
        return false;
      }
    }

    return true;
  };

  // Validate an Attribute Name
  isAttributeNameValid = language => {
    if (
      this.props.item_bank_attribute.item_bank_attribute_text[language] &&
      this.props.item_bank_attribute.item_bank_attribute_text[language][0].text === ""
    ) {
      return false;
    }
    return true;
  };

  handleOpen = () => {
    this.setState({ showTranslationPopup: true });
  };

  handleClose = () => {
    this.setState({ showTranslationPopup: false });
  };

  /*--------------------------*/
  //                          //
  //       Initializers       //
  //                          //
  /*--------------------------*/

  /*--------------------------*/
  //       Attributes         //
  /*--------------------------*/

  // State Initializer for Attributes Fields
  getInitialAttributeNames = () => {
    // reordering array of item bank attribute values
    const attributeNames = this.props.item_bank_attribute.item_bank_attribute_text;
    if (attributeNames) {
      return attributeNames;
    }
    return {};
  };

  // State Initializer for Attributes Fields Validity
  getInitialAttributeNamesValidity = () => {
    // Get all languages to populate attributeNamesValidity
    const languagesArray = LOCALIZE.getAvailableLanguages();
    const attributeNameValidObject = {};

    // Populate each language with "True" for valid
    for (let i = 0; i < languagesArray.length; i++) {
      attributeNameValidObject[languagesArray[i]] = this.isAttributeNameValid(languagesArray[i]);
    }

    return attributeNameValidObject;
  };

  /*--------------------------*/
  //    Attribute's Values    //
  /*--------------------------*/

  // State Initializer for Attribute Values Fields
  getInitialItemBankAttributeValues = () => {
    const { item_bank_attribute_values } = this.props.item_bank_attribute;

    if (item_bank_attribute_values) {
      return item_bank_attribute_values;
    }
    return [];
  };

  // State Initializer for Attribute Values Validity
  getInitialItemBankAttributeValuesValidity = () => {
    const { item_bank_attribute_values } = this.props.item_bank_attribute;

    const tempArray = [];

    if (item_bank_attribute_values) {
      // Populate each validity with "True" for valid
      for (let i = 0; i < item_bank_attribute_values.length; i++) {
        tempArray[i] = this.isValueValid(item_bank_attribute_values[i]);
      }
    }
    return tempArray;
  };

  /*--------------------------*/
  //                          //
  //        Styling           //
  //                          //
  /*--------------------------*/

  /*--------------------------*/
  //    Draggable Styling     //
  /*--------------------------*/
  getItemStyle = (isDragging, draggableStyle) => ({
    // some basic styles to make the items look a bit nicer
    userSelect: "none",

    // change background colour if dragging
    background: isDragging ? "#0099A8" : "white",
    color: "#00565e",

    // styles we need to apply on draggables
    ...draggableStyle
  });

  /*--------------------------*/
  //    Droppable Styling     //
  /*--------------------------*/
  getListStyle = isDraggingOver => ({
    background: isDraggingOver ? "#cdcdcd" : "white",
    padding: "12px 24px 12px 24px"
  });

  render() {
    return (
      <div style={styles.mainContainer}>
        <Row style={styles.variableNameRow}>
          {makeLabel(
            `${this.props.currentLanguage}_name`,
            "",
            `item-attribute-${this.props.currentLanguage}-name-${this.props.INDEX}`,
            {},
            false,
            LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.itemAttributes
              .itemAttributeCollapsingItem.attributeTitle,
            {
              xl: 3,
              lg: 3,
              md: 3,
              sm: 3
            }
          )}
          {makeTextBoxField(
            `${this.props.currentLanguage}_name`,
            LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.itemAttributes
              .itemAttributeCollapsingItem,
            typeof this.state.attributeNames[this.props.currentLanguage] !== "undefined"
              ? this.state.attributeNames[this.props.currentLanguage][0].text
              : "",
            this.state.attributeNamesValidity[this.props.currentLanguage],
            this.attributeNameValidation,
            `item-attribute-${this.props.currentLanguage}-name-${this.props.INDEX}`,
            !this.areAttributeNamesValid(),
            {},
            true,
            {
              xl: 8,
              lg: 8,
              md: 8,
              sm: 8
            }
          )}
          <Col xl={1} lg={1} md={1} sm={1} className={"text-center"}>
            <StyledTooltip
              id={`item-bank-attribute-translations`}
              place="top"
              type={TYPE.light}
              effect={EFFECT.solid}
              triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
              tooltipElement={
                <div style={styles.allUnset}>
                  <CustomButton
                    className="h-100"
                    dataTip=""
                    dataFor={`item-bank-attribute-translations`}
                    label={<FontAwesomeIcon icon={faLanguage} />}
                    ariaLabel={
                      LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                        .itemAttributes.itemAttributeCollapsingItem.attributeTranslationsButtonLabel
                    }
                    action={() => this.handleOpen()}
                    customStyle={{
                      ...styles.translateButton,
                      fontSize: this.props.accommodations.fontSize.replace("px", "") * 1.5
                    }}
                    buttonTheme={THEME.SECONDARY}
                  />
                </div>
              }
              tooltipContent={
                <div>
                  <p>
                    {
                      LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                        .itemAttributes.itemAttributeCollapsingItem.attributeTranslationsButtonLabel
                    }
                  </p>
                </div>
              }
            />
          </Col>
        </Row>
        <Row style={styles.rowStyle}>
          <Col style={styles.labelContainer}>
            <span style={styles.attributeValueTitle}>
              {
                LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                  .itemAttributes.itemAttributeCollapsingItem.attributeValuesTitle
              }
            </span>
            <StyledTooltip
              id={`item-bank-attribute-value-add`}
              place="top"
              type={TYPE.light}
              effect={EFFECT.solid}
              triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
              tooltipElement={
                <div style={styles.allUnset}>
                  <CustomButton
                    dataTip=""
                    dataFor={`item-bank-attribute-value-add`}
                    label={<FontAwesomeIcon icon={faPlusCircle} />}
                    ariaLabel={
                      LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                        .itemAttributes.itemAttributeCollapsingItem.createNewAttributeValueButton
                    }
                    action={this.handleCreateNewItemAttributeValue}
                    customStyle={styles.actionButton}
                    buttonTheme={THEME.SECONDARY}
                    disabled={!this.areAttributeNamesValid()}
                  />
                </div>
              }
              tooltipContent={
                <div>
                  <p>
                    {
                      LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                        .itemAttributes.itemAttributeCollapsingItem.createNewAttributeValueButton
                    }
                  </p>
                </div>
              }
            />
          </Col>
        </Row>
        <div>
          <DragDropContext
            onDragEnd={result => {
              this.onDragEndItemBankAttributeValuesSection(result);
            }}
          >
            <Droppable
              droppableId={`item-bank-attribute-${this.props.INDEX}-value-section-droppable`}
            >
              {(provided, snapshot) => (
                <div
                  {...provided.droppableProps}
                  ref={provided.innerRef}
                  style={this.getListStyle(snapshot.isDraggingOver)}
                >
                  {this.state.itemBankAttributeValues.map(
                    (item_bank_attribute_value, value_index) => (
                      <CustomDraggable
                        item_bank_attribute_index={this.props.INDEX}
                        item_bank_attribute_value={item_bank_attribute_value}
                        index={value_index}
                        getItemStyle={this.getItemStyle}
                        onTextAreaComponentChange={e =>
                          this.onValueTextAreaComponentChange(e, value_index)
                        }
                        deleteItem={() =>
                          this.handleDeleteAttributeValue(item_bank_attribute_value.id, value_index)
                        }
                        currentLanguage={this.props.currentLanguage}
                        validity={this.state.itemBankAttributeValuesValidity[value_index]}
                      />
                    )
                  )}
                  {provided.placeholder}
                </div>
              )}
            </Droppable>
          </DragDropContext>
        </div>
        {/* TODO: Create a new Component for Translations PopupBox */}
        <PopupBox
          show={this.state.showTranslationPopup}
          title={
            LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.itemAttributes
              .itemAttributeCollapsingItem.attributeNameTranslationsPopup.title
          }
          handleClose={() => this.handleClose()}
          description={
            <div>
              <div style={styles.translationsAttributeNameFields}>
                {LOCALIZE.getAvailableLanguages().map(language => (
                  <Row style={styles.rowStyle}>
                    {makeLabel(
                      `${language}_name`,
                      LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                        .itemAttributes.itemAttributeCollapsingItem,
                      `item-attribute-${language}-name-${this.props.INDEX}`,
                      {},
                      false
                    )}
                    {makeTextBoxField(
                      `${language}_name`,
                      LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                        .itemAttributes.itemAttributeCollapsingItem,
                      typeof this.state.attributeNames[language] !== "undefined"
                        ? this.state.attributeNames[language][0].text
                        : "",
                      this.state.attributeNamesValidity[language],
                      this.attributeNameValidation,
                      `item-attribute-${language}-name-${this.props.INDEX}`
                    )}
                  </Row>
                ))}
              </div>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.ok}
          rightButtonAction={() => this.handleClose()}
          rightButtonState={!this.areAttributeNamesValid()}
          isBackdropStatic={!this.areAttributeNamesValid()}
          shouldCloseOnEsc={this.areAttributeNamesValid()}
        />
      </div>
    );
  }
}

export { ItemAttributeForm as unconnectedItemAttributeForm };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    item_bank_attributes: state.itemBank.item_bank_data.item_bank_attributes,
    item_bank_data: state.itemBank.item_bank_data,
    triggerItemBankFieldUpdates: state.itemBank.triggerItemBankFieldUpdates
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      modifyItemBankField,
      replaceItemBankField,
      setItemBankData
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ItemAttributeForm);
