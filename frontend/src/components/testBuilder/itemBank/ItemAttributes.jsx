import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import LOCALIZE from "../../../text_resources";
import { Container } from "react-bootstrap";
import CustomButton, { THEME } from "../../commons/CustomButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faSpinner,
  faPlusCircle,
  faChevronCircleUp,
  faUpload,
  faCheck
} from "@fortawesome/free-solid-svg-icons";

import {
  ITEM_BANK_ATTRIBUTES,
  addItemBankField,
  replaceItemBankField,
  updateItemBankData,
  setItemBankData,
  setItemBankAttributesInitialData,
  setItemBankInitialData,
  getSelectedItemBankData
} from "../../../modules/ItemBankRedux";
import { getNextTempInNumberSeries, orderByOrder } from "../helpers";
import ELEMENT_TO_UPDATE from "./Constants";
import PopupBox, { BUTTON_TYPE } from "../../commons/PopupBox";
import SystemMessage, { MESSAGE_TYPE } from "../../commons/SystemMessage";
import { getItemBankHeightCalculations } from "../../../helpers/inTestHeightCalculations";
import { DragDropContext, Droppable } from "react-beautiful-dnd";
import CustomDraggable from "../../commons/CustomDraggable/CustomDraggable";
import * as _ from "lodash";

const styles = {
  mainContainer: {
    width: "100%",
    padding: "24px 12px",
    border: "1px solid #cdcdcd",
    borderTop: "none",
    overflowY: "auto"
  },
  buttonIcon: {
    marginRight: 6
  },
  createButton: {
    margin: "12px 0",
    padding: "0 15px 0 15px",
    display: "inline-block"
  },
  collapseAllButton: {
    margin: "12px 0",
    padding: "0 15px 0 15px",
    display: "inline-block"
  },
  uploadDataButtonContainer: {
    textAlign: "center",
    marginTop: 24
  },
  uploadDataButton: {
    minWidth: 200
  },
  errorMessageContainer: {
    textAlign: "center",
    border: "1px solid #923534",
    padding: 12,
    marginBottom: 12
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6
  },
  loading: {
    width: "100%",
    minHeight: 60,
    textAlign: "center",
    fontSize: "32px",
    padding: 6
  }
};

// Resetting the order value by using the array's current order
const adjustOrders = list => {
  const newList = list;
  for (let i = 0; i < list.length; i++) {
    newList[i].order = i + 1;
  }
  return newList;
};

// Moving an item in a list
// Specify:
// - list: an array
// - startIndex: the item at the specified index that we want to move
// - endIndex: the final index where the item will be moved
const reorderList = (list, startIndex, endIndex) => {
  const result = Array.from(list);

  // Reordering in the list
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

class ItemAttributes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      triggerRerender: false,
      showDeleteDialog: false,
      attributeIdToBeDeleted: undefined,
      isDraggable: true,
      attributesCollapsedStatusArray: this.populateInitialAttributesCollapsedStatusArray(),
      triggerCollapseAll: true
    };
  }

  componentDidUpdate = (prevProps, prevState) => {
    // if currentTab gets updated
    if (prevProps.currentTab !== this.props.currentTab) {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }
    // if any of the collapsable attributes's status changed
    if (prevState.attributesCollapsedStatusArray !== this.state.attributesCollapsedStatusArray) {
      this.verifyCollapsedStatus();
    }
  };

  handleCreateNewItemAttribute = () => {
    const newObj = {
      id: getNextTempInNumberSeries(this.props.item_bank_attributes, "id"),
      order: this.props.item_bank_attributes.length + 1,
      item_bank_attribute_text: {},
      item_bank_attribute_values: []
    };
    this.props.addItemBankField(ITEM_BANK_ATTRIBUTES, newObj, "order");
    // removing the upload data error (if displayed)
    this.setState({
      attributesCollapsedStatusArray: this.handleCreateNewAttributeCollapse(newObj.id)
    });
  };

  // onDragEnd - Attribute Section
  onDragEndItemBankAttributeSection(result) {
    // dropped outside the list
    if (!result.destination) {
      return;
    }

    // creating deep copy of item_bank_attributes
    const copyOfItemBankAttributes = _.cloneDeep(this.props.item_bank_attributes);

    // reordering of the item_bank_attributes
    let newItemBankAttributes = reorderList(
      copyOfItemBankAttributes,
      result.source.index,
      result.destination.index
    );
    // Changing the order values
    newItemBankAttributes = adjustOrders(newItemBankAttributes);

    // reordering of the attributesCollapsedStatusArray
    const newAttributesCollapsedStatusArray = reorderList(
      _.cloneDeep(this.state.attributesCollapsedStatusArray),
      result.source.index,
      result.destination.index
    );
    this.setState({
      attributesCollapsedStatusArray: newAttributesCollapsedStatusArray
    });

    const new_item_bank_data = {
      ...this.props.item_bank_data
    };
    new_item_bank_data.item_bank_attributes = newItemBankAttributes;

    this.props.setItemBankData(new_item_bank_data);
  }

  handleDelete = () => {
    // delete the item attribute
    const objArray = this.props.item_bank_attributes.filter(
      obj => obj.id !== this.state.attributeIdToBeDeleted
    );
    this.handleDeleteAttributeCollapse();
    // reordering array of item bank attributes + updating order values
    objArray.sort(orderByOrder);
    for (let i = 0; i < objArray.length; i++) {
      objArray[i].order = i + 1;
    }
    this.props.replaceItemBankField(ITEM_BANK_ATTRIBUTES, objArray, "order");

    // When deleting an attribute: collapse all variables
    this.collapseAllAttributes();

    this.closeDeleteDialog();
  };

  areAllItemBankAttributesValid = () => {
    // initializing needed variables
    const { item_bank_attributes } = this.props;
    let isValid = true;

    // list of available languages
    const languagesArray = LOCALIZE.getAvailableLanguages();

    // looping in all item bank attributes
    for (let i = 0; i < item_bank_attributes.length; i++) {
      // looping in all languages
      for (let j = 0; j < languagesArray.length; j++) {
        // verify attribute name
        if (!this.isItemBankAttributeValid(languagesArray[j], item_bank_attributes[i])) {
          isValid = false;
          break;
        }
      }
      // verify values
      if (!this.areAttributeValuesValid(item_bank_attributes[i])) {
        isValid = false;
      }
    }
    return isValid;
  };

  // Validation of an Attribute by specifying the language and index of the attribute
  isItemBankAttributeValid = (language, item_bank_attribute) => {
    if (
      typeof item_bank_attribute.item_bank_attribute_text === "undefined" ||
      typeof item_bank_attribute.item_bank_attribute_text[language] === "undefined" ||
      item_bank_attribute.item_bank_attribute_text[language][0].text === ""
    ) {
      return false;
    }
    return true;
  };

  // Validate values of an attribute
  areAttributeValuesValid = item_bank_attribute => {
    // Verify if there's any value
    if (item_bank_attribute.item_bank_attribute_values.length <= 0) {
      return false;
    }
    // looping in item bank attribute values of current item bank attribute
    for (let i = 0; i < item_bank_attribute.item_bank_attribute_values.length; i++) {
      // making sure that the value identifier is defined
      if (item_bank_attribute.item_bank_attribute_values[i].value.text === "") {
        return false;
      }
    }
    return true;
  };

  handleUploadData = () => {
    // making sure that all item bank attributes are valid
    if (this.areAllItemBankAttributesValid()) {
      const data = this.props.item_bank_data;
      data.element_to_update = ELEMENT_TO_UPDATE.itemBankAttribute;

      this.props.updateItemBankData(data).then(response => {
        if (!response.error) {
          // updating item bank attribute redux ids
          // updating redux states
          this.props
            .getSelectedItemBankData(
              this.props.item_bank_data.item_bank_definition[0].custom_item_bank_id
            )
            .then(response => {
              const copyOfItemBankData = this.props.item_bank_data;
              copyOfItemBankData.item_bank_attributes = response.item_bank_attributes;
              copyOfItemBankData.item_bank_items = response.item_bank_items;
              this.props.setItemBankData(_.cloneDeep(copyOfItemBankData));

              // Set the values for the initial data of the Item Bank + Item Bank Attributes
              // These are used to detect changes on multiple components of Item Bank
              this.props.setItemBankInitialData(_.cloneDeep(copyOfItemBankData));
              this.props.setItemBankAttributesInitialData(
                _.cloneDeep(copyOfItemBankData.item_bank_attributes)
              );
            });

          // should never happen (except if you did not provide correctly all needed parameters)
        } else {
          throw new Error("Something went wrong during the update item bank data process");
        }
      });
    }
  };

  openDeleteDialog = id => {
    this.setState({ showDeleteDialog: true, attributeIdToBeDeleted: id });
  };

  closeDeleteDialog = () => {
    this.setState({ showDeleteDialog: false, attributeIdToBeDeleted: undefined });
  };

  draggableSwitch = index => {
    const newArray = _.cloneDeep(this.state.attributesCollapsedStatusArray);

    newArray[index].isCollapsed = !newArray[index].isCollapsed;

    this.setState({ attributesCollapsedStatusArray: newArray });
  };

  // Verify if any of the attributes has been expended
  // If yes: disable draggable
  // Else: enable draggable
  verifyCollapsedStatus = () => {
    let isDraggableStatus = true;

    for (let i = 0; i < this.state.attributesCollapsedStatusArray.length; i++) {
      if (!this.state.attributesCollapsedStatusArray[i].isCollapsed) {
        isDraggableStatus = false;
        break;
      }
    }
    this.setState({ isDraggable: isDraggableStatus });
  };

  // Initialize an array with all attributes with their collapsed status
  populateInitialAttributesCollapsedStatusArray = () => {
    const newArray = [];

    this.props.item_bank_attributes.forEach((item_bank_attribute, index) => {
      newArray[index] = {
        id: item_bank_attribute.id,
        isCollapsed: true
      };
    });
    return newArray;
  };

  collapseAllAttributes = () => {
    this.setState({
      attributesCollapsedStatusArray: this.populateInitialAttributesCollapsedStatusArray(),
      triggerCollapseAll: !this.state.triggerCollapseAll
    });
  };

  handleCreateNewAttributeCollapse = id => {
    const newArray = _.cloneDeep(this.state.attributesCollapsedStatusArray);

    newArray[newArray.length] = {
      id: id,
      isCollapsed: true
    };

    return newArray;
  };

  handleDeleteAttributeCollapse = () => {
    const id = this.state.attributeIdToBeDeleted;
    const filteredArray = this.state.attributesCollapsedStatusArray.filter(
      attribute => attribute.id !== id
    );
    this.setState({
      attributesCollapsedStatusArray: filteredArray
    });
  };

  // will return true if changes are detected
  changesDetected = () => {
    if (_.isEqual(this.props.item_bank_attributes, this.props.itemBankAttributesInitialData)) {
      return false;
    }
    return true;
  };

  // will disable the upload button
  isUploadDisabled = () => {
    // if there aren't any changes
    if (!this.changesDetected()) {
      return true;
    }
    // if there are changes detected and attributes aren't valid
    if (this.changesDetected() && !this.areAllItemBankAttributesValid()) {
      return true;
    }

    return false;
  };

  /*--------------------------*/
  //                          //
  //        Styling           //
  //                          //
  /*--------------------------*/

  // Draggable Styling
  getDraggableStyle = (isDragging, draggableStyle) => ({
    // some basic styles to make the items look a bit nicer
    userSelect: "none",

    // change background colour if dragging
    background: isDragging ? "#0099A8" : "white",
    color: "#00565e",

    // styles we need to apply on draggables
    ...draggableStyle
  });

  // Droppable Styling
  getDroppableStyle = isDraggingOver => ({
    background: isDraggingOver ? "#cdcdcd" : "white"
  });

  render() {
    // calculating the height of the top tabs and above (without considering the nav bar)
    // =========================================================================================
    let heightOfBackToItemBankSelectionButton = 0;
    let heightOfTopTabs = 0;
    let topPageHeight = 0;
    if (document.getElementById("back-to-item-bank-selection-btn") != null) {
      heightOfBackToItemBankSelectionButton = document.getElementById(
        "back-to-item-bank-selection-btn"
      ).offsetHeight;
      if (document.getElementsByClassName("nav nav-tabs").length > 0) {
        heightOfTopTabs = document.getElementsByClassName("nav nav-tabs")[0].offsetHeight;
      }
    } else {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }

    topPageHeight += heightOfBackToItemBankSelectionButton + heightOfTopTabs + 24; // height of "back to item bank selection button" + height of margin under button

    const customHeight = getItemBankHeightCalculations(
      this.props.accommodations.fontSize,
      this.props.accommodations.spacing,
      this.props.testNavBarHeight,
      topPageHeight,
      0
    );
    // =========================================================================================
    return (
      <div style={{ ...styles.mainContainer, ...customHeight.maxHeight }}>
        <Container>
          {this.props.itemsTableRendering && (
            <div style={styles.loading}>
              {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
              <label className="fa fa-spinner fa-spin">
                <FontAwesomeIcon icon={faSpinner} />
              </label>
            </div>
          )}
          {!this.props.itemsTableRendering && (
            <div>
              <div>
                <div style={styles.createButton}>
                  <CustomButton
                    label={
                      <>
                        <FontAwesomeIcon icon={faPlusCircle} style={styles.buttonIcon} />
                        <span>
                          {
                            LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                              .itemAttributes.createNewButton
                          }
                        </span>
                      </>
                    }
                    action={this.handleCreateNewItemAttribute}
                    buttonTheme={THEME.PRIMARY}
                  />
                </div>
                {!this.state.isDraggable && (
                  <div style={styles.collapseAllButton}>
                    <CustomButton
                      label={
                        <>
                          <FontAwesomeIcon icon={faChevronCircleUp} style={styles.buttonIcon} />
                          <span>
                            {
                              LOCALIZE.testBuilder.testDefinition.selectTestDefinition
                                .itemBankEditor.itemAttributes.collapseAll
                            }
                          </span>
                        </>
                      }
                      action={this.collapseAllAttributes}
                      buttonTheme={THEME.SECONDARY}
                    />
                  </div>
                )}
              </div>
              <div>
                <DragDropContext
                  onDragEnd={result => {
                    this.onDragEndItemBankAttributeSection(result);
                  }}
                >
                  <Droppable
                    isDropDisabled={!this.state.isDraggable}
                    droppableId="item-bank-attribute-section-droppable"
                  >
                    {(provided, snapshot) => (
                      <div
                        {...provided.droppableProps}
                        ref={provided.innerRef}
                        style={this.getDroppableStyle(snapshot.isDraggingOver)}
                      >
                        {this.props.item_bank_attributes.map((item_bank_attribute, index) => (
                          <CustomDraggable
                            item_bank_attribute={item_bank_attribute}
                            index={index}
                            getItemStyle={this.getDraggableStyle}
                            deleteItem={() => {
                              this.openDeleteDialog(item_bank_attribute.id);
                            }}
                            currentLanguage={this.props.currentLanguage}
                            sideButtonsOnTop={true}
                            isDragDisabled={!this.state.isDraggable}
                            isDraggable={this.state.isDraggable}
                            draggableSwitch={() => {
                              this.draggableSwitch(index);
                            }}
                            triggerCollapse={this.state.triggerCollapseAll}
                          />
                        ))}
                        {provided.placeholder}
                      </div>
                    )}
                  </Droppable>
                </DragDropContext>
              </div>
              <div style={styles.uploadDataButtonContainer}>
                <CustomButton
                  label={
                    <>
                      <FontAwesomeIcon
                        icon={this.changesDetected() ? faUpload : faCheck}
                        style={styles.buttonIcon}
                      />
                      <span>
                        {this.changesDetected()
                          ? LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                              .itemAttributes.uploadDataButton
                          : LOCALIZE.commons.saved}
                      </span>
                    </>
                  }
                  action={this.handleUploadData}
                  customStyle={styles.uploadDataButton}
                  buttonTheme={this.changesDetected() ? THEME.PRIMARY : THEME.SUCCESS}
                  disabled={this.isUploadDisabled()}
                />
              </div>
            </div>
          )}
        </Container>
        <PopupBox
          show={this.state.showDeleteDialog}
          handleClose={this.closeDeleteDialog}
          title={
            LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
              .deleteConfirmationPopup.title
          }
          description={
            <SystemMessage
              messageType={MESSAGE_TYPE.error}
              title={LOCALIZE.commons.warning}
              message={
                <p>
                  {
                    LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                      .deleteConfirmationPopup.description
                  }
                </p>
              }
            />
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={LOCALIZE.commons.deleteButton}
          rightButtonAction={this.handleDelete}
          size="lg"
        />
      </div>
    );
  }
}

export { ItemAttributes as unconnectedItemAttributes };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    item_bank_data: state.itemBank.item_bank_data,
    item_bank_definition: state.itemBank.item_bank_data.item_bank_definition[0],
    item_bank_attributes: state.itemBank.item_bank_data.item_bank_attributes,
    itemBankAttributesInitialData: state.itemBank.itemBankAttributesInitialData,
    testNavBarHeight: state.testSection.testNavBarHeight,
    currentTab: state.navTabs.currentTopTab,
    itemsTableRendering: state.itemBank.itemsTableRendering
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      addItemBankField,
      replaceItemBankField,
      updateItemBankData,
      setItemBankData,
      setItemBankInitialData,
      setItemBankAttributesInitialData,
      getSelectedItemBankData
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ItemAttributes);
