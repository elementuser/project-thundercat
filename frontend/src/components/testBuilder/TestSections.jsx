import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlusCircle } from "@fortawesome/free-solid-svg-icons";
import { Button } from "react-bootstrap";
import CollapsingItemContainer from "../eMIB/CollapsingItemContainer";
import TestSectionForm from "./TestSectionForm";
import {
  getNextInNumberSeries,
  collapsingItemMoveUpFunctionality,
  collapsingItemMoveDownFunctionality,
  deleteQuestionData,
  deleteQuestionRulesData
} from "./helpers";
import {
  addTestDefinitionField,
  replaceTestDefinitionField,
  setTestDefinitionValidationErrors,
  PAGE_SECTIONS,
  PAGE_SECTION_DEFINITIONS,
  TEST_SECTION_COMPONENTS,
  SECTION_COMPONENT_PAGES,
  TEST_SECTIONS,
  NEXT_SECTION_BUTTONS
} from "../../modules/TestBuilderRedux";
import { LANGUAGES } from "../../modules/LocalizeRedux";
import questionListSubSectionValidation, {
  subSectionsValidation,
  sectionComponentPagesValidation,
  validateTestSections
} from "./validation";

export const styles = {
  mainContainer: {
    width: "100%"
  },
  collapsingContainer: {
    width: "90%",
    margin: "auto"
  },
  input: {
    width: 270,
    height: 38,
    padding: "3px 6px 3px 6px",
    borderRadius: 4,
    display: "block"
  },
  label: {
    padding: "3px 0 0 4px"
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  popover: {
    zIndex: 9999,
    padding: "0 12px",
    margin: 0
  },
  borderBox: {
    borderBottom: "1px solid",
    borderRadius: "5px",
    padding: "5px",
    borderColor: "#CECECE",
    color: "red"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6
  },
  rowStyle: {
    margin: "5px"
  },
  validationErrorMessageContainer: {
    border: "1px solid #923534",
    padding: 12,
    marginBottom: 12
  },
  validationErrorMessage: {
    textAlign: "center",
    color: "#923534",
    fontWeight: "bold"
  }
};

class TestSections extends Component {
  state = {
    showMinimumOfThreeTestSectionsError: false,
    showDoesNotContainQuitSectionError: false,
    showDoesNotContainFinishSectionError: false
  };

  componentDidMount = () => {
    this.getValidationErrors();
  };

  componentDidUpdate = prevProps => {
    // if any of the upload validation error state gets updated
    if (prevProps.validationErrors !== this.props.validationErrors) {
      this.getValidationErrors();
    }
    // if testSections size gets updated
    if (prevProps.testSections.length !== this.props.testSections.length) {
      this.getTestSectionOrders();
      // initializing copyOfValidationErrors
      let copyOfValidationErrors = this.props.validationErrors;
      // calling test sections validation
      copyOfValidationErrors = validateTestSections(
        copyOfValidationErrors,
        this.props.testSections
      );
      // calling question list sub-section validation
      copyOfValidationErrors = questionListSubSectionValidation(
        copyOfValidationErrors,
        this.props.testSectionComponents,
        this.props.questions
      );
      // calling sub-sections validation
      copyOfValidationErrors = subSectionsValidation(
        copyOfValidationErrors,
        this.props.testSections,
        this.props.testSectionComponents
      );
      // calling section component pages (page content titles) validation
      copyOfValidationErrors = sectionComponentPagesValidation(
        copyOfValidationErrors,
        this.props.testSections,
        this.props.testSectionComponents,
        this.props.sectionComponentPages
      );
      // updating redux states
      this.props.setTestDefinitionValidationErrors(copyOfValidationErrors);
      // get validation errors
      this.getValidationErrors();
    }
    // if test definition field gets updated
    if (
      prevProps.triggerTestDefinitionFieldUpdates !== this.props.triggerTestDefinitionFieldUpdates
    ) {
      // get validation errors
      this.getValidationErrors();
    }
  };

  getValidationErrors = () => {
    // initializing needed variables
    let showMinimumOfThreeTestSectionsError = false;
    let showDoesNotContainQuitSectionError = false;
    let showDoesNotContainFinishSectionError = false;

    // making sure the the validation errors state is defined
    if (typeof this.props.validationErrors !== "undefined") {
      // at least three test sections error
      if (!this.props.validationErrors.testDefinitionContainsAtLeastThreeTestSections) {
        showMinimumOfThreeTestSectionsError = true;
      }
      // at least a Quit test section error
      if (!this.props.validationErrors.testDefinitionContainsQuitTestSection) {
        showDoesNotContainQuitSectionError = true;
      }
      // at least a Finish test section error
      if (!this.props.validationErrors.testDefinitionContainsFinishTestSection) {
        showDoesNotContainFinishSectionError = true;
      }
    }
    // updating needed states
    this.setState({
      showMinimumOfThreeTestSectionsError: showMinimumOfThreeTestSectionsError,
      showDoesNotContainQuitSectionError: showDoesNotContainQuitSectionError,
      showDoesNotContainFinishSectionError: showDoesNotContainFinishSectionError
    });
  };

  addTestSection = () => {
    const newObj = {
      id: 0,
      order: 1,
      section_type: 2,
      default_time: null,
      en_title: "",
      fr_title: "",
      next_section_button_type: 0,
      scoring_type: 0,
      minimum_score: 0,
      uses_notepad: false,
      uses_calculator: false,
      block_cheating: false,
      item_exposure: false,
      default_tab: null,
      test_definition: 1
    };
    newObj.order = getNextInNumberSeries(this.props.testSections, "order");
    newObj.id = getNextInNumberSeries(this.props.testSections, "id");
    this.props.addTestDefinitionField(TEST_SECTIONS, newObj, newObj.order);

    // getting updated test sections
    let updatedTestSections = this.props.testSections;
    updatedTestSections = updatedTestSections.push(newObj);
    // initializing copyOfValidationErrors
    let copyOfValidationErrors = this.props.validationErrors;
    // calling test sections validation
    copyOfValidationErrors = validateTestSections(copyOfValidationErrors, updatedTestSections);
    // updating redux states
    this.props.setTestDefinitionValidationErrors(copyOfValidationErrors);
  };

  handleDelete = id => {
    // delete the test section component
    let objArray = this.props.testSections.filter(obj => obj.id !== id);
    this.props.replaceTestDefinitionField(TEST_SECTIONS, objArray);

    // delete the next section button
    objArray = this.props.nextSectionButtons.filter(obj => obj.test_section !== id);
    this.props.replaceTestDefinitionField(NEXT_SECTION_BUTTONS, objArray);

    // delete the section component page
    const sectionCompsToDelete = [];
    objArray = this.props.testSectionComponents.filter(obj => {
      if (obj.test_section.indexOf(id) === -1) {
        return obj;
      }
      // eslint-disable-next-line no-param-reassign
      obj.test_section = obj.test_section.filter(ts => ts !== id);
      if (obj.test_section.length > 0) {
        return obj;
      }
      sectionCompsToDelete.push(obj.id);
      return null;
    });
    this.props.replaceTestDefinitionField(TEST_SECTION_COMPONENTS, objArray);

    // delete the section component page
    const pageSectionsToDelete = [];
    objArray = this.props.sectionComponentPages.filter(obj => {
      if (sectionCompsToDelete.indexOf(obj.test_section_component) === -1) {
        return obj;
      }
      pageSectionsToDelete.push(obj.id);
      return null;
    });
    this.props.replaceTestDefinitionField(SECTION_COMPONENT_PAGES, objArray);

    // delete the page section
    const pageSectionDefinitionsToDelete = [];
    objArray = this.props.pageSections.filter(obj => {
      if (pageSectionsToDelete.indexOf(obj.section_component_page) === -1) {
        return obj;
      }
      pageSectionDefinitionsToDelete.push(obj.id);
      return null;
    });
    this.props.replaceTestDefinitionField(PAGE_SECTIONS, objArray);

    // delete the page section definitions
    objArray = this.props.pageSectionDefinitions.filter(
      obj => pageSectionDefinitionsToDelete.indexOf(obj.page_section) === -1
    );
    this.props.replaceTestDefinitionField(PAGE_SECTION_DEFINITIONS, objArray);

    // deleting question data
    deleteQuestionData(
      this.props.replaceTestDefinitionField,
      sectionCompsToDelete,
      this.props.questions,
      this.props.questionSections,
      this.props.questionSectionDefinitions,
      this.props.answers,
      this.props.answerDetails,
      this.props.multipleChoiceQuestionDetails
    );

    // deleting question rules data
    deleteQuestionRulesData(
      this.props.replaceTestDefinitionField,
      sectionCompsToDelete,
      this.props.questionListRules
    );
  };

  handleMoveUp = index => {
    // getting new test sections array
    collapsingItemMoveUpFunctionality(index, this.props.testSections).then(objArray => {
      // updating redux states
      this.props.replaceTestDefinitionField(TEST_SECTIONS, objArray);
    });
  };

  handleMoveDown = index => {
    // getting new test sections array
    collapsingItemMoveDownFunctionality(index, this.props.testSections).then(objArray => {
      // updating redux states
      this.props.replaceTestDefinitionField(TEST_SECTIONS, objArray);
    });
  };

  getTestSectionOrders = () => {
    const { testSections } = this.props;
    // initializing newTestSectionComponents array
    const newTestSections = [];

    // looping in testSections
    for (let i = 0; i < testSections.length; i++) {
      // pushing new object (with the right order) in newTestSectionComponents array
      newTestSections.push(testSections[i]);
      newTestSections[i].order = i + 1;
    }
    // updating redux state
    this.props.replaceTestDefinitionField(TEST_SECTIONS, newTestSections);
  };

  render() {
    const { testSections } = this.props;

    const customFontSize = {
      // adding 5px to current font size
      fontSize: `${parseInt(this.props.accommodations.fontSize.split("px")[0]) + 5}px`
    };

    return (
      <div style={styles.mainContainer}>
        <h2>{LOCALIZE.testBuilder.testSection.title}</h2>
        {(this.state.showMinimumOfThreeTestSectionsError ||
          this.state.showDoesNotContainFinishSectionError ||
          this.state.showDoesNotContainQuitSectionError) && (
          <div style={styles.validationErrorMessageContainer}>
            {this.state.showMinimumOfThreeTestSectionsError && (
              <p style={{ ...styles.validationErrorMessage, ...customFontSize }}>
                {
                  LOCALIZE.testBuilder.testSection.validationErrors
                    .mustContainAtLeastThreeTestSectionError
                }
              </p>
            )}
            {!this.state.showMinimumOfThreeTestSectionsError &&
              this.state.showDoesNotContainFinishSectionError && (
                <p style={{ ...styles.validationErrorMessage, ...customFontSize }}>
                  {
                    LOCALIZE.testBuilder.testSection.validationErrors
                      .mustContainFinishTestSectionError
                  }
                </p>
              )}
            {!this.state.showMinimumOfThreeTestSectionsError &&
              this.state.showDoesNotContainQuitSectionError && (
                <p style={{ ...styles.validationErrorMessage, ...customFontSize }}>
                  {
                    LOCALIZE.testBuilder.testSection.validationErrors
                      .mustContainQuitTestSectionError
                  }
                </p>
              )}
          </div>
        )}
        <p>{LOCALIZE.testBuilder.testSection.description}</p>
        <Button variant={"primary"} onClick={this.addTestSection}>
          <FontAwesomeIcon icon={faPlusCircle} style={{ margins: "auto", marginRight: "5px" }} />
          {LOCALIZE.testBuilder.testSection.addButton}
        </Button>
        <div style={styles.borderBox} />
        {testSections.map((section, index) => {
          let enButtonDetails = {};
          let frButtonDetails = {};
          for (const obj of this.props.nextSectionButtons) {
            if (obj.test_section === section.id && obj.language === LANGUAGES.english) {
              enButtonDetails = obj;
            }
            if (obj.test_section === section.id && obj.language === LANGUAGES.french) {
              frButtonDetails = obj;
            }
          }
          enButtonDetails.test_section = section.id;
          frButtonDetails.test_section = section.id;

          return (
            <CollapsingItemContainer
              key={index}
              index={index}
              usesMoveUpAndDownFunctionalities={true}
              moveUpAction={() => this.handleMoveUp(index)}
              moveUpButtonDisabled={index === 0}
              moveDownAction={() => this.handleMoveDown(index)}
              moveDownButtonDisabled={index === testSections.length - 1}
              title={
                <label>
                  {LOCALIZE.formatString(
                    LOCALIZE.testBuilder.testSection.collapsableItemName,
                    section.order,
                    section[`${this.props.currentLanguage}_title`]
                  )}
                </label>
              }
              body={
                <TestSectionForm
                  testSection={section}
                  INDEX={index}
                  enButtonDetails={enButtonDetails}
                  frButtonDetails={frButtonDetails}
                  handleDelete={this.handleDelete}
                />
              }
            />
          );
        })}
      </div>
    );
  }
}

export { TestSections as unconnectedTestSections };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    testSections: state.testBuilder.test_sections,
    testSectionComponents: state.testBuilder.test_section_components,
    sectionComponentPages: state.testBuilder.section_component_pages,
    pageSections: state.testBuilder.page_sections,
    pageSectionDefinitions: state.testBuilder.page_section_definitions,
    nextSectionButtons: state.testBuilder.next_section_buttons,
    validationErrors: state.testBuilder.validation_errors,
    accommodations: state.accommodations,
    questions: state.testBuilder.questions,
    questionSections: state.testBuilder.question_sections,
    questionSectionDefinitions: state.testBuilder.question_section_definitions,
    questionListRules: state.testBuilder.question_list_rules,
    answers: state.testBuilder.answers,
    answerDetails: state.testBuilder.answer_details,
    multipleChoiceQuestionDetails: state.testBuilder.multiple_choice_question_details,
    triggerTestDefinitionFieldUpdates: state.testBuilder.triggerTestDefinitionFieldUpdates
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    { addTestDefinitionField, replaceTestDefinitionField, setTestDefinitionValidationErrors },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(TestSections);
