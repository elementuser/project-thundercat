import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import CustomButton, { THEME } from "../commons/CustomButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUpload, faFolderOpen, faSpinner } from "@fortawesome/free-solid-svg-icons";
import {
  setSelectedTestDefinitionId,
  triggerTestDefinitionLoadingFromJson
} from "../../modules/TestBuilderRedux";
import StyledTooltip, { TYPE, EFFECT, TRIGGER_TYPE } from "../authentication/StyledTooltip";
import "../../css/test-builder.css";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";
import { BUTTON_STATE } from "../commons/PopupBox";

const styles = {
  mainDiv: {
    width: "100%"
  },
  mainContainer: {
    display: "flex"
  },
  FieldsContainer: {
    margin: 24
  },
  errorMessageContainer: {
    border: "1px solid #923534",
    padding: 12,
    marginBottom: 12,
    color: "#923534",
    fontWeight: "bold",
    textAlign: "center"
  },
  uploadSuccessMessage: {
    textAlign: "center",
    color: "#278400",
    fontWeight: "bold",
    marginBottom: 12
  },
  uploadButtonContainer: {
    marginTop: 24
  },
  uploadButton: {
    minWidth: 150
  },
  buttonLabel: {
    marginLeft: 6
  },
  popupCustomStyle: {
    minWidth: 850
  },
  jsonUploadMainContainer: {
    width: "50%",
    position: "relative"
  },
  jsonUploadInputContainer: {
    width: "100%",
    float: "left"
  },
  uploadIconContainer: {
    position: "absolute",
    right: 0
  },
  uploadIconButton: {
    padding: "3px 12px",
    minWidth: 25,
    minHeight: 38,
    borderLeft: "1px solid #00565e",
    borderRadius: "0 4px 4px 0"
  },
  input: {
    width: "100%",
    minHeight: 38,
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565e",
    borderRadius: 4
  },
  jsonUploadTextField: {
    padding: "3px 26% 3px 6px",
    textOverflow: "ellipsis"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6,
    width: "100%",
    textAlign: "left"
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  }
};

class UploadJson extends Component {
  state = {
    isFirstLoad: true,
    isUploadLoading: false,
    showInvalidJsonErrorMessage: false,
    newUploadedTestData: {},
    showUploadJsonSuccessMessage: false,
    isLoading: false,
    isUploadButtonDisabled: true,
    selectedFileDetails: {
      name: ""
    },
    isValidFile: true,
    displayInvalidFileTypeError: false,
    selectedFileData: {}
  };

  uploadJSON = () => {
    this.setState({ isUploadLoading: true }, () => {
      try {
        // converting selected file data (test definition data) to JSON
        const testDefinitionDataJsonFormat = JSON.parse(this.state.selectedFileData).new_test;
        // making sure that we don't update the triggerTestDefinitionLoading redux state to not trigger the 'populateTestDefinition' function in TestBuilder component
        testDefinitionDataJsonFormat.triggerTestDefinitionLoading =
          this.props.triggerTestDefinitionLoading;
        // setting redux states
        this.props.setSelectedTestDefinitionId("TEMP-1");
        this.props.triggerTestDefinitionLoadingFromJson(testDefinitionDataJsonFormat);
        // invalid JSON file
      } catch {
        this.setState({ showInvalidJsonErrorMessage: true, isUploadLoading: false });
      }
      // resetting ref value, so the onChange function is called everytime (allows users to select the same file multiple times in a row)
      this.upload.value = "";
    });
  };

  handleUploadFile = () => {
    // open file explorer
    this.upload.click();
  };

  onFileSelection = event => {
    // making sure that the target.files array contains a file (length will be 0 if the user select "cancel" on the file explorer and you don't want to override the previous state in that case)
    if (event.target.files.length > 0) {
      // updating states (including the selected file details and all success/failed file related states)
      this.setState(
        {
          isLoading: true,
          selectedFileDetails: event.target.files[0],
          showUploadJsonSuccessMessage: false,
          displayInvalidFileTypeError: false,
          showInvalidJsonErrorMessage: false
        },
        () => {
          event.stopPropagation();
          event.preventDefault();

          // making sure that the selected file is a json file
          // valid file type
          if (this.state.selectedFileDetails.type === "application/json") {
            // parsing file data
            const fileReader = new FileReader();
            fileReader.readAsText(this.state.selectedFileDetails, "UTF-8");
            fileReader.onload = event => {
              // updating displayInvalidFileTypeError state
              this.setState({
                isValidFile: true,
                displayInvalidFileTypeError: false,
                selectedFileDetails: this.state.selectedFileDetails,
                selectedFileData: event.target.result,
                isLoading: false
              });
            };

            // invalid file type
          } else {
            // updating needed state (including reinitializing the selectedFileDetails and selectedFileData states)
            this.setState(
              {
                displayInvalidFileTypeError: true,
                showInvalidJsonErrorMessage: false,
                selectedFileDetails: { name: "" },
                selectedFileData: {},
                isValidFile: false,
                isLoading: false
              },
              () => {
                // focusing on candidates upload input field
                document.getElementById("json-upload-text-field").focus();
              }
            );
          }
        }
      );
    }
  };

  render() {
    const customFontSize = {
      // adding 5px to current font size
      fontSize: `${parseInt(this.props.accommodations.fontSize.split("px")[0]) + 5}px`
    };

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <div style={styles.mainDiv}>
        <h2>{LOCALIZE.testBuilder.testDefinition.selectTestDefinition.uploadJson.title}</h2>
        <p>{LOCALIZE.testBuilder.testDefinition.selectTestDefinition.uploadJson.description}</p>
        <div style={styles.FieldsContainer}>
          {this.state.showInvalidJsonErrorMessage && (
            <div style={{ ...styles.errorMessageContainer, ...customFontSize }}>
              <p>
                {
                  LOCALIZE.testBuilder.testDefinition.selectTestDefinition.uploadJson
                    .invalidJsonError
                }
              </p>
            </div>
          )}
          {this.state.showUploadJsonSuccessMessage && (
            <div style={{ ...styles.uploadSuccessMessage, ...customFontSize }}>
              <p>
                {LOCALIZE.formatString(
                  LOCALIZE.testBuilder.testDefinition.selectTestDefinition.uploadJson
                    .uploadJsonSuccessful,
                  this.state.newUploadedTestData.parent_code,
                  this.state.newUploadedTestData.test_code,
                  this.state.newUploadedTestData.version
                )}
              </p>
            </div>
          )}
          <div>
            <label id="upload-json-text-area-label" htmlFor="upload-json-text-area">
              {LOCALIZE.testBuilder.testDefinition.selectTestDefinition.uploadJson.textAreaLabel}
            </label>
          </div>
          <div style={styles.mainContainer}>
            <div style={styles.jsonUploadMainContainer}>
              <div style={styles.jsonUploadInputContainer}>
                {!this.state.isLoading && (
                  <input
                    id="json-upload-text-field"
                    className={
                      this.state.isValidFile && !this.state.showInvalidJsonErrorMessage
                        ? "valid-field"
                        : "invalid-field"
                    }
                    aria-invalid={!this.state.isValidFile}
                    type="text"
                    style={{
                      ...styles.input,
                      ...styles.jsonUploadTextField,
                      ...accommodationsStyle
                    }}
                    value={this.state.selectedFileDetails.name}
                    onChange={() => {}}
                    readOnly={true}
                  ></input>
                )}
                <label htmlFor="json-upload-text-field" style={styles.hiddenText}>
                  {LOCALIZE.formatString(
                    LOCALIZE.testBuilder.testDefinition.selectTestDefinition.uploadJson
                      .fileSelectedAccessibility,
                    this.state.selectedFileDetails.name
                  )}
                </label>
              </div>
              <div style={styles.uploadIconContainer}>
                <StyledTooltip
                  id="json-upload-button-tooltip"
                  place="top"
                  type={TYPE.light}
                  effect={EFFECT.solid}
                  triggerType={[TRIGGER_TYPE.focus, TRIGGER_TYPE.hover]}
                  tooltipElement={
                    <CustomButton
                      dataTip=""
                      dataFor="json-upload-button-tooltip"
                      label={
                        <>
                          <FontAwesomeIcon icon={faFolderOpen} />
                          <label style={styles.hiddenText}>
                            {
                              LOCALIZE.testBuilder.testDefinition.selectTestDefinition.uploadJson
                                .browseButton
                            }
                          </label>
                        </>
                      }
                      action={this.handleUploadFile}
                      customStyle={styles.uploadIconButton}
                      buttonTheme={`${THEME.SECONDARY} ${
                        this.state.isValidFile && !this.state.showInvalidJsonErrorMessage
                          ? ""
                          : "upload-json-custom-browse-button-error"
                      }`}
                    />
                  }
                  tooltipContent={
                    <div>
                      <p>
                        {
                          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.uploadJson
                            .browseButton
                        }
                      </p>
                    </div>
                  }
                />
                <input
                  id="file-explorer-handler"
                  type="file"
                  tabIndex={-1}
                  // eslint-disable-next-line no-return-assign
                  ref={ref => (this.upload = ref)}
                  style={styles.hiddenText}
                  onChange={this.onFileSelection.bind(this)}
                />
              </div>
            </div>
          </div>
          {this.state.displayInvalidFileTypeError && (
            <label
              htmlFor="json-upload-text-field"
              style={styles.errorMessage}
              className="notranslate"
            >
              {
                LOCALIZE.testBuilder.testDefinition.selectTestDefinition.uploadJson
                  .wrongFileTypeError
              }
            </label>
          )}
          <div style={styles.uploadButtonContainer}>
            <CustomButton
              label={
                this.state.isUploadLoading ? (
                  // eslint-disable-next-line jsx-a11y/label-has-associated-control
                  <label className="fa fa-spinner fa-spin">
                    <FontAwesomeIcon icon={faSpinner} />
                  </label>
                ) : (
                  <>
                    <FontAwesomeIcon icon={faUpload} />
                    <span style={styles.buttonLabel}>
                      {
                        LOCALIZE.testBuilder.testDefinition.selectTestDefinition.uploadJson
                          .uploadJsonButton
                      }
                    </span>
                  </>
                )
              }
              action={this.uploadJSON}
              buttonTheme={THEME.PRIMARY}
              customStyle={styles.uploadButton}
              disabled={
                this.state.isUploadLoading
                  ? BUTTON_STATE.disabled
                  : Object.entries(this.state.selectedFileData).length > 0
                  ? BUTTON_STATE.enabled
                  : BUTTON_STATE.disabled
              }
            />
          </div>
        </div>
      </div>
    );
  }
}

export { UploadJson as unconnectedUploadJson };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    triggerTestDefinitionLoading: state.testBuilder.triggerTestDefinitionLoading,
    triggerTestDefinitionLoadingFromJson: state.testBuilder.triggerTestDefinitionLoadingFromJson
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setSelectedTestDefinitionId,
      triggerTestDefinitionLoadingFromJson
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(UploadJson);
