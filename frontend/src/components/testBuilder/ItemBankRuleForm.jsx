import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Row } from "react-bootstrap";
import { makeLabel, makeTextBoxField, makeSaveDeleteButtons, makeDropDownField } from "./helpers";
import {
  modifyTestDefinitionField,
  replaceTestDefinitionField,
  triggerTestDefinitionFieldUpdates,
  setQuestionRulesValidState,
  ITEM_BANK_RULES
} from "../../modules/TestBuilderRedux";
import { styles } from "./TestSectionForm";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import {
  getItemBankBundlesForTestBuilder,
  getItemBanksForTestBuilder
} from "../../modules/ItemBankRedux";

class ItemBankRuleForm extends Component {
  state = {
    showDialog: false,
    itemBankOptions: [],
    isLoadingItemBankOptions: true,
    itemBankBundleOptions: [],
    isLoadingItemBankBundleOptions: false,
    rule: {}
  };

  componentDidMount = () => {
    // populating item bank options
    this.populateItemBankOptions();
  };

  modifyField = (inputName, value) => {
    const newObj = { ...this.props.rule, ...this.state.rule };
    newObj[`${inputName}`] = value;
    this.setState({ rule: newObj });
  };

  onSelectChange = (event, action) => {
    // updating item bank selection
    if (action.name === "item_bank") {
      // populating item bank bundle options
      this.populateItemBankBundleOptions(event.value);
      // making sure that the item bank bundle selection is an empty object
      this.modifyField("item_bank_bundle", {});
      // adding small delay to make sure both field are properly updated
      setTimeout(() => {
        this.modifyField(action.name, event);
      }, 100);
    } else {
      this.modifyField(action.name, event);
    }
  };

  handleSave = () => {
    const { rule } = this.props;
    this.props.modifyTestDefinitionField(ITEM_BANK_RULES, { ...rule, ...this.state.rule }, rule.id);
  };

  openDialog = () => {
    this.setState({ showDialog: true });
  };

  closeDialog = () => {
    this.setState({ showDialog: false });
  };

  confirmDelete = () => {
    // delete the question list rule
    const { id } = this.props.rule;
    const objArray = this.props.itemBankRules.filter(obj => obj.id !== id);
    // making sure that the order attributes are still the right ones
    for (let i = 0; i < objArray.length; i++) {
      objArray[i].order = i + 1;
    }
    this.props.replaceTestDefinitionField(ITEM_BANK_RULES, objArray);

    this.closeDialog();
    this.props.expandItem();
    // trigger test definition field updates
    this.props.triggerTestDefinitionFieldUpdates();
  };

  populateItemBankOptions = () => {
    // initializing needed variables
    const itemBankOptions = [];
    const rule = { ...this.props.rule, ...this.state.rule };

    this.setState({ isLoadingItemBankOptions: true }, () => {
      // getting item banks
      this.props.getItemBanksForTestBuilder().then(response => {
        // looping in response
        for (let i = 0; i < response.length; i++) {
          itemBankOptions.push({
            label: `${response[i].custom_item_bank_id} (${
              response[i][`${this.props.currentLanguage}_name`]
            })`,
            value: response[i].id,
            language_id: response[i].language
          });
        }
        // updating needed states
        this.setState({ itemBankOptions: itemBankOptions, isLoadingItemBankOptions: false }, () => {
          // item bank has already been selected + there are no bundle options yet
          if (
            Object.entries(rule.item_bank).length > 0 &&
            this.state.itemBankBundleOptions.length <= 0
          ) {
            // populating item bank bundle options
            this.populateItemBankBundleOptions(rule.item_bank.value);
          }
        });
      });
    });
  };

  populateItemBankBundleOptions = itemBankId => {
    // initializing
    const itemBankBundleOptions = [];

    this.setState({ isLoadingItemBankBundleOptions: true }, () => {
      // getting item banks
      this.props.getItemBankBundlesForTestBuilder(itemBankId).then(response => {
        // looping in response
        for (let i = 0; i < response.length; i++) {
          itemBankBundleOptions.push({
            label: response[i].name,
            value: response[i].id
          });
        }
        // updating needed states
        this.setState({
          itemBankBundleOptions: itemBankBundleOptions,
          isLoadingItemBankBundleOptions: false
        });
      });
    });
  };

  render() {
    const rule = { ...this.props.rule, ...this.state.rule };
    const { itemBankOptions, itemBankBundleOptions } = this.state;

    return (
      <>
        <Row style={styles.rowStyle}>
          {makeLabel("order", LOCALIZE.testBuilder.itemBankRules)}
          {makeTextBoxField(
            "order",
            LOCALIZE.testBuilder.itemBankRules,
            rule.order,
            true,
            () => {},
            "item-bank-rules-order",
            true
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("item_bank", LOCALIZE.testBuilder.itemBankRules)}
          {makeDropDownField(
            "item_bank",
            LOCALIZE.testBuilder.itemBankRules,
            rule.item_bank,
            itemBankOptions,
            this.onSelectChange,
            this.state.isLoadingItemBankOptions,
            true
          )}
        </Row>

        {Object.entries(rule.item_bank).length > 0 && (
          <Row style={styles.rowStyle}>
            {makeLabel("item_bank_bundle", LOCALIZE.testBuilder.itemBankRules)}
            {makeDropDownField(
              "item_bank_bundle",
              LOCALIZE.testBuilder.itemBankRules,
              rule.item_bank_bundle,
              itemBankBundleOptions,
              this.onSelectChange,
              this.state.isLoadingItemBankBundleOptions,
              true
            )}
          </Row>
        )}

        <Row className="justify-content-between" style={styles.buttonRowStyle}>
          {makeSaveDeleteButtons(
            this.handleSave,
            this.openDialog,
            LOCALIZE.commons.applyButton,
            LOCALIZE.commons.deleteButton,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            Object.entries(rule.item_bank).length <= 0 ||
              Object.entries(rule.item_bank_bundle).length <= 0
          )}
        </Row>
        <PopupBox
          show={this.state.showDialog}
          handleClose={this.closeDialog}
          title={"Delete Confirmation"}
          description={
            <div>
              <p>
                Deleting this item will delete all the content displayed. Are you sure you want to
                delete this object?
              </p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={LOCALIZE.commons.deleteButton}
          rightButtonAction={this.confirmDelete}
        />
      </>
    );
  }
}

export { ItemBankRuleForm as unconnectedItemBankRuleForm };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    itemBankRules: state.testBuilder.item_bank_rules
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      modifyTestDefinitionField,
      replaceTestDefinitionField,
      triggerTestDefinitionFieldUpdates,
      setQuestionRulesValidState,
      getItemBanksForTestBuilder,
      getItemBankBundlesForTestBuilder
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ItemBankRuleForm);
