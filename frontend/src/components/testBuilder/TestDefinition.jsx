import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Row, Container, Col } from "react-bootstrap";
import {
  makeLabel,
  makeTextBoxField,
  makeTextAreaField,
  noSpecialCharacters,
  isNumberOrEmpty
} from "./helpers";
import {
  modifyTestDefinitionField,
  TEST_DEFINITION,
  setTestDefinitionValidationErrors
} from "../../modules/TestBuilderRedux";
import CustomButton, { THEME } from "../commons/CustomButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck } from "@fortawesome/free-solid-svg-icons";
import getSwitchTransformScale from "../../helpers/switchTransformScale";
import Switch from "react-switch";
import { validateTestDefinitionData } from "./validation";

export const styles = {
  mainContainer: {
    width: "100%"
  },
  borderBox: {
    borderTop: "1px solid",
    borderRadius: "5px",
    padding: "5px",
    borderColor: "#CECECE",
    color: "red",
    margin: "5px"
  },
  rowStyle: {
    margin: "5px"
  },
  applyButtonContainer: {
    textAlign: "center",
    marginTop: 36
  },
  applyButton: {
    minWidth: 200
  },
  buttonLabel: {
    marginLeft: 6
  },
  customLabelForTextAreaStyle: {
    marginTop: 0,
    marginBottom: 0
  },
  customTextAreaStyle: {
    height: 60
  },
  activeState: {
    fontWeight: "bold",
    color: "#278400"
  },
  inactiveState: {
    fontWeight: "bold",
    color: "#923534"
  },
  rightColumnContainer: {
    display: "table-cell",
    verticalAlign: "middle",
    height: 32
  },
  validationErrorMessageContainer: {
    border: "1px solid #923534",
    padding: 12,
    marginBottom: 12
  },
  validationErrorMessage: {
    textAlign: "center",
    color: "#923534",
    fontWeight: "bold"
  }
};

class TestDefinition extends Component {
  localize = LOCALIZE.testBuilder.testDefinition;

  state = {
    test_definition: {},
    isVersionValid: true,
    isRetestValid: true,
    isTestCodeValid: true,
    isParentCodeValid: true,
    isFrNameValid: true,
    isEnNameValid: true,
    // default values
    switchHeight: 25,
    switchWidth: 50,
    areSwitchStatesLoading: false,
    showInvalidTestDefinitionDataError: false
  };

  componentDidMount = () => {
    this.getUpdatedSwitchDimensions();
    this.getValidationErrors();
  };

  componentDidUpdate = prevProps => {
    // if font size gets updated
    if (prevProps.accommodations.fontSize !== this.props.accommodations.fontSize) {
      this.getUpdatedSwitchDimensions();
    }
    // if test definition field gets updated
    if (
      prevProps.triggerTestDefinitionFieldUpdates !== this.props.triggerTestDefinitionFieldUpdates
    ) {
      // getting orders
      this.getValidationErrors();
    }
  };

  modifyField = (inputName, value) => {
    const newObj = { ...this.props.test_definition, ...this.state.test_definition };
    newObj[`${inputName}`] = value;
    this.setState({ test_definition: newObj });
  };

  retestValidation = (name, value) => {
    if (isNumberOrEmpty(value)) {
      this.setState({ isRetestValid: true });
      this.modifyField(name, parseInt(value) || 0);
    }
  };

  testCodeValidation = (name, value) => {
    const orginalValue = value;
    // allow only alphanumeric, slash and dash (0 to 25 chars)
    const regexExpression = /^([a-zA-Z0-9-]{0,25})$/;
    if (regexExpression.test(orginalValue)) {
      if (noSpecialCharacters(value)) {
        this.setState({ isTestCodeValid: true });
        this.modifyField(name, value.toUpperCase());
      } else {
        this.setState({ isTestCodeValid: false });
        this.modifyField(name, orginalValue);
      }
    }
  };

  parentCodeValidation = (name, value) => {
    const orginalValue = value;
    // allow only alphanumeric, slash and dash (0 to 25 chars)
    const regexExpression = /^([a-zA-Z0-9-]{0,25})$/;
    if (regexExpression.test(orginalValue)) {
      if (noSpecialCharacters(value)) {
        this.setState({ isParentCodeValid: true });
        this.modifyField(name, value.toUpperCase());
      } else {
        this.setState({ isParentCodeValid: false });
        this.modifyField(name, orginalValue);
      }
    }
  };

  enNameValidation = (name, value) => {
    // allow maximum of 175 chars (minimum 1 char)
    const regexExpression = /^(.{1,175})$/;
    if (regexExpression.test(value)) {
      this.setState({ isEnNameValid: true });
      this.modifyField(name, value);
    } else {
      this.setState({ isEnNameValid: false });
      this.modifyField(name, value);
    }
  };

  frNameValidation = (name, value) => {
    // allow maximum of 175 chars (minimum 1 char)
    const regexExpression = /^(.{1,175})$/;
    if (regexExpression.test(value)) {
      this.setState({ isFrNameValid: true });
      this.modifyField(name, value);
    } else {
      this.setState({ isFrNameValid: false });
      this.modifyField(name, value);
    }
  };

  getValidationErrors = () => {
    // initializing needed variables
    let showInvalidTestDefinitionDataError = false;

    // making sure the the validation errors state is defined
    if (typeof this.props.validationErrors !== "undefined") {
      // scoring method error
      if (!this.props.validationErrors.testDefinitionContainsValidTestDefinitionData) {
        showInvalidTestDefinitionDataError = true;
      }
    }
    // updating needed states
    this.setState({
      showInvalidTestDefinitionDataError: showInvalidTestDefinitionDataError
    });
  };

  handleSave = () => {
    // updating redux state
    const newObj = {
      ...this.props.test_definition,
      ...this.state.test_definition,
      active: false
    };
    this.props.modifyTestDefinitionField(TEST_DEFINITION, newObj, this.props.test_definition.id);
    // calling field validation
    this.handleFieldsValidation(newObj);
  };

  handleFieldsValidation = testDefinitionData => {
    // initializing needed variables
    let isParentCodeValid = true;
    let isTestCodeValid = true;
    let isEnNameValid = true;
    let isFrNameValid = true;

    // making sure that all required fields are filled
    // Parent Code
    if (
      typeof testDefinitionData.parent_code === "undefined" ||
      testDefinitionData.parent_code === ""
    ) {
      isParentCodeValid = false;
    }
    // Test Code
    if (
      typeof testDefinitionData.test_code === "undefined" ||
      testDefinitionData.test_code === ""
    ) {
      isTestCodeValid = false;
    }
    // English Name
    if (typeof testDefinitionData.en_name === "undefined" || testDefinitionData.en_name === "") {
      isEnNameValid = false;
    }
    // French Name
    if (typeof testDefinitionData.fr_name === "undefined" || testDefinitionData.fr_name === "") {
      isFrNameValid = false;
    }

    // updating states
    this.setState({
      isParentCodeValid: isParentCodeValid,
      isTestCodeValid: isTestCodeValid,
      isEnNameValid: isEnNameValid,
      isFrNameValid: isFrNameValid
    });

    // calling test definition data validation
    const validationErrors = validateTestDefinitionData(
      this.props.validationErrors,
      testDefinitionData
    );

    // updating redux states
    this.props.setTestDefinitionValidationErrors(validationErrors);
  };

  // getting switch dimensions and updating states
  getUpdatedSwitchDimensions = () => {
    // need this loading state, otherwise the switch diameter is not calculated on state change
    this.setState({ areSwitchStatesLoading: true }, () => {
      this.setState(
        {
          switchHeight: getSwitchTransformScale(this.props.accommodations.fontSize).height,
          switchWidth: getSwitchTransformScale(this.props.accommodations.fontSize).width
        },
        () => {
          this.setState({ areSwitchStatesLoading: false });
        }
      );
    });
  };

  handleEnableUIT = () => {
    this.modifyField("is_uit", true);
    this.modifyField("is_public", false);
  };

  handleDisableUIT = () => {
    this.modifyField("is_uit", false);
    this.modifyField("is_public", false);
  };

  handleEnableSampleTest = () => {
    this.modifyField("is_uit", false);
    this.modifyField("is_public", true);
  };

  handleDisableSampleTest = () => {
    this.modifyField("is_public", false);
    this.modifyField("count_up", false);
  };

  handleEnableCountUp = () => {
    this.modifyField("count_up", true);
  };

  handleDisableCountUp = () => {
    this.modifyField("count_up", false);
  };

  render() {
    const test_definition = { ...this.props.test_definition, ...this.state.test_definition };
    const testName = `: ${this.props.test_definition.test_code} v${this.props.test_definition.version}`;

    const customFontSize = {
      // adding 5px to current font size
      fontSize: `${parseInt(this.props.accommodations.fontSize.split("px")[0]) + 5}px`
    };

    return (
      <div style={styles.mainContainer}>
        <h2>{this.localize.title + testName}</h2>
        {this.state.showInvalidTestDefinitionDataError && (
          <div style={styles.validationErrorMessageContainer}>
            <p style={{ ...styles.validationErrorMessage, ...customFontSize }}>
              {LOCALIZE.testBuilder.testDefinition.validationErrors.invalidTestDefinitionDataError}
            </p>
          </div>
        )}

        <Container>
          <div style={styles.borderBox} />
          {this.props.test_definition && (
            <>
              <Row style={styles.rowStyle}>
                {makeLabel("parent_code", this.localize)}
                {makeTextBoxField(
                  "parent_code",
                  this.localize,
                  test_definition.parent_code,
                  this.state.isParentCodeValid,
                  this.parentCodeValidation
                )}
              </Row>
              <Row style={styles.rowStyle}>
                {makeLabel("test_code", this.localize)}
                {makeTextBoxField(
                  "test_code",
                  this.localize,
                  test_definition.test_code,
                  this.state.isTestCodeValid,
                  this.testCodeValidation
                )}
              </Row>
              <Row style={styles.rowStyle}>
                {makeLabel("en_name", this.localize)}
                {makeTextBoxField(
                  "en_name",
                  this.localize,
                  test_definition.en_name,
                  this.state.isEnNameValid,
                  this.enNameValidation
                )}
              </Row>
              <Row style={styles.rowStyle}>
                {makeLabel("fr_name", this.localize)}
                {makeTextBoxField(
                  "fr_name",
                  this.localize,
                  test_definition.fr_name,
                  this.state.isFrNameValid,
                  this.frNameValidation
                )}
              </Row>
              <Row style={styles.rowStyle}>
                {makeLabel("version", this.localize)}
                {makeTextBoxField(
                  "version",
                  this.localize,
                  test_definition.version,
                  this.state.isVersionValid,
                  {},
                  "",
                  true
                )}
              </Row>
              <Row style={styles.rowStyle}>
                {makeLabel("retest_period", this.localize)}
                {makeTextBoxField(
                  "retest_period",
                  this.localize,
                  String(test_definition.retest_period),
                  this.state.isRetestValid,
                  this.retestValidation
                )}
              </Row>
              <Row style={styles.rowStyle}>
                {makeLabel(
                  "version_notes",
                  this.localize,
                  "test-definition-version-notes-label",
                  styles.customLabelForTextAreaStyle
                )}
                {makeTextAreaField(
                  "version_notes",
                  this.localize,
                  test_definition.version_notes,
                  true,
                  {},
                  "test-definition-version-notes",
                  true,
                  styles.customTextAreaStyle
                )}
              </Row>
              <Row style={styles.rowStyle}>
                {makeLabel("is_uit", this.localize, "activate-deactivate-is-uit-switch")}
                <Col>
                  <div style={styles.rightColumnContainer}>
                    {!this.state.areSwitchStatesLoading && (
                      <Switch
                        onChange={
                          test_definition.is_uit ? this.handleDisableUIT : this.handleEnableUIT
                        }
                        checked={test_definition.is_uit}
                        aria-labelledby="activate-deactivate-is-uit-switch"
                        height={this.state.switchHeight}
                        width={this.state.switchWidth}
                        className="switch-custom-style"
                      />
                    )}
                  </div>
                </Col>
              </Row>
              <Row style={styles.rowStyle}>
                {makeLabel("is_public", this.localize, "activate-deactivate-sample-test-switch")}
                <Col>
                  <div style={styles.rightColumnContainer}>
                    {!this.state.areSwitchStatesLoading && (
                      <Switch
                        onChange={
                          test_definition.is_public
                            ? this.handleDisableSampleTest
                            : this.handleEnableSampleTest
                        }
                        checked={test_definition.is_public}
                        aria-labelledby="activate-deactivate-sample-test-switch-tooltip-make-label"
                        height={this.state.switchHeight}
                        width={this.state.switchWidth}
                        className="switch-custom-style"
                      />
                    )}
                  </div>
                </Col>
              </Row>
              {test_definition.is_public && (
                <Row style={styles.rowStyle}>
                  {makeLabel("count_up", this.localize, "activate-deactivate-count-up-switch")}
                  <Col>
                    <div style={styles.rightColumnContainer}>
                      {!this.state.areSwitchStatesLoading && (
                        <Switch
                          onChange={
                            test_definition.count_up
                              ? this.handleDisableCountUp
                              : this.handleEnableCountUp
                          }
                          checked={test_definition.count_up}
                          aria-labelledby="activate-deactivate-count-up-switch"
                          height={this.state.switchHeight}
                          width={this.state.switchWidth}
                          className="switch-custom-style"
                        />
                      )}
                    </div>
                  </Col>
                </Row>
              )}
              <Row style={styles.rowStyle}>
                {makeLabel("active_status", this.localize)}
                <Col>
                  <div style={styles.rightColumnContainer}>
                    <span
                      style={test_definition.active ? styles.activeState : styles.inactiveState}
                    >
                      {test_definition.active ? LOCALIZE.commons.active : LOCALIZE.commons.inactive}
                    </span>
                  </div>
                </Col>
              </Row>

              <div style={styles.applyButtonContainer}>
                <CustomButton
                  label={
                    <>
                      <FontAwesomeIcon icon={faCheck} />
                      <span style={styles.buttonLabel}>{LOCALIZE.commons.applyButton}</span>
                    </>
                  }
                  action={this.handleSave}
                  customStyle={styles.applyButton}
                  buttonTheme={THEME.PRIMARY}
                />
              </div>
            </>
          )}
        </Container>
      </div>
    );
  }
}

export { TestDefinition as unconnectedTestDefinition };

const mapStateToProps = (state, ownProps) => {
  // done because only one test definition is returned ever
  const INDEX = 0;
  return {
    test_definition: state.testBuilder.test_definition[INDEX],
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    triggerTestDefinitionFieldUpdates: state.testBuilder.triggerTestDefinitionFieldUpdates,
    validationErrors: state.testBuilder.validation_errors
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      modifyTestDefinitionField,
      setTestDefinitionValidationErrors
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(TestDefinition);
