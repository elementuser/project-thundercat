import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import {
  getTestDefinitionData,
  updateSearchAllActiveTestVersionsDefinitionsStates,
  updateCurrentTestDefinitionAllActiveTestVersionsPageState,
  updateCurrentTestDefinitionAllActiveTestVersionsPageSizeState,
  setSelectedTestDefinitionId,
  triggerTestDefinitionLoading,
  activateTestDefinition,
  modifyTestDefinitionField,
  TEST_DEFINITION
} from "../../modules/TestBuilderRedux";
import GenericTable, { COMMON_STYLE } from "../commons/GenericTable";
import { getTestDefinitionVersionsCollected } from "../../modules/PermissionsRedux";
import CustomButton, { THEME } from "../commons/CustomButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBinoculars, faStopCircle, faTimes } from "@fortawesome/free-solid-svg-icons";
import SearchBarWithDisplayOptions from "../commons/SearchBarWithDisplayOptions";
import Pagination from "../commons/Pagination";
import StyledTooltip, { TYPE, EFFECT, TRIGGER_TYPE } from "../authentication/StyledTooltip";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";

const styles = {
  tableContainer: {
    margin: 24
  },
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  bold: {
    fontWeight: "bold"
  },
  allUnset: {
    all: "unset"
  }
};

class AllActiveTestVersions extends Component {
  state = {
    currentlyLoading: false,
    rowsDefinition: {},
    selectedTestDefinition: {},
    numberOfTestDefinitionAllActiveTestVersionsPages: 1,
    resultsFound: 0,
    currentTestDefinitionData: {},
    showDeactivateTestVersionConfirmationPopup: false
  };

  componentDidMount = () => {
    this.getTestDefinitionData();
  };

  componentDidUpdate = prevProps => {
    // if testDefinitionAllActiveTestVersionsPaginationPageSize get updated
    if (
      prevProps.testDefinitionAllActiveTestVersionsPaginationPageSize !==
      this.props.testDefinitionAllActiveTestVersionsPaginationPageSize
    ) {
      this.getTestDefinitionDataBasedOnActiveSearch();
    }
    // if testDefinitionAllActiveTestVersionsPaginationPage get updated
    if (
      prevProps.testDefinitionAllActiveTestVersionsPaginationPage !==
      this.props.testDefinitionAllActiveTestVersionsPaginationPage
    ) {
      this.getTestDefinitionDataBasedOnActiveSearch();
    }
    // if search testDefinitionAllActiveTestVersionsKeyword gets updated
    if (
      prevProps.testDefinitionAllActiveTestVersionsKeyword !==
      this.props.testDefinitionAllActiveTestVersionsKeyword
    ) {
      // if testDefinitionAllActiveTestVersionsKeyword redux state is empty
      if (this.props.testDefinitionAllActiveTestVersionsKeyword !== "") {
        this.getFoundTestDefinitionData();
      } else if (
        this.props.testDefinitionAllActiveTestVersionsKeyword === "" &&
        this.props.testDefinitionAllActiveTestVersionsActiveSearch
      ) {
        this.getFoundTestDefinitionData();
      }
    }
    // if testDefinitionAllActiveTestVersionsActiveSearch gets updated
    if (
      prevProps.testDefinitionAllActiveTestVersionsActiveSearch !==
      this.props.testDefinitionAllActiveTestVersionsActiveSearch
    ) {
      // there is no current active search (on clear search action)
      if (!this.props.testDefinitionAllActiveTestVersionsActiveSearch) {
        this.getTestDefinitionData();
        // there is a current active search (on search action)
      } else {
        this.getFoundTestDefinitionData();
      }
    }
    // if triggerSelectTestDefinitionTablesRefreshState gets updated
    if (
      prevProps.triggerSelectTestDefinitionTablesRefreshState !==
      this.props.triggerSelectTestDefinitionTablesRefreshState
    ) {
      this.getTestDefinitionDataBasedOnActiveSearch();
    }
  };

  // get test definition data based on the testDefinitionAllActiveTestVersionsActiveSearch redux state
  getTestDefinitionDataBasedOnActiveSearch = () => {
    // current search
    if (this.props.testDefinitionAllActiveTestVersionsActiveSearch) {
      this.getFoundTestDefinitionData();
      // no current search
    } else {
      this.getTestDefinitionData();
    }
  };

  getTestDefinitionData = () => {
    // if selectedTestDefinitionId is null
    if (this.props.selectedTestDefinitionId === null) {
      this.setState({ currentlyLoading: true }, () => {
        // initializing needed object and array for rowsDefinition (needed for GenericTable component)
        let rowsDefinition = {};
        const data = [];
        this.props
          .getTestDefinitionData(
            false,
            null,
            null,
            null,
            this.props.testDefinitionAllActiveTestVersionsPaginationPage,
            this.props.testDefinitionAllActiveTestVersionsPaginationPageSize,
            null,
            null,
            true,
            false
          )
          .then(response => {
            for (let i = 0; i < response.body.results.length; i++) {
              // populating data object with provided data
              data.push({
                column_1: response.body.results[i].details[0].parent_code,
                column_2: response.body.results[i].details[0].test_code,
                column_3: response.body.results[i].details[0][`${this.props.currentLanguage}_name`],
                column_4: response.body.results[i].details[0].version,
                column_5: this.populateColumnFive(i, response)
              });
            }
            // updating rowsDefinition object with provided data and needed style
            rowsDefinition = {
              column_1_style: COMMON_STYLE.LEFT_TEXT,
              column_2_style: COMMON_STYLE.LEFT_TEXT,
              column_3_style: COMMON_STYLE.LEFT_TEXT,
              column_4_style: COMMON_STYLE.CENTERED_TEXT,
              column_5_style: COMMON_STYLE.CENTERED_TEXT,
              data: data
            };
            // saving results in state
            this.setState({
              rowsDefinition: rowsDefinition,
              currentlyLoading: false,
              numberOfTestDefinitionAllActiveTestVersionsPages: Math.ceil(
                parseInt(response.body.count) /
                  this.props.testDefinitionAllActiveTestVersionsPaginationPageSize
              )
            });
          });
      });
    }
  };

  getFoundTestDefinitionData = () => {
    this.setState({ currentlyLoading: true }, () => {
      // initializing needed object and array for rowsDefinition (needed for GenericTable component)
      let rowsDefinition = {};
      const data = [];
      this.props
        .getTestDefinitionData(
          false,
          null,
          null,
          null,
          this.props.testDefinitionAllActiveTestVersionsPaginationPage,
          this.props.testDefinitionAllActiveTestVersionsPaginationPageSize,
          // Encode search keyword so we can use special characters
          encodeURIComponent(this.props.testDefinitionAllActiveTestVersionsKeyword),
          this.props.currentLanguage,
          true,
          false
        )
        .then(response => {
          // there are no results found
          if (response.body[0] === "no results found") {
            this.setState(
              {
                rowsDefinition: {},
                numberOfTestDefinitionAllActiveTestVersionsPages: 1,
                resultsFound: 0
              },
              () => {
                this.setState({ currentlyLoading: false });
              }
            );
            // there is at least one result found
          } else {
            for (let i = 0; i < response.body.results.length; i++) {
              // populating data object with provided data
              data.push({
                column_1: response.body.results[i].details[0].parent_code,
                column_2: response.body.results[i].details[0].test_code,
                column_3: response.body.results[i].details[0][`${this.props.currentLanguage}_name`],
                column_4: response.body.results[i].details[0].version,
                column_5: this.populateColumnFive(i, response)
              });
            }
            // updating rowsDefinition object with provided data and needed style
            rowsDefinition = {
              column_1_style: COMMON_STYLE.LEFT_TEXT,
              column_2_style: COMMON_STYLE.LEFT_TEXT,
              column_3_style: COMMON_STYLE.LEFT_TEXT,
              column_4_style: COMMON_STYLE.CENTERED_TEXT,
              column_5_style: COMMON_STYLE.CENTERED_TEXT,
              data: data
            };
            // saving results in state
            this.setState({
              rowsDefinition: rowsDefinition,
              currentlyLoading: false,
              numberOfTestDefinitionAllActiveTestVersionsPages: Math.ceil(
                parseInt(response.body.count) /
                  this.props.testDefinitionAllActiveTestVersionsPaginationPageSize
              ),
              resultsFound: response.body.count
            });
          }
        });
    });
  };

  populateColumnFive = (i, response) => {
    return (
      <div className="d-flex justify-content-center flex-nowrap">
        <StyledTooltip
          id={`view-test-definition-selected-active-test-version-${i}`}
          place="top"
          type={TYPE.light}
          effect={EFFECT.solid}
          triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
          tooltipElement={
            <div style={styles.allUnset}>
              <CustomButton
                buttonId={`view-test-definition-selected-active-test-version-id-${i}`}
                dataTip=""
                dataFor={`view-test-definition-selected-active-test-version-${i}`}
                label={<FontAwesomeIcon icon={faBinoculars} />}
                action={() => {
                  this.viewSelectedTestDefinition(response.body.results[i].details[0]);
                }}
                customStyle={styles.actionButton}
                buttonTheme={THEME.SECONDARY}
                ariaLabel={LOCALIZE.formatString(
                  LOCALIZE.testBuilder.testDefinition.selectTestDefinition.allActiveTestVersions
                    .table.viewButtonAriaLabel,
                  response.body.results[i].details[0].parent_code,
                  response.body.results[i].details[0].test_code,
                  response.body.results[i].details[0][`${this.props.currentLanguage}_name`],
                  response.body.results[i].details[0].version
                )}
                disabled={response.body.results[i].action_buttons_disabled}
              />
            </div>
          }
          tooltipContent={
            <div>
              <p>
                {
                  LOCALIZE.testBuilder.testDefinition.selectTestDefinition.allActiveTestVersions
                    .table.viewButtonTooltip
                }
              </p>
            </div>
          }
        />
        <StyledTooltip
          id={`deactivate-test-definition-selected-active-test-version-${i}`}
          place="top"
          type={TYPE.light}
          effect={EFFECT.solid}
          triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
          tooltipElement={
            <div style={styles.allUnset}>
              <CustomButton
                buttonId={`deactivate-test-definition-selected-active-test-version-id-${i}`}
                dataTip=""
                dataFor={`deactivate-test-definition-selected-active-test-version-${i}`}
                label={<FontAwesomeIcon icon={faStopCircle} />}
                action={() => {
                  this.openDeactivateTestVersionConfirmationPopup(
                    response.body.results[i].details[0]
                  );
                }}
                customStyle={styles.actionButton}
                buttonTheme={THEME.SECONDARY}
                ariaLabel={LOCALIZE.formatString(
                  LOCALIZE.testBuilder.testDefinition.selectTestDefinition.allActiveTestVersions
                    .table.deactivateButtonAriaLabel,
                  response.body.results[i].details[0].parent_code,
                  response.body.results[i].details[0].test_code,
                  response.body.results[i].details[0][`${this.props.currentLanguage}_name`],
                  response.body.results[i].details[0].version
                )}
                disabled={response.body.results[i].action_buttons_disabled}
              />
            </div>
          }
          tooltipContent={
            <div>
              <p>
                {
                  LOCALIZE.testBuilder.testDefinition.selectTestDefinition.allActiveTestVersions
                    .table.deactivateButtonTooltip
                }
              </p>
            </div>
          }
        />
      </div>
    );
  };

  viewSelectedTestDefinition = currentTestDefinitionData => {
    // getting test definition data (based on parent code and test code)
    this.props
      .getTestDefinitionData(
        false,
        currentTestDefinitionData.parent_code,
        currentTestDefinitionData.test_code,
        currentTestDefinitionData.version,
        // page and page_size null here in order to get all the data, not only for a specific page (need all available versions)
        null,
        null,
        null,
        null,
        false,
        false
      )
      .then(response => {
        // update selectedTestDefinitionId redux state
        this.props.setSelectedTestDefinitionId(response.body.results[0].details[0].id);
        this.props.triggerTestDefinitionLoading();
      });
  };

  deactivateSelectedTestDefinition = () => {
    this.props
      .activateTestDefinition(this.state.currentTestDefinitionData.id, false)
      .then(response => {
        if (response.ok) {
          this.modifyField("active", false, this.state.currentTestDefinitionData);
          // refresh table
          this.getTestDefinitionDataBasedOnActiveSearch();
          // close popup
          this.setState({
            showDeactivateTestVersionConfirmationPopup: false,
            currentTestDefinitionData: {}
          });
        }
      });
  };

  modifyField = (inputName, value, currentTestDefinitionData) => {
    const newObj = currentTestDefinitionData;
    newObj[`${inputName}`] = value;
    this.props.modifyTestDefinitionField(TEST_DEFINITION, newObj, newObj.id);
  };

  openDeactivateTestVersionConfirmationPopup = currentTestDefinitionData => {
    this.setState({
      showDeactivateTestVersionConfirmationPopup: true,
      currentTestDefinitionData: currentTestDefinitionData
    });
  };

  closeDeactivateTestVersionConfirmationPopup = () => {
    this.setState({
      showDeactivateTestVersionConfirmationPopup: false,
      currentTestDefinitionData: {}
    });
  };

  render() {
    const columnsDefinition = [
      {
        label:
          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.allActiveTestVersions.table
            .column1,
        style: { ...{ width: "15%" }, ...COMMON_STYLE.LEFT_TEXT }
      },
      {
        label:
          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.allActiveTestVersions.table
            .column2,
        style: { ...{ width: "20%" }, ...COMMON_STYLE.LEFT_TEXT }
      },
      {
        label:
          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.allActiveTestVersions.table
            .column3,
        style: { width: "40%" }
      },
      {
        label:
          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.allActiveTestVersions.table
            .column4,
        style: { ...{ width: "10%" }, ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label:
          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.allActiveTestVersions.table
            .column5,
        style: { ...{ width: "15%" }, ...COMMON_STYLE.CENTERED_TEXT }
      }
    ];

    return (
      <div>
        <h2>
          {LOCALIZE.testBuilder.testDefinition.selectTestDefinition.allActiveTestVersions.title}
        </h2>
        <p>
          {
            LOCALIZE.testBuilder.testDefinition.selectTestDefinition.allActiveTestVersions
              .description
          }
        </p>
        <div style={styles.tableContainer}>
          <div>
            <SearchBarWithDisplayOptions
              idPrefix={"select-active-test-definition-all-active-test-versions"}
              currentlyLoading={this.state.currentlyLoading}
              updateSearchStates={this.props.updateSearchAllActiveTestVersionsDefinitionsStates}
              updatePageState={this.props.updateCurrentTestDefinitionAllActiveTestVersionsPageState}
              paginationPageSize={Number(
                this.props.testDefinitionAllActiveTestVersionsPaginationPageSize
              )}
              updatePaginationPageSize={
                this.props.updateCurrentTestDefinitionAllActiveTestVersionsPageSizeState
              }
              data={this.state.rowsDefinition.data || []}
              resultsFound={this.state.resultsFound}
            />
            <GenericTable
              classnamePrefix="select-test-definition"
              columnsDefinition={columnsDefinition}
              rowsDefinition={this.state.rowsDefinition}
              emptyTableMessage={
                LOCALIZE.testBuilder.testDefinition.selectTestDefinition.allActiveTestVersions.table
                  .noData
              }
              currentlyLoading={this.state.currentlyLoading}
            />
            <Pagination
              paginationContainerId={
                "select-active-test-definition-all-active-test-versions-pagination-pages-link"
              }
              pageCount={this.state.numberOfTestDefinitionAllActiveTestVersionsPages}
              paginationPage={this.props.testDefinitionAllActiveTestVersionsPaginationPage}
              updatePaginationPageState={
                this.props.updateCurrentTestDefinitionAllActiveTestVersionsPageState
              }
              firstTableRowId={"view-test-definition-selected-active-test-version-id-0"}
            />
          </div>
        </div>
        <PopupBox
          show={this.state.showDeactivateTestVersionConfirmationPopup}
          handleClose={() => {}}
          isBackdropStatic={true}
          shouldCloseOnEsc={false}
          overflowVisible={true}
          title={
            LOCALIZE.testBuilder.testDefinition.selectTestDefinition.allActiveTestVersions.popup
              .title
          }
          description={
            <p>
              {LOCALIZE.formatString(
                LOCALIZE.testBuilder.testDefinition.selectTestDefinition.allActiveTestVersions.popup
                  .description,
                <span
                  style={styles.bold}
                >{`${this.state.currentTestDefinitionData.parent_code} ${this.state.currentTestDefinitionData.test_code} v${this.state.currentTestDefinitionData.version}`}</span>
              )}
            </p>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={
            LOCALIZE.testBuilder.testDefinition.selectTestDefinition.allActiveTestVersions.popup
              .rightButton
          }
          rightButtonIcon={faStopCircle}
          rightButtonAction={this.deactivateSelectedTestDefinition}
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeDeactivateTestVersionConfirmationPopup}
        />
      </div>
    );
  }
}

export { AllActiveTestVersions as unconnectedAllActiveTestVersions };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    testDefinitionAllActiveTestVersionsPaginationPageSize:
      state.testBuilder.testDefinitionAllActiveTestVersionsPaginationPageSize,
    testDefinitionAllActiveTestVersionsPaginationPage:
      state.testBuilder.testDefinitionAllActiveTestVersionsPaginationPage,
    testDefinitionAllActiveTestVersionsKeyword:
      state.testBuilder.testDefinitionAllActiveTestVersionsKeyword,
    testDefinitionAllActiveTestVersionsActiveSearch:
      state.testBuilder.testDefinitionAllActiveTestVersionsActiveSearch,
    selectedTestDefinitionId: state.testBuilder.selectedTestDefinitionId,
    triggerSelectTestDefinitionTablesRefreshState:
      state.testBuilder.triggerSelectTestDefinitionTablesRefreshState
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getTestDefinitionVersionsCollected,
      getTestDefinitionData,
      updateCurrentTestDefinitionAllActiveTestVersionsPageState,
      updateCurrentTestDefinitionAllActiveTestVersionsPageSizeState,
      updateSearchAllActiveTestVersionsDefinitionsStates,
      setSelectedTestDefinitionId,
      triggerTestDefinitionLoading,
      activateTestDefinition,
      modifyTestDefinitionField
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(AllActiveTestVersions);
