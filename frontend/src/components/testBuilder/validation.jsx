/* eslint-disable no-param-reassign */
import {
  TestSectionComponentType,
  TestSectionScoringTypeObject,
  TestSectionType
} from "../testFactory/Constants";

export const QUESTION_RULE_ERROR_TYPE = {
  UNDEFINED_PPC_QUESTION_ID_OF_DEPENDENT_QUESTION:
    "undefined_ppc_question_id_of_dependent_question",
  NOT_ENOUGH_PPC_QUESTION_ID_DEFINED_BASED_ON_NB_OF_QUESTIONS:
    "not_enough_ppc_question_id_defined_based_on_nb_of_questions"
};

// making sure that the question list sub-section contains at least one question
const questionListSubSectionValidation = (validationErrors, testSectionComponents, questions) => {
  // initializing needed variables
  let testDefinitionContainsValidQuestionLists = true;

  // getting sub-sections where component_type is QUESTION_LIST
  const questionListSubSections = testSectionComponents.filter(
    obj => obj.component_type === TestSectionComponentType.QUESTION_LIST
  );

  // looping in questionListSubSections
  for (let i = 0; i < questionListSubSections.length; i++) {
    // initializing containsAtLeastOneQuestion
    let containsAtLeastOneQuestion = false;
    // looping in questions
    for (let j = 0; j < questions.length; j++) {
      // current question is related to current test section component
      if (questions[j].test_section_component === questionListSubSections[i].id) {
        // setting containsAtLeastOneQuestion to true
        containsAtLeastOneQuestion = true;
        break;
      }
    }
    // if containsAtLeastOneQuestion is false
    if (!containsAtLeastOneQuestion) {
      // setting testDefinitionContainsValidQuestionLists to false
      testDefinitionContainsValidQuestionLists = false;
      break;
    }
  }
  // setting testDefinitionContainsValidQuestionLists
  validationErrors.testDefinitionContainsValidQuestionLists =
    testDefinitionContainsValidQuestionLists;
  return validationErrors;
};

// making sure that all test sections where scoring method is set to AUTO SCORE contain at least one sub-section where component type is set to QUESTION LIST
export const subSectionsValidation = (validationErrors, testSections, testSectionComponents) => {
  // initializing testDefinitionContainsValidSubSections
  let testDefinitionContainsValidSubSections = true;
  // getting test sections where scoring_type is AUTO SCORE
  const testSectionsSetToAutoScore = testSections.filter(
    obj => obj.scoring_type === TestSectionScoringTypeObject.AUTO_SCORE.value
  );
  // looping in testSectionsSetToAutoScore
  for (let i = 0; i < testSectionsSetToAutoScore.length; i++) {
    // getting related test section components
    const relatedTestSectionComponents = testSectionComponents.filter(
      obj => obj.test_section[0] === testSectionsSetToAutoScore[i].id
    );
    // initializing containsAtLeastOneQuestionList
    let containsAtLeastOneQuestionList = false;
    // looping in relatedTestSectionComponents
    for (let j = 0; j < relatedTestSectionComponents.length; j++) {
      // if component_type of current test section component is QUESTION_LIST
      if (
        relatedTestSectionComponents[j].component_type === TestSectionComponentType.QUESTION_LIST ||
        relatedTestSectionComponents[j].component_type === TestSectionComponentType.ITEM_BANK
      ) {
        containsAtLeastOneQuestionList = true;
        break;
      }
    }
    // if containsAtLeastOneQuestionList is set to false
    if (!containsAtLeastOneQuestionList) {
      // setting testDefinitionContainsValidSubSections to false
      testDefinitionContainsValidSubSections = false;
      break;
    }
  }

  // setting testDefinitionContainsValidSubSections
  validationErrors.testDefinitionContainsValidSubSections = testDefinitionContainsValidSubSections;
  return validationErrors;
};

// making sure that all TOP TABS test sections and all SIDE NAVIGATION sub-sections have defined page content titles (section component pages)
export const sectionComponentPagesValidation = (
  validationErrors,
  testSections,
  testSectionComponents,
  sectionComponentPages
) => {
  // initializing testDefinitionContainsValidSectionComponentPages
  let testDefinitionContainsValidSectionComponentPages = true;
  // looping in testSections
  for (let i = 0; i < testSections.length; i++) {
    // getting related test section components where component_type is not QUESTION LIST
    const relatedTestSectionComponents = testSectionComponents.filter(
      obj =>
        obj.test_section[0] === testSections[i].id &&
        obj.component_type !== TestSectionComponentType.QUESTION_LIST
    );
    // initializing hasAssociatedSectionComponentPage
    let hasAssociatedSectionComponentPage = true;
    // looping in relatedTestSectionComponents
    for (let j = 0; j < relatedTestSectionComponents.length; j++) {
      // if not a QUESTION_LIST & ITEM_BANK test section component type
      if (
        relatedTestSectionComponents[j].component_type !== TestSectionComponentType.ITEM_BANK &&
        relatedTestSectionComponents[j].component_type !== TestSectionComponentType.QUESTION_LIST
      ) {
        // verifying if current test section component is associated to a section component page (page content title)
        hasAssociatedSectionComponentPage =
          sectionComponentPages.filter(
            obj => obj.test_section_component === relatedTestSectionComponents[j].id
          ).length > 0;
      }
    }
    // if hasAssociatedSectionComponentPage is set to false
    if (!hasAssociatedSectionComponentPage) {
      testDefinitionContainsValidSectionComponentPages = false;
      break;
    }
  }
  // setting testDefinitionContainsValidSectionComponentPages
  validationErrors.testDefinitionContainsValidSectionComponentPages =
    testDefinitionContainsValidSectionComponentPages;
  return validationErrors;
};

export const itemBankRulesValidation = (validationErrors, testSectionComponents, itemBankRules) => {
  let testDefinitionContainsValidItemBankRules = true;
  for (let i = 0; i < testSectionComponents.length; i++) {
    if (testSectionComponents[i].component_type === TestSectionComponentType.ITEM_BANK) {
      // getting item bank rules related to the current test section component
      const related_item_bank_rules = itemBankRules.filter(
        obj => obj.test_section_component === testSectionComponents[i].id
      );
      // no rules are defined
      if (related_item_bank_rules.length <= 0) {
        testDefinitionContainsValidItemBankRules = false;
      } else {
        // looping in related_item_bank_rules
        for (let j = 0; j < related_item_bank_rules.length; j++) {
          // making sure that all needed attributes are defined
          if (
            Object.entries(related_item_bank_rules[j].item_bank).length <= 0 ||
            Object.entries(related_item_bank_rules[j].item_bank_bundle).length <= 0
          ) {
            testDefinitionContainsValidItemBankRules = false;
          }
        }
      }
    }
  }
  // setting testDefinitionContainsValidSectionComponentPages
  validationErrors.testDefinitionContainsValidItemBankRules =
    testDefinitionContainsValidItemBankRules;
  return validationErrors;
};

// ref: https://www.tutorialspoint.com/all-combinations-of-sums-for-array-in-javascript
function buildCombinations(arr, num) {
  const res = [];
  let temp;
  let i;
  let j;
  // eslint-disable-next-line no-bitwise
  const max = 1 << arr.length;
  for (i = 0; i < max; i++) {
    temp = [];
    for (j = 0; j < arr.length; j++) {
      // eslint-disable-next-line no-bitwise
      if (i & (1 << j)) {
        temp.push(arr[j]);
      }
    }
    if (temp.length === num) {
      res.push(
        temp.reduce((a, b) => {
          return a + b;
        })
      );
    }
  }
  return res;
}

// useful to validate the number of questions of question rules
export function validateQuestionRulesData(questions, questionRules) {
  // initializing isValid
  let isValid = true;
  // initializing questionsDataPerRule
  const questionsDataPerRule = [];
  // looping in questions
  for (let i = 0; i < questions.length; i++) {
    // current question block type is already part of questionsDataPerRule array
    if (
      questionsDataPerRule.filter(
        obj => obj.question_block_type === questions[i].question_block_type
      ).length > 0
    ) {
      // incrementing the amountOfQuestions value
      const index = questionsDataPerRule.findIndex(
        obj => obj.question_block_type === questions[i].question_block_type
      );
      // updating questionsDataPerRule data
      questionsDataPerRule[index].amountOfQuestions =
        parseInt(questionsDataPerRule[index].amountOfQuestions) + 1;
      // questionsDataPerRule[index].questions.push(questions[i].id);
      // current question block type is not part of questionsDataPerRule array
    } else {
      // initializing arrayOfDependenciesArray
      const arrayOfDependenciesArray = [];
      // looping in questions where question block type matches
      const questionsWithSameBlockType = questions.filter(
        obj => obj.question_block_type === questions[i].question_block_type
      );
      for (let i = 0; i < questionsWithSameBlockType.length; i++) {
        if (
          arrayOfDependenciesArray.filter(obj => obj.indexOf(questionsWithSameBlockType[i].id) >= 0)
            .length <= 0
        ) {
          if (questionsWithSameBlockType[i].dependencies.length > 0) {
            arrayOfDependenciesArray.push(questionsWithSameBlockType[i].dependencies);
          }
        }
      }
      // pushing data to questionsDataPerRule array
      questionsDataPerRule.push({
        question_block_type: questions[i].question_block_type,
        amountOfQuestions: 1,
        arrayOfArraysOfDependentQuestions: arrayOfDependenciesArray,
        questionWithoutDependencies: questions.filter(
          obj =>
            obj.question_block_type === questions[i].question_block_type &&
            obj.dependencies.length <= 0
        )
      });
    }
  }

  // looping in questionRules
  for (let i = 0; i < questionRules.length; i++) {
    // getting questions data of current question block type
    const currentQuestionsData = questionsDataPerRule.filter(
      obj => obj.question_block_type === questionRules[i].question_block_type[0]
    )[0];
    // if currentQuestionsData is undefined (can happen if you have a defined rule in Test Section Components that is not used in at least one question)
    if (typeof currentQuestionsData === "undefined") {
      // updating isValid state
      isValid = false;
      break;
    }
    // number of questions is different than 0 and the amount of questions in the rule
    else if (
      questionRules[i].number_of_questions !== 0 &&
      questionRules[i].number_of_questions !== currentQuestionsData.amountOfQuestions
    ) {
      // initializing questionsGroupsArray
      const questionsGroupsArray = [];
      // building questionsGroupsArray
      // looping in arrayOfArraysOfDependentQuestions
      for (let i = 0; i < currentQuestionsData.arrayOfArraysOfDependentQuestions.length; i++) {
        questionsGroupsArray.push(
          currentQuestionsData.arrayOfArraysOfDependentQuestions[i].length + 1
        );
      }
      // looping in questions without dependencies
      for (let j = 0; j < currentQuestionsData.questionWithoutDependencies.length; j++) {
        questionsGroupsArray.push(1);
      }
      // sorting questionsGroupsArray
      questionsGroupsArray.sort();

      // initializing possibilitiesArray
      let possibilitiesArray = [];

      // looping in questionsGroupsArray
      for (let k = 1; k <= questionsGroupsArray.length; k++) {
        // getting available combinations based on the current index (which is the amout of numbers to sum together)
        const combinationsArray = buildCombinations(questionsGroupsArray, k);
        // concatenating possibilitiesArray
        possibilitiesArray = possibilitiesArray.concat(combinationsArray);
      }

      // removing duplicates
      possibilitiesArray = possibilitiesArray.filter((item, pos) => {
        return possibilitiesArray.indexOf(item) === pos;
      });
      // numeric value of the number of questions is not part of possibilitiesArray
      if (possibilitiesArray.indexOf(questionRules[i].number_of_questions) < 0) {
        // updating isValid state
        isValid = false;
      }
    }
  }

  return isValid;
}

// useful to validate and make sure that there is enough defined ppc_question_id based on the number of questions of question rules
export function validateQuestionRulesDataWithEnabledItemExposure(questions, questionRules) {
  // initializing isValid and errorType
  let isValid = true;
  let errorType = null;
  // looping in question rules
  for (let i = 0; i < questionRules.length; i++) {
    // initializing questionsWithDefinedPpcQuestionId
    let questionsWithDefinedPpcQuestionId = 0;
    // getting all questions related to current rule
    const concernedQuestions = questions.filter(
      obj => obj.question_block_type === questionRules[i].question_block_type[0]
    );
    // looping in concernedQuestions
    for (let j = 0; j < concernedQuestions.length; j++) {
      // current question has dependencies
      if (concernedQuestions[j].dependencies > 0) {
        // ppc_question_id of current question is not defined
        if (concernedQuestions[j].ppc_question_id === "") {
          // returning invalid state with UNDEFINED_PPC_QUESTION_ID_OF_DEPENDENT_QUESTION error
          isValid = false;
          errorType = QUESTION_RULE_ERROR_TYPE.UNDEFINED_PPC_QUESTION_ID_OF_DEPENDENT_QUESTION;
          return { is_valid: isValid, error_type: errorType };
          // ppc_question_id of current question is defined
        }
        // incrementing questionsWithDefinedPpcQuestionId
        questionsWithDefinedPpcQuestionId += 1;

        // looping in dependencies
        for (let k = 0; k < concernedQuestions[j].dependencies.length; k++) {
          // getting dependent question data
          const dependentQuestionData = concernedQuestions.filter(
            obj => obj.id === concernedQuestions[j].dependencies[k]
          );
          // ppc_question_id of current dependent question is not defined
          if (dependentQuestionData.ppc_question_id === "") {
            // returning invalid state with UNDEFINED_PPC_QUESTION_ID_OF_DEPENDENT_QUESTION error
            isValid = false;
            errorType = QUESTION_RULE_ERROR_TYPE.UNDEFINED_PPC_QUESTION_ID_OF_DEPENDENT_QUESTION;
            return { is_valid: isValid, error_type: errorType };
            // ppc_question_id of current dependent question is defined
          }
          // incrementing questionsWithDefinedPpcQuestionId
          questionsWithDefinedPpcQuestionId += 1;
        }
        // current question has no dependencies
      } else {
        // if ppc_question_id of current question is defined
        // eslint-disable-next-line no-lonely-if
        if (concernedQuestions[j].ppc_question_id !== "") {
          // incrementing questionsWithDefinedPpcQuestionId
          questionsWithDefinedPpcQuestionId += 1;
        }
      }
    }
    // if number_of_questions of current rule is greater than questionsWithDefinedPpcQuestionId
    if (questionRules[i].number_of_questions > questionsWithDefinedPpcQuestionId) {
      // returning invalid state with NOT_ENOUGH_PPC_QUESTION_ID_DEFINED_BASED_ON_NB_OF_QUESTIONS error
      isValid = false;
      errorType =
        QUESTION_RULE_ERROR_TYPE.NOT_ENOUGH_PPC_QUESTION_ID_DEFINED_BASED_ON_NB_OF_QUESTIONS;
      return { is_valid: isValid, error_type: errorType };
    }
  }
  // everything is valid
  return { is_valid: isValid, error_type: errorType };
}

// useful to validate and make sure that there is enough defined ppc_question_id based on the number of questions of question rules
export function validateTestSections(validationErrors, testSections) {
  // initializing needed variables
  const testDefinitionContainsAtLeastThreeTestSections = testSections.length >= 3;
  let testDefinitionContainsQuitTestSection = false;
  let testDefinitionContainsFinishTestSection = false;

  // if it contains at least three test sections
  if (testDefinitionContainsAtLeastThreeTestSections) {
    // looping in test sections
    for (let i = 0; i < testSections.length; i++) {
      // test section section type is QUIT
      if (testSections[i].section_type === TestSectionType.QUIT) {
        testDefinitionContainsQuitTestSection = true;
        // test section section type is FINISH
      } else if (testSections[i].section_type === TestSectionType.FINISH) {
        testDefinitionContainsFinishTestSection = true;
      }
    }
  }

  // setting testDefinitionContainsValidSubSections
  validationErrors.testDefinitionContainsAtLeastThreeTestSections =
    testDefinitionContainsAtLeastThreeTestSections;
  validationErrors.testDefinitionContainsQuitTestSection = testDefinitionContainsQuitTestSection;
  validationErrors.testDefinitionContainsFinishTestSection =
    testDefinitionContainsFinishTestSection;
  return validationErrors;
}

// making sure that the test definition contains valid test definition data (Parent Code, Test Code, etc)
export function validateTestDefinitionData(validationErrors, testDefinition) {
  // initializing needed variables
  let testDefinitionContainsValidTestDefinitionData = true;
  // making sure that all required fields are filled
  // Parent Code
  if (typeof testDefinition.parent_code === "undefined" || testDefinition.parent_code === "") {
    testDefinitionContainsValidTestDefinitionData = false;
  }
  // Test Code
  else if (typeof testDefinition.test_code === "undefined" || testDefinition.test_code === "") {
    testDefinitionContainsValidTestDefinitionData = false;
  }
  // English Name
  else if (typeof testDefinition.en_name === "undefined" || testDefinition.en_name === "") {
    testDefinitionContainsValidTestDefinitionData = false;
  }
  // French Name
  else if (typeof testDefinition.fr_name === "undefined" || testDefinition.fr_name === "") {
    testDefinitionContainsValidTestDefinitionData = false;
  }

  // setting testDefinitionContainsValidSubSections
  validationErrors.testDefinitionContainsValidTestDefinitionData =
    testDefinitionContainsValidTestDefinitionData;
  return validationErrors;
}

export default questionListSubSectionValidation;
