import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlusCircle } from "@fortawesome/free-solid-svg-icons";
import { Button, Row, Container } from "react-bootstrap";
import LOCALIZE from "../../text_resources";
import CollapsingItemContainer from "../eMIB/CollapsingItemContainer";
import {
  makeDropDownField,
  makeLabel,
  getNextInNumberSeries,
  collapsingItemMoveUpFunctionality,
  collapsingItemMoveDownFunctionality,
  deleteQuestionData,
  deleteQuestionRulesData
} from "./helpers";
import TestSectionComponentForm from "./TestSectionComponentForm";
import {
  addTestDefinitionField,
  replaceTestDefinitionField,
  PAGE_SECTIONS,
  PAGE_SECTION_DEFINITIONS,
  TEST_SECTION_COMPONENTS,
  SECTION_COMPONENT_PAGES,
  replaceTestDefinitionArray,
  setTestDefinitionValidationErrors,
  QUESTION_LIST_RULES,
  triggerTestDefinitionFieldUpdates
} from "../../modules/TestBuilderRedux";
import { TestSectionComponentType } from "../testFactory/Constants";
import questionListSubSectionValidation, {
  subSectionsValidation,
  sectionComponentPagesValidation,
  QUESTION_RULE_ERROR_TYPE,
  itemBankRulesValidation
} from "./validation";

export const styles = {
  mainContainer: {
    width: "100%"
  },
  label: {
    padding: "3px 0 0 4px"
  },
  borderBox: {
    borderBottom: "1px solid",
    borderRadius: "5px",
    padding: "5px",
    borderColor: "#CECECE",
    color: "red"
  },
  rowStyle: {
    margin: "5px"
  },
  validationErrorMessageContainer: {
    border: "1px solid #923534",
    padding: 12,
    marginBottom: 12
  },
  validationErrorMessage: {
    textAlign: "center",
    color: "#923534",
    fontWeight: "bold"
  }
};

class TestSectionComponents extends Component {
  state = {
    selectedParentTestSection: "",
    showInvalidQuestionRulesError: false,
    showInvalidSubSectionsError: false,
    showInvalidItemBankRulesError: false,
    testDefinitionQuestionRulesErrorType: null
  };

  componentDidMount = () => {
    // getting orders
    this.getTestSectionComponentOrders();
    // get validation errors
    this.getValidationErrors();
  };

  componentDidUpdate = prevProps => {
    // if length of testSectionComponents or testSections gets updated
    if (
      prevProps.testSectionComponents !== this.props.testSectionComponents ||
      prevProps.testSections !== this.props.testSections ||
      prevProps.questionRulesValidState !== this.props.questionRulesValidState ||
      prevProps.questions !== this.props.questions
    ) {
      if (prevProps.testSectionComponents.length !== this.props.testSectionComponents.length) {
        // getting orders
        this.getTestSectionComponentOrders();
      }
      // initializing copyOfValidationErrors
      let copyOfValidationErrors = this.props.validationErrors;
      // calling question list sub-section validation
      copyOfValidationErrors = questionListSubSectionValidation(
        copyOfValidationErrors,
        this.props.testSectionComponents,
        this.props.questions
      );
      // calling sub-sections validation
      copyOfValidationErrors = subSectionsValidation(
        copyOfValidationErrors,
        this.props.testSections,
        this.props.testSectionComponents
      );
      // calling section component pages (page content titles) validation
      copyOfValidationErrors = sectionComponentPagesValidation(
        copyOfValidationErrors,
        this.props.testSections,
        this.props.testSectionComponents,
        this.props.sectionComponentPages
      );
      // calling item bank rules validation (for all test section component type ITEM_BANK)
      copyOfValidationErrors = itemBankRulesValidation(
        copyOfValidationErrors,
        this.props.testSectionComponents,
        this.props.itemBankRules
      );
      // updating redux states
      this.props.setTestDefinitionValidationErrors(copyOfValidationErrors);
      // get validation errors
      this.getValidationErrors();
    }
    // if test definition field gets updated
    if (
      prevProps.triggerTestDefinitionFieldUpdates !== this.props.triggerTestDefinitionFieldUpdates
    ) {
      // getting orders
      this.getTestSectionComponentOrders();
      // get validation errors
      this.getValidationErrors();
    }
    // if any of the upload validation error state gets updated
    if (prevProps.validationErrors !== this.props.validationErrors) {
      // get validation errors
      this.getValidationErrors();
    }
  };

  addTestSectionComponent = () => {
    const newObj = {
      id: 1,
      order: 1,
      component_type: TestSectionComponentType.SINGLE_PAGE,
      en_title: "template",
      fr_title: "template",
      language: "--",
      test_section: [this.state.selectedParentTestSection.value],
      shuffle_all_questions: false,
      shuffle_question_blocks: false
    };
    newObj.id = getNextInNumberSeries(this.props.testSectionComponents, "id");
    this.props.addTestDefinitionField(TEST_SECTION_COMPONENTS, newObj, 1);
    // getting orders
    this.getTestSectionComponentOrders();
    // calling section component pages (page content titles) validation
    const validationErrors = sectionComponentPagesValidation(
      this.props.validationErrors,
      this.props.testSections,
      this.props.testSectionComponents,
      this.props.sectionComponentPages
    );
    // updating redux states
    this.props.setTestDefinitionValidationErrors(validationErrors);
    // trigger test definition field updates
    this.props.triggerTestDefinitionFieldUpdates();
  };

  makeTestSectionOptions = (array, lang) => {
    return array.map(item => {
      return { label: item[`${lang}_title`], value: item.id };
    });
  };

  selectParentTestSection = event => {
    this.setState({ selectedParentTestSection: event }, () => {
      // getting orders
      this.getTestSectionComponentOrders();
    });
  };

  handleDelete = id => {
    // delete the test section component
    const sectionCompsToDelete = [];
    let objArray = this.props.testSectionComponents.filter(obj => {
      if (obj.id !== id) {
        return obj;
      }
      sectionCompsToDelete.push(obj.id);
      return null;
    });
    this.props.replaceTestDefinitionField(TEST_SECTION_COMPONENTS, objArray);
    // delete the section component page
    const pageSectionsToDelete = [];
    objArray = this.props.sectionComponentPages.filter(obj => {
      if (obj.test_section_component !== id) {
        return obj;
      }
      pageSectionsToDelete.push(obj.id);
      return null;
    });
    this.props.replaceTestDefinitionField(SECTION_COMPONENT_PAGES, objArray);

    // delete the page section
    const pageSectionDefinitionsToDelete = [];
    objArray = this.props.pageSections.filter(obj => {
      if (pageSectionsToDelete.indexOf(obj.section_component_page) === -1) {
        return obj;
      }
      pageSectionDefinitionsToDelete.push(obj.id);
      return null;
    });
    this.props.replaceTestDefinitionField(PAGE_SECTIONS, objArray);

    // delete the page section definitions
    objArray = this.props.pageSectionDefinitions.filter(obj => {
      if (pageSectionDefinitionsToDelete.indexOf(obj.page_section) === -1) {
        return obj;
      }
      return null;
    });
    this.props.replaceTestDefinitionField(PAGE_SECTION_DEFINITIONS, objArray);

    // deleting question data
    deleteQuestionData(
      this.props.replaceTestDefinitionField,
      sectionCompsToDelete,
      this.props.questions,
      this.props.questionSections,
      this.props.questionSectionDefinitions,
      this.props.answers,
      this.props.answerDetails,
      this.props.multipleChoiceQuestionDetails
    );

    // deleting question rules data
    deleteQuestionRulesData(
      this.props.replaceTestDefinitionField,
      sectionCompsToDelete,
      this.props.questionListRules
    );

    // trigger test definition field updates
    this.props.triggerTestDefinitionFieldUpdates();
  };

  handleMoveUp = index => {
    // getting new test section component array
    collapsingItemMoveUpFunctionality(index, this.props.testSectionComponents)
      .then(objArray => {
        // updating redux states
        this.props.replaceTestDefinitionArray(TEST_SECTION_COMPONENTS, objArray);
      })
      .then(() => {
        // getting orders
        this.getTestSectionComponentOrders();
      });
  };

  handleMoveDown = index => {
    // getting new test section component array
    collapsingItemMoveDownFunctionality(index, this.props.testSectionComponents)
      .then(objArray => {
        // updating redux states
        this.props.replaceTestDefinitionArray(TEST_SECTION_COMPONENTS, objArray);
      })
      .then(() => {
        // getting orders
        this.getTestSectionComponentOrders();
      });
  };

  getTestSectionComponentOrders = () => {
    // initializing props variables
    const { testSectionComponents, questionListRules } = this.props;

    // ========== TEST SECTION COMPONENTS ORDER ==========
    // initializing newTestSectionComponents array
    const newTestSectionComponents = [];

    // grouping array by test section
    // source: https://edisondevadoss.medium.com/javascript-group-an-array-of-objects-by-key-afc85c35d07e
    const grouped_by_test_section = testSectionComponents.reduce((r, a) => {
      // eslint-disable-next-line no-param-reassign
      r[a.test_section[0]] = [...(r[a.test_section[0]] || []), a];
      return r;
    }, {});
    const grouped_by_test_section_array = Object.keys(grouped_by_test_section).map(key => [
      grouped_by_test_section[key]
    ]);

    // looping in grouped_by_test_section_array
    for (let i = 0; i < grouped_by_test_section_array.length; i++) {
      // looping in array of current grouped_by_test_section_array
      for (let j = 0; j < grouped_by_test_section_array[i][0].length; j++) {
        // pushing new object (with the right order) in newTestSectionComponents array
        newTestSectionComponents.push({
          id: grouped_by_test_section_array[i][0][j].id,
          component_type: grouped_by_test_section_array[i][0][j].component_type,
          en_title: grouped_by_test_section_array[i][0][j].en_title,
          fr_title: grouped_by_test_section_array[i][0][j].fr_title,
          language: grouped_by_test_section_array[i][0][j].language,
          shuffle_all_questions: grouped_by_test_section_array[i][0][j].shuffle_all_questions,
          shuffle_question_blocks: grouped_by_test_section_array[i][0][j].shuffle_question_blocks,
          test_section: grouped_by_test_section_array[i][0][j].test_section,
          order: j + 1
        });
      }
    }
    // updating redux state
    this.props.replaceTestDefinitionArray(TEST_SECTION_COMPONENTS, newTestSectionComponents);

    // ========== QUESTION LIST RULES ORDER ==========
    // initializing newQuestionListRules array
    const newQuestionListRules = [];

    // grouping array by test section component
    // source: https://edisondevadoss.medium.com/javascript-group-an-array-of-objects-by-key-afc85c35d07e
    const grouped_by_test_section_component = questionListRules.reduce((r, a) => {
      // eslint-disable-next-line no-param-reassign
      r[a.test_section_component] = [...(r[a.test_section_component] || []), a];
      return r;
    }, {});
    const grouped_by_test_section_component_array = Object.keys(
      grouped_by_test_section_component
    ).map(key => [grouped_by_test_section_component[key]]);

    // looping in grouped_by_test_section_component_array
    for (let i = 0; i < grouped_by_test_section_component_array.length; i++) {
      // looping in array of current grouped_by_test_section_component_array
      for (let j = 0; j < grouped_by_test_section_component_array[i][0].length; j++) {
        // pushing new object (with the right order) in newTestSectionComponents array
        newQuestionListRules.push({
          id: grouped_by_test_section_component_array[i][0][j].id,
          number_of_questions: grouped_by_test_section_component_array[i][0][j].number_of_questions,
          question_block_type: grouped_by_test_section_component_array[i][0][j].question_block_type,
          shuffle: grouped_by_test_section_component_array[i][0][j].shuffle,
          test_section_component:
            grouped_by_test_section_component_array[i][0][j].test_section_component,
          order: j + 1
        });
      }
    }
    this.props.replaceTestDefinitionArray(QUESTION_LIST_RULES, newQuestionListRules);
  };

  getValidationErrors = () => {
    // initializing needed variables
    let showInvalidQuestionRulesError = false;
    let showInvalidSubSectionsError = false;
    let showInvalidItemBankRulesError = false;
    let testDefinitionQuestionRulesErrorType = null;

    // making sure the the validation errors state is defined
    if (typeof this.props.validationErrors !== "undefined") {
      // question rules error
      if (!this.props.validationErrors.testDefinitionContainsValidQuestionRules) {
        showInvalidQuestionRulesError = true;
        // checking if testDefinitionQuestionRulesErrorType is defined
        if (this.props.validationErrors.testDefinitionQuestionRulesErrorType !== null) {
          testDefinitionQuestionRulesErrorType =
            this.props.validationErrors.testDefinitionQuestionRulesErrorType;
        }
      }
      // sub-section validation error
      if (!this.props.validationErrors.testDefinitionContainsValidSubSections) {
        showInvalidSubSectionsError = true;
      }
      // item bank rules validation error
      if (!this.props.validationErrors.testDefinitionContainsValidItemBankRules) {
        showInvalidItemBankRulesError = true;
      }
    }
    // updating needed states
    this.setState({
      showInvalidQuestionRulesError: showInvalidQuestionRulesError,
      showInvalidSubSectionsError: showInvalidSubSectionsError,
      showInvalidItemBankRulesError: showInvalidItemBankRulesError,
      testDefinitionQuestionRulesErrorType: testDefinitionQuestionRulesErrorType
    });
  };

  render() {
    const { testSectionComponents } = this.props;

    const options = this.makeTestSectionOptions(
      this.props.testSections,
      this.props.currentLanguage
    );

    const customFontSize = {
      // adding 5px to current font size
      fontSize: `${parseInt(this.props.accommodations.fontSize.split("px")[0]) + 5}px`
    };

    return (
      <div style={styles.mainContainer}>
        <h2>{LOCALIZE.testBuilder.testSectionComponents.title}</h2>
        {this.state.showInvalidQuestionRulesError && (
          <div style={styles.validationErrorMessageContainer}>
            <p style={{ ...styles.validationErrorMessage, ...customFontSize }}>
              {
                LOCALIZE.testBuilder.testSectionComponents.validationErrors
                  .mustFixTheQuestionsRulesErrors
              }
            </p>
            {this.state.testDefinitionQuestionRulesErrorType ===
              QUESTION_RULE_ERROR_TYPE.NOT_ENOUGH_PPC_QUESTION_ID_DEFINED_BASED_ON_NB_OF_QUESTIONS && (
              <p style={{ ...styles.validationErrorMessage, ...customFontSize }}>
                {
                  LOCALIZE.testBuilder.testSectionComponents.validationErrors
                    .notEnoughDefinedPpcQuestionIdBasedOnNbOfQuestions
                }
              </p>
            )}
            {this.state.testDefinitionQuestionRulesErrorType ===
              QUESTION_RULE_ERROR_TYPE.UNDEFINED_PPC_QUESTION_ID_OF_DEPENDENT_QUESTION && (
              <p style={{ ...styles.validationErrorMessage, ...customFontSize }}>
                {
                  LOCALIZE.testBuilder.testSectionComponents.validationErrors
                    .undefinedPpcQuestionIdInDependentQuestion
                }
              </p>
            )}
          </div>
        )}
        {this.state.showInvalidSubSectionsError && (
          <div style={styles.validationErrorMessageContainer}>
            <p style={{ ...styles.validationErrorMessage, ...customFontSize }}>
              {
                LOCALIZE.testBuilder.testSectionComponents.validationErrors
                  .mustContainQuestionListComponentType
              }
            </p>
          </div>
        )}
        {this.state.showInvalidItemBankRulesError && (
          <div style={styles.validationErrorMessageContainer}>
            <p style={{ ...styles.validationErrorMessage, ...customFontSize }}>
              {LOCALIZE.testBuilder.testSectionComponents.validationErrors.mustContainItemBankRules}
            </p>
          </div>
        )}
        <p>{LOCALIZE.testBuilder.testSectionComponents.description}</p>
        <Container>
          <form onChange={this.onChange}>
            <Row style={styles.rowStyle}>
              {makeLabel("parentTestSection", LOCALIZE.testBuilder.testSectionComponents)}
              {makeDropDownField(
                "parentTestSection",
                LOCALIZE.testBuilder.testSectionComponents,
                this.state.selectedParentTestSection,
                options,
                this.selectParentTestSection
              )}
            </Row>
          </form>
        </Container>
        <Button
          variant="primary"
          onClick={this.addTestSectionComponent}
          disabled={this.state.selectedParentTestSection === ""}
        >
          <FontAwesomeIcon icon={faPlusCircle} style={{ margins: "auto", marginRight: "5px" }} />
          {LOCALIZE.testBuilder.testSectionComponents.addButton}
        </Button>
        <div style={styles.borderBox} />

        {this.state.selectedParentTestSection !== "" &&
          testSectionComponents.map((section, index) => {
            if (section.test_section.indexOf(this.state.selectedParentTestSection.value) >= 0) {
              const questionListRules = this.props.questionListRules.filter(
                obj => obj.test_section_component === section.id
              );
              return (
                <CollapsingItemContainer
                  key={index}
                  index={index}
                  usesMoveUpAndDownFunctionalities={true}
                  moveUpAction={() => this.handleMoveUp(index)}
                  moveUpButtonDisabled={section.order === 1}
                  moveDownAction={() => this.handleMoveDown(index)}
                  moveDownButtonDisabled={
                    index === testSectionComponents.length - 1
                      ? true
                      : testSectionComponents[index + 1].order <= section.order
                  }
                  title={
                    <label>
                      {LOCALIZE.formatString(
                        LOCALIZE.testBuilder.testSectionComponents.collapsableItemName,
                        section.order,
                        section[`${this.props.currentLanguage}_title`]
                      )}
                    </label>
                  }
                  body={
                    <TestSectionComponentForm
                      testSectionComponent={section}
                      questionListRules={questionListRules}
                      handleDelete={this.handleDelete}
                    />
                  }
                />
              );
            }
            return null;
          })}
      </div>
    );
  }
}

export { TestSectionComponents as unconnectedTestSectionComponents };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    testSections: state.testBuilder.test_sections,
    testSectionComponents: state.testBuilder.test_section_components,
    sectionComponentPages: state.testBuilder.section_component_pages,
    pageSections: state.testBuilder.page_sections,
    pageSectionDefinitions: state.testBuilder.page_section_definitions,
    questionListRules: state.testBuilder.question_list_rules,
    itemBankRules: state.testBuilder.item_bank_rules,
    questionRulesValidState: state.testBuilder.question_rules_valid_state,
    triggerTestDefinitionFieldUpdates: state.testBuilder.triggerTestDefinitionFieldUpdates,
    validationErrors: state.testBuilder.validation_errors,
    accommodations: state.accommodations,
    componentPageSections: state.testBuilder.page_sections,
    questions: state.testBuilder.questions,
    questionSections: state.testBuilder.question_sections,
    questionSectionDefinitions: state.testBuilder.question_section_definitions,
    answers: state.testBuilder.answers,
    answerDetails: state.testBuilder.answer_details,
    multipleChoiceQuestionDetails: state.testBuilder.multiple_choice_question_details
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      addTestDefinitionField,
      replaceTestDefinitionField,
      replaceTestDefinitionArray,
      setTestDefinitionValidationErrors,
      triggerTestDefinitionFieldUpdates
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(TestSectionComponents);
