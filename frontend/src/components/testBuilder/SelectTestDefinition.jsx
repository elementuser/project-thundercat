import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { getTestDefinitionVersionsCollected } from "../../modules/PermissionsRedux";
import SideNavigation from "../eMIB/SideNavigation";
import AllTests from "./AllTests";
import AllActiveTestVersions from "./AllActiveTestVersions";
import ArchivedTestCodes from "./ArchivedTestCodes";
import UploadJson from "./UploadJson";
import AllItemBanks from "./itemBank/AllItemBanks";
import { setTestDefinitionSelectionSideNavState } from "../../modules/TestBuilderRedux";

const styles = {
  tabContainer: {
    zIndex: 1,
    padding: 0
  },
  tabContent: {
    backgroundColor: "transparent",
    overflowY: "auto",
    height: "100%",
    padding: 5
  }
};

class SelectTestDefinition extends Component {
  getSelectTestDefinitionSections = () => {
    const sectionsArray = [
      {
        menuString:
          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.sideNavigationItems.allTests,
        body: <AllTests />
      },
      {
        menuString:
          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.sideNavigationItems
            .AllActiveTestVersions,
        body: <AllActiveTestVersions />
      },
      {
        menuString:
          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.sideNavigationItems
            .archivedTestCodes,
        body: <ArchivedTestCodes />
      },
      {
        menuString:
          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.sideNavigationItems.uploadJson,
        body: <UploadJson />
      }
    ];

    return sectionsArray;
  };

  render() {
    const specs = this.getSelectTestDefinitionSections();

    return (
      <div>
        <div>
          <SideNavigation
            specs={specs}
            startIndex={this.props.selectedTestDefinitionSelectionSideNavItem}
            displayNextPreviousButton={false}
            isMain={true}
            tabContainerStyle={styles.tabContainer}
            tabContentStyle={styles.tabContent}
            navStyle={styles.nav}
            bodyContentCustomStyle={styles.sideNavBodyContent}
            updateSelectedSideNavItem={this.props.setTestDefinitionSelectionSideNavState}
          />
        </div>
      </div>
    );
  }
}

export { SelectTestDefinition as unconnectedSelectTestDefinition };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    selectedTestDefinitionSelectionSideNavItem:
      state.testBuilder.selectedTestDefinitionSelectionSideNavItem,
    isTb: state.userPermissions.isTb,
    isTd: state.userPermissions.isTd
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getTestDefinitionVersionsCollected,
      setTestDefinitionSelectionSideNavState
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(SelectTestDefinition);
