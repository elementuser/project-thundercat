import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Row, Button } from "react-bootstrap";
import {
  makeLabel,
  makeTextBoxField,
  makeTextAreaField,
  isNumber,
  makeDropDownField,
  makeMultiDropDownField,
  allValid,
  testLanguageHasFrench,
  testLanguageHasEnglish,
  getNextInNumberSeries
} from "./helpers";
import {
  modifyTestDefinitionField,
  replaceTestDefinitionField,
  addTestDefinitionField,
  EMAILS,
  EMAIL_DETAILS,
  SITUATION_EXAMPLE_RATINGS,
  SITUATION_EXAMPLE_RATING_DETAILS
} from "../../modules/TestBuilderRedux";
import { styles } from "./TestSectionForm";
import { UnconnectedTopTabs as TopTabs } from "../commons/TopTabs";
import QuestionSituationForm from "./QuestionSituationForm";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlusCircle } from "@fortawesome/free-solid-svg-icons";
import { LANGUAGES } from "../commons/Translation";
import CollapsingItemContainer from "../eMIB/CollapsingItemContainer";
import SituationExampleRatingForm from "./SituationExampleRatingForm";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";

class EmailQuestionForm extends Component {
  state = {
    showCompetencyRemoveWarning: false,
    isEnEmailIdValid: true,
    isFrEmailIdValid: true,
    question: {},
    enEmail: {},
    frEmail: {},
    email: {}
  };

  addExampleRating = competency => {
    const { testLanguage, question } = this.props;
    const newSituation = {
      score: 1,
      question: question.id,
      competency_type: competency
    };
    newSituation.id = getNextInNumberSeries(this.props.situationRatings, "id");
    this.props.addTestDefinitionField(SITUATION_EXAMPLE_RATINGS, newSituation, newSituation.id);
    // create test language rating details stuff
    let newDetails = {};
    if (testLanguageHasEnglish(testLanguage)) {
      newDetails = {
        example: "Temp",
        example_rating: newSituation.id,
        language: LANGUAGES.english
      };
      newDetails.id = getNextInNumberSeries(this.props.situationRatingDetails, "id");
      this.props.addTestDefinitionField(
        SITUATION_EXAMPLE_RATING_DETAILS,
        newDetails,
        newDetails.id
      );
    }
    if (testLanguageHasFrench(testLanguage)) {
      newDetails = {
        example: "FR Temp",
        example_rating: newSituation.id,
        language: LANGUAGES.french
      };
      newDetails.id = getNextInNumberSeries(this.props.situationRatingDetails, "id") + 1;
      this.props.addTestDefinitionField(
        SITUATION_EXAMPLE_RATING_DETAILS,
        newDetails,
        newDetails.id
      );
    }
  };

  handleDeleteRating = id => {
    // delete the rating
    let objArray = this.props.situationRatings.filter(obj => obj.id !== id);
    this.props.replaceTestDefinitionField(SITUATION_EXAMPLE_RATINGS, objArray);

    // delete the rating details
    objArray = this.props.situationDetails.filter(obj => {
      if (obj.question !== id) {
        return obj;
      }
      return null;
    });

    this.props.replaceTestDefinitionField(SITUATION_EXAMPLE_RATING_DETAILS, objArray);
  };

  modifyEmail = (inputName, value) => {
    const newObj = { ...this.props.email, ...this.state.email };
    newObj[`${inputName}`] = value;
    this.setState({ email: newObj });
  };

  modifyFrEmail = (inputName, value) => {
    const newObj = { ...this.props.questionDetails.fr, ...this.state.frEmail };
    newObj[`${inputName}`] = value;
    this.setState({ frEmail: newObj });
  };

  modifyEnEmail = (inputName, value) => {
    const newObj = { ...this.props.questionDetails.en, ...this.state.enEmail };
    newObj[`${inputName}`] = value;
    this.setState({ enEmail: newObj });
  };

  emailIdValidation = (name, value) => {
    if (isNumber(value)) {
      this.setState({ isEnEmailIdValid: true });
      this.modifyEmail(name, value);
    }
  };

  onSelectChange = (event, action) => {
    this.modifyEmail(action.name, event.value);
  };

  onMultiSelectChange = (event, action) => {
    const email = { ...this.props.questionDetails.email, ...this.state.email };
    let newArray = [];
    if (action.action === "remove-value") {
      newArray = [...email[`${action.name}`]].filter(obj => obj !== action.removedValue.value);
      this.setState({ newCompetency: { array: newArray, name: action.name } }, this.openDialog());
    } else if (action.action === "clear") {
      newArray = [...email[`${action.name}`], action.option.value];
      this.setState({ newCompetency: { array: newArray, name: action.name } }, this.openDialog());
    } else if (action.action === "select-option") {
      newArray = [...email[`${action.name}`], action.option.value];
      this.modifyEmail(action.name, newArray);
    }
  };

  handleSave = () => {
    if (allValid(this.state)) {
      const { email } = this.props.questionDetails;
      this.props.modifyTestDefinitionField(EMAILS, { ...email, ...this.state.email }, email.id);
      if (testLanguageHasEnglish(this.props.testLanguage)) {
        const enEmail = this.props.questionDetails.en;
        this.props.modifyTestDefinitionField(
          EMAIL_DETAILS,
          { ...enEmail, ...this.state.enEmail },
          enEmail.id
        );
      }
      if (testLanguageHasFrench(this.props.testLanguage)) {
        const frEmail = this.props.questionDetails.fr;
        this.props.modifyTestDefinitionField(
          EMAIL_DETAILS,
          { ...frEmail, ...this.state.frEmail },
          frEmail.id
        );
      }
      // call parents handle save
      this.props.handleSave();
    }
  };

  openDialog = () => {
    this.setState({ showCompetencyRemoveWarning: true });
  };

  closeDialog = () => {
    this.setState({ showCompetencyRemoveWarning: false });
  };

  confirmRemove = () => {
    this.modifyEmail(this.state.newCompetency.name, this.state.newCompetency.array);
    this.closeDialog();
  };

  getLanguageTabs = (enEmail, frEmail) => {
    const frPageSectionTypeContent = (
      <>
        <Row style={styles.rowStyle}>
          {makeLabel(
            "subject_field",
            LOCALIZE.testBuilder.emailQuestions,
            "address-book-question-fr-subject-field"
          )}
          {makeTextBoxField(
            "subject_field",
            LOCALIZE.testBuilder.questions,
            frEmail.subject_field,
            true,
            this.modifyFrEmail,
            "address-book-question-fr-subject-field"
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel(
            "date_field",
            LOCALIZE.testBuilder.emailQuestions,
            "address-book-question-fr-date-field"
          )}
          {makeTextBoxField(
            "date_field",
            LOCALIZE.testBuilder.questions,
            frEmail.date_field,
            true,
            this.modifyFrEmail,
            "address-book-question-fr-date-field"
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("body", LOCALIZE.testBuilder.emailQuestions, "address-book-question-fr-body")}
        </Row>
        <Row style={styles.rowStyle}>
          {makeTextAreaField(
            "body",
            LOCALIZE.testBuilder.questions,
            frEmail.body,
            true,
            this.modifyFrEmail,
            "address-book-question-fr-body"
          )}
        </Row>
      </>
    );
    const enPageSectionTypeContent = (
      <>
        <Row style={styles.rowStyle}>
          {makeLabel(
            "subject_field",
            LOCALIZE.testBuilder.emailQuestions,
            "address-book-question-en-subject-field"
          )}
          {makeTextBoxField(
            "subject_field",
            LOCALIZE.testBuilder.questions,
            enEmail.subject_field,
            true,
            this.modifyEnEmail,
            "address-book-question-en-subject-field"
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel(
            "date_field",
            LOCALIZE.testBuilder.emailQuestions,
            "address-book-question-en-date-field"
          )}
          {makeTextBoxField(
            "date_field",
            LOCALIZE.testBuilder.questions,
            enEmail.date_field,
            true,
            this.modifyEnEmail,
            "address-book-question-en-date-field"
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("body", LOCALIZE.testBuilder.emailQuestions, "address-book-question-en-body")}
        </Row>
        <Row style={styles.rowStyle}>
          {makeTextAreaField(
            "body",
            LOCALIZE.testBuilder.questions,
            enEmail.body,
            true,
            this.modifyEnEmail,
            "address-book-question-en-body"
          )}
        </Row>
      </>
    );
    const TABS = [];
    if (testLanguageHasEnglish(this.props.testLanguage)) {
      TABS.push({
        key: LOCALIZE.commons.english,
        tabName: LOCALIZE.commons.english,
        body: enPageSectionTypeContent
      });
    }
    if (testLanguageHasFrench(this.props.testLanguage)) {
      TABS.push({
        key: LOCALIZE.commons.french,
        tabName: LOCALIZE.commons.french,
        body: frPageSectionTypeContent
      });
    }

    return TABS;
  };

  selectedOption = field => {
    const fieldVar = Array.isArray(field) ? field : [field];
    const array = [];
    for (const contactId of fieldVar) {
      for (const contact of this.props.addressBook) {
        if (contactId === contact.id) {
          array.push({ label: contact.name, value: contact.id });
        }
      }
    }
    return array;
  };

  selectedCompetency = field => {
    const fieldVar = Array.isArray(field) ? field : [field];
    const array = [];
    for (const id of fieldVar) {
      for (const contact of this.props.competencyTypes) {
        if (id === contact.id) {
          array.push({ label: contact[`${this.props.currentLanguage}_name`], value: contact.id });
        }
      }
    }
    return array;
  };

  render() {
    const { questionDetails } = this.props;
    const situationDetails = { en: questionDetails.enSituation, fr: questionDetails.frSituation };
    const enEmail = { ...questionDetails.en, ...this.state.enEmail };
    const frEmail = { ...questionDetails.fr, ...this.state.frEmail };
    const email = { ...questionDetails.email, ...this.state.email };
    const TABS = this.getLanguageTabs(enEmail, frEmail);

    const contactOptions = this.props.addressBook.map(contact => {
      return { label: contact.name, value: contact.id };
    });
    const competencyOptions = this.props.competencyTypes.map(comp => {
      return { label: comp[`${this.props.currentLanguage}_name`], value: comp.id };
    });

    const competency = this.selectedCompetency(email.competency_types);
    const toField = this.selectedOption(email.to_field);
    const fromField = this.selectedOption(email.from_field)[0];
    const ccField = this.selectedOption(email.cc_field);

    return (
      <div>
        <Row style={styles.rowStyle}>
          {makeLabel("email_id", LOCALIZE.testBuilder.emailQuestions)}
          {makeTextBoxField(
            "email_id",
            LOCALIZE.testBuilder.emailQuestions,
            email.email_id,
            true,
            this.emailIdValidation
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("from_field", LOCALIZE.testBuilder.emailQuestions)}
          {makeDropDownField(
            "from_field",
            LOCALIZE.testBuilder.emailQuestions,
            fromField,
            contactOptions,
            this.onSelectChange
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("to_field", LOCALIZE.testBuilder.emailQuestions)}
          {makeMultiDropDownField(
            "to_field",
            LOCALIZE.testBuilder.emailQuestions,
            toField,
            contactOptions,
            this.onMultiSelectChange
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("cc_field", LOCALIZE.testBuilder.emailQuestions)}
          {makeMultiDropDownField(
            "cc_field",
            LOCALIZE.testBuilder.emailQuestions,
            ccField,
            contactOptions,
            this.onMultiSelectChange
          )}
        </Row>
        <TopTabs TABS={TABS} defaultTab={TABS[0].key} />

        <h2>{LOCALIZE.testBuilder.emailQuestions.scoringTitle}</h2>
        <Row style={styles.rowStyle}>
          {makeLabel("competency_types", LOCALIZE.testBuilder.emailQuestions)}
          {makeMultiDropDownField(
            "competency_types",
            LOCALIZE.testBuilder.emailQuestions,
            competency,
            competencyOptions,
            this.onMultiSelectChange
          )}
        </Row>
        <QuestionSituationForm
          situationDetails={situationDetails}
          handleSave={this.handleSave}
          handleDelete={this.props.handleDelete}
          testLanguage={this.props.testLanguage}
        />
        <h4>{LOCALIZE.testBuilder.emailQuestions.exampleRatingsTitle}</h4>
        {competency.map((comp, key) => {
          return (
            <div key={key}>
              <h5 style={styles.padding15}>{comp.label}</h5>

              {this.props.situationRatings
                .filter(
                  obj =>
                    obj.question === this.props.question.id && obj.competency_type === comp.value
                )
                .map((rating, index) => {
                  const details = {};
                  for (const detail of this.props.situationRatingDetails) {
                    if (detail.example_rating === rating.id) {
                      if (detail.language === LANGUAGES.english) {
                        details.en = detail;
                      }
                      if (detail.language === LANGUAGES.french) {
                        details.fr = detail;
                      }
                    }
                  }
                  return (
                    // TODO (fnormand): add move up and down functionalities
                    <CollapsingItemContainer
                      key={index}
                      title={<label>{rating.score}</label>}
                      body={
                        <SituationExampleRatingForm
                          rating={rating}
                          details={details}
                          handleDelete={this.handleDeleteRating}
                          testLanguage={this.props.testLanguage}
                        />
                      }
                    />
                  );
                })}
              <Button variant={"primary"} onClick={() => this.addExampleRating(comp.value)}>
                <FontAwesomeIcon icon={faPlusCircle} style={styles.addButton} />
                {LOCALIZE.testBuilder.questionSections.addButton}
              </Button>
            </div>
          );
        })}
        <PopupBox
          show={this.state.showCompetencyRemoveWarning}
          handleClose={this.closeDialog}
          title={LOCALIZE.commons.deleteConfirmation}
          description={
            <div>
              <p>{LOCALIZE.testBuilder.emailQuestions.deleteDescription}</p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={LOCALIZE.commons.deleteButton}
          rightButtonAction={this.confirmRemove}
        />
      </div>
    );
  }
}

export { EmailQuestionForm as UnconnectedEmailQuestionForm };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    addressBook: state.testBuilder.address_book,
    competencyTypes: state.testBuilder.competency_types,
    situationRatings: state.testBuilder.situation_example_ratings,
    situationRatingDetails: state.testBuilder.situation_example_rating_details
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    { modifyTestDefinitionField, replaceTestDefinitionField, addTestDefinitionField },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(EmailQuestionForm);
