import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Row } from "react-bootstrap";
import {
  makeLabel,
  makeSaveDeleteButtons,
  makeTextAreaField,
  testLanguageHasEnglish,
  testLanguageHasFrench,
  getNextInNumberSeries
} from "./helpers";
import {
  modifyTestDefinitionField,
  QUESTION_SECTION_DEFINITIONS,
  replaceTestDefinitionField,
  addTestDefinitionField
} from "../../modules/TestBuilderRedux";
import { styles } from "./TestSectionForm";
import { UnconnectedTopTabs as TopTabs } from "../commons/TopTabs";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import { LANGUAGES } from "../commons/Translation";

class MarkdownQuestionSectionForm extends Component {
  state = {
    showDialog: false,
    enDefinition: {},
    frDefinition: {}
  };

  modifyEnDefinition = (inputName, value) => {
    const newObj = { ...this.props.enDefinition, ...this.state.enDefinition };
    newObj[`${inputName}`] = value;
    this.setState({ enDefinition: newObj });
  };

  modifyFrDefinition = (inputName, value) => {
    const newObj = { ...this.props.frDefinition, ...this.state.frDefinition };
    newObj[`${inputName}`] = value;
    this.setState({ frDefinition: newObj });
  };

  openDialog = () => {
    this.setState({ showDialog: true });
  };

  closeDialog = () => {
    this.setState({ showDialog: false });
  };

  confirmDelete = () => {
    this.props.handleDelete();
    this.closeDialog();
  };

  handleSave = () => {
    const { questionSection } = this.props;

    let enDefinition = {};
    let frDefinition = {};

    // enDefinition does not exist
    if (Object.entries(this.props.enDefinition).length <= 0) {
      // creating the object with the needed attributes
      enDefinition = {
        id: getNextInNumberSeries(this.props.questionSectionDefinitions, "id"),
        content:
          Object.entries(this.state.enDefinition).length <= 0
            ? ""
            : this.state.enDefinition.content,
        language: LANGUAGES.english,
        question_section: questionSection.id,
        question_section_type: questionSection.question_section_type
      };
      // enDefinition exists
    } else {
      enDefinition = {
        ...this.props.enDefinition,
        ...this.state.enDefinition,
        question_section_type: questionSection.question_section_type
      };
    }

    // frDefinition does not exist
    if (Object.entries(this.props.frDefinition).length <= 0) {
      // creating the object with the needed attributes
      frDefinition = {
        id: getNextInNumberSeries(this.props.questionSectionDefinitions, "id"),
        content:
          Object.entries(this.state.frDefinition).length <= 0
            ? ""
            : this.state.frDefinition.content,
        language: LANGUAGES.french,
        question_section: questionSection.id,
        question_section_type: questionSection.question_section_type
      };
      // frDefinition exists
    } else {
      frDefinition = {
        ...this.props.frDefinition,
        ...this.state.frDefinition,
        question_section_type: questionSection.question_section_type
      };
    }

    if (testLanguageHasEnglish(this.props.testLanguage)) {
      this.props.modifyTestDefinitionField(
        QUESTION_SECTION_DEFINITIONS,
        enDefinition,
        enDefinition.id
      );
    }
    if (testLanguageHasFrench(this.props.testLanguage)) {
      this.props.modifyTestDefinitionField(
        QUESTION_SECTION_DEFINITIONS,
        frDefinition,
        frDefinition.id
      );
    }
    this.props.handleSave(questionSection.question_section_type);
  };

  getMarkdownSectionTabs = (enDefinition, frDefinition) => {
    const frPageSectionTypeContent = (
      <>
        <Row style={styles.rowStyle}>
          {makeLabel(
            "content",
            LOCALIZE.testBuilder.questionSectionMarkdown,
            "question-section-fr-content-field"
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeTextAreaField(
            "content",
            LOCALIZE.testBuilder.questionSectionMarkdown,
            frDefinition.content,
            true,
            this.modifyFrDefinition,
            "question-section-fr-content-field"
          )}
        </Row>
      </>
    );
    const enPageSectionTypeContent = (
      <>
        <Row style={styles.rowStyle}>
          {makeLabel(
            "content",
            LOCALIZE.testBuilder.questionSectionMarkdown,
            "question-section-en-content-field"
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeTextAreaField(
            "content",
            LOCALIZE.testBuilder.questionSectionMarkdown,
            enDefinition.content,
            true,
            this.modifyEnDefinition,
            "question-section-en-content-field"
          )}
        </Row>
      </>
    );

    const TABS = [];
    if (testLanguageHasEnglish(this.props.testLanguage)) {
      TABS.push({
        key: LOCALIZE.commons.english,
        tabName: LOCALIZE.commons.english,
        body: enPageSectionTypeContent
      });
    }
    if (testLanguageHasFrench(this.props.testLanguage)) {
      TABS.push({
        key: LOCALIZE.commons.french,
        tabName: LOCALIZE.commons.french,
        body: frPageSectionTypeContent
      });
    }
    return TABS;
  };

  render() {
    const enDefinition = { ...this.props.enDefinition, ...this.state.enDefinition };
    const frDefinition = { ...this.props.frDefinition, ...this.state.frDefinition };

    const TABS = this.getMarkdownSectionTabs(enDefinition, frDefinition);

    return (
      <div style={styles.mainContiner}>
        <TopTabs TABS={TABS} defaultTab={TABS[0].key} />

        <Row className="justify-content-between" style={styles.buttonRowStyle}>
          {makeSaveDeleteButtons(
            this.handleSave,
            this.openDialog,
            LOCALIZE.commons.applyButton,
            LOCALIZE.commons.deleteButton
          )}
        </Row>
        <PopupBox
          show={this.state.showDialog}
          handleClose={this.closeDialog}
          title={LOCALIZE.testBuilder.questionSectionMarkdown.delete.title}
          description={<div>{LOCALIZE.testBuilder.questionSectionMarkdown.delete.content}</div>}
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={LOCALIZE.commons.deleteButton}
          rightButtonAction={this.confirmDelete}
        />
      </div>
    );
  }
}

export { MarkdownQuestionSectionForm as unconnectedMarkdownQuestionSectionForm };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    questionSections: state.testBuilder.question_sections,
    questionSectionDefinitions: state.testBuilder.question_section_definitions
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    { modifyTestDefinitionField, replaceTestDefinitionField, addTestDefinitionField },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(MarkdownQuestionSectionForm);
