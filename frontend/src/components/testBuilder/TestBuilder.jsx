import React, { Component } from "react";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { isTokenStillValid } from "../../modules/LoginRedux";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import ContentContainer from "../commons/ContentContainer";
import "../../css/test-administration.css";
import { styles as SystemAdministrationStyles } from "../etta/SystemAdministration";
import SideNavigation from "../eMIB/SideNavigation";
import TestDefinition from "./TestDefinition";
import TestSections from "./TestSections";
import TestSectionComponents from "./TestSectionComponents";
import SectionComponentPages from "./SectionComponentPages";
import ComponentPageSections from "./ComponentPageSections";
import TestManagement from "./TestManagement";
import { Button, Row, Col, Tab, Tabs } from "react-bootstrap";
import Select from "react-select";
import AddressBookContacts from "./AddressBookContacts";
import { HEADER_HEIGHT } from "../eMIB/constants";
import { history } from "../../store-index";
import { PATH } from "../commons/Constants";
import { resetTestFactoryState } from "../../modules/TestSectionRedux";
import { resetNotepadState } from "../../modules/NotepadRedux";
import { resetQuestionListState } from "../../modules/QuestionListRedux";
import { resetTopTabsState, switchSideTab, switchTopTab } from "../../modules/NavTabsRedux";
import {
  setTestSectionToView,
  setSelectedTestDefinitionId,
  resetTestBuilderState,
  getTestDefinitionExtract,
  loadTestDefinition,
  setTestDefinitionValidationErrors,
  setQuestionRulesValidState,
  setQuestionRulesValidStateWhenItemExposureEnabled,
  setTestBuilderSideNavState,
  setNewTestDefinitionUploadedState,
  setTestBuilderTopTab
} from "../../modules/TestBuilderRedux";
import Questions from "./Questions";
import ScoringMethod from "./ScoringMethod";
import QuestionBlockTypes from "./QuestionBlockTypes";
import CompetencyTypes from "./CompetencyTypes";
import SelectTestDefinition from "./SelectTestDefinition";
import CustomButton, { THEME } from "../commons/CustomButton";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import SystemMessage, { MESSAGE_TYPE } from "../commons/SystemMessage";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faBinoculars,
  faArrowAltCircleLeft,
  faTimes,
  faCheck
} from "@fortawesome/free-solid-svg-icons";
import { TestSectionComponentType, TestSectionScoringTypeObject } from "../testFactory/Constants";
import LastLogin from "../authentication/LastLogin";
import {
  validateQuestionRulesData,
  validateQuestionRulesDataWithEnabledItemExposure
} from "./validation";
import AllItemBanks from "./itemBank/AllItemBanks";
import TopTabs from "../commons/TopTabs";
import { triggerAppRerender } from "../../modules/AppRedux";

const styles = {
  backButtonStyle: {
    marginBottom: 24
  },
  borderBox: {
    borderBottom: "1px solid",
    borderRadius: "5px",
    margin: "0px 15px 0px 15px",
    padding: "15px",
    borderColor: "#CECECE"
  },
  tabContent: {
    backgroundColor: "transparent",
    overflowY: "auto",
    padding: "5px 5px 5px 5px"
  },
  loadingLabel: {
    paddingTop: "20px"
  },
  buttonLabel: {
    marginLeft: 6
  },
  appPadding: {
    padding: "15px"
  },
  backgroundColor: {
    backgroundColor: "#F8F8F8"
  }
};

class TestBuilder extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  state = {
    viewTestSectionOrderNumber: undefined,
    isTestDefinitionSelected: this.props.selectedTestDefinitionId !== null,
    isLoading: false,
    showBackToTestDefinitionSelectionPopup: false,
    displayAddressBookContacts: false,
    displayCompetencyTypes: false,
    isTestBuilderDashboard: false,
    triggerRerender: false
  };

  // calling redux state on page load to get the first name and last name
  componentDidMount = () => {
    this._isMounted = true;
    // check if the token is still valid before getting user information
    this.props.isTokenStillValid().then(bool => {
      // if the token is still valid
      if (this._isMounted && bool) {
        // populate test definition
        this.props.switchTopTab(100);
      }
    });
    // getting dynamic side nav items
    this.getSideNavigationItemsToDisplay();
    // to display last login if it is a test builder dashboard
    if (this.props.currentHomePage === PATH.testBuilder) {
      this.setState({
        isTestBuilderDashboard: true
      });
    }
    // triggering app rerender to make sure that the special page height and overflow is properly set
    this.props.triggerAppRerender();
  };

  componentDidUpdate(prevProps, prevState, snapshot) {
    // check if the token is still valid before getting user information
    this.props.isTokenStillValid().then(bool => {
      // if the token is still valid
      if (this._isMounted && bool) {
        // if selectedTestDefinitionId gets updated
        if (prevProps.selectedTestDefinitionId !== this.props.selectedTestDefinitionId) {
          // if defined
          if (this.props.selectedTestDefinitionId !== null) {
            this.setState({
              isTestDefinitionSelected: true
            });
          }
        }
      }
    });
    // if triggerTestDefinitionLoading gets updated
    if (prevProps.triggerTestDefinitionLoading !== this.props.triggerTestDefinitionLoading) {
      this.populateTestDefinition();
    }
    // if triggerTestDefinitionLoadingFromJson gets updated
    if (
      prevProps.triggerTestDefinitionLoadingFromJson !==
      this.props.triggerTestDefinitionLoadingFromJson
    ) {
      this.populateTestDefinitionBasedOnJsonContent();
    }
    // if testSections gets updated
    if (prevProps.testSections !== this.props.testSections) {
      // getting dynamic side nav items
      this.getSideNavigationItemsToDisplay();
    }
    // if testSectionComponents gets updated
    if (prevProps.testSectionComponents !== this.props.testSectionComponents) {
      // getting dynamic side nav items
      this.getSideNavigationItemsToDisplay();
    }
    // if scoringMethodValidState gets updated
    if (prevProps.scoringMethodValidState !== this.props.scoringMethodValidState) {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }
  }

  componentWillUnmount = () => {
    this._isMounted = false;
  };

  // Returns array where each item indicates specifications related to How To Page including the title and the body
  getTestBuilderSections = () => {
    let sideNavArray = [];
    sideNavArray = [
      {
        menuString: LOCALIZE.testBuilder.sideNavItems.testDefinition,
        body: <TestDefinition />,
        error:
          typeof this.props.validationErrors !== "undefined"
            ? !this.props.validationErrors.testDefinitionContainsValidTestDefinitionData
            : false
      },
      {
        menuString: LOCALIZE.testBuilder.sideNavItems.testSections,
        body: <TestSections />,
        error:
          typeof this.props.validationErrors !== "undefined"
            ? !this.props.validationErrors.testDefinitionContainsAtLeastThreeTestSections ||
              !this.props.validationErrors.testDefinitionContainsQuitTestSection ||
              !this.props.validationErrors.testDefinitionContainsFinishTestSection
            : false
      },
      {
        menuString: LOCALIZE.testBuilder.sideNavItems.testSectionComponents,
        body: <TestSectionComponents />,
        error:
          typeof this.props.validationErrors !== "undefined"
            ? !this.props.validationErrors.testDefinitionContainsValidQuestionRules ||
              !this.props.validationErrors.testDefinitionContainsValidSubSections ||
              !this.props.validationErrors.testDefinitionContainsValidItemBankRules
            : false
      },
      {
        menuString: LOCALIZE.testBuilder.sideNavItems.sectionComponentPages,
        body: <SectionComponentPages />,
        error:
          typeof this.props.validationErrors !== "undefined"
            ? !this.props.validationErrors.testDefinitionContainsValidSectionComponentPages
            : false
      },
      {
        menuString: LOCALIZE.testBuilder.sideNavItems.componentPageSections,
        body: <ComponentPageSections />
      },
      {
        menuString: LOCALIZE.testBuilder.sideNavItems.questionBlockTypes,
        body: <QuestionBlockTypes />
      },
      {
        menuString: LOCALIZE.testBuilder.sideNavItems.questions,
        body: <Questions />,
        error:
          typeof this.props.validationErrors !== "undefined"
            ? !this.props.validationErrors.testDefinitionContainsValidQuestionLists
            : false
      },
      {
        menuString: LOCALIZE.testBuilder.sideNavItems.scoring,
        body: <ScoringMethod />,
        error:
          typeof this.props.validationErrors !== "undefined"
            ? !this.props.validationErrors.testDefinitionContainsValidScoringMethod
            : false
      },
      {
        menuString: LOCALIZE.testBuilder.sideNavItems.testManagement,
        body: <TestManagement />
      }
    ];

    // if displayAddressBookContacts is true
    if (this.state.displayAddressBookContacts) {
      // adding AddressBookContacts side nav items to the 5th index
      sideNavArray.splice(5, 0, {
        menuString: LOCALIZE.testBuilder.sideNavItems.addressBook,
        body: <AddressBookContacts />
      });
    }

    // if displayCompetencyTypes is true
    if (this.state.displayCompetencyTypes) {
      // getting the right index depending on displayAddressBookContacts state
      let index = 8;
      if (this.state.displayAddressBookContacts) {
        index = 9;
      }
      // adding CompetencyTypes side nav items to the 8th/9th index (depending on the displayAddressBookContacts state)
      sideNavArray.splice(index, 0, {
        menuString: LOCALIZE.testBuilder.sideNavItems.competencyTypes,
        body: <CompetencyTypes />
      });
    }

    return sideNavArray;
  };

  getSideNavigationItemsToDisplay = () => {
    // initializing needed variables
    let displayAddressBookContacts = false;
    let displayCompetencyTypes = false;

    // looping in test sections
    for (let i = 0; i < this.props.testSections.length; i++) {
      // if at least one test section component has an Inbox as component type
      if (
        this.props.testSections[i].scoring_type === TestSectionScoringTypeObject.COMPETENCY.value
      ) {
        displayCompetencyTypes = true;
      }
    }

    // looping in test section components
    for (let i = 0; i < this.props.testSectionComponents.length; i++) {
      // if at least one test section component has an Inbox as component type
      if (this.props.testSectionComponents[i].component_type === TestSectionComponentType.INBOX) {
        displayAddressBookContacts = true;
      }
    }

    // setting needed states
    this.setState({
      displayCompetencyTypes: displayCompetencyTypes,
      displayAddressBookContacts: displayAddressBookContacts
    });

    // if validationErrors is not defined
    if (typeof this.props.validationErrors === "undefined") {
      // initializing redux test definition validation errors state
      this.props.setTestDefinitionValidationErrors({
        testDefinitionContainsValidTestDefinitionData: true,
        testDefinitionContainsAtLeastThreeTestSections: true,
        testDefinitionContainsQuitTestSection: true,
        testDefinitionContainsFinishTestSection: true,
        testDefinitionContainsValidQuestionRules: true,
        testDefinitionQuestionRulesErrorType: null,
        testDefinitionContainsValidScoringMethod: true,
        testDefinitionContainsValidSubSections: true,
        testDefinitionContainsValidQuestionLists: true,
        testDefinitionContainsValidSectionComponentPages: true,
        testDefinitionContainsValidItemBankRules: true
      });
    }
  };

  goToTestView = () => {
    this.props.setTestSectionToView(this.state.viewTestSectionOrderNumber);
    this.resetAllRedux();
    history.push(PATH.testBuilderTest);
  };

  resetAllRedux = () => {
    this.props.resetTestFactoryState();
    this.props.resetNotepadState();
    this.props.resetQuestionListState();
    this.props.resetTopTabsState();
  };

  selectTestSection = event => {
    this.setState({ viewTestSectionOrderNumber: event.value });
  };

  handleGoBackToTestDefinitionSelection = () => {
    this.setState({ isLoading: true, isTestDefinitionSelected: false }, () => {
      // reset all test builder redux states
      this.props.resetTestBuilderState();

      // Setting tab back to test builder
      this.props.setTestBuilderTopTab("test-builder");

      // reloading page
      window.location.reload();
    });
  };

  populateTestDefinition = () => {
    this.setState({ isLoading: true }, () => {
      let testDefinitionId = null;
      // "null" comes from the backend (since the default redux state is null without double quotes)
      // "undefined" right after triggering the "New Test Definition" action
      if (
        this.props.selectedTestDefinitionId === "null" ||
        typeof this.props.selectedTestDefinitionId === "undefined"
      ) {
        testDefinitionId = this.props.test_definition.id;
        // selectedTestDefinitionId is defined
      } else {
        testDefinitionId = this.props.selectedTestDefinitionId;
      }
      // ==================== CREATING NEW TEST DEFINITION ====================
      if (testDefinitionId === "NEW-TD") {
        // setting new test definition redux state (setting version to <undefined> + setting other needed states)
        const testDefinitionData = {
          test_definition: [
            {
              id: testDefinitionId,
              parent_code: "",
              test_code: "",
              version: LOCALIZE.testBuilder.testDefinition.undefinedVersion,
              retest_period: 0,
              en_name: "",
              fr_name: "",
              is_public: null,
              count_up: false,
              active: false,
              test_language: null
            }
          ],
          item_bank_rules: [],
          test_sections: [],
          next_section_buttons: [],
          test_section_components: [],
          section_component_pages: [],
          page_sections: [],
          page_section_definitions: [],
          address_book: [],
          address_book_contacts: [],
          questions: [],
          question_list_rules: [],
          question_block_types: [],
          question_sections: [],
          question_section_definitions: [],
          emails: [],
          email_details: [],
          contact_details: [],
          answers: [],
          answer_details: [],
          multiple_choice_question_details: [],
          competency_types: [],
          question_situations: [],
          situation_example_ratings: [],
          situation_example_rating_details: [],
          scoring_method_type: ""
        };
        this.props.loadTestDefinition(testDefinitionData);
        // updating validation errors state
        this.props.setTestDefinitionValidationErrors({
          testDefinitionContainsValidTestDefinitionData: true,
          testDefinitionContainsAtLeastThreeTestSections: true,
          testDefinitionContainsQuitTestSection: true,
          testDefinitionContainsFinishTestSection: true,
          testDefinitionContainsValidQuestionRules: true,
          testDefinitionQuestionRulesErrorType: null,
          testDefinitionContainsValidScoringMethod: true,
          testDefinitionContainsValidSubSections: true,
          testDefinitionContainsValidQuestionLists: true,
          testDefinitionContainsValidSectionComponentPages: true,
          testDefinitionContainsValidItemBankRules: true
        });
        // updating isLoading state
        this.setState({ isLoading: false });
        // ==================== SELECTING EXISTING TEST DEFINITION ====================
      } else {
        // getting test definition extract data
        this.props
          .getTestDefinitionExtract(testDefinitionId, this.props.currentLanguage)
          .then(response => {
            // making sure that we don't update the triggerTestDefinitionLoadingFromJson and newTestDefinitionUploaded redux states
            response.body.new_test.triggerTestDefinitionLoadingFromJson =
              this.props.triggerTestDefinitionLoadingFromJson;
            response.body.new_test.newTestDefinitionUploaded = this.props.newTestDefinitionUploaded;
            this.props.loadTestDefinition(response.body.new_test);
            // need to call the question rules validation here to have the redux states properly initialized
            // ==================== QUESTION RULES VALIDATION ==========
            // basic question rules validation
            // call question rules validation
            const areQuestionsRulesValid = validateQuestionRulesData(
              response.body.new_test.questions,
              response.body.new_test.question_list_rules
            );
            // setting redux question rules validity state
            this.props.setQuestionRulesValidState(areQuestionsRulesValid);

            // question rules validation if at least one section has item_exposure enabled
            // initializing testSectionComponentIdsArray
            const testSectionComponentIdsArray = [];
            // looping in questions
            for (let i = 0; i < response.body.new_test.questions.length; i++) {
              // if test_section_component of current question is not already in testSectionComponentIdsArray
              if (
                !testSectionComponentIdsArray.includes(
                  response.body.new_test.questions[i].test_section_component
                )
              ) {
                // populating testSectionComponentIdsArray
                testSectionComponentIdsArray.push(
                  response.body.new_test.questions[i].test_section_component
                );
              }
            }
            // initializing areQuestionsRulesValidWhenItemExposureEnabled
            let areQuestionsRulesValidWhenItemExposureEnabled = {
              is_valid: true,
              error_type: null
            };
            // looping in testSectionComponentIdsArray
            for (let i = 0; i < testSectionComponentIdsArray.length; i++) {
              // getting current test section
              const currentTestSection = response.body.new_test.test_sections.filter(
                obj =>
                  obj.id ===
                  response.body.new_test.test_section_components.filter(
                    obj => obj.id === testSectionComponentIdsArray[i]
                  )[0].test_section[0]
              )[0];
              // if current test section has item_exposure enabled
              if (currentTestSection.item_exposure) {
                areQuestionsRulesValidWhenItemExposureEnabled =
                  validateQuestionRulesDataWithEnabledItemExposure(
                    response.body.new_test.questions,
                    response.body.new_test.question_list_rules
                  );
                // not valid
                if (!areQuestionsRulesValidWhenItemExposureEnabled.is_valid) {
                  // setting question rules redux states
                  this.props.setQuestionRulesValidStateWhenItemExposureEnabled(
                    areQuestionsRulesValidWhenItemExposureEnabled.is_valid,
                    areQuestionsRulesValidWhenItemExposureEnabled.error_type
                  );
                  break;
                }
                // setting question rules redux states
                this.props.setQuestionRulesValidStateWhenItemExposureEnabled(
                  areQuestionsRulesValidWhenItemExposureEnabled.is_valid,
                  areQuestionsRulesValidWhenItemExposureEnabled.error_type
                );
              }
            }
            // ==================== QUESTION RULES VALIDATION (END) ==========
          })
          .then(() => {
            // handling selected side navigation logic
            // if test has just been uploaded
            if (this.props.newTestDefinitionUploaded) {
              // updating selected side navigation item (to Test Management item)
              const sideNavigationItems = this.getTestBuilderSections();
              this.props.setTestBuilderSideNavState(sideNavigationItems.length);
              // setting newTestDefinitionUploaded to false
              this.props.setNewTestDefinitionUploadedState(false);
            }
          })
          .then(() => {
            // updating isLoading state
            this.setState({ isLoading: false });
          });
      }
    });
  };

  populateTestDefinitionBasedOnJsonContent = () => {
    this.setState({ isLoading: true }, () => {
      // creating copy of test_definition
      const copyOfTestDefinition = this.props.test_definition;
      // updating version number and version notes
      copyOfTestDefinition.version = LOCALIZE.testBuilder.testDefinition.undefinedVersion;
      copyOfTestDefinition.version_notes = "";
      this.setState({ isLoading: false });
    });
  };

  openBackToTestDefinitionSelectionPopup = () => {
    this.setState({ showBackToTestDefinitionSelectionPopup: true });
  };

  closeBackToTestDefinitionSelectionPopup = () => {
    this.setState({ showBackToTestDefinitionSelectionPopup: false });
  };

  render() {
    const specs = this.getTestBuilderSections();
    const testSectionOptions = this.props.testSections.map(testSection => {
      return {
        label: testSection[`${this.props.currentLanguage}_title`],
        value: testSection.order
      };
    });

    const TABS = [];

    // display top tabs including item banks
    if (!this.state.isTestDefinitionSelected && this.props.isTd) {
      TABS.push(
        {
          // TODO: turn into a component call, like <TestBuilder... />
          key: "item-banks",
          tabName: LOCALIZE.testBuilder.itemBanksContainerLabel,
          body: (
            <div
              style={{
                ...SystemAdministrationStyles.sectionContainer,
                ...styles.appPadding,
                ...styles.backgroundColor
              }}
            >
              <AllItemBanks />
            </div>
          )
        },
        {
          key: "test-builder",
          tabName: LOCALIZE.testBuilder.containerLabel,
          body: (
            <div style={styles.backgroundColor}>
              {this.props.selectedTestDefinitionId === null && (
                <div style={SystemAdministrationStyles.sectionContainer}>
                  <SelectTestDefinition />
                </div>
              )}
            </div>
          )
        }
      );
      // display top tabs excluding item banks for non test developers
    } else if (!this.state.isTestDefinitionSelected && !this.props.isTd) {
      TABS.push({
        key: "test-builder",
        tabName: LOCALIZE.testBuilder.containerLabel,
        body: (
          <div style={styles.backgroundColor}>
            {this.props.selectedTestDefinitionId === null && (
              <div style={SystemAdministrationStyles.sectionContainer}>
                <SelectTestDefinition />
              </div>
            )}
          </div>
        )
      });
      // display the test builder inside of a test definition
    } else {
      TABS.push({
        key: "test-builder",
        tabName: LOCALIZE.testBuilder.containerLabel,
        body: (
          <div
            style={{ ...SystemAdministrationStyles.sectionContainer, ...styles.backgroundColor }}
          >
            <div style={styles.borderBox}>
              <Row>
                <Col>
                  <Select
                    styles={{
                      // Fixes the overlapping problem of the component
                      menu: provided => ({ ...provided, zIndex: 9999 })
                    }}
                    options={testSectionOptions}
                    placeholder={LOCALIZE.testBuilder.testDefinitionSelectPlaceholder}
                    onChange={this.selectTestSection}
                  />
                </Col>
                <Col>
                  <Button
                    disabled={this.state.viewTestSectionOrderNumber === undefined}
                    variant={this.props.variant}
                    onClick={this.goToTestView}
                    style={{ width: "100%" }}
                  >
                    <>
                      <FontAwesomeIcon icon={faBinoculars} />
                      <span style={styles.buttonLabel}>{LOCALIZE.testBuilder.inTestView}</span>
                    </>
                  </Button>
                </Col>
              </Row>
            </div>
            <SideNavigation
              specs={specs}
              startIndex={this.props.selectedSideNavItem}
              displayNextPreviousButton={false}
              isMain={true}
              tabContainerStyle={SystemAdministrationStyles.tabContainer}
              tabContentStyle={styles.tabContent}
              navStyle={SystemAdministrationStyles.nav}
              bodyContentCustomStyle={SystemAdministrationStyles.sideNavBodyContent}
              updateSelectedSideNavItem={this.props.setTestBuilderSideNavState}
            />
          </div>
        )
      });
    }

    return (
      <div className="app" style={styles.appPadding}>
        {this.state.isTestBuilderDashboard && <LastLogin lastLoginDate={this.props.lastLogin} />}
        <Helmet>
          <html lang={this.props.currentLanguage} />
          <title className="notranslate">{LOCALIZE.titles.testBuilder}</title>
        </Helmet>
        <ContentContainer>
          <div id="main-content" role="main">
            {!this.state.isTestDefinitionSelected && (
              <div
                id="user-welcome-message-div"
                style={styles.header}
                aria-labelledby="user-welcome-message"
                className="notranslate"
              >
                <h1 id="user-welcome-message" className="green-divider">
                  {LOCALIZE.formatString(
                    LOCALIZE.profile.title,
                    this.props.firstName,
                    this.props.lastName
                  )}
                </h1>
              </div>
            )}
            <div>
              {this.state.isTestDefinitionSelected && (
                <div style={styles.backButtonStyle}>
                  <CustomButton
                    label={
                      <>
                        <FontAwesomeIcon icon={faArrowAltCircleLeft} />
                        <span style={styles.buttonLabel}>
                          {LOCALIZE.testBuilder.backToTestDefinitionSelection}
                        </span>
                      </>
                    }
                    action={this.openBackToTestDefinitionSelectionPopup}
                    buttonTheme={THEME.SECONDARY}
                  />
                </div>
              )}
              {!this.state.isLoading && (
                <Row>
                  <Col>
                    <TopTabs
                      TABS={TABS}
                      setTab={this.props.setTestBuilderTopTab}
                      customCurrentTabProp={this.props.testBuilderTopTab}
                    />
                  </Col>
                </Row>
              )}
              {this.state.isLoading && (
                <h2 style={styles.loadingLabel}>{LOCALIZE.commons.loading}</h2>
              )}
            </div>
          </div>
        </ContentContainer>
        <PopupBox
          show={this.state.showBackToTestDefinitionSelectionPopup}
          handleClose={this.closeBackToTestDefinitionSelectionPopup}
          title={LOCALIZE.testBuilder.backToTestDefinitionSelectionPopup.title}
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.warning}
                title={LOCALIZE.commons.warning}
                message={
                  <p>
                    {
                      LOCALIZE.testBuilder.backToTestDefinitionSelectionPopup
                        .systemMessageDescription
                    }
                  </p>
                }
              />
              <p>{LOCALIZE.testBuilder.backToTestDefinitionSelectionPopup.description}</p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.confirm}
          rightButtonAction={this.handleGoBackToTestDefinitionSelection}
          rightButtonIcon={faCheck}
        />
      </div>
    );
  }
}

export { TestBuilder as UnconnectedTestBuilder };

const mapStateToProps = (state, ownProps) => {
  // done because only one test definition is returned ever
  const INDEX = 0;
  return {
    test_definition: state.testBuilder.test_definition[INDEX],
    currentLanguage: state.localize.language,
    firstName: state.user.firstName,
    lastName: state.user.lastName,
    testSections: state.testBuilder.test_sections,
    testSectionComponents: state.testBuilder.test_section_components,
    sectionComponentPages: state.testBuilder.section_component_pages,
    questions: state.testBuilder.questions,
    selectedTestDefinitionId: state.testBuilder.selectedTestDefinitionId,
    triggerTestDefinitionLoading: state.testBuilder.triggerTestDefinitionLoading,
    triggerTestDefinitionLoadingFromJson: state.testBuilder.triggerTestDefinitionLoadingFromJson,
    validationErrors: state.testBuilder.validation_errors,
    lastLogin: state.user.lastLogin,
    currentHomePage: state.userPermissions.currentHomePage,
    scoringMethodValidState: state.testBuilder.scoring_method_valid_state,
    selectedSideNavItem: state.testBuilder.selectedSideNavItem,
    newTestDefinitionUploaded: state.testBuilder.newTestDefinitionUploaded,
    testBuilderTopTab: state.testBuilder.testBuilderTopTab,
    isTd: state.userPermissions.isTd
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      isTokenStillValid,
      resetTestFactoryState,
      resetNotepadState,
      resetQuestionListState,
      resetTopTabsState,
      setTestSectionToView,
      switchSideTab,
      switchTopTab,
      setSelectedTestDefinitionId,
      resetTestBuilderState,
      getTestDefinitionExtract,
      loadTestDefinition,
      setTestDefinitionValidationErrors,
      setQuestionRulesValidStateWhenItemExposureEnabled,
      setQuestionRulesValidState,
      setTestBuilderSideNavState,
      setNewTestDefinitionUploadedState,
      setTestBuilderTopTab,
      triggerAppRerender
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(TestBuilder);
