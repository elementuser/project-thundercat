import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Row } from "react-bootstrap";
import { makeLabel, makeTextBoxField, makeDropDownField, allValid } from "./helpers";
import {
  modifyTestDefinitionField,
  replaceTestDefinitionField,
  PAGE_SECTIONS
} from "../../modules/TestBuilderRedux";
import { styles } from "./TestSectionForm";
import { PageSectionType } from "../testFactory/Constants";
import MarkdownPageSectionForm from "./MarkdownPageSectionForm";
import SampleEmailPageSectionForm from "./SampleEmailPageSectionForm";
import SampleEmailResponsePageSectionForm from "./SampleEmailResponsePageSectionForm";
import SampleTaskResponsePageSectionForm from "./SampleTaskResponsePageSectionForm";
import ZoomImagePageSectionForm from "./ZoomImagePageSectionForm";
import TreeDescriptionPageSectionForm from "./TreeDescriptionPageSectionForm";

class ComponentPageSectionForm extends Component {
  state = {
    componentPageSection: {}
  };

  modifyField = (inputName, value) => {
    const newObj = { ...this.props.componentPageSection, ...this.state.componentPageSection };
    newObj[`${inputName}`] = value;
    this.setState({ componentPageSection: newObj });
  };

  onSelectChange = (event, action) => {
    this.modifyField(action.name, event.value);
  };

  handleSave = pageSectionType => {
    if (allValid(this.state)) {
      const page = {
        ...this.props.componentPageSection,
        ...this.state.componentPageSection,
        page_section_type: pageSectionType
      };
      this.props.modifyTestDefinitionField(
        PAGE_SECTIONS,
        page,
        this.props.componentPageSection.id,
        "section_component_page"
      );
      this.props.expandItem();
    }
  };

  selectedOption = componentPageSection => {
    for (const property of Object.getOwnPropertyNames(PageSectionType)) {
      if (componentPageSection.page_section_type === PageSectionType[`${property}`]) {
        return { label: property, value: componentPageSection.page_section_type };
      }
    }
    return undefined;
  };

  confirmDelete = () => {
    this.props.handleDelete(this.props.componentPageSection.id);
    this.props.expandItem();
  };

  render() {
    const componentPageSection = {
      ...this.props.componentPageSection,
      ...this.state.componentPageSection
    };

    const options = Object.getOwnPropertyNames(PageSectionType).map(property => {
      return { label: property, value: PageSectionType[property] };
    });
    const selectedOption = this.selectedOption(componentPageSection);

    return (
      <div style={styles.mainContainer}>
        <Row style={styles.rowStyle}>
          {makeLabel("order", LOCALIZE.testBuilder.componentPageSections)}
          {makeTextBoxField(
            "order",
            LOCALIZE.testBuilder.componentPageSections,
            componentPageSection.order,
            true,
            () => {},
            "component-page-section-order",
            true
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("page_section_type", LOCALIZE.testBuilder.componentPageSections)}
          {makeDropDownField(
            "page_section_type",
            LOCALIZE.testBuilder.componentPageSections,
            selectedOption,
            options,
            this.onSelectChange
          )}
        </Row>
        {selectedOption.value === PageSectionType.MARKDOWN && (
          <MarkdownPageSectionForm
            enPageDefinition={this.props.enPageDefinition}
            frPageDefinition={this.props.frPageDefinition}
            handleSave={this.handleSave}
            handleDelete={this.confirmDelete}
            testLanguage={this.props.testLanguage}
          />
        )}
        {selectedOption.value === PageSectionType.SAMPLE_EMAIL && (
          <SampleEmailPageSectionForm
            enPageDefinition={this.props.enPageDefinition}
            frPageDefinition={this.props.frPageDefinition}
            handleSave={this.handleSave}
            handleDelete={this.confirmDelete}
            testLanguage={this.props.testLanguage}
          />
        )}
        {selectedOption.value === PageSectionType.SAMPLE_EMAIL_RESPONSE && (
          <SampleEmailResponsePageSectionForm
            enPageDefinition={this.props.enPageDefinition}
            frPageDefinition={this.props.frPageDefinition}
            handleSave={this.handleSave}
            handleDelete={this.confirmDelete}
            testLanguage={this.props.testLanguage}
          />
        )}
        {selectedOption.value === PageSectionType.SAMPLE_TASK_RESPONSE && (
          <SampleTaskResponsePageSectionForm
            enPageDefinition={this.props.enPageDefinition}
            frPageDefinition={this.props.frPageDefinition}
            handleSave={this.handleSave}
            handleDelete={this.confirmDelete}
            testLanguage={this.props.testLanguage}
          />
        )}
        {selectedOption.value === PageSectionType.ZOOM_IMAGE && (
          <ZoomImagePageSectionForm
            enPageDefinition={this.props.enPageDefinition}
            frPageDefinition={this.props.frPageDefinition}
            handleSave={this.handleSave}
            handleDelete={this.confirmDelete}
            testLanguage={this.props.testLanguage}
          />
        )}
        {selectedOption.value === PageSectionType.TREE_DESCRIPTION && (
          <TreeDescriptionPageSectionForm
            enPageDefinition={this.props.enPageDefinition}
            frPageDefinition={this.props.frPageDefinition}
            options={this.props.options}
            handleSave={this.handleSave}
            handleDelete={this.confirmDelete}
            testLanguage={this.props.testLanguage}
          />
        )}
      </div>
    );
  }
}

export { ComponentPageSectionForm as unconnectedComponentPageSectionForm };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ modifyTestDefinitionField, replaceTestDefinitionField }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ComponentPageSectionForm);
