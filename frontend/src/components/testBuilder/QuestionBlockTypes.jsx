import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlusCircle } from "@fortawesome/free-solid-svg-icons";
import { Button } from "react-bootstrap";
import CollapsingItemContainer from "../eMIB/CollapsingItemContainer";
import { getNextInNumberSeries } from "./helpers";
import {
  replaceTestDefinitionField,
  addTestDefinitionField,
  QUESTION_BLOCK_TYPES
} from "../../modules/TestBuilderRedux";
import cascadeDeleteQuestion from "../../modules/TestBuilderReduxUtility";
import QuestionBlockTypeForm from "./QuestionBlockTypeForm";

export const styles = {
  mainContainer: {
    width: "100%"
  },
  label: {
    padding: "3px 0 0 4px"
  },
  borderBox: {
    borderBottom: "1px solid",
    borderRadius: "5px",
    padding: "5px",
    borderColor: "#CECECE",
    color: "red"
  },
  rowStyle: {
    margin: "5px"
  }
};

class QuestionBlockTypes extends Component {
  state = {};

  addQuestionBlockType = () => {
    // //add a new inbox question
    const newQuestionBlockType = {
      name: "Temp"
    };
    newQuestionBlockType.id = getNextInNumberSeries(this.props.questionBlockTypes, "id");
    this.props.addTestDefinitionField(
      QUESTION_BLOCK_TYPES,
      newQuestionBlockType,
      newQuestionBlockType.id
    );
  };

  makeHeader = () => {
    return (
      <>
        <h2>{LOCALIZE.testBuilder.questionBlockTypes.topTitle}</h2>
        <p>{LOCALIZE.testBuilder.questionBlockTypes.description}</p>
        <Button variant={"primary"} onClick={this.addQuestionBlockType}>
          <FontAwesomeIcon icon={faPlusCircle} style={{ margins: "auto", marginRight: "5px" }} />
          {LOCALIZE.testBuilder.questionBlockTypes.addButton}
        </Button>
        <div style={styles.borderBox} />
      </>
    );
  };

  handleDelete = id => {
    const objArray = this.props.questionBlockTypes.filter(obj => obj.id !== id);
    this.props.replaceTestDefinitionField(QUESTION_BLOCK_TYPES, objArray);
  };

  render() {
    const { questionBlockTypes } = this.props;
    return (
      <div style={styles.mainContainer}>
        {this.makeHeader()}

        {questionBlockTypes.map((questionBlockType, index) => {
          return (
            <CollapsingItemContainer
              key={index}
              index={index}
              // iconType={}
              title={<label>{questionBlockType.name}</label>}
              body={
                <QuestionBlockTypeForm
                  questionBlockType={questionBlockType}
                  handleDelete={this.handleDelete}
                />
              }
            />
          );
        })}
      </div>
    );
  }
}

export { QuestionBlockTypes as unconnectedQuestionBlockTypes };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    questionBlockTypes: state.testBuilder.question_block_types
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    { replaceTestDefinitionField, cascadeDeleteQuestion, addTestDefinitionField },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(QuestionBlockTypes);
