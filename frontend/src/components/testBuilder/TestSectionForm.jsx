import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Row, Col } from "react-bootstrap";
import LOCALIZE from "../../text_resources";
import {
  makeLabel,
  makeTextBoxField,
  makeDropDownField,
  isNumber,
  isNumberOrEmpty,
  allValid
} from "./helpers";
import {
  modifyTestDefinitionField,
  TEST_SECTIONS,
  setQuestionRulesValidState,
  setQuestionRulesValidStateWhenItemExposureEnabled,
  setTestDefinitionValidationErrors
} from "../../modules/TestBuilderRedux";
import { TestSectionType, TestSectionScoringTypeObject } from "../testFactory/Constants";
import NextSectionButtonForm from "./NextSectionButtonForm";
import Switch from "react-switch";
import getSwitchTransformScale from "../../helpers/switchTransformScale";
import questionListSubSectionValidation, {
  subSectionsValidation,
  sectionComponentPagesValidation,
  validateQuestionRulesData,
  validateQuestionRulesDataWithEnabledItemExposure,
  validateTestSections
} from "./validation";

const scoringOptions = Object.getOwnPropertyNames(TestSectionScoringTypeObject).map(property => {
  return TestSectionScoringTypeObject[property];
});

export const styles = {
  mainContainer: {
    width: "95%",
    margin: "auto"
  },
  input: {
    width: 270,
    height: 38,
    padding: "3px 6px 3px 6px",
    borderRadius: 4,
    display: "block"
  },
  label: {
    padding: "3px 0 0 4px"
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  popover: {
    zIndex: 9999,
    padding: "0 12px",
    margin: 0
  },
  rowStyle: {
    margin: "5px"
  },
  buttonRowStyle: {
    margin: "15px"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6
  },
  borderBox: {
    borderBottom: "1px solid",
    borderRadius: "5px",
    padding: "5px",
    borderColor: "#CECECE",
    color: "red",
    margin: "5px"
  },
  addButton: {
    margins: "auto",
    marginRight: "5px"
  },
  padding15: {
    paddingTop: 15
  },
  rightColumnContainer: {
    display: "table-cell",
    verticalAlign: "middle",
    height: 32
  }
};

class TestSectionForm extends Component {
  state = {
    isMinimumScoreValid: true,
    isDefaultTabValid: true,
    isFrTitleValid: true,
    isEnTitleValid: true,
    testSection: {},
    showDialog: false,
    isDefaultTimeValid: true,
    scoring: scoringOptions.filter(obj => obj.value === this.props.testSection.scoring_type)[0],
    // default values
    switchHeight: 25,
    switchWidth: 50,
    areSwitchStatesLoading: false
  };

  componentDidMount = () => {
    this.getUpdatedSwitchDimensions();
  };

  componentDidUpdate = prevProps => {
    if (prevProps.accommodations.fontSize !== this.props.accommodations.fontSize) {
      this.getUpdatedSwitchDimensions();
    }
  };

  modifyField = (inputName, value) => {
    const newObj = { ...this.props.testSection, ...this.state.testSection };
    newObj[`${inputName}`] = value;
    this.setState({ testSection: newObj });
  };

  onSelectChange = (event, action) => {
    this.modifyField(action.name, event.value);
  };

  onScoringChange = (event, action) => {
    this.setState({ scoring: event });
  };

  minimumScoreValidation = (name, value) => {
    if (isNumber(value)) {
      this.setState({ isMinimumScoreValid: true });
      this.modifyField(name, parseInt(value));
    }
  };

  defaultTimeValidation = (name, value) => {
    if (isNumberOrEmpty(value)) {
      this.setState({ isDefaultTimeValid: true });
      this.modifyField(name, value);
    }
  };

  defaultTabValidation = (name, value) => {
    if (isNumber(value)) {
      this.setState({ isDefaultTabValid: true });
      this.modifyField(name, value);
    }
  };

  frTitleValidation = (name, value) => {
    // allow maximum of 150 chars
    const regexExpression = /^(.{0,150})$/;
    if (regexExpression.test(value)) {
      this.setState({ isFrTitleValid: true });
      this.modifyField(name, value);
    }
  };

  enTitleValidation = (name, value) => {
    // allow maximum of 150 chars
    const regexExpression = /^(.{0,150})$/;
    if (regexExpression.test(value)) {
      this.setState({ isEnTitleValid: true });
      this.modifyField(name, value);
    }
  };

  handleSave = buttonType => {
    if (allValid(this.state)) {
      const newTestSection = {
        ...this.props.testSection,
        ...this.state.testSection,
        next_section_button_type: buttonType,
        scoring_type: this.state.scoring.value
      };
      this.props.modifyTestDefinitionField(
        TEST_SECTIONS,
        newTestSection,
        this.props.testSection.id
      );
      this.props.expandItem();
      // call question rules validation
      const areQuestionsRulesValid = validateQuestionRulesData(
        this.props.questions,
        this.props.questionListRules
      );
      // setting redux question rules validity state
      this.props.setQuestionRulesValidState(areQuestionsRulesValid);

      // if test section has item_exposure enabled
      // initializing areQuestionsRulesValidWhenItemExposureEnabled
      let areQuestionsRulesValidWhenItemExposureEnabled = { is_valid: true, error_type: null };
      if (newTestSection.item_exposure) {
        areQuestionsRulesValidWhenItemExposureEnabled =
          validateQuestionRulesDataWithEnabledItemExposure(
            this.props.questions,
            this.props.questionListRules
          );
        // setting question rules redux states
        this.props.setQuestionRulesValidStateWhenItemExposureEnabled(
          areQuestionsRulesValidWhenItemExposureEnabled.is_valid,
          areQuestionsRulesValidWhenItemExposureEnabled.error_type
        );
      }

      // creating a copy of validationErrors and updating testDefinitionContainsValidQuestionRules attribute
      let copyOfValidationErrors = this.props.validationErrors;
      copyOfValidationErrors.testDefinitionContainsValidQuestionRules =
        areQuestionsRulesValid && areQuestionsRulesValidWhenItemExposureEnabled.is_valid;

      // if invalid question rules
      if (!copyOfValidationErrors.testDefinitionContainsValidQuestionRules) {
        // setting testDefinitionQuestionRulesErrorType
        copyOfValidationErrors.testDefinitionQuestionRulesErrorType =
          areQuestionsRulesValidWhenItemExposureEnabled.error_type;
      }

      // getting updated testSections
      const updatedTestSections = this.props.testSections;

      // getting index of current test section
      const indexOfTestSection = updatedTestSections.findIndex(obj => obj.id === newTestSection.id);
      updatedTestSections[indexOfTestSection] = newTestSection;

      // calling test sections validation
      copyOfValidationErrors = validateTestSections(copyOfValidationErrors, updatedTestSections);

      // calling question list sub-section validation
      copyOfValidationErrors = questionListSubSectionValidation(
        copyOfValidationErrors,
        this.props.testSectionComponents,
        this.props.questions
      );
      // calling sub-sections validation
      copyOfValidationErrors = subSectionsValidation(
        copyOfValidationErrors,
        updatedTestSections,
        this.props.testSectionComponents
      );
      // calling section component pages (page content titles) validation
      copyOfValidationErrors = sectionComponentPagesValidation(
        copyOfValidationErrors,
        this.props.testSections,
        this.props.testSectionComponents,
        this.props.sectionComponentPages
      );

      // setting redux test definition validation errors
      this.props.setTestDefinitionValidationErrors(copyOfValidationErrors);
    }
  };

  openDialog = () => {
    this.setState({ showDialog: true });
  };

  closeDialog = () => {
    this.setState({ showDialog: false });
  };

  selectedOption = testSection => {
    for (const property of Object.getOwnPropertyNames(TestSectionType)) {
      if (testSection.section_type === TestSectionType[`${property}`]) {
        return { label: property, value: testSection.section_type };
      }
    }
    return undefined;
  };

  confirmDelete = () => {
    this.props.handleDelete(this.props.testSection.id);
    this.props.expandItem();
  };

  // getting switch dimensions and updating states
  getUpdatedSwitchDimensions = () => {
    // need this loading state, otherwise the switch diameter is not calculated on state change
    this.setState({ areSwitchStatesLoading: true }, () => {
      this.setState(
        {
          switchHeight: getSwitchTransformScale(this.props.accommodations.fontSize).height,
          switchWidth: getSwitchTransformScale(this.props.accommodations.fontSize).width
        },
        () => {
          this.setState({ areSwitchStatesLoading: false });
        }
      );
    });
  };

  handleEnableNotepad = () => {
    this.modifyField("uses_notepad", true);
  };

  handleDisableNotepad = () => {
    this.modifyField("uses_notepad", false);
  };

  handleEnableCalculator = () => {
    this.modifyField("uses_calculator", true);
  };

  handleDisableCalculator = () => {
    this.modifyField("uses_calculator", false);
  };

  handleEnableBlockCheating = () => {
    this.modifyField("block_cheating", true);
  };

  handleDisableBlockCheating = () => {
    this.modifyField("block_cheating", false);
  };

  handleEnableCgt = () => {
    this.modifyField("item_exposure", true);
  };

  handleDisableCgt = () => {
    this.modifyField("item_exposure", false);
  };

  render() {
    const testSection = { ...this.props.testSection, ...this.state.testSection };
    const selectedType = this.selectedOption(testSection);

    const options = Object.getOwnPropertyNames(TestSectionType).map(property => {
      return { label: property, value: TestSectionType[property] };
    });

    return (
      <div style={styles.mainContainer}>
        <Row style={styles.rowStyle}>
          {makeLabel("en_title", LOCALIZE.testBuilder.testSection)}
          {makeTextBoxField(
            "en_title",
            LOCALIZE.testBuilder.testSection,
            testSection.en_title,
            this.state.isEnTitleValid,
            this.enTitleValidation
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("fr_title", LOCALIZE.testBuilder.testSection)}
          {makeTextBoxField(
            "fr_title",
            LOCALIZE.testBuilder.testSection,
            testSection.fr_title,
            this.state.isFrTitleValid,
            this.frTitleValidation
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("default_time", LOCALIZE.testBuilder.testSection)}
          {makeTextBoxField(
            "default_time",
            LOCALIZE.testBuilder.testSection,
            testSection.default_time,
            this.state.isDefaultTimeValid,
            this.defaultTimeValidation
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("uses_notepad", LOCALIZE.testBuilder.testSection, "notepad-label")}
          <Col>
            <div style={styles.rightColumnContainer}>
              {!this.state.areSwitchStatesLoading && (
                <Switch
                  onChange={
                    testSection.uses_notepad ? this.handleDisableNotepad : this.handleEnableNotepad
                  }
                  checked={testSection.uses_notepad}
                  aria-labelledby="notepad-label-tooltip-make-label"
                  height={this.state.switchHeight}
                  width={this.state.switchWidth}
                  className="switch-custom-style"
                />
              )}
            </div>
          </Col>
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("uses_calculator", LOCALIZE.testBuilder.testSection, "calculator-label")}
          <Col>
            <div style={styles.rightColumnContainer}>
              {!this.state.areSwitchStatesLoading && (
                <Switch
                  onChange={
                    testSection.uses_calculator
                      ? this.handleDisableCalculator
                      : this.handleEnableCalculator
                  }
                  checked={testSection.uses_calculator}
                  aria-labelledby="calculator-label-tooltip-make-label"
                  height={this.state.switchHeight}
                  width={this.state.switchWidth}
                  className="switch-custom-style"
                />
              )}
            </div>
          </Col>
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("block_cheating", LOCALIZE.testBuilder.testSection, "block-cheating-label")}
          <Col>
            <div style={styles.rightColumnContainer}>
              {!this.state.areSwitchStatesLoading && (
                <Switch
                  onChange={
                    testSection.block_cheating
                      ? this.handleDisableBlockCheating
                      : this.handleEnableBlockCheating
                  }
                  checked={testSection.block_cheating}
                  aria-labelledby="block-cheating-label-tooltip-make-label"
                  height={this.state.switchHeight}
                  width={this.state.switchWidth}
                  className="switch-custom-style"
                />
              )}
            </div>
          </Col>
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("item_exposure", LOCALIZE.testBuilder.testSection, "item-exposure-label")}
          <Col>
            <div style={styles.rightColumnContainer}>
              {!this.state.areSwitchStatesLoading && (
                <Switch
                  onChange={
                    testSection.item_exposure ? this.handleDisableCgt : this.handleEnableCgt
                  }
                  checked={testSection.item_exposure}
                  aria-labelledby="item-exposure-label-tooltip-make-label"
                  height={this.state.switchHeight}
                  width={this.state.switchWidth}
                  className="switch-custom-style"
                />
              )}
            </div>
          </Col>
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("scoring_type", LOCALIZE.testBuilder.testSection)}
          {makeDropDownField(
            "scoring_type",
            LOCALIZE.testBuilder.testSection,
            this.state.scoring,
            scoringOptions,
            this.onScoringChange
          )}
        </Row>
        {this.state.scoring.value !== 0 && (
          <Row style={styles.rowStyle}>
            {makeLabel("minimum_score", LOCALIZE.testBuilder.testSection)}
            {makeTextBoxField(
              "minimum_score",
              LOCALIZE.testBuilder.testSection,
              testSection.minimum_score,
              this.state.isMinimumScoreValid,
              this.minimumScoreValidation
            )}
          </Row>
        )}
        <Row style={styles.rowStyle}>
          {makeLabel("order", LOCALIZE.testBuilder.testSection)}
          {makeTextBoxField(
            "order",
            LOCALIZE.testBuilder.testSection,
            testSection.order,
            true,
            () => {},
            "test-section-order",
            true
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("section_type", LOCALIZE.testBuilder.testSection)}
          {makeDropDownField(
            "section_type",
            LOCALIZE.testBuilder.testSection,
            selectedType,
            options,
            this.onSelectChange
          )}
        </Row>
        {testSection.section_type === TestSectionType.TOP_TABS && (
          <Row style={styles.rowStyle}>
            {makeLabel("default_tab", LOCALIZE.testBuilder.testSection)}
            {makeTextBoxField(
              "default_tab",
              LOCALIZE.testBuilder.testSection,
              String(testSection.default_tab),
              this.state.isDefaultTabValid,
              this.defaultTabValidation
            )}
          </Row>
        )}
        <NextSectionButtonForm
          nextSectionButtons={this.props.nextSectionButtons}
          nextSectionButtonType={testSection.next_section_button_type}
          handleSave={this.handleSave}
          handleDelete={this.confirmDelete}
          testSectionType={testSection.section_type}
          isTestSectionValid={this.allValid}
          enButtonDetails={this.props.enButtonDetails}
          frButtonDetails={this.props.frButtonDetails}
          deletePopupContent={LOCALIZE.testBuilder.testSection.deletePopup.content}
        />
      </div>
    );
  }
}

export { TestSectionForm as unconnectedTestSectionForm };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    testSection: state.testBuilder.test_sections[ownProps.INDEX],
    testSections: state.testBuilder.test_sections,
    testSectionComponents: state.testBuilder.test_section_components,
    accommodations: state.accommodations,
    validationErrors: state.testBuilder.validation_errors,
    questionListRules: state.testBuilder.question_list_rules,
    questions: state.testBuilder.questions,
    sectionComponentPages: state.testBuilder.section_component_pages
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      modifyTestDefinitionField,
      setQuestionRulesValidState,
      setQuestionRulesValidStateWhenItemExposureEnabled,
      setTestDefinitionValidationErrors
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(TestSectionForm);
