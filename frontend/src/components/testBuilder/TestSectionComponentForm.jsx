import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Row, Col, Button } from "react-bootstrap";
import {
  makeLabel,
  makeTextBoxField,
  makeDropDownField,
  makeSaveDeleteButtons,
  allValid,
  getNextInNumberSeries,
  styles as helperStyles,
  languageOptions,
  collapsingItemMoveUpFunctionality,
  collapsingItemMoveDownFunctionality,
  deleteQuestionData,
  deleteQuestionRulesData
} from "./helpers";
import {
  modifyTestDefinitionField,
  QUESTION_LIST_RULES,
  addTestDefinitionField,
  triggerTestDefinitionFieldUpdates,
  replaceTestDefinitionField,
  setQuestionRulesValidState,
  setQuestionRulesValidStateWhenItemExposureEnabled,
  setTestDefinitionValidationErrors,
  ITEM_BANK_RULES
} from "../../modules/TestBuilderRedux";
import { TestSectionComponentType, TestSectionType } from "../testFactory/Constants";
import { styles } from "./TestSectionForm";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import CollapsingItemContainer from "../eMIB/CollapsingItemContainer";
import QuestionListRuleForm from "./QuestionListRuleForm";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlusCircle } from "@fortawesome/free-solid-svg-icons";
import { faQuestionCircle } from "@fortawesome/free-regular-svg-icons";
import StyledTooltip, { TYPE, EFFECT, TRIGGER_TYPE } from "../authentication/StyledTooltip";
import { LANGUAGES_SHORT } from "../../modules/LocalizeRedux";
import getSwitchTransformScale from "../../helpers/switchTransformScale";
import Switch from "react-switch";
import questionListSubSectionValidation, {
  subSectionsValidation,
  sectionComponentPagesValidation,
  validateQuestionRulesData,
  validateQuestionRulesDataWithEnabledItemExposure
} from "./validation";
import ItemBankRuleForm from "./ItemBankRuleForm";

class TestSectionComponentForm extends Component {
  state = {
    showDialog: false,
    isEnTitleValid: true,
    isFrTitleValid: true,
    testSectionComponent: {},
    isValidTestSectionComponent: true,
    // default values
    switchHeight: 25,
    switchWidth: 50,
    areSwitchStatesLoading: false
  };

  componentDidMount = () => {
    this.getUpdatedSwitchDimensions();
  };

  componentDidUpdate = prevProps => {
    if (prevProps.accommodations.fontSize !== this.props.accommodations.fontSize) {
      this.getUpdatedSwitchDimensions();
    }
    // if length of questionListRules gets updated
    if (prevProps.questionListRules.length !== this.props.questionListRules.length) {
      // trigger test definition field updates
      this.props.triggerTestDefinitionFieldUpdates();
    }
  };

  modifyField = (inputName, value) => {
    const newObj = { ...this.props.testSectionComponent, ...this.state.testSectionComponent };
    newObj[`${inputName}`] = value;
    this.setState({ testSectionComponent: newObj });
  };

  frTitleValidation = (name, value) => {
    // allow maximum of 150 chars
    const regexExpression = /^(.{0,150})$/;
    if (regexExpression.test(value)) {
      this.setState({ isFrTitleValid: true });
      this.modifyField(name, value);
    }
  };

  enTitleValidation = (name, value) => {
    // allow maximum of 150 chars
    const regexExpression = /^(.{0,150})$/;
    if (regexExpression.test(value)) {
      this.setState({ isEnTitleValid: true });
      this.modifyField(name, value);
    }
  };

  onSelectChange = (event, action) => {
    this.modifyField(action.name, event.value);
  };

  handleSave = () => {
    const currentTestSectionComponent = {
      ...this.props.testSectionComponent,
      ...this.state.testSectionComponent
    };

    const areQuestionsRulesValid = validateQuestionRulesData(
      this.props.questions,
      this.props.allQuestionListRules
    );
    // if test section has item_exposure enabled
    const currentTestSection = this.props.testSections.filter(
      obj => obj.id === this.props.testSectionComponent.test_section[0]
    )[0];
    // initializing areQuestionsRulesValidWhenItemExposureEnabled
    let areQuestionsRulesValidWhenItemExposureEnabled = { is_valid: true, error_type: null };
    if (currentTestSection.item_exposure) {
      areQuestionsRulesValidWhenItemExposureEnabled =
        validateQuestionRulesDataWithEnabledItemExposure(
          this.props.questions,
          this.props.questionListRules
        );
      // setting question rules redux states
      this.props.setQuestionRulesValidStateWhenItemExposureEnabled(
        areQuestionsRulesValidWhenItemExposureEnabled.is_valid,
        areQuestionsRulesValidWhenItemExposureEnabled.error_type
      );
    }
    // setting redux question rules validity state
    this.props.setQuestionRulesValidState(areQuestionsRulesValid);

    // creating a copy of validationErrors and updating testDefinitionContainsValidQuestionRules attribute
    let copyOfValidationErrors = this.props.validationErrors;
    copyOfValidationErrors.testDefinitionContainsValidQuestionRules =
      areQuestionsRulesValid && areQuestionsRulesValidWhenItemExposureEnabled.is_valid;

    // if invalid question rules
    if (!copyOfValidationErrors.testDefinitionContainsValidQuestionRules) {
      // setting testDefinitionQuestionRulesErrorType
      copyOfValidationErrors.testDefinitionQuestionRulesErrorType =
        areQuestionsRulesValidWhenItemExposureEnabled.error_type;
    }

    // getting updated testSectionComponents
    const updatedTestSectionComponents = this.props.testSectionComponents;

    // getting index of current test section component
    const indexOfTestSectionComponent = updatedTestSectionComponents.findIndex(
      obj => obj.id === currentTestSectionComponent.id
    );
    updatedTestSectionComponents[indexOfTestSectionComponent] = currentTestSectionComponent;

    // calling question list sub-section validation
    copyOfValidationErrors = questionListSubSectionValidation(
      copyOfValidationErrors,
      this.props.testSectionComponents,
      this.props.questions
    );

    // test section component validation
    // initializing isValidTestSectionComponent
    let isValidTestSectionComponent = true;

    // getting related test section type
    const sectionType = this.props.testSections.filter(
      obj => obj.id === currentTestSectionComponent.test_section[0]
    )[0].section_type;

    // if section type is FINISH (3) or QUIT (4)
    if (sectionType === TestSectionType.FINISH || sectionType === TestSectionType.QUIT) {
      isValidTestSectionComponent =
        currentTestSectionComponent.component_type === TestSectionComponentType.SINGLE_PAGE;
    }

    // calling sub-sections validation
    copyOfValidationErrors = subSectionsValidation(
      copyOfValidationErrors,
      this.props.testSections,
      updatedTestSectionComponents
    );

    // calling section component pages (page content titles) validation
    copyOfValidationErrors = sectionComponentPagesValidation(
      copyOfValidationErrors,
      this.props.testSections,
      updatedTestSectionComponents,
      this.props.sectionComponentPages
    );

    // setting redux test definition validation errors
    this.props.setTestDefinitionValidationErrors(copyOfValidationErrors);

    // updating isValidTestSectionComponent
    this.setState({ isValidTestSectionComponent: isValidTestSectionComponent }, () => {
      if (allValid(this.state) && areQuestionsRulesValid) {
        // if component_type of current test section component is not QUESTION_LIST
        if (currentTestSectionComponent.component_type !== TestSectionComponentType.QUESTION_LIST) {
          // deleting question data related to current test section component
          deleteQuestionData(
            this.props.replaceTestDefinitionField,
            [currentTestSectionComponent.id],
            this.props.questions,
            this.props.questionSections,
            this.props.questionSectionDefinitions,
            this.props.answers,
            this.props.answerDetails,
            this.props.multipleChoiceQuestionDetails
          );
          // deleting question rules data related to current test section component
          deleteQuestionRulesData(
            this.props.replaceTestDefinitionField,
            [currentTestSectionComponent.id],
            this.props.allQuestionListRules
          );
        }
        this.props.modifyTestDefinitionField(
          "test_section_components",
          currentTestSectionComponent,
          this.props.testSectionComponent.id
        );
        this.props.expandItem();
      }
      // trigger test definition field updates
      this.props.triggerTestDefinitionFieldUpdates();
    });
  };

  addQuestionListRule = () => {
    const newRule = {
      number_of_questions: 0,
      question_block_type: [],
      test_section_component: this.props.testSectionComponent.id,
      shuffle: false
    };
    newRule.id = getNextInNumberSeries(this.props.allQuestionListRules, "id");
    this.props.addTestDefinitionField(QUESTION_LIST_RULES, newRule, newRule.id);
    // trigger test definition field updates
    this.props.triggerTestDefinitionFieldUpdates();
  };

  addItemBankRule = () => {
    const newRule = {
      test_section_component: this.props.testSectionComponent.id,
      order: this.props.itemBankRules.length + 1,
      item_bank: {},
      item_bank_bundle: {}
    };
    newRule.id = getNextInNumberSeries(this.props.itemBankRules, "id");
    this.props.addTestDefinitionField(ITEM_BANK_RULES, newRule, newRule.id);
    // trigger test definition field updates
    this.props.triggerTestDefinitionFieldUpdates();
  };

  openDialog = () => {
    this.setState({ showDialog: true });
  };

  closeDialog = () => {
    this.setState({ showDialog: false });
  };

  selectedOption = testSectionComponent => {
    for (const property of Object.getOwnPropertyNames(TestSectionComponentType)) {
      if (testSectionComponent.component_type === TestSectionComponentType[`${property}`]) {
        return { label: property, value: testSectionComponent.component_type };
      }
    }
    return undefined;
  };

  selectedLanguage = language => {
    if (language === LANGUAGES_SHORT.en) {
      return { label: LOCALIZE.commons.english, value: LANGUAGES_SHORT.en };
    }
    if (language === LANGUAGES_SHORT.fr) {
      return { label: LOCALIZE.commons.french, value: LANGUAGES_SHORT.fr };
    }
    return { label: LOCALIZE.commons.bilingual, value: undefined };
  };

  confirmDelete = () => {
    this.props.handleDelete(this.props.testSectionComponent.id);
  };

  // getting switch dimensions and updating states
  getUpdatedSwitchDimensions = () => {
    // need this loading state, otherwise the switch diameter is not calculated on state change
    this.setState({ areSwitchStatesLoading: true }, () => {
      this.setState(
        {
          switchHeight: getSwitchTransformScale(this.props.accommodations.fontSize).height,
          switchWidth: getSwitchTransformScale(this.props.accommodations.fontSize).width
        },
        () => {
          this.setState({ areSwitchStatesLoading: false });
        }
      );
    });
  };

  handleEnableShuffleQuestions = () => {
    this.modifyField("shuffle_all_questions", true);
    // disabling shuffle_question_blocks
    this.modifyField("shuffle_question_blocks", false);
  };

  handleDisableShuffleQuestions = () => {
    this.modifyField("shuffle_all_questions", false);
  };

  handleEnableShuffleQuestionBlocks = () => {
    this.modifyField("shuffle_question_blocks", true);
    // disabling shuffle_all_questions
    this.modifyField("shuffle_all_questions", false);
  };

  handleDisableShuffleQuestionBlocks = () => {
    this.modifyField("shuffle_question_blocks", false);
  };

  handleMoveUp = (index, source) => {
    // getting all related ordered rules data
    const allRelatedOrderedRulesdData =
      source === TestSectionComponentType.QUESTION_LIST
        ? this.getAllQuestionListRulesOrderedData()
        : this.getAllItemBankRulesOrderedData();
    // getting new question list rules array
    collapsingItemMoveUpFunctionality(index, allRelatedOrderedRulesdData.currentRules)
      .then(objArray => {
        // creating final array
        const final_array = objArray.concat(allRelatedOrderedRulesdData.otherRules);
        // updating redux states
        this.props.replaceTestDefinitionField(
          source === TestSectionComponentType.QUESTION_LIST ? QUESTION_LIST_RULES : ITEM_BANK_RULES,
          final_array
        );
      })
      .then(() => {
        // getting orders
        this.props.triggerTestDefinitionFieldUpdates();
      });
  };

  handleMoveDown = (index, source) => {
    // getting all related ordered rules data
    const allRelatedOrderedRulesdData =
      source === TestSectionComponentType.QUESTION_LIST
        ? this.getAllQuestionListRulesOrderedData()
        : this.getAllItemBankRulesOrderedData();
    // getting new question list rules array
    collapsingItemMoveDownFunctionality(index, allRelatedOrderedRulesdData.currentRules)
      .then(objArray => {
        // creating final array
        const final_array = objArray.concat(allRelatedOrderedRulesdData.otherRules);
        // updating redux states
        this.props.replaceTestDefinitionField(
          source === TestSectionComponentType.QUESTION_LIST ? QUESTION_LIST_RULES : ITEM_BANK_RULES,
          final_array
        );
      })
      .then(() => {
        // getting orders
        this.props.triggerTestDefinitionFieldUpdates();
      });
  };

  getAllQuestionListRulesOrderedData = () => {
    // initializing needed arrays
    const currentQuestionListRules = [];
    const otherQuestionListRules = [];
    // looping in all question list rules
    for (let i = 0; i < this.props.allQuestionListRules.length; i++) {
      // pushing current test section component question list rules to currentQuestionListRules array
      if (
        this.props.testSectionComponent.id ===
        this.props.allQuestionListRules[i].test_section_component
      ) {
        currentQuestionListRules.push(this.props.allQuestionListRules[i]);
        // pushing other test section component question list rules to otherQuestionListRules array
      } else {
        otherQuestionListRules.push(this.props.allQuestionListRules[i]);
      }
    }
    return {
      currentRules: currentQuestionListRules,
      otherRules: otherQuestionListRules
    };
  };

  getAllItemBankRulesOrderedData = () => {
    // initializing needed arrays
    const currentItemBankRules = [];
    const otherItemBankRules = [];
    // looping in all question list rules
    for (let i = 0; i < this.props.itemBankRules.length; i++) {
      // pushing current test section component question list rules to currentItemBankRules array
      if (
        this.props.testSectionComponent.id === this.props.itemBankRules[i].test_section_component
      ) {
        currentItemBankRules.push(this.props.itemBankRules[i]);
        // pushing other test section component question list rules to otherItemBankRules array
      } else {
        otherItemBankRules.push(this.props.itemBankRules[i]);
      }
    }
    return {
      currentRules: currentItemBankRules,
      otherRules: otherItemBankRules
    };
  };

  getComponentTypeOptions = testSectionComponent => {
    // initializing needed variables
    let options = [];

    // getting related test section type
    const sectionType = this.props.testSections.filter(
      obj => obj.id === testSectionComponent.test_section[0]
    )[0].section_type;

    // if section type is FINISH (3) or QUIT (4)
    if (sectionType === TestSectionType.FINISH || sectionType === TestSectionType.QUIT) {
      // populating options (only SINGLE_PAGE)
      options = Object.getOwnPropertyNames(TestSectionComponentType)
        .map(property => {
          return { label: property, value: TestSectionComponentType[property] };
        })
        .filter(obj => obj.value === TestSectionComponentType.SINGLE_PAGE);
      // if section type is TOP_TABS (1) or SINGLE_COMPONENT (2)
    } else {
      // populating options (all available options)
      options = Object.getOwnPropertyNames(TestSectionComponentType).map(property => {
        return { label: property, value: TestSectionComponentType[property] };
      });
    }
    return options;
  };

  getDisabledSaveButtonState = () => {
    // initialized saveButtonDisabledState
    let saveButtonDisabledState = false;

    // if no rules are defined
    if (this.props.itemBankRules.length <= 0) {
      // disabled button
      saveButtonDisabledState = true;
    }

    // looping in itemBankRules
    for (let i = 0; i < this.props.itemBankRules.length; i++) {
      // item bank selection or item bank bundle selection object is empty
      if (
        Object.entries(this.props.itemBankRules[i].item_bank).length <= 0 ||
        Object.entries(this.props.itemBankRules[i].item_bank_bundle).length <= 0
      ) {
        // disabled button
        saveButtonDisabledState = true;
        break;
      }
    }

    // returning saveButtonDisabledState
    return saveButtonDisabledState;
  };

  render() {
    const testSectionComponent = {
      ...this.props.testSectionComponent,
      ...this.state.testSectionComponent
    };

    const options = this.getComponentTypeOptions(testSectionComponent);
    const selectedOption = this.selectedOption(testSectionComponent);
    const selectedLanguage = this.selectedLanguage(testSectionComponent.language);

    // only question lists can have rules-> question blocks
    const canHaveRules =
      testSectionComponent.component_type === TestSectionComponentType.QUESTION_LIST ||
      testSectionComponent.component_type === TestSectionComponentType.INBOX ||
      testSectionComponent.component_type === TestSectionComponentType.ITEM_BANK;
    const { questionListRules, itemBankRules } = this.props;

    // Disable the Add Rule button
    // - If there are no question block types
    // - AND if it is NOT a component type equals to Item Bank
    const disabledAddButton =
      this.props.questionBlockTypes.length < 1 &&
      testSectionComponent.component_type !== TestSectionComponentType.ITEM_BANK;
    return (
      <div style={styles.mainContainer}>
        <Row style={styles.rowStyle}>
          {makeLabel("en_title", LOCALIZE.testBuilder.testSectionComponents)}
          {makeTextBoxField(
            "en_title",
            LOCALIZE.testBuilder.testSectionComponents,
            testSectionComponent.en_title,
            this.state.isEnTitleValid,
            this.enTitleValidation
          )}
        </Row>

        <Row style={styles.rowStyle}>
          {makeLabel("fr_title", LOCALIZE.testBuilder.testSectionComponents)}
          {makeTextBoxField(
            "fr_title",
            LOCALIZE.testBuilder.testSectionComponents,
            testSectionComponent.fr_title,
            this.state.isFrTitleValid,
            this.frTitleValidation
          )}
        </Row>

        <Row style={styles.rowStyle}>
          {makeLabel("order", LOCALIZE.testBuilder.testSectionComponents)}
          {makeTextBoxField(
            "order",
            LOCALIZE.testBuilder.testSectionComponents,
            testSectionComponent.order,
            true,
            () => {},
            "test-section-components-order",
            true
          )}
        </Row>

        <Row style={styles.rowStyle}>
          {makeLabel("component_type", LOCALIZE.testBuilder.testSectionComponents)}
          {makeDropDownField(
            "component_type",
            LOCALIZE.testBuilder.testSectionComponents,
            selectedOption,
            options,
            this.onSelectChange,
            false,
            this.state.isValidTestSectionComponent
          )}
        </Row>

        {testSectionComponent.component_type !== TestSectionComponentType.ITEM_BANK && (
          <Row style={styles.rowStyle}>
            {makeLabel("language", LOCALIZE.testBuilder.testSectionComponents)}
            {makeDropDownField(
              "language",
              LOCALIZE.testBuilder.testSectionComponents,
              selectedLanguage,
              languageOptions,
              this.onSelectChange
            )}
          </Row>
        )}

        {canHaveRules && (
          <>
            {testSectionComponent.component_type !== TestSectionComponentType.ITEM_BANK && (
              <Row style={styles.rowStyle}>
                {makeLabel(
                  "shuffle_all_questions",
                  LOCALIZE.testBuilder.testSectionComponents,
                  "shuffle-question-label"
                )}
                <Col>
                  <div style={styles.rightColumnContainer}>
                    {!this.state.areSwitchStatesLoading && (
                      <Switch
                        onChange={
                          testSectionComponent.shuffle_all_questions
                            ? this.handleDisableShuffleQuestions
                            : this.handleEnableShuffleQuestions
                        }
                        checked={testSectionComponent.shuffle_all_questions}
                        aria-labelledby="shuffle-question-label"
                        height={this.state.switchHeight}
                        width={this.state.switchWidth}
                        className="switch-custom-style"
                      />
                    )}
                  </div>
                </Col>
              </Row>
            )}
            <Row style={styles.rowStyle}>
              {makeLabel(
                testSectionComponent.component_type === TestSectionComponentType.QUESTION_LIST
                  ? "shuffle_question_blocks"
                  : "shuffle_item_bank_rules",
                LOCALIZE.testBuilder.testSectionComponents,
                testSectionComponent.component_type === TestSectionComponentType.QUESTION_LIST
                  ? "shuffle-question_blocks-label"
                  : "shuffle_item_bank_rules-label"
              )}
              <Col>
                <div style={styles.rightColumnContainer}>
                  {!this.state.areSwitchStatesLoading && (
                    <Switch
                      onChange={
                        testSectionComponent.shuffle_question_blocks
                          ? this.handleDisableShuffleQuestionBlocks
                          : this.handleEnableShuffleQuestionBlocks
                      }
                      checked={testSectionComponent.shuffle_question_blocks}
                      aria-labelledby="shuffle-question_blocks-label"
                      height={this.state.switchHeight}
                      width={this.state.switchWidth}
                      className="switch-custom-style"
                    />
                  )}
                </div>
              </Col>
            </Row>
            {testSectionComponent.component_type === TestSectionComponentType.QUESTION_LIST ? (
              <div>
                <h4>{LOCALIZE.testBuilder.questionListRules.title}</h4>
                {questionListRules.map((rule, index) => {
                  return (
                    <CollapsingItemContainer
                      key={index}
                      index={index}
                      usesMoveUpAndDownFunctionalities={true}
                      moveUpAction={() =>
                        this.handleMoveUp(index, TestSectionComponentType.QUESTION_LIST)
                      }
                      moveUpButtonDisabled={rule.order === 1}
                      moveDownAction={() =>
                        this.handleMoveDown(index, TestSectionComponentType.QUESTION_LIST)
                      }
                      moveDownButtonDisabled={
                        index === questionListRules.length - 1
                          ? true
                          : questionListRules[index + 1].order <= rule.order
                      }
                      title={
                        <label>
                          {LOCALIZE.formatString(
                            LOCALIZE.testBuilder.questionListRules.collapsableItemName,
                            index + 1
                          )}
                        </label>
                      }
                      body={
                        <QuestionListRuleForm
                          rule={rule}
                          testSectionComponent={testSectionComponent}
                        />
                      }
                    />
                  );
                })}
              </div>
            ) : (
              <div>
                <h4>{LOCALIZE.testBuilder.itemBankRules.title}</h4>
                {itemBankRules
                  .filter(obj => obj.test_section_component === testSectionComponent.id)
                  .map((rule, index) => {
                    return (
                      <CollapsingItemContainer
                        key={index}
                        index={index}
                        usesMoveUpAndDownFunctionalities={true}
                        moveUpAction={() =>
                          this.handleMoveUp(index, TestSectionComponentType.ITEM_BANK_RULES)
                        }
                        moveUpButtonDisabled={rule.order === 1}
                        moveDownAction={() =>
                          this.handleMoveDown(index, TestSectionComponentType.ITEM_BANK_RULES)
                        }
                        moveDownButtonDisabled={
                          index === itemBankRules.length - 1
                            ? true
                            : itemBankRules[index + 1].order <= rule.order
                        }
                        title={
                          <label>
                            {LOCALIZE.formatString(
                              LOCALIZE.testBuilder.itemBankRules.collapsableItemName,
                              index + 1
                            )}
                          </label>
                        }
                        body={
                          <ItemBankRuleForm
                            rule={rule}
                            testSectionComponent={testSectionComponent}
                          />
                        }
                      />
                    );
                  })}
              </div>
            )}

            <Button
              variant={"primary"}
              onClick={
                testSectionComponent.component_type === TestSectionComponentType.QUESTION_LIST
                  ? this.addQuestionListRule
                  : this.addItemBankRule
              }
              disabled={disabledAddButton}
            >
              <FontAwesomeIcon
                icon={faPlusCircle}
                style={{ margins: "auto", marginRight: "5px" }}
              />
              {LOCALIZE.testBuilder.questionListRules.addButton.title}
            </Button>
            {testSectionComponent.component_type === TestSectionComponentType.QUESTION_LIST && (
              <StyledTooltip
                id="add-rule-tooltip"
                place="right"
                type={TYPE.light}
                effect={EFFECT.solid}
                triggerType={[TRIGGER_TYPE.hover]}
                tooltipElement={
                  <Button
                    data-tip=""
                    data-for="add-rule-tooltip"
                    tabIndex="-1"
                    variant="link"
                    style={{
                      ...helperStyles.tooltipIconContainer,
                      ...helperStyles.tooltipMarginTop
                    }}
                  >
                    <FontAwesomeIcon
                      icon={faQuestionCircle}
                      style={helperStyles.secondaryEmailTooltipIcon}
                    ></FontAwesomeIcon>
                  </Button>
                }
                tooltipContent={
                  <div>
                    <p>{LOCALIZE.testBuilder.questionListRules.addButton.titleTooltip}</p>
                  </div>
                }
              />
            )}
            <label id={`add-rule-tooltip-for-accessibility`} style={helperStyles.hiddenText}>
              , {LOCALIZE.testBuilder.questionListRules.addButton.titleTooltip}
            </label>
          </>
        )}
        <Row className="justify-content-between" style={styles.buttonRowStyle}>
          {makeSaveDeleteButtons(
            this.handleSave,
            this.openDialog,
            LOCALIZE.commons.applyButton,
            LOCALIZE.commons.deleteButton,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            testSectionComponent.component_type === TestSectionComponentType.ITEM_BANK
              ? this.getDisabledSaveButtonState()
              : false
          )}
        </Row>
        <PopupBox
          show={this.state.showDialog}
          handleClose={this.closeDialog}
          title={"Delete Confirmation"}
          description={
            <div>
              <p>
                Deleting this item will delete all the content displayed and all child objects that
                are related to this object. Are you sure you want to delete this object?
              </p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={LOCALIZE.commons.deleteButton}
          rightButtonAction={this.confirmDelete}
        />
      </div>
    );
  }
}

export { TestSectionComponentForm as unconnectedTestSectionForm };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    questionBlockTypes: state.testBuilder.question_block_types,
    accommodations: state.accommodations,
    triggerTestDefinitionFieldUpdates: state.testBuilder.triggerTestDefinitionFieldUpdates,
    allQuestionListRules: state.testBuilder.question_list_rules,
    itemBankRules: state.testBuilder.item_bank_rules,
    questions: state.testBuilder.questions,
    questionSections: state.testBuilder.question_sections,
    questionSectionDefinitions: state.testBuilder.question_section_definitions,
    answers: state.testBuilder.answers,
    answerDetails: state.testBuilder.answer_details,
    multipleChoiceQuestionDetails: state.testBuilder.multiple_choice_question_details,
    validationErrors: state.testBuilder.validation_errors,
    testSections: state.testBuilder.test_sections,
    testSectionComponents: state.testBuilder.test_section_components,
    sectionComponentPages: state.testBuilder.section_component_pages
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      modifyTestDefinitionField,
      addTestDefinitionField,
      triggerTestDefinitionFieldUpdates,
      replaceTestDefinitionField,
      setQuestionRulesValidState,
      setTestDefinitionValidationErrors,
      setQuestionRulesValidStateWhenItemExposureEnabled
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(TestSectionComponentForm);
