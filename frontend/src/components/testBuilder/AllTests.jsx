import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import {
  getTestDefinitionData,
  updateSearchAllTestDefinitionsStates,
  updateCurrentTestDefinitionAllTestsPageState,
  updateCurrentTestDefinitionAllTestsPageSizeState,
  setSelectedTestDefinitionId,
  triggerTestDefinitionLoading,
  archiveTestDefinitionTestCode,
  getTestDefinitionLatestVersion,
  triggerSelectTestDefinitionTablesRefresh
} from "../../modules/TestBuilderRedux";
import { Row } from "react-bootstrap";
import GenericTable, { COMMON_STYLE } from "../commons/GenericTable";
import CustomButton, { THEME } from "../commons/CustomButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCheck,
  faTimes,
  faSearch,
  faBinoculars,
  faFileArchive,
  faPlusCircle
} from "@fortawesome/free-solid-svg-icons";
import PopupBox, { BUTTON_TYPE, BUTTON_STATE } from "../commons/PopupBox";
import { makeDropDownField, makeLabel } from "./helpers";
import SearchBarWithDisplayOptions from "../commons/SearchBarWithDisplayOptions";
import Pagination from "../commons/Pagination";
import StyledTooltip, { TYPE, EFFECT, TRIGGER_TYPE } from "../authentication/StyledTooltip";
import SystemMessage, { MESSAGE_TYPE } from "../commons/SystemMessage";

const styles = {
  tableContainer: {
    margin: 24
  },
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  popupTestInfoContainer: {
    margin: "12px 0"
  },
  popupTestInfoItem: {
    display: "table",
    width: "100%",
    margin: "0 36px"
  },
  popupTestInfoLeftColumn: {
    display: "table-cell",
    width: "30%",
    fontWeight: "bold"
  },
  popupTestInfoRightColumn: {
    display: "table-cell",
    width: "70%",
    verticalAlign: "middle"
  },
  versionNotesContentStyle: {
    width: "95%",
    whiteSpace: "pre-line"
  },
  selectTestVersionDropdownContainer: {
    margin: "18px 0"
  },
  bold: {
    fontWeight: "bold"
  },
  allUnset: {
    all: "unset"
  },
  buttonLabel: {
    margin: 0,
    paddingLeft: 6
  }
};

class AllTests extends Component {
  state = {
    currentlyLoading: false,
    rowsDefinition: {},
    selectedTestDefinition: {},
    showTestDefinitionPopup: false,
    testDefinitionVersionOptions: [],
    selectedTestDefinitionVersion: { label: "", value: "" },
    numberOfTestDefinitionAllTestsPages: 1,
    resultsFound: 0,
    showComplementarySelectedTestVersionInfo: false,
    isTestActive: false,
    isSampleTest: false,
    isUit: false,
    versionNotes: "",
    showArchiveConfirmationPopup: false,
    currentTestDefinitionData: {},
    archiveProcessLoading: false
  };

  componentDidMount = () => {
    this.getTestDefinitionData();
  };

  componentDidUpdate = prevProps => {
    // if testDefinitionAllTestsPaginationPageSize get updated
    if (
      prevProps.testDefinitionAllTestsPaginationPageSize !==
      this.props.testDefinitionAllTestsPaginationPageSize
    ) {
      this.getTestDefinitionDataBasedOnActiveSearch();
    }
    // if testDefinitionAllTestsPaginationPage get updated
    if (
      prevProps.testDefinitionAllTestsPaginationPage !==
      this.props.testDefinitionAllTestsPaginationPage
    ) {
      this.getTestDefinitionDataBasedOnActiveSearch();
    }
    // if search testDefinitionAllTestsKeyword gets updated
    if (prevProps.testDefinitionAllTestsKeyword !== this.props.testDefinitionAllTestsKeyword) {
      // if testDefinitionAllTestsKeyword redux state is empty
      if (this.props.testDefinitionAllTestsKeyword !== "") {
        this.getFoundTestDefinitionData();
      } else if (
        this.props.testDefinitionAllTestsKeyword === "" &&
        this.props.testDefinitionAllTestsActiveSearch
      ) {
        this.getFoundTestDefinitionData();
      }
    }
    // if permissionsActiveSearch gets updated
    if (
      prevProps.testDefinitionAllTestsActiveSearch !== this.props.testDefinitionAllTestsActiveSearch
    ) {
      // there is no current active search (on clear search action)
      if (!this.props.testDefinitionAllTestsActiveSearch) {
        this.getTestDefinitionData();
        // there is a current active search (on search action)
      } else {
        this.getFoundTestDefinitionData();
      }
    }
    // if triggerSelectTestDefinitionTablesRefreshState gets updated
    if (
      prevProps.triggerSelectTestDefinitionTablesRefreshState !==
      this.props.triggerSelectTestDefinitionTablesRefreshState
    ) {
      this.getTestDefinitionDataBasedOnActiveSearch();
    }
  };

  // get test definition data based on the testDefinitionAllTestsActiveSearch redux state
  getTestDefinitionDataBasedOnActiveSearch = () => {
    // current search
    if (this.props.testDefinitionAllTestsActiveSearch) {
      this.getFoundTestDefinitionData();
      // no current search
    } else {
      this.getTestDefinitionData();
    }
  };

  getTestDefinitionData = () => {
    // if selectedTestDefinitionId is null
    if (this.props.selectedTestDefinitionId === null) {
      this.setState({ currentlyLoading: true }, () => {
        // initializing needed object and array for rowsDefinition (needed for GenericTable component)
        let rowsDefinition = {};
        const data = [];
        this.props
          .getTestDefinitionData(
            true,
            null,
            null,
            null,
            this.props.testDefinitionAllTestsPaginationPage,
            this.props.testDefinitionAllTestsPaginationPageSize,
            null,
            null,
            false,
            false
          )
          .then(response => {
            for (let i = 0; i < response.body.results.length; i++) {
              // populating data object with provided data
              data.push({
                column_1: response.body.results[i].details[0].parent_code,
                column_2: response.body.results[i].details[0].test_code,
                column_3: response.body.results[i].details[0][`${this.props.currentLanguage}_name`],
                column_4: this.populateColumnFour(i, response)
              });
            }
            // updating rowsDefinition object with provided data and needed style
            rowsDefinition = {
              column_1_style: COMMON_STYLE.LEFT_TEXT,
              column_2_style: COMMON_STYLE.LEFT_TEXT,
              column_3_style: COMMON_STYLE.LEFT_TEXT,
              column_4_style: COMMON_STYLE.CENTERED_TEXT,
              data: data
            };
            // saving results in state
            this.setState({
              rowsDefinition: rowsDefinition,
              currentlyLoading: false,
              numberOfTestDefinitionAllTestsPages: Math.ceil(
                parseInt(response.body.count) / this.props.testDefinitionAllTestsPaginationPageSize
              )
            });
          });
      });
    }
  };

  getFoundTestDefinitionData = () => {
    this.setState({ currentlyLoading: true }, () => {
      // initializing needed object and array for rowsDefinition (needed for GenericTable component)
      let rowsDefinition = {};
      const data = [];
      this.props
        .getTestDefinitionData(
          true,
          null,
          null,
          null,
          this.props.testDefinitionAllTestsPaginationPage,
          this.props.testDefinitionAllTestsPaginationPageSize,
          this.props.testDefinitionAllTestsKeyword,
          this.props.currentLanguage,
          false,
          false
        )
        .then(response => {
          // there are no results found
          if (response.body[0] === "no results found") {
            this.setState(
              {
                rowsDefinition: {},
                numberOfTestDefinitionAllTestsPages: 1,
                resultsFound: 0
              },
              () => {
                this.setState({ currentlyLoading: false });
              }
            );
            // there is at least one result found
          } else {
            for (let i = 0; i < response.body.results.length; i++) {
              // populating data object with provided data
              data.push({
                column_1: response.body.results[i].details[0].parent_code,
                column_2: response.body.results[i].details[0].test_code,
                column_3: response.body.results[i].details[0][`${this.props.currentLanguage}_name`],
                column_4: this.populateColumnFour(i, response)
              });
            }
            // updating rowsDefinition object with provided data and needed style
            rowsDefinition = {
              column_1_style: COMMON_STYLE.LEFT_TEXT,
              column_2_style: COMMON_STYLE.LEFT_TEXT,
              column_3_style: COMMON_STYLE.LEFT_TEXT,
              column_4_style: COMMON_STYLE.CENTERED_TEXT,
              data: data
            };
            // saving results in state
            this.setState({
              rowsDefinition: rowsDefinition,
              currentlyLoading: false,
              numberOfTestDefinitionAllTestsPages: Math.ceil(
                parseInt(response.body.count) / this.props.testDefinitionAllTestsPaginationPageSize
              ),
              resultsFound: response.body.count
            });
          }
        });
    });
  };

  populateColumnFour = (i, response) => {
    return (
      <div className="d-flex justify-content-center flex-nowrap">
        <StyledTooltip
          id={`test-definition-all-tests-select-specific-test-version-${i}`}
          place="top"
          type={TYPE.light}
          effect={EFFECT.solid}
          triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
          tooltipElement={
            <div style={styles.allUnset}>
              <CustomButton
                buttonId={`test-definition-all-tests-select-specific-test-version-id-${i}`}
                dataTip=""
                dataFor={`test-definition-all-tests-select-specific-test-version-${i}`}
                label={<FontAwesomeIcon icon={faSearch} />}
                action={() => {
                  this.viewSelectedTestDefinition(response.body.results[i].details[0]);
                }}
                customStyle={styles.actionButton}
                buttonTheme={THEME.SECONDARY}
                ariaLabel={LOCALIZE.formatString(
                  LOCALIZE.testBuilder.testDefinition.selectTestDefinition.allTests.table
                    .selectSpecificTestVersionLabel,
                  response.body.results[i].details[0].parent_code,
                  response.body.results[i].details[0].test_code,
                  response.body.results[i].details[0][`${this.props.currentLanguage}_name`]
                )}
                disabled={response.body.results[i].action_buttons_disabled}
              />
            </div>
          }
          tooltipContent={
            <div>
              <p>
                {
                  LOCALIZE.testBuilder.testDefinition.selectTestDefinition.allTests.table
                    .selectSpecificTestVersionTooltip
                }
              </p>
            </div>
          }
        />
        <StyledTooltip
          id={`test-definition-all-tests-select-latest-test-version-${i}`}
          place="top"
          type={TYPE.light}
          effect={EFFECT.solid}
          triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
          tooltipElement={
            <div style={styles.allUnset}>
              <CustomButton
                buttonId={`test-definition-all-tests-select-latest-test-version-id-${i}`}
                dataTip=""
                dataFor={`test-definition-all-tests-select-latest-test-version-${i}`}
                label={<FontAwesomeIcon icon={faBinoculars} />}
                action={() => {
                  this.viewLatestTestDefinitionVersion(response.body.results[i].details[0]);
                }}
                customStyle={styles.actionButton}
                buttonTheme={THEME.SECONDARY}
                ariaLabel={LOCALIZE.formatString(
                  LOCALIZE.testBuilder.testDefinition.selectTestDefinition.allTests.table
                    .selectLatestTestVersionLabel,
                  response.body.results[i].details[0].parent_code,
                  response.body.results[i].details[0].test_code,
                  response.body.results[i].details[0][`${this.props.currentLanguage}_name`]
                )}
                disabled={response.body.results[i].action_buttons_disabled}
              />
            </div>
          }
          tooltipContent={
            <div>
              <p>
                {
                  LOCALIZE.testBuilder.testDefinition.selectTestDefinition.allTests.table
                    .selectLatestTestVersionTooltip
                }
              </p>
            </div>
          }
        />
        <StyledTooltip
          id={`test-definition-all-tests-archive-test-code-${i}`}
          place="top"
          type={TYPE.light}
          effect={EFFECT.solid}
          triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
          tooltipElement={
            <div style={styles.allUnset}>
              <CustomButton
                buttonId={`test-definition-all-tests-archive-test-code-id-${i}`}
                dataTip=""
                dataFor={`test-definition-all-tests-archive-test-code-${i}`}
                label={<FontAwesomeIcon icon={faFileArchive} />}
                action={() => {
                  this.archiveTestDefinitionTestCode(response.body.results[i].details[0]);
                }}
                customStyle={styles.actionButton}
                buttonTheme={THEME.SECONDARY}
                ariaLabel={LOCALIZE.formatString(
                  LOCALIZE.testBuilder.testDefinition.selectTestDefinition.allTests.table
                    .archiveTestCodeLabel,
                  response.body.results[i].details[0].parent_code,
                  response.body.results[i].details[0].test_code,
                  response.body.results[i].details[0][`${this.props.currentLanguage}_name`]
                )}
                disabled={response.body.results[i].action_buttons_disabled}
              />
            </div>
          }
          tooltipContent={
            <div>
              <p>
                {
                  LOCALIZE.testBuilder.testDefinition.selectTestDefinition.allTests.table
                    .archiveTestCodeTooltip
                }
              </p>
            </div>
          }
        />
      </div>
    );
  };

  viewSelectedTestDefinition = currentTestDefinitionData => {
    // getting test definition data (based on parent code and test code)
    this.props
      .getTestDefinitionData(
        false,
        currentTestDefinitionData.parent_code,
        currentTestDefinitionData.test_code,
        null,
        // page and page_size null here in order to get all the data, not only for a specific page (need all available versions)
        null,
        null,
        null,
        null,
        false,
        false
      )
      .then(response => {
        // initializing testDefinitionVersionOptions array
        const testDefinitionVersionOptions = [];
        // looping in test definition data
        for (let i = 0; i < response.body.results.length; i++) {
          // pushing data in testDefinitionVersionOptions array
          testDefinitionVersionOptions.push({
            label: `${response.body.results[i].details[0].version}`,
            value: `${response.body.results[i].details[0].version}`
          });
        }
        // updating needed states
        this.setState(
          {
            selectedTestDefinition: currentTestDefinitionData,
            testDefinitionVersionOptions: testDefinitionVersionOptions
          },
          () => {
            this.setState({ showTestDefinitionPopup: true });
          }
        );
      });
  };

  viewLatestTestDefinitionVersion = currentTestDefinitionData => {
    // gettin latest test version based on provided parameters
    this.props
      .getTestDefinitionLatestVersion(
        currentTestDefinitionData.parent_code,
        currentTestDefinitionData.test_code
      )
      .then(response => {
        // update selectedTestDefinitionId redux state
        this.props.setSelectedTestDefinitionId(response.body.details[0].id);
        this.props.triggerTestDefinitionLoading();
      });
  };

  archiveTestDefinitionTestCode = currentTestDefinitionData => {
    this.setState({
      showArchiveConfirmationPopup: true,
      currentTestDefinitionData: currentTestDefinitionData
    });
  };

  handleArchiveTestDefinitionTestCode = () => {
    this.setState({ archiveProcessLoading: true }, () => {
      // archiving test code
      this.props
        .archiveTestDefinitionTestCode(
          this.state.currentTestDefinitionData.parent_code,
          this.state.currentTestDefinitionData.test_code,
          true
        )
        .then(response => {
          if (response.ok) {
            // refreshing All Tests table
            this.props.triggerSelectTestDefinitionTablesRefresh();
            // updating needed states
            this.setState({
              showArchiveConfirmationPopup: false,
              currentTestDefinitionData: {},
              archiveProcessLoading: false
            });
          } else {
            // temporary console logs until better way is made
            console.log(response);
          }
        });
    });
  };

  closeTestDefinitionPopup = () => {
    this.setState({
      showTestDefinitionPopup: false,
      testDefinitionVersionOptions: [],
      selectedTestDefinitionVersion: { label: "", value: "" },
      showComplementarySelectedTestVersionInfo: false
    });
  };

  closeArchiveTestCodePopup = () => {
    this.setState({ showArchiveConfirmationPopup: false, currentTestDefinitionData: {} });
  };

  onSelectTestDefinitionVersion = event => {
    this.setState({ selectedTestDefinitionVersion: event }, () => {
      this.props
        .getTestDefinitionData(
          false,
          this.state.selectedTestDefinition.parent_code,
          this.state.selectedTestDefinition.test_code,
          this.state.selectedTestDefinitionVersion.value,
          null,
          null,
          null,
          null,
          false,
          false
        )
        .then(response => {
          this.setState({
            showComplementarySelectedTestVersionInfo: true,
            isTestActive: response.body.results[0].details[0].active,
            isSampleTest: response.body.results[0].details[0].is_public,
            isUit: response.body.results[0].details[0].is_uit,
            versionNotes: response.body.results[0].details[0].version_notes
          });
        });
    });
  };

  selectTestDefinition = () => {
    // getting test definition data based on provided parent code, test code and version
    this.props
      .getTestDefinitionData(
        false,
        this.state.selectedTestDefinition.parent_code,
        this.state.selectedTestDefinition.test_code,
        this.state.selectedTestDefinitionVersion.value,
        null,
        null,
        null,
        null,
        false,
        false
      )
      .then(response => {
        // update selectedTestDefinitionId redux state
        this.props.setSelectedTestDefinitionId(response.body.results[0].details[0].id);
        this.props.triggerTestDefinitionLoading();
      });
  };

  handleCreateNewTestDefinition = () => {
    // update selectedTestDefinitionId redux state
    this.props.setSelectedTestDefinitionId("NEW-TD"); // If you update this ID, please make sure to update it in TestBuilder component as well (useful to detect if it a brand new test definition)
    this.props.triggerTestDefinitionLoading();
  };

  render() {
    const columnsDefinition = [
      {
        label: LOCALIZE.testBuilder.testDefinition.selectTestDefinition.allTests.table.column1,
        style: { ...{ width: "15%" }, ...COMMON_STYLE.LEFT_TEXT }
      },
      {
        label: LOCALIZE.testBuilder.testDefinition.selectTestDefinition.allTests.table.column2,
        style: { ...{ width: "25%" }, ...COMMON_STYLE.LEFT_TEXT }
      },
      {
        label: LOCALIZE.testBuilder.testDefinition.selectTestDefinition.allTests.table.column3,
        style: { width: "45%" }
      },
      {
        label: LOCALIZE.testBuilder.testDefinition.selectTestDefinition.allTests.table.column4,
        style: { ...{ width: "15%" }, ...COMMON_STYLE.CENTERED_TEXT }
      }
    ];

    return (
      <div>
        <h2>{LOCALIZE.testBuilder.testDefinition.selectTestDefinition.allTests.title}</h2>
        <p>{LOCALIZE.testBuilder.testDefinition.selectTestDefinition.allTests.description}</p>
        <div style={styles.tableContainer}>
          <div>
            <CustomButton
              label={
                <>
                  <FontAwesomeIcon icon={faPlusCircle} />
                  <span id="new-test-definition-button" style={styles.buttonLabel}>
                    {
                      LOCALIZE.testBuilder.testDefinition.selectTestDefinition
                        .newTestDefinitionButton
                    }
                  </span>
                </>
              }
              action={() => this.handleCreateNewTestDefinition()}
              buttonTheme={THEME.PRIMARY}
            />
            <SearchBarWithDisplayOptions
              idPrefix={"select-test-definition-all-tests"}
              currentlyLoading={this.state.currentlyLoading}
              updateSearchStates={this.props.updateSearchAllTestDefinitionsStates}
              updatePageState={this.props.updateCurrentTestDefinitionAllTestsPageState}
              paginationPageSize={Number(this.props.testDefinitionAllTestsPaginationPageSize)}
              updatePaginationPageSize={this.props.updateCurrentTestDefinitionAllTestsPageSizeState}
              data={this.state.rowsDefinition.data || []}
              resultsFound={this.state.resultsFound}
            />
            <GenericTable
              classnamePrefix="select-test-definition"
              columnsDefinition={columnsDefinition}
              rowsDefinition={this.state.rowsDefinition}
              emptyTableMessage={
                LOCALIZE.testBuilder.testDefinition.selectTestDefinition.allTests.table.noData
              }
              currentlyLoading={this.state.currentlyLoading}
            />
            <Pagination
              paginationContainerId={"select-test-definition-all-tests-pagination-pages-link"}
              pageCount={this.state.numberOfTestDefinitionAllTestsPages}
              paginationPage={this.props.testDefinitionAllTestsPaginationPage}
              updatePaginationPageState={this.props.updateCurrentTestDefinitionAllTestsPageState}
              firstTableRowId={"test-definition-all-tests-select-specific-test-version-id-0"}
            />
          </div>
        </div>
        <PopupBox
          show={this.state.showTestDefinitionPopup}
          handleClose={() => {}}
          isBackdropStatic={true}
          shouldCloseOnEsc={false}
          overflowVisible={false}
          title={
            LOCALIZE.testBuilder.testDefinition.selectTestDefinition.allTests.testDefinitionPopup
              .title
          }
          description={
            <div>
              <div>
                <div style={styles.popupTestInfoContainer}>
                  <div style={styles.popupTestInfoItem}>
                    <div style={styles.popupTestInfoLeftColumn}>
                      <p>
                        {
                          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.allTests
                            .testDefinitionPopup.parentCode
                        }
                      </p>
                    </div>
                    <div style={styles.popupTestInfoRightColumn}>
                      <p>{this.state.selectedTestDefinition.parent_code}</p>
                    </div>
                  </div>
                  <div style={styles.popupTestInfoItem}>
                    <div style={styles.popupTestInfoLeftColumn}>
                      <p>
                        {
                          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.allTests
                            .testDefinitionPopup.testCode
                        }
                      </p>
                    </div>
                    <div style={styles.popupTestInfoRightColumn}>
                      <p>{this.state.selectedTestDefinition.test_code}</p>
                    </div>
                  </div>
                  <div style={styles.popupTestInfoItem}>
                    <div style={styles.popupTestInfoLeftColumn}>
                      <p>
                        {
                          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.allTests
                            .testDefinitionPopup.name
                        }
                      </p>
                    </div>
                    <div style={styles.popupTestInfoRightColumn}>
                      <p>
                        {this.state.selectedTestDefinition[`${this.props.currentLanguage}_name`]}
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div style={styles.selectTestVersionDropdownContainer}>
                <Row style={styles.rowStyle}>
                  {makeLabel(
                    "dropdown",
                    LOCALIZE.testBuilder.testDefinition.selectTestDefinition.allTests
                      .testDefinitionPopup
                  )}
                  {makeDropDownField(
                    "dropdown",
                    LOCALIZE.testBuilder.testDefinition.selectTestDefinition.allTests
                      .testDefinitionPopup,
                    this.state.selectedTestDefinitionVersion,
                    this.state.testDefinitionVersionOptions,
                    this.onSelectTestDefinitionVersion
                  )}
                </Row>
              </div>
              {this.state.showComplementarySelectedTestVersionInfo && (
                <div>
                  <div>
                    <div>
                      <div>
                        <div style={styles.popupTestInfoItem}>
                          <div style={styles.popupTestInfoLeftColumn}>
                            <p>
                              {
                                LOCALIZE.testBuilder.testDefinition.selectTestDefinition.allTests
                                  .testDefinitionPopup.versionNotes
                              }
                            </p>
                          </div>
                          <div style={styles.popupTestInfoRightColumn}>
                            <p style={styles.versionNotesContentStyle}>
                              {this.state.versionNotes !== ""
                                ? this.state.versionNotes
                                : LOCALIZE.commons.na}
                            </p>
                          </div>
                        </div>
                      </div>
                      <div>
                        <div style={styles.popupTestInfoItem}>
                          <div style={styles.popupTestInfoLeftColumn}>
                            <p>
                              {
                                LOCALIZE.testBuilder.testDefinition.selectTestDefinition.allTests
                                  .testDefinitionPopup.uitStatus
                              }
                            </p>
                          </div>
                          <div style={styles.popupTestInfoRightColumn}>
                            <p style={styles.versionNotesContentStyle}>
                              {this.state.isUit ? LOCALIZE.commons.yes : LOCALIZE.commons.no}
                            </p>
                          </div>
                        </div>
                      </div>
                      <div style={styles.popupTestInfoItem}>
                        <div style={styles.popupTestInfoLeftColumn}>
                          <p>
                            {
                              LOCALIZE.testBuilder.testDefinition.selectTestDefinition.allTests
                                .testDefinitionPopup.sampleTestStatus
                            }
                          </p>
                        </div>
                        <div style={styles.popupTestInfoRightColumn}>
                          <p>
                            {this.state.isSampleTest ? LOCALIZE.commons.yes : LOCALIZE.commons.no}
                          </p>
                        </div>
                      </div>
                    </div>
                    <div style={styles.popupTestInfoItem}>
                      <div style={styles.popupTestInfoLeftColumn}>
                        <p>
                          {
                            LOCALIZE.testBuilder.testDefinition.selectTestDefinition.allTests
                              .testDefinitionPopup.testStatus
                          }
                        </p>
                      </div>
                      <div style={styles.popupTestInfoRightColumn}>
                        <p>
                          {this.state.isTestActive
                            ? LOCALIZE.commons.active
                            : LOCALIZE.commons.inactive}
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              )}
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={
            LOCALIZE.testBuilder.testDefinition.selectTestDefinition.allTests.testDefinitionPopup
              .rightButton
          }
          rightButtonIcon={faCheck}
          rightButtonAction={this.selectTestDefinition}
          rightButtonState={
            this.state.selectedTestDefinitionVersion.label !== ""
              ? BUTTON_STATE.enabled
              : BUTTON_STATE.disabled
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeTestDefinitionPopup}
        />
        <PopupBox
          show={this.state.showArchiveConfirmationPopup}
          handleClose={() => {}}
          isBackdropStatic={true}
          shouldCloseOnEsc={false}
          overflowVisible={true}
          title={
            LOCALIZE.testBuilder.testDefinition.selectTestDefinition.allTests.archiveTestCodePopup
              .title
          }
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.warning}
                title={LOCALIZE.commons.warning}
                message={
                  <p>
                    {LOCALIZE.formatString(
                      LOCALIZE.testBuilder.testDefinition.selectTestDefinition.allTests
                        .archiveTestCodePopup.systemMessageDescription,
                      <span
                        style={styles.bold}
                      >{`${this.state.currentTestDefinitionData.parent_code} ${this.state.currentTestDefinitionData.test_code}`}</span>
                    )}
                  </p>
                }
              />

              <p>
                {
                  LOCALIZE.testBuilder.testDefinition.selectTestDefinition.allTests
                    .archiveTestCodePopup.description
                }
              </p>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={
            LOCALIZE.testBuilder.testDefinition.selectTestDefinition.allTests.archiveTestCodePopup
              .rightButton
          }
          rightButtonIcon={faFileArchive}
          rightButtonAction={this.handleArchiveTestDefinitionTestCode}
          rightButtonState={
            this.state.archiveProcessLoading ? BUTTON_STATE.disabled : BUTTON_STATE.enabled
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeArchiveTestCodePopup}
        />
      </div>
    );
  }
}

export { AllTests as unconnectedAllTests };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    testDefinitionAllTestsPaginationPageSize:
      state.testBuilder.testDefinitionAllTestsPaginationPageSize,
    testDefinitionAllTestsPaginationPage: state.testBuilder.testDefinitionAllTestsPaginationPage,
    testDefinitionAllTestsKeyword: state.testBuilder.testDefinitionAllTestsKeyword,
    testDefinitionAllTestsActiveSearch: state.testBuilder.testDefinitionAllTestsActiveSearch,
    selectedTestDefinitionId: state.testBuilder.selectedTestDefinitionId,
    triggerSelectTestDefinitionTablesRefreshState:
      state.testBuilder.triggerSelectTestDefinitionTablesRefreshState
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getTestDefinitionData,
      updateCurrentTestDefinitionAllTestsPageState,
      updateCurrentTestDefinitionAllTestsPageSizeState,
      updateSearchAllTestDefinitionsStates,
      setSelectedTestDefinitionId,
      triggerTestDefinitionLoading,
      archiveTestDefinitionTestCode,
      getTestDefinitionLatestVersion,
      triggerSelectTestDefinitionTablesRefresh
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(AllTests);
