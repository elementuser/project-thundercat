import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Row } from "react-bootstrap";
import { makeLabel, makeTextBoxField, makeSaveDeleteButtons, allValid } from "./helpers";
import { modifyTestDefinitionField } from "../../modules/TestBuilderRedux";
import { styles } from "./TestSectionForm";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";

class SectionComponentPageForm extends Component {
  state = {
    showDialog: false,
    isEnTitleValid: true,
    isFrTitleValid: true,
    sectionComponentPage: {}
  };

  modifyField = (inputName, value) => {
    const newObj = { ...this.props.sectionComponentPage, ...this.state.sectionComponentPage };
    newObj[`${inputName}`] = value;
    this.setState({ sectionComponentPage: newObj });
  };

  frTitleValidation = (name, value) => {
    // allow maximum of 150 chars
    const regexExpression = /^(.{0,150})$/;
    if (regexExpression.test(value)) {
      this.setState({ isFrTitleValid: true });
      this.modifyField(name, value);
    }
  };

  enTitleValidation = (name, value) => {
    // allow maximum of 150 chars
    const regexExpression = /^(.{0,150})$/;
    if (regexExpression.test(value)) {
      this.setState({ isEnTitleValid: true });
      this.modifyField(name, value);
    }
  };

  onSelectChange = (event, action) => {
    this.modifyField(action.name, event.value);
  };

  handleSave = () => {
    if (allValid(this.state)) {
      this.props.modifyTestDefinitionField(
        "section_component_pages",
        { ...this.props.sectionComponentPage, ...this.state.sectionComponentPage },
        this.props.sectionComponentPage.id
      );
      this.props.expandItem();
    }
  };

  confirmDelete = () => {
    this.props.handleDelete(this.props.sectionComponentPage.id);
    this.props.expandItem();
    this.closeDialog();
  };

  openDialog = () => {
    this.setState({ showDialog: true });
  };

  closeDialog = () => {
    this.setState({ showDialog: false });
  };

  render() {
    const sectionComponentPage = {
      ...this.props.sectionComponentPage,
      ...this.state.sectionComponentPage
    };

    return (
      <div style={styles.mainContainer}>
        <Row style={styles.rowStyle}>
          {makeLabel("en_title", LOCALIZE.testBuilder.sectionComponentPages)}
          {makeTextBoxField(
            "en_title",
            LOCALIZE.testBuilder.sectionComponentPages,
            sectionComponentPage.en_title,
            this.state.isEnTitleValid,
            this.enTitleValidation
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("fr_title", LOCALIZE.testBuilder.sectionComponentPages)}
          {makeTextBoxField(
            "fr_title",
            LOCALIZE.testBuilder.sectionComponentPages,
            sectionComponentPage.fr_title,
            this.state.isFrTitleValid,
            this.frTitleValidation
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("order", LOCALIZE.testBuilder.sectionComponentPages)}
          {makeTextBoxField(
            "order",
            LOCALIZE.testBuilder.sectionComponentPages,
            sectionComponentPage.order,
            true,
            () => {},
            "test-section-component-page-order",
            true
          )}
        </Row>
        <Row className="justify-content-between" style={styles.buttonRowStyle}>
          {makeSaveDeleteButtons(
            this.handleSave,
            this.openDialog,
            LOCALIZE.commons.applyButton,
            LOCALIZE.commons.deleteButton
          )}
        </Row>

        <PopupBox
          show={this.state.showDialog}
          handleClose={this.closeDialog}
          title={"Delete Confirmation"}
          description={
            <div>
              <p>
                Deleting this item will delete all the content displayed and all child objects that
                are related to this object. Are you sure you want to delete this object?
              </p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={LOCALIZE.commons.deleteButton}
          rightButtonAction={this.confirmDelete}
        />
      </div>
    );
  }
}

export { SectionComponentPageForm as unconnectedSectionComponentPageForm };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({ modifyTestDefinitionField }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SectionComponentPageForm);
