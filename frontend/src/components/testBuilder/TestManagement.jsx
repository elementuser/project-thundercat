import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Row, Container } from "react-bootstrap";
import PopupBox, { BUTTON_STATE, BUTTON_TYPE } from "../commons/PopupBox";
import {
  modifyTestDefinitionField,
  uploadTest,
  getTestDefinitionExtract,
  loadTestDefinition,
  activateTestDefinition,
  TEST_DEFINITION,
  setTestDefinitionValidationErrors,
  triggerTestDefinitionLoading,
  getTestDefinitionLatestVersion,
  setSelectedTestDefinitionId,
  setQuestionRulesValidStateWhenItemExposureEnabled,
  triggerTestDefinitionFieldUpdates,
  setNewTestDefinitionUploadedState
} from "../../modules/TestBuilderRedux";
import CustomButton, { THEME } from "../commons/CustomButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCheck,
  faDownload,
  faSpinner,
  faTimes,
  faUpload
} from "@fortawesome/free-solid-svg-icons";
import getSwitchTransformScale from "../../helpers/switchTransformScale";
import Switch from "react-switch";
import { makeLabel, makeTextAreaField } from "./helpers";
import SystemMessage, { MESSAGE_TYPE } from "../commons/SystemMessage";
import questionListSubSectionValidation, {
  subSectionsValidation,
  sectionComponentPagesValidation,
  validateQuestionRulesData,
  validateQuestionRulesDataWithEnabledItemExposure,
  validateTestSections,
  validateTestDefinitionData
} from "./validation";

export const styles = {
  mainContainer: {
    width: "100%"
  },
  borderBox: {
    borderTop: "1px solid",
    borderRadius: "5px",
    padding: "5px",
    borderColor: "#CECECE",
    color: "#923534",
    margin: "5px"
  },
  buttonCustomStyle: {
    minWidth: 200
  },
  errorMessageContainer: {
    border: "1px solid #923534",
    padding: 12,
    marginBottom: 12
  },
  uploadFailedErrorMessage: {
    textAlign: "center",
    color: "#923534",
    fontWeight: "bold"
  },
  itemContainer: {
    display: "table",
    width: "100%",
    margin: "24px 0"
  },
  labelContainer: {
    display: "table-cell",
    verticalAlign: "middle",
    fontWeight: "bold",
    width: "40%"
  },
  buttonContainer: {
    display: "table-cell",
    verticalAlign: "middle"
  },
  loadingContainer: {
    textAlign: "center"
  },
  buttonLabel: {
    marginLeft: 6
  },
  rowStyle: {
    margin: "18px 5px"
  },
  customLabelForTextAreaStyle: {
    marginTop: 0,
    marginBottom: 0
  },
  customTextAreaStyle: {
    height: 60
  }
};

class TestManagement extends Component {
  localize = LOCALIZE.testBuilder.testDefinition;

  state = {
    showUploadDialog: false,
    showUploadJSONDialog: false,
    showFailedUploadDialog: false,
    test_definition: {},
    JSONUpload: "",
    showUploadFailedErrorMessage: false,
    showUploadTestComplementaryErrorMessages: false,
    uploadUploadTestComplementaryErrorContent: "",
    uploadUploadTestComplementaryErrorMessagesContent: "",
    showUploadJsonComplementaryErrorMessages: false,
    currentlyUploadingTest: false,
    testUploadedSuccessfully: false,
    latestTestDefinitionVersionData: {},
    // default values
    switchHeight: 25,
    switchWidth: 50,
    areSwitchStatesLoading: false,
    versionNotesContent: { name: "", value: "" },
    triggerUploadAction: false
  };

  componentDidMount = () => {
    // making sure the the validation errors state is defined
    if (typeof this.props.validationErrors !== "undefined") {
      // if there is at least one validation error
      if (
        !this.props.validationErrors.testDefinitionContainsValidTestDefinitionData ||
        !this.props.validationErrors.testDefinitionContainsAtLeastThreeTestSections ||
        !this.props.validationErrors.testDefinitionContainsQuitTestSection ||
        !this.props.validationErrors.testDefinitionContainsFinishTestSection ||
        !this.props.validationErrors.testDefinitionContainsValidScoringMethod ||
        !this.props.validationErrors.testDefinitionContainsValidQuestionRules ||
        !this.props.validationErrors.testDefinitionContainsValidItemBankRules
      ) {
        // updating needed states
        this.setState({
          showUploadFailedErrorMessage: true,
          showUploadTestComplementaryErrorMessages: false,
          showUploadJsonComplementaryErrorMessages: false
        });
      }
    }
    this.getUpdatedSwitchDimensions();
  };

  componentDidUpdate = (prevProps, prevState) => {
    if (prevProps.test_definition.version !== this.props.test_definition.version) {
      this.resetValidationStates();
    }
    if (prevProps.accommodations.fontSize !== this.props.accommodations.fontSize) {
      this.getUpdatedSwitchDimensions();
    }
    // if triggerUploadAction gets updated
    if (prevState.triggerUploadAction !== this.state.triggerUploadAction) {
      // upload test in the DB
      this.uploadTest();
    }
    // if any of the main sections (including validation errors) get updated
    if (
      prevProps.validationErrors !== this.props.validationErrors ||
      prevProps.scoringMethodValidState !== this.props.scoringMethodValidState ||
      prevProps.testSections !== this.props.testSections ||
      prevProps.testSectionComponents !== this.props.testSectionComponents ||
      prevProps.questions !== this.props.questions
    ) {
      // if there is no validation errors
      if (
        this.props.validationErrors.testDefinitionContainsValidTestDefinitionData &&
        this.props.validationErrors.testDefinitionContainsAtLeastThreeTestSections &&
        this.props.validationErrors.testDefinitionContainsQuitTestSection &&
        this.props.validationErrors.testDefinitionContainsFinishTestSection &&
        this.props.validationErrors.testDefinitionContainsValidScoringMethod &&
        this.props.validationErrors.testDefinitionContainsValidQuestionRules &&
        this.props.validationErrors.testDefinitionContainsValidSubSections &&
        this.props.validationErrors.testDefinitionContainsValidQuestionLists &&
        this.props.validationErrors.testDefinitionContainsValidSectionComponentPages &&
        this.props.validationErrors.testDefinitionContainsValidItemBankRules
      ) {
        // removing all errors
        this.setState({
          showUploadFailedErrorMessage: false,
          showUploadTestComplementaryErrorMessages: false,
          showUploadJsonComplementaryErrorMessages: false
        });
      } else {
        // display error message
        this.setState({
          showUploadFailedErrorMessage: true
        });
      }
    }
  };

  resetValidationStates = () => {
    this.setState({
      showUploadFailedErrorMessage: false,
      showUploadTestComplementaryErrorMessages: false,
      uploadUploadTestComplementaryErrorContent: "",
      uploadUploadTestComplementaryErrorMessagesContent: "",
      showUploadJsonComplementaryErrorMessages: false
    });
  };

  closeDialog = () => {
    this.setState({
      showUploadDialog: false,
      showUploadJSONDialog: false
    });
  };

  openUploadDialog = () => {
    this.setState({ showUploadDialog: true });
  };

  confirmUpload = () => {
    const isTestDefinitionValid = this.validateTestDefinition();
    // trigger test definition field updates
    this.props.triggerTestDefinitionFieldUpdates();
    // test definition is valid
    if (isTestDefinitionValid) {
      this.setState({ currentlyUploadingTest: true }, () => {
        // update test definition data (version notes)
        this.modifyField(this.state.versionNotesContent.name, this.state.versionNotesContent.value);
        this.setState({ triggerUploadAction: !this.state.triggerUploadAction });
      });
      // invalid test definition
    } else {
      // updating needed states
      this.setState({
        showUploadFailedErrorMessage: true,
        showUploadTestComplementaryErrorMessages: false,
        uploadUploadTestComplementaryErrorContent: "",
        uploadUploadTestComplementaryErrorMessagesContent: "",
        showUploadJsonComplementaryErrorMessages: false,
        currentlyUploadingTest: false,
        testUploadedSuccessfully: false,
        showUploadDialog: false
      });
    }
  };

  uploadTest = () => {
    // creating a copy of newTestDefinition
    const copyOfNewTestDefinition = this.props.newTestDefinition;
    // making sure that the version notes are up to date with redux states
    copyOfNewTestDefinition.test_definition[0].version_notes = this.state.versionNotesContent.value;
    this.props.uploadTest(copyOfNewTestDefinition).then(response => {
      if (response.ok) {
        // updating needed states
        this.setState(
          {
            showUploadFailedErrorMessage: false,
            showUploadTestComplementaryErrorMessages: false,
            showUploadJsonComplementaryErrorMessages: false
          },
          () => {
            this.props
              .getTestDefinitionLatestVersion(
                this.props.test_definition.parent_code,
                this.props.test_definition.test_code
              )
              .then(response => {
                // set needed states
                this.setState({
                  currentlyUploadingTest: false,
                  testUploadedSuccessfully: true,
                  latestTestDefinitionVersionData: response
                });
              });
          }
        );
      } else {
        // updating needed states
        this.setState({
          showUploadFailedErrorMessage: false,
          showUploadTestComplementaryErrorMessages: true,
          uploadUploadTestComplementaryErrorContent: response.body.error,
          uploadUploadTestComplementaryErrorMessagesContent: response.body.message,
          showUploadJsonComplementaryErrorMessages: false,
          currentlyUploadingTest: false,
          testUploadedSuccessfully: false,
          showUploadDialog: false,
          versionNotesContent: { name: "", value: "" }
        });
      }
      // temporary console logs until better way is made
      console.log(response);
    });
  };

  // upload test validation
  validateTestDefinition = () => {
    // calling test definition data validation
    let validationErrors = validateTestDefinitionData(
      this.props.validationErrors,
      this.props.test_definition
    );

    // calling test sections validation
    validationErrors = validateTestSections(this.props.validationErrors, this.props.testSections);

    // calling question list sub-section validation
    validationErrors = questionListSubSectionValidation(
      this.props.validationErrors,
      this.props.testSectionComponents,
      this.props.questions
    );
    // calling sub-sections validation
    validationErrors = subSectionsValidation(
      validationErrors,
      this.props.testSections,
      this.props.testSectionComponents
    );
    // calling section component pages (page content titles) validation
    validationErrors = sectionComponentPagesValidation(
      validationErrors,
      this.props.testSections,
      this.props.testSectionComponents,
      this.props.sectionComponentPages
    );

    // making sure that all validation requirements are respected
    if (
      validationErrors.testDefinitionContainsValidTestDefinitionData &&
      validationErrors.testDefinitionContainsAtLeastThreeTestSections &&
      validationErrors.testDefinitionContainsQuitTestSection &&
      validationErrors.testDefinitionContainsFinishTestSection &&
      this.props.questionRulesValidState &&
      this.props.scoringMethodValidState &&
      validationErrors.testDefinitionContainsValidSubSections &&
      validationErrors.testDefinitionContainsValidQuestionLists &&
      validationErrors.testDefinitionContainsValidSectionComponentPages &&
      validationErrors.testDefinitionContainsValidItemBankRules
    ) {
      // updating redux validation errors state
      this.props.setTestDefinitionValidationErrors({
        testDefinitionContainsValidTestDefinitionData: true,
        testDefinitionContainsAtLeastThreeTestSections: true,
        testDefinitionContainsQuitTestSection: true,
        testDefinitionContainsFinishTestSection: true,
        testDefinitionContainsValidQuestionRules: true,
        testDefinitionQuestionRulesErrorType: null,
        testDefinitionContainsValidScoringMethod: true,
        testDefinitionContainsValidSubSections: true,
        testDefinitionContainsValidQuestionLists: true,
        testDefinitionContainsValidSectionComponentPages: true,
        testDefinitionContainsValidItemBankRules: true
      });
      return true;
    }
    this.getValidationErrors();
    return false;
  };

  getValidationErrors = () => {
    try {
      // ==================== QUESTION RULES VALIDATION ==========
      // basic question rules validation
      let testDefinitionContainsValidQuestionRules = validateQuestionRulesData(
        this.props.questions,
        this.props.allQuestionListRules
      );
      // question rules validation if at least one section has item_exposure enabled
      // initializing testSectionComponentIdsArray
      const testSectionComponentIdsArray = [];
      // looping in questions
      for (let i = 0; i < this.props.questions.length; i++) {
        // if test_section_component of current question is not already in testSectionComponentIdsArray
        if (
          !testSectionComponentIdsArray.includes(this.props.questions[i].test_section_component)
        ) {
          // populating testSectionComponentIdsArray
          testSectionComponentIdsArray.push(this.props.questions[i].test_section_component);
        }
      }
      // initializing testDefinitionQuestionRulesErrorType
      let testDefinitionQuestionRulesErrorType = null;
      // initializing areQuestionsRulesValidWhenItemExposureEnabled
      let areQuestionsRulesValidWhenItemExposureEnabled = { is_valid: true, error_type: null };
      // looping in testSectionComponentIdsArray
      for (let i = 0; i < testSectionComponentIdsArray.length; i++) {
        // getting current test section
        const currentTestSection = this.props.testSections.filter(
          obj =>
            obj.id ===
            this.props.testSectionComponents.filter(
              obj => obj.id === testSectionComponentIdsArray[i]
            )[0].test_section[0]
        )[0];
        // if current test section has item_exposure enabled
        if (currentTestSection.item_exposure) {
          areQuestionsRulesValidWhenItemExposureEnabled =
            validateQuestionRulesDataWithEnabledItemExposure(
              this.props.questions,
              this.props.allQuestionListRules
            );
          // not valid
          if (!areQuestionsRulesValidWhenItemExposureEnabled.is_valid) {
            // setting testDefinitionContainsValidQuestionRules and testDefinitionQuestionRulesErrorType
            testDefinitionContainsValidQuestionRules =
              areQuestionsRulesValidWhenItemExposureEnabled.is_valid;
            testDefinitionQuestionRulesErrorType =
              areQuestionsRulesValidWhenItemExposureEnabled.error_type;
            // setting question rules redux states
            this.props.setQuestionRulesValidStateWhenItemExposureEnabled(
              areQuestionsRulesValidWhenItemExposureEnabled.is_valid,
              areQuestionsRulesValidWhenItemExposureEnabled.error_type
            );
            break;
          }
          // setting question rules redux states
          this.props.setQuestionRulesValidStateWhenItemExposureEnabled(
            areQuestionsRulesValidWhenItemExposureEnabled.is_valid,
            areQuestionsRulesValidWhenItemExposureEnabled.error_type
          );
        }
      }
      // ==================== QUESTION RULES VALIDATION (END) ==========

      // ==================== SCORING METHOD VALIDATION ==========
      let testDefinitionContainsValidScoringMethod = true;
      // does not have valid scoring method
      if (
        !this.props.scoringMethodValidState ||
        typeof this.props.scoringMethodValidState === "undefined"
      ) {
        testDefinitionContainsValidScoringMethod = false;
      }
      // ==================== SCORING METHOD VALIDATION (END) ==========
      // updating redux validation errors state
      this.props.setTestDefinitionValidationErrors({
        testDefinitionContainsValidTestDefinitionData:
          this.props.validationErrors.testDefinitionContainsValidTestDefinitionData,
        testDefinitionContainsAtLeastThreeTestSections:
          this.props.validationErrors.testDefinitionContainsAtLeastThreeTestSections,
        testDefinitionContainsQuitTestSection:
          this.props.validationErrors.testDefinitionContainsQuitTestSection,
        testDefinitionContainsFinishTestSection:
          this.props.validationErrors.testDefinitionContainsFinishTestSection,
        testDefinitionContainsValidQuestionRules: testDefinitionContainsValidQuestionRules,
        testDefinitionQuestionRulesErrorType: testDefinitionQuestionRulesErrorType,
        testDefinitionContainsValidScoringMethod: testDefinitionContainsValidScoringMethod,
        testDefinitionContainsValidSubSections:
          this.props.validationErrors.testDefinitionContainsValidSubSections,
        testDefinitionContainsValidQuestionLists:
          this.props.validationErrors.testDefinitionContainsValidQuestionLists,
        testDefinitionContainsValidSectionComponentPages:
          this.props.validationErrors.testDefinitionContainsValidSectionComponentPages,
        testDefinitionContainsValidItemBankRules:
          this.props.validationErrors.testDefinitionContainsValidItemBankRules
      });
      // something happened during the upload process (can usually happen if you modify and try to upload a JSON file)
    } catch {
      // display failed upload popup + update needed states
      this.setState({
        showFailedUploadDialog: true
      });
    }
  };

  closeFailedUploadPopup = () => {
    this.setState({
      showFailedUploadDialog: false,
      showUploadTestComplementaryErrorMessages: false,
      uploadUploadTestComplementaryErrorContent: "",
      uploadUploadTestComplementaryErrorMessagesContent: "",
      showUploadJsonComplementaryErrorMessages: false,
      currentlyUploadingTest: false,
      testUploadedSuccessfully: false,
      showUploadFailedErrorMessage: false
    });
  };

  modifyField = (inputName, value) => {
    const newObj = { ...this.props.test_definition, ...this.state.test_definition };
    newObj[`${inputName}`] = value;
    this.setState({ test_definition: newObj }, () => {
      this.props.modifyTestDefinitionField(TEST_DEFINITION, newObj, newObj.id);
    });
  };

  handleDeativate = () => {
    this.props.activateTestDefinition(this.props.test_definition.id, false).then(response => {
      if (response.ok) {
        this.modifyField("active", false);
      }
    });
  };

  handleActivate = () => {
    this.props.activateTestDefinition(this.props.test_definition.id, true).then(response => {
      if (response.ok) {
        this.modifyField("active", true);
      }
    });
  };

  // WILL FAIL: NEED TO REPLACE THE selectedTestDefinition STATE
  downloadTestDefinition = async () => {
    this.props
      .getTestDefinitionExtract(this.props.test_definition.id, this.props.currentLanguage)
      .then(response => {
        if (response.ok) {
          this.downloadFile(response.body);
        } else {
          // temporary console logs until better way is made
          console.log(response);
        }
      });
  };

  downloadFile = async response => {
    const fileName = `${response.new_test.test_definition[0].test_code}`;
    const json = JSON.stringify(response);
    const blob = new Blob([json], { type: "application/json" });
    const href = await URL.createObjectURL(blob);
    const link = document.createElement("a");
    link.href = href;
    link.download = `${fileName}.json`;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  };

  handleNewUploadedTestDownload = () => {
    this.props.setSelectedTestDefinitionId(
      this.state.latestTestDefinitionVersionData.body.details[0].id
    );
    this.props.triggerTestDefinitionLoading();
    this.props.setNewTestDefinitionUploadedState(true);
    this.setState({ showUploadConfirmationPopup: false });
  };

  // getting switch dimensions and updating states
  getUpdatedSwitchDimensions = () => {
    // need this loading state, otherwise the switch diameter is not calculated on state change
    this.setState({ areSwitchStatesLoading: true }, () => {
      this.setState(
        {
          switchHeight: getSwitchTransformScale(this.props.accommodations.fontSize).height,
          switchWidth: getSwitchTransformScale(this.props.accommodations.fontSize).width
        },
        () => {
          this.setState({ areSwitchStatesLoading: false });
        }
      );
    });
  };

  onVersionNotesChange = (name, value) => {
    // allow maximum of 500 chars
    const regexExpression = /^(.{0,500})$/;
    if (regexExpression.test(value)) {
      this.setState({ versionNotesContent: { name: name, value: value } });
    }
  };

  render() {
    const test_definition = { ...this.props.test_definition, ...this.state.test_definition };
    const testName = `: ${this.props.test_definition.test_code} v${this.props.test_definition.version}`;
    const isTestActive = test_definition.active;

    const customFontSize = {
      // adding 5px to current font size
      fontSize: `${parseInt(this.props.accommodations.fontSize.split("px")[0]) + 5}px`
    };

    return (
      <div style={styles.mainContainer}>
        <h2>{this.localize.title + testName}</h2>
        {this.state.showUploadFailedErrorMessage && (
          <div style={styles.errorMessageContainer}>
            <p style={{ ...styles.uploadFailedErrorMessage, ...customFontSize }}>
              {LOCALIZE.testBuilder.testDefinition.uploadFailedErrorMessage}
            </p>
          </div>
        )}
        {(this.state.showUploadTestComplementaryErrorMessages ||
          this.state.showUploadJsonComplementaryErrorMessages) && (
          <div style={styles.errorMessageContainer}>
            {this.state.showUploadTestComplementaryErrorMessages && (
              <p style={{ ...styles.uploadFailedErrorMessage, ...customFontSize }}>
                {LOCALIZE.formatString(
                  LOCALIZE.testBuilder.testDefinition.uploadFailedComplementaryErrorMessages,
                  this.state.uploadUploadTestComplementaryErrorContent
                )}
              </p>
            )}
            {this.state.showUploadJsonComplementaryErrorMessages && (
              <p style={{ ...styles.uploadFailedErrorMessage, ...customFontSize }}>
                {LOCALIZE.testBuilder.testDefinition.uploadJsonFailedComplementaryErrorMessages}
              </p>
            )}
            {this.state.uploadUploadTestComplementaryErrorMessagesContent !== "" &&
              this.state.uploadUploadTestComplementaryErrorMessagesContent.map((message, index) => {
                return (
                  <div key={index}>
                    <span style={{ ...styles.uploadFailedErrorMessage, ...{ textAlign: "left" } }}>
                      {message[`${this.props.currentLanguage}`]}
                    </span>
                  </div>
                );
              })}
          </div>
        )}

        <p>{LOCALIZE.testBuilder.testManagement.description}</p>

        <Container>
          <div style={styles.borderBox} />
          {this.props.test_definition && (
            <div>
              <div style={styles.itemContainer}>
                <div style={styles.labelContainer}>
                  {makeLabel(
                    isTestActive ? "deactivateTestVersion" : "activateTestVersion",
                    LOCALIZE.testBuilder.testManagement,
                    "activate-deactivate-switch"
                  )}
                </div>
                <div style={styles.buttonContainer}>
                  {!this.state.areSwitchStatesLoading && (
                    <Switch
                      onChange={isTestActive ? this.handleDeativate : this.handleActivate}
                      checked={isTestActive}
                      aria-labelledby="activate-deactivate-switch"
                      height={this.state.switchHeight}
                      width={this.state.switchWidth}
                    />
                  )}
                </div>
              </div>
              <div style={styles.itemContainer}>
                <div style={styles.labelContainer}>
                  {makeLabel(
                    "uploadTestVersion",
                    LOCALIZE.testBuilder.testManagement,
                    "upload-test-version-label"
                  )}
                </div>
                <div style={styles.buttonContainer}>
                  <CustomButton
                    label={
                      <>
                        <FontAwesomeIcon icon={faUpload} />
                        <span style={styles.buttonLabel}>
                          {LOCALIZE.testBuilder.testManagement.uploadTestVersionButton}
                        </span>
                      </>
                    }
                    action={this.openUploadDialog}
                    customStyle={styles.buttonCustomStyle}
                    buttonTheme={THEME.PRIMARY}
                  />
                </div>
              </div>
              <div style={styles.itemContainer}>
                <div style={styles.labelContainer}>
                  {makeLabel(
                    "downloadTestVersion",
                    LOCALIZE.testBuilder.testManagement,
                    "download-json-label"
                  )}
                </div>
                <div style={styles.buttonContainer}>
                  <CustomButton
                    label={
                      <>
                        <FontAwesomeIcon icon={faDownload} />
                        <span style={styles.buttonLabel}>
                          {LOCALIZE.testBuilder.testManagement.downloadTestVersionButton}
                        </span>
                      </>
                    }
                    action={this.downloadTestDefinition}
                    customStyle={styles.buttonCustomStyle}
                    buttonTheme={THEME.PRIMARY}
                  />
                </div>
              </div>
            </div>
          )}
        </Container>

        <PopupBox
          show={this.state.showUploadDialog}
          handleClose={() => {}}
          title={
            !this.state.testUploadedSuccessfully
              ? this.localize.uploadTestDefinition.title1
              : this.localize.uploadTestDefinition.title2
          }
          description={
            <div>
              <div>
                {!this.state.testUploadedSuccessfully && (
                  <div>
                    <Row style={styles.rowStyle}>
                      {makeLabel(
                        "version_notes",
                        this.localize,
                        "test-definition-version-notes-label",
                        styles.customLabelForTextAreaStyle
                      )}
                      {makeTextAreaField(
                        "version_notes",
                        this.localize,
                        this.state.versionNotesContent.value,
                        true,
                        this.onVersionNotesChange,
                        "test-definition-version-notes",
                        this.state.currentlyUploadingTest,
                        styles.customTextAreaStyle
                      )}
                    </Row>
                    <div>
                      <p>{this.localize.uploadTestDefinition.description1}</p>
                    </div>
                  </div>
                )}
                {this.state.testUploadedSuccessfully && (
                  <div>
                    <p>
                      {
                        <SystemMessage
                          messageType={MESSAGE_TYPE.success}
                          title={LOCALIZE.commons.success}
                          message={this.localize.uploadTestDefinition.description2}
                        />
                      }
                    </p>
                  </div>
                )}
              </div>
            </div>
          }
          leftButtonType={
            !this.state.testUploadedSuccessfully ? BUTTON_TYPE.secondary : BUTTON_TYPE.none
          }
          leftButtonTitle={!this.state.testUploadedSuccessfully ? LOCALIZE.commons.cancel : ""}
          leftButtonAction={
            !this.state.testUploadedSuccessfully ? () => this.closeDialog() : () => {}
          }
          leftButtonState={
            this.state.currentlyUploadingTest ? BUTTON_STATE.disabled : BUTTON_STATE.enabled
          }
          leftButtonIcon={faTimes}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={
            this.state.currentlyUploadingTest ? (
              // eslint-disable-next-line jsx-a11y/label-has-associated-control
              <label className="fa fa-spinner fa-spin">
                <FontAwesomeIcon icon={faSpinner} />
              </label>
            ) : !this.state.testUploadedSuccessfully ? (
              LOCALIZE.commons.upload
            ) : (
              LOCALIZE.commons.ok
            )
          }
          rightButtonAction={
            !this.state.testUploadedSuccessfully
              ? this.confirmUpload
              : this.handleNewUploadedTestDownload
          }
          rightButtonState={
            this.state.versionNotesContent.value === ""
              ? BUTTON_STATE.disabled
              : this.state.currentlyUploadingTest
              ? BUTTON_STATE.disabled
              : BUTTON_STATE.enabled
          }
          rightButtonIcon={this.state.currentlyUploadingTest ? "" : faCheck}
        />
        <PopupBox
          show={this.state.showFailedUploadDialog}
          handleClose={() => {}}
          title={this.localize.uploadFailedPopup.title}
          description={
            <div>
              <p>
                {
                  <SystemMessage
                    messageType={MESSAGE_TYPE.error}
                    title={LOCALIZE.commons.error}
                    message={this.localize.uploadFailedPopup.systemMessage}
                  />
                }
              </p>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.ok}
          rightButtonAction={this.closeFailedUploadPopup}
        />
      </div>
    );
  }
}

export { TestManagement as unconnectedTestManagement };

const mapStateToProps = (state, ownProps) => {
  // done because only one test definition is returned ever
  const INDEX = 0;
  return {
    test_definition: state.testBuilder.test_definition[INDEX],
    currentLanguage: state.localize.language,
    newTestDefinition: state.testBuilder,
    scoringMethodValidState: state.testBuilder.scoring_method_valid_state,
    accommodations: state.accommodations,
    validationErrors: state.testBuilder.validation_errors,
    questionRulesValidState: state.testBuilder.question_rules_valid_state,
    allQuestionListRules: state.testBuilder.question_list_rules,
    questions: state.testBuilder.questions,
    testSections: state.testBuilder.test_sections,
    testSectionComponents: state.testBuilder.test_section_components,
    sectionComponentPages: state.testBuilder.section_component_pages
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      modifyTestDefinitionField,
      uploadTest,
      getTestDefinitionExtract,
      loadTestDefinition,
      activateTestDefinition,
      setTestDefinitionValidationErrors,
      triggerTestDefinitionLoading,
      getTestDefinitionLatestVersion,
      setSelectedTestDefinitionId,
      setQuestionRulesValidStateWhenItemExposureEnabled,
      triggerTestDefinitionFieldUpdates,
      setNewTestDefinitionUploadedState
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(TestManagement);
