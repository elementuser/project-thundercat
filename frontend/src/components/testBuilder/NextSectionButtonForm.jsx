import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Row, Col } from "react-bootstrap";
import LOCALIZE from "../../text_resources";
import { LANGUAGES } from "../../modules/LocalizeRedux";
import {
  makeLabel,
  makeTextBoxField,
  makeSaveDeleteButtons,
  makeDropDownField,
  getNextInNumberSeries,
  allValid,
  makeTextAreaField
} from "./helpers";
import {
  modifyTestDefinitionField,
  replaceTestDefinitionField,
  NEXT_SECTION_BUTTONS
} from "../../modules/TestBuilderRedux";
import { styles } from "./TestSectionForm";
import { UnconnectedTopTabs as TopTabs } from "../commons/TopTabs";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import {
  NextSectionButtonType,
  NextSectionButtonTypeObject,
  TestSectionType
} from "../testFactory/Constants";
import Switch from "react-switch";
import getSwitchTransformScale from "../../helpers/switchTransformScale";

const options = Object.getOwnPropertyNames(NextSectionButtonTypeObject).map(property => {
  return NextSectionButtonTypeObject[property];
});

class NextSectionButtonForm extends Component {
  state = {
    showDialog: false,
    isEnTitleValid: true,
    isENContentValid: true,
    isOrderValid: true,
    next_section_button_type: options.filter(
      obj => obj.value === this.props.nextSectionButtonType
    )[0],
    enButtonDetails: {
      language: LANGUAGES.english
    },
    frButtonDetails: {
      language: LANGUAGES.french
    },
    selectedParent: {},
    isValidTestNextSectionButtonType: true,
    // default values
    switchHeight: 25,
    switchWidth: 50,
    areSwitchStatesLoading: false
  };

  modifyEnButtonText = (inputName, value) => {
    // allow maximum of 50 chars
    const regexExpression = /^(.{0,50})$/;
    if (regexExpression.test(value)) {
      const newObj = { ...this.props.enButtonDetails, ...this.state.enButtonDetails };
      newObj[`${inputName}`] = value;
      this.setState({ enButtonDetails: newObj });
    }
  };

  modifyFrButtonText = (inputName, value) => {
    // allow maximum of 50 chars
    const regexExpression = /^(.{0,50})$/;
    if (regexExpression.test(value)) {
      const newObj = { ...this.props.frButtonDetails, ...this.state.frButtonDetails };
      newObj[`${inputName}`] = value;
      this.setState({ frButtonDetails: newObj });
    }
  };

  modifyEnPopupTitle = (inputName, value) => {
    // allow maximum of 150 chars
    const regexExpression = /^(.{0,150})$/;
    if (regexExpression.test(value)) {
      const newObj = { ...this.props.enButtonDetails, ...this.state.enButtonDetails };
      newObj[`${inputName}`] = value;
      this.setState({ enButtonDetails: newObj });
    }
  };

  modifyFrPopupTitle = (inputName, value) => {
    // allow maximum of 150 chars
    const regexExpression = /^(.{0,150})$/;
    if (regexExpression.test(value)) {
      const newObj = { ...this.props.frButtonDetails, ...this.state.frButtonDetails };
      newObj[`${inputName}`] = value;
      this.setState({ frButtonDetails: newObj });
    }
  };

  modifyEnPopupContent = (inputName, value) => {
    // allow maximum of 500 chars
    if (value.length <= 500) {
      const newObj = { ...this.props.enButtonDetails, ...this.state.enButtonDetails };
      newObj[`${inputName}`] = value;
      this.setState({ enButtonDetails: newObj });
    }
  };

  modifyFrPopupContent = (inputName, value) => {
    // allow maximum of 500 chars
    if (value.length <= 500) {
      const newObj = { ...this.props.frButtonDetails, ...this.state.frButtonDetails };
      newObj[`${inputName}`] = value;
      this.setState({ frButtonDetails: newObj });
    }
  };

  handleSave = () => {
    // validating the next section button button type
    // initializing isValidTestNextSectionButtonType
    let isValidTestNextSectionButtonType = true;
    // if test section type is FINISH (3) or QUIT (4)
    if (
      this.props.testSectionType === TestSectionType.FINISH ||
      this.props.testSectionType === TestSectionType.QUIT
    ) {
      // validating the next section button type (needs to be NONE to be valid)
      isValidTestNextSectionButtonType =
        this.state.next_section_button_type.value === NextSectionButtonTypeObject.NONE.value;
      // if test section type is TOP_TABS (1) or SINGLE_COMPONENT (2)
    } else {
      isValidTestNextSectionButtonType =
        this.state.next_section_button_type.value !== NextSectionButtonTypeObject.NONE.value;
    }
    // updating isValidTestNextSectionButtonType state
    this.setState({ isValidTestNextSectionButtonType: isValidTestNextSectionButtonType }, () => {
      if (allValid(this.state)) {
        const enButton = {
          ...this.props.enButtonDetails,
          ...this.state.enButtonDetails,
          next_section_button_type: this.state.next_section_button_type.value
        };
        if (this.props.enButtonDetails.id === undefined) {
          enButton.id = getNextInNumberSeries(this.props.nextSectionButtons, "id");
        }
        this.props.modifyTestDefinitionField(NEXT_SECTION_BUTTONS, enButton, enButton.id);

        const frButton = {
          ...this.props.frButtonDetails,
          ...this.state.frButtonDetails,
          next_section_button_type: this.state.next_section_button_type.value
        };

        if (this.props.frButtonDetails.id === undefined) {
          frButton.id = getNextInNumberSeries(this.props.nextSectionButtons, "id") + 1;
        }
        this.props.modifyTestDefinitionField(NEXT_SECTION_BUTTONS, frButton, frButton.id);

        // call parent save function
        this.props.handleSave(this.state.next_section_button_type.value);
      }
    });
  };

  openDialog = () => {
    this.setState({ showDialog: true });
  };

  closeDialog = () => {
    this.setState({ showDialog: false });
  };

  confirmDelete = () => {
    this.props.handleDelete();
    // this.props.expandItem();
    this.closeDialog();
  };

  onSelectChange = (event, action) => {
    this.setState({ next_section_button_type: event });
  };

  modifyField = (inputName, value) => {
    const newObj = { ...this.props.frButtonDetails, ...this.state.frButtonDetails };
    const newObj2 = { ...this.props.enButtonDetails, ...this.state.enButtonDetails };
    newObj[`${inputName}`] = value;
    newObj2[`${inputName}`] = value;
    this.setState({ frButtonDetails: newObj, enButtonDetails: newObj2 });
  };

  // getting switch dimensions and updating states
  getUpdatedSwitchDimensions = () => {
    // need this loading state, otherwise the switch diameter is not calculated on state change
    this.setState({ areSwitchStatesLoading: true }, () => {
      this.setState(
        {
          switchHeight: getSwitchTransformScale(this.props.accommodations.fontSize).height,
          switchWidth: getSwitchTransformScale(this.props.accommodations.fontSize).width
        },
        () => {
          this.setState({ areSwitchStatesLoading: false });
        }
      );
    });
  };

  handleEnableConfirmProceed = () => {
    this.modifyField("confirm_proceed", true);
  };

  handleDisableConfirmProceed = () => {
    this.modifyField("confirm_proceed", false);
  };

  getNextSectionButtonTypeOptions = () => {
    // initializing customOptions
    let customOptions = options;
    // getting section type
    const { testSectionType } = this.props;
    // section type is FINISH (3) or QUIT (4)
    if (testSectionType === TestSectionType.FINISH || testSectionType === TestSectionType.QUIT) {
      customOptions = options.filter(obj => obj.value === NextSectionButtonType.NONE);
      // section type is TOP_TABS (1) or SINGLE_COMPONENT (2)
    } else {
      customOptions = options.filter(obj => obj.value !== NextSectionButtonType.NONE);
    }
    // returning the custom options
    return customOptions;
  };

  getLanguageTabs = () => {
    const enButtonDetails = {
      ...this.props.enButtonDetails,
      ...this.state.enButtonDetails
    };
    const frButtonDetails = {
      ...this.props.frButtonDetails,
      ...this.state.frButtonDetails
    };
    const frPageSectionTypeContent = (
      <>
        <Row style={styles.rowStyle}>
          {makeLabel(
            "button_text",
            LOCALIZE.testBuilder.nextSectionButton,
            "next-section-button-fr-button-text"
          )}
          {makeTextBoxField(
            "button_text",
            LOCALIZE.testBuilder.nextSectionButton,
            frButtonDetails.button_text,
            true,
            this.modifyFrButtonText,
            "next-section-button-fr-button-text"
          )}
        </Row>
        {this.state.next_section_button_type === NextSectionButtonTypeObject.POPUP && (
          <>
            <Row style={styles.rowStyle}>
              {makeLabel(
                "title",
                LOCALIZE.testBuilder.nextSectionButton,
                "next-section-button-fr-popup-title"
              )}
              {makeTextBoxField(
                "title",
                LOCALIZE.testBuilder.nextSectionButton,
                frButtonDetails.title,
                true,
                this.modifyFrPopupTitle,
                "next-section-button-fr-popup-title"
              )}
            </Row>
            <Row style={styles.rowStyle}>
              {makeLabel(
                "confirm_proceed",
                LOCALIZE.testBuilder.nextSectionButton,
                "next-section-button-fr-popup-confirm_proceed"
              )}
              <Col>
                <div style={styles.rightColumnContainer}>
                  {!this.state.areSwitchStatesLoading && (
                    <Switch
                      onChange={
                        frButtonDetails.confirm_proceed
                          ? this.handleDisableConfirmProceed
                          : this.handleEnableConfirmProceed
                      }
                      checked={frButtonDetails.confirm_proceed}
                      aria-labelledby="next-section-button-fr-popup-confirm_proceed"
                      height={this.state.switchHeight}
                      width={this.state.switchWidth}
                      className="switch-custom-style"
                    />
                  )}
                </div>
              </Col>
            </Row>
            <Row style={styles.rowStyle}>
              {makeLabel(
                "content",
                LOCALIZE.testBuilder.nextSectionButton,
                "next-section-button-fr-content"
              )}
            </Row>
            <Row style={styles.rowStyle}>
              {makeTextAreaField(
                "content",
                LOCALIZE.testBuilder.nextSectionButton,
                frButtonDetails.content,
                true,
                this.modifyFrPopupContent,
                "next-section-button-fr-popup-content"
              )}
            </Row>
          </>
        )}
      </>
    );
    const enPageSectionTypeContent = (
      <>
        <Row style={styles.rowStyle}>
          {makeLabel(
            "button_text",
            LOCALIZE.testBuilder.nextSectionButton,
            "next-section-button-en-button-text"
          )}
          {makeTextBoxField(
            "button_text",
            LOCALIZE.testBuilder.nextSectionButton,
            enButtonDetails.button_text,
            true,
            this.modifyEnButtonText,
            "next-section-button-en-button-text"
          )}
        </Row>
        {this.state.next_section_button_type === NextSectionButtonTypeObject.POPUP && (
          <>
            <Row style={styles.rowStyle}>
              {makeLabel(
                "title",
                LOCALIZE.testBuilder.nextSectionButton,
                "next-section-button-en-popup-title"
              )}
              {makeTextBoxField(
                "title",
                LOCALIZE.testBuilder.nextSectionButton,
                enButtonDetails.title,
                true,
                this.modifyEnPopupTitle,
                "next-section-button-en-popup-title"
              )}
            </Row>
            <Row style={styles.rowStyle}>
              {makeLabel(
                "confirm_proceed",
                LOCALIZE.testBuilder.nextSectionButton,
                "next-section-button-en-popup-confirm_proceed"
              )}
              <Col>
                <div style={styles.rightColumnContainer}>
                  {!this.state.areSwitchStatesLoading && (
                    <Switch
                      onChange={
                        frButtonDetails.confirm_proceed
                          ? this.handleDisableConfirmProceed
                          : this.handleEnableConfirmProceed
                      }
                      checked={frButtonDetails.confirm_proceed}
                      aria-labelledby="next-section-button-en-popup-confirm_proceed"
                      height={this.state.switchHeight}
                      width={this.state.switchWidth}
                      className="switch-custom-style"
                    />
                  )}
                </div>
              </Col>
            </Row>
            <Row style={styles.rowStyle}>
              {makeLabel(
                "content",
                LOCALIZE.testBuilder.nextSectionButton,
                "next-section-button-en-content"
              )}
            </Row>
            <Row style={styles.rowStyle}>
              {makeTextAreaField(
                "content",
                LOCALIZE.testBuilder.nextSectionButton,
                enButtonDetails.content,
                true,
                this.modifyEnPopupContent,
                "next-section-button-en-popup-content"
              )}
            </Row>
          </>
        )}
      </>
    );

    const TABS = [
      {
        key: LOCALIZE.commons.english,
        tabName: LOCALIZE.commons.english,
        body: enPageSectionTypeContent
      },
      {
        key: LOCALIZE.commons.french,
        tabName: LOCALIZE.commons.french,
        body: frPageSectionTypeContent
      }
    ];

    return TABS;
  };

  render() {
    const TABS = this.getLanguageTabs();

    // getting available button type options
    const buttonTypeOptions = this.getNextSectionButtonTypeOptions();

    return (
      <>
        <h4>{LOCALIZE.testBuilder.nextSectionButton.header}</h4>
        <Row style={styles.rowStyle}>
          {makeLabel("button_type", LOCALIZE.testBuilder.nextSectionButton)}
          {makeDropDownField(
            "button_type",
            LOCALIZE.testBuilder.nextSectionButton,
            this.state.next_section_button_type,
            buttonTypeOptions,
            this.onSelectChange,
            false,
            this.state.isValidTestNextSectionButtonType
          )}
        </Row>

        {this.state.next_section_button_type.value !== NextSectionButtonTypeObject.NONE.value && (
          <TopTabs TABS={TABS} defaultTab={TABS[0].key} activeKey={TABS[0].key} />
        )}

        <Row className="justify-content-between" style={styles.buttonRowStyle}>
          {makeSaveDeleteButtons(
            this.handleSave,
            this.openDialog,
            LOCALIZE.commons.applyButton,
            LOCALIZE.commons.deleteButton
          )}
        </Row>
        <PopupBox
          show={this.state.showDialog}
          handleClose={this.closeDialog}
          title={LOCALIZE.testBuilder.testSection.deletePopup.title}
          description={<div>{this.props.deletePopupContent}</div>}
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={LOCALIZE.commons.deleteButton}
          rightButtonAction={this.confirmDelete}
        />
      </>
    );
  }
}

export { NextSectionButtonForm as unconnectedNextSectionButtonForm };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    nextSectionButtons: state.testBuilder.next_section_buttons
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ modifyTestDefinitionField, replaceTestDefinitionField }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(NextSectionButtonForm);
