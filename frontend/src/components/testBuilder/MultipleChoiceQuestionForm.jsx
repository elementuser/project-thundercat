import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Row, Button } from "react-bootstrap";
import {
  getNextInNumberSeries,
  makeSaveDeleteButtons,
  allValid,
  testLanguageHasEnglish,
  testLanguageHasFrench,
  collapsingItemMoveUpFunctionality,
  collapsingItemMoveDownFunctionality
} from "./helpers";
import {
  modifyTestDefinitionField,
  addTestDefinitionField,
  ANSWERS,
  ANSWER_DETAILS,
  QUESTION_SECTIONS,
  QUESTION_SECTION_DEFINITIONS,
  replaceTestDefinitionArray,
  replaceTestDefinitionField
} from "../../modules/TestBuilderRedux";
import { styles } from "./TestSectionForm";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import QuestionSectionForm from "./QuestionSectionForm";
import CollapsingItemContainer from "../eMIB/CollapsingItemContainer";
import AnswerForm from "./AnswerForm";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlusCircle } from "@fortawesome/free-solid-svg-icons";
import { LANGUAGES } from "../commons/Translation";
import { QuestionSectionType } from "../testFactory/Constants";
import { removeHtmlFromString } from "../commons/reports/utils";

const componentStyles = {
  boldText: {
    fontWeight: "bold"
  },
  nonBoldText: {
    fontWeight: "normal"
  }
};

class MultipleChoiceQuestionForm extends Component {
  state = {
    showDialog: false,
    question: {}
  };

  componentDidMount = () => {
    // getting orders
    this.getQuestionSectionsOrders();
    this.getQuestionAnswersOrders();
  };

  componentDidUpdate = prevProps => {
    // if length of testSectionComponents gets updated
    if (
      prevProps.questionDetails.questionSections.length !==
      this.props.questionDetails.questionSections.length
    ) {
      // getting orders
      this.getQuestionSectionsOrders();
    }
    // if length of answers gets updated
    if (prevProps.answers.length !== this.props.answers.length) {
      // getting orders
      this.getQuestionAnswersOrders();
    }
    // if test definition field gets updated
    if (
      prevProps.triggerTestDefinitionFieldUpdates !== this.props.triggerTestDefinitionFieldUpdates
    ) {
      // getting orders
      this.getQuestionSectionsOrders();
      this.getQuestionAnswersOrders();
    }
  };

  addAnswer = () => {
    const { testLanguage, question } = this.props;

    const newAnswer = {
      scoring_value: 0,
      ppc_answer_id: "",
      question: question.id,
      order: 1
    };
    newAnswer.id = getNextInNumberSeries(this.props.answers, "id");
    this.props.addTestDefinitionField(ANSWERS, newAnswer, newAnswer.id);

    // create test language answer details stuff
    let newAnswerDetails = {};
    if (testLanguageHasEnglish(testLanguage)) {
      newAnswerDetails = {
        content: "Temp",
        answer: newAnswer.id,
        language: LANGUAGES.english
      };
      newAnswerDetails.id = getNextInNumberSeries(this.props.answerDetails, "id");
      this.props.addTestDefinitionField(ANSWER_DETAILS, newAnswerDetails, newAnswerDetails.id);
    }
    if (testLanguageHasFrench(testLanguage)) {
      newAnswerDetails = {
        content: "Temp",
        answer: newAnswer.id,
        language: LANGUAGES.french
      };
      newAnswerDetails.id = getNextInNumberSeries(this.props.answerDetails, "id") + 1;
      this.props.addTestDefinitionField(ANSWER_DETAILS, newAnswerDetails, newAnswerDetails.id);
    }
    // getting orders
    this.getQuestionAnswersOrders();
  };

  addQuestionSection = () => {
    const { testLanguage, question } = this.props;
    const newQuestionSection = {
      question_section_type: QuestionSectionType.MARKDOWN,
      question: question.id
    };
    newQuestionSection.id = getNextInNumberSeries(this.props.questionSections, "id");
    newQuestionSection.order = getNextInNumberSeries(
      this.props.questionSections.filter(obj => obj.question === question.id),
      "order"
    );
    this.props.addTestDefinitionField(QUESTION_SECTIONS, newQuestionSection, newQuestionSection.id);

    // create test language answer details stuff
    let newQuestionSectionDefinition = {};
    if (testLanguageHasEnglish(testLanguage)) {
      newQuestionSectionDefinition = {
        content: "Temp",
        question_section: newQuestionSection.id,
        language: LANGUAGES.english,
        question_section_type: QuestionSectionType.MARKDOWN
      };
      newQuestionSectionDefinition.id = getNextInNumberSeries(
        this.props.questionSectionDefinitions,
        "id"
      );
      this.props.addTestDefinitionField(
        QUESTION_SECTION_DEFINITIONS,
        newQuestionSectionDefinition,
        newQuestionSectionDefinition.id
      );
    }
    if (testLanguageHasFrench(testLanguage)) {
      newQuestionSectionDefinition = {
        content: "FR Temp",
        question_section: newQuestionSection.id,
        language: LANGUAGES.french,
        question_section_type: QuestionSectionType.MARKDOWN
      };
      newQuestionSectionDefinition.id =
        getNextInNumberSeries(this.props.questionSectionDefinitions, "id") + 1;
      this.props.addTestDefinitionField(
        QUESTION_SECTION_DEFINITIONS,
        newQuestionSectionDefinition,
        newQuestionSectionDefinition.id
      );
    }
    // getting orders
    this.getQuestionSectionsOrders();
  };

  handleSave = () => {
    if (allValid(this.state)) {
      // call parents handle save
      this.props.handleSave();
    }
  };

  openDialog = () => {
    this.setState({ showDialog: true });
  };

  closeDialog = () => {
    this.setState({ showDialog: false });
  };

  confirmDelete = () => {
    this.props.handleDelete();
    this.closeDialog();
  };

  handleDuplicateQuestion = () => {
    if (allValid(this.state)) {
      // handle duplicate
      this.props.handleDuplicate();
    }
  };

  handlePreviewQuestion = () => {
    // handle preview
    this.props.handlePreview();
  };

  selectedOption = field => {
    const fieldVar = Array.isArray(field) ? field : [field];
    const array = [];
    for (const contactId of fieldVar) {
      for (const contact of this.props.addressBook) {
        if (contactId === contact.id) {
          array.push({ label: contact.name, value: contact.id });
        }
      }
    }
    return array;
  };

  handleMoveQuestionSectionUp = index => {
    // getting all question sections ordered data
    const allQuestionSectionsOrderedData = this.getAllQuestionSectionsOrderedData(index);
    // getting new question sections array
    collapsingItemMoveUpFunctionality(index, allQuestionSectionsOrderedData.currentQuestionSecions)
      .then(objArray => {
        // creating final array
        const final_array = objArray.concat(allQuestionSectionsOrderedData.otherQuestionSections);
        // updating redux states
        this.props.replaceTestDefinitionArray(QUESTION_SECTIONS, final_array);
      })
      .then(() => {
        // getting orders
        this.getQuestionSectionsOrders();
      });
  };

  handleMoveQuestionSectionDown = index => {
    // getting all question sections ordered data
    const allQuestionSectionsOrderedData = this.getAllQuestionSectionsOrderedData(index);
    // getting new question sections array
    collapsingItemMoveDownFunctionality(
      index,
      allQuestionSectionsOrderedData.currentQuestionSecions
    )
      .then(objArray => {
        // creating final array
        const final_array = objArray.concat(allQuestionSectionsOrderedData.otherQuestionSections);
        // updating redux states
        this.props.replaceTestDefinitionArray(QUESTION_SECTIONS, final_array);
      })
      .then(() => {
        // getting orders
        this.getQuestionSectionsOrders();
      });
  };

  getQuestionSectionsOrders = () => {
    // initializing props variables
    const { questionSections } = this.props;

    // initializing newQuestionSections array
    const newQuestionSections = [];

    // grouping array by question
    // source: https://edisondevadoss.medium.com/javascript-group-an-array-of-objects-by-key-afc85c35d07e
    const grouped_by_question = questionSections.reduce((r, a) => {
      // eslint-disable-next-line no-param-reassign
      r[a.question] = [...(r[a.question] || []), a];
      return r;
    }, {});
    const grouped_by_question_array = Object.keys(grouped_by_question).map(key => [
      grouped_by_question[key]
    ]);

    // looping in grouped_by_question_array
    for (let i = 0; i < grouped_by_question_array.length; i++) {
      // looping in array of current grouped_by_question_array
      for (let j = 0; j < grouped_by_question_array[i][0].length; j++) {
        // pushing new object (with the right order) in newQuestions array
        newQuestionSections.push({
          id: grouped_by_question_array[i][0][j].id,
          question: grouped_by_question_array[i][0][j].question,
          question_section_type: grouped_by_question_array[i][0][j].question_section_type,
          order: j + 1
        });
      }
    }
    // updating redux state
    this.props.replaceTestDefinitionArray(QUESTION_SECTIONS, newQuestionSections);
  };

  getAllQuestionSectionsOrderedData = index => {
    // initializing needed arrays
    const currentQuestionSecions = [];
    const otherQuestionSections = [];

    // looping in all question sections
    for (let i = 0; i < this.props.questionSections.length; i++) {
      // pushing current question section to currentQuestionSecions array
      if (
        this.props.questionDetails.questionSections[index].question ===
        this.props.questionSections[i].question
      ) {
        currentQuestionSecions.push(this.props.questionSections[i]);
        // pushing other question section to otherQuestionSections array
      } else {
        otherQuestionSections.push(this.props.questionSections[i]);
      }
    }
    return {
      currentQuestionSecions: currentQuestionSecions,
      otherQuestionSections: otherQuestionSections
    };
  };

  handleMoveQuestionAnswerUp = index => {
    // getting all question answers ordered data
    const allQuestionAnswersOrderedData = this.getAllQuestionAnswersOrderedData(index);
    // getting new question answers array
    collapsingItemMoveUpFunctionality(index, allQuestionAnswersOrderedData.currentQuestionAnswer)
      .then(objArray => {
        // creating final array
        const final_array = objArray.concat(allQuestionAnswersOrderedData.otherQuestionAnswer);
        // updating redux states
        this.props.replaceTestDefinitionField(ANSWERS, final_array);
      })
      .then(() => {
        // getting orders
        this.getQuestionAnswersOrders();
      });
  };

  handleMoveQuestionAnswerDown = index => {
    // getting all question answers ordered data
    const allQuestionAnswersOrderedData = this.getAllQuestionAnswersOrderedData(index);
    // getting new question answers array
    collapsingItemMoveDownFunctionality(index, allQuestionAnswersOrderedData.currentQuestionAnswer)
      .then(objArray => {
        // creating final array
        const final_array = objArray.concat(allQuestionAnswersOrderedData.otherQuestionAnswer);
        // updating redux states
        this.props.replaceTestDefinitionField(ANSWERS, final_array);
      })
      .then(() => {
        // getting orders
        this.getQuestionAnswersOrders();
      });
  };

  getQuestionAnswersOrders = () => {
    // initializing props variables
    const { answers } = this.props;

    // initializing newQuestionAnswers array
    const newQuestionAnswers = [];

    // grouping array by question
    // source: https://edisondevadoss.medium.com/javascript-group-an-array-of-objects-by-key-afc85c35d07e
    const grouped_by_question = answers.reduce((r, a) => {
      // eslint-disable-next-line no-param-reassign
      r[a.question] = [...(r[a.question] || []), a];
      return r;
    }, {});
    const grouped_by_question_array = Object.keys(grouped_by_question).map(key => [
      grouped_by_question[key]
    ]);

    // looping in grouped_by_question_array
    for (let i = 0; i < grouped_by_question_array.length; i++) {
      // looping in array of current grouped_by_question_array
      for (let j = 0; j < grouped_by_question_array[i][0].length; j++) {
        // pushing new object (with the right order) in newQuestions array
        newQuestionAnswers.push({
          id: grouped_by_question_array[i][0][j].id,
          question: grouped_by_question_array[i][0][j].question,
          scoring_value: grouped_by_question_array[i][0][j].scoring_value,
          order: j + 1,
          ppc_answer_id: grouped_by_question_array[i][0][j].ppc_answer_id
        });
      }
    }
    // updating redux state
    this.props.replaceTestDefinitionArray(ANSWERS, newQuestionAnswers);
  };

  getAllQuestionAnswersOrderedData = index => {
    // initializing needed arrays
    const questionAnswersRelatedToCurrentQuestion = [];
    const currentQuestionAnswer = [];
    const otherQuestionAnswer = [];

    // building questionAnswersRelatedToCurrentQuestion array
    for (let i = 0; i < this.props.answers.length; i++) {
      if (this.props.answers[i].question === this.props.question.id) {
        questionAnswersRelatedToCurrentQuestion.push(this.props.answers[i]);
      }
    }

    // looping in all question answers
    for (let i = 0; i < this.props.answers.length; i++) {
      // pushing current answers to currentQuestionAnswer array
      if (
        questionAnswersRelatedToCurrentQuestion[index].question === this.props.answers[i].question
      ) {
        currentQuestionAnswer.push(this.props.answers[i]);
        // pushing other question answers to otherQuestionAnswer array
      } else {
        otherQuestionAnswer.push(this.props.answers[i]);
      }
    }
    return {
      currentQuestionAnswer: currentQuestionAnswer,
      otherQuestionAnswer: otherQuestionAnswer
    };
  };

  getAnswerText = (enDefinition, frDefinition) => {
    // initializing answerText
    let answerText = LOCALIZE.commons.none;
    // current language is English
    if (this.props.currentLanguage === LANGUAGES.english) {
      // prioritizing English display (if possible)
      // checking if enDefinition is defined
      if (Object.entries(enDefinition).length > 0) {
        // making sure that there is text content
        if (enDefinition.content !== "") {
          // getting first 15 chars of answer
          answerText = `${removeHtmlFromString(enDefinition.content).substring(0, 15)}...`;
        }
        // checking if frDefinition is defined
      } else if (Object.entries(frDefinition).length > 0) {
        // making sure that there is text content
        if (frDefinition.content !== "") {
          // getting first 15 chars of answer
          answerText = `${removeHtmlFromString(frDefinition.content).substring(0, 15)}...`;
        }
      }
      // current language is French
    } else {
      // prioritizing French display (if possible)
      // checking if frDefinition is defined
      if (Object.entries(frDefinition).length > 0) {
        // making sure that there is text content
        if (frDefinition.content !== "") {
          // getting first 15 chars of answer
          answerText = `${removeHtmlFromString(frDefinition.content).substring(0, 15)}...`;
        }
        // checking if enDefinition is defined
      } else if (Object.entries(enDefinition).length > 0) {
        // making sure that there is text content
        if (enDefinition.content !== "") {
          // getting first 15 chars of answer
          answerText = `${removeHtmlFromString(frDefinition.content).substring(0, 15)}...`;
        }
      }
    }

    return answerText;
  };

  render() {
    return (
      <div>
        <h4>{LOCALIZE.testBuilder.questionSections.title}</h4>
        {this.props.questionDetails.questionSections.map((questionSection, index) => {
          let enDefinition = {};
          let frDefinition = {};
          for (const definition of this.props.questionDetails.questionSectionDefinitions) {
            if (
              definition.question_section === questionSection.id &&
              definition.language === LANGUAGES.english
            ) {
              enDefinition = definition;
            }
            if (
              definition.question_section === questionSection.id &&
              definition.language === LANGUAGES.french
            ) {
              frDefinition = definition;
            }
          }

          return (
            <CollapsingItemContainer
              key={index}
              index={index}
              usesMoveUpAndDownFunctionalities={true}
              moveUpAction={() => this.handleMoveQuestionSectionUp(index)}
              moveUpButtonDisabled={questionSection.order === 1}
              moveDownAction={() => this.handleMoveQuestionSectionDown(index)}
              moveDownButtonDisabled={
                index === this.props.questionDetails.questionSections.length - 1
                  ? true
                  : this.props.questionDetails.questionSections[index + 1].order <=
                    questionSection.order
              }
              title={
                <label>
                  {LOCALIZE.formatString(
                    LOCALIZE.testBuilder.questionSections.collapsableItemName,
                    questionSection.order
                  )}
                </label>
              }
              body={
                <QuestionSectionForm
                  question={this.props.question}
                  questionSection={questionSection}
                  frDefinition={frDefinition}
                  enDefinition={enDefinition}
                  testLanguage={this.props.testLanguage}
                />
              }
            />
          );
        })}
        <Button variant={"primary"} onClick={this.addQuestionSection}>
          <FontAwesomeIcon icon={faPlusCircle} style={{ margins: "auto", marginRight: "5px" }} />
          {LOCALIZE.testBuilder.questionSections.addButton}
        </Button>

        <h4>{LOCALIZE.testBuilder.answers.title}</h4>
        {this.props.questionDetails.answers.map((answer, index) => {
          let enDefinition = {};
          let frDefinition = {};
          for (const definition of this.props.questionDetails.answerDetails) {
            if (definition.answer === answer.id && definition.language === LANGUAGES.english) {
              enDefinition = definition;
            }
            if (definition.answer === answer.id && definition.language === LANGUAGES.french) {
              frDefinition = definition;
            }
          }

          // getting answers related to current selected question
          const currentAnswers = [];
          for (let i = 0; i < this.props.answers.length; i++) {
            if (this.props.answers[i].question === answer.question) {
              currentAnswers.push(this.props.answers[i]);
            }
          }

          return (
            <CollapsingItemContainer
              key={index}
              index={index}
              usesMoveUpAndDownFunctionalities={true}
              moveUpAction={() => this.handleMoveQuestionAnswerUp(index)}
              moveUpButtonDisabled={answer.order === 1}
              moveDownAction={() => this.handleMoveQuestionAnswerDown(index)}
              moveDownButtonDisabled={
                index === this.props.answers.length - 1
                  ? true
                  : index === currentAnswers.length - 1
                  ? true
                  : currentAnswers[index + 1].order <= answer.order
              }
              title={
                <label style={componentStyles.boldText}>
                  <span>{`${answer.order}. ${LOCALIZE.testBuilder.answers.collapsableItemNames.id}`}</span>{" "}
                  <span style={componentStyles.nonBoldText}>
                    {answer.ppc_answer_id !== "" && typeof answer.ppc_answer_id !== "undefined"
                      ? answer.ppc_answer_id
                      : LOCALIZE.commons.none}{" "}
                  </span>
                  <span>{LOCALIZE.testBuilder.answers.collapsableItemNames.value} </span>
                  <span style={componentStyles.nonBoldText}>{answer.scoring_value} </span>
                  <span>{LOCALIZE.testBuilder.answers.collapsableItemNames.text} </span>
                  <span style={componentStyles.nonBoldText}>
                    {this.getAnswerText(enDefinition, frDefinition)}
                  </span>
                </label>
              }
              body={
                <AnswerForm
                  question={this.props.question}
                  answer={answer}
                  frDefinition={frDefinition}
                  enDefinition={enDefinition}
                  testLanguage={this.props.testLanguage}
                />
              }
            />
          );
        })}
        <Button variant={"primary"} onClick={this.addAnswer}>
          <FontAwesomeIcon icon={faPlusCircle} style={{ margins: "auto", marginRight: "5px" }} />
          {LOCALIZE.testBuilder.answers.addButton}
        </Button>

        <Row className="justify-content-between" style={styles.buttonRowStyle}>
          {makeSaveDeleteButtons(
            this.handleSave,
            this.openDialog,
            LOCALIZE.commons.applyButton,
            LOCALIZE.commons.deleteButton,
            null,
            true,
            LOCALIZE.commons.duplicateButton,
            this.handleDuplicateQuestion,
            true,
            LOCALIZE.commons.previewButton,
            this.handlePreviewQuestion
          )}
        </Row>
        <PopupBox
          show={this.state.showDialog}
          handleClose={this.closeDialog}
          title={"Delete Confirmation"}
          description={
            <div>
              <p>
                Deleting this item will delete all the content displayed. Are you sure you want to
                delete this object?
              </p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={LOCALIZE.commons.deleteButton}
          rightButtonAction={this.confirmDelete}
        />
      </div>
    );
  }
}

export { MultipleChoiceQuestionForm as unconnectedMultipleChoiceQuestionForm };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    answers: state.testBuilder.answers,
    answerDetails: state.testBuilder.answer_details,
    questionSections: state.testBuilder.question_sections,
    questionSectionDefinitions: state.testBuilder.question_section_definitions,
    triggerTestDefinitionFieldUpdates: state.testBuilder.triggerTestDefinitionFieldUpdates
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      modifyTestDefinitionField,
      addTestDefinitionField,
      replaceTestDefinitionArray,
      replaceTestDefinitionField
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(MultipleChoiceQuestionForm);
