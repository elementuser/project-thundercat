import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Row } from "react-bootstrap";
import { makeLabel, makeTextBoxField, makeDropDownField, allValid } from "./helpers";
import {
  modifyTestDefinitionField,
  replaceTestDefinitionField,
  QUESTION_SECTIONS,
  QUESTION_SECTION_DEFINITIONS
} from "../../modules/TestBuilderRedux";
import { styles } from "./TestSectionForm";
import { QuestionSectionType, QuestionSectionTypeObject } from "../testFactory/Constants";
import MarkdownQuestionSectionForm from "./MarkdownQuestionSectionForm";

const typeOptions = Object.getOwnPropertyNames(QuestionSectionTypeObject).map(property => {
  return QuestionSectionTypeObject[property];
});

class QuestionSectionForm extends Component {
  state = {
    question: {},
    questionSection: {}
  };

  selectedOption = questionSection => {
    for (const property of Object.getOwnPropertyNames(QuestionSectionType)) {
      if (questionSection.question_section_type === QuestionSectionType[`${property}`]) {
        return { label: property, value: questionSection.question_section_type };
      }
    }
    return typeOptions[0];
  };

  modifyQuestionSection = (inputName, value) => {
    const newObj = { ...this.props.questionSection, ...this.state.questionSection };
    newObj[`${inputName}`] = value;
    this.setState({ questionSection: newObj });
  };

  onSelectChange = (event, action) => {
    this.modifyQuestionSection(action.name, event.value);
  };

  handleSave = sectionType => {
    const questionSection = {
      ...this.props.questionSection,
      ...this.state.questionSection,
      question_section_type: sectionType
    };
    if (allValid(this.state)) {
      this.props.modifyTestDefinitionField(QUESTION_SECTIONS, questionSection, questionSection.id);
    }
  };

  confirmDelete = () => {
    // delete the question section where id and type are equal (some ids can be the same due to different obj in one array)
    const { id } = this.props.questionSection;
    const type = this.props.questionSection.question_section_type;
    let objArray = this.props.questionSections.filter(obj => {
      return obj.id !== id || obj.question_section_type !== type;
    });
    this.props.replaceTestDefinitionField(QUESTION_SECTIONS, objArray);

    // delete the answer details where id and type are equal (some ids can be the same due to different obj in one array)
    objArray = this.props.questionSectionDefinitions.filter(obj => {
      return obj.question_section !== id || obj.question_section_type !== type;
    });
    this.props.replaceTestDefinitionField(QUESTION_SECTION_DEFINITIONS, objArray);

    this.props.expandItem();
  };

  render() {
    const questionSection = { ...this.props.questionSection, ...this.state.questionSection };
    const { enDefinition, frDefinition } = this.props;
    const selected = this.selectedOption(questionSection);
    const selectedValue = questionSection.question_section_type;

    return (
      <div style={styles.mainContiner}>
        <Row style={styles.rowStyle}>
          {makeLabel("order", LOCALIZE.testBuilder.questionSections)}
          {makeTextBoxField(
            "order",
            LOCALIZE.testBuilder.questionSections,
            questionSection.order,
            true,
            () => {},
            "question-sections-order",
            true
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("question_section_type", LOCALIZE.testBuilder.questionSections)}
          {makeDropDownField(
            "question_section_type",
            LOCALIZE.testBuilder.questionSections,
            selected,
            typeOptions,
            this.onSelectChange
          )}
        </Row>

        {selectedValue === QuestionSectionTypeObject.MARKDOWN.value && (
          <MarkdownQuestionSectionForm
            question={this.props.question}
            questionSection={questionSection}
            frDefinition={frDefinition}
            enDefinition={enDefinition}
            handleSave={this.handleSave}
            handleDelete={this.confirmDelete}
            testLanguage={this.props.testLanguage}
          />
        )}
      </div>
    );
  }
}

export { QuestionSectionForm as unconnectedQuestionSectionForm };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    questionSections: state.testBuilder.question_sections,
    questionSectionDefinitions: state.testBuilder.question_section_definitions
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ modifyTestDefinitionField, replaceTestDefinitionField }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(QuestionSectionForm);
