import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import {
  getTestDefinitionData,
  updateSearchArchivedTestCodesDefinitionsStates,
  updateCurrentTestDefinitionArchivedTestCodesPageState,
  updateCurrentTestDefinitionArchivedTestCodesPageSizeState,
  archiveTestDefinitionTestCode,
  triggerSelectTestDefinitionTablesRefresh
} from "../../modules/TestBuilderRedux";
import GenericTable, { COMMON_STYLE } from "../commons/GenericTable";
import CustomButton, { THEME } from "../commons/CustomButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBoxOpen, faTimes } from "@fortawesome/free-solid-svg-icons";
import PopupBox, { BUTTON_TYPE, BUTTON_STATE } from "../commons/PopupBox";
import SearchBarWithDisplayOptions from "../commons/SearchBarWithDisplayOptions";
import Pagination from "../commons/Pagination";

const styles = {
  tableContainer: {
    margin: 24
  },
  buttonIcon: {
    marginRight: 6
  },
  actionButton: {
    minWidth: 100
  },
  popupCustomStyle: {
    minWidth: 850
  },
  popupTestInfoContainer: {
    margin: "12px 0"
  },
  popupTestInfoItem: {
    display: "table",
    width: "100%",
    margin: "0 36px"
  },
  popupTestInfoLeftColumn: {
    display: "table-cell",
    width: "30%",
    fontWeight: "bold"
  },
  popupTestInfoRightColumn: {
    display: "table-cell",
    width: "70%"
  },
  selectTestVersionDropdownContainer: {
    margin: "18px 0"
  },
  bold: {
    fontWeight: "bold"
  }
};

class ArchivedTestCodes extends Component {
  state = {
    currentlyLoading: false,
    rowsDefinition: {},
    testDefinitionVersionOptions: [],
    numberOfTestDefinitionArchivedTestCodesPages: 1,
    resultsFound: 0,
    showRestoreConfirmationPopup: false,
    currentTestDefinitionData: {},
    restoreProcessLoading: false
  };

  componentDidMount = () => {
    this.getTestDefinitionData();
  };

  componentDidUpdate = prevProps => {
    // if testDefinitionArchivedTestCodesPaginationPageSize get updated
    if (
      prevProps.testDefinitionArchivedTestCodesPaginationPageSize !==
      this.props.testDefinitionArchivedTestCodesPaginationPageSize
    ) {
      this.getTestDefinitionDataBasedOnActiveSearch();
    }
    // if testDefinitionArchivedTestCodesPaginationPage get updated
    if (
      prevProps.testDefinitionArchivedTestCodesPaginationPage !==
      this.props.testDefinitionArchivedTestCodesPaginationPage
    ) {
      this.getTestDefinitionDataBasedOnActiveSearch();
    }
    // if search testDefinitionArchivedTestCodesKeyword gets updated
    if (
      prevProps.testDefinitionArchivedTestCodesKeyword !==
      this.props.testDefinitionArchivedTestCodesKeyword
    ) {
      // if testDefinitionArchivedTestCodesKeyword redux state is empty
      if (this.props.testDefinitionArchivedTestCodesKeyword !== "") {
        this.getFoundTestDefinitionData();
      } else if (
        this.props.testDefinitionArchivedTestCodesKeyword === "" &&
        this.props.testDefinitionArchivedTestCodesActiveSearch
      ) {
        this.getFoundTestDefinitionData();
      }
    }
    // if permissionsActiveSearch gets updated
    if (
      prevProps.testDefinitionArchivedTestCodesActiveSearch !==
      this.props.testDefinitionArchivedTestCodesActiveSearch
    ) {
      // there is no current active search (on clear search action)
      if (!this.props.testDefinitionArchivedTestCodesActiveSearch) {
        this.getTestDefinitionData();
        // there is a current active search (on search action)
      } else {
        this.getFoundTestDefinitionData();
      }
    }
    // if triggerSelectTestDefinitionTablesRefreshState gets updated
    if (
      prevProps.triggerSelectTestDefinitionTablesRefreshState !==
      this.props.triggerSelectTestDefinitionTablesRefreshState
    ) {
      this.getTestDefinitionDataBasedOnActiveSearch();
    }
  };

  // get test definition data based on the testDefinitionArchivedTestCodesActiveSearch redux state
  getTestDefinitionDataBasedOnActiveSearch = () => {
    // current search
    if (this.props.testDefinitionArchivedTestCodesActiveSearch) {
      this.getFoundTestDefinitionData();
      // no current search
    } else {
      this.getTestDefinitionData();
    }
  };

  getTestDefinitionData = () => {
    // if selectedTestDefinitionId is null
    if (this.props.selectedTestDefinitionId === null) {
      this.setState({ currentlyLoading: true }, () => {
        // initializing needed object and array for rowsDefinition (needed for GenericTable component)
        let rowsDefinition = {};
        const data = [];
        this.props
          .getTestDefinitionData(
            true,
            null,
            null,
            null,
            this.props.testDefinitionArchivedTestCodesPaginationPage,
            this.props.testDefinitionArchivedTestCodesPaginationPageSize,
            null,
            null,
            false,
            true
          )
          .then(response => {
            for (let i = 0; i < response.body.results.length; i++) {
              // populating data object with provided data
              data.push({
                column_1: response.body.results[i].details[0].parent_code,
                column_2: response.body.results[i].details[0].test_code,
                column_3: response.body.results[i].details[0][`${this.props.currentLanguage}_name`],
                column_4: (
                  <CustomButton
                    buttonId={`test-definition-archived-test-codes-restore-button-row-${i}`}
                    label={
                      <>
                        <FontAwesomeIcon icon={faBoxOpen} style={styles.buttonIcon} />
                        <span>
                          {
                            LOCALIZE.testBuilder.testDefinition.selectTestDefinition
                              .archivedTestCodes.table.restoreButton
                          }
                        </span>
                      </>
                    }
                    action={() => {
                      this.openRestoreConfirmationPopup(response.body.results[i].details[0]);
                    }}
                    customStyle={styles.actionButton}
                    buttonTheme={THEME.SECONDARY}
                    ariaLabel={LOCALIZE.formatString(
                      LOCALIZE.testBuilder.testDefinition.selectTestDefinition.archivedTestCodes
                        .table.restoreButtonAriaLabel,
                      response.body.results[i].details[0].parent_code,
                      response.body.results[i].details[0].test_code,
                      response.body.results[i].details[0][`${this.props.currentLanguage}_name`]
                    )}
                    disabled={response.body.results[i].action_buttons_disabled}
                  />
                )
              });
            }
            // updating rowsDefinition object with provided data and needed style
            rowsDefinition = {
              column_1_style: COMMON_STYLE.LEFT_TEXT,
              column_2_style: COMMON_STYLE.LEFT_TEXT,
              column_3_style: COMMON_STYLE.LEFT_TEXT,
              column_4_style: COMMON_STYLE.CENTERED_TEXT,
              data: data
            };
            // saving results in state
            this.setState({
              rowsDefinition: rowsDefinition,
              currentlyLoading: false,
              numberOfTestDefinitionArchivedTestCodesPages: Math.ceil(
                parseInt(response.body.count) /
                  this.props.testDefinitionArchivedTestCodesPaginationPageSize
              )
            });
          });
      });
    }
  };

  getFoundTestDefinitionData = () => {
    this.setState({ currentlyLoading: true }, () => {
      // initializing needed object and array for rowsDefinition (needed for GenericTable component)
      let rowsDefinition = {};
      const data = [];
      this.props
        .getTestDefinitionData(
          true,
          null,
          null,
          null,
          this.props.testDefinitionArchivedTestCodesPaginationPage,
          this.props.testDefinitionArchivedTestCodesPaginationPageSize,
          this.props.testDefinitionArchivedTestCodesKeyword,
          this.props.currentLanguage,
          false,
          true
        )
        .then(response => {
          // there are no results found
          if (response.body[0] === "no results found") {
            this.setState(
              {
                rowsDefinition: {},
                numberOfTestDefinitionArchivedTestCodesPages: 1,
                resultsFound: 0
              },
              () => {
                this.setState({ currentlyLoading: false });
              }
            );
            // there is at least one result found
          } else {
            for (let i = 0; i < response.body.results.length; i++) {
              // populating data object with provided data
              data.push({
                column_1: response.body.results[i].details[0].parent_code,
                column_2: response.body.results[i].details[0].test_code,
                column_3: response.body.results[i].details[0][`${this.props.currentLanguage}_name`],
                column_4: (
                  <CustomButton
                    buttonId={`test-definition-archived-test-codes-restore-button-row-${i}`}
                    label={
                      <>
                        <FontAwesomeIcon icon={faBoxOpen} style={styles.buttonIcon} />
                        <span>
                          {
                            LOCALIZE.testBuilder.testDefinition.selectTestDefinition
                              .archivedTestCodes.table.restoreButton
                          }
                        </span>
                      </>
                    }
                    action={() => {
                      this.openRestoreConfirmationPopup(response.body.results[i].details[0]);
                    }}
                    customStyle={styles.actionButton}
                    buttonTheme={THEME.SECONDARY}
                    ariaLabel={LOCALIZE.formatString(
                      LOCALIZE.testBuilder.testDefinition.selectTestDefinition.archivedTestCodes
                        .table.restoreButtonAriaLabel,
                      response.body.results[i].details[0].parent_code,
                      response.body.results[i].details[0].test_code,
                      response.body.results[i].details[0][`${this.props.currentLanguage}_name`]
                    )}
                  />
                )
              });
            }
            // updating rowsDefinition object with provided data and needed style
            rowsDefinition = {
              column_1_style: COMMON_STYLE.LEFT_TEXT,
              column_2_style: COMMON_STYLE.LEFT_TEXT,
              column_3_style: COMMON_STYLE.LEFT_TEXT,
              column_4_style: COMMON_STYLE.CENTERED_TEXT,
              data: data
            };
            // saving results in state
            this.setState({
              rowsDefinition: rowsDefinition,
              currentlyLoading: false,
              numberOfTestDefinitionArchivedTestCodesPages: Math.ceil(
                parseInt(response.body.count) /
                  this.props.testDefinitionArchivedTestCodesPaginationPageSize
              ),
              resultsFound: response.body.count
            });
          }
        });
    });
  };

  restoreSelectedTestDefinitionTestCode = () => {
    this.setState({ restoreProcessLoading: true }, () => {
      // restoring test code
      this.props
        .archiveTestDefinitionTestCode(
          this.state.currentTestDefinitionData.parent_code,
          this.state.currentTestDefinitionData.test_code,
          false
        )
        .then(response => {
          if (response.ok) {
            // refreshing Archived Test Codes table
            this.props.triggerSelectTestDefinitionTablesRefresh();
            // updating needed states
            this.setState({
              showRestoreConfirmationPopup: false,
              currentTestDefinitionData: {},
              restoreProcessLoading: false
            });
          } else {
            // temporary console logs until better way is made
            console.log(response);
          }
        });
    });
  };

  openRestoreConfirmationPopup = currentTestDefinitionData => {
    this.setState({
      showRestoreConfirmationPopup: true,
      currentTestDefinitionData: currentTestDefinitionData
    });
  };

  closeRestoreConfirmationPopup = () => {
    this.setState({
      showRestoreConfirmationPopup: false,
      currentTestDefinitionData: {}
    });
  };

  render() {
    const columnsDefinition = [
      {
        label:
          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.archivedTestCodes.table.column1,
        style: { ...{ width: "15%" }, ...COMMON_STYLE.LEFT_TEXT }
      },
      {
        label:
          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.archivedTestCodes.table.column2,
        style: { ...{ width: "25%" }, ...COMMON_STYLE.LEFT_TEXT }
      },
      {
        label:
          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.archivedTestCodes.table.column3,
        style: { width: "45%" }
      },
      {
        label:
          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.archivedTestCodes.table.column4,
        style: { ...{ width: "15%" }, ...COMMON_STYLE.CENTERED_TEXT }
      }
    ];

    return (
      <div>
        <h2>{LOCALIZE.testBuilder.testDefinition.selectTestDefinition.archivedTestCodes.title}</h2>
        <p>
          {LOCALIZE.testBuilder.testDefinition.selectTestDefinition.archivedTestCodes.description}
        </p>
        <div style={styles.tableContainer}>
          <div>
            <SearchBarWithDisplayOptions
              idPrefix={"select-archived-test-definition"}
              currentlyLoading={this.state.currentlyLoading}
              updateSearchStates={this.props.updateSearchArchivedTestCodesDefinitionsStates}
              updatePageState={this.props.updateCurrentTestDefinitionArchivedTestCodesPageState}
              paginationPageSize={Number(
                this.props.testDefinitionArchivedTestCodesPaginationPageSize
              )}
              updatePaginationPageSize={
                this.props.updateCurrentTestDefinitionArchivedTestCodesPageSizeState
              }
              data={this.state.rowsDefinition.data || []}
              resultsFound={this.state.resultsFound}
            />
            <GenericTable
              classnamePrefix="select-test-definition"
              columnsDefinition={columnsDefinition}
              rowsDefinition={this.state.rowsDefinition}
              emptyTableMessage={
                LOCALIZE.testBuilder.testDefinition.selectTestDefinition.archivedTestCodes.table
                  .noData
              }
              currentlyLoading={this.state.currentlyLoading}
            />
            <Pagination
              paginationContainerId={"select-archived-test-definition-pagination-pages-link"}
              pageCount={this.state.numberOfTestDefinitionArchivedTestCodesPages}
              paginationPage={this.props.testDefinitionArchivedTestCodesPaginationPage}
              updatePaginationPageState={
                this.props.updateCurrentTestDefinitionArchivedTestCodesPageState
              }
              firstTableRowId={"test-definition-archived-test-codes-restore-button-row-0"}
            />
          </div>
        </div>
        <PopupBox
          show={this.state.showRestoreConfirmationPopup}
          handleClose={() => {}}
          isBackdropStatic={true}
          shouldCloseOnEsc={false}
          overflowVisible={true}
          title={
            LOCALIZE.testBuilder.testDefinition.selectTestDefinition.archivedTestCodes.popup.title
          }
          description={
            <p>
              {LOCALIZE.formatString(
                LOCALIZE.testBuilder.testDefinition.selectTestDefinition.archivedTestCodes.popup
                  .description,
                <span
                  style={styles.bold}
                >{`${this.state.currentTestDefinitionData.parent_code} ${this.state.currentTestDefinitionData.test_code}`}</span>
              )}
            </p>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={
            LOCALIZE.testBuilder.testDefinition.selectTestDefinition.archivedTestCodes.popup
              .rightButton
          }
          rightButtonIcon={faBoxOpen}
          rightButtonAction={this.restoreSelectedTestDefinitionTestCode}
          rightButtonState={
            this.state.restoreProcessLoading ? BUTTON_STATE.disabled : BUTTON_STATE.enabled
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeRestoreConfirmationPopup}
        />
      </div>
    );
  }
}

export { ArchivedTestCodes as unconnectedArchivedTestCodes };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    testDefinitionArchivedTestCodesPaginationPageSize:
      state.testBuilder.testDefinitionArchivedTestCodesPaginationPageSize,
    testDefinitionArchivedTestCodesPaginationPage:
      state.testBuilder.testDefinitionArchivedTestCodesPaginationPage,
    testDefinitionArchivedTestCodesKeyword:
      state.testBuilder.testDefinitionArchivedTestCodesKeyword,
    testDefinitionArchivedTestCodesActiveSearch:
      state.testBuilder.testDefinitionArchivedTestCodesActiveSearch,
    selectedTestDefinitionId: state.testBuilder.selectedTestDefinitionId,
    triggerSelectTestDefinitionTablesRefreshState:
      state.testBuilder.triggerSelectTestDefinitionTablesRefreshState
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getTestDefinitionData,
      updateCurrentTestDefinitionArchivedTestCodesPageState,
      updateCurrentTestDefinitionArchivedTestCodesPageSizeState,
      updateSearchArchivedTestCodesDefinitionsStates,
      archiveTestDefinitionTestCode,
      triggerSelectTestDefinitionTablesRefresh
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ArchivedTestCodes);
