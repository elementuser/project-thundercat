import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faClone, faQuestionCircle, faTrashAlt } from "@fortawesome/free-regular-svg-icons";
import { faBinoculars, faCheck } from "@fortawesome/free-solid-svg-icons";
import { Button, Col } from "react-bootstrap";
import "../../css/collapsing-item.css";
import LOCALIZE from "../../text_resources";
import { LANGUAGES } from "../commons/Translation";
import StyledTooltip, { TYPE, EFFECT, TRIGGER_TYPE } from "../authentication/StyledTooltip";
import DropdownSelect from "../commons/DropdownSelect";
import CustomButton, { THEME } from "../commons/CustomButton";
import {
  QUESTIONS,
  QUESTION_SECTIONS,
  ANSWERS,
  ANSWER_DETAILS,
  MULTIPLE_CHOICE_QUESTION_DETAILS,
  QUESTION_SECTION_DEFINITIONS,
  QUESTION_LIST_RULES
} from "../../modules/TestBuilderRedux";

export const styles = {
  mainContainer: {
    width: "100%"
  },
  input: {
    width: "100%",
    minHeight: 38,
    padding: "3px 6px 3px 6px",
    borderRadius: 4,
    display: "block"
  },
  textFieldContainer: {
    marginTop: "auto",
    marginBottom: "auto"
  },
  textAreaInput: {
    width: "100%",
    height: 270,
    padding: "3px 6px 3px 6px",
    borderRadius: 4,
    display: "block"
  },
  label: {
    padding: "3px 0 0 4px"
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  tooltipIconContainer: {
    padding: "0 6px"
  },
  tooltipIcon: {
    color: "#00565e"
  },
  tooltipContent: {
    fontWeight: "normal"
  },
  secondaryEmailTooltipIcon: {
    color: "#00565e"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6
  },
  borderBox: {
    borderBottom: "1px solid",
    borderRadius: "5px",
    padding: "5px",
    borderColor: "#CECECE",
    color: "red",
    margin: "5px"
  },
  rowStyle: {
    margin: "5px"
  },
  buttonRowStyle: {
    margin: "15px"
  },
  button: {
    minWidth: 200
  },
  labelContainer: {
    marginTop: "auto",
    marginBottom: "auto",
    textAlign: "left"
  },
  displayTableCell: {
    display: "table-cell"
  },
  checkboxFieldContainer: {
    marginLeft: 6,
    marginRight: "auto"
  },
  checkbox: {
    margin: "0 0 6px 12px",
    verticalAlign: "middle"
  },
  checkboxLabel: {
    paddingLeft: "15px",
    margin: 0,
    maxWidth: "95%",
    display: "inline"
  },
  buttonLabel: {
    marginLeft: 6
  }
};

const getValuesThenPassTo = (event, action, func, dataObj = {}) => {
  let value = "";
  let name = "";
  // validationtype

  // check if select input
  if (event.target === undefined) {
    value = action.option.value;
    name = action.name;
  }
  // else do regular input info get
  else {
    value = event.target.value;
    name = event.target.name;
    if (event.target.type === "checkbox") {
      value = event.target.checked;
    }
  }
  func(name, value, dataObj);
};

export const makeLabel = (
  inputName,
  localize,
  idName = inputName,
  customStyle = {},
  includeTooltip = true,
  customLabel = "",
  customColSizes = {
    xl: 6,
    lg: 6,
    md: 6,
    sm: 6
  }
) => {
  return (
    <Col
      xl={customColSizes.xl}
      lg={customColSizes.lg}
      md={customColSizes.md}
      sm={customColSizes.sm}
      style={{ ...styles.labelContainer, ...customStyle }}
    >
      <div id={`${idName}-title`} style={styles.displayTableCell}>
        <label id={`${idName}`} htmlFor={`${idName}-input`}>
          {customLabel === "" ? localize[`${inputName}`].title : customLabel}
        </label>
        {includeTooltip && (
          <StyledTooltip
            id={`${idName}-tooltip-make-label`}
            place="right"
            type={TYPE.light}
            effect={EFFECT.solid}
            triggerType={[TRIGGER_TYPE.hover]}
            tooltipElement={
              <Button
                data-tip=""
                data-for={`${idName}-tooltip-make-label`}
                tabIndex="-1"
                variant="link"
                style={styles.tooltipIconContainer}
              >
                <FontAwesomeIcon icon={faQuestionCircle} style={styles.secondaryEmailTooltipIcon} />
              </Button>
            }
            tooltipContent={
              <div style={styles.tooltipContent}>
                <p>{customLabel === "" ? localize[`${inputName}`].titleTooltip : customLabel}</p>
              </div>
            }
          />
        )}
        <label id={`${idName}-tooltip-for-accessibility`} style={styles.hiddenText}>
          {customLabel === "" ? localize[`${inputName}`].titleTooltip : customLabel}
        </label>
      </div>
    </Col>
  );
};

export const makeLabelFromObject = obj => {
  const { name, title, toolTip, idName = name } = obj;
  return (
    <Col style={styles.labelContainer}>
      <div id={`${idName}-title`}>
        <label id={`${idName}`} htmlFor={`${name}-input`}>
          {title}
        </label>
        <StyledTooltip
          id={`${idName}-tooltip-make-label-from-obj`}
          place="right"
          type={TYPE.light}
          effect={EFFECT.solid}
          triggerType={[TRIGGER_TYPE.hover]}
          tooltipElement={
            <Button
              data-tip=""
              data-for={`${idName}-tooltip-make-label-from-obj`}
              tabIndex="-1"
              variant="link"
              style={styles.tooltipIconContainer}
            >
              <FontAwesomeIcon icon={faQuestionCircle} style={styles.secondaryEmailTooltipIcon} />
            </Button>
          }
          tooltipContent={
            <div style={styles.tooltipContent}>
              <p>{toolTip}</p>
            </div>
          }
        />
        <label id={`${idName}-tooltip-for-accessibility`} style={styles.hiddenText}>
          , {toolTip}
        </label>
      </div>
    </Col>
  );
};

export const makeTextBoxField = (
  inputName,
  localize,
  property = "",
  isValid = true,
  onChange = () => {},
  idName = inputName,
  disabled = false,
  customStyle = {},
  onChangePassValues = true,
  customColSizes = {
    xl: 6,
    lg: 6,
    md: 6,
    sm: 6
  }
) => {
  return (
    <Col
      xl={customColSizes.xl}
      lg={customColSizes.lg}
      md={customColSizes.md}
      sm={customColSizes.sm}
      style={styles.textFieldContainer}
    >
      <input
        name={inputName}
        id={`${idName}-input`}
        className={isValid ? "valid-field" : "invalid-field"}
        aria-labelledby={`${idName} ${idName}-tooltip-for-accessibility invalid-${idName}-msg`}
        aria-required={false}
        aria-invalid={!isValid}
        style={{ ...styles.input, width: "100%", ...customStyle }}
        type="text"
        value={property}
        onChange={
          onChangePassValues
            ? (event, action) => getValuesThenPassTo(event, action, onChange)
            : onChange
        }
        disabled={disabled}
      />

      {!isValid && (
        <label
          id={`invalid-${idName}-msg`}
          htmlFor={`${idName}-input`}
          style={styles.errorMessage}
          className="notranslate"
        >
          {inputName !== "" ? localize[`${inputName}`].errorMessage : localize.errorMessage}
        </label>
      )}
    </Col>
  );
};

export const makeTextAreaField = (
  inputName,
  localize,
  property = "",
  isValid = true,
  onChange = () => {},
  idName = inputName,
  disabled = false,
  customStyle = {}
) => {
  return (
    <Col>
      <textarea
        name={inputName}
        id={`${idName}-input`}
        className={isValid ? "valid-field" : "invalid-field"}
        aria-labelledby={`${idName} ${idName}-tooltip-for-accessibility invalid-${idName}-msg`}
        aria-required={false}
        aria-invalid={!isValid}
        style={{ ...styles.textAreaInput, ...customStyle }}
        type="textarea"
        value={property}
        onChange={(event, action) => getValuesThenPassTo(event, action, onChange)}
        disabled={disabled}
      />

      {!isValid && (
        <label id={`invalid-${idName}-msg`} style={styles.errorMessage} className="notranslate">
          {localize[`${inputName}`].errorMessage}
        </label>
      )}
    </Col>
  );
};

export const makeCheckBoxField = (
  inputName,
  localize,
  property = false,
  onChange = () => {},
  idName = inputName,
  disabled = false
) => {
  return (
    <Col>
      <div className="custom-control custom-checkbox" style={styles.checkboxZone}>
        {disabled ? (
          <input
            name={inputName}
            type="checkbox"
            className="custom-control-input"
            id={`${idName}-id`}
            checked={property}
            disabled
            onChange={(event, action) => getValuesThenPassTo(event, action, onChange)}
          />
        ) : (
          <input
            name={inputName}
            type="checkbox"
            className="custom-control-input"
            id={`${idName}-id`}
            checked={property}
            onChange={(event, action) => getValuesThenPassTo(event, action, onChange)}
          />
        )}
        <label className="custom-control-label" htmlFor={`${idName}-id`}>
          {localize[`${inputName}`].titleTooltip}
        </label>
      </div>
    </Col>
  );
};

export const makeCheckBoxFieldFromObject = (obj, fontSize) => {
  const {
    inputName,
    toolTip,
    property = false,
    onChange = () => {},
    idName = inputName,
    dataObj
  } = obj;

  // converting current font size in Int
  const fontSizeInt = parseInt(fontSize.substring(0, 2));
  // initializing transform scale (used for checkbox size depending on the font size selected)
  let transformScale = "";
  // converting checkbox scale based on font size
  switch (true) {
    case fontSizeInt <= 22:
      transformScale = "scale(1)";
      break;
    case fontSizeInt > 22 && fontSizeInt <= 30:
      transformScale = "scale(1.5)";
      break;
    case fontSizeInt > 30 && fontSizeInt <= 40:
      transformScale = "scale(2)";
      break;
    case fontSizeInt > 40:
      transformScale = "scale(2.2)";
      break;
    default:
      transformScale = "scale(1)";
  }
  return (
    <Col>
      <div style={styles.checkboxFieldContainer}>
        <input
          name={inputName}
          type="checkbox"
          style={{ ...styles.checkbox, ...{ transform: transformScale } }}
          id={`${idName}-id`}
          checked={property}
          onChange={(event, action) => getValuesThenPassTo(event, action, onChange, dataObj)}
        />

        <label htmlFor={`${idName}-id`} style={styles.checkboxLabel}>
          {toolTip}
        </label>
      </div>
    </Col>
  );
};

export const makeDropDownField = (
  inputName,
  localize,
  value = {},
  options,
  onChange,
  isDisabled = false,
  isValid = true,
  isClearable = false
) => {
  return (
    <Col>
      <DropdownSelect
        idPrefix={inputName}
        name={inputName}
        ariaLabelledBy={`${inputName}-selected ${inputName}-placeholder`}
        isDisabled={isDisabled}
        isValid={isValid}
        options={options}
        onChange={onChange}
        defaultValue={value}
        hasPlaceholder={true}
        customPlaceholderWording={
          localize[`${inputName}`].selectPlaceholder || LOCALIZE.commons.pleaseSelect
        }
        isClearable={isClearable}
        orderByLabels={false}
      ></DropdownSelect>
      {!isValid && (
        <label id={`invalid-${inputName}-msg`} style={styles.errorMessage} className="notranslate">
          {localize[`${inputName}`].errorMessage}
        </label>
      )}
    </Col>
  );
};

export const makeMultiDropDownField = (
  inputName,
  localize,
  value = null,
  options,
  onChange,
  isDisabled = false
) => {
  const customStyles = {
    control: (provided, state) => ({
      ...provided,
      ...{ border: "1px solid #00565e" },
      // style when focused
      ...(state.isFocused ? { outline: "none", boxShadow: "0 0 10px #00565e" } : {})
    }),
    // Fixes the overlapping problem of the component
    menu: provided => ({ ...provided, zIndex: 9999 })
  };

  return (
    <Col>
      <DropdownSelect
        idPrefix={inputName}
        name={inputName}
        ariaLabelledBy={`${inputName}-selected ${inputName}-placeholder`}
        customStyle={customStyles}
        isMulti={true}
        isDisabled={isDisabled}
        options={options}
        onChange={onChange}
        defaultValue={value}
        hasPlaceholder={true}
        customPlaceholderWording={
          localize[`${inputName}`].selectPlaceholder || LOCALIZE.commons.pleaseSelect
        }
        isClearable={true}
        orderByLabels={false}
      ></DropdownSelect>
    </Col>
  );
};

export const getNextInNumberSeries = (array, fieldName) => {
  let max = 0;
  for (const obj of array) {
    if (obj[`${fieldName}`] >= max) {
      max = obj[`${fieldName}`];
    }
  }
  return max + 1;
};

export const getNextTempInNumberSeries = (array, fieldName) => {
  let max = 0;
  for (const obj of array) {
    // checking if current ID is a real or temp ID
    let fieldValue = null;
    if (typeof obj[`${fieldName}`] === "number") {
      fieldValue = obj[`${fieldName}`];
    } else {
      fieldValue = parseInt(obj[`${fieldName}`].split("TEMP-")[1]);
    }
    if (fieldValue >= max) {
      max = fieldValue;
    }
  }
  return `TEMP-${max + 1}`;
};

export const makeSaveDeleteButtons = (
  saveFunc,
  deleteFunc,
  saveText,
  deleteText,
  dataSaved = undefined,
  duplicateButton = undefined,
  duplicateButtonText = undefined,
  duplicateButtonFunc = undefined,
  previewButton = undefined,
  previewButtonText = undefined,
  previewButtonFunc = undefined,
  saveButtonDisabled = false
) => {
  return (
    <>
      <Col className="text-center" md={3}>
        <CustomButton
          buttonId="unit-test-delete-item-button"
          label={
            <>
              <FontAwesomeIcon icon={faTrashAlt} />
              <span style={styles.buttonLabel}>{deleteText}</span>
            </>
          }
          customStyle={styles.button}
          action={deleteFunc}
          buttonTheme={THEME.DANGER}
        />
      </Col>
      {duplicateButton && (
        <Col className="text-center" md={3}>
          <CustomButton
            buttonId="unit-test-duplicate-item-button"
            label={
              <>
                <FontAwesomeIcon icon={faClone} />
                <span style={styles.buttonLabel}>{duplicateButtonText}</span>
              </>
            }
            customStyle={styles.button}
            action={duplicateButtonFunc}
            buttonTheme={THEME.SECONDARY}
          />
        </Col>
      )}
      {previewButton && (
        <Col className="text-center" md={3}>
          <CustomButton
            buttonId="unit-test-preview-item-button"
            label={
              <>
                <FontAwesomeIcon icon={faBinoculars} />
                <span style={styles.buttonLabel}>{previewButtonText}</span>
              </>
            }
            customStyle={styles.button}
            action={previewButtonFunc}
            buttonTheme={THEME.SECONDARY}
          />
        </Col>
      )}
      <Col className="text-center" md={3}>
        <CustomButton
          buttonId="unit-test-save-item-button"
          label={
            <>
              <FontAwesomeIcon icon={faCheck} />
              <span style={styles.buttonLabel}>
                {dataSaved ? LOCALIZE.commons.saved : saveText}
              </span>
            </>
          }
          customStyle={styles.button}
          action={saveFunc}
          buttonTheme={dataSaved ? THEME.SUCCESS : THEME.PRIMARY}
          disabled={saveButtonDisabled ? true : !!dataSaved}
        />
      </Col>
    </>
  );
};

export const isDecimal = value => {
  return /^\d+\.\d$/.test(value);
};

export const isNumber = value => {
  return /^[0-9]+$/.test(value);
};

export const isNumberOrEmpty = value => {
  return /^[0-9]*$/.test(value);
};

export const noSpecialCharacters = value => {
  return /^[a-zA-ZÀ-Ÿ0-9-. ]{1,}$/.test(value);
};

// whether the test is bilingual or english
export const testLanguageHasEnglish = testLanguage => {
  if (testLanguage === "--" || testLanguage === null || testLanguage === LANGUAGES.english) {
    return true;
  }
  return false;
};
// whether the test is bilingual or french
export const testLanguageHasFrench = testLanguage => {
  if (testLanguage === "--" || testLanguage === null || testLanguage === LANGUAGES.french) {
    return true;
  }
  return false;
};

// Get test section component's language
export const testSectionComponentLanguage = (componentID, components) => {
  for (const component of components) {
    if (component.id === componentID) {
      return component.language;
    }
  }
  // return the identifier for bilingual test
  return "--";
};

// return lang for html lang tag
export const getTestLanguage = (componentLanguage, stateLanguage) => {
  if (componentLanguage === "--" || componentLanguage === null) {
    return stateLanguage;
  }
  return componentLanguage;
};

export const languageOptions = [
  { label: LOCALIZE.commons.bilingual, value: "--" },
  { label: LOCALIZE.commons.english, value: LANGUAGES.english },
  { label: LOCALIZE.commons.french, value: LANGUAGES.french }
];

export const allValid = state => {
  for (const property of Object.keys(state)) {
    if (property.indexOf("Valid") >= 0 && !state[property]) {
      return false;
    }
  }
  return true;
};

// must be the same values as in scoringmethostypes method_type_codename
// don't forget to update the constants in ...\backend\cms\views\utils.py (SCORING_METHOD_TYPE)
export const SCORING_METHOD_TYPE = {
  THRESHOLD: "THRESHOLD",
  PASS_FAIL: "PASS_FAIL",
  RAW: "RAW",
  PERCENTAGE: "PERCENTAGE",
  NONE: "NONE"
};

// useful for collapsing item container component when calling the move up functionality (when usesMoveUpAndDownFunctionalities props is set to true)
export async function collapsingItemMoveUpFunctionality(index, array) {
  // initializing objArray
  let objArray = [];
  // creating a copy of the array
  objArray = Array.from(array);
  // swapping array elements
  [objArray[index], objArray[index - 1]] = [objArray[index - 1], objArray[index]];
  // updating orders
  objArray[index - 1].order = index;
  objArray[index].order = index + 1;
  return objArray;
}

// useful for collapsing item container component when calling the move down functionality (when usesMoveUpAndDownFunctionalities props is set to true)
export async function collapsingItemMoveDownFunctionality(index, array) {
  // initializing objArray
  let objArray = [];
  // creating a copy of the array
  objArray = Array.from(array);
  // swapping array elements
  [objArray[index], objArray[index + 1]] = [objArray[index + 1], objArray[index]];
  // updating orders
  objArray[index].order = index + 1;
  objArray[index + 1].order = index + 2;
  return objArray;
}

export function orderByOrder(a, b) {
  if (a.order < b.order) {
    return -1;
  }
  if (a.order > b.order) {
    return 1;
  }
  return 0;
}

// handling the deletion of question data when deleting or updating test sections and/or sub-sections
export const deleteQuestionData = (
  replaceTestDefinitionField,
  concernedTestSectionComponents,
  questions,
  questionSections,
  questionSectionDefinitions,
  answers,
  answerDetails,
  multipleChoiceQuestionDetails
) => {
  // initializing objArray
  let objArray = [];

  // delete questions
  const questionsToDelete = [];
  objArray = questions.filter(obj => {
    if (concernedTestSectionComponents.indexOf(obj.test_section_component) === -1) {
      return obj;
    }
    questionsToDelete.push(obj.id);
    return null;
  });
  replaceTestDefinitionField(QUESTIONS, objArray);

  // delete question sections
  const questionSectionsToDelete = [];
  objArray = questionSections.filter(obj => {
    if (questionsToDelete.indexOf(obj.question) === -1) {
      return obj;
    }
    questionSectionsToDelete.push(obj.id);
    return null;
  });
  replaceTestDefinitionField(QUESTION_SECTIONS, objArray);

  // delete question section definitions
  objArray = questionSectionDefinitions.filter(obj => {
    if (questionSectionsToDelete.indexOf(obj.question_section) === -1) {
      return obj;
    }
    return null;
  });
  replaceTestDefinitionField(QUESTION_SECTION_DEFINITIONS, objArray);

  // delete question answers
  const questionAnswersToDelete = [];
  objArray = answers.filter(obj => {
    if (questionsToDelete.indexOf(obj.question) === -1) {
      return obj;
    }
    questionAnswersToDelete.push(obj.id);
    return null;
  });
  replaceTestDefinitionField(ANSWERS, objArray);

  // delete question answer details
  objArray = answerDetails.filter(obj => {
    if (questionAnswersToDelete.indexOf(obj.answer) === -1) {
      return obj;
    }
    return null;
  });
  replaceTestDefinitionField(ANSWER_DETAILS, objArray);

  // delete question multiple choice answer details
  objArray = multipleChoiceQuestionDetails.filter(obj => {
    if (questionsToDelete.indexOf(obj.question) === -1) {
      return obj;
    }
    return null;
  });
  replaceTestDefinitionField(MULTIPLE_CHOICE_QUESTION_DETAILS, objArray);
};

// handling the deletion of question rules data when deleting or updating sub-sections
export const deleteQuestionRulesData = (
  replaceTestDefinitionField,
  testSectionComponentIds,
  questionListRules
) => {
  // initializing finalQuestionListRulesArray
  let finalQuestionListRulesArray = questionListRules;
  // looping in test section components
  for (let i = 0; i < testSectionComponentIds.length; i++) {
    // deleting question list rules related to current test section component
    finalQuestionListRulesArray = questionListRules.filter(
      obj => obj.test_section_component !== testSectionComponentIds[i]
    );
  }
  replaceTestDefinitionField(QUESTION_LIST_RULES, finalQuestionListRulesArray);
};
