import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Row } from "react-bootstrap";
import {
  makeLabel,
  makeSaveDeleteButtons,
  makeTextAreaField,
  makeTextBoxField,
  allValid
} from "./helpers";
import {
  modifyTestDefinitionField,
  replaceTestDefinitionField,
  PAGE_SECTION_DEFINITIONS
} from "../../modules/TestBuilderRedux";
import { styles } from "./TestSectionForm";
import { UnconnectedTopTabs as TopTabs } from "../commons/TopTabs";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import { LANGUAGES } from "../commons/Translation";
import { PageSectionType } from "../testFactory/Constants";

class SampleEmailResponsePageSectionForm extends Component {
  state = {
    showDialog: false,
    enPageDefinition: { page_section_type: PageSectionType.SAMPLE_EMAIL_RESPONSE },
    frPageDefinition: { page_section_type: PageSectionType.SAMPLE_EMAIL_RESPONSE }
  };

  modifyField = (inputName, value) => {
    const newObj = { ...this.state.pageDefinition };
    newObj[`${inputName}`] = value;
    this.setState({ pageDefinition: newObj });
  };

  modifyEnPageDefinition = (inputName, value) => {
    const newObj = { ...this.props.enPageDefinition, ...this.state.enPageDefinition };
    newObj[`${inputName}`] = value;
    this.setState({ enPageDefinition: newObj });
  };

  modifyFrPageDefinition = (inputName, value) => {
    const newObj = { ...this.props.frPageDefinition, ...this.state.frPageDefinition };
    newObj[`${inputName}`] = value;
    this.setState({ frPageDefinition: newObj });
  };

  handleSave = () => {
    if (allValid(this.state)) {
      const frPage = {
        ...this.props.frPageDefinition,
        ...this.state.frPageDefinition
      };
      const enPage = {
        ...this.props.enPageDefinition,
        ...this.state.enPageDefinition
      };
      this.props.modifyTestDefinitionField(
        PAGE_SECTION_DEFINITIONS,
        enPage,
        this.props.enPageDefinition.id,
        PageSectionType.SAMPLE_EMAIL_RESPONSE
      );
      this.props.modifyTestDefinitionField(
        PAGE_SECTION_DEFINITIONS,
        frPage,
        this.props.frPageDefinition.id,
        PageSectionType.SAMPLE_EMAIL_RESPONSE
      );
      this.props.handleSave();
    }
  };

  openDialog = () => {
    this.setState({ showDialog: true });
  };

  closeDialog = () => {
    this.setState({ showDialog: false });
  };

  confirmDelete = () => {
    this.props.handleDelete();
    this.closeDialog();
  };

  getLanguageTabs = () => {
    const enPageDefinition = {
      ...this.props.enPageDefinition,
      ...this.state.enPageDefinition
    };
    const frPageDefinition = {
      ...this.props.frPageDefinition,
      ...this.state.frPageDefinition
    };
    const enPageSectionTypeContent = (
      <>
        <Row style={styles.rowStyle}>
          {makeLabel("to_field", LOCALIZE.testBuilder.sampleEmailResonsePageSection)}
          {makeTextBoxField(
            "to_field",
            LOCALIZE.testBuilder.sampleEmailResonsePageSection,
            enPageDefinition.to_field,
            true,
            this.modifyEnPageDefinition,
            "en-to_field"
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("cc_field", LOCALIZE.testBuilder.sampleEmailResonsePageSection)}
          {makeTextBoxField(
            "cc_field",
            LOCALIZE.testBuilder.sampleEmailResonsePageSection,
            enPageDefinition.cc_field,
            true,
            this.modifyEnPageDefinition,
            "en-cc_field"
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("response", LOCALIZE.testBuilder.sampleEmailResonsePageSection)}
        </Row>
        <Row style={styles.rowStyle}>
          {makeTextAreaField(
            "response",
            LOCALIZE.testBuilder.sampleEmailResonsePageSection,
            enPageDefinition.response,
            true,
            this.modifyEnPageDefinition,
            "en-response"
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("reason", LOCALIZE.testBuilder.sampleEmailResonsePageSection)}
        </Row>
        <Row style={styles.rowStyle}>
          {makeTextAreaField(
            "reason",
            LOCALIZE.testBuilder.sampleEmailResonsePageSection,
            enPageDefinition.reason,
            true,
            this.modifyEnPageDefinition,
            "en-reason"
          )}
        </Row>
      </>
    );
    const frPageSectionTypeContent = (
      <>
        <Row style={styles.rowStyle}>
          {makeLabel("to_field", LOCALIZE.testBuilder.sampleEmailResonsePageSection)}
          {makeTextBoxField(
            "to_field",
            LOCALIZE.testBuilder.sampleEmailResonsePageSection,
            frPageDefinition.to_field,
            true,
            this.modifyFrPageDefinition,
            "fr-to_field"
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("cc_field", LOCALIZE.testBuilder.sampleEmailResonsePageSection)}
          {makeTextBoxField(
            "cc_field",
            LOCALIZE.testBuilder.sampleEmailResonsePageSection,
            frPageDefinition.cc_field,
            true,
            this.modifyFrPageDefinition,
            "fr-cc_field"
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("response", LOCALIZE.testBuilder.sampleEmailResonsePageSection)}
        </Row>
        <Row style={styles.rowStyle}>
          {makeTextAreaField(
            "response",
            LOCALIZE.testBuilder.sampleEmailResonsePageSection,
            frPageDefinition.response,
            true,
            this.modifyFrPageDefinition,
            "fr-response"
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("reason", LOCALIZE.testBuilder.sampleEmailResonsePageSection)}
        </Row>
        <Row style={styles.rowStyle}>
          {makeTextAreaField(
            "reason",
            LOCALIZE.testBuilder.sampleEmailResonsePageSection,
            frPageDefinition.reason,
            true,
            this.modifyFrPageDefinition,
            "fr-reason"
          )}
        </Row>
      </>
    );
    const { testLanguage } = this.props;
    const TABS = [];
    if (testLanguage === "--" || testLanguage === null || testLanguage === LANGUAGES.english) {
      TABS.push({
        key: LOCALIZE.commons.english,
        tabName: LOCALIZE.commons.english,
        body: enPageSectionTypeContent
      });
    }
    if (testLanguage === "--" || testLanguage === null || testLanguage === LANGUAGES.french) {
      TABS.push({
        key: LOCALIZE.commons.french,
        tabName: LOCALIZE.commons.french,
        body: frPageSectionTypeContent
      });
    }

    return TABS;
  };

  render() {
    const TABS = this.getLanguageTabs();
    return (
      <div id="unit-test-sample-email-response-page-section">
        <TopTabs TABS={TABS} defaultTab={TABS[0].key} />

        <Row className="justify-content-between" style={styles.buttonRowStyle}>
          {makeSaveDeleteButtons(
            this.handleSave,
            this.openDialog,
            LOCALIZE.commons.applyButton,
            LOCALIZE.commons.deleteButton
          )}
        </Row>
        <PopupBox
          show={this.state.showDialog}
          handleClose={this.closeDialog}
          title={"Delete Confirmation"}
          description={
            <div>
              <p>
                Deleting this item will delete all the content displayed. Are you sure you want to
                delete this object?
              </p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={LOCALIZE.commons.deleteButton}
          rightButtonAction={this.confirmDelete}
        />
      </div>
    );
  }
}

export { SampleEmailResponsePageSectionForm as unconnectedSampleEmailResponsePageSectionForm };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ modifyTestDefinitionField, replaceTestDefinitionField }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SampleEmailResponsePageSectionForm);
