import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Row } from "react-bootstrap";
import { makeLabel, allValid, makeTextBoxField, makeSaveDeleteButtons } from "./helpers";
import {
  modifyTestDefinitionField,
  replaceTestDefinitionField,
  COMPETENCY_TYPES
} from "../../modules/TestBuilderRedux";
import { styles } from "./TestSectionForm";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";

class CompetencyTypeForm extends Component {
  state = {
    showDialog: false,
    competencyType: {}
  };

  modifyField = (inputName, value) => {
    const newObj = { ...this.props.competencyType, ...this.state.competencyType };
    newObj[`${inputName}`] = value;
    this.setState({ competencyType: newObj });
  };

  nameValidation = (name, value) => {
    this.modifyField(name, value);
  };

  handleSave = () => {
    if (allValid(this.state)) {
      this.props.modifyTestDefinitionField(
        COMPETENCY_TYPES,
        { ...this.props.competencyType, ...this.state.competencyType },
        this.props.competencyType.id
      );
      this.props.expandItem();
    }
  };

  openDialog = () => {
    this.setState({ showDialog: true });
  };

  closeDialog = () => {
    this.setState({ showDialog: false });
  };

  confirmDelete = () => {
    this.props.handleDelete(this.props.competencyType.id);
    this.props.expandItem();
    this.closeDialog();
  };

  render() {
    const competencyType = {
      ...this.props.competencyType,
      ...this.state.competencyType
    };
    return (
      <div style={styles.mainContainer}>
        <Row style={styles.rowStyle}>
          {makeLabel("en_name", LOCALIZE.testBuilder.competencyTypes)}
          {makeTextBoxField(
            "en_name",
            LOCALIZE.testBuilder.competencyTypes,
            competencyType.en_name,
            true,
            this.nameValidation
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("fr_name", LOCALIZE.testBuilder.competencyTypes)}
          {makeTextBoxField(
            "fr_name",
            LOCALIZE.testBuilder.competencyTypes,
            competencyType.fr_name,
            true,
            this.nameValidation
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("max_score", LOCALIZE.testBuilder.competencyTypes)}
          {makeTextBoxField(
            "max_score",
            LOCALIZE.testBuilder.competencyTypes,
            competencyType.max_score,
            true,
            this.nameValidation
          )}
        </Row>

        <Row className="justify-content-between" style={styles.buttonRowStyle}>
          {makeSaveDeleteButtons(
            this.handleSave,
            this.openDialog,
            LOCALIZE.commons.applyButton,
            LOCALIZE.commons.deleteButton
          )}
        </Row>
        <PopupBox
          show={this.state.showDialog}
          handleClose={this.closeDialog}
          title={"Delete Confirmation"}
          description={
            <div>
              <p>
                Deleting this item will delete all the content displayed. Are you sure you want to
                delete this object?
              </p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={LOCALIZE.commons.deleteButton}
          rightButtonAction={this.confirmDelete}
        />
      </div>
    );
  }
}

export { CompetencyTypeForm as UnconnectedCompetencyTypeForm };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ modifyTestDefinitionField, replaceTestDefinitionField }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(CompetencyTypeForm);
