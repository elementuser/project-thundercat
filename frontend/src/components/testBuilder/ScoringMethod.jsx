import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck, faPlusCircle } from "@fortawesome/free-solid-svg-icons";
import { Button, Row, Container } from "react-bootstrap";
import LOCALIZE, { LOCALIZE_OBJ } from "../../text_resources";
import CollapsingItemContainer from "../eMIB/CollapsingItemContainer";
import {
  makeDropDownField,
  makeLabel,
  makeTextBoxField,
  SCORING_METHOD_TYPE,
  getNextInNumberSeries
} from "./helpers";
import {
  getScoringMethodTypes,
  setScoringMethodType,
  setScoringPassFailMinimumScore,
  addScoringThresholdField,
  replaceScoringThresholdField,
  SCORING_THRESHOLD,
  setFinalScoringThresholdConversions,
  setScoringMethodValidState,
  setScoringThreshold,
  setTestDefinitionValidationErrors
} from "../../modules/TestBuilderRedux";
import CustomButton, { THEME } from "../commons/CustomButton";
import ScoringThresholdForm from "./ScoringThresholdForm";

export const styles = {
  mainContainer: {
    width: "100%"
  },
  label: {
    padding: "3px 0 0 4px"
  },
  borderBox: {
    borderBottom: "1px solid",
    borderRadius: "5px",
    padding: "5px",
    borderColor: "#CECECE",
    color: "red"
  },
  rowStyle: {
    margin: "5px"
  },
  passFailMainContainer: {
    padding: "10px 0"
  },
  applyButton: {
    minWidth: 200
  },
  applyButtonContainer: {
    textAlign: "center",
    marginTop: 36
  },
  collapsingItemContainerBorder: {
    border: "3px solid #923534",
    padding: 12,
    marginBottom: 12
  },
  invalidScoringMethodErrorContainer: {
    marginTop: 12
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0
  },
  validationErrorMessageContainer: {
    border: "1px solid #923534",
    padding: 12,
    marginBottom: 12
  },
  validationErrorMessage: {
    textAlign: "center",
    color: "#923534",
    fontWeight: "bold"
  },
  buttonLabel: {
    marginLeft: 6
  }
};

class ScoringMethod extends Component {
  state = {
    scoringMethodTypes: [],
    selectedScoringMethodType: this.props.scoringMethodType,
    displayAddScoringThresholdButton: false,
    displayScoringThresholdValidationError: false,
    displayPassFailMinimumScoreField: false,
    scoringPassFailMinimumScoreContent: this.props.scoringPassFailMinimumScore || "",
    displayScoringPassFailMinimumScoreError: false,
    triggerCollapsingItemContainerStateReset: false,
    displayScoringMethodUndefinedError: false,
    showInvalidScoringMethodError: false
  };

  componentDidMount = () => {
    // get available scoring method types
    this.getScoringMethodTypes();
    // get current scoring method type status
    this.getScoringMethodTypeStatus();
    // get validation errors
    this.getValidationErrors();
  };

  componentDidUpdate = prevProps => {
    // if scoringMethodType redux props gets updated
    if (prevProps.scoringMethodType !== this.props.scoringMethodType) {
      // update selectedScoringMethodType state
      this.setState({ selectedScoringMethodType: this.props.scoringMethodType });
      // get updated scoring method type status
      this.getScoringMethodTypeStatus();
    }
    // if scoringThreshold gets updated
    if (prevProps.scoringThreshold !== this.props.scoringThreshold) {
      // resetting collapsing item container local states
      this.setState({
        triggerCollapsingItemContainerStateReset:
          !this.state.triggerCollapsingItemContainerStateReset
      });
    }
    if (prevProps.currentLanguage !== this.props.currentLanguage) {
      // get available scoring method types
      this.getScoringMethodTypes();
      // get current scoring method type status
      this.getScoringMethodTypeStatus();
      // get updated scoring threshold (to get the templates in the right language)
      this.getUpdatedScoringThreshold();
    }
    // if scoringPassFailMinimumScore redux props gets updated
    if (prevProps.scoringPassFailMinimumScore !== this.props.scoringPassFailMinimumScore) {
      // update selectedScoringMethodType state
      this.setState({ scoringPassFailMinimumScoreContent: this.props.scoringPassFailMinimumScore });
    }
    // if any of the upload validation error state gets updated
    if (prevProps.validationErrors !== this.props.validationErrors) {
      // get validation errors
      this.getValidationErrors();
    }
  };

  getValidationErrors = () => {
    // initializing needed variables
    let showInvalidScoringMethodError = false;

    // making sure the the validation errors state is defined
    if (typeof this.props.validationErrors !== "undefined") {
      // scoring method error
      if (!this.props.validationErrors.testDefinitionContainsValidScoringMethod) {
        showInvalidScoringMethodError = true;
      }
    }
    // updating needed states
    this.setState({
      showInvalidScoringMethodError: showInvalidScoringMethodError
    });
  };

  getScoringMethodTypes = () => {
    // getting available scoring method types
    this.props.getScoringMethodTypes(this.props.currentLanguage).then(response => {
      const scoringMethodTypes = [];
      // looping in available scoring method types
      for (let i = 0; i < response.body.length; i++) {
        // pushing data to scoringMethodTypes array
        scoringMethodTypes.push(response.body[i]);
      }
      // updating scoringMethodTypes state
      this.setState({ scoringMethodTypes: scoringMethodTypes }, () => {
        // make sure that scoringMethodType is not undefined
        if (typeof this.props.scoringMethodType !== "undefined") {
          // updating scoring method type redux state (allows us to get the scoring method type in the right language when switching language)
          this.props.setScoringMethodType(
            this.getUpdatedSelectedMethodTypeObject(this.props.scoringMethodType.value)
          );
        }
      });
    });
  };

  getScoringMethodTypeStatus = () => {
    // make sure that scoringMethodType is not undefined
    if (typeof this.props.scoringMethodType !== "undefined") {
      // if scoringMethodType is THRESHOLD
      if (this.props.scoringMethodType.value === SCORING_METHOD_TYPE.THRESHOLD) {
        // display add scoring threashold button
        this.setState({
          displayAddScoringThresholdButton: true,
          displayPassFailMinimumScoreField: false
        });
        // if scoringMethodType is PASS_FAIL
      } else if (this.props.scoringMethodType.value === SCORING_METHOD_TYPE.PASS_FAIL) {
        // display add scoring threashold button
        this.setState({
          displayAddScoringThresholdButton: false,
          displayPassFailMinimumScoreField: true
        });
        // hide add scoring threashold button and scoring pass fail minimum score field
      } else {
        this.setState({
          displayAddScoringThresholdButton: false,
          displayPassFailMinimumScoreField: false
        });
      }
    } else {
      this.setState({
        displayAddScoringThresholdButton: false,
        displayPassFailMinimumScoreField: false
      });
    }
  };

  getUpdatedScoringThreshold = () => {
    // initializing
    const scoringThresholdArray = this.props.scoringThreshold;
    // looping in scoringThresholdArray
    for (let i = 0; i < scoringThresholdArray.length; i++) {
      // minimum score value is the same as in the template (meaning that the whole scoring threshold contains template values)
      if (
        scoringThresholdArray[i].minimum_score ===
          LOCALIZE_OBJ.en.testBuilder.scoringMethod.scoringThreshold.collapsingItem.minimumScore
            .placeholder ||
        scoringThresholdArray[i].minimum_score ===
          LOCALIZE_OBJ.fr.testBuilder.scoringMethod.scoringThreshold.collapsingItem.minimumScore
            .placeholder
      ) {
        // updating template values (so they are displayed in the right language)
        scoringThresholdArray[i].minimum_score =
          LOCALIZE.testBuilder.scoringMethod.scoringThreshold.collapsingItem.minimumScore.placeholder;
        scoringThresholdArray[i].maximum_score =
          LOCALIZE.testBuilder.scoringMethod.scoringThreshold.collapsingItem.maximumScore.placeholder;
        scoringThresholdArray[i].en_conversion_value =
          LOCALIZE.testBuilder.scoringMethod.scoringThreshold.collapsingItem.enConversionValue.placeholder;
        scoringThresholdArray[i].fr_conversion_value =
          LOCALIZE.testBuilder.scoringMethod.scoringThreshold.collapsingItem.frConversionValue.placeholder;
      }
    }
    this.props.setScoringThreshold(scoringThresholdArray);
  };

  getUpdatedSelectedMethodTypeObject = methodTypeValue => {
    // initializing scoringMethodType object
    let scoringMethodType = "";
    // looping in scoringMethodTypes array
    for (let i = 0; i < this.state.scoringMethodTypes.length; i++) {
      // current method type codename is the same as the one provided
      if (this.state.scoringMethodTypes[i].method_type_codename === methodTypeValue) {
        // updating scoringMethodType object
        scoringMethodType = {
          label: this.state.scoringMethodTypes[i][`${this.props.currentLanguage}_name`],
          value: methodTypeValue
        };
        break;
      }
    }
    return scoringMethodType;
  };

  addScoringThreshold = () => {
    const newObj = {
      id: 1,
      minimum_score:
        LOCALIZE.testBuilder.scoringMethod.scoringThreshold.collapsingItem.minimumScore.placeholder,
      maximum_score:
        LOCALIZE.testBuilder.scoringMethod.scoringThreshold.collapsingItem.maximumScore.placeholder,
      en_conversion_value:
        LOCALIZE.testBuilder.scoringMethod.scoringThreshold.collapsingItem.enConversionValue
          .placeholder,
      fr_conversion_value:
        LOCALIZE.testBuilder.scoringMethod.scoringThreshold.collapsingItem.frConversionValue
          .placeholder
    };
    if (typeof this.props.scoringThreshold !== "undefined") {
      newObj.id = getNextInNumberSeries(this.props.scoringThreshold, "id");
    }
    this.props.addScoringThresholdField(SCORING_THRESHOLD, newObj, "minimum_score");
  };

  makeScoringMethodTypeOptions = (array, lang) => {
    return array.map(item => {
      return { label: item[`${lang}_name`], value: item.method_type_codename };
    });
  };

  selectScoringMethodType = event => {
    // reinitializing error message states
    this.setState(
      {
        displayScoringMethodUndefinedError: false,
        displayScoringThresholdValidationError: false,
        displayScoringPassFailMinimumScoreError: false
      },
      () => {
        this.props.setScoringMethodType(event);
      }
    );
  };

  modifyScoringPassFailMinimumScore = (inputName, value) => {
    // allow only numeric entries
    const regex = /^[0-9]*$/;
    if (value === "" || regex.test(value)) {
      this.setState({ scoringPassFailMinimumScoreContent: value });
    }
  };

  handleDelete = id => {
    // delete the scoring threshold item
    const objArray = this.props.scoringThreshold.filter(obj => obj.id !== id);
    this.props.replaceScoringThresholdField(SCORING_THRESHOLD, objArray, "minimum_score");
  };

  handleApplyScoringThreshold = () => {
    // initializing isValidScoringThresholdCollection
    let isValidScoringThresholdCollection = true;
    if (this.props.scoringThreshold.length <= 0) {
      isValidScoringThresholdCollection = false;
    } else {
      // order the scoring threshold items by minimum score
      const orderedScoringThresholdItems = this.props.scoringThreshold.sort(
        this.orderByMinimumScore
      );
      for (let i = 0; i < orderedScoringThresholdItems.length; i++) {
        // making sure that the current item is not the template
        if (
          orderedScoringThresholdItems[i].minimum_score ===
          LOCALIZE.testBuilder.scoringMethod.scoringThreshold.collapsingItem.minimumScore
            .placeholder
        ) {
          isValidScoringThresholdCollection = false;
          break;
        }
        // making sure that this is not the last item
        if (i < orderedScoringThresholdItems.length - 1) {
          // making sure that the maximum score + 1 of current item is equal to the minimum score of the next item
          if (
            parseInt(orderedScoringThresholdItems[i].maximum_score) + 1 !==
            parseInt(orderedScoringThresholdItems[i + 1].minimum_score)
          ) {
            isValidScoringThresholdCollection = false;
            break;
          }
        }
      }
    }
    // valid scoring threshold collection
    if (isValidScoringThresholdCollection) {
      // remove scoring threshold validation error
      this.setState({
        displayScoringThresholdValidationError: false,
        displayScoringMethodUndefinedError: false
      });
      // set final scoring threshold conversions redux state
      this.props.setFinalScoringThresholdConversions(this.props.scoringThreshold);
      // set scoring method valid state
      this.props.setScoringMethodValidState(true);
      // updating validation errors state
      this.updateTestDefinitionValidationErrorsState();
      // invalid scoring threshold collection
    } else {
      // trigger scoring threshold validation error
      this.setState(
        {
          displayScoringThresholdValidationError: true,
          displayScoringMethodUndefinedError: false
        },
        () => {
          // set scoring method valid state
          this.props.setScoringMethodValidState(false);
        }
      );
    }
  };

  handleApplyScoringPassFailMinimumScore = () => {
    if (
      this.state.scoringPassFailMinimumScoreContent === "" ||
      this.state.scoringPassFailMinimumScoreContent.length <= 0
    ) {
      this.setState({ displayScoringPassFailMinimumScoreError: true }, () => {
        // set scoring method valid state
        this.props.setScoringMethodValidState(false);
      });
    } else {
      this.setState(
        {
          displayScoringPassFailMinimumScoreError: false,
          displayScoringMethodUndefinedError: false
        },
        () => {
          this.props.setScoringPassFailMinimumScore(this.state.scoringPassFailMinimumScoreContent);
          // set scoring method valid state
          this.props.setScoringMethodValidState(true);
          // updating validation errors state
          this.updateTestDefinitionValidationErrorsState();
        }
      );
    }
  };

  handleApplyOtherScoringMethods = () => {
    this.setState({ displayScoringMethodUndefinedError: false }, () => {
      // set scoring method valid state
      this.props.setScoringMethodValidState(true);
      // updating validation errors state
      this.updateTestDefinitionValidationErrorsState();
    });
  };

  handleApplyAction = () => {
    // scoringMethodType is defined
    if (typeof this.props.scoringMethodType !== "undefined") {
      // threshold scoring method
      if (this.props.scoringMethodType.value === SCORING_METHOD_TYPE.THRESHOLD) {
        this.handleApplyScoringThreshold();
        // pass/fail scoring method
      } else if (this.props.scoringMethodType.value === SCORING_METHOD_TYPE.PASS_FAIL) {
        this.handleApplyScoringPassFailMinimumScore();
        // other scoring methods
      } else {
        this.handleApplyOtherScoringMethods();
      }
      // scoringMethodType is undefined
    } else {
      this.setState({ displayScoringMethodUndefinedError: true }, () => {
        // set scoring method valid state
        this.props.setScoringMethodValidState(false);
      });
    }
  };

  // this function should only be called when the scoring method valid state is set to true
  updateTestDefinitionValidationErrorsState = () => {
    // creating a copy of validationErrors
    const copyOfValidationErrors = this.props.validationErrors;
    // setting testDefinitionContainsValidScoringMethod to true
    copyOfValidationErrors.testDefinitionContainsValidScoringMethod = true;
    // updating needed states
    this.props.setTestDefinitionValidationErrors(copyOfValidationErrors);
    this.setState({ showInvalidScoringMethodError: false });
  };

  getDropdownValidityStatus = () => {
    if (
      this.state.displayScoringMethodUndefinedError ||
      this.state.displayScoringThresholdValidationError
    ) {
      return false;
    }
    return true;
  };

  orderByMinimumScore = (a, b) => {
    if (parseInt(a.minimum_score) < parseInt(b.minimum_score)) {
      return -1;
    }
    if (parseInt(a.minimum_score) > parseInt(b.minimum_score)) {
      return 1;
    }
    return 0;
  };

  render() {
    const options = this.makeScoringMethodTypeOptions(
      this.state.scoringMethodTypes,
      this.props.currentLanguage
    );

    const customFontSize = {
      // adding 5px to current font size
      fontSize: `${parseInt(this.props.accommodations.fontSize.split("px")[0]) + 5}px`
    };

    return (
      <div style={styles.mainContainer}>
        <h2>{LOCALIZE.testBuilder.scoringMethod.title}</h2>
        {this.state.showInvalidScoringMethodError && (
          <div style={styles.validationErrorMessageContainer}>
            <p style={{ ...styles.validationErrorMessage, ...customFontSize }}>
              {LOCALIZE.testBuilder.scoringMethod.validationErrors.mustApplyScoringMethodError}
            </p>
          </div>
        )}
        <p>{LOCALIZE.testBuilder.scoringMethod.description}</p>
        <Container>
          <form onChange={this.onChange}>
            <Row style={styles.rowStyle}>
              {makeLabel("scoringMethodType", LOCALIZE.testBuilder.scoringMethod)}
              {makeDropDownField(
                "scoringMethodType",
                LOCALIZE.testBuilder.scoringMethod,
                this.state.selectedScoringMethodType,
                options,
                this.selectScoringMethodType,
                false,
                this.getDropdownValidityStatus()
              )}
            </Row>
          </form>
        </Container>
        {this.state.displayAddScoringThresholdButton && (
          <Button variant="primary" onClick={this.addScoringThreshold}>
            <FontAwesomeIcon icon={faPlusCircle} style={{ margins: "auto", marginRight: "5px" }} />
            {LOCALIZE.testBuilder.scoringMethod.scoringThreshold.addButton}
          </Button>
        )}
        <div style={styles.borderBox} />
        {this.state.selectedScoringMethodType !== "" && (
          <div>
            <div>
              {this.state.displayPassFailMinimumScoreField && (
                <div style={styles.passFailMainContainer}>
                  <Container>
                    <div>
                      <form onChange={this.onChange}>
                        <Row style={styles.rowStyle}>
                          {makeLabel(
                            "scoringPassFailMinimumScore",
                            LOCALIZE.testBuilder.scoringMethod
                          )}
                          {makeTextBoxField(
                            "scoringPassFailMinimumScore",
                            LOCALIZE.testBuilder.scoringMethod,
                            this.state.scoringPassFailMinimumScoreContent,
                            !this.state.displayScoringPassFailMinimumScoreError,
                            this.modifyScoringPassFailMinimumScore,
                            "scoring-pass-fail-minimum-score-field"
                          )}
                        </Row>
                      </form>
                    </div>
                  </Container>
                </div>
              )}
            </div>
            <div>
              {this.state.displayAddScoringThresholdButton &&
                typeof this.props.scoringThreshold !== "undefined" && (
                  <div>
                    <div
                      style={
                        this.state.displayScoringThresholdValidationError
                          ? styles.collapsingItemContainerBorder
                          : {}
                      }
                    >
                      {this.props.scoringThreshold.map((scoringThreashold, index) => {
                        return (
                          <CollapsingItemContainer
                            key={index}
                            index={index}
                            triggerStateReset={this.state.triggerCollapsingItemContainerStateReset}
                            title={
                              <label>
                                {LOCALIZE.formatString(
                                  LOCALIZE.testBuilder.scoringMethod.scoringThreshold.collapsingItem
                                    .title,
                                  scoringThreashold.minimum_score !== "" &&
                                    scoringThreashold.minimum_score !==
                                      LOCALIZE.testBuilder.scoringMethod.scoringThreshold
                                        .collapsingItem.minimumScore.placeholder
                                    ? scoringThreashold.minimum_score
                                    : LOCALIZE.testBuilder.scoringMethod.scoringThreshold
                                        .collapsingItem.minimumScore.placeholder,
                                  scoringThreashold.maximum_score !== "" &&
                                    scoringThreashold.maximum_score !==
                                      LOCALIZE.testBuilder.scoringMethod.scoringThreshold
                                        .collapsingItem.maximumScore.placeholder
                                    ? scoringThreashold.maximum_score
                                    : LOCALIZE.testBuilder.scoringMethod.scoringThreshold
                                        .collapsingItem.maximumScore.placeholder,
                                  scoringThreashold[
                                    `${this.props.currentLanguage}_conversion_value`
                                  ] !== "" &&
                                    scoringThreashold[
                                      `${this.props.currentLanguage}_conversion_value`
                                    ] !==
                                      LOCALIZE.testBuilder.scoringMethod.scoringThreshold
                                        .collapsingItem[
                                        `${this.props.currentLanguage}ConversionValue`
                                      ].placeholder
                                    ? scoringThreashold[
                                        `${this.props.currentLanguage}_conversion_value`
                                      ]
                                    : LOCALIZE.testBuilder.scoringMethod.scoringThreshold
                                        .collapsingItem[
                                        `${this.props.currentLanguage}ConversionValue`
                                      ].placeholder
                                )}
                              </label>
                            }
                            body={
                              <ScoringThresholdForm
                                selectedScoringThresholdIndex={index}
                                scoringThreasholdElementId={scoringThreashold.id}
                                scoringThreshold={this.props.scoringThreshold}
                                handleDelete={this.handleDelete}
                              />
                            }
                          />
                        );
                      })}
                    </div>
                    <div>
                      {this.state.displayScoringThresholdValidationError && (
                        <div style={styles.errorMessage} className="notranslate">
                          {
                            LOCALIZE.testBuilder.scoringMethod.scoringThreshold
                              .invalidCollectionError
                          }
                        </div>
                      )}
                    </div>
                  </div>
                )}
            </div>
            {this.state.displayScoringMethodUndefinedError && (
              <div style={styles.invalidScoringMethodErrorContainer}>
                <div style={styles.errorMessage} className="notranslate">
                  {LOCALIZE.testBuilder.scoringMethod.scoringMethodUndefinedError}
                </div>
              </div>
            )}
            <div style={styles.applyButtonContainer}>
              <CustomButton
                label={
                  <>
                    <FontAwesomeIcon icon={faCheck} />
                    <span style={styles.buttonLabel}>{LOCALIZE.commons.applyButton}</span>
                  </>
                }
                action={this.handleApplyAction}
                customStyle={styles.applyButton}
                buttonTheme={THEME.PRIMARY}
              />
            </div>
          </div>
        )}
      </div>
    );
  }
}

export { ScoringMethod as unconnectedScoringMethod };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    scoringMethodType: state.testBuilder.scoring_method_type,
    scoringMethodValidState: state.testBuilder.scoring_method_valid_state,
    scoringThreshold: state.testBuilder.scoring_threshold,
    scoringPassFailMinimumScore: state.testBuilder.scoring_pass_fail_minimum_score,
    validationErrors: state.testBuilder.validation_errors,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getScoringMethodTypes,
      setScoringMethodType,
      setScoringPassFailMinimumScore,
      addScoringThresholdField,
      replaceScoringThresholdField,
      setFinalScoringThresholdConversions,
      setScoringMethodValidState,
      setScoringThreshold,
      setTestDefinitionValidationErrors
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ScoringMethod);
