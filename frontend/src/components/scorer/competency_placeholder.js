export const competencies = {
  bar_score_internal_name: "emibPizzaTestBarScore",
  parent_test: "emibPizzaTest",
  scoring_key: {
    bar_score: [
      {
        question_id: 1,
        competencies: [
          {
            en_type: "comp1",
            fr_type: "FR comp1",
            en_situation: "Situational placeholder text #1",
            fr_situation: "FR Situational placeholder text #1",
            range: { start: 1, end: 7, increment: 1 },
            rating_list: [
              {
                point_value: 1.5,
                text_en: "#1 comp1; supporting text for 1.5",
                text_fr: "FR #1 comp1; supporting text for 1.5"
              },
              {
                point_value: 2.6,
                text_en: "#1 comp1; supporting text for 2.6",
                text_fr: "FR #1 comp1; supporting text for 2.6"
              },
              {
                point_value: 3.4,
                text_en: "#1 comp1; supporting text for 3.4",
                text_fr: "FR #1 comp1; supporting text for 3.4"
              },
              {
                point_value: 4.6,
                text_en: "#1 comp1; supporting text for 4.6",
                text_fr: "FR #1 comp1; supporting text for 4.6"
              },
              {
                point_value: 5.3,
                text_en: "#1 comp1; supporting text for 5.3",
                text_fr: "FR #1 comp1; supporting text for 5.3"
              },
              {
                point_value: 6.8,
                text_en: "#1 comp1; supporting text for 6.8",
                text_fr: "FR #1 comp1; supporting text for 6.8"
              }
            ]
          },
          {
            en_type: "comp2",
            fr_type: "FR comp2",
            en_situation: "Situational placeholder text #2",
            fr_situation: "FR Situational placeholder text #2",
            range: { start: 1, end: 7, increment: 1 },
            rating_list: [
              {
                point_value: 1.1,
                text_en: "#1 comp2; supporting text for 1.1",
                text_fr: "FR #1 comp2; supporting text for 1.1"
              },
              {
                point_value: 2.1,
                text_en: "#1 comp2; supporting text for 2.1",
                text_fr: "FR #1 comp2; supporting text for 2.1"
              },
              {
                point_value: 3.1,
                text_en: "#1 comp2; supporting text for 3.1",
                text_fr: "FR #1 comp2; supporting text for 3.1"
              },
              {
                point_value: 4.1,
                text_en: "#1 comp2; supporting text for 4.1",
                text_fr: "FR #1 comp2; supporting text for 4.1"
              },
              {
                point_value: 5.1,
                text_en: "#1 comp2; supporting text for 5.1",
                text_fr: "FR #1 comp2; supporting text for 5.1"
              },
              {
                point_value: 6.1,
                text_en: "#1 comp2; supporting text for 6.1",
                text_fr: "FR #1 comp2; supporting text for 6.1"
              }
            ]
          },
          {
            en_type: "comp3",
            fr_type: "FR comp3",
            en_situation: "Situational placeholder text #3",
            fr_situation: "FR Situational placeholder text #3",
            range: { start: 1, end: 7, increment: 1 },
            rating_list: [
              {
                point_value: 1.2,
                text_en: "#1 comp3; supporting text for 1.2",
                text_fr: "FR #1 comp3; supporting text for 1.2"
              },
              {
                point_value: 2.2,
                text_en: "#1 comp3; supporting text for 2.2",
                text_fr: "FR #1 comp3; supporting text for 2.2"
              },
              {
                point_value: 3.2,
                text_en: "#1 comp3; supporting text for 3.2",
                text_fr: "FR #1 comp3; supporting text for 3.2"
              },
              {
                point_value: 4.2,
                text_en: "#1 comp3; supporting text for 4.2",
                text_fr: "FR #1 comp3; supporting text for 4.2"
              },
              {
                point_value: 5.2,
                text_en: "#1 comp3; supporting text for 5.2",
                text_fr: "FR #1 comp3; supporting text for 5.2"
              },
              {
                point_value: 6.2,
                text_en: "#1 comp3; supporting text for 6.2",
                text_fr: "FR #1 comp3; supporting text for 6.2"
              }
            ]
          }
        ]
      },
      {
        question_id: 2,
        competencies: [
          {
            en_type: "comp4",
            fr_type: "FR comp4",
            en_situation: "Situational placeholder text #4",
            fr_situation: "FR Situational placeholder text #4",
            range: { start: 1, end: 7, increment: 1 },
            rating_list: [
              {
                point_value: 1.3,
                text_en: "#2 comp4; supporting text for 1.3",
                text_fr: "FR #2 comp4; supporting text for 1.3"
              },
              {
                point_value: 2.3,
                text_en: "#2 comp4; supporting text for 2.3",
                text_fr: "FR #2 comp4; supporting text for 2.3"
              },
              {
                point_value: 3.3,
                text_en: "#2 comp4; supporting text for 3.3",
                text_fr: "FR #2 comp4; supporting text for 3.3"
              },
              {
                point_value: 4.3,
                text_en: "#2 comp4; supporting text for 4.3",
                text_fr: "FR #2 comp4; supporting text for 4.3"
              },
              {
                point_value: 5.3,
                text_en: "#2 comp4; supporting text for 5.3",
                text_fr: "FR #2 comp4; supporting text for 5.3"
              },
              {
                point_value: 6.3,
                text_en: "#2 comp4; supporting text for 6.3",
                text_fr: "FR #2 comp4; supporting text for 6.3"
              }
            ]
          },
          {
            en_type: "comp5",
            fr_type: "FR comp5",
            en_situation: "Situational placeholder text #5",
            fr_situation: "FR Situational placeholder text #5",
            range: { start: 1, end: 7, increment: 1 },
            rating_list: [
              {
                point_value: 1.4,
                text_en: "#2 comp5; supporting text for 1.4",
                text_fr: "FR #2 comp5; supporting text for 1.4"
              },
              {
                point_value: 2.4,
                text_en: "#2 comp5; supporting text for 2.4",
                text_fr: "FR #2 comp5; supporting text for 2.4"
              },
              {
                point_value: 3.4,
                text_en: "#2 comp5; supporting text for 3.4",
                text_fr: "FR #2 comp5; supporting text for 3.4"
              },
              {
                point_value: 4.4,
                text_en: "#2 comp5; supporting text for 4.4",
                text_fr: "FR #2 comp5; supporting text for 4.4"
              },
              {
                point_value: 5.4,
                text_en: "#2 comp5; supporting text for 5.4",
                text_fr: "FR #2 comp5; supporting text for 5.4"
              },
              {
                point_value: 6.4,
                text_en: "#2 comp5; supporting text for 6.4",
                text_fr: "FR #2 comp5; supporting text for 6.4"
              }
            ]
          }
        ]
      },
      {
        question_id: 3,
        competencies: [
          {
            en_type: "comp6",
            fr_type: "FR comp6",
            en_situation: "Situational placeholder text #6",
            fr_situation: "FR Situational placeholder text #6",
            range: { start: 1, end: 7, increment: 1 },
            rating_list: [
              {
                point_value: 1.5,
                text_en: "#3 comp6; supporting text for 1.5",
                text_fr: "FR #3 comp6; supporting text for 1.5"
              },
              {
                point_value: 2.5,
                text_en: "#3 comp6; supporting text for 2.5",
                text_fr: "FR #3 comp6; supporting text for 2.5"
              },
              {
                point_value: 3.5,
                text_en: "#3 comp6; supporting text for 3.5",
                text_fr: "FR #3 comp6; supporting text for 3.5"
              },
              {
                point_value: 4.5,
                text_en: "#3 comp6; supporting text for 4.5",
                text_fr: "FR #3 comp6; supporting text for 4.5"
              },
              {
                point_value: 5.5,
                text_en: "#3 comp6; supporting text for 5.5",
                text_fr: "FR #3 comp6; supporting text for 5.5"
              },
              {
                point_value: 6.5,
                text_en: "#3 comp6; supporting text for 6.5",
                text_fr: "FR #3 comp6; supporting text for 6.5"
              }
            ]
          }
        ]
      },
      {
        question_id: 4,
        competencies: [
          {
            en_type: "comp7",
            fr_type: "FR comp7",
            en_situation: "Situational placeholder text #7",
            fr_situation: "FR Situational placeholder text #7",
            range: { start: 1, end: 7, increment: 1 },
            rating_list: [
              {
                point_value: 1.6,
                text_en: "#4 comp7; supporting text for 1.6",
                text_fr: "FR #4 comp7; supporting text for 1.6"
              },
              {
                point_value: 2.6,
                text_en: "#4 comp7; supporting text for 2.6",
                text_fr: "FR #4 comp7; supporting text for 2.6"
              },
              {
                point_value: 3.6,
                text_en: "#4 comp7; supporting text for 3.6",
                text_fr: "FR #4 comp7; supporting text for 3.6"
              },
              {
                point_value: 4.6,
                text_en: "#4 comp7; supporting text for 4.6",
                text_fr: "FR #4 comp7; supporting text for 4.6"
              },
              {
                point_value: 5.6,
                text_en: "#4 comp7; supporting text for 5.6",
                text_fr: "FR #4 comp7; supporting text for 5.6"
              },
              {
                point_value: 6.6,
                text_en: "#4 comp7; supporting text for 6.6",
                text_fr: "FR #4 comp7; supporting text for 6.6"
              }
            ]
          }
        ]
      },
      {
        question_id: 5,
        competencies: [
          {
            en_type: "comp8",
            fr_type: "FR comp8",
            en_situation: "Situational placeholder text #8",
            fr_situation: "FR Situational placeholder text #8",
            range: { start: 1, end: 7, increment: 1 },
            rating_list: [
              {
                point_value: 1.7,
                text_en: "#5 comp8; supporting text for 1.7",
                text_fr: "FR #5 comp8; supporting text for 1.7"
              },
              {
                point_value: 2.7,
                text_en: "#5 comp8; supporting text for 2.7",
                text_fr: "FR #5 comp8; supporting text for 2.7"
              },
              {
                point_value: 3.7,
                text_en: "#5 comp8; supporting text for 3.7",
                text_fr: "FR #5 comp8; supporting text for 3.7"
              },
              {
                point_value: 4.7,
                text_en: "#5 comp8; supporting text for 4.7",
                text_fr: "FR #5 comp8; supporting text for 4.7"
              },
              {
                point_value: 5.7,
                text_en: "#5 comp8; supporting text for 5.7",
                text_fr: "FR #5 comp8; supporting text for 5.7"
              },
              {
                point_value: 6.7,
                text_en: "#5 comp8; supporting text for 6.7",
                text_fr: "FR #5 comp8; supporting text for 6.7"
              }
            ]
          }
        ]
      },
      {
        question_id: 6,
        competencies: [
          {
            en_type: "comp9",
            fr_type: "FR comp9",
            en_situation: "Situational placeholder text #9",
            fr_situation: "FR Situational placeholder text #9",
            range: { start: 1, end: 7, increment: 1 },
            rating_list: [
              {
                point_value: 1.8,
                text_en: "#6 comp9; supporting text for 1.8",
                text_fr: "FR #6 comp9; supporting text for 1.8"
              },
              {
                point_value: 2.8,
                text_en: "#6 comp9; supporting text for 2.8",
                text_fr: "FR #6 comp9; supporting text for 2.8"
              },
              {
                point_value: 3.8,
                text_en: "#6 comp9; supporting text for 3.8",
                text_fr: "FR #6 comp9; supporting text for 3.8"
              },
              {
                point_value: 4.8,
                text_en: "#6 comp9; supporting text for 4.8",
                text_fr: "FR #6 comp9; supporting text for 4.8"
              },
              {
                point_value: 5.8,
                text_en: "#6 comp9; supporting text for 5.8",
                text_fr: "FR #6 comp9; supporting text for 5.8"
              },
              {
                point_value: 6.8,
                text_en: "#6 comp9; supporting text for 6.8",
                text_fr: "FR #6 comp9; supporting text for 6.8"
              }
            ]
          }
        ]
      },
      {
        question_id: 7,
        competencies: [
          {
            en_type: "comp10",
            fr_type: "FR comp10",
            en_situation: "Situational placeholder text #10",
            fr_situation: "FR Situational placeholder text #10",
            range: { start: 1, end: 7, increment: 1 },
            rating_list: [
              {
                point_value: 1.9,
                text_en: "#7 comp10; supporting text for 1.9",
                text_fr: "FR #7 comp10; supporting text for 1.9"
              },
              {
                point_value: 2.9,
                text_en: "#7 comp10; supporting text for 2.9",
                text_fr: "FR #7 comp10; supporting text for 2.9"
              },
              {
                point_value: 3.9,
                text_en: "#7 comp10; supporting text for 3.9",
                text_fr: "FR #7 comp10; supporting text for 3.9"
              },
              {
                point_value: 4.9,
                text_en: "#7 comp10; supporting text for 4.9",
                text_fr: "FR #7 comp10; supporting text for 4.9"
              },
              {
                point_value: 5.9,
                text_en: "#7 comp10; supporting text for 5.9",
                text_fr: "FR #7 comp10; supporting text for 5.9"
              },
              {
                point_value: 6.9,
                text_en: "#7 comp10; supporting text for 6.9",
                text_fr: "FR #7 comp10; supporting text for 6.9"
              }
            ]
          }
        ]
      },
      {
        question_id: 8,
        competencies: [
          {
            en_type: "comp11",
            fr_type: "FR comp11",
            en_situation: "Situational placeholder text #11",
            fr_situation: "FR Situational placeholder text #11",
            range: { start: 1, end: 7, increment: 1 },
            rating_list: [
              {
                point_value: 1.1,
                text_en: "#8 comp11; supporting text for 1.1",
                text_fr: "FR #8 comp11; supporting text for 1.1"
              },
              {
                point_value: 2.1,
                text_en: "#8 comp11; supporting text for 2.1",
                text_fr: "FR #8 comp11; supporting text for 2.1"
              },
              {
                point_value: 3.1,
                text_en: "#8 comp11; supporting text for 3.1",
                text_fr: "FR #8 comp11; supporting text for 3.1"
              },
              {
                point_value: 4.1,
                text_en: "#8 comp11; supporting text for 4.1",
                text_fr: "FR #8 comp11; supporting text for 4.1"
              },
              {
                point_value: 5.1,
                text_en: "#8 comp11; supporting text for 5.1",
                text_fr: "FR #8 comp11; supporting text for 5.1"
              },
              {
                point_value: 6.1,
                text_en: "#8 comp11; supporting text for 6.1",
                text_fr: "FR #8 comp11; supporting text for 6.1"
              }
            ]
          }
        ]
      },
      {
        question_id: 9,
        competencies: [
          {
            en_type: "comp12",
            fr_type: "FR comp12",
            en_situation: "Situational placeholder text #12",
            fr_situation: "FR Situational placeholder text #12",
            range: { start: 1, end: 7, increment: 1 },
            rating_list: [
              {
                point_value: 1.2,
                text_en: "#9 comp12; supporting text for 1.2",
                text_fr: "FR #9 comp12; supporting text for 1.2"
              },
              {
                point_value: 2.2,
                text_en: "#9 comp12; supporting text for 2.2",
                text_fr: "FR #9 comp12; supporting text for 2.2"
              },
              {
                point_value: 3.2,
                text_en: "#9 comp12; supporting text for 3.2",
                text_fr: "FR #9 comp12; supporting text for 3.2"
              },
              {
                point_value: 4.2,
                text_en: "#9 comp12; supporting text for 4.2",
                text_fr: "FR #9 comp12; supporting text for 4.2"
              },
              {
                point_value: 5.2,
                text_en: "#9 comp12; supporting text for 5.2",
                text_fr: "FR #9 comp12; supporting text for 5.2"
              },
              {
                point_value: 6.2,
                text_en: "#9 comp12; supporting text for 6.2",
                text_fr: "FR #9 comp12; supporting text for 6.2"
              }
            ]
          }
        ]
      },
      {
        question_id: 10,
        competencies: [
          {
            en_type: "comp13",
            fr_type: "FR comp13",
            en_situation: "Situational placeholder text #13",
            fr_situation: "FR Situational placeholder text #13",
            range: { start: 1, end: 7, increment: 1 },
            rating_list: [
              {
                point_value: 1.3,
                text_en: "#10 comp13; supporting text for 1.3",
                text_fr: "FR #10 comp13; supporting text for 1.3"
              },
              {
                point_value: 2.3,
                text_en: "#10 comp13; supporting text for 2.3",
                text_fr: "FR #10 comp13; supporting text for 2.3"
              },
              {
                point_value: 3.3,
                text_en: "#10 comp13; supporting text for 3.3",
                text_fr: "FR #10 comp13; supporting text for 3.3"
              },
              {
                point_value: 4.3,
                text_en: "#10 comp13; supporting text for 4.3",
                text_fr: "FR #10 comp13; supporting text for 4.3"
              },
              {
                point_value: 5.3,
                text_en: "#10 comp13; supporting text for 5.3",
                text_fr: "FR #10 comp13; supporting text for 5.3"
              },
              {
                point_value: 6.3,
                text_en: "#10 comp13; supporting text for 6.3",
                text_fr: "FR #10 comp13; supporting text for 6.3"
              }
            ]
          }
        ]
      }
    ]
  }
};
