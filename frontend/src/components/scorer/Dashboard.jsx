import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import PropTypes from "prop-types";
import LOCALIZE from "../../text_resources";
import { LANGUAGES } from "../../modules/LocalizeRedux";
import { PATH } from "../commons/Constants";
import ContentContainer from "../commons/ContentContainer";
import { getAssignedTests, setScorerTest } from "../../modules/ScorerRedux";
import { setContentUnLoaded } from "../../modules/TestSectionRedux";
import ScorerTable from "./ScorerTable";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMarker } from "@fortawesome/free-solid-svg-icons";
import { COMMON_STYLE } from "../commons/GenericTable";
import { withRouter } from "react-router-dom";

export const styles = {
  header: {
    marginBottom: 24
  },
  scoreTestBtn: {
    minWidth: 125,
    padding: "2px 10px"
  },
  scoreText: {
    paddingLeft: "10px",
    margin: 0
  }
};

class Dashboard extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  static propTypes = {
    // provided by redux
    getAssignedTests: PropTypes.func,
    username: PropTypes.string.isRequired,
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    currentLanguage: PropTypes.string.isRequired,
    scorerPaginationPageSize: PropTypes.number.isRequired
  };

  state = {
    numberOfPages: 1,
    nextPageNumber: 0,
    previousPageNumber: 0,
    currentlyLoading: false,
    assignedTests: [],
    rowsDefinition: {}
  };

  componentDidMount = () => {
    this._isMounted = true;
    this.populateAssignedTests();
  };

  componentWillUnmount = () => {
    this._isMounted = false;
  };

  componentDidUpdate = prevProps => {
    if (prevProps.currentLanguage !== this.props.currentLanguage) {
      this.populateAssignedTests();
    }
    if (prevProps.scorerPaginationPage !== this.props.scorerPaginationPage) {
      this.populateAssignedTests();
    }
    if (prevProps.scorerPaginationPageSize !== this.props.scorerPaginationPageSize) {
      this.populateAssignedTests();
    }
  };

  populateAssignedTests = () => {
    if (this._isMounted) {
      this.setState({ currentlyLoading: true }, () => {
        const assignedTests = [];
        this.props
          .getAssignedTests(
            this.props.username,
            this.props.scorerPaginationPage,
            this.props.scorerPaginationPageSize
          )
          .then(response => {
            this.populateAssignedTestsObject(assignedTests, response);
          })
          .then(() => {
            if (this._isMounted) {
              this.setState({ currentlyLoading: false });
            }
          });
      });
    }
  };

  populateAssignedTestsObject = (assignedTests, response) => {
    // initializing needed object and array for rowsDefinition props (needed for GenericTable component)
    let rowsDefinition = {};
    const data = [];
    // looping in response given
    for (let i = 0; i < response.results.length; i++) {
      const result = response.results[i];
      // pushing needed data in assigned tests array
      assignedTests.push({
        id: result.id,
        assigned_test_id: result.assigned_test_id,
        assigned_test_status: result.assigned_test_status,
        assigned_test_start_date: result.assigned_test_start_date,
        assigned_test_code: result.assigned_test_test_code,
        assigned_test_en_name: result.assigned_test_en_name,
        assigned_test_fr_name: result.assigned_test_fr_name,
        assigned_test_submit_date: result.assigned_test_submit_date,
        assigned_test_test_session_language: result.assigned_test_test_session_language,
        scorer_username: result.scorer_username,
        status: result.status,
        score_date: result.score_date,
        assigned_test: result.assigned_test
      });
      // pushing needed data in data array (needed for GenericTable component)
      data.push({
        column_1: result.assigned_test_id,
        column_2: result.assigned_test_submit_date,
        column_3: result[`assigned_test_${this.props.currentLanguage}_name`],
        column_4:
          result.assigned_test_test_session_language === LANGUAGES.english
            ? LOCALIZE.scorer.assignedTest.table.en
            : result.assigned_test_test_session_language === LANGUAGES.french
            ? LOCALIZE.scorer.assignedTest.table.fr
            : null,
        column_5: (
          <button
            id={`score-button-row-${i}`}
            className="btn btn-secondary"
            style={styles.scoreTestBtn}
            onClick={() => this.scoreTest(result.assigned_test_id)}
            aria-label={LOCALIZE.formatString(
              LOCALIZE.scorer.assignedTest.table.actionButtonAriaLabel,
              result.assigned_test_id,
              result.assigned_test_submit_date,
              result[`assigned_test_${this.props.currentLanguage}_name`],
              result.assigned_test_test_session_language === LANGUAGES.english
                ? LOCALIZE.scorer.assignedTest.table.en
                : result.assigned_test_test_session_language === LANGUAGES.french
                ? LOCALIZE.scorer.assignedTest.table.fr
                : null
            )}
          >
            <FontAwesomeIcon icon={faMarker} />
            <label style={styles.scoreText}>
              {LOCALIZE.scorer.assignedTest.table.actionButtonLabel}
            </label>
          </button>
        )
      });
    }

    // updating rowsDefinition object with provided data and needed style
    rowsDefinition = {
      column_1_style: {},
      column_2_style: COMMON_STYLE.CENTERED_TEXT,
      column_3_style: COMMON_STYLE.CENTERED_TEXT,
      column_4_style: COMMON_STYLE.CENTERED_TEXT,
      column_5_style: COMMON_STYLE.CENTERED_TEXT,
      data: data
    };

    // saving results in state
    if (this._isMounted) {
      this.setState({
        assignedTests: assignedTests,
        rowsDefinition: rowsDefinition,
        numberOfPages: Math.ceil(response.count / this.props.scorerPaginationPageSize),
        nextPageNumber: response.next_page_number,
        previousPageNumber: response.previous_page_number
      });
    }
  };

  scoreTest = assigned_test_id => {
    this.props.setContentUnLoaded();
    this.props.setScorerTest(assigned_test_id);
    this.props.history.push(PATH.scorerBase + PATH.scorerTest);
    // TODO set as actively scored test; possibly assign to TA for now; update status
  };

  render() {
    return (
      <div>
        <ContentContainer>
          <div>
            <div id="main-content" role="main">
              <div
                id="user-welcome-message-div"
                style={styles.header}
                tabIndex={0}
                aria-labelledby="user-welcome-message"
                className="notranslate"
              >
                <h1 id="user-welcome-message" className="green-divider">
                  {LOCALIZE.formatString(
                    LOCALIZE.profile.title,
                    this.props.firstName,
                    this.props.lastName
                  )}
                </h1>
              </div>
            </div>
            <ScorerTable
              currentLanguage={this.props.currentLanguage}
              numberOfPages={this.state.numberOfPages}
              currentlyLoading={this.state.currentlyLoading}
              assignedTests={this.state.assignedTests}
              rowsDefinition={this.state.rowsDefinition}
              scoreTest={this.scoreTest}
            ></ScorerTable>
          </div>
        </ContentContainer>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    username: state.user.username,
    firstName: state.user.firstName,
    lastName: state.user.lastName,
    currentLanguage: state.localize.language,
    scorerPaginationPage: state.scorer.scorerPaginationPage,
    scorerPaginationPageSize: state.scorer.scorerPaginationPageSize
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAssignedTests,
      setScorerTest,
      setContentUnLoaded
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Dashboard));
