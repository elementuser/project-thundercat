import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import LOCALIZE from "../../text_resources";
import ReactPaginate from "react-paginate";
import Select from "react-select";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCaretLeft, faCaretRight } from "@fortawesome/free-solid-svg-icons";
import getDisplayOptions from "../../helpers/getDisplayOptions";
import { bindActionCreators } from "redux";
import {
  updateCurrentScorerAssignedTestPageState,
  updateScorerAssignedTestPageSizeState
} from "../../modules/ScorerRedux";
import "../../css/etta.css";
import { styles as ActivePermissionsStyles } from "../etta/permissions/ActivePermissions";
import GenericTable, { COMMON_STYLE } from "../commons/GenericTable";

export const styles = {
  ...ActivePermissionsStyles,
  ...{
    spacingDiv: {
      position: "relative",
      width: 950,
      display: "table-cell"
    }
  }
};

class ScorerTable extends Component {
  static propTypes = {
    currentLanguage: PropTypes.string.isRequired,
    numberOfPages: PropTypes.number.isRequired,
    currentlyLoading: PropTypes.bool.isRequired,
    assignedTests: PropTypes.array.isRequired,
    rowsDefinition: PropTypes.object.isRequired,
    scoreTest: PropTypes.func,
    // props from redux
    updateCurrentScorerAssignedTestPageState: PropTypes.func,
    updateScorerAssignedTestPageSizeState: PropTypes.func,
    scorerPaginationPage: PropTypes.number.isRequired,
    scorerPaginationPageSize: PropTypes.number.isRequired
  };

  state = {
    displayOptionsArray: [],
    displayOptionSelectedValue: {
      label: `${this.props.scorerPaginationPageSize}`,
      value: this.props.scorerPaginationPageSize
    },
    assignedTests: [],
    rowsDefinition: {}
  };

  componentDidMount = () => {
    this.populateDisplayOptions();
  };

  componentDidUpdate = prevProps => {
    if (prevProps.assignedTests !== this.props.assignedTests) {
      this.setState({ assignedTests: this.props.assignedTests });
    }
    if (prevProps.rowsDefinition !== this.props.rowsDefinition) {
      this.setState({ rowsDefinition: this.props.rowsDefinition });
    }
  };

  // handling page changes based on pagination selection
  handlePageChange = id => {
    // "+1" because redux scorerPaginationPage uses index 1 and react-paginate (id) uses index 0
    const selectedPage = id.selected + 1;
    // update current page redux state
    this.props.updateCurrentScorerAssignedTestPageState(selectedPage);
    // focusing on the first table's row
    document.getElementById("score-button-row-0").focus();
  };

  getSelectedDisplayOption = selectedOption => {
    this.setState(
      {
        displayOptionSelectedValue: selectedOption
      },
      () => {
        // update page size
        this.props.updateScorerAssignedTestPageSizeState(selectedOption.value);
        // go back to the first page to avoid display bugs
        this.props.updateCurrentScorerAssignedTestPageState(1);
      }
    );
  };

  // populate display options
  populateDisplayOptions = () => {
    this.setState({ displayOptionsArray: getDisplayOptions() });
  };

  render() {
    const columnsDefinition = [
      {
        label: LOCALIZE.scorer.assignedTest.table.assignedTestId,
        style: { width: "20%" }
      },
      {
        label: LOCALIZE.scorer.assignedTest.table.completionDate,
        style: { ...{ width: "20%" }, ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label: LOCALIZE.scorer.assignedTest.table.testName,
        style: { ...{ width: "20%" }, ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label: LOCALIZE.scorer.assignedTest.table.testLanguage,
        style: { ...{ width: "20%" }, ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label: LOCALIZE.scorer.assignedTest.table.action,
        style: { ...{ width: "20%" }, ...COMMON_STYLE.CENTERED_TEXT }
      }
    ];
    return (
      <div>
        <div style={styles.spacingDiv} />
        <div style={styles.displayOptionContainer}>
          <div style={styles.displayOptionLabel}>
            <label id="display-options-label">
              {LOCALIZE.scorer.assignedTest.displayOptionLabel}
            </label>
            <label id="display-option-accessibility" style={styles.hiddenText}>
              {LOCALIZE.scorer.assignedTest.displayOptionAccessibility}
            </label>
          </div>
          <div style={styles.displayOptionDropdown}>
            <Select
              id="display-options-dropdown"
              name="display-options"
              aria-labelledby="display-options-label display-option-accessibility display-option-current-value-accessibility"
              placeholder=""
              options={this.state.displayOptionsArray}
              onChange={this.getSelectedDisplayOption}
              clearable={false}
              value={this.state.displayOptionSelectedValue}
            ></Select>
            <label
              id="display-option-current-value-accessibility"
              style={styles.hiddenText}
            >{`${LOCALIZE.scorer.assignedTest.displayOptionCurrentValueAccessibility} ${this.state.displayOptionSelectedValue.value}`}</label>
          </div>
        </div>
        <GenericTable
          classnamePrefix="scorer"
          columnsDefinition={columnsDefinition}
          rowsDefinition={this.state.rowsDefinition}
          emptyTableMessage={LOCALIZE.scorer.assignedTest.noResultsFound}
          currentlyLoading={this.props.currentlyLoading}
        />
        <div style={styles.paginationContainer}>
          {/* TODO: add SearchBarWithDisplayOptions + replace ReactPaginate with Pagination component */}
          <ReactPaginate
            pageCount={this.props.numberOfPages}
            containerClassName={"pagination"}
            breakClassName={"break"}
            activeClassName={"active-page"}
            marginPagesDisplayed={3}
            // "-1" because react-paginate uses index 0 and scorerPaginationPage redux state uses index 1
            forcePage={this.props.scorerPaginationPage - 1}
            onPageChange={page => {
              this.handlePageChange(page);
            }}
            previousLabel={
              <div style={styles.paginationIcon}>
                <label style={styles.hiddenText}>
                  {LOCALIZE.scorer.assignedTest.previousPageButton}
                </label>
                <FontAwesomeIcon icon={faCaretLeft} />
              </div>
            }
            nextLabel={
              <div style={styles.paginationIcon}>
                <label style={styles.hiddenText}>
                  {LOCALIZE.scorer.assignedTest.nextPageButton}
                </label>
                <FontAwesomeIcon icon={faCaretRight} />
              </div>
            }
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    scorerPaginationPage: state.scorer.scorerPaginationPage,
    scorerPaginationPageSize: state.scorer.scorerPaginationPageSize
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateCurrentScorerAssignedTestPageState,
      updateScorerAssignedTestPageSizeState
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ScorerTable);
