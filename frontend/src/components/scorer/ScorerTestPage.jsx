import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { setLanguage } from "../../modules/LocalizeRedux";
import TestSectionComponentFactory from "../testFactory/TestSectionComponentFactory";
import { resetQuestionListState } from "../../modules/QuestionListRedux";
import { BANNER_STYLE } from "../eMIB/constants";
import {
  setTestHeight,
  updateTestSection,
  setStartTime,
  setScorerContentLoaded,
  setContentUnLoaded,
  resetTestFactoryState,
  updateCheatingAttempts,
  getUpdatedTestStartTime
} from "../../modules/TestSectionRedux";
import { updateAssignedTestId, resetAssignedTestState } from "../../modules/AssignedTestsRedux";
import {
  getInboxTestSection,
  updateCompetencies,
  getScores,
  loadScores
} from "../../modules/ScorerRedux";

import { resetInboxState } from "../../modules/EmailInboxRedux";
import { resetNotepadState } from "../../modules/NotepadRedux";

import scorescoreUitTestSection, { scoreUitTest } from "../../modules/ScoringRedux";
import { resetTopTabsState } from "../../modules/NavTabsRedux";
import { deactivateTest, quitTest } from "../../modules/TestStatusRedux";
import LOCALIZE from "../../text_resources";
import { Row, Col } from "react-bootstrap";
import ScorerFactory from "./ScorerFactory";

const styles = {
  container: {
    maxWidth: 1440,
    minWidth: 570,
    paddingTop: 20,
    paddingRight: 20,
    paddingLeft: 20,
    margin: "0px auto",
    textAlign: "left"
  },
  scorerPanel: {
    paddingLeft: 0,
    paddingRight: 20
  },
  testPanel: {
    paddingLeft: 0,
    paddingRight: 0
  }
};

class ScorerTestPage extends Component {
  componentDidMount = () => {
    // get the test section from the parent page
    if (!this.props.scorerTestSectionLoaded) {
      this.getTestSection(this.props.assignedTestId);
      this.props.updateAssignedTestId(this.props.assignedTestId, undefined, null);
    }
  };

  componentWillUnmount = () => {
    this.props.setScorerContentLoaded();
  };

  getTestSection = assignedTestId => {
    this.props.getInboxTestSection(assignedTestId).then(response => {
      if (response.error) {
        return;
      }
      this.props.setContentUnLoaded();
      // update competencies
      // use factory here based on scoring type
      this.props.updateCompetencies(response.scorer);
      this.props.getScores(this.props.assignedTestId).then(response => {
        this.props.loadScores(response);
      });
      this.props.setTestHeight(BANNER_STYLE);
      this.props.updateTestSection(response);
      this.props.setScorerContentLoaded();
    });
  };

  render() {
    if (!this.props.scorerTestSectionLoaded) {
      return <h2 style={{ ...styles.container, margin: "auto", paddingTop: "20px" }}>loading</h2>;
    }
    const { testSection } = this.props;
    return (
      <Row style={{ margin: "auto" }}>
        <Col
          style={styles.testPanel}
          role="region"
          aria-label={LOCALIZE.ariaLabel.topNavigationSection}
        >
          <div style={styles.container}>
            <TestSectionComponentFactory
              testSection={testSection.localize[this.props.currentLanguage]}
              handleNext={() => {}}
              handleFinishTest={() => {}}
              timeLimit={testSection.default_time}
              testSectionStartTime={this.props.testSectionStartTime}
              timeout={() => {}}
              testSectionType={testSection.section_type}
              resetAllRedux={this.resetAllRedux}
              defaultTab={testSection.default_tab}
              usesNotepad={testSection.uses_notepad}
              usesCalculator={testSection.uses_calculator}
              readOnly={true} // means no bottom bar, scorer will render
              accommodations={this.props.accommodations}
            />
          </div>
        </Col>
        {/* create a factory here based on scoring type */}
        {this.props.currentQuestion && this.props.scorerTestSectionLoaded && (
          <Col
            role="complementary"
            aria-label={LOCALIZE.ariaLabel.scorerPanel}
            md="auto"
            style={styles.scorerPanel}
          >
            <ScorerFactory
              scorerType={testSection.scoring_type}
              assignedScorerTest={this.props.assignedTestId}
            />
          </Col>
        )}
      </Row>
    );
  }
}

export { ScorerTestPage as UnconnectedScorerTestPage };
const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    testSection: state.testSection.testSection,
    scorerTestSectionLoaded: state.testSection.isScorerLoaded,
    testSectionStartTime: state.testSection.startTime,
    assignedTestId: state.scorer.assignedTestId,
    testId: state.assignedTest.testId,
    isTestActive: state.testStatus.isTestActive,
    previousStatus: state.testStatus.previousStatus,
    currentQuestion: state.questionList.currentQuestion,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setLanguage,
      getInboxTestSection,
      updateCompetencies,
      getScores,
      loadScores,
      setTestHeight,
      updateTestSection,
      setScorerContentLoaded,
      setContentUnLoaded,
      setStartTime,
      resetTestFactoryState,
      resetAssignedTestState,
      resetInboxState,
      resetNotepadState,
      resetQuestionListState,
      resetTopTabsState,
      scorescoreUitTestSection,
      scoreUitTest,
      updateCheatingAttempts,
      deactivateTest,
      quitTest,
      getUpdatedTestStartTime,
      updateAssignedTestId
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ScorerTestPage));
