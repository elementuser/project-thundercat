import React, { Component } from "react";
import ScorerTestView from "./ScorerTestView";
import { PATH } from "../commons/Constants";
import { Route, Switch } from "react-router";
import Dashboard from "./Dashboard";

export const styles = {
  header: {
    marginBottom: 24
  }
};

class ScorerRoutes extends Component {
  render() {
    return (
      <div>
        <Switch>
          <Route exact path={`${this.props.match.url}`} component={() => <Dashboard />} />
          <Route
            exact
            path={`${this.props.match.url}${PATH.scorerTest}`}
            component={() => <ScorerTestView />}
          />
        </Switch>
      </div>
    );
  }
}

export default ScorerRoutes;
