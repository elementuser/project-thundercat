import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import LOCALIZE from "../../text_resources";
import React, { Component } from "react";
import { BANNER_STYLE } from "../eMIB/constants";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMinusCircle, faPlusCircle } from "@fortawesome/free-solid-svg-icons";
import { Row } from "react-bootstrap";
import { styles as content_styles } from "../commons/ContentContainer";
import { UnconnectedTopTabs as TopTabs } from "../commons/TopTabs";
import CompetencyTab from "./CompetencyTab";

const SECTION_HEIGHT = `calc(100vh - ${BANNER_STYLE + 36}px)`;
const HIDDEN_HEIGHT = `calc(100vh - ${BANNER_STYLE - 5}px)`;
const MIN_WIDTH = content_styles.container.maxWidth * 0.5;
const SIDEBAR_COLOR = "#004db8";
const BORDER_COLOR = "#CECECE";
const HEADER_HEIGHT = 40;

const styles = {
  windowPadding: {
    margin: "39px 10px 10px 10px",
    paddingTop: 15,
    paddingRight: 0,
    paddingLeft: 0,
    textAlign: "left",
    paddingBottom: 45
  },
  label: {
    textAlign: "left",
    paddingLeft: 5,
    float: "right",
    fontSize: "16px",
    fontWeight: "bold",
    color: "#FFFFFF",
    backgroundColor: SIDEBAR_COLOR,
    cursor: "pointer"
  },
  hideSidebarBtn: {
    backgroundColor: SIDEBAR_COLOR,
    border: "none",
    float: "left",
    cursor: "pointer"
  },
  headerSection: {
    width: "100%",
    padding: "8px 12px",
    height: HEADER_HEIGHT
  },
  content: {
    backgroundColor: SIDEBAR_COLOR,
    borderWidth: "1px",
    borderStyle: "solid",
    borderColor: SIDEBAR_COLOR,
    width: "100%"
  },
  textAreaRow: {
    padding: 0,
    margin: 0,
    resize: "none",
    border: "none",
    height: SECTION_HEIGHT,
    width: MIN_WIDTH,
    borderTop: "none",
    overflow: "auto"
  },
  sidebar: {
    padding: 10,
    resize: "none",
    width: "100%",
    borderWidth: "2px",
    borderStyle: "solid",
    borderColor: BORDER_COLOR
  },
  openSidebarBtn: {
    height: HIDDEN_HEIGHT,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    cursor: "pointer",
    whiteSpace: "normal",
    padding: 2,
    backgroundColor: SIDEBAR_COLOR,
    borderColor: SIDEBAR_COLOR,
    overflow: "wrap",
    display: "block"
  },
  openButton: {
    color: "#FFFFFF",
    display: "block",
    cursor: "pointer"
  },
  closeButton: {
    position: "bottom",
    color: "#FFFFFF",
    backgroundColor: SIDEBAR_COLOR,
    float: "center",
    cursor: "pointer"
  },
  noPadding: {
    padding: "0 0 0 0",
    margin: "0 0 0 0"
  }
};

class ScorerSidebar extends Component {
  static props = {
    // Provided by Redux
    currentLanguage: PropTypes.string,
    currentQuestion: PropTypes.object
  };

  state = {
    isSidebarHidden: false
  };

  makeTABS = (ratings, SCALE_HEIGHT, MAX_POINTS, competency, score = {}) => {
    return (
      <CompetencyTab
        ratings={ratings}
        SCALE_HEIGHT={SCALE_HEIGHT}
        MAX_POINTS={MAX_POINTS + 1}
        competency={competency}
        score={{ score: "0", rationale: "", ...score }}
        currentQuestion={this.props.currentQuestion}
      />
    );
  };

  getTABS = (ratings, SCALE_HEIGHT) => {
    const competencyMap = {};
    let TABS = null;
    for (const comp of this.props.competencyTypes) {
      competencyMap[comp.id] = comp;
    }
    if (this.props.currentQuestion) {
      TABS = this.props.currentQuestion.email.competency_types.map(competency => ({
        key: competency,
        tabName: competencyMap[competency][`${this.props.currentLanguage}_name`],
        body: this.makeTABS(
          ratings.filter(obj => obj.competency_type === competency),
          SCALE_HEIGHT,
          competencyMap[competency].max_score,
          competencyMap[competency],
          this.getScore(competency)
        )
      }));
    }
    return TABS;
  };

  getScore = competency => {
    let score = {};
    if (this.props.answerScores) {
      // eslint-disable-next-line prefer-destructuring
      score = this.props.answerScores.filter(obj => obj.competency === competency)[0];
    }
    return score;
  };

  toggleSidebar = () => {
    this.setState({ isSidebarHidden: !this.state.isSidebarHidden });
  };

  switchCompetency = competency => {
    const comp = this.props.answerScores.filter(obj => obj.competency === competency)[0] || {};
    this.setState({ currentScore: comp });
  };

  render() {
    if (!this.props.currentQuestion || !this.props.currentQuestion.example_ratings) {
      return null;
    }
    const ratings = this.props.currentQuestion.example_ratings.sort((a, b) =>
      a.point_value > b.point_value ? 1 : -1
    );
    const { isSidebarHidden } = this.state;
    const SCALE_HEIGHT = 300;

    const TABS = this.getTABS(ratings, SCALE_HEIGHT);
    return (
      <div id="sidebar" style={styles.windowPadding}>
        <Row id="sidebar-header" style={styles.noPadding}>
          <div style={isSidebarHidden ? {} : styles.content}>
            <div style={isSidebarHidden ? {} : styles.headerSection}>
              {!isSidebarHidden && (
                <button
                  id="sidebar-button"
                  onClick={this.toggleSidebar}
                  style={styles.hideSidebarBtn}
                >
                  <FontAwesomeIcon style={styles.closeButton} icon={faMinusCircle} />
                  <span style={styles.label}>
                    <label htmlFor={"sidebar-field"}>{LOCALIZE.scorer.sidebar.title}</label>
                  </span>
                </button>
              )}
              {isSidebarHidden && (
                <button
                  id="sidebar-button"
                  onClick={this.toggleSidebar}
                  style={styles.openSidebarBtn}
                >
                  <span style={styles.openButton}>
                    <FontAwesomeIcon icon={faPlusCircle} />
                  </span>
                  <span style={styles.openButton}>{LOCALIZE.scorer.sidebar.title}</span>
                </button>
              )}
            </div>
          </div>
        </Row>
        {!isSidebarHidden && (
          <Row id="sidebar-body" style={styles.textAreaRow}>
            <div id="sidebar-field" style={styles.sidebar}>
              {!this.props.currentQuestion && <h2>{LOCALIZE.scorer.selectQuestion}</h2>}
              {this.props.currentQuestion && (
                <TopTabs
                  TABS={TABS}
                  defaultTab={TABS[0].key}
                  notifySwitchTab={this.switchCompetency}
                />
              )}
            </div>
          </Row>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  let currentQuestion = null;
  let answerScores = [];
  const questionIndex = state.questionList.currentQuestion;
  for (const question of state.scorer.questions) {
    if (String(question.id) === String(questionIndex)) {
      currentQuestion = question;
      break;
    }
  }
  if (currentQuestion) {
    answerScores = state.scorer.answerScores.filter(obj => obj.question === currentQuestion.id);
  }

  return {
    currentLanguage: state.localize.language,
    currentQuestion: currentQuestion,
    assignedTestId: state.scorer.assignedTestId,
    competencyTypes: state.scorer.competencyTypes,
    answerScores: answerScores
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ScorerSidebar);
