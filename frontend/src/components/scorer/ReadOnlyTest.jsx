import React, { Component } from "react";

class ReadOnlyTest extends Component {
  render() {
    return (
      <div>
        <h1>Read Only Test</h1>
        <p>COMING SOON!</p>
      </div>
    );
  }
}

export default ReadOnlyTest;
