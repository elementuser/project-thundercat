import React, { Component } from "react";
import { withRouter, Route, Switch, Redirect } from "react-router-dom";
import { PATH } from "../commons/Constants";
import SampleTestsDashboard from "./SampleTestsDashboard";
import TestPage from "../testFactory/TestPage";

class SampleTestsRoutes extends Component {
  render() {
    return (
      <Switch>
        <Route exact path={this.props.match.url} component={SampleTestsDashboard} />
        <Route
          path={`${PATH.sampleTests}/test-factory`}
          render={() => <TestPage sampleTest={true} />}
        />
        <Redirect to={this.props.match.url} />
      </Switch>
    );
  }
}

export { SampleTestsRoutes as UnconnectedSampleTestsRoutes };

export default withRouter(SampleTestsRoutes);
