import { Col, Row } from "react-bootstrap";
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getLineSpacingCSS } from "../../../../modules/AccommodationsRedux";
import LOCALIZE from "../../../../text_resources";
import CustomButton, { THEME } from "../../CustomButton";
import { faInfoCircle, faTimes } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import PopupBox, { BUTTON_TYPE } from "../../PopupBox";
import Switch from "react-switch";
import getSwitchTransformScale from "../../../../helpers/switchTransformScale";
import StyledTooltip, { EFFECT, TRIGGER_TYPE, TYPE } from "../../../authentication/StyledTooltip";

const columnSizes = {
  firstRowFirstColumn: {
    xs: 2,
    sm: 2,
    md: 2,
    lg: 2,
    xl: 2
  },
  firstRowSecondColumn: {
    xs: 2,
    sm: 2,
    md: 2,
    lg: 2,
    xl: 2
  },
  firstRowThirdColumn: {
    xs: 6,
    sm: 6,
    md: 6,
    lg: 6,
    xl: 6
  },
  firstRowFourthColumn: {
    xs: 2,
    sm: 2,
    md: 2,
    lg: 2,
    xl: 2
  },
  secondRowFirstColumn: {
    xs: 2,
    sm: 2,
    md: 2,
    lg: 2,
    xl: 2
  },
  secondRowSecondColumn: {
    xs: 10,
    sm: 10,
    md: 10,
    lg: 10,
    xl: 10
  },
  detailedPopupFirstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 4,
    xl: 4
  },
  detailedPopupSecondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 8,
    xl: 8
  }
};

const styles = {
  mainContainer: {
    padding: 6,
    border: "1px solid #005E56",
    borderRadius: 4
  },
  firstRowStyle: {
    margin: 0,
    marginBottom: 6
  },
  secondRowStyle: {
    margin: 0
  },
  textArea: {
    padding: "6px 12px",
    border: "1px solid #00565E",
    borderRadius: 4,
    width: "100%",
    minHeight: 38,
    resize: "none",
    overflow: "hidden"
  },
  labels: {
    marginRight: 12
  },
  scoreInput: {
    padding: "6px 12px",
    border: "1px solid #00565E",
    borderRadius: 4,
    resize: "none",
    textAlign: "center",
    width: 75
  },
  colTextArea: {
    padding: 0,
    textAligh: "right"
  },
  tooltipButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e",
    transform: "scale(1.7)"
  },
  allUnset: {
    all: "unset"
  },
  detailedPopupRowStyle: {
    marginBottom: 12
  },
  detailedPopupTextInput: {
    overflowY: "auto",
    height: 150,
    marginBottom: "-8px"
  },
  detailedPopupRationaleInput: {
    overflowY: "auto",
    height: 90,
    marginBottom: "-8px"
  },
  detailedPopupInput: {
    padding: "6px 12px",
    border: "1px solid #00565E",
    borderRadius: 4
  },
  centerText: {
    textAlign: "center"
  }
};

class TestOptionComponent extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.PropTypes = {
      item: PropTypes.object,
      onTextInputChange: PropTypes.func,
      onHistoricalOptionIdInputChange: PropTypes.func,
      onRationaleInputChange: PropTypes.func,
      onScoreInputChange: PropTypes.func,
      draggable: PropTypes.bool,
      getItemStyle: PropTypes.func,
      changeExcludeFromShuffle: PropTypes.func
    };

    TestOptionComponent.defaultProps = {
      text: "Example",
      draggable: false
    };

    this.state = {
      showDetailsPopup: false,
      // default values
      switchHeight: 25,
      switchWidth: 50
    };
  }

  componentDidMount = () => {
    this.triggerTextAreaHeightCalculation();
    this.getUpdatedSwitchDimensions();
  };

  componentDidUpdate = prevProps => {
    // if accommodations or item_options or selectedContentLanguage gets updated
    if (
      prevProps.accommodations !== this.props.accommodations ||
      prevProps.itemData.item_options !== this.props.itemData.item_options ||
      prevProps.selectedContentLanguage !== this.props.selectedContentLanguage
    ) {
      // calling input height calculation function
      this.triggerTextAreaHeightCalculation();
    }
    // if fontSize gets updated
    if (prevProps.accommodations.fontSize !== this.props.accommodations.fontSize) {
      this.getUpdatedSwitchDimensions();
    }
  };

  // getting switch dimensions and updating states
  getUpdatedSwitchDimensions = () => {
    // need this loading state, otherwise the switch diameter is not calculated on state change
    this.setState({ isLoading: true }, () => {
      this.setState(
        {
          switchHeight: getSwitchTransformScale(this.props.accommodations.fontSize).height,
          switchWidth: getSwitchTransformScale(this.props.accommodations.fontSize).width
        },
        () => {
          this.setState({ isLoading: false });
        }
      );
    });
  };

  triggerTextAreaHeightCalculation = () => {
    const textarea = document.getElementById(`item-option-input-${this.props.item.id}`);
    textarea.style.height = "1px";
    textarea.style.height = `${textarea.scrollHeight}px`;
  };

  openDetailsPopup = () => {
    this.setState({ showDetailsPopup: true });
  };

  closeDetailsPopup = () => {
    this.setState({ showDetailsPopup: false });
  };

  render() {
    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <div className="w-100" style={styles.mainContainer}>
        <Row
          className="w-100 align-items-center"
          draggable={this.props.draggable}
          style={styles.firstRowStyle}
        >
          <Col
            xl={columnSizes.firstRowFirstColumn.xl}
            lg={columnSizes.firstRowFirstColumn.lg}
            md={columnSizes.firstRowFirstColumn.md}
            sm={columnSizes.firstRowFirstColumn.sm}
            xs={columnSizes.firstRowFirstColumn.xs}
            style={styles.colTextArea}
            className="align-items-center"
          >
            <label htmlFor={`item-option-score-input-${this.props.item.id}`} style={styles.labels}>
              {
                LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                  .content.score
              }
            </label>
          </Col>
          <Col
            xl={columnSizes.firstRowSecondColumn.xl}
            lg={columnSizes.firstRowSecondColumn.lg}
            md={columnSizes.firstRowSecondColumn.md}
            sm={columnSizes.firstRowSecondColumn.sm}
            xs={columnSizes.firstRowSecondColumn.xs}
            style={styles.colTextArea}
            className="align-items-center"
          >
            <input
              className="valid-field"
              type="text"
              id={`item-option-score-input-${this.props.item.id}`}
              style={{ ...styles.scoreInput, ...accommodationsStyle }}
              onChange={this.props.onScoreInputChange}
              value={this.props.item.score}
            />
          </Col>
          <Col
            xl={columnSizes.firstRowThirdColumn.xl}
            lg={columnSizes.firstRowThirdColumn.lg}
            md={columnSizes.firstRowThirdColumn.md}
            sm={columnSizes.firstRowThirdColumn.sm}
            xs={columnSizes.firstRowThirdColumn.xs}
            className="text-center align-items-center"
          >
            {this.props.itemData.shuffle_options && (
              <Row className="align-items-center">
                <Col className="text-right" xl={8} lg={8} md={8} sm={8}>
                  <label
                    htmlFor={`item-option-exclude-from-shuffle-switch-${this.props.item.id}`}
                    style={styles.centerText}
                  >
                    {
                      LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                        .content.excludeFromShuffle
                    }
                  </label>
                </Col>
                <Col className="text-center" xl={4} lg={4} md={4} sm={4}>
                  <Switch
                    onChange={this.props.changeExcludeFromShuffle}
                    checked={this.props.item.exclude_from_shuffle}
                    aria-labelledby={`item-option-exclude-from-shuffle-switch-${this.props.item.id}`}
                    height={this.state.switchHeight}
                    width={this.state.switchWidth}
                  />
                </Col>
              </Row>
            )}
          </Col>
          <Col
            xl={columnSizes.firstRowFourthColumn.xl}
            lg={columnSizes.firstRowFourthColumn.lg}
            md={columnSizes.firstRowFourthColumn.md}
            sm={columnSizes.firstRowFourthColumn.sm}
            xs={columnSizes.firstRowFourthColumn.xs}
            style={styles.colTextArea}
            className="text-right"
          >
            <StyledTooltip
              id={`add-item-option-details-${this.props.item.id}`}
              place="top"
              type={TYPE.light}
              effect={EFFECT.solid}
              triggerType={[TRIGGER_TYPE.hover]}
              tooltipElement={
                <div style={styles.allUnset}>
                  <CustomButton
                    dataTip=""
                    dataFor={`add-item-option-details-${this.props.item.id}`}
                    label={
                      <>
                        <FontAwesomeIcon icon={faInfoCircle} />
                      </>
                    }
                    customStyle={styles.tooltipButton}
                    action={this.openDetailsPopup}
                    buttonTheme={THEME.SECONDARY}
                  />
                </div>
              }
              tooltipContent={
                <div>
                  {
                    LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                      .content.detailsButton
                  }
                </div>
              }
            />
          </Col>
        </Row>
        <Row
          className="w-100 align-items-center justify-content-between"
          draggable={this.props.draggable}
          style={styles.secondRowStyle}
        >
          <Col
            xl={columnSizes.secondRowFirstColumn.xl}
            lg={columnSizes.secondRowFirstColumn.lg}
            md={columnSizes.secondRowFirstColumn.md}
            sm={columnSizes.secondRowFirstColumn.sm}
            xs={columnSizes.secondRowFirstColumn.xs}
            style={styles.colTextArea}
          >
            <label htmlFor={`item-option-input-${this.props.item.id}`} style={styles.labels}>
              {
                LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                  .content.text
              }
            </label>
          </Col>
          <Col
            xl={columnSizes.secondRowSecondColumn.xl}
            lg={columnSizes.secondRowSecondColumn.lg}
            md={columnSizes.secondRowSecondColumn.md}
            sm={columnSizes.secondRowSecondColumn.sm}
            xs={columnSizes.secondRowSecondColumn.xs}
            style={styles.colTextArea}
          >
            <textarea
              id={`item-option-input-${this.props.item.id}`}
              className="w-100 valid-field"
              style={styles.textArea}
              onChange={this.props.onTextInputChange}
              value={this.props.item.text}
            />
          </Col>
        </Row>
        <PopupBox
          show={this.state.showDetailsPopup}
          handleClose={() => {}}
          title={
            LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items.content
              .detailsPopup.title
          }
          description={
            <div>
              <Row role="presentation" style={styles.detailedPopupRowStyle}>
                <Col
                  xl={columnSizes.detailedPopupFirstColumn.xl}
                  lg={columnSizes.detailedPopupFirstColumn.lg}
                  md={columnSizes.detailedPopupFirstColumn.md}
                  sm={columnSizes.detailedPopupFirstColumn.sm}
                >
                  <label htmlFor={`detailed-popup-item-option-input-${this.props.item.id}`}>
                    {
                      LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                        .content.detailsPopup.textLabel
                    }
                  </label>
                </Col>
                <Col
                  xl={columnSizes.detailedPopupSecondColumn.xl}
                  lg={columnSizes.detailedPopupSecondColumn.lg}
                  md={columnSizes.detailedPopupSecondColumn.md}
                  sm={columnSizes.detailedPopupSecondColumn.sm}
                >
                  <textarea
                    id={`detailed-popup-item-option-input-${this.props.item.id}`}
                    className="w-100 valid-field"
                    style={{ ...styles.textArea, ...styles.detailedPopupTextInput }}
                    onChange={this.props.onTextInputChange}
                    value={this.props.item.text}
                  />
                </Col>
              </Row>
              <Row
                className="align-items-center"
                role="presentation"
                style={styles.detailedPopupRowStyle}
              >
                <Col
                  xl={columnSizes.detailedPopupFirstColumn.xl}
                  lg={columnSizes.detailedPopupFirstColumn.lg}
                  md={columnSizes.detailedPopupFirstColumn.md}
                  sm={columnSizes.detailedPopupFirstColumn.sm}
                >
                  <label
                    htmlFor={`detailed-popup-historical-option-id-input-${this.props.item.id}`}
                  >
                    {
                      LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                        .content.detailsPopup.historicalOptionIdLabel
                    }
                  </label>
                </Col>
                <Col
                  xl={columnSizes.detailedPopupSecondColumn.xl}
                  lg={columnSizes.detailedPopupSecondColumn.lg}
                  md={columnSizes.detailedPopupSecondColumn.md}
                  sm={columnSizes.detailedPopupSecondColumn.sm}
                >
                  <input
                    id={`detailed-popup-historical-option-id-input-${this.props.item.id}`}
                    className="w-100 valid-field"
                    style={styles.detailedPopupInput}
                    onChange={this.props.onHistoricalOptionIdInputChange}
                    value={this.props.item.historical_option_id}
                  />
                </Col>
              </Row>
              <Row role="presentation" style={styles.detailedPopupRowStyle}>
                <Col
                  xl={columnSizes.detailedPopupFirstColumn.xl}
                  lg={columnSizes.detailedPopupFirstColumn.lg}
                  md={columnSizes.detailedPopupFirstColumn.md}
                  sm={columnSizes.detailedPopupFirstColumn.sm}
                >
                  <label htmlFor={`detailed-popup-rationale-input-${this.props.item.id}`}>
                    {
                      LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                        .content.detailsPopup.rationaleLabel
                    }
                  </label>
                </Col>
                <Col
                  xl={columnSizes.detailedPopupSecondColumn.xl}
                  lg={columnSizes.detailedPopupSecondColumn.lg}
                  md={columnSizes.detailedPopupSecondColumn.md}
                  sm={columnSizes.detailedPopupSecondColumn.sm}
                >
                  <textarea
                    id={`detailed-popup-rationale-input-${this.props.item.id}`}
                    className="w-100 valid-field"
                    style={{ ...styles.textArea, ...styles.detailedPopupRationaleInput }}
                    onChange={this.props.onRationaleInputChange}
                    value={this.props.item.rationale}
                  />
                </Col>
              </Row>
              {this.props.itemData.shuffle_options && (
                <Row
                  className="align-items-center"
                  role="presentation"
                  style={styles.detailedPopupRowStyle}
                >
                  <Col
                    xl={columnSizes.detailedPopupFirstColumn.xl}
                    lg={columnSizes.detailedPopupFirstColumn.lg}
                    md={columnSizes.detailedPopupFirstColumn.md}
                    sm={columnSizes.detailedPopupFirstColumn.sm}
                  >
                    <label
                      id={`detailed-popup-exclude-from-shuffle-switch-label-${this.props.item.id}`}
                    >
                      {
                        LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                          .items.content.detailsPopup.excludeFromShuffleLabel
                      }
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.detailedPopupSecondColumn.xl}
                    lg={columnSizes.detailedPopupSecondColumn.lg}
                    md={columnSizes.detailedPopupSecondColumn.md}
                    sm={columnSizes.detailedPopupSecondColumn.sm}
                  >
                    <Switch
                      onChange={this.props.changeExcludeFromShuffle}
                      checked={this.props.item.exclude_from_shuffle}
                      aria-labelledby={`detailed-popup-exclude-from-shuffle-switch-label-${this.props.item.id}`}
                      height={this.state.switchHeight}
                      width={this.state.switchWidth}
                    />
                  </Col>
                </Row>
              )}
              <Row
                className="align-items-center"
                role="presentation"
                style={styles.detailedPopupRowStyle}
              >
                <Col
                  xl={columnSizes.detailedPopupFirstColumn.xl}
                  lg={columnSizes.detailedPopupFirstColumn.lg}
                  md={columnSizes.detailedPopupFirstColumn.md}
                  sm={columnSizes.detailedPopupFirstColumn.sm}
                >
                  <label htmlFor={`detailed-popup-score-input-${this.props.item.id}`}>
                    {
                      LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                        .content.detailsPopup.scoreLabel
                    }
                  </label>
                </Col>
                <Col
                  xl={columnSizes.detailedPopupSecondColumn.xl}
                  lg={columnSizes.detailedPopupSecondColumn.lg}
                  md={columnSizes.detailedPopupSecondColumn.md}
                  sm={columnSizes.detailedPopupSecondColumn.sm}
                >
                  <input
                    id={`detailed-popup-score-input-${this.props.item.id}`}
                    className="w-100 valid-field"
                    style={styles.detailedPopupInput}
                    onChange={this.props.onScoreInputChange}
                    value={this.props.item.score}
                  />
                </Col>
              </Row>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.saveButton}
          rightButtonIcon={faTimes}
          rightButtonAction={this.closeDetailsPopup}
        />
      </div>
    );
  }
}

export { TestOptionComponent as unconnectedTestOptionComponent };

const mapStateToProps = (state, ownProps) => {
  return {
    accommodations: state.accommodations,
    itemData: state.itemBank.itemData
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(TestOptionComponent);
