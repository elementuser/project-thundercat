import { Row } from "react-bootstrap";
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as _ from "lodash";
import { makeTextBoxField } from "../../../testBuilder/helpers";
import LOCALIZE from "../../../../text_resources";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 12,
    xl: 12
  }
};

const styles = {
  input: {
    textAlign: "center"
  }
};

class ItemBankBundleBundleContentBundleRuleContentComponent extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.PropTypes = {
      ruleData: PropTypes.object,
      draggable: PropTypes.bool
    };

    ItemBankBundleBundleContentBundleRuleContentComponent.defaultProps = {
      draggable: true
    };
  }

  render() {
    return (
      <Row
        className="w-100 align-items-center justify-content-between"
        draggable={this.props.draggable}
      >
        {makeTextBoxField(
          `bundle-content-bundle-rule-content-input-${this.props.ruleData.display_order}`,
          "",
          `${this.props.ruleData.bundle_data.name} ${
            this.props.ruleData.bundle_data.version_text !== ""
              ? `[${this.props.ruleData.bundle_data.version_text}]`
              : ""
          } (${
            this.props.ruleData.bundle_data.active
              ? LOCALIZE.commons.active
              : LOCALIZE.commons.inactive
          })`,
          true,
          () => {},
          `bundle-content-bundle-rule-content-input-${this.props.ruleData.display_order}-id`,
          true,
          styles.input,
          false,
          {
            xl: columnSizes.firstColumn.xl,
            lg: columnSizes.firstColumn.lg,
            md: columnSizes.firstColumn.md,
            sm: columnSizes.firstColumn.sm,
            xs: columnSizes.firstColumn.xs
          }
        )}
      </Row>
    );
  }
}

export { ItemBankBundleBundleContentBundleRuleContentComponent as unconnectedItemBankBundleBundleContentBundleRuleContentComponent };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ItemBankBundleBundleContentBundleRuleContentComponent);
