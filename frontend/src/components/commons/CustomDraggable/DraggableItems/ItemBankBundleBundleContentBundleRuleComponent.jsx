import { Col, Row } from "react-bootstrap";
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as _ from "lodash";
import LOCALIZE from "../../../../text_resources";
import CollapsingItemContainer from "../../../eMIB/CollapsingItemContainer";
import { Droppable } from "react-beautiful-dnd";
import CustomDraggable from "../CustomDraggable";
import { BUNDLE_RULE_DROPPABLE_ID } from "../../../testBuilder/itemBank/bundles/BundleContentTab";
import { getLineSpacingCSS } from "../../../../modules/AccommodationsRedux";
import { setBundleData } from "../../../../modules/ItemBankRedux";
import getSwitchTransformScale from "../../../../helpers/switchTransformScale";
import Switch from "react-switch";

// Variable used to specify max length of the values in the collapsable title
const MAX_VALUES_PRINT_LENGTH = 55;

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 12,
    xl: 12
  },
  firstRuleColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 6,
    xl: 6
  },
  secondRuleColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 6,
    xl: 6
  }
};

const styles = {
  row: {
    padding: 0,
    margin: 0
  },
  bold: {
    fontWeight: "bold"
  },
  ruleContentContainer: {
    padding: "0 12px"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6,
    width: 250
  },
  input: {
    width: 100,
    minHeight: 38,
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565e",
    borderRadius: 4,
    textAlign: "center"
  },
  bundleRuleRowContainer: {
    padding: "4px 0"
  }
};

class ItemBankBundleBundleContentBundleRuleComponent extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.PropTypes = {
      bundleRule: PropTypes.object,
      index: PropTypes.number,
      draggable: PropTypes.bool,
      disabledBundleContentBundleRuleContentDroppable: PropTypes.bool
    };

    ItemBankBundleBundleContentBundleRuleComponent.defaultProps = {
      draggable: true
    };

    this.state = {
      numberOfBundlesToPull:
        this.props.bundleData.bundle_bundles_rule_data[this.props.index].number_of_bundles,
      isLoading: false,
      // default values
      switchHeight: 25,
      switchWidth: 50
    };
  }

  componentDidMount = () => {
    // validating number of bundles to pull
    this.validateNumberOfBundlesToPull();
    this.getUpdatedSwitchDimensions();
  };

  componentDidUpdate = prevProps => {
    // if fontSize gets updated
    if (prevProps.accommodations.fontSize !== this.props.accommodations.fontSize) {
      this.getUpdatedSwitchDimensions();
    }
  };

  // Draggable Styling
  getItemStyle = (isDragging, draggableStyle) => ({
    // some basic styles to make the items look a bit nicer
    userSelect: "none",

    // change background colour if dragging
    background: isDragging ? "#0099A8" : "white",
    color: "#00565e",

    // styles we need to apply on draggables
    ...draggableStyle
  });

  // Droppable Styling
  getListStyle = isDraggingOver => ({
    background: isDraggingOver ? "#cdcdcd" : "white",
    padding: "12px 24px 12px 24px"
  });

  // getting switch dimensions and updating states
  getUpdatedSwitchDimensions = () => {
    // need this loading state, otherwise the switch diameter is not calculated on state change
    this.setState({ isLoading: true }, () => {
      this.setState(
        {
          switchHeight: getSwitchTransformScale(this.props.accommodations.fontSize).height,
          switchWidth: getSwitchTransformScale(this.props.accommodations.fontSize).width
        },
        () => {
          this.setState({ isLoading: false });
        }
      );
    });
  };

  // Used to print the attribute name in the Collapsable's title
  printFormattedRuleTitle = () => {
    let finalText = "";
    finalText = LOCALIZE.formatString(
      LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles.bundleContent
        .bundleRuleCollapsingItemContainer.title,
      this.props.index + 1
    );

    this.props.bundleData.bundle_bundles_rule_data[
      this.props.index
    ].bundle_bundles_rule_associations.forEach((value, index) => {
      // first index
      if (index === 0) {
        finalText += ` - ${value.bundle_data.name}`;
      } else {
        finalText += `, ${value.bundle_data.name}`;
      }
    });

    // if we have more than MAX_VALUES_PRINT_LENGTH chars, trim the final string so we have MAX_VALUES_PRINT_LENGTH-3 chars + "..."
    if (finalText.length > MAX_VALUES_PRINT_LENGTH) {
      finalText = finalText.substring(0, MAX_VALUES_PRINT_LENGTH - 3);
      finalText += "...";
    }

    return finalText;
  };

  validateNumberOfBundlesToPull = () => {
    let isValidNumberOfBundlesToPull = true;

    // numberOfBundlesToPull is 0
    // numberOfBundlesToPull is greater than total amount of associated items
    if (
      this.state.numberOfBundlesToPull === 0 ||
      this.state.numberOfBundlesToPull >
        this.props.bundleData.bundle_bundles_rule_data[this.props.index]
          .bundle_bundles_rule_associations.length
    ) {
      // setting isValidNumberOfBundlesToPull to false
      isValidNumberOfBundlesToPull = false;
    }

    // updating needed states
    this.setState({ isValidNumberOfBundlesToPull: isValidNumberOfBundlesToPull });
  };

  updateNumberOfBundlesToPullContent = event => {
    const numberOfBundlesToPull = event.target.value;
    // allow only numeric values (0 to 5 chars)
    const regexExpression = /^([0-9]{0,5})$/;
    if (regexExpression.test(numberOfBundlesToPull)) {
      this.setState(
        {
          numberOfBundlesToPull: numberOfBundlesToPull !== "" ? parseInt(numberOfBundlesToPull) : ""
        },
        () => {
          // validating number of bundles to pull
          this.validateNumberOfBundlesToPull();
        }
      );

      // Create a buffer before updating the redux state
      // Clear the old typingTimer
      clearTimeout(this.state.typingTimer);

      // Change the typingTimer to a timeout of 200 millisec
      // When user stops writing, this will go through after that time
      // Otherwise, the timer will be reset to that time
      const newTimer = setTimeout(() => {
        // updating redux states
        // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
        const newBundleBundlesRuleData = _.cloneDeep(
          this.props.bundleData.bundle_bundles_rule_data
        );
        newBundleBundlesRuleData[this.props.index].number_of_bundles =
          this.state.numberOfBundlesToPull;

        const new_obj = {
          ...this.props.bundleData,
          bundle_bundles_rule_data: newBundleBundlesRuleData
        };

        // setting new bundleData redux state
        this.props.setBundleData(new_obj);
      }, 200);

      this.setState({ typingTimer: newTimer });
    }
  };

  changeShuffleBundles = event => {
    // updating redux states
    // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
    const newBundleBundlesRuleData = _.cloneDeep(this.props.bundleData.bundle_bundles_rule_data);
    newBundleBundlesRuleData[this.props.index].shuffle_bundles = event;

    const new_obj = {
      ...this.props.bundleData,
      bundle_bundles_rule_data: newBundleBundlesRuleData
    };

    // setting new bundleData redux state
    this.props.setBundleData(new_obj);
  };

  changeKeepItemsTogether = event => {
    // updating redux states
    // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
    const newBundleBundlesRuleData = _.cloneDeep(this.props.bundleData.bundle_bundles_rule_data);
    newBundleBundlesRuleData[this.props.index].keep_items_together = event;

    const new_obj = {
      ...this.props.bundleData,
      bundle_bundles_rule_data: newBundleBundlesRuleData
    };

    // setting new bundleData redux state
    this.props.setBundleData(new_obj);
  };

  render() {
    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <Row
        className="w-100 align-items-center justify-content-between"
        draggable={this.props.draggable}
        style={styles.row}
      >
        <Col
          xl={columnSizes.firstColumn.xl}
          lg={columnSizes.firstColumn.lg}
          md={columnSizes.firstColumn.md}
          sm={columnSizes.firstColumn.sm}
          xs={columnSizes.firstColumn.xs}
          className="d-flex justify-content-center w-100"
          draggable={false}
        >
          <CollapsingItemContainer
            draggable={false}
            key={this.props.index}
            index={this.props.index}
            triggerCollapse={this.props.triggerCollapse}
            title={<span style={styles.bold}>{this.printFormattedRuleTitle()}</span>}
            body={
              <div>
                <div style={styles.ruleContentContainer}>
                  <Row className="w-100 align-items-center" style={styles.bundleRuleRowContainer}>
                    <Col
                      xl={columnSizes.firstRuleColumn.xl}
                      lg={columnSizes.firstRuleColumn.lg}
                      md={columnSizes.firstRuleColumn.md}
                      sm={columnSizes.firstRuleColumn.sm}
                      xs={columnSizes.firstRuleColumn.xs}
                      className="d-flex w-100"
                    >
                      <label
                        htmlFor={`item-bank-bundles-bundles-content-number-of-bundles-to-pull-${this.props.index}`}
                      >
                        {
                          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                            .bundles.bundleContent.bundleRuleCollapsingItemContainer
                            .numberOfBundlesToPull
                        }
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondRuleColumn.xl}
                      lg={columnSizes.secondRuleColumn.lg}
                      md={columnSizes.secondRuleColumn.md}
                      sm={columnSizes.secondRuleColumn.sm}
                      xs={columnSizes.secondRuleColumn.xs}
                      className="w-100"
                    >
                      <input
                        id={`item-bank-bundles-bundles-content-number-of-bundles-to-pull-${this.props.index}`}
                        className={
                          this.state.isValidNumberOfBundlesToPull ? "valid-field" : "invalid-field"
                        }
                        aria-required={false}
                        aria-invalid={!this.state.isValidNumberOfBundlesToPull}
                        style={{ ...styles.input, ...accommodationsStyle }}
                        type="text"
                        placeholder={LOCALIZE.commons.all}
                        value={this.state.numberOfBundlesToPull}
                        onChange={this.updateNumberOfBundlesToPullContent}
                      ></input>
                      {!this.state.isValidNumberOfBundlesToPull && (
                        <div>
                          <label
                            htmlFor={`item-bank-bundles-bundles-content-number-of-bundles-to-pull-${this.props.index}`}
                            style={styles.errorMessage}
                          >
                            {
                              LOCALIZE.testBuilder.testDefinition.selectTestDefinition
                                .itemBankEditor.bundles.bundleContent
                                .bundleRuleCollapsingItemContainer.numberOfBundlesToPullErrorMessage
                            }
                          </label>
                        </div>
                      )}
                    </Col>
                  </Row>
                  <Row className="w-100 align-items-center" style={styles.bundleRuleRowContainer}>
                    <Col
                      xl={columnSizes.firstRuleColumn.xl}
                      lg={columnSizes.firstRuleColumn.lg}
                      md={columnSizes.firstRuleColumn.md}
                      sm={columnSizes.firstRuleColumn.sm}
                      xs={columnSizes.firstRuleColumn.xs}
                      className="d-flex w-100"
                    >
                      <label
                        id={`item-bank-bundles-bundles-content-shuffle-bundles-${this.props.index}`}
                      >
                        {
                          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                            .bundles.bundleContent.bundleRuleCollapsingItemContainer.shuffleBundles
                        }
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondRuleColumn.xl}
                      lg={columnSizes.secondRuleColumn.lg}
                      md={columnSizes.secondRuleColumn.md}
                      sm={columnSizes.secondRuleColumn.sm}
                      xs={columnSizes.secondRuleColumn.xs}
                      className="w-100"
                    >
                      {!this.state.isLoading && (
                        <Switch
                          onChange={this.changeShuffleBundles}
                          checked={
                            this.props.bundleData.bundle_bundles_rule_data[this.props.index]
                              .shuffle_bundles
                          }
                          aria-labelledby={`item-bank-bundles-bundles-content-shuffle-bundles-${this.props.index}`}
                          height={this.state.switchHeight}
                          width={this.state.switchWidth}
                        />
                      )}
                    </Col>
                  </Row>
                  <Row className="w-100 align-items-center" style={styles.bundleRuleRowContainer}>
                    <Col
                      xl={columnSizes.firstRuleColumn.xl}
                      lg={columnSizes.firstRuleColumn.lg}
                      md={columnSizes.firstRuleColumn.md}
                      sm={columnSizes.firstRuleColumn.sm}
                      xs={columnSizes.firstRuleColumn.xs}
                      className="d-flex w-100"
                    >
                      <label
                        id={`item-bank-bundles-bundles-content-keep-items-together-${this.props.index}`}
                      >
                        {
                          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                            .bundles.bundleContent.bundleRuleCollapsingItemContainer
                            .keepItemsTogether
                        }
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondRuleColumn.xl}
                      lg={columnSizes.secondRuleColumn.lg}
                      md={columnSizes.secondRuleColumn.md}
                      sm={columnSizes.secondRuleColumn.sm}
                      xs={columnSizes.secondRuleColumn.xs}
                      className="w-100"
                    >
                      {!this.state.isLoading && (
                        <Switch
                          onChange={this.changeKeepItemsTogether}
                          checked={
                            this.props.bundleData.bundle_bundles_rule_data[this.props.index]
                              .keep_items_together
                          }
                          aria-labelledby={`item-bank-bundles-bundles-content-keep-items-together-${this.props.index}`}
                          height={this.state.switchHeight}
                          width={this.state.switchWidth}
                        />
                      )}
                    </Col>
                  </Row>
                </div>
                <Droppable
                  droppableId={`${BUNDLE_RULE_DROPPABLE_ID.bundleContentBundleRuleContentDroppable}-${this.props.index}`}
                  isDropDisabled={this.props.disabledBundleContentBundleRuleContentDroppable}
                >
                  {(provided, snapshot) => (
                    <div
                      {...provided.droppableProps}
                      ref={provided.innerRef}
                      style={this.getListStyle(snapshot.isDraggingOver)}
                    >
                      {this.props.bundleRule.bundle_bundles_rule_associations.map(
                        (ruleData, index) => (
                          <CustomDraggable
                            key={`bundle-content-bundle-rule-content-key-${index}`}
                            bundleBundlesRuleContentData={ruleData}
                            index={index}
                            getItemStyle={this.getItemStyle}
                            deleteItem={() => {}}
                            customDeleteButtonClass="align-items-center"
                            isDeletable={false}
                          />
                        )
                      )}
                      {provided.placeholder}
                    </div>
                  )}
                </Droppable>
              </div>
            }
          />
        </Col>
      </Row>
    );
  }
}

export { ItemBankBundleBundleContentBundleRuleComponent as unconnectedItemBankBundleBundleContentBundleRuleComponent };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    bundleData: state.itemBank.bundleData
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setBundleData
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ItemBankBundleBundleContentBundleRuleComponent);
