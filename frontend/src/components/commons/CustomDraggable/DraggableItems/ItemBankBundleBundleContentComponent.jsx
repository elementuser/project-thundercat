import { Row } from "react-bootstrap";
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as _ from "lodash";
import { makeTextBoxField } from "../../../testBuilder/helpers";
import LOCALIZE from "../../../../text_resources";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 12,
    xl: 12
  }
};

const styles = {
  input: {
    textAlign: "center"
  }
};

class ItemBankBundleBundleContentComponent extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.PropTypes = {
      bundle: PropTypes.object,
      draggable: PropTypes.bool
    };

    ItemBankBundleBundleContentComponent.defaultProps = {
      draggable: true
    };
  }

  render() {
    return (
      <Row
        className="w-100 align-items-center justify-content-between"
        draggable={this.props.draggable}
      >
        {makeTextBoxField(
          `item-content-bundle-input-${this.props.bundle.display_order}`,
          "",
          `${this.props.bundle.bundle_data.name} ${
            this.props.bundle.bundle_data.version_text !== ""
              ? `[${this.props.bundle.bundle_data.version_text}]`
              : ""
          } (${
            this.props.bundle.bundle_data.active
              ? LOCALIZE.commons.active
              : LOCALIZE.commons.inactive
          })`,
          true,
          () => {},
          `item-content-bundle-input-${this.props.bundle.bundle_link_id}-id`,
          true,
          styles.input,
          false,
          {
            xl: columnSizes.firstColumn.xl,
            lg: columnSizes.firstColumn.lg,
            md: columnSizes.firstColumn.md,
            sm: columnSizes.firstColumn.sm,
            xs: columnSizes.firstColumn.xs
          }
        )}
      </Row>
    );
  }
}

export { ItemBankBundleBundleContentComponent as unconnectedItemBankBundleBundleContentComponent };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ItemBankBundleBundleContentComponent);
