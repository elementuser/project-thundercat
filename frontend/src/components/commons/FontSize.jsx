import React, { Component } from "react";
import LOCALIZE from "../../text_resources";
import { Col, Row } from "react-bootstrap";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import {
  setFontSize,
  saveUserAccommodations,
  triggerRerender
} from "../../modules/AccommodationsRedux";

import { fontSizeDefinition } from "./Constants";

import DropdownSelect from "./DropdownSelect";

class FontSize extends Component {
  state = {
    fontSize: { label: "default", value: "default" }
  };

  onChange = event => {
    this.props.setFontSize(event.value);

    if (this.props.authenticated) {
      this.props
        .saveUserAccommodations({
          ...this.props.accommodations,
          fontSize: event.value
        })
        .then(() => {
          // trigger rerender so dynamic height in test is updated
          this.props.triggerRerender();
        });
    }
  };

  getValue = () => {
    return { label: this.props.fontSize, value: this.props.fontSize };
  };

  render() {
    const value = this.getValue();

    const options = fontSizeDefinition();

    // defining size here because it wouldn't allow modifcation of values when defined outside.
    const styles = {
      container: {
        padding: "12px 25px",
        width: "100%"
      },
      textSizeLabel: {
        marginTop: "auto",
        marginBottom: "auto",
        width: "50%",
        display: "table-cell",
        verticalAlign: "middle"
      }
    };
    if (value && value.value !== "default") {
      styles.container.fontFamily = value.value;
    }

    return (
      <div style={styles.container}>
        <Row role="presentation">
          <Col xl={6} lg={6} md={12} sm={12} style={styles.textSizeLabel}>
            <label id="font-size-label">{LOCALIZE.settings.fontSize.label}</label>
          </Col>
          <Col xl={6} lg={6} md={12} sm={12}>
            <DropdownSelect
              idPrefix="font-size"
              onChange={this.onChange}
              defaultValue={value}
              options={options}
              ariaLabelledBy="font-size-label"
              orderByLabels={false}
              menuPlacement={"bottom"}
            />
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    authenticated: state.login.authenticated,
    fontSize: state.accommodations.fontSize,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ setFontSize, saveUserAccommodations, triggerRerender }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(FontSize);
