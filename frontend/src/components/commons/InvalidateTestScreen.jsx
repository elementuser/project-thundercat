import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import LOCALIZE from "../../text_resources";
import ContentContainer from "./ContentContainer";
import getInTestHeightCalculations from "../../helpers/inTestHeightCalculations";
import { PATH } from "./Constants";
import invalidateTestReasonRedux from "../../modules/InvalidateTestRedux";
import TestNavBar from "../../TestNavBar";
import CustomButton, { THEME } from "./CustomButton";
import { updateInvalidateTest } from "../../modules/TestStatusRedux";

const styles = {
  tabSection: {
    paddingLeft: 18,
    marginBottom: "-3px"
  },
  tabLabel: {
    padding: "4px 24px",
    marginBottom: 0,
    position: "relative"
  },
  tabBorder: {
    borderBottom: "5px solid #00565e",
    width: "100%",
    position: "absolute",
    bottom: 0,
    left: 0,
    borderRadius: "4px 4px 0 0"
  },
  tabContentContainer: {
    borderStyle: "solid",
    borderWidth: "3px 1px 1px 1px",
    borderColor: "#CECECE",
    padding: "12px 0",
    overflowY: "scroll"
  },
  descriptionContainer: {
    margin: 60,
    fontWeight: "bold"
  },
  buttonsDiv: {
    textAlign: "center",
    marginTop: 100
  },
  button: {
    minWidth: 175
  }
};

export class InvalidateTestScreen extends Component {
  constructor(props) {
    super(props);
    this.topTabsHeightRef = React.createRef();
  }

  componentWillUnmount = () => {
    // in case candidate hits the back button
    this.props.updateInvalidateTest(false);
  };

  handleReturnToDashboard = () => {
    // redirecting to dashboard page
    this.props.updateInvalidateTest(false);
    this.props.history.push(PATH.dashboard);
  };

  render() {
    const tabTitleCustomFontSize = {
      // adding 12px to selected font-size
      fontSize: `${parseInt(this.props.accommodations.fontSize.split("px")[0]) + 12}px`
    };

    const customHeight = getInTestHeightCalculations(
      this.props.accommodations.fontSize,
      this.props.accommodations.spacing,
      this.props.testNavBarHeight,
      this.props.topTabsHeight,
      this.props.testFooterHeight
    );

    const dynamicMarginBottom = this.props.accommodations.spacing ? { marginBottom: "-5em" } : {};
    return (
      <div className="app">
        <Helmet>
          <html lang={this.props.currentLanguage} />
          <title className="notranslate">{LOCALIZE.candidateInvalidateTestScreen.tabTitle}</title>
        </Helmet>
        <TestNavBar updateBackendTest={() => {}} quitTestHidden={true} />
        <ContentContainer>
          <div
            id="test-invalidated-tab"
            ref={this.topTabsHeightRef}
            style={styles.tabSection}
            tabIndex={0}
          >
            <div>
              <label style={{ ...styles.tabLabel, ...tabTitleCustomFontSize }}>
                {LOCALIZE.candidateInvalidateTestScreen.tabTitle}
                <span style={styles.tabBorder}></span>
              </label>
            </div>
          </div>
          <div
            style={{ ...styles.tabContentContainer, ...customHeight, ...dynamicMarginBottom }}
            tabIndex={0}
          >
            <div style={styles.descriptionContainer}>
              <p>{LOCALIZE.candidateInvalidateTestScreen.description.part1}</p>
              <p>{LOCALIZE.candidateInvalidateTestScreen.description.part2}</p>
              <br />
            </div>
            <div style={styles.buttonsDiv}>
              <CustomButton
                label={LOCALIZE.candidateInvalidateTestScreen.description.returnToDashboard}
                customStyle={styles.button}
                buttonTheme={THEME.SECONDARY}
                action={this.handleReturnToDashboard}
              />
            </div>
          </div>
        </ContentContainer>
      </div>
    );
  }
}

export { InvalidateTestScreen as unconnectedInvalidateTestScreen };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    testSection: state.testSection.testSection,
    assignedTestId: state.assignedTest.assignedTestId,
    testId: state.assignedTest.testId,
    topTabsHeight: state.testSection.topTabsHeight,
    testNavBarHeight: state.testSection.testNavBarHeight,
    testFooterHeight: state.testSection.testFooterHeight,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ invalidateTestReasonRedux, updateInvalidateTest }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(InvalidateTestScreen);
