import React, { Component } from "react";
import LOCALIZE from "../../text_resources";
import { Row, Col } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faChevronRight,
  faChevronLeft,
  faExclamationCircle
} from "@fortawesome/free-solid-svg-icons";
import PropTypes from "prop-types";
import CustomButton, { THEME } from "./CustomButton";

const styles = {
  buttonRowPadding: {
    paddingBottom: 60,
    paddingTop: 10,
    margin: "0 auto",
    marginBottom: "2em"
  },
  buttonNext: {
    float: "right"
  },
  buttonReview: {
    justifyContent: "center",
    width: "100%"
  },
  span: {
    paddingLeft: 5,
    paddingRight: 5
  }
};

class NextPreviousButtonNav extends Component {
  static propTypes = {
    showNext: PropTypes.bool,
    showPrevious: PropTypes.bool,
    onChangeToNext: PropTypes.func.isRequired,
    onChangeToPrevious: PropTypes.func.isRequired,
    onMarkForReview: PropTypes.func
  };

  render() {
    return (
      <Row style={styles.buttonRowPadding} lang={this.props.lang}>
        <Col sm>
          {this.props.showPrevious && (
            <CustomButton
              label={
                <>
                  <FontAwesomeIcon icon={faChevronLeft} />
                  <span style={styles.span} className="notranslate">
                    {LOCALIZE.commons.previous}
                  </span>
                </>
              }
              action={this.props.onChangeToPrevious}
              buttonTheme={THEME.SECONDARY}
              ariaLabel={LOCALIZE.commons.previous}
            ></CustomButton>
          )}
        </Col>

        <Col sm>
          {this.props.showReview && (
            <CustomButton
              ariaPressed={this.props.isMarkedForReview}
              label={
                <div>
                  <FontAwesomeIcon icon={faExclamationCircle} />
                  <span style={styles.span} className="notranslate">
                    {this.props.isMarkedForReview
                      ? LOCALIZE.mcTest.questionList.markedReviewButton
                      : LOCALIZE.mcTest.questionList.reviewButton}
                  </span>
                </div>
              }
              action={this.props.onMarkForReview}
              customStyle={styles.buttonReview}
              buttonTheme={THEME.SECONDARY}
              ariaLabel={LOCALIZE.mcTest.questionList.reviewButton}
            ></CustomButton>
          )}
        </Col>

        <Col sm>
          {this.props.showNext && (
            <CustomButton
              label={
                <div>
                  <span style={styles.span} className="notranslate">
                    {LOCALIZE.commons.next}
                  </span>
                  <FontAwesomeIcon icon={faChevronRight} />
                </div>
              }
              action={this.props.onChangeToNext}
              customStyle={styles.buttonNext}
              buttonTheme={THEME.SECONDARY}
              ariaLabel={LOCALIZE.commons.next}
            ></CustomButton>
          )}
        </Col>
      </Row>
    );
  }
}

export default NextPreviousButtonNav;
