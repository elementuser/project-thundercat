import React, { Component } from "react";
import PropTypes from "prop-types";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  updateTimerState,
  triggerTimeUpdate,
  updateValidTimerState,
  resetTimerStates
} from "../../modules/SetTimerRedux";
import { getFormattedTime, hoursMinutesToMinutes } from "../../helpers/timeConversion";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSortUp, faSortDown } from "@fortawesome/free-solid-svg-icons";

// default min/max hours/minutes values
const DEFAULT_VALUES = {
  minHours: 0,
  maxHours: 99,
  minMinutes: 0,
  maxMinutes: 59,
  hoursLeaps: 1,
  minutesLeaps: 1
};

const styles = {
  mainContainer: {
    textAlign: "center",
    color: "#00565e"
  },
  timerContainer: {
    margin: "0 auto",
    border: "2px solid #00565e",
    borderRadius: 8
  },
  timerInsideContainer: {
    position: "relative",
    display: "table",
    margin: "0 auto"
  },
  hoursContainer: {
    display: "table-cell",
    padding: "0 18px"
  },
  colonContainer: {
    display: "table-cell",
    verticalAlign: "middle"
  },
  minutesContainer: {
    display: "table-cell",
    padding: "0 18px"
  },
  buttonsContainer: {
    lineHeight: "28px",
    width: "78%",
    margin: "0 auto",
    display: "table",
    zIndex: "999"
  },
  incrementButton: {
    marginTop: 6
  },
  decrementButton: {
    marginBottom: 6
  },
  incrementDecrementIcon: {
    color: "#00565e",
    transform: "scale(1.5)"
  },
  incrementIcon: {
    verticalAlign: "bottom"
  },
  decrementIcon: {
    verticalAlign: "top"
  },
  disabledIcon: {
    // default disabled color
    color: "#DDDDDD"
  },
  timeDetails: {
    display: "table-cell"
  },
  incrementDecrementButton: {
    background: "transparent",
    border: "none",
    width: "50%"
  },
  floatLeft: {
    float: "left"
  },
  floatRight: {
    float: "right"
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  }
};

class SetTimer extends Component {
  // don't forget to set default times using the redux function in SetTimerRedux for validation (setDefaultTimes)
  static propTypes = {
    index: PropTypes.number.isRequired,
    // provide initial hours/minutes values (mandatory - provide them as string for formatting purposes - "03:15" for example)
    currentHours: PropTypes.string.isRequired,
    currentMinutes: PropTypes.string.isRequired,
    // provide min/max hours/minutes if needed (optional), as default values are defined in DEFAULT_VALUES
    minHours: PropTypes.number,
    maxHours: PropTypes.number,
    minMinutes: PropTypes.number,
    maxMinutes: PropTypes.number,
    // provide leap values for incrementation/decrementation of hours/minutes (optional), as default values are defined in DEFAULT_VALUES
    hoursLeaps: PropTypes.number,
    minutesLeaps: PropTypes.number,
    // control whether or not the timer is valid
    customTimeValidationState: PropTypes.bool,

    // provided by redux
    updateTimerState: PropTypes.func,
    triggerTimeUpdate: PropTypes.func,
    updateValidTimerState: PropTypes.func,
    resetTimerStates: PropTypes.func
  };

  state = {
    currentHours: this.props.currentHours,
    currentMinutes: this.props.currentMinutes
  };

  componentDidMount = () => {
    const { currentTimer } = this.props;
    if (typeof currentTimer[this.props.index] === "undefined") {
      currentTimer.push({
        hours: this.props.currentHours,
        minutes: this.props.currentMinutes
      });
      this.props.updateTimerState(currentTimer);
    }
  };

  componentDidUpdate = () => {
    this.props.triggerTimeUpdate();
    // if defaultTimes is defined
    if (this.props.defaultTimes !== []) {
      // validating timer
      this.validateTimer();
    }
  };

  // validating time and updating needed redux state
  validateTimer = () => {
    let isValidTime = true;
    if (typeof this.props.customTimeValidationState !== "undefined") {
      isValidTime = this.props.customTimeValidationState;
    } else {
      // looping in default times array
      for (let i = 0; i < this.props.defaultTimes.length; i++) {
        // current time (in minutes) is les than current default time (invalid)
        if (
          hoursMinutesToMinutes(
            Number(this.props.currentTimer[i].hours),
            Number(this.props.currentTimer[i].minutes)
          ) < this.props.defaultTimes[i]
        ) {
          isValidTime = false;
          break;
        }
      }
    }

    // updating needed redux state
    this.props.updateValidTimerState(isValidTime);
  };

  // incrementing hours number
  incrementHours = () => {
    const { currentTimer } = this.props;
    const updatedHours =
      Number(this.state.currentHours) +
      (this.props.hoursLeaps ? this.props.hoursLeaps : DEFAULT_VALUES.hoursLeaps);
    // updating state with formatted hours number
    this.setState({ currentHours: getFormattedTime(updatedHours) }, () => {
      this.validateTimer();
    });
    currentTimer[this.props.index] = {
      hours: getFormattedTime(updatedHours),
      minutes: this.state.currentMinutes
    };
    // update time redux states
    this.props.updateTimerState(currentTimer);
  };

  // decrementing hours number
  decrementHours = () => {
    const { currentTimer } = this.props;
    const updatedHours =
      Number(this.state.currentHours) -
      (this.props.hoursLeaps ? this.props.hoursLeaps : DEFAULT_VALUES.hoursLeaps);
    // updating state with formatted hours number
    this.setState({ currentHours: getFormattedTime(updatedHours) }, () => {
      this.validateTimer();
    });
    currentTimer[this.props.index] = {
      hours: getFormattedTime(updatedHours),
      minutes: this.state.currentMinutes
    };
    // update time redux state
    this.props.updateTimerState(currentTimer);
  };

  // incrementing minutes number
  incrementMinutes = () => {
    const { currentTimer } = this.props;
    const updatedMinutes =
      Number(this.state.currentMinutes) +
      (this.props.minutesLeaps ? this.props.minutesLeaps : DEFAULT_VALUES.minutesLeaps);
    // updating state with formatted hours number
    this.setState({ currentMinutes: getFormattedTime(updatedMinutes) }, () => {
      this.validateTimer();
    });
    currentTimer[this.props.index] = {
      hours: this.state.currentHours,
      minutes: getFormattedTime(updatedMinutes)
    };
    // update time redux state
    this.props.updateTimerState(currentTimer);
  };

  // decrementing minutes number
  decrementMinutes = () => {
    const { currentTimer } = this.props;
    const updatedMinutes =
      Number(this.state.currentMinutes) -
      (this.props.minutesLeaps ? this.props.minutesLeaps : DEFAULT_VALUES.minutesLeaps);
    // updating state with formatted hours number
    this.setState({ currentMinutes: getFormattedTime(updatedMinutes) }, () => {
      this.validateTimer();
    });
    currentTimer[this.props.index] = {
      hours: this.state.currentHours,
      minutes: getFormattedTime(updatedMinutes)
    };
    // update time redux state
    this.props.updateTimerState(currentTimer);
  };

  render() {
    const accommodationStyles = {
      fontSize: this.props.accommodations.fontSize
    };

    // converting current font size in Int
    const fontSizeInt = parseInt(this.props.accommodations.fontSize.substring(0, 2));
    // initializing timer width
    let timerWidth = 0;
    // converting timerWidth based on selected font size
    switch (true) {
      case fontSizeInt < 12:
        timerWidth = 160;
        break;
      case fontSizeInt >= 12 && fontSizeInt < 16:
        timerWidth = 200;
        break;
      case fontSizeInt >= 16 && fontSizeInt < 20:
        timerWidth = 230;
        break;
      case fontSizeInt >= 20:
        timerWidth = "auto";
        break;
      default:
        timerWidth = 230;
    }

    return (
      <div style={{ ...styles.mainContainer, ...accommodationStyles }}>
        <div style={{ ...styles.timerContainer, ...{ width: timerWidth } }}>
          <div style={styles.timerInsideContainer}>
            <div style={styles.hoursContainer}>
              <div>
                <button
                  id="increment-hours-button"
                  style={styles.incrementDecrementButton}
                  onClick={this.incrementHours}
                  // disabling increment hours button when max hours is reached (defined with maxHours props or 99 if not provided)
                  disabled={
                    !(
                      Number(this.state.currentHours) <
                      (this.props.maxHours ? this.props.maxHours : DEFAULT_VALUES.maxHours)
                    )
                  }
                  aria-label={LOCALIZE.formatString(
                    LOCALIZE.testAdministration.activeCandidates.editTimePopup.incrementHoursButton,
                    this.state.currentHours
                  )}
                >
                  <FontAwesomeIcon
                    icon={faSortUp}
                    style={{
                      ...styles.incrementDecrementIcon,
                      ...styles.incrementIcon,
                      ...(Number(this.state.currentHours) <
                      (this.props.maxHours ? this.props.maxHours : DEFAULT_VALUES.maxHours)
                        ? {}
                        : styles.disabledIcon)
                    }}
                  />
                </button>
              </div>
              <div>
                <span>{this.state.currentHours}</span>
              </div>
              <div>
                <span>{LOCALIZE.testAdministration.activeCandidates.editTimePopup.hours}</span>
              </div>
              <div>
                <button
                  id="decrement-hours-button"
                  style={styles.incrementDecrementButton}
                  onClick={this.decrementHours}
                  // disabling decrement hours button when min hours is reached (defined with minHours props or 0 if not provided)
                  disabled={
                    !(
                      Number(this.state.currentHours) >
                      (this.props.minHours ? this.props.minHours : DEFAULT_VALUES.minHours)
                    )
                  }
                  aria-label={LOCALIZE.formatString(
                    LOCALIZE.testAdministration.activeCandidates.editTimePopup.decrementHoursButton,
                    this.state.currentHours
                  )}
                >
                  <FontAwesomeIcon
                    icon={faSortDown}
                    style={{
                      ...styles.incrementDecrementIcon,
                      ...styles.decrementIcon,
                      ...(Number(this.state.currentHours) >
                      (this.props.minHours ? this.props.minHours : DEFAULT_VALUES.minHours)
                        ? {}
                        : styles.disabledIcon)
                    }}
                  />
                </button>
              </div>
            </div>
            <div style={styles.colonContainer}>
              <span>:</span>
            </div>
            <div style={styles.minutesContainer}>
              <div>
                <div>
                  <button
                    id="increment-minutes-button"
                    style={styles.incrementDecrementButton}
                    onClick={this.incrementMinutes}
                    // disabling increment minutes button when max minutes is reached (defined with maxMinutes props or 59 if not provided)
                    disabled={
                      !(
                        Number(this.state.currentMinutes) <
                        (this.props.maxMinutes ? this.props.maxMinutes : DEFAULT_VALUES.maxMinutes)
                      )
                    }
                    aria-label={LOCALIZE.formatString(
                      LOCALIZE.testAdministration.activeCandidates.editTimePopup
                        .incrementMinutesButton,
                      this.state.currentMinutes
                    )}
                  >
                    <FontAwesomeIcon
                      icon={faSortUp}
                      style={{
                        ...styles.incrementDecrementIcon,
                        ...styles.incrementIcon,
                        ...(Number(this.state.currentMinutes) <
                        (this.props.maxMinutes ? this.props.maxMinutes : DEFAULT_VALUES.maxMinutes)
                          ? {}
                          : styles.disabledIcon)
                      }}
                    />
                  </button>
                </div>
                <div>
                  <span>{this.state.currentMinutes}</span>
                </div>
                <div>
                  <span>{LOCALIZE.testAdministration.activeCandidates.editTimePopup.minutes}</span>
                </div>
                <div>
                  <button
                    id="decrement-minutes-button"
                    style={styles.incrementDecrementButton}
                    onClick={this.decrementMinutes}
                    // disabling decrement minutes button when min minutes is reached (defined with minMinutes props or 0 if not provided)
                    disabled={
                      !(
                        Number(this.state.currentMinutes) >
                        (this.props.minMinutes ? this.props.minMinutes : DEFAULT_VALUES.minMinutes)
                      )
                    }
                    aria-label={LOCALIZE.formatString(
                      LOCALIZE.testAdministration.activeCandidates.editTimePopup
                        .decrementMinutesButton,
                      this.state.currentMinutes
                    )}
                  >
                    <FontAwesomeIcon
                      icon={faSortDown}
                      style={{
                        ...styles.incrementDecrementIcon,
                        ...styles.decrementIcon,
                        ...(Number(this.state.currentMinutes) >
                        (this.props.minMinutes ? this.props.minMinutes : DEFAULT_VALUES.minMinutes)
                          ? {}
                          : styles.disabledIcon)
                      }}
                    />
                  </button>
                </div>
              </div>
              <div style={styles.buttonsContainer}>
                <label tabIndex={0} style={styles.hiddenText}>
                  {LOCALIZE.formatString(
                    LOCALIZE.testAdministration.activeCandidates.editTimePopup.timerCurrentValue,
                    this.state.currentHours,
                    this.state.currentMinutes
                  )}
                </label>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export { SetTimer as unconnectedSetTimer };

const mapStateToProps = (state, ownProps) => {
  return {
    currentTimer: state.timer.currentTimer,
    defaultTimes: state.timer.defaultTimes,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateTimerState,
      triggerTimeUpdate,
      updateValidTimerState,
      resetTimerStates
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(SetTimer);
