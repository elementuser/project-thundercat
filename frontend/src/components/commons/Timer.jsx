import React, { Component } from "react";
import LOCALIZE from "../../text_resources";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMinusCircle, faPlusCircle } from "@fortawesome/free-solid-svg-icons";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import TimerCalculation from "./TimerCalculation";
import CustomButton, { THEME } from "./CustomButton";
import { setCurrentTime, getServerTime } from "../../modules/TestSectionRedux";

const styles = {
  timeContainer: {
    float: "left",
    border: "2px solid #00565e",
    padding: 5,
    borderTopRightRadius: 5,
    borderBottomRightRadius: 5,
    fontWeight: "bold",
    color: "#00565e",
    backgroundColor: "#FFFFFF",
    textAlign: "center"
  },
  toggleButtonOpen: {
    float: "left",
    borderBottomLeftRadius: 5,
    borderTopLeftRadius: 5,
    borderBottomRightRadius: 0,
    borderTopRightRadius: 0
  },
  toggleButton: {
    float: "left",
    borderRadius: 5
  },
  label: {
    paddingLeft: 5
  },
  timeOut: {
    color: "#D3080C"
  }
};

class Timer extends Component {
  static propTypes = {
    timeout: PropTypes.func.isRequired,
    startTime: PropTypes.string,
    timeLimit: PropTypes.number,
    isSampleTest: PropTypes.bool
  };

  state = {
    hidden: false,
    pollingState: undefined
  };

  componentDidMount() {
    // making sure that the current time is synced with the server time
    this.updateTimer();
    // this event listener prevents the timer from lagging when
    // the user minimizes the window or browses another tab
    document.addEventListener("visibilitychange", this.updateTimer);
    // setting up the polling function
    if (this.state.pollingState) {
      clearInterval(this.state.pollingState);
    }
    // creating new polling interval (interval of 30 seconds)
    const interval = setInterval(this.updateTimer, 30000);
    this.setState({ pollingState: interval });
  }

  componentWillUnmount() {
    // remove the event listener when unmounting the timer
    document.removeEventListener("visibilityChange", this.updateTimer);
    clearInterval(this.state.pollingState);
  }

  updateTimer = () => {
    this.props.getServerTime().then(response => {
      this.props.setCurrentTime(response);
    });
  };

  componentDidUpdate = (prevProps, prevState) => {
    if (prevState.hidden !== this.state.hidden) {
      if (!this.state.hidden) {
        this.updateTimer();
      }
    }
  };

  toggleVisibility = () => {
    this.setState({ hidden: !this.state.hidden });
  };

  timeout = () => {
    this.props.timeout();
  };

  render() {
    const { hidden } = this.state;
    const isTimeAlmostOut = false;
    return (
      <div>
        <CustomButton
          label={
            <div>
              <FontAwesomeIcon icon={hidden ? faPlusCircle : faMinusCircle} />
              <span style={styles.label} className="notranslate">
                {LOCALIZE.emibTest.testFooter.timer.timer}
              </span>
            </div>
          }
          id="unit-test-toggle-timer"
          action={this.toggleVisibility}
          customStyle={hidden ? styles.toggleButton : styles.toggleButtonOpen}
          ariaLabel={LOCALIZE.emibTest.testFooter.timer.timerHide}
          ariaPressed={hidden}
          buttonTheme={THEME.PRIMARY_TIME}
        ></CustomButton>
        <div>
          {!hidden && (
            <div style={styles.timeContainer}>
              <div style={isTimeAlmostOut ? styles.timeOut : {}}>
                <span id="unit-test-time-label">
                  <TimerCalculation
                    currentTime={this.props.currentTime}
                    startTime={this.props.startTime}
                    timeLimit={this.props.timeLimit}
                    timeoutFunction={this.timeout}
                    // if this is a sample test AND the count_up flag is set to true
                    countUp={
                      !!(this.props.testSection.is_public && this.props.testSection.count_up)
                    }
                    enableLoadingLogic={true}
                  />
                </span>
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export { Timer as UnconnectedTimer };

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setCurrentTime,
      getServerTime
    },
    dispatch
  );

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    testSection: state.testSection.testSection,
    currentTime: state.testSection.currentTime
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Timer);
