import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import PropTypes from "prop-types";
import PopupBox, { BUTTON_TYPE } from "./PopupBox";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCog, faTimes } from "@fortawesome/free-solid-svg-icons";
import LOCALIZE from "../../text_resources";
import FontFamily from "./FontFamily";
import FontSize from "./FontSize";
import CustomButton, { THEME } from "./CustomButton";
import Spacing from "./Spacing";
import { triggerClosingDialog } from "../../modules/AccommodationsRedux";
import { Row, Col } from "react-bootstrap";

const styles = {
  button: {
    minWidth: 40
  },
  iconLabel: {
    marginRight: 6
  },
  paddingNavButton: {
    padding: "0 5px 0 5px"
  }
};

class Settings extends Component {
  state = {
    isDialogOpen: false
  };

  static propTypes = {
    isTestActive: PropTypes.bool,
    isTestInvalidated: PropTypes.bool
  };

  openDialog = () => {
    this.setState({ isDialogOpen: true });
  };

  closeDialog = () => {
    this.props.triggerClosingDialog();
    this.setState({ isDialogOpen: false });
  };

  render() {
    return (
      <div style={styles.paddingNavButton}>
        <CustomButton
          label={
            <div>
              <span style={styles.iconLabel} className="notranslate">
                {LOCALIZE.siteNavBar.settings}
              </span>
              <FontAwesomeIcon icon={faCog} />
            </div>
          }
          action={this.openDialog}
          customStyle={styles.button}
          buttonTheme={
            this.props.isTestActive || this.props.isTestInvalidated
              ? THEME.PRIMARY_IN_TEST
              : THEME.SECONDARY
          }
          ariaLabel={LOCALIZE.settings.systemSettings}
        />
        <PopupBox
          show={this.state.isDialogOpen}
          handleClose={this.closeDialog}
          title={LOCALIZE.settings.systemSettings}
          description={
            <div>
              <section aria-labelledby="text-size-instructions">
                <Row>
                  <Col>
                    <h2 id="text-size-instructions">{LOCALIZE.settings.fontSize.title}</h2>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <p>{LOCALIZE.settings.fontSize.description}</p>
                  </Col>
                </Row>
                <FontSize />
              </section>
              <section aria-labelledby="font-instructions">
                <Row>
                  <Col>
                    <h2 id="font-instructions">{LOCALIZE.settings.fontStyle.title}</h2>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <p>{LOCALIZE.settings.fontStyle.description}</p>
                  </Col>
                </Row>
                <FontFamily />
              </section>
              <section aria-labelledby="line-spacing-instructions">
                <Row>
                  <Col>
                    <h2 id="line-spacing-instructions">{LOCALIZE.settings.lineSpacing.title}</h2>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <p>{LOCALIZE.settings.lineSpacing.description}</p>
                  </Col>
                </Row>
                <Spacing />
              </section>
              <section aria-labelledby="zoom-instructions">
                <Row>
                  <Col>
                    <h2 id="zoom-instructions">{LOCALIZE.settings.zoom.title}</h2>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <p>{LOCALIZE.settings.zoom.description}</p>
                  </Col>
                </Row>
              </section>
              <section aria-labelledby="color-instructions">
                <Row>
                  <Col>
                    <h2 id="color-instructions">{LOCALIZE.settings.color.title}</h2>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <p>{LOCALIZE.settings.color.description}</p>
                  </Col>
                </Row>
              </section>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonIcon={faTimes}
          rightButtonTitle={LOCALIZE.commons.close}
          rightButtonLabel={LOCALIZE.commons.close}
          rightButtonAction={this.closeDialog}
        />
      </div>
    );
  }
}

export { Settings as UnconnectedSettings };
const mapStateToProps = (state, ownProps) => {
  return {};
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      triggerClosingDialog
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Settings);
