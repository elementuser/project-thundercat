import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import PopupBox, { BUTTON_TYPE, BUTTON_STATE } from "./PopupBox";
import LOCALIZE from "../../text_resources";
import { quitTest } from "../../modules/TestStatusRedux";
import CustomButton, { THEME } from "./CustomButton";
import getCheckboxTransformScale from "../../helpers/checkboxTransformScale";

const styles = {
  button: {
    minWidth: 25
  },
  checkboxMainContainer: {
    padding: "12px 0 0 12px"
  },
  checkboxZone: {
    paddingBottom: 12,
    display: "table",
    height: 36
  },
  checkboxContainer: {
    display: "table-cell",
    verticalAlign: "middle"
  },
  checkboxLabelContainer: {
    display: "table-cell"
  },
  checkbox: {
    margin: "0 16px",
    verticalAlign: "middle"
  },
  checkboxLabel: {
    paddingLeft: 12,
    margin: 0,
    wordWrap: "wrap"
  },
  paddingNavButton: {
    padding: "0 5px 0 5px"
  }
};

const quitConditions = () => {
  return [
    { text: LOCALIZE.emibTest.testFooter.quitTestPopupBox.checkboxOne, checked: false },
    { text: LOCALIZE.emibTest.testFooter.quitTestPopupBox.checkboxTwo, checked: false },
    { text: LOCALIZE.emibTest.testFooter.quitTestPopupBox.checkboxThree, checked: false }
  ];
};

class QuitTest extends Component {
  static propTypes = {
    setFocusOnQuitTestButton: PropTypes.bool.isRequired,
    // Provided by Redux
    isTestActive: PropTypes.bool,
    currentAssignedTestId: PropTypes.number,
    handleQuitTest: PropTypes.func
  };

  state = {
    showQuitPopup: false,
    quitConditions: quitConditions()
  };

  closePopup = () => {
    this.setState({ showQuitPopup: false });
    // reset all checkbox states on close
    this.resetCheckboxStates();
  };

  openQuitPopup = () => {
    // Ensure language state is reset.
    this.setState({ quitConditions: quitConditions() });
    // Open the dialog.
    this.setState({ showQuitPopup: true });
  };

  resetCheckboxStates = () => {
    this.setState({ quitConditions: quitConditions() });
  };

  toggleCheckbox = id => {
    const updatedQuitConditions = Array.from(this.state.quitConditions);
    updatedQuitConditions[id].checked = !updatedQuitConditions[id].checked;
    this.setState({ quitConditions: updatedQuitConditions });
  };

  isChecked = currentCheckbox => {
    return currentCheckbox.checked;
  };

  // focusing on Quit test button on enter eMIB action
  componentDidMount = () => {
    if (this.props.setFocusOnQuitTestButton) {
      document.getElementById("quit-test-btn").focus();
    }
  };

  render() {
    const { quitConditions } = this.state;
    const allChecked = quitConditions.every(this.isChecked);

    const submitButtonState = allChecked ? BUTTON_STATE.enabled : BUTTON_STATE.disabled;

    const checkboxTransformScale = getCheckboxTransformScale(this.props.accommodations.fontSize);
    return (
      <div>
        {this.props.isTestActive && (
          <div style={styles.paddingNavButton}>
            <CustomButton
              label={<span className="notranslate">{LOCALIZE.commons.quitTest}</span>}
              action={this.openQuitPopup}
              customStyle={styles.button}
              buttonTheme={this.props.isTestActive ? THEME.PRIMARY_IN_TEST : THEME.PRIMARY}
              ariaLabel={LOCALIZE.ariaLabel.quitTest}
            />
            <PopupBox
              show={this.state.showQuitPopup}
              handleClose={this.closePopup}
              title={LOCALIZE.emibTest.testFooter.quitTestPopupBox.title}
              description={
                <div>
                  <p className="notranslate">
                    {LOCALIZE.emibTest.testFooter.quitTestPopupBox.description1}
                  </p>
                  <p className="notranslate">
                    {LOCALIZE.emibTest.testFooter.quitTestPopupBox.description2}
                  </p>
                  <div style={styles.checkboxMainContainer}>
                    {this.state.quitConditions.map((condition, id) => {
                      return (
                        <div key={id} style={styles.checkboxZone}>
                          <div style={styles.checkboxContainer}>
                            <input
                              type="checkbox"
                              style={{
                                ...styles.checkbox,
                                ...{ transform: checkboxTransformScale }
                              }}
                              id={`checkbox-condition-${id}`}
                              checked={condition.checked}
                              onChange={() => this.toggleCheckbox(id)}
                            />
                          </div>
                          <div style={styles.checkboxLabelContainer}>
                            <label
                              htmlFor={`checkbox-condition-${id}`}
                              style={styles.checkboxLabel}
                              className="notranslate"
                            >
                              {condition.text}
                            </label>
                          </div>
                        </div>
                      );
                    })}
                  </div>
                </div>
              }
              leftButtonType={BUTTON_TYPE.primary}
              leftButtonTitle={LOCALIZE.commons.returnToTest}
              rightButtonType={BUTTON_TYPE.danger}
              rightButtonTitle={LOCALIZE.commons.quitTest}
              rightButtonAction={this.props.handleQuitTest}
              rightButtonState={submitButtonState}
            />
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    isTestActive: state.testStatus.isTestActive,
    currentAssignedTestId: state.testStatus.currentAssignedTestId,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      quitTest
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(QuitTest);
