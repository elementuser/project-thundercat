import React, { Component } from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { useMenuState, Menu, MenuButton, MenuItem } from "reakit/Menu";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMinusCircle, faPlusCircle, faSortDown } from "@fortawesome/free-solid-svg-icons";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";
import { toggleNotepad, resetNotepadState } from "../../modules/NotepadRedux";
import { toggleCalculator, resetCalculatorState } from "../../modules/CalculatorRedux";
import "../../css/tools-button.css";
import { PATH } from "./Constants";

const styles = {
  menuButton: {
    padding: "6px 12px",
    backgroundColor: "white",
    color: "#00565e",
    borderRadius: "0.35rem",
    border: "1px solid #00565e",
    fontWeight: "bold"
  },
  menuButtonInTest: {
    padding: "6px 12px",
    backgroundColor: "#343a40",
    color: "white",
    borderRadius: "0.35rem",
    border: "1px solid white",
    fontWeight: "bold"
  },
  icon: {
    marginRight: 12,
    color: "white"
  },
  text: {
    color: "white"
  },
  menuItem: {
    position: "relative",
    display: "block",
    border: "none",
    textAlign: "left"
  },
  menu: {
    zIndex: 9999
  },
  menuArrow: {
    verticalAlign: "inherit",
    marginLeft: 4
  },
  row: {
    padding: "10px 0 10px 0"
  },
  paddingNavButton: {
    padding: "0 5px 0 5px"
  }
};

class ToolsButton extends Component {
  constructor(props, context) {
    super(props, context);

    this.PropTypes = {
      calledInRealTest: PropTypes.bool,
      hideButton: PropTypes.bool
    };

    ToolsButton.defaultProps = {
      calledInRealTest: false,
      hideButton: false
    };

    this.state = {
      displayButton: false,
      displayOptions: true,
      menuItems: []
    };
  }

  componentDidMount = () => {
    // if we are currently in the test builder page (not the in-test view)
    if (window.location.pathname === PATH.testBuilder) {
      // resetting calculator and notepad redux states
      this.props.resetCalculatorState();
      this.props.resetNotepadState();
      this.setState({ displayButton: false });
      // checking if the calculator or notepad is enabled in this current test section (display button if that's the case)
    } else if (this.props.testSection.uses_notepad || this.props.testSection.uses_calculator) {
      this.setState({ displayButton: true });
    } else {
      // resetting calculator and notepad redux states
      this.props.resetCalculatorState();
      this.props.resetNotepadState();
      this.setState({ displayButton: false });
    }
  };

  componentDidUpdate = prevProps => {
    // if testSection gets updated
    if (prevProps.testSection !== this.props.testSection) {
      // if we are currently in the test builder page (not the in-test view)
      if (window.location.pathname === PATH.testBuilder) {
        // resetting calculator and notepad redux states
        this.props.resetCalculatorState();
        this.props.resetNotepadState();
        this.setState({ displayButton: false });
        // checking if the calculator or notepad is enabled in this current test section (display button if that's the case)
      } else if (this.props.testSection.uses_notepad || this.props.testSection.uses_calculator) {
        this.setState({ displayButton: true });
      } else {
        // resetting calculator and notepad redux states
        this.props.resetCalculatorState();
        this.props.resetNotepadState();
        this.setState({ displayButton: false });
      }
    }
    // if hideButton gets updated
    if (prevProps.hideButton !== this.props.hideButton) {
      // hideButton is set to true
      if (this.props.hideButton) {
        // resetting calculator and notepad redux states
        this.props.resetCalculatorState();
        this.props.resetNotepadState();
        this.setState({ displayButton: false });
      }
    }
    // if pathname gets updated
    if (prevProps.location.pathname !== this.props.location.pathname) {
      // if pathname does not include sample test or test base path
      if (
        !this.props.location.pathname.includes(PATH.sampleTests) ||
        !this.props.location.pathname.includes(PATH.testBase)
      ) {
        // resetting calculator and notepad redux states
        this.props.resetCalculatorState();
        this.props.resetNotepadState();
        this.setState({ displayButton: false });
      }
    }
  };

  render() {
    const style = {
      fontFamily: this.props.accommodations.fontFamily,
      fontSize: this.props.accommodations.fontSize
    };
    const { myHookValue } = this.props;

    const componentFunctions = {
      style: style,
      state: this.state
    };
    return <>{myHookValue(componentFunctions)}</>;
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    testSection: state.testSection.testSection,
    isNotepadHidden: state.notepad.isNotepadHidden,
    isCalculatorHidden: state.calculator.isCalculatorHidden
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      toggleNotepad,
      resetNotepadState,
      toggleCalculator,
      resetCalculatorState
    },
    dispatch
  );

const getItemList = (props, menu, style) => {
  let className = "";
  let classNameActive = "";

  const items = [];
  if (props.calledInRealTest) {
    className = "dropdown-item-in-test";
    classNameActive = "dropdown-item-in-test dropdown-item-active-in-test";
  } else {
    className = "dropdown-item";
    classNameActive = "dropdown-item dropdown-item-active";
  }

  if (props.testSection.uses_calculator) {
    items.push([
      <MenuItem
        className={props.isCalculatorHidden ? className : classNameActive}
        {...menu}
        key={1}
        style={styles.menuItem}
        onClick={() => props.toggleCalculator()}
        id={1}
      >
        <FontAwesomeIcon
          style={styles.icon}
          icon={props.isCalculatorHidden ? faPlusCircle : faMinusCircle}
        />
        <span style={{ ...styles.text, ...style }} className="notranslate">
          {LOCALIZE.commons.calculator.title}
        </span>
      </MenuItem>
    ]);
  }
  if (props.testSection.uses_notepad) {
    items.push([
      <MenuItem
        className={props.isNotepadHidden ? className : classNameActive}
        {...menu}
        key={2}
        style={styles.menuItem}
        onClick={() => props.toggleNotepad()}
        id={2}
      >
        <FontAwesomeIcon
          style={styles.icon}
          icon={props.isNotepadHidden ? faPlusCircle : faMinusCircle}
        />
        <span style={{ ...styles.text, ...style }} className="notranslate">
          {LOCALIZE.commons.notepad.title}
        </span>
      </MenuItem>
    ]);
  }
  return items;
};

function MenuHooks(props) {
  const menu = useMenuState();
  return function renderReakit(componentFunctions) {
    return (
      <>
        {componentFunctions.state.displayButton && (
          <div style={styles.paddingNavButton}>
            <div className="notranslate">
              <MenuButton
                {...menu}
                className={props.calledInRealTest ? "tools-button-in-test" : "tools-button"}
                style={
                  props.calledInRealTest
                    ? { ...styles.menuButtonInTest, ...componentFunctions.style }
                    : { ...styles.menuButton, ...componentFunctions.style }
                }
                id="in-test-tools-menu-button"
              >
                {LOCALIZE.commons.tools}
                <FontAwesomeIcon style={styles.menuArrow} icon={faSortDown} />
              </MenuButton>
              <Menu {...menu} aria-label={LOCALIZE.commons.tools} style={{ ...styles.menu }}>
                {getItemList(props, menu, componentFunctions.style).map((item, id) => (
                  <div key={id}>{item}</div>
                ))}
              </Menu>
            </div>
          </div>
        )}
      </>
    );
  };
}

function withMyHook(Component) {
  return function WrappedComponent(componentFunctions) {
    const myHookValue = MenuHooks(componentFunctions);
    return <Component {...componentFunctions} myHookValue={myHookValue} />;
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(withMyHook(ToolsButton)));
