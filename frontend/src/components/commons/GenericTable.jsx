import React, { Component } from "react";
import PropTypes from "prop-types";
import { alternateColorsStyle } from "./Constants";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import { Table } from "react-bootstrap";
import "../../css/cat-theme.css";

export const COMMON_STYLE = {
  CENTERED_TEXT: {
    textAlign: "center",
    padding: 12
  },
  LEFT_TEXT: {
    textAlign: "left",
    padding: 12
  }
};

const styles = {
  table: {
    width: "100%",
    minHeight: 100,
    marginBottom: 0
  },
  tableHead: {
    minHeight: 60,
    backgroundColor: "#00565e",
    color: "white",
    fontWeight: "bold",
    position: "sticky",
    top: 0,
    zIndex: 1
  },
  paddingLeft: {
    paddingLeft: 12
  },
  displayNone: {
    display: "none"
  },
  loading: {
    width: "100%",
    minHeight: 60,
    textAlign: "center",
    fontSize: "32px",
    padding: 6
  },
  td: {
    verticalAlign: "middle"
  },
  nonInfiniteWordLength: {
    maxWidth: 500,
    wordWrap: "break-word"
  },
  noBorderBottom: {
    borderBottom: "none"
  }
};

// used to make changes to the String
export const getDecodedString = str => {
  // if it is a Date, replace '-' with ''&#8209;' so it doesn't break-word
  if (Date.parse(str)) {
    const txt = document.createElement("textarea");
    txt.innerHTML = str.toString().replaceAll("-", "&#8209;");
    return txt.value;
  }

  // if it is a time in format HH : mm, replace ' ' with '&nbsp;' so it doesn't break-word
  if (str && str.toString().includes(":")) {
    const isValid = /^([0-1]?[0-9]|2[0-4]) : ([0-5][0-9])(:[0-5][0-9])?$/.test(str);

    if (isValid) {
      const txt = document.createElement("textarea");
      txt.innerHTML = str.toString().replaceAll(" ", "&nbsp;");
      return txt.value;
    }
  }

  // otherwise, return the same initial String
  return str;
};

class GenericTable extends Component {
  constructor(props, context) {
    super(props, context);

    this.PropTypes = {
      classnamePrefix: PropTypes.string.isRequired,
      /* accessibility props
      1. your table contains buttons?
          ==> set this props to true or don't define it (default is true) + implement your own aria-label inside your <button> tag
      2. your table doesn't contain any button?
          ==> set this props to false (aria-label logic will be implemented automatically)
      */
      tableWithButtons: PropTypes.bool,
      /* 
      parameters:
        - label (required)
        - style (if no style is needed, simply put "style: {}"")
      example: [{ label: "Column 1", style: { color: "red" } }, { label: "Column 2", style: {} }]
      */
      columnsDefinition: PropTypes.array.isRequired,
      /*
      parameters:
        - column_x_style (column_1_style: {})
        - data (data: [ { column_1: "", column_2: "", column_3: <button />, ... }, {}, {}, ... ])
      */
      rowsDefinition: PropTypes.object.isRequired,
      // provide desired message for when rowsDefinition = {} or when rowsDefinition.data = []
      emptyTableMessage: PropTypes.string.isRequired,
      currentlyLoading: PropTypes.bool.isRequired,
      triggerReRender: PropTypes.bool,
      hasHeaderHtml: PropTypes.bool,
      headerHtml: PropTypes.string,
      hasLastRowHtml: PropTypes.bool,
      lastRowHtml: PropTypes.string,
      style: PropTypes.object
    };

    GenericTable.defaultProps = {
      tableWithButtons: true,
      hasHeaderHtml: false,
      hasLastRowHtml: false,
      style: {}
    };
  }

  state = {
    dataProvided: false,
    ariaLabel: ""
  };

  componentDidUpdate = prevProps => {
    // if rowsDefinition gets updated
    if (
      prevProps.rowsDefinition !== this.props.rowsDefinition ||
      prevProps.triggerReRender !== this.props.triggerReRender
    ) {
      // set dataProvided to false whenever rowsDefinition gets updated
      this.setState({ dataProvided: false });
      // if rowsDefinition data object is defined
      if (typeof this.props.rowsDefinition.data !== "undefined") {
        // if rowsDefinition data is not empty
        if (this.props.rowsDefinition.data.length > 0) {
          // set dataProvided to true
          this.setState({ dataProvided: true });
        }
      }
    }
  };

  // dynamically populating each table row based on the number of columns provided (props)
  populateTableRow = row => {
    // initializing rowData array
    const rowData = [];
    // looping in numberOfColumns props
    for (let i = 1; i <= this.props.columnsDefinition.length; i++) {
      // pushing column data in rowData array (generating dynamically all needed <td> tags)
      rowData.push(
        <td
          key={i}
          id={`${this.props.classnamePrefix}-column-${i}-label`}
          // by default, there is some padding-left style applied to row items
          style={{
            ...styles.td,
            ...styles.paddingLeft,
            ...this.props.rowsDefinition[`column_${i}_style`],
            ...styles.nonInfiniteWordLength
          }}
        >
          <div className="notranslate">{getDecodedString(row[`column_${i}`])}</div>
        </td>
      );
    }
    // returning array of <td> tags
    return rowData;
  };

  // generating aria-label for tables without buttons (accessibility)
  // this function will be called only if tableWithButtons props is set to false (default is true)
  generateAriaLabel = row => {
    let ariaLabel = "";
    // looping in columnsDefinition
    for (let i = 1; i <= this.props.columnsDefinition.length; i++) {
      // concatenate string to generate the needed aria-label
      ariaLabel = ariaLabel.concat(
        `${this.props.columnsDefinition[i - 1].label}, ${row[`column_${i}`]}, `
      );
    }
    // saving result in state
    this.setState({ ariaLabel: ariaLabel });
  };

  render() {
    return (
      <Table
        responsive
        id={`${this.props.classnamePrefix}-table-id`}
        style={{ ...styles.table, ...this.props.style }}
        role="table"
      >
        <thead tabIndex={0}>
          <tr id={`${this.props.classnamePrefix}-table-header-id`} style={styles.tableHead}>
            {this.props.hasHeaderHtml && this.props.headerHtml}
            {!this.props.hasHeaderHtml &&
              this.props.columnsDefinition.map((column, id) => {
                return (
                  <th
                    key={id}
                    // using +1 here to start at index 1 instead of 0
                    id={`${this.props.classnamePrefix}-column-${id + 1}`}
                    scope="col"
                    // by default, there is some padding-left style applied to column items
                    style={{
                      ...styles.paddingLeft,
                      ...column.style,
                      ...styles.nonInfiniteWordLength,
                      ...styles.noBorderBottom
                    }}
                    className="align-middle"
                  >
                    {column.label}
                  </th>
                );
              })}
          </tr>
        </thead>
        <tbody>
          {this.state.dataProvided &&
            !this.props.currentlyLoading &&
            // making sure that the rowsDefinition.data is defined to avoid page break
            typeof this.props.rowsDefinition.data !== "undefined" &&
            this.props.rowsDefinition.data.map((row, id) => {
              return (
                <tr
                  id={`${this.props.classnamePrefix}-table-row-${id}`}
                  key={id}
                  style={alternateColorsStyle(id, 60)}
                  tabIndex={this.props.tableWithButtons ? "-1" : "0"}
                  onFocus={
                    this.props.tableWithButtons
                      ? () => {}
                      : () => {
                          this.generateAriaLabel(row);
                        }
                  }
                  aria-label={this.state.ariaLabel}
                >
                  {this.populateTableRow(row)}
                </tr>
              );
            })}
          {!this.state.dataProvided && !this.props.currentlyLoading && (
            <tr id={`${this.props.classnamePrefix}-no-data`} tabIndex={0}>
              <td
                colSpan={this.props.columnsDefinition.length}
                style={{
                  ...alternateColorsStyle(0, 60),
                  ...styles.td,
                  ...styles.nonInfiniteWordLength
                }}
              >
                <label className="notranslate">{this.props.emptyTableMessage}</label>
              </td>
            </tr>
          )}
          {this.props.currentlyLoading && (
            <tr style={styles.loading}>
              <td
                colSpan={this.props.columnsDefinition.length}
                style={styles.nonInfiniteWordLength}
              >
                {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
                <label className="fa fa-spinner fa-spin">
                  <FontAwesomeIcon icon={faSpinner} />
                </label>
              </td>
            </tr>
          )}
          {this.props.hasLastRowHtml && (
            <tr aria-label={this.state.ariaLabel}>{this.props.lastRowHtml}</tr>
          )}
        </tbody>
      </Table>
    );
  }
}

export default GenericTable;
