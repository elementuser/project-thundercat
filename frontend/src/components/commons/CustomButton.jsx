import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import "../../css/custom-button.css";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";

export const THEME = {
  PRIMARY: "primary",
  PRIMARY_IN_TEST: "primary-in-test",
  SECONDARY: "secondary",
  SUCCESS: "success",
  DANGER: "danger",
  PRIMARY_WIDE: "primary-wide",
  PRIMARY_TIME: "primary-time"
};

const styles = {
  basics: {
    minWidth: 100,
    fontWeight: "bold",
    padding: "6px 12px",
    borderRadius: "0.35rem",
    border: "1px solid transparent"
  },
  disabled: {
    opacity: 0.4,
    cursor: "not-allowed"
  }
};

class CustomButton extends Component {
  static propTypes = {
    buttonId: PropTypes.string,
    className: PropTypes.string,
    label: PropTypes.any.isRequired,
    action: PropTypes.func,
    type: PropTypes.string,
    customStyle: PropTypes.object,
    buttonTheme: PropTypes.string,
    disabled: PropTypes.bool,
    language: PropTypes.string,
    ariaLabel: PropTypes.string,
    ariaExpanded: PropTypes.bool,
    ariaDescribedBy: PropTypes.string,
    tabIndex: PropTypes.string,
    ariaPressed: PropTypes.bool,
    // needed for tooltips (react-tooltip/StyledTooltip)
    dataFor: PropTypes.string,
    dataTip: PropTypes.string
  };

  render() {
    let accommodationsStyle = {};

    // Only set the fontSize to the accommodation styling if
    // we haven't specified a customStyle that has a fontSize
    if ((this.props.customStyle && !this.props.customStyle.fontSize) || !this.props.customStyle) {
      accommodationsStyle = {
        fontSize: this.props.accommodations.fontSize
      };
    }

    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    let style = { ...styles.basics, ...this.props.customStyle, ...accommodationsStyle };
    if (this.props.disabled) {
      style = { ...style, ...styles.disabled };
    }

    return (
      <button
        id={this.props.buttonId}
        className={`${this.props.buttonTheme} ${this.props.className}`}
        data-for={this.props.dataFor}
        data-tip={this.props.dataTip}
        style={style}
        type={this.props.type}
        onClick={() => {
          if (!this.props.disabled && this.props.action) {
            this.props.action();
          }
        }}
        lang={this.props.lang}
        aria-label={this.props.ariaLabel}
        aria-expanded={this.props.ariaExpanded}
        aria-describedby={this.props.ariaDescribedBy}
        tabIndex={this.props.tabIndex}
        aria-pressed={this.props.ariaPressed}
        aria-disabled={this.props.disabled}
      >
        {this.props.label}
      </button>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations
  };
};

export default connect(mapStateToProps, null)(CustomButton);
