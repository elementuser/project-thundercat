import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import CustomButton, { THEME } from "./CustomButton";
import DropdownSelect from "./DropdownSelect";
import getDisplayOptions from "../../helpers/getDisplayOptions";
import { Row, Col } from "react-bootstrap";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 7,
    xl: 7
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 5,
    xl: 5
  }
};

const styles = {
  searchBarAndDisplayContainer: {
    margin: "18px 0 12px 0",
    width: "100%"
  },
  searchBarLabel: {
    fontWeight: "bold",
    textAlign: "right",
    paddingLeft: 0
  },
  searchBarContainer: {
    position: "relative"
  },
  searchBarInput: {
    minHeight: 38,
    border: "1px solid #00565e",
    borderRadius: "4px 0 0 4px",
    height: "100%"
  },
  searchIconButton: {
    padding: "3px 12px",
    minWidth: 25,
    minHeight: 38,
    borderLeft: "none",
    borderRadius: "0 4px 4px 0"
  },
  clearSearchResults: {
    background: "transparent",
    border: "none",
    marginLeft: 18,
    cursor: "pointer",
    padding: "4px 8px",
    // default link color code
    color: "#0000EE"
  },
  displayOptionContainer: {
    textAlign: "right",
    verticalAlign: "middle"
  },
  displayOptionLabel: {
    fontWeight: "bold",
    paddingLeft: 0
  },
  displayOptionDropdown: {
    float: "right",
    textAlign: "center",
    maxWidth: 200,
    paddingRight: 0
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  col: {
    display: "inline-block"
  },
  searchBarCol: {
    paddingRight: 0
  },
  searchBarLabelHidden: {
    fontWeight: "bold",
    paddingLeft: 0,
    color: "transparent",
    userSelect: "none" /* Standard */
  },
  bottomRowSearch: {
    height: "fit-content"
  },
  hiddenBottomRowSearch: {
    height: "0px",
    marginBottom: 0,
    marginRight: 0,
    marginLeft: 0
  },
  marginLeft: {
    marginLeft: 12
  }
};

class SearchBarWithDisplayOptions extends Component {
  static propTypes = {
    idPrefix: PropTypes.string.isRequired,
    currentlyLoading: PropTypes.bool.isRequired,
    updateSearchStates: PropTypes.func.isRequired,
    updatePageState: PropTypes.func.isRequired,
    paginationPageSize: PropTypes.number.isRequired,
    updatePaginationPageSize: PropTypes.func.isRequired,
    data: PropTypes.array.isRequired,
    resultsFound: PropTypes.number.isRequired,
    searchBarContent: PropTypes.string
  };

  state = {
    searchBarContent: this.props.searchBarContent ? this.props.searchBarContent : "",
    displayResultsFound: false,
    displayOptionSelectedValue: {
      label: `${this.props.paginationPageSize}`,
      value: this.props.paginationPageSize
    },
    displayOptionsArray: []
  };

  componentDidMount = () => {
    this.populateDisplayOptions();
    if (this.state.searchBarContent !== "") {
      this.handleSearch();
    }
  };

  // populate display options
  populateDisplayOptions = () => {
    this.setState({ displayOptionsArray: getDisplayOptions() });
  };

  // update searchBarContent content
  updateSearchBarContent = event => {
    const searchBarContent = event.target.value;
    this.setState({
      searchBarContent: searchBarContent
    });
  };

  // handling search functionality
  handleSearch = () => {
    // initializing searchBarContent state to null so search functionality is triggered even if the search bar content is still the same
    this.props.updateSearchStates(null, true);
    this.setState({ displayResultsFound: true }, () => {
      // go back to the first page to avoid display bugs
      this.props.updatePageState(1);
      // updating search keyword and active search redux states
      this.props.updateSearchStates(this.state.searchBarContent, true);
    });
  };

  // handling clear search functionality
  handleClearSearch = () => {
    // updating search keyword and active search redux states
    this.props.updateSearchStates("", false);
    // hide results found label + put back all existing data + delete search bar content
    this.setState(
      {
        displayResultsFound: false,
        searchBarContent: ""
      },
      () => {
        // go back to the first page to avoid display bugs
        this.props.updatePageState(1);
        // focusing on search bar
        document.getElementById(`${this.props.idPrefix}-search-bar`).focus();
      }
    );
  };

  // get selected display option
  getSelectedDisplayOption = selectedOption => {
    this.setState(
      {
        displayOptionSelectedValue: selectedOption
      },
      () => {
        // update page size
        this.props.updatePaginationPageSize(selectedOption.value);
        // go back to the first page to avoid display bugs
        this.props.updatePageState(1);
      }
    );
  };

  render() {
    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <Row
        className="align-items-start justify-content-center"
        style={styles.searchBarAndDisplayContainer}
      >
        <Col
          xl={columnSizes.firstColumn.xl}
          lg={columnSizes.firstColumn.lg}
          md={columnSizes.firstColumn.md}
          sm={columnSizes.firstColumn.sm}
          xs={columnSizes.firstColumn.xs}
        >
          <Row className="align-items-center justify-content-start">
            <Col xs={"auto"} style={styles.searchBarLabel}>
              <label
                htmlFor={`${this.props.idPrefix}-search-bar`}
                id={`${this.props.idPrefix}-search-bar-title`}
              >
                {LOCALIZE.SearchBarWithDisplayOptions.searchBarTitle}
              </label>
            </Col>
            <Col style={styles.searchBarCol}>
              <div className="input-group" style={styles.searchBarContainer}>
                <input
                  id={`${this.props.idPrefix}-search-bar`}
                  className="valid-field form-control"
                  aria-labelledby={`search-bar-title`}
                  style={{ ...styles.searchBarInput, ...accommodationsStyle }}
                  type="text"
                  value={this.state.searchBarContent}
                  onChange={this.updateSearchBarContent}
                  onKeyPress={event => {
                    if (event.key === "Enter" && !this.props.currentlyLoading) {
                      this.handleSearch();
                    }
                  }}
                ></input>
                <div className="input-group-append">
                  <CustomButton
                    label={
                      <>
                        <FontAwesomeIcon icon={faSearch} />
                        <label style={styles.hiddenText}>
                          {LOCALIZE.SearchBarWithDisplayOptions.searchBarTitle}
                        </label>
                      </>
                    }
                    action={this.handleSearch}
                    customStyle={styles.searchIconButton}
                    buttonTheme={THEME.SECONDARY}
                    disabled={this.props.currentlyLoading}
                    ariaLabel={LOCALIZE.SearchBarWithDisplayOptions.searchBarTitle}
                  />
                </div>
              </div>
            </Col>
            <div className="w-100"></div>
            <Col
              xs={"auto"}
              style={
                this.state.displayResultsFound && !this.props.currentlyLoading
                  ? { ...styles.searchBarLabelHidden, ...styles.bottomRowSearch }
                  : { ...styles.hiddenBottomRowSearch, ...styles.searchBarLabelHidden }
              }
            >
              <label
                aria-hidden={"true"}
                htmlFor={`${this.props.idPrefix}-search-bar`}
                id={`${this.props.idPrefix}-search-bar-title`}
              >
                {LOCALIZE.SearchBarWithDisplayOptions.searchBarTitle}
              </label>
            </Col>
            <Col>
              {this.state.displayResultsFound && !this.props.currentlyLoading && (
                <div id="search-bar-results-found-main-div">
                  <label
                    id={`${this.props.idPrefix}-results-found`}
                    htmlFor={`${this.props.idPrefix}-search-bar`}
                    tabIndex={0}
                  >
                    {LOCALIZE.formatString(
                      LOCALIZE.SearchBarWithDisplayOptions.resultsFound,
                      this.props.resultsFound
                    )}
                  </label>
                  <button
                    className="clear-search"
                    style={styles.clearSearchResults}
                    tabIndex={0}
                    onClick={this.handleClearSearch}
                    aria-label={LOCALIZE.SearchBarWithDisplayOptions.clearSearch}
                  >
                    {LOCALIZE.SearchBarWithDisplayOptions.clearSearch}
                  </button>
                </div>
              )}
            </Col>
          </Row>
        </Col>
        <Col
          xl={columnSizes.secondColumn.xl}
          lg={columnSizes.secondColumn.lg}
          md={columnSizes.secondColumn.md}
          sm={columnSizes.secondColumn.sm}
          xs={columnSizes.secondColumn.xs}
          style={styles.displayOptionContainer}
        >
          <Row className="justify-content-end align-items-center" style={styles.marginLeft}>
            <Col xs={"auto"} style={styles.displayOptionLabel}>
              <label id={`${this.props.idPrefix}-display-options-label`}>
                {LOCALIZE.SearchBarWithDisplayOptions.displayOptionLabel}
              </label>
              <label
                id={`${this.props.idPrefix}-display-option-accessibility`}
                style={styles.hiddenText}
              >
                {LOCALIZE.SearchBarWithDisplayOptions.displayOptionAccessibility}
              </label>
            </Col>
            <Col style={styles.displayOptionDropdown}>
              <DropdownSelect
                idPrefix={`${this.props.idPrefix}-display-options-dropdown`}
                ariaLabelledBy={`${this.props.idPrefix}-display-option-accessibility`}
                options={this.state.displayOptionsArray}
                onChange={this.getSelectedDisplayOption}
                defaultValue={this.state.displayOptionSelectedValue}
                orderByLabels={false}
                className={"col"}
              ></DropdownSelect>
              <label
                id={`${this.props.idPrefix}-display-option-current-value-accessibility`}
                style={styles.hiddenText}
              >{`${LOCALIZE.SearchBarWithDisplayOptions.displayOptionCurrentValueAccessibility} ${this.state.displayOptionSelectedValue.value}`}</label>
            </Col>
          </Row>
        </Col>
        <a
          href={`#${this.props.idPrefix}-pagination-pages-link`}
          className="visually-hidden"
          id="skip-to-pagination-pages-link-id"
        >
          {LOCALIZE.commons.pagination.skipToPagesLink}
        </a>
      </Row>
    );
  }
}

export { SearchBarWithDisplayOptions as unconnectedSearchBarWithDisplayOptions };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations
  };
};

export default connect(mapStateToProps, null)(SearchBarWithDisplayOptions);
