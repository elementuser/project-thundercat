import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import LOCALIZE from "../../text_resources";
import ContentContainer from "./ContentContainer";
import TimerCalculation from "./TimerCalculation";
import {
  unpauseTest,
  updateTestStatus,
  deactivateTest,
  getCurrentTestStatus,
  invalidateTest
} from "../../modules/TestStatusRedux";
import {
  setTopTabsHeight,
  setTestFooterHeight,
  resetTestFactoryState,
  getServerTime,
  setCurrentTime
} from "../../modules/TestSectionRedux";
import TEST_STATUS from "../ta/Constants";
import { PATH } from "./Constants";
import SessionStorage, { ACTION, ITEM } from "../../SessionStorage";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";
import getInTestHeightCalculations from "../../helpers/inTestHeightCalculations";
import { resetNotepadState } from "../../modules/NotepadRedux";
import { resetAssignedTestState } from "../../modules/AssignedTestsRedux";
import { resetTopTabsState } from "../../modules/NavTabsRedux";
import { resetQuestionListState } from "../../modules/QuestionListRedux";
import { resetInboxState } from "../../modules/EmailInboxRedux";
import { Navbar } from "react-bootstrap";
import CustomButton, { THEME } from "./CustomButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPauseCircle, faTimes } from "@fortawesome/free-solid-svg-icons";
import PopupBox, { BUTTON_TYPE } from "./PopupBox";
import { BreakBankActions } from "../testFactory/Constants";
import { triggerBreakBankAction } from "../../modules/BreakBankRedux";
import moment from "moment";

const styles = {
  tabSection: {
    paddingLeft: 18,
    marginBottom: "-3px"
  },
  tabLabel: {
    padding: "4px 24px",
    marginBottom: 0,
    position: "relative"
  },
  tabBorder: {
    borderBottom: "5px solid #00565e",
    width: "100%",
    position: "absolute",
    bottom: 0,
    left: 0,
    borderRadius: "4px 4px 0 0"
  },
  tabContentContainer: {
    borderStyle: "solid",
    borderWidth: "3px 1px 1px 1px",
    borderColor: "#CECECE",
    padding: "12px 0",
    overflowY: "scroll"
  },
  descriptionContainer: {
    margin: 60,
    fontWeight: "bold"
  },
  timerContainer: {
    textAlign: "center"
  },
  timer: {
    width: 300,
    height: 100,
    color: "#00565e",
    fontWeight: "bold",
    margin: "0 auto",
    border: "1px solid #00565e",
    display: "table"
  },
  customTimerStyle: {
    display: "table-cell",
    verticalAlign: "middle",
    padding: 12
  },
  footer: {
    borderTop: "1px solid #96a8b2",
    backgroundColor: "#D5DEE0"
  },
  footerIndex: {
    zIndex: 1
  },
  unpauseButtonContainer: {
    margin: "0 auto"
  },
  buttonLabel: {
    margin: 0,
    paddingLeft: 6
  }
};

class PauseScreen extends Component {
  static propTypes = {
    testId: PropTypes.number,
    assignedTestId: PropTypes.number,
    previousStatus: PropTypes.number,
    testAccessCode: PropTypes.string,
    // Provided by Redux
    unpauseTest: PropTypes.func,
    updateTestStatus: PropTypes.func,
    deactivateTest: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.topTabsHeightRef = React.createRef();
    this.testFooterHeightRef = React.createRef();
  }

  state = {
    showUnpausePopup: false,
    pollingState: undefined
  };

  componentDidMount = () => {
    // focusing on test locked tab
    if (document.getElementById("test-paused-tab")) {
      document.getElementById("test-paused-tab").focus();
    }
    // updating nav bar and test footer height redux state
    if (this.topTabsHeightRef.current) {
      this.props.setTopTabsHeight(this.topTabsHeightRef.current.clientHeight);
    }
    // updating footer height redux state
    if (this.testFooterHeightRef.current) {
      this.props.setTestFooterHeight(this.testFooterHeightRef.current.clientHeight);
    }

    this.gettingAndSettingServerTime();

    // setting up the polling function
    if (this.state.pollingState) {
      clearInterval(this.state.pollingState);
    }
    // creating new polling interval (interval of 30 seconds)
    const interval = setInterval(this.gettingAndSettingServerTime, 30000);
    this.setState({ pollingState: interval });
  };

  componentDidUpdate = prevProps => {
    // updating nav bar and test footer height redux state
    if (this.topTabsHeightRef.current) {
      this.props.setTopTabsHeight(this.topTabsHeightRef.current.clientHeight);
    }

    // update on font size change
    if (prevProps.accommodations !== this.props.accommodations) {
      this.props.setTestFooterHeight(this.testFooterHeightRef.current.clientHeight);
    }
  };

  componentWillUnmount = () => {
    clearInterval(this.state.pollingState);
  };

  gettingAndSettingServerTime = () => {
    // getting and setting current time
    this.props.getServerTime().then(response => {
      this.props.setCurrentTime(response);
    });
  };

  handleTimerTimeout = () => {
    // initializing kickoutUser
    let kickoutUser = false;
    // getting current test status in order to make sure that it is the right one (if not, redirect the user accordingly)
    this.props
      .getCurrentTestStatus(this.props.assignedTestId)
      .then(testStatusData => {
        // current section is a pre-test section
        if (this.props.testSection.default_time === null) {
          // expected test status = PRE_TEST
          if (
            testStatusData.status !== TEST_STATUS.PRE_TEST &&
            testStatusData.status !== TEST_STATUS.LOCKED &&
            testStatusData.status !== TEST_STATUS.PAUSED
          ) {
            // kicking candidate out
            // invalidating test + redirecting to invalidated test page
            this.props.invalidateTest();
            this.props.resetTestFactoryState();
            this.props.resetInboxState();
            this.props.resetNotepadState();
            this.props.resetAssignedTestState();
            this.props.resetQuestionListState();
            this.props.resetTopTabsState();
            kickoutUser = true;
            window.location.href = PATH.invalidateTest;
          }
        } // current section is a timed section
        else if (this.props.testSection.default_time !== null) {
          // expected test status = ACTIVE
          if (
            testStatusData.status !== TEST_STATUS.ACTIVE &&
            testStatusData.status !== TEST_STATUS.LOCKED &&
            testStatusData.status !== TEST_STATUS.PAUSED
          ) {
            // kicking candidate out
            // invalidating test + redirecting to invalidated test page
            this.props.invalidateTest();
            this.props.resetTestFactoryState();
            this.props.resetInboxState();
            this.props.resetNotepadState();
            this.props.resetAssignedTestState();
            this.props.resetQuestionListState();
            this.props.resetTopTabsState();
            kickoutUser = true;
            window.location.href = PATH.invalidateTest;
          }
        }
        return kickoutUser;
      })
      .then(kickoutUser => {
        if (!kickoutUser) {
          // test was paused but not started yet
          if (this.props.previousStatus === TEST_STATUS.READY) {
            this.props
              .updateTestStatus(this.props.assignedTestId, TEST_STATUS.READY, null)
              .then(() => {
                // deactivating the test
                this.props.deactivateTest();
                // removing TEST_ACTIVE from local storage
                SessionStorage(ACTION.REMOVE, ITEM.TEST_ACTIVE);
                // redirecting to dashboard page
                window.location.href = PATH.dashboard;
              });
            // test was paused while in pre-test sections
          } else if (this.props.previousStatus === TEST_STATUS.PRE_TEST) {
            this.props
              .updateTestStatus(this.props.assignedTestId, TEST_STATUS.PRE_TEST, null)
              .then(() => {
                // redirecting to test page
                window.location.href = PATH.testBase;
              });
            // test was started and then paused
          } else {
            this.props
              .updateTestStatus(this.props.assignedTestId, TEST_STATUS.ACTIVE, TEST_STATUS.PAUSED)
              .then(() => {
                // unpausing test
                this.props.unpauseTest();
              });
          }
        }
      });
  };

  handleUnpause = () => {
    const data = {
      accommodation_request_id: this.props.accommodationRequestId,
      action_type: BreakBankActions.UNPAUSE,
      test_section_id: this.props.testSection.id
    };
    this.props.triggerBreakBankAction(data).then(response => {
      // successful request
      if (response.ok) {
        // getting and setting current time
        this.props.getServerTime().then(response => {
          this.props.setCurrentTime(response);
          // unpausing test
          this.props.unpauseTest();
        });
        // should never happen
      } else {
        throw new Error("An error occurred during the pause request process");
      }
    });
  };

  openUnpausePopup = () => {
    this.setState({ showUnpausePopup: true });
  };

  closeUnpausePopup = () => {
    this.setState({ showUnpausePopup: false });
  };

  render() {
    const accommodationStyles = {
      fontFamily: this.props.accommodations.fontFamily,
      // adding 32px to provided accommodations font size
      fontSize: `${parseInt(this.props.accommodations.fontSize.split("px")[0]) + 32}px`
    };

    const tabTitleCustomFontSize = {
      // adding 12px to selected font-size
      fontSize: `${parseInt(this.props.accommodations.fontSize.split("px")[0]) + 12}px`
    };

    const customHeight = getInTestHeightCalculations(
      this.props.accommodations.fontSize,
      this.props.accommodations.spacing,
      this.props.testNavBarHeight,
      this.props.topTabsHeight,
      this.props.testFooterHeight
    );

    const dynamicMarginBottom = this.props.accommodations.spacing ? { marginBottom: "-5em" } : {};
    return (
      <div>
        <ContentContainer>
          <div
            id="test-paused-tab"
            ref={this.topTabsHeightRef}
            style={styles.tabSection}
            tabIndex={0}
          >
            <div>
              <label style={{ ...styles.tabLabel, ...tabTitleCustomFontSize }}>
                {LOCALIZE.candidatePauseScreen.tabTitle}
                <span style={styles.tabBorder}></span>
              </label>
            </div>
          </div>
          <div
            style={{ ...styles.tabContentContainer, ...customHeight, ...dynamicMarginBottom }}
            tabIndex={0}
          >
            <div style={styles.descriptionContainer}>
              <p>{LOCALIZE.candidatePauseScreen.description.part1}</p>
              <p>{LOCALIZE.candidatePauseScreen.description.part2}</p>
              <p>{LOCALIZE.candidatePauseScreen.description.part3}</p>
            </div>
            <div style={styles.timerContainer}>
              <div
                style={{
                  ...accommodationStyles,
                  ...styles.timer,
                  ...(this.props.accommodations.spacing
                    ? { ...getLineSpacingCSS(), ...{ paddingBottom: "0.5em" } }
                    : {})
                }}
              >
                <TimerCalculation
                  currentTime={
                    typeof this.props.currentTime === "string"
                      ? moment(this.props.currentTime)
                      : this.props.currentTime
                  }
                  startTime={this.props.pauseStartDate}
                  timeLimit={this.props.pauseTime}
                  timeoutFunction={this.handleTimerTimeout}
                  customStyle={styles.customTimerStyle}
                />
              </div>
            </div>
          </div>
        </ContentContainer>
        <div role="contentinfo" className="fixed-bottom" style={styles.footerIndex}>
          <Navbar ref={this.testFooterHeightRef} fixed="bottom" style={styles.footer}>
            <div style={styles.unpauseButtonContainer}>
              <CustomButton
                buttonId="unit-test-pause-btn"
                label={
                  <>
                    <FontAwesomeIcon icon={faPauseCircle} />
                    <span style={styles.buttonLabel}>
                      {LOCALIZE.emibTest.testFooter.unpauseButton}
                    </span>
                  </>
                }
                action={this.openUnpausePopup}
                buttonTheme={THEME.PRIMARY}
                ariaLabel={LOCALIZE.emibTest.testFooter.unpauseButton}
                // TODO
                disabled={false}
              ></CustomButton>
            </div>
          </Navbar>
        </div>
        <PopupBox
          show={this.state.showUnpausePopup}
          title={LOCALIZE.emibTest.testFooter.unpausePopup.title}
          size={"lg"}
          handleClose={() => {}}
          description={
            <div>
              <p>{LOCALIZE.emibTest.testFooter.unpausePopup.description1}</p>
              <p>{LOCALIZE.emibTest.testFooter.unpausePopup.description2}</p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonIcon={faTimes}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonLabel={LOCALIZE.commons.cancel}
          leftButtonAction={this.closeUnpausePopup}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonIcon={faPauseCircle}
          rightButtonTitle={LOCALIZE.emibTest.testFooter.unpausePopup.unpauseButton}
          rightButtonLabel={LOCALIZE.emibTest.testFooter.unpausePopup.unpauseButton}
          rightButtonAction={this.handleUnpause}
        />
      </div>
    );
  }
}

export { PauseScreen as unconnectedPauseScreen };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    pauseTime: state.testStatus.pauseTime,
    pauseStartDate: state.testStatus.pauseStartDate,
    username: state.user.username,
    topTabsHeight: state.testSection.topTabsHeight,
    testNavBarHeight: state.testSection.testNavBarHeight,
    testFooterHeight: state.testSection.testFooterHeight,
    testSection: state.testSection.testSection,
    accommodations: state.accommodations,
    accommodationRequestId: state.assignedTest.accommodationRequestId,
    currentTime: state.testSection.currentTime
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      unpauseTest,
      updateTestStatus,
      deactivateTest,
      setTopTabsHeight,
      setTestFooterHeight,
      getCurrentTestStatus,
      resetNotepadState,
      resetAssignedTestState,
      resetTopTabsState,
      resetQuestionListState,
      invalidateTest,
      resetTestFactoryState,
      resetInboxState,
      triggerBreakBankAction,
      getServerTime,
      setCurrentTime
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(PauseScreen);
