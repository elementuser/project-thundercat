/* eslint-disable no-unused-expressions */
import React, { Component } from "react";
import PropTypes from "prop-types";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheckCircle } from "@fortawesome/free-solid-svg-icons";
import {
  updateDatePicked,
  updateDatePickedValidState,
  resetDatePickedStates
} from "../../modules/DatePickerRedux";
import DropdownSelect from "./DropdownSelect";
import { Col, Row } from "react-bootstrap";

export const ERROR_MESSAGE = {
  default: "default"
};

const columnSizes = {
  dayColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 4,
    xl: 4
  },
  monthColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 4,
    xl: 4
  },
  yearColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 4,
    xl: 4
  },
  firstColumn: {
    xs: 8,
    sm: 8,
    md: 8,
    lg: 12,
    xl: 12
  },
  secondColumn: {
    xs: 4,
    sm: 4,
    md: 4,
    lg: 12,
    xl: 12
  }
};

const styles = {
  labelContainer: {
    textAlign: "center"
  },
  dateLabel: {
    padding: "3px 0",
    margin: 0
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6
  },
  validIcon: {
    color: "#278400",
    display: "table-cell",
    paddingLeft: 12
  },
  checkMark: {
    marginRight: 6
  },
  rowDayMonthYear: {
    paddingBottom: "5px",
    paddingTop: "5px"
  },
  dropdownCol: {
    maxWidth: "268px"
  }
};

// ref: https://stackoverflow.com/questions/8647893/regular-expression-leap-years-and-more
export function validateDatePicked(inputProvided) {
  // replacing all dashes with forward slashes (to avoid IE date validation incompatibility)
  const newInput = inputProvided.split("-").join("/");
  // date is fully provided
  if (newInput !== "null/null/null") {
    const date = new Date(newInput);
    const input = newInput.split("/");

    return (
      date.getFullYear() === +input[0] &&
      date.getMonth() + 1 === +input[1] &&
      date.getDate() === +input[2]
    );
  }
  return true;
}

class DatePicker extends Component {
  constructor(props, context) {
    super(props, context);

    this.PropTypes = {
      // provide refs if you need focus functionality for error validation
      dateDayFieldRef: PropTypes.object,
      dateMonthFieldRef: PropTypes.object,
      dateYearFieldRef: PropTypes.object,
      // provide date label id for accessibility/aria-labels
      dateLabelId: PropTypes.string,
      // options: {value: <value>, label: <label>}
      // provide if you want to allow the deselect action of any dropdown
      includeDeselectOption: PropTypes.bool,
      // if you do not provide custom options, the options will be the default ones (Day: from 1 to 31, Month: from 1 to 12, Year: from 1900 to today's date)
      // note that "{ value: "", label: LOCALIZE.datePicker.none }" option needs to be added for each custom options if defined
      customDayOptions: PropTypes.array,
      customMonthOptions: PropTypes.array,
      customYearOptions: PropTypes.array,
      // helps for validation
      triggerValidation: PropTypes.bool,
      // allow to manually display the date picker as invalid date
      isValidCompleteDatePicked: PropTypes.bool,
      // allow to manually display the date picker as invalid since the date is earlier than now
      isValidFutureDatePicked: PropTypes.bool,
      // allow a custom invalid date message
      customInvalidDateErrorMessage: props.string,
      // allow displaying/hiding valid checkmark icon (default is true)
      displayValidIcon: PropTypes.bool,
      // provide initial field values if needed ({value: <dsired_initial_value>, label: <dsired_initial_value>})
      initialDateDayValue: PropTypes.object,
      initialDateMonthValue: PropTypes.object,
      initialDateYearValue: PropTypes.object,
      // provide using a trigger state if you want to reset all fields values (day, month and year) to empty
      triggerResetFieldValues: PropTypes.bool,
      // provided by redux
      updateDatePicked: PropTypes.func,
      updateDatePickedValidState: PropTypes.func,
      resetDatePickedStates: PropTypes.func,
      // allow to validate that the selected date is a valid date, but also a future date (will be invalid if the selected date is in the past)
      futureDateValidation: PropTypes.bool,
      // allow to disable all three dropdowns (static date with initial values)
      disabledDropdowns: PropTypes.bool,
      // allow to call a function on input change
      onInputChange: PropTypes.func,
      // allow to set the menu placement orientation
      menuPlacement: PropTypes.string,
      // allow current day/date to be valid
      allowCurrentDay: PropTypes.bool,
      // allow to set menu max height
      maxMenuHeight: PropTypes.string,
      // allow to have time values (HH:MM)
      allowTimeValue: PropTypes.bool,
      // provide refs if you need focus functionality for error validation
      timeHourFieldRef: PropTypes.object,
      timeMinFieldRef: PropTypes.object,
      // provide initial field values for the time ({value: <dsired_initial_value>, label: <dsired_initial_value>})
      initialTimeHourValue: PropTypes.object,
      initialTimeMinValue: PropTypes.object,
      // if you do not provide custom options, the options will be the default ones (Hour: from 0 to 23, Minute: from 0 to 59)
      // note that "{ value: "", label: LOCALIZE.datePicker.none }" option needs to be added for each custom options if defined
      customHourOptions: PropTypes.array,
      customMinOptions: PropTypes.array,
      // provide if you need a custom placeholder
      customPlaceholder: PropTypes.string
    };

    DatePicker.defaultProps = {
      displayValidIcon: false,
      futureDateValidation: false,
      disabledDropdowns: false,
      customInvalidDateErrorMessage: ERROR_MESSAGE.default,
      menuPlacement: "auto",
      allowCurrentDay: true,
      maxMenuHeight: "200px",
      allowTimeValue: false,
      includeDeselectOption: false,
      customPlaceholder: ""
    };
  }

  state = {
    // date fields
    // day
    dateDayOptions: [],
    dateDaySelectedValue: this.props.initialDateDayValue ? this.props.initialDateDayValue : "",
    // month
    dateMonthOptions: [],
    dateMonthSelectedValue: this.props.initialDateMonthValue
      ? this.props.initialDateMonthValue
      : "",
    // year
    dateYearOptions: [],
    dateYearSelectedValue: this.props.initialDateYearValue ? this.props.initialDateYearValue : "",
    // date validation
    isValidCompleteDatePicked: this.props.isValidCompleteDatePicked
      ? this.props.isValidCompleteDatePicked
      : true,
    isValidFutureDatePicked: true,
    allowCurrentDay: this.props.allowCurrentDay ? this.props.allowCurrentDay : false,
    // time fields
    // hour
    timeHourOptions: [],
    timeHourSelectedValue: this.props.initialTimeHourValue ? this.props.initialTimeHourValue : "",
    // minute
    timeMinOptions: [],
    timeMinSelectedValue: this.props.initialTimeMinValue ? this.props.initialTimeMinValue : "",
    // time validation
    validationTriggered: false
  };

  componentDidMount = () => {
    // reseting date picked valid state (redux state)
    this.props.resetDatePickedStates();
    // date fields - populating dropdowns
    this.populateDateDayOptions();
    this.populateDateMonthOptions();
    this.populateDateYearOptions();
    // update date picked redux state if initial values have been provided
    if (
      this.props.initialDateYearValue &&
      this.props.initialDateMonthValue &&
      this.props.initialDateDayValue
    ) {
      this.props.updateDatePicked(
        `${this.state.dateYearSelectedValue.value}-${this.state.dateMonthSelectedValue.value}-${this.state.dateDaySelectedValue.value}`
      );
    }

    // if we want the time fields
    if (this.props.allowTimeValue) {
      // time fields - populating dropdowns
      this.populateTimeHourOptions();
      this.populateTimeMinOptions();
      // update date picked redux state if initial values have been provided
      if (this.props.initialTimeHourValue && this.props.initialTimeMinValue) {
        this.props.updateDatePicked(
          `${this.state.dateYearSelectedValue.value}-${this.state.dateMonthSelectedValue.value}-${this.state.dateDaySelectedValue.value}`
        );
      }
    }
  };

  componentDidUpdate = (prevProps, prevState) => {
    // if isValidCompleteDatePicked props gets updated
    if (prevProps.isValidCompleteDatePicked !== this.props.isValidCompleteDatePicked) {
      this.setState({ isValidCompleteDatePicked: this.props.isValidCompleteDatePicked });
    }
    // if isValidCompleteDatePicked props gets updated
    if (prevProps.isValidFutureDatePicked !== this.props.isValidFutureDatePicked) {
      this.setState({ isValidFutureDatePicked: this.props.isValidFutureDatePicked });
    }
    // if allowCurrentDay props gets updated
    if (prevProps.allowCurrentDay !== this.props.allowCurrentDay) {
      this.setState({ allowCurrentDay: this.props.allowCurrentDay });
    }
    // if validation gets triggered
    if (prevProps.triggerValidation !== this.props.triggerValidation) {
      this.dateValidation();
    }
    // getting initial date day, month, year values if provided on props (but not disabled) change and updating completeDatePicked state
    // if day, month or year props or state changes
    if (
      (prevProps.initialDateDayValue !== this.props.initialDateDayValue ||
        prevProps.initialDateMonthValue !== this.props.initialDateMonthValue ||
        prevProps.initialDateYearValue !== this.props.initialDateYearValue ||
        prevState.dateDaySelectedValue !== this.state.dateDaySelectedValue ||
        prevState.dateMonthSelectedValue !== this.state.dateMonthSelectedValue ||
        prevState.dateYearSelectedValue !== this.state.dateYearSelectedValue) &&
      !this.props.disabledDropdowns
    ) {
      // updating completeDatePicked state with the new value(s)
      this.props.updateDatePicked(
        `${this.state.dateYearSelectedValue !== "" ? this.state.dateYearSelectedValue.value : ""}-${
          this.state.dateMonthSelectedValue !== ""
            ? this.getFormattedDateField(this.state.dateMonthSelectedValue.value)
            : ""
        }-${
          this.state.dateDaySelectedValue !== ""
            ? this.getFormattedDateField(this.state.dateDaySelectedValue.value)
            : ""
        }`
      );
    }
    // if triggerResetFieldValues gets updated
    if (prevProps.triggerResetFieldValues !== this.props.triggerResetFieldValues) {
      // resetting selected options
      this.setState({
        dateDaySelectedValue: "",
        dateMonthSelectedValue: "",
        dateYearSelectedValue: "",
        isValidCompleteDatePicked: true,
        isValidFutureDatePicked: true
      });
    }
  };

  dateValidation = () => {
    const validDateBool = validateDatePicked(this.props.completeDatePicked);
    let validFutureDateBool = true;
    if (validDateBool) {
      validFutureDateBool = this.validateFutureDatePicked(
        this.props.completeDatePicked,
        this.props.allowCurrentDay
      );
    }
    // validating date and updating state
    this.setState({
      isValidCompleteDatePicked:
        // if isValidCompleteDatePicked props is provided
        typeof this.props.isValidCompleteDatePicked !== "undefined"
          ? (validDateBool && this.state.isValidCompleteDatePicked) ||
            (validDateBool && this.props.isValidCompleteDatePicked)
          : validDateBool,
      isValidFutureDatePicked: validFutureDateBool,
      validationTriggered: true
    });
    // updating valid date picked redux state
    if (this.props.futureDateValidation) {
      this.props.updateDatePickedValidState(validDateBool && validFutureDateBool);
    } else {
      this.props.updateDatePickedValidState(validDateBool);
    }
    // updating complete date picked redux state
    this.props.updateDatePicked(this.props.completeDatePicked);
  };

  // getting formatted date fields (day or month fields only)
  // if label is 5 for example, it will return 05 and if label is 16, it will return 16 as expected
  getFormattedDateField = label => {
    // if label is only one digit (5 for exemple)
    if (label !== null && label.length < 2) {
      // put a '0' in front of the label
      return `0${label}`;
      // else simply return the label
    }

    return label;
  };

  // populate day field from 1 to 31
  populateDateDayOptions = () => {
    const dayOptionsArray = [];
    // if includeDeselectOption is set to true
    if (this.props.includeDeselectOption) {
      dayOptionsArray.push({ value: null, label: LOCALIZE.datePicker.deselectOption });
    }
    // loop from 1 to 31
    for (let i = 1; i <= 31; i++) {
      // push each value in dayOptionsArray
      if (i < 10) {
        dayOptionsArray.push({ value: i, label: `0${i}` });
      } else {
        dayOptionsArray.push({ value: i, label: `${i}` });
      }
    }
    // saving result in state
    this.setState({ dateDayOptions: dayOptionsArray });
  };

  // populate month field from 1 to 12
  populateDateMonthOptions = () => {
    const monthOptionsArray = [];
    // if includeDeselectOption is set to true
    if (this.props.includeDeselectOption) {
      monthOptionsArray.push({ value: null, label: LOCALIZE.datePicker.deselectOption });
    }
    // loop from 1 to 12
    for (let i = 1; i <= 12; i++) {
      // push each value in monthOptionsArray
      if (i < 10) {
        monthOptionsArray.push({ value: i, label: `0${i}` });
      } else {
        monthOptionsArray.push({ value: i, label: `${i}` });
      }
    }
    // saving result in state
    this.setState({ dateMonthOptions: monthOptionsArray });
  };

  // populate year field from 1900 to current year
  populateDateYearOptions = () => {
    const yearOptionsArray = [];
    // loop from 1900 to current year
    for (let i = 1900; i <= new Date().getFullYear(); i++) {
      // push each value in yearOptionsArray
      yearOptionsArray.push({ value: i, label: `${i}` });
    }
    // sorting (descending order)
    yearOptionsArray.reverse();
    // if includeDeselectOption is set to true
    if (this.props.includeDeselectOption) {
      yearOptionsArray.unshift({ value: null, label: LOCALIZE.datePicker.deselectOption });
    }
    // saving result in state
    this.setState({ dateYearOptions: yearOptionsArray });
  };

  // get selected day
  getSelectedDateDay = selectedOption => {
    this.setState(
      {
        dateDaySelectedValue: selectedOption
      },
      () => {
        this.props.onInputChange && this.props.onInputChange();
      }
    );
  };

  // get selected month
  getSelectedDateMonth = selectedOption => {
    this.setState(
      {
        dateMonthSelectedValue: selectedOption
      },
      () => {
        this.props.onInputChange && this.props.onInputChange();
      }
    );
  };

  // get selected year
  getSelectedDateYear = selectedOption => {
    this.setState(
      {
        dateYearSelectedValue: selectedOption
      },
      () => {
        this.props.onInputChange && this.props.onInputChange();
      }
    );
  };

  // TIME
  // populate hours field from 0 to 23
  populateTimeHourOptions = () => {
    const hourOptionsArray = [];
    // loop from 0 to 23
    for (let i = 0; i <= 23; i++) {
      // push each value in dayOptionsArray
      if (i < 10) {
        hourOptionsArray.push({ value: i, label: `0${i}` });
      } else {
        hourOptionsArray.push({ value: i, label: `${i}` });
      }
    }
    // saving result in state
    this.setState({ timeHourOptions: hourOptionsArray });
  };

  // populate minutes field from 0 to 59
  populateTimeMinOptions = () => {
    const minuteOptionsArray = [];
    // loop from 0 to 59
    for (let i = 0; i <= 59; i++) {
      // push each value in monthOptionsArray
      if (i < 10) {
        minuteOptionsArray.push({ value: i, label: `0${i}` });
      } else {
        minuteOptionsArray.push({ value: i, label: `${i}` });
      }
    }
    // saving result in state
    this.setState({ timeMinOptions: minuteOptionsArray });
  };

  // get selected hour
  getSelectedTimeHour = selectedOption => {
    this.setState(
      {
        timeHourSelectedValue: selectedOption
      },
      () => {
        this.props.onInputChange && this.props.onInputChange();
      }
    );
  };

  // get selected minute
  getSelectedTimeMin = selectedOption => {
    this.setState(
      {
        timeMinSelectedValue: selectedOption
      },
      () => {
        this.props.onInputChange && this.props.onInputChange();
      }
    );
  };

  validateFutureDatePicked(input, allowCurrentDay) {
    // replacing all dashes with forward slashes (to avoid IE date validation incompatibility)
    const newInput = input.split("-").join("/");
    let date = new Date(newInput);
    // if futureDateValidation props is defined/set to true
    if (this.props.futureDateValidation) {
      // if allowCurrentDay is set to true
      if (allowCurrentDay) {
        // add one day to current date (example: Tue Dec 1 2021 00:00:00 ==> Tue Dec 2 2021 00:00:00)
        date = new Date(date.setDate(date.getDate() + 1));
        // this is a future date (including today)
        if (date > new Date()) {
          return true;
          // if allowCurrentDay is set to false
        }
      }
      // this is a future date (from tomorrow to future date)
      if (date > new Date()) {
        return true;
        // valid date, but in the past, so invalid
      }
      return false;

      // futureDateValidation is not defined/set to false, so return true since it is valid
    }
    return true;
  }

  render() {
    const {
      dateDaySelectedValue,
      dateMonthSelectedValue,
      dateYearSelectedValue,
      timeHourSelectedValue,
      timeMinSelectedValue
    } = this.state;

    // converting current font size in Int
    const fontSizeInt = parseInt(this.props.accommodations.fontSize.substring(0, 2));
    // initializing dropdown width array (used to define the width of the date picker dropdowns depending on the font size selected)
    let dropdownWidthArray = [];
    // initializing label margin (used only when font size is really big)
    let monthLabelMargin = "0px";
    // converting date picker dropdowns width
    switch (true) {
      case fontSizeInt <= 10:
        dropdownWidthArray = 120;
        break;
      case fontSizeInt > 10 && fontSizeInt <= 14:
        dropdownWidthArray = 132;
        break;
      case fontSizeInt > 14 && fontSizeInt <= 18:
        dropdownWidthArray = 145;
        break;
      case fontSizeInt > 18 && fontSizeInt <= 22:
        dropdownWidthArray = 160;
        break;
      case fontSizeInt > 22 && fontSizeInt <= 30:
        dropdownWidthArray = 180;
        break;
      case fontSizeInt > 30 && fontSizeInt <= 36:
        dropdownWidthArray = 240;
        break;
      case fontSizeInt > 36:
        dropdownWidthArray = 370;
        monthLabelMargin = "0 -30px";
        break;
      default:
        dropdownWidthArray = 145;
    }

    return (
      <div role="presentation" className="notranslate">
        <Row>
          <Col
            xl={columnSizes.dayColumn.xl}
            lg={columnSizes.dayColumn.lg}
            md={columnSizes.dayColumn.md}
            sm={columnSizes.dayColumn.sm}
            xs={columnSizes.dayColumn.xs}
          >
            <Row className="justify-content-center" style={styles.rowDayMonthYear}>
              <Col
                xl={columnSizes.firstColumn.xl}
                lg={columnSizes.firstColumn.lg}
                md={columnSizes.firstColumn.md}
                sm={columnSizes.firstColumn.sm}
                xs={columnSizes.firstColumn.xs}
                className="order-xl-1 order-lg-1 order-md-2 order-sm-2 order-2"
                style={styles.dropdownCol}
              >
                <DropdownSelect
                  customRef={this.props.dateDayFieldRef}
                  idPrefix="date-picker-day-field"
                  isValid={
                    !!(this.state.isValidCompleteDatePicked && this.state.isValidFutureDatePicked)
                  }
                  ariaLabelledBy={`${this.props.dateLabelId} ${
                    !this.state.isValidCompleteDatePicked ? "date-error" : ""
                  } ${
                    this.state.isValidCompleteDatePicked && !this.state.isValidFutureDatePicked
                      ? "future-date-error"
                      : ""
                  } ${this.props.ariaLabelledBy || ""} day-field-selected current-day-value-intro`}
                  options={
                    this.props.customDayOptions
                      ? this.props.customDayOptions
                      : this.state.dateDayOptions
                  }
                  orderByLabels={false}
                  ariaRequired={true}
                  onChange={this.getSelectedDateDay}
                  defaultValue={dateDaySelectedValue}
                  isDisabled={this.props.disabledDropdowns}
                  hasPlaceholder={true}
                  customPlaceholderWording={
                    this.props.customPlaceholder !== "" && this.props.customPlaceholder
                  }
                  menuPlacement={this.props.menuPlacement}
                  maxMenuHeight={this.props.maxMenuHeight}
                ></DropdownSelect>
              </Col>
              <Col
                xl={columnSizes.secondColumn.xl}
                lg={columnSizes.secondColumn.lg}
                md={columnSizes.secondColumn.md}
                sm={columnSizes.secondColumn.sm}
                xs={columnSizes.secondColumn.xs}
                className="order-xl-2 order-lg-2 order-md-1 order-sm-1 order-1"
                style={styles.dropdownCol}
              >
                <div style={styles.labelContainer}>
                  <label id="day-field-unit-test" style={styles.dateLabel}>
                    {LOCALIZE.datePicker.dayField}
                  </label>
                  <label id="day-field-selected" style={styles.hiddenText}>
                    {LOCALIZE.datePicker.dayFieldSelected}
                  </label>
                  <label id="current-day-value-intro" style={styles.hiddenText}>
                    {LOCALIZE.datePicker.currentValue}
                  </label>
                </div>
              </Col>
            </Row>
          </Col>
          <Col
            xl={columnSizes.monthColumn.xl}
            lg={columnSizes.monthColumn.lg}
            md={columnSizes.monthColumn.md}
            sm={columnSizes.monthColumn.sm}
            xs={columnSizes.monthColumn.xs}
          >
            <Row className="justify-content-center" style={styles.rowDayMonthYear}>
              <Col
                xl={columnSizes.firstColumn.xl}
                lg={columnSizes.firstColumn.lg}
                md={columnSizes.firstColumn.md}
                sm={columnSizes.firstColumn.sm}
                xs={columnSizes.firstColumn.xs}
                className="order-xl-1 order-lg-1 order-md-2 order-sm-2 order-2"
                style={styles.dropdownCol}
              >
                <DropdownSelect
                  customRef={this.props.dateMonthFieldRef}
                  idPrefix="date-picker-month-field"
                  isValid={
                    !!(this.state.isValidCompleteDatePicked && this.state.isValidFutureDatePicked)
                  }
                  ariaLabelledBy={`${this.props.dateLabelId} month-field-selected current-month-value-intro`}
                  ariaRequired={true}
                  options={
                    this.props.customMonthOptions
                      ? this.props.customMonthOptions
                      : this.state.dateMonthOptions
                  }
                  orderByLabels={false}
                  onChange={this.getSelectedDateMonth}
                  defaultValue={dateMonthSelectedValue}
                  isDisabled={this.props.disabledDropdowns}
                  hasPlaceholder={true}
                  customPlaceholderWording={
                    this.props.customPlaceholder !== "" && this.props.customPlaceholder
                  }
                  menuPlacement={this.props.menuPlacement}
                  maxMenuHeight={this.props.maxMenuHeight}
                ></DropdownSelect>
              </Col>
              <Col
                xl={columnSizes.secondColumn.xl}
                lg={columnSizes.secondColumn.lg}
                md={columnSizes.secondColumn.md}
                sm={columnSizes.secondColumn.sm}
                xs={columnSizes.secondColumn.xs}
                className="order-xl-2 order-lg-2 order-md-1 order-sm-1 order-1"
                style={styles.dropdownCol}
              >
                <div style={styles.labelContainer}>
                  <label
                    id="month-field-unit-test"
                    style={{ ...styles.dateLabel, ...{ margin: monthLabelMargin } }}
                  >
                    {LOCALIZE.datePicker.monthField}
                  </label>
                  <label id="month-field-selected" style={styles.hiddenText}>
                    {LOCALIZE.datePicker.monthFieldSelected}
                  </label>
                  <label id="current-month-value-intro" style={styles.hiddenText}>
                    {LOCALIZE.datePicker.currentValue}
                  </label>
                </div>
              </Col>
            </Row>
          </Col>
          <Col
            xl={columnSizes.yearColumn.xl}
            lg={columnSizes.yearColumn.lg}
            md={columnSizes.yearColumn.md}
            sm={columnSizes.yearColumn.sm}
            xs={columnSizes.yearColumn.xs}
          >
            <Row className="justify-content-center" style={styles.rowDayMonthYear}>
              <Col
                xl={columnSizes.firstColumn.xl}
                lg={columnSizes.firstColumn.lg}
                md={columnSizes.firstColumn.md}
                sm={columnSizes.firstColumn.sm}
                xs={columnSizes.firstColumn.xs}
                className="order-xl-1 order-lg-1 order-md-2 order-sm-2 order-2"
                style={styles.dropdownCol}
              >
                <DropdownSelect
                  customRef={this.props.dateYearFieldRef}
                  idPrefix="date-picker-year-field"
                  isValid={
                    !!(this.state.isValidCompleteDatePicked && this.state.isValidFutureDatePicked)
                  }
                  ariaLabelledBy={`${this.props.dateLabelId} year-field-selected current-year-value-intro`}
                  ariaRequired={true}
                  options={
                    this.props.customYearOptions
                      ? this.props.customYearOptions
                      : this.state.dateYearOptions
                  }
                  orderByLabels={false}
                  onChange={this.getSelectedDateYear}
                  defaultValue={dateYearSelectedValue}
                  isDisabled={this.props.disabledDropdowns}
                  hasPlaceholder={true}
                  customPlaceholderWording={
                    this.props.customPlaceholder !== "" && this.props.customPlaceholder
                  }
                  menuPlacement={this.props.menuPlacement}
                  maxMenuHeight={this.props.maxMenuHeight}
                ></DropdownSelect>
              </Col>
              <Col
                xl={columnSizes.secondColumn.xl}
                lg={columnSizes.secondColumn.lg}
                md={columnSizes.secondColumn.md}
                sm={columnSizes.secondColumn.sm}
                xs={columnSizes.secondColumn.xs}
                className="order-xl-2 order-lg-2 order-md-1 order-sm-1 order-1"
                style={styles.dropdownCol}
              >
                <div style={styles.labelContainer}>
                  <label id="year-field-unit-test" style={styles.dateLabel}>
                    {LOCALIZE.datePicker.yearField}
                  </label>
                  <label id="year-field-selected" style={styles.hiddenText}>
                    {LOCALIZE.datePicker.yearFieldSelected}
                  </label>
                  <label id="current-year-value-intro" style={styles.hiddenText}>
                    {LOCALIZE.datePicker.currentValue}
                  </label>
                </div>
              </Col>
            </Row>
          </Col>
          {this.props.displayValidIcon &&
            this.state.isValidCompleteDatePicked &&
            this.state.isValidFutureDatePicked &&
            this.state.validationTriggered && (
              <div style={styles.validIcon}>
                <FontAwesomeIcon style={styles.checkMark} icon={faCheckCircle} />
                {LOCALIZE.commons.valid}
              </div>
            )}
        </Row>
        {this.props.allowTimeValue && (
          <Row>
            <Col
              xl={columnSizes.dayColumn.xl}
              lg={columnSizes.dayColumn.lg}
              md={columnSizes.dayColumn.md}
              sm={columnSizes.dayColumn.sm}
              xs={columnSizes.dayColumn.xs}
            >
              <Row className="justify-content-center" style={styles.rowDayMonthYear}>
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  className="order-xl-1 order-lg-1 order-md-2 order-sm-2 order-2"
                  style={styles.dropdownCol}
                >
                  <DropdownSelect
                    customRef={this.props.timeHourFieldRef}
                    idPrefix="date-picker-hour-field"
                    isValid={
                      !!(this.state.isValidCompleteDatePicked && this.state.isValidFutureDatePicked)
                    }
                    ariaLabelledBy={`${this.props.dateLabelId}  ${
                      !this.state.isValidCompleteDatePicked ? "date-error" : ""
                    } ${
                      this.state.isValidCompleteDatePicked && !this.state.isValidFutureDatePicked
                        ? "future-date-error"
                        : ""
                    } hour-field-selected current-hour-value-intro ${
                      this.props.ariaLabelledBy || ""
                    }`}
                    options={
                      this.props.customHourOptions
                        ? this.props.customHourOptions
                        : this.state.timeHourOptions
                    }
                    orderByLabels={false}
                    ariaRequired={true}
                    onChange={this.getSelectedTimeHour}
                    defaultValue={timeHourSelectedValue}
                    isDisabled={this.props.disabledDropdowns}
                    hasPlaceholder={true}
                    customPlaceholderWording={
                      this.props.customPlaceholder !== "" && this.props.customPlaceholder
                    }
                    menuPlacement={this.props.menuPlacement}
                    maxMenuHeight={this.props.maxMenuHeight}
                  ></DropdownSelect>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  className="order-xl-2 order-lg-2 order-md-1 order-sm-1 order-1"
                  style={styles.dropdownCol}
                >
                  <div style={styles.labelContainer}>
                    <label id="hour-field-unit-test" style={styles.dateLabel}>
                      {LOCALIZE.datePicker.hourField}
                    </label>
                    <label id="hour-field-selected" style={styles.hiddenText}>
                      {LOCALIZE.datePicker.hourFieldSelected}
                    </label>
                    <label id="current-hour-value-intro" style={styles.hiddenText}>
                      {LOCALIZE.datePicker.currentValue}
                    </label>
                  </div>
                </Col>
              </Row>
            </Col>
            <Col
              xl={columnSizes.monthColumn.xl}
              lg={columnSizes.monthColumn.lg}
              md={columnSizes.monthColumn.md}
              sm={columnSizes.monthColumn.sm}
              xs={columnSizes.monthColumn.xs}
            >
              <Row className="justify-content-center" style={styles.rowDayMonthYear}>
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  className="order-xl-1 order-lg-1 order-md-2 order-sm-2 order-2"
                  style={styles.dropdownCol}
                >
                  <DropdownSelect
                    customRef={this.props.timeMinFieldRef}
                    idPrefix="date-picker-minute-field"
                    isValid={
                      !!(this.state.isValidCompleteDatePicked && this.state.isValidFutureDatePicked)
                    }
                    ariaLabelledBy={`${this.props.dateLabelId} minute-field-selected current-minute-value-intro`}
                    ariaRequired={true}
                    options={
                      this.props.customMinOptions
                        ? this.props.customMinOptions
                        : this.state.timeMinOptions
                    }
                    orderByLabels={false}
                    onChange={this.getSelectedTimeMin}
                    defaultValue={timeMinSelectedValue}
                    isDisabled={this.props.disabledDropdowns}
                    hasPlaceholder={true}
                    customPlaceholderWording={
                      this.props.customPlaceholder !== "" && this.props.customPlaceholder
                    }
                    menuPlacement={this.props.menuPlacement}
                    maxMenuHeight={this.props.maxMenuHeight}
                  ></DropdownSelect>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  className="order-xl-2 order-lg-2 order-md-1 order-sm-1 order-1"
                  style={styles.dropdownCol}
                >
                  <div style={styles.labelContainer}>
                    <label
                      id="minute-field-unit-test"
                      style={{ ...styles.dateLabel, ...{ margin: monthLabelMargin } }}
                    >
                      {LOCALIZE.datePicker.minuteField}
                    </label>
                    <label id="minute-field-selected" style={styles.hiddenText}>
                      {LOCALIZE.datePicker.minuteFieldSelected}
                    </label>
                    <label id="current-minute-value-intro" style={styles.hiddenText}>
                      {LOCALIZE.datePicker.currentValue}
                    </label>
                  </div>
                </Col>
              </Row>
            </Col>
            {this.props.displayValidIcon &&
              this.state.isValidCompleteDatePicked &&
              this.state.isValidFutureDatePicked &&
              this.state.validationTriggered && (
                <div style={styles.validIcon}>
                  <FontAwesomeIcon style={styles.checkMark} icon={faCheckCircle} />
                  {LOCALIZE.commons.valid}
                </div>
              )}
          </Row>
        )}
        {!this.state.isValidCompleteDatePicked &&
          this.props.customInvalidDateErrorMessage !== "" && (
            <label
              id="date-error"
              htmlFor="selected-day-field"
              style={styles.errorMessage}
              className="notranslate"
            >
              {this.props.customInvalidDateErrorMessage !== ERROR_MESSAGE.default
                ? this.props.customInvalidDateErrorMessage
                : LOCALIZE.datePicker.datePickedError}
            </label>
          )}
        {this.state.isValidCompleteDatePicked &&
          !this.state.isValidFutureDatePicked &&
          !this.props.allowCurrentDay && (
            <label
              id="future-date-error"
              htmlFor="selected-day-field"
              style={styles.errorMessage}
              className="notranslate"
            >
              {LOCALIZE.datePicker.futureDatePickedError}
            </label>
          )}
        {this.state.isValidCompleteDatePicked &&
          !this.state.isValidFutureDatePicked &&
          this.props.allowCurrentDay && (
            <label
              id="future-date-error"
              htmlFor="selected-day-field"
              style={styles.errorMessage}
              className="notranslate"
            >
              {LOCALIZE.datePicker.futureDatePickedIncludingTodayError}
            </label>
          )}
      </div>
    );
  }
}

export { DatePicker as unconnectedDatePicker };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    completeDatePicked: state.datePicker.completeDatePicked,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateDatePicked,
      updateDatePickedValidState,
      resetDatePickedStates
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(DatePicker);
