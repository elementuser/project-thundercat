import React, { Component } from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { PATH } from "../../App";
import CustomButton, { THEME } from "./CustomButton";

const styles = {
  navlink: {
    minWidth: 50
  },
  paddingNavButton: {
    padding: "0 5px 0 5px"
  }
};

class LoginButton extends Component {
  static propTypes = {
    // Props from Redux
    currentLanguage: PropTypes.string
  };

  handleClick = () => {
    this.props.history.push(PATH.login);
  };

  render() {
    return (
      <div>
        {!this.props.authenticated && (
          <div style={styles.paddingNavButton}>
            <CustomButton
              label={LOCALIZE.commons.login}
              action={this.handleClick}
              customStyle={styles.navlink}
              buttonTheme={THEME.PRIMARY}
            />
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    authenticated: state.login.authenticated,
    currentLanguage: state.localize.language
  };
};

export default connect(mapStateToProps, null)(withRouter(LoginButton));
