import React, { Component } from "react";
import LOCALIZE from "../../text_resources";
import { Col, Row } from "react-bootstrap";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import {
  setFontFamily,
  saveUserAccommodations,
  triggerRerender
} from "../../modules/AccommodationsRedux";
import "../../css/font-select.css";
import { fontFamilyDefinition } from "./Constants";
import DropdownSelect from "./DropdownSelect";

class FontFamily extends Component {
  state = { fontStyle: { label: "default", value: "default" } };

  static propTypes = {};

  onChange = event => {
    this.props.setFontFamily(event.value);
    if (this.props.authenticated) {
      this.props
        .saveUserAccommodations({
          ...this.props.accommodations,
          fontFamily: event.value
        })
        .then(() => {
          // trigger rerender so dynamic height in test is updated
          this.props.triggerRerender();
        });
    }
  };

  getValue = () => {
    return { label: this.props.fontFamily, value: this.props.fontFamily };
  };

  render() {
    const value = this.getValue();

    const options = fontFamilyDefinition();

    // defining styles here because it wouldn't allow modifcation of values when defined outside.
    const styles = {
      container: {
        padding: "12px 25px",
        width: "100%"
      }
    };
    if (value && value.label !== "default") {
      styles.container.fontFamily = value.value;
    }

    return (
      <div style={styles.container}>
        <Row role="presentation">
          <Col xl={6} lg={6} md={12} sm={12}>
            <label id="font-fam-label">{LOCALIZE.settings.fontStyle.label}</label>
          </Col>
          <Col xl={6} lg={6} md={12} sm={12}>
            <DropdownSelect
              idPrefix="font-family"
              onChange={this.onChange}
              defaultValue={value}
              options={options}
              ariaLabelledBy="font-fam-label"
              orderByLabels={false}
            />
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    fontFamily: state.accommodations.fontFamily,
    authenticated: state.login.authenticated,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ setFontFamily, saveUserAccommodations, triggerRerender }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(FontFamily);
