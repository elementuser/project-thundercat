import LOCALIZE from "../../text_resources";

export const URL_PREFIX = {
  // all pages that is using this item bank prefix will have some special styling (in App.js) related to the height and the overflow of the page
  itemBank: "/item-bank/"
};

const PATH = {
  login: "/oec-cat/login",
  dashboard: "/oec-cat/check-in",
  status: "/oec-cat/status",
  emibSampleTest: "/oec-cat/emib-sample",
  test: "/oec-cat/active",
  testBase: "/oec-cat/test",
  sampleTests: "/oec-cat/sample-tests",
  instructions: "/oec-cat/instructions",
  overview: "/oec-cat/overview",
  submit: "/oec-cat/submit",
  quit: "/oec-cat/quit",
  testAdministration: "/oec-cat/test-sessions",
  profile: "/oec-cat/profile",
  incidentReport: "/oec-cat/incident-report",
  myTests: "/oec-cat/my-tests",
  contactUs: "/oec-cat/contact-us",
  systemAdministration: "/oec-cat/system-administration",
  ppcAdministration: "/oec-cat/ppc-administration",
  scorerBase: "/oec-cat/scorer",
  scorerTest: "/oec-cat/test",
  uit: "/oec-cat/uit",
  testBuilder: "/oec-cat/test-builder",
  itemBankAccesses: "/oec-cat/item-bank-accesses",
  itemBankEditor: `/oec-cat${URL_PREFIX.itemBank}item-bank-editor`,
  itemEditor: `/oec-cat${URL_PREFIX.itemBank}item-editor`,
  bundleEditor: `/oec-cat${URL_PREFIX.itemBank}bundle-editor`,
  testBuilderTest: "/oec-cat/test-builder-test",
  resetPassword: "/oec-cat/reset-password",
  invalidateTest: "/oec-cat/invalidate-test",
  userLookUpDetails: "/oec-cat/selected-user-details",
  orderlessTestAdministratorDetails: "/oec-cat/selected-orderless-test-administrator-details"
};

// this function will put alternate colors (grey and white rows) in the respsective table
/* parameters:
    - id (from the map function)
    - height (desired row height)
*/
const alternateColorsStyle = (id, height) => {
  const styles = {
    rowContainerBasicStyle: {
      width: "100%",
      height: `${height}px`,
      padding: "8px 0 8px 12px"
    },
    rowContainerDark: {
      backgroundColor: "#F3F3F3"
    },
    rowContainerLight: {
      backgroundColor: "white"
    },
    rowContainerBorder: {
      borderTop: "1px solid #CECECE"
    }
  };

  if (id === 0) {
    return { ...styles.rowContainerBasicStyle, ...styles.rowContainerLight };
  }
  if (id % 2 === 0) {
    return {
      ...styles.rowContainerBasicStyle,
      ...styles.rowContainerBorder,
      ...styles.rowContainerLight
    };
  }
  return {
    ...styles.rowContainerBasicStyle,
    ...styles.rowContainerBorder,
    ...styles.rowContainerDark
  };
};

const fontSizeDefinition = () => {
  return [
    { label: LOCALIZE.accommodations.defaultFonts.fontSize, value: "16px" },
    { label: "8px", value: "8px" },
    { label: "10px", value: "10px" },
    { label: "11px", value: "11px" },
    { label: "12px", value: "12px" },
    { label: "13px", value: "13px" },
    { label: "14px", value: "14px" },
    { label: "15px", value: "15px" },
    { label: "17px", value: "17px" },
    { label: "18px", value: "18px" },
    { label: "19px", value: "19px" },
    { label: "20px", value: "20px" },
    { label: "22px", value: "22px" },
    { label: "24px", value: "24px" },
    { label: "25px", value: "25px" },
    { label: "26px", value: "26px" },
    { label: "28px", value: "28px" },
    { label: "30px", value: "30px" },
    { label: "36px", value: "36px" },
    { label: "40px", value: "40px" },
    { label: "48px", value: "48px" }
  ];
};

const fontFamilyDefinition = () => {
  return [
    { label: LOCALIZE.accommodations.defaultFonts.fontFamily, value: "Nunito Sans" },
    { label: "APHont", value: "APHont" },
    { label: "Arial", value: "Arial" },
    { label: "Arial Black", value: "Arial Black" },
    { label: "Arial Narrow", value: "Arial Narrow" },
    { label: "Calibri", value: "Calibri" },
    { label: "Times New Roman", value: "Times New Roman" },
    { label: "Verdana", value: "Verdana" },
    { label: "OpenDyslexic", value: "OpenDyslexic" },
    { label: "Tahoma", value: "Tahoma" }
  ];
};

export { PATH, alternateColorsStyle, fontSizeDefinition, fontFamilyDefinition };
