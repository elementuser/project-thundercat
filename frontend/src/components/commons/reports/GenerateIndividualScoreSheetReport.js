// TODO uncomment this for section scores and minimum scores when clients are ok with adding it.
// TODO currently this is out of scope for SLE ACC
// import { TestSectionScoringTypeObject } from "../../testFactory/Constants";
import getConvertedScore from "../../../helpers/scoreConversion";
import LOCALIZE from "../../../text_resources";
import { REPORT_REQUESTOR } from "./Constants";
import getFormattedStatus from "./utils";

function generateIndividualScoreSheetReport(providedReportDataArray, reportRequestor) {
  // initializing reportData
  const reportData = [];

  // pushing columns definition
  reportData.push([
    LOCALIZE.reports.individualScoreSheet.personID,
    LOCALIZE.reports.individualScoreSheet.emailAddress,
    LOCALIZE.reports.individualScoreSheet.name,
    LOCALIZE.reports.individualScoreSheet.pri,
    LOCALIZE.reports.individualScoreSheet.orderNumber,
    LOCALIZE.reports.individualScoreSheet.assessmentProcessOrReferenceNb,
    LOCALIZE.reports.individualScoreSheet.test,
    LOCALIZE.reports.individualScoreSheet.testDescriptionFR,
    LOCALIZE.reports.individualScoreSheet.testDescriptionEN,
    LOCALIZE.reports.individualScoreSheet.testStartDate,
    ...(reportRequestor !== REPORT_REQUESTOR.ta
      ? [LOCALIZE.reports.individualScoreSheet.score]
      : []),
    // ...(reportRequestor !== REPORT_REQUESTOR.ta ? ["Pourcentage (%)"] : []),
    LOCALIZE.reports.individualScoreSheet.level,
    LOCALIZE.reports.individualScoreSheet.testStatus
  ]);

  // looping in provided providedReportDataArray and formatting data
  for (let i = 0; i < providedReportDataArray.length; i++) {
    reportData.push([
      providedReportDataArray[i].user_id,
      providedReportDataArray[i].candidate_email,
      `"${providedReportDataArray[i].candidate_last_name}, ${providedReportDataArray[i].candidate_first_name}"`,
      providedReportDataArray[i].candidate_pri_or_military_nbr,
      providedReportDataArray[i].test_order_number,
      providedReportDataArray[i].process_number !== null
        ? providedReportDataArray[i].process_number
        : providedReportDataArray[i].reference_number,
      providedReportDataArray[i].td_test_code,
      providedReportDataArray[i].td_fr_name,
      providedReportDataArray[i].td_en_name,
      // only getting short date without timestamp
      providedReportDataArray[i].submit_date === null
        ? null
        : providedReportDataArray[i].submit_date.substring(0, 10),
      ...(reportRequestor !== REPORT_REQUESTOR.ta
        ? [
            providedReportDataArray[i].test_score !== null
              ? providedReportDataArray[i].test_score
              : ""
          ]
        : []),
      // ...(reportRequestor !== REPORT_REQUESTOR.ta
      //   ? TODO (fnormand): add percentage logic here
      //   : []),
      getConvertedScore(
        // LOCALIZE.getLanguage() returns either "en" or "fr"
        providedReportDataArray[i][`level_${LOCALIZE.getLanguage()}`],
        providedReportDataArray[i].test_status
      ),
      getFormattedStatus(providedReportDataArray[i].test_status)
    ]);

    // add test section scores.
    // TODO uncomment this for section scores and minimum scores when clients are ok with adding it.
    // TODO currently this is out of scope for SLE ACC
    // const numberOfScorableSections = 0;
    // for (let index = 0; index < providedReportDataArray[i].assigned_test_sections.length; index++) {
    //   const assignedTestSection = providedReportDataArray[i].assigned_test_sections[index];
    //   // scorable test section to add to report
    //   if (
    //     assignedTestSection.test_section.scoring_type !==
    //     TestSectionScoringTypeObject.NOT_SCORABLE.value
    //   ) {
    //     if (numberOfScorableSections <= index) {
    //       // if this is a new column add headers
    //       reportDataColumns.push(
    //         `Résultat de la section ${assignedTestSection.test_section.fr_title} / Section ${assignedTestSection.test_section.en_title} Score`
    //       );
    //       reportDataColumns.push(
    //         `Résultat minimum de la section ${assignedTestSection.test_section.fr_title} / Section ${assignedTestSection.test_section.en_title} Minimum Score`
    //       );
    //     }
    //     data.push(assignedTestSection.score);
    //     data.push(assignedTestSection.minimum_score);
    //   }
    // }
  }

  return reportData;
}

export default generateIndividualScoreSheetReport;
