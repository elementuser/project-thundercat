/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Component } from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import LOCALIZE from "../../../text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  getTestsBasedOnTestOrderNumber,
  getCandidatesBasedOnSelectedTest,
  setReportSelectedParameters,
  setGenerateReportDisabledState,
  getCandidateTests
} from "../../../modules/ReportsRedux";
import DropdownSelect from "../DropdownSelect";
import { Row, Col } from "react-bootstrap";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 4,
    xl: 4
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 8,
    xl: 8
  }
};
const styles = {
  optionSelectionContainer: {
    padding: "12px 24px",
    margin: "0 auto",
    width: "80%"
  },
  labelContainer: {
    verticalAlign: "middle",
    paddingRight: 12
  },
  singleDropdownContainer: {
    verticalAlign: "middle",
    width: "100%"
  },
  loading: {
    width: "100%",
    minHeight: 60,
    textAlign: "center",
    fontSize: "32px",
    padding: 6
  }
};

class CandidateActionsReport extends Component {
  static propTypes = {
    getTestOrderNumbers: PropTypes.func.isRequired,
    currentlyLoading: PropTypes.bool.isRequired
  };

  state = {
    isLoadingTestOrderNumber: true,
    testOrderNumberVisible: false,
    testOrderNumberOptions: [],
    testOrderNumberData: [],
    selectedTestOrderNumber: "",
    isLoadingTests: true,
    testVisible: false,
    testOptions: [],
    testData: [],
    selectedTest: "",
    candidateVisible: false,
    candidateOptions: [],
    candidateData: [],
    isLoadingCandidates: true,
    selectedCandidate: "",
    isLoadingMultiAssignedTests: true,
    multiAssignedTestsSelectionVisible: false,
    multiAssignedTestsSelectionOptions: [],
    selectedAssignedTest: ""
  };

  // setting all states to their initial values
  async getInitialState() {
    this.setState({
      isLoadingTestOrderNumber: true,
      testOrderNumberVisible: false,
      testOrderNumberOptions: [],
      testOrderNumberData: [],
      selectedTestOrderNumber: "",
      isLoadingTests: true,
      testVisible: false,
      testOptions: [],
      testData: [],
      selectedTest: "",
      isLoadingCandidates: true,
      candidateVisible: false,
      candidateOptions: [],
      candidateData: [],
      selectedCandidate: ""
    });
  }

  componentDidMount = () => {
    this.populateTestOrderNumberOptions();
  };

  // populating test order number options
  populateTestOrderNumberOptions = () => {
    this.setState(
      {
        testOrderNumberVisible: true,
        testVisible: false,
        candidateVisible: false,
        multiAssignedTestsSelectionVisible: false,
        isLoadingTestOrderNumber: true
      },
      () => {
        this.props
          .getTestOrderNumbers()
          .then(response => {
            this.setState({ testOrderNumberData: response });
          })
          .then(() => {
            const testOrderNumberOptions = [];
            for (let i = 0; i < this.state.testOrderNumberData.length; i++) {
              testOrderNumberOptions.push({
                // adding " - <ta_username>", so TAs that used the same reference number are not all selected in the dropdown
                value: `${
                  this.state.testOrderNumberData[i].test_order_number !== null
                    ? this.state.testOrderNumberData[i].test_order_number
                    : this.state.testOrderNumberData[i].reference_number
                } - ${this.state.testOrderNumberData[i].ta_username}`,
                label: `${
                  this.state.testOrderNumberData[i].test_order_number !== null
                    ? this.state.testOrderNumberData[i].test_order_number
                    : this.state.testOrderNumberData[i].reference_number
                } (${
                  this.state.testOrderNumberData[i].test_order_number !== null
                    ? this.state.testOrderNumberData[i].staffing_process_number
                    : this.state.testOrderNumberData[i].ta_email
                })`,
                orderless: this.state.testOrderNumberData[i].test_order_number === null,
                ta_username: this.state.testOrderNumberData[i].ta_username
              });
            }
            this.getInitialState().then(() => {
              // disabling generate report button
              this.props.setGenerateReportDisabledState(true);
              this.setState({
                testOrderNumberOptions: testOrderNumberOptions,
                testOrderNumberVisible: true,
                isLoadingTestOrderNumber: false
              });
            });
          });
      }
    );
  };

  // get selected test order number
  getSelectedTestOrderNumber = selectedOption => {
    this.setState(
      {
        testVisible: true,
        candidateVisible: false,
        multiAssignedTestsSelectionVisible: false,
        isLoadingTests: true
      },
      () => {
        this.props
          .getTestsBasedOnTestOrderNumber(
            // splitting with the " - " since we've added that in the value option to avoid multi selection in the dropdown
            selectedOption.value.split(" - ")[0],
            selectedOption.orderless,
            false,
            selectedOption.ta_username
          )
          .then(response => {
            this.setState({ testData: response });
          })
          .then(() => {
            const testOptions = [];
            for (let i = 0; i < this.state.testData.length; i++) {
              testOptions.push({
                value: this.state.testData[i].test_id,
                label: `${this.state.testData[i][`${this.props.currentLanguage}_test_name`]} (v${
                  this.state.testData[i].test_version
                })`,
                orderless: selectedOption.orderless
              });
            }
            // disabling generate report button
            this.props.setGenerateReportDisabledState(true);
            this.setState({
              selectedTestOrderNumber: selectedOption,
              selectedTest: "",
              selectedCandidate: "",
              testOptions: testOptions,
              testVisible: true,
              candidateVisible: false,
              selectedAssignedTest: "",
              multiAssignedTestsSelectionOptions: [],
              multiAssignedTestsSelectionVisible: false,
              isLoadingTests: false
            });
          });
      }
    );
  };

  // get selected test
  getSelectedTest = selectedOption => {
    this.setState(
      {
        candidateVisible: true,
        multiAssignedTestsSelectionVisible: false,
        isLoadingCandidates: true
      },
      () => {
        this.props
          .getCandidatesBasedOnSelectedTest(
            // splitting with the " - " since we've added that in the value option to avoid multi selection in the dropdown
            this.state.selectedTestOrderNumber.value.split(" - ")[0],
            selectedOption.orderless,
            selectedOption.value,
            false,
            this.state.selectedTestOrderNumber.ta_username
          )
          .then(response => {
            this.setState({ candidateData: response });
          })
          .then(() => {
            // populating candidate options
            const candidateOptions = [];
            for (let i = 0; i < this.state.candidateData.length; i++) {
              candidateOptions.push({
                value: this.state.candidateData[i].candidate_username,
                label: `${this.state.candidateData[i].candidate_last_name}, ${this.state.candidateData[i].candidate_first_name} (${this.state.candidateData[i].candidate_email})`,
                orderless: selectedOption.orderless
              });
            }
            // disabling generate report button
            this.props.setGenerateReportDisabledState(true);
            this.setState({
              selectedTest: selectedOption,
              selectedCandidate: "",
              candidateOptions: candidateOptions,
              // candidate dropdown visible only if the report type is 1 (individual Score Sheet)
              candidateVisible: true,
              selectedAssignedTest: "",
              multiAssignedTestsSelectionOptions: [],
              multiAssignedTestsSelectionVisible: false,
              isLoadingCandidates: false
            });
          });
      }
    );
  };

  // get selected candidate
  getSelectedCandidate = selectedOption => {
    this.setState(
      {
        selectedCandidate: selectedOption,
        multiAssignedTestsSelectionVisible: true,
        isLoadingMultiAssignedTests: true
      },
      () => {
        // getting tests based on provided test order number, test ID and username ID
        this.props
          .getCandidateTests(
            // splitting with the " - " since we've added that in the value option to avoid multi selection in the dropdown
            this.state.selectedTestOrderNumber.value.split(" - ")[0],
            this.state.selectedTest.value,
            this.state.selectedCandidate.value,
            selectedOption.orderless
          )
          .then(response => {
            // populating multi assigned tests selection options
            const multiAssignedTestsSelectionOptions = [];
            for (let i = 0; i < response.length; i++) {
              multiAssignedTestsSelectionOptions.push({
                value: response[i].id,
                label: `${
                  LOCALIZE.reports.candidateActionsReport.multiAssignedTestsSelectionOptions
                    .startDate
                } ${
                  response[i].start_date === null
                    ? LOCALIZE.commons.none
                    : response[i].start_date.split(".")[0]
                } | ${
                  LOCALIZE.reports.candidateActionsReport.multiAssignedTestsSelectionOptions
                    .submitDate
                } ${
                  response[i].submit_date === null
                    ? LOCALIZE.commons.none
                    : response[i].submit_date.split(".")[0]
                } | ${
                  LOCALIZE.reports.candidateActionsReport.multiAssignedTestsSelectionOptions
                    .modifyDate
                } ${
                  response[i].modify_date === null
                    ? LOCALIZE.commons.none
                    : response[i].modify_date.split(".")[0]
                }`,
                orderless: selectedOption.orderless
              });
            }
            // if we're getting more than 1 test
            if (response.length > 1) {
              // disabling generate report button
              this.props.setGenerateReportDisabledState(true);
              this.setState({
                selectedAssignedTest: "",
                multiAssignedTestsSelectionOptions: multiAssignedTestsSelectionOptions,
                multiAssignedTestsSelectionVisible: true,
                isLoadingMultiAssignedTests: false
              });
              // getting only 1 test
            } else {
              // updating all needed redux states
              this.props.setReportSelectedParameters(
                selectedOption.orderless,
                this.state.selectedTestOrderNumber,
                this.state.selectedTest,
                this.state.selectedCandidate,
                null,
                null,
                null,
                null,
                null,
                multiAssignedTestsSelectionOptions[0]
              );
              // enabling generate report button
              this.props.setGenerateReportDisabledState(false);
              this.setState({
                selectedAssignedTest: "",
                multiAssignedTestsSelectionOptions: [],
                multiAssignedTestsSelectionVisible: false,
                isLoadingMultiAssignedTests: false
              });
            }
          });
      }
    );
  };

  // get selected assigned test
  getSelectedAssignedTest = selectedOption => {
    this.setState(
      {
        selectedAssignedTest: selectedOption
      },
      () => {
        // updating all needed redux states
        this.props.setReportSelectedParameters(
          selectedOption.orderless,
          this.state.selectedTestOrderNumber,
          this.state.selectedTest,
          this.state.selectedCandidate,
          null,
          null,
          null,
          null,
          null,
          this.state.selectedAssignedTest
        );
        // enabling generate report button
        this.props.setGenerateReportDisabledState(false);
      }
    );
  };

  render() {
    const {
      isLoadingTestOrderNumber,
      testOrderNumberOptions,
      testOrderNumberVisible,
      selectedTestOrderNumber,
      testOptions,
      isLoadingTests,
      testVisible,
      selectedTest,
      isLoadingCandidates,
      candidateVisible,
      candidateOptions,
      selectedCandidate,
      isLoadingMultiAssignedTests,
      multiAssignedTestsSelectionVisible,
      multiAssignedTestsSelectionOptions,
      selectedAssignedTest
    } = this.state;
    return (
      <div>
        {testOrderNumberVisible && (
          <Row
            className="align-items-center justify-content-start"
            id="unit-test-test-order-number-dropdown"
            style={styles.optionSelectionContainer}
            role="presentation"
          >
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
              style={styles.labelContainer}
            >
              <label id="test-order-number-label">{LOCALIZE.reports.testOrderNumberLabel}</label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={styles.singleDropdownContainer}
            >
              {isLoadingTestOrderNumber ? (
                <label className="fa fa-spinner fa-spin" style={styles.loading}>
                  <FontAwesomeIcon icon={faSpinner} />
                </label>
              ) : (
                <DropdownSelect
                  idPrefix="individual-report-test-order-number-dropdown"
                  ariaLabelledBy="test-order-number-label"
                  placeholder=""
                  isDisabled={this.props.currentlyLoading}
                  options={testOrderNumberOptions}
                  onChange={this.getSelectedTestOrderNumber}
                  defaultValue={selectedTestOrderNumber}
                  hasPlaceholder={true}
                ></DropdownSelect>
              )}
            </Col>
          </Row>
        )}
        {testVisible && (
          <Row
            className="align-items-center justify-content-start"
            id="unit-test-test-dropdown"
            style={styles.optionSelectionContainer}
            role="presentation"
          >
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
              style={styles.labelContainer}
            >
              <label id="test-label">{LOCALIZE.reports.testLabel}</label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={styles.singleDropdownContainer}
            >
              {isLoadingTests ? (
                <label className="fa fa-spinner fa-spin" style={styles.loading}>
                  <FontAwesomeIcon icon={faSpinner} />
                </label>
              ) : (
                <DropdownSelect
                  idPrefix="individual-report-test-dropdown"
                  ariaLabelledBy="test-label"
                  placeholder=""
                  isDisabled={this.props.currentlyLoading}
                  options={testOptions}
                  onChange={this.getSelectedTest}
                  defaultValue={selectedTest}
                  hasPlaceholder={true}
                ></DropdownSelect>
              )}
            </Col>
          </Row>
        )}
        {candidateVisible && (
          <Row
            className="align-items-center justify-content-start"
            id="unit-test-candidate-dropdown"
            style={styles.optionSelectionContainer}
            role="presentation"
          >
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
              style={styles.labelContainer}
            >
              <label id="candidate-label">{LOCALIZE.reports.candidateLabel}</label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={styles.singleDropdownContainer}
            >
              {isLoadingCandidates ? (
                <label className="fa fa-spinner fa-spin" style={styles.loading}>
                  <FontAwesomeIcon icon={faSpinner} />
                </label>
              ) : (
                <DropdownSelect
                  idPrefix="individual-report-candidate-dropdown"
                  ariaLabelledBy="candidate-label"
                  placeholder=""
                  isDisabled={this.props.currentlyLoading}
                  options={candidateOptions}
                  onChange={this.getSelectedCandidate}
                  defaultValue={selectedCandidate}
                  hasPlaceholder={true}
                ></DropdownSelect>
              )}
            </Col>
          </Row>
        )}
        {multiAssignedTestsSelectionVisible && (
          <Row
            className="align-items-center justify-content-start"
            id="unit-test-multi-assigned-tests-dropdown"
            style={styles.optionSelectionContainer}
            role="presentation"
          >
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
              style={styles.labelContainer}
            >
              <label id="multi-assigned-tests-label">
                {LOCALIZE.reports.candidateActionsReport.multiAssignedTestsSelectionOptions.label}
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={styles.singleDropdownContainer}
            >
              {isLoadingMultiAssignedTests ? (
                <label className="fa fa-spinner fa-spin" style={styles.loading}>
                  <FontAwesomeIcon icon={faSpinner} />
                </label>
              ) : (
                <DropdownSelect
                  idPrefix="candidate-actions-report-multi-assigned-tests-dropdown"
                  ariaLabelledBy="multi-assigned-tests-label"
                  placeholder=""
                  isDisabled={this.props.currentlyLoading}
                  options={multiAssignedTestsSelectionOptions}
                  onChange={this.getSelectedAssignedTest}
                  defaultValue={selectedAssignedTest}
                  hasPlaceholder={true}
                ></DropdownSelect>
              )}
            </Col>
          </Row>
        )}
      </div>
    );
  }
}

export { CandidateActionsReport as unconnectedCandidateActionsReport };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    username: state.user.username
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getTestsBasedOnTestOrderNumber,
      getCandidatesBasedOnSelectedTest,
      setReportSelectedParameters,
      setGenerateReportDisabledState,
      getCandidateTests
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(CandidateActionsReport));
