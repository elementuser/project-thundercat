/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Component } from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import LOCALIZE from "../../../text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  setReportSelectedParameters,
  setGenerateReportDisabledState,
  getTestDefinitionData
} from "../../../modules/ReportsRedux";
import DatePicker, { validateDatePicked } from "../DatePicker";
import { populateCustomYearsFutureDateOptions } from "../../../helpers/populateCustomDatePickerOptions";
import { updateDatePicked } from "../../../modules/DatePickerRedux";
import DropdownSelect from "../DropdownSelect";
import { Row, Col } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 4,
    xl: 4
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 8,
    xl: 8
  }
};
const styles = {
  optionSelectionContainer: {
    padding: "12px 24px",
    margin: "0 auto",
    width: "80%"
  },
  labelContainer: {
    verticalAlign: "middle",
    paddingRight: 12
  },
  singleDropdownContainer: {
    verticalAlign: "middle",
    width: "100%"
  },
  dateError: {
    paddingTop: 0,
    color: "#923534",
    fontWeight: "bold"
  },
  dateLabelsOnError: {
    color: "#923534",
    fontWeight: "bold"
  },
  loading: {
    width: "100%",
    minHeight: 60,
    textAlign: "center",
    fontSize: "32px",
    padding: 6
  }
};

class TestTakerReport extends Component {
  constructor(props) {
    super(props);
    // From date params
    this.dateFromDayFieldRef = React.createRef();
    this.dateFromMonthFieldRef = React.createRef();
    this.dateFromYearFieldRef = React.createRef();
    // To date params
    this.dateToDayFieldRef = React.createRef();
    this.dateToMonthFieldRef = React.createRef();
    this.dateToYearFieldRef = React.createRef();

    this.PropTypes = {
      currentlyLoading: PropTypes.bool.isRequired
    };
  }

  state = {
    isLoadingParentCode: true,
    parentCodeVisible: false,
    parentCodeOptions: [],
    selectedParentCode: "",
    isLoadingTestCode: true,
    testCodeVisible: false,
    testCodeOptions: [],
    selectedTestCode: "",
    dateFromVisible: false,
    dateToVisible: false,
    selectedDateFrom: null,
    selectedDateTo: null,
    triggerResetDateFromValues: false,
    triggerResetDateToValues: false,
    isValidFromDatePicked: true,
    isValidToDatePicked: true,
    isValidComboFromToDatePicked: true
  };

  // setting all states to their initial values
  async getInitialState() {
    this.setState({
      isLoadingParentCode: true,
      parentCodeVisible: false,
      parentCodeOptions: [],
      selectedParentCode: "",
      isLoadingTestCode: true,
      testCodeVisible: false,
      testCodeOptions: [],
      selectedTestCode: "",
      dateFromVisible: false,
      dateToVisible: false,
      isValidFromDatePicked: true,
      isValidToDatePicked: true,
      selectedDateFrom: null,
      selectedDateTo: null,
      triggerResetDateFromValues: false,
      triggerResetDateToValues: false
    });
  }

  componentDidMount = () => {
    this.populateParentCodesOptions();
  };

  // populate parent code options
  populateParentCodesOptions = () => {
    this.setState(
      {
        parentCodeVisible: true,
        testCodeVisible: false,
        dateFromVisible: false,
        dateToVisible: false,
        isLoadingParentCode: true
      },
      () => {
        this.props.getTestDefinitionData().then(response => {
          const parentCodeOptions = [];
          for (let i = 0; i < response.length; i++) {
            parentCodeOptions.push({
              value: response[i].parent_code,
              label: response[i].parent_code
            });
          }
          this.getInitialState().then(() => {
            // disabling generate report button
            this.props.setGenerateReportDisabledState(true);
            this.setState({
              parentCodeVisible: true,
              parentCodeOptions: parentCodeOptions,
              isLoadingParentCode: false
            });
          });
        });
      }
    );
  };

  // get selected parent code
  getSelectedParentCode = selectedOption => {
    this.setState(
      {
        selectedParentCode: selectedOption,
        testCodeVisible: true,
        dateFromVisible: false,
        dateToVisible: false,
        isLoadingTestCode: true
      },
      () => {
        this.props.getTestDefinitionData(selectedOption.value).then(response => {
          const testCodeOptions = [];
          for (let i = 0; i < response.length; i++) {
            testCodeOptions.push({
              value: response[i].test_code,
              label: response[i].test_code
            });
          }
          // disabling generate report button
          this.props.setGenerateReportDisabledState(true);
          this.setState({
            testCodeVisible: true,
            testCodeOptions: testCodeOptions,
            selectedTestCode: "",
            dateFromVisible: false,
            dateToVisible: false,
            isValidFromDatePicked: true,
            isValidToDatePicked: true,
            selectedDateFrom: null,
            selectedDateTo: null,
            isLoadingTestCode: false
          });
        });
      }
    );
  };

  // get selected test code
  getSelectedTestCode = selectedOption => {
    this.setState(
      {
        selectedTestCode: selectedOption
      },
      () => {
        // disabling generate report button
        this.props.setGenerateReportDisabledState(true);
        this.setState({
          dateFromVisible: true,
          dateToVisible: false,
          isValidFromDatePicked: true,
          isValidToDatePicked: true,
          selectedDateFrom: null,
          triggerResetDateFromValues: !this.state.triggerResetDateFromValues,
          selectedDateTo: null,
          triggerResetDateToValues: !this.state.triggerResetDateToValues
        });
      }
    );
  };

  // handling date updates
  handleDateUpdates = () => {
    let dateFrom = null;
    let dateTo = null;

    // verify if date from date has been fully provided
    if (
      this.dateFromDayFieldRef.current.props.value !== "" &&
      this.dateFromMonthFieldRef.current.props.value !== "" &&
      this.dateFromYearFieldRef.current.props.value !== ""
    ) {
      // create the date format
      dateFrom = `${this.dateFromYearFieldRef.current.props.value.value}-${this.dateFromMonthFieldRef.current.props.value.value}-${this.dateFromDayFieldRef.current.props.value.value}`;

      // verify if the date is valid
      const isFromValidDate = validateDatePicked(dateFrom);

      // if the date is valid, show the To Date field + set isValidFromDatePicked to true
      if (isFromValidDate) {
        this.setState(
          { dateToVisible: true, isValidFromDatePicked: true },
          // combo validation is here since we have to make sure isValidFromDatePicked's state has been changed before
          this.comboFromToDateValidation(dateFrom, dateTo)
        );
      }
      // in the case where the From Date has been changed and is not valid, only change isValidFromDatePicked to false (we want to keep seeing the To Date field)
      else {
        this.setState({ isValidFromDatePicked: false }, () => {
          this.props.setGenerateReportDisabledState(true);
        });
      }

      // verify if Date To field is visible, meaning that the Date From date has been fully provided and is valid at least once
      if (this.state.dateToVisible) {
        // date to date has been fully provided
        if (
          this.dateToDayFieldRef.current.props.value !== "" &&
          this.dateToMonthFieldRef.current.props.value !== "" &&
          this.dateToYearFieldRef.current.props.value !== ""
        ) {
          // create the date format
          dateTo = `${this.dateToYearFieldRef.current.props.value.value}-${this.dateToMonthFieldRef.current.props.value.value}-${this.dateToDayFieldRef.current.props.value.value}`;

          // verify if the date is valid
          const isToValidDate = validateDatePicked(dateTo);

          // if the date is valid, set isValidToDatePicked to true
          if (isToValidDate) {
            this.setState({ isValidToDatePicked: true }, () => {
              // combo validation is here since we have to make sure isValidToDatePicked's state has been changed before
              this.comboFromToDateValidation(dateFrom, dateTo);
            });
          }
          // if the date is not valid, set isValidToDatePicked to false
          else {
            this.setState({ isValidToDatePicked: false }, () => {
              this.props.setGenerateReportDisabledState(true);
            });
          }
        }
      }
    }
  };

  // From To Date Combo validation
  comboFromToDateValidation = (dateFrom, dateTo) => {
    // if both dates have been provided and validated
    if (this.state.isValidFromDatePicked && this.state.isValidToDatePicked && dateFrom && dateTo) {
      // valid dates
      if (this.dateValidation(dateFrom, dateTo)) {
        // create the date format (DD-MM-YYYY)
        const newDateFrom = `${this.dateFromDayFieldRef.current.props.value.value}-${this.dateFromMonthFieldRef.current.props.value.value}-${this.dateFromYearFieldRef.current.props.value.value}`;
        const newDateTo = `${this.dateToDayFieldRef.current.props.value.value}-${this.dateToMonthFieldRef.current.props.value.value}-${this.dateToYearFieldRef.current.props.value.value}`;

        this.setState(
          {
            selectedDateFrom: newDateFrom,
            selectedDateTo: newDateTo,
            isValidComboFromToDatePicked: true
          },
          () => {
            // updating all needed redux states
            this.props.setReportSelectedParameters(
              false,
              null,
              null,
              null,
              this.state.selectedParentCode,
              this.state.selectedTestCode,
              null,
              this.state.selectedDateFrom,
              this.state.selectedDateTo,
              null
            );
            // enabling generate report button
            this.props.setGenerateReportDisabledState(false);
          }
        );
      } else {
        this.setState(
          {
            isValidComboFromToDatePicked: false
          },
          () => {
            this.props.setGenerateReportDisabledState(true);
          }
        );
      }
    } else {
      this.setState(
        {
          isValidComboFromToDatePicked: true
        },
        () => {
          this.props.setGenerateReportDisabledState(true);
        }
      );
    }
  };

  // date validation
  dateValidation = (from, to) => {
    // converting string dates in dates (provided format: 'yyyy-mm-dd')
    const dateFrom = new Date(from.split("-")[0], from.split("-")[1] - 1, from.split("-")[2]);
    const dateTo = new Date(to.split("-")[0], to.split("-")[1] - 1, to.split("-")[2]);

    // date from is greater than date to (invalid)
    if (dateFrom > dateTo) {
      return false;
    }
    // everything is valid
    return true;
  };

  // this function is called for accessibility purposes
  // it will update the completeDatePicked redux state on focus, so the screen reader will read the current selected date
  onDateFromFocus = () => {
    this.props.updateDatePicked(
      `${
        this.dateFromYearFieldRef.current
          ? this.dateFromYearFieldRef.current.props.value.label
          : LOCALIZE.datePicker.none
      }-${
        this.dateFromMonthFieldRef.current
          ? this.dateFromMonthFieldRef.current.props.value.label
          : LOCALIZE.datePicker.none
      }-${
        this.dateFromDayFieldRef.current
          ? this.dateFromDayFieldRef.current.props.value.label
          : LOCALIZE.datePicker.none
      }`
    );
  };

  // this function is called for accessibility purposes
  // it will update the completeDatePicked redux state on focus, so the screen reader will read the current selected date
  onDateToFocus = () => {
    this.props.updateDatePicked(
      `${
        this.dateToYearFieldRef.current
          ? this.dateToYearFieldRef.current.props.value.label
          : LOCALIZE.datePicker.none
      }-${
        this.dateToMonthFieldRef.current
          ? this.dateToMonthFieldRef.current.props.value.label
          : LOCALIZE.datePicker.none
      }-${
        this.dateToDayFieldRef.current
          ? this.dateToDayFieldRef.current.props.value.label
          : LOCALIZE.datePicker.none
      }`
    );
  };

  render() {
    const {
      isLoadingParentCode,
      parentCodeVisible,
      parentCodeOptions,
      selectedParentCode,
      isLoadingTestCode,
      testCodeVisible,
      testCodeOptions,
      selectedTestCode,
      dateFromVisible,
      dateToVisible,
      triggerResetDateFromValues,
      triggerResetDateToValues,
      displayDateError
    } = this.state;
    return (
      <div>
        {parentCodeVisible && (
          <Row
            className="align-items-center justify-content-start"
            id="unit-test-parent-code-dropdown"
            style={styles.optionSelectionContainer}
            role="presentation"
          >
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
              style={styles.labelContainer}
            >
              <label id="parent-code-label">{LOCALIZE.reports.parentCodeLabel}</label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={styles.singleDropdownContainer}
            >
              {isLoadingParentCode ? (
                <label className="fa fa-spinner fa-spin" style={styles.loading}>
                  <FontAwesomeIcon icon={faSpinner} />
                </label>
              ) : (
                <DropdownSelect
                  idPrefix="test-taker-report-parent-code"
                  ariaLabelledBy="parent-code-label"
                  placeholder=""
                  isDisabled={this.props.currentlyLoading}
                  options={parentCodeOptions}
                  onChange={this.getSelectedParentCode}
                  defaultValue={selectedParentCode}
                  hasPlaceholder={true}
                ></DropdownSelect>
              )}
            </Col>
          </Row>
        )}
        {testCodeVisible && (
          <Row
            className="align-items-center justify-content-start"
            id="unit-test-test-code-dropdown"
            style={styles.optionSelectionContainer}
            role="presentation"
          >
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
              style={styles.labelContainer}
            >
              <label id="test-code-label">{LOCALIZE.reports.testCodeLabel}</label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={styles.singleDropdownContainer}
            >
              {isLoadingTestCode ? (
                <label className="fa fa-spinner fa-spin" style={styles.loading}>
                  <FontAwesomeIcon icon={faSpinner} />
                </label>
              ) : (
                <DropdownSelect
                  idPrefix="test-taker-report-test-code"
                  ariaLabelledBy="test-code-label"
                  placeholder=""
                  isDisabled={this.props.currentlyLoading}
                  options={testCodeOptions}
                  onChange={this.getSelectedTestCode}
                  defaultValue={selectedTestCode}
                  hasPlaceholder={true}
                ></DropdownSelect>
              )}
            </Col>
          </Row>
        )}
        {dateFromVisible && (
          <Row
            className="align-items-center justify-content-start"
            style={styles.optionSelectionContainer}
            role="presentation"
          >
            <Col
              xl={12}
              lg={12}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
              style={
                !this.state.isValidComboFromToDatePicked
                  ? { ...styles.labelContainer, ...styles.dateLabelsOnError }
                  : styles.labelContainer
              }
            >
              <label id="date-from-label">{LOCALIZE.reports.dateFromLabel}</label>
            </Col>
            <Col
              xl={12}
              lg={12}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={styles.singleDropdownContainer}
              onFocus={this.onDateFromFocus}
            >
              <DatePicker
                dateDayFieldRef={this.dateFromDayFieldRef}
                dateMonthFieldRef={this.dateFromMonthFieldRef}
                dateYearFieldRef={this.dateFromYearFieldRef}
                dateLabelId={"date-from-label"}
                onInputChange={this.handleDateUpdates}
                customValidation={true}
                customYearOptions={populateCustomYearsFutureDateOptions()}
                triggerResetFieldValues={triggerResetDateFromValues}
                disabledDropdowns={this.props.currentlyLoading}
                isValidCompleteDatePicked={
                  this.state.isValidFromDatePicked && this.state.isValidComboFromToDatePicked
                }
                customInvalidDateErrorMessage={
                  // is the date valid (ex: February 31th isn't)
                  this.state.isValidFromDatePicked
                    ? // is the combo valid
                      !this.state.isValidComboFromToDatePicked && ""
                    : // if the end date isn't a valid date
                      LOCALIZE.datePicker.datePickedError
                }
              />
            </Col>
          </Row>
        )}
        {dateToVisible && (
          <Row
            className="align-items-center justify-content-start"
            style={styles.optionSelectionContainer}
            role="presentation"
          >
            <Col
              xl={12}
              lg={12}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
              style={
                !this.state.isValidComboFromToDatePicked
                  ? { ...styles.labelContainer, ...styles.dateLabelsOnError }
                  : styles.labelContainer
              }
            >
              <label id="date-to-label">{LOCALIZE.reports.dateToLabel}</label>
            </Col>
            <Col
              xl={12}
              lg={12}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={styles.singleDropdownContainer}
              onFocus={this.onDateToFocus}
            >
              <DatePicker
                dateDayFieldRef={this.dateToDayFieldRef}
                dateMonthFieldRef={this.dateToMonthFieldRef}
                dateYearFieldRef={this.dateToYearFieldRef}
                dateLabelId={"date-to-label"}
                onInputChange={this.handleDateUpdates}
                customValidation={true}
                customYearOptions={populateCustomYearsFutureDateOptions()}
                triggerResetFieldValues={triggerResetDateToValues}
                disabledDropdowns={this.props.currentlyLoading}
                isValidCompleteDatePicked={
                  this.state.isValidToDatePicked && this.state.isValidComboFromToDatePicked
                }
                customInvalidDateErrorMessage={
                  // is the date valid (ex: February 31th isn't)
                  this.state.isValidToDatePicked
                    ? // is the combo valid
                      !this.state.isValidComboFromToDatePicked && LOCALIZE.reports.dateError
                    : // if the end date isn't a valid date
                      LOCALIZE.datePicker.datePickedError
                }
              />
            </Col>
          </Row>
        )}
      </div>
    );
  }
}

export { TestTakerReport as unconnectedTestTakerReport };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setReportSelectedParameters,
      setGenerateReportDisabledState,
      getTestDefinitionData,
      updateDatePicked
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(TestTakerReport));
