import getConvertedScore from "../../../helpers/scoreConversion";
import LOCALIZE from "../../../text_resources";
import getFormattedStatus from "./utils";

function generateResultsReport(providedReportDataArray) {
  // initializing reportData
  const reportData = [];

  // pushing columns definition
  reportData.push([
    LOCALIZE.reports.resultsReport.firstname,
    LOCALIZE.reports.resultsReport.lastname,
    LOCALIZE.reports.resultsReport.emailAddress,
    LOCALIZE.reports.resultsReport.uitInvitationEmail,
    LOCALIZE.reports.resultsReport.pri,
    LOCALIZE.reports.resultsReport.orderNumber,
    LOCALIZE.reports.resultsReport.assessmentProcessOrReferenceNb,
    LOCALIZE.reports.resultsReport.test,
    LOCALIZE.reports.resultsReport.testDescriptionFR,
    LOCALIZE.reports.resultsReport.testDescriptionEN,
    LOCALIZE.reports.resultsReport.testDate,
    LOCALIZE.reports.resultsReport.score,
    LOCALIZE.reports.resultsReport.level,
    LOCALIZE.reports.resultsReport.testStatus
  ]);

  // looping in provided providedReportDataArray and formatting data
  for (let i = 0; i < providedReportDataArray.length; i++) {
    reportData.push([
      providedReportDataArray[i].candidate_first_name,
      providedReportDataArray[i].candidate_last_name,
      providedReportDataArray[i].candidate_email,
      providedReportDataArray[i].uit_candidate_email !== null
        ? providedReportDataArray[i].uit_candidate_email
        : LOCALIZE.commons.na,
      providedReportDataArray[i].candidate_pri_or_military_nbr,
      providedReportDataArray[i].test_order_number,
      providedReportDataArray[i].process_number !== null
        ? providedReportDataArray[i].process_number
        : providedReportDataArray[i].reference_number,
      providedReportDataArray[i].td_test_code,
      providedReportDataArray[i].td_fr_name,
      providedReportDataArray[i].td_en_name,
      // only getting short date without timestamp
      providedReportDataArray[i].submit_date === null
        ? null
        : providedReportDataArray[i].submit_date.substring(0, 10),
      providedReportDataArray[i].test_score,
      // ...(reportRequestor !== REPORT_REQUESTOR.ta
      //   ? TODO (fnormand): add percentage logic here
      //   : []),
      getConvertedScore(
        // LOCALIZE.getLanguage() returns either "en" or "fr"
        providedReportDataArray[i][`level_${LOCALIZE.getLanguage()}`],
        providedReportDataArray[i].test_status
      ),
      getFormattedStatus(providedReportDataArray[i].test_status)
    ]);
  }

  return reportData;
}

export default generateResultsReport;
