import React, { Component } from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import LOCALIZE from "../../../text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import CustomButton, { THEME } from "../CustomButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPoll, faSpinner } from "@fortawesome/free-solid-svg-icons";
import { REPORT_TYPES, REPORT_REQUESTOR } from "./Constants";
import { CSVLink } from "react-csv";
import generateIndividualScoreSheetReport from "./GenerateIndividualScoreSheetReport";
import generateResultsReport from "./GenerateResultsReport";
import generateFinancialReport from "./GenerateFinancialReport";
import generateTestContentReport from "./GenerateTestContentReport";
import GenerateTestTakerReport from "./GenerateTestTakerReport";
import generateCandidateActionsReport from "./GenerateCandidateActionsReport";
import PopupBox, { BUTTON_TYPE } from "../PopupBox";
import {
  getTaAssignedTestOrderNumbers,
  getAllExistingTestOrderNumbers,
  getResultsReportData,
  getFinancialReportData,
  getTestContentReportData,
  getTestTakerReportData,
  getCandidateActionsReportData,
  setGenerateReportDisabledState,
  resetReportsStates
} from "../../../modules/ReportsRedux";
import IndividualScoreSheetReport from "./IndividualScoreSheetReport";
import ResultsReport from "./ResultsReport";
import FinancialReport from "./FinancialReport";
import TestContentReport from "./TestContentReport";
import TestTakerReport from "./TestTakerReport";
import CandidateActionsReport from "./CandidateActionsReport";
import DropdownSelect from "../DropdownSelect";
import { Row, Col } from "react-bootstrap";
import SystemMessage, { MESSAGE_TYPE } from "../SystemMessage";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 4,
    xl: 4
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 8,
    xl: 8
  }
};

const styles = {
  dropdownsMainContainer: {
    padding: 24,
    width: "100%"
  },
  optionSelectionContainer: {
    padding: "12px 24px",
    margin: "0 auto",
    width: "80%"
  },
  labelContainer: {
    verticalAlign: "middle",
    paddingRight: 12
  },
  singleDropdownContainer: {
    verticalAlign: "middle",
    width: "100%"
  },
  generateButtonContainer: {
    textAlign: "center",
    marginTop: 18
  },
  generateButtonIcon: {
    transform: "scale(1.5)",
    padding: "1.5px",
    marginRight: 12
  },
  generateButtonLabel: {
    minWidth: 175
  }
};

class ReportGenerator extends Component {
  static propTypes = {
    reportRequestor: PropTypes.string.isRequired,
    reportTypeOptions: PropTypes.array.isRequired
  };

  constructor(props) {
    super(props);
    this.createReportLinkButtonRef = React.createRef();
  }

  state = {
    selectedReportType: "",
    generateReportButtonDisabled: true,
    reportCreationLoading: false,
    formattedReportData: [],
    reportName: "",
    displayNoDataError: false
  };

  componentDidUpdate = PrevProps => {
    if (PrevProps.generateReportButtonDisabled !== this.props.generateReportButtonDisabled) {
      this.setState({ generateReportButtonDisabled: this.props.generateReportButtonDisabled });
    }
  };

  // get selected report type
  getSelectedReportType = selectedOption => {
    this.setState(
      {
        selectedReportType: selectedOption
      },
      () => {
        // disabling generate report button
        this.props.setGenerateReportDisabledState(true);
      }
    );
  };

  // generating report extract
  generateReport = async () => {
    // initializing variables
    let testsString = "";
    this.setState({ reportCreationLoading: true }, () => {
      switch (this.state.selectedReportType.value) {
        // Individual Score Sheet
        case REPORT_TYPES.individualScoreSheet.value:
          // third parameter is set to false, so duplicates are not removed
          this.props
            .getResultsReportData(
              this.props.selectedTestOrderNumber.value,
              this.props.orderlessRequest,
              this.props.selectedTest.value,
              false,
              this.props.selectedCandidate.value,
              // if requested by TA ==> provide own username
              // if requested by other user ==> provide null, so getting data regardless TAs
              this.props.reportRequestor === REPORT_REQUESTOR.ta ? this.props.username : null
            )
            .then(response => {
              // getting an array
              if (response.length >= 0) {
                this.handleReportDataFormatting(response);
                // should never happen
              } else {
                // display no data popup
                this.setState({ displayNoDataError: true });
              }
            });
          break;
        // Results Report
        case REPORT_TYPES.resultsReport.value:
          // looping in selected tests
          for (let i = 0; i < this.props.selectedTest.length; i++) {
            // first test, don't add comma in front of the test_id
            if (i === 0) {
              testsString += `${this.props.selectedTest[i].value}`;
              // all other tests, add comma in front of the test_id
            } else {
              testsString += `,${this.props.selectedTest[i].value}`;
            }
          }

          // third parameter is set to false, so duplicates are not removed
          this.props
            .getResultsReportData(
              this.props.selectedTestOrderNumber.value,
              this.props.orderlessRequest,
              testsString,
              false,
              null,
              // if requested by TA ==> provide own username
              // if requested by other user ==> provide null, so getting data regardless TAs
              this.props.reportRequestor === REPORT_REQUESTOR.ta ? this.props.username : null
            )
            .then(response => {
              // getting an array
              if (response.length >= 0) {
                this.handleReportDataFormatting(response);
                // should never happen
              } else {
                // display no data popup
                this.setState({ displayNoDataError: true });
              }
            });
          break;
        // Financial Report
        case REPORT_TYPES.financialReport.value:
          this.props
            .getFinancialReportData(this.props.selectedDateFrom, this.props.selectedDateTo)
            .then(response => {
              // getting an array
              if (response.length >= 0) {
                this.handleReportDataFormatting(response);
                // should never happen
              } else {
                // display no data popup
                this.setState({ displayNoDataError: true });
              }
            });
          break;
        // Test Content Report
        case REPORT_TYPES.testContentReport.value:
          this.props
            .getTestContentReportData(
              this.props.selectedParentCode.value,
              this.props.selectedTestCode.value,
              this.props.selectedTestVersion.value
            )
            .then(response => {
              // getting an array
              if (response.length >= 0) {
                this.handleReportDataFormatting(response);
                // should never happen
              } else {
                // display no data popup
                this.setState({ displayNoDataError: true });
              }
            });
          break;
        // Test Taker Report
        case REPORT_TYPES.testTakerReport.value:
          this.props
            .getTestTakerReportData(
              this.props.selectedParentCode.value,
              this.props.selectedTestCode.value,
              this.props.selectedDateFrom,
              this.props.selectedDateTo
            )
            .then(response => {
              // getting an array
              if (response.length >= 0) {
                this.handleReportDataFormatting(response);
                // should never happen
              } else {
                // display no data popup
                this.setState({ displayNoDataError: true });
              }
            });
          break;
        // Candidate Actions Report
        case REPORT_TYPES.candidateActionsReport.value:
          this.props
            .getCandidateActionsReportData(this.props.selectedAssignedTest.value)
            .then(response => {
              // getting an array
              if (response.length >= 0) {
                this.handleReportDataFormatting(response);
                // should never happen
              } else {
                // display no data popup
                this.setState({ displayNoDataError: true });
              }
            });
          break;
        default:
          break;
      }
    });
  };

  // formatting report data
  handleReportDataFormatting = reportData => {
    // if there is at least one entry based on the provided parameters
    if (reportData.length > 0) {
      // initializing variables
      let reportName = "";
      let formattedReportData = [];
      // formatting report data and creating report name
      switch (this.state.selectedReportType.value) {
        // inidividual score sheet report type
        case REPORT_TYPES.individualScoreSheet.value:
          // generating report data
          formattedReportData = generateIndividualScoreSheetReport(
            reportData,
            this.props.reportRequestor
          );
          // generating report name
          reportName = `${LOCALIZE.reports.reportTypes.individualScoreSheet} - ${reportData[0].candidate_last_name} ${reportData[0].candidate_first_name}.csv`;
          break;
        // results report type
        case REPORT_TYPES.resultsReport.value:
          // generating report data
          formattedReportData = generateResultsReport(reportData);
          // generating report name
          reportName = `${LOCALIZE.reports.reportTypes.resultsReport} - ${reportData[0].test_order_number} (${reportData[0].staffing_process_number}).csv`;
          break;
        // financial report type
        case REPORT_TYPES.financialReport.value:
          // generating report data
          formattedReportData = generateFinancialReport(reportData);
          // generating report name
          reportName = `${LOCALIZE.reports.reportTypes.financialReport} - ${
            LOCALIZE.reports.reportTypes.financialReportFromDateLabel
          } ${this.getFormattedDate(this.props.selectedDateFrom)} ${
            LOCALIZE.reports.reportTypes.financialReportToDateLabel
          } ${this.getFormattedDate(this.props.selectedDateTo)}.csv`;
          break;
        // test content report type
        case REPORT_TYPES.testContentReport.value:
          // generating report data
          formattedReportData = generateTestContentReport(reportData);
          // generating report name
          reportName = `${LOCALIZE.reports.reportTypes.testContentReport} - ${this.props.selectedParentCode.value} - ${this.props.selectedTestCode.value}(v${this.props.selectedTestVersion.value}).csv`;
          break;
        // test taker report type
        case REPORT_TYPES.testTakerReport.value:
          // generating report data
          formattedReportData = GenerateTestTakerReport(reportData);
          // generating report name
          reportName = `${LOCALIZE.reports.reportTypes.testTakerReport} - ${
            this.props.selectedParentCode.value
          } - ${this.props.selectedTestCode.value} (${this.getFormattedDate(
            this.props.selectedDateFrom
          )} - ${this.getFormattedDate(this.props.selectedDateTo)}).csv`;
          break;
        // candidate actions report type
        case REPORT_TYPES.candidateActionsReport.value:
          // generating report data
          formattedReportData = generateCandidateActionsReport(reportData);
          // generating report name
          reportName = `${LOCALIZE.reports.reportTypes.candidateActionsReport} - ${this.props.selectedTestOrderNumber.value} - ${this.props.selectedTest.label} - ${this.props.selectedCandidate.label}.csv`;
          break;
        // should never happen
        default:
          break;
      }

      this.setState({ formattedReportData: formattedReportData, reportName: reportName }, () => {
        this.setState({ reportCreationLoading: false }, () => {
          // once all data has been created, simulate click on hidden create report link to generate the downloadable csv file
          this.createReportLinkButtonRef.current.link.click();
          // clearing all fields
          this.resetAllFields();
        });
      });
      // there is no data to show based on the provided parameters
    } else {
      // display no data popup
      this.setState({ displayNoDataError: true });
    }
  };

  // getting formatted date
  getFormattedDate = date => {
    const day = parseInt(date.split("-")[0]) < 10 ? `0${date.split("-")[0]}` : date.split("-")[0];
    const month = parseInt(date.split("-")[1]) < 10 ? `0${date.split("-")[1]}` : date.split("-")[1];
    const year = date.split("-")[2];
    return `${year}-${month}-${day}`;
  };

  // resetting all fields
  resetAllFields = () => {
    this.setState({ reportCreationLoading: true }, () => {
      this.setState(
        {
          selectedReportType: "",
          generateReportButtonDisabled: true,
          reportCreationLoading: false,
          formattedReportData: [],
          reportName: "",
          displayNoDataError: false
        },
        () => {
          // resetting reports redux states
          this.props.resetReportsStates();
          this.setState({ reportCreationLoading: false });
        }
      );
    });
  };

  closeNoDataPopup = () => {
    this.setState({ displayNoDataError: false, reportCreationLoading: false });
  };

  render() {
    const {
      selectedReportType,
      generateReportButtonDisabled,
      reportCreationLoading,
      formattedReportData,
      reportName,
      displayNoDataError
    } = this.state;

    return (
      <div>
        <div>
          <div style={styles.dropdownsMainContainer}>
            <Row
              className="align-items-center justify-content-start"
              id="unit-test-report-type-dropdown"
              style={styles.optionSelectionContainer}
              role="presentation"
            >
              <Col
                xl={columnSizes.firstColumn.xl}
                lg={columnSizes.firstColumn.lg}
                md={columnSizes.firstColumn.md}
                sm={columnSizes.firstColumn.sm}
                xs={columnSizes.firstColumn.xs}
                style={styles.labelContainer}
              >
                <label id="report-type-label">{LOCALIZE.reports.reportTypeLabel}</label>
              </Col>
              <Col
                xl={columnSizes.secondColumn.xl}
                lg={columnSizes.secondColumn.lg}
                md={columnSizes.secondColumn.md}
                sm={columnSizes.secondColumn.sm}
                xs={columnSizes.secondColumn.xs}
                style={styles.singleDropdownContainer}
              >
                <DropdownSelect
                  idPrefix="report-type"
                  name="report-type-options"
                  ariaLabelledBy="report-type-label"
                  ariaRequired={true}
                  isDisabled={reportCreationLoading}
                  options={this.props.reportTypeOptions}
                  onChange={this.getSelectedReportType}
                  hasPlaceholder={true}
                  defaultValue={selectedReportType}
                  orderByLabels={false}
                ></DropdownSelect>
              </Col>
            </Row>
            {selectedReportType !== null &&
              selectedReportType.value === REPORT_TYPES.individualScoreSheet.value && (
                <IndividualScoreSheetReport
                  reportRequestor={this.props.reportRequestor}
                  getTestOrderNumbers={
                    this.props.reportRequestor === REPORT_REQUESTOR.ta
                      ? this.props.getTaAssignedTestOrderNumbers
                      : this.props.getAllExistingTestOrderNumbers
                  }
                  currentlyLoading={reportCreationLoading}
                />
              )}
            {selectedReportType !== null &&
              selectedReportType.value === REPORT_TYPES.resultsReport.value && (
                <ResultsReport
                  reportRequestor={this.props.reportRequestor}
                  getTestOrderNumbers={
                    this.props.reportRequestor === REPORT_REQUESTOR.ta
                      ? this.props.getTaAssignedTestOrderNumbers
                      : this.props.getAllExistingTestOrderNumbers
                  }
                  currentlyLoading={reportCreationLoading}
                />
              )}
            {selectedReportType !== null &&
              selectedReportType.value === REPORT_TYPES.financialReport.value && (
                <FinancialReport currentlyLoading={reportCreationLoading} />
              )}
            {selectedReportType !== null &&
              selectedReportType.value === REPORT_TYPES.testContentReport.value && (
                <TestContentReport currentlyLoading={reportCreationLoading} />
              )}
            {selectedReportType !== null &&
              selectedReportType.value === REPORT_TYPES.testTakerReport.value && (
                <TestTakerReport currentlyLoading={reportCreationLoading} />
              )}
            {selectedReportType !== null &&
              selectedReportType.value === REPORT_TYPES.candidateActionsReport.value && (
                <CandidateActionsReport
                  getTestOrderNumbers={this.props.getAllExistingTestOrderNumbers}
                  currentlyLoading={reportCreationLoading}
                />
              )}
          </div>
          <div style={styles.generateButtonContainer}>
            <CustomButton
              label={
                reportCreationLoading ? (
                  // eslint-disable-next-line jsx-a11y/label-has-associated-control
                  <label className="fa fa-spinner fa-spin">
                    <FontAwesomeIcon icon={faSpinner} />
                  </label>
                ) : (
                  <>
                    <span>
                      <FontAwesomeIcon icon={faPoll} style={styles.generateButtonIcon} />
                    </span>
                    <span>{LOCALIZE.reports.generateButton}</span>
                  </>
                )
              }
              action={this.generateReport}
              customStyle={styles.generateButtonLabel}
              buttonTheme={THEME.PRIMARY}
              disabled={generateReportButtonDisabled ? true : !!this.state.reportCreationLoading}
            />
          </div>
        </div>
        <CSVLink
          ref={this.createReportLinkButtonRef}
          asyncOnClick={true}
          data={formattedReportData}
          filename={reportName}
          enclosingCharacter=""
        ></CSVLink>
        <PopupBox
          show={displayNoDataError}
          title={LOCALIZE.reports.noDataPopup.title}
          handleClose={() => {}}
          size={"lg"}
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.info}
                title={LOCALIZE.commons.info}
                message={<p>{LOCALIZE.reports.noDataPopup.description}</p>}
              />
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.ok}
          rightButtonAction={() => this.closeNoDataPopup()}
        />
      </div>
    );
  }
}

export { ReportGenerator as unconnectedReportGenerator };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    username: state.user.username,
    orderlessRequest: state.reports.orderlessRequest,
    selectedTestOrderNumber: state.reports.selectedTestOrderNumber,
    selectedTest: state.reports.selectedTest,
    selectedCandidate: state.reports.selectedCandidate,
    selectedParentCode: state.reports.selectedParentCode,
    selectedTestCode: state.reports.selectedTestCode,
    selectedTestVersion: state.reports.selectedTestVersion,
    selectedDateFrom: state.reports.selectedDateFrom,
    selectedDateTo: state.reports.selectedDateTo,
    selectedAssignedTest: state.reports.selectedAssignedTest,
    generateReportButtonDisabled: state.reports.generateReportButtonDisabled
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getTaAssignedTestOrderNumbers,
      getAllExistingTestOrderNumbers,
      getResultsReportData,
      getFinancialReportData,
      getTestContentReportData,
      getTestTakerReportData,
      setGenerateReportDisabledState,
      resetReportsStates,
      getCandidateActionsReportData
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ReportGenerator));
