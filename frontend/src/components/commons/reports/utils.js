import TEST_STATUS from "../../ta/Constants";
import LOCALIZE from "../../../text_resources";

function getFormattedStatus(statusCode) {
  switch (statusCode) {
    case TEST_STATUS.ASSIGNED:
      return LOCALIZE.reports.testStatus.assigned;
    case TEST_STATUS.READY:
      return LOCALIZE.reports.testStatus.ready;
    case TEST_STATUS.PRE_TEST:
      return LOCALIZE.reports.testStatus.preTest;
    case TEST_STATUS.ACTIVE:
      return LOCALIZE.reports.testStatus.active;
    case TEST_STATUS.LOCKED:
      return LOCALIZE.reports.testStatus.locked;
    case TEST_STATUS.PAUSED:
      return LOCALIZE.reports.testStatus.paused;
    case TEST_STATUS.QUIT:
      return LOCALIZE.reports.testStatus.quit;
    case TEST_STATUS.SUBMITTED:
      return LOCALIZE.reports.testStatus.submitted;
    case TEST_STATUS.UNASSIGNED:
      return LOCALIZE.reports.testStatus.unassigned;
    default:
      return LOCALIZE.commons.na;
  }
}

// ref: https://css-tricks.com/snippets/javascript/strip-html-tags-in-javascript/
export function removeHtmlFromString(string) {
  const newString = string.replace(/(<([^>]+)>)/gi, "");
  return newString;
}

export function getLanguage(language_id) {
  // language is English
  if (language_id === 1) {
    return LOCALIZE.commons.english;
  }
  // language is French

  return LOCALIZE.commons.french;
}

export function removeSpecificLineChars(text) {
  return text.replaceAll("\n", "").replaceAll("\r", "").replaceAll("\t", "").replaceAll("\\", "");
}

export default getFormattedStatus;
