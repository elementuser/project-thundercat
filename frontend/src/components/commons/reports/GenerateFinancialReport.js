import LOCALIZE from "../../../text_resources";
import getFormattedStatus from "./utils";

function generateFinancialReport(providedReportDataArray) {
  // initializing reportData
  const reportData = [];
  const { selectedLanguage } = LOCALIZE.reports.testTakerReport;

  // pushing columns definition
  reportData.push([
    LOCALIZE.reports.financialReport.candidateLastName,
    LOCALIZE.reports.financialReport.candidateFirstName,
    LOCALIZE.reports.financialReport.candidatePri,
    LOCALIZE.reports.financialReport.taWorkEmail,
    LOCALIZE.reports.financialReport.taOrg,
    LOCALIZE.reports.financialReport.requestingDepartment,
    LOCALIZE.reports.financialReport.orderNumber,
    LOCALIZE.reports.financialReport.assessmentProcess,
    LOCALIZE.reports.financialReport.orgCode,
    LOCALIZE.reports.financialReport.fisOrgCode,
    LOCALIZE.reports.financialReport.fisRefCode,
    LOCALIZE.reports.financialReport.billingContactName,
    LOCALIZE.reports.financialReport.testStatus,
    LOCALIZE.reports.financialReport.isInvalid,
    LOCALIZE.reports.financialReport.testDate,
    LOCALIZE.reports.financialReport.testVersion,
    LOCALIZE.reports.financialReport.testDescriptionFR,
    LOCALIZE.reports.financialReport.testDescriptionEN
  ]);
  // looping in provided providedReportDataArray and formatting data
  // enclose ta_org and requesting_dep in double quotes to escape any commas
  for (let i = 0; i < providedReportDataArray.length; i++) {
    reportData.push([
      providedReportDataArray[i].candidate_last_name,
      providedReportDataArray[i].candidate_first_name,
      providedReportDataArray[i].candidate_pri,
      providedReportDataArray[i].ta_email,
      providedReportDataArray[i].ta_org_en !== null
        ? `${providedReportDataArray[i][`department_description_${selectedLanguage}`]}`
        : "",
      providedReportDataArray[i].requesting_dep_en !== null
        ? `${providedReportDataArray[i][`requesting_department_${selectedLanguage}`]}`
        : "",
      providedReportDataArray[i].order_no !== null
        ? providedReportDataArray[i].order_no
        : LOCALIZE.reports.orderlessRequest,
      providedReportDataArray[i].assessment_process !== null
        ? providedReportDataArray[i].assessment_process
        : providedReportDataArray[i].reference_number,
      providedReportDataArray[i].org_code,
      providedReportDataArray[i].fis_org_code,
      providedReportDataArray[i].fis_ref_code,
      providedReportDataArray[i].billing_contact_name,
      getFormattedStatus(providedReportDataArray[i].test_status),
      providedReportDataArray[i].is_invalid === true
        ? LOCALIZE.commons.true
        : LOCALIZE.commons.false,
      providedReportDataArray[i].submit_date,
      providedReportDataArray[i].test_code,
      providedReportDataArray[i].test_description_fr,
      providedReportDataArray[i].test_description_en
    ]);
  }

  return reportData;
}

export default generateFinancialReport;
