import React, { Component } from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import LOCALIZE from "../../../text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  setReportSelectedParameters,
  setGenerateReportDisabledState
} from "../../../modules/ReportsRedux";
import DatePicker, { validateDatePicked } from "../DatePicker";
import { populateCustomYearsFutureDateOptions } from "../../../helpers/populateCustomDatePickerOptions";
import { updateDatePicked } from "../../../modules/DatePickerRedux";
import { Row, Col } from "react-bootstrap";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 4,
    xl: 4
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 8,
    xl: 8
  }
};
const styles = {
  optionSelectionContainer: {
    padding: "12px 24px",
    margin: "0 auto",
    width: "80%"
  },
  labelContainer: {
    verticalAlign: "middle",
    paddingRight: 12
  },
  singleDropdownContainer: {
    verticalAlign: "middle",
    width: "100%"
  },
  dateError: {
    paddingTop: 0,
    color: "#923534",
    fontWeight: "bold"
  },
  dateLabelsOnError: {
    color: "#923534",
    fontWeight: "bold"
  }
};

class FinancialReport extends Component {
  constructor(props) {
    super(props);
    this.dateFromDayFieldRef = React.createRef();
    this.dateFromMonthFieldRef = React.createRef();
    this.dateFromYearFieldRef = React.createRef();
    this.dateToDayFieldRef = React.createRef();
    this.dateToMonthFieldRef = React.createRef();
    this.dateToYearFieldRef = React.createRef();

    this.PropTypes = {
      currentlyLoading: PropTypes.bool.isRequired
    };
  }

  state = {
    dateToVisible: false,
    displayDateError: false,
    displayInvalidDateFromError: false,
    displayInvalidDateToError: false,
    selectedDateFrom: null,
    selectedDateTo: null
  };

  // handling date updates
  handleDateUpdates = () => {
    let dateFrom = null;
    let dateTo = null;

    // date from date has been fully provided
    if (
      this.dateFromDayFieldRef.current.props.value !== "" &&
      this.dateFromMonthFieldRef.current.props.value !== "" &&
      this.dateFromYearFieldRef.current.props.value !== ""
    ) {
      dateFrom = `${this.dateFromDayFieldRef.current.props.value.value}-${this.dateFromMonthFieldRef.current.props.value.value}-${this.dateFromYearFieldRef.current.props.value.value}`;
      this.setState({ dateToVisible: true });
    }

    // date to field is visible, meaning that the date from date has been fully provided
    if (this.state.dateToVisible) {
      // date to date has been fully provided
      if (
        this.dateToDayFieldRef.current.props.value !== "" &&
        this.dateToMonthFieldRef.current.props.value !== "" &&
        this.dateToYearFieldRef.current.props.value !== ""
      ) {
        dateTo = `${this.dateToDayFieldRef.current.props.value.value}-${this.dateToMonthFieldRef.current.props.value.value}-${this.dateToYearFieldRef.current.props.value.value}`;
      }
    }
    // both dates have been provided
    if (dateFrom !== null && dateTo !== null) {
      // valid dates
      if (this.dateValidation(dateFrom, dateTo)) {
        //
        this.setState(
          {
            generateReportButtonDisabled: false,
            selectedDateFrom: dateFrom,
            selectedDateTo: dateTo,
            displayInvalidDateFromError: false,
            displayInvalidDateToError: false,
            displayDateError: false
          },
          () => {
            // updating all needed redux states
            this.props.setReportSelectedParameters(
              false,
              null,
              null,
              null,
              null,
              null,
              null,
              this.state.selectedDateFrom,
              this.state.selectedDateTo,
              null
            );
            // enabling generate report button
            this.props.setGenerateReportDisabledState(false);
          }
        );
      } else {
        this.setState({
          // disable generate button on invalid date
          generateReportButtonDisabled: true
        });
        this.props.setGenerateReportDisabledState(true);
      }
    }
  };

  // date validation
  dateValidation = (from, to) => {
    // converting string dates in dates (provided format: 'dd-mm-yyyy')
    const dateFrom = new Date(from.split("-")[2], from.split("-")[1] - 1, from.split("-")[0]);
    const dateTo = new Date(to.split("-")[2], to.split("-")[1] - 1, to.split("-")[0]);

    // dates validation
    // reformatting date in format 'yyyy-mm-dd' for validation function
    const isValidDateFrom = validateDatePicked(
      `${from.split("-")[2]}-${from.split("-")[1]}-${from.split("-")[0]}`
    );
    const isValidDateTo = validateDatePicked(
      `${to.split("-")[2]}-${to.split("-")[1]}-${to.split("-")[0]}`
    );

    // invalid from date
    if (!isValidDateFrom || !isValidDateTo) {
      this.setState({
        displayInvalidDateFromError: !isValidDateFrom,
        displayInvalidDateToError: !isValidDateTo,
        displayDateError: false,
        generateReportButtonDisabled: true
      });
      return false;
    }
    // date from is greater to date to (invalid)
    if (dateFrom > dateTo) {
      this.setState({
        displayDateError: true,
        generateReportButtonDisabled: true,
        displayInvalidDateFromError: !isValidDateFrom,
        displayInvalidDateToError: !isValidDateTo
      });
      return false;
    }
    // everything is valid
    return true;
  };

  // this function is called for accessibility purposes
  // it will update the completeDatePicked redux state on focus, so the screen reader will read the current selected date
  onDateFromFocus = () => {
    this.props.updateDatePicked(
      `${
        this.dateFromYearFieldRef.current
          ? this.dateFromYearFieldRef.current.props.value.label
          : LOCALIZE.datePicker.none
      }-${
        this.dateFromMonthFieldRef.current
          ? this.dateFromMonthFieldRef.current.props.value.label
          : LOCALIZE.datePicker.none
      }-${
        this.dateFromDayFieldRef.current
          ? this.dateFromDayFieldRef.current.props.value.label
          : LOCALIZE.datePicker.none
      }`
    );
  };

  // this function is called for accessibility purposes
  // it will update the completeDatePicked redux state on focus, so the screen reader will read the current selected date
  onDateToFocus = () => {
    this.props.updateDatePicked(
      `${
        this.dateToYearFieldRef.current
          ? this.dateToYearFieldRef.current.props.value.label
          : LOCALIZE.datePicker.none
      }-${
        this.dateToMonthFieldRef.current
          ? this.dateToMonthFieldRef.current.props.value.label
          : LOCALIZE.datePicker.none
      }-${
        this.dateToDayFieldRef.current
          ? this.dateToDayFieldRef.current.props.value.label
          : LOCALIZE.datePicker.none
      }`
    );
  };

  render() {
    const {
      dateToVisible,
      displayInvalidDateFromError,
      displayInvalidDateToError,
      displayDateError
    } = this.state;
    return (
      <div>
        <Row
          className="align-items-center justify-content-start"
          style={styles.optionSelectionContainer}
          role="presentation"
        >
          <Col
            xl={columnSizes.firstColumn.xl}
            lg={columnSizes.firstColumn.lg}
            md={columnSizes.firstColumn.md}
            sm={columnSizes.firstColumn.sm}
            xs={columnSizes.firstColumn.xs}
            style={
              displayDateError
                ? { ...styles.labelContainer, ...styles.dateLabelsOnError }
                : styles.labelContainer
            }
          >
            <label id="date-from-label">{LOCALIZE.reports.dateFromLabel}</label>
          </Col>
          <Col
            xl={columnSizes.secondColumn.xl}
            lg={columnSizes.secondColumn.lg}
            md={columnSizes.secondColumn.md}
            sm={columnSizes.secondColumn.sm}
            xs={columnSizes.secondColumn.xs}
            style={styles.singleDropdownContainer}
            onFocus={this.onDateFromFocus}
          >
            <DatePicker
              dateDayFieldRef={this.dateFromDayFieldRef}
              dateMonthFieldRef={this.dateFromMonthFieldRef}
              dateYearFieldRef={this.dateFromYearFieldRef}
              dateLabelId={"date-from-label"}
              onInputChange={this.handleDateUpdates}
              isValidCompleteDatePicked={!displayInvalidDateFromError}
              customYearOptions={populateCustomYearsFutureDateOptions()}
              disabledDropdowns={this.props.currentlyLoading}
            />
          </Col>
        </Row>
        {dateToVisible && (
          <Row
            className="align-items-center justify-content-start"
            style={styles.optionSelectionContainer}
            role="presentation"
          >
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
              style={
                displayDateError
                  ? { ...styles.labelContainer, ...styles.dateLabelsOnError }
                  : styles.labelContainer
              }
            >
              <label id="date-to-label">{LOCALIZE.reports.dateToLabel}</label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={styles.singleDropdownContainer}
              onFocus={this.onDateToFocus}
            >
              <DatePicker
                dateDayFieldRef={this.dateToDayFieldRef}
                dateMonthFieldRef={this.dateToMonthFieldRef}
                dateYearFieldRef={this.dateToYearFieldRef}
                dateLabelId={"date-to-label"}
                onInputChange={this.handleDateUpdates}
                isValidCompleteDatePicked={!displayInvalidDateToError}
                customYearOptions={populateCustomYearsFutureDateOptions()}
                disabledDropdowns={this.props.currentlyLoading}
              />
            </Col>
          </Row>
        )}
        {displayDateError && (
          <Row
            className="align-items-center justify-content-end"
            style={styles.optionSelectionContainer}
          >
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
            >
              <p tabIndex={"0"} style={styles.dateError}>
                {LOCALIZE.reports.dateError}
              </p>
            </Col>
          </Row>
        )}
      </div>
    );
  }
}

export { FinancialReport as unconnectedFinancialReport };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    completeDatePicked: state.datePicker.completeDatePicked
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setReportSelectedParameters,
      updateDatePicked,
      setGenerateReportDisabledState
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(FinancialReport));
