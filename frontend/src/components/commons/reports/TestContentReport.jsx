/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Component } from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import LOCALIZE from "../../../text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  setReportSelectedParameters,
  setGenerateReportDisabledState,
  getTestDefinitionData
} from "../../../modules/ReportsRedux";
import DropdownSelect from "../DropdownSelect";
import { Row, Col } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 4,
    xl: 4
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 8,
    xl: 8
  }
};
const styles = {
  optionSelectionContainer: {
    padding: "12px 24px",
    margin: "0 auto",
    width: "80%"
  },
  labelContainer: {
    verticalAlign: "middle",
    paddingRight: 12
  },
  singleDropdownContainer: {
    verticalAlign: "middle",
    width: "100%"
  },
  loading: {
    width: "100%",
    minHeight: 60,
    textAlign: "center",
    fontSize: "32px",
    padding: 6
  }
};

class TestContentReport extends Component {
  static propTypes = {
    currentlyLoading: PropTypes.bool.isRequired
  };

  state = {
    isLoadingParentCode: true,
    parentCodeVisible: false,
    parentCodeOptions: [],
    selectedParentCode: "",
    isLoadingTestCode: true,
    testCodeVisible: false,
    testCodeOptions: [],
    selectedTestCode: "",
    isLoadingTestVersion: true,
    testVersionVisible: false,
    testVersionOptions: [],
    selectedTestVersion: ""
  };

  // setting all states to their initial values
  async getInitialState() {
    this.setState({
      isLoadingParentCode: true,
      parentCodeVisible: false,
      parentCodeOptions: [],
      selectedParentCode: "",
      isLoadingTestCode: true,
      testCodeVisible: false,
      testCodeOptions: [],
      selectedTestCode: "",
      isLoadingTestVersion: true,
      testVersionVisible: false,
      testVersionOptions: [],
      selectedTestVersion: ""
    });
  }

  componentDidMount = () => {
    this.populateParentCodesOptions();
  };

  // populate parent code options
  populateParentCodesOptions = () => {
    this.setState(
      {
        parentCodeVisible: true,
        testCodeVisible: false,
        testVersionVisible: false,
        isLoadingParentCode: true
      },
      () => {
        this.props.getTestDefinitionData().then(response => {
          const parentCodeOptions = [];
          for (let i = 0; i < response.length; i++) {
            parentCodeOptions.push({
              value: response[i].parent_code,
              label: response[i].parent_code
            });
          }
          this.getInitialState().then(() => {
            // disabling generate report button
            this.props.setGenerateReportDisabledState(true);
            this.setState({
              parentCodeVisible: true,
              parentCodeOptions: parentCodeOptions,
              isLoadingParentCode: false
            });
          });
        });
      }
    );
  };

  // get selected parent code
  getSelectedParentCode = selectedOption => {
    this.setState(
      {
        selectedParentCode: selectedOption,
        testCodeVisible: true,
        testVersionVisible: false,
        isLoadingTestCode: true
      },
      () => {
        this.props.getTestDefinitionData(selectedOption.value).then(response => {
          const testCodeOptions = [];
          for (let i = 0; i < response.length; i++) {
            testCodeOptions.push({
              value: response[i].test_code,
              label: response[i].test_code
            });
          }
          // disabling generate report button
          this.props.setGenerateReportDisabledState(true);
          this.setState({
            testCodeVisible: true,
            testCodeOptions: testCodeOptions,
            selectedTestCode: "",
            testVersionVisible: false,
            testVersionOptions: null,
            selectedTestVersion: "",
            isLoadingTestCode: false
          });
        });
      }
    );
  };

  // get selected test code
  getSelectedTestCode = selectedOption => {
    this.setState(
      {
        selectedTestCode: selectedOption,
        testVersionVisible: true,
        isLoadingTestVersion: true
      },
      () => {
        this.props
          .getTestDefinitionData(this.state.selectedParentCode.value, selectedOption.value)
          .then(response => {
            const testVersionOptions = [];
            for (let i = 0; i < response.length; i++) {
              testVersionOptions.push({
                value: response[i].version,
                label: `${response[i].version}`
              });
            }
            // disabling generate report button
            this.props.setGenerateReportDisabledState(true);
            this.setState({
              testVersionVisible: true,
              testVersionOptions: testVersionOptions,
              selectedTestVersion: "",
              isLoadingTestVersion: false
            });
          });
      }
    );
  };

  // get selected test version
  getSelectedTestVersion = selectedOption => {
    this.setState(
      {
        selectedTestVersion: selectedOption
      },
      () => {
        // updating all needed redux states
        this.props.setReportSelectedParameters(
          false,
          null,
          null,
          null,
          this.state.selectedParentCode,
          this.state.selectedTestCode,
          this.state.selectedTestVersion,
          null,
          null,
          null
        );
        // enabling generate report button
        this.props.setGenerateReportDisabledState(false);
      }
    );
  };

  render() {
    const {
      isLoadingParentCode,
      parentCodeVisible,
      parentCodeOptions,
      selectedParentCode,
      isLoadingTestCode,
      testCodeVisible,
      testCodeOptions,
      selectedTestCode,
      isLoadingTestVersion,
      testVersionVisible,
      testVersionOptions,
      selectedTestVersion
    } = this.state;
    return (
      <div>
        {parentCodeVisible && (
          <Row
            className="align-items-center justify-content-start"
            id="unit-test-parent-code-dropdown"
            style={styles.optionSelectionContainer}
            role="presentation"
          >
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
              style={styles.labelContainer}
            >
              <label id="parent-code-label">{LOCALIZE.reports.parentCodeLabel}</label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={styles.singleDropdownContainer}
            >
              {isLoadingParentCode ? (
                <label className="fa fa-spinner fa-spin" style={styles.loading}>
                  <FontAwesomeIcon icon={faSpinner} />
                </label>
              ) : (
                <DropdownSelect
                  idPrefix="test-content-report-parent-code-dropdown"
                  ariaLabelledBy="parent-code-label"
                  isDisabled={this.props.currentlyLoading}
                  options={parentCodeOptions}
                  onChange={this.getSelectedParentCode}
                  defaultValue={selectedParentCode}
                  hasPlaceholder={true}
                ></DropdownSelect>
              )}
            </Col>
          </Row>
        )}
        {testCodeVisible && (
          <Row
            className="align-items-center justify-content-start"
            id="unit-test-test-code-dropdown"
            style={styles.optionSelectionContainer}
            role="presentation"
          >
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
              style={styles.labelContainer}
            >
              <label id="test-code-label">{LOCALIZE.reports.testCodeLabel}</label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={styles.singleDropdownContainer}
            >
              {isLoadingTestCode ? (
                <label className="fa fa-spinner fa-spin" style={styles.loading}>
                  <FontAwesomeIcon icon={faSpinner} />
                </label>
              ) : (
                <DropdownSelect
                  idPrefix="test-content-report-test-code-dropdown"
                  ariaLabelledBy="test-code-label"
                  isDisabled={this.props.currentlyLoading}
                  options={testCodeOptions}
                  onChange={this.getSelectedTestCode}
                  defaultValue={selectedTestCode}
                  hasPlaceholder={true}
                ></DropdownSelect>
              )}
            </Col>
          </Row>
        )}
        {testVersionVisible && (
          <Row
            className="align-items-center justify-content-start"
            id="unit-test-test-version-dropdown"
            style={styles.optionSelectionContainer}
            role="presentation"
          >
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
              style={styles.labelContainer}
            >
              <label id="test-version-label">{LOCALIZE.reports.testVersionLabel}</label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={styles.singleDropdownContainer}
            >
              {isLoadingTestVersion ? (
                <label className="fa fa-spinner fa-spin" style={styles.loading}>
                  <FontAwesomeIcon icon={faSpinner} />
                </label>
              ) : (
                <DropdownSelect
                  idPrefix="test-content-report-test-version-dropdown"
                  ariaLabelledBy="test-version-label"
                  isDisabled={this.props.currentlyLoading}
                  options={testVersionOptions}
                  onChange={this.getSelectedTestVersion}
                  defaultValue={selectedTestVersion}
                  hasPlaceholder={true}
                  orderByLabels={false}
                ></DropdownSelect>
              )}
            </Col>
          </Row>
        )}
      </div>
    );
  }
}

export { TestContentReport as unconnectedTestContentReport };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setReportSelectedParameters,
      setGenerateReportDisabledState,
      getTestDefinitionData
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(TestContentReport));
