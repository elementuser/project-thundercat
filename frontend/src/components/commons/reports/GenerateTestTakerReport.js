import LOCALIZE from "../../../text_resources";
import getConvertedScore from "../../../helpers/scoreConversion";
import getFormattedStatus, { getLanguage, removeHtmlFromString } from "./utils";
import { LANGUAGES, LANGUAGE_IDS } from "../../../modules/LocalizeRedux";
import EXTENDED_PROFILE_EE_INFO, {
  ABORIGINAL_ID,
  DISABILITY_ID,
  VISIBLE_MINORITY_ID
} from "../../profile/Constants";
import { ITEM_OPTION_TYPE } from "../../testBuilder/itemBank/items/Constants";

const EXTENEDED_PROFILE_DATA_TYPE = {
  currentEmployer: "current_employer",
  organization: "organization",
  group: "group",
  level: "level",
  residence: "residence",
  education: "education",
  gender: "gender",
  identifyAsWoman: "woman",
  aboriginal: "aboriginal",
  aboriginalInuit: "aboriginal_inuit",
  aboriginalMetis: "aboriginal_metis",
  aboriginalNAIndianFirstNation: "aboriginal_na_indian_first_nation",
  visibleMinority: "visible_minority",
  visibleMinorityBlack: "visible_minority_black",
  visibleMinorityChinese: "visible_minority_chinese",
  visibleMinorityFilipino: "visible_minority_filipino",
  visibleMinorityJapanese: "visible_minority_japanese",
  visibleMinorityKorean: "visible_minority_korean",
  visibleMinorityNonWhiteLatinx: "visible_minority_non_white_latinx",
  visibleMinorityNonWhiteMiddleEastern: "visible_minority_non_white_middle_eastern",
  visibleMinorityMixedOrigin: "visible_minority_mixed_origin",
  visibleMinoritySouthAsianEastIndian: "visible_minority_south_asian_east_indian",
  visibleMinoritySoutheastAsian: "visible_minority_southeast_asian",
  visibleMinorityOther: "visible_minority_other",
  visibleMinorityOtherDetails: "visible_minority_other_details",
  disability: "disability",
  disabilityVisual: "disability_visual",
  disabilityDexterity: "disability_dexterity",
  disabilityHearing: "disability_hearing",
  disabilityMobility: "disability_mobility",
  disabilitySpeech: "disability_speech",
  disabilityOther: "disability_other"
};

// getting all combined question sections text of specific question
function getQuestionText(assigned_question, questionTextArray, language, displayHtml) {
  // initializing finalText
  let finalText = "";

  // questions coming from item bank
  if (typeof assigned_question.item_bank_id !== "undefined") {
    // looping in questionTextArray
    for (let i = 0; i < questionTextArray.length; i++) {
      finalText += questionTextArray[i].text;
      finalText += " ";
    }
    // question from test builder
  } else {
    // looping in questionTextArray
    for (let i = 0; i < questionTextArray.length; i++) {
      // ENGLISH
      if (language === LANGUAGES.english) {
        // making sure that the text (EN) is defined
        if (questionTextArray[i].question_sections_type_markdown_en) {
          // display HTML
          if (displayHtml) {
            // removing starting and ending double quotes from string
            finalText += questionTextArray[i].question_sections_type_markdown_html_en.substring(
              1,
              questionTextArray[i].question_sections_type_markdown_html_en.length - 1
            );
            finalText += " ";
            // do not display HTML
          } else {
            // removing starting and ending double quotes from string
            finalText += questionTextArray[i].question_sections_type_markdown_en.substring(
              1,
              questionTextArray[i].question_sections_type_markdown_en.length - 1
            );
            finalText += " ";
          }
        }
        // FRENCH
      } else {
        // making sure that the text (FR) is defined
        if (questionTextArray[i].question_sections_type_markdown_fr) {
          // display HTML
          if (displayHtml) {
            // removing starting and ending double quotes from string
            finalText += questionTextArray[i].question_sections_type_markdown_html_fr.substring(
              1,
              questionTextArray[i].question_sections_type_markdown_html_fr.length - 1
            );
            finalText += " ";
            // do not display HTML
          } else {
            // removing starting and ending double quotes from string
            finalText += questionTextArray[i].question_sections_type_markdown_fr.substring(
              1,
              questionTextArray[i].question_sections_type_markdown_fr.length - 1
            );
            finalText += " ";
          }
        }
      }
    }
  }
  // display HTML
  if (displayHtml) {
    // adding double quotes to final strings, so commas are not considered as separators and are well formatted in the CSV
    return `"${finalText}"`;
  }
  // do not display HTML
  // adding double quotes to final strings + removing HTML tags, so commas are not considered as separators and are well formatted in the CSV
  return `"${removeHtmlFromString(finalText)}"`;
}

function getAnswerChoiceOrder(fromItemBank, answerChoices, index) {
  // initializing assignedAnswerChoiceOrder
  let assignedAnswerChoiceOrder = "";

  // questions coming from item bank
  if (fromItemBank) {
    // getting respective answer choice data
    const respective_answer_choice_data_en = answerChoices.filter(
      obj =>
        obj.language_id === LANGUAGE_IDS.english &&
        obj.option_type === ITEM_OPTION_TYPE.multipleChoice
    );
    const respective_answer_choice_data_fr = answerChoices.filter(
      obj =>
        obj.language_id === LANGUAGE_IDS.french &&
        obj.option_type === ITEM_OPTION_TYPE.multipleChoice
    );
    if (typeof respective_answer_choice_data_en[index] !== "undefined") {
      assignedAnswerChoiceOrder =
        respective_answer_choice_data_en[index].assigned_answer_choice_order;
    } else if (typeof respective_answer_choice_data_fr[index] !== "undefined") {
      assignedAnswerChoiceOrder =
        respective_answer_choice_data_fr[index].assigned_answer_choice_order;
    }
  }
  // question from test builder
  else {
    if (typeof answerChoices[index] !== "undefined") {
      assignedAnswerChoiceOrder = answerChoices[index].assigned_answer_choice_order;
    }
  }

  return assignedAnswerChoiceOrder;
}

function getAnswerChoiceText(fromItemBank, answerChoices, index, languageId, displayHtml) {
  // initializing fnialAnswerChoiceText
  let fnialAnswerChoiceText = "";

  // questions coming from item bank
  if (fromItemBank) {
    // getting respective answer choice data
    const respective_answer_choice_data = answerChoices.filter(
      obj => obj.language_id === languageId && obj.option_type === ITEM_OPTION_TYPE.multipleChoice
    );
    if (typeof respective_answer_choice_data[index] !== "undefined") {
      fnialAnswerChoiceText = respective_answer_choice_data[index].text;
    }
  }
  // question from test builder
  else {
    // ENGLISH
    if (languageId === LANGUAGE_IDS.english) {
      if (typeof answerChoices[index] !== "undefined") {
        fnialAnswerChoiceText = answerChoices[index].answer_details_en;
      }
    }
    // ENGLISH
    else if (languageId === LANGUAGE_IDS.french) {
      if (typeof answerChoices[index] !== "undefined") {
        fnialAnswerChoiceText = answerChoices[index].answer_details_fr;
      }
    }
  }

  // display HTML
  if (displayHtml) {
    // adding double quotes to final strings, so commas are not considered as separators and are well formatted in the CSV
    return `"${fnialAnswerChoiceText}"`;
  }
  // do not display HTML
  // adding double quotes to final strings + removing HTML tags, so commas are not considered as separators and are well formatted in the CSV
  return `"${removeHtmlFromString(fnialAnswerChoiceText)}"`;
}

function getFormattedUserCode(userId) {
  // initializing finalString
  let finalString = "";
  // getting number of needed zeros (total of 9 chars including user ID)
  const numberOfZerosNeeded = parseInt(9 - userId.toString().length);
  // looping in numberOfZerosNeeded
  for (let i = 0; i < numberOfZerosNeeded; i++) {
    // adding zero
    finalString = finalString.concat("0");
  }
  // adding user ID to finalString
  finalString = finalString.concat(`${userId}`);
  // adding fromCharCode in order to display leading zeros
  return `${finalString}${String.fromCharCode(8203)}`;
}

function getFormattedEeGroupSelection(groupId) {
  switch (groupId) {
    case EXTENDED_PROFILE_EE_INFO.no:
      return 0;
    case EXTENDED_PROFILE_EE_INFO.yes:
      return 1;
    case EXTENDED_PROFILE_EE_INFO.preferNotToSay:
      return 9;
    default:
      return "";
  }
}

function getFormattedAboriginalSubGroup(subGroupData, aboriginalId) {
  // getting aboriginalSubGroupData based on provided ID
  const aboriginalSubGroupData = subGroupData.filter(obj => obj.abrg_id === aboriginalId)[0];
  // checking if prefer not to say is selected
  if (subGroupData[0].checked === true) {
    return 9;
  }
  // checking if specified ID is checked
  if (aboriginalSubGroupData.checked === true) {
    return 1;
  }
  return 0;
}

function getFormattedVisibleMinoritySubGroup(subGroupData, visibleMinorityId) {
  // getting visibleMinoritySubGroupData based on provided ID
  const visibleMinoritySubGroupData = subGroupData.filter(
    obj => obj.vismin_id === visibleMinorityId
  )[0];
  // checking if prefer not to say is selected
  if (subGroupData[0].checked === true) {
    return 9;
  }
  // checking if specified ID is checked
  if (visibleMinoritySubGroupData.checked === true) {
    return 1;
  }
  return 0;
}

function getFormattedDisabilitySubGroup(subGroupData, disabilityId) {
  // getting disabilitySubGroupData based on provided ID
  const disabilitySubGroupData = subGroupData.filter(obj => obj.dsbl_id === disabilityId)[0];
  // checking if prefer not to say is selected
  if (subGroupData[0].checked === true) {
    return 9;
  }
  // checking if specified ID is checked
  if (disabilitySubGroupData.checked === true) {
    return 1;
  }
  return 0;
}

function getextendedProfileLabel(extendedProfileData, dataType) {
  const { selectedLanguage } = LOCALIZE.reports.testTakerReport;

  if (extendedProfileData.length > 0) {
    switch (dataType) {
      // current employer
      case EXTENEDED_PROFILE_DATA_TYPE.currentEmployer:
        if (extendedProfileData[0].current_employer !== null) {
          return extendedProfileData[0].current_employer[0][selectedLanguage];
        }
        return LOCALIZE.commons.na;
      // organization
      case EXTENEDED_PROFILE_DATA_TYPE.organization:
        if (extendedProfileData[0].organization !== null) {
          return extendedProfileData[0].organization[0][selectedLanguage];
        }
        return LOCALIZE.commons.na;
      // group
      case EXTENEDED_PROFILE_DATA_TYPE.group:
        if (extendedProfileData[0].group_and_level !== null) {
          return extendedProfileData[0].group_and_level[0].class_grp_cd;
        }
        return LOCALIZE.commons.na;
      // level
      case EXTENEDED_PROFILE_DATA_TYPE.level:
        if (extendedProfileData[0].group_and_level !== null) {
          if (extendedProfileData[0].group_and_level[0].class_lvl_cd !== null) {
            return extendedProfileData[0].group_and_level[0].class_lvl_cd;
          }
          return LOCALIZE.commons.na;
        }
        return LOCALIZE.commons.na;
      // residence
      case EXTENEDED_PROFILE_DATA_TYPE.residence:
        if (extendedProfileData[0].residence !== null) {
          return extendedProfileData[0].residence[0][selectedLanguage];
        }
        return LOCALIZE.commons.na;
      // education
      case EXTENEDED_PROFILE_DATA_TYPE.education:
        if (extendedProfileData[0].education !== null) {
          return extendedProfileData[0].education[0][selectedLanguage];
        }
        return LOCALIZE.commons.na;
      // gender
      case EXTENEDED_PROFILE_DATA_TYPE.gender:
        if (extendedProfileData[0].gender !== null) {
          return extendedProfileData[0].gender[0][selectedLanguage];
        }
        return LOCALIZE.commons.na;
      default:
        return LOCALIZE.commons.na;
    }
  } else {
    return LOCALIZE.commons.na;
  }
}

function getextendedEeInfoData(data, dataType) {
  if (data.length > 0) {
    switch (dataType) {
      // identify as woman
      case EXTENEDED_PROFILE_DATA_TYPE.identifyAsWoman:
        return getFormattedEeGroupSelection(
          data[0].identify_as_woman !== null ? data[0].identify_as_woman.identify_as_woman_id : null
        );
      // aboriginal
      case EXTENEDED_PROFILE_DATA_TYPE.aboriginal:
        return getFormattedEeGroupSelection(
          data[0].aboriginal !== null ? data[0].aboriginal.aboriginal_id : null
        );
      // aboriginal inuit
      case EXTENEDED_PROFILE_DATA_TYPE.aboriginalInuit:
        if (
          data[0].aboriginal !== null &&
          typeof data[0].aboriginal.aboriginal_sub_groups_array !== "undefined"
        ) {
          return getFormattedAboriginalSubGroup(
            data[0].aboriginal.aboriginal_sub_groups_array,
            ABORIGINAL_ID.inuit
          );
        }
        return "";
      // aboriginal metis
      case EXTENEDED_PROFILE_DATA_TYPE.aboriginalMetis:
        if (
          data[0].aboriginal !== null &&
          typeof data[0].aboriginal.aboriginal_sub_groups_array !== "undefined"
        ) {
          return getFormattedAboriginalSubGroup(
            data[0].aboriginal.aboriginal_sub_groups_array,
            ABORIGINAL_ID.metis
          );
        }
        return "";
      // aboriginal NA Indian/First Nation
      case EXTENEDED_PROFILE_DATA_TYPE.aboriginalNAIndianFirstNation:
        if (
          data[0].aboriginal !== null &&
          typeof data[0].aboriginal.aboriginal_sub_groups_array !== "undefined"
        ) {
          return getFormattedAboriginalSubGroup(
            data[0].aboriginal.aboriginal_sub_groups_array,
            ABORIGINAL_ID.naIndianFirstNation
          );
        }
        return "";
      // visible minority
      case EXTENEDED_PROFILE_DATA_TYPE.visibleMinority:
        return getFormattedEeGroupSelection(
          data[0].visible_minority !== null ? data[0].visible_minority.visible_minority_id : null
        );
      // visible minority black
      case EXTENEDED_PROFILE_DATA_TYPE.visibleMinorityBlack:
        if (
          data[0].visible_minority !== null &&
          typeof data[0].visible_minority.visible_minority_sub_groups_array !== "undefined"
        ) {
          return getFormattedVisibleMinoritySubGroup(
            data[0].visible_minority.visible_minority_sub_groups_array,
            VISIBLE_MINORITY_ID.black
          );
        }
        return "";
      // visible minority chinese
      case EXTENEDED_PROFILE_DATA_TYPE.visibleMinorityChinese:
        if (
          data[0].visible_minority !== null &&
          typeof data[0].visible_minority.visible_minority_sub_groups_array !== "undefined"
        ) {
          return getFormattedVisibleMinoritySubGroup(
            data[0].visible_minority.visible_minority_sub_groups_array,
            VISIBLE_MINORITY_ID.chinese
          );
        }
        return "";
      // visible minority filipino
      case EXTENEDED_PROFILE_DATA_TYPE.visibleMinorityFilipino:
        if (
          data[0].visible_minority !== null &&
          typeof data[0].visible_minority.visible_minority_sub_groups_array !== "undefined"
        ) {
          return getFormattedVisibleMinoritySubGroup(
            data[0].visible_minority.visible_minority_sub_groups_array,
            VISIBLE_MINORITY_ID.filipino
          );
        }
        return "";
      // visible minority japanese
      case EXTENEDED_PROFILE_DATA_TYPE.visibleMinorityJapanese:
        if (
          data[0].visible_minority !== null &&
          typeof data[0].visible_minority.visible_minority_sub_groups_array !== "undefined"
        ) {
          return getFormattedVisibleMinoritySubGroup(
            data[0].visible_minority.visible_minority_sub_groups_array,
            VISIBLE_MINORITY_ID.japanese
          );
        }
        return "";
      // visible minority korean
      case EXTENEDED_PROFILE_DATA_TYPE.visibleMinorityKorean:
        if (
          data[0].visible_minority !== null &&
          typeof data[0].visible_minority.visible_minority_sub_groups_array !== "undefined"
        ) {
          return getFormattedVisibleMinoritySubGroup(
            data[0].visible_minority.visible_minority_sub_groups_array,
            VISIBLE_MINORITY_ID.korean
          );
        }
        return "";
      // visible minority non-white latinx
      case EXTENEDED_PROFILE_DATA_TYPE.visibleMinorityNonWhiteLatinx:
        if (
          data[0].visible_minority !== null &&
          typeof data[0].visible_minority.visible_minority_sub_groups_array !== "undefined"
        ) {
          return getFormattedVisibleMinoritySubGroup(
            data[0].visible_minority.visible_minority_sub_groups_array,
            VISIBLE_MINORITY_ID.nonWhiteLatinAmerican
          );
        }
        return "";
      // visible minority non-white middle eastern
      case EXTENEDED_PROFILE_DATA_TYPE.visibleMinorityNonWhiteMiddleEastern:
        if (
          data[0].visible_minority !== null &&
          typeof data[0].visible_minority.visible_minority_sub_groups_array !== "undefined"
        ) {
          return getFormattedVisibleMinoritySubGroup(
            data[0].visible_minority.visible_minority_sub_groups_array,
            VISIBLE_MINORITY_ID.nonWhiteMiddleEastern
          );
        }
        return "";
      // visible minority mixed origin
      case EXTENEDED_PROFILE_DATA_TYPE.visibleMinorityMixedOrigin:
        if (
          data[0].visible_minority !== null &&
          typeof data[0].visible_minority.visible_minority_sub_groups_array !== "undefined"
        ) {
          return getFormattedVisibleMinoritySubGroup(
            data[0].visible_minority.visible_minority_sub_groups_array,
            VISIBLE_MINORITY_ID.mixedOrigin
          );
        }
        return "";
      // visible minority south asian/east indian
      case EXTENEDED_PROFILE_DATA_TYPE.visibleMinoritySouthAsianEastIndian:
        if (
          data[0].visible_minority !== null &&
          typeof data[0].visible_minority.visible_minority_sub_groups_array !== "undefined"
        ) {
          return getFormattedVisibleMinoritySubGroup(
            data[0].visible_minority.visible_minority_sub_groups_array,
            VISIBLE_MINORITY_ID.southAsianEastIndian
          );
        }
        return "";
      // visible minority southeast asian
      case EXTENEDED_PROFILE_DATA_TYPE.visibleMinoritySoutheastAsian:
        if (
          data[0].visible_minority !== null &&
          typeof data[0].visible_minority.visible_minority_sub_groups_array !== "undefined"
        ) {
          return getFormattedVisibleMinoritySubGroup(
            data[0].visible_minority.visible_minority_sub_groups_array,
            VISIBLE_MINORITY_ID.southeastAsian
          );
        }
        return "";
      // visible minority other
      case EXTENEDED_PROFILE_DATA_TYPE.visibleMinorityOther:
        if (
          data[0].visible_minority !== null &&
          typeof data[0].visible_minority.visible_minority_sub_groups_array !== "undefined"
        ) {
          return getFormattedVisibleMinoritySubGroup(
            data[0].visible_minority.visible_minority_sub_groups_array,
            VISIBLE_MINORITY_ID.other
          );
        }
        return "";
      // visible minority other (details)
      case EXTENEDED_PROFILE_DATA_TYPE.visibleMinorityOtherDetails:
        if (
          data[0].visible_minority !== null &&
          typeof data[0].visible_minority.visible_minority_sub_groups_array !== "undefined"
        ) {
          const otherDetails = data[0].visible_minority.visible_minority_sub_groups_array.filter(
            obj => obj.vismin_id === VISIBLE_MINORITY_ID.other
          )[0];
          if (otherDetails.checked === true) {
            return otherDetails.other_specification;
          }
          return "";
        }
        return "";
      // disability
      case EXTENEDED_PROFILE_DATA_TYPE.disability:
        return getFormattedEeGroupSelection(
          data[0].disability !== null ? data[0].disability.disability_id : null
        );
      // disability visual
      case EXTENEDED_PROFILE_DATA_TYPE.disabilityVisual:
        if (
          data[0].disability !== null &&
          typeof data[0].disability.disability_sub_groups_array !== "undefined"
        ) {
          return getFormattedDisabilitySubGroup(
            data[0].disability.disability_sub_groups_array,
            DISABILITY_ID.visual
          );
        }
        return "";
      // disability dexterity
      case EXTENEDED_PROFILE_DATA_TYPE.disabilityDexterity:
        if (
          data[0].disability !== null &&
          typeof data[0].disability.disability_sub_groups_array !== "undefined"
        ) {
          return getFormattedDisabilitySubGroup(
            data[0].disability.disability_sub_groups_array,
            DISABILITY_ID.dexterity
          );
        }
        return "";
      // disability hearing
      case EXTENEDED_PROFILE_DATA_TYPE.disabilityHearing:
        if (
          data[0].disability !== null &&
          typeof data[0].disability.disability_sub_groups_array !== "undefined"
        ) {
          return getFormattedDisabilitySubGroup(
            data[0].disability.disability_sub_groups_array,
            DISABILITY_ID.hearing
          );
        }
        return "";
      // disability mobility
      case EXTENEDED_PROFILE_DATA_TYPE.disabilityMobility:
        if (
          data[0].disability !== null &&
          typeof data[0].disability.disability_sub_groups_array !== "undefined"
        ) {
          return getFormattedDisabilitySubGroup(
            data[0].disability.disability_sub_groups_array,
            DISABILITY_ID.mobility
          );
        }
        return "";
      // disability speech
      case EXTENEDED_PROFILE_DATA_TYPE.disabilitySpeech:
        if (
          data[0].disability !== null &&
          typeof data[0].disability.disability_sub_groups_array !== "undefined"
        ) {
          return getFormattedDisabilitySubGroup(
            data[0].disability.disability_sub_groups_array,
            DISABILITY_ID.speech
          );
        }
        return "";
      // disability other
      case EXTENEDED_PROFILE_DATA_TYPE.disabilityOther:
        if (
          data[0].disability !== null &&
          typeof data[0].disability.disability_sub_groups_array !== "undefined"
        ) {
          return getFormattedDisabilitySubGroup(
            data[0].disability.disability_sub_groups_array,
            DISABILITY_ID.other
          );
        }
        return "";
      // disability other (details)
      case EXTENEDED_PROFILE_DATA_TYPE.disabilityOtherDetails:
        if (
          data[0].disability !== null &&
          typeof data[0].disability.disability_sub_groups_array !== "undefined"
        ) {
          const otherDetails = data[0].disability.disability_sub_groups_array.filter(
            obj => obj.dsbl_id === DISABILITY_ID.other
          )[0];
          if (otherDetails.checked === true) {
            return otherDetails.other_specification;
          }
          return "";
        }
        return "";
      default:
        return "";
    }
  } else {
    return "";
  }
}

function getQuestionLanguage(assigned_question) {
  // initializing questionSectionLanguage
  let questionSectionLanguage = null;

  // BILINGUAL
  if (assigned_question.section_language === LANGUAGES.bilingual) {
    questionSectionLanguage = LOCALIZE.commons.bilingual;
    // FRENCH
  } else if (assigned_question.section_language === LANGUAGES.french) {
    questionSectionLanguage = LOCALIZE.commons.french;
    // ENGLISH
  } else if (assigned_question.section_language === LANGUAGES.english) {
    questionSectionLanguage = LOCALIZE.commons.english;
    // should not happen
  } else {
    questionSectionLanguage = LOCALIZE.commons.error;
  }

  return questionSectionLanguage;
}

function generateTestTakerReport(providedReportDataArray) {
  // initializing reportData
  const reportData = [];

  // getting maximum amount of answer choices based on provided report data
  let maximumAmountOfAnswerChoices = 0;
  // looping in provided providedReportDataArray and formatting data
  for (let i = 0; i < providedReportDataArray.length; i++) {
    // looping in assigned questions
    for (let j = 0; j < providedReportDataArray[i].assigned_questions.length; j++) {
      for (let k = 0; k < providedReportDataArray[i].assigned_questions[j].length; k++) {
        // question coming from the Item Bank
        if (
          typeof providedReportDataArray[i].assigned_questions[j][k].item_bank_id !== "undefined"
        ) {
          // if answer_choices length is greater than the maximumAmountOfAnswerChoices
          if (
            providedReportDataArray[i].assigned_questions[j][k].answer_choices.filter(
              obj =>
                obj.language_id === LANGUAGE_IDS.french &&
                obj.option_type === ITEM_OPTION_TYPE.multipleChoice
            ).length > maximumAmountOfAnswerChoices
          ) {
            // setting new max value
            maximumAmountOfAnswerChoices = providedReportDataArray[i].assigned_questions[j][
              k
            ].answer_choices.filter(
              obj =>
                obj.language_id === LANGUAGE_IDS.french &&
                obj.option_type === ITEM_OPTION_TYPE.multipleChoice
            ).length;
          } else if (
            providedReportDataArray[i].assigned_questions[j][k].answer_choices.filter(
              obj =>
                obj.language_id === LANGUAGE_IDS.english &&
                obj.option_type === ITEM_OPTION_TYPE.multipleChoice
            ).length > maximumAmountOfAnswerChoices
          ) {
            // setting new max value
            maximumAmountOfAnswerChoices = providedReportDataArray[i].assigned_questions[j][
              k
            ].answer_choices.filter(
              obj =>
                obj.language_id === LANGUAGE_IDS.english &&
                obj.option_type === ITEM_OPTION_TYPE.multipleChoice
            ).length;
          }
          // question coming from the test builder
        } else {
          // if answer_choices length is greater than the maximumAmountOfAnswerChoices
          if (
            providedReportDataArray[i].assigned_questions[j][k].answer_choices.length >
            maximumAmountOfAnswerChoices
          ) {
            // setting new max value
            maximumAmountOfAnswerChoices =
              providedReportDataArray[i].assigned_questions[j][k].answer_choices.length;
          }
        }
      }
    }
  }

  // pushing columns definition
  reportData.push([
    LOCALIZE.reports.testTakerReport.userCode,
    LOCALIZE.reports.testTakerReport.username,
    LOCALIZE.reports.testTakerReport.firstname,
    LOCALIZE.reports.testTakerReport.lastname,
    LOCALIZE.reports.testTakerReport.pri,
    LOCALIZE.reports.testTakerReport.dateOfBirth,
    LOCALIZE.reports.testTakerReport.currentEmployer,
    LOCALIZE.reports.testTakerReport.org,
    LOCALIZE.reports.testTakerReport.group,
    LOCALIZE.reports.testTakerReport.level,
    LOCALIZE.reports.testTakerReport.residence,
    LOCALIZE.reports.testTakerReport.education,
    LOCALIZE.reports.testTakerReport.gender,
    LOCALIZE.reports.testTakerReport.identifyAsWoman,
    LOCALIZE.reports.testTakerReport.aboriginal,
    LOCALIZE.reports.testTakerReport.aboriginalInuit,
    LOCALIZE.reports.testTakerReport.aboriginalMetis,
    LOCALIZE.reports.testTakerReport.aboriginalNAIndianFirstNation,
    LOCALIZE.reports.testTakerReport.visibleMinority,
    LOCALIZE.reports.testTakerReport.visibleMinorityBlack,
    LOCALIZE.reports.testTakerReport.visibleMinorityChinese,
    LOCALIZE.reports.testTakerReport.visibleMinorityFilipino,
    LOCALIZE.reports.testTakerReport.visibleMinorityJapanese,
    LOCALIZE.reports.testTakerReport.visibleMinorityKorean,
    LOCALIZE.reports.testTakerReport.visibleMinorityNonWhiteLatinx,
    LOCALIZE.reports.testTakerReport.visibleMinorityNonWhiteMiddleEastern,
    LOCALIZE.reports.testTakerReport.visibleMinorityMixedOrigin,
    LOCALIZE.reports.testTakerReport.visibleMinoritySoutheastAsian,
    LOCALIZE.reports.testTakerReport.visibleMinoritySouthAsianEastIndian,
    LOCALIZE.reports.testTakerReport.visibleMinorityOther,
    LOCALIZE.reports.testTakerReport.visibleMinorityOtherDetails,
    LOCALIZE.reports.testTakerReport.disability,
    LOCALIZE.reports.testTakerReport.disabilityVisual,
    LOCALIZE.reports.testTakerReport.disabilityDexterity,
    LOCALIZE.reports.testTakerReport.disabilityHearing,
    LOCALIZE.reports.testTakerReport.disabilityMobility,
    LOCALIZE.reports.testTakerReport.disabilitySpeech,
    LOCALIZE.reports.testTakerReport.disabilityOther,
    LOCALIZE.reports.testTakerReport.disabilityOtherDetails,
    LOCALIZE.reports.testTakerReport.requestingDepartment,
    LOCALIZE.reports.testTakerReport.administrationMode,
    LOCALIZE.reports.testTakerReport.reasonForTesting,
    LOCALIZE.reports.testTakerReport.levelRequired,
    LOCALIZE.reports.testTakerReport.testNumber,
    LOCALIZE.reports.testTakerReport.testSectionLanguage,
    LOCALIZE.reports.testTakerReport.testForm,
    LOCALIZE.reports.testTakerReport.catVersion,
    LOCALIZE.reports.testTakerReport.testDate,
    LOCALIZE.reports.testTakerReport.testStartDate,
    LOCALIZE.reports.testTakerReport.submitTestDate,
    LOCALIZE.reports.testTakerReport.questionSectionNumber,
    LOCALIZE.reports.testTakerReport.rdItemID,
    LOCALIZE.reports.testTakerReport.pilotItem,
    LOCALIZE.reports.testTakerReport.itemOrderMaster,
    LOCALIZE.reports.testTakerReport.itemOrderPresentation,
    LOCALIZE.reports.testTakerReport.itemOrderViewed,
    LOCALIZE.reports.testTakerReport.questionTextFr,
    LOCALIZE.reports.testTakerReport.questionTextEn,
    LOCALIZE.reports.testTakerReport.questionTextHtmlFr,
    LOCALIZE.reports.testTakerReport.questionTextHtmlEn
  ]);

  // looping in maximumAmountOfAnswerChoices
  for (let i = 0; i < maximumAmountOfAnswerChoices; i++) {
    // pushing columns definition
    reportData[0].push(
      LOCALIZE.formatString(LOCALIZE.reports.testTakerReport.choiceXPpcAnswerId, i + 1),
      LOCALIZE.formatString(LOCALIZE.reports.testTakerReport.choiceXPoints, i + 1),
      LOCALIZE.formatString(LOCALIZE.reports.testTakerReport.choiceXPresentation, i + 1),
      LOCALIZE.formatString(LOCALIZE.reports.testTakerReport.choiceXTextFr, i + 1),
      LOCALIZE.formatString(LOCALIZE.reports.testTakerReport.choiceXTextEn, i + 1),
      LOCALIZE.formatString(LOCALIZE.reports.testTakerReport.choiceXTextHtmlFr, i + 1),
      LOCALIZE.formatString(LOCALIZE.reports.testTakerReport.choiceXTextHtmlEn, i + 1)
    );
  }

  // pushing columns definition
  reportData[0].push(
    LOCALIZE.reports.testTakerReport.languageOfAnswer,
    LOCALIZE.reports.testTakerReport.timeSpent,
    LOCALIZE.reports.testTakerReport.responseMasterOrder,
    LOCALIZE.reports.testTakerReport.responseChoiceId,
    LOCALIZE.reports.testTakerReport.scoringValue,
    LOCALIZE.reports.testTakerReport.sectionScore,
    LOCALIZE.reports.testTakerReport.totalScore,
    LOCALIZE.reports.testTakerReport.convertedScore,
    LOCALIZE.reports.testTakerReport.testStatus,
    LOCALIZE.reports.testTakerReport.testSubmissionMode,
    LOCALIZE.reports.testTakerReport.scoreValidity
  );

  // looping in provided providedReportDataArray and formatting data
  for (let i = 0; i < providedReportDataArray.length; i++) {
    // initializing variables
    let questionSection = 1;
    let initialTestSectionComponentId = 0;
    // making sure that the assigned_questions array contains at least one value to avoid index out of bounds exception
    if (providedReportDataArray[i].assigned_questions.length > 0) {
      if (providedReportDataArray[i].assigned_questions[0].length > 0) {
        initialTestSectionComponentId = parseInt(
          providedReportDataArray[i].assigned_questions[0][0].test_section_component
        );
      }

      // looping in assigned questions
      for (let j = 0; j < providedReportDataArray[i].assigned_questions.length; j++) {
        for (let k = 0; k < providedReportDataArray[i].assigned_questions[j].length; k++) {
          // getting language of question
          let questionSectionLanguage = getQuestionLanguage(
            providedReportDataArray[i].assigned_questions[j][k]
          );

          // initializing temp arrays
          let tempArray1 = [];
          const tempArray2 = [];
          let tempArray3 = [];
          // if question list section changes
          if (
            parseInt(providedReportDataArray[i].assigned_questions[j][k].test_section_component) >
            initialTestSectionComponentId
          ) {
            // reinitializing initialTestSectionComponentId
            initialTestSectionComponentId =
              providedReportDataArray[i].assigned_questions[j][k].test_section_component;
            // incrementing questionSection
            questionSection += 1;
            // getting language of current section
            if (
              providedReportDataArray[i].assigned_questions[j][k].section_language ===
              LANGUAGES.english
            ) {
              questionSectionLanguage = LOCALIZE.commons.english;
            } else if (
              providedReportDataArray[i].assigned_questions[j][k].section_language ===
              LANGUAGES.french
            ) {
              questionSectionLanguage = LOCALIZE.commons.french;
            } else {
              questionSectionLanguage = LOCALIZE.commons.bilingual;
            }
          }

          // creating report data
          tempArray1 = [
            getFormattedUserCode(providedReportDataArray[i].user_data[0].id),
            providedReportDataArray[i].user_data[0].email,
            providedReportDataArray[i].user_data[0].first_name,
            providedReportDataArray[i].user_data[0].last_name,
            providedReportDataArray[i].user_data[0].pri_or_military_nbr,
            providedReportDataArray[i].user_data[0].birth_date,
            getextendedProfileLabel(
              providedReportDataArray[i].extended_profile_data,
              EXTENEDED_PROFILE_DATA_TYPE.currentEmployer
            ),
            getextendedProfileLabel(
              providedReportDataArray[i].extended_profile_data,
              EXTENEDED_PROFILE_DATA_TYPE.organization
            ),
            getextendedProfileLabel(
              providedReportDataArray[i].extended_profile_data,
              EXTENEDED_PROFILE_DATA_TYPE.group
            ),
            getextendedProfileLabel(
              providedReportDataArray[i].extended_profile_data,
              EXTENEDED_PROFILE_DATA_TYPE.level
            ),
            getextendedProfileLabel(
              providedReportDataArray[i].extended_profile_data,
              EXTENEDED_PROFILE_DATA_TYPE.residence
            ),
            getextendedProfileLabel(
              providedReportDataArray[i].extended_profile_data,
              EXTENEDED_PROFILE_DATA_TYPE.education
            ),
            getextendedProfileLabel(
              providedReportDataArray[i].extended_profile_data,
              EXTENEDED_PROFILE_DATA_TYPE.gender
            ),
            getextendedEeInfoData(
              providedReportDataArray[i].extended_profile_data,
              EXTENEDED_PROFILE_DATA_TYPE.identifyAsWoman
            ),
            getextendedEeInfoData(
              providedReportDataArray[i].extended_profile_data,
              EXTENEDED_PROFILE_DATA_TYPE.aboriginal
            ),
            getextendedEeInfoData(
              providedReportDataArray[i].extended_profile_data,
              EXTENEDED_PROFILE_DATA_TYPE.aboriginalInuit
            ),
            getextendedEeInfoData(
              providedReportDataArray[i].extended_profile_data,
              EXTENEDED_PROFILE_DATA_TYPE.aboriginalMetis
            ),
            getextendedEeInfoData(
              providedReportDataArray[i].extended_profile_data,
              EXTENEDED_PROFILE_DATA_TYPE.aboriginalNAIndianFirstNation
            ),
            getextendedEeInfoData(
              providedReportDataArray[i].extended_profile_data,
              EXTENEDED_PROFILE_DATA_TYPE.visibleMinority
            ),
            getextendedEeInfoData(
              providedReportDataArray[i].extended_profile_data,
              EXTENEDED_PROFILE_DATA_TYPE.visibleMinorityBlack
            ),
            getextendedEeInfoData(
              providedReportDataArray[i].extended_profile_data,
              EXTENEDED_PROFILE_DATA_TYPE.visibleMinorityChinese
            ),
            getextendedEeInfoData(
              providedReportDataArray[i].extended_profile_data,
              EXTENEDED_PROFILE_DATA_TYPE.visibleMinorityFilipino
            ),
            getextendedEeInfoData(
              providedReportDataArray[i].extended_profile_data,
              EXTENEDED_PROFILE_DATA_TYPE.visibleMinorityJapanese
            ),
            getextendedEeInfoData(
              providedReportDataArray[i].extended_profile_data,
              EXTENEDED_PROFILE_DATA_TYPE.visibleMinorityKorean
            ),
            getextendedEeInfoData(
              providedReportDataArray[i].extended_profile_data,
              EXTENEDED_PROFILE_DATA_TYPE.visibleMinorityNonWhiteLatinx
            ),
            getextendedEeInfoData(
              providedReportDataArray[i].extended_profile_data,
              EXTENEDED_PROFILE_DATA_TYPE.visibleMinorityNonWhiteMiddleEastern
            ),
            getextendedEeInfoData(
              providedReportDataArray[i].extended_profile_data,
              EXTENEDED_PROFILE_DATA_TYPE.visibleMinorityMixedOrigin
            ),
            getextendedEeInfoData(
              providedReportDataArray[i].extended_profile_data,
              EXTENEDED_PROFILE_DATA_TYPE.visibleMinoritySouthAsianEastIndian
            ),
            getextendedEeInfoData(
              providedReportDataArray[i].extended_profile_data,
              EXTENEDED_PROFILE_DATA_TYPE.visibleMinoritySoutheastAsian
            ),
            getextendedEeInfoData(
              providedReportDataArray[i].extended_profile_data,
              EXTENEDED_PROFILE_DATA_TYPE.visibleMinorityOther
            ),
            getextendedEeInfoData(
              providedReportDataArray[i].extended_profile_data,
              EXTENEDED_PROFILE_DATA_TYPE.visibleMinorityOtherDetails
            ),
            getextendedEeInfoData(
              providedReportDataArray[i].extended_profile_data,
              EXTENEDED_PROFILE_DATA_TYPE.disability
            ),
            getextendedEeInfoData(
              providedReportDataArray[i].extended_profile_data,
              EXTENEDED_PROFILE_DATA_TYPE.disabilityVisual
            ),
            getextendedEeInfoData(
              providedReportDataArray[i].extended_profile_data,
              EXTENEDED_PROFILE_DATA_TYPE.disabilityDexterity
            ),
            getextendedEeInfoData(
              providedReportDataArray[i].extended_profile_data,
              EXTENEDED_PROFILE_DATA_TYPE.disabilityHearing
            ),
            getextendedEeInfoData(
              providedReportDataArray[i].extended_profile_data,
              EXTENEDED_PROFILE_DATA_TYPE.disabilityMobility
            ),
            getextendedEeInfoData(
              providedReportDataArray[i].extended_profile_data,
              EXTENEDED_PROFILE_DATA_TYPE.disabilitySpeech
            ),
            getextendedEeInfoData(
              providedReportDataArray[i].extended_profile_data,
              EXTENEDED_PROFILE_DATA_TYPE.disabilityOther
            ),
            getextendedEeInfoData(
              providedReportDataArray[i].extended_profile_data,
              EXTENEDED_PROFILE_DATA_TYPE.disabilityOtherDetails
            ),
            providedReportDataArray[i].financial_data !== null
              ? providedReportDataArray[i].financial_data.department_ministry_data[
                  LOCALIZE.reports.testTakerReport.requestingDepartmentLanguage
                ]
              : providedReportDataArray[i].orderless_financial_data !== null
              ? providedReportDataArray[i].orderless_financial_data.department_data[
                  LOCALIZE.reports.testTakerReport.requestingDepartmentLanguage
                ]
              : "",
            providedReportDataArray[i].uit_invite_id === null
              ? LOCALIZE.reports.testTakerReport.assisted
              : LOCALIZE.reports.testTakerReport.automated,
            providedReportDataArray[i].orderless_financial_data !== null
              ? providedReportDataArray[i].orderless_financial_data.reason_for_testing_data !== null
                ? providedReportDataArray[i].orderless_financial_data.reason_for_testing_data[
                    `description_${LOCALIZE.commons.currentLanguage}`
                  ]
                : ""
              : "",
            providedReportDataArray[i].orderless_financial_data !== null
              ? providedReportDataArray[i].orderless_financial_data.level_required !== null
                ? providedReportDataArray[i].orderless_financial_data.level_required
                : ""
              : "",
            `${providedReportDataArray[i].test_data[0].parent_code}${String.fromCharCode(8203)}`,
            questionSectionLanguage,
            `${providedReportDataArray[i].test_data[0].test_code}${String.fromCharCode(8203)}`,
            providedReportDataArray[i].test_data[0].version,
            providedReportDataArray[i].start_date.split(".")[0],
            providedReportDataArray[i].first_timed_section_start_time !== null
              ? providedReportDataArray[i].first_timed_section_start_time.split(".")[0]
              : LOCALIZE.commons.none,
            providedReportDataArray[i].submit_date !== null
              ? providedReportDataArray[i].submit_date.split(".")[0]
              : LOCALIZE.commons.none,
            questionSection,
            providedReportDataArray[i].assigned_questions[j][k].ppc_question_id !== ""
              ? providedReportDataArray[i].assigned_questions[j][k].ppc_question_id
              : "",
            providedReportDataArray[i].assigned_questions[j][k].pilot === true
              ? LOCALIZE.commons.yes
              : LOCALIZE.commons.no,
            providedReportDataArray[i].assigned_questions[j][k].order
              ? providedReportDataArray[i].assigned_questions[j][k].order
              : "",
            providedReportDataArray[i].assigned_questions[j][k].presentation_order
              ? providedReportDataArray[i].assigned_questions[j][k].presentation_order
              : "",
            providedReportDataArray[i].assigned_questions[j][k].viewed_order
              ? providedReportDataArray[i].assigned_questions[j][k].viewed_order
              : "",
            providedReportDataArray[i].assigned_questions[j][k].question_text_fr.length > 0
              ? getQuestionText(
                  providedReportDataArray[i].assigned_questions[j][k],
                  providedReportDataArray[i].assigned_questions[j][k].question_text_fr,
                  LANGUAGES.french,
                  false
                )
              : "",
            providedReportDataArray[i].assigned_questions[j][k].question_text_en.length > 0
              ? getQuestionText(
                  providedReportDataArray[i].assigned_questions[j][k],
                  providedReportDataArray[i].assigned_questions[j][k].question_text_en,
                  LANGUAGES.english,
                  false
                )
              : "",
            providedReportDataArray[i].assigned_questions[j][k].question_text_fr.length > 0
              ? getQuestionText(
                  providedReportDataArray[i].assigned_questions[j][k],
                  providedReportDataArray[i].assigned_questions[j][k].question_text_fr,
                  LANGUAGES.french,
                  true
                )
              : "",
            providedReportDataArray[i].assigned_questions[j][k].question_text_en.length > 0
              ? getQuestionText(
                  providedReportDataArray[i].assigned_questions[j][k],
                  providedReportDataArray[i].assigned_questions[j][k].question_text_en,
                  LANGUAGES.english,
                  true
                )
              : ""
          ];
          // looping in answer_choices
          for (let l = 0; l < maximumAmountOfAnswerChoices; l++) {
            tempArray2.push(
              providedReportDataArray[i].assigned_questions[j][k].answer_choices[l]
                ? providedReportDataArray[i].assigned_questions[j][k].answer_choices[l]
                    .ppc_answer_id !== ""
                  ? providedReportDataArray[i].assigned_questions[j][k].answer_choices[l]
                      .ppc_answer_id
                  : ""
                : "",
              providedReportDataArray[i].assigned_questions[j][k].answer_choices[l]
                ? providedReportDataArray[i].assigned_questions[j][k].answer_choices[l]
                    .scoring_value
                : "",
              getAnswerChoiceOrder(
                typeof providedReportDataArray[i].assigned_questions[j][k].item_bank_id !==
                  "undefined",
                providedReportDataArray[i].assigned_questions[j][k].answer_choices,
                l
              ),
              getAnswerChoiceText(
                typeof providedReportDataArray[i].assigned_questions[j][k].item_bank_id !==
                  "undefined",
                providedReportDataArray[i].assigned_questions[j][k].answer_choices,
                l,
                LANGUAGE_IDS.french,
                false
              ),
              getAnswerChoiceText(
                typeof providedReportDataArray[i].assigned_questions[j][k].item_bank_id !==
                  "undefined",
                providedReportDataArray[i].assigned_questions[j][k].answer_choices,
                l,
                LANGUAGE_IDS.english,
                false
              ),
              getAnswerChoiceText(
                typeof providedReportDataArray[i].assigned_questions[j][k].item_bank_id !==
                  "undefined",
                providedReportDataArray[i].assigned_questions[j][k].answer_choices,
                l,
                LANGUAGE_IDS.french,
                true
              ),
              getAnswerChoiceText(
                typeof providedReportDataArray[i].assigned_questions[j][k].item_bank_id !==
                  "undefined",
                providedReportDataArray[i].assigned_questions[j][k].answer_choices,
                l,
                LANGUAGE_IDS.english,
                true
              )
            );
          }
          tempArray3 = [
            providedReportDataArray[i].assigned_questions[j][k].candidate_answer_data !== null
              ? providedReportDataArray[i].assigned_questions[j][k].candidate_answer_data
                  .selected_language !== null
                ? getLanguage(
                    providedReportDataArray[i].assigned_questions[j][k].candidate_answer_data
                      .selected_language
                  )
                : LOCALIZE.commons.na
              : "",
            providedReportDataArray[i].assigned_questions[j][k].candidate_answer_data !== null
              ? providedReportDataArray[i].assigned_questions[j][k].candidate_answer_data.time_spent
                ? providedReportDataArray[i].assigned_questions[j][k].candidate_answer_data
                    .time_spent
                : ""
              : "",
            providedReportDataArray[i].assigned_questions[j][k].candidate_answer_data !== null
              ? Object.entries(
                  providedReportDataArray[i].assigned_questions[j][k].answer_choices.filter(
                    obj =>
                      obj.id ===
                      providedReportDataArray[i].assigned_questions[j][k].candidate_answer_data
                        .selected_answer_id
                  )
                ).length > 0
                ? providedReportDataArray[i].assigned_questions[j][k].answer_choices.filter(
                    obj =>
                      obj.id ===
                      providedReportDataArray[i].assigned_questions[j][k].candidate_answer_data
                        .selected_answer_id
                  )[0].order
                : ""
              : "",
            providedReportDataArray[i].assigned_questions[j][k].candidate_answer_data !== null
              ? providedReportDataArray[i].assigned_questions[j][k].candidate_answer_data
                  .selected_answer_id
              : "",
            providedReportDataArray[i].assigned_questions[j][k].candidate_answer_data !== null
              ? Object.entries(
                  providedReportDataArray[i].assigned_questions[j][k].answer_choices.filter(
                    obj =>
                      obj.id ===
                      providedReportDataArray[i].assigned_questions[j][k].candidate_answer_data
                        .selected_answer_id
                  )
                ).length > 0
                ? providedReportDataArray[i].assigned_questions[j][k].pilot
                  ? ""
                  : providedReportDataArray[i].assigned_questions[j][k].answer_choices.filter(
                      obj =>
                        obj.id ===
                        providedReportDataArray[i].assigned_questions[j][k].candidate_answer_data
                          .selected_answer_id
                    )[0].scoring_value
                : ""
              : "",
            providedReportDataArray[i].test_sections_score.length > 0
              ? providedReportDataArray[i].test_sections_score[questionSection - 1]
              : LOCALIZE.commons.na,
            providedReportDataArray[i].total_score !== null
              ? providedReportDataArray[i].total_score
              : "",
            getConvertedScore(
              // LOCALIZE.getLanguage() returns either "en" or "fr"
              providedReportDataArray[i][`${LOCALIZE.getLanguage()}_converted_score`],
              providedReportDataArray[i].status
            ),
            getFormattedStatus(providedReportDataArray[i].status),
            providedReportDataArray[i].test_sections_timed_out_data.length > 0
              ? providedReportDataArray[i].test_sections_timed_out_data[questionSection - 1] ===
                true
                ? LOCALIZE.reports.timedOut
                : getFormattedStatus(providedReportDataArray[i].status)
              : LOCALIZE.commons.na,
            providedReportDataArray[i].is_invalid === true
              ? LOCALIZE.commons.invalid
              : LOCALIZE.commons.valid
          ];

          reportData.push(tempArray1.concat(tempArray2).concat(tempArray3));
        }
      }
      // adding empty row to separate tests/users
      if (providedReportDataArray[i].assigned_questions.length > 0) {
        reportData.push([""]);
      }
    }
  }

  return reportData;
}

export default generateTestTakerReport;
