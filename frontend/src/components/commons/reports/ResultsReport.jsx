/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Component } from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import LOCALIZE from "../../../text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  getTestsBasedOnTestOrderNumber,
  setReportSelectedParameters,
  setGenerateReportDisabledState
} from "../../../modules/ReportsRedux";
import DropdownSelect from "../DropdownSelect";
import { Row, Col } from "react-bootstrap";
import { REPORT_REQUESTOR } from "./Constants";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 4,
    xl: 4
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 8,
    xl: 8
  }
};
const styles = {
  optionSelectionContainer: {
    padding: "12px 24px",
    margin: "0 auto",
    width: "80%"
  },
  labelContainer: {
    verticalAlign: "middle",
    paddingRight: 12
  },
  singleDropdownContainer: {
    verticalAlign: "middle",
    width: "100%"
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  loading: {
    width: "100%",
    minHeight: 60,
    textAlign: "center",
    fontSize: "32px",
    padding: 6
  }
};

class ResultsReport extends Component {
  static propTypes = {
    reportRequestor: PropTypes.string.isRequired,
    getTestOrderNumbers: PropTypes.func.isRequired,
    currentlyLoading: PropTypes.bool.isRequired
  };

  state = {
    isLoadingTestOrderNumber: true,
    testOrderNumberVisible: false,
    testOrderNumberOptions: [],
    testOrderNumberData: [],
    selectedTestOrderNumber: "",
    isLoadingTests: true,
    testVisible: false,
    testOptions: [],
    testData: [],
    selectedTest: "",
    selectedTestsAccessibility: LOCALIZE.commons.none
  };

  // setting all states to their initial values
  async getInitialState() {
    this.setState({
      isLoadingTestOrderNumber: true,
      testOrderNumberVisible: false,
      testOrderNumberOptions: [],
      testOrderNumberData: [],
      selectedTestOrderNumber: "",
      isLoadingTests: true,
      testVisible: false,
      testOptions: [],
      testData: [],
      selectedTest: ""
    });
  }

  componentDidMount = () => {
    this.populateTestOrderNumberOptions();
  };

  // populating test order number options
  populateTestOrderNumberOptions = () => {
    this.setState(
      {
        testOrderNumberVisible: true,
        testVisible: false,
        isLoadingTestOrderNumber: true
      },
      () => {
        this.props
          .getTestOrderNumbers()
          .then(response => {
            this.setState({ testOrderNumberData: response });
          })
          .then(() => {
            const testOrderNumberOptions = [];
            for (let i = 0; i < this.state.testOrderNumberData.length; i++) {
              // requested by TA
              if (this.props.reportRequestor === REPORT_REQUESTOR.ta) {
                testOrderNumberOptions.push({
                  // adding " - <ta_username>", so TAs that used the same reference number are not all selected in the dropdown
                  value: `${
                    this.state.testOrderNumberData[i].test_order_number !== null
                      ? this.state.testOrderNumberData[i].test_order_number
                      : this.state.testOrderNumberData[i].reference_number
                  } - ${
                    this.state.testOrderNumberData[i].test_order_number !== null
                      ? this.state.testOrderNumberData[i].staffing_process_number
                      : LOCALIZE.reports.orderlessRequest
                  }`,
                  label: `${
                    this.state.testOrderNumberData[i].test_order_number !== null
                      ? this.state.testOrderNumberData[i].test_order_number
                      : this.state.testOrderNumberData[i].reference_number
                  } (${
                    this.state.testOrderNumberData[i].test_order_number !== null
                      ? this.state.testOrderNumberData[i].staffing_process_number
                      : LOCALIZE.reports.orderlessRequest
                  })`,
                  orderless: this.state.testOrderNumberData[i].test_order_number === null,
                  ta_username: this.state.testOrderNumberData[i].ta_username
                });
              } else {
                testOrderNumberOptions.push({
                  // adding " - <ta_username>", so TAs that used the same reference number are not all selected in the dropdown
                  value: `${
                    this.state.testOrderNumberData[i].test_order_number !== null
                      ? this.state.testOrderNumberData[i].test_order_number
                      : this.state.testOrderNumberData[i].reference_number
                  } - ${
                    this.state.testOrderNumberData[i].test_order_number !== null
                      ? this.state.testOrderNumberData[i].staffing_process_number
                      : this.state.testOrderNumberData[i].ta_username
                  }`,
                  label: `${
                    this.state.testOrderNumberData[i].test_order_number !== null
                      ? this.state.testOrderNumberData[i].test_order_number
                      : this.state.testOrderNumberData[i].reference_number
                  } (${
                    this.state.testOrderNumberData[i].test_order_number !== null
                      ? this.state.testOrderNumberData[i].staffing_process_number
                      : this.state.testOrderNumberData[i].ta_email
                  })`,
                  orderless: this.state.testOrderNumberData[i].test_order_number === null,
                  ta_username: this.state.testOrderNumberData[i].ta_username
                });
              }
            }
            this.getInitialState().then(() => {
              // disabling generate report button
              this.props.setGenerateReportDisabledState(true);
              this.setState({
                testOrderNumberOptions: testOrderNumberOptions,
                testOrderNumberVisible: true,
                isLoadingTestOrderNumber: false
              });
            });
          });
      }
    );
  };

  // get selected test order number
  getSelectedTestOrderNumber = selectedOption => {
    this.setState(
      {
        testVisible: true,
        isLoadingTests: true
      },
      () => {
        this.props
          .getTestsBasedOnTestOrderNumber(
            // splitting with the " - " since we've added that in the value option to avoid multi selection in the dropdown
            selectedOption.value.split(" - ")[0],
            selectedOption.orderless,
            this.props.reportRequestor === REPORT_REQUESTOR.ta,
            this.props.reportRequestor !== REPORT_REQUESTOR.ta ? selectedOption.ta_username : null
          )
          .then(response => {
            this.setState({ testData: response });
          })
          .then(() => {
            const testOptions = [];
            for (let i = 0; i < this.state.testData.length; i++) {
              testOptions.push({
                value: this.state.testData[i].test_id,
                label: `${this.state.testData[i][`${this.props.currentLanguage}_test_name`]} (v${
                  this.state.testData[i].test_version
                })`,
                orderless: selectedOption.orderless
              });
            }
            // disabling generate report button
            this.props.setGenerateReportDisabledState(true);
            this.setState({
              selectedTestOrderNumber: selectedOption,
              selectedTest: "",
              selectedCandidate: null,
              testOptions: testOptions,
              testVisible: true,
              isLoadingTests: false
            });
          });
      }
    );
  };

  // get selected test
  getSelectedTest = selectedOption => {
    // selectedOption will be null if the user remove the last selected option
    if (selectedOption != null) {
      // length will be 0 if the user hit "clear all" options
      if (selectedOption.length > 0) {
        let selectedTestsAccessibility = "";
        for (let i = 0; i < selectedOption.length; i++) {
          selectedTestsAccessibility += `${selectedOption[i].label},`;
        }
        this.setState(
          {
            selectedTest: selectedOption,
            selectedTestsAccessibility: selectedTestsAccessibility
          },
          () => {
            // updating all needed redux states
            this.props.setReportSelectedParameters(
              // checking first element of the selected options (table) to see if that is an orderless request
              selectedOption[0].orderless,
              {
                value: this.state.selectedTestOrderNumber.value.split(" - ")[0],
                label: this.state.selectedTestOrderNumber.label
              },
              this.state.selectedTest,
              null,
              null,
              null,
              null,
              null,
              null,
              null
            );
            // enabling generate report button
            this.props.setGenerateReportDisabledState(false);
          }
        );
      } else {
        this.setState({
          selectedTest: ""
        });
        // disabling generate report button
        this.props.setGenerateReportDisabledState(true);
      }
    } else {
      this.setState({
        selectedTest: ""
      });
      // disabling generate report button
      this.props.setGenerateReportDisabledState(true);
    }
  };

  render() {
    const {
      isLoadingTestOrderNumber,
      testOrderNumberOptions,
      testOrderNumberVisible,
      selectedTestOrderNumber,
      testOptions,
      isLoadingTests,
      testVisible,
      selectedTest
    } = this.state;
    return (
      <div>
        {testOrderNumberVisible && (
          <Row
            className="align-items-center justify-content-start"
            id="unit-test-test-order-number-dropdown"
            style={styles.optionSelectionContainer}
            role="presentation"
          >
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
              style={styles.labelContainer}
            >
              <label id="test-order-number-label">{LOCALIZE.reports.testOrderNumberLabel}</label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={styles.singleDropdownContainer}
            >
              {isLoadingTestOrderNumber ? (
                <label className="fa fa-spinner fa-spin" style={styles.loading}>
                  <FontAwesomeIcon icon={faSpinner} />
                </label>
              ) : (
                <DropdownSelect
                  idPrefix="results-report-test-order-number-dropdown"
                  ariaLabelledBy="test-order-number-label"
                  isDisabled={this.props.currentlyLoading}
                  options={testOrderNumberOptions}
                  onChange={this.getSelectedTestOrderNumber}
                  defaultValue={selectedTestOrderNumber}
                  hasPlaceholder={true}
                ></DropdownSelect>
              )}
            </Col>
          </Row>
        )}
        {testVisible && (
          <Row
            className="align-items-center justify-content-start"
            id="unit-test-test-dropdown"
            style={styles.optionSelectionContainer}
            role="presentation"
          >
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
              style={styles.labelContainer}
            >
              <label id="test-label">{LOCALIZE.reports.testsLabel}</label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={styles.singleDropdownContainer}
            >
              {isLoadingTests ? (
                <label className="fa fa-spinner fa-spin" style={styles.loading}>
                  <FontAwesomeIcon icon={faSpinner} />
                </label>
              ) : (
                <DropdownSelect
                  idPrefix="results-report-test-dropdown"
                  ariaLabelledBy="test-label tests-current-value-accessibility"
                  isDisabled={this.props.currentlyLoading}
                  hasPlaceholder={true}
                  isMulti={true}
                  options={testOptions}
                  onChange={this.getSelectedTest}
                  defaultValue={selectedTest}
                ></DropdownSelect>
              )}
            </Col>
            <label
              id="tests-current-value-accessibility"
              style={styles.hiddenText}
            >{`${LOCALIZE.reports.testLabelAccessibility} ${this.state.selectedTestsAccessibility}`}</label>
          </Row>
        )}
      </div>
    );
  }
}

export { ResultsReport as unconnectedResultsReport };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getTestsBasedOnTestOrderNumber,
      setReportSelectedParameters,
      setGenerateReportDisabledState
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ResultsReport));
