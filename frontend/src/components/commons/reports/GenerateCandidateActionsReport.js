import LOCALIZE from "../../../text_resources";
import getFormattedStatus from "./utils";

function generateCandidateActionsReport(providedReportDataArray) {
  // initializing reportData
  const reportData = [];

  // pushing columns definition
  reportData.push([
    LOCALIZE.reports.candidateActionsReport.origin,
    LOCALIZE.reports.candidateActionsReport.historyDate,
    LOCALIZE.reports.candidateActionsReport.historyType,
    LOCALIZE.reports.candidateActionsReport.status,
    LOCALIZE.reports.candidateActionsReport.previousStatus,
    LOCALIZE.reports.candidateActionsReport.startDate,
    LOCALIZE.reports.candidateActionsReport.submitDate,
    LOCALIZE.reports.candidateActionsReport.testAccessCode,
    LOCALIZE.reports.candidateActionsReport.totalScore,
    LOCALIZE.reports.candidateActionsReport.testAdministrator,
    LOCALIZE.reports.candidateActionsReport.testSessionLanguage,
    LOCALIZE.reports.candidateActionsReport.enConvertedScore,
    LOCALIZE.reports.candidateActionsReport.frConvertedScore,
    LOCALIZE.reports.candidateActionsReport.uitInviteId,
    LOCALIZE.reports.candidateActionsReport.isInvalid,
    LOCALIZE.reports.candidateActionsReport.timeType,
    LOCALIZE.reports.candidateActionsReport.assignedTestSectionId,
    LOCALIZE.reports.candidateActionsReport.candidateAnswerId,
    LOCALIZE.reports.candidateActionsReport.markForReview,
    LOCALIZE.reports.candidateActionsReport.questionId,
    LOCALIZE.reports.candidateActionsReport.selectedLanguage,
    LOCALIZE.reports.candidateActionsReport.answerId,
    LOCALIZE.reports.candidateActionsReport.candidateAnswerIdRef
  ]);

  // looping in provided providedReportDataArray and formatting data
  for (let i = 0; i < providedReportDataArray.length; i++) {
    // creating report data
    reportData.push([
      providedReportDataArray[i].origin,
      providedReportDataArray[i].history_date,
      providedReportDataArray[i].history_type,
      getFormattedStatus(providedReportDataArray[i].status) === LOCALIZE.commons.na
        ? ""
        : getFormattedStatus(providedReportDataArray[i].status),
      getFormattedStatus(providedReportDataArray[i].previous_status) === LOCALIZE.commons.na
        ? ""
        : getFormattedStatus(providedReportDataArray[i].previous_status),
      providedReportDataArray[i].start_date,
      providedReportDataArray[i].submit_date,
      providedReportDataArray[i].test_access_code,
      providedReportDataArray[i].total_score,
      providedReportDataArray[i].ta_id,
      providedReportDataArray[i].test_session_language_id === 1
        ? LOCALIZE.commons.english
        : LOCALIZE.commons.french,
      providedReportDataArray[i].en_converted_score,
      providedReportDataArray[i].fr_converted_score,
      providedReportDataArray[i].uit_invite_id,
      providedReportDataArray[i].is_invalid,
      providedReportDataArray[i].time_type,
      providedReportDataArray[i].assigned_test_section_id,
      providedReportDataArray[i].candidate_answer_id,
      providedReportDataArray[i].mark_for_review,
      providedReportDataArray[i].question_id,
      providedReportDataArray[i].selected_language_id !== ""
        ? providedReportDataArray[i].selected_language_id === 1
          ? LOCALIZE.commons.english
          : LOCALIZE.commons.french
        : "",
      providedReportDataArray[i].answer_id,
      providedReportDataArray[i].candidate_answer_id_ref
    ]);
  }

  return reportData;
}

export default generateCandidateActionsReport;
