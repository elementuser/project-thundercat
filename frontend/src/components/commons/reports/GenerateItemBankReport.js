import LOCALIZE from "../../../text_resources";
import { ITEM_CONTENT_TYPE, ITEM_OPTION_TYPE } from "../../testBuilder/itemBank/items/Constants";
import { removeHtmlFromString, removeSpecificLineChars } from "./utils";

function getItemRegularStemLabels(maxRegularStems) {
  const labels = [];

  for (let i = 0; i < maxRegularStems; i++) {
    labels.push(LOCALIZE.formatString(LOCALIZE.reports.itemBankReport.stemId, i + 1));
    labels.push(LOCALIZE.formatString(LOCALIZE.reports.itemBankReport.stemTextEn, i + 1));
    labels.push(LOCALIZE.formatString(LOCALIZE.reports.itemBankReport.stemTextHtmlEn, i + 1));
    labels.push(LOCALIZE.formatString(LOCALIZE.reports.itemBankReport.stemTextFr, i + 1));
    labels.push(LOCALIZE.formatString(LOCALIZE.reports.itemBankReport.stemTextHtmlFr, i + 1));
  }

  return labels;
}

function getItemScreenReaderStemLabels(maxScreenReaderStems) {
  const labels = [];

  for (let i = 0; i < maxScreenReaderStems; i++) {
    labels.push(LOCALIZE.formatString(LOCALIZE.reports.itemBankReport.stemScreenReaderId, i + 1));
    labels.push(
      LOCALIZE.formatString(LOCALIZE.reports.itemBankReport.stemScreenReaderTextEn, i + 1)
    );
    labels.push(
      LOCALIZE.formatString(LOCALIZE.reports.itemBankReport.stemScreenReaderTextHtmlEn, i + 1)
    );
    labels.push(
      LOCALIZE.formatString(LOCALIZE.reports.itemBankReport.stemScreenReaderTextFr, i + 1)
    );
    labels.push(
      LOCALIZE.formatString(LOCALIZE.reports.itemBankReport.stemScreenReaderTextHtmlFr, i + 1)
    );
  }
  return labels;
}

function getItemRegularOptionsLabels(maxRegularOptions) {
  const labels = [];

  for (let i = 0; i < maxRegularOptions; i++) {
    labels.push(LOCALIZE.formatString(LOCALIZE.reports.itemBankReport.optionHistoricalId, i + 1));
    labels.push(LOCALIZE.formatString(LOCALIZE.reports.itemBankReport.optionTextEn, i + 1));
    labels.push(LOCALIZE.formatString(LOCALIZE.reports.itemBankReport.optionTextHtmlEn, i + 1));
    labels.push(LOCALIZE.formatString(LOCALIZE.reports.itemBankReport.optionTextFr, i + 1));
    labels.push(LOCALIZE.formatString(LOCALIZE.reports.itemBankReport.optionTextHtmlFr, i + 1));
    labels.push(LOCALIZE.formatString(LOCALIZE.reports.itemBankReport.optionScore, i + 1));
  }

  return labels;
}

function getItemScreenReaderOptionsLabels(maxScreenReaderOptions) {
  const labels = [];

  for (let i = 0; i < maxScreenReaderOptions; i++) {
    labels.push(
      LOCALIZE.formatString(LOCALIZE.reports.itemBankReport.optionScreenReaderHistoricalId, i + 1)
    );
    labels.push(
      LOCALIZE.formatString(LOCALIZE.reports.itemBankReport.optionScreenReaderTextEn, i + 1)
    );
    labels.push(
      LOCALIZE.formatString(LOCALIZE.reports.itemBankReport.optionScreenReaderTextHtmlEn, i + 1)
    );
    labels.push(
      LOCALIZE.formatString(LOCALIZE.reports.itemBankReport.optionScreenReaderTextFr, i + 1)
    );
    labels.push(
      LOCALIZE.formatString(LOCALIZE.reports.itemBankReport.optionScreenReaderTextHtmlFr, i + 1)
    );
    labels.push(
      LOCALIZE.formatString(LOCALIZE.reports.itemBankReport.optionScreenReaderScore, i + 1)
    );
  }

  return labels;
}

function getItemAttributeValueLabels(providedReportDataArray, numberOfItemAttributes) {
  const labels = [];

  for (let i = 0; i < numberOfItemAttributes; i++) {
    labels.push(
      LOCALIZE.formatString(
        providedReportDataArray[0].item_attribute_values[i][
          `attribute_text_${LOCALIZE.reports.itemBankReport.selectedLanguage}`
        ]
      )
    );
  }

  return labels;
}

// this function is not future proof at all
// this is basically a temporary function
function getScoringKeyData(itemOptions, languageData) {
  const scoringKey = [];
  const scoringKeyId = [];

  // looping in all available languages
  for (let j = 0; j < languageData.length; j++) {
    if (
      typeof itemOptions[languageData[j].ISO_Code_1] !== "undefined" &&
      itemOptions[languageData[j].ISO_Code_1].filter(obj => {
        return obj.option_type === ITEM_OPTION_TYPE.multipleChoice;
      }).length > 0
    ) {
      // looping in item options (of type multiple choice)
      for (
        let i = 0;
        i <
        itemOptions[languageData[j].ISO_Code_1].filter(obj => {
          return obj.option_type === ITEM_OPTION_TYPE.multipleChoice;
        }).length;
        i++
      ) {
        // populating scoringKey array if score is greater than 0
        if (parseInt(itemOptions[languageData[j].ISO_Code_1][i].score) > 0) {
          if (!scoringKey.includes(i + 1)) {
            scoringKey.push(i + 1);
            scoringKeyId.push(
              itemOptions[languageData[j].ISO_Code_1][i].historical_option_id !== ""
                ? itemOptions[languageData[j].ISO_Code_1][i].historical_option_id
                : LOCALIZE.commons.na
            );
          }
        }
      }
    }
  }

  return [`"${scoringKey}"`, `"${scoringKeyId}"`];
}

function getItemRegularStems(itemContent, maxRegularStems, languageData) {
  // defining number of columns per stem
  const numberOfParametersPerLanguage = 2;
  // number of languages * number of parameters per language + 1 (item content ID)
  const numberOfColumnsPerStem = languageData.length * numberOfParametersPerLanguage + 1;

  // initializing itemStems
  const itemStems = [];

  // looping in maxRegularStems
  for (let i = 0; i < maxRegularStems; i++) {
    // ==================== ITEM CONTENT ID ====================
    let itemContentIdSet = false;
    // looping in all available languages
    for (let j = 0; j < languageData.length; j++) {
      if (
        typeof itemContent[languageData[j].ISO_Code_1] !== "undefined" &&
        itemContent[languageData[j].ISO_Code_1].filter(obj => {
          return obj.content_type === ITEM_CONTENT_TYPE.markdown;
        }).length > 0 &&
        typeof itemContent[languageData[j].ISO_Code_1].filter(obj => {
          return obj.content_type === ITEM_CONTENT_TYPE.markdown;
        })[i] !== "undefined"
      ) {
        itemStems.push(
          itemContent[languageData[j].ISO_Code_1].filter(obj => {
            return obj.content_type === ITEM_CONTENT_TYPE.markdown;
          })[i].item_content_id
        );
        itemContentIdSet = true;
        break;
      }
    }
    // ==================== ITEM CONTENT ID (END) ====================
    // no item content ID set (no stems for this current iteration)
    if (!itemContentIdSet) {
      // pushing empty string to array
      for (let k = 0; k < numberOfColumnsPerStem; k++) {
        itemStems.push("");
      }
    } else {
      // ==================== ITEM CONTENT DATA ====================
      // looping in all available languages
      for (let j = 0; j < languageData.length; j++) {
        if (
          typeof itemContent[languageData[j].ISO_Code_1] !== "undefined" &&
          itemContent[languageData[j].ISO_Code_1].filter(obj => {
            return obj.content_type === ITEM_CONTENT_TYPE.markdown;
          }).length > 0 &&
          typeof itemContent[languageData[j].ISO_Code_1].filter(obj => {
            return obj.content_type === ITEM_CONTENT_TYPE.markdown;
          })[i] !== "undefined"
        ) {
          // replacing all '"' with '""'
          const transformedString = itemContent[languageData[j].ISO_Code_1]
            .filter(obj => {
              return obj.content_type === ITEM_CONTENT_TYPE.markdown;
            })
            [i].text.replaceAll(`"`, `""`);
          itemStems.push(
            `"${removeHtmlFromString(transformedString)}"`,
            `"${removeSpecificLineChars(transformedString)}"`
          );
        } else {
          // pushing empty string to array
          for (let k = 0; k < numberOfParametersPerLanguage; k++) {
            itemStems.push("");
          }
        }
      }
      // ==================== ITEM CONTENT DATA (END) ====================
    }
  }

  return itemStems;
}

function getItemScreenReaderStems(itemContent, maxScreenReaderStems, languageData) {
  // defining number of columns per stem
  const numberOfParametersPerLanguage = 2;
  // number of languages * number of parameters per language + 1 (item content ID)
  const numberOfColumnsPerStem = languageData.length * numberOfParametersPerLanguage + 1;

  // initializing itemStems
  const itemStems = [];

  // looping in maxScreenReaderStems
  for (let i = 0; i < maxScreenReaderStems; i++) {
    // ==================== ITEM CONTENT SCREEN READER ID ====================
    let itemContentIdSet = false;
    // looping in all available languages
    for (let j = 0; j < languageData.length; j++) {
      if (
        typeof itemContent[languageData[j].ISO_Code_1] !== "undefined" &&
        itemContent[languageData[j].ISO_Code_1].filter(obj => {
          return obj.content_type === ITEM_CONTENT_TYPE.screenReader;
        }).length > 0 &&
        typeof itemContent[languageData[j].ISO_Code_1].filter(obj => {
          return obj.content_type === ITEM_CONTENT_TYPE.screenReader;
        })[i] !== "undefined"
      ) {
        itemStems.push(
          itemContent[languageData[j].ISO_Code_1].filter(obj => {
            return obj.content_type === ITEM_CONTENT_TYPE.screenReader;
          })[i].item_content_id
        );
        itemContentIdSet = true;
        break;
      }
    }
    // ==================== ITEM CONTENT SCREEN READER ID (END) ====================
    // no item content ID set (no stems for this current iteration)
    if (!itemContentIdSet) {
      // pushing empty string to array
      for (let k = 0; k < numberOfColumnsPerStem; k++) {
        itemStems.push("");
      }
    } else {
      // ==================== ITEM CONTENT SCREEN READER DATA ====================
      // looping in all available languages
      for (let j = 0; j < languageData.length; j++) {
        if (
          typeof itemContent[languageData[j].ISO_Code_1] !== "undefined" &&
          itemContent[languageData[j].ISO_Code_1].filter(obj => {
            return obj.content_type === ITEM_CONTENT_TYPE.screenReader;
          }).length > 0 &&
          typeof itemContent[languageData[j].ISO_Code_1].filter(obj => {
            return obj.content_type === ITEM_CONTENT_TYPE.screenReader;
          })[i] !== "undefined"
        ) {
          // replacing all '"' with '""'
          const transformedString = itemContent[languageData[j].ISO_Code_1]
            .filter(obj => {
              return obj.content_type === ITEM_CONTENT_TYPE.screenReader;
            })
            [i].text.replaceAll(`"`, `""`);
          itemStems.push(
            `"${removeHtmlFromString(transformedString)}"`,
            `"${removeSpecificLineChars(transformedString)}"`
          );
        } else {
          // pushing empty string to array
          for (let k = 0; k < numberOfParametersPerLanguage; k++) {
            itemStems.push("");
          }
        }
      }
      // ==================== ITEM CONTENT SCREEN READER DATA (END) ====================
    }
  }

  return itemStems;
}

function getItemRegularOptions(itemOptions, maxRegularOptions, languageData) {
  // defining number of columns per stem
  const numberOfParametersPerLanguage = 2;
  // number of languages * number of parameters per language + 2 (item historical ID & score)
  const numberOfColumnsPerOption = languageData.length * numberOfParametersPerLanguage + 2;

  // initializing itemOptionsArray
  const itemOptionsArray = [];

  // looping in maxRegularOptions
  for (let i = 0; i < maxRegularOptions; i++) {
    // initializing itemOptionScore
    let itemOptionScore = "";
    // ==================== ITEM OPTION ID ====================
    let itemOptionIdSet = false;
    // looping in all available languages
    for (let j = 0; j < languageData.length; j++) {
      if (
        typeof itemOptions[languageData[j].ISO_Code_1] !== "undefined" &&
        itemOptions[languageData[j].ISO_Code_1].filter(obj => {
          return obj.option_type === ITEM_OPTION_TYPE.multipleChoice;
        }).length > 0 &&
        typeof itemOptions[languageData[j].ISO_Code_1].filter(obj => {
          return obj.option_type === ITEM_OPTION_TYPE.multipleChoice;
        })[i] !== "undefined"
      ) {
        itemOptionsArray.push(
          itemOptions[languageData[j].ISO_Code_1].filter(obj => {
            return obj.option_type === ITEM_OPTION_TYPE.multipleChoice;
          })[i].historical_option_id
        );
        itemOptionIdSet = true;
        break;
      }
    }
    // ==================== ITEM OPTION ID (END) ====================
    // no item option ID set (no options for this current iteration)
    if (!itemOptionIdSet) {
      // pushing empty string to array
      for (let k = 0; k < numberOfColumnsPerOption; k++) {
        itemOptionsArray.push("");
      }
    } else {
      // ==================== ITEM OPTION DATA ====================
      // looping in all available languages
      for (let j = 0; j < languageData.length; j++) {
        if (
          typeof itemOptions[languageData[j].ISO_Code_1] !== "undefined" &&
          itemOptions[languageData[j].ISO_Code_1].filter(obj => {
            return obj.option_type === ITEM_OPTION_TYPE.multipleChoice;
          }).length > 0 &&
          typeof itemOptions[languageData[j].ISO_Code_1].filter(obj => {
            return obj.option_type === ITEM_OPTION_TYPE.multipleChoice;
          })[i] !== "undefined"
        ) {
          // replacing all '"' with '""'
          const transformedString = itemOptions[languageData[j].ISO_Code_1]
            .filter(obj => {
              return obj.option_type === ITEM_OPTION_TYPE.multipleChoice;
            })
            [i].text.replaceAll(`"`, `""`);
          itemOptionsArray.push(
            `"${removeHtmlFromString(transformedString)}"`,
            `"${removeSpecificLineChars(transformedString)}"`
          );
          // updating item option score
          itemOptionScore = itemOptions[languageData[j].ISO_Code_1].filter(obj => {
            return obj.option_type === ITEM_OPTION_TYPE.multipleChoice;
          })[i].score;
        } else {
          // pushing empty string to array
          for (let k = 0; k < numberOfParametersPerLanguage; k++) {
            itemOptionsArray.push("");
          }
        }
      }
      // adding item option score to array
      itemOptionsArray.push(itemOptionScore);
      // ==================== ITEM OPTION DATA (END) ====================
    }
  }

  return itemOptionsArray;
}

function getItemScreenReaderOptions(itemOptions, maxScreenReaderOptions, languageData) {
  // defining number of columns per stem
  const numberOfParametersPerLanguage = 2;
  // number of languages X number of parameters per language + 2 (item historical ID & score)
  const numberOfColumnsPerOption = languageData.length * numberOfParametersPerLanguage + 2;

  // initializing itemOptionsArray
  const itemOptionsArray = [];

  // looping in maxScreenReaderOptions
  for (let i = 0; i < maxScreenReaderOptions; i++) {
    // initializing itemOptionScore
    let itemOptionScore = "";
    // ==================== ITEM OPTION ID ====================
    let itemOptionIdSet = false;
    // looping in all available languages
    for (let j = 0; j < languageData.length; j++) {
      if (
        typeof itemOptions[languageData[j].ISO_Code_1] !== "undefined" &&
        itemOptions[languageData[j].ISO_Code_1].filter(obj => {
          return obj.option_type === ITEM_OPTION_TYPE.multipleChoiceScreenReader;
        }).length > 0 &&
        typeof itemOptions[languageData[j].ISO_Code_1].filter(obj => {
          return obj.option_type === ITEM_OPTION_TYPE.multipleChoiceScreenReader;
        })[i] !== "undefined"
      ) {
        itemOptionsArray.push(
          itemOptions[languageData[j].ISO_Code_1].filter(obj => {
            return obj.option_type === ITEM_OPTION_TYPE.multipleChoiceScreenReader;
          })[i].historical_option_id
        );
        itemOptionIdSet = true;
        break;
      }
    }
    // ==================== ITEM OPTION ID (END) ====================
    // no item option ID set (no options for this current iteration)
    if (!itemOptionIdSet) {
      // pushing empty string to array
      for (let k = 0; k < numberOfColumnsPerOption; k++) {
        itemOptionsArray.push("");
      }
    } else {
      // ==================== ITEM OPTION DATA ====================
      // looping in all available languages
      for (let j = 0; j < languageData.length; j++) {
        if (
          typeof itemOptions[languageData[j].ISO_Code_1] !== "undefined" &&
          itemOptions[languageData[j].ISO_Code_1].filter(obj => {
            return obj.option_type === ITEM_OPTION_TYPE.multipleChoiceScreenReader;
          }).length > 0 &&
          typeof itemOptions[languageData[j].ISO_Code_1].filter(obj => {
            return obj.option_type === ITEM_OPTION_TYPE.multipleChoiceScreenReader;
          })[i] !== "undefined"
        ) {
          // replacing all '"' with '""'
          const transformedString = itemOptions[languageData[j].ISO_Code_1]
            .filter(obj => {
              return obj.option_type === ITEM_OPTION_TYPE.multipleChoiceScreenReader;
            })
            [i].text.replaceAll(`"`, `""`);
          itemOptionsArray.push(
            `"${removeHtmlFromString(transformedString)}"`,
            `"${removeSpecificLineChars(transformedString)}"`
          );
          // updating item option score
          itemOptionScore = itemOptions[languageData[j].ISO_Code_1].filter(obj => {
            return obj.option_type === ITEM_OPTION_TYPE.multipleChoiceScreenReader;
          })[i].score;
        } else {
          // pushing empty string to array
          for (let k = 0; k < numberOfParametersPerLanguage; k++) {
            itemOptionsArray.push("");
          }
        }
      }
      // adding item option score to array
      itemOptionsArray.push(itemOptionScore);
      // ==================== ITEM OPTION DATA (END) ====================
    }
  }

  return itemOptionsArray;
}

function getItemAttributeValues(itemAttributeValues) {
  // initializing itemAttributeValuesArray
  const itemAttributeValuesArray = [];
  // looping in itemAttributeValues
  for (let i = 0; i < itemAttributeValues.length; i++) {
    // populating itemAttributeValuesArray
    itemAttributeValuesArray.push(itemAttributeValues[i].text);
  }

  return itemAttributeValuesArray;
}

function generateItemBankReport(providedReportDataArray, languageData) {
  // ==================== MAX NUMBER OF STEMS/OPTIONS ====================
  let maxRegularStems = 0;
  let maxScreenReaderStems = 0;
  let maxRegularOptions = 0;
  let maxScreenReaderOptions = 0;
  const numberOfItemAttributes =
    providedReportDataArray.length > 0
      ? providedReportDataArray[0].item_attribute_values.length
      : 0;

  for (let i = 0; i < providedReportDataArray.length; i++) {
    for (let j = 0; j < languageData.length; j++) {
      // ==================== ITEM CONTENT ====================
      if (
        typeof providedReportDataArray[i].item_content[languageData[j].ISO_Code_1] !==
          "undefined" &&
        providedReportDataArray[i].item_content[languageData[j].ISO_Code_1].length > 0
      ) {
        // regular stems
        if (
          providedReportDataArray[i].item_content[languageData[j].ISO_Code_1].filter(obj => {
            return obj.content_type === ITEM_CONTENT_TYPE.markdown;
          }).length > maxRegularStems
        ) {
          maxRegularStems = providedReportDataArray[i].item_content[
            languageData[j].ISO_Code_1
          ].filter(obj => {
            return obj.content_type === ITEM_CONTENT_TYPE.markdown;
          }).length;
        }

        // screen reader stems
        if (
          providedReportDataArray[i].item_content[languageData[j].ISO_Code_1].filter(obj => {
            return obj.content_type === ITEM_CONTENT_TYPE.screenReader;
          }).length > maxScreenReaderStems
        ) {
          maxScreenReaderStems = providedReportDataArray[i].item_content[
            languageData[j].ISO_Code_1
          ].filter(obj => {
            return obj.content_type === ITEM_CONTENT_TYPE.screenReader;
          }).length;
        }
      }
      // ==================== ITEM CONTENT (END) ====================

      // ==================== ITEM OPTIONS ====================
      if (
        typeof providedReportDataArray[i].item_options[languageData[j].ISO_Code_1] !==
          "undefined" &&
        providedReportDataArray[i].item_options[languageData[j].ISO_Code_1].length > 0
      ) {
        // regular options
        if (
          providedReportDataArray[i].item_options[languageData[j].ISO_Code_1].filter(obj => {
            return obj.option_type === ITEM_OPTION_TYPE.multipleChoice;
          }).length > maxRegularOptions
        ) {
          maxRegularOptions = providedReportDataArray[i].item_options[
            languageData[j].ISO_Code_1
          ].filter(obj => {
            return obj.option_type === ITEM_OPTION_TYPE.multipleChoice;
          }).length;
        }

        // screen reader options
        if (
          providedReportDataArray[i].item_options[languageData[j].ISO_Code_1].filter(obj => {
            return obj.option_type === ITEM_OPTION_TYPE.multipleChoiceScreenReader;
          }).length > maxScreenReaderOptions
        ) {
          maxScreenReaderOptions = providedReportDataArray[i].item_options[
            languageData[j].ISO_Code_1
          ].filter(obj => {
            return obj.option_type === ITEM_OPTION_TYPE.multipleChoiceScreenReader;
          }).length;
        }
      }
      // ==================== ITEM OPTIONS (END) ====================
    }
  }
  // ==================== MAX NUMBER OF STEMS/OPTIONS (END) ====================

  // initializing reportData
  const reportData = [];

  // pushing columns definition
  reportData.push([
    LOCALIZE.reports.itemBankReport.systemId,
    LOCALIZE.reports.itemBankReport.version,
    LOCALIZE.reports.itemBankReport.historicalId,
    LOCALIZE.reports.itemBankReport.itemStatus,
    LOCALIZE.reports.itemBankReport.scoringKey,
    LOCALIZE.reports.itemBankReport.scoringKeyId,
    getItemRegularStemLabels(maxRegularStems),
    ...(maxScreenReaderStems > 0 ? getItemScreenReaderStemLabels(maxScreenReaderStems) : []),
    getItemRegularOptionsLabels(maxRegularOptions),
    ...(maxScreenReaderStems > 0 ? getItemScreenReaderOptionsLabels(maxScreenReaderOptions) : []),
    LOCALIZE.reports.itemBankReport.itemType,
    ...(numberOfItemAttributes > 0
      ? getItemAttributeValueLabels(providedReportDataArray, numberOfItemAttributes)
      : [])
  ]);

  // looping in provided providedReportDataArray and formatting data
  for (let i = 0; i < providedReportDataArray.length; i++) {
    reportData.push([
      providedReportDataArray[i].system_id,
      providedReportDataArray[i].version,
      providedReportDataArray[i].historical_id,
      providedReportDataArray[i][
        `development_status_name_${LOCALIZE.reports.itemBankReport.selectedLanguage}`
      ],
      getScoringKeyData(providedReportDataArray[i].item_options, languageData),
      getItemRegularStems(providedReportDataArray[i].item_content, maxRegularStems, languageData),
      ...(maxScreenReaderStems > 0
        ? getItemScreenReaderStems(
            providedReportDataArray[i].item_content,
            maxScreenReaderStems,
            languageData
          )
        : []),
      getItemRegularOptions(
        providedReportDataArray[i].item_options,
        maxRegularOptions,
        languageData
      ),
      ...(maxScreenReaderStems > 0
        ? getItemScreenReaderOptions(
            providedReportDataArray[i].item_options,
            maxScreenReaderOptions,
            languageData
          )
        : []),
      providedReportDataArray[i][
        `response_format_name_${LOCALIZE.reports.itemBankReport.selectedLanguage}`
      ],
      ...(numberOfItemAttributes > 0
        ? getItemAttributeValues(providedReportDataArray[i].item_attribute_values)
        : [])
    ]);
  }

  return reportData;
}

export default generateItemBankReport;
