import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Tabs, Tab, Row, Col } from "react-bootstrap";
import LOCALIZE from "../../text_resources";
import { switchTopTab } from "../../modules/NavTabsRedux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlusCircle } from "@fortawesome/free-solid-svg-icons";
import StyledTooltip, { EFFECT, TRIGGER_TYPE, TYPE } from "../authentication/StyledTooltip";
import "../../css/top-tabs.css";

const ADD_TAB_BUTTON_KEY = "add-tab-button-key";

const styles = {
  container: {
    maxWidth: 1400,
    minWidth: 600,
    paddingTop: 20,
    paddingRight: 20,
    paddingLeft: 20,
    margin: "0px auto"
  },
  tabsContainerWidth: {
    // preventing the notepad to go under the EmibTabs container
    width: "calc(1%)"
    // paddingRight: 0
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  margin0: {
    margin: 0
  },
  addTabButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    margin: "3px 12px -4px 12px",
    color: "#00565e"
  },
  allUnset: {
    all: "unset"
  }
};

class TopTabs extends Component {
  state = {
    // to enable or disable hidden message related to the only tab available as far as the test is not fully started (for screen reader users only)
    currentTab: undefined,
    oneTabOnlyHiddenMsgEnabled: false
  };

  constructor(props, context) {
    super(props, context);

    this.PropTypes = {
      customStyle: PropTypes.object,
      setTab: PropTypes.func,
      customCurrentTabProp: PropTypes.string,
      containsAddTabButton: PropTypes.bool,
      addTabButtonAction: PropTypes.func
    };

    TopTabs.defaultProps = {
      containsAddTabButton: false
    };
  }

  componentDidUpdate = prevProps => {
    // if defaultTab gets updated
    if (prevProps.defaultTab !== this.props.defaultTab) {
      this.switchTab(this.props.defaultTab, false);
    }
  };

  // enabling the "single tab available" message for screen reader users on enter eMIB action, meaning before the test fully starts
  componentDidMount = () => {
    let currentTab = this.props.defaultTab;
    if (this.props.customCurrentTabProp) {
      currentTab = this.props.customCurrentTabProp;
    } else if (this.props.currentTab >= 1) {
      currentTab = this.props.currentTab;
    }
    this.switchTab(currentTab, true);
  };

  // if the component is exported unconnected, switchTopTab is undefined
  // unconnected is used in testbuilder
  switchTab = (tab, calledOnRender) => {
    // Add tab button is selected
    if (tab === ADD_TAB_BUTTON_KEY && !calledOnRender) {
      this.props.addTabButtonAction();
    } else {
      if (this.props.switchTopTab) {
        if (this.props.setTab) {
          this.props.setTab(tab);
        } else {
          this.props.switchTopTab(tab);
        }
      } else {
        this.setState({ currentTab: tab });
      }
      if (this.props.notifySwitchTab) {
        this.props.notifySwitchTab(tab);
      }
    }
  };

  // check if the current tab is valid for the TABS provided
  // might be false because of reusable component
  getCurrentTab = () => {
    const { currentTab, customCurrentTabProp, TABS } = this.props;
    let currentTabProp = null;
    if (customCurrentTabProp) {
      currentTabProp = customCurrentTabProp;
    } else {
      currentTabProp = currentTab;
    }
    for (const tab of TABS) {
      if (currentTabProp === tab.key) {
        return currentTabProp;
      }
    }
    return this.state.currentTab || this.props.defaultTab;
  };

  render() {
    // let TABS = this.getTabs();
    const { TABS, containsAddTabButton } = this.props;
    const currentTab = this.getCurrentTab();
    return (
      <div>
        <Row style={styles.margin0}>
          <Col
            role="region"
            aria-label={LOCALIZE.ariaLabel.topNavigationSection}
            style={
              this.props.customStyle
                ? { ...styles.tabsContainerWidth, ...this.props.customStyle }
                : styles.tabsContainerWidth
            }
          >
            <div id="tab-container" tabIndex={-1}>
              {this.state.oneTabOnlyHiddenMsgEnabled && (
                <span id="only-one-tab-available-for-now-msg" style={styles.hiddenText}>
                  {LOCALIZE.emibTest.howToPage.testInstructions.onlyOneTabAvailableForNowMsg}
                </span>
              )}
              <Tabs
                defaultActiveKey={currentTab}
                id="top-tabs"
                activeKey={currentTab}
                onSelect={key => this.switchTab(key, false)}
                aria-labelledby="only-one-tab-available-for-now-msg"
              >
                {TABS.map((tab, index) => {
                  return (
                    <Tab
                      key={index}
                      eventKey={tab.key}
                      title={tab.tabName}
                      style={{ border: "1px", borderColor: "#00565e" }}
                    >
                      <div style={{ border: "1px", borderColor: "#00565e" }}>{tab.body}</div>
                    </Tab>
                  );
                })}
                {containsAddTabButton && (
                  <Tab
                    key={ADD_TAB_BUTTON_KEY}
                    eventKey={ADD_TAB_BUTTON_KEY}
                    title={
                      <StyledTooltip
                        id="add-tab-tooltip"
                        place="top"
                        type={TYPE.light}
                        effect={EFFECT.solid}
                        triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
                        tooltipElement={
                          <div style={styles.allUnset}>
                            <FontAwesomeIcon
                              data-tip=""
                              data-for="add-tab-tooltip"
                              aria-labelledby="add-tab-tooltip-content"
                              tabIndex="-1"
                              icon={faPlusCircle}
                              style={styles.addTabButton}
                            />
                          </div>
                        }
                        tooltipContent={
                          <div id="add-tab-tooltip-content">
                            <p>{LOCALIZE.commons.addButton}</p>
                          </div>
                        }
                      />
                    }
                  ></Tab>
                )}
              </Tabs>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

export { TopTabs as UnconnectedTopTabs };
const mapStateToProps = (state, ownProps) => {
  return {
    currentTab: state.navTabs.currentTopTab
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      switchTopTab
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(TopTabs);
