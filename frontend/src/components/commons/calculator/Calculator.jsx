// ref: https://github.com/niinpatel/calculator-react

import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import LOCALIZE from "../../../text_resources";
import ResultComponent from "./ResultComponent";
import KeyPadComponent from "./KeyPadComponent";
import { Col, Row } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMinusCircle } from "@fortawesome/free-solid-svg-icons";
import { getLineSpacingCSS } from "../../../modules/AccommodationsRedux";
import { toggleCalculator } from "../../../modules/CalculatorRedux";
import { getInTestCalculatorHeightCalculations } from "../../../helpers/inTestHeightCalculations";

const styles = {
  calculatorContainer: {
    width: 335,
    border: "1px solid #00565e"
  },
  calculatorWithHeaderContainer: {
    border: "1px solid #00565e",
    margin: "12px auto",
    width: "100%"
  },
  calculatorCol: {
    paddingRight: 0
  },
  label: {
    textAlign: "left",
    paddingLeft: 5,
    float: "right",
    fontSize: "16px",
    fontWeight: "bold",
    color: "#00565e",
    cursor: "pointer"
  },
  content: {
    backgroundColor: "white",
    borderWidth: "1px 1px 0px 1px",
    borderStyle: "solid",
    borderColor: "#00565e",
    width: "100%"
  },
  calculatorHeader: {
    width: "100%"
  },
  hideCalculatorBtn: {
    backgroundColor: "transparent",
    border: "none",
    cursor: "pointer"
  },
  headerSection: {
    borderBottom: "1.5px solid #88C800",
    width: "100%",
    padding: "8px 12px"
  },
  closeButton: {
    position: "bottom",
    color: "#00565E",
    float: "center",
    cursor: "pointer"
  }
};

class Calculator extends Component {
  constructor(props, context) {
    super(props, context);

    this.calculatorHeaderHeightRef = React.createRef();

    this.PropTypes = {
      showHeader: PropTypes.bool,
      calledInTestBuilder: PropTypes.bool,
      usesNotepad: PropTypes.bool
    };

    Calculator.defaultProps = {
      showHeader: false,
      usesNotepad: false
    };

    this.state = {
      result: "0",
      triggerRerender: false
    };
  }

  componentDidUpdate = prevProps => {
    // if isCalculatorHidden gets updated
    if (prevProps.isCalculatorHidden !== this.props.isCalculatorHidden) {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }

    // if isNotepadHidden gets updated
    if (prevProps.isNotepadHidden !== this.props.isNotepadHidden) {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }
  };

  onClick = button => {
    // making sure that the button is defined
    if (typeof button !== "undefined") {
      if (button === "=") {
        // making sure that the result is not "Error" before trying to calculate the result
        if (this.state.result !== LOCALIZE.commons.calculator.errorMessage) {
          this.calculate();
        }
      } else if (button === "C") {
        this.reset();
      } else if (button === "backspace") {
        this.backspace();
      } else {
        const newResult = this.handleAddbutton(button);
        this.setState({
          result: newResult
        });
      }
    }
  };

  handleAddbutton = button => {
    // initializing newResult
    let newResult = this.state.result;

    // getting last char of the result string
    const lastChar = newResult.slice(-1);

    // button pressed is either +, -, * or /
    if (button === "+" || button === "-" || button === "/" || button === "*") {
      // last char entered is either +, -, * or /
      if (lastChar === "+" || lastChar === "-" || lastChar === "/" || lastChar === "*") {
        // replacing operator
        newResult = this.state.result.slice(0, -1) + button;
        // last char is not an operator
      } else {
        // adding button pressed to result string
        newResult += button;
      }
      // button pressed is not an operator
    } else {
      // result is 0
      if (newResult === "0") {
        // reinitializing newResult to ""
        newResult = "";
      }
      // adding button pressed to result string
      newResult += button;
    }
    return newResult;
  };

  calculate = () => {
    const { result } = this.state;

    try {
      this.setState({
        // eslint-disable-next-line
        result: (eval(result) || "0") + ""
      });
    } catch (e) {
      this.setState({
        result: LOCALIZE.commons.calculator.errorMessage
      });
    }
  };

  reset = () => {
    this.setState({
      result: "0"
    });
  };

  backspace = () => {
    if (this.state.result !== "0") {
      this.setState({
        result: this.state.result.slice(0, -1) || "0"
      });
    }
  };

  toggleCalculator = () => {
    this.props.toggleCalculator();
  };

  render() {
    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    let customHeight = {};
    // if notepad is hidden and calculator is displayed or notepad is simply note used as tool
    if ((this.props.isNotepadHidden && !this.props.isCalculatorHidden) || !this.props.usesNotepad) {
      let { topTabsHeight } = this.props;

      // add extra height if in test builder
      if (this.props.calledInTestBuilder) {
        let heightOfBackToTestBuilderButton = 0;
        if (document.getElementById("back-to-test-builder-button") != null) {
          heightOfBackToTestBuilderButton = document.getElementById(
            "back-to-test-builder-button"
          ).offsetHeight;
        }
        topTabsHeight += heightOfBackToTestBuilderButton + 20; // height of "back to test builder button" + height of padding under button
      }

      customHeight = getInTestCalculatorHeightCalculations(
        this.props.accommodations.fontSize,
        this.props.accommodations.spacing,
        this.props.testNavBarHeight,
        topTabsHeight,
        this.props.notepadHeaderHeight,
        this.props.testFooterHeight
      );
    }

    return (
      <div id="calculator-main-div">
        {this.props.showHeader && (
          <Col id="calculator" style={styles.calculatorCol}>
            <Row
              id="calculator-header"
              ref={this.calculatorHeaderHeightRef}
              style={{ ...styles.calculatorHeader, ...customHeight }}
            >
              <div style={styles.content}>
                <div style={styles.headerSection} role="application">
                  <button
                    id="calculator-button"
                    onClick={() => this.toggleCalculator()}
                    style={styles.hideCalculatorBtn}
                    aria-label={LOCALIZE.commons.calculator.hideCalculator}
                  >
                    <span>
                      <FontAwesomeIcon style={styles.closeButton} icon={faMinusCircle} />
                      <span style={{ ...styles.label, ...accommodationsStyle }}>
                        <label htmlFor={"text-area-calculator"} className="notranslate">
                          {LOCALIZE.commons.calculator.title}
                        </label>
                      </span>
                    </span>
                  </button>
                </div>
                <div style={styles.calculatorWithHeaderContainer}>
                  <ResultComponent result={this.state.result} />
                  <KeyPadComponent onClick={this.onClick} />
                </div>
              </div>
            </Row>
          </Col>
        )}
        {!this.props.showHeader && (
          <div style={styles.calculatorContainer}>
            <ResultComponent result={this.state.result} />
            <KeyPadComponent onClick={this.onClick} />
          </div>
        )}
      </div>
    );
  }
}

export { Calculator as UnconnectedCalculator };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    testNavBarHeight: state.testSection.testNavBarHeight,
    topTabsHeight: state.testSection.topTabsHeight,
    notepadHeaderHeight: state.testSection.notepadHeaderHeight,
    testFooterHeight: state.testSection.testFooterHeight,
    isNotepadHidden: state.notepad.isNotepadHidden,
    isCalculatorHidden: state.calculator.isCalculatorHidden
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({ toggleCalculator }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Calculator);
