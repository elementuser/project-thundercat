import React, { Component } from "react";
import { connect } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBackspace } from "@fortawesome/free-solid-svg-icons";
import LOCALIZE from "../../../text_resources";

const styles = {
  buttonContainer: {
    display: "block",
    backgroundColor: "#bbb",
    height: "100%"
  },
  buttonCustomStyle: {
    width: "25%",
    height: "20%",
    backgroundColor: "white",
    padding: 0,
    color: "#00565e",
    fontWeight: "bold"
  },
  specialButtonCustomStyle: {
    backgroundColor: "#00565e",
    color: "white"
  }
};

class KeyPadComponent extends Component {
  componentDidMount = () => {
    // creating new event listener for the calculator main div
    const calculatorMainDiv = document.querySelector("#calculator-main-div");
    if (calculatorMainDiv) {
      calculatorMainDiv.addEventListener("keydown", this.handleKeydown);
    }
  };

  componentWillUnmount = () => {
    document
      .querySelector("#calculator-main-div")
      .removeEventListener("keydown", this.handleKeydown);
  };

  handleKeydown = event => {
    switch (event.key) {
      case "Escape":
        document.getElementById("reset-calculator-button").click();
        break;
      case "Backspace":
        document.getElementById("backspace-calculator-button").click();
        break;
      case "1":
        document.getElementById("number-1-calculator-button").click();
        break;
      case "2":
        document.getElementById("number-2-calculator-button").click();
        break;
      case "3":
        document.getElementById("number-3-calculator-button").click();
        break;
      case "4":
        document.getElementById("number-4-calculator-button").click();
        break;
      case "5":
        document.getElementById("number-5-calculator-button").click();
        break;
      case "6":
        document.getElementById("number-6-calculator-button").click();
        break;
      case "7":
        document.getElementById("number-7-calculator-button").click();
        break;
      case "8":
        document.getElementById("number-8-calculator-button").click();
        break;
      case "9":
        document.getElementById("number-9-calculator-button").click();
        break;
      case "0":
        document.getElementById("number-0-calculator-button").click();
        break;
      case ".":
        document.getElementById("dot-calculator-button").click();
        break;
      case ",":
        document.getElementById("dot-calculator-button").click();
        break;
      case "(":
        document.getElementById("open-parenthesis-calculator-button").click();
        break;
      case ")":
        document.getElementById("close-parenthesis-calculator-button").click();
        break;
      case "+":
        document.getElementById("addition-calculator-button").click();
        break;
      case "-":
        document.getElementById("substraction-calculator-button").click();
        break;
      case "*":
        document.getElementById("multiplication-calculator-button").click();
        break;
      case "/":
        document.getElementById("division-calculator-button").click();
        break;
      case "Enter":
        document.getElementById("equal-calculator-button").click();
        break;
      default:
        // do nothing
        break;
    }
  };

  render() {
    let customFontSize = "25px";
    // if font size > 25
    if (parseInt(this.props.accommodations.fontSize.split("px")[0]) > 25) {
      customFontSize = this.props.accommodations.fontSize;
    }
    const accommodationStyles = {
      fontFamily: this.props.accommodations.fontFamily,
      //   fontSize: this.props.accommodations.fontSize
      fontSize: customFontSize
    };

    return (
      <div style={styles.buttonContainer}>
        <button
          id="reset-calculator-button"
          name="C"
          onClick={e => this.props.onClick(e.target.name)}
          style={{
            ...styles.buttonCustomStyle,
            ...styles.specialButtonCustomStyle,
            ...accommodationStyles
          }}
        >
          C
        </button>
        <button
          id="open-parenthesis-calculator-button"
          name="("
          onClick={e => this.props.onClick(e.target.name)}
          style={{
            ...styles.buttonCustomStyle,
            ...styles.specialButtonCustomStyle,
            ...accommodationStyles
          }}
        >
          (
        </button>
        <button
          id="close-parenthesis-calculator-button"
          name=")"
          onClick={e => this.props.onClick(e.target.name)}
          style={{
            ...styles.buttonCustomStyle,
            ...styles.specialButtonCustomStyle,
            ...accommodationStyles
          }}
        >
          )
        </button>
        <button
          id="division-calculator-button"
          name="/"
          onClick={e => this.props.onClick(e.target.name)}
          style={{
            ...styles.buttonCustomStyle,
            ...styles.specialButtonCustomStyle,
            ...accommodationStyles
          }}
        >
          /
        </button>
        <br />
        <button
          id="number-7-calculator-button"
          name="7"
          onClick={e => this.props.onClick(e.target.name)}
          style={{ ...styles.buttonCustomStyle, ...accommodationStyles }}
        >
          7
        </button>
        <button
          id="number-8-calculator-button"
          name="8"
          onClick={e => this.props.onClick(e.target.name)}
          style={{ ...styles.buttonCustomStyle, ...accommodationStyles }}
        >
          8
        </button>
        <button
          id="number-9-calculator-button"
          name="9"
          onClick={e => this.props.onClick(e.target.name)}
          style={{ ...styles.buttonCustomStyle, ...accommodationStyles }}
        >
          9
        </button>
        <button
          id="multiplication-calculator-button"
          name="*"
          onClick={e => this.props.onClick(e.target.name)}
          style={{
            ...styles.buttonCustomStyle,
            ...styles.specialButtonCustomStyle,
            ...accommodationStyles
          }}
        >
          x
        </button>
        <br />
        <button
          id="number-4-calculator-button"
          name="4"
          onClick={e => this.props.onClick(e.target.name)}
          style={{ ...styles.buttonCustomStyle, ...accommodationStyles }}
        >
          4
        </button>
        <button
          id="number-5-calculator-button"
          name="5"
          onClick={e => this.props.onClick(e.target.name)}
          style={{ ...styles.buttonCustomStyle, ...accommodationStyles }}
        >
          5
        </button>
        <button
          id="number-6-calculator-button"
          name="6"
          onClick={e => this.props.onClick(e.target.name)}
          style={{ ...styles.buttonCustomStyle, ...accommodationStyles }}
        >
          6
        </button>
        <button
          id="substraction-calculator-button"
          name="-"
          onClick={e => this.props.onClick(e.target.name)}
          style={{
            ...styles.buttonCustomStyle,
            ...styles.specialButtonCustomStyle,
            ...accommodationStyles
          }}
        >
          -
        </button>
        <br />
        <button
          id="number-1-calculator-button"
          name="1"
          onClick={e => this.props.onClick(e.target.name)}
          style={{ ...styles.buttonCustomStyle, ...accommodationStyles }}
        >
          1
        </button>
        <button
          id="number-2-calculator-button"
          name="2"
          onClick={e => this.props.onClick(e.target.name)}
          style={{ ...styles.buttonCustomStyle, ...accommodationStyles }}
        >
          2
        </button>
        <button
          id="number-3-calculator-button"
          name="3"
          onClick={e => this.props.onClick(e.target.name)}
          style={{ ...styles.buttonCustomStyle, ...accommodationStyles }}
        >
          3
        </button>
        <button
          id="addition-calculator-button"
          name="+"
          onClick={e => this.props.onClick(e.target.name)}
          style={{
            ...styles.buttonCustomStyle,
            ...styles.specialButtonCustomStyle,
            ...accommodationStyles
          }}
        >
          +
        </button>
        <br />
        <button
          id="number-0-calculator-button"
          name="0"
          onClick={e => this.props.onClick(e.target.name)}
          style={{ ...styles.buttonCustomStyle, ...accommodationStyles }}
        >
          0
        </button>
        <button
          id="dot-calculator-button"
          name="."
          onClick={e => this.props.onClick(e.target.name)}
          style={{ ...styles.buttonCustomStyle, ...accommodationStyles }}
        >
          .
        </button>
        <button
          id="backspace-calculator-button"
          name="backspace"
          aria-label={LOCALIZE.commons.calculator.backspaceButton}
          onClick={e => this.props.onClick(e.target.name)}
          style={{ ...styles.buttonCustomStyle, ...accommodationStyles }}
        >
          {
            <span onClick={() => document.getElementById("backspace-calculator-button").click()}>
              <FontAwesomeIcon icon={faBackspace} />
            </span>
          }
        </button>
        <button
          id="equal-calculator-button"
          name="="
          onClick={e => {
            const calculatorResult = document.getElementById("calculator-result");
            if (calculatorResult) {
              calculatorResult.focus();
            }
            return this.props.onClick(e.target.name);
          }}
          style={{
            ...styles.buttonCustomStyle,
            ...styles.specialButtonCustomStyle,
            ...accommodationStyles
          }}
        >
          =
        </button>
        <br />
      </div>
    );
  }
}

export { KeyPadComponent as UnconnectedKeyPadComponent };

const mapStateToProps = (state, ownProps) => {
  return {
    accommodations: state.accommodations
  };
};

export default connect(mapStateToProps, null)(KeyPadComponent);
