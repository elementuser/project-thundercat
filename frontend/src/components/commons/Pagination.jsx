import React from "react";
import PropTypes from "prop-types";
import LOCALIZE from "../../text_resources";
import ReactPaginate from "react-paginate";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCaretLeft, faCaretRight } from "@fortawesome/free-solid-svg-icons";

const styles = {
  paginationContainer: {
    display: "flex",
    paddingBottom: 25
  },
  paginationIcon: {
    padding: "0 6px"
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  }
};

class Pagination extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.PropTypes = {
      pageCount: PropTypes.number.isRequired,
      paginationPage: PropTypes.number.isRequired,
      updatePaginationPageState: PropTypes.func.isRequired,
      firstTableRowId: PropTypes.string.isRequired,
      paginationContainerId: PropTypes.string.isRequired,
      marginPagesDisplayed: PropTypes.number
    };

    Pagination.defaultProps = {
      marginPagesDisplayed: 3
    };
  }

  // handling page changes based on pagination selection
  handlePageChange = id => {
    // "+1" because redux pagination page uses index 1 and react-paginate (id) uses index 0
    const selectedPage = id.selected + 1;
    // update current page redux state
    this.props.updatePaginationPageState(selectedPage);
    // focusing on the first table's row
    if (document.getElementById(this.props.firstTableRowId)) {
      document.getElementById(this.props.firstTableRowId).focus();
    }
  };

  render() {
    const { pageCount, marginPagesDisplayed, paginationPage } = this.props;

    return (
      <div style={styles.paginationContainer} id={this.props.paginationContainerId}>
        <ReactPaginate
          pageCount={pageCount}
          containerClassName={"pagination"}
          breakClassName={"break"}
          activeClassName={"active-page"}
          marginPagesDisplayed={marginPagesDisplayed}
          // "-1" because react-paginate uses index 0 and allActiveCandidatesPaginationPage redux state uses index 1
          forcePage={paginationPage - 1}
          onPageChange={page => {
            this.handlePageChange(page);
          }}
          previousLabel={
            <div style={styles.paginationIcon}>
              <label style={styles.hiddenText}>
                {LOCALIZE.commons.pagination.previousPageButton}
              </label>
              <FontAwesomeIcon icon={faCaretLeft} />
            </div>
          }
          nextLabel={
            <div style={styles.paginationIcon}>
              <label style={styles.hiddenText}>{LOCALIZE.commons.pagination.nextPageButton}</label>
              <FontAwesomeIcon icon={faCaretRight} />
            </div>
          }
          breakLabel={
            <span>
              <span>...</span>
              <span
                role="img"
                aria-label={LOCALIZE.commons.pagination.breakAriaLabel}
                style={styles.hiddenText}
              ></span>
            </span>
          }
          nextAriaLabel={LOCALIZE.commons.pagination.nextPageButton}
          previousAriaLabel={LOCALIZE.commons.pagination.previousPageButton}
        />
      </div>
    );
  }
}

export default Pagination;
