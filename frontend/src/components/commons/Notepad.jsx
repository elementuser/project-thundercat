import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import LOCALIZE from "../../text_resources";
import "../../css/emib-tabs.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMinusCircle } from "@fortawesome/free-solid-svg-icons";
import { Col, Row } from "react-bootstrap";
import { setNotepadContent, toggleNotepad } from "../../modules/NotepadRedux";
import { updateNotepad, getNotepad } from "../../modules/UpdateNotepadRedux";
import { setNotepadHeaderHeight } from "../../modules/TestSectionRedux";
import { getInTestNotepadHeightCalculations } from "../../helpers/inTestHeightCalculations";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";

class Notepad extends Component {
  styles = {
    notepadCol: {
      paddingRight: 0
    },
    notepadHeader: {
      width: "100%"
    },
    label: {
      textAlign: "left",
      paddingLeft: 5,
      float: "right",
      fontSize: "16px",
      fontWeight: "bold",
      color: "#00565e",
      cursor: "pointer"
    },
    hideNotepadBtn: {
      backgroundColor: "transparent",
      border: "none",
      cursor: "pointer"
    },
    hideNotepadBtnIcon: {
      paddingRight: 5
    },
    headerSection: {
      borderBottom: "1.5px solid #88C800",
      width: "100%",
      padding: "8px 12px"
    },
    content: {
      backgroundColor: "white",
      borderWidth: "1px 1px 0px 1px",
      borderStyle: "solid",
      borderColor: "#00565e",
      width: "100%"
    },
    textAreaRow: {
      padding: 0,
      resize: "none",
      border: "none",
      overflow: "scrollY",
      width: "100%",
      borderTop: "none"
    },
    textArea: {
      padding: 10,
      resize: "none",
      width: "100%",
      borderWidth: "0px 1px 1px 1px",
      borderStyle: "solid",
      borderColor: "#00565e"
    },
    openNotepadBtn: {
      borderBottomLeftRadius: 0,
      borderBottomRightRadius: 0,
      cursor: "pointer",
      whiteSpace: "normal",
      padding: 2,
      backgroundColor: "#00565e",
      borderColor: "#00565e",
      overflow: "wrap",
      display: "block"
    },
    openText: {
      color: "#FFFFFF",
      display: "block",
      cursor: "pointer"
    },
    closeButton: {
      position: "bottom",
      color: "#00565E",
      float: "center",
      cursor: "pointer"
    },
    noPadding: {
      padding: "0 0 0 0",
      margin: "0 0 0 0"
    }
  };

  constructor(props) {
    super(props);
    this.notePadHeaderHeightRef = React.createRef();

    this.PropTypes = {
      notepadContent: PropTypes.string,
      setNotepadContent: PropTypes.func,
      toggleNotepad: PropTypes.func,
      isNotepadHidden: PropTypes.bool,
      calledInTestBuilder: PropTypes.bool,
      calledWithCalculator: PropTypes.bool
    };

    Notepad.defaultProps = {
      calledWithCalculator: false
    };

    this.state = {
      notepadTimeout: 0,
      triggerRerender: false
    };
  }

  componentDidMount = () => {
    this.getNotepadContent();
    // updating notepad header height redux state
    if (this.notePadHeaderHeightRef.current) {
      this.props.setNotepadHeaderHeight(this.notePadHeaderHeightRef.current.clientHeight);
    }
    // if called with calculator
    if (this.props.calledWithCalculator) {
      // setting a 100ms delay, so the calculator is rendered before (useful for height calculation)
      setTimeout(() => {
        this.setState({ triggerRerender: !this.state.triggerRerender });
      }, 100);
    }
  };

  componentDidUpdate = prevProps => {
    // updating notepad header height redux state
    if (this.notePadHeaderHeightRef.current) {
      this.props.setNotepadHeaderHeight(this.notePadHeaderHeightRef.current.clientHeight);
    }

    // if accommodations gets updated
    if (prevProps.accommodations !== this.props.accommodations) {
      // updating notepad header height redux state
      this.props.setNotepadHeaderHeight(this.notePadHeaderHeightRef.current.clientHeight);
    }

    // if isCalculatorHidden gets updated
    if (prevProps.isCalculatorHidden !== this.props.isCalculatorHidden) {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }

    // if isNotepadHidden gets updated
    if (prevProps.isNotepadHidden !== this.props.isNotepadHidden) {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }

    // if triggerRerender gets updated
    if (prevProps.triggerRerender !== this.props.triggerRerender) {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }
  };

  getNotepadContent = () => {
    // assignedTestId exists (not a sample test) + notepad content redux state is null (it will avoid calling it each time the page is refreshed)
    if (
      typeof this.props.assignedTestId !== "undefined" &&
      this.props.assignedTestId !== null &&
      this.props.notepadContent === null
    ) {
      // load the notepad from the DB (or will return "" if there is nothing saved)
      this.props.getNotepad(this.props.assignedTestId).then(response => {
        this.props.setNotepadContent(response.notepad);
      });
    }
  };

  handleNotepadContent = event => {
    this.props.setNotepadContent(event.target.value);
    if (typeof this.props.assignedTestId !== "undefined" && this.props.assignedTestId !== null) {
      // creating timeout function so we don't overload server
      if (this.state.notepadTimeout) {
        clearTimeout(this.state.notepadTimeout);
      }
      const timeout = setTimeout(this.saveNotepadContent, 5000);
      this.setState({ notepadTimeout: timeout });
    }
  };

  saveNotepadContent = () => {
    this.props.updateNotepad(this.props.assignedTestId, this.props.notepadContent);
  };

  toggleNotepad = () => {
    this.props.toggleNotepad();
  };

  render() {
    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    let { topTabsHeight } = this.props;

    // add extra height if in test builder
    if (this.props.calledInTestBuilder) {
      let heightOfBackToTestBuilderButton = 0;
      if (document.getElementById("back-to-test-builder-button") != null) {
        heightOfBackToTestBuilderButton = document.getElementById(
          "back-to-test-builder-button"
        ).offsetHeight;
      }
      topTabsHeight += heightOfBackToTestBuilderButton + 20; // height of "back to test builder button" + height of padding under button
    }

    let heightOfCalculator = null;
    if (document.getElementById("calculator-main-div") != null) {
      heightOfCalculator = document.getElementById("calculator-main-div").offsetHeight;
    }

    const customHeight = getInTestNotepadHeightCalculations(
      this.props.accommodations.fontSize,
      this.props.accommodations.spacing,
      this.props.testNavBarHeight,
      topTabsHeight,
      this.props.notepadHeaderHeight,
      this.props.testFooterHeight,
      heightOfCalculator
    );

    return (
      <Col id="notepad" style={this.styles.notepadCol} className="notranslate">
        <Row
          id="notepad-header"
          ref={this.notePadHeaderHeightRef}
          style={this.styles.notepadHeader}
        >
          <div style={this.styles.content}>
            <div style={this.styles.headerSection} role="application">
              <button
                id="notepad-button"
                onClick={() => this.toggleNotepad()}
                style={this.styles.hideNotepadBtn}
                aria-label={LOCALIZE.commons.notepad.hideNotepad}
              >
                <span>
                  <FontAwesomeIcon style={this.styles.closeButton} icon={faMinusCircle} />
                  <span style={{ ...this.styles.label, ...accommodationsStyle }}>
                    <label htmlFor={"text-area-notepad"} className="notranslate">
                      {LOCALIZE.commons.notepad.title}
                    </label>
                  </span>
                </span>
              </button>
            </div>
          </div>
        </Row>
        <Row id="notepad-body" style={{ ...this.styles.textAreaRow, ...customHeight }}>
          <textarea
            id="text-area-notepad"
            aria-placeholder={LOCALIZE.commons.notepad.placeholder}
            maxLength="10000"
            className="text-area"
            style={{ ...this.styles.textArea, ...accommodationsStyle, ...customHeight }}
            rows={22}
            placeholder={LOCALIZE.commons.notepad.placeholder}
            value={this.props.notepadContent !== null ? this.props.notepadContent : ""}
            onChange={this.handleNotepadContent}
          />
        </Row>
      </Col>
    );
  }
}

export { Notepad as UnconnectedNotepad };

const mapStateToProps = (state, ownProps) => {
  return {
    notepadContent: state.notepad.notepadContent,
    isNotepadHidden: state.notepad.isNotepadHidden,
    isCalculatorHidden: state.calculator.isCalculatorHidden,
    triggerRerender: state.calculator.triggerRerender,
    assignedTestId: state.assignedTest.assignedTestId,
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    testNavBarHeight: state.testSection.testNavBarHeight,
    topTabsHeight: state.testSection.topTabsHeight,
    notepadHeaderHeight: state.testSection.notepadHeaderHeight,
    testFooterHeight: state.testSection.testFooterHeight
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getNotepad,
      setNotepadContent,
      toggleNotepad,
      updateNotepad,
      setNotepadHeaderHeight
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Notepad);
