import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import LOCALIZE from "../../text_resources";
import ContentContainer from "./ContentContainer";
import {
  getTestSection,
  setTopTabsHeight,
  setTestFooterHeight
} from "../../modules/TestSectionRedux";
import { unlockTest } from "../../modules/TestStatusRedux";
import getInTestHeightCalculations from "../../helpers/inTestHeightCalculations";

const styles = {
  tabSection: {
    paddingLeft: 18,
    marginBottom: "-3px"
  },
  tabLabel: {
    padding: "4px 24px",
    marginBottom: 0,
    position: "relative"
  },
  tabBorder: {
    borderBottom: "5px solid #00565e",
    width: "100%",
    position: "absolute",
    bottom: 0,
    left: 0,
    borderRadius: "4px 4px 0 0"
  },
  tabContentContainer: {
    borderStyle: "solid",
    borderWidth: "3px 1px 1px 1px",
    borderColor: "#CECECE",
    padding: "12px 0",
    overflowY: "scroll"
  },
  descriptionContainer: {
    margin: 60,
    fontWeight: "bold"
  }
};

class LockScreen extends Component {
  constructor(props) {
    super(props);
    this.topTabsHeightRef = React.createRef();
  }

  componentDidMount = () => {
    // focusing on test locked tab
    if (document.getElementById("test-locked-tab")) {
      document.getElementById("test-locked-tab").focus();
    }
    // updating nav bar and test footer height redux state
    if (this.topTabsHeightRef.current) {
      this.props.setTopTabsHeight(this.topTabsHeightRef.current.clientHeight);
      this.props.setTestFooterHeight(0);
    }
  };

  componentDidUpdate = () => {
    // updating nav bar and test footer height redux state
    if (this.topTabsHeightRef.current) {
      this.props.setTopTabsHeight(this.topTabsHeightRef.current.clientHeight);
      this.props.setTestFooterHeight(0);
    }
  };

  render() {
    const tabTitleCustomFontSize = {
      // adding 12px to selected font-size
      fontSize: `${parseInt(this.props.accommodations.fontSize.split("px")[0]) + 12}px`
    };

    const customHeight = getInTestHeightCalculations(
      this.props.accommodations.fontSize,
      this.props.accommodations.spacing,
      this.props.testNavBarHeight,
      this.props.topTabsHeight,
      this.props.testFooterHeight
    );

    const dynamicMarginBottom = this.props.accommodations.spacing ? { marginBottom: "-5em" } : {};

    return (
      <div>
        <ContentContainer>
          <div
            id="test-locked-tab"
            ref={this.topTabsHeightRef}
            style={styles.tabSection}
            tabIndex={0}
          >
            <div>
              <label style={{ ...styles.tabLabel, ...tabTitleCustomFontSize }}>
                {LOCALIZE.candidateLockScreen.tabTitle}
                <span style={styles.tabBorder}></span>
              </label>
            </div>
          </div>
          <div
            style={{ ...styles.tabContentContainer, ...customHeight, ...dynamicMarginBottom }}
            tabIndex={0}
          >
            <div style={styles.descriptionContainer}>
              <p>{LOCALIZE.candidateLockScreen.description.part1}</p>
              <p>{LOCALIZE.candidateLockScreen.description.part2}</p>
              <p>{LOCALIZE.candidateLockScreen.description.part3}</p>
              <br />
              <p>{LOCALIZE.candidateLockScreen.description.part4}</p>
            </div>
          </div>
        </ContentContainer>
      </div>
    );
  }
}

export { LockScreen as unconnectedLockScreen };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    testSection: state.testSection.testSection,
    assignedTestId: state.assignedTest.assignedTestId,
    testId: state.assignedTest.testId,
    topTabsHeight: state.testSection.topTabsHeight,
    testNavBarHeight: state.testSection.testNavBarHeight,
    testFooterHeight: state.testSection.testFooterHeight,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getTestSection,
      setTopTabsHeight,
      setTestFooterHeight,
      unlockTest
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(LockScreen);
