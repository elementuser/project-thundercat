import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import PropTypes from "prop-types";
import { getSelectedUserTestPermissions } from "../../../modules/PermissionsRedux";
import { LANGUAGES } from "../../../modules/LocalizeRedux";
import { faSort, faSortDown, faSpinner } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import LOCALIZE from "../../../text_resources";
import { Table } from "react-bootstrap";
import { getDecodedString } from "../../commons/GenericTable";

const styles = {
  container: {
    padding: "12px 0 0 24px"
  },
  tableContainer: {
    margin: "24px 22px 0 0",
    minHeight: 100,
    border: "1px solid #00565e",
    borderRadius: "4px 4px 0 0",
    backgroundColor: "white"
  },
  permissionsTable: {
    title: {
      display: "table-cell",
      verticalAlign: "middle",
      fontWeight: "bold"
    }
  },
  testPermissionsTable: {
    spacer: {
      margin: "24px 0"
    },
    title: {
      width: "calc(100vw)",
      height: 50,
      color: "white",
      backgroundColor: "#00565e",
      display: "table-cell",
      verticalAlign: "middle",
      fontWeight: "bold",
      paddingLeft: 12,
      borderRadius: "4px 4px 0 0"
    },
    container: {
      width: "95%",
      margin: "12px auto",
      borderCollapse: "collapse",
      backgroundColor: "white"
    },
    containerEmpty: {
      borderCollapse: "collapse",
      backgroundColor: "white",
      width: "100%",
      height: "60px",
      padding: "8px 0px 8px 12px",
      verticalAlign: "middle",
      maxWidth: "500px",
      overflowWrap: "break-word"
    },
    testVersionsTable: {
      head: {
        width: "100%",
        color: "#00565e"
      },
      title: {
        width: "26.666%",
        verticalAlign: "middle",
        textAlign: "center",
        padding: "6px 12px"
      },
      testCodeTitle: {
        verticalAlign: "middle",
        padding: "6px 12px 6px 48px"
      },
      testCodeLabel: {
        padding: "6px 12px 6px 48px"
      },
      orderNumberLabel: {
        textAlign: "center",
        padding: "6px 12px"
      },
      expirationLabel: {
        textAlign: "center",
        padding: "6px 12px"
      },
      iconButton: {
        border: "none",
        background: "transparent",
        color: "#00565e"
      },
      sortIcon: {
        marginLeft: 6,
        cursor: "pointer"
      },
      sortIconActif: {
        marginLeft: 6,
        cursor: "pointer",
        verticalAlign: "top"
      }
    }
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  loading: {
    width: "100%",
    height: 60,
    textAlign: "center",
    fontSize: "32px",
    padding: 6
  },
  tableNoPermissionPadding: {
    padding: 12
  }
};

export class UserTestPermissions extends Component {
  static propTypes = {
    // Props from Redux
    getSelectedUserTestPermissions: PropTypes.func
  };

  state = {
    testPermissions: [],
    testPermissionsTableSortingElement: 1,
    isLoadingTestPermissions: false
  };

  componentDidMount = () => {
    this.populateSelectedUserTestPermissions(this.state.testPermissionsTableSortingElement);
  };

  componentDidUpdate = (prevProps, prevState) => {
    // if testPermissionsTableSortingElement state gets updated
    if (
      prevState.testPermissionsTableSortingElement !== this.state.testPermissionsTableSortingElement
    ) {
      this.populateSelectedUserTestPermissions(this.state.testPermissionsTableSortingElement);
    }
  };

  // populate test permissions based on the user' test permissions
  /* sorting sources:
      1: sorting by test versions (default)
      2: sorting by test order number
      3: sorting by expiry date
  */
  populateSelectedUserTestPermissions = sortingSource => {
    this.setState({ isLoadingTestPermissions: true }, () => {
      const currentTestPermissionsArray = [];
      this.props
        .getSelectedUserTestPermissions(this.props.selectedUsername)
        .then(response => {
          // looping in test permissions
          for (let i = 0; i < response.length; i++) {
            // pushing results in currentTestPermissionsArray array
            currentTestPermissionsArray.push({
              en_test_name: response[i].test.en_name,
              fr_test_name: response[i].test.fr_name,
              test_code: response[i].test.test_code,
              test_order_number: response[i].test_order_number,
              staffing_process_number: response[i].staffing_process_number,
              expiry_date: response[i].expiry_date
            });
            // sorting test permissions
            if (sortingSource === 1) {
              currentTestPermissionsArray.sort(this.sortByTestCode);
            } else if (sortingSource === 2) {
              currentTestPermissionsArray.sort(this.sortByTestOrderNumber);
            } else if (sortingSource === 3) {
              currentTestPermissionsArray.sort(this.sortByStaffingProcessNumber);
            } else if (sortingSource === 4) {
              currentTestPermissionsArray.sort(this.sortByExpiryDate);
            }
            // saving populated array in the state
            this.setState({
              testPermissions: currentTestPermissionsArray
            });
          }
        })
        .then(() => {
          this.setState({ isLoadingTestPermissions: false });
        });
    });
  };

  // sorting functions (https://www.sitepoint.com/sort-an-array-of-objects-in-javascript/)
  // sorting by test versions
  sortByTestCode = (a, b) => {
    let item_a = "";
    let item_b = "";
    if (this.props.currentLanguage === LANGUAGES.english) {
      item_a = a.en_test_name.toUpperCase();
      item_b = b.en_test_name.toUpperCase();
    } else {
      item_a = a.fr_test_name.toUpperCase();
      item_b = b.fr_test_name.toUpperCase();
    }

    let comparison = 0;
    if (item_a > item_b) {
      comparison = 1;
    } else if (item_a < item_b) {
      comparison = -1;
    }
    return comparison;
  };

  // sorting by test order number
  sortByTestOrderNumber = (a, b) => {
    const item_a = a.test_order_number.toUpperCase();
    const item_b = b.test_order_number.toUpperCase();

    let comparison = 0;
    if (item_a > item_b) {
      comparison = 1;
    } else if (item_a < item_b) {
      comparison = -1;
    }
    return comparison;
  };

  // sorting by staffing process number
  sortByStaffingProcessNumber = (a, b) => {
    const item_a = a.staffing_process_number.toUpperCase();
    const item_b = b.staffing_process_number.toUpperCase();

    let comparison = 0;
    if (item_a > item_b) {
      comparison = 1;
    } else if (item_a < item_b) {
      comparison = -1;
    }
    return comparison;
  };

  // sorting by expiry date
  sortByExpiryDate = (a, b) => {
    const item_a = a.expiry_date.toUpperCase();
    const item_b = b.expiry_date.toUpperCase();

    let comparison = 0;
    if (item_a > item_b) {
      comparison = 1;
    } else if (item_a < item_b) {
      comparison = -1;
    }
    return comparison;
  };

  handleSortByTestVertions = () => {
    this.setState({ testPermissionsTableSortingElement: 1 });
  };

  handleSortByTestOrderNumber = () => {
    this.setState({ testPermissionsTableSortingElement: 2 });
  };

  handleSortByStaffingProcessNumber = () => {
    this.setState({ testPermissionsTableSortingElement: 3 });
  };

  handleSortByExpiryDate = () => {
    this.setState({ testPermissionsTableSortingElement: 4 });
  };

  render() {
    return (
      <div>
        {this.state.testPermissions.length > 0 && !this.state.isLoadingTestPermissions && (
          <section
            aria-labelledby="test-permissions-section"
            style={styles.testPermissionsTable.spacer}
            tabIndex={0}
          >
            <h2 id="test-permissions-section">
              {LOCALIZE.systemAdministrator.userLookUpDetails.testPermissions.title}
            </h2>
            <div style={styles.container}>
              <p>
                {LOCALIZE.formatString(
                  LOCALIZE.systemAdministrator.userLookUpDetails.testPermissions.description,
                  this.props.selectedUserFirstname,
                  this.props.selectedUserLastname
                )}
              </p>
              <div style={styles.tableContainer}>
                <div style={styles.testPermissionsTable.title}>
                  <span>{LOCALIZE.profile.testPermissions.table.title}</span>
                </div>
                <div className="table-responsive-no-styling">
                  <Table
                    responsive
                    style={styles.testPermissionsTable.container}
                    role="table"
                    tabIndex={0}
                  >
                    <thead>
                      <tr style={styles.testPermissionsTable.testVersionsTable.head}>
                        <th
                          scope="col"
                          style={styles.testPermissionsTable.testVersionsTable.testCodeTitle}
                        >
                          <span id="test-access-title-column">
                            {LOCALIZE.profile.testPermissions.table.column1}
                          </span>
                          <button
                            style={styles.testPermissionsTable.testVersionsTable.iconButton}
                            onClick={this.handleSortByTestVertions}
                          >
                            <FontAwesomeIcon
                              icon={
                                this.state.testPermissionsTableSortingElement === 1
                                  ? faSortDown
                                  : faSort
                              }
                              style={
                                this.state.testPermissionsTableSortingElement === 1
                                  ? styles.testPermissionsTable.testVersionsTable.sortIconActif
                                  : styles.testPermissionsTable.testVersionsTable.sortIcon
                              }
                            ></FontAwesomeIcon>
                            <span style={styles.hiddenText}>
                              {LOCALIZE.profile.testPermissions.table.column1OrderItem}
                            </span>
                          </button>
                        </th>
                        <th scope="col" style={styles.testPermissionsTable.testVersionsTable.title}>
                          <span id="test-order-number-column">
                            {LOCALIZE.profile.testPermissions.table.column2}
                          </span>
                          <button
                            style={styles.testPermissionsTable.testVersionsTable.iconButton}
                            onClick={this.handleSortByTestOrderNumber}
                          >
                            <FontAwesomeIcon
                              icon={
                                this.state.testPermissionsTableSortingElement === 2
                                  ? faSortDown
                                  : faSort
                              }
                              style={
                                this.state.testPermissionsTableSortingElement === 2
                                  ? styles.testPermissionsTable.testVersionsTable.sortIconActif
                                  : styles.testPermissionsTable.testVersionsTable.sortIcon
                              }
                            ></FontAwesomeIcon>
                            <span style={styles.hiddenText}>
                              {LOCALIZE.profile.testPermissions.table.column2OrderItem}
                            </span>
                          </button>
                        </th>
                        <th scope="col" style={styles.testPermissionsTable.testVersionsTable.title}>
                          <span id="staffing-process-number-column">
                            {LOCALIZE.profile.testPermissions.table.column3}
                          </span>
                          <button
                            style={styles.testPermissionsTable.testVersionsTable.iconButton}
                            onClick={this.handleSortByStaffingProcessNumber}
                          >
                            <FontAwesomeIcon
                              icon={
                                this.state.testPermissionsTableSortingElement === 3
                                  ? faSortDown
                                  : faSort
                              }
                              style={
                                this.state.testPermissionsTableSortingElement === 3
                                  ? styles.testPermissionsTable.testVersionsTable.sortIconActif
                                  : styles.testPermissionsTable.testVersionsTable.sortIcon
                              }
                            ></FontAwesomeIcon>
                            <span style={styles.hiddenText}>
                              {LOCALIZE.profile.testPermissions.table.column3OrderItem}
                            </span>
                          </button>
                        </th>
                        <th scope="col" style={styles.testPermissionsTable.testVersionsTable.title}>
                          <span id="expiry-date-column">
                            {LOCALIZE.profile.testPermissions.table.column4}
                          </span>
                          <button
                            style={styles.testPermissionsTable.testVersionsTable.iconButton}
                            onClick={this.handleSortByExpiryDate}
                          >
                            <FontAwesomeIcon
                              icon={
                                this.state.testPermissionsTableSortingElement === 4
                                  ? faSortDown
                                  : faSort
                              }
                              style={
                                this.state.testPermissionsTableSortingElement === 4
                                  ? styles.testPermissionsTable.testVersionsTable.sortIconActif
                                  : styles.testPermissionsTable.testVersionsTable.sortIcon
                              }
                            ></FontAwesomeIcon>
                            <span style={styles.hiddenText}>
                              {LOCALIZE.profile.testPermissions.table.column4OrderItem}
                            </span>
                          </button>
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.testPermissions.map((testPermission, id) => (
                        <tr
                          key={id}
                          tabIndex={0}
                          aria-labelledby={`test-access-title-column test-permission-label-${id} test-order-number-column test-permission-order-number-${id} staffing-process-number-column test-permission-staffing-process-number-${id} expiry-date-column test-permission-expiration-${id}`}
                        >
                          <td
                            id={`test-permission-label-${id}`}
                            style={styles.testPermissionsTable.testVersionsTable.testCodeLabel}
                          >
                            <span>{testPermission.test_code}</span>
                          </td>
                          <td
                            id={`test-permission-order-number-${id}`}
                            style={styles.testPermissionsTable.testVersionsTable.orderNumberLabel}
                          >
                            <span>{testPermission.test_order_number}</span>
                          </td>
                          <td
                            id={`test-permission-staffing-process-number-${id}`}
                            style={styles.testPermissionsTable.testVersionsTable.orderNumberLabel}
                          >
                            <span>{testPermission.staffing_process_number}</span>
                          </td>
                          <td
                            id={`test-permission-expiration-${id}`}
                            style={styles.testPermissionsTable.testVersionsTable.expirationLabel}
                          >
                            <span>{getDecodedString(testPermission.expiry_date)}</span>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </Table>
                </div>
              </div>
            </div>
          </section>
        )}
        {this.state.testPermissions.length === 0 && !this.state.isLoadingTestPermissions && (
          <section
            aria-labelledby="test-permissions-section"
            style={styles.testPermissionsTable.spacer}
            tabIndex={0}
          >
            <h2 id="test-permissions-section">
              {LOCALIZE.systemAdministrator.userLookUpDetails.testPermissions.title}
            </h2>
            <div style={styles.container}>
              <p>
                {LOCALIZE.formatString(
                  LOCALIZE.systemAdministrator.userLookUpDetails.testPermissions.description,
                  this.props.selectedUserFirstname,
                  this.props.selectedUserLastname
                )}
              </p>
              <div style={styles.tableContainer}>
                <div style={styles.testPermissionsTable.title}>
                  <span>{LOCALIZE.profile.testPermissions.table.title}</span>
                </div>
                <table style={styles.testPermissionsTable.containerEmpty} tabIndex={0}>
                  <tbody>
                    <tr>
                      <td style={styles.tableNoPermissionPadding}>
                        {
                          LOCALIZE.systemAdministrator.userLookUpDetails.testPermissions
                            .noTestPermissions
                        }
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </section>
        )}
        {this.state.isLoadingTestPermissions && (
          <div style={styles.loading}>
            {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
            <label className="fa fa-spinner fa-spin">
              <FontAwesomeIcon icon={faSpinner} />
            </label>
          </div>
        )}
      </div>
    );
  }
}
const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    selectedUsername: state.userLookUp.selectedUsername,
    selectedUserFirstname: state.userLookUp.selectedUserFirstname,
    selectedUserLastname: state.userLookUp.selectedUserLastname
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ getSelectedUserTestPermissions }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(UserTestPermissions);
