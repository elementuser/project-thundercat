import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import PropTypes from "prop-types";
import LOCALIZE from "../../../text_resources";
import {
  getSelectedUserPermissions,
  getSelectedUserPendingPermissions
} from "../../../modules/PermissionsRedux";
import UserTestPermissions from "./UserTestPermissions";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import GenericTable from "../../commons/GenericTable";

export const COMMON_STYLE = {
  CENTERED_TEXT: {
    textAlign: "center",
    padding: "12px"
  },
  LEFT_TEXT: {
    textAlign: "left",
    padding: "12px"
  }
};

const styles = {
  container: {
    padding: "12px 0 0 24px"
  },
  tableContainer: {
    margin: "10px 22px 0 0"
  },
  containerBasicStyle: {
    height: 60,

    display: "table",
    padding: "0 12px",
    borderTop: "1px solid #CECECE"
  },
  content: {
    width: "calc(100vw)",
    display: "table-cell",
    verticalAlign: "middle"
  },
  permissionsTable: {
    titleContainer: {
      width: "100%",
      height: 50,
      color: "white",
      backgroundColor: "#00565e",
      display: "table",
      paddingLeft: 12
    },
    title: {
      display: "table-cell",
      verticalAlign: "middle",
      fontWeight: "bold"
    },
    rowContainerBasicStyle: {
      width: "100%",
      minHeight: 60,
      padding: "8px 0 8px 12px",
      display: "table"
    },
    rowContainerDark: {
      backgroundColor: "#F3F3F3"
    },
    rowContainerLight: {
      backgroundColor: "white"
    },
    rowContainerBorder: {
      borderTop: "1px solid #CECECE"
    },
    existingPermissions: {
      label: {
        width: "30%",
        padding: "0 12px",
        display: "table-cell",
        verticalAlign: "middle"
      },
      description: {
        width: "55%",
        padding: "0 12px",
        display: "table-cell",
        verticalAlign: "middle"
      },
      status: {
        width: "15%",
        padding: "0 12px",
        display: "table-cell",
        textAlign: "center",
        verticalAlign: "middle"
      }
    }
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  loading: {
    width: "100%",
    height: 60,
    textAlign: "center",
    fontSize: "32px",
    padding: 6
  },
  noBorderBottom: {
    borderBottom: "none"
  }
};

export class UserRightsAndPermissions extends Component {
  static propTypes = {
    // Props from Redux
    getUserPermissions: PropTypes.func,
    getUserPendingPermissions: PropTypes.func
  };

  state = {
    permissions: [],
    noAvailablePermission: false,
    noAvailablePendingPermission: false,
    isLoading: false,
    rowsDefinition: {}
  };

  componentDidMount = () => {
    this.populateSelectedUserRights();
  };

  populateSelectedUserRights = () => {
    this.setState({ isLoading: true }, () => {
      const currentUserPermissions = [];
      // getting user' permissions
      this.props
        .getSelectedUserPermissions(this.props.selectedUsername)
        .then(response => {
          // if user is a super user
          if (this.props.isSuperUser) {
            currentUserPermissions.push({
              text: LOCALIZE.profile.permissions.systemPermissionInformation.superUser.title,
              description:
                LOCALIZE.profile.permissions.systemPermissionInformation.superUser.permission,
              pendingStatus: false
            });
          } else {
            // looping in user' permissions
            for (let i = 0; i < response.length; i++) {
              // if user is not a super user and has at least one permission
              if (response[0] !== "No Permission") {
                // pushing results in currentUserPermissions array
                currentUserPermissions.push({
                  text: response[i][`${this.props.currentLanguage}_name`],
                  description: response[i][`${this.props.currentLanguage}_description`],
                  pendingStatus: false
                });
              }
            }

            if (response.length === 0) {
              this.setState({ noAvailablePermission: true });
            }
          }
        })
        .then(() => {
          // getting user' pending permissions
          this.props
            .getSelectedUserPendingPermissions(this.props.selectedUsername)
            .then(pendingPermissionsResponse => {
              // if not a super user
              if (!this.props.isSuperUser) {
                for (let i = 0; i < pendingPermissionsResponse.length; i++) {
                  currentUserPermissions.push({
                    text: pendingPermissionsResponse[i][`${this.props.currentLanguage}_name`],
                    description:
                      pendingPermissionsResponse[i][`${this.props.currentLanguage}_description`],
                    pendingStatus: true
                  });
                }
              }

              if (pendingPermissionsResponse.length === 0) {
                this.setState({ noAvailablePendingPermission: true });
              }
              // saving populated array in the state
              this.setState({ permissions: currentUserPermissions });
              // trigger populate table rows
              this.populateTableRows();
            });
        })
        .then(() => {
          this.setState({ isLoading: false });
        });
    });
  };

  // populating table rows for the GenericTable component
  populateTableRows = () => {
    const { permissions } = this.state;

    // initializing needed object and array for rowsDefinition state (needed for GenericTable component)
    let rowsDefinition = {};
    const data = [];

    for (let i = 0; i < permissions.length; i++) {
      // pushing needed data in data array (needed for GenericTable component)
      data.push({
        column_1: permissions[i].text,
        column_2: permissions[i].description,
        column_3: permissions[i].pendingStatus
          ? LOCALIZE.profile.permissions.systemPermissionInformation.pending
          : ""
      });
    }

    // updating rowsDefinition object with provided data and needed style
    rowsDefinition = {
      column_1_style: COMMON_STYLE.LEFT_TEXT,
      column_2_style: COMMON_STYLE.LEFT_TEXT,
      column_3_style: COMMON_STYLE.LEFT_TEXT,
      data: data
    };

    this.setState({
      rowsDefinition: rowsDefinition
    });
  };

  // Populate Custom Header Content
  populateHeaderHtml = () => {
    return (
      <th colSpan={3} style={styles.noBorderBottom}>
        {LOCALIZE.profile.permissions.systemPermissionInformation.title}
      </th>
    );
  };

  render() {
    const columnsDefinition = [
      {
        label: ""
      },
      {
        label: ""
      },
      {
        label: ""
      }
    ];

    return (
      <div>
        <section aria-labelledby="permissions-section" tabIndex={0}>
          <h2 id="permissions-section">
            {LOCALIZE.systemAdministrator.userLookUpDetails.userRights.title}
          </h2>

          <div style={styles.container}>
            <p>
              {LOCALIZE.formatString(
                LOCALIZE.systemAdministrator.userLookUpDetails.userRights.description,
                this.props.selectedUserFirstname,
                this.props.selectedUserLastname
              )}
            </p>

            <div style={styles.tableContainer}>
              <GenericTable
                classnamePrefix="user-rights-permissions"
                columnsDefinition={columnsDefinition}
                rowsDefinition={this.state.rowsDefinition}
                emptyTableMessage={
                  LOCALIZE.systemAdministrator.userLookUpDetails.userRights.noRights
                }
                currentlyLoading={this.state.isLoadingTestPermissions}
                tableWithButtons={false}
                hasHeaderHtml={true}
                headerHtml={this.populateHeaderHtml()}
              ></GenericTable>
            </div>
          </div>
        </section>
        {this.state.isLoading && (
          <div style={styles.loading}>
            {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
            <label className="fa fa-spinner fa-spin">
              <FontAwesomeIcon icon={faSpinner} />
            </label>
          </div>
        )}
        <UserTestPermissions />
      </div>
    );
  }
}
const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    selectedUsername: state.userLookUp.selectedUsername,
    selectedUserFirstname: state.userLookUp.selectedUserFirstname,
    selectedUserLastname: state.userLookUp.selectedUserLastname
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ getSelectedUserPermissions, getSelectedUserPendingPermissions }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(UserRightsAndPermissions);
