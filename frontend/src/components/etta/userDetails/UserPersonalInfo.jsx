import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../../text_resources";
import { bindActionCreators } from "redux";
import { getSelectedUserPersonalInfo } from "../../../modules/UserLookUpRedux";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { getSelectedUserExtendedProfile } from "../../../modules/UserProfileRedux";
import moment from "moment";
import { Col, Row, Table } from "react-bootstrap";
import { getLineSpacingCSS } from "../../../modules/AccommodationsRedux";
import DatePicker from "../../commons/DatePicker";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 6,
    lg: 6,
    xl: 6
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 6,
    lg: 6,
    xl: 6
  }
};

const styles = {
  container: {
    marginBottom: 24
  },
  tableContainer: {
    marginBottom: 0
  },
  table: {
    width: "100%",
    borderWidth: 1,
    borderStyle: "solid",
    borderColor: "#CECECE",
    borderTop: "1px solid #00565e",
    tableLayout: "fixed"
  },
  loading: {
    width: "100%",
    minHeight: 60,
    textAlign: "center",
    fontSize: "32px",
    padding: 6
  },
  userInfoTable: {
    title: {
      height: 50,
      color: "white",
      backgroundColor: "#00565e",
      verticalAlign: "middle",
      fontWeight: "bold",
      paddingLeft: 12,
      borderBottom: "none"
    },
    rowContainerBasicStyle: {
      width: "100%",
      minHeight: 60,
      display: "table"
    },
    container: {
      borderCollapse: "collapse",
      margin: 24
    },
    inputContainer: {
      verticalAlign: "middle",
      padding: 0
    },
    itemContainer: {
      width: "100%",
      margin: "12px auto"
    },
    labelContainer: {
      verticalAlign: "middle",
      padding: "0 12px 0 0"
    }
  },
  input: {
    width: "100%",
    minHeight: 38,
    padding: "3px 6px 3px 6px",
    borderRadius: 4
  },
  userInfoLeftColumn: {
    fontWeight: "bold",
    border: "none"
  },
  userInfoRightColumn: {
    verticalAlign: "middle",
    border: "none"
  },
  noBorderBottom: {
    borderBottom: "none"
  },
  emptyTableTd: {
    border: "none",
    verticalAlign: "middle"
  }
};

export class UserPersonalInfo extends Component {
  state = {
    selectedUserPersonalInfo: [],
    selectedUserAdditionalInfo: [],
    isLoading: false,
    lastLoginDate: ""
  };

  componentDidMount = () => {
    // get selected user personal Info
    this.getSelectedUserPersonalInfo();
    // get selected user extended profile Information
    this.getSelectedUserAdditionalInfo();
  };

  getSelectedUserPersonalInfo = () => {
    this.setState({ isLoading: true }, () => {
      const selectedUserPersonalInfoArray = [];
      this.props
        .getSelectedUserPersonalInfo(this.props.selectedUsername)
        .then(response => {
          if (response.length > 0) {
            selectedUserPersonalInfoArray.push({
              firstName: response[0].first_name,
              lastName: response[0].last_name,
              primaryEmail: response[0].email,
              secondaryEmail: response[0].secondary_email,
              dateOfBirth: response[0].birth_date,
              psrsApplicantId: response[0].psrs_applicant_id,
              pri: response[0].pri,
              military_nbr: response[0].military_service_nbr,
              lastLogin: this.formattedLastLoginDate(response[0].last_login),
              lastPasswordChange: response[0].last_password_change,
              accountCreationDate: response[0].date_joined
            });
            this.setState({ selectedUserPersonalInfo: selectedUserPersonalInfoArray[0] });
          }
        })
        .then(() => {
          this.setState({ isLoading: false });
        });
    });
  };

  getSelectedUserAdditionalInfo = () => {
    this.setState({ isLoading: true }, () => {
      const selectedUserExtendedProfileInfoArray = [];
      this.props
        .getSelectedUserExtendedProfile(this.props.selectedUsername)
        .then(response => {
          if (response.body.message !== "no extended profile for the requested user") {
            selectedUserExtendedProfileInfoArray.push({
              currentEmployer: `${response.body[`employer_${this.props.currentLanguage}_name`]}`,
              organization: `${response.body[`organization_${this.props.currentLanguage}_name`]}`,
              group: response.body.group,
              subGroup: response.body.sub_group,
              level: response.body.level,
              residence: `${response.body[`residence_${this.props.currentLanguage}_name`]}`,
              education: `${response.body[`education_${this.props.currentLanguage}_name`]}`,
              gender: `${response.body[`gender_${this.props.currentLanguage}_name`]}`
            });
            this.setState({ selectedUserAdditionalInfo: selectedUserExtendedProfileInfoArray[0] });
          }
        })
        .then(() => {
          this.setState({ isLoading: false });
        });
    });
  };

  // format last login date
  formattedLastLoginDate = lastLoginDate => {
    if (lastLoginDate !== null) {
      return moment(lastLoginDate).format("YYYY-MM-DD");
    }
  };

  render() {
    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <>
        <div style={styles.container}>
          <Table responsive style={styles.tableContainer}>
            <thead>
              <tr style={styles.userInfoTable.title}>
                <th style={styles.noBorderBottom}>
                  <span>
                    {LOCALIZE.systemAdministrator.userLookUpDetails.userPersonalInfo.title}
                  </span>
                </th>
              </tr>
            </thead>
            {!this.state.isLoading ? (
              <div style={styles.userInfoTable.container}>
                <Row
                  className="align-items-center justify-content-start"
                  id="personal-info-first-name"
                  style={styles.userInfoTable.itemContainer}
                  role="presentation"
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.userInfoTable.labelContainer}
                  >
                    <label id="first-name" htmlFor="first-name-input">
                      {LOCALIZE.systemAdministrator.userLookUpDetails.userPersonalInfo.firstName}
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.userInfoTable.inputContainer}
                  >
                    <input
                      id="first-name-input"
                      disabled={true}
                      className={"valid-field"}
                      aria-required={true}
                      style={{ ...styles.input, ...accommodationsStyle }}
                      type="text"
                      value={this.state.selectedUserPersonalInfo.firstName}
                      onChange={() => {}}
                    />
                  </Col>
                </Row>
                <Row
                  className="align-items-center justify-content-start"
                  id="personal-info-last-name"
                  style={styles.userInfoTable.itemContainer}
                  role="presentation"
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.userInfoTable.labelContainer}
                  >
                    <label id="last-name" htmlFor="last-name-input">
                      {LOCALIZE.systemAdministrator.userLookUpDetails.userPersonalInfo.lastName}
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.userInfoTable.inputContainer}
                  >
                    <input
                      id="last-name-input"
                      disabled={true}
                      className={"valid-field"}
                      aria-required={true}
                      style={{ ...styles.input, ...accommodationsStyle }}
                      type="text"
                      value={this.state.selectedUserPersonalInfo.lastName}
                      onChange={() => {}}
                    />
                  </Col>
                </Row>
                <Row
                  className="align-items-center justify-content-start"
                  id="personal-info-primary-email-address"
                  style={styles.userInfoTable.itemContainer}
                  role="presentation"
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.userInfoTable.labelContainer}
                  >
                    <label id="primary-email-address" htmlFor="primary-email-address-input">
                      {
                        LOCALIZE.systemAdministrator.userLookUpDetails.userPersonalInfo
                          .primaryEmailAddress
                      }
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.userInfoTable.inputContainer}
                  >
                    <input
                      id="primary-email-address-input"
                      disabled={true}
                      className={"valid-field"}
                      aria-required={true}
                      style={{ ...styles.input, ...accommodationsStyle }}
                      type="text"
                      value={this.state.selectedUserPersonalInfo.primaryEmail}
                      onChange={() => {}}
                    />
                  </Col>
                </Row>
                <Row
                  className="align-items-center justify-content-start"
                  id="personal-info-secondary-email-address"
                  style={styles.userInfoTable.itemContainer}
                  role="presentation"
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.userInfoTable.labelContainer}
                  >
                    <label id="secondary-email-address" htmlFor="secondary-email-address-input">
                      {
                        LOCALIZE.systemAdministrator.userLookUpDetails.userPersonalInfo
                          .secondaryEmailAddress
                      }
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.userInfoTable.inputContainer}
                  >
                    <input
                      id="secondary-email-address-input"
                      disabled={true}
                      className={"valid-field"}
                      aria-required={true}
                      style={{ ...styles.input, ...accommodationsStyle }}
                      type="text"
                      value={this.state.selectedUserPersonalInfo.secondaryEmail}
                      onChange={() => {}}
                    />
                  </Col>
                </Row>
                <Row
                  className="align-items-center justify-content-start"
                  id="personal-info-dob"
                  style={styles.userInfoTable.itemContainer}
                  role="presentation"
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.userInfoTable.labelContainer}
                  >
                    <label id="dob" htmlFor="dob-input">
                      {LOCALIZE.systemAdministrator.userLookUpDetails.userPersonalInfo.dateOfBirth}
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.userInfoTable.inputContainer}
                  >
                    <DatePicker
                      ariaLabelledBy={"date-of-birth-tooltip-for-accessibility"}
                      dateLabelId={"dob"}
                      initialDateDayValue={
                        typeof this.state.selectedUserPersonalInfo.dateOfBirth !== "undefined" && {
                          label: this.state.selectedUserPersonalInfo.dateOfBirth.split("-")[2],
                          value: Number(
                            this.state.selectedUserPersonalInfo.dateOfBirth.split("-")[2]
                          )
                        }
                      }
                      initialDateMonthValue={
                        typeof this.state.selectedUserPersonalInfo.dateOfBirth !== "undefined" && {
                          label: this.state.selectedUserPersonalInfo.dateOfBirth.split("-")[1],
                          value: Number(
                            this.state.selectedUserPersonalInfo.dateOfBirth.split("-")[1]
                          )
                        }
                      }
                      initialDateYearValue={
                        typeof this.state.selectedUserPersonalInfo.dateOfBirth !== "undefined" && {
                          label: this.state.selectedUserPersonalInfo.dateOfBirth.split("-")[0],
                          value: Number(
                            this.state.selectedUserPersonalInfo.dateOfBirth.split("-")[0]
                          )
                        }
                      }
                      disabledDropdowns={true}
                      displayValidIcon={false}
                    />
                  </Col>
                </Row>
                <Row
                  className="align-items-center justify-content-start"
                  id="personal-info-psrs-applicant"
                  style={styles.userInfoTable.itemContainer}
                  role="presentation"
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.userInfoTable.labelContainer}
                  >
                    <label id="psrs-applicant" htmlFor="psrs-applicant-input">
                      {
                        LOCALIZE.systemAdministrator.userLookUpDetails.userPersonalInfo
                          .psrsApplicantId
                      }
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.userInfoTable.inputContainer}
                  >
                    <input
                      id="psrs-applicant-input"
                      disabled={true}
                      className={"valid-field"}
                      aria-required={true}
                      style={{ ...styles.input, ...accommodationsStyle }}
                      type="text"
                      value={this.state.selectedUserPersonalInfo.psrsApplicantId}
                      onChange={() => {}}
                    />
                  </Col>
                </Row>
                <Row
                  className="align-items-center justify-content-start"
                  id="personal-info-pri-or-service-nbr"
                  style={styles.userInfoTable.itemContainer}
                  role="presentation"
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.userInfoTable.labelContainer}
                  >
                    <label id="pri-or-service-nbr" htmlFor="pri-or-service-nbr-input">
                      {LOCALIZE.systemAdministrator.userLookUpDetails.userPersonalInfo.pri}
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.userInfoTable.inputContainer}
                  >
                    <input
                      id="pri-or-service-nbr-input"
                      disabled={true}
                      className={"valid-field"}
                      aria-required={true}
                      style={{ ...styles.input, ...accommodationsStyle }}
                      type="text"
                      value={this.state.selectedUserPersonalInfo.pri}
                      onChange={() => {}}
                    />
                  </Col>
                </Row>
                <Row
                  className="align-items-center justify-content-start"
                  id="personal-info-last-login"
                  style={styles.userInfoTable.itemContainer}
                  role="presentation"
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.userInfoTable.labelContainer}
                  >
                    <label id="last-login" htmlFor="last-login-input">
                      {LOCALIZE.systemAdministrator.userLookUpDetails.userPersonalInfo.lastLogin}
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.userInfoTable.inputContainer}
                  >
                    <input
                      id="last-login-input"
                      disabled={true}
                      className={"valid-field"}
                      aria-required={true}
                      style={{ ...styles.input, ...accommodationsStyle }}
                      type="text"
                      value={this.state.selectedUserPersonalInfo.lastLogin}
                      onChange={() => {}}
                    />
                  </Col>
                </Row>
                <Row
                  className="align-items-center justify-content-start"
                  id="personal-info-last-password-change"
                  style={styles.userInfoTable.itemContainer}
                  role="presentation"
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.userInfoTable.labelContainer}
                  >
                    <label id="last-password-change" htmlFor="last-password-change-input">
                      {
                        LOCALIZE.systemAdministrator.userLookUpDetails.userPersonalInfo
                          .lastPasswordChange
                      }
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.userInfoTable.inputContainer}
                  >
                    <input
                      id="last-password-change-input"
                      disabled={true}
                      className={"valid-field"}
                      aria-required={true}
                      style={{ ...styles.input, ...accommodationsStyle }}
                      type="text"
                      value={this.state.selectedUserPersonalInfo.lastPasswordChange}
                      onChange={() => {}}
                    />
                  </Col>
                </Row>
                <Row
                  className="align-items-center justify-content-start"
                  id="personal-info-account-creation-date"
                  style={styles.userInfoTable.itemContainer}
                  role="presentation"
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.userInfoTable.labelContainer}
                  >
                    <label id="account-creation-date" htmlFor="account-creation-date-input">
                      {
                        LOCALIZE.systemAdministrator.userLookUpDetails.userPersonalInfo
                          .accountCreationDate
                      }
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.userInfoTable.inputContainer}
                  >
                    <input
                      id="account-creation-date-input"
                      disabled={true}
                      className={"valid-field"}
                      aria-required={true}
                      style={{ ...styles.input, ...accommodationsStyle }}
                      type="text"
                      value={
                        typeof this.state.selectedUserPersonalInfo.accountCreationDate !==
                          "undefined" &&
                        this.state.selectedUserPersonalInfo.accountCreationDate.split("T")[0]
                      }
                      onChange={() => {}}
                    />
                  </Col>
                </Row>
              </div>
            ) : (
              <tbody>
                {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
                <tr>
                  <td>
                    <label
                      htmlFor="spinner"
                      style={styles.loading}
                      className="fa fa-spinner fa-spin"
                    >
                      <FontAwesomeIcon id="spinner" icon={faSpinner} />
                    </label>
                  </td>
                </tr>
              </tbody>
            )}
          </Table>
        </div>

        <div>
          <Table responsive style={styles.tableContainer}>
            <thead>
              <tr style={styles.userInfoTable.title}>
                <th style={styles.noBorderBottom}>
                  <span>
                    {LOCALIZE.systemAdministrator.userLookUpDetails.userPersonalInfo.additionalInfo}
                  </span>
                </th>
              </tr>
            </thead>

            {!this.state.isLoading ? (
              <div style={styles.userInfoTable.container}>
                <Row
                  className="align-items-center justify-content-start"
                  id="additional-info-current-employer"
                  style={styles.userInfoTable.itemContainer}
                  role="presentation"
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.userInfoTable.labelContainer}
                  >
                    <label id="current-employer" htmlFor="current-employer-input">
                      {
                        LOCALIZE.systemAdministrator.userLookUpDetails.userPersonalInfo
                          .currentEmployer
                      }
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.userInfoTable.inputContainer}
                  >
                    <input
                      id="current-employer-input"
                      disabled={true}
                      className={"valid-field"}
                      aria-required={true}
                      style={{ ...styles.input, ...accommodationsStyle }}
                      type="text"
                      value={this.state.selectedUserAdditionalInfo.currentEmployer}
                      onChange={() => {}}
                    />
                  </Col>
                </Row>
                <Row
                  className="align-items-center justify-content-start"
                  id="additional-info-organization"
                  style={styles.userInfoTable.itemContainer}
                  role="presentation"
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.userInfoTable.labelContainer}
                  >
                    <label id="organization" htmlFor="organization-input">
                      {LOCALIZE.systemAdministrator.userLookUpDetails.userPersonalInfo.organization}
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.userInfoTable.inputContainer}
                  >
                    <input
                      id="organization-input"
                      disabled={true}
                      className={"valid-field"}
                      aria-required={true}
                      style={{ ...styles.input, ...accommodationsStyle }}
                      type="text"
                      value={this.state.selectedUserAdditionalInfo.organization}
                      onChange={() => {}}
                    />
                  </Col>
                </Row>
                <Row
                  className="align-items-center justify-content-start"
                  id="additional-info-group"
                  style={styles.userInfoTable.itemContainer}
                  role="presentation"
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.userInfoTable.labelContainer}
                  >
                    <label id="group" htmlFor="group-input">
                      {LOCALIZE.systemAdministrator.userLookUpDetails.userPersonalInfo.group}
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.userInfoTable.inputContainer}
                  >
                    <input
                      id="group-input"
                      disabled={true}
                      className={"valid-field"}
                      aria-required={true}
                      style={{ ...styles.input, ...accommodationsStyle }}
                      type="text"
                      value={this.state.selectedUserAdditionalInfo.group}
                      onChange={() => {}}
                    />
                  </Col>
                </Row>
                <Row
                  className="align-items-center justify-content-start"
                  id="additional-info-sub-group"
                  style={styles.userInfoTable.itemContainer}
                  role="presentation"
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.userInfoTable.labelContainer}
                  >
                    <label id="sub-group" htmlFor="sub-group-input">
                      {LOCALIZE.systemAdministrator.userLookUpDetails.userPersonalInfo.subGroup}
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.userInfoTable.inputContainer}
                  >
                    <input
                      id="sub-group-input"
                      disabled={true}
                      className={"valid-field"}
                      aria-required={true}
                      style={{ ...styles.input, ...accommodationsStyle }}
                      type="text"
                      value={this.state.selectedUserAdditionalInfo.subGroup}
                      onChange={() => {}}
                    />
                  </Col>
                </Row>
                <Row
                  className="align-items-center justify-content-start"
                  id="additional-info-level"
                  style={styles.userInfoTable.itemContainer}
                  role="presentation"
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.userInfoTable.labelContainer}
                  >
                    <label id="level" htmlFor="level-input">
                      {LOCALIZE.systemAdministrator.userLookUpDetails.userPersonalInfo.level}
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.userInfoTable.inputContainer}
                  >
                    <input
                      id="level-input"
                      disabled={true}
                      className={"valid-field"}
                      aria-required={true}
                      style={{ ...styles.input, ...accommodationsStyle }}
                      type="text"
                      value={this.state.selectedUserAdditionalInfo.level}
                      onChange={() => {}}
                    />
                  </Col>
                </Row>
                <Row
                  className="align-items-center justify-content-start"
                  id="additional-info-residence"
                  style={styles.userInfoTable.itemContainer}
                  role="presentation"
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.userInfoTable.labelContainer}
                  >
                    <label id="residence" htmlFor="residence-input">
                      {LOCALIZE.systemAdministrator.userLookUpDetails.userPersonalInfo.residence}
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.userInfoTable.inputContainer}
                  >
                    <input
                      id="residence-input"
                      disabled={true}
                      className={"valid-field"}
                      aria-required={true}
                      style={{ ...styles.input, ...accommodationsStyle }}
                      type="text"
                      value={this.state.selectedUserAdditionalInfo.residence}
                      onChange={() => {}}
                    />
                  </Col>
                </Row>
                <Row
                  className="align-items-center justify-content-start"
                  id="additional-info-education"
                  style={styles.userInfoTable.itemContainer}
                  role="presentation"
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.userInfoTable.labelContainer}
                  >
                    <label id="education" htmlFor="education-input">
                      {LOCALIZE.systemAdministrator.userLookUpDetails.userPersonalInfo.education}
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.userInfoTable.inputContainer}
                  >
                    <input
                      id="education-input"
                      disabled={true}
                      className={"valid-field"}
                      aria-required={true}
                      style={{ ...styles.input, ...accommodationsStyle }}
                      type="text"
                      value={this.state.selectedUserAdditionalInfo.education}
                      onChange={() => {}}
                    />
                  </Col>
                </Row>
                <Row
                  className="align-items-center justify-content-start"
                  id="additional-info-gender"
                  style={styles.userInfoTable.itemContainer}
                  role="presentation"
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.userInfoTable.labelContainer}
                  >
                    <label id="gender" htmlFor="gender-input">
                      {LOCALIZE.systemAdministrator.userLookUpDetails.userPersonalInfo.gender}
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.userInfoTable.inputContainer}
                  >
                    <input
                      id="gender-input"
                      disabled={true}
                      className={"valid-field"}
                      aria-required={true}
                      style={{ ...styles.input, ...accommodationsStyle }}
                      type="text"
                      value={this.state.selectedUserAdditionalInfo.gender}
                      onChange={() => {}}
                    />
                  </Col>
                </Row>
              </div>
            ) : (
              <tbody>
                {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
                <tr>
                  <td>
                    <label
                      htmlFor="spinner"
                      style={styles.loading}
                      className="fa fa-spinner fa-spin"
                    >
                      <FontAwesomeIcon id="spinner" icon={faSpinner} />
                    </label>
                  </td>
                </tr>
              </tbody>
            )}
          </Table>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    selectedUsername: state.userLookUp.selectedUsername
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getSelectedUserPersonalInfo,
      getSelectedUserExtendedProfile
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(UserPersonalInfo);
