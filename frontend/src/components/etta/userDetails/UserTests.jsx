import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import LOCALIZE from "../../../text_resources";
import GenericTable, { COMMON_STYLE } from "../../commons/GenericTable";
import { getAllTests } from "../../../modules/MyTestsRedux";
import CustomButton, { THEME } from "../../commons/CustomButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faTimesCircle,
  faTimes,
  faBinoculars,
  faDownload
} from "@fortawesome/free-solid-svg-icons";
import {
  faSquare,
  faCheckSquare,
  faTimesCircle as regularFaTimesCircle
} from "@fortawesome/free-regular-svg-icons";
import getRetestDate from "../../../helpers/retestPeriodCalculations";
import getConvertedScore from "../../../helpers/scoreConversion";
import getFormattedTestStatus from "../../ta/TestStatus";
import {
  invalidateCandidateAsEtta,
  validateCandidateAsEtta
} from "../../../modules/ActiveTestsRedux";
import SystemMessage, { MESSAGE_TYPE } from "../../commons/SystemMessage";
import PopupBox, { BUTTON_TYPE, BUTTON_STATE } from "../../commons/PopupBox";
import getInvalidateTestReasonRedux from "../../../modules/InvalidateTestRedux";
import StyledTooltip, { TYPE, EFFECT, TRIGGER_TYPE } from "../../authentication/StyledTooltip";
import { Row, Col } from "react-bootstrap";
import {
  getAllExistingTestOrderNumbers,
  getCandidateActionsReportData
} from "../../../modules/ReportsRedux";
import generateCandidateActionsReport from "../../commons/reports/GenerateCandidateActionsReport";
import { CSVLink } from "react-csv";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 5,
    xl: 5
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 7,
    xl: 7
  }
};

const styles = {
  buttonIcon: {
    marginRight: 6
  },
  input: {
    width: "100%",
    minHeight: 38,
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565e",
    borderRadius: 4
  },
  textAreaInput: {
    height: 85,
    overflowY: "auto",
    resize: "none",
    display: "block",
    width: "calc(100% - 0.5px)"
  },
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  allUnset: {
    all: "unset"
  },
  popupTestInfoContainer: {
    margin: "12px 0"
  },
  popupTestInfoItem: {
    margin: "0 36px"
  },
  popupTestInfoLeftColumn: {
    fontWeight: "bold"
  },
  popupTestInfoRightColumn: {
    verticalAlign: "middle"
  },
  faTimesCircle: {
    width: "100%",
    minHeight: 20,
    textAlign: "center",
    color: "#cc0000"
  },
  regularFaTimesCircle: {
    width: "100%",
    minHeight: 20,
    textAlign: "center",
    color: "#00565e"
  },
  faBinoculars: {
    width: "100%",
    minHeight: 20,
    textAlign: "center"
  },
  faCheck: {
    color: "#00565e"
  },
  bold: {
    fontWeight: "bold"
  }
};
export class UserTests extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  constructor(props) {
    super(props);
    this.createReportLinkButtonRefTemp = React.createRef();
  }

  state = {
    rowsDefinition: {},
    currentlyLoading: false,
    showInvalidatePopup: false,
    showValidatePopup: false,
    selectedTestToAction: {},
    isValidReasonForInvalidatingTest: true,
    reasonForInvalidatingTest: "",
    invalidationLoading: false,
    isValidReasonForValidatingTest: true,
    reasonForValidatingTest: "",
    validationLoading: false,
    allTests: [],
    viewSelectedTest: {},
    viewSelectedTestPopup: false,
    reportCreationLoading: false,
    formattedReportData: [],
    reportName: ""
  };

  componentDidMount = () => {
    this.populateUserTests();
  };

  componentDidUpdate = prevProps => {
    // if accommodations gets updated
    if (prevProps.accommodations !== this.props.accommodations) {
      this.populateUserTests();
    }
  };

  componentWillUnmount() {
    this._isMounted = false;
  }

  populateUserTests = () => {
    this._isMounted = true;
    // initializing needed object and array for rowsDefinition props (needed for GenericTable component)
    let rowsDefinition = {};
    const data = [];
    const allTestsArray = [];
    this.setState({ currentlyLoading: true }, () => {
      this.props
        .getAllTests(this.props.selectedUsername)
        .then(response => {
          if (this._isMounted) {
            for (let i = 0; i < response.length; i++) {
              // pushing needed data in allTestsArray
              allTestsArray.push({
                id: response[i].id,
                test: response[i].test,
                test_name: `${response[i].test[`${this.props.currentLanguage}_name`]}`,
                retest_date:
                  parseInt(response[i].test.retest_period) !== 0
                    ? getRetestDate(
                        response[i].start_date,
                        parseInt(response[i].test.retest_period)
                      )
                    : LOCALIZE.commons.na,
                score: getConvertedScore(
                  response[i][`${this.props.currentLanguage}_converted_score`],
                  response[i].status
                ),
                status: getFormattedTestStatus(Number(response[i].status)),
                test_access_code: response[i].test_access_code,
                ta_email: response[i].ta,
                ta_name: `${response[i].ta_last_name}, ${response[i].ta_first_name}`,
                test_order_number: response[i].test_order_number,
                total_score: response[i].total_score,
                invalidTestReason: response[i].invalidate_test_reason,
                assessment_process_reference_nbr: response[i].assessment_process_reference_nbr
              });
              // pushing needed data in data array (needed for GenericTable component)
              data.push({
                column_1: `${response[i].test.test_code}`,
                column_2:
                  response[i].start_date !== null
                    ? response[i].start_date.split("T")[0]
                    : LOCALIZE.commons.na,
                column_3:
                  parseInt(response[i].test.retest_period) !== 0
                    ? getRetestDate(
                        response[i].start_date,
                        parseInt(response[i].test.retest_period)
                      )
                    : LOCALIZE.commons.na,
                column_4: getConvertedScore(
                  response[i][`${this.props.currentLanguage}_converted_score`],
                  response[i].status
                ),
                column_5: getFormattedTestStatus(Number(response[i].status)),
                column_6: this.getFormattedTestValidityFlag(response[i].is_invalid),
                column_7: this.populateColumnSix(i, response[i])
              });
            }
          }
          // updating rowsDefinition object with provided data and needed style
          rowsDefinition = {
            column_1_style: COMMON_STYLE.LEFT_TEXT,
            column_2_style: COMMON_STYLE.LEFT_TEXT,
            column_3_style: COMMON_STYLE.LEFT_TEXT,
            column_4_style: COMMON_STYLE.CENTERED_TEXT,
            column_5_style: COMMON_STYLE.CENTERED_TEXT,
            column_6_style: COMMON_STYLE.CENTERED_TEXT,
            column_7_style: COMMON_STYLE.CENTERED_TEXT,
            data: data
          };

          // saving results in state
          this.setState({
            rowsDefinition: rowsDefinition,
            allTests: allTestsArray
          });
        })
        .then(() => {
          if (this._isMounted) {
            this.setState({ currentlyLoading: false });
          }
        });
    });
  };

  getFormattedTestValidityFlag = isInvalid => {
    const customCheckboxFontSize = {
      // adding 6px to selected font-size
      fontSize: `${parseInt(this.props.accommodations.fontSize.split("px")[0]) + 6}px`
    };
    return (
      <div>
        <FontAwesomeIcon
          icon={isInvalid ? faCheckSquare : faSquare}
          style={{ ...styles.faCheck, ...customCheckboxFontSize }}
        />
      </div>
    );
  };

  runGenerateCandidateActionsReport = response => {
    this.setState({
      reportName: `${LOCALIZE.reports.reportTypes.candidateActionsReport} - ${response.test_order_number} - ${response.test.test_code} - ${response.username}.csv`
    });

    this.props
      // .getCandidateActionsReportData(this.props.selectedAssignedTest.value)
      .getCandidateActionsReportData(response.id)
      .then(response => {
        // getting an array
        if (response.length >= 0) {
          this.handleReportDataFormatting(response);
          console.log(response);
          // should never happen
        } else {
          // display no data popup
          this.setState({ displayNoDataError: true });
        }
      });
  };

  // formatting report data
  handleReportDataFormatting = reportData => {
    // if there is at least one entry based on the provided parameters

    // initializing variables
    let formattedReportData = [];

    // generating report data
    formattedReportData = generateCandidateActionsReport(reportData);

    this.setState({ formattedReportData: formattedReportData }, () => {
      this.setState({ reportCreationLoading: false }, () => {
        // once all data has been created, simulate click on hidden create report link to generate the downloadable csv file
        this.createReportLinkButtonRefTemp.current.link.click();
      });
    });
  };

  populateColumnSix = (i, response) => {
    return (
      <div className="d-flex justify-content-center flex-nowrap">
        <StyledTooltip
          id={`generate-candidate-actions-report-specific-test-${i}`}
          place="top"
          type={TYPE.light}
          effect={EFFECT.solid}
          triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
          tooltipElement={
            <div style={styles.allUnset}>
              <CustomButton
                dataTip=""
                dataFor={`generate-candidate-actions-report-specific-test-${i}`}
                label={<FontAwesomeIcon icon={faDownload} style={styles.faBinoculars} />}
                action={() => this.runGenerateCandidateActionsReport(response)}
                buttonTheme={THEME.SECONDARY}
                customStyle={styles.actionButton}
                ariaLabel={LOCALIZE.formatString(
                  LOCALIZE.systemAdministrator.userLookUpDetails.Tests.table
                    .candidateActionReportAria,
                  response.test.test_code
                )}
              />
            </div>
          }
          tooltipContent={
            <div>
              <p>
                {
                  LOCALIZE.systemAdministrator.userLookUpDetails.Tests.table
                    .downloadCandidateActionReport
                }
              </p>
            </div>
          }
        />

        <StyledTooltip
          id={`view-selected-user-specific-test-${i}`}
          place="top"
          type={TYPE.light}
          effect={EFFECT.solid}
          triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
          tooltipElement={
            <div style={styles.allUnset}>
              <CustomButton
                dataTip=""
                dataFor={`view-selected-user-specific-test-${i}`}
                label={<FontAwesomeIcon icon={faBinoculars} style={styles.faBinoculars} />}
                action={() => {
                  this.viewTestDetailsPopup(i);
                }}
                customStyle={styles.actionButton}
                buttonTheme={THEME.SECONDARY}
                ariaLabel={LOCALIZE.formatString(
                  LOCALIZE.systemAdministrator.userLookUpDetails.Tests.table
                    .viewButtonAccessibility,
                  `${response.test.test_code}`
                )}
              />
            </div>
          }
          tooltipContent={
            <div>
              <p>
                {LOCALIZE.systemAdministrator.userLookUpDetails.Tests.table.viewTestDetailsTooltip}
              </p>
            </div>
          }
        />
        <StyledTooltip
          id={`selected-user-invalidate-validate-specific-test-${i}`}
          place="top"
          type={TYPE.light}
          effect={EFFECT.solid}
          triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
          tooltipElement={
            <div style={styles.allUnset}>
              <CustomButton
                dataTip=""
                dataFor={`selected-user-invalidate-validate-specific-test-${i}`}
                label={
                  <FontAwesomeIcon
                    icon={response.is_invalid ? regularFaTimesCircle : faTimesCircle}
                    style={response.is_invalid ? styles.regularFaTimesCircle : styles.faTimesCircle}
                  />
                }
                action={
                  response.is_invalid
                    ? () => {
                        this.openValidatePopup(i);
                      }
                    : () => {
                        this.openInvalidatePopup(i);
                      }
                }
                buttonTheme={THEME.SECONDARY}
                customStyle={styles.actionButton}
                ariaLabel={
                  response.is_invalid
                    ? LOCALIZE.formatString(
                        LOCALIZE.systemAdministrator.userLookUpDetails.Tests.table
                          .validateButtonAccessibility,
                        `${response.test.test_code}`
                      )
                    : LOCALIZE.formatString(
                        LOCALIZE.systemAdministrator.userLookUpDetails.Tests.table
                          .invalidateButtonAccessibility,
                        `${response.test.test_code}`
                      )
                }
              />
            </div>
          }
          tooltipContent={
            <div>
              <p>
                {response.is_invalid
                  ? LOCALIZE.systemAdministrator.userLookUpDetails.Tests.table.validateTestTooltip
                  : LOCALIZE.systemAdministrator.userLookUpDetails.Tests.table
                      .invalidateTestTooltip}
              </p>
            </div>
          }
        />
      </div>
    );
  };

  // open view test details pop up
  viewTestDetailsPopup = id => {
    this.setState({
      viewSelectedTestPopup: true,
      viewSelectedTest: this.state.allTests[id]
    });
  };

  // close view test details pop up
  closeTestDetailsPopup = () => {
    this.setState({
      viewSelectedTestPopup: false,
      viewSelectedTest: {}
    });
  };

  // open pop up
  openInvalidatePopup = id => {
    this.setState({
      showInvalidatePopup: true,
      selectedTestToAction: this.state.allTests[id]
    });
  };

  //  close pop up
  closeInvalidatePopup = () => {
    this.setState({
      showInvalidatePopup: false,
      selectedTestToAction: {},
      isValidReasonForInvalidatingTest: true,
      reasonForInvalidatingTest: "",
      invalidationLoading: false
    });
  };

  // open pop up
  openValidatePopup = id => {
    this.setState({
      showValidatePopup: true,
      selectedTestToAction: this.state.allTests[id]
    });
  };

  //  close pop up
  closeValidatePopup = () => {
    this.setState({
      showValidatePopup: false,
      selectedTestToAction: {},
      isValidReasonForValidatingTest: true,
      reasonForValidatingTest: "",
      validationLoading: false
    });
  };

  updateReasonForInvalidatingTest = event => {
    const reasonForInvalidatingTest = event.target.value;
    if (Object.keys(reasonForInvalidatingTest).length !== 0) {
      this.setState({
        isValidReasonForInvalidatingTest: false,
        reasonForInvalidatingTest: reasonForInvalidatingTest
      });
    } else {
      this.setState({
        isValidReasonForInvalidatingTest: true,
        reasonForInvalidatingTest: reasonForInvalidatingTest
      });
    }
  };

  updateReasonForValidatingTest = event => {
    const reasonForValidatingTest = event.target.value;
    if (Object.keys(reasonForValidatingTest).length !== 0) {
      this.setState({
        isValidReasonForValidatingTest: false,
        reasonForValidatingTest: reasonForValidatingTest
      });
    } else {
      this.setState({
        isValidReasonForValidatingTest: true,
        reasonForValidatingTest: reasonForValidatingTest
      });
    }
  };

  handleInvalidate = () => {
    this.setState({ invalidationLoading: true }, () => {
      const data = {
        id: this.state.selectedTestToAction.id,
        username_id: this.props.selectedUsername,
        test_id: this.state.selectedTestToAction.test.id,
        invalidate_test_reason: this.state.reasonForInvalidatingTest
      };
      this.props.invalidateCandidateAsEtta(data).then(response => {
        // if request succeeded
        if (response.status === 200) {
          // update user tests table
          this.populateUserTests();
          // should never happen
        } else {
          throw new Error("Something went wrong during invalidating active tests");
        }
        // close pop up
        this.closeInvalidatePopup();
      });
    });
  };

  handleValidate = () => {
    this.setState({ validationLoading: true }, () => {
      const data = {
        id: this.state.selectedTestToAction.id,
        username_id: this.props.selectedUsername,
        test_id: this.state.selectedTestToAction.test.id,
        validate_test_reason: this.state.reasonForValidatingTest
      };
      this.props.validateCandidateAsEtta(data).then(response => {
        // if request succeeded
        if (response.status === 200) {
          // update user tests table
          this.populateUserTests();
          // should never happen
        } else {
          throw new Error("Something went wrong during validating active tests");
        }
        // close pop up
        this.closeValidatePopup();
      });
    });
  };

  getTADetails = (taName, taEmail) => {
    return `${taName} (${taEmail})`;
  };

  render() {
    const columnsDefinition = [
      {
        label: LOCALIZE.systemAdministrator.userLookUpDetails.Tests.table.columnOne,
        style: { ...{ width: "15%" }, ...COMMON_STYLE.LEFT_TEXT }
      },
      {
        label: LOCALIZE.systemAdministrator.userLookUpDetails.Tests.table.columnTwo,
        style: { ...{ width: "15%" }, ...COMMON_STYLE.LEFT_TEXT }
      },
      {
        label: LOCALIZE.systemAdministrator.userLookUpDetails.Tests.table.columnThree,
        style: { ...{ width: "15%" }, ...COMMON_STYLE.LEFT_TEXT }
      },
      {
        label: LOCALIZE.systemAdministrator.userLookUpDetails.Tests.table.columnFour,
        style: { ...{ width: "15%" }, ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label: LOCALIZE.systemAdministrator.userLookUpDetails.Tests.table.columnFive,
        style: { ...{ width: "15%" }, ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label: LOCALIZE.systemAdministrator.userLookUpDetails.Tests.table.columnSix,
        style: { ...{ width: "10%" }, ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label: LOCALIZE.systemAdministrator.userLookUpDetails.Tests.table.columnSeven,
        style: { ...{ width: "15%" }, ...COMMON_STYLE.CENTERED_TEXT }
      }
    ];
    return (
      <div>
        <h2>
          {LOCALIZE.formatString(
            LOCALIZE.systemAdministrator.userLookUpDetails.Tests.title,
            this.props.selectedUserFirstname,
            this.props.selectedUserLastname
          )}
        </h2>
        <div>
          <p>
            {LOCALIZE.formatString(
              LOCALIZE.systemAdministrator.userLookUpDetails.Tests.description,
              this.props.selectedUserFirstname,
              this.props.selectedUserLastname
            )}
          </p>
        </div>
        <div>
          <GenericTable
            classnamePrefix="selected-user-all-tests"
            columnsDefinition={columnsDefinition}
            rowsDefinition={this.state.rowsDefinition}
            emptyTableMessage={LOCALIZE.systemAdministrator.userLookUpDetails.Tests.table.noTests}
            currentlyLoading={this.state.currentlyLoading}
          />
        </div>
        <CSVLink
          ref={this.createReportLinkButtonRefTemp}
          asyncOnClick={true}
          data={this.state.formattedReportData}
          filename={this.state.reportName}
          enclosingCharacter=""
        ></CSVLink>
        <PopupBox
          show={this.state.viewSelectedTestPopup}
          handleClose={() => {}}
          isBackdropStatic={true}
          shouldCloseOnEsc={false}
          overflowVisible={false}
          title={LOCALIZE.systemAdministrator.userLookUpDetails.Tests.viewSelectedTestPopup.title}
          description={
            <>
              <p>
                {
                  LOCALIZE.systemAdministrator.userLookUpDetails.Tests.viewSelectedTestPopup
                    .description
                }
              </p>
              <div style={styles.popupTestInfoContainer}>
                <Row
                  className="align-items-top justify-content-start"
                  style={styles.popupTestInfoItem}
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.popupTestInfoLeftColumn}
                  >
                    <p>
                      {
                        LOCALIZE.systemAdministrator.userLookUpDetails.Tests.viewSelectedTestPopup
                          .testName
                      }
                    </p>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.popupTestInfoRightColumn}
                  >
                    <p>{this.state.viewSelectedTest.test_name}</p>
                  </Col>
                </Row>
                <Row
                  className="align-items-top justify-content-start"
                  style={styles.popupTestInfoItem}
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.popupTestInfoLeftColumn}
                  >
                    <p>
                      {
                        LOCALIZE.systemAdministrator.userLookUpDetails.Tests.viewSelectedTestPopup
                          .testCode
                      }
                    </p>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.popupTestInfoRightColumn}
                  >
                    <p>
                      {Object.keys(this.state.viewSelectedTest).length > 0 &&
                        this.state.viewSelectedTest.test.test_code}
                    </p>
                  </Col>
                </Row>
                <Row
                  className="align-items-top justify-content-start"
                  style={styles.popupTestInfoItem}
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.popupTestInfoLeftColumn}
                  >
                    <p>
                      {
                        LOCALIZE.systemAdministrator.userLookUpDetails.Tests.viewSelectedTestPopup
                          .version
                      }
                    </p>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.popupTestInfoRightColumn}
                  >
                    {Object.keys(this.state.viewSelectedTest).length > 0 &&
                      this.state.viewSelectedTest.test.version}
                  </Col>
                </Row>
                <Row
                  className="align-items-top justify-content-start"
                  style={styles.popupTestInfoItem}
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.popupTestInfoLeftColumn}
                  >
                    <p>
                      {
                        LOCALIZE.systemAdministrator.userLookUpDetails.Tests.viewSelectedTestPopup
                          .testAdministrator
                      }
                    </p>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.popupTestInfoRightColumn}
                  >
                    <p>
                      {this.getTADetails(
                        this.state.viewSelectedTest.ta_name,
                        this.state.viewSelectedTest.ta_email
                      )}
                    </p>
                  </Col>
                </Row>
                <Row
                  className="align-items-top justify-content-start"
                  style={styles.popupTestInfoItem}
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.popupTestInfoLeftColumn}
                  >
                    <p>
                      {
                        LOCALIZE.systemAdministrator.userLookUpDetails.Tests.viewSelectedTestPopup
                          .testAccessCode
                      }
                    </p>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.popupTestInfoRightColumn}
                  >
                    <p>{this.state.viewSelectedTest.test_access_code}</p>
                  </Col>
                </Row>
                <Row
                  className="align-items-top justify-content-start"
                  style={styles.popupTestInfoItem}
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.popupTestInfoLeftColumn}
                  >
                    <p>
                      {
                        LOCALIZE.systemAdministrator.userLookUpDetails.Tests.viewSelectedTestPopup
                          .testOrderNumber
                      }
                    </p>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.popupTestInfoRightColumn}
                  >
                    <p>
                      {this.state.viewSelectedTest.test_order_number !== null
                        ? this.state.viewSelectedTest.test_order_number
                        : LOCALIZE.reports.orderlessRequest}
                    </p>
                  </Col>
                </Row>
                <Row
                  className="align-items-top justify-content-start"
                  style={styles.popupTestInfoItem}
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.popupTestInfoLeftColumn}
                  >
                    <p>
                      {
                        LOCALIZE.systemAdministrator.userLookUpDetails.Tests.viewSelectedTestPopup
                          .assessmentProcessOrReferenceNb
                      }
                    </p>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.popupTestInfoRightColumn}
                  >
                    <p>{this.state.viewSelectedTest.assessment_process_reference_nbr}</p>
                  </Col>
                </Row>
                <Row
                  className="align-items-top justify-content-start"
                  style={styles.popupTestInfoItem}
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.popupTestInfoLeftColumn}
                  >
                    <p>
                      {
                        LOCALIZE.systemAdministrator.userLookUpDetails.Tests.viewSelectedTestPopup
                          .totalScore
                      }
                    </p>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.popupTestInfoRightColumn}
                  >
                    <p>{this.state.viewSelectedTest.total_score}</p>
                  </Col>
                </Row>
                <Row
                  className="align-items-top justify-content-start"
                  style={styles.popupTestInfoItem}
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.popupTestInfoLeftColumn}
                  >
                    <p>
                      {
                        LOCALIZE.systemAdministrator.userLookUpDetails.Tests.viewSelectedTestPopup
                          .invalidTestReason
                      }
                    </p>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.popupTestInfoRightColumn}
                  >
                    <p>{this.state.viewSelectedTest.invalidTestReason}</p>
                  </Col>
                </Row>
              </div>
            </>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.close}
          rightButtonIcon={faTimes}
          rightButtonAction={this.closeTestDetailsPopup}
        />
        <PopupBox
          show={this.state.showInvalidatePopup}
          handleClose={() => {}}
          isBackdropStatic={false}
          shouldCloseOnEsc={true}
          title={LOCALIZE.systemAdministrator.activeTests.invalidatePopup.title}
          size="lg"
          overflowVisible={false}
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.error}
                title={LOCALIZE.commons.warning}
                message={
                  <p>
                    {LOCALIZE.formatString(
                      LOCALIZE.systemAdministrator.activeTests.invalidatePopup.description,
                      <span style={styles.bold}>{this.props.selectedUserFirstname}</span>,
                      <span style={styles.bold}>{this.props.selectedUserLastname}</span>
                    )}
                  </p>
                }
              />
              <div style={styles.popupTestInfoContainer}>
                <label id="reason-for-invalidating-test">
                  {
                    LOCALIZE.systemAdministrator.activeTests.invalidatePopup
                      .reasonForInvalidatingTheTest
                  }
                </label>
                <textarea
                  id="reason-for-invalidating"
                  aria-labelledby="reason-for-invalidating-test"
                  aria-required={true}
                  style={{ ...styles.input, ...styles.textAreaInput }}
                  value={this.state.reasonForInvalidatingTest}
                  onChange={this.updateReasonForInvalidatingTest}
                  maxLength="300"
                ></textarea>
              </div>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeInvalidatePopup}
          leftButtonState={
            this.state.invalidationLoading ? BUTTON_STATE.disabled : BUTTON_STATE.enabled
          }
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={LOCALIZE.systemAdministrator.activeTests.invalidatePopup.actionButton}
          rightButtonIcon={faTimesCircle}
          rightButtonState={
            this.state.invalidationLoading
              ? BUTTON_STATE.disabled
              : !this.state.isValidReasonForInvalidatingTest
              ? BUTTON_STATE.enabled
              : BUTTON_STATE.disabled
          }
          rightButtonAction={this.handleInvalidate}
        />
        <PopupBox
          show={this.state.showValidatePopup}
          handleClose={() => {}}
          isBackdropStatic={false}
          shouldCloseOnEsc={true}
          title={LOCALIZE.systemAdministrator.activeTests.validatePopup.title}
          size="lg"
          overflowVisible={false}
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.warning}
                title={LOCALIZE.commons.warning}
                message={
                  <p>
                    {LOCALIZE.formatString(
                      LOCALIZE.systemAdministrator.activeTests.validatePopup.description,
                      <span style={styles.bold}>{this.props.selectedUserFirstname}</span>,
                      <span style={styles.bold}>{this.props.selectedUserLastname}</span>
                    )}
                  </p>
                }
              />
              <div style={styles.popupTestInfoContainer}>
                <label id="reason-for-validating-test">
                  {
                    LOCALIZE.systemAdministrator.activeTests.validatePopup
                      .reasonForInvalidatingTheTest
                  }
                </label>
                <textarea
                  id="reason-for-validating"
                  aria-labelledby="reason-for-validating-test"
                  aria-required={true}
                  style={{ ...styles.input, ...styles.textAreaInput }}
                  value={this.state.reasonForValidatingTest}
                  onChange={this.updateReasonForValidatingTest}
                  maxLength="300"
                ></textarea>
              </div>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeValidatePopup}
          leftButtonState={
            this.state.validationLoading ? BUTTON_STATE.disabled : BUTTON_STATE.enabled
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.systemAdministrator.activeTests.validatePopup.actionButton}
          rightButtonIcon={faTimesCircle}
          rightButtonState={
            this.state.validationLoading
              ? BUTTON_STATE.disabled
              : !this.state.isValidReasonForValidatingTest
              ? BUTTON_STATE.enabled
              : BUTTON_STATE.disabled
          }
          rightButtonAction={this.handleValidate}
        />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    selectedUsername: state.userLookUp.selectedUsername,
    selectedUserFirstname: state.userLookUp.selectedUserFirstname,
    selectedUserLastname: state.userLookUp.selectedUserLastname,
    selectedAssignedTest: state.reports.selectedAssignedTest
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAllTests,
      invalidateCandidateAsEtta,
      validateCandidateAsEtta,
      getInvalidateTestReasonRedux,
      getAllExistingTestOrderNumbers,
      generateCandidateActionsReport,
      getCandidateActionsReportData
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(UserTests);
