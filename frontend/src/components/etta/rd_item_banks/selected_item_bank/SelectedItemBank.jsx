/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../../../text_resources";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import ContentContainer from "../../../commons/ContentContainer";
import SideNavigation from "../../../eMIB/SideNavigation";
import CustomButton, { THEME } from "../../../commons/CustomButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faArrowAltCircleLeft,
  faCheck,
  faSpinner,
  faTimes
} from "@fortawesome/free-solid-svg-icons";
import { history } from "../../../../store-index";
import { PATH } from "../../../commons/Constants";
import { resetSelectedActiveItemBanksStates } from "../../../../modules/RDItemBankRedux";
import PopupBox, { BUTTON_TYPE } from "../../../commons/PopupBox";
import SystemMessage, { MESSAGE_TYPE } from "../../../commons/SystemMessage";
import ItemBankAccesses from "./ItemBankAccesses";

const styles = {
  loadingContainer: {
    margin: "24px 0",
    textAlign: "center"
  },
  loading: {
    width: "100%",
    height: 60,
    textAlign: "center",
    fontSize: "32px",
    padding: 6
  },
  backButtonStyle: {
    marginBottom: 24
  },
  buttonLabel: {
    marginLeft: 6
  },
  sectionContainerLabelDiv: {
    paddingLeft: 18,
    marginBottom: "-3px"
  },
  sectionContainerLabel: {
    padding: "4px 24px",
    marginBottom: 0,
    position: "relative"
  },
  tabStyleBorder: {
    borderBottom: "5px solid #00565e",
    width: "100%",
    position: "absolute",
    bottom: 0,
    left: 0,
    borderRadius: "4px 4px 0 0"
  },
  sectionContainer: {
    borderStyle: "solid",
    borderWidth: "3px 1px 1px 1px",
    borderColor: "#CECECE",
    minHeight: 400,
    padding: "12px 0",
    marginBottom: 24
  },
  tabContainer: {
    zIndex: 1,
    padding: 0
  },
  tabContent: {
    backgroundColor: "transparent",
    overflowY: "auto",
    height: "100%",
    padding: 5
  },
  nav: {
    marginTop: 10,
    marginLeft: 10,
    width: 206,
    textAlign: "center"
  },
  body: {
    paddingLeft: 12
  },
  sideNavBodyContent: {
    marginTop: "-12px"
  },
  appPadding: {
    padding: "15px"
  }
};

class SelectedItemBank extends Component {
  state = {
    isLoading: true,
    showBackToRDOperationsPopup: false,
    triggerRerender: false
  };

  componentDidMount = () => {
    // if itemBankData is defined
    if (
      typeof this.props.itemBankData !== "undefined" &&
      Object.entries(this.props.itemBankData).length > 0
    ) {
      this.setState({ isLoading: false });
      // a user is trying to access the item bank accesses page while the props are not properly set (like trying to manually access the URL for example)
    } else {
      // waiting 3 seconds
      setTimeout(() => {
        // checking if the itemBankData is now defined
        if (
          typeof this.props.itemBankData !== "undefined" &&
          Object.entries(this.props.itemBankData).length > 0
        ) {
          this.setState({ isLoading: false });
        } else {
          // redirecting user to the System Administration page
          history.push(PATH.systemAdministration);
        }
      }, 3000);
    }
  };

  componentDidUpdate = prevProps => {
    // if itemBankData gets updated
    if (prevProps.itemBankData !== this.props.itemBankData) {
      // itemBankData is defined
      if (
        typeof this.props.itemBankData !== "undefined" &&
        Object.entries(this.props.itemBankData).length > 0
      ) {
        this.setState({
          isLoading: false
        });
      }
    }
    // if triggerRerender gets updated
    if (prevProps.triggerRerender !== this.props.triggerRerender) {
      this.setState({
        triggerRerender: !this.state.triggerRerender
      });
    }
  };

  openBackToRDOperationsPopup = () => {
    this.setState({ showBackToRDOperationsPopup: true });
  };

  closeBackToRDOperationsPopup = () => {
    this.setState({ showBackToRDOperationsPopup: false });
  };

  handleBackToItemBankSelection = () => {
    // resetting item bank redux states
    this.props.resetSelectedActiveItemBanksStates();
    // redirecting user to the System Administration page
    history.push(PATH.systemAdministration);
  };

  getItemBankEditorSections = () => {
    const sideNavArray = [
      {
        menuString:
          LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks.selectedItemBank.tabs
            .selectedItemBank.sideNavigationItems.ItemBankAccesses.ItemTitle,
        body: <ItemBankAccesses />
      }
    ];
    return sideNavArray;
  };

  render() {
    const specs = this.getItemBankEditorSections();
    return (
      <div className="app" style={styles.appPadding}>
        <Helmet>
          <html lang={this.props.currentLanguage} />
          <title className="notranslate">{LOCALIZE.titles.itemBankPermissions}</title>
        </Helmet>
        <ContentContainer>
          <div>
            {this.state.isLoading ? (
              <div style={styles.loadingContainer}>
                <label className="fa fa-spinner fa-spin" style={styles.loading}>
                  <FontAwesomeIcon icon={faSpinner} />
                </label>
              </div>
            ) : (
              <div>
                <div style={styles.backButtonStyle}>
                  <CustomButton
                    label={
                      <>
                        <FontAwesomeIcon icon={faArrowAltCircleLeft} />
                        <span style={styles.buttonLabel}>
                          {
                            LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks
                              .selectedItemBank.backToRdOperationsButton
                          }
                        </span>
                      </>
                    }
                    action={this.openBackToRDOperationsPopup}
                    buttonTheme={THEME.SECONDARY}
                  />
                </div>
                <div style={styles.sectionContainerLabelDiv}>
                  <div>
                    <label style={styles.sectionContainerLabel}>
                      {LOCALIZE.formatString(
                        LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks.selectedItemBank
                          .tabs.selectedItemBank.title,
                        this.props.itemBankData.custom_item_bank_id
                      )}
                      <span style={styles.tabStyleBorder}></span>
                    </label>
                  </div>
                </div>
                <div style={styles.sectionContainer}>
                  <section aria-label={LOCALIZE.ariaLabel.sideNavigationSection}>
                    <SideNavigation
                      specs={specs}
                      startIndex={0}
                      displayNextPreviousButton={false}
                      isMain={true}
                      tabContainerStyle={styles.tabContainer}
                      tabContentStyle={styles.tabContent}
                      navStyle={styles.nav}
                      bodyContentCustomStyle={styles.sideNavBodyContent}
                    />
                  </section>
                </div>
              </div>
            )}
          </div>
        </ContentContainer>
        <PopupBox
          show={this.state.showBackToRDOperationsPopup}
          handleClose={this.closeBackToRDOperationsPopup}
          title={
            LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks.selectedItemBank
              .backToRdOperationsPopup.title
          }
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.warning}
                title={LOCALIZE.commons.warning}
                message={
                  <p>
                    {
                      LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks.selectedItemBank
                        .backToRdOperationsPopup.warningDescription
                    }
                  </p>
                }
              />
              <p>
                {
                  LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks.selectedItemBank
                    .backToRdOperationsPopup.description
                }
              </p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.confirm}
          rightButtonAction={this.handleBackToItemBankSelection}
          rightButtonIcon={faCheck}
          size="lg"
        />
      </div>
    );
  }
}

export { SelectedItemBank as unconnectedSelectedItemBank };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    itemBankData: state.rdItemBanks.itemBankData,
    triggerRerender: state.rdItemBanks.triggerRerender
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      resetSelectedActiveItemBanksStates
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(SelectedItemBank);
