/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../../../text_resources";
import { bindActionCreators } from "redux";
import { Col, Row } from "react-bootstrap";
import { getLineSpacingCSS } from "../../../../modules/AccommodationsRedux";
import {
  setItemBankData,
  updateItemBankDataAsRdOperations,
  triggerRerender,
  getSelectedItemBankData,
  addTestDeveloper,
  deleteTestDeveloper,
  getItemBankAccessTypes,
  updateTestDeveloperAccess
} from "../../../../modules/RDItemBankRedux";
import CustomButton, { THEME } from "../../../commons/CustomButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCheck,
  faPlusCircle,
  faSpinner,
  faTimes,
  faTrashAlt
} from "@fortawesome/free-solid-svg-icons";
import { LANGUAGES } from "../../../commons/Translation";
import DropdownSelect from "../../../commons/DropdownSelect";
import { getOrganization } from "../../../../modules/ExtendedProfileOptionsRedux";
import { LANGUAGE_IDS } from "../../../../modules/LocalizeRedux";
import GenericTable, { COMMON_STYLE } from "../../../commons/GenericTable";
import StyledTooltip, { EFFECT, TRIGGER_TYPE, TYPE } from "../../../authentication/StyledTooltip";
import PopupBox, { BUTTON_STATE, BUTTON_TYPE } from "../../../commons/PopupBox";
import { getUsersBasedOnSpecifiedPermission } from "../../../../modules/PermissionsRedux";
import SystemMessage, { MESSAGE_TYPE } from "../../../commons/SystemMessage";
import { PERMISSION } from "../../../profile/Constants";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 5,
    lg: 5,
    xl: 5
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 7,
    lg: 7,
    xl: 7
  }
};

const styles = {
  mainContainer: {
    width: "100%"
  },
  formContainer: {
    marginTop: 24
  },
  itemContainer: {
    width: "100%",
    margin: "12px 0"
  },
  labelContainer: {
    verticalAlign: "middle"
  },
  label: {
    paddingRight: 12,
    margin: 0
  },
  inputContainer: {
    verticalAlign: "middle"
  },
  input: {
    width: "100%",
    minHeight: 38,
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565e",
    borderRadius: 4
  },
  applyButtonContainer: {
    textAlign: "center",
    marginTop: 36
  },
  applyButton: {
    minWidth: 200
  },
  buttonIcon: {
    marginRight: 6
  },
  buttonLabel: {
    margin: 0,
    paddingLeft: 6
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    margin: "4px 0 0 4px"
  },
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  allUnset: {
    all: "unset"
  },
  addTDButtonContainer: {
    margin: "36px 0 24px 0"
  },
  loadingContainer: {
    textAlign: "center"
  },
  bold: {
    fontWeight: "bold"
  },
  customLoadingContainer: {
    display: "grid",
    gridTemplateColumns: "1fr"
  },
  loadingOverlappingStyle: {
    gridRowStart: "1",
    gridColumnStart: "1"
  },
  transparentText: {
    color: "transparent"
  },
  icon: {
    paddingRight: 6,
    verticalAlign: "middle"
  },
  customDropdownStyle: {
    textAlign: "left"
  },
  loading: {
    minHeight: 38,
    padding: 6
  }
};

class ItemBankAccesses extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  static propTypes = {};

  state = {
    dataHasBeenModified: false,
    isValidForm: true,
    isLoading: false,
    itemBankPrefix: { content: this.props.itemBankData.custom_item_bank_id },
    initialItemBankPrefix: this.props.itemBankData.custom_item_bank_id,
    itemBankAlreadyExists: false,
    itemBankNameEn: { content: this.props.itemBankData.en_name },
    initialItemBankNameEn: this.props.itemBankData.en_name,
    itemBankNameFr: { content: this.props.itemBankData.fr_name },
    initialItemBankNameFr: this.props.itemBankData.fr_name,
    organisationsOptions: [],
    itemBankSelectedOrganisationOption: {
      label:
        this.props.currentLanguage === LANGUAGES.english
          ? `${this.props.itemBankData.department_data.edesc} (${this.props.itemBankData.department_data.eabrv})`
          : `${this.props.itemBankData.department_data.fdesc} (${this.props.itemBankData.department_data.fabrv})`,
      value: `${this.props.itemBankData.department_id}`
    },
    itemBankInitialSelectedOrganisationOption: {
      label:
        this.props.currentLanguage === LANGUAGES.english
          ? `${this.props.itemBankData.department_data.edesc} (${this.props.itemBankData.department_data.eabrv})`
          : `${this.props.itemBankData.department_data.fdesc} (${this.props.itemBankData.department_data.fabrv})`,
      value: `${this.props.itemBankData.department_id}`
    },
    languageOptions: [],
    itemBankSelectedLanguageOption: [],
    itemBankInitialSelectedLanguageOption: [],
    rowsDefinition: {},
    rowsCurrentlyLoading: true,
    showAddTestDeveloperPopup: false,
    testDeveloperOptions: [],
    isLoadingTestDeveloperOptions: true,
    isLoadingAddTestDeveloper: false,
    selectedTestDeveloperToAdd: [],
    showDeleteTestDeveloperPopup: false,
    isLoadingDeleteTestDeveloper: false,
    selectedTestDeveloperToDelete: {},
    accessLevelOptions: [],
    isLoadingUpdateTestAccess: false,
    triggerRerender: false
  };

  componentDidMount = () => {
    // populating organisation options
    this.populateOrganisationsOptions();
    // populating language options
    this.populateLanguageOptions();
    // populating rows
    this.populateRows();
    // setting itemBankSelectedLanguageOption state
    this.setState({
      itemBankSelectedLanguageOption: this.getCurrentLanguage(this.props.itemBankData.language),
      itemBankInitialSelectedLanguageOption: this.getCurrentLanguage(
        this.props.itemBankData.language
      )
    });
  };

  populateOrganisationsOptions = () => {
    const organisationsOptions = [];
    this.props.getOrganization().then(response => {
      for (let i = 0; i < response.body.length; i++) {
        // Interface is in English
        if (this.props.currentLanguage === LANGUAGES.english) {
          organisationsOptions.push({
            label: `${response.body[i].edesc} (${response.body[i].eabrv})`,
            value: `${response.body[i].dept_id}`
          });
          // Interface is in French
        } else {
          organisationsOptions.push({
            label: `${response.body[i].fdesc} (${response.body[i].fabrv})`,
            value: `${response.body[i].dept_id}`
          });
        }
      }
      this.setState({ organisationsOptions: organisationsOptions });
    });
  };

  populateLanguageOptions = () => {
    const languageOptions = [];
    // english option
    languageOptions.push({
      label: LOCALIZE.commons.english,
      value: LANGUAGE_IDS.english
    });
    // french option
    languageOptions.push({
      label: LOCALIZE.commons.french,
      value: LANGUAGE_IDS.french
    });
    // bilingual option
    languageOptions.push({
      label: LOCALIZE.commons.bilingual,
      value: null
    });
    // saving results in state
    this.setState({ languageOptions: languageOptions });
  };

  populateRows = (triggerLoading = true, index = null) => {
    this.setState({ rowsCurrentlyLoading: triggerLoading }, () => {
      // populating level access options
      // initializing accessLevelOptions
      const accessLevelOptions = [];
      // getting item bank level access options
      this.props.getItemBankAccessTypes().then(response => {
        for (let i = 0; i < response.length; i++) {
          accessLevelOptions.push({
            label: response[i][`${this.props.currentLanguage}_description`],
            value: response[i].id
          });
        }
        this.setState({ accessLevelOptions: accessLevelOptions }, () => {
          // initializing needed object and array for rowsDefinition (needed for GenericTable component)
          let rowsDefinition = {};
          const data = [];
          for (let i = 0; i < this.props.itemBankData.item_bank_permissions.length; i++) {
            // triggerLoading is set to false
            if (!triggerLoading && index !== null) {
              // i matched the provided index
              if (i === index) {
                // populating data object with provided data (loading is triggered for current iteration)
                data.push({
                  column_1: `${this.props.itemBankData.item_bank_permissions[i].test_developer_data.last_name}, ${this.props.itemBankData.item_bank_permissions[i].test_developer_data.first_name} (${this.props.itemBankData.item_bank_permissions[i].username})`,
                  column_2: this.populateActiveItemBanksColumnTwo(i, true),
                  column_3: this.populateActiveItemBanksColumnThree(
                    i,
                    this.props.itemBankData.item_bank_permissions
                  )
                });
              } else {
                // populating data object with provided data (with disabled dropdown)
                data.push({
                  column_1: `${this.props.itemBankData.item_bank_permissions[i].test_developer_data.last_name}, ${this.props.itemBankData.item_bank_permissions[i].test_developer_data.first_name} (${this.props.itemBankData.item_bank_permissions[i].test_developer_data.email})`,
                  column_2: this.populateActiveItemBanksColumnTwo(i, false, true),
                  column_3: this.populateActiveItemBanksColumnThree(
                    i,
                    this.props.itemBankData.item_bank_permissions
                  )
                });
              }
            } else {
              // populating data object with provided data
              data.push({
                column_1: `${this.props.itemBankData.item_bank_permissions[i].test_developer_data.last_name}, ${this.props.itemBankData.item_bank_permissions[i].test_developer_data.first_name} (${this.props.itemBankData.item_bank_permissions[i].test_developer_data.email})`,
                column_2: this.populateActiveItemBanksColumnTwo(i),
                column_3: this.populateActiveItemBanksColumnThree(
                  i,
                  this.props.itemBankData.item_bank_permissions
                )
              });
            }
          }

          // updating rowsDefinition object with provided data and needed style
          rowsDefinition = {
            column_1_style: COMMON_STYLE.LEFT_TEXT,
            column_2_style: COMMON_STYLE.CENTERED_TEXT,
            column_3_style: COMMON_STYLE.CENTERED_TEXT,
            data: data
          };
          // saving results in state
          this.setState(
            {
              rowsDefinition: rowsDefinition,
              rowsCurrentlyLoading: false
            },
            () => {
              // populating test developer options
              this.populateTestDeveloperOptions();
            }
          );
        });
      });
    });
  };

  populateActiveItemBanksColumnTwo = (i, loadingTriggered = false, disabledDropdown = false) => {
    // loadingTriggered is provided and set to true
    if (loadingTriggered) {
      return (
        <label className="fa fa-spinner fa-spin" style={styles.loading}>
          <FontAwesomeIcon icon={faSpinner} />
        </label>
      );
    }
    return (
      <div style={styles.customDropdownStyle}>
        <DropdownSelect
          idPrefix={`item-bank-access-level-${i}`}
          isValid={true}
          ariaRequired={true}
          ariaLabelledBy="item-bank-accesses-column-2"
          hasPlaceholder={false}
          options={this.state.accessLevelOptions}
          onChange={(options, usernameId, index) => {
            this.getSelectedAccessLevelOption(
              options,
              this.props.itemBankData.item_bank_permissions[i].username,
              i
            );
          }}
          defaultValue={{
            label:
              this.props.itemBankData.item_bank_permissions[i].item_bank_access_type_data[
                `${this.props.currentLanguage}_description`
              ],
            value: this.props.itemBankData.item_bank_permissions[i].item_bank_access_type_data.id
          }}
          menuPlacement={"top"}
          isDisabled={disabledDropdown}
          orderByLabels={false}
        />
      </div>
    );
  };

  populateActiveItemBanksColumnThree = (i, item_bank_permissions) => {
    return (
      <div className="d-flex justify-content-center flex-nowrap">
        <StyledTooltip
          id={`item-bank-permissions-view-button-${i}`}
          place="top"
          type={TYPE.light}
          effect={EFFECT.solid}
          triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
          tooltipElement={
            <div style={styles.allUnset}>
              <CustomButton
                dataTip=""
                dataFor={`item-bank-permissions-view-button-${i}`}
                label={<FontAwesomeIcon icon={faTrashAlt} />}
                ariaLabel={LOCALIZE.formatString(
                  LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks.selectedItemBank.tabs
                    .selectedItemBank.sideNavigationItems.ItemBankAccesses.table
                    .deleteButtonAriaLabel,
                  item_bank_permissions[i].username
                )}
                action={() => {
                  this.openDeleteTestDeveloperPopup(item_bank_permissions[i]);
                }}
                customStyle={styles.actionButton}
                buttonTheme={THEME.SECONDARY}
              />
            </div>
          }
          tooltipContent={
            <div>
              <p>
                {
                  LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks.selectedItemBank.tabs
                    .selectedItemBank.sideNavigationItems.ItemBankAccesses.table.deleteButtonTooltip
                }
              </p>
            </div>
          }
        />
      </div>
    );
  };

  populateTestDeveloperOptions = () => {
    this.setState({ isLoadingTestDeveloperOptions: true }, () => {
      // getting all existing TAs and removing the ones that exist in the table from the options
      const tasInTable = [];
      // if there is data in the table
      if (this.state.rowsDefinition.data.length > 0) {
        for (let i = 0; i < this.state.rowsDefinition.data.length; i++) {
          // adding test developer username (email) to tasInTable
          tasInTable.push(this.state.rowsDefinition.data[i].column_1.split("(")[1].slice(0, -1));
        }
      }
      const testDeveloperOptions = [];
      this.props.getUsersBasedOnSpecifiedPermission(PERMISSION.testDeveloper).then(response => {
        for (let i = 0; i < response.length; i++) {
          if (!tasInTable.includes(response[i].user)) {
            testDeveloperOptions.push({
              label: `${response[i].last_name}, ${response[i].first_name} (${response[i].email})`,
              value: `${response[i].user}`
            });
          }
        }
      });
      this.setState({
        testDeveloperOptions: testDeveloperOptions,
        isLoadingTestDeveloperOptions: false
      });
    });
  };

  openDeleteTestDeveloperPopup = currentSelectedItemBankTestDeveloper => {
    this.setState({
      selectedTestDeveloperToDelete: currentSelectedItemBankTestDeveloper,
      showDeleteTestDeveloperPopup: true
    });
  };

  closeDeleteTestDeveloperPopup = () => {
    this.setState({
      selectedTestDeveloperToDelete: {},
      showDeleteTestDeveloperPopup: false
    });
  };

  handleDeleteTestDeveloper = () => {
    this.setState({ isLoadingDeleteTestDeveloper: true }, () => {
      this.props
        .deleteTestDeveloper(
          this.state.selectedTestDeveloperToDelete.username,
          this.props.itemBankData.id
        )
        .then(response => {
          if (response.ok) {
            // getting selected item bank data
            this.props.getSelectedItemBankData(this.props.itemBankData.id).then(response => {
              // making sure that the item bank data redux state is up to date
              this.props.setItemBankData(response);
              // closing add test developer popup and re-poplating rows
              this.setState(
                {
                  showDeleteTestDeveloperPopup: false,
                  selectedTestDeveloperToDelete: {}
                },
                () => {
                  this.populateRows();
                  setTimeout(() => {
                    this.setState({ isLoadingDeleteTestDeveloper: false });
                  }, 250);
                }
              );
            });
            // should never happen
          } else {
            throw new Error("An error occurred during the delete test developer process");
          }
        });
    });
  };

  getCurrentLanguage = languageId => {
    switch (languageId) {
      case LANGUAGE_IDS.english:
        return {
          label: LOCALIZE.commons.english,
          value: LANGUAGE_IDS.english
        };
      case LANGUAGE_IDS.french:
        return {
          label: LOCALIZE.commons.french,
          value: LANGUAGE_IDS.french
        };
      default:
        return {
          label: LOCALIZE.commons.bilingual,
          value: null
        };
    }
  };

  updateItemBankPrefixContent = event => {
    const itemBankPrefix = event.target.value;
    // allow maximum of 25 chars without space and special chars
    const regexExpression = /^(.{0,24}[a-zA-Z0-9-/_])$/;
    if (itemBankPrefix === "" || regexExpression.test(itemBankPrefix)) {
      this.setState(
        { itemBankPrefix: { content: itemBankPrefix.toUpperCase() }, itemBankAlreadyExists: false },
        () => {
          // checking if all the data is the same as in the DB
          this.checkForChangesInForm();
          // validating form
          this.validateForm();
        }
      );
    }
  };

  updateItemBankNameEnContent = event => {
    const itemBankNameEn = event.target.value;
    // allow maximum of 150 characters
    if (itemBankNameEn.length <= 150) {
      this.setState({ itemBankNameEn: { content: itemBankNameEn } }, () => {
        // checking if all the data is the same as in the DB
        this.checkForChangesInForm();
        // validating form
        this.validateForm();
      });
    }
  };

  updateItemBankNameFrContent = event => {
    const itemBankNameFr = event.target.value;
    // allow maximum of 150 characters
    if (itemBankNameFr.length <= 150) {
      this.setState({ itemBankNameFr: { content: itemBankNameFr } }, () => {
        // checking if all the data is the same as in the DB
        this.checkForChangesInForm();
        // validating form
        this.validateForm();
      });
    }
  };

  getSelectedOrganisationOption = option => {
    this.setState(
      {
        itemBankSelectedOrganisationOption: option
      },
      () => {
        // checking if all the data is the same as in the DB
        this.checkForChangesInForm();
        // validating form
        this.validateForm();
      }
    );
  };

  getSelectedLanguageOption = option => {
    this.setState(
      {
        itemBankSelectedLanguageOption: option
      },
      () => {
        // checking if all the data is the same as in the DB
        this.checkForChangesInForm();
        // validating form
        this.validateForm();
      }
    );
  };

  checkForChangesInForm = () => {
    // initializing needed variables
    let dataHasBeenModified = false;
    let itemBankPredixModified = false;
    let itemBankNameEnModified = false;
    let itemBankNameFrModified = false;
    let itemBankDepartmentModified = false;
    let itemBankLanguageModified = false;

    // checking if itemBankPrefix has been modified
    if (this.state.itemBankPrefix.content !== this.props.itemBankData.custom_item_bank_id) {
      itemBankPredixModified = true;
    }

    // checking if itemBankNameEn has been modified
    if (this.state.itemBankNameEn.content !== this.props.itemBankData.en_name) {
      itemBankNameEnModified = true;
    }

    // checking if itemBankNameFr has been modified
    if (this.state.itemBankNameFr.content !== this.props.itemBankData.fr_name) {
      itemBankNameFrModified = true;
    }

    // checking if itemBankDepartment has been modified
    if (
      this.state.itemBankSelectedOrganisationOption.value.toString() !==
      this.props.itemBankData.department_data.dept_id.toString()
    ) {
      itemBankDepartmentModified = true;
    }

    // checking if itemBankLanguage has been modified
    const itemBankSelectedLanguageOptionValue = this.state.itemBankSelectedLanguageOption.value;
    const itemBankLanguageProps = this.props.itemBankData.language;
    // itemBankSelectedLanguageOptionValue is not null (EN or FR)
    if (itemBankSelectedLanguageOptionValue !== null) {
      // converting value to string
      itemBankSelectedLanguageOptionValue.toString();
    }
    // itemBankLanguageProps is not null (EN or FR)
    if (itemBankLanguageProps !== null) {
      // converting value to string
      itemBankLanguageProps.toString();
    }
    if (itemBankSelectedLanguageOptionValue !== itemBankLanguageProps) {
      itemBankLanguageModified = true;
    }

    // if any data has been modified
    if (
      itemBankPredixModified ||
      itemBankNameEnModified ||
      itemBankNameFrModified ||
      itemBankDepartmentModified ||
      itemBankLanguageModified
    ) {
      dataHasBeenModified = true;
    }

    this.setState({ dataHasBeenModified: dataHasBeenModified });
  };

  validateForm = () => {
    // initializing needed variables
    let isValidForm = false;

    // making sure that all mandatory fields are filled
    const isValidItemBankPrefix = this.state.itemBankPrefix.content !== "";

    // if all mandatory fields are valid
    if (isValidItemBankPrefix) {
      isValidForm = true;
    }

    this.setState({ isValidForm: isValidForm });
  };

  handleApplyChanges = () => {
    this.setState({ isLoading: true }, () => {
      // updating itemBankData redux states
      const { itemBankData } = this.props;
      itemBankData.custom_item_bank_id = this.state.itemBankPrefix.content;
      itemBankData.en_name = this.state.itemBankNameEn.content;
      itemBankData.fr_name = this.state.itemBankNameFr.content;
      itemBankData.department_id = this.state.itemBankSelectedOrganisationOption.value;
      itemBankData.department_data.dept_id = this.state.itemBankSelectedOrganisationOption.value;
      itemBankData.language = this.state.itemBankSelectedLanguageOption.value;
      this.props.setItemBankData(itemBankData);
      // adding small delay to make sure that the props are properly set before updating the DB
      setTimeout(() => {
        // updating data in DB
        const body = this.props.itemBankData;
        this.props.updateItemBankDataAsRdOperations(body).then(response => {
          if (response.ok) {
            // setting needed states
            this.checkForChangesInForm();
            this.setState({
              itemBankAlreadyExists: false,
              initialItemBankPrefix: this.props.itemBankData.custom_item_bank_id,
              initialItemBankNameEn: this.props.itemBankData.en_name,
              initialItemBankNameFr: this.props.itemBankData.fr_name,
              itemBankInitialSelectedOrganisationOption: {
                label:
                  this.props.currentLanguage === LANGUAGES.english
                    ? `${this.props.itemBankData.department_data.edesc} (${this.props.itemBankData.department_data.eabrv})`
                    : `${this.props.itemBankData.department_data.fdesc} (${this.props.itemBankData.department_data.fabrv})`,
                value: this.props.itemBankData.department_data.dept_id
              },
              itemBankInitialSelectedLanguageOption: this.getCurrentLanguage(
                this.props.itemBankData.language
              )
            });
            // getting selected item bank data
            this.props.getSelectedItemBankData(this.props.itemBankData.id).then(response => {
              // making sure that the item bank data redux state is up to date
              this.props.setItemBankData(response);
              this.props.triggerRerender();
              this.setState({ isLoading: false });
            });
            // item bank prefix already exists
          } else if (response.status === 409) {
            // updating itemBankData redux state with the initial values
            itemBankData.custom_item_bank_id = this.state.initialItemBankPrefix;
            itemBankData.en_name = this.state.initialItemBankNameEn;
            itemBankData.fr_name = this.state.initialItemBankNameFr;
            itemBankData.department_id = this.state.itemBankInitialSelectedOrganisationOption.value;
            itemBankData.department_data.dept_id =
              this.state.itemBankInitialSelectedOrganisationOption.value;
            itemBankData.language = this.state.itemBankInitialSelectedLanguageOption.value;
            this.props.setItemBankData(itemBankData);
            this.setState({ isLoading: false, itemBankAlreadyExists: true });
            // should never happen
          } else {
            throw new Error("An error occurred during the update item bank data process");
          }
        });
      }, 250);
    });
  };

  getSelectedTestDeveloper = option => {
    this.setState({
      selectedTestDeveloperToAdd: option
    });
  };

  openAddTestDeveloperPopup = () => {
    this.setState({ showAddTestDeveloperPopup: true });
  };

  closeAddTestDeveloperPopup = () => {
    this.setState({ showAddTestDeveloperPopup: false, selectedTestDeveloperToAdd: [] });
  };

  handleAddTestDeveloper = () => {
    this.setState({ isLoadingAddTestDeveloper: true }, () => {
      this.props
        .addTestDeveloper(this.state.selectedTestDeveloperToAdd.value, this.props.itemBankData.id)
        .then(response => {
          if (response.ok) {
            // getting selected item bank data
            this.props.getSelectedItemBankData(this.props.itemBankData.id).then(response => {
              // making sure that the item bank data redux state is up to date
              this.props.setItemBankData(response);
              // closing add test developer popup and re-poplating rows
              this.setState(
                { showAddTestDeveloperPopup: false, selectedTestDeveloperToAdd: [] },
                () => {
                  this.populateRows();
                  setTimeout(() => {
                    this.setState({ isLoadingAddTestDeveloper: false });
                  }, 250);
                }
              );
            });

            // should never happen
          } else {
            throw new Error("An error occurred during the add test developer process");
          }
        });
    });
  };

  getSelectedAccessLevelOption = (option, usernameId, index) => {
    this.setState({ isLoadingUpdateTestAccess: true }, () => {
      // populating rows
      this.populateRows(false, index);
      this.props
        .updateTestDeveloperAccess(usernameId, this.props.itemBankData.id, option.value)
        .then(response => {
          if (response.ok) {
            // getting selected item bank data
            this.props
              .getSelectedItemBankData(this.props.itemBankData.id)
              .then(response => {
                // making sure that the item bank data redux state is up to date
                this.props.setItemBankData(response);
              })
              .then(() => {
                this.setState(
                  {
                    isLoadingUpdateTestAccess: false
                  },
                  () => {
                    // populating rows
                    this.populateRows(false);
                  }
                );
              });
            // should never happen
          } else {
            throw new Error("An error occurred during the updated test developer access process");
          }
        });
    });
  };

  render() {
    const columnsDefinition = [
      {
        label:
          LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks.selectedItemBank.tabs
            .selectedItemBank.sideNavigationItems.ItemBankAccesses.table.column1,
        style: { width: "55%", ...COMMON_STYLE.LEFT_TEXT }
      },
      {
        label:
          LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks.selectedItemBank.tabs
            .selectedItemBank.sideNavigationItems.ItemBankAccesses.table.column2,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label:
          LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks.selectedItemBank.tabs
            .selectedItemBank.sideNavigationItems.ItemBankAccesses.table.column3,
        style: COMMON_STYLE.CENTERED_TEXT
      }
    ];

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    const currentItemBankPrefixContent = {
      ...{ content: this.props.itemBankData.custom_item_bank_id },
      ...this.state.itemBankPrefix
    };

    const currentItemBankNameEnContent = {
      ...{ content: this.props.itemBankData.en_name },
      ...this.state.itemBankNameEn
    };

    const currentItemBankNameFrContent = {
      ...{ content: this.props.itemBankData.fr_name },
      ...this.state.itemBankNameFr
    };

    return (
      <div style={styles.mainContainer}>
        <div>
          <h2>
            {LOCALIZE.formatString(
              LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks.selectedItemBank.tabs
                .selectedItemBank.sideNavigationItems.ItemBankAccesses.title,
              this.props.itemBankData.custom_item_bank_id
            )}
          </h2>
          <p>
            {
              LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks.selectedItemBank.tabs
                .selectedItemBank.sideNavigationItems.ItemBankAccesses.description
            }
          </p>
        </div>
        <div style={styles.formContainer}>
          <Row className="align-items-center justify-content-start" style={styles.itemContainer}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
              style={styles.labelContainer}
            >
              <label style={styles.label}>
                <label id="item-bank-prefix-title">
                  {
                    LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks.selectedItemBank
                      .tabs.selectedItemBank.sideNavigationItems.ItemBankAccesses.itemBankPrefix
                  }
                </label>
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={styles.inputContainer}
            >
              <input
                id="item-bank-prefix"
                className={!this.state.itemBankAlreadyExists ? "valid-field" : "invalid-field"}
                aria-labelledby="item-bank-prefix-title item-bank-prefix-error"
                aria-required={true}
                aria-invalid={this.state.itemBankAlreadyExists}
                style={{ ...styles.input, ...accommodationsStyle }}
                type="text"
                value={currentItemBankPrefixContent.content}
                onChange={this.updateItemBankPrefixContent}
              ></input>
              {this.state.itemBankAlreadyExists && (
                <label
                  id="item-bank-prefix-error"
                  htmlFor="item-bank-prefix"
                  style={styles.errorMessage}
                  className="notranslate"
                >
                  {
                    LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks.selectedItemBank
                      .tabs.selectedItemBank.sideNavigationItems.ItemBankAccesses
                      .itemBankAlreadyExistsError
                  }
                </label>
              )}
            </Col>
          </Row>
          <Row className="align-items-center justify-content-start" style={styles.itemContainer}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
              style={styles.labelContainer}
            >
              <label style={styles.label}>
                <label id="item-bank-name-en-title">
                  {
                    LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks.selectedItemBank
                      .tabs.selectedItemBank.sideNavigationItems.ItemBankAccesses.itemBankNameEn
                  }
                </label>
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={styles.inputContainer}
            >
              <input
                id="item-bank-name-en"
                className={"valid-field"}
                aria-labelledby="item-bank-name-en-title"
                aria-required={false}
                aria-invalid={true}
                style={{ ...styles.input, ...accommodationsStyle }}
                type="text"
                value={currentItemBankNameEnContent.content}
                onChange={this.updateItemBankNameEnContent}
              ></input>
            </Col>
          </Row>
          <Row className="align-items-center justify-content-start" style={styles.itemContainer}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
              style={styles.labelContainer}
            >
              <label style={styles.label}>
                <label id="item-bank-name-fr-title">
                  {
                    LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks.selectedItemBank
                      .tabs.selectedItemBank.sideNavigationItems.ItemBankAccesses.itemBankNameFr
                  }
                </label>
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={styles.inputContainer}
            >
              <input
                id="item-bank-name-fr"
                className={"valid-field"}
                aria-labelledby="item-bank-name-fr-title"
                aria-required={false}
                aria-invalid={true}
                style={{ ...styles.input, ...accommodationsStyle }}
                type="text"
                value={currentItemBankNameFrContent.content}
                onChange={this.updateItemBankNameFrContent}
              ></input>
            </Col>
          </Row>
          <Row className="align-items-center justify-content-start" style={styles.itemContainer}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
              style={styles.labelContainer}
            >
              <label style={styles.label}>
                <label id="item-bank-department-title">
                  {
                    LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks.selectedItemBank
                      .tabs.selectedItemBank.sideNavigationItems.ItemBankAccesses.itemBankDepartment
                  }
                </label>
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={styles.inputContainer}
            >
              <DropdownSelect
                idPrefix="item-bank-department"
                isValid={true}
                ariaRequired={true}
                ariaLabelledBy="item-bank-department-title"
                hasPlaceholder={true}
                options={this.state.organisationsOptions}
                onChange={this.getSelectedOrganisationOption}
                defaultValue={this.state.itemBankSelectedOrganisationOption}
                isMulti={false}
              />
            </Col>
          </Row>
          <Row className="align-items-center justify-content-start" style={styles.itemContainer}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
              style={styles.labelContainer}
            >
              <label style={styles.label}>
                <label id="item-bank-language-title">
                  {
                    LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks.selectedItemBank
                      .tabs.selectedItemBank.sideNavigationItems.ItemBankAccesses.itemBankLanguage
                  }
                </label>
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={styles.inputContainer}
            >
              <DropdownSelect
                idPrefix="item-bank-language"
                isValid={true}
                ariaRequired={true}
                ariaLabelledBy="item-bank-language-title"
                hasPlaceholder={true}
                options={this.state.languageOptions}
                onChange={this.getSelectedLanguageOption}
                defaultValue={this.state.itemBankSelectedLanguageOption}
                isMulti={false}
              />
            </Col>
          </Row>
        </div>
        <div style={styles.applyButtonContainer}>
          <CustomButton
            label={
              <>
                {this.state.isLoading ? (
                  <label className="fa fa-spinner fa-spin">
                    <FontAwesomeIcon icon={faSpinner} />
                  </label>
                ) : (
                  <>
                    <FontAwesomeIcon icon={faCheck} style={styles.buttonIcon} />
                    <span>
                      {this.state.dataHasBeenModified
                        ? LOCALIZE.commons.applyButton
                        : LOCALIZE.commons.saved}
                    </span>
                  </>
                )}
              </>
            }
            action={this.handleApplyChanges}
            customStyle={styles.applyButton}
            buttonTheme={this.state.dataHasBeenModified ? THEME.PRIMARY : THEME.SUCCESS}
            disabled={
              !this.state.isValidForm || !this.state.dataHasBeenModified || this.state.isLoading
            }
          />
        </div>
        <div>
          <div style={styles.addTDButtonContainer}>
            <CustomButton
              label={
                <>
                  <FontAwesomeIcon icon={faPlusCircle} />
                  <span id="new-item-bank-test-developer" style={styles.buttonLabel}>
                    {
                      LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks.selectedItemBank
                        .tabs.selectedItemBank.sideNavigationItems.ItemBankAccesses
                        .addTestDeveloperButton
                    }
                  </span>
                </>
              }
              action={() => this.openAddTestDeveloperPopup()}
              buttonTheme={THEME.PRIMARY}
            />
          </div>
          <div>
            <GenericTable
              classnamePrefix="item-bank-accesses"
              columnsDefinition={columnsDefinition}
              rowsDefinition={this.state.rowsDefinition}
              emptyTableMessage={
                LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks.selectedItemBank.tabs
                  .selectedItemBank.sideNavigationItems.ItemBankAccesses.table.noData
              }
              currentlyLoading={this.state.rowsCurrentlyLoading}
            />
          </div>
        </div>
        <PopupBox
          show={this.state.showAddTestDeveloperPopup}
          handleClose={() => {}}
          shouldCloseOnEsc={false}
          displayCloseButton={false}
          isBackdropStatic={true}
          overflowVisible={true}
          size={"lg"}
          title={LOCALIZE.formatString(
            LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks.selectedItemBank.tabs
              .selectedItemBank.sideNavigationItems.ItemBankAccesses.addTestDeveloperPopup.title,
            this.props.itemBankData.custom_item_bank_id
          )}
          description={
            <div style={styles.popupContainer}>
              <div>
                <p style={styles.description}>
                  {
                    LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks.selectedItemBank
                      .tabs.selectedItemBank.sideNavigationItems.ItemBankAccesses
                      .addTestDeveloperPopup.description
                  }
                </p>
                <Row
                  className="align-items-center justify-content-start"
                  style={styles.itemContainer}
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.labelContainer}
                  >
                    <label style={styles.label}>
                      <label id="add-item-bank-test-developer-test-developer-title">
                        {
                          LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks
                            .selectedItemBank.tabs.selectedItemBank.sideNavigationItems
                            .ItemBankAccesses.addTestDeveloperPopup.testDeveloperLabel
                        }
                      </label>
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.inputContainer}
                  >
                    {this.state.isLoadingTestDeveloperOptions ? (
                      <div style={styles.loadingContainer}>
                        <label className="fa fa-spinner fa-spin">
                          <FontAwesomeIcon icon={faSpinner} />
                        </label>
                      </div>
                    ) : (
                      <DropdownSelect
                        idPrefix="add-item-bank-test-developer-test-developer"
                        isValid={true}
                        ariaRequired={true}
                        ariaLabelledBy="add-item-bank-test-developer-test-developer-title"
                        hasPlaceholder={true}
                        options={this.state.testDeveloperOptions}
                        onChange={this.getSelectedTestDeveloper}
                        defaultValue={this.state.selectedTestDeveloperToAdd}
                        isMulti={false}
                      />
                    )}
                  </Col>
                </Row>
              </div>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonIcon={faTimes}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonLabel={LOCALIZE.commons.cancel}
          leftButtonAction={this.closeAddTestDeveloperPopup}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonIcon={!this.state.isLoadingAddTestDeveloper ? faPlusCircle : ""}
          rightButtonTitle={
            !this.state.isLoadingAddTestDeveloper ? (
              LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks.selectedItemBank.tabs
                .selectedItemBank.sideNavigationItems.ItemBankAccesses.addTestDeveloperPopup
                .addButton
            ) : (
              <div style={styles.customLoadingContainer}>
                {/* this div is useful to get the right size of the button while loading */}
                <div style={{ ...styles.loadingOverlappingStyle, ...styles.transparentText }}>
                  <FontAwesomeIcon icon={faPlusCircle} style={styles.icon} />
                  {
                    LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks.selectedItemBank
                      .tabs.selectedItemBank.sideNavigationItems.ItemBankAccesses
                      .addTestDeveloperPopup.addButton
                  }
                </div>
                <div style={styles.loadingOverlappingStyle}>
                  <label className="fa fa-spinner fa-spin">
                    <FontAwesomeIcon icon={faSpinner} />
                  </label>
                </div>
              </div>
            )
          }
          rightButtonLabel={
            LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks.selectedItemBank.tabs
              .selectedItemBank.sideNavigationItems.ItemBankAccesses.addTestDeveloperPopup.addButton
          }
          rightButtonAction={this.handleAddTestDeveloper}
          rightButtonState={
            this.state.isLoadingAddTestDeveloper
              ? BUTTON_STATE.disabled
              : this.state.selectedTestDeveloperToAdd.length > 0 ||
                Object.entries(this.state.selectedTestDeveloperToAdd).length > 0
              ? BUTTON_STATE.enabled
              : BUTTON_STATE.disabled
          }
        />
        <PopupBox
          show={this.state.showDeleteTestDeveloperPopup}
          handleClose={() => {}}
          shouldCloseOnEsc={false}
          displayCloseButton={false}
          isBackdropStatic={true}
          overflowVisible={true}
          size={"lg"}
          title={LOCALIZE.formatString(
            LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks.selectedItemBank.tabs
              .selectedItemBank.sideNavigationItems.ItemBankAccesses.deleteTestDeveloperPopup.title,
            this.props.itemBankData.custom_item_bank_id
          )}
          description={
            <div style={styles.popupContainer}>
              <SystemMessage
                messageType={MESSAGE_TYPE.error}
                title={LOCALIZE.commons.warning}
                message={
                  <p>
                    {LOCALIZE.formatString(
                      LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks.selectedItemBank
                        .tabs.selectedItemBank.sideNavigationItems.ItemBankAccesses
                        .deleteTestDeveloperPopup.description,
                      <span style={styles.bold}>
                        {this.state.selectedTestDeveloperToDelete.username}
                      </span>
                    )}
                  </p>
                }
              />
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonIcon={faTimes}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonLabel={LOCALIZE.commons.cancel}
          leftButtonAction={this.closeDeleteTestDeveloperPopup}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonIcon={!this.state.isLoadingDeleteTestDeveloper ? faTrashAlt : ""}
          rightButtonTitle={
            !this.state.isLoadingDeleteTestDeveloper ? (
              LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks.selectedItemBank.tabs
                .selectedItemBank.sideNavigationItems.ItemBankAccesses.deleteTestDeveloperPopup
                .deleteButton
            ) : (
              <div style={styles.customLoadingContainer}>
                {/* this div is useful to get the right size of the button while loading */}
                <div style={{ ...styles.loadingOverlappingStyle, ...styles.transparentText }}>
                  <FontAwesomeIcon icon={faPlusCircle} style={styles.icon} />
                  {
                    LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks.selectedItemBank
                      .tabs.selectedItemBank.sideNavigationItems.ItemBankAccesses
                      .deleteTestDeveloperPopup.deleteButton
                  }
                </div>
                <div style={styles.loadingOverlappingStyle}>
                  <label className="fa fa-spinner fa-spin">
                    <FontAwesomeIcon icon={faSpinner} />
                  </label>
                </div>
              </div>
            )
          }
          rightButtonLabel={
            LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks.selectedItemBank.tabs
              .selectedItemBank.sideNavigationItems.ItemBankAccesses.deleteTestDeveloperPopup
              .deleteButton
          }
          rightButtonAction={this.handleDeleteTestDeveloper}
          rightButtonState={
            !this.state.isLoadingDeleteTestDeveloper ? BUTTON_STATE.enabled : BUTTON_STATE.disabled
          }
        />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    itemBankData: state.rdItemBanks.itemBankData
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setItemBankData,
      updateItemBankDataAsRdOperations,
      triggerRerender,
      getOrganization,
      getSelectedItemBankData,
      getUsersBasedOnSpecifiedPermission,
      addTestDeveloper,
      deleteTestDeveloper,
      getItemBankAccessTypes,
      updateTestDeveloperAccess
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ItemBankAccesses);
