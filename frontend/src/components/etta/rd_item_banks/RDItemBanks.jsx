import React, { Component } from "react";
// import PropTypes from "prop-types";
import { connect } from "react-redux";
import LOCALIZE from "../../../text_resources";
import { bindActionCreators } from "redux";
import { Tabs, Tab, Row, Col } from "react-bootstrap";
import ActiveItemBanks from "./ActiveItemBanks";
import ArchivedItemBanks from "./ArchivedItemBanks";
import {
  getAllItemBanks,
  getFoundItemBanks,
  updateActiveItemBanksPageState,
  getSelectedItemBankData,
  setItemBankData
} from "../../../modules/RDItemBankRedux";
import { COMMON_STYLE } from "../../commons/GenericTable";
import StyledTooltip, { EFFECT, TRIGGER_TYPE, TYPE } from "../../authentication/StyledTooltip";
import CustomButton, { THEME } from "../../commons/CustomButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBinoculars } from "@fortawesome/free-solid-svg-icons";
import { LANGUAGES } from "../../../modules/LocalizeRedux";
import { history } from "../../../store-index";
import { PATH } from "../../commons/Constants";
import { PERMISSION } from "../../profile/Constants";

const styles = {
  mainContainer: {
    width: "100%"
  },
  tabNavigation: {
    borderBottom: "none"
  },
  tabContainer: {
    marginTop: 2,
    borderTop: "3px solid #00565e",
    backgroundColor: "#F8F8F8"
  },
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  allUnset: {
    all: "unset"
  }
};

class RDItemBanks extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  static propTypes = {};

  state = {
    activeItemBanksCurrentlyLoading: true,
    rowsDefinitionActiveItemBanks: {},
    numberOfActiveItemBanksPages: 1,
    activeItemBanksResultsFound: 0
  };

  componentDidMount = () => {
    this.props.updateActiveItemBanksPageState(1);
    this.getAllItemBanksBasedOnActiveSearch();
  };

  componentDidUpdate = prevProps => {
    // if currentLanguage gets updated
    if (prevProps.currentLanguage !== this.props.currentLanguage) {
      this.getAllItemBanksBasedOnActiveSearch();
    }
    // if itemBanksPaginationPageSize get updated
    if (
      prevProps.activeItemBanksPaginationPageSize !== this.props.activeItemBanksPaginationPageSize
    ) {
      this.getAllItemBanksBasedOnActiveSearch();
    }
    // if testDefinitionAllTestsPaginationPage get updated
    if (prevProps.activeItemBanksPaginationPage !== this.props.activeItemBanksPaginationPage) {
      this.getAllItemBanksBasedOnActiveSearch();
    }
    // if search itemBanksKeyword gets updated
    if (prevProps.activeItemBanksKeyword !== this.props.activeItemBanksKeyword) {
      // if itemBanksKeyword redux state is empty
      if (this.props.itemBanksKeyword !== "") {
        this.getAllItemBanksBasedOnActiveSearch();
      } else if (this.props.itemBanksKeyword === "" && this.props.activeItemBanksActiveSearch) {
        this.getAllItemBanksBasedOnActiveSearch();
      }
    }
    // if activeItemBanksActiveSearch gets updated
    if (prevProps.activeItemBanksActiveSearch !== this.props.activeItemBanksActiveSearch) {
      // there is no current active search (on clear search action)
      if (!this.props.activeItemBanksActiveSearch) {
        this.getAllActiveItemBanksData();
        // there is a current active search (on search action)
      } else {
        this.getFoundActiveItemBanksData();
      }
    }
    // if triggerTableUpdates gets updated
    if (prevProps.triggerTableUpdates !== this.props.triggerTableUpdates) {
      this.getAllItemBanksBasedOnActiveSearch();
    }
  };

  // get item banks data based on the activeItemBanksActiveSearch redux state
  getAllItemBanksBasedOnActiveSearch = () => {
    // current search
    if (this.props.activeItemBanksActiveSearch) {
      this.getFoundActiveItemBanksData();
      // no current search
    } else {
      this.getAllActiveItemBanksData();
    }
  };

  getAllActiveItemBanksData = () => {
    this.setState({ activeItemBanksCurrentlyLoading: true }, () => {
      // initializing needed object and array for rowsDefinition (needed for GenericTable component)
      let rowsDefinition = {};
      const data = [];
      this.props
        .getAllItemBanks(
          PERMISSION.rdOperations,
          this.props.activeItemBanksPaginationPage,
          this.props.activeItemBanksPaginationPageSize
        )
        .then(response => {
          for (let i = 0; i < response.results.length; i++) {
            // populating data object with provided data
            data.push({
              column_1: response.results[i].custom_item_bank_id,
              column_2: response.results[i].modify_date.split("T")[0],
              column_3:
                this.props.currentLanguage === LANGUAGES.english
                  ? `${response.results[i].department_data.edesc} (${response.results[i].department_data.eabrv})`
                  : `${response.results[i].department_data.fdesc} (${response.results[i].department_data.fabrv})`,
              column_4: this.populateActiveItemBanksColumnFour(i, response)
            });
          }

          // updating rowsDefinition object with provided data and needed style
          rowsDefinition = {
            column_1_style: COMMON_STYLE.LEFT_TEXT,
            column_2_style: COMMON_STYLE.CENTERED_TEXT,
            column_3_style: COMMON_STYLE.CENTERED_TEXT,
            column_4_style: COMMON_STYLE.CENTERED_TEXT,
            data: data
          };
          // saving results in state
          this.setState({
            rowsDefinitionActiveItemBanks: rowsDefinition,
            activeItemBanksCurrentlyLoading: false,
            numberOfActiveItemBanksPages: Math.ceil(
              parseInt(response.count) / this.props.activeItemBanksPaginationPageSize
            )
          });
        });
    });
  };

  getFoundActiveItemBanksData = () => {
    this.setState({ activeItemBanksCurrentlyLoading: true }, () => {
      // initializing needed object and array for rowsDefinition (needed for GenericTable component)
      let rowsDefinition = {};
      const data = [];
      setTimeout(() => {
        this.props
          .getFoundItemBanks(
            this.props.activeItemBanksKeyword,
            this.props.currentLanguage,
            PERMISSION.rdOperations,
            this.props.activeItemBanksPaginationPage,
            this.props.activeItemBanksPaginationPageSize
          )
          .then(response => {
            // there are no results found
            if (response[0] === "no results found") {
              this.setState(
                {
                  rowsDefinitionActiveItemBanks: {},
                  numberOfActiveItemBanksPages: 1,
                  activeItemBanksResultsFound: 0
                },
                () => {
                  this.setState({ activeItemBanksCurrentlyLoading: false }, () => {
                    // make sure that this element exsits to avoid any error
                    if (document.getElementById("active-item-banks-results-found")) {
                      document.getElementById("active-item-banks-results-found").focus();
                    }
                  });
                }
              );
              // there is at least one result found
            } else {
              for (let i = 0; i < response.results.length; i++) {
                // populating data object with provided data
                data.push({
                  column_1: response.results[i].custom_item_bank_id,
                  column_2: response.results[i].modify_date.split("T")[0],
                  column_3:
                    this.props.currentLanguage === LANGUAGES.english
                      ? `${response.results[i].department_data.edesc} (${response.results[i].department_data.eabrv})`
                      : `${response.results[i].department_data.fdesc} (${response.results[i].department_data.fabrv})`,
                  column_4: this.populateActiveItemBanksColumnFour(i, response)
                });
              }

              // updating rowsDefinition object with provided data and needed style
              rowsDefinition = {
                column_1_style: COMMON_STYLE.LEFT_TEXT,
                column_2_style: COMMON_STYLE.CENTERED_TEXT,
                column_3_style: COMMON_STYLE.CENTERED_TEXT,
                column_4_style: COMMON_STYLE.CENTERED_TEXT,
                data: data
              };
              // saving results in state
              this.setState(
                {
                  rowsDefinitionActiveItemBanks: rowsDefinition,
                  activeItemBanksCurrentlyLoading: false,
                  numberOfActiveItemBanksPages: Math.ceil(
                    parseInt(response.count) / this.props.activeItemBanksPaginationPageSize
                  ),
                  activeItemBanksResultsFound: response.count
                },
                () => {
                  // make sure that this element exsits to avoid any error
                  if (document.getElementById("active-item-banks-results-found")) {
                    document.getElementById("active-item-banks-results-found").focus();
                  }
                }
              );
            }
          });
      }, 100);
    });
  };

  populateActiveItemBanksColumnFour = (i, response) => {
    return (
      <div className="d-flex justify-content-center flex-nowrap">
        <StyledTooltip
          id={`active-item-banks-view-button-${i}`}
          place="top"
          type={TYPE.light}
          effect={EFFECT.solid}
          triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
          tooltipElement={
            <div style={styles.allUnset}>
              <CustomButton
                dataTip=""
                dataFor={`active-item-banks-view-button-${i}`}
                label={<FontAwesomeIcon icon={faBinoculars} />}
                ariaLabel={LOCALIZE.formatString(
                  LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBanks.table
                    .viewButtonAriaLabel,
                  response.results[i].custom_item_bank_id
                )}
                action={() => {
                  this.viewSelectedItemBank(response.results[i]);
                }}
                customStyle={styles.actionButton}
                buttonTheme={THEME.SECONDARY}
              />
            </div>
          }
          tooltipContent={
            <div>
              <p>
                {
                  LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBanks.table
                    .viewButtonTooltip
                }
              </p>
            </div>
          }
        />
      </div>
    );
  };

  viewSelectedItemBank = currentSelectedItemBank => {
    // getting selected item bank data (including permissions data)
    this.props
      .getSelectedItemBankData(currentSelectedItemBank.id)
      .then(response => {
        this.props.setItemBankData(response);
      })
      .then(() => {
        // redirecting user to the item bank accesses page
        history.push(PATH.itemBankAccesses);
      });
  };

  render() {
    const TABS = [
      {
        key: "active-item-banks",
        tabName: LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks.title,
        body: (
          <ActiveItemBanks
            currentlyLoading={this.state.activeItemBanksCurrentlyLoading}
            rowsDefinition={this.state.rowsDefinitionActiveItemBanks}
            resultsFound={this.state.activeItemBanksResultsFound}
            numberOfActiveItemBanksPages={this.state.numberOfActiveItemBanksPages}
          />
        )
      },
      {
        key: "active-permissions",
        tabName: LOCALIZE.systemAdministrator.itemBanks.tabs.archivedItemBanks.title,
        body: <ArchivedItemBanks />
      }
    ];
    return (
      <div style={styles.mainContainer}>
        <div>
          <h2>{LOCALIZE.systemAdministrator.itemBanks.title}</h2>
          <p>{LOCALIZE.systemAdministrator.itemBanks.description}</p>
        </div>
        <div>
          <Row>
            <Col>
              <Tabs
                defaultActiveKey="active-item-banks"
                id="item-banks-tabs"
                style={styles.tabNavigation}
              >
                {TABS.map((tab, index) => {
                  return (
                    <Tab
                      key={index}
                      eventKey={tab.key}
                      title={tab.tabName}
                      style={styles.tabContainer}
                    >
                      {tab.body}
                    </Tab>
                  );
                })}
              </Tabs>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    activeItemBanksPaginationPage: state.rdItemBanks.activeItemBanksPaginationPage,
    activeItemBanksPaginationPageSize: state.rdItemBanks.activeItemBanksPaginationPageSize,
    activeItemBanksKeyword: state.rdItemBanks.activeItemBanksKeyword,
    activeItemBanksActiveSearch: state.rdItemBanks.activeItemBanksActiveSearch,
    triggerTableUpdates: state.rdItemBanks.triggerTableUpdates
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAllItemBanks,
      getFoundItemBanks,
      updateActiveItemBanksPageState,
      getSelectedItemBankData,
      setItemBankData
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(RDItemBanks);
