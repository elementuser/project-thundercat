import {
  faCaretLeft,
  faCaretRight,
  faPlusCircle,
  faTimes
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import LOCALIZE from "../../../text_resources";
import { bindActionCreators } from "redux";
import CustomButton, { THEME } from "../../commons/CustomButton";
import GenericTable, { COMMON_STYLE } from "../../commons/GenericTable";
import SearchBarWithDisplayOptions from "../../commons/SearchBarWithDisplayOptions";
import {
  updateActiveSearchItemBanksStates,
  updateActiveItemBanksPageState,
  updateActiveItemBanksPageSizeState,
  createNewItemBank,
  triggerTableUpdates
} from "../../../modules/RDItemBankRedux";
import ReactPaginate from "react-paginate";
import { Col, Row } from "react-bootstrap";
import PopupBox, { BUTTON_STATE, BUTTON_TYPE } from "../../commons/PopupBox";
import { getLineSpacingCSS } from "../../../modules/AccommodationsRedux";
import { LANGUAGES } from "../../commons/Translation";
import DropdownSelect from "../../commons/DropdownSelect";
import { getOrganization } from "../../../modules/ExtendedProfileOptionsRedux";
import SystemMessage, { MESSAGE_TYPE } from "../../commons/SystemMessage";
import { LANGUAGE_IDS } from "../../../modules/LocalizeRedux";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 5,
    xl: 5
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 7,
    xl: 7
  }
};

const styles = {
  mainContainer: {
    marginTop: 24
  },
  buttonLabel: {
    margin: 0,
    paddingLeft: 6
  },
  popupContainer: {
    padding: "10px 15px"
  },
  description: {
    marginBottom: 12
  },
  itemContainer: {
    width: "100%",
    margin: "12px 0"
  },
  labelContainer: {
    verticalAlign: "middle"
  },
  label: {
    paddingRight: 12,
    margin: 0
  },
  inputContainer: {
    verticalAlign: "middle"
  },
  input: {
    width: "100%",
    minHeight: 38,
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565e",
    borderRadius: 4
  },
  textAreaInput: {
    width: "100%",
    height: 85,
    padding: "3px 6px 3px 6px",
    borderRadius: 4,
    display: "block",
    resize: "none"
  },
  paginationContainer: {
    display: "flex"
  },
  paginationIcon: {
    padding: "0 6px"
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    margin: "4px 0 0 4px"
  }
};

class ActiveItemBanks extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  static propTypes = {
    currentlyLoading: PropTypes.bool.isRequired,
    rowsDefinition: PropTypes.object.isRequired,
    resultsFound: PropTypes.number.isRequired,
    numberOfActiveItemBanksPages: PropTypes.number.isRequired
  };

  state = {
    rowsDefinition: {},
    currentlyLoading: true,
    resultsFound: this.props.resultsFound,
    numberOfActiveItemBanksPages: this.props.numberOfActiveItemBanksPages,
    showNewItemBankPopup: false,
    newItemBankCreatedSuccesssfully: false,
    isValidNewItemBankForm: false,
    organisationsOptions: [],
    newItemBankSelectedOrganisationOption: [],
    newItemBankPrefix: "",
    languageOptions: [],
    newItemBankSelectedLanguageOption: [],
    newItemBankComments: "",
    itemBankAlreadyExists: false
  };

  componentDidMount = () => {
    // populating organisation options
    this.populateOrganisationsOptions();
    // populating language options
    this.populateLanguageOptions();
  };

  componentDidUpdate = prevProps => {
    // if rowsDefinition gets updated
    if (prevProps.rowsDefinition !== this.props.rowsDefinition) {
      this.setState({ rowsDefinition: this.props.rowsDefinition });
    }
    // if currentlyLoading gets updated
    if (prevProps.currentlyLoading !== this.props.currentlyLoading) {
      this.setState({ currentlyLoading: this.props.currentlyLoading });
    }
    // if resultsFound gets updated
    if (prevProps.resultsFound !== this.props.resultsFound) {
      this.setState({ resultsFound: this.props.resultsFound });
    }
    // if numberOfActiveItemBanksPages gets updated
    if (prevProps.numberOfActiveItemBanksPages !== this.props.numberOfActiveItemBanksPages) {
      this.setState({ numberOfActiveItemBanksPages: this.props.numberOfActiveItemBanksPages });
    }
  };

  handlePageChange = id => {
    // "+1" because redux itemBanksPaginationPage uses index 1 and react-paginate (id) uses index 0
    const selectedPage = id.selected + 1;
    // update current page redux state
    this.props.updateActiveItemBanksPageState(selectedPage);
    // focusing on the first table's row (make sure that the row exists before trying to focus to avoid errors)
    if (document.getElementById("active-item-banks-tbl-table-row-0")) {
      document.getElementById("active-item-banks-tbl-table-row-0").focus();
    }
  };

  populateOrganisationsOptions = () => {
    const organisationsOptions = [];
    this.props.getOrganization().then(response => {
      for (let i = 0; i < response.body.length; i++) {
        // Interface is in English
        if (this.props.currentLanguage === LANGUAGES.english) {
          organisationsOptions.push({
            label: `${response.body[i].edesc} (${response.body[i].eabrv})`,
            value: `${response.body[i].dept_id}`
          });
          // Interface is in French
        } else {
          organisationsOptions.push({
            label: `${response.body[i].fdesc} (${response.body[i].fabrv})`,
            value: `${response.body[i].dept_id}`
          });
        }
      }
      this.setState({ organisationsOptions: organisationsOptions });
    });
  };

  populateLanguageOptions = () => {
    const languageOptions = [];
    // english option
    languageOptions.push({
      label: LOCALIZE.commons.english,
      value: LANGUAGE_IDS.english
    });
    // french option
    languageOptions.push({
      label: LOCALIZE.commons.french,
      value: LANGUAGE_IDS.french
    });
    // bilingual option
    languageOptions.push({
      label: LOCALIZE.commons.bilingual,
      value: null
    });
    // saving results in state
    this.setState({ languageOptions: languageOptions });
  };

  openNewItemBankPopup = () => {
    this.setState({ showNewItemBankPopup: true });
  };

  resetFormStates = () => {
    this.setState({
      showNewItemBankPopup: false,
      newItemBankCreatedSuccesssfully: false,
      isValidNewItemBankForm: false,
      newItemBankSelectedOrganisationOption: [],
      newItemBankPrefix: "",
      newItemBankSelectedLanguageOption: [],
      newItemBankComments: "",
      itemBankAlreadyExists: false
    });
  };

  handleCreateNewItemBank = () => {
    const body = {
      department_id: this.state.newItemBankSelectedOrganisationOption.value,
      custom_item_bank_id: this.state.newItemBankPrefix,
      language_id:
        // if bilingual is selected, send empty string
        this.state.newItemBankSelectedLanguageOption.value !== null
          ? this.state.newItemBankSelectedLanguageOption.value
          : "",
      comments: this.state.newItemBankComments
    };
    this.props.createNewItemBank(body).then(response => {
      // success
      if (response.ok) {
        // show successful popup
        this.setState({ newItemBankCreatedSuccesssfully: true, itemBankAlreadyExists: false });
        // trigger table updates
        this.props.triggerTableUpdates();
        // item bank already exists
      } else {
        this.setState(
          { itemBankAlreadyExists: true, newItemBankCreatedSuccesssfully: false },
          () => {
            // focusing on invalid field
            document.getElementById("item-bank-prefix").focus();
          }
        );
      }
    });
  };

  getSelectedOrganisationOption = option => {
    this.setState(
      {
        newItemBankSelectedOrganisationOption: option
      },
      () => {
        this.validateNewItemBankForm();
      }
    );
  };

  updateItemBankPrefixContent = event => {
    const newItemBankPrefix = event.target.value;
    // allow maximum of 25 chars without space and special chars
    const regexExpression = /^(.{0,24}[a-zA-Z0-9-/_])$/;
    if (newItemBankPrefix === "" || regexExpression.test(newItemBankPrefix)) {
      this.setState(
        { newItemBankPrefix: newItemBankPrefix.toUpperCase(), itemBankAlreadyExists: false },
        () => {
          this.validateNewItemBankForm();
        }
      );
    }
  };

  getSelectedLanguageOption = option => {
    this.setState(
      {
        newItemBankSelectedLanguageOption: option
      },
      () => {
        this.validateNewItemBankForm();
      }
    );
  };

  updateItemBankCommentsContent = event => {
    const newItemBankComments = event.target.value;
    this.setState(
      {
        newItemBankComments: newItemBankComments
      },
      () => {
        this.validateNewItemBankForm();
      }
    );
  };

  validateNewItemBankForm = () => {
    // initializing needed variables
    let isValidForm = false;
    const isValidDepartment =
      typeof this.state.newItemBankSelectedOrganisationOption.value !== "undefined";
    const isValidPrefix = this.state.newItemBankPrefix !== "";
    const isValidLanguage =
      typeof this.state.newItemBankSelectedLanguageOption.value !== "undefined";

    // everything is valid
    if (isValidDepartment && isValidPrefix && isValidLanguage) {
      isValidForm = true;
    }

    this.setState({ isValidNewItemBankForm: isValidForm });
  };

  render() {
    const columnsDefinition = [
      {
        label: LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks.table.column1,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label: LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks.table.column2,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks.table.column3,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks.table.column4,
        style: COMMON_STYLE.CENTERED_TEXT
      }
    ];

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <div style={styles.mainContainer}>
        <CustomButton
          label={
            <>
              <FontAwesomeIcon icon={faPlusCircle} />
              <span id="new-item-bank-button" style={styles.buttonLabel}>
                {LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks.newItemBankButton}
              </span>
            </>
          }
          action={() => this.openNewItemBankPopup()}
          buttonTheme={THEME.PRIMARY}
        />
        <SearchBarWithDisplayOptions
          idPrefix={"active-item-banks"}
          currentlyLoading={this.state.currentlyLoading}
          updateSearchStates={this.props.updateActiveSearchItemBanksStates}
          updatePageState={this.props.updateActiveItemBanksPageState}
          paginationPageSize={this.props.activeItemBanksPaginationPageSize}
          updatePaginationPageSize={this.props.updateActiveItemBanksPageSizeState}
          data={this.state.rowsDefinition.results || []}
          resultsFound={this.state.resultsFound}
          searchBarContent={this.props.activeItemBanksKeyword}
        />
        <GenericTable
          classnamePrefix="active-item-banks-tbl"
          columnsDefinition={columnsDefinition}
          rowsDefinition={this.state.rowsDefinition}
          emptyTableMessage={
            LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks.table.noData
          }
          currentlyLoading={this.state.currentlyLoading}
        />
        <div style={styles.paginationContainer}>
          <ReactPaginate
            pageCount={this.state.numberOfActiveItemBanksPages}
            containerClassName={"pagination"}
            breakClassName={"break"}
            activeClassName={"active-page"}
            marginPagesDisplayed={3}
            // "-1" because react-paginate uses index 0 and itemBanksPaginationPage redux state uses index 1
            forcePage={this.props.activeItemBanksPaginationPage - 1}
            onPageChange={page => {
              this.handlePageChange(page);
            }}
            previousLabel={
              <div style={styles.paginationIcon}>
                <label style={styles.hiddenText}>
                  {LOCALIZE.commons.pagination.previousPageButton}
                </label>
                <FontAwesomeIcon icon={faCaretLeft} />
              </div>
            }
            nextLabel={
              <div style={styles.paginationIcon}>
                <label style={styles.hiddenText}>
                  {LOCALIZE.commons.pagination.nextPageButton}
                </label>
                <FontAwesomeIcon icon={faCaretRight} />
              </div>
            }
          />
        </div>
        <PopupBox
          show={this.state.showNewItemBankPopup}
          handleClose={() => {}}
          shouldCloseOnEsc={false}
          displayCloseButton={false}
          isBackdropStatic={false}
          size={this.state.newItemBankCreatedSuccesssfully ? "lg" : "xl"}
          title={LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks.newItemBankPopup.title}
          description={
            <div style={styles.popupContainer}>
              {this.state.newItemBankCreatedSuccesssfully ? (
                <div>
                  <SystemMessage
                    messageType={MESSAGE_TYPE.success}
                    title={LOCALIZE.commons.success}
                    message={
                      <p>
                        {
                          LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks
                            .newItemBankPopup.successMessage
                        }
                      </p>
                    }
                  />
                </div>
              ) : (
                <div>
                  <p style={styles.description}>
                    {
                      LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks.newItemBankPopup
                        .description
                    }
                  </p>
                  <Row
                    className="align-items-center justify-content-start"
                    style={styles.itemContainer}
                  >
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                      style={styles.labelContainer}
                    >
                      <label style={styles.label}>
                        <label id="item-bank-department-title">
                          {
                            LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks
                              .newItemBankPopup.itemBankDepartment
                          }
                        </label>
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                      style={styles.inputContainer}
                    >
                      <DropdownSelect
                        idPrefix="item-bank-department"
                        isValid={true}
                        ariaRequired={true}
                        ariaLabelledBy="item-bank-department-title"
                        hasPlaceholder={true}
                        options={this.state.organisationsOptions}
                        onChange={this.getSelectedOrganisationOption}
                        defaultValue={this.state.newItemBankSelectedOrganisationOption}
                        isMulti={false}
                      />
                    </Col>
                  </Row>
                  <Row
                    className="align-items-center justify-content-start"
                    style={styles.itemContainer}
                  >
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                      style={styles.labelContainer}
                    >
                      <label style={styles.label}>
                        <label id="item-bank-prefix-title" htmlFor="item-bank-prefix">
                          {
                            LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks
                              .newItemBankPopup.itemBankPrefix
                          }
                        </label>
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                      style={styles.inputContainer}
                    >
                      <input
                        id="item-bank-prefix"
                        className={
                          !this.state.itemBankAlreadyExists ? "valid-field" : "invalid-field"
                        }
                        aria-labelledby="item-bank-prefix-title item-bank-prefix-error"
                        aria-required={true}
                        aria-invalid={this.state.itemBankAlreadyExists}
                        style={{ ...styles.input, ...accommodationsStyle }}
                        type="text"
                        value={this.state.newItemBankPrefix}
                        onChange={this.updateItemBankPrefixContent}
                      ></input>
                      {this.state.itemBankAlreadyExists && (
                        <label
                          id="item-bank-prefix-error"
                          htmlFor="item-bank-prefix"
                          style={styles.errorMessage}
                          className="notranslate"
                        >
                          {
                            LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks
                              .newItemBankPopup.itemBankAlreadyExistsError
                          }
                        </label>
                      )}
                    </Col>
                  </Row>
                  <Row
                    className="align-items-center justify-content-start"
                    style={styles.itemContainer}
                  >
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                      style={styles.labelContainer}
                    >
                      <label style={styles.label}>
                        <label id="item-bank-language-title">
                          {
                            LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks
                              .newItemBankPopup.itemBankLanguage
                          }
                        </label>
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                      style={styles.inputContainer}
                    >
                      <DropdownSelect
                        idPrefix="item-bank-language"
                        isValid={true}
                        ariaRequired={true}
                        ariaLabelledBy="item-bank-language-title"
                        hasPlaceholder={true}
                        options={this.state.languageOptions}
                        onChange={this.getSelectedLanguageOption}
                        defaultValue={this.state.newItemBankSelectedLanguageOption}
                        isMulti={false}
                        menuPlacement={"top"}
                      />
                    </Col>
                  </Row>
                  <Row
                    className="align-items-center justify-content-start"
                    style={styles.itemContainer}
                  >
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                      style={styles.labelContainer}
                    >
                      <label style={styles.label}>
                        <label id="item-bank-comments-title" htmlFor="item-bank-comments">
                          {
                            LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks
                              .newItemBankPopup.itemBankComments
                          }
                        </label>
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                      style={styles.inputContainer}
                    >
                      <textarea
                        id="item-bank-comments"
                        // className={this.state.isValidGocEmail ? "valid-field" : "invalid-field"}
                        aria-labelledby="item-bank-comments-title "
                        aria-required={true}
                        // aria-invalid={!this.state.isValidGocEmail}
                        style={{ ...styles.textAreaInput, ...accommodationsStyle }}
                        value={this.state.newItemBankComments}
                        onChange={this.updateItemBankCommentsContent}
                        maxLength="200"
                      ></textarea>
                    </Col>
                  </Row>
                </div>
              )}
            </div>
          }
          leftButtonType={
            !this.state.newItemBankCreatedSuccesssfully ? BUTTON_TYPE.secondary : BUTTON_TYPE.none
          }
          leftButtonIcon={faTimes}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonLabel={LOCALIZE.commons.cancel}
          leftButtonAction={this.resetFormStates}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonIcon={!this.state.newItemBankCreatedSuccesssfully ? faPlusCircle : ""}
          rightButtonTitle={
            !this.state.newItemBankCreatedSuccesssfully
              ? LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks.newItemBankPopup
                  .rightButton
              : LOCALIZE.commons.ok
          }
          rightButtonLabel={
            !this.state.newItemBankCreatedSuccesssfully
              ? LOCALIZE.systemAdministrator.itemBanks.tabs.activeItemBanks.newItemBankPopup
                  .rightButton
              : LOCALIZE.commons.ok
          }
          rightButtonAction={
            !this.state.newItemBankCreatedSuccesssfully
              ? this.handleCreateNewItemBank
              : this.resetFormStates
          }
          rightButtonState={
            this.state.isValidNewItemBankForm ? BUTTON_STATE.enabled : BUTTON_STATE.disabled
          }
        />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    activeItemBanksPaginationPage: state.rdItemBanks.activeItemBanksPaginationPage,
    activeItemBanksPaginationPageSize: state.rdItemBanks.activeItemBanksPaginationPageSize,
    activeItemBanksKeyword: state.rdItemBanks.activeItemBanksKeyword,
    activeItemBanksActiveSearch: state.rdItemBanks.activeItemBanksActiveSearch
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateActiveSearchItemBanksStates,
      updateActiveItemBanksPageState,
      updateActiveItemBanksPageSizeState,
      createNewItemBank,
      getOrganization,
      triggerTableUpdates
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ActiveItemBanks);
