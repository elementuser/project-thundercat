import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import LOCALIZE from "../../../text_resources";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBinoculars, faClone, faTimes } from "@fortawesome/free-solid-svg-icons";
import StyledTooltip, { TYPE, EFFECT, TRIGGER_TYPE } from "../../authentication/StyledTooltip";
import {
  getAllArchivedSystemAlerts,
  getFoundArchivedSystemAlerts,
  updateArchivedSystemAlertsPageState,
  updateArchivedSystemAlertsPageSizeState,
  updateArchivedSystemAlertsStates,
  getCriticalityOptions,
  resetSystemAlertData,
  getSystemAlertData,
  updateSystemAlertData,
  updateSystemAlertDataValidFormState,
  createSystemAlert,
  updateSystemAlertDataPreviewValidState,
  triggerActiveSystemAlertTableRerender
} from "../../../modules/SystemAlertsRedux";
import "../../../css/etta.css";
import GenericTable, { COMMON_STYLE } from "../../commons/GenericTable";
import CustomButton, { THEME } from "../../commons/CustomButton";
import SearchBarWithDisplayOptions from "../../commons/SearchBarWithDisplayOptions";
import Pagination from "../../commons/Pagination";
import PopupBox, { BUTTON_STATE, BUTTON_TYPE } from "../../commons/PopupBox";
import SystemAlertDataPopup from "./SystemAlertDataPopup";
import SystemMessage, { MESSAGE_TYPE } from "../../commons/SystemMessage";
import AlertMessage from "../../commons/AlertMessage";
import getCriticalityDataForSystemAlert from "../../../helpers/criticality";
import { LANGUAGES } from "../../commons/Translation";

const styles = {
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  buttonIcon: {
    marginRight: 0
  },
  allUnset: {
    all: "unset"
  }
};

class ArchivedSystemAlerts extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  static propTypes = {
    // provided by redux
    getAllArchivedSystemAlerts: PropTypes.func,
    getFoundArchivedSystemAlerts: PropTypes.func,
    updateArchivedSystemAlertsPageState: PropTypes.func,
    updateArchivedSystemAlertsPageSizeState: PropTypes.func,
    updateArchivedSystemAlertsStates: PropTypes.func
  };

  state = {
    displayResultsFound: false,
    resultsFound: 0,
    clearSearchTriggered: false,
    archivedSystemAlerts: [],
    rowsDefinition: {},
    numberOfPages: 1,
    nextPageNumber: 0,
    previousPageNumber: 0,
    currentlyLoading: true,
    criticalityOptions: [],
    showViewPopup: false,
    showClonePopup: false,
    cloneSystemAlertPopupRequestSuccessful: false,
    previewSystemAlertPopupCurrentLanguage: this.props.currentLanguage,
    criticalityCodename: "",
    showSystemAlertPreviewPopup: false
  };

  componentDidMount = () => {
    // initialize pagination page redux state
    this.props.updateArchivedSystemAlertsPageState(1);
    // if page size is not defined, initialize it
    if (typeof this.props.archivedSystemAlertsPageSize === "undefined") {
      this.props.updateArchivedSystemAlertsPageSizeState(25);
    }
    // initialize active search redux state
    this.props.updateArchivedSystemAlertsStates("", false);
    // populating criticality options
    this.props.getCriticalityOptions().then(response => {
      this.setState({ criticalityOptions: response }, () => {
        // populating data
        this.populateArchivedSystemAlertsBasedOnActiveSearch();
      });
    });
  };

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentDidUpdate = prevProps => {
    // if currentLanguage gets updated
    if (prevProps.currentLanguage !== this.props.currentLanguage) {
      this.populateArchivedSystemAlertsBasedOnActiveSearch();
    }
    // if archivedSystemAlertsPage gets updated
    if (prevProps.archivedSystemAlertsPage !== this.props.archivedSystemAlertsPage) {
      this.populateArchivedSystemAlertsBasedOnActiveSearch();
    }
    // if archivedSystemAlertsPageSize get updated
    if (prevProps.archivedSystemAlertsPageSize !== this.props.archivedSystemAlertsPageSize) {
      this.populateArchivedSystemAlertsBasedOnActiveSearch();
    }
    // if search archivedSystemAlertsKeyword gets updated
    if (prevProps.archivedSystemAlertsKeyword !== this.props.archivedSystemAlertsKeyword) {
      // if archivedSystemAlertsKeyword redux state is empty
      if (this.props.archivedSystemAlertsKeyword !== "") {
        this.populateFoundArchivedSystemAlerts();
      } else if (
        this.props.archivedSystemAlertsKeyword === "" &&
        this.props.archivedSystemAlertsActiveSearch
      ) {
        this.populateFoundArchivedSystemAlerts();
      }
    }
    // if archivedSystemAlertsActiveSearch gets updated
    if (
      prevProps.archivedSystemAlertsActiveSearch !== this.props.archivedSystemAlertsActiveSearch
    ) {
      // there is no current active search (on clear search action)
      if (!this.props.archivedSystemAlertsActiveSearch) {
        // triggering clear search action state
        this.setState({ clearSearchTriggered: true }, () => {
          this.populateArchivedSystemAlerts();
        });
        // there is a current active search (on search action)
      } else {
        this.populateFoundArchivedSystemAlerts();
      }
    }
    // if triggerArchivedSystemAlertTableRerender gets updated
    if (
      prevProps.triggerArchivedSystemAlertTableRerender !==
      this.props.triggerArchivedSystemAlertTableRerender
    ) {
      this.populateArchivedSystemAlertsBasedOnActiveSearch();
    }
  };

  getFormattedCriticality = criticalityId => {
    // getting index of respective criticality ID
    const index = this.state.criticalityOptions.findIndex(obj => {
      return obj.id === criticalityId;
    });
    // getting respective criticality data
    return this.state.criticalityOptions[index][`description_${this.props.currentLanguage}`];
  };

  // populating archived system alerts
  populateArchivedSystemAlerts = () => {
    this._isMounted = true;
    this.setState({ currentlyLoading: true }, () => {
      const archivedSystemAlertsArray = [];
      this.props
        .getAllArchivedSystemAlerts(
          this.props.archivedSystemAlertsPage,
          this.props.archivedSystemAlertsPageSize
        )
        .then(response => {
          if (this._isMounted) {
            this.populateArchivedSystemAlertsObject(archivedSystemAlertsArray, response);
          }
        })
        .then(() => {
          if (this._isMounted) {
            if (this.state.clearSearchTriggered) {
              // go back to the first page to avoid display bugs
              this.props.updateArchivedSystemAlertsPageState(1);
              this.setState({ clearSearchTriggered: false });
            }
          }
        })
        .then(() => {
          if (this._isMounted) {
            this.setState({ currentlyLoading: false });
          }
        });
    });
  };

  // populating all found active test permissions based on a search
  populateFoundArchivedSystemAlerts = () => {
    this.setState({ currentlyLoading: true }, () => {
      const archivedSystemAlertsArray = [];
      this.props
        .getFoundArchivedSystemAlerts(
          this.props.archivedSystemAlertsKeyword,
          this.props.currentLanguage,
          this.props.archivedSystemAlertsPage,
          this.props.archivedSystemAlertsPageSize
        )
        .then(response => {
          // there are no results found
          if (response[0] === "no results found") {
            this.setState({
              archivedSystemAlerts: [],
              rowsDefinition: {},
              numberOfPages: 1,
              nextPageNumber: 0,
              previousPageNumber: 0,
              resultsFound: 0
            });
            // there is at least one result found
          } else {
            this.populateArchivedSystemAlertsObject(archivedSystemAlertsArray, response);
            this.setState({ resultsFound: response.count });
          }
        })
        .then(() => {
          this.setState({ currentlyLoading: false, displayResultsFound: true }, () => {
            // if there is at least one result found
            if (archivedSystemAlertsArray.length > 0) {
              // focusing on results found label only if save confirmation popup is not displayed (avoiding focus bug)
              // make sure that this element exsits to avoid any error
              if (document.getElementById("archived-system-alerts-results-found")) {
                document.getElementById("archived-system-alerts-results-found").focus();
              }
              // no results found
            } else {
              // focusing on no results found row
              document.getElementById("archived-system-alerts-no-data").focus();
            }
          });
        });
    });
  };

  populateArchivedSystemAlertsObject = (archivedSystemAlertsArray, response) => {
    // initializing needed object and array for rowsDefinition props (needed for GenericTable component)
    let rowsDefinition = {};
    const data = [];
    // making sure that there are some results
    if (typeof response.results !== "undefined") {
      // looping in response given
      for (let i = 0; i < response.results.length; i++) {
        const currentResult = response.results[i];
        // pushing needed data in archivedSystemAlerts array
        archivedSystemAlertsArray.push({
          id: currentResult.id,
          title: currentResult.title,
          criticality: currentResult.criticality,
          active_date: currentResult.active_date,
          end_date: currentResult.end_date,
          message_text_en: currentResult.message_text_en,
          message_text_fr: currentResult.message_text_fr
        });
        // pushing needed data in data array (needed for GenericTable component)
        data.push({
          column_1: `${currentResult.title}`,
          column_2: this.getFormattedCriticality(currentResult.criticality),
          column_3: `${currentResult.active_date.split("T")[0]} ${currentResult.active_date
            .split("T")[1]
            .substring(0, 5)}`,
          column_4: `${currentResult.end_date.split("T")[0]} ${currentResult.end_date
            .split("T")[1]
            .substring(0, 5)}`,
          column_5: (
            <div className="d-flex justify-content-center flex-nowrap">
              <StyledTooltip
                id={`archived-system-alerts-view-button-${i}`}
                place="top"
                type={TYPE.light}
                effect={EFFECT.solid}
                triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      dataTip=""
                      dataFor={`archived-system-alerts-view-button-${i}`}
                      label={
                        <>
                          <FontAwesomeIcon icon={faBinoculars} style={styles.buttonIcon} />
                        </>
                      }
                      ariaLabel={
                        LOCALIZE.systemAdministrator.systemAlerts.tabs.archivedSystemAlerts.table
                          .viewTooltip
                      }
                      action={() => this.handleViewSelectedArchivedSystemAlert(i)}
                      customStyle={styles.actionButton}
                      buttonTheme={THEME.SECONDARY}
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>
                      {
                        LOCALIZE.systemAdministrator.systemAlerts.tabs.archivedSystemAlerts.table
                          .viewTooltip
                      }
                    </p>
                  </div>
                }
              />
              <StyledTooltip
                id={`archived-system-alerts-clone-button-${i}`}
                place="top"
                type={TYPE.light}
                effect={EFFECT.solid}
                triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      dataTip=""
                      dataFor={`archived-system-alerts-clone-button-${i}`}
                      label={
                        <>
                          <FontAwesomeIcon icon={faClone} style={styles.buttonIcon} />
                        </>
                      }
                      ariaLabel={
                        LOCALIZE.systemAdministrator.systemAlerts.tabs.archivedSystemAlerts.table
                          .cloneTooltip
                      }
                      action={() => this.handleCloneSelectedArchivedSystemAlert(i)}
                      customStyle={styles.actionButton}
                      buttonTheme={THEME.SECONDARY}
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>
                      {
                        LOCALIZE.systemAdministrator.systemAlerts.tabs.archivedSystemAlerts.table
                          .cloneTooltip
                      }
                    </p>
                  </div>
                }
              />
            </div>
          )
        });
      }
    }

    // updating rowsDefinition object with provided data and needed style
    rowsDefinition = {
      column_1_style: COMMON_STYLE.LEFT_TEXT,
      column_2_style: COMMON_STYLE.CENTERED_TEXT,
      column_3_style: COMMON_STYLE.CENTERED_TEXT,
      column_4_style: COMMON_STYLE.CENTERED_TEXT,
      column_5_style: COMMON_STYLE.CENTERED_TEXT,
      data: data
    };

    // saving results in state
    this.setState({
      archivedSystemAlerts: archivedSystemAlertsArray,
      rowsDefinition: rowsDefinition,
      numberOfPages: Math.ceil(response.count / this.props.archivedSystemAlertsPageSize),
      nextPageNumber: response.next_page_number,
      previousPageNumber: response.previous_page_number
    });
  };

  // populating active test permissions based on the archivedSystemAlertsActiveSearch redux state
  populateArchivedSystemAlertsBasedOnActiveSearch = () => {
    // current search
    if (this.props.archivedSystemAlertsActiveSearch) {
      this.populateFoundArchivedSystemAlerts();
      // no current search
    } else {
      this.populateArchivedSystemAlerts();
    }
  };

  // handling view selected archived system alert
  handleViewSelectedArchivedSystemAlert = index => {
    // getting system alert data
    const systemAlertId = this.state.archivedSystemAlerts[index].id;
    this.props.getSystemAlertData(systemAlertId).then(response => {
      // updating system alert data redux state
      const systemAlertData = {
        title: response.title,
        criticality: {
          value: response.criticality_data.id,
          label: response.criticality_data[`description_${this.props.currentLanguage}`]
        },
        startDate: response.active_date,
        endDate: response.end_date,
        enMessage: response.message_text_en,
        frMessage: response.message_text_fr
      };
      this.props.updateSystemAlertData(systemAlertData);
      // adding small delay before opening the popup to make sure that the redux states are properly loaded
      setTimeout(() => {
        // opening popup
        this.setState({
          showViewPopup: true,
          criticalityCodename: response.criticality_data.codename
        });
      }, 100);
    });
  };

  handleTogglePreviewLanguage = () => {
    // current preview language is English
    if (this.state.previewSystemAlertPopupCurrentLanguage === LANGUAGES.english) {
      // toggle language
      this.setState({ previewSystemAlertPopupCurrentLanguage: LANGUAGES.french });
      // current preview language is Frebcg
    } else {
      // toggle language
      this.setState({ previewSystemAlertPopupCurrentLanguage: LANGUAGES.english });
    }
  };

  closeSystemAlertPreviewPopup = () => {
    this.setState({ showSystemAlertPreviewPopup: false });
  };

  handleCloneSelectedArchivedSystemAlert = index => {
    // updating systemAlertDataPreviewValidState redux state
    this.props.updateSystemAlertDataPreviewValidState(true);
    // getting system alert data
    const systemAlertId = this.state.archivedSystemAlerts[index].id;
    this.props.getSystemAlertData(systemAlertId).then(response => {
      // updating system alert data redux state
      const systemAlertData = {
        title: response.title,
        criticality: {
          value: response.criticality_data.id,
          label: response.criticality_data[`description_${this.props.currentLanguage}`]
        },
        startDate: "",
        endDate: "",
        enMessage: response.message_text_en,
        frMessage: response.message_text_fr
      };
      this.props.updateSystemAlertData(systemAlertData);
      this.props.updateSystemAlertDataValidFormState(false);
      // adding small delay before opening the popup to make sure that the redux states are properly loaded
      setTimeout(() => {
        // opening popup
        this.setState({ showClonePopup: true });
      }, 100);
    });
  };

  closeViewPopup = () => {
    // resetting system alert data redux states
    this.props.resetSystemAlertData();
    // closing popup
    this.setState({
      showViewPopup: false,
      previewSystemAlertPopupCurrentLanguage: this.props.currentLanguage
    });
  };

  closeClonePopup = () => {
    // updating systemAlertDataPreviewValidState redux state
    this.props.updateSystemAlertDataPreviewValidState(false);
    // resetting system alert data redux states
    this.props.resetSystemAlertData();
    // closing popup
    this.setState({ showClonePopup: false }, () => {
      // adding small delay + setting cloneSystemAlertPopupRequestSuccessful state only after setting the showClonePopup state to avoid showing a quick flash of the default popup content (form)
      setTimeout(() => {
        this.setState({ cloneSystemAlertPopupRequestSuccessful: false });
        // resetting system alert data redux states
        this.props.resetSystemAlertData();
      }, 100);
    });
  };

  openSystemAlertPreviewPopup = () => {
    // getting criticality data based on provided ID
    this.props
      .getCriticalityOptions(this.props.systemAlertData.criticality.value)
      .then(response => {
        // updating needed states
        this.setState({
          showSystemAlertPreviewPopup: true,
          criticalityCodename: response.codename
        });
      });
  };

  handleCloneSystemAlert = () => {
    // needed parameters to create the system alert
    const body = {
      title: this.props.systemAlertData.title,
      criticality: this.props.systemAlertData.criticality.value,
      active_date: this.props.systemAlertData.startDate,
      end_date: this.props.systemAlertData.endDate,
      message_text_en: this.props.systemAlertData.enMessage,
      message_text_fr: this.props.systemAlertData.frMessage
    };

    // adding orderless test administrators
    this.props.createSystemAlert(body).then(response => {
      // if there is a validation error
      if (response.ok) {
        this.setState({
          cloneSystemAlertPopupRequestSuccessful: true
        });
        this.props.triggerActiveSystemAlertTableRerender();
        // should never happen
      } else {
        throw new Error("An error occurred during the add system alert request process");
      }
    });
  };

  render() {
    const columnsDefinition = [
      {
        label: LOCALIZE.systemAdministrator.systemAlerts.tabs.archivedSystemAlerts.table.column_1,
        style: {}
      },
      {
        label: LOCALIZE.systemAdministrator.systemAlerts.tabs.archivedSystemAlerts.table.column_2,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.systemAdministrator.systemAlerts.tabs.archivedSystemAlerts.table.column_3,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.systemAdministrator.systemAlerts.tabs.archivedSystemAlerts.table.column_4,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.systemAdministrator.systemAlerts.tabs.archivedSystemAlerts.table.column_5,
        style: COMMON_STYLE.CENTERED_TEXT
      }
    ];

    return (
      <div>
        <SearchBarWithDisplayOptions
          idPrefix={"archived-system-alerts"}
          currentlyLoading={this.state.currentlyLoading}
          updateSearchStates={this.props.updateArchivedSystemAlertsStates}
          updatePageState={this.props.updateArchivedSystemAlertsPageState}
          paginationPageSize={Number(this.props.archivedSystemAlertsPageSize)}
          updatePaginationPageSize={this.props.updateArchivedSystemAlertsPageSizeState}
          data={this.state.archivedSystemAlerts}
          resultsFound={this.state.resultsFound}
        />
        <div>
          <GenericTable
            classnamePrefix="archived-system-alerts"
            columnsDefinition={columnsDefinition}
            rowsDefinition={this.state.rowsDefinition}
            emptyTableMessage={LOCALIZE.SearchBarWithDisplayOptions.noResultsFound}
            currentlyLoading={this.state.currentlyLoading}
          />
          <Pagination
            paginationContainerId={"archived-system-alerts-pagination-pages-link"}
            pageCount={this.state.numberOfPages}
            paginationPage={this.props.archivedSystemAlertsPage}
            updatePaginationPageState={this.props.updateArchivedSystemAlertsPageState}
            firstTableRowId={"archived-system-alerts-table-row-0"}
          />
        </div>
        <PopupBox
          show={this.state.showViewPopup}
          handleClose={() => {}}
          isBackdropStatic={true}
          shouldCloseOnEsc={false}
          size={"lg"}
          title={LOCALIZE.systemAdministrator.systemAlerts.tabs.archivedSystemAlerts.viewPopupTitle}
          description={
            <div>
              <AlertMessage
                alertType={getCriticalityDataForSystemAlert(this.state.criticalityCodename)}
                message={
                  this.state.previewSystemAlertPopupCurrentLanguage === LANGUAGES.english
                    ? this.props.systemAlertData.enMessage
                    : this.props.systemAlertData.frMessage
                }
              />
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={
            this.state.previewSystemAlertPopupCurrentLanguage === LANGUAGES.english
              ? LOCALIZE.commons.french
              : LOCALIZE.commons.english
          }
          leftButtonLabel={
            this.props.previewSystemAlertPopupCurrentLanguage === LANGUAGES.english
              ? LOCALIZE.commons.french
              : LOCALIZE.commons.english
          }
          leftButtonAction={this.handleTogglePreviewLanguage}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonIcon={faTimes}
          rightButtonTitle={LOCALIZE.commons.close}
          rightButtonLabel={LOCALIZE.commons.close}
          rightButtonAction={this.closeViewPopup}
        />
        <PopupBox
          show={this.state.showClonePopup}
          handleClose={() => {}}
          isBackdropStatic={true}
          shouldCloseOnEsc={false}
          displayCloseButton={!this.state.cloneSystemAlertPopupRequestSuccessful}
          closeButtonAction={this.closeClonePopup}
          size={this.state.cloneSystemAlertPopupRequestSuccessful ? "lg" : "xl"}
          title={
            LOCALIZE.systemAdministrator.systemAlerts.tabs.archivedSystemAlerts.clonePopupTitle
          }
          description={
            <div>
              {!this.state.cloneSystemAlertPopupRequestSuccessful && <SystemAlertDataPopup />}
              {this.state.cloneSystemAlertPopupRequestSuccessful && (
                <SystemMessage
                  messageType={MESSAGE_TYPE.success}
                  title={LOCALIZE.commons.success}
                  message={
                    <p>
                      {
                        LOCALIZE.systemAdministrator.systemAlerts.tabs.archivedSystemAlerts
                          .cloneSuccessfullyMessage
                      }
                    </p>
                  }
                />
              )}
            </div>
          }
          leftButtonType={
            !this.state.cloneSystemAlertPopupRequestSuccessful
              ? BUTTON_TYPE.secondary
              : BUTTON_TYPE.none
          }
          leftButtonIcon={faBinoculars}
          leftButtonTitle={LOCALIZE.commons.previewButton}
          leftButtonLabel={LOCALIZE.commons.previewButton}
          leftButtonAction={this.openSystemAlertPreviewPopup}
          leftButtonState={
            this.props.systemAlertDataPreviewValidState
              ? BUTTON_STATE.enabled
              : BUTTON_STATE.disabled
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonIcon={!this.state.cloneSystemAlertPopupRequestSuccessful ? faClone : ""}
          rightButtonTitle={
            !this.state.cloneSystemAlertPopupRequestSuccessful
              ? LOCALIZE.systemAdministrator.systemAlerts.tabs.archivedSystemAlerts.table
                  .cloneTooltip
              : LOCALIZE.commons.ok
          }
          rightButtonLabel={
            !this.state.cloneSystemAlertPopupRequestSuccessful
              ? LOCALIZE.systemAdministrator.systemAlerts.tabs.archivedSystemAlerts.table
                  .cloneTooltip
              : LOCALIZE.commons.ok
          }
          rightButtonAction={
            !this.state.cloneSystemAlertPopupRequestSuccessful
              ? this.handleCloneSystemAlert
              : this.closeClonePopup
          }
          rightButtonState={
            this.props.systemAlertDataValidForm ? BUTTON_STATE.enabled : BUTTON_STATE.disabled
          }
        />
        <PopupBox
          show={this.state.showSystemAlertPreviewPopup}
          handleClose={() => {}}
          isBackdropStatic={true}
          shouldCloseOnEsc={false}
          size={"lg"}
          title={LOCALIZE.commons.previewButton}
          description={
            <div>
              <AlertMessage
                alertType={getCriticalityDataForSystemAlert(this.state.criticalityCodename)}
                message={
                  this.state.previewSystemAlertPopupCurrentLanguage === LANGUAGES.english
                    ? this.props.systemAlertData.enMessage
                    : this.props.systemAlertData.frMessage
                }
              />
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={
            this.state.previewSystemAlertPopupCurrentLanguage === LANGUAGES.english
              ? LOCALIZE.commons.french
              : LOCALIZE.commons.english
          }
          leftButtonLabel={
            this.props.previewSystemAlertPopupCurrentLanguage === LANGUAGES.english
              ? LOCALIZE.commons.french
              : LOCALIZE.commons.english
          }
          leftButtonAction={this.handleTogglePreviewLanguage}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonIcon={faTimes}
          rightButtonTitle={LOCALIZE.commons.close}
          rightButtonLabel={LOCALIZE.commons.close}
          rightButtonAction={this.closeSystemAlertPreviewPopup}
        />
      </div>
    );
  }
}

export { ArchivedSystemAlerts as unconnectedArchivedSystemAlerts };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    archivedSystemAlertsPageSize: state.systemAlerts.archivedSystemAlertsPageSize,
    archivedSystemAlertsPage: state.systemAlerts.archivedSystemAlertsPage,
    archivedSystemAlertsKeyword: state.systemAlerts.archivedSystemAlertsKeyword,
    archivedSystemAlertsActiveSearch: state.systemAlerts.archivedSystemAlertsActiveSearch,
    systemAlertData: state.systemAlerts.systemAlertData,
    systemAlertDataValidForm: state.systemAlerts.systemAlertDataValidForm,
    systemAlertDataPreviewValidState: state.systemAlerts.systemAlertDataPreviewValidState,
    triggerArchivedSystemAlertTableRerender:
      state.systemAlerts.triggerArchivedSystemAlertTableRerender
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateArchivedSystemAlertsPageState,
      updateArchivedSystemAlertsPageSizeState,
      updateArchivedSystemAlertsStates,
      getAllArchivedSystemAlerts,
      getFoundArchivedSystemAlerts,
      getCriticalityOptions,
      resetSystemAlertData,
      getSystemAlertData,
      updateSystemAlertData,
      updateSystemAlertDataValidFormState,
      createSystemAlert,
      updateSystemAlertDataPreviewValidState,
      triggerActiveSystemAlertTableRerender
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ArchivedSystemAlerts);
