import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import LOCALIZE from "../../../text_resources";
import { bindActionCreators } from "redux";
import { Col, Container, Row } from "react-bootstrap";
import { getLineSpacingCSS } from "../../../modules/AccommodationsRedux";
import {
  getCriticalityOptions,
  updateSystemAlertData,
  updateSystemAlertDataValidFormState,
  updateSystemAlertDataPreviewValidState
} from "../../../modules/SystemAlertsRedux";
import DropdownSelect from "../../commons/DropdownSelect";
import DatePicker, { validateDatePicked } from "../../commons/DatePicker";
import populateCustomFutureYearsDateOptions from "../../../helpers/populateCustomDatePickerOptions";
import AddSystemAlertMessagePopupTextTabs from "./AddSystemAlertMessagePopupTextTabs";
import moment from "moment";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 12,
    xl: 3
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 12,
    xl: 9
  }
};

const styles = {
  mainContainer: {
    width: "100%"
  },
  labelContainer: {
    display: "table-cell",
    verticalAlign: "middle"
  },
  label: {
    overflowWrap: "break-word",
    margin: "0 12px 0 0"
  },
  inputContainer: {
    display: "table-cell",
    width: "100%",
    verticalAlign: "middle"
  },
  fieldSeparator: {
    marginTop: 18
  },
  colCenteredLabel: {
    display: "flex",
    alignItems: "center",
    width: "100%"
  },
  colCentered: {
    alignItems: "center",
    width: "100%"
  },
  contentFlex: {
    flex: "0 0 100%"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    margin: "4px 0 0 4px"
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  input: {
    width: "100%",
    minHeight: 38,
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565e",
    borderRadius: 4
  }
};

class SystemAlertDataPopup extends Component {
  state = {
    triggerRerender: false,
    systemAlertEnMessage: this.props.systemAlertData.enMessage || "",
    systemAlertFrMessage: this.props.systemAlertData.frMessage || "",
    criticalityOptions: [],
    isValidStartEndDateCombo: true,
    startDateState: this.props.disabledStartDate
      ? undefined
      : this.props.systemAlertData.startDate || undefined,
    isValidCompleteStartDatePicked: true,
    endDateState: this.props.systemAlertData.endDate || undefined,
    isValidCompleteEndDatePicked: true
  };

  constructor(props) {
    super(props);

    // Start DateTime
    this.startDateDayFieldRef = React.createRef();
    this.startDateMonthFieldRef = React.createRef();
    this.startDateYearFieldRef = React.createRef();
    this.startDateHourFieldRef = React.createRef();
    this.startDateMinFieldRef = React.createRef();

    // End DateTime
    this.endDateDayFieldRef = React.createRef();
    this.endDateMonthFieldRef = React.createRef();
    this.endDateYearFieldRef = React.createRef();
    this.endDateHourFieldRef = React.createRef();
    this.endDateMinFieldRef = React.createRef();

    this.PropTypes = {
      disabledFields: PropTypes.bool,
      // this props is useful for the modify popup when the start date is in the past
      disabledStartDate: PropTypes.bool,
      initialSystemAlertDataValidFormState: PropTypes.bool
    };

    SystemAlertDataPopup.defaultProps = {
      disabledFields: false,
      initialSystemAlertDataValidFormState: false
    };
  }

  componentDidMount = () => {
    // populating criticality options
    this.populatecriticalityOptions();
    this.props.updateSystemAlertDataValidFormState(this.props.initialSystemAlertDataValidFormState);
  };

  // populating criticality options for dropdown
  populatecriticalityOptions = () => {
    this.props.getCriticalityOptions().then(response => {
      const criticalityOptions = [];

      Object.keys(response).forEach(key => {
        criticalityOptions.push({
          label: response[key][`description_${this.props.currentLanguage}`],
          value: response[key].id
        });
      });

      this.setState({ criticalityOptions: criticalityOptions });
    });
  };

  // get system alert title
  getSystemAlertTitle = event => {
    const { systemAlertData } = this.props;
    systemAlertData.title = event.target.value;
    this.props.updateSystemAlertData(systemAlertData);
    this.setState({ triggerRerender: !this.state.triggerRerender }, () => {
      // getting form validity
      this.getFormValidity();
    });
  };

  // get selected criticality option from the dropdown
  getSelectedCriticalityOption = option => {
    const { systemAlertData } = this.props;
    systemAlertData.criticality = option;
    this.props.updateSystemAlertData(systemAlertData);
    this.setState({ triggerRerender: !this.state.triggerRerender }, () => {
      // getting form validity
      this.getFormValidity();
    });
  };

  // update systemAlertMessageEn when user enters a value in Message Text English
  updateSystemAlertMessageEn = event => {
    const { systemAlertData } = this.props;
    systemAlertData.enMessage = event.target.value;
    this.props.updateSystemAlertData(systemAlertData);
    this.setState(
      {
        triggerRerender: !this.state.triggerRerender,
        systemAlertEnMessage: event.target.value
      },
      () => {
        // getting form validity
        this.getFormValidity();
      }
    );
  };

  // update systemAlertMessageFr when user enters a value in Message Text French
  updateSystemAlertMessageFr = event => {
    const { systemAlertData } = this.props;
    systemAlertData.frMessage = event.target.value;
    this.props.updateSystemAlertData(systemAlertData);
    this.setState(
      {
        triggerRerender: !this.state.triggerRerender,
        systemAlertFrMessage: event.target.value
      },
      () => {
        // getting form validity
        this.getFormValidity();
      }
    );
  };

  // validate if the start date is valid
  triggerStartDateValidation = () => {
    // make sure that the start date is enabled before calling the validation
    if (!this.props.disabledStartDate) {
      // verify if all needed paramenters have been selected
      if (
        this.startDateYearFieldRef.current.state.value !== "" &&
        this.startDateMonthFieldRef.current.state.value !== "" &&
        this.startDateDayFieldRef.current.state.value !== "" &&
        this.startDateHourFieldRef.current.state.value !== "" &&
        this.startDateMinFieldRef.current.state.value !== ""
      ) {
        // use DatePicker's validation of the date
        const isValidDatePicked = validateDatePicked(
          `${this.startDateYearFieldRef.current.state.value.value}-${this.startDateMonthFieldRef.current.state.value.value}-${this.startDateDayFieldRef.current.state.value.value}`
        );

        // if DatePicker's validation is true, then:
        // 1- set isValidCompleteStartDatePicked to true
        // 2- start to validate if the chosen date is a valid start date
        if (isValidDatePicked) {
          let currentDate = new Date(
            this.startDateYearFieldRef.current.state.value.value,
            this.startDateMonthFieldRef.current.state.value.value - 1,
            this.startDateDayFieldRef.current.state.value.value,
            this.startDateHourFieldRef.current.state.value.value,
            this.startDateMinFieldRef.current.state.value.value,
            0,
            0
          );

          // create a moment object with our date so we can compare
          currentDate = moment(currentDate);

          // if the date is the same or older than now, we can say that the date is valid
          if (currentDate.isSameOrAfter(moment())) {
            // set the value of startDateState to the chosen date in format "YYYY-MM-DDTHH:mm:ss"
            currentDate = moment(currentDate).format("YYYY-MM-DDTHH:mm:ss");
            this.setState(
              {
                isValidCompleteStartDatePicked: true,
                startDateState: currentDate
              },
              () => {
                // endDateState is defined
                if (this.state.endDateState !== "" && this.state.endDateState !== undefined) {
                  this.triggerStartEndDateComboValidation();
                }
              }
            );
          }
          // make sure to set startDateState to ""
          else {
            this.setState(
              {
                isValidCompleteStartDatePicked: true,
                startDateState: "",
                isValidStartEndDateCombo: true
              },
              () => {
                // getting form validity
                this.getFormValidity();
              }
            );
          }
        }
        // if DatePicker's validation has failed, put isValidCompleteStartDatePicked to false
        else {
          this.setState(
            {
              isValidCompleteStartDatePicked: false,
              isValidStartEndDateCombo: true,
              startDateState: ""
            },
            () => {
              // getting form validity
              this.getFormValidity();
            }
          );
        }
      }
      // getting form validity
      this.getFormValidity();
    }
  };

  // validate if the end date is valid
  triggerEndDateValidation = () => {
    // verify if all needed paramenters have been selected
    if (
      this.endDateYearFieldRef.current.state.value !== "" &&
      this.endDateMonthFieldRef.current.state.value !== "" &&
      this.endDateDayFieldRef.current.state.value !== "" &&
      this.endDateHourFieldRef.current.state.value !== "" &&
      this.endDateMinFieldRef.current.state.value !== ""
    ) {
      // use DatePicker's validation of the date
      const isValidDatePicked = validateDatePicked(
        `${this.endDateYearFieldRef.current.state.value.value}-${this.endDateMonthFieldRef.current.state.value.value}-${this.endDateDayFieldRef.current.state.value.value}`
      );

      // if DatePicker's validation is true, then:
      // 1- set isValidCompleteStartDatePicked to true
      // 2- start to validate if the chosen date is a valid end date
      if (isValidDatePicked) {
        let currentDate = new Date(
          this.endDateYearFieldRef.current.state.value.value,
          this.endDateMonthFieldRef.current.state.value.value - 1,
          this.endDateDayFieldRef.current.state.value.value,
          this.endDateHourFieldRef.current.state.value.value,
          this.endDateMinFieldRef.current.state.value.value,
          0,
          0
        );

        // create a moment object with our date so we can compare
        currentDate = moment(currentDate);

        // if the date is older than now, we can say that the end date is valid
        if (currentDate.isAfter(moment())) {
          currentDate = moment(currentDate).format("YYYY-MM-DDTHH:mm:ss");
          this.setState(
            {
              isValidCompleteEndDatePicked: true,
              endDateState: currentDate
            },
            () => {
              // startDateState is defined
              if (this.state.startDateState !== "" && this.state.startDateState !== undefined) {
                this.triggerStartEndDateComboValidation();
                // disabledStartDate is set to true
              } else if (this.props.disabledStartDate) {
                // getting form validity
                this.getFormValidity();
              }
            }
          );
        }
        // make sure to set endDateState to ""
        else {
          this.setState(
            {
              isValidCompleteEndDatePicked: true,
              endDateState: "",
              isValidStartEndDateCombo: true
            },
            () => {
              // getting form validity
              this.getFormValidity();
            }
          );
        }
      }
      // if DatePicker's validation has failed, put isValidCompleteStartDatePicked to false
      else {
        this.setState(
          {
            isValidCompleteEndDatePicked: false,
            isValidStartEndDateCombo: true,
            endDateState: ""
          },
          () => {
            // getting form validity
            this.getFormValidity();
          }
        );
      }
    }
    // getting form validity
    this.getFormValidity();
  };

  // validate if the start and end date combo is valid
  triggerStartEndDateComboValidation = () => {
    const startDate = moment(this.state.startDateState, "YYYY-MM-DDTHH:mm:ss");
    const endDate = moment(this.state.endDateState, "YYYY-MM-DDTHH:mm:ss");

    // if end date is after start date, then the combo is valid
    if (endDate.isAfter(startDate)) {
      this.setState(
        {
          isValidStartEndDateCombo: true
        },
        () => {
          // getting form validity
          this.getFormValidity();
        }
      );
    } else {
      this.setState(
        {
          isValidStartEndDateCombo: false
        },
        () => {
          // getting form validity
          this.getFormValidity();
        }
      );
    }
  };

  getFormValidity = () => {
    // initializing needed variables
    let isValidForm = false;
    let isReadyForPreview = false;
    const isValidTitle = this.props.systemAlertData.title !== "";
    const isValidCriticality = typeof this.props.systemAlertData.criticality.value !== "undefined";
    const isValidEnMessage = this.props.systemAlertData.enMessage !== "";
    const isValidFrMessage = this.props.systemAlertData.frMessage !== "";
    let isValidStartDate = this.props.disabledStartDate;
    let isValidEndDate = false;

    // start date validation
    if (
      this.state.isValidCompleteStartDatePicked &&
      this.state.startDateState &&
      this.state.isValidStartEndDateCombo
    ) {
      isValidStartDate = true;
      // updating redux state
      const { systemAlertData } = this.props;
      systemAlertData.startDate = this.state.startDateState;
      this.props.updateSystemAlertData(systemAlertData);
    }

    // end date validation
    if (
      this.state.isValidCompleteEndDatePicked &&
      this.state.endDateState &&
      this.state.isValidStartEndDateCombo
    ) {
      isValidEndDate = true;
      // updating redux state
      const { systemAlertData } = this.props;
      systemAlertData.endDate = this.state.endDateState;
      this.props.updateSystemAlertData(systemAlertData);
    }

    // if all fields are valid
    if (
      isValidTitle &&
      isValidCriticality &&
      isValidEnMessage &&
      isValidFrMessage &&
      isValidStartDate &&
      isValidEndDate
    ) {
      // setting isValidForm to true
      isValidForm = true;
    }

    // checking if ready for preview
    if (isValidCriticality && isValidEnMessage && isValidFrMessage) {
      isReadyForPreview = true;
    }

    // updating redus states
    this.props.updateSystemAlertDataValidFormState(isValidForm);
    this.props.updateSystemAlertDataPreviewValidState(isReadyForPreview);
  };

  render() {
    // converting dates
    const convertedStartDate = new Date(this.props.systemAlertData.startDate);
    const convertedEndDate = new Date(this.props.systemAlertData.endDate);

    // accommodations
    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <div style={styles.mainContainer}>
        <Container>
          <Row style={styles.fieldSeparator}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
              style={{ ...styles.labelContainer, ...styles.colCenteredLabel }}
            >
              <label
                id="system-alert-popup-title-label"
                htmlFor="system-alert-popup-title-input"
                style={{ ...styles.label, ...styles.contentFlex }}
              >
                {
                  LOCALIZE.systemAdministrator.systemAlerts.tabs.activeSystemAlerts
                    .addSystemAlertPopup.fields.title
                }
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={{ ...styles.inputContainer, ...styles.colCentered }}
            >
              <input
                id="system-alert-popup-title-input"
                disabled={this.props.disabledFields}
                style={{ ...styles.input, ...accommodationsStyle }}
                type="text"
                value={this.props.systemAlertData.title}
                onChange={this.getSystemAlertTitle}
              ></input>
            </Col>
          </Row>
          <Row style={styles.fieldSeparator}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
              style={{ ...styles.labelContainer, ...styles.colCenteredLabel }}
            >
              <label
                id="system-alert-popup-criticality-label"
                style={{ ...styles.label, ...styles.contentFlex }}
              >
                {
                  LOCALIZE.systemAdministrator.systemAlerts.tabs.activeSystemAlerts
                    .addSystemAlertPopup.fields.criticality
                }
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={{ ...styles.inputContainer, ...styles.colCentered }}
            >
              <DropdownSelect
                idPrefix="system-alert-popup-criticality-dropdown"
                // isValid={true}
                ariaRequired={true}
                ariaLabelledBy="system-alert-popup-criticality-label"
                isDisabled={this.props.disabledFields}
                hasPlaceholder={true}
                options={this.state.criticalityOptions}
                onChange={this.getSelectedCriticalityOption}
                isMulti={false}
                orderByValues={true}
                orderByLabels={false}
                defaultValue={this.props.systemAlertData.criticality}
              />
            </Col>
          </Row>
          <Row style={styles.fieldSeparator}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
              style={{ ...styles.labelContainer, ...styles.colCenteredLabel }}
            >
              <label
                id="system-alert-popup-start-date-label"
                style={{ ...styles.label, ...styles.contentFlex }}
              >
                {
                  LOCALIZE.systemAdministrator.systemAlerts.tabs.activeSystemAlerts
                    .addSystemAlertPopup.fields.startDate
                }
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={{ ...styles.inputContainer, ...styles.colCentered }}
            >
              <DatePicker
                dateDayFieldRef={this.startDateDayFieldRef}
                initialDateDayValue={
                  this.props.systemAlertData.startDate !== "" && {
                    value: convertedStartDate.getDate(),
                    label:
                      convertedStartDate.getDate() < 10
                        ? `0${convertedStartDate.getDate()}`
                        : convertedStartDate.getDate()
                  }
                }
                dateMonthFieldRef={this.startDateMonthFieldRef}
                initialDateMonthValue={
                  this.props.systemAlertData.startDate !== "" && {
                    value: convertedStartDate.getMonth() + 1,
                    label:
                      convertedStartDate.getMonth() + 1 < 10
                        ? `0${convertedStartDate.getMonth() + 1}`
                        : convertedStartDate.getMonth() + 1
                  }
                }
                dateYearFieldRef={this.startDateYearFieldRef}
                initialDateYearValue={
                  this.props.systemAlertData.startDate !== "" && {
                    value: convertedStartDate.getFullYear(),
                    label: convertedStartDate.getFullYear()
                  }
                }
                timeHourFieldRef={this.startDateHourFieldRef}
                initialTimeHourValue={
                  this.props.systemAlertData.startDate !== "" && {
                    value: convertedStartDate.getHours(),
                    label:
                      convertedStartDate.getHours() < 10
                        ? `0${convertedStartDate.getHours()}`
                        : convertedStartDate.getHours()
                  }
                }
                timeMinFieldRef={this.startDateMinFieldRef}
                initialTimeMinValue={
                  this.props.systemAlertData.startDate !== "" && {
                    value: convertedStartDate.getMinutes(),
                    label:
                      convertedStartDate.getMinutes() < 10
                        ? `0${convertedStartDate.getMinutes()}`
                        : convertedStartDate.getMinutes()
                  }
                }
                dateLabelId={"system-alert-start-date-field"}
                menuPlacement={"bottom"}
                disabledDropdowns={this.props.disabledFields || this.props.disabledStartDate}
                allowTimeValue={true}
                onInputChange={this.triggerStartDateValidation}
                customYearOptions={populateCustomFutureYearsDateOptions(5)}
                isValidCompleteDatePicked={
                  this.state.isValidCompleteStartDatePicked &&
                  this.state.startDateState !== "" &&
                  this.state.isValidStartEndDateCombo
                }
                customInvalidDateErrorMessage={
                  // is the date valid (ex: February 31th isn't)
                  this.state.isValidCompleteStartDatePicked
                    ? // is the combo valid
                      this.state.isValidStartEndDateCombo
                      ? // if the combo is valid, then we know that the error is related to the start date (before today's date)
                        LOCALIZE.datePicker.futureDatePickedIncludingTodayError
                      : ""
                    : // if the end date isn't a valid date
                      LOCALIZE.datePicker.datePickedError
                }
              />
            </Col>
          </Row>
          <Row style={styles.fieldSeparator}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
              style={{ ...styles.labelContainer, ...styles.colCenteredLabel }}
            >
              <label
                id="system-alert-popup-end-date-label"
                style={{ ...styles.label, ...styles.contentFlex }}
              >
                {
                  LOCALIZE.systemAdministrator.systemAlerts.tabs.activeSystemAlerts
                    .addSystemAlertPopup.fields.endDate
                }
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={{ ...styles.inputContainer, ...styles.colCentered }}
            >
              <DatePicker
                dateDayFieldRef={this.endDateDayFieldRef}
                initialDateDayValue={
                  this.props.systemAlertData.endDate !== "" && {
                    value: convertedEndDate.getDate(),
                    label:
                      convertedEndDate.getDate() < 10
                        ? `0${convertedEndDate.getDate()}`
                        : convertedEndDate.getDate()
                  }
                }
                dateMonthFieldRef={this.endDateMonthFieldRef}
                initialDateMonthValue={
                  this.props.systemAlertData.endDate !== "" && {
                    value: convertedEndDate.getMonth() + 1,
                    label:
                      convertedEndDate.getMonth() + 1 < 10
                        ? `0${convertedEndDate.getMonth() + 1}`
                        : convertedEndDate.getMonth() + 1
                  }
                }
                dateYearFieldRef={this.endDateYearFieldRef}
                initialDateYearValue={
                  this.props.systemAlertData.endDate !== "" && {
                    value: convertedEndDate.getFullYear(),
                    label: convertedEndDate.getFullYear()
                  }
                }
                timeHourFieldRef={this.endDateHourFieldRef}
                initialTimeHourValue={
                  this.props.systemAlertData.endDate !== "" && {
                    value: convertedEndDate.getHours(),
                    label:
                      convertedEndDate.getHours() < 10
                        ? `0${convertedEndDate.getHours()}`
                        : convertedEndDate.getHours()
                  }
                }
                timeMinFieldRef={this.endDateMinFieldRef}
                initialTimeMinValue={
                  this.props.systemAlertData.endDate !== "" && {
                    value: convertedEndDate.getMinutes(),
                    label:
                      convertedEndDate.getMinutes() < 10
                        ? `0${convertedEndDate.getMinutes()}`
                        : convertedEndDate.getMinutes()
                  }
                }
                dateLabelId={"system-alert-end-date-field"}
                menuPlacement={"bottom"}
                disabledDropdowns={this.props.disabledFields}
                allowTimeValue={true}
                onInputChange={this.triggerEndDateValidation}
                customYearOptions={populateCustomFutureYearsDateOptions(5)}
                isValidCompleteDatePicked={
                  this.state.isValidCompleteEndDatePicked &&
                  this.state.endDateState !== "" &&
                  this.state.isValidStartEndDateCombo
                }
                customInvalidDateErrorMessage={
                  // is the date valid (ex: February 31th isn't)
                  this.state.isValidCompleteEndDatePicked
                    ? // is the combo valid
                      this.state.isValidStartEndDateCombo
                      ? // if the combo is valid, then we know that the error is related to the end date (before today's date)
                        LOCALIZE.datePicker.futureDatePickedError
                      : // if the combo isn't valid
                        LOCALIZE.datePicker.comboStartEndDatesPickedError
                    : // if the end date isn't a valid date
                      LOCALIZE.datePicker.datePickedError
                }
              />
            </Col>
          </Row>
          <Row style={styles.fieldSeparator}>
            <Col
              xl={12}
              lg={12}
              md={12}
              sm={12}
              xs={12}
              style={{ ...styles.labelContainer, ...styles.colCenteredLabel }}
            >
              <label
                id="add-system-alert-popup-message-text-label"
                style={{ ...styles.label, ...styles.contentFlex }}
              >
                {
                  LOCALIZE.systemAdministrator.systemAlerts.tabs.activeSystemAlerts
                    .addSystemAlertPopup.fields.messageText
                }
              </label>
            </Col>
            <Col
              xl={12}
              lg={12}
              md={12}
              sm={12}
              xs={12}
              style={{ ...styles.inputContainer, ...styles.colCentered }}
            >
              <AddSystemAlertMessagePopupTextTabs
                systemAlertMessageEn={this.state.systemAlertEnMessage}
                systemAlertMessageFr={this.state.systemAlertFrMessage}
                updateSystemAlertMessageEn={this.updateSystemAlertMessageEn}
                updateSystemAlertMessageFr={this.updateSystemAlertMessageFr}
                disabledFields={this.props.disabledFields}
              />
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export { SystemAlertDataPopup as unconnectedSystemAlertDataPopup };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    systemAlertData: state.systemAlerts.systemAlertData
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getCriticalityOptions,
      updateSystemAlertData,
      updateSystemAlertDataValidFormState,
      updateSystemAlertDataPreviewValidState
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(SystemAlertDataPopup);
