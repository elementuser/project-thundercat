import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import LOCALIZE from "../../../text_resources";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faBinoculars,
  faPlusCircle,
  faSave,
  faSpinner,
  faTimes,
  faTrashAlt,
  faPencilAlt,
  faFileArchive
} from "@fortawesome/free-solid-svg-icons";
import StyledTooltip, { TYPE, EFFECT, TRIGGER_TYPE } from "../../authentication/StyledTooltip";
import {
  createSystemAlert,
  modifySystemAlert,
  archiveSystemAlert,
  getCriticalityOptions,
  resetSystemAlertData,
  updateSystemAlertDataPreviewValidState,
  updateSystemAlertData,
  getAllActiveSystemAlerts,
  getSystemAlertData,
  updateSystemAlertDataValidFormState,
  triggerArchivedSystemAlertTableRerender
} from "../../../modules/SystemAlertsRedux";
import "../../../css/etta.css";
import GenericTable, { COMMON_STYLE } from "../../commons/GenericTable";
import CustomButton, { THEME } from "../../commons/CustomButton";
import { getLineSpacingCSS } from "../../../modules/AccommodationsRedux";
import PopupBox, { BUTTON_STATE, BUTTON_TYPE } from "../../commons/PopupBox";
import { Col, Row } from "react-bootstrap";
import { LANGUAGES } from "../../commons/Translation";
import SystemMessage, { MESSAGE_TYPE } from "../../commons/SystemMessage";
import SystemAlertDataPopup from "./SystemAlertDataPopup";
import AlertMessage from "../../commons/AlertMessage";
import getCriticalityDataForSystemAlert from "../../../helpers/criticality";
import moment, { now } from "moment";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 12,
    xl: 3
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 12,
    xl: 9
  }
};

const styles = {
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  buttonIcon: {
    marginRight: 0
  },
  testAdminRow: {
    padding: "6px 6px 6px 12px"
  },
  addButtonContainer: {
    marginTop: 18
  },
  buttonLabel: {
    margin: 0,
    paddingLeft: 6
  },
  labelContainer: {
    display: "table-cell",
    verticalAlign: "middle"
  },
  label: {
    overflowWrap: "break-word",
    margin: "0 12px 0 0"
  },
  inputContainer: {
    display: "table-cell",
    width: "100%",
    verticalAlign: "middle"
  },
  fieldSeparator: {
    marginTop: 18
  },
  colCenteredLabel: {
    display: "flex",
    alignItems: "center",
    width: "100%"
  },
  colCentered: {
    alignItems: "center",
    width: "100%"
  },
  contentFlex: {
    flex: "0 0 100%"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    margin: "4px 0 0 4px"
  },
  allUnset: {
    all: "unset"
  },
  boldText: {
    fontWeight: "bold"
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  popupDescriptionContainer: {
    marginTop: 12
  },
  input: {
    width: "100%",
    minHeight: 38,
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565e",
    borderRadius: 4
  },
  genericTable: {
    margin: "18px 0px 12px"
  }
};

class ActiveSystemAlerts extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  static propTypes = {
    // provided by redux
    getAllActiveSystemAlerts: PropTypes.func
  };

  state = {
    clearSearchTriggered: false,
    rowsDefinition: {},
    previousPageNumber: 0,
    currentlyLoading: false,
    showAddSystemAlertPopup: false,
    popupDataCurrentlyLoading: false,
    addSystemAlertPopupRequestSuccessful: false,
    showSuccessAddSystemAlertPopup: false,
    criticalityCodename: "",
    showSystemAlertPreviewPopup: false,
    previewSystemAlertPopupCurrentLanguage: this.props.currentLanguage,
    triggerRerender: false,
    criticalityOptions: [],
    showSystemAlertViewPopup: false,
    showModifyPopup: false,
    activeSystemAlerts: [],
    modifySystemAlertPopupRequestSuccessful: false,
    showArchivePopup: false,
    archiveSystemAlertPopupRequestSuccessful: false,
    disabledStartDate: false
  };

  componentDidMount = () => {
    this.populateSystemAlerts();
    // populating criticality options
    this.props.getCriticalityOptions().then(response => {
      this.setState({ criticalityOptions: response });
    });
    // resetting system alert data redux states
    this.props.resetSystemAlertData();
    // updating systemAlertDataPreviewValidState redux state
    this.props.updateSystemAlertDataPreviewValidState(false);
  };

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentDidUpdate = prevProps => {
    // if triggerActiveSystemAlertTableRerender gets updated
    if (
      prevProps.triggerActiveSystemAlertTableRerender !==
      this.props.triggerActiveSystemAlertTableRerender
    ) {
      // update table
      this.populateSystemAlerts();
    }
  };

  populateSystemAlerts = () => {
    this._isMounted = true;
    this.setState({ currentlyLoading: true }, () => {
      const activeSystemAlertsArray = [];
      this.props
        .getAllActiveSystemAlerts()
        .then(response => {
          if (this._isMounted) {
            this.populateActiveSystemAlertsObject(activeSystemAlertsArray, response);
          }
        })
        .then(() => {
          if (this._isMounted) {
            this.setState({ currentlyLoading: false });
          }
        });
    });
  };

  getFormattedCriticality = criticalityId => {
    // getting index of respective criticality ID
    const index = this.state.criticalityOptions.findIndex(obj => {
      return obj.id === criticalityId;
    });
    // getting respective criticality data
    return this.state.criticalityOptions[index][`description_${this.props.currentLanguage}`];
  };

  populateActiveSystemAlertsObject = (activeSystemAlertsArray, response) => {
    // initializing needed object and array for rowsDefinition props (needed for GenericTable component)
    let rowsDefinition = {};
    const data = [];
    // making sure that there are some results
    if (typeof response !== "undefined") {
      // looping in response given
      for (let i = 0; i < response.length; i++) {
        const currentResult = response[i];
        // pushing needed data in activeSystemAlerts array
        activeSystemAlertsArray.push({
          id: currentResult.id,
          title: currentResult.title,
          criticality: currentResult.criticality,
          active_date: currentResult.active_date,
          end_date: currentResult.end_date,
          message_text_en: currentResult.message_text_en,
          message_text_fr: currentResult.message_text_fr
        });
        // pushing needed data in data array (needed for GenericTable component)
        data.push({
          column_1: `${currentResult.title}`,
          column_2: this.getFormattedCriticality(currentResult.criticality),
          column_3: `${currentResult.active_date.split("T")[0]} ${currentResult.active_date
            .split("T")[1]
            .substring(0, 5)}`,
          column_4: `${currentResult.end_date.split("T")[0]} ${currentResult.end_date
            .split("T")[1]
            .substring(0, 5)}`,
          column_5: (
            <div className="d-flex justify-content-center flex-nowrap">
              <StyledTooltip
                id={`active-system-alerts-view-button-${i}`}
                place="top"
                type={TYPE.light}
                effect={EFFECT.solid}
                triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      dataTip=""
                      dataFor={`active-system-alerts-view-button-${i}`}
                      label={
                        <>
                          <FontAwesomeIcon icon={faBinoculars} style={styles.buttonIcon} />
                        </>
                      }
                      ariaLabel={
                        LOCALIZE.systemAdministrator.systemAlerts.tabs.activeSystemAlerts.table
                          .viewTooltip
                      }
                      action={() => this.handleViewSelectedActiveSystemAlert(i)}
                      customStyle={styles.actionButton}
                      buttonTheme={THEME.SECONDARY}
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>
                      {
                        LOCALIZE.systemAdministrator.systemAlerts.tabs.activeSystemAlerts.table
                          .viewTooltip
                      }
                    </p>
                  </div>
                }
              />
              <StyledTooltip
                id={`active-system-alerts-modify-button-${i}`}
                place="top"
                type={TYPE.light}
                effect={EFFECT.solid}
                triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      dataTip=""
                      dataFor={`active-system-alerts-modify-button-${i}`}
                      label={
                        <>
                          <FontAwesomeIcon icon={faPencilAlt} style={styles.buttonIcon} />
                        </>
                      }
                      ariaLabel={
                        LOCALIZE.systemAdministrator.systemAlerts.tabs.activeSystemAlerts.table
                          .modifyTooltip
                      }
                      action={() => this.handleModifySelectedActiveSystemAlert(i)}
                      customStyle={styles.actionButton}
                      buttonTheme={THEME.SECONDARY}
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>
                      {
                        LOCALIZE.systemAdministrator.systemAlerts.tabs.activeSystemAlerts.table
                          .modifyTooltip
                      }
                    </p>
                  </div>
                }
              />
              <StyledTooltip
                id={`active-system-alerts-archive-button-${i}`}
                place="top"
                type={TYPE.light}
                effect={EFFECT.solid}
                triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      dataTip=""
                      dataFor={`active-system-alerts-archive-button-${i}`}
                      label={
                        <>
                          <FontAwesomeIcon icon={faFileArchive} style={styles.buttonIcon} />
                        </>
                      }
                      ariaLabel={
                        LOCALIZE.systemAdministrator.systemAlerts.tabs.activeSystemAlerts.table
                          .archiveToolTip
                      }
                      action={() => this.handleArchiveSelectedActiveSystemAlert(i)}
                      customStyle={styles.actionButton}
                      buttonTheme={THEME.SECONDARY}
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>
                      {
                        LOCALIZE.systemAdministrator.systemAlerts.tabs.activeSystemAlerts.table
                          .archiveToolTip
                      }
                    </p>
                  </div>
                }
              />
            </div>
          )
        });
      }
    }

    // updating rowsDefinition object with provided data and needed style
    rowsDefinition = {
      column_1_style: COMMON_STYLE.LEFT_TEXT,
      column_2_style: COMMON_STYLE.CENTERED_TEXT,
      column_3_style: COMMON_STYLE.CENTERED_TEXT,
      column_4_style: COMMON_STYLE.CENTERED_TEXT,
      column_5_style: COMMON_STYLE.CENTERED_TEXT,
      data: data
    };

    // saving results in state
    this.setState({
      activeSystemAlerts: activeSystemAlertsArray,
      rowsDefinition: rowsDefinition,
      previousPageNumber: response.previous_page_number
    });
  };

  handleAddNewSystemAlert = () => {
    this.setState({ showAddSystemAlertPopup: true });
  };

  closeAddSystemAlertPopup = () => {
    // updating systemAlertDataPreviewValidState redux state
    this.props.updateSystemAlertDataPreviewValidState(false);
    this.setState(
      {
        showAddSystemAlertPopup: false,
        previewSystemAlertPopupCurrentLanguage: this.props.currentLanguage
      },
      () => {
        // adding small delay + setting addSystemAlertPopupRequestSuccessful state only after setting the showAddSystemAlertPopup state to avoid showing a quick flash of the default popup content (form)
        setTimeout(() => {
          this.setState({ addSystemAlertPopupRequestSuccessful: false });
          // resetting system alert data redux states
          this.props.resetSystemAlertData();
        }, 100);
      }
    );
  };

  openSystemAlertPreviewPopup = () => {
    // getting criticality data based on provided ID
    this.props
      .getCriticalityOptions(this.props.systemAlertData.criticality.value)
      .then(response => {
        // updating needed states
        this.setState({
          showSystemAlertPreviewPopup: true,
          criticalityCodename: response.codename
        });
      });
  };

  closeSystemAlertPreviewPopup = () => {
    this.setState({ showSystemAlertPreviewPopup: false });
  };

  closeSystemAlertViewPopup = () => {
    this.setState({ showSystemAlertViewPopup: false });
  };

  handleTogglePreviewLanguage = () => {
    // current preview language is English
    if (this.state.previewSystemAlertPopupCurrentLanguage === LANGUAGES.english) {
      // toggle language
      this.setState({ previewSystemAlertPopupCurrentLanguage: LANGUAGES.french });
      // current preview language is Frebcg
    } else {
      // toggle language
      this.setState({ previewSystemAlertPopupCurrentLanguage: LANGUAGES.english });
    }
  };

  handleAddSystemAlert = () => {
    // needed parameters to create the system alert
    const body = {
      title: this.props.systemAlertData.title,
      criticality: this.props.systemAlertData.criticality.value,
      active_date: this.props.systemAlertData.startDate,
      end_date: this.props.systemAlertData.endDate,
      message_text_en: this.props.systemAlertData.enMessage,
      message_text_fr: this.props.systemAlertData.frMessage
    };

    // call backend to create the new system alert
    this.props.createSystemAlert(body).then(response => {
      // if there is a validation error
      if (response.ok) {
        this.setState({
          addSystemAlertPopupRequestSuccessful: true
        });
        // update table
        this.populateSystemAlerts();
        // should never happen
      } else {
        throw new Error("An error occurred during the add system alert request process");
      }
    });
  };

  handleModifySystemAlert = index => {
    // needed parameters to create the system alert
    const body = {
      id: this.props.systemAlertData.id,
      title: this.props.systemAlertData.title,
      criticality: this.props.systemAlertData.criticality.value,
      active_date: this.props.systemAlertData.startDate,
      end_date: this.props.systemAlertData.endDate,
      message_text_en: this.props.systemAlertData.enMessage,
      message_text_fr: this.props.systemAlertData.frMessage
    };

    // call backend to create the new system alert
    this.props.modifySystemAlert(body).then(response => {
      // if there is a validation error
      if (response.ok) {
        this.setState({
          modifySystemAlertPopupRequestSuccessful: true
        });
        // should never happen
      } else {
        throw new Error("An error occurred during the add system alert request process");
      }
    });
  };

  handleArchiveSystemAlert = index => {
    // needed parameters to create the system alert
    const body = {
      id: this.props.systemAlertData.id
    };

    // call backend to create the new system alert
    this.props.archiveSystemAlert(body).then(response => {
      // if there is a validation error
      if (response.ok) {
        this.setState({
          archiveSystemAlertPopupRequestSuccessful: true
        });
        this.populateSystemAlerts();
        this.props.triggerArchivedSystemAlertTableRerender();
        // should never happen
      } else {
        throw new Error("An error occurred during the add system alert request process");
      }
    });
  };

  // handling view selected active system alert
  handleViewSelectedActiveSystemAlert = index => {
    // getting system alert data
    const systemAlertId = this.state.activeSystemAlerts[index].id;
    this.props.getSystemAlertData(systemAlertId).then(response => {
      // updating system alert data redux state
      const systemAlertData = {
        title: response.title,
        criticality: {
          value: response.criticality_data.id,
          label: response.criticality_data[`description_${this.props.currentLanguage}`]
        },
        startDate: response.active_date,
        endDate: response.end_date,
        enMessage: response.message_text_en,
        frMessage: response.message_text_fr
      };
      this.props.updateSystemAlertData(systemAlertData);
      // adding small delay before opening the popup to make sure that the redux states are properly loaded
      setTimeout(() => {
        // opening popup
        this.setState({
          showSystemAlertViewPopup: true,
          criticalityCodename: response.criticality_data.codename
        });
      }, 100);
    });
  };

  // handling modify selected active system alert
  handleModifySelectedActiveSystemAlert = index => {
    // updating systemAlertDataPreviewValidState redux state
    this.props.updateSystemAlertDataPreviewValidState(true);
    // getting system alert data
    const systemAlertId = this.state.activeSystemAlerts[index].id;
    this.props.getSystemAlertData(systemAlertId).then(response => {
      // updating system alert data redux state
      const systemAlertData = {
        id: response.id,
        title: response.title,
        criticality: {
          value: response.criticality_data.id,
          label: response.criticality_data[`description_${this.props.currentLanguage}`]
        },
        startDate: response.active_date,
        endDate: response.end_date,
        enMessage: response.message_text_en,
        frMessage: response.message_text_fr
      };
      this.props.updateSystemAlertData(systemAlertData);
      // adding small delay before opening the popup to make sure that the redux states are properly loaded
      setTimeout(() => {
        // opening popup
        this.setState({
          showModifyPopup: true,
          criticalityCodename: response.criticality_data.codename,
          disabledStartDate: this.getStartDateStatus(this.state.activeSystemAlerts[index])
        });
      }, 100);
    });
  };

  // handling archive selected active system alert
  handleArchiveSelectedActiveSystemAlert = index => {
    // getting system alert data
    const systemAlertId = this.state.activeSystemAlerts[index].id;
    // updating system alert data redux state
    const systemAlertData = {
      id: systemAlertId
    };
    this.props.updateSystemAlertData(systemAlertData);
    // adding small delay before opening the popup to make sure that the redux states are properly loaded
    setTimeout(() => {
      // opening popup
      this.setState({
        showArchivePopup: true
      });
    }, 100);
  };

  closeModifyPopup = () => {
    // updating systemAlertDataPreviewValidState redux state
    this.props.updateSystemAlertDataPreviewValidState(false);
    // resetting system alert data redux states
    this.props.resetSystemAlertData();
    // closing popup
    this.setState({ showModifyPopup: false }, () => {
      // adding small delay + setting cloneSystemAlertPopupRequestSuccessful state only after setting the showClonePopup state to avoid showing a quick flash of the default popup content (form)
      setTimeout(() => {
        this.setState({ modifySystemAlertPopupRequestSuccessful: false });
        // resetting system alert data redux states
        this.props.resetSystemAlertData();
        // update table
        this.populateSystemAlerts();
      }, 100);
    });
  };

  closeArchivePopup = () => {
    // updating systemAlertDataPreviewValidState redux state
    this.props.updateSystemAlertDataPreviewValidState(false);
    // resetting system alert data redux states
    this.props.resetSystemAlertData();
    // closing popup
    this.setState({ showArchivePopup: false }, () => {
      // adding small delay + setting cloneSystemAlertPopupRequestSuccessful state only after setting the showClonePopup state to avoid showing a quick flash of the default popup content (form)
      setTimeout(() => {
        this.setState({ archiveSystemAlertPopupRequestSuccessful: false });
      }, 100);
    });
  };

  getStartDateStatus = activeSystemAlertData => {
    // initializing disabledStartDate
    let disabledStartDate = false;

    // getting current time
    let currentDate = new Date(now);
    currentDate = moment(currentDate);

    // getting start date
    const startDate = new Date(activeSystemAlertData.active_date);

    // start date is in the past
    if (moment().isSameOrAfter(startDate)) {
      disabledStartDate = true;
    }

    return disabledStartDate;
  };

  render() {
    const columnsDefinition = [
      {
        label: LOCALIZE.systemAdministrator.systemAlerts.tabs.activeSystemAlerts.table.column_1,
        style: {}
      },
      {
        label: LOCALIZE.systemAdministrator.systemAlerts.tabs.activeSystemAlerts.table.column_2,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.systemAdministrator.systemAlerts.tabs.activeSystemAlerts.table.column_3,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.systemAdministrator.systemAlerts.tabs.activeSystemAlerts.table.column_4,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.systemAdministrator.systemAlerts.tabs.activeSystemAlerts.table.column_5,
        style: COMMON_STYLE.CENTERED_TEXT
      }
    ];
    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    // setting the popup overflow visible variable
    let popupOverflowVisible = false;

    // if spacing is enabled
    if (this.props.accommodations.spacing) {
      popupOverflowVisible = false;
      // if font size is greater than 35px
    } else if (parseInt(this.props.accommodations.fontSize.split("px")[0]) > 29) {
      popupOverflowVisible = false;
      // if test order number, test to administer and test session language are defined/selected
    }

    return (
      <div>
        <Row className="align-items-center justify-content-start" style={styles.addButtonContainer}>
          <Col xl={"auto"} lg={"auto"} md={"auto"} sm={"auto"} xs={"auto"}>
            <CustomButton
              label={
                <>
                  {this.state.popupDataCurrentlyLoading && (
                    // eslint-disable-next-line jsx-a11y/label-has-associated-control
                    <label className="fa fa-spinner fa-spin">
                      <FontAwesomeIcon icon={faSpinner} />
                    </label>
                  )}
                  {!this.state.popupDataCurrentlyLoading && (
                    <>
                      <FontAwesomeIcon icon={faPlusCircle} />
                      <span id="add-system-alert-button" style={styles.buttonLabel}>
                        {
                          LOCALIZE.systemAdministrator.systemAlerts.tabs.activeSystemAlerts
                            .addNewSystemAlertButton
                        }
                      </span>
                    </>
                  )}
                </>
              }
              disabled={this.state.popupDataCurrentlyLoading}
              action={() => this.handleAddNewSystemAlert()}
              buttonTheme={THEME.PRIMARY}
            />
          </Col>
        </Row>
        <div style={styles.genericTable}>
          <GenericTable
            classnamePrefix="active-system-alerts"
            columnsDefinition={columnsDefinition}
            rowsDefinition={this.state.rowsDefinition}
            emptyTableMessage={LOCALIZE.SearchBarWithDisplayOptions.noResultsFound}
            currentlyLoading={this.state.currentlyLoading}
          />
        </div>
        <PopupBox
          show={this.state.showAddSystemAlertPopup}
          handleClose={() => {}}
          isBackdropStatic={true}
          shouldCloseOnEsc={false}
          displayCloseButton={!this.state.addSystemAlertPopupRequestSuccessful}
          closeButtonAction={this.closeAddSystemAlertPopup}
          overflowVisible={popupOverflowVisible}
          size={this.state.addSystemAlertPopupRequestSuccessful ? "lg" : "xl"}
          title={
            LOCALIZE.systemAdministrator.systemAlerts.tabs.activeSystemAlerts.addSystemAlertPopup
              .title
          }
          description={
            <div>
              {!this.state.addSystemAlertPopupRequestSuccessful && <SystemAlertDataPopup />}
              {this.state.addSystemAlertPopupRequestSuccessful && (
                <SystemMessage
                  messageType={MESSAGE_TYPE.success}
                  title={LOCALIZE.commons.success}
                  message={
                    <p>
                      {
                        LOCALIZE.systemAdministrator.systemAlerts.tabs.activeSystemAlerts
                          .addSystemAlertPopup.successfulMessage
                      }
                    </p>
                  }
                />
              )}
            </div>
          }
          leftButtonType={
            !this.state.addSystemAlertPopupRequestSuccessful
              ? BUTTON_TYPE.secondary
              : BUTTON_TYPE.none
          }
          leftButtonIcon={faBinoculars}
          leftButtonTitle={LOCALIZE.commons.previewButton}
          leftButtonLabel={LOCALIZE.commons.previewButton}
          leftButtonAction={this.openSystemAlertPreviewPopup}
          leftButtonState={
            this.props.systemAlertDataPreviewValidState
              ? BUTTON_STATE.enabled
              : BUTTON_STATE.disabled
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonIcon={!this.state.addSystemAlertPopupRequestSuccessful ? faSave : ""}
          rightButtonTitle={
            !this.state.addSystemAlertPopupRequestSuccessful
              ? LOCALIZE.systemAdministrator.systemAlerts.tabs.activeSystemAlerts
                  .addSystemAlertPopup.buttons.rightButton
              : LOCALIZE.commons.ok
          }
          rightButtonLabel={
            !this.state.addSystemAlertPopupRequestSuccessful
              ? LOCALIZE.systemAdministrator.systemAlerts.tabs.activeSystemAlerts
                  .addSystemAlertPopup.buttons.rightButton
              : LOCALIZE.commons.ok
          }
          rightButtonAction={
            !this.state.addSystemAlertPopupRequestSuccessful
              ? this.handleAddSystemAlert
              : this.closeAddSystemAlertPopup
          }
          rightButtonState={
            this.props.systemAlertDataValidForm ? BUTTON_STATE.enabled : BUTTON_STATE.disabled
          }
        />
        <PopupBox
          show={this.state.showSystemAlertPreviewPopup}
          handleClose={() => {}}
          isBackdropStatic={true}
          shouldCloseOnEsc={false}
          size={"lg"}
          title={LOCALIZE.commons.previewButton}
          description={
            <div>
              <AlertMessage
                alertType={getCriticalityDataForSystemAlert(this.state.criticalityCodename)}
                message={
                  this.state.previewSystemAlertPopupCurrentLanguage === LANGUAGES.english
                    ? this.props.systemAlertData.enMessage
                    : this.props.systemAlertData.frMessage
                }
              />
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={
            this.state.previewSystemAlertPopupCurrentLanguage === LANGUAGES.english
              ? LOCALIZE.commons.french
              : LOCALIZE.commons.english
          }
          leftButtonLabel={
            this.props.previewSystemAlertPopupCurrentLanguage === LANGUAGES.english
              ? LOCALIZE.commons.french
              : LOCALIZE.commons.english
          }
          leftButtonAction={this.handleTogglePreviewLanguage}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonIcon={faTimes}
          rightButtonTitle={LOCALIZE.commons.close}
          rightButtonLabel={LOCALIZE.commons.close}
          rightButtonAction={this.closeSystemAlertPreviewPopup}
        />
        <PopupBox
          show={this.state.showSystemAlertViewPopup}
          handleClose={() => {}}
          isBackdropStatic={true}
          shouldCloseOnEsc={false}
          size={"lg"}
          title={LOCALIZE.systemAdministrator.systemAlerts.tabs.activeSystemAlerts.viewPopupTitle}
          description={
            <div>
              <AlertMessage
                alertType={getCriticalityDataForSystemAlert(this.state.criticalityCodename)}
                message={
                  this.state.previewSystemAlertPopupCurrentLanguage === LANGUAGES.english
                    ? this.props.systemAlertData.enMessage
                    : this.props.systemAlertData.frMessage
                }
              />
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={
            this.state.previewSystemAlertPopupCurrentLanguage === LANGUAGES.english
              ? LOCALIZE.commons.french
              : LOCALIZE.commons.english
          }
          leftButtonLabel={
            this.props.previewSystemAlertPopupCurrentLanguage === LANGUAGES.english
              ? LOCALIZE.commons.french
              : LOCALIZE.commons.english
          }
          leftButtonAction={this.handleTogglePreviewLanguage}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonIcon={faTimes}
          rightButtonTitle={LOCALIZE.commons.close}
          rightButtonLabel={LOCALIZE.commons.close}
          rightButtonAction={this.closeSystemAlertViewPopup}
        />
        <PopupBox
          show={this.state.showSuccessAddSystemAlertPopup}
          title={
            LOCALIZE.systemAdministrator.testAccesses.tabs.orderlessTestAccesses
              .confirmDeleteOrderlessTaPopup.title
          }
          handleClose={() => {}}
          size={"lg"}
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.error}
                title={LOCALIZE.commons.warning}
                message={
                  <p>
                    {LOCALIZE.formatString(
                      LOCALIZE.systemAdministrator.testAccesses.tabs.orderlessTestAccesses
                        .confirmDeleteOrderlessTaPopup.systemMessageDescription,
                      <span style={styles.boldText}>{this.state.selectedOrderlessTaToDelete}</span>
                    )}
                  </p>
                }
              />
              <p style={styles.popupDescriptionContainer}>
                {
                  LOCALIZE.systemAdministrator.testAccesses.tabs.orderlessTestAccesses
                    .confirmDeleteOrderlessTaPopup.description
                }
              </p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          leftButtonAction={() => this.closeAddSystemAlertPopup()}
          leftButtonState={
            !this.state.deleteOrderlessTaPopupLoading ? BUTTON_STATE.enabled : BUTTON_STATE.disabled
          }
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={
            !this.state.deleteOrderlessTaPopupLoading ? (
              LOCALIZE.commons.confirm
            ) : (
              // eslint-disable-next-line jsx-a11y/label-has-associated-control
              <label className="fa fa-spinner fa-spin">
                <FontAwesomeIcon icon={faSpinner} />
              </label>
            )
          }
          rightButtonIcon={!this.state.deleteOrderlessTaPopupLoading ? faTrashAlt : ""}
          rightButtonAction={() => this.handleDeleteSelectedOrderlessTestAdministrator()}
          rightButtonState={
            !this.state.deleteOrderlessTaPopupLoading ? BUTTON_STATE.enabled : BUTTON_STATE.disabled
          }
        />
        <PopupBox
          show={this.state.showModifyPopup}
          handleClose={() => {}}
          isBackdropStatic={true}
          shouldCloseOnEsc={false}
          displayCloseButton={!this.state.modifySystemAlertPopupRequestSuccessful}
          closeButtonAction={this.closeModifyPopup}
          size={this.state.modifySystemAlertPopupRequestSuccessful ? "lg" : "xl"}
          title={LOCALIZE.systemAdministrator.systemAlerts.tabs.activeSystemAlerts.modifyPopupTitle}
          description={
            <div>
              {!this.state.modifySystemAlertPopupRequestSuccessful && (
                <SystemAlertDataPopup
                  disabledStartDate={this.state.disabledStartDate}
                  initialSystemAlertDataValidFormState={true}
                />
              )}
              {this.state.modifySystemAlertPopupRequestSuccessful && (
                <SystemMessage
                  messageType={MESSAGE_TYPE.success}
                  title={LOCALIZE.commons.success}
                  message={
                    <p>
                      {
                        LOCALIZE.systemAdministrator.systemAlerts.tabs.activeSystemAlerts
                          .modifySuccessfullyMessage
                      }
                    </p>
                  }
                />
              )}
            </div>
          }
          leftButtonType={
            !this.state.modifySystemAlertPopupRequestSuccessful
              ? BUTTON_TYPE.secondary
              : BUTTON_TYPE.none
          }
          leftButtonIcon={faBinoculars}
          leftButtonTitle={LOCALIZE.commons.previewButton}
          leftButtonLabel={LOCALIZE.commons.previewButton}
          leftButtonAction={this.openSystemAlertPreviewPopup}
          leftButtonState={
            this.props.systemAlertDataPreviewValidState
              ? BUTTON_STATE.enabled
              : BUTTON_STATE.disabled
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonIcon={!this.state.modifySystemAlertPopupRequestSuccessful ? faPencilAlt : ""}
          rightButtonTitle={
            !this.state.modifySystemAlertPopupRequestSuccessful
              ? LOCALIZE.systemAdministrator.systemAlerts.tabs.activeSystemAlerts.modifyRightButton
              : LOCALIZE.commons.ok
          }
          rightButtonLabel={
            !this.state.modifySystemAlertPopupRequestSuccessful
              ? LOCALIZE.systemAdministrator.systemAlerts.tabs.activeSystemAlerts.modifyRightButton
              : LOCALIZE.commons.ok
          }
          rightButtonAction={
            !this.state.modifySystemAlertPopupRequestSuccessful
              ? this.handleModifySystemAlert
              : this.closeModifyPopup
          }
          rightButtonState={
            this.props.systemAlertDataValidForm ? BUTTON_STATE.enabled : BUTTON_STATE.disabled
          }
        />
        <PopupBox
          show={this.state.showArchivePopup}
          handleClose={() => {}}
          isBackdropStatic={true}
          shouldCloseOnEsc={false}
          size={"lg"}
          title={
            LOCALIZE.systemAdministrator.systemAlerts.tabs.activeSystemAlerts
              .archiveSystemAlertPopup.title
          }
          description={
            <div>
              {!this.state.archiveSystemAlertPopupRequestSuccessful && (
                <SystemMessage
                  messageType={MESSAGE_TYPE.warning}
                  title={
                    LOCALIZE.systemAdministrator.systemAlerts.tabs.activeSystemAlerts
                      .archiveSystemAlertPopup.systemMessageTitle
                  }
                  message={
                    <p>
                      {
                        LOCALIZE.systemAdministrator.systemAlerts.tabs.activeSystemAlerts
                          .archiveSystemAlertPopup.systemMessageMessage
                      }
                    </p>
                  }
                />
              )}
              {this.state.archiveSystemAlertPopupRequestSuccessful && (
                <SystemMessage
                  messageType={MESSAGE_TYPE.success}
                  title={LOCALIZE.commons.success}
                  message={
                    <p>
                      {
                        LOCALIZE.systemAdministrator.systemAlerts.tabs.activeSystemAlerts
                          .archiveSystemAlertPopup.archiveSuccessfullyMessage
                      }
                    </p>
                  }
                />
              )}
            </div>
          }
          leftButtonType={
            !this.state.archiveSystemAlertPopupRequestSuccessful
              ? BUTTON_TYPE.secondary
              : BUTTON_TYPE.none
          }
          leftButtonIcon={faTimes}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonLabel={LOCALIZE.commons.cancel}
          leftButtonAction={this.closeArchivePopup}
          leftButtonState={BUTTON_STATE.enabled}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonIcon={
            !this.state.archiveSystemAlertPopupRequestSuccessful ? faFileArchive : ""
          }
          rightButtonTitle={
            !this.state.archiveSystemAlertPopupRequestSuccessful
              ? LOCALIZE.systemAdministrator.systemAlerts.tabs.activeSystemAlerts
                  .archiveSystemAlertPopup.archiveRightButton
              : LOCALIZE.commons.ok
          }
          rightButtonLabel={
            !this.state.archiveSystemAlertPopupRequestSuccessful
              ? LOCALIZE.systemAdministrator.systemAlerts.tabs.activeSystemAlerts
                  .archiveSystemAlertPopup.archiveRightButton
              : LOCALIZE.commons.ok
          }
          rightButtonAction={
            !this.state.archiveSystemAlertPopupRequestSuccessful
              ? this.handleArchiveSystemAlert
              : this.closeArchivePopup
          }
          rightButtonState={BUTTON_STATE.enabled}
        />
      </div>
    );
  }
}

export { ActiveSystemAlerts as unconnectedActiveSystemAlerts };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    systemAlertData: state.systemAlerts.systemAlertData,
    systemAlertDataValidForm: state.systemAlerts.systemAlertDataValidForm,
    systemAlertDataPreviewValidState: state.systemAlerts.systemAlertDataPreviewValidState,
    triggerActiveSystemAlertTableRerender: state.systemAlerts.triggerActiveSystemAlertTableRerender
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      createSystemAlert,
      modifySystemAlert,
      archiveSystemAlert,
      getCriticalityOptions,
      resetSystemAlertData,
      updateSystemAlertDataPreviewValidState,
      updateSystemAlertData,
      getAllActiveSystemAlerts,
      getSystemAlertData,
      updateSystemAlertDataValidFormState,
      triggerArchivedSystemAlertTableRerender
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ActiveSystemAlerts);
