import React, { Component } from "react";
import { Container } from "react-bootstrap";
import { useTabState, Tab, TabList, TabPanel } from "reakit/Tab";
import { connect } from "react-redux";
import LOCALIZE from "../../../text_resources";
import { getLineSpacingCSS } from "../../../modules/AccommodationsRedux";
import PropTypes from "prop-types";

const styles = {
  row: {
    position: "relative"
  },
  bottomMargin: {
    marginBottom: 32
  },
  menuTabStyle: {
    padding: 8,
    border: "2px solid transparent",
    borderTopLeftRadius: "0.35rem",
    borderTopRightRadius: "0.35rem",
    borderBottomWidth: "4px",
    color: "#00565e",
    borderColor: "transparent transparent #00565e",
    marginBottom: "-2px",
    fontWeight: "bold",
    textDecoration: "none",
    background: "white",
    marginLeft: "2px"
  },
  menuTabInactive: {
    padding: 8,
    border: "none",
    textDecoration: "underlined",
    background: "white",
    marginLeft: "2px"
  },
  input: {
    width: "100%",
    minHeight: 100,
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565e",
    borderRadius: 4
  },
  reasonInput: {
    height: "100%",
    resize: "none",
    width: "100%",
    marginTop: "2px",
    borderTop: "3px solid rgb(0, 86, 94)"
  },
  messageTextBox: {
    padding: "0 12px"
  }
};

function AddSystemAlertMessagePopupTextTabsHooks(props) {
  const tab = useTabState();

  let accommodationsStyle = {
    fontSize: props.accommodations.fontSize
  };
  const lineSpacingStyle = getLineSpacingCSS();
  if (props.accommodations.spacing) {
    accommodationsStyle = {
      ...accommodationsStyle,
      ...{
        lineHeight: lineSpacingStyle.lineHeight,
        letterSpacing: lineSpacingStyle.letterSpacing,
        wordSpacing: lineSpacingStyle.wordSpacing
      }
    };
  }

  return (
    <div style={styles.messageTextBox}>
      <TabList {...tab}>
        <Tab
          {...tab}
          id={"add-system-alerts-popup-message-text-tabs-english"}
          aria-labelledby="add-system-alert-popup-message-text-label add-system-alerts-popup-message-text-tabs-english-label"
          style={
            tab.selectedId === "add-system-alerts-popup-message-text-tabs-english"
              ? styles.menuTabStyle
              : styles.menuTabInactive
          }
        >
          <span id={"add-system-alerts-popup-message-text-tabs-english-label"}>
            {
              LOCALIZE.systemAdministrator.systemAlerts.tabs.activeSystemAlerts.addSystemAlertPopup
                .fields.messageTextTabs.english
            }
          </span>
        </Tab>
        <Tab
          {...tab}
          id={"add-system-alerts-popup-message-text-tabs-french"}
          aria-labelledby="add-system-alert-popup-message-text-label add-system-alerts-popup-message-text-tabs-french-label"
          style={
            tab.selectedId === "add-system-alerts-popup-message-text-tabs-french"
              ? styles.menuTabStyle
              : styles.menuTabInactive
          }
        >
          <span id={"add-system-alerts-popup-message-text-tabs-french-label"}>
            {
              LOCALIZE.systemAdministrator.systemAlerts.tabs.activeSystemAlerts.addSystemAlertPopup
                .fields.messageTextTabs.french
            }
          </span>
        </Tab>
      </TabList>
      <TabPanel tabIndex="-1" {...tab}>
        <textarea
          id="add-system-alerts-popup-message-text-english"
          aria-labelledby="add-system-alert-popup-message-text-label"
          aria-required={true}
          style={{
            ...styles.input,
            ...styles.reasonInput,
            ...accommodationsStyle
          }}
          maxLength="300"
          value={props.systemAlertMessageEn}
          disabled={props.disabledFields}
          onChange={props.updateSystemAlertMessageEn}
        ></textarea>
      </TabPanel>
      <TabPanel tabIndex="-1" {...tab}>
        <textarea
          id="add-system-alerts-popup-message-text-french"
          aria-labelledby="add-system-alert-popup-message-text-label"
          aria-required={true}
          style={{
            ...styles.input,
            ...styles.reasonInput,
            ...accommodationsStyle
          }}
          maxLength="300"
          value={props.systemAlertMessageFr}
          disabled={props.disabledFields}
          onChange={props.updateSystemAlertMessageFr}
        ></textarea>
      </TabPanel>
    </div>
  );
}

function withMyHook(Component) {
  return function WrappedComponent(props) {
    const myHookValue = AddSystemAlertMessagePopupTextTabsHooks(props);
    return <Component {...props} myHookValue={myHookValue} />;
  };
}

const mapStateToProps = (state, ownProps) => {
  return {
    accommodations: state.accommodations
  };
};

class AddSystemAlertMessagePopupTextTabs extends Component {
  constructor(props, context) {
    super(props, context);
    this.PropTypes = {
      updateSystemAlertMessageEn: PropTypes.func,
      updateSystemAlertMessageFr: PropTypes.func,
      disabledFields: PropTypes.bool
    };
  }

  render() {
    const { myHookValue } = this.props;

    return <>{myHookValue}</>;
  }
}

export default connect(mapStateToProps)(withMyHook(AddSystemAlertMessagePopupTextTabs));
