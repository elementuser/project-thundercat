import React, { Component } from "react";
import PropTypes from "prop-types";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { isTokenStillValid } from "../../modules/LoginRedux";
import { bindActionCreators } from "redux";
import ContentContainer from "../commons/ContentContainer";
import { Helmet } from "react-helmet";
import SideNavigation from "../eMIB/SideNavigation";
// import Dashboard from "./Dashboard";
import ActiveTests from "./ActiveTests";
import TestAccessCodes from "./TestAccessCodes";
import Permissions from "./permissions/Permissions";
import TestAccesses from "./test_accesses/TestAccesses";
import UserLookUp from "./UserLookUp";
// import IncidentReports from "./IncidentReports";
// import Scoring from "./Scoring";
// import ReScoring from "./ReScoring";
import Reports from "./Reports";
import LastLogin from "../authentication/LastLogin";
import { PATH } from "../commons/Constants";
import { setBOUserSideNavState } from "../../modules/UserLookUpRedux";
import SystemAlerts from "./system_alerts/SystemAlerts";
import RDItemBanks from "./rd_item_banks/RDItemBanks";

export const styles = {
  header: {
    marginBottom: 24
  },
  sectionContainerLabelDiv: {
    paddingLeft: 18,
    marginBottom: "-3px"
  },
  sectionContainerLabel: {
    padding: "4px 24px",
    marginBottom: 0,
    position: "relative"
  },
  tabStyleBorder: {
    borderBottom: "5px solid #00565e",
    width: "100%",
    position: "absolute",
    bottom: 0,
    left: 0,
    borderRadius: "4px 4px 0 0"
  },
  sectionContainer: {
    borderStyle: "solid",
    borderWidth: "3px 1px 1px 1px",
    borderColor: "#CECECE",
    minHeight: 400,
    padding: "12px 0",
    marginBottom: 24
  },
  tabContainer: {
    zIndex: 1,
    padding: 0
  },
  tabContent: {
    backgroundColor: "transparent",
    height: "100%",
    padding: 5
  },
  nav: {
    marginTop: 10,
    marginLeft: 10,
    width: 206,
    textAlign: "center"
  },
  body: {
    paddingLeft: 12
  },
  sideNavBodyContent: {
    marginTop: "-12px"
  },
  appPadding: {
    padding: "15px"
  }
};

class SystemAdministration extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  static propTypes = {
    // Props from Redux
    isTokenStillValid: PropTypes.func
  };

  state = {
    isLoaded: false,
    isEttaDashboard: false
  };

  componentDidMount = () => {
    this._isMounted = true;
    // check if the token is still valid before getting user information
    this.props.isTokenStillValid().then(bool => {
      // if the token is still valid
      if (this._isMounted && bool) {
        this.setState({
          isLoaded: true
        });
      }
    });

    if (this.props.currentHomePage === PATH.systemAdministration) {
      this.setState({
        isEttaDashboard: true
      });
    }
  };

  componentWillUnmount = () => {
    this._isMounted = false;
  };

  // Returns array where each item indicates specifications related to How To Page including the title and the body
  getSystemAdministrationSections = () => {
    const sideNavItems = [];

    // for both ETTA and R&D Operations users
    sideNavItems.push({
      menuString: LOCALIZE.systemAdministrator.sideNavItems.permissions,
      body: <Permissions triggerPopulatePendingPermissions={this.state.isLoaded} />
    });

    // for R&D Operations users only
    if (this.props.isRdOperations) {
      sideNavItems.push({
        menuString: LOCALIZE.systemAdministrator.sideNavItems.itemBanks,
        body: <RDItemBanks />
      });
    }

    // for ETTA users only
    if (this.props.isEtta) {
      sideNavItems.push(
        {
          menuString: LOCALIZE.systemAdministrator.sideNavItems.testAccesses,
          body: <TestAccesses />
        },
        {
          menuString: LOCALIZE.systemAdministrator.sideNavItems.testAccessCodes,
          body: <TestAccessCodes />
        },
        {
          menuString: LOCALIZE.systemAdministrator.sideNavItems.activeTests,
          body: <ActiveTests />
        },
        {
          menuString: LOCALIZE.systemAdministrator.sideNavItems.userLookUp,
          body: <UserLookUp />
        },
        {
          menuString: LOCALIZE.systemAdministrator.sideNavItems.reports,
          body: <Reports />
        },
        {
          menuString: LOCALIZE.systemAdministrator.sideNavItems.systemAlert,
          body: <SystemAlerts />
        }
      );
    }
    return sideNavItems;
  };

  render() {
    const specs = this.getSystemAdministrationSections();
    return (
      <div className="app" style={styles.appPadding}>
        <Helmet>
          <html lang={this.props.currentLanguage} />
          <title className="notranslate">
            {this.props.isEtta && !this.props.isRdOperations
              ? LOCALIZE.titles.SystemAdministration
              : !this.props.isEtta && this.props.isRdOperations
              ? LOCALIZE.titles.rdOperations
              : LOCALIZE.titles.systemAdminAndRdOperations}
          </title>
        </Helmet>
        {this.state.isEttaDashboard && <LastLogin lastLoginDate={this.props.lastLogin} />}
        <ContentContainer>
          <div id="main-content" role="main">
            <div
              id="user-welcome-message-div"
              style={styles.header}
              aria-labelledby="user-welcome-message"
              className="notranslate"
            >
              <h1 id="user-welcome-message" className="green-divider">
                {LOCALIZE.formatString(
                  LOCALIZE.profile.title,
                  this.props.firstName,
                  this.props.lastName
                )}
              </h1>
            </div>
            <div>
              <div style={styles.sectionContainerLabelDiv}>
                <div>
                  <label style={styles.sectionContainerLabel} className="notranslate">
                    {this.props.isEtta && !this.props.isRdOperations
                      ? LOCALIZE.systemAdministrator.containerLabelEtta
                      : !this.props.isEtta && this.props.isRdOperations
                      ? LOCALIZE.systemAdministrator.containerLabelRD
                      : LOCALIZE.systemAdministrator.containerLabelBoth}
                    <span style={styles.tabStyleBorder}></span>
                  </label>
                </div>
              </div>
              <div style={styles.sectionContainer}>
                <section aria-label={LOCALIZE.ariaLabel.sideNavigationSection}>
                  <SideNavigation
                    specs={specs}
                    startIndex={this.props.selectedSideNavItem}
                    loadOnClick={true}
                    displayNextPreviousButton={false}
                    isMain={true}
                    tabContainerStyle={styles.tabContainer}
                    tabContentStyle={styles.tabContent}
                    navStyle={styles.nav}
                    bodyContentCustomStyle={styles.sideNavBodyContent}
                    updateSelectedSideNavItem={this.props.setBOUserSideNavState}
                  />
                </section>
              </div>
            </div>
          </div>
        </ContentContainer>
      </div>
    );
  }
}

export { SystemAdministration as unconnectedSystemAdministration };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    firstName: state.user.firstName,
    lastName: state.user.lastName,
    lastLogin: state.user.lastLogin,
    currentHomePage: state.userPermissions.currentHomePage,
    selectedSideNavItem: state.userLookUp.selectedSideNavItem,
    isEtta: state.userPermissions.isEtta,
    isRdOperations: state.userPermissions.isRdOperations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      isTokenStillValid,
      setBOUserSideNavState
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(SystemAdministration);
