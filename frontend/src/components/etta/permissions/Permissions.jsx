import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import LOCALIZE from "../../../text_resources";
import { bindActionCreators } from "redux";
import { Tabs, Tab, Row, Col } from "react-bootstrap";
import PermissionRequests from "./PermissionRequests";
import ActivePermissions from "./ActivePermissions";
import {
  getPendingPermissions,
  getActivePermissions,
  getFoundActivePermissions,
  updateCurrentPermissionsPageState,
  updatePermissionsPageSizeState,
  getFoundPendingPermissions,
  updateSearchActivePermissionsStates,
  updateCurrentPendingPermissionsPageState,
  updatePendingPermissionsPageSizeState,
  updateSearchActivePendingPermissionsStates
} from "../../../modules/PermissionsRedux";
import { COMMON_STYLE } from "../../commons/GenericTable";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit, faBinoculars } from "@fortawesome/free-solid-svg-icons";
import CustomButton, { THEME } from "../../commons/CustomButton";

const styles = {
  mainContainer: {
    width: "100%"
  },
  tabNavigation: {
    borderBottom: "none"
  },
  tabContainer: {
    marginTop: 2,
    borderTop: "3px solid #00565e",
    backgroundColor: "#F8F8F8"
  },
  viewEditDetailsBtn: {
    minWidth: 100
  },
  buttonIcon: {
    marginRight: 6
  },
  viewPermissionRequestButton: {
    marginLeft: 6
  }
};

class Permissions extends Component {
  static propTypes = {
    triggerPopulatePendingPermissions: PropTypes.bool.isRequired,
    // Props from Redux
    getPendingPermissions: PropTypes.func,
    getActivePermissions: PropTypes.func,
    getFoundActivePermissions: PropTypes.func,
    updateCurrentPermissionsPageState: PropTypes.func,
    updatePermissionsPageSizeState: PropTypes.func,
    updateSearchActivePermissionsStates: PropTypes.func,
    getFoundPendingPermissions: PropTypes.func,
    updateCurrentPendingPermissionsPageState: PropTypes.func,
    updatePendingPermissionsPageSizeState: PropTypes.func,
    updateSearchActivePendingPermissionsStates: PropTypes.func
  };

  state = {
    pendingPermissions: [],
    activePermissions: [],
    selectedActivePermission: {},
    rowsDefinitionActivePermissions: {},
    rowsDefinitionPermissionsRequests: {},
    activePermissionsCount: 0,
    numberOfActivePermissionsPages: 1,
    numberOfPendingPermissionsPages: 1,
    activePermissionsResultsFound: 0,
    pendingPermissionsResultsFound: 0,
    clearSearchPermissionsTriggered: false,
    clearSearchPendingPermissionsTriggered: false,
    currentlyLoadingActivePermissions: true,
    currentlyLoadingPermissionRequests: true,
    triggerShowViewPermissionRequestPopup: false,
    selectedPermissionRequestData: {}
  };

  componentDidMount = () => {
    // initialize pagination page redux state
    this.props.updateCurrentPermissionsPageState(1);
    this.props.updateCurrentPendingPermissionsPageState(1);
    // initialize search permissionsKeyword, permissionsActiveSearch/pendingPermissionsActiveSearch redux states
    this.props.updateSearchActivePermissionsStates("", false);
    this.props.updateSearchActivePendingPermissionsStates("", false);
    // populating tables
    this.populatePendingPermissions();
    this.populateActivePermissionsBasedOnActiveSearch();
  };

  componentDidUpdate = prevProps => {
    // if language gets updated
    if (prevProps.currentLanguage !== this.props.currentLanguage) {
      this.populatePendingPermissions();
      this.populateActivePermissionsBasedOnActiveSearch();
    }
    // if triggerPopulatePendingPermissions gets updated
    if (
      prevProps.triggerPopulatePendingPermissions !== this.props.triggerPopulatePendingPermissions
    ) {
      this.populatePendingPermissions();
      this.populateActivePermissionsBasedOnActiveSearch();
    }
    // if permissionsPaginationPage gets updated
    if (prevProps.permissionsPaginationPage !== this.props.permissionsPaginationPage) {
      this.populateActivePermissionsBasedOnActiveSearch();
    }
    // if permissionsPaginationPageSize get updated
    if (prevProps.permissionsPaginationPageSize !== this.props.permissionsPaginationPageSize) {
      this.populateActivePermissionsBasedOnActiveSearch();
    }
    // if search permissionsKeyword gets updated
    if (prevProps.permissionsKeyword !== this.props.permissionsKeyword) {
      // if permissionsKeyword redux state is empty
      if (this.props.permissionsKeyword !== "") {
        this.populateFoundActivePermissions();
      } else if (this.props.permissionsKeyword === "" && this.props.permissionsActiveSearch) {
        this.populateFoundActivePermissions();
      }
    }
    // if permissionsActiveSearch gets updated
    if (prevProps.permissionsActiveSearch !== this.props.permissionsActiveSearch) {
      // there is no current active search (on clear search action)
      if (!this.props.permissionsActiveSearch) {
        // triggering clear search action state
        this.setState({ clearSearchPermissionsTriggered: true }, () => {
          this.populateActivePermissions();
        });
        // there is a current active search (on search action)
      } else {
        this.populateFoundActivePermissions();
      }
    }
    // if pendingPermissionsPaginationPage gets updated
    if (
      prevProps.pendingPermissionsPaginationPage !== this.props.pendingPermissionsPaginationPage
    ) {
      this.populatePendingPermissionsBasedOnActiveSearch();
    }
    // if permissionsPaginationPageSize get updated
    if (
      prevProps.pendingPermissionsPaginationPageSize !==
      this.props.pendingPermissionsPaginationPageSize
    ) {
      this.populatePendingPermissionsBasedOnActiveSearch();
    }
    // if search pendingPermissionsKeyword gets updated
    if (prevProps.pendingPermissionsKeyword !== this.props.pendingPermissionsKeyword) {
      // if permissionsKeyword redux state is empty
      if (this.props.pendingPermissionsKeyword !== "") {
        this.populateFoundPendingPermissions();
      } else if (
        this.props.pendingPermissionsKeyword === "" &&
        this.props.pendingPermissionsActiveSearch
      ) {
        this.populateFoundPendingPermissions();
      }
    }
    // if pendingPermissionsActiveSearch gets updated
    if (prevProps.pendingPermissionsActiveSearch !== this.props.pendingPermissionsActiveSearch) {
      // there is no current active search (on clear search action)
      if (!this.props.pendingPermissionsActiveSearch) {
        // triggering clear search action state
        this.setState({ clearSearchPendingPermissionsTriggered: true }, () => {
          this.populatePendingPermissions();
        });
        // there is a current active search (on search action)
      } else {
        this.populateFoundPendingPermissions();
      }
    }
  };

  // populating active permissions based on the permissionsActiveSearch redux state
  populateActivePermissionsBasedOnActiveSearch = () => {
    // current search
    if (this.props.permissionsActiveSearch) {
      this.populateFoundActivePermissions();
      // no current search
    } else {
      this.populateActivePermissions();
    }
  };

  // populating pending permissions based on the pendingPermissionsActiveSearch redux state
  populatePendingPermissionsBasedOnActiveSearch = () => {
    // current search
    if (this.props.pendingPermissionsActiveSearch) {
      this.populateFoundPendingPermissions();
      // no current search
    } else {
      this.populatePendingPermissions();
    }
  };

  // populating all existing pending permissions
  populatePendingPermissions = () => {
    // setting currentlyLoadingPermissionRequests to true as soon as this function is called
    this.setState({ currentlyLoadingPermissionRequests: true }, () => {
      const pendingPermissions = [];
      this.props
        .getPendingPermissions(
          this.props.pendingPermissionsPaginationPage,
          this.props.pendingPermissionsPaginationPageSize
        )
        .then(response => {
          this.populatePendingPermissionsObject(pendingPermissions, response);
        })
        .then(() => {
          if (this.state.clearSearchPendingPermissionsTriggered) {
            // go back to the first page to avoid display bugs
            this.props.updateCurrentPendingPermissionsPageState(1);
            this.setState({ clearSearchPendingPermissionsTriggered: false });
          }
        })
        .then(() => {
          // setting currentlyLoadingPermissionRequests to false as soon as all data has been provided to the interface
          this.setState({
            currentlyLoadingPermissionRequests: false
          });
        });
    });
  };

  // populating all found pending permissions based on a search
  populateFoundPendingPermissions = () => {
    // setting currentlyLoadingPermissionRequests to true as soon as this function is called
    this.setState({ currentlyLoadingPermissionRequests: true }, () => {
      const pendingPermissions = [];
      this.props
        .getFoundPendingPermissions(
          this.props.currentLanguage,
          this.props.pendingPermissionsKeyword,
          this.props.pendingPermissionsPaginationPage,
          this.props.pendingPermissionsPaginationPageSize
        )
        .then(response => {
          // there are no results found
          if (response[0] === "no results found") {
            this.setState({
              pendingPermissions: [],
              rowsDefinitionPermissionsRequests: {},
              numberOfPendingPermissionsPages: 1,
              pendingPermissionsResultsFound: 0
            });
            // there is at least one result found
          } else {
            this.populatePendingPermissionsObject(pendingPermissions, response);
            this.setState({ pendingPermissionsResultsFound: response.count });
          }
        })
        .then(() => {
          // setting currentlyLoadingPermissionRequests to false as soon as all data has been provided to the interface
          this.setState({ currentlyLoadingPermissionRequests: false }, () => {
            // if there is at least one result found
            if (pendingPermissions.length > 0) {
              // focusing on results found label
              // make sure that this element exsits to avoid any error
              if (document.getElementById("permission-requests-results-found")) {
                document.getElementById("permission-requests-results-found").focus();
              }
              // no results found
            } else {
              // focusing on no results found row
              document.getElementById("permission-requests-no-data").focus();
            }
          });
        });
    });
  };

  populatePermissionsRequestPopupData = index => {
    this.setState({
      triggerShowViewPermissionRequestPopup: !this.state.triggerShowViewPermissionRequestPopup,
      selectedPermissionRequestData: {
        username: this.state.pendingPermissions[index].username,
        firstName: this.state.pendingPermissions[index].first_name,
        lastName: this.state.pendingPermissions[index].last_name,
        gocEmail: this.state.pendingPermissions[index].goc_email,
        priOrMilitaryNbr: this.state.pendingPermissions[index].pri_or_military_nbr,
        supervisor: this.state.pendingPermissions[index].supervisor,
        supervisorEmail: this.state.pendingPermissions[index].supervisor_email,
        rationale: this.state.pendingPermissions[index].rationale,
        permissionRequested: this.state.pendingPermissions[index].permission_requested,
        permissionRequestedId: this.state.pendingPermissions[index].permission_requested_id,
        permissionRequestId: this.state.pendingPermissions[index].permission_request_id
      }
    });
  };

  populatePendingPermissionsObject = (pendingPermissions, response) => {
    // initializing needed object and array for rowsDefinitionPermissionsRequests props (needed for GenericTable component)
    let rowsDefinitionPermissionsRequests = {};
    const data = [];
    // making sure that response.results exists (to avoid unwanted errors)
    if (response.results) {
      // looping in all existing pending permissions
      for (let i = 0; i < response.results.length; i++) {
        // pushing needed data in pendingPermissions array
        pendingPermissions.push({
          username: response.results[i].username,
          permission_requested_id: response.results[i].permission_requested,
          permission_requested: response.results[i][`${this.props.currentLanguage}_name`],
          first_name: response.results[i].first_name,
          last_name: response.results[i].last_name,
          goc_email: response.results[i].goc_email,
          pri_or_military_nbr: response.results[i].pri_or_military_nbr,
          supervisor: response.results[i].supervisor,
          supervisor_email: response.results[i].supervisor_email,
          rationale: response.results[i].rationale,
          permission_request_id: response.results[i].permission_request_id
        });
        // pushing needed data in data array (needed for GenericTable component)
        data.push({
          column_1: response.results[i][`${this.props.currentLanguage}_name`],
          column_2: `${response.results[i].last_name}, ${response.results[i].first_name} (${response.results[i].email})`,
          column_3: response.results[i].request_date.substring(0, 10),
          column_4: (
            <CustomButton
              label={
                <div>
                  <FontAwesomeIcon icon={faBinoculars} />
                  <span style={styles.viewPermissionRequestButton}>
                    {LOCALIZE.systemAdministrator.permissions.tabs.permissionRequests.viewButton}
                  </span>
                </div>
              }
              action={() => this.populatePermissionsRequestPopupData(i)}
              buttonTheme={THEME.SECONDARY}
              ariaLabel={LOCALIZE.formatString(
                LOCALIZE.systemAdministrator.permissions.tabs.permissionRequests
                  .viewButtonAccessibility,
                response.results[i].first_name,
                response.results[i].last_name
              )}
            />
          )
        });
      }
    }

    // updating rowsDefinitionPermissionsRequests object with provided data and needed style
    rowsDefinitionPermissionsRequests = {
      column_1_style: { padding: "0 12px" },
      column_2_style: { padding: "0 12px" },
      column_3_style: COMMON_STYLE.CENTERED_TEXT,
      column_4_style: COMMON_STYLE.CENTERED_TEXT,
      padding: "0 12px",
      data: data
    };

    // saving results in state
    this.setState({
      pendingPermissions: pendingPermissions,
      rowsDefinitionPermissionsRequests: rowsDefinitionPermissionsRequests,
      numberOfPendingPermissionsPages: Math.ceil(
        response.count / this.props.pendingPermissionsPaginationPageSize
      )
    });
  };

  // populating all existing active permissions
  populateActivePermissions = () => {
    // setting currentlyLoadingActivePermissions to true as soon as this function is called
    this.setState({ currentlyLoadingActivePermissions: true }, () => {
      const activePermissions = [];
      this.props
        .getActivePermissions(
          this.props.permissionsPaginationPage,
          this.props.permissionsPaginationPageSize
        )
        .then(response => {
          this.populateActivePermissionsObject(activePermissions, response);
        })
        .then(() => {
          if (this.state.clearSearchPermissionsTriggered) {
            // go back to the first page to avoid display bugs
            this.props.updateCurrentPermissionsPageState(1);
            this.setState({ clearSearchPermissionsTriggered: false });
          }
        })
        .then(() => {
          // setting currentlyLoadingActivePermissions to false as soon as all data has been provided to the interface
          this.setState({ currentlyLoadingActivePermissions: false });
        });
    });
  };

  // populating all found active permissions based on a search
  populateFoundActivePermissions = () => {
    // setting currentlyLoadingActivePermissions to true as soon as this function is called
    this.setState({ currentlyLoadingActivePermissions: true }, () => {
      const activePermissions = [];
      this.props
        .getFoundActivePermissions(
          this.props.currentLanguage,
          this.props.permissionsKeyword,
          this.props.permissionsPaginationPage,
          this.props.permissionsPaginationPageSize
        )
        .then(response => {
          // there are no results found
          if (response[0] === "no results found") {
            this.setState({
              activePermissions: [],
              rowsDefinitionActivePermissions: {},
              numberOfActivePermissionsPages: 1,
              activePermissionsResultsFound: 0
            });
            // there is at least one result found
          } else {
            this.populateActivePermissionsObject(activePermissions, response);
            this.setState({ activePermissionsResultsFound: response.count });
          }
        })
        .then(() => {
          // setting currentlyLoadingActivePermissions to false as soon as all data has been provided to the interface
          this.setState({ currentlyLoadingActivePermissions: false }, () => {
            // if there is at least one result found
            if (activePermissions.length > 0) {
              // focusing on results found label
              // make sure that this element exsits to avoid any error
              if (document.getElementById("active-permissions-results-found")) {
                document.getElementById("active-permissions-results-found").focus();
              }
              // no results found
            } else {
              // focusing on no results found row
              document.getElementById("active-permissions-no-data").focus();
            }
          });
        });
    });
  };

  populateActivePermissionsObject = (activePermissions, response) => {
    // initializing needed object and array for rowsDefinitionActivePermissions props (needed for GenericTable component)
    let rowsDefinitionActivePermissions = {};
    const data = [];
    // making sure that response.results exists (to avoid unwanted errors)
    if (response.results) {
      // looping in response given
      for (let i = 0; i < response.results.length; i++) {
        // pushing needed data in activePermissions array
        activePermissions.push({
          username: response.results[i].user,
          user_permission_id: response.results[i].user_permission_id,
          permission: response.results[i][`${this.props.currentLanguage}_name`],
          first_name: response.results[i].first_name,
          last_name: response.results[i].last_name,
          permission_id: response.results[i].permission,
          goc_email: response.results[i].goc_email,
          pri_or_military_nbr: response.results[i].pri_or_military_nbr,
          supervisor: response.results[i].supervisor,
          supervisor_email: response.results[i].supervisor_email
        });
        // pushing needed data in data array (needed for GenericTable component)
        data.push({
          column_1: `${response.results[i].last_name}, ${response.results[i].first_name} (${response.results[i].email})`,
          column_2: response.results[i][`${this.props.currentLanguage}_name`],
          column_3: (
            <CustomButton
              buttonId={`active-permissions-button-row-${i}`}
              label={
                <>
                  <FontAwesomeIcon icon={faEdit} style={styles.buttonIcon} />
                  <span>
                    {
                      LOCALIZE.systemAdministrator.permissions.tabs.activePermissions.table
                        .actionButtonLabel
                    }
                  </span>
                </>
              }
              action={() => {
                this.handleViewSelectedPermission(i);
              }}
              customStyle={styles.viewEditDetailsBtn}
              buttonTheme={THEME.SECONDARY}
              ariaLabel={LOCALIZE.formatString(
                LOCALIZE.systemAdministrator.permissions.tabs.activePermissions.table
                  .actionButtonAriaLabel,
                response.results[i].first_name,
                response.results[i].last_name
              )}
            />
          )
        });
      }
    }

    // updating rowsDefinitionActivePermissions object with provided data and needed style
    rowsDefinitionActivePermissions = {
      column_1_style: { padding: "0 12px", overflowWrap: "break-word" },
      column_2_style: { padding: "0 12px", overflowWrap: "break-word" },
      column_3_style: COMMON_STYLE.CENTERED_TEXT,
      padding: "0 12px",
      data: data
    };

    // saving results in state
    this.setState({
      activePermissions: activePermissions,
      rowsDefinitionActivePermissions: rowsDefinitionActivePermissions,
      numberOfActivePermissionsPages: Math.ceil(
        response.count / this.props.permissionsPaginationPageSize
      )
    });
  };

  handleViewSelectedPermission = id => {
    this.setState({
      selectedActivePermission: {
        userPermissionId: this.state.activePermissions[id].user_permission_id,
        firstName: this.state.activePermissions[id].first_name,
        lastName: this.state.activePermissions[id].last_name,
        gocEmail: this.state.activePermissions[id].goc_email,
        priOrMilitaryNbr: this.state.activePermissions[id].pri_or_military_nbr,
        supervisor: this.state.activePermissions[id].supervisor,
        supervisorEmail: this.state.activePermissions[id].supervisor_email,
        permission: this.state.activePermissions[id].permission
      }
    });
  };

  render() {
    const TABS = [
      {
        key: "permission-requests",
        tabName: LOCALIZE.systemAdministrator.permissions.tabs.permissionRequests.title,
        body: (
          <PermissionRequests
            pendingPermissions={this.state.pendingPermissions}
            populatePendingPermissions={this.populatePendingPermissions}
            populateActivePermissions={this.populateActivePermissions}
            rowsDefinition={this.state.rowsDefinitionPermissionsRequests}
            triggerShowViewPermissionRequestPopup={this.state.triggerShowViewPermissionRequestPopup}
            selectedPermissionRequestData={this.state.selectedPermissionRequestData}
            numberOfPages={this.state.numberOfPendingPermissionsPages}
            resultsFound={this.state.pendingPermissionsResultsFound}
            currentlyLoading={this.state.currentlyLoadingPermissionRequests}
            populateFoundPendingPermissions={this.populateFoundPendingPermissions}
          />
        )
      },
      {
        key: "active-permissions",
        tabName: LOCALIZE.systemAdministrator.permissions.tabs.activePermissions.title,
        body: (
          <ActivePermissions
            activePermissions={this.state.activePermissions}
            selectedActivePermission={this.state.selectedActivePermission}
            rowsDefinition={this.state.rowsDefinitionActivePermissions}
            populateActivePermissions={this.populateActivePermissions}
            populateFoundActivePermissions={this.populateFoundActivePermissions}
            numberOfPages={this.state.numberOfActivePermissionsPages}
            resultsFound={this.state.activePermissionsResultsFound}
            currentlyLoading={this.state.currentlyLoadingActivePermissions}
          />
        )
      }
    ];
    return (
      <div style={styles.mainContainer}>
        <div>
          <h2>{LOCALIZE.systemAdministrator.sideNavItems.permissions}</h2>
          <p>{LOCALIZE.systemAdministrator.permissions.description}</p>
        </div>
        <div>
          <Row>
            <Col>
              <Tabs
                defaultActiveKey="permission-requests"
                id="permissions-tabs"
                style={styles.tabNavigation}
              >
                {TABS.map((tab, index) => {
                  return (
                    <Tab
                      key={index}
                      eventKey={tab.key}
                      title={tab.tabName}
                      style={styles.tabContainer}
                    >
                      {tab.body}
                    </Tab>
                  );
                })}
              </Tabs>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    permissionsPaginationPage: state.userPermissions.permissionsPaginationPage,
    permissionsPaginationPageSize: state.userPermissions.permissionsPaginationPageSize,
    permissionsKeyword: state.userPermissions.permissionsKeyword,
    permissionsActiveSearch: state.userPermissions.permissionsActiveSearch,
    pendingPermissionsPaginationPage: state.userPermissions.pendingPermissionsPaginationPage,
    pendingPermissionsPaginationPageSize:
      state.userPermissions.pendingPermissionsPaginationPageSize,
    pendingPermissionsKeyword: state.userPermissions.pendingPermissionsKeyword,
    pendingPermissionsActiveSearch: state.userPermissions.pendingPermissionsActiveSearch
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getPendingPermissions,
      getActivePermissions,
      getFoundActivePermissions,
      updateCurrentPermissionsPageState,
      updatePermissionsPageSizeState,
      updateSearchActivePermissionsStates,
      getFoundPendingPermissions,
      updateCurrentPendingPermissionsPageState,
      updatePendingPermissionsPageSizeState,
      updateSearchActivePendingPermissionsStates
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Permissions);
