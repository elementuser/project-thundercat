import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { faMinusCircle, faCheck } from "@fortawesome/free-solid-svg-icons";
import LOCALIZE from "../../../text_resources";
import PopupBox, { BUTTON_TYPE } from "../../commons/PopupBox";
import {
  grantPermission,
  denyPermission,
  updateCurrentPendingPermissionsPageState,
  updatePendingPermissionsPageSizeState,
  updateSearchActivePendingPermissionsStates
} from "../../../modules/PermissionsRedux";
import "../../../css/etta.css";
import { getLineSpacingCSS } from "../../../modules/AccommodationsRedux";
import GenericTable, { COMMON_STYLE } from "../../commons/GenericTable";
import SearchBarWithDisplayOptions from "../../commons/SearchBarWithDisplayOptions";
import Pagination from "../../commons/Pagination";
import { Row, Col } from "react-bootstrap";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 4,
    xl: 4
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 8,
    xl: 8
  }
};

const styles = {
  formContainer: {
    padding: "24px 6px"
  },
  itemContainer: {
    width: "100%",
    margin: "12px 0"
  },
  labelContainer: {
    verticalAlign: "middle"
  },
  label: {
    paddingRight: 12,
    margin: 0
  },
  inputContainer: {
    verticalAlign: "middle"
  },
  input: {
    width: "100%",
    minHeight: 38,
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565e",
    borderRadius: 4
  },
  textAreaBox: {
    width: "100%",
    verticalAlign: "middle"
  },
  textAreaInput: {
    height: "100%",
    overflowY: "auto",
    resize: "none",
    display: "block",
    width: "calc(100% - 0.5px)"
  },
  description: {
    marginBottom: 12
  },
  userName: {
    fontWeight: "bold"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6
  }
};

class PermissionRequests extends Component {
  static propTypes = {
    pendingPermissions: PropTypes.array.isRequired,
    populatePendingPermissions: PropTypes.func.isRequired,
    populateActivePermissions: PropTypes.func.isRequired,
    populateFoundPendingPermissions: PropTypes.func.isRequired,
    rowsDefinition: PropTypes.object.isRequired,
    triggerShowViewPermissionRequestPopup: PropTypes.bool.isRequired,
    selectedPermissionRequestData: PropTypes.object.isRequired,
    numberOfPages: PropTypes.number.isRequired,
    resultsFound: PropTypes.number.isRequired,
    currentlyLoading: PropTypes.bool.isRequired,
    // provided by redux
    grantPermission: PropTypes.func,
    denyPermission: PropTypes.func,
    updateCurrentPendingPermissionsPageState: PropTypes.func,
    updatePendingPermissionsPageSizeState: PropTypes.func,
    updateSearchActivePendingPermissionsStates: PropTypes.func
  };

  state = {
    showViewPermissionRequestPopup: false,
    username: "",
    firstName: "",
    lastName: "",
    gocEmail: "",
    priOrMilitaryNbr: "",
    supervisor: "",
    supervisorEmail: "",
    rationale: "",
    permissionRequested: "",
    permissionRequestedId: "",
    permissionRequestId: "",
    deniedReason: "",
    isValidDeniedReasonOnDeny: true,
    isValidDeniedReasonOnApprove: true,
    usernameNoLongerExistsError: false,
    rowsDefinition: {},
    triggerShowViewPermissionRequestPopup: false,
    searchBarContent: "",
    displayOptionSelectedValue: {
      label: `${this.props.pendingPermissionsPaginationPageSize}`,
      value: this.props.pendingPermissionsPaginationPageSize
    }
  };

  componentDidUpdate = prevProps => {
    // if rowsDefinition gets updated
    if (prevProps.rowsDefinition !== this.props.rowsDefinition) {
      this.setState({
        rowsDefinition: this.props.rowsDefinition
      });
    }
    // if triggerShowViewPermissionRequestPopup gets updated
    if (
      prevProps.triggerShowViewPermissionRequestPopup !==
      this.props.triggerShowViewPermissionRequestPopup
    ) {
      // open view/edit popup
      this.setState({
        showViewPermissionRequestPopup: true,
        username: this.props.selectedPermissionRequestData.username,
        firstName: this.props.selectedPermissionRequestData.firstName,
        lastName: this.props.selectedPermissionRequestData.lastName,
        gocEmail: this.props.selectedPermissionRequestData.gocEmail,
        priOrMilitaryNbr: this.props.selectedPermissionRequestData.priOrMilitaryNbr,
        supervisor: this.props.selectedPermissionRequestData.supervisor,
        supervisorEmail: this.props.selectedPermissionRequestData.supervisorEmail,
        rationale: this.props.selectedPermissionRequestData.rationale,
        permissionRequested: this.props.selectedPermissionRequestData.permissionRequested,
        permissionRequestedId: this.props.selectedPermissionRequestData.permissionRequestedId,
        permissionRequestId: this.props.selectedPermissionRequestData.permissionRequestId
      });
    }
  };

  closePermissionRequestPopop = () => {
    this.setState({ showViewPermissionRequestPopup: false });
  };

  // resetting state whenever the popup is being closed
  handleClosePermissionRequestPopup = () => {
    this.setState({
      username: "",
      firstName: "",
      lastName: "",
      gocEmail: "",
      priOrMilitaryNbr: "",
      supervisor: "",
      supervisorEmail: "",
      rationale: "",
      permissionRequested: "",
      permissionRequestedId: "",
      permissionRequestId: "",
      deniedReason: "",
      isValidDeniedReasonOnDeny: true,
      isValidDeniedReasonOnApprove: true,
      usernameNoLongerExistsError: false
    });
  };

  // handle approve permission request
  handleApprovePermissionRequest = () => {
    if (this.validateFormOnApprove()) {
      // grant specified permission to specified user (note that this API is also deleting the related permission request)
      this.props
        .grantPermission(this.state.username, this.state.permissionRequestedId)
        .then(response => {
          // if permission has been granted successfully
          if (response.status === 200) {
            // re-populating pending permissions and active permissions tables
            this.props.populateActivePermissions();

            if (this.props.pendingPermissionsActiveSearch) {
              this.props.populateFoundPendingPermissions();
            } else {
              this.props.populatePendingPermissions();
            }
            // closing popup
            this.setState({ showViewPermissionRequestPopup: false });
            // if there is an error (shouldn't happen)
          } else if (response.error !== "") {
            // username has been deleted, but the request still open
            if (response.error === "the specified username does not exist") {
              this.setState({ usernameNoLongerExistsError: true });
            }
            // should never happen
          } else {
            throw new Error("Something went wrong during grant permission process");
          }
        });
    }
  };

  // handle deny permission request
  handleDenyPermissionRequest = () => {
    if (this.validateFormOnDeny()) {
      // deleting specified permission_request_id from permission request table
      this.props.denyPermission(this.state.permissionRequestId).then(response => {
        // if permission has been deleted successfully
        if (response.status === 200) {
          // re-populating pending permissions table
          if (this.props.pendingPermissionsActiveSearch) {
            this.props.populateFoundPendingPermissions();
          } else {
            this.props.populatePendingPermissions();
          }
          // closing popup
          this.setState({ showViewPermissionRequestPopup: false });
          // should never happen
        } else {
          throw new Error("Something went wrong during deny permission process");
        }
      });
    }
  };

  // update denied reason content
  updateDeniedReasonContent = event => {
    const deniedReasonContent = event.target.value;
    this.setState({
      deniedReason: deniedReasonContent
    });
  };

  // handles form validation and returns bool based on fields validity
  validateFormOnDeny = () => {
    let isValidForm = false;

    // denied reason validation
    // must contain at least one letter to be valid
    const regexExpression = /^(.*[A-Za-z].*)$/;
    const isValidDeniedReasonOnDeny = !!regexExpression.test(this.state.deniedReason);

    // if all needed fields are valid
    if (isValidDeniedReasonOnDeny) {
      isValidForm = true;
    } else {
      // focusing denied reason field
      document.getElementById("denied-reason").focus();
    }

    // setting states
    this.setState({
      isValidDeniedReasonOnDeny: isValidDeniedReasonOnDeny,
      isValidDeniedReasonOnApprove: true
    });

    return isValidForm;
  };

  validateFormOnApprove = () => {
    let isValidForm = false;

    const isValidDeniedReasonOnApprove = this.state.deniedReason === "";

    // if denied reasons is empty
    if (isValidDeniedReasonOnApprove) {
      isValidForm = true;
    } else {
      // focusing denied reason field
      document.getElementById("denied-reason").focus();
    }

    this.setState({
      isValidDeniedReasonOnApprove: isValidDeniedReasonOnApprove,
      isValidDeniedReasonOnDeny: true
    });

    return isValidForm;
  };

  render() {
    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    const columnsDefinition = [
      {
        label: LOCALIZE.systemAdministrator.permissions.tabs.permissionRequests.table.columnOne,
        style: { width: "30%" }
      },
      {
        label: LOCALIZE.systemAdministrator.permissions.tabs.permissionRequests.table.columnTwo,
        style: { width: "30%" }
      },
      {
        label: LOCALIZE.systemAdministrator.permissions.tabs.permissionRequests.table.columnThree,
        style: { ...{ width: "20%" }, ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label: LOCALIZE.systemAdministrator.permissions.tabs.permissionRequests.table.columnFour,
        style: { ...{ width: "20%" }, ...COMMON_STYLE.CENTERED_TEXT }
      }
    ];
    return (
      <div>
        <SearchBarWithDisplayOptions
          idPrefix={"permission-requests"}
          currentlyLoading={this.props.currentlyLoading}
          updateSearchStates={this.props.updateSearchActivePendingPermissionsStates}
          updatePageState={this.props.updateCurrentPendingPermissionsPageState}
          paginationPageSize={Number(this.props.pendingPermissionsPaginationPageSize)}
          updatePaginationPageSize={this.props.updatePendingPermissionsPageSizeState}
          data={this.props.pendingPermissions}
          resultsFound={this.props.resultsFound}
        />
        <div>
          <GenericTable
            classnamePrefix="permission-requests"
            columnsDefinition={columnsDefinition}
            rowsDefinition={this.state.rowsDefinition}
            emptyTableMessage={
              LOCALIZE.systemAdministrator.permissions.tabs.permissionRequests.table.noPermission
            }
            currentlyLoading={this.props.currentlyLoading}
          />
        </div>
        <Pagination
          paginationContainerId={"permission-requests-pagination-pages-link"}
          pageCount={this.props.numberOfPages}
          paginationPage={this.props.pendingPermissionsPaginationPage}
          updatePaginationPageState={this.props.updateCurrentPendingPermissionsPageState}
          firstTableRowId={"permission-requests-table-row-0"}
        />
        <PopupBox
          show={this.state.showViewPermissionRequestPopup}
          handleClose={() => {}}
          closeButtonAction={this.closePermissionRequestPopop}
          shouldCloseOnEsc={true}
          onPopupClose={this.handleClosePermissionRequestPopup}
          displayCloseButton={true}
          isBackdropStatic={true}
          title={LOCALIZE.systemAdministrator.permissions.tabs.permissionRequests.popup.title}
          description={
            <div style={styles.formContainer}>
              <p style={styles.description}>
                {LOCALIZE.formatString(
                  LOCALIZE.systemAdministrator.permissions.tabs.permissionRequests.popup
                    .description,
                  <span style={styles.userName}>
                    {this.state.firstName} {this.state.lastName}
                  </span>
                )}
              </p>
              <Row
                className="align-items-center justify-content-start"
                style={styles.itemContainer}
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={styles.labelContainer}
                >
                  <label style={styles.label}>
                    <label>{LOCALIZE.profile.permissions.addPermissionPopup.gocEmail}</label>
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={styles.inputContainer}
                >
                  <input
                    id="goc-email"
                    disabled={true}
                    style={{ ...styles.input, ...accommodationsStyle }}
                    type="text"
                    value={this.state.gocEmail}
                    onChange={() => {}}
                  ></input>
                </Col>
              </Row>
              <Row
                className="align-items-center justify-content-start"
                style={styles.itemContainer}
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={styles.labelContainer}
                >
                  <label style={styles.label}>
                    <label>{LOCALIZE.profile.permissions.addPermissionPopup.pri}</label>
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={styles.inputContainer}
                >
                  <input
                    id="pri-or-military-number"
                    disabled={true}
                    style={{ ...styles.input, ...accommodationsStyle }}
                    type="text"
                    value={this.state.priOrMilitaryNbr}
                    onChange={() => {}}
                  ></input>
                </Col>
              </Row>
              <Row
                className="align-items-center justify-content-start"
                style={styles.itemContainer}
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={styles.labelContainer}
                >
                  <label style={styles.label}>
                    <label>{LOCALIZE.profile.permissions.addPermissionPopup.supervisor}</label>
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={styles.inputContainer}
                >
                  <input
                    id="supervisor"
                    disabled={true}
                    style={{ ...styles.input, ...accommodationsStyle }}
                    type="text"
                    value={this.state.supervisor}
                    onChange={() => {}}
                  ></input>
                </Col>
              </Row>
              <Row
                className="align-items-center justify-content-start"
                style={styles.itemContainer}
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={styles.labelContainer}
                >
                  <label style={styles.label}>
                    <label>{LOCALIZE.profile.permissions.addPermissionPopup.supervisorEmail}</label>
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={styles.inputContainer}
                >
                  <input
                    id="supervisor-email"
                    disabled={true}
                    style={{ ...styles.input, ...accommodationsStyle }}
                    type="text"
                    value={this.state.supervisorEmail}
                    onChange={() => {}}
                  ></input>
                </Col>
              </Row>
              <Row
                className="align-items-center justify-content-start"
                style={styles.itemContainer}
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={styles.labelContainer}
                >
                  <label style={styles.label}>
                    <label>{LOCALIZE.profile.permissions.addPermissionPopup.rationale}</label>
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={styles.textAreaBox}
                >
                  <textarea
                    id="rationale"
                    disabled={true}
                    style={{ ...styles.input, ...styles.textAreaInput, ...accommodationsStyle }}
                    type="text"
                    value={this.state.rationale}
                    onChange={() => {}}
                  ></textarea>
                </Col>
              </Row>
              <Row
                className="align-items-center justify-content-start"
                style={styles.itemContainer}
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={styles.labelContainer}
                >
                  <label style={styles.label}>
                    <label>
                      {
                        LOCALIZE.systemAdministrator.permissions.tabs.permissionRequests.popup
                          .permissionRequested
                      }
                    </label>
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={styles.inputContainer}
                >
                  <input
                    id="permission-requested"
                    disabled={true}
                    style={{ ...styles.input, ...accommodationsStyle }}
                    type="text"
                    value={this.state.permissionRequested}
                    onChange={() => {}}
                  ></input>
                </Col>
              </Row>
              <Row
                className="align-items-center justify-content-start"
                style={styles.itemContainer}
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={styles.labelContainer}
                >
                  <label style={styles.label}>
                    <label id="denied-reason-label" htmlFor="denied-reason">
                      {
                        LOCALIZE.systemAdministrator.permissions.tabs.permissionRequests.popup
                          .deniedReason
                      }
                    </label>
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={styles.textAreaBox}
                >
                  <textarea
                    className={
                      this.state.isValidDeniedReasonOnApprove &&
                      this.state.isValidDeniedReasonOnDeny
                        ? "valid-field"
                        : "invalid-field"
                    }
                    id="denied-reason"
                    style={{ ...styles.input, ...styles.textAreaInput, ...accommodationsStyle }}
                    type="text"
                    aria-labelledby="denied-reason-label denied-reason-missing-content-error denied-reason-empty-content-error"
                    value={this.state.deniedReason}
                    onChange={this.updateDeniedReasonContent}
                  ></textarea>
                </Col>
              </Row>
              {!this.state.isValidDeniedReasonOnDeny && (
                <Row
                  className="align-items-center justify-content-end"
                  style={styles.itemContainer}
                >
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.inputContainer}
                  >
                    <label
                      id="denied-reason-missing-content-error"
                      htmlFor="denied-reason"
                      style={styles.errorMessage}
                      className="notranslate"
                    >
                      {
                        LOCALIZE.systemAdministrator.permissions.tabs.permissionRequests.popup
                          .grantPermissionErrors.missingDeniedReasonError
                      }
                    </label>
                  </Col>
                </Row>
              )}
              {!this.state.isValidDeniedReasonOnApprove && (
                <Row
                  className="align-items-center justify-content-end"
                  style={styles.itemContainer}
                >
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.inputContainer}
                  >
                    <label
                      id="denied-reason-empty-content-error"
                      htmlFor="denied-reason"
                      style={styles.errorMessage}
                      className="notranslate"
                    >
                      {
                        LOCALIZE.systemAdministrator.permissions.tabs.permissionRequests.popup
                          .grantPermissionErrors.notEmptyDeniedReasonError
                      }
                    </label>
                  </Col>
                </Row>
              )}
              {this.state.usernameNoLongerExistsError && (
                <Row className="align-items-center justify-content-end">
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.inputContainer}
                  >
                    <label style={styles.errorMessage} className="notranslate">
                      {
                        LOCALIZE.systemAdministrator.permissions.tabs.permissionRequests.popup
                          .grantPermissionErrors.usernameDoesNotExistError
                      }
                    </label>
                  </Col>
                </Row>
              )}
            </div>
          }
          leftButtonType={BUTTON_TYPE.danger}
          leftButtonIcon={faMinusCircle}
          leftButtonTitle={
            LOCALIZE.systemAdministrator.permissions.tabs.permissionRequests.popup.denyButton
          }
          leftButtonLabel={
            LOCALIZE.systemAdministrator.permissions.tabs.permissionRequests.popup.denyButton
          }
          leftButtonAction={this.handleDenyPermissionRequest}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonIcon={faCheck}
          rightButtonTitle={
            LOCALIZE.systemAdministrator.permissions.tabs.permissionRequests.popup.grantButton
          }
          rightButtonLabel={
            LOCALIZE.systemAdministrator.permissions.tabs.permissionRequests.popup.grantButton
          }
          rightButtonCustomStyle={{ backgroundColor: "#278400" }}
          rightButtonAction={this.handleApprovePermissionRequest}
        />
      </div>
    );
  }
}

export { PermissionRequests as unconnectedPermissionRequests };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    pendingPermissionsPaginationPage: state.userPermissions.pendingPermissionsPaginationPage,
    pendingPermissionsPaginationPageSize:
      state.userPermissions.pendingPermissionsPaginationPageSize,
    pendingPermissionsActiveSearch: state.userPermissions.pendingPermissionsActiveSearch
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      grantPermission,
      denyPermission,
      updateCurrentPendingPermissionsPageState,
      updatePendingPermissionsPageSizeState,
      updateSearchActivePendingPermissionsStates
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(PermissionRequests);
