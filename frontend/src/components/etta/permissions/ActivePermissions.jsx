import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import LOCALIZE from "../../../text_resources";
import { faTrashAlt, faSave, faTimes } from "@fortawesome/free-solid-svg-icons";
import PopupBox, { BUTTON_TYPE } from "../../commons/PopupBox";
import validateName, { validateEmail } from "../../../helpers/regexValidator";
import "../../../css/etta.css";
import {
  updateActivePermission,
  deleteActivePermission,
  updateCurrentPermissionsPageState,
  updatePermissionsPageSizeState,
  updateSearchActivePermissionsStates
} from "../../../modules/PermissionsRedux";
import SystemMessage, { MESSAGE_TYPE } from "../../commons/SystemMessage";
import getDisplayOptions from "../../../helpers/getDisplayOptions";
import GenericTable, { COMMON_STYLE } from "../../commons/GenericTable";
import { getLineSpacingCSS } from "../../../modules/AccommodationsRedux";
import SearchBarWithDisplayOptions from "../../commons/SearchBarWithDisplayOptions";
import Pagination from "../../commons/Pagination";
import { Row, Col } from "react-bootstrap";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 5,
    xl: 5
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 7,
    xl: 7
  }
};

export const styles = {
  popupContainer: {
    padding: "10px 15px"
  },
  userName: {
    fontWeight: "bold"
  },
  description: {
    marginBottom: 12
  },
  itemContainer: {
    width: "100%",
    margin: "12px 0"
  },
  labelContainer: {
    verticalAlign: "middle"
  },
  label: {
    paddingRight: 12,
    margin: 0
  },
  reasonLabel: {
    paddingTop: 16
  },
  inputContainer: {
    verticalAlign: "middle"
  },
  input: {
    width: "100%",
    minHeight: 38,
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565e",
    borderRadius: 4
  },
  reasonInput: {
    height: "100%",
    resize: "none",
    marginTop: 6,
    width: "100%"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    margin: "4px 0 0 4px"
  },
  ReasonErrorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginLeft: 4
  },
  boldText: {
    fontWeight: "bold"
  },
  loading: {
    width: "100%",
    height: 60,
    textAlign: "center",
    fontSize: "32px",
    padding: 6
  },
  displayNone: {
    display: "none"
  }
};

class ActivePermissions extends Component {
  static propTypes = {
    activePermissions: PropTypes.array.isRequired,
    selectedActivePermission: PropTypes.object.isRequired,
    rowsDefinition: PropTypes.object.isRequired,
    populateActivePermissions: PropTypes.func.isRequired,
    populateFoundActivePermissions: PropTypes.func.isRequired,
    numberOfPages: PropTypes.number.isRequired,
    resultsFound: PropTypes.number.isRequired,
    currentlyLoading: PropTypes.bool,
    // provided by redux
    updateActivePermission: PropTypes.func,
    deleteActivePermission: PropTypes.func,
    updateCurrentPermissionsPageState: PropTypes.func,
    updatePermissionsPageSizeState: PropTypes.func,
    updateSearchActivePermissionsStates: PropTypes.func
  };

  state = {
    showViewEditDetailsPopup: false,
    showDeleteConfirmationPopup: false,
    showUpdatePermissionDataConfirmationPopup: false,
    isValidGocEmail: true,
    isValidSupervisor: true,
    isValidSupervisorEmail: true,
    reasonForModifications: "",
    isValidReasonForModifications: true,
    searchBarContent: "",
    displayResultsFound: false,
    selectedActivePermission: {},
    rowsDefinition: {},
    displayOptionsArray: [],
    displayOptionSelectedValue: {
      label: `${this.props.permissionsPaginationPageSize}`,
      value: this.props.permissionsPaginationPageSize
    }
  };

  componentDidMount = () => {
    this.populateDisplayOptions();
  };

  componentDidUpdate = prevProps => {
    // if rowsDefinition gets updated
    if (prevProps.rowsDefinition !== this.props.rowsDefinition) {
      this.setState({
        rowsDefinition: this.props.rowsDefinition
      });
    }
    // if selectedActivePermission gets updated
    if (prevProps.selectedActivePermission !== this.props.selectedActivePermission) {
      // open view/edit popup
      this.setState({
        showViewEditDetailsPopup: true,
        selectedActivePermission: this.props.selectedActivePermission
      });
    }
  };

  // update gocEmail content
  updateGocEmailContent = event => {
    const gocEmail = event.target.value;
    this.setState(prevState => {
      const selectedActivePermission = { ...prevState.selectedActivePermission };
      selectedActivePermission.gocEmail = gocEmail;
      return { selectedActivePermission };
    });
  };

  // update supervisor content
  updateSupervisorContent = event => {
    const supervisor = event.target.value;
    this.setState(prevState => {
      const selectedActivePermission = { ...prevState.selectedActivePermission };
      selectedActivePermission.supervisor = supervisor;
      return { selectedActivePermission };
    });
  };

  // update supervisorEmail content
  updateSupervisorEmailContent = event => {
    const supervisorEmail = event.target.value;
    this.setState(prevState => {
      const selectedActivePermission = { ...prevState.selectedActivePermission };
      selectedActivePermission.supervisorEmail = supervisorEmail;
      return { selectedActivePermission };
    });
  };

  // update reasonForModifications content
  updateReasonForModificationsContent = event => {
    const reasonForModifications = event.target.value;
    this.setState({
      reasonForModifications: reasonForModifications
    });
  };

  // update searchBarContent content
  updateSearchBarContent = event => {
    const searchBarContent = event.target.value;
    this.setState({
      searchBarContent: searchBarContent
    });
  };

  closeViewEditDetailsPopup = () => {
    this.setState({ showViewEditDetailsPopup: false });
  };

  // resetting states whenever the popup is being closed
  handleCloseViewEditDetailsPopup = () => {
    this.setState({
      isValidGocEmail: true,
      isValidSupervisor: true,
      isValidSupervisorEmail: true,
      isValidReasonForModifications: true,
      reasonForModifications: ""
    });
  };

  // handling delete permission action
  handleDeletePermission = () => {
    const body = {
      user_permission_id: this.state.selectedActivePermission.userPermissionId,
      reason_for_deletion: this.state.reasonForModifications
    };
    // deleting specified user permission
    this.props.deleteActivePermission(body).then(response => {
      // permission has been deleted
      if (response.status === 200) {
        // re-populating active permissions table
        if (this.props.permissionsActiveSearch) {
          this.props.populateFoundActivePermissions();
        } else {
          this.props.populateActivePermissions();
        }
        this.setState({ showViewEditDetailsPopup: false, showDeleteConfirmationPopup: false });
        // should never happen
      } else {
        throw new Error("Something went wrong during delete permission process");
      }
    });
  };

  openDeleteConfirmationPopup = () => {
    // reason for modifications validation
    // must contain at least one letter to be valid
    const regexExpression = /^(.*[A-Za-z].*)$/;
    const isValidReasonForModifications = !!regexExpression.test(this.state.reasonForModifications);
    // valid reason for modifications
    if (isValidReasonForModifications) {
      this.setState({ showDeleteConfirmationPopup: true });
      // invalid reason for modifications
    } else {
      this.setState({ isValidReasonForModifications: false }, () => {
        document.getElementById("reason-for-modifications").focus();
      });
    }
  };

  closeDeleteConfirmationPopup = () => {
    this.setState({
      showDeleteConfirmationPopup: false,
      isValidReasonForModifications: true
    });
  };

  // handling save data action
  handleSaveData = () => {
    if (this.validateForm()) {
      const data = {
        userPermissionId: this.state.selectedActivePermission.userPermissionId,
        gocEmail: this.state.selectedActivePermission.gocEmail,
        supervisor: this.state.selectedActivePermission.supervisor,
        supervisorEmail: this.state.selectedActivePermission.supervisorEmail,
        reason_for_modification: this.state.reasonForModifications
      };
      this.props.updateActivePermission(data).then(response => {
        // permission data has been saved
        if (response.status === 200) {
          // re-populating active permissions table
          if (this.props.permissionsActiveSearch) {
            this.props.populateFoundActivePermissions();
          } else {
            this.props.populateActivePermissions();
          }
          this.setState({
            showViewEditDetailsPopup: false,
            showUpdatePermissionDataConfirmationPopup: true
          });
          // should never happen
        } else {
          throw new Error("Something went wrong during update permission data process");
        }
      });
    }
  };

  closeUpdateConfirmationPopup = () => {
    this.setState({ showUpdatePermissionDataConfirmationPopup: false });
  };

  // validating form (on save data action)
  validateForm = () => {
    const { gocEmail, supervisor, supervisorEmail } = this.state.selectedActivePermission;
    const { reasonForModifications } = this.state;

    let isFormValid = false;

    // goc email validation
    const isValidGocEmail = validateEmail(gocEmail);

    // supervisor validation
    const isValidSupervisor = validateName(supervisor);

    // supervisor email validation
    const isValidSupervisorEmail = validateEmail(supervisorEmail);

    // reason for modifications validation
    // must contain at least one letter to be valid
    const regexExpression = /^(.*[A-Za-z].*)$/;
    const isValidReasonForModifications = !!regexExpression.test(reasonForModifications);

    // saving all validation results in states
    this.setState(
      {
        isValidGocEmail: isValidGocEmail,
        isValidSupervisor: isValidSupervisor,
        isValidSupervisorEmail: isValidSupervisorEmail,
        isValidReasonForModifications: isValidReasonForModifications
      },
      () => {
        this.focusOnHighestErrorField();
      }
    );

    // checking if all validations are met
    if (
      isValidGocEmail &&
      isValidSupervisor &&
      isValidSupervisorEmail &&
      isValidReasonForModifications
    ) {
      isFormValid = true;
    }

    return isFormValid;
  };

  // analysing field by field and focusing on the highest error field
  focusOnHighestErrorField = () => {
    if (!this.state.isValidGocEmail) {
      document.getElementById("goc-email").focus();
    } else if (!this.state.isValidSupervisor) {
      document.getElementById("supervisor").focus();
    } else if (!this.state.isValidSupervisorEmail) {
      document.getElementById("supervisor-email").focus();
    } else if (!this.state.isValidReasonForModifications) {
      document.getElementById("reason-for-modifications").focus();
    }
  };

  // populate display options
  populateDisplayOptions = () => {
    this.setState({ displayOptionsArray: getDisplayOptions() });
  };

  render() {
    const columnsDefinition = [
      {
        label: LOCALIZE.systemAdministrator.permissions.tabs.activePermissions.table.user,
        style: { width: "40%" }
      },
      {
        label: LOCALIZE.systemAdministrator.permissions.tabs.activePermissions.table.permission,
        style: { width: "30%" }
      },
      {
        label: LOCALIZE.systemAdministrator.permissions.tabs.activePermissions.table.action,
        style: { ...{ width: "30%" }, ...COMMON_STYLE.CENTERED_TEXT }
      }
    ];

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }
    return (
      <div>
        <SearchBarWithDisplayOptions
          idPrefix={"active-permissions"}
          currentlyLoading={this.props.currentlyLoading}
          updateSearchStates={this.props.updateSearchActivePermissionsStates}
          updatePageState={this.props.updateCurrentPermissionsPageState}
          paginationPageSize={Number(this.props.permissionsPaginationPageSize)}
          updatePaginationPageSize={this.props.updatePermissionsPageSizeState}
          data={this.props.activePermissions}
          resultsFound={this.props.resultsFound}
        />
        <div>
          <GenericTable
            classnamePrefix="active-permissions"
            columnsDefinition={columnsDefinition}
            rowsDefinition={this.state.rowsDefinition}
            emptyTableMessage={LOCALIZE.SearchBarWithDisplayOptions.noResultsFound}
            currentlyLoading={this.props.currentlyLoading}
          />
          <Pagination
            paginationContainerId={"active-permissions-pagination-pages-link"}
            pageCount={this.props.numberOfPages}
            paginationPage={this.props.permissionsPaginationPage}
            updatePaginationPageState={this.props.updateCurrentPermissionsPageState}
            firstTableRowId={"active-permissions-table-row-0"}
          />
        </div>
        <PopupBox
          show={this.state.showViewEditDetailsPopup}
          handleClose={() => {}}
          closeButtonAction={this.closeViewEditDetailsPopup}
          shouldCloseOnEsc={true}
          onPopupClose={this.handleCloseViewEditDetailsPopup}
          displayCloseButton={true}
          isBackdropStatic={true}
          title={
            LOCALIZE.systemAdministrator.permissions.tabs.activePermissions.viewEditDetailsPopup
              .title
          }
          description={
            <div style={styles.popupContainer}>
              <p style={styles.description}>
                {LOCALIZE.formatString(
                  LOCALIZE.systemAdministrator.permissions.tabs.activePermissions
                    .viewEditDetailsPopup.description,
                  <span style={styles.userName}>
                    {this.state.selectedActivePermission.firstName}{" "}
                    {this.state.selectedActivePermission.lastName}
                  </span>
                )}
              </p>
              <Row
                className="align-items-center justify-content-start"
                style={styles.itemContainer}
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={styles.labelContainer}
                >
                  <label style={styles.label}>
                    <label id="goc-email-title" htmlFor="goc-email">
                      {LOCALIZE.profile.permissions.addPermissionPopup.gocEmail}
                    </label>
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={styles.inputContainer}
                >
                  <input
                    id="goc-email"
                    className={this.state.isValidGocEmail ? "valid-field" : "invalid-field"}
                    aria-labelledby="goc-email-title goc-email-error"
                    aria-required={true}
                    aria-invalid={!this.state.isValidGocEmail}
                    style={{ ...styles.input, ...accommodationsStyle }}
                    type="text"
                    value={this.state.selectedActivePermission.gocEmail}
                    onChange={this.updateGocEmailContent}
                  ></input>
                  {!this.state.isValidGocEmail && (
                    <label
                      id="goc-email-error"
                      htmlFor="goc-email"
                      style={styles.errorMessage}
                      className="notranslate"
                    >
                      {LOCALIZE.profile.permissions.addPermissionPopup.gocEmailError}
                    </label>
                  )}
                </Col>
              </Row>
              <Row
                className="align-items-center justify-content-start"
                style={styles.itemContainer}
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={styles.labelContainer}
                >
                  <label style={styles.label}>
                    <label>{LOCALIZE.profile.permissions.addPermissionPopup.pri}</label>
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={styles.inputContainer}
                >
                  <input
                    id="pri-or-military-number"
                    disabled={true}
                    style={{ ...styles.input, ...accommodationsStyle }}
                    type="text"
                    value={this.state.selectedActivePermission.priOrMilitaryNbr}
                    onChange={() => {}}
                  ></input>
                </Col>
              </Row>
              <Row
                className="align-items-center justify-content-start"
                style={styles.itemContainer}
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={styles.labelContainer}
                >
                  <label style={styles.label}>
                    <label id="supervisor-title" htmlFor="supervisor">
                      {LOCALIZE.profile.permissions.addPermissionPopup.supervisor}
                    </label>
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={styles.inputContainer}
                >
                  <input
                    id="supervisor"
                    className={this.state.isValidSupervisor ? "valid-field" : "invalid-field"}
                    aria-labelledby="supervisor-title supervisor-error"
                    aria-required={true}
                    aria-invalid={!this.state.isValidSupervisor}
                    style={{ ...styles.input, ...accommodationsStyle }}
                    type="text"
                    value={this.state.selectedActivePermission.supervisor}
                    onChange={this.updateSupervisorContent}
                  ></input>
                  {!this.state.isValidSupervisor && (
                    <label
                      id="supervisor-error"
                      htmlFor="supervisor"
                      style={styles.errorMessage}
                      className="notranslate"
                    >
                      {LOCALIZE.profile.permissions.addPermissionPopup.supervisorError}
                    </label>
                  )}
                </Col>
              </Row>
              <Row
                className="align-items-center justify-content-start"
                style={styles.itemContainer}
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={styles.labelContainer}
                >
                  <label style={styles.label}>
                    <label id="supervisor-email-title" htmlFor="supervisor-email">
                      {LOCALIZE.profile.permissions.addPermissionPopup.supervisorEmail}
                    </label>
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={styles.inputContainer}
                >
                  <input
                    id="supervisor-email"
                    className={this.state.isValidSupervisorEmail ? "valid-field" : "invalid-field"}
                    aria-labelledby="supervisor-email-title supervisor-email-error"
                    aria-required={true}
                    aria-invalid={!this.state.isValidSupervisorEmail}
                    style={{ ...styles.input, ...accommodationsStyle }}
                    type="text"
                    value={this.state.selectedActivePermission.supervisorEmail}
                    onChange={this.updateSupervisorEmailContent}
                  ></input>
                  {!this.state.isValidSupervisorEmail && (
                    <label
                      id="supervisor-email-error"
                      htmlFor="supervisor-email"
                      style={styles.errorMessage}
                      className="notranslate"
                    >
                      {LOCALIZE.profile.permissions.addPermissionPopup.supervisorEmailError}
                    </label>
                  )}
                </Col>
              </Row>
              <Row
                className="align-items-center justify-content-start"
                style={styles.itemContainer}
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={styles.labelContainer}
                >
                  <label style={styles.label}>
                    <label>
                      {
                        LOCALIZE.systemAdministrator.permissions.tabs.activePermissions
                          .viewEditDetailsPopup.permission
                      }
                    </label>
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={styles.inputContainer}
                >
                  <input
                    id="permission"
                    disabled={true}
                    style={{ ...styles.input, ...accommodationsStyle }}
                    type="text"
                    value={this.state.selectedActivePermission.permission}
                    onChange={() => {}}
                  ></input>
                </Col>
              </Row>
              <Row
                className="align-items-center justify-content-start"
                style={styles.itemContainer}
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                >
                  <label style={styles.reasonLabel}>
                    <label id="reason-for-modifications-title" htmlFor="reason-for-modifications">
                      {
                        LOCALIZE.systemAdministrator.permissions.tabs.activePermissions
                          .viewEditDetailsPopup.reasonForModification
                      }
                    </label>
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                >
                  <textarea
                    id="reason-for-modifications"
                    className={
                      this.state.isValidReasonForModifications ? "valid-field" : "invalid-field"
                    }
                    aria-labelledby="reason-for-modifications-title reason-for-modifications-error"
                    aria-required={true}
                    aria-invalid={!this.state.isValidReasonForModifications}
                    style={{ ...styles.input, ...styles.reasonInput, ...accommodationsStyle }}
                    value={this.state.reasonForModifications}
                    onChange={this.updateReasonForModificationsContent}
                    maxLength="300"
                  ></textarea>
                </Col>
              </Row>
              {!this.state.isValidReasonForModifications && (
                <Row
                  className="align-items-center justify-content-end"
                  style={styles.itemContainer}
                >
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    <label
                      id="reason-for-modifications-error"
                      htmlFor="reason-for-modifications"
                      style={styles.ReasonErrorMessage}
                    >
                      {
                        LOCALIZE.systemAdministrator.permissions.tabs.activePermissions
                          .viewEditDetailsPopup.reasonForModificationError
                      }
                    </label>
                  </Col>
                </Row>
              )}
            </div>
          }
          leftButtonType={BUTTON_TYPE.danger}
          leftButtonIcon={faTrashAlt}
          leftButtonTitle={
            LOCALIZE.systemAdministrator.permissions.tabs.activePermissions.viewEditDetailsPopup
              .deleteButton
          }
          leftButtonLabel={
            LOCALIZE.systemAdministrator.permissions.tabs.activePermissions.viewEditDetailsPopup
              .deleteButton
          }
          leftButtonAction={this.openDeleteConfirmationPopup}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonIcon={faSave}
          rightButtonTitle={
            LOCALIZE.systemAdministrator.permissions.tabs.activePermissions.viewEditDetailsPopup
              .saveButton
          }
          rightButtonLabel={
            LOCALIZE.systemAdministrator.permissions.tabs.activePermissions.viewEditDetailsPopup
              .saveButton
          }
          rightButtonCustomStyle={{ backgroundColor: "#278400" }}
          rightButtonAction={this.handleSaveData}
        />
        <PopupBox
          show={this.state.showDeleteConfirmationPopup}
          handleClose={this.closeDeleteConfirmationPopup}
          isBackdropStatic={false}
          shouldCloseOnEsc={true}
          title={
            LOCALIZE.systemAdministrator.permissions.tabs.activePermissions
              .deletePermissionConfirmationPopup.title
          }
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.error}
                title={LOCALIZE.commons.warning}
                message={
                  <div>
                    <p>
                      {LOCALIZE.formatString(
                        LOCALIZE.systemAdministrator.permissions.tabs.activePermissions
                          .deletePermissionConfirmationPopup.systemMessageDescription,
                        <span style={styles.boldText}>
                          {this.state.selectedActivePermission.permission}
                        </span>,
                        <span style={styles.boldText}>
                          {this.state.selectedActivePermission.firstName}{" "}
                          {this.state.selectedActivePermission.lastName}
                        </span>
                      )}
                    </p>
                  </div>
                }
              />
            </div>
          }
          leftButtonType={BUTTON_TYPE.primary}
          leftButtonIcon={faTimes}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonLabel={LOCALIZE.commons.cancel}
          leftButtonAction={this.closeDeleteConfirmationPopup}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonIcon={faTrashAlt}
          rightButtonTitle={
            LOCALIZE.systemAdministrator.permissions.tabs.activePermissions.viewEditDetailsPopup
              .deleteButton
          }
          rightButtonLabel={
            LOCALIZE.systemAdministrator.permissions.tabs.activePermissions.viewEditDetailsPopup
              .deleteButton
          }
          rightButtonAction={this.handleDeletePermission}
          size="lg"
        />
        <PopupBox
          show={this.state.showUpdatePermissionDataConfirmationPopup}
          handleClose={() => {}}
          isBackdropStatic={true}
          shouldCloseOnEsc={false}
          title={
            LOCALIZE.systemAdministrator.permissions.tabs.activePermissions
              .updatePermissionDataConfirmationPopup.title
          }
          description={
            <div>
              <p>
                {
                  <SystemMessage
                    messageType={MESSAGE_TYPE.success}
                    title={LOCALIZE.commons.success}
                    message={LOCALIZE.formatString(
                      LOCALIZE.systemAdministrator.permissions.tabs.activePermissions
                        .updatePermissionDataConfirmationPopup.description,
                      <span style={styles.boldText}>
                        {this.state.selectedActivePermission.firstName}{" "}
                        {this.state.selectedActivePermission.lastName}
                      </span>
                    )}
                  />
                }
              </p>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.ok}
          rightButtonAction={this.closeUpdateConfirmationPopup}
        />
      </div>
    );
  }
}

export { ActivePermissions as unconnectedActivePermissions };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    permissionsPaginationPageSize: state.userPermissions.permissionsPaginationPageSize,
    permissionsPaginationPage: state.userPermissions.permissionsPaginationPage,
    permissionsActiveSearch: state.userPermissions.permissionsActiveSearch,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateActivePermission,
      deleteActivePermission,
      updateCurrentPermissionsPageState,
      updatePermissionsPageSizeState,
      updateSearchActivePermissionsStates
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ActivePermissions);
