import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes, faTimesCircle } from "@fortawesome/free-solid-svg-icons";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import GenericTable, { COMMON_STYLE } from "../commons/GenericTable";
import getFormattedTestStatus from "../ta/TestStatus";
import { styles as ActivePermissionsStyles } from "./permissions/ActivePermissions";
import {
  getAllActiveTests,
  getFoundAssignedCandidates,
  updateCurrentAllActiveCandidatesPageState,
  updateAllActiveCandidatesPageSizeState,
  updateSearchAllActiveCandidateStates,
  invalidateCandidateAsEtta
} from "../../modules/ActiveTestsRedux";
import { getTimeInHoursMinutes } from "../../helpers/timeConversion";
import SearchBarWithDisplayOptions from "../commons/SearchBarWithDisplayOptions";
import Pagination from "../commons/Pagination";
import CustomButton, { THEME } from "../commons/CustomButton";
import SystemMessage, { MESSAGE_TYPE } from "../commons/SystemMessage";
import PopupBox, { BUTTON_TYPE, BUTTON_STATE } from "../commons/PopupBox";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";
import StyledTooltip, { EFFECT, TRIGGER_TYPE, TYPE } from "../authentication/StyledTooltip";

const styles = {
  bold: {
    fontWeight: "bold"
  },
  buttonIcon: {
    marginRight: 6
  },
  input: {
    width: "100%",
    minHeight: 38,
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565e",
    borderRadius: 4
  },
  textAreaInput: {
    height: 85,
    overflowY: "auto",
    resize: "none",
    display: "block",
    width: "calc(100% - 0.5px)"
  },
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#cc0000"
  },
  allUnset: {
    all: "unset"
  },
  deleteIcon: {
    transform: "scale(1.2)"
  }
};

class ActiveTests extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  static propTypes = {
    // props from Redux
    getAllActiveTests: PropTypes.func,
    getFoundAssignedCandidates: PropTypes.func,
    updateCurrentAllActiveCandidatesPageState: PropTypes.func,
    updateAllActiveCandidatesPageSizeState: PropTypes.func,
    updateSearchAllActiveCandidateStates: PropTypes.func
  };

  state = {
    displayResultsFound: false,
    resultsFound: 0,
    clearSearchTriggered: false,
    activeCandidates: [],
    rowsDefinition: {},
    currentlyLoading: false,
    numberOfPages: 1,
    nextPageNumber: 0,
    previousPageNumber: 0,
    showInvalidatePopup: false,
    selectedActiveTestToDisable: {},
    isValidReasonForInvalidatingTest: true,
    reasonForInvalidatingTest: ""
  };

  componentDidMount = () => {
    // initialize pagination page redux state
    this.props.updateCurrentAllActiveCandidatesPageState(1);
    // initialize active search redux state
    this.props.updateSearchAllActiveCandidateStates("", false);
    this.populateAllActiveCandidates();
  };

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentDidUpdate = prevProps => {
    // // if currentLanguage gets updated
    if (prevProps.currentLanguage !== this.props.currentLanguage) {
      this.populateActiveCandidatesBasedOnActiveSearch();
    }
    // if activeTestsPaginationPage gets updated
    if (
      prevProps.allActiveCandidatesPaginationPage !== this.props.allActiveCandidatesPaginationPage
    ) {
      this.populateActiveCandidatesBasedOnActiveSearch();
    }
    // // if activeTestsPaginationPageSize get updated
    if (
      prevProps.allActiveCandidatesPaginationPageSize !==
      this.props.allActiveCandidatesPaginationPageSize
    ) {
      this.populateActiveCandidatesBasedOnActiveSearch();
    }
    // // if search activeTestsKeyword gets updated
    if (prevProps.allActiveCandidatesKeyword !== this.props.allActiveCandidatesKeyword) {
      // if activeTestsKeyword redux state is empty
      if (this.props.allActiveCandidatesKeyword !== "") {
        this.populateFoundActiveCandidates();
      } else if (
        this.props.allActiveCandidatesKeyword === "" &&
        this.props.allActiveCandidatesActiveSearch
      ) {
        this.populateFoundActiveCandidates();
      }
    }
    // // if activeTestsActiveSearch gets updated
    if (prevProps.allActiveCandidatesActiveSearch !== this.props.allActiveCandidatesActiveSearch) {
      // there is no current active search (on clear search action)
      if (!this.props.allActiveCandidatesActiveSearch) {
        // triggering clear search action state
        this.setState({ clearSearchTriggered: true }, () => {
          this.populateAllActiveCandidates();
        });
        // there is a current active search (on search action)
      } else {
        this.populateFoundActiveCandidates();
      }
    }
  };

  // open pop up
  openInvalidatePopup = id => {
    this.setState({
      showInvalidatePopup: true,
      selectedActiveTestToDisable: this.state.activeCandidates[id]
    });
  };

  //  close pop up
  closeInvalidatePopup = () => {
    this.setState({
      showInvalidatePopup: false,
      selectedActiveTestToDisable: {},
      isValidReasonForInvalidatingTest: true,
      reasonForInvalidatingTest: ""
    });
  };

  updateReasonForInvalidatingTest = event => {
    const reasonForInvalidatingTest = event.target.value;
    if (Object.keys(reasonForInvalidatingTest).length !== 0) {
      this.setState({
        isValidReasonForInvalidatingTest: false,
        reasonForInvalidatingTest: reasonForInvalidatingTest
      });
    } else {
      this.setState({
        isValidReasonForInvalidatingTest: true,
        reasonForInvalidatingTest: reasonForInvalidatingTest
      });
    }
  };

  // to display all active candidates
  populateAllActiveCandidates = () => {
    this._isMounted = true;
    this.setState({ currentlyLoading: true }, () => {
      const allActiveCandidatesArray = [];

      this.props
        .getAllActiveTests(
          this.props.allActiveCandidatesPaginationPage,
          this.props.allActiveCandidatesPaginationPageSize
        )
        .then(response => {
          if (this._isMounted) {
            this.populateAllActiveCandidatesObject(allActiveCandidatesArray, response);
          }
        })
        .then(() => {
          if (this._isMounted) {
            if (this.state.clearSearchTriggered) {
              // go back to the first page to avoid display bugs
              this.props.updateCurrentAllActiveCandidatesPageState(1);
              this.setState({ clearSearchTriggered: false });
            }
          }
        })
        .then(() => {
          if (this._isMounted) {
            this.setState({ currentlyLoading: false });
          }
        });
    });
  };

  // to display found active candidates based on search
  populateFoundActiveCandidates = () => {
    this.setState({ currentlyLoading: true }, () => {
      const allActiveCandidatesArray = [];
      this.props
        .getFoundAssignedCandidates(
          this.props.currentLanguage,
          this.props.allActiveCandidatesKeyword,
          this.props.allActiveCandidatesPaginationPage,
          this.props.allActiveCandidatesPaginationPageSize
        )
        .then(response => {
          // there are no results found
          if (response[0] === "no results found") {
            this.setState({
              activeCandidates: [],
              rowsDefinition: {},
              numberOfPages: 1,
              nextPageNumber: 0,
              previousPageNumber: 0,
              resultsFound: 0
            });
            // there is at least one result found
          } else {
            this.populateAllActiveCandidatesObject(allActiveCandidatesArray, response);
            this.setState({ resultsFound: response.count });
          }
        })
        .then(() => {
          this.setState({ currentlyLoading: false, displayResultsFound: true }, () => {
            // if there is at least one result found
            if (allActiveCandidatesArray.length > 0) {
              // make sure that this element exsits to avoid any error
              if (document.getElementById("all-active-candidates-results-found")) {
                document.getElementById("all-active-candidates-results-found").focus();
              }
              // no results found
            } else {
              // focusing on no results found row
              document.getElementById("all-active-candidates-no-data").focus();
            }
          });
        });
    });
  };

  populateAllActiveCandidatesObject = (allActiveCandidatesArray, response) => {
    // initializing needed object and array for rowsDefinition props (needed for GenericTable component)
    let rowsDefinition = {};
    const data = [];

    // making sure that there are some results
    if (typeof response.results !== "undefined") {
      for (let i = 0; i < response.results.length; i++) {
        const currentResult = response.results[i];
        // pushing needed data in allActiveCandidatesArray
        allActiveCandidatesArray.push({
          id: currentResult.assigned_test_id,
          candidate_last_name: currentResult.candidate_last_name,
          candidate_first_name: currentResult.candidate_first_name,
          test_id: currentResult.test_id,
          test_code: currentResult.test_code,
          status: currentResult.test_status,
          ta: currentResult.ta_id,
          ta_first_name: currentResult.ta_first_name,
          ta_last_name: currentResult.ta_last_name,
          test_order_number: currentResult.test_order_number,
          reference_number: currentResult.reference_number,
          username: currentResult.candidate_username
        });

        // populating data object with provided data
        data.push({
          column_1: `${currentResult.candidate_last_name}, ${currentResult.candidate_first_name} (${currentResult.candidate_email})`,
          column_2: `${currentResult.ta_last_name}, ${currentResult.ta_first_name}`,
          column_3: currentResult.test_code,
          column_4:
            currentResult.test_order_number !== null
              ? currentResult.test_order_number
              : `${currentResult.reference_number} (${LOCALIZE.reports.orderlessRequest})`,
          column_5: getFormattedTestStatus(Number(currentResult.test_status)),
          column_6: `${getTimeInHoursMinutes(Number(currentResult.time_limit)).formattedHours} : ${
            getTimeInHoursMinutes(Number(currentResult.time_limit)).formattedMinutes
          }`,
          column_7: (
            <StyledTooltip
              id={`test-access-code-delete-row-${i}`}
              place="top"
              type={TYPE.light}
              effect={EFFECT.solid}
              triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
              tooltipElement={
                <div style={styles.allUnset}>
                  <CustomButton
                    className="inline-flex align-items-center justify-content-center flex-nowrap"
                    dataTip=""
                    dataFor={`test-access-code-delete-row-${i}`}
                    label={
                      <>
                        <FontAwesomeIcon icon={faTimesCircle} style={styles.deleteIcon} />
                      </>
                    }
                    action={() => {
                      this.openInvalidatePopup(i);
                    }}
                    customStyle={styles.actionButton}
                    buttonTheme={THEME.DANGER}
                    ariaLabel={LOCALIZE.formatString(
                      LOCALIZE.systemAdministrator.activeTests.table.invalidateButtonAccessibility,
                      `${currentResult.candidate_last_name}, ${currentResult.candidate_first_name}`
                    )}
                  />
                </div>
              }
              tooltipContent={
                <div>
                  <p>{LOCALIZE.systemAdministrator.activeTests.table.actionButton}</p>
                </div>
              }
            />
          )
        });
      }
    }

    // updating rowsDefinition object with provided data and needed style
    rowsDefinition = {
      column_1_style: COMMON_STYLE.LEFT_TEXT,
      column_2_style: COMMON_STYLE.CENTERED_TEXT,
      column_3_style: COMMON_STYLE.CENTERED_TEXT,
      column_4_style: COMMON_STYLE.CENTERED_TEXT,
      column_5_style: COMMON_STYLE.CENTERED_TEXT,
      column_6_style: COMMON_STYLE.CENTERED_TEXT,
      column_7_style: COMMON_STYLE.CENTERED_TEXT,
      data: data
    };

    // saving results in state
    this.setState({
      activeCandidates: allActiveCandidatesArray,
      rowsDefinition: rowsDefinition,
      numberOfPages: Math.ceil(response.count / this.props.allActiveCandidatesPaginationPageSize),
      nextPageNumber: response.next_page_number,
      previousPageNumber: response.previous_page_number
    });
  };

  populateActiveCandidatesBasedOnActiveSearch = () => {
    // current search
    if (this.props.allActiveCandidatesActiveSearch) {
      this.populateFoundActiveCandidates();
      // no search
    } else {
      this.populateAllActiveCandidates();
    }
  };

  handleInvalidate = () => {
    const data = {
      id: this.state.selectedActiveTestToDisable.id,
      username_id: this.state.selectedActiveTestToDisable.username,
      test_id: this.state.selectedActiveTestToDisable.test_id,
      invalidate_test_reason: this.state.reasonForInvalidatingTest
    };

    this.props
      .invalidateCandidateAsEtta(data)
      .then(response => {
        // if request succeeded
        if (response.status === 200) {
          // updating active candidates table
          this.updateAssignedCandidatesTable();
          // go back to the first page to avoid display bugs
          this.props.updateCurrentAllActiveCandidatesPageState(1);
          // should never happen
        } else {
          throw new Error("Something went wrong during invalidating active tests");
        }
      })
      .then(() => {
        // close pop up
        this.closeInvalidatePopup();
      });
  };

  updateAssignedCandidatesTable = () => {
    this.populateAllActiveCandidates();
  };

  // getting total test time based on all timed test sections
  getTotalTestTime = assignedTestSections => {
    // initializing totalTestTime
    let totalTestTime = 0;
    // looping in all assigned test sections
    for (let i = 0; i < assignedTestSections.length; i++) {
      // test_section_time is not null
      if (assignedTestSections[i].test_section_time !== null) {
        // incrementing totalTestTime
        totalTestTime += assignedTestSections[i].test_section_time;
      }
    }
    // returning formatted test time
    return `${getTimeInHoursMinutes(Number(totalTestTime)).formattedHours} : ${
      getTimeInHoursMinutes(Number(totalTestTime)).formattedMinutes
    }`;
  };

  render() {
    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    const columnsDefinition = [
      {
        label: LOCALIZE.systemAdministrator.activeTests.table.columnOne
      },
      {
        label: LOCALIZE.systemAdministrator.activeTests.table.columnTwo,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.systemAdministrator.activeTests.table.columnThree,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.systemAdministrator.activeTests.table.columnFour,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.systemAdministrator.activeTests.table.columnFive,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.systemAdministrator.activeTests.table.columnSix,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.systemAdministrator.activeTests.table.columnSeven,
        style: COMMON_STYLE.CENTERED_TEXT
      }
    ];

    return (
      <div>
        <h2>{LOCALIZE.systemAdministrator.sideNavItems.activeTests}</h2>
        <p>{LOCALIZE.systemAdministrator.activeTests.description}</p>
        <SearchBarWithDisplayOptions
          idPrefix={"all-active-candidates"}
          currentlyLoading={this.state.currentlyLoading}
          updateSearchStates={this.props.updateSearchAllActiveCandidateStates}
          updatePageState={this.props.updateCurrentAllActiveCandidatesPageState}
          paginationPageSize={Number(this.props.allActiveCandidatesPaginationPageSize)}
          updatePaginationPageSize={this.props.updateAllActiveCandidatesPageSizeState}
          data={this.state.activeCandidates}
          resultsFound={this.state.resultsFound}
        />
        <div>
          <GenericTable
            classnamePrefix="all-active-candidates"
            columnsDefinition={columnsDefinition}
            rowsDefinition={this.state.rowsDefinition}
            emptyTableMessage={LOCALIZE.systemAdministrator.activeTests.table.noActiveCandidates}
            currentlyLoading={this.state.currentlyLoading}
          />
        </div>
        <Pagination
          paginationContainerId={"all-active-candidates-pagination-pages-link"}
          pageCount={this.state.numberOfPages}
          paginationPage={this.props.allActiveCandidatesPaginationPage}
          updatePaginationPageState={this.props.updateCurrentAllActiveCandidatesPageState}
          firstTableRowId={"all-active-candidates-table-row-0"}
        />
        <PopupBox
          show={this.state.showInvalidatePopup}
          handleClose={() => {}}
          isBackdropStatic={false}
          shouldCloseOnEsc={true}
          title={LOCALIZE.systemAdministrator.activeTests.invalidatePopup.title}
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.error}
                title={LOCALIZE.commons.warning}
                message={
                  <p>
                    {LOCALIZE.formatString(
                      LOCALIZE.systemAdministrator.activeTests.invalidatePopup.description,
                      <span style={styles.bold}>
                        {this.state.selectedActiveTestToDisable.candidate_first_name}
                      </span>,
                      <span style={styles.bold}>
                        {this.state.selectedActiveTestToDisable.candidate_last_name}
                      </span>
                    )}
                  </p>
                }
              />
              <div>
                <label
                  id="reason-for-invalidating-test"
                  style={ActivePermissionsStyles.reasonLabel}
                >
                  {
                    LOCALIZE.systemAdministrator.activeTests.invalidatePopup
                      .reasonForInvalidatingTheTest
                  }
                </label>
                <textarea
                  id="reason-for-invalidating"
                  aria-labelledby="reason-for-invalidating-test"
                  aria-required={true}
                  style={{ ...styles.input, ...styles.textAreaInput, ...accommodationsStyle }}
                  value={this.state.reasonForInvalidatingTest}
                  onChange={this.updateReasonForInvalidatingTest}
                  maxLength="300"
                ></textarea>
              </div>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeInvalidatePopup}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={LOCALIZE.systemAdministrator.activeTests.invalidatePopup.actionButton}
          rightButtonIcon={faTimesCircle}
          rightButtonState={
            !this.state.isValidReasonForInvalidatingTest
              ? BUTTON_STATE.enabled
              : BUTTON_STATE.disabled
          }
          rightButtonAction={this.handleInvalidate}
          size="lg"
        />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    allActiveCandidatesPaginationPage: state.activeTests.allActiveCandidatesPaginationPage,
    allActiveCandidatesPaginationPageSize: state.activeTests.allActiveCandidatesPaginationPageSize,
    allActiveCandidatesKeyword: state.activeTests.allActiveCandidatesKeyword,
    allActiveCandidatesActiveSearch: state.activeTests.allActiveCandidatesActiveSearch
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAllActiveTests,
      getFoundAssignedCandidates,
      updateCurrentAllActiveCandidatesPageState,
      updateAllActiveCandidatesPageSizeState,
      updateSearchAllActiveCandidateStates,
      invalidateCandidateAsEtta
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ActiveTests);
