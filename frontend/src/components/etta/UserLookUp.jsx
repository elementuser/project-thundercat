import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import GenericTable, { COMMON_STYLE } from "../commons/GenericTable";
import SearchBarWithDisplayOptions from "../commons/SearchBarWithDisplayOptions";
import Pagination from "../commons/Pagination";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBinoculars } from "@fortawesome/free-solid-svg-icons";
import {
  getAllUsers,
  getAllFoundUsers,
  updateUserLookUpPageState,
  updateUserLookUpPageSizeState,
  updateSearchUserLookUpStates,
  setSelectedUsername,
  setSelectedUserFirstname,
  setSelectedUserLastname
} from "../../modules/UserLookUpRedux";
import PropTypes from "prop-types";
import CustomButton, { THEME } from "../commons/CustomButton";
import { history } from "../../store-index";
import { PATH } from "../commons/Constants";

const styles = {
  buttonLabel: {
    marginLeft: 6
  }
};

export class UserLookUp extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  static propTypes = {
    // props from Redux
    getAllUsers: PropTypes.func,
    getAllFoundUsers: PropTypes.func,
    updateUserLookUpPageState: PropTypes.func,
    updateUserLookUpPageSizeState: PropTypes.func,
    updateSearchUserLookUpStates: PropTypes.func
  };

  state = {
    rowsDefinition: {},
    currentlyLoading: false,
    displayResultsFound: false,
    resultsFound: 0,
    clearSearchTriggered: false,
    allUsers: [],
    numberOfPages: 1,
    nextPageNumber: 0,
    previousPageNumber: 0,
    isSearchActive: this.props.userLookUpKeyword !== null
  };

  componentDidMount = () => {
    // initialize pagination page redux state
    this.props.updateUserLookUpPageState(1);
    this.populateActiveUsersBasedOnActiveSearch();
  };

  componentDidUpdate = prevProps => {
    // if currentLanguage gets updated
    if (prevProps.currentLanguage !== this.props.currentLanguage) {
      this.populateActiveUsersBasedOnActiveSearch();
    }
    // if userLookUpPaginationPage gets updated
    if (prevProps.userLookUpPaginationPage !== this.props.userLookUpPaginationPage) {
      this.populateActiveUsersBasedOnActiveSearch();
    }
    // // if userLookUpPaginationPageSize get updated
    if (prevProps.userLookUpPaginationPageSize !== this.props.userLookUpPaginationPageSize) {
      this.populateActiveUsersBasedOnActiveSearch();
    }
    // // if search userLookUpKeyword gets updated
    if (prevProps.userLookUpKeyword !== this.props.userLookUpKeyword) {
      // if userLookUpKeyword redux state is empty
      if (this.props.userLookUpKeyword !== "") {
        this.populateFoundUsers();
      } else if (this.props.userLookUpKeyword === "" && this.props.userLookUpActiveSearch) {
        this.populateFoundUsers();
      }
    }
    // // if userLookUpActiveSearch gets updated
    if (prevProps.userLookUpActiveSearch !== this.props.userLookUpActiveSearch) {
      // there is no current active search (on clear search action)
      if (!this.props.userLookUpActiveSearch) {
        // triggering clear search action state
        this.setState({ clearSearchTriggered: true }, () => {
          this.populateAllUsers();
        });
        // there is a current active search (on search action)
      } else {
        this.populateFoundUsers();
      }
    }
  };

  componentWillUnmount() {
    this._isMounted = false;
  }

  populateAllUsers = () => {
    this._isMounted = true;
    this.setState({ currentlyLoading: true }, () => {
      const allUsersArray = [];
      this.props
        .getAllUsers(this.props.userLookUpPaginationPage, this.props.userLookUpPaginationPageSize)
        .then(response => {
          if (this._isMounted) {
            this.populateAllUsersObject(allUsersArray, response);
          }
        })
        .then(() => {
          if (this._isMounted) {
            if (this.state.clearSearchTriggered) {
              // go back to the first page to avoid display bugs
              this.props.updateUserLookUpPageState(1);
              this.setState({ clearSearchTriggered: false });
            }
          }
        })
        .then(() => {
          if (this._isMounted) {
            this.setState({ currentlyLoading: false });
          }
        });
    });
  };

  // populating all found users based on a search
  populateFoundUsers = () => {
    // making sure that the table is not currently loading
    if (!this.state.currentlyLoading) {
      this.setState({ currentlyLoading: true }, () => {
        const allUsersArray = [];
        setTimeout(() => {
          this.props
            .getAllFoundUsers(
              this.props.currentLanguage,
              // Encode search keyword so we can use special characters
              encodeURIComponent(this.props.userLookUpKeyword),
              this.props.userLookUpPaginationPage,
              this.props.userLookUpPaginationPageSize
            )
            .then(response => {
              if (response[0] === "no results found") {
                this.setState({
                  allUsers: [],
                  rowsDefinition: {},
                  numberOfPages: 1,
                  nextPageNumber: 0,
                  previousPageNumber: 0,
                  resultsFound: 0
                });
                // there is at least one result found
              } else {
                this.populateAllUsersObject(allUsersArray, response);
                this.setState({ resultsFound: response.count });
              }
            })
            .then(() => {
              this.setState(
                {
                  currentlyLoading: false,
                  displayResultsFound: true
                },
                () => {
                  // if there is at least one result found
                  if (allUsersArray.length > 0) {
                    // make sure that this element exsits to avoid any error
                    if (document.getElementById("user-look-up-results-found")) {
                      document.getElementById("user-look-up-results-found").focus();
                    }
                    // no results found
                  } else {
                    // focusing on no results found row
                    document.getElementById("user-look-up-no-data").focus();
                  }
                }
              );
            });
        }, 100);
      });
    }
  };

  populateAllUsersObject = (allUsersArray, response) => {
    // initializing needed object and array for rowsDefinition props (needed for GenericTable component)
    let rowsDefinition = {};
    const data = [];
    // making sure that there are some results
    if (typeof response.results !== "undefined") {
      // looping in response given
      for (let i = 0; i < response.results.length; i++) {
        const currentResult = response.results[i];
        // pushing needed data in allUsersarray
        allUsersArray.push({
          username: currentResult.email,
          candidate_first_name: currentResult.first_name,
          candidate_last_name: currentResult.last_name,
          candidate_dob: currentResult.date_of_birth
        });
        // pushing needed data in data array (needed for GenericTable component)
        data.push({
          column_1: currentResult.email,
          column_2: currentResult.first_name,
          column_3: currentResult.last_name,
          column_4: currentResult.date_of_birth,
          column_5: (
            <CustomButton
              buttonId={`user-look-up-view-row-${i}`}
              label={
                <div>
                  <FontAwesomeIcon icon={faBinoculars} />
                  <span style={styles.buttonLabel}>
                    {LOCALIZE.systemAdministrator.userLookUp.table.viewButton}
                  </span>
                </div>
              }
              buttonTheme={THEME.SECONDARY}
              action={() => {
                this.openUserLookUpDetailsPage(
                  currentResult.username,
                  currentResult.first_name,
                  currentResult.last_name
                );
              }}
              ariaLabel={LOCALIZE.formatString(
                LOCALIZE.systemAdministrator.userLookUp.viewButtonAccessibility,
                currentResult.first_name,
                currentResult.last_name
              )}
            />
          )
        });
      }
    }

    // updating rowsDefinition object with provided data and needed style
    rowsDefinition = {
      column_1_style: COMMON_STYLE.LEFT_TEXT,
      column_2_style: COMMON_STYLE.LEFT_TEXT,
      column_3_style: COMMON_STYLE.LEFT_TEXT,
      column_4_style: COMMON_STYLE.CENTERED_TEXT,
      column_5_style: COMMON_STYLE.CENTERED_TEXT,
      data: data
    };

    // saving results in state
    this.setState({
      allUsers: allUsersArray,
      rowsDefinition: rowsDefinition,
      numberOfPages: Math.ceil(response.count / this.props.userLookUpPaginationPageSize),
      nextPageNumber: response.next_page_number,
      previousPageNumber: response.previous_page_number
    });
  };

  openUserLookUpDetailsPage = (username, firstname, lastname) => {
    this.props.setSelectedUsername(username);
    this.props.setSelectedUserFirstname(firstname);
    this.props.setSelectedUserLastname(lastname);
    history.push(PATH.userLookUpDetails);
  };

  populateActiveUsersBasedOnActiveSearch = () => {
    // current search
    if (this.props.userLookUpActiveSearch) {
      this.populateFoundUsers();
      // no search
    } else {
      this.populateAllUsers();
    }
  };

  render() {
    const columnsDefinition = [
      {
        label: LOCALIZE.systemAdministrator.userLookUp.table.columnOne,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label: LOCALIZE.systemAdministrator.userLookUp.table.columnTwo,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label: LOCALIZE.systemAdministrator.userLookUp.table.columnThree,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label: LOCALIZE.systemAdministrator.userLookUp.table.columnFour,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.systemAdministrator.userLookUp.table.columnFive,
        style: COMMON_STYLE.CENTERED_TEXT
      }
    ];
    return (
      <div>
        <h2>{LOCALIZE.systemAdministrator.sideNavItems.userLookUp}</h2>
        <p>{LOCALIZE.systemAdministrator.userLookUp.description}</p>
        <SearchBarWithDisplayOptions
          idPrefix={"user-look-up"}
          currentlyLoading={this.state.currentlyLoading}
          updateSearchStates={this.props.updateSearchUserLookUpStates}
          updatePageState={this.props.updateUserLookUpPageState}
          paginationPageSize={Number(this.props.userLookUpPaginationPageSize)}
          updatePaginationPageSize={this.props.updateUserLookUpPageSizeState}
          data={this.state.allUsers}
          resultsFound={this.state.resultsFound}
          searchBarContent={this.props.userLookUpKeyword}
        />
        <GenericTable
          classnamePrefix="user-look-up"
          columnsDefinition={columnsDefinition}
          rowsDefinition={this.state.rowsDefinition}
          emptyTableMessage={LOCALIZE.systemAdministrator.userLookUp.table.noUsers}
          currentlyLoading={this.state.currentlyLoading}
        />
        <Pagination
          paginationContainerId={"user-look-up-pagination-pages-link"}
          pageCount={this.state.numberOfPages}
          paginationPage={this.props.userLookUpPaginationPage}
          updatePaginationPageState={this.props.updateUserLookUpPageState}
          firstTableRowId={"user-look-up-table-row-0"}
        />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    userLookUpPaginationPageSize: state.userLookUp.userLookUpPaginationPageSize,
    userLookUpPaginationPage: state.userLookUp.userLookUpPaginationPage,
    userLookUpKeyword: state.userLookUp.userLookUpKeyword,
    userLookUpActiveSearch: state.userLookUp.userLookUpActiveSearch
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAllUsers,
      getAllFoundUsers,
      updateUserLookUpPageState,
      updateUserLookUpPageSizeState,
      updateSearchUserLookUpStates,
      setSelectedUsername,
      setSelectedUserFirstname,
      setSelectedUserLastname
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(UserLookUp);
