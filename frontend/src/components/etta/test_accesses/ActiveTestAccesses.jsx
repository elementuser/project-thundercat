import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import LOCALIZE from "../../../text_resources";
import { LANGUAGES } from "../../../modules/LocalizeRedux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrashAlt, faSave, faTimes, faEdit } from "@fortawesome/free-solid-svg-icons";
import { styles as ActivePermissionsStyles } from "../permissions/ActivePermissions";
import { styles as AssignTestAccessesStyles } from "./AssignTestAccesses";
import {
  getAllActiveTestPermissions,
  getFoundActiveTestPermissions,
  updateCurrentTestPermissionsPageState,
  updateTestPermissionsPageSizeState,
  updateSearchActiveTestPermissionsStates,
  deleteTestPermission,
  updateTestPermission
} from "../../../modules/PermissionsRedux";
import "../../../css/etta.css";
import PopupBox, { BUTTON_TYPE } from "../../commons/PopupBox";
import DatePicker from "../../commons/DatePicker";
import populateCustomFutureYearsDateOptions from "../../../helpers/populateCustomDatePickerOptions";
import SystemMessage, { MESSAGE_TYPE } from "../../commons/SystemMessage";
import GenericTable, { COMMON_STYLE } from "../../commons/GenericTable";
import CustomButton, { THEME } from "../../commons/CustomButton";
import { getLineSpacingCSS } from "../../../modules/AccommodationsRedux";
import SearchBarWithDisplayOptions from "../../commons/SearchBarWithDisplayOptions";
import Pagination from "../../commons/Pagination";
import { Container, Row, Col } from "react-bootstrap";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 5,
    xl: 5
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 7,
    xl: 7
  }
};

const styles = {
  viewButton: {
    minWidth: 100
  },
  buttonIcon: {
    marginRight: 6
  },
  labelContainer: {
    display: "table-cell"
  },
  boldText: {
    fontWeight: "bold"
  },
  testAdminRow: {
    padding: "6px 6px 6px 12px"
  },
  colCentered: {
    display: "flex",
    alignItems: "center",
    width: "100%"
  },
  contentFlex: {
    flex: "0 0 100%"
  }
};

class ActiveTestAccesses extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  expiryDateRef = React.createRef();

  static propTypes = {
    // provided by redux
    getAllActiveTestPermissions: PropTypes.func,
    getFoundActiveTestPermissions: PropTypes.func,
    updateCurrentTestPermissionsPageState: PropTypes.func,
    updateTestPermissionsPageSizeState: PropTypes.func,
    updateSearchActiveTestPermissionsStates: PropTypes.func,
    deleteTestPermission: PropTypes.func,
    updateTestPermission: PropTypes.func
  };

  state = {
    displayResultsFound: false,
    searchBarContent: "",
    resultsFound: 0,
    clearSearchTriggered: false,
    displayOptionsArray: [],
    displayOptionSelectedValue: {
      label: `${this.props.testPermissionsPaginationPageSize}`,
      value: this.props.testPermissionsPaginationPageSize
    },
    activeTestPermissions: [],
    rowsDefinition: {},
    numberOfPages: 1,
    nextPageNumber: 0,
    previousPageNumber: 0,
    currentlyLoading: false,
    showViewTestPermissionPopup: false,
    test_permission_id: "",
    user: "",
    username: "",
    email: "",
    testOrderNumber: "",
    staffingProcessNumber: "",
    departmentMinistryCode: "",
    billingContact: "",
    billingContactInfo: "",
    isOrg: "",
    isRef: "",
    testAccess: "",
    expiryDate: "",
    reasonForModifications: "",
    isValidReasonForModifications: true,
    triggerExpiryDateValidation: false,
    showDeleteConfirmationPopup: false,
    showSaveConfirmationPopup: false
  };

  componentDidMount = () => {
    // initialize pagination page redux state
    this.props.updateCurrentTestPermissionsPageState(1);
    // initialize active search redux state
    this.props.updateSearchActiveTestPermissionsStates("", false);
    this.populateActiveTestPermissions();
  };

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentDidUpdate = prevProps => {
    // if currentLanguage gets updated
    if (prevProps.currentLanguage !== this.props.currentLanguage) {
      this.populateActiveTestPermissionsBasedOnActiveSearch();
    }
    // if testPermissionsPaginationPage gets updated
    if (prevProps.testPermissionsPaginationPage !== this.props.testPermissionsPaginationPage) {
      this.populateActiveTestPermissionsBasedOnActiveSearch();
    }
    // if testPermissionsPaginationPageSize get updated
    if (
      prevProps.testPermissionsPaginationPageSize !== this.props.testPermissionsPaginationPageSize
    ) {
      this.populateActiveTestPermissionsBasedOnActiveSearch();
    }
    // if search testPermissionsKeyword gets updated
    if (prevProps.testPermissionsKeyword !== this.props.testPermissionsKeyword) {
      // if testPermissionsKeyword redux state is empty
      if (this.props.testPermissionsKeyword !== "") {
        this.populateFoundActiveTestPermissions();
      } else if (
        this.props.testPermissionsKeyword === "" &&
        this.props.testPermissionsActiveSearch
      ) {
        this.populateFoundActiveTestPermissions();
      }
    }
    // if testPermissionsActiveSearch gets updated
    if (prevProps.testPermissionsActiveSearch !== this.props.testPermissionsActiveSearch) {
      // there is no current active search (on clear search action)
      if (!this.props.testPermissionsActiveSearch) {
        // triggering clear search action state
        this.setState({ clearSearchTriggered: true }, () => {
          this.populateActiveTestPermissions();
        });
        // there is a current active search (on search action)
      } else {
        this.populateFoundActiveTestPermissions();
      }
    }
    // if triggerPopulateTestPermissions gets updated
    if (prevProps.triggerPopulateTestPermissions !== this.props.triggerPopulateTestPermissions) {
      this.populateActiveTestPermissionsBasedOnActiveSearch();
    }
    // if completeDatePicked gets updated
    if (prevProps.completeDatePicked !== this.props.completeDatePicked) {
      // set new expiryDate state
      this.setState({ expiryDate: this.props.completeDatePicked });
    }
  };

  // update searchBarContent content
  updateSearchBarContent = event => {
    const searchBarContent = event.target.value;
    this.setState({
      searchBarContent: searchBarContent
    });
  };

  // populating active test permissions
  populateActiveTestPermissions = () => {
    this._isMounted = true;
    this.setState({ currentlyLoading: true }, () => {
      const activeTestPermissionsArray = [];
      this.props
        .getAllActiveTestPermissions(
          this.props.testPermissionsPaginationPage,
          this.props.testPermissionsPaginationPageSize
        )
        .then(response => {
          if (this._isMounted) {
            this.populateActiveTestPermissionsObject(activeTestPermissionsArray, response);
          }
        })
        .then(() => {
          if (this._isMounted) {
            if (this.state.clearSearchTriggered) {
              // go back to the first page to avoid display bugs
              this.props.updateCurrentTestPermissionsPageState(1);
              this.setState({ clearSearchTriggered: false });
            }
          }
        })
        .then(() => {
          if (this._isMounted) {
            this.setState({ currentlyLoading: false });
          }
        });
    });
  };

  // populating all found active test permissions based on a search
  populateFoundActiveTestPermissions = () => {
    this.setState({ currentlyLoading: true }, () => {
      const activeTestPermissionsArray = [];
      this.props
        .getFoundActiveTestPermissions(
          this.props.currentLanguage,
          this.props.testPermissionsKeyword,
          this.props.testPermissionsPaginationPage,
          this.props.testPermissionsPaginationPageSize
        )
        .then(response => {
          // there are no results found
          if (response[0] === "no results found") {
            this.setState({
              activeTestPermissions: [],
              rowsDefinition: {},
              numberOfPages: 1,
              nextPageNumber: 0,
              previousPageNumber: 0,
              resultsFound: 0
            });
            // there is at least one result found
          } else {
            this.populateActiveTestPermissionsObject(activeTestPermissionsArray, response);
            this.setState({ resultsFound: response.count });
          }
        })
        .then(() => {
          this.setState({ currentlyLoading: false, displayResultsFound: true }, () => {
            // if there is at least one result found
            if (activeTestPermissionsArray.length > 0) {
              // focusing on results found label only if save confirmation popup is not displayed (avoiding focus bug)
              if (!this.state.showSaveConfirmationPopup) {
                // make sure that this element exsits to avoid any error
                if (document.getElementById("active-test-accesses-results-found")) {
                  document.getElementById("active-test-accesses-results-found").focus();
                }
              }
              // no results found
            } else {
              // focusing on no results found row
              document.getElementById("active-test-accesses-no-data").focus();
            }
          });
        });
    });
  };

  populateActiveTestPermissionsObject = (activeTestPermissionsArray, response) => {
    // initializing needed object and array for rowsDefinition props (needed for GenericTable component)
    let rowsDefinition = {};
    const data = [];
    // making sure that there are some results
    if (typeof response.results !== "undefined") {
      // looping in response given
      for (let i = 0; i < response.results.length; i++) {
        const currentResult = response.results[i];
        // pushing needed data in activeTestPermissions array
        activeTestPermissionsArray.push({
          id: currentResult.id,
          first_name: currentResult.first_name,
          last_name: currentResult.last_name,
          en_test_name: currentResult.test.en_name,
          fr_test_name: currentResult.test.fr_name,
          version: currentResult.test.version,
          test_order_number: currentResult.test_order_number,
          expiry_date: currentResult.expiry_date,
          username: currentResult.username,
          email: currentResult.email,
          staffing_process_number: currentResult.staffing_process_number,
          department_ministry_code: currentResult.department_ministry_code,
          billing_contact: currentResult.billing_contact,
          billing_contact_info: currentResult.billing_contact_info,
          is_org: currentResult.is_org,
          is_ref: currentResult.is_ref
        });
        // pushing needed data in data array (needed for GenericTable component)
        data.push({
          column_1: `${currentResult.last_name}, ${currentResult.first_name}`,
          column_2: `
          ${currentResult.test[`${this.props.currentLanguage}_name`]} v${
            currentResult.test.version
          }`,
          column_3: currentResult.test_order_number,
          column_4: currentResult.expiry_date,
          column_5: (
            <CustomButton
              buttonId={`active-test-accesses-button-row-${i}`}
              label={
                <>
                  <FontAwesomeIcon icon={faEdit} style={styles.buttonIcon} />
                  <span>
                    {
                      LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses.table
                        .actionButtonLabel
                    }
                  </span>
                </>
              }
              action={() => this.handleOpenViewPopup(i)}
              customStyle={styles.viewButton}
              buttonTheme={THEME.SECONDARY}
              ariaLabel={LOCALIZE.formatString(
                LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses.table
                  .actionButtonAriaLabel,
                currentResult.first_name,
                currentResult.last_name,
                currentResult.test[`${this.props.currentLanguage}_name`],
                currentResult.test_order_number,
                currentResult.expiry_date
              )}
            />
          )
        });
      }
    }

    // updating rowsDefinition object with provided data and needed style
    rowsDefinition = {
      column_1_style: styles.testAdminRow,
      column_2_style: COMMON_STYLE.LEFT_TEXT,
      column_3_style: COMMON_STYLE.LEFT_TEXT,
      column_4_style: COMMON_STYLE.CENTERED_TEXT,
      column_5_style: COMMON_STYLE.CENTERED_TEXT,
      data: data
    };

    // saving results in state
    this.setState({
      activeTestPermissions: activeTestPermissionsArray,
      rowsDefinition: rowsDefinition,
      numberOfPages: Math.ceil(response.count / this.props.testPermissionsPaginationPageSize),
      nextPageNumber: response.next_page_number,
      previousPageNumber: response.previous_page_number
    });
  };

  // populating active test permissions based on the testPermissionsActiveSearch redux state
  populateActiveTestPermissionsBasedOnActiveSearch = () => {
    // current search
    if (this.props.testPermissionsActiveSearch) {
      this.populateFoundActiveTestPermissions();
      // no current search
    } else {
      this.populateActiveTestPermissions();
    }
  };

  // handling open selected test permission view popup
  handleOpenViewPopup = id => {
    const testAccess =
      this.props.currentLanguage === LANGUAGES.english
        ? this.state.activeTestPermissions[id].en_test_name
        : this.state.activeTestPermissions[id].fr_test_name;
    this.setState(
      {
        test_permission_id: this.state.activeTestPermissions[id].id,
        user: `${this.state.activeTestPermissions[id].first_name} ${this.state.activeTestPermissions[id].last_name}`,
        username: this.state.activeTestPermissions[id].username,
        email: this.state.activeTestPermissions[id].email,
        testOrderNumber: this.state.activeTestPermissions[id].test_order_number,
        staffingProcessNumber: this.state.activeTestPermissions[id].staffing_process_number,
        departmentMinistryCode: this.state.activeTestPermissions[id].department_ministry_code,
        billingContact: this.state.activeTestPermissions[id].billing_contact,
        billingContactInfo: this.state.activeTestPermissions[id].billing_contact_info,
        isOrg: this.state.activeTestPermissions[id].is_org,
        isRef: this.state.activeTestPermissions[id].is_ref,
        testAccess: `${testAccess} v${this.state.activeTestPermissions[id].version}`,
        expiryDate: this.state.activeTestPermissions[id].expiry_date
      },
      () => {
        this.setState({ showViewTestPermissionPopup: true });
      }
    );
  };

  // getting initial date year value
  getInitialDateYearValue = expiryDate => {
    const initialDateYearValue = expiryDate.split("-")[0];
    return { label: initialDateYearValue, value: Number(initialDateYearValue) };
  };

  // getting initial date month value
  getInitialDateMonthValue = expiryDate => {
    const initialDateMonthValue = expiryDate.split("-")[1];
    let croppedInitialDateMonthValue = initialDateMonthValue;
    // needs to be defnied
    if (typeof initialDateMonthValue !== "undefined") {
      // if month is between 01 and 09
      if (initialDateMonthValue.charAt(0) === "0") {
        // remove first char to only get the number (03 -> 3)
        croppedInitialDateMonthValue = initialDateMonthValue.substr(1);
      }
    }
    return { label: initialDateMonthValue, value: Number(croppedInitialDateMonthValue) };
  };

  // getting initial date day value
  getInitialDateDayValue = expiryDate => {
    const initialDateDayValue = expiryDate.split("-")[2];
    let croppedInitialDateDayValue = initialDateDayValue;
    // needs to be defnied
    if (typeof initialDateDayValue !== "undefined") {
      // if day is between 01 and 09
      if (initialDateDayValue.charAt(0) === "0") {
        // remove first char to only get the number (03 -> 3)
        croppedInitialDateDayValue = initialDateDayValue.substr(1);
      }
    }
    return { label: initialDateDayValue, value: Number(croppedInitialDateDayValue) };
  };

  // update reasonForModifications content
  updateReasonForModificationsContent = event => {
    const reasonForModifications = event.target.value;
    this.setState({
      reasonForModifications: reasonForModifications
    });
  };

  handleCloseViewPopup = () => {
    this.setState({
      showViewTestPermissionPopup: false,
      isValidReasonForModifications: true,
      reasonForModifications: ""
    });
  };

  handleDeleteTestPermission = () => {
    const body = {
      test_permission_id: this.state.test_permission_id,
      reason_for_deletion: this.state.reasonForModifications
    };
    this.props.deleteTestPermission(body).then(response => {
      if (response.status === 200) {
        // re-populating test permissions table
        this.populateActiveTestPermissionsBasedOnActiveSearch();
        this.setState({ showViewTestPermissionPopup: false, reasonForModifications: "" });
      } else {
        throw new Error("An error occurred during the delete test permission request process");
      }
    });
  };

  openDeleteConfirmationPopup = () => {
    // reason for modifications validation
    // must contain at least one letter to be valid
    const regexExpression = /^(.*[A-Za-z].*)$/;
    const isValidReasonForModifications = !!regexExpression.test(this.state.reasonForModifications);
    // valid reason for modifications
    if (isValidReasonForModifications) {
      this.setState({ showDeleteConfirmationPopup: true });
      // invalid reason for modifications
    } else {
      this.setState({ isValidReasonForModifications: false }, () => {
        document.getElementById("reason-for-modifications").focus();
      });
    }
  };

  closeDeleteConfirmationPopup = () => {
    this.setState({
      showDeleteConfirmationPopup: false,
      isValidReasonForModifications: true
    });
  };

  closeSaveConfirmationPopup = () => {
    this.setState({ showSaveConfirmationPopup: false });
  };

  handleSaveTestPermission = () => {
    // reason for modifications validation
    // must contain at least one letter to be valid
    const regexExpression = /^(.*[A-Za-z].*)$/;
    const isValidReasonForModifications = !!regexExpression.test(this.state.reasonForModifications);

    // trigger date validation
    this.setState({ triggerExpiryDateValidation: !this.state.triggerExpiryDateValidation }, () => {
      // saving validation results in states
      this.setState(
        {
          isValidReasonForModifications: isValidReasonForModifications
        },
        () => {
          // all fields are valid
          if (this.state.isValidReasonForModifications && this.props.completeDateValidState) {
            const body = {
              test_permission_id: this.state.test_permission_id,
              expiry_date: this.state.expiryDate,
              reason_for_modification: this.state.reasonForModifications
            };
            this.props.updateTestPermission(body).then(response => {
              if (response.status === 200) {
                // re-populating test permissions table
                this.populateActiveTestPermissionsBasedOnActiveSearch();
                this.setState({
                  showViewTestPermissionPopup: false,
                  showSaveConfirmationPopup: true,
                  reasonForModifications: ""
                });
              } else {
                throw new Error(
                  "An error occurred during the delete test permission request process"
                );
              }
            });
            // there is at least one field invalid, so focus on the highest error field
          } else {
            this.focusOnHighestErrorField();
          }
        }
      );
    });
  };

  // analysing field by field and focusing on the highest error field
  focusOnHighestErrorField = () => {
    if (!this.props.completeDateValidState) {
      this.expiryDateRef.current.focus();
    } else if (!this.state.isValidReasonForModifications) {
      document.getElementById("reason-for-modifications").focus();
    }
  };

  render() {
    const columnsDefinition = [
      {
        label:
          LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses.table.testAdministrator,
        style: { width: "20%" }
      },
      {
        label: LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses.table.test,
        style: { ...{ width: "25%" }, ...COMMON_STYLE.LEFT_TEXT }
      },
      {
        label: LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses.table.orderNumber,
        style: { ...{ width: "20%" }, ...COMMON_STYLE.LEFT_TEXT }
      },
      {
        label: LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses.table.expiryDate,
        style: { ...{ width: "20%" }, ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label: LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses.table.action,
        style: { ...{ width: "15%" }, ...COMMON_STYLE.CENTERED_TEXT }
      }
    ];

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <div>
        <SearchBarWithDisplayOptions
          idPrefix={"active-test-accesses"}
          currentlyLoading={this.state.currentlyLoading}
          updateSearchStates={this.props.updateSearchActiveTestPermissionsStates}
          updatePageState={this.props.updateCurrentTestPermissionsPageState}
          paginationPageSize={Number(this.props.testPermissionsPaginationPageSize)}
          updatePaginationPageSize={this.props.updateTestPermissionsPageSizeState}
          data={this.state.activeTestPermissions}
          resultsFound={this.state.resultsFound}
        />
        <div>
          <GenericTable
            classnamePrefix="active-test-accesses"
            columnsDefinition={columnsDefinition}
            rowsDefinition={this.state.rowsDefinition}
            emptyTableMessage={LOCALIZE.SearchBarWithDisplayOptions.noResultsFound}
            currentlyLoading={this.state.currentlyLoading}
          />
          <Pagination
            paginationContainerId={"active-test-accesses-pagination-pages-link"}
            pageCount={this.state.numberOfPages}
            paginationPage={this.props.testPermissionsPaginationPage}
            updatePaginationPageState={this.props.updateCurrentTestPermissionsPageState}
            firstTableRowId={"active-test-accesses-table-row-0"}
          />
        </div>
        <PopupBox
          show={this.state.showViewTestPermissionPopup}
          handleClose={() => {}}
          isBackdropStatic={true}
          shouldCloseOnEsc={false}
          displayCloseButton={true}
          closeButtonAction={this.handleCloseViewPopup}
          title={
            LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses
              .viewTestPermissionPopup.title
          }
          description={
            <Container>
              <Row className="align-items-center justify-content-start">
                <p>
                  {LOCALIZE.formatString(
                    LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses
                      .viewTestPermissionPopup.description,
                    <span style={{ ...styles.boldText, ...styles.contentFlex }}>
                      {this.state.user}
                    </span>
                  )}
                </p>
              </Row>
              <Row
                className="align-items-center justify-content-start"
                style={AssignTestAccessesStyles.fieldSeparator}
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={{ ...styles.labelContainer, ...styles.colCentered }}
                >
                  <label style={{ ...AssignTestAccessesStyles.label, ...styles.contentFlex }}>
                    {
                      LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses
                        .viewTestPermissionPopup.username
                    }
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={{ ...AssignTestAccessesStyles.inputContainer, ...styles.colCentered }}
                >
                  <input
                    id="username"
                    type="text"
                    disabled={true}
                    style={{
                      ...AssignTestAccessesStyles.input,
                      ...accommodationsStyle,
                      ...styles.contentFlex
                    }}
                    value={this.state.email}
                    onChange={() => {}}
                  ></input>
                </Col>
              </Row>
              <Row
                className="align-items-center justify-content-start"
                style={AssignTestAccessesStyles.fieldSeparator}
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={{ ...styles.labelContainer, ...styles.colCentered }}
                >
                  <label style={{ ...AssignTestAccessesStyles.label, ...styles.contentFlex }}>
                    {
                      LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses
                        .testOrderNumberLabel
                    }
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={{ ...AssignTestAccessesStyles.inputContainer, ...styles.colCentered }}
                >
                  <input
                    id="test-order-number"
                    type="text"
                    disabled={true}
                    style={{
                      ...AssignTestAccessesStyles.input,
                      ...accommodationsStyle,
                      ...styles.contentFlex
                    }}
                    value={this.state.testOrderNumber}
                    onChange={() => {}}
                  ></input>
                </Col>
              </Row>
              <Row
                className="align-items-center justify-content-start"
                style={AssignTestAccessesStyles.fieldSeparator}
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={{ ...styles.labelContainer, ...styles.colCentered }}
                >
                  <label style={{ ...AssignTestAccessesStyles.label, ...styles.contentFlex }}>
                    {
                      LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses
                        .staffingProcessNumber
                    }
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={{ ...AssignTestAccessesStyles.inputContainer, ...styles.colCentered }}
                >
                  <input
                    id="staffing-process-number"
                    type="text"
                    disabled={true}
                    style={{
                      ...AssignTestAccessesStyles.input,
                      ...accommodationsStyle,
                      ...styles.contentFlex
                    }}
                    value={this.state.staffingProcessNumber}
                    onChange={() => {}}
                  ></input>
                </Col>
              </Row>
              <Row
                className="align-items-center justify-content-start"
                style={AssignTestAccessesStyles.fieldSeparator}
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={{ ...styles.labelContainer, ...styles.colCentered }}
                >
                  <label style={{ ...AssignTestAccessesStyles.label, ...styles.contentFlex }}>
                    {
                      LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses
                        .departmentMinistry
                    }
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={{ ...AssignTestAccessesStyles.inputContainer, ...styles.colCentered }}
                >
                  <input
                    id="department-ministry-code"
                    type="text"
                    disabled={true}
                    style={{
                      ...AssignTestAccessesStyles.input,
                      ...accommodationsStyle,
                      ...styles.contentFlex
                    }}
                    value={this.state.departmentMinistryCode}
                    onChange={() => {}}
                  ></input>
                </Col>
              </Row>
              <Row
                className="align-items-center justify-content-start"
                style={AssignTestAccessesStyles.fieldSeparator}
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={{ ...styles.labelContainer, ...styles.colCentered }}
                >
                  <label style={{ ...AssignTestAccessesStyles.label, ...styles.contentFlex }}>
                    {
                      LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses
                        .billingContact
                    }
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={{ ...AssignTestAccessesStyles.inputContainer, ...styles.colCentered }}
                >
                  <input
                    id="billing-contact"
                    type="text"
                    disabled={true}
                    style={{
                      ...AssignTestAccessesStyles.input,
                      ...accommodationsStyle,
                      ...styles.contentFlex
                    }}
                    value={this.state.billingContact}
                    onChange={() => {}}
                  ></input>
                </Col>
              </Row>
              <Row
                className="align-items-center justify-content-start"
                style={AssignTestAccessesStyles.fieldSeparator}
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={{ ...styles.labelContainer, ...styles.colCentered }}
                >
                  <label style={{ ...AssignTestAccessesStyles.label, ...styles.contentFlex }}>
                    {
                      LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses
                        .billingContactInfo
                    }
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={{ ...AssignTestAccessesStyles.inputContainer, ...styles.colCentered }}
                >
                  <input
                    id="billing-contact-info"
                    type="text"
                    disabled={true}
                    style={{
                      ...AssignTestAccessesStyles.input,
                      ...accommodationsStyle,
                      ...styles.contentFlex
                    }}
                    value={this.state.billingContactInfo}
                    onChange={() => {}}
                  ></input>
                </Col>
              </Row>
              <Row
                className="align-items-center justify-content-start"
                style={AssignTestAccessesStyles.fieldSeparator}
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={{ ...styles.labelContainer, ...styles.colCentered }}
                >
                  <label style={{ ...AssignTestAccessesStyles.label, ...styles.contentFlex }}>
                    {LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.isOrg}
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={{ ...AssignTestAccessesStyles.inputContainer, ...styles.colCentered }}
                >
                  <input
                    id="is-org"
                    type="text"
                    disabled={true}
                    style={{
                      ...AssignTestAccessesStyles.input,
                      ...accommodationsStyle,
                      ...styles.contentFlex
                    }}
                    value={this.state.isOrg}
                    onChange={() => {}}
                  ></input>
                </Col>
              </Row>
              <Row
                className="align-items-center justify-content-start"
                style={AssignTestAccessesStyles.fieldSeparator}
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={{ ...styles.labelContainer, ...styles.colCentered }}
                >
                  <label style={{ ...AssignTestAccessesStyles.label, ...styles.contentFlex }}>
                    {LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.isRef}
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={{ ...AssignTestAccessesStyles.inputContainer, ...styles.colCentered }}
                >
                  <input
                    id="is-ref"
                    type="text"
                    disabled={true}
                    style={{
                      ...AssignTestAccessesStyles.input,
                      ...accommodationsStyle,
                      ...styles.contentFlex
                    }}
                    value={this.state.isRef}
                    onChange={() => {}}
                  ></input>
                </Col>
              </Row>
              <Row
                className="align-items-center justify-content-start"
                style={AssignTestAccessesStyles.fieldSeparator}
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={{ ...styles.labelContainer, ...styles.colCentered }}
                >
                  <label style={{ ...AssignTestAccessesStyles.label, ...styles.contentFlex }}>
                    {LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.testAccess}
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={{ ...AssignTestAccessesStyles.inputContainer, ...styles.colCentered }}
                >
                  <input
                    id="test-access"
                    type="text"
                    disabled={true}
                    style={{
                      ...AssignTestAccessesStyles.input,
                      ...accommodationsStyle,
                      ...styles.contentFlex
                    }}
                    value={this.state.testAccess}
                    onChange={() => {}}
                  ></input>
                </Col>
              </Row>
              <Row
                className="align-items-center justify-content-start"
                style={AssignTestAccessesStyles.fieldSeparator}
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={12}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={{
                    ...AssignTestAccessesStyles.expiryDateLabelContainer,
                    ...styles.colCentered
                  }}
                >
                  <label
                    id="expiry-date-label"
                    style={{ ...AssignTestAccessesStyles.expiryDateLabel, ...styles.contentFlex }}
                  >
                    {LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.expiryDate}
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={12}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={{ ...AssignTestAccessesStyles.inputContainer, ...styles.colCentered }}
                >
                  <DatePicker
                    dateDayFieldRef={this.expiryDateRef}
                    dateLabelId={"expiry-date-label"}
                    triggerValidation={this.state.triggerExpiryDateValidation}
                    displayValidIcon={false}
                    customYearOptions={populateCustomFutureYearsDateOptions(5)}
                    initialDateYearValue={this.getInitialDateYearValue(this.state.expiryDate)}
                    initialDateMonthValue={this.getInitialDateMonthValue(this.state.expiryDate)}
                    initialDateDayValue={this.getInitialDateDayValue(this.state.expiryDate)}
                    futureDateValidation={true}
                    menuPlacement={"top"}
                  />
                </Col>
              </Row>
              <Row className="align-items-center justify-content-start">
                <Col
                  xl={12}
                  lg={12}
                  md={12}
                  sm={12}
                  xs={12}
                  style={{ ...AssignTestAccessesStyles.inputContainer, ...styles.colCentered }}
                >
                  <label
                    id="reason-for-modification-label"
                    style={ActivePermissionsStyles.reasonLabel}
                  >
                    {
                      LOCALIZE.systemAdministrator.permissions.tabs.activePermissions
                        .viewEditDetailsPopup.reasonForModification
                    }
                  </label>
                </Col>
              </Row>
              <Row className="align-items-center justify-content-start">
                <Col
                  xl={12}
                  lg={12}
                  md={12}
                  sm={12}
                  xs={12}
                  style={{ ...AssignTestAccessesStyles.inputContainer, ...styles.colCentered }}
                >
                  <textarea
                    id="reason-for-modifications"
                    className={
                      this.state.isValidReasonForModifications ? "valid-field" : "invalid-field"
                    }
                    aria-labelledby="reason-for-modification-label reason-for-modifications-error"
                    aria-required={true}
                    aria-invalid={!this.state.isValidReasonForModifications}
                    style={{
                      ...ActivePermissionsStyles.input,
                      ...ActivePermissionsStyles.reasonInput,
                      ...accommodationsStyle
                    }}
                    value={this.state.reasonForModifications}
                    onChange={this.updateReasonForModificationsContent}
                    maxLength="300"
                  ></textarea>
                </Col>
              </Row>
              {!this.state.isValidReasonForModifications && (
                <Row className="align-items-center justify-content-start">
                  <Col
                    xl={12}
                    lg={12}
                    md={12}
                    sm={12}
                    xs={12}
                    style={{ ...AssignTestAccessesStyles.inputContainer, ...styles.colCentered }}
                  >
                    <label
                      id="reason-for-modifications-error"
                      htmlFor="reason-for-modifications"
                      style={ActivePermissionsStyles.ReasonErrorMessage}
                    >
                      {
                        LOCALIZE.systemAdministrator.permissions.tabs.activePermissions
                          .viewEditDetailsPopup.reasonForModificationError
                      }
                    </label>
                  </Col>
                </Row>
              )}
            </Container>
          }
          leftButtonType={BUTTON_TYPE.danger}
          leftButtonIcon={faTrashAlt}
          leftButtonTitle={
            LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses
              .viewTestPermissionPopup.deleteButtonAccessibility
          }
          leftButtonLabel={
            LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses
              .viewTestPermissionPopup.deleteButtonAccessibility
          }
          leftButtonAction={this.openDeleteConfirmationPopup}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonCustomStyle={{ backgroundColor: "#278400" }}
          rightButtonIcon={faSave}
          rightButtonTitle={
            LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses
              .viewTestPermissionPopup.saveButtonAccessibility
          }
          rightButtonLabel={
            LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses
              .viewTestPermissionPopup.saveButtonAccessibility
          }
          rightButtonAction={this.handleSaveTestPermission}
        />
        <PopupBox
          show={this.state.showDeleteConfirmationPopup}
          handleClose={this.closeDeleteConfirmationPopup}
          isBackdropStatic={false}
          shouldCloseOnEsc={true}
          size={"lg"}
          title={
            LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses
              .deleteConfirmationPopup.title
          }
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.error}
                title={LOCALIZE.commons.warning}
                message={
                  <div>
                    <p>
                      {LOCALIZE.formatString(
                        LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses
                          .deleteConfirmationPopup.systemMessageDescription,
                        <span style={ActivePermissionsStyles.boldText}>
                          {this.state.testAccess}
                        </span>,
                        <span style={ActivePermissionsStyles.boldText}>
                          {this.state.testOrderNumber}
                        </span>,
                        <span style={ActivePermissionsStyles.boldText}>{this.state.user}</span>
                      )}
                    </p>
                  </div>
                }
              />
            </div>
          }
          leftButtonType={BUTTON_TYPE.primary}
          leftButtonIcon={faTimes}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonLabel={LOCALIZE.commons.cancel}
          leftButtonAction={this.closeDeleteConfirmationPopup}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonIcon={faTrashAlt}
          rightButtonTitle={
            LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses
              .viewTestPermissionPopup.deleteButtonAccessibility
          }
          rightButtonLabel={
            LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses
              .viewTestPermissionPopup.deleteButtonAccessibility
          }
          rightButtonAction={this.handleDeleteTestPermission}
        />
        <PopupBox
          show={this.state.showSaveConfirmationPopup}
          handleClose={() => {}}
          isBackdropStatic={true}
          shouldCloseOnEsc={false}
          title={
            LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses.saveConfirmationPopup
              .title
          }
          description={
            <div>
              <p>
                {
                  <SystemMessage
                    messageType={MESSAGE_TYPE.success}
                    title={LOCALIZE.commons.success}
                    message={LOCALIZE.formatString(
                      LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses
                        .saveConfirmationPopup.description,
                      <span style={ActivePermissionsStyles.boldText}>{this.state.user}</span>
                    )}
                  />
                }
              </p>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.ok}
          rightButtonAction={this.closeSaveConfirmationPopup}
        />
      </div>
    );
  }
}

export { ActiveTestAccesses as unconnectedActiveTestAccesses };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    testPermissionsPaginationPageSize: state.userPermissions.testPermissionsPaginationPageSize,
    testPermissionsPaginationPage: state.userPermissions.testPermissionsPaginationPage,
    testPermissionsKeyword: state.userPermissions.testPermissionsKeyword,
    triggerPopulateTestPermissions: state.userPermissions.triggerPopulateTestPermissions,
    testPermissionsActiveSearch: state.userPermissions.testPermissionsActiveSearch,
    completeDatePicked: state.datePicker.completeDatePicked,
    completeDateValidState: state.datePicker.completeDateValidState,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAllActiveTestPermissions,
      getFoundActiveTestPermissions,
      updateCurrentTestPermissionsPageState,
      updateTestPermissionsPageSizeState,
      updateSearchActiveTestPermissionsStates,
      deleteTestPermission,
      updateTestPermission
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ActiveTestAccesses);
