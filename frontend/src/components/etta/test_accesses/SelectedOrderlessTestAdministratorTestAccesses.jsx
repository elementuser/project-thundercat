/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getLineSpacingCSS } from "../../../modules/AccommodationsRedux";
import LOCALIZE from "../../../text_resources";
import {
  updateCurrentOrderlessTestAccessesPageState,
  updateOrderlessTestAccessesPageSizeState,
  updateSearchOrderlessTestAccessesStates,
  getOrderlessTestAccesses,
  updateOrderlessTestAccessesData,
  addDeleteOrderlessTestAccess,
  getFoundOrderlessTestAccesses,
  updateOrderlessTestAdministratorDepartment,
  setSelectedOrderlesstestAdministratorState
} from "../../../modules/PermissionsRedux";
import SearchBarWithDisplayOptions from "../../commons/SearchBarWithDisplayOptions";
import GenericTable, { COMMON_STYLE } from "../../commons/GenericTable";
import Pagination from "../../commons/Pagination";
import Switch from "react-switch";
import getSwitchTransformScale from "../../../helpers/switchTransformScale";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Col, Container, Row } from "react-bootstrap";
import DropdownSelect from "../../commons/DropdownSelect";
import { LANGUAGES } from "../../commons/Translation";
import { getOrganization } from "../../../modules/ExtendedProfileOptionsRedux";
import PopupBox, { BUTTON_TYPE } from "../../commons/PopupBox";
import SystemMessage, { MESSAGE_TYPE } from "../../commons/SystemMessage";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 5,
    xl: 5
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 7,
    xl: 7
  }
};

const styles = {
  boldText: {
    fontWeight: "bold"
  },
  assignedColumnStyle: {
    padding: "12px 32px"
  },
  saveButtonContainer: {
    marginTop: 24
  },
  fieldSeparator: {
    marginTop: 18
  },
  colCenteredLabel: {
    display: "flex",
    alignItems: "center",
    width: "100%"
  },
  colCentered: {
    alignItems: "center",
    width: "100%"
  },
  labelContainer: {
    display: "table-cell",
    verticalAlign: "middle"
  },
  label: {
    overflowWrap: "break-word"
  },
  contentFlex: {
    flex: "0 0 100%"
  },
  inputContainer: {
    width: "100%",
    verticalAlign: "middle"
  },
  loadingDepartmentSpinnerContainer: {
    textAlign: "center"
  }
};

class SelectedOrderlessTestAdministratorTestAccesses extends Component {
  static propTypes = {
    // provided by redux
    updateCurrentOrderlessTestAccessesPageState: PropTypes.func,
    updateOrderlessTestAccessesPageSizeState: PropTypes.func,
    updateSearchOrderlessTestAccessesStates: PropTypes.func
  };

  state = {
    displayResultsFound: false,
    currentlyLoading: false,
    orderlessTestAccesses: [],
    resultsFound: 0,
    rowsDefinition: {},
    numberOfPages: 1,
    clearSearchTriggered: false,
    isLoadingOrganisations: true,
    organisationsOptions: [],
    organisationSelectedOption: [],
    callingGetFoundOrderlessTestAccesses: false,
    showSaveConfirmationPopup: false,
    // default values
    switchHeight: 25,
    switchWidth: 50
  };

  componentDidMount = () => {
    // initialize pagination page redux state
    this.props.updateCurrentOrderlessTestAccessesPageState(1);
    // if page size is not defined, initialize it
    if (typeof this.props.orderlessTestAccessesPaginationPageSize === "undefined") {
      this.props.updateOrderlessTestAccessesPageSizeState(25);
    }
    // populating/getting needed data on render
    this.getUpdatedSwitchDimensions();
    this.populateTestAccessesTable();
    this.populateOrganisationsOptions();
  };

  componentDidUpdate = prevProps => {
    // if font size gets updated
    if (prevProps.accommodations.fontSize !== this.props.accommodations.fontSize) {
      this.getUpdatedSwitchDimensions();
    }
    // if currentLanguage gets updated
    if (prevProps.currentLanguage !== this.props.currentLanguage) {
      this.populateOrderlessTestAccessesBasedOnActiveSearch();
    }
    // if orderlessTestAccessesPaginationPage gets updated
    if (
      prevProps.orderlessTestAccessesPaginationPage !==
      this.props.orderlessTestAccessesPaginationPage
    ) {
      this.populateOrderlessTestAccessesBasedOnActiveSearch();
    }
    // if orderlessTestAccessesPaginationPageSize get updated
    if (
      prevProps.orderlessTestAccessesPaginationPageSize !==
      this.props.orderlessTestAccessesPaginationPageSize
    ) {
      this.populateOrderlessTestAccessesBasedOnActiveSearch();
    }
    // if search orderlessTestAccessesKeyword gets updated
    if (prevProps.orderlessTestAccessesKeyword !== this.props.orderlessTestAccessesKeyword) {
      // if orderlessTestAccessesKeyword redux state is empty
      if (this.props.orderlessTestAccessesKeyword !== "") {
        this.populateFoundOrderlessTestAccesses();
      } else if (
        this.props.orderlessTestAccessesKeyword === "" &&
        this.props.orderlessTestAccessesActiveSearch
      ) {
        this.populateFoundOrderlessTestAccesses();
      }
    }
    // if orderlessTestAccessesActiveSearch gets updated
    if (
      prevProps.orderlessTestAccessesActiveSearch !== this.props.orderlessTestAccessesActiveSearch
    ) {
      // there is no current active search (on clear search action)
      if (!this.props.orderlessTestAccessesActiveSearch) {
        // triggering clear search action state
        this.setState({ clearSearchTriggered: true }, () => {
          this.populateTestAccessesTable();
        });
        // there is a current active search (on search action)
      } else {
        this.populateFoundOrderlessTestAccesses();
      }
    }
    // if triggerpopulateOrderlessTestAccesses gets updated
    if (
      prevProps.triggerpopulateOrderlessTestAccesses !==
      this.props.triggerpopulateOrderlessTestAccesses
    ) {
      this.populateOrderlessTestAccessesBasedOnActiveSearch();
    }
  };

  // populating active test permissions based on the orderlessTestAccessesActiveSearch redux state
  populateOrderlessTestAccessesBasedOnActiveSearch = () => {
    // current search
    if (this.props.orderlessTestAccessesActiveSearch) {
      this.populateFoundOrderlessTestAccesses();
      // no current search
    } else {
      this.populateTestAccessesTable();
    }
  };

  populateTestAccessesTable = () => {
    this.setState({ currentlyLoading: true }, () => {
      const orderlessTestAccessesArray = [];
      this.props
        .getOrderlessTestAccesses(
          this.props.orderlessTestAccessesPaginationPage,
          this.props.orderlessTestAccessesPaginationPageSize,
          this.props.selectedOrderlessTestAdministratorData.username_id
        )
        .then(response => {
          this.populateOrderlessTestAccessesObject(orderlessTestAccessesArray, response);
        })
        .then(() => {
          if (this.state.clearSearchTriggered) {
            // go back to the first page to avoid display bugs
            this.props.updateCurrentOrderlessTestAccessesPageState(1);
            this.setState({ clearSearchTriggered: false });
          }
        })
        .then(() => {
          this.setState({ currentlyLoading: false, callingGetFoundActiveTestAccessCodes: false });
        });
    });
  };

  populateOrderlessTestAccessesObject = (orderlessTestAccessesArray, response) => {
    // initializing needed object and array for rowsDefinition props (needed for GenericTable component)
    let rowsDefinition = {};
    const data = [];
    // making sure that there are some results
    if (typeof response.results !== "undefined") {
      // looping in response given
      for (let i = 0; i < response.results.length; i++) {
        const currentResult = response.results[i];
        // pushing needed data in orderlessTestAccesses array
        orderlessTestAccessesArray.push({
          parent_code: currentResult.parent_code,
          test_code: currentResult.test_code,
          test_desc_en: currentResult.test_desc_en,
          test_desc_fr: currentResult.test_desc_fr,
          has_test_access: currentResult.has_test_access
        });
        // pushing needed data in data array (needed for GenericTable component)
        data.push({
          column_1: `${currentResult.parent_code} - ${currentResult.test_code}`,
          column_2: `${currentResult[`test_desc_${this.props.currentLanguage}`]}`,
          column_3: (
            <div>
              <Switch
                onChange={(checked, event, id, parent_code, test_code) =>
                  this.onSwitchChange(checked, currentResult.parent_code, currentResult.test_code)
                }
                checked={orderlessTestAccessesArray[i].has_test_access}
                // aria-labelledby="text-spacing"
                height={this.state.switchHeight}
                width={this.state.switchWidth}
              />
            </div>
          )
        });
      }
    }

    // updating rowsDefinition object with provided data and needed style
    rowsDefinition = {
      column_1_style: {},
      column_2_style: {},
      column_3_style: COMMON_STYLE.CENTERED_TEXT,
      data: data
    };

    // update needed props states
    this.props.updateOrderlessTestAccessesData(data);

    // saving results in state
    this.setState({
      orderlessTestAccesses: orderlessTestAccessesArray,
      rowsDefinition: rowsDefinition,
      numberOfPages: Math.ceil(response.count / this.props.orderlessTestAccessesPaginationPageSize),
      nextPageNumber: response.next_page_number,
      previousPageNumber: response.previous_page_number
    });
  };

  // populating all found active test permissions based on a search
  populateFoundOrderlessTestAccesses = () => {
    this.setState(
      { currentlyLoading: true },
      () => {
        // adding small delay to avoi calling that API multiple times
        setTimeout(() => {
          // if callingGetFoundOrderlessTestAccesses is set to false
          if (!this.state.callingGetFoundOrderlessTestAccesses) {
            // updating callingGetFoundOrderlessTestAccesses to true (while the API is being called)
            this.setState({ callingGetFoundOrderlessTestAccesses: true });
            const orderlessTestAccessesArray = [];
            this.props
              .getFoundOrderlessTestAccesses(
                this.props.orderlessTestAccessesKeyword,
                this.props.currentLanguage,
                this.props.orderlessTestAccessesPaginationPage,
                this.props.orderlessTestAccessesPaginationPageSize,
                this.props.selectedOrderlessTestAdministratorData.username_id
              )
              .then(response => {
                // there are no results found
                if (response[0] === "no results found") {
                  this.setState({
                    orderlessTestAccesses: [],
                    rowsDefinition: {},
                    numberOfPages: 1,
                    nextPageNumber: 0,
                    previousPageNumber: 0,
                    resultsFound: 0
                  });
                  // there is at least one result found
                } else {
                  this.populateOrderlessTestAccessesObject(orderlessTestAccessesArray, response);
                  this.setState({ resultsFound: response.count });
                }
              })
              .then(() => {
                this.setState(
                  {
                    currentlyLoading: false,
                    displayResultsFound: true,
                    callingGetFoundOrderlessTestAccesses: false
                  },
                  () => {
                    // if there is at least one result found
                    if (orderlessTestAccessesArray.length > 0) {
                      // focusing on results found label only if save confirmation popup is not displayed (avoiding focus bug)
                      if (!this.state.showSaveConfirmationPopup) {
                        // make sure that this element exsits to avoid any error
                        if (document.getElementById("orderless-test-accesses-results-found")) {
                          document.getElementById("orderless-test-accesses-results-found").focus();
                        }
                      }
                      // no results found
                    } else {
                      // focusing on no results found row
                      document.getElementById("orderless-test-accesses-no-data").focus();
                    }
                  }
                );
              });
          }
        });
      },
      100
    );
  };

  updateTestAccessesTable = index => {
    // initializing needed object and array for rowsDefinition props (needed for GenericTable component)
    let rowsDefinition = {};
    const data = [];
    let updatedSwitchState = null;
    // looping in response given
    for (let i = 0; i < this.state.orderlessTestAccesses.length; i++) {
      // if current selected test access
      if (i === index) {
        updatedSwitchState = this.state.orderlessTestAccesses[index].has_test_access;
      }
      const currentResult = this.state.orderlessTestAccesses[i];
      // pushing needed data in data array (needed for GenericTable component)
      data.push({
        column_1: `${currentResult.parent_code} - ${currentResult.test_code}`,
        column_2: `${currentResult[`test_desc_${this.props.currentLanguage}`]}`,
        column_3:
          i === index ? (
            <div>
              <label className="fa fa-spinner fa-spin">
                <FontAwesomeIcon icon={faSpinner} />
              </label>
            </div>
          ) : (
            <div>
              <Switch
                onChange={(checked, parent_code, test_code) =>
                  this.onSwitchChange(checked, currentResult.parent_code, currentResult.test_code)
                }
                checked={this.state.orderlessTestAccesses[i].has_test_access}
                disabled={true}
                // aria-labelledby="text-spacing"
                height={this.state.switchHeight}
                width={this.state.switchWidth}
              />
            </div>
          )
      });
    }

    // updating rowsDefinition object with provided data and needed style
    rowsDefinition = {
      column_1_style: {},
      column_2_style: {},
      column_3_style: COMMON_STYLE.CENTERED_TEXT,
      data: data
    };

    // saving results in state
    this.setState(
      {
        rowsDefinition: rowsDefinition
      },
      () => {
        const currentSelectedTestAccess = data[index];

        const parent_code = currentSelectedTestAccess.column_1.split(" - ")[0];
        const test_code = currentSelectedTestAccess.column_1.split(" - ")[1];

        const body = {
          parent_code: parent_code,
          test_code: test_code,
          ta_extended_profile_id:
            this.props.selectedOrderlessTestAdministratorData.ta_extended_profile_id,
          add_test_access: updatedSwitchState
        };
        // adding/deleting test access
        this.props.addDeleteOrderlessTestAccess(body).then(response => {
          // test access successfully added
          if (response.ok) {
            // looping in response given
            for (let i = 0; i < this.state.orderlessTestAccesses.length; i++) {
              const currentResult = this.state.orderlessTestAccesses[i];
              rowsDefinition.data[i].column_3 = (
                <div>
                  <Switch
                    onChange={(checked, parent_code, test_code) =>
                      this.onSwitchChange(
                        checked,
                        currentResult.parent_code,
                        currentResult.test_code
                      )
                    }
                    checked={this.state.orderlessTestAccesses[i].has_test_access}
                    // aria-labelledby="text-spacing"
                    height={this.state.switchHeight}
                    width={this.state.switchWidth}
                  />
                </div>
              );
            }
            this.setState({ rowsDefinition: rowsDefinition });
            // should never happen
          } else {
            throw new Error(
              "An error occurred during the add/delete orderless test access process"
            );
          }
        });
      }
    );
  };

  populateOrganisationsOptions = () => {
    const organisationsOptions = [];
    this.props.getOrganization().then(response => {
      for (let i = 0; i < response.body.length; i++) {
        // Interface is in English
        if (this.props.currentLanguage === LANGUAGES.english) {
          organisationsOptions.push({
            label: `${response.body[i].edesc} (${response.body[i].eabrv})`,
            value: response.body[i].dept_id
          });
          // Interface is in French
        } else {
          organisationsOptions.push({
            label: `${response.body[i].fdesc} (${response.body[i].fabrv})`,
            value: response.body[i].dept_id
          });
        }
      }

      // getting initial department
      const initialDepartment = organisationsOptions.filter(
        obj =>
          parseInt(obj.value) === this.props.selectedOrderlessTestAdministratorData.department_id
      )[0];

      // updating needed states
      this.setState({
        organisationsOptions: organisationsOptions,
        organisationSelectedOption: initialDepartment,
        isLoadingOrganisations: false
      });
    });
  };

  onSwitchChange = (checked, parent_code, test_code) => {
    // initializing copy of arrays
    const copyOfOrderlessTestAccesses = this.state.orderlessTestAccesses;

    // getting index of the current selected combined parent and test codes
    const index = this.state.orderlessTestAccesses.findIndex(
      obj => obj.parent_code === parent_code && obj.test_code === test_code
    );
    // updating the switch value
    copyOfOrderlessTestAccesses[index].has_test_access = checked;
    // updating needed states
    this.setState(
      {
        orderlessTestAccesses: copyOfOrderlessTestAccesses
      },
      () => {
        this.updateTestAccessesTable(index);
      }
    );
  };

  // getting switch dimensions and updating states
  getUpdatedSwitchDimensions = () => {
    // need this loading state, otherwise the switch diameter is not calculated on state change
    this.setState(
      {
        switchHeight: getSwitchTransformScale(this.props.accommodations.fontSize).height,
        switchWidth: getSwitchTransformScale(this.props.accommodations.fontSize).width
      },
      // TODO: Improve this call with only a re-render of the table (without calling the API again)
      () => {
        this.populateTestAccessesTable();
      }
    );
  };

  getSelectedOrganisationOption = option => {
    // making sure that the department has been changed (avoid calling the DB for no reason)
    if (this.state.organisationSelectedOption.value !== option.value) {
      this.setState(
        {
          organisationSelectedOption: option
        },
        () => {
          // updating TA's department
          const body = {
            test_administrator: this.props.selectedOrderlessTestAdministratorData.username_id,
            dept_id: option.value
          };
          this.props.updateOrderlessTestAdministratorDepartment(body).then(response => {
            // test access successfully added
            if (response.ok) {
              // creating copy of selectedOrderlessTestAdministratorData
              const copyOfSelectedOrderlessTestAdministratorData =
                this.props.selectedOrderlessTestAdministratorData;
              // updating the dept_id
              copyOfSelectedOrderlessTestAdministratorData.department_id = option.value;
              this.props.setSelectedOrderlesstestAdministratorState(
                copyOfSelectedOrderlessTestAdministratorData
              );
              // display save confirmation popup and update needed states
              this.setState({
                showSaveConfirmationPopup: true,
                organisationSelectedOption: option
              });
              // should never happen
            } else {
              throw new Error(
                "An error occurred during the add orderless test administrator(s) request process"
              );
            }
          });
        }
      );
    }
  };

  closeConfirmationPopup = () => {
    this.setState({ showSaveConfirmationPopup: false });
  };

  render() {
    const columnsDefinition = [
      {
        label:
          LOCALIZE.systemAdministrator.testAccesses.tabs.orderlessTestAccesses
            .selectedOrderlessTestAdministrator.testAccesses.table.column_1,
        style: {}
      },
      {
        label:
          LOCALIZE.systemAdministrator.testAccesses.tabs.orderlessTestAccesses
            .selectedOrderlessTestAdministrator.testAccesses.table.column_2,
        style: {}
      },
      {
        label:
          LOCALIZE.systemAdministrator.testAccesses.tabs.orderlessTestAccesses
            .selectedOrderlessTestAdministrator.testAccesses.table.column_3,
        style: { ...COMMON_STYLE.CENTERED_TEXT, ...styles.assignedColumnStyle }
      }
    ];

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <div>
        <div>
          <h2>
            {
              LOCALIZE.systemAdministrator.testAccesses.tabs.orderlessTestAccesses
                .selectedOrderlessTestAdministrator.testAccesses.departmentUpdateTitle
            }
          </h2>
          <Container>
            {this.state.isLoadingOrganisations && (
              <Row style={styles.fieldSeparator}>
                <Col className="text-center">
                  <label className="fa fa-spinner fa-spin">
                    <FontAwesomeIcon icon={faSpinner} />
                  </label>
                </Col>
              </Row>
            )}
            {!this.state.isLoadingOrganisations && (
              <>
                <Row style={styles.fieldSeparator}>
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={{ ...styles.labelContainer, ...styles.colCenteredLabel }}
                  >
                    <label
                      id="update-orderless-ta-department-label"
                      style={{ ...styles.label, ...styles.contentFlex }}
                    >
                      {
                        LOCALIZE.systemAdministrator.testAccesses.tabs.orderlessTestAccesses
                          .selectedOrderlessTestAdministrator.testAccesses.departmentLabel
                      }
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={{ ...styles.inputContainer, ...styles.colCentered }}
                  >
                    <DropdownSelect
                      idPrefix="update-orderless-ta-department"
                      isValid={this.state.isValidUsers}
                      ariaRequired={true}
                      ariaLabelledBy="update-orderless-ta-department-label"
                      hasPlaceholder={true}
                      options={this.state.organisationsOptions}
                      onChange={this.getSelectedOrganisationOption}
                      defaultValue={this.state.organisationSelectedOption}
                      isMulti={false}
                    />
                  </Col>
                </Row>
              </>
            )}
          </Container>
        </div>
        <div>
          <h2>
            {
              LOCALIZE.systemAdministrator.testAccesses.tabs.orderlessTestAccesses
                .selectedOrderlessTestAdministrator.testAccesses.title
            }
          </h2>
          <p>
            {LOCALIZE.formatString(
              LOCALIZE.systemAdministrator.testAccesses.tabs.orderlessTestAccesses
                .selectedOrderlessTestAdministrator.testAccesses.description,
              <span style={styles.boldText}>
                {this.props.selectedOrderlessTestAdministratorData.first_name}
              </span>,
              <span style={styles.boldText}>
                {this.props.selectedOrderlessTestAdministratorData.last_name}
              </span>
            )}
          </p>
        </div>
        <div>
          <SearchBarWithDisplayOptions
            idPrefix={"orderless-test-accesses"}
            currentlyLoading={this.state.currentlyLoading}
            updateSearchStates={this.props.updateSearchOrderlessTestAccessesStates}
            updatePageState={this.props.updateCurrentOrderlessTestAccessesPageState}
            paginationPageSize={Number(this.props.orderlessTestAccessesPaginationPageSize)}
            updatePaginationPageSize={this.props.updateOrderlessTestAccessesPageSizeState}
            data={this.state.orderlessTestAccesses}
            resultsFound={this.state.resultsFound}
          />
          <div>
            <GenericTable
              classnamePrefix="orderless-test-accesses"
              columnsDefinition={columnsDefinition}
              rowsDefinition={this.state.rowsDefinition}
              emptyTableMessage={
                LOCALIZE.systemAdministrator.testAccesses.tabs.orderlessTestAccesses
                  .selectedOrderlessTestAdministrator.testAccesses.table.noResultsFound
              }
              currentlyLoading={this.state.currentlyLoading}
            />
            <Pagination
              paginationContainerId={"orderless-test-accesses-pagination-pages-link"}
              pageCount={this.state.numberOfPages}
              paginationPage={this.props.orderlessTestAccessesPaginationPage}
              updatePaginationPageState={this.props.updateCurrentOrderlessTestAccessesPageState}
              firstTableRowId={"orderless-test-accesses-table-row-0"}
            />
          </div>
        </div>
        <PopupBox
          show={this.state.showSaveConfirmationPopup}
          title={
            LOCALIZE.systemAdministrator.testAccesses.tabs.orderlessTestAccesses
              .selectedOrderlessTestAdministrator.saveConfirmationPopup.title
          }
          handleClose={() => {}}
          size={"lg"}
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.success}
                title={LOCALIZE.commons.confirm}
                message={
                  <p>
                    {
                      LOCALIZE.systemAdministrator.testAccesses.tabs.orderlessTestAccesses
                        .selectedOrderlessTestAdministrator.saveConfirmationPopup
                        .systemMessageDescription
                    }
                  </p>
                }
              />
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.ok}
          rightButtonAction={() => this.closeConfirmationPopup()}
        />
      </div>
    );
  }
}

export { SelectedOrderlessTestAdministratorTestAccesses as unconnectedSelectedOrderlessTestAdministratorTestAccesses };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    selectedOrderlessTestAdministratorData:
      state.userPermissions.selectedOrderlessTestAdministratorData,
    orderlessTestAccessesPaginationPageSize:
      state.userPermissions.orderlessTestAccessesPaginationPageSize,
    orderlessTestAccessesPaginationPage: state.userPermissions.orderlessTestAccessesPaginationPage,
    orderlessTestAccessesKeyword: state.userPermissions.orderlessTestAccessesKeyword,
    orderlessTestAccessesActiveSearch: state.userPermissions.orderlessTestAccessesActiveSearch,
    triggerpopulateOrderlessTestAccesses:
      state.userPermissions.triggerpopulateOrderlessTestAccesses,
    orderlessTestAccessesData: state.userPermissions.orderlessTestAccessesData
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateCurrentOrderlessTestAccessesPageState,
      updateOrderlessTestAccessesPageSizeState,
      updateSearchOrderlessTestAccessesStates,
      getOrderlessTestAccesses,
      getFoundOrderlessTestAccesses,
      updateOrderlessTestAccessesData,
      getOrganization,
      addDeleteOrderlessTestAccess,
      updateOrderlessTestAdministratorDepartment,
      setSelectedOrderlesstestAdministratorState
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SelectedOrderlessTestAdministratorTestAccesses);
