import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import LOCALIZE from "../../../text_resources";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faBinoculars,
  faPlusCircle,
  faSpinner,
  faTimes,
  faTrashAlt
} from "@fortawesome/free-solid-svg-icons";
import StyledTooltip, { TYPE, EFFECT, TRIGGER_TYPE } from "../../authentication/StyledTooltip";
import {
  getUnassignedOrderlessTestAdministrators,
  getAllOrderlessTestAdministrators,
  getFoundOrderlessTestAdministrators,
  updateCurrentOrderlessTestAdministratorsPageState,
  updateOrderlessTestAdministratorsPageSizeState,
  updateSearchOrderlessTestAdministratorsStates,
  addOrderlessTestAdministrators,
  setSelectedOrderlesstestAdministratorState,
  deleteOrderlessTestAdministrator,
  updateTriggerPopulateOrderlessTestAdministratorsState
} from "../../../modules/PermissionsRedux";
import "../../../css/etta.css";
import GenericTable, { COMMON_STYLE } from "../../commons/GenericTable";
import CustomButton, { THEME } from "../../commons/CustomButton";
import { getLineSpacingCSS } from "../../../modules/AccommodationsRedux";
import SearchBarWithDisplayOptions from "../../commons/SearchBarWithDisplayOptions";
import Pagination from "../../commons/Pagination";
import PopupBox, { BUTTON_STATE, BUTTON_TYPE } from "../../commons/PopupBox";
import { Col, Container, Row } from "react-bootstrap";
import DropdownSelect from "../../commons/DropdownSelect";
import { getOrganization } from "../../../modules/ExtendedProfileOptionsRedux";
import { LANGUAGES } from "../../commons/Translation";
import SystemMessage, { MESSAGE_TYPE } from "../../commons/SystemMessage";
import { history } from "../../../store-index";
import { PATH } from "../../commons/Constants";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 12,
    xl: 6
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 12,
    xl: 6
  }
};

const styles = {
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  buttonIcon: {
    marginRight: 0
  },
  testAdminRow: {
    padding: "6px 6px 6px 12px"
  },
  addButtonContainer: {
    marginTop: 18
  },
  buttonLabel: {
    margin: 0,
    paddingLeft: 6
  },
  labelContainer: {
    display: "table-cell",
    verticalAlign: "middle"
  },
  label: {
    overflowWrap: "break-word",
    margin: "0 12px 0 0"
  },
  inputContainer: {
    display: "table-cell",
    width: "100%",
    verticalAlign: "middle"
  },
  fieldSeparator: {
    marginTop: 18
  },
  colCenteredLabel: {
    display: "flex",
    alignItems: "center",
    width: "100%"
  },
  colCentered: {
    alignItems: "center",
    width: "100%"
  },
  contentFlex: {
    flex: "0 0 100%"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    margin: "4px 0 0 4px"
  },
  allUnset: {
    all: "unset"
  },
  boldText: {
    fontWeight: "bold"
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  popupDescriptionContainer: {
    marginTop: 12
  }
};

class OrderlessTestAccesses extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  static propTypes = {
    // provided by redux
    getAllOrderlessTestAdministrators: PropTypes.func,
    getFoundOrderlessTestAdministrators: PropTypes.func,
    updateCurrentOrderlessTestAdministratorsPageState: PropTypes.func,
    updateOrderlessTestAdministratorsPageSizeState: PropTypes.func,
    updateSearchOrderlessTestAdministratorsStates: PropTypes.func,
    updateTestPermission: PropTypes.func
  };

  state = {
    displayResultsFound: false,
    searchBarContent: "",
    resultsFound: 0,
    clearSearchTriggered: false,
    displayOptionsArray: [],
    displayOptionSelectedValue: {
      label: `${this.props.orderlessTestAdministratorsPaginationPageSize}`,
      value: this.props.orderlessTestAdministratorsPaginationPageSize
    },
    orderlessTestAdministrators: [],
    rowsDefinition: {},
    numberOfPages: 1,
    nextPageNumber: 0,
    previousPageNumber: 0,
    currentlyLoading: false,
    showAddOrderlessTaPopup: false,
    popupDataCurrentlyLoading: false,
    testAdministratorsOptions: [],
    usersSelectedOptions: [],
    currentSelectedOptionsAccessibility: LOCALIZE.commons.none,
    organisationsOptions: [],
    organisationSelectedOption: [],
    isValidUsers: true,
    invalid_user_complementary_message: "",
    addOrderlessPopupRequestSuccessful: false,
    showConfirmDeleteOrderlessTaPopup: false,
    selectedOrderlessTaToDelete: "",
    deleteOrderlessTaPopupLoading: false
  };

  componentDidMount = () => {
    // initialize pagination page redux state
    this.props.updateCurrentOrderlessTestAdministratorsPageState(1);
    // if page size is not defined, initialize it
    if (typeof this.props.orderlessTestAdministratorsPaginationPageSize === "undefined") {
      this.props.updateOrderlessTestAdministratorsPageSizeState(25);
    }
    // initialize active search redux state
    this.props.updateSearchOrderlessTestAdministratorsStates("", false);
    this.populateOrderlessTestAdministrators();
    this.populateTestAdministratorsOptions();
    this.populateOrganisationsOptions();
  };

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentDidUpdate = prevProps => {
    // if currentLanguage gets updated
    if (prevProps.currentLanguage !== this.props.currentLanguage) {
      this.populateOrderlessTestAdministratorsBasedOnActiveSearch();
    }
    // if orderlessTestAdministratorsPaginationPage gets updated
    if (
      prevProps.orderlessTestAdministratorsPaginationPage !==
      this.props.orderlessTestAdministratorsPaginationPage
    ) {
      this.populateOrderlessTestAdministratorsBasedOnActiveSearch();
    }
    // if orderlessTestAdministratorsPaginationPageSize get updated
    if (
      prevProps.orderlessTestAdministratorsPaginationPageSize !==
      this.props.orderlessTestAdministratorsPaginationPageSize
    ) {
      this.populateOrderlessTestAdministratorsBasedOnActiveSearch();
    }
    // if search orderlessTestAdministratorsKeyword gets updated
    if (
      prevProps.orderlessTestAdministratorsKeyword !== this.props.orderlessTestAdministratorsKeyword
    ) {
      // if orderlessTestAdministratorsKeyword redux state is empty
      if (this.props.orderlessTestAdministratorsKeyword !== "") {
        this.populateFoundOrderlessTestAdministrators();
      } else if (
        this.props.orderlessTestAdministratorsKeyword === "" &&
        this.props.orderlessTestAdministratorsActiveSearch
      ) {
        this.populateFoundOrderlessTestAdministrators();
      }
    }
    // if orderlessTestAdministratorsActiveSearch gets updated
    if (
      prevProps.orderlessTestAdministratorsActiveSearch !==
      this.props.orderlessTestAdministratorsActiveSearch
    ) {
      // there is no current active search (on clear search action)
      if (!this.props.orderlessTestAdministratorsActiveSearch) {
        // triggering clear search action state
        this.setState({ clearSearchTriggered: true }, () => {
          this.populateOrderlessTestAdministrators();
        });
        // there is a current active search (on search action)
      } else {
        this.populateFoundOrderlessTestAdministrators();
      }
    }
    // if triggerpopulateOrderlessTestAdministrators gets updated
    if (
      prevProps.triggerpopulateOrderlessTestAdministrators !==
      this.props.triggerpopulateOrderlessTestAdministrators
    ) {
      this.populateOrderlessTestAdministratorsBasedOnActiveSearch();
    }
  };

  // update searchBarContent content
  updateSearchBarContent = event => {
    const searchBarContent = event.target.value;
    this.setState({
      searchBarContent: searchBarContent
    });
  };

  // populating orderless test permissions
  populateOrderlessTestAdministrators = () => {
    this._isMounted = true;
    this.setState({ currentlyLoading: true }, () => {
      const orderlessTestAdministratorsArray = [];
      this.props
        .getAllOrderlessTestAdministrators(
          this.props.orderlessTestAdministratorsPaginationPage,
          this.props.orderlessTestAdministratorsPaginationPageSize
        )
        .then(response => {
          if (this._isMounted) {
            this.populateOrderlessTestAdministratorsObject(
              orderlessTestAdministratorsArray,
              response
            );
          }
        })
        .then(() => {
          if (this._isMounted) {
            if (this.state.clearSearchTriggered) {
              // go back to the first page to avoid display bugs
              this.props.updateCurrentOrderlessTestAdministratorsPageState(1);
              this.setState({ clearSearchTriggered: false });
            }
          }
        })
        .then(() => {
          if (this._isMounted) {
            this.setState({ currentlyLoading: false });
          }
        });
    });
  };

  // populating all found active test permissions based on a search
  populateFoundOrderlessTestAdministrators = () => {
    this.setState({ currentlyLoading: true }, () => {
      const orderlessTestAdministratorsArray = [];
      this.props
        .getFoundOrderlessTestAdministrators(
          this.props.orderlessTestAdministratorsKeyword,
          this.props.currentLanguage,
          this.props.orderlessTestAdministratorsPaginationPage,
          this.props.orderlessTestAdministratorsPaginationPageSize
        )
        .then(response => {
          // there are no results found
          if (response[0] === "no results found") {
            this.setState({
              orderlessTestAdministrators: [],
              rowsDefinition: {},
              numberOfPages: 1,
              nextPageNumber: 0,
              previousPageNumber: 0,
              resultsFound: 0
            });
            // there is at least one result found
          } else {
            this.populateOrderlessTestAdministratorsObject(
              orderlessTestAdministratorsArray,
              response
            );
            this.setState({ resultsFound: response.count });
          }
        })
        .then(() => {
          this.setState({ currentlyLoading: false, displayResultsFound: true }, () => {
            // if there is at least one result found
            if (orderlessTestAdministratorsArray.length > 0) {
              // focusing on results found label only if save confirmation popup is not displayed (avoiding focus bug)
              // make sure that this element exsits to avoid any error
              if (document.getElementById("orderless-test-administrators-results-found")) {
                document.getElementById("orderless-test-administrators-results-found").focus();
              }
              // no results found
            } else {
              // focusing on no results found row
              document.getElementById("orderless-test-administrators-no-data").focus();
            }
          });
        });
    });
  };

  populateOrderlessTestAdministratorsObject = (orderlessTestAdministratorsArray, response) => {
    // initializing needed object and array for rowsDefinition props (needed for GenericTable component)
    let rowsDefinition = {};
    const data = [];
    // making sure that there are some results
    if (typeof response.results !== "undefined") {
      // looping in response given
      for (let i = 0; i < response.results.length; i++) {
        const currentResult = response.results[i];
        // pushing needed data in orderlessTestAdministrators array
        orderlessTestAdministratorsArray.push({
          id: currentResult.id,
          dept_id: currentResult.dept_id,
          abrv_en: currentResult.abrv_en,
          abrv_fr: currentResult.abrv_fr,
          desc_en: currentResult.desc_en,
          desc_fr: currentResult.desc_fr,
          username_id: currentResult.username,
          first_name: currentResult.first_name,
          last_name: currentResult.last_name,
          email: currentResult.email
        });
        // pushing needed data in data array (needed for GenericTable component)
        data.push({
          column_1: `${currentResult.last_name}, ${currentResult.first_name} (${currentResult.email})`,
          column_2: `${currentResult[`desc_${this.props.currentLanguage}`]} (${
            currentResult[`abrv_${this.props.currentLanguage}`]
          })`,
          column_3: (
            <div className="d-flex justify-content-center flex-nowrap">
              <StyledTooltip
                id={`orderless-test-permissions-view-button-${i}`}
                place="top"
                type={TYPE.light}
                effect={EFFECT.solid}
                triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      dataTip=""
                      dataFor={`orderless-test-permissions-view-button-${i}`}
                      label={
                        <>
                          <FontAwesomeIcon icon={faBinoculars} style={styles.buttonIcon} />
                        </>
                      }
                      action={() => this.handleViewSelectedOrderlessTestPermission(i)}
                      customStyle={styles.actionButton}
                      buttonTheme={THEME.SECONDARY}
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>
                      {
                        LOCALIZE.systemAdministrator.testAccesses.tabs.orderlessTestAccesses.table
                          .viewTooltip
                      }
                    </p>
                  </div>
                }
              />
              <StyledTooltip
                id={`orderless-test-permissions-delete-button-${i}`}
                place="top"
                type={TYPE.light}
                effect={EFFECT.solid}
                triggerType={[TRIGGER_TYPE.hover, TRIGGER_TYPE.focus]}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      dataTip=""
                      dataFor={`orderless-test-permissions-delete-button-${i}`}
                      label={
                        <>
                          <FontAwesomeIcon icon={faTrashAlt} style={styles.buttonIcon} />
                        </>
                      }
                      action={() => this.openConfirmDeleteOrderlessTAPopup(i)}
                      customStyle={styles.actionButton}
                      buttonTheme={THEME.SECONDARY}
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>
                      {
                        LOCALIZE.systemAdministrator.testAccesses.tabs.orderlessTestAccesses.table
                          .deleteTooltip
                      }
                    </p>
                  </div>
                }
              />
            </div>
          )
        });
      }
    }

    // updating rowsDefinition object with provided data and needed style
    rowsDefinition = {
      column_1_style: styles.testAdminRow,
      column_2_style: COMMON_STYLE.LEFT_TEXT,
      column_3_style: COMMON_STYLE.CENTERED_TEXT,
      data: data
    };

    // saving results in state
    this.setState({
      orderlessTestAdministrators: orderlessTestAdministratorsArray,
      rowsDefinition: rowsDefinition,
      numberOfPages: Math.ceil(
        response.count / this.props.orderlessTestAdministratorsPaginationPageSize
      ),
      nextPageNumber: response.next_page_number,
      previousPageNumber: response.previous_page_number
    });
  };

  // populating active test permissions based on the orderlessTestAdministratorsActiveSearch redux state
  populateOrderlessTestAdministratorsBasedOnActiveSearch = () => {
    // current search
    if (this.props.orderlessTestAdministratorsActiveSearch) {
      this.populateFoundOrderlessTestAdministrators();
      // no current search
    } else {
      this.populateOrderlessTestAdministrators();
    }
  };

  // populate test administrators options (all users that have test administrators permission/role)
  populateTestAdministratorsOptions = () => {
    this.setState({ popupDataCurrentlyLoading: true }, () => {
      const testAdministratorsOptions = [];
      this.props
        .getUnassignedOrderlessTestAdministrators()
        .then(response => {
          for (let i = 0; i < response.length; i++) {
            testAdministratorsOptions.push({
              label: `${response[i].last_name}, ${response[i].first_name} - ${response[i].pri_or_military_nbr}`,
              value: `${response[i].user}`
            });
          }
        })
        .then(() => {
          this.setState({
            testAdministratorsOptions: testAdministratorsOptions,
            popupDataCurrentlyLoading: false
          });
        });
    });
  };

  populateOrganisationsOptions = () => {
    const organisationsOptions = [];
    this.props.getOrganization().then(response => {
      for (let i = 0; i < response.body.length; i++) {
        // Interface is in English
        if (this.props.currentLanguage === LANGUAGES.english) {
          organisationsOptions.push({
            label: `${response.body[i].edesc} (${response.body[i].eabrv})`,
            value: `${response.body[i].dept_id}`
          });
          // Interface is in French
        } else {
          organisationsOptions.push({
            label: `${response.body[i].fdesc} (${response.body[i].fabrv})`,
            value: `${response.body[i].dept_id}`
          });
        }
      }
      this.setState({ organisationsOptions: organisationsOptions });
    });
  };

  getSelectedOrganisationOption = option => {
    this.setState({
      organisationSelectedOption: option
    });
  };

  handleAddNewOrderlessTestAdministrator = () => {
    this.setState({ showAddOrderlessTaPopup: true });
  };

  closeAddOrderlessTaPopup = () => {
    this.setState({
      showAddOrderlessTaPopup: false,
      usersSelectedOptions: [],
      currentSelectedOptionsAccessibility: LOCALIZE.commons.none,
      organisationSelectedOption: [],
      isValidUsers: true,
      invalid_user_complementary_message: "",
      addOrderlessPopupRequestSuccessful: false
    });
  };

  handleAddOrderlessTas = () => {
    // formatting the users selected options
    const final_usersSelectedOptions = [];
    for (let i = 0; i < this.state.usersSelectedOptions.length; i++) {
      final_usersSelectedOptions.push(this.state.usersSelectedOptions[i].value);
    }

    const body = {
      test_administrators: final_usersSelectedOptions,
      department: this.state.organisationSelectedOption.value
    };

    // adding orderless test administrators
    this.props.addOrderlessTestAdministrators(body).then(response => {
      // if there is a validation error
      if (response.validation_error) {
        // getting the label of the invalid user (based on provided username_id)
        let invalid_user = "";
        // looping in usersSelectedOptions to get the label of the invalid user
        for (let i = 0; i < this.state.usersSelectedOptions.length; i++) {
          if (this.state.usersSelectedOptions[i].value === response.validation_error) {
            invalid_user = this.state.usersSelectedOptions[i].label;
          }
        }
        this.setState({ isValidUsers: false, invalid_user_complementary_message: invalid_user });
        // successful request
      } else if (response.ok) {
        this.setState(
          {
            isValidUsers: true,
            invalid_user_complementary_message: "",
            addOrderlessPopupRequestSuccessful: true
          },
          () => {
            // updating test administrators table
            this.populateOrderlessTestAdministratorsBasedOnActiveSearch();
            // updating the test administrator options
            this.populateTestAdministratorsOptions();
          }
        );
        // should never happen
      } else {
        throw new Error(
          "An error occurred during the add orderless test administrator(s) request process"
        );
      }
    });
  };

  // get selected users options
  getSelectedUsersOptions = selectedOption => {
    let option = null;
    if (selectedOption === null) {
      option = [];
    } else {
      option = selectedOption;
    }
    this.setState({
      usersSelectedOptions: option
    });
    this.getSelectedUsersOptionsAccessibility(option);
  };

  // getting/formatting current selected users options (for accessibility only)
  getSelectedUsersOptionsAccessibility = options => {
    let currentOptions = "";
    if (options.length > 0) {
      for (let i = 0; i < options.length; i++) {
        currentOptions += `${options[i].label}, `;
      }
    } else {
      currentOptions = LOCALIZE.commons.none;
    }
    this.setState({ currentSelectedOptionsAccessibility: currentOptions });
  };

  // handling view selected orderless test permission
  handleViewSelectedOrderlessTestPermission = index => {
    // setting selected orderless test administrator data
    const orderlessTestAdministratorData = {
      ta_extended_profile_id: this.state.orderlessTestAdministrators[index].id,
      username_id: this.state.orderlessTestAdministrators[index].username_id,
      first_name: this.state.orderlessTestAdministrators[index].first_name,
      last_name: this.state.orderlessTestAdministrators[index].last_name,
      email: this.state.orderlessTestAdministrators[index].email,
      department_id: this.state.orderlessTestAdministrators[index].dept_id,
      department_en: this.state.orderlessTestAdministrators[index].desc_en,
      department_fr: this.state.orderlessTestAdministrators[index].desc_fr
    };
    this.props.setSelectedOrderlesstestAdministratorState(orderlessTestAdministratorData);
    history.push(PATH.orderlessTestAdministratorDetails);
  };

  // handling delete selected orderless test permissions group
  handleDeleteSelectedOrderlessTestAdministrator = () => {
    this.setState({ deleteOrderlessTaPopupLoading: true }, () => {
      const body = {
        test_administrator: this.state.selectedOrderlessTaToDelete
      };
      // deleting TA with all related test accesses
      this.props.deleteOrderlessTestAdministrator(body).then(response => {
        // successfully deleted
        if (response.ok) {
          this.setState(
            {
              showConfirmDeleteOrderlessTaPopup: false,
              test_administrator: "",
              deleteOrderlessTaPopupLoading: false
            },
            () => {
              // updating test administrators table
              this.populateOrderlessTestAdministratorsBasedOnActiveSearch();
              // updating the test administrator options
              this.populateTestAdministratorsOptions();
            }
          );
          // should never happen
        } else {
          throw new Error(
            "An error occurred during the delete orderless test administrator request process"
          );
        }
      });
    });
  };

  openConfirmDeleteOrderlessTAPopup = index => {
    const selectedOrderlessTaToDelete = this.state.orderlessTestAdministrators[index].username_id;
    this.setState({
      showConfirmDeleteOrderlessTaPopup: true,
      selectedOrderlessTaToDelete: selectedOrderlessTaToDelete
    });
  };

  closeConfirmDeleteOrderlessTAPopup = () => {
    this.setState({
      showConfirmDeleteOrderlessTaPopup: false,
      selectedOrderlessTaToDelete: ""
    });
  };

  render() {
    const columnsDefinition = [
      {
        label: LOCALIZE.systemAdministrator.testAccesses.tabs.orderlessTestAccesses.table.column_1,
        style: {}
      },
      {
        label: LOCALIZE.systemAdministrator.testAccesses.tabs.orderlessTestAccesses.table.column_2,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label: LOCALIZE.systemAdministrator.testAccesses.tabs.orderlessTestAccesses.table.column_3,
        style: COMMON_STYLE.CENTERED_TEXT
      }
    ];

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    // setting the popup overflow visible variable
    let popupOverflowVisible = true;

    // if spacing is enabled
    if (this.props.accommodations.spacing) {
      popupOverflowVisible = false;
      // if font size is greater than 35px
    } else if (parseInt(this.props.accommodations.fontSize.split("px")[0]) > 29) {
      popupOverflowVisible = false;
      // if test order number, test to administer and test session language are defined/selected
    }

    return (
      <div>
        <Row className="align-items-center justify-content-start" style={styles.addButtonContainer}>
          <Col xl={"auto"} lg={"auto"} md={"auto"} sm={"auto"} xs={"auto"}>
            <CustomButton
              label={
                <>
                  {this.state.popupDataCurrentlyLoading && (
                    // eslint-disable-next-line jsx-a11y/label-has-associated-control
                    <label className="fa fa-spinner fa-spin">
                      <FontAwesomeIcon icon={faSpinner} />
                    </label>
                  )}
                  {!this.state.popupDataCurrentlyLoading && (
                    <>
                      <FontAwesomeIcon icon={faPlusCircle} />
                      <span id="add-orderless-test-administrator-button" style={styles.buttonLabel}>
                        {
                          LOCALIZE.systemAdministrator.testAccesses.tabs.orderlessTestAccesses
                            .addNewOrderlessTaButton
                        }
                      </span>
                    </>
                  )}
                </>
              }
              disabled={this.state.popupDataCurrentlyLoading}
              action={() => this.handleAddNewOrderlessTestAdministrator()}
              buttonTheme={THEME.PRIMARY}
            />
          </Col>
        </Row>
        <SearchBarWithDisplayOptions
          idPrefix={"orderless-test-administrators"}
          currentlyLoading={this.state.currentlyLoading}
          updateSearchStates={this.props.updateSearchOrderlessTestAdministratorsStates}
          updatePageState={this.props.updateCurrentOrderlessTestAdministratorsPageState}
          paginationPageSize={Number(this.props.orderlessTestAdministratorsPaginationPageSize)}
          updatePaginationPageSize={this.props.updateOrderlessTestAdministratorsPageSizeState}
          data={this.state.orderlessTestAdministrators}
          resultsFound={this.state.resultsFound}
        />
        <div>
          <GenericTable
            classnamePrefix="orderless-test-administrators"
            columnsDefinition={columnsDefinition}
            rowsDefinition={this.state.rowsDefinition}
            emptyTableMessage={LOCALIZE.SearchBarWithDisplayOptions.noResultsFound}
            currentlyLoading={this.state.currentlyLoading}
          />
          <Pagination
            paginationContainerId={"orderless-test-administrators-pagination-pages-link"}
            pageCount={this.state.numberOfPages}
            paginationPage={this.props.orderlessTestAdministratorsPaginationPage}
            updatePaginationPageState={this.props.updateCurrentOrderlessTestAdministratorsPageState}
            firstTableRowId={"orderless-test-administrators-table-row-0"}
          />
        </div>
        <PopupBox
          show={this.state.showAddOrderlessTaPopup}
          handleClose={() => {}}
          isBackdropStatic={true}
          shouldCloseOnEsc={false}
          overflowVisible={popupOverflowVisible}
          title={
            LOCALIZE.systemAdministrator.testAccesses.tabs.orderlessTestAccesses.addOrderlessTaPopup
              .title
          }
          size={parseInt(this.props.accommodations.fontSize.split("px")[0]) > 35 ? "xl" : "lg"}
          description={
            <div>
              {!this.state.addOrderlessPopupRequestSuccessful && (
                <Container>
                  <Row>
                    <p>
                      {
                        LOCALIZE.systemAdministrator.testAccesses.tabs.orderlessTestAccesses
                          .addOrderlessTaPopup.description
                      }
                    </p>
                  </Row>
                  <Row style={styles.fieldSeparator}>
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                      style={{ ...styles.labelContainer, ...styles.colCenteredLabel }}
                    >
                      <label
                        id="add-orderless-ta-popup-tas-label"
                        style={{ ...styles.label, ...styles.contentFlex }}
                      >
                        {
                          LOCALIZE.systemAdministrator.testAccesses.tabs.orderlessTestAccesses
                            .addOrderlessTaPopup.testAdministrators
                        }
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                      style={{ ...styles.inputContainer, ...styles.colCentered }}
                    >
                      <DropdownSelect
                        idPrefix="add-orderless-ta-popup-test-administrators"
                        isValid={this.state.isValidUsers}
                        ariaRequired={true}
                        ariaLabelledBy="add-orderless-ta-popup-tas-label add-orderless-ta-popup-users-current-value-accessibility add-orderless-ta-popup-test-administrators-error"
                        hasPlaceholder={true}
                        options={this.state.testAdministratorsOptions}
                        onChange={this.getSelectedUsersOptions}
                        defaultValue={this.state.usersSelectedOptions}
                        isMulti={true}
                      />
                      <label
                        id="add-orderless-ta-popup-users-current-value-accessibility"
                        style={styles.hiddenText}
                      >{`${this.state.currentSelectedOptionsAccessibility}`}</label>
                    </Col>
                  </Row>
                  {!this.state.isValidUsers && (
                    <Row className="justify-content-end">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                        style={{ ...styles.colCentered }}
                      >
                        <label
                          id="add-orderless-ta-popup-test-administrators-error"
                          htmlFor="add-orderless-ta-popup-test-administrators"
                          style={{ ...styles.errorMessage, ...styles.contentFlex }}
                        >
                          {LOCALIZE.formatString(
                            LOCALIZE.systemAdministrator.testAccesses.tabs.orderlessTestAccesses
                              .addOrderlessTaPopup.invalidTestAdministrators,
                            this.state.invalid_user_complementary_message
                          )}
                        </label>
                      </Col>
                    </Row>
                  )}
                  <Row style={styles.fieldSeparator}>
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                      style={{ ...styles.labelContainer, ...styles.colCenteredLabel }}
                    >
                      <label
                        id="add-orderless-ta-popup-department-ministry-label"
                        style={{ ...styles.label, ...styles.contentFlex }}
                      >
                        {
                          LOCALIZE.systemAdministrator.testAccesses.tabs.orderlessTestAccesses
                            .addOrderlessTaPopup.departmentMinistry
                        }
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                      style={{ ...styles.inputContainer, ...styles.colCentered }}
                    >
                      <DropdownSelect
                        idPrefix="add-orderless-ta-popup-department-ministry"
                        isValid={true}
                        ariaRequired={true}
                        ariaLabelledBy="add-orderless-ta-popup-department-ministry-label"
                        hasPlaceholder={true}
                        options={this.state.organisationsOptions}
                        onChange={this.getSelectedOrganisationOption}
                        defaultValue={this.state.organisationSelectedOption}
                        isMulti={false}
                        menuPlacement={"top"}
                      />
                    </Col>
                  </Row>
                </Container>
              )}
              {this.state.addOrderlessPopupRequestSuccessful && (
                <SystemMessage
                  messageType={MESSAGE_TYPE.success}
                  title={LOCALIZE.commons.success}
                  message={
                    <p>
                      {
                        LOCALIZE.systemAdministrator.testAccesses.tabs.orderlessTestAccesses
                          .addOrderlessTaPopup.successfulMessage
                      }
                    </p>
                  }
                />
              )}
            </div>
          }
          leftButtonType={
            !this.state.addOrderlessPopupRequestSuccessful
              ? BUTTON_TYPE.secondary
              : BUTTON_TYPE.none
          }
          leftButtonIcon={faTimes}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonLabel={LOCALIZE.commons.cancel}
          leftButtonAction={this.closeAddOrderlessTaPopup}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonIcon={!this.state.addOrderlessPopupRequestSuccessful ? faPlusCircle : ""}
          rightButtonTitle={
            !this.state.addOrderlessPopupRequestSuccessful
              ? LOCALIZE.commons.saved
              : LOCALIZE.commons.close
          }
          rightButtonLabel={
            !this.state.addOrderlessPopupRequestSuccessful
              ? LOCALIZE.commons.saved
              : LOCALIZE.commons.close
          }
          rightButtonAction={
            !this.state.addOrderlessPopupRequestSuccessful
              ? this.handleAddOrderlessTas
              : this.closeAddOrderlessTaPopup
          }
          rightButtonState={
            this.state.addOrderlessPopupRequestSuccessful
              ? BUTTON_STATE.enabled
              : this.state.usersSelectedOptions.length <= 0 ||
                this.state.organisationSelectedOption.length <= 0
              ? BUTTON_STATE.disabled
              : BUTTON_STATE.enabled
          }
        />
        <PopupBox
          show={this.state.showConfirmDeleteOrderlessTaPopup}
          title={
            LOCALIZE.systemAdministrator.testAccesses.tabs.orderlessTestAccesses
              .confirmDeleteOrderlessTaPopup.title
          }
          handleClose={() => {}}
          size={"lg"}
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.error}
                title={LOCALIZE.commons.warning}
                message={
                  <p>
                    {LOCALIZE.formatString(
                      LOCALIZE.systemAdministrator.testAccesses.tabs.orderlessTestAccesses
                        .confirmDeleteOrderlessTaPopup.systemMessageDescription,
                      <span style={styles.boldText}>{this.state.selectedOrderlessTaToDelete}</span>
                    )}
                  </p>
                }
              />
              <p style={styles.popupDescriptionContainer}>
                {
                  LOCALIZE.systemAdministrator.testAccesses.tabs.orderlessTestAccesses
                    .confirmDeleteOrderlessTaPopup.description
                }
              </p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          leftButtonAction={() => this.closeConfirmDeleteOrderlessTAPopup()}
          leftButtonState={
            !this.state.deleteOrderlessTaPopupLoading ? BUTTON_STATE.enabled : BUTTON_STATE.disabled
          }
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={
            !this.state.deleteOrderlessTaPopupLoading ? (
              LOCALIZE.commons.confirm
            ) : (
              // eslint-disable-next-line jsx-a11y/label-has-associated-control
              <label className="fa fa-spinner fa-spin">
                <FontAwesomeIcon icon={faSpinner} />
              </label>
            )
          }
          rightButtonIcon={!this.state.deleteOrderlessTaPopupLoading ? faTrashAlt : ""}
          rightButtonAction={() => this.handleDeleteSelectedOrderlessTestAdministrator()}
          rightButtonState={
            !this.state.deleteOrderlessTaPopupLoading ? BUTTON_STATE.enabled : BUTTON_STATE.disabled
          }
        />
      </div>
    );
  }
}

export { OrderlessTestAccesses as unconnectedOrderlessTestAccesses };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    orderlessTestAdministratorsPaginationPageSize:
      state.userPermissions.orderlessTestAdministratorsPaginationPageSize,
    orderlessTestAdministratorsPaginationPage:
      state.userPermissions.orderlessTestAdministratorsPaginationPage,
    orderlessTestAdministratorsKeyword: state.userPermissions.orderlessTestAdministratorsKeyword,
    orderlessTestAdministratorsActiveSearch:
      state.userPermissions.orderlessTestAdministratorsActiveSearch,
    triggerpopulateOrderlessTestAdministrators:
      state.userPermissions.triggerpopulateOrderlessTestAdministrators,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getUnassignedOrderlessTestAdministrators,
      getAllOrderlessTestAdministrators,
      getFoundOrderlessTestAdministrators,
      updateCurrentOrderlessTestAdministratorsPageState,
      updateOrderlessTestAdministratorsPageSizeState,
      updateSearchOrderlessTestAdministratorsStates,
      getOrganization,
      addOrderlessTestAdministrators,
      setSelectedOrderlesstestAdministratorState,
      deleteOrderlessTestAdministrator,
      updateTriggerPopulateOrderlessTestAdministratorsState
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(OrderlessTestAccesses);
