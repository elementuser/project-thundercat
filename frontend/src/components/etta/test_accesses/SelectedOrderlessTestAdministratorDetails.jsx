import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getLineSpacingCSS } from "../../../modules/AccommodationsRedux";
import LOCALIZE from "../../../text_resources";
import SelectedOrderlessTestAdministratorTestAccesses from "./SelectedOrderlessTestAdministratorTestAccesses";
import ContentContainer from "../../commons/ContentContainer";
import CustomButton, { THEME } from "../../commons/CustomButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowAltCircleLeft } from "@fortawesome/free-solid-svg-icons";
import SideNavigation from "../../eMIB/SideNavigation";
import { history } from "../../../store-index";
import { PATH } from "../../commons/Constants";

const styles = {
  sectionContainerLabelDiv: {
    paddingLeft: 18,
    marginBottom: "-3px"
  },
  backButtonStyle: {
    marginBottom: 24
  },
  sectionContainerLabel: {
    padding: "4px 24px",
    marginBottom: 0,
    position: "relative"
  },
  tabStyleBorder: {
    borderBottom: "5px solid #00565e",
    width: "100%",
    position: "absolute",
    bottom: 0,
    left: 0,
    borderRadius: "4px 4px 0 0"
  },
  sectionContainer: {
    borderStyle: "solid",
    borderWidth: "3px 1px 1px 1px",
    borderColor: "#CECECE",
    minHeight: 400,
    padding: "12px 0",
    marginBottom: 24
  },
  tabContainer: {
    zIndex: 1,
    padding: 0
  },
  tabContent: {
    backgroundColor: "transparent",
    height: "100%",
    padding: 5
  },
  nav: {
    marginTop: 10,
    marginLeft: 10,
    width: 206,
    textAlign: "center"
  },
  body: {
    paddingLeft: 12
  },
  sideNavBodyContent: {
    marginTop: "-12px"
  },
  buttonLabel: {
    marginLeft: 6
  }
};

class SelectedOrderlessTestAdministratorDetails extends Component {
  static propTypes = {
    // provided by redux
  };

  state = {};

  componentDidMount = () => {};

  goBackToOrderlessTestAdministratorSelection = () => {
    history.push(PATH.systemAdministration);
  };

  getSelectedOrderlessTestAdministratorSections = () => {
    return [
      {
        menuString:
          LOCALIZE.systemAdministrator.testAccesses.tabs.orderlessTestAccesses
            .selectedOrderlessTestAdministrator.sideNavItem1Title,
        body: <SelectedOrderlessTestAdministratorTestAccesses />
      }
    ];
  };

  render() {
    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    const specs = this.getSelectedOrderlessTestAdministratorSections();

    return (
      <div>
        <ContentContainer>
          <div id="main-content" role="main">
            <div style={styles.backButtonStyle}>
              <CustomButton
                label={
                  <>
                    <FontAwesomeIcon icon={faArrowAltCircleLeft} />
                    <span style={styles.buttonLabel}>
                      {
                        LOCALIZE.systemAdministrator.testAccesses.tabs.orderlessTestAccesses
                          .selectedOrderlessTestAdministrator
                          .backToOrderlessTestAdministratorSelectionButton
                      }
                    </span>
                  </>
                }
                action={this.goBackToOrderlessTestAdministratorSelection}
                buttonTheme={THEME.SECONDARY}
              />
            </div>
            <div>
              <div style={styles.sectionContainerLabelDiv}>
                <div>
                  <label style={styles.sectionContainerLabel}>
                    {LOCALIZE.formatString(
                      LOCALIZE.systemAdministrator.testAccesses.tabs.orderlessTestAccesses
                        .selectedOrderlessTestAdministrator.tabTitle,
                      this.props.selectedOrderlessTestAdministratorData.email
                    )}

                    <span style={styles.tabStyleBorder}></span>
                  </label>
                </div>
              </div>
              <div style={styles.sectionContainer}>
                <SideNavigation
                  specs={specs}
                  startIndex={0}
                  displayNextPreviousButton={false}
                  isMain={true}
                  tabContainerStyle={styles.tabContainer}
                  tabContentStyle={styles.tabContent}
                  navStyle={styles.nav}
                  bodyContentCustomStyle={styles.sideNavBodyContent}
                />
              </div>
            </div>
          </div>
        </ContentContainer>
      </div>
    );
  }
}

export { SelectedOrderlessTestAdministratorDetails as unconnectedSelectedOrderlessTestAdministratorDetails };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    selectedOrderlessTestAdministratorData:
      state.userPermissions.selectedOrderlessTestAdministratorData
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SelectedOrderlessTestAdministratorDetails);
