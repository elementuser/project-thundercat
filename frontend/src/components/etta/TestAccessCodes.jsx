import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes, faTrashAlt } from "@fortawesome/free-solid-svg-icons";

import GenericTable, { COMMON_STYLE } from "../commons/GenericTable";
import SearchBarWithDisplayOptions from "../commons/SearchBarWithDisplayOptions";
import Pagination from "../commons/Pagination";
import {
  getAllActiveTestAccessCodesRedux,
  getFoundActiveTestAccessCodes,
  updateCurrentTestAccessCodesPageState,
  updateTestAccessCodesPageSizeState,
  updateSearchActiveTestAccessCodesStates
} from "../../modules/TestAccessCodesRedux";
import { styles as ActivePermissionsStyles } from "./permissions/ActivePermissions";
import CustomButton, { THEME } from "../commons/CustomButton";
import SystemMessage, { MESSAGE_TYPE } from "../commons/SystemMessage";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import { deleteTestAccessCode } from "../../modules/TestAdministrationRedux";

const styles = {
  buttonIcon: {
    marginRight: 6
  }
};

class TestAccessCodes extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  static propTypes = {
    // props from Redux
    getAllActiveTestAccessCodesRedux: PropTypes.func,
    getFoundActiveTestAccessCodes: PropTypes.func,
    updateCurrentTestAccessCodesPageState: PropTypes.func,
    updateTestAccessCodesPageSizeState: PropTypes.func,
    updateSearchActiveTestAccessCodesStates: PropTypes.func,
    deleteTestAccessCode: PropTypes.func
  };

  state = {
    displayResultsFound: false,
    resultsFound: 0,
    clearSearchTriggered: false,
    activeTestAccessCodes: [],
    rowsDefinition: {},
    numberOfPages: 1,
    nextPageNumber: 0,
    previousPageNumber: 0,
    currentlyLoading: false,
    showDisableConfirmationPopup: false,
    selectedTestAccessCodeToDisable: {},
    callingGetFoundActiveTestAccessCodes: false
  };

  componentDidMount = () => {
    // initialize pagination page redux state
    this.props.updateCurrentTestAccessCodesPageState(1);
    // initialize active search redux state
    this.props.updateSearchActiveTestAccessCodesStates("", false);
    this.populateActiveTestAccessCodes();
  };

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentDidUpdate = prevProps => {
    // // if currentLanguage gets updated
    if (prevProps.currentLanguage !== this.props.currentLanguage) {
      this.populateActiveTestAccessCodesBasedOnActiveSearch();
    }
    // // if testAccessCodesPaginationPage gets updated
    if (prevProps.testAccessCodesPaginationPage !== this.props.testAccessCodesPaginationPage) {
      this.populateActiveTestAccessCodesBasedOnActiveSearch();
    }
    // // if testAccessCodesPaginationPageSize get updated
    if (
      prevProps.testAccessCodesPaginationPageSize !== this.props.testAccessCodesPaginationPageSize
    ) {
      this.populateActiveTestAccessCodesBasedOnActiveSearch();
    }
    // // if search testAccessCodesKeyword gets updated
    if (prevProps.testAccessCodeKeyword !== this.props.testAccessCodeKeyword) {
      // if testAccessCodesKeyword redux state is empty
      if (this.props.testAccessCodeKeyword !== "") {
        this.populateFoundActiveTestAccessCodes();
      } else if (this.props.testAccessCodeKeyword === "" && this.props.testAccessCodeActiveSearch) {
        this.populateFoundActiveTestAccessCodes();
      }
    }
    // // if testAccessCodesActiveSearch gets updated
    if (prevProps.testAccessCodeActiveSearch !== this.props.testAccessCodeActiveSearch) {
      // there is no current active search (on clear search action)
      if (!this.props.testAccessCodeActiveSearch) {
        // triggering clear search action state
        this.setState({ clearSearchTriggered: true }, () => {
          this.populateActiveTestAccessCodes();
        });
        // there is a current active search (on search action)
      } else {
        this.populateFoundActiveTestAccessCodes();
      }
    }
  };

  openDisableConfirmationPopup = id => {
    this.setState({
      showDisableConfirmationPopup: true,
      selectedTestAccessCodeToDisable: this.state.activeTestAccessCodes[id]
    });
  };

  closeDisableConfirmationPopup = () => {
    this.setState({ showDisableConfirmationPopup: false, selectedTestAccessCodeToDisable: {} });
  };

  // populate active test access codes
  populateActiveTestAccessCodes = () => {
    this._isMounted = true;
    this.setState({ currentlyLoading: true }, () => {
      const activeTestAccessCodesArray = [];
      this.props
        .getAllActiveTestAccessCodesRedux(
          this.props.testAccessCodesPaginationPage,
          this.props.testAccessCodesPaginationPageSize
        )
        .then(response => {
          if (this._isMounted) {
            this.populateActiveTestAccessCodeObject(activeTestAccessCodesArray, response);
          }
        })
        .then(() => {
          if (this._isMounted) {
            if (this.state.clearSearchTriggered) {
              // go back to the first page to avoid display bugs
              this.props.updateCurrentTestAccessCodesPageState(1);
              this.setState({ clearSearchTriggered: false });
            }
          }
        })
        .then(() => {
          if (this._isMounted) {
            this.setState({ currentlyLoading: false });
          }
        });
    });
  };

  // populating all found active test access codes based on a search
  populateFoundActiveTestAccessCodes = () => {
    this.setState({ currentlyLoading: true }, () => {
      // adding small delay to avoi calling that API multiple times
      setTimeout(() => {
        // if callingGetFoundActiveTestAccessCodes is set to false
        if (!this.state.callingGetFoundActiveTestAccessCodes) {
          // updating callingGetFoundActiveTestAccessCodes to true (while the API is being called)
          this.setState({ callingGetFoundActiveTestAccessCodes: true });
          const activeTestAccessCodesArray = [];
          this.props
            .getFoundActiveTestAccessCodes(
              this.props.currentLanguage,
              this.props.testAccessCodeKeyword,
              this.props.testAccessCodesPaginationPage,
              this.props.testAccessCodesPaginationPageSize
            )
            .then(response => {
              // there are no results found
              if (response[0] === "no results found") {
                this.setState({
                  activeTestAccessCodes: [],
                  rowsDefinition: {},
                  numberOfPages: 1,
                  nextPageNumber: 0,
                  previousPageNumber: 0,
                  resultsFound: 0
                });
                // there is at least one result found
              } else {
                this.populateActiveTestAccessCodeObject(activeTestAccessCodesArray, response);
                this.setState({ resultsFound: response.count });
              }
            })
            .then(() => {
              this.setState(
                {
                  currentlyLoading: false,
                  displayResultsFound: true,
                  callingGetFoundActiveTestAccessCodes: false
                },
                () => {
                  // if there is at least one result found
                  if (activeTestAccessCodesArray.length > 0) {
                    // make sure that this element exsits to avoid any error
                    if (document.getElementById("test-access-codes-results-found")) {
                      document.getElementById("test-access-codes-results-found").focus();
                    }
                    // no results found
                  } else {
                    // focusing on no results found row
                    document.getElementById("test-access-codes-no-data").focus();
                  }
                }
              );
            });
        }
      }, 100);
    });
  };

  populateActiveTestAccessCodeObject = (activeTestAccessCodesArray, response) => {
    // initializing needed object and array for rowsDefinition props (needed for GenericTable component)
    let rowsDefinition = {};
    const data = [];
    // making sure that there are some results
    if (typeof response.results !== "undefined") {
      // looping in response given
      for (let i = 0; i < response.results.length; i++) {
        const currentResult = response.results[i];
        // pushing needed data in activeTestAccessCodesarray
        activeTestAccessCodesArray.push({
          id: currentResult.id,
          test_access_code: currentResult.test_access_code,
          test_order_number: currentResult.test_order_number,
          reference_number: currentResult.reference_number,
          ta_username: currentResult.ta_username,
          test_id: currentResult.test_id,
          first_name: currentResult.ta_first_name,
          last_name: currentResult.ta_last_name,
          ta_goc_email: currentResult.ta_goc_email,
          en_test_name: currentResult.en_test_name,
          fr_test_name: currentResult.fr_test_name,
          created_date: currentResult.created_date
        });
        // pushing needed data in data array (needed for GenericTable component)
        data.push({
          column_1: currentResult.test_access_code,
          column_2: `${currentResult[`${this.props.currentLanguage}_test_name`]}`,
          column_3:
            currentResult.test_order_number !== null
              ? currentResult.test_order_number
              : `${currentResult.reference_number} (${LOCALIZE.reports.orderlessRequest})`,
          column_4: `${currentResult.ta_last_name}, ${currentResult.ta_first_name} (${currentResult.ta_goc_email})`,
          column_5: currentResult.created_date,
          column_6: (
            <CustomButton
              buttonId={`test-access-code-delete-row-${i}`}
              label={
                <>
                  <FontAwesomeIcon icon={faTrashAlt} style={styles.buttonIcon} />
                  <span style={styles.buttonLabel}>
                    {LOCALIZE.testAdministration.testAccessCodes.table.actionButton}
                  </span>
                </>
              }
              action={() => {
                this.openDisableConfirmationPopup(i);
              }}
              buttonTheme={THEME.SECONDARY}
              ariaLabel={LOCALIZE.formatString(
                LOCALIZE.systemAdministrator.testAccessCodes.table.deleteButtonAccessibility,
                currentResult.test_access_code
              )}
            />
          )
        });
      }
    }

    // updating rowsDefinition object with provided data and needed style
    rowsDefinition = {
      column_1_style: COMMON_STYLE.LEFT_TEXT,
      column_2_style: COMMON_STYLE.LEFT_TEXT,
      column_3_style: COMMON_STYLE.LEFT_TEXT,
      column_4_style: COMMON_STYLE.LEFT_TEXT,
      column_5_style: COMMON_STYLE.CENTERED_TEXT,
      column_6_style: COMMON_STYLE.CENTERED_TEXT,
      data: data
    };

    // saving results in state
    this.setState({
      activeTestAccessCodes: activeTestAccessCodesArray,
      rowsDefinition: rowsDefinition,
      numberOfPages: Math.ceil(response.count / this.props.testAccessCodesPaginationPageSize),
      nextPageNumber: response.next_page_number,
      previousPageNumber: response.previous_page_number
    });
  };

  populateActiveTestAccessCodesBasedOnActiveSearch = () => {
    // current search
    if (this.props.testAccessCodeActiveSearch) {
      this.populateFoundActiveTestAccessCodes();
      // no search
    } else {
      this.populateActiveTestAccessCodes();
    }
  };

  // handle disable selected test access code
  handleDisableTestAccessCode = () => {
    this.props
      .deleteTestAccessCode(this.state.selectedTestAccessCodeToDisable.test_access_code)
      .then(response => {
        // if request succeeded
        if (response.status === 200) {
          // re-populating active test access codes table
          this.populateActiveTestAccessCodes();
          // go back to the first page to avoid display bugs
          this.props.updateCurrentTestAccessCodesPageState(1);
          // close pop up
          this.closeDisableConfirmationPopup();
          // should never happen
        } else {
          throw new Error("Something went wrong during disable test access code process");
        }
      });
  };

  render() {
    const columnsDefinition = [
      {
        label: LOCALIZE.systemAdministrator.testAccessCodes.table.columnOne,
        style: { ...{ width: "15%" }, ...COMMON_STYLE.LEFT_TEXT }
      },
      {
        label: LOCALIZE.systemAdministrator.testAccessCodes.table.columnTwo,
        style: { ...{ width: "25%" }, ...COMMON_STYLE.LEFT_TEXT }
      },
      {
        label: LOCALIZE.systemAdministrator.testAccessCodes.table.columnThree,
        style: { ...{ width: "15%" }, ...COMMON_STYLE.LEFT_TEXT }
      },
      {
        label: LOCALIZE.systemAdministrator.testAccessCodes.table.columnFour,
        style: { ...{ width: "15%" }, ...COMMON_STYLE.LEFT_TEXT }
      },
      {
        label: LOCALIZE.systemAdministrator.testAccessCodes.table.columnFive,
        style: { ...{ width: "15%" }, ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label: LOCALIZE.systemAdministrator.testAccessCodes.table.columnSix,
        style: { ...{ width: "15%" }, ...COMMON_STYLE.CENTERED_TEXT }
      }
    ];

    return (
      <div>
        <h2>{LOCALIZE.systemAdministrator.sideNavItems.testAccessCodes}</h2>
        <p>{LOCALIZE.systemAdministrator.testAccessCodes.description}</p>
        <SearchBarWithDisplayOptions
          idPrefix={"test-access-codes"}
          currentlyLoading={this.state.currentlyLoading}
          updateSearchStates={this.props.updateSearchActiveTestAccessCodesStates}
          updatePageState={this.props.updateCurrentTestAccessCodesPageState}
          paginationPageSize={Number(this.props.testAccessCodesPaginationPageSize)}
          updatePaginationPageSize={this.props.updateTestAccessCodesPageSizeState}
          data={this.state.activeTestAccessCodes}
          resultsFound={this.state.resultsFound}
        />
        <div>
          <GenericTable
            classnamePrefix="test-access-codes"
            columnsDefinition={columnsDefinition}
            rowsDefinition={this.state.rowsDefinition}
            emptyTableMessage={LOCALIZE.systemAdministrator.testAccessCodes.table.noTestAccessCode}
            currentlyLoading={this.state.currentlyLoading}
          />
        </div>
        <Pagination
          paginationContainerId={"test-access-codes-pagination-pages-link"}
          pageCount={this.state.numberOfPages}
          paginationPage={this.props.testAccessCodesPaginationPage}
          updatePaginationPageState={this.props.updateCurrentTestAccessCodesPageState}
          firstTableRowId={"test-access-codes-table-row-0"}
        />
        <PopupBox
          show={this.state.showDisableConfirmationPopup}
          handleClose={() => {}}
          isBackdropStatic={false}
          shouldCloseOnEsc={true}
          title={
            LOCALIZE.testAdministration.testAccessCodes.disableTestAccessCodeConfirmationPopup.title
          }
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.error}
                title={LOCALIZE.commons.warning}
                message={
                  <p>
                    {LOCALIZE.formatString(
                      LOCALIZE.testAdministration.testAccessCodes
                        .disableTestAccessCodeConfirmationPopup.description,
                      <span style={ActivePermissionsStyles.boldText}>
                        {this.state.selectedTestAccessCodeToDisable.test_access_code}
                      </span>
                    )}
                  </p>
                }
              />
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeDisableConfirmationPopup}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={LOCALIZE.testAdministration.testAccessCodes.table.actionButton}
          rightButtonIcon={faTrashAlt}
          rightButtonAction={this.handleDisableTestAccessCode}
          size={"lg"}
        />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    testAccessCodesPaginationPageSize: state.testAccessCodes.testAccessCodesPaginationPageSize,
    testAccessCodesPaginationPage: state.testAccessCodes.testAccessCodesPaginationPage,
    testAccessCodeKeyword: state.testAccessCodes.testAccessCodeKeyword,
    testAccessCodeActiveSearch: state.testAccessCodes.testAccessCodeActiveSearch
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAllActiveTestAccessCodesRedux,
      getFoundActiveTestAccessCodes,
      updateCurrentTestAccessCodesPageState,
      updateTestAccessCodesPageSizeState,
      updateSearchActiveTestAccessCodesStates,
      deleteTestAccessCode
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(TestAccessCodes);
