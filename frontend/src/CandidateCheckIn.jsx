import React, { Component } from "react";
import LOCALIZE from "./text_resources";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import PopupBox, { BUTTON_TYPE } from "./components/commons/PopupBox";
import updateCheckInRoom from "./modules/UpdateCheckInRoomRedux";
import CustomButton, { THEME } from "./components/commons/CustomButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck, faTimes, faLink } from "@fortawesome/free-solid-svg-icons";
import { getLineSpacingCSS } from "./modules/AccommodationsRedux";
import { Col, Row } from "react-bootstrap";
import { PATH } from "./components/commons/Constants";
import SystemMessage, { MESSAGE_TYPE } from "./components/commons/SystemMessage";

const styles = {
  checkInBtnContainer: {
    textAlign: "center",
    marginTop: 80
  },
  checkInBtn: {
    minWidth: 250
  },
  textInput: {
    width: "75%",
    padding: "3px 6px 3px 6px",
    borderRadius: 4,
    minHeight: 32
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6,
    width: 250
  },
  popupWidth: {
    width: "100%"
  },
  buttonLabel: {
    margin: 0,
    paddingLeft: 6
  },
  row: {
    padding: "15px 40px 15px 40px"
  },
  uncompletedProfileDescription: {
    marginTop: 18
  },
  profileLink: {
    textDecoration: "underline",
    color: "#0278A4",
    cursor: "pointer",
    backgroundColor: "transparent",
    border: "none",
    padding: 0
  }
};

class CandidateCheckIn extends Component {
  static propTypes = {
    // Props from Redux
    updateCheckInRoom: PropTypes.func
  };

  state = {
    showCheckInPopup: false,
    testAccessCode: "",
    isInvalidTestAccessCode: false,
    testAccessCodeAlreadyCheckedIn: false,
    currentTestAlreadyInActiveState: false
  };

  handleCheckInPopup = () => {
    this.setState({ ...this.state, showCheckInPopup: true });
  };

  handleCheckin = () => {
    const { testAccessCode } = this.state;

    this.props.updateCheckInRoom(testAccessCode).then(response => {
      if (response.ok) {
        this.setState({
          ...this.state,
          showCheckInPopup: false,
          testAccessCode: "",
          isInvalidTestAccessCode: false,
          testAccessCodeAlreadyCheckedIn: false,
          currentTestAlreadyInActiveState: false
        });
        this.props.handleCheckIn(testAccessCode);
      } else if (
        response.error ===
        "This room has already been checked in using the provided test access code."
      ) {
        this.setState(
          {
            ...this.state,
            testAccessCodeAlreadyCheckedIn: true,
            isInvalidTestAccessCode: false,
            currentTestAlreadyInActiveState: false
          },
          () => {
            document.getElementById("test-access-code-field").focus();
          }
        );
      } else if (
        response.error === "The test you are trying to check-in is already in an active state."
      ) {
        this.setState(
          {
            ...this.state,
            testAccessCodeAlreadyCheckedIn: false,
            isInvalidTestAccessCode: false,
            currentTestAlreadyInActiveState: true
          },
          () => {
            document.getElementById("test-access-code-field").focus();
          }
        );
      } else {
        this.setState(
          {
            ...this.state,
            isInvalidTestAccessCode: true,
            testAccessCodeAlreadyCheckedIn: false,
            currentTestAlreadyInActiveState: false
          },
          () => {
            document.getElementById("test-access-code-field").focus();
          }
        );
      }
    });
  };

  handleUserProfileRedirect = () => {
    // redirecting user to profile page
    window.location.href = PATH.profile;
  };

  getTestAccessCode = event => {
    this.setState({ testAccessCode: event.target.value.toUpperCase() });
  };

  handleKeyDown = event => {
    // if key pressed is "Enter"
    if (event.key === "Enter") {
      // handling check-in action
      this.handleCheckin();
    }
  };

  closeDialog = () => {
    this.setState({
      ...this.state,
      testAccessCode: "",
      showCheckInPopup: false,
      isInvalidTestAccessCode: false,
      testAccessCodeAlreadyCheckedIn: false,
      currentTestAlreadyInActiveState: false
    });
  };

  render() {
    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }
    return (
      <div id="unit-test-candidate-check-in">
        {this.props.isLoaded ? (
          <div
            style={styles.checkInBtnContainer}
            id="unit-test-check-in-button"
            className="notranslate"
          >
            <CustomButton
              label={
                <>
                  <FontAwesomeIcon icon={faCheck} />
                  <span id="candidate-check-in" style={styles.buttonLabel} className="notranslate">
                    {LOCALIZE.candidateCheckIn.button}
                  </span>
                </>
              }
              customStyle={styles.checkInBtn}
              action={this.handleCheckInPopup}
              buttonTheme={THEME.PRIMARY}
            />
          </div>
        ) : (
          <div style={styles.checkInBtnContainer} id="unit-test-check-in-loading-button">
            <CustomButton
              label={
                <>
                  <FontAwesomeIcon icon={faCheck} />
                  <span style={styles.buttonLabel} className="notranslate">
                    {LOCALIZE.candidateCheckIn.loading}
                  </span>
                </>
              }
              customStyle={styles.checkInBtn}
              buttonTheme={THEME.PRIMARY}
              disabled={true}
            />
          </div>
        )}
        <PopupBox
          show={this.state.showCheckInPopup}
          title={
            this.props.profileCompleted
              ? LOCALIZE.candidateCheckIn.popup.title
              : LOCALIZE.candidateCheckIn.popup.incompletedProfileTitle
          }
          handleClose={() => {}}
          size="lg"
          description={
            <div>
              {this.props.profileCompleted ? (
                <div>
                  <Row style={styles.row}>
                    <p>{LOCALIZE.candidateCheckIn.popup.description}</p>
                  </Row>
                  <Row className="align-items-center justify-content-center" style={styles.row}>
                    <Col className="align-self-center" sm={12} md={12} lg={6} xl={6}>
                      <div className="align-middle font-weight-bold text-xl-center text-lg-center text-md-left text-sm-left text-xs-left">
                        <label htmlFor="test-access-code-field" className="notranslate">
                          {LOCALIZE.candidateCheckIn.popup.textLabel}
                        </label>
                      </div>
                    </Col>
                    <Col className="align-self-center" sm={12} md={12} lg={6} xl={6}>
                      <div
                        className={
                          "text-xl-center text-lg-center text-md-left text-sm-left text-xs-left"
                        }
                      >
                        <input
                          aria-required={"true"}
                          id="test-access-code-field"
                          className={
                            this.state.isInvalidTestAccessCode ||
                            this.state.testAccessCodeAlreadyCheckedIn ||
                            this.state.currentTestAlreadyInActiveState
                              ? "invalid-field"
                              : "valid-field"
                          }
                          type="text"
                          style={{ ...styles.textInput, ...accommodationsStyle }}
                          value={this.state.testAccessCode}
                          onChange={this.getTestAccessCode}
                          onKeyDown={this.handleKeyDown}
                        />
                        <div>
                          {this.state.isInvalidTestAccessCode && (
                            <label
                              id="invalid-test-access-error"
                              htmlFor="test-access-code-field"
                              style={styles.errorMessage}
                              className="notranslate"
                            >
                              {LOCALIZE.candidateCheckIn.popup.textLabelError}
                            </label>
                          )}
                          {this.state.testAccessCodeAlreadyCheckedIn && (
                            <label
                              id="test-access-code-already-checked-in-error"
                              htmlFor="test-access-code-field"
                              style={styles.errorMessage}
                              className="notranslate"
                            >
                              {LOCALIZE.candidateCheckIn.popup.testAccessCodeAlreadyUsedError}
                            </label>
                          )}
                          {this.state.currentTestAlreadyInActiveState && (
                            <label
                              id="test-already-in-active-state-error"
                              htmlFor="test-access-code-field"
                              style={styles.errorMessage}
                              className="notranslate"
                            >
                              {LOCALIZE.candidateCheckIn.popup.currentTestAlreadyInActiveStateError}
                            </label>
                          )}
                        </div>
                      </div>
                    </Col>
                  </Row>
                </div>
              ) : (
                <div>
                  <SystemMessage
                    messageType={MESSAGE_TYPE.info}
                    title={LOCALIZE.commons.info}
                    message={
                      <p className="notranslate">
                        {LOCALIZE.candidateCheckIn.popup.incompletedProfileSystemMessage}
                      </p>
                    }
                  />
                  <p style={styles.uncompletedProfileDescription} className="notranslate">
                    {LOCALIZE.formatString(
                      LOCALIZE.candidateCheckIn.popup.incompletedProfileDescription,
                      <button
                        aria-label={LOCALIZE.candidateCheckIn.popup.incompletedProfileLink}
                        tabIndex="0"
                        onClick={this.handleUserProfileRedirect}
                        style={styles.profileLink}
                      >
                        {LOCALIZE.candidateCheckIn.popup.incompletedProfileLink}
                      </button>
                    )}
                  </p>
                </div>
              )}
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonIcon={faTimes}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonLabel={LOCALIZE.commons.cancel}
          leftButtonAction={this.closeDialog}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonIcon={this.props.profileCompleted ? faCheck : faLink}
          rightButtonTitle={
            this.props.profileCompleted
              ? LOCALIZE.candidateCheckIn.button
              : LOCALIZE.candidateCheckIn.popup.incompletedProfileRightButton
          }
          rightButtonLabel={
            this.props.profileCompleted
              ? LOCALIZE.candidateCheckIn.button
              : LOCALIZE.candidateCheckIn.popup.incompletedProfileRightButton
          }
          rightButtonAction={
            this.props.profileCompleted ? this.handleCheckin : this.handleUserProfileRedirect
          }
        />
      </div>
    );
  }
}

export { CandidateCheckIn as UnconnectedCandidateCheckIn };

const mapStateToProps = (state, ownProps) => {
  return {
    accommodations: state.accommodations,
    profileCompleted: state.user.profileCompleted
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateCheckInRoom
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(CandidateCheckIn);
