import { createStore, applyMiddleware, compose } from "redux";
import { connectRouter, routerMiddleware } from "connected-react-router";
import thunk from "redux-thunk";
import { createBrowserHistory } from "history";
import rootReducer from "./modules/index";
import SessionStorage, { ACTION, ITEM } from "./SessionStorage";

export const history = createBrowserHistory();

const enhancers = [];
const middleware = [thunk, routerMiddleware(history)];

if (process.env.NODE_ENV === "development") {
  const devToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION__;

  if (typeof devToolsExtension === "function") {
    enhancers.push(devToolsExtension());
  }
}

const loadState = () => {
  try {
    const serializedState = SessionStorage(ACTION.GET, ITEM.STATE);
    if (serializedState === null) {
      return undefined;
    }
    return JSON.parse(serializedState);
  } catch (err) {
    return undefined;
  }
};

const saveState = state => {
  try {
    const serializedState = JSON.stringify(state);
    SessionStorage(ACTION.SET, ITEM.STATE, serializedState);
  } catch {
    // ignore write errors
  }
};

const persistedState = loadState();

const composedEnhancers = compose(applyMiddleware(...middleware), ...enhancers);

const store = createStore(connectRouter(history)(rootReducer), persistedState, composedEnhancers);

store.subscribe(() => {
  saveState(store.getState());
});

export default store;
