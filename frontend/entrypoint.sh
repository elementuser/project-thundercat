#!/bin/bash

# Temporary fix for React 16 if needed: https://stackoverflow.com/a/75967183
# This isn't required for CAT, but leaving this here & commented it out in case it's needed in the future
#export NODE_OPTIONS=--openssl-legacy-provider

# Set correct Yarn version: https://yarnpkg.com/getting-started/install
yarn set version stable

printf "\n\n========== BEGIN - LINKING DEPENDENCIES ==========\n\n"
yarn
printf "\n\n========== END - LINKING DEPENDENCIES ==========\n\n"
yarn start
