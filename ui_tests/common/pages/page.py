from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from common.page_builder import PageBuilder


class Page:
    """
    Functions that need to be called from page classes and return correct page object:
        - home_button (if not homepage.py)
        - sample_tests_button (if not sample_tests.py)
        - various menu functions (evenutally)
        - logout (evenutally)
    I.e., open_sample_tests() in homepage.py calls Page.sample_tests_button() and returns SampleTests
    """

    def __init__(self, driver):
        self.driver = driver
        self.page_builder = PageBuilder(self.driver)

    def logout(self):
        self.open_menu_item("Logout")

    def home_button(self):
        language = self.get_language()

        if language == "en":
            self.driver.find_element(By.LINK_TEXT, "Home").click()
        elif language == "fr":
            self.driver.find_element(By.LINK_TEXT, "Accueil").click()

        return self.page_builder.return_page_object("Check-in")

    def sample_tests_button(self):
        language = self.get_language()

        if language == "en":
            self.driver.find_element(By.LINK_TEXT, "Sample Tests").click()
        elif language == "fr":
            self.driver.find_element(By.LINK_TEXT, "Échantillons de tests").click()

        return self.page_builder.return_page_object("Sample Tests")

    def settings_button(self):
        self.driver.find_element(
            By.XPATH, "//button[contains(@aria-label, 'IE')]"
        ).click()

    def get_language(self):
        """returns current language"""
        html_root = self.driver.find_element(By.CSS_SELECTOR, "html")
        return html_root.get_attribute("lang")

    def toggle_language(self):
        html_root = self.driver.find_element(By.CSS_SELECTOR, "html")
        language = html_root.get_attribute("lang")
        if language == "en":
            self.driver.find_element(
                By.XPATH, "//button[contains(text(), 'Français')]"
            ).click()
        elif language == "fr":
            self.driver.find_element(
                By.XPATH, "//button[contains(text(), 'English')]"
            ).click()

    def open_menu_item(self, link_text):
        """Clicks a menu option based on it's English link text"""
        hrefs_table = {
            "System Administrator": "/system-administration",
            "PPC Administrator": "/ppc-administration",
            "Scorer": "/scorer",
            "Test Administrator": "/test-sessions",
            "Check-in": "/check-in",
            "My Profile": "/profile",
            "Incident Reports": "/incident-report",
            "My Tests": "/my-tests",
            "Contact Us": "/contact-us",
            "Logout": "/",
        }

        # open dropdown
        self.driver.find_element(By.ID, "dropdown-basic").click()
        self.driver.find_element(
            By.XPATH, "//a[contains(@href, '" + hrefs_table[link_text] + "')]"
        ).click()

        return self.page_builder.return_page_object(link_text)

    def return_page_object(self, name):
        return self.page_builder.return_page_object(name)

    """
    The methods below are used to open tests in Homepage or Sample Tests.
    Eventually (when Sample Tests is removed) these methods should be moved to Homepage
    so Page only contains shared header methods.
    """

    def select_test(self, eng_name):
        """
        Opens the specified test in Sample Tests or Homepage

        Args:
            test_name (str): English name of the test to open
        """
        if self.get_language() == "fr":
            test_name = self.__get_french_name(eng_name)
        else:
            test_name = eng_name

        # find the row in the table containing test_name
        test_row = self.driver.find_element(
            By.XPATH, "//td[contains(text(), '" + test_name + "')]"
        )
        test_row = test_row.find_element(By.XPATH, "..")

        # click the view button for the passed in test
        test_button = test_row.find_element(By.CSS_SELECTOR, "td > button")
        test_button.click()
        return self.__get_test_object(eng_name)

    def __get_test_object(self, eng_name):
        if eng_name == "Sample e-MIB":
            return self.return_page_object("Sample e-MIB")
        elif eng_name == "Pizza Test":
            return self.return_page_object("Pizza Test")

    def __get_french_name(self, eng_name):
        """Returns the French version when given an English test name"""
        translations = {
            "Sample e-MIB": "Échantillon de la BRG-e",
            "Pizza Test": "FR Pizza Test",
        }

        return translations.get(eng_name)
