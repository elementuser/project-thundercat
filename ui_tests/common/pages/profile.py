from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from common.pages.page import Page

# This class is here for the whole profile page.


class Profile(Page):
    def __init__(self, driver):
        """This is the first thing that will be loaded when this class is called."""
        self.driver = driver
        try:
            WebDriverWait(driver, 10).until(EC.title_is("CAT - Profile"))
        except:
            raise ValueError(
                "This is not the Profile page or the page title is incorrect"
            )

    #### These def's are for the Personal Info part of the Profile page. ####

    def personal_info_tab(self):
        """This Def will click the side tab to get into the Personal Info page and clicks it."""

        self.driver.find_element(
            By.LINK_TEXT, "left-tabs-navigation-tab-event_key_1"
        ).click()

    def personal_info_form(self, d_birth, m_birth, y_birth, s_email, pri_number):
        """This Def fills in the form on the page."""

        # The date of birth functionality does not seem to be there for this to work
        """

        date_birth_input = self.driver.find_element(By.ID, "selected-day-field")
        date_birth_input.clear()
        date_birth_input.send_keys(d_birth)

        month_birth_input = self.driver.find_element(By.ID, "selected-month-field")
        month_birth_input.clear()
        month_birth_input.send_keys(m_birth)

        year_birth_input = self.driver.find_element(By.ID, "selected-year-field")
        year_birth_input.clear()
        year_birth_input.send_keys(y_birth)
        """
        # These lines will find the input for the secondary email and fill in the new one
        secondary_email_input = self.driver.find_element(
            By.XPATH, "//*[@id='secondary-email-input']"
        )
        secondary_email_input.clear()
        secondary_email_input.send_keys(s_email)

        # These lines will find the input for the PRI and fill in the new one
        pri_number_input = self.driver.find_element(By.XPATH, "//*[@id='pri-input']")
        pri_number_input.clear()
        pri_number_input.send_keys(pri_number)

        # This will find and click the save button.
        self.driver.find_element(
            By.XPATH,
            "//*[@id='data-saved-successfully']/button",
        ).click()

    def pri_info(self):
        """This Def will click the info button for the PRI"""
        self.driver.find_element(
            By.XPATH,
            "//*[@id='pri-title']/button",
        ).click()

    def date_birth_info(self):
        """This Def will click the info button for the date of birth"""
        self.driver.find_element(
            By.XPATH,
            "//*[@id='dob-title']/p/button",
        ).click()

    def secondary_email_info(self):
        """This Def will click the info button for the secondary email"""
        self.driver.find_element(
            By.XPATH,
            "//*[@id='pri-or-military-nbr-title']/button",
        ).click()

    #### All these def's are for the Password page in profile ####

    def password_tab(self):
        """This Def will click the side tab to get into the Password page"""
        self.driver.find_element(By.ID, "left-tabs-navigation-tab-event_key_2").click()

    def current_password(self, current_p):
        """This Def will click the current password box and fill in the text from the input."""
        password = self.driver.find_element(By.ID, "current-password-input")
        password.clear()
        password.send_keys(current_p)

    def new_password(self, new_p):
        """This Def will click the new password box and fill in the text from the input"""
        password = self.driver.find_element(By.ID, "new-password-input")
        password.clear()
        password.send_keys(new_p)

    def new_confirm_password(self, new_cp):
        """This Def will click the confirm password box and fill in the text from in the input."""
        password = self.driver.find_element(By.ID, "confirm-password-input")
        password.clear()
        password.send_keys(new_cp)

    def save_password(self):
        """This Def finds the button to save and clicks it to save the input in the form."""
        self.driver.find_element(By.ID, "new-password-saved-successfully").click()

    def reveal_current_password(self):
        """This Def will find the button to reveal the current password and click it."""
        self.driver.find_element(
            By.XPATH,
            "//*[@id='left-tabs-navigation-tabpane-event_key_2']/div/div/div/div/div[1]/div[1]/div[1]/div[2]/div[1]/span",
        ).click()

    def reveal_new_password(self):
        """This Def will find the button to reveal the new password and click it."""
        self.driver.find_element(
            By.XPATH,
            "//*[@id='left-tabs-navigation-tabpane-event_key_2']/div/div/div/div/div[1]/div[1]/div[2]/div[2]/div[1]/span",
        ).click()

    def reveal_confirm_password(self):
        """This Def will find the button to reveal the confirm password and click it."""
        self.driver.find_element(
            By.XPATH,
            "//*[@id='left-tabs-navigation-tabpane-event_key_2']/div/div/div/div/div[1]/div[1]/div[3]/div[2]/div[1]/span",
        ).click()

    def new_password_info(self):
        """This Def will find the button the show info about new password info and click it."""
        self.driver.find_element(
            By.XPATH,
            "//*[@id='left-tabs-navigation-tabpane-event_key_2']/div/div/div/div/div[1]/div[1]/div[2]/div[1]/button",
        ).click()

    #### All these def's are for the Preferences Page in profile ####

    def preferences_tab(self):
        """This Def finds the preference tab in the header drop down and clicks it."""
        self.driver.find_element(By.ID, "left-tabs-navigation-tab-event_key_3").click()

    def notification_primary(self):
        """This Def finds the primary email button to be checked and clicks it."""
        self.driver.find_element(By.ID, "notification-checkbox-0").click()

    def notification_secondary(self):
        """This Def finds the secondary email button to be checked and clicks it."""
        self.driver.find_element(By.ID, "notification-checkbox-1").click()

    def display_profile_picture(self):
        """This Def finds the profile display button to be checked and clicks it."""
        self.driver.find_element(By.ID, "display-checkbox-0").click()

    def display_tooltips_icon(self):
        """This Def finds the die tool tips icon button to be checked and clicks it."""
        self.driver.find_element(By.ID, "display-checkbox-1").click()

    #### All these def's are for the Permissions ####

    def permissions_tab(self):
        """This Def finds the prermissions tab in the header drop down and clicks it."""
        self.driver.find_element(By.ID, "left-tabs-navigation-tab-event_key_4").click()

    def permissions_check_test(self):
        """This Def finds whether the user has the permission for test"""

        if (
            len(self.driver.find_elements(By.XPATH, "//*[@id='permission-label-0']"))
            != 0
        ):
            self.driver.find_element(By.XPATH, "//*[@id='permission-label-0']").click()
            print("Does have permision")
        else:
            print("You do not have the Test permission")

    def permissions_check_ppc(self):
        """This Def finds whether the user has the permission for PPC"""
        if (
            len(self.driver.find_elements(By.XPATH, "//*[@id='permission-label-1']"))
            != 0
        ):
            self.driver.find_element(By.XPATH, "//*[@id='permission-label-1']").click()
            print("Does have permision")
        else:
            print("You do not have the PPC permission")

    def permissions_check_system(self):
        """This Def finds whether the user has the permission for System"""
        if (
            len(self.driver.find_elements(By.XPATH, "//*[@id='permission-label-2']"))
            != 0
        ):
            self.driver.find_element(By.XPATH, "//*[@id='permission-label-2']").click()
            print("Does have permision")
        else:
            print("You do not have the System permission")

    def permissions_tool_tip(self):
        """This Def finds the tool tip on the page and clicks it."""
        self.driver.find_element(
            By.XPATH,
            "//*[@id='left-tabs-navigation-tabpane-event_key_4']/div/div/div/div/section/div/div/div[1]/div/button",
        ).click()

    def permissions_request(self):
        """This Def finds the button for requesting permissions and clicks it."""
        self.driver.find_element(
            By.XPATH,
            "//*[@id='left-tabs-navigation-tabpane-event_key_4']/div/div/div/div/section/div/div/div[2]/div/div",
        ).click()

    def permissions_systems_form(
        self, email_input, pri_input, supervisor_input, s_email, rationale_input
    ):
        """This Def finds the inputs in the form and fills them in based on the info given."""
        email = self.driver.find_element(By.XPATH, "//*[@id='goc-email']")
        email.clear()
        email.send_keys(email_input)

        pri = self.driver.find_element(By.XPATH, "//*[@id='pri-or-military-nbr']")
        pri.clear()
        pri.send_keys(pri_input)

        supervisor = self.driver.find_element(By.XPATH, "//*[@id='supervisor']")
        supervisor.clear()
        supervisor.send_keys(supervisor_input)

        supervisor_email = self.driver.find_element(
            By.XPATH, "//*[@id='supervisor-email']"
        )
        supervisor_email.clear()
        supervisor_email.send_keys(s_email)

        rationale = self.driver.find_element(By.XPATH, "//*[@id='rationale']")
        rationale.clear()
        rationale.send_keys(rationale_input)

    def permissions_assignment_test(self):
        """Inside the request this Def will find the Test administrator button and clicks it."""
        self.driver.find_element(
            By.XPATH,
            "//*[@id='permission-checkbox-0']",
        ).click()

    def permissions_assignment_system(self):
        """Inside the request this Def will find the System administrator button and clicks it."""
        self.driver.find_element(
            By.XPATH,
            "//*[@id='permission-checkbox-1']",
        ).click()

    def permissions_assignment_ppc(self):
        """Inside the request this Def will find the PPC administrator button and clicks it."""
        self.driver.find_element(
            By.XPATH,
            "//*[@id='permission-checkbox-2']",
        ).click()

    def permissions_cancel(self):
        """This Def finds the button to cancel the form and clicks it."""
        self.driver.find_element(
            By.XPATH,
            "//*[@id='unit-test-left-btn']",
        ).click()

    def permissions_submit(self):
        """This Def fidns the button to submit the form and clicks it."""
        self.driver.find_element(
            By.XPATH,
            "//*[@id='unit-test-right-btn']",
        ).click()

    #### All these def's are for the Merge ####

    def profile_merge_tab(self):
        """This Def finds the merge button in the drop down from the header and clicks it."""
        self.driver.find_element(By.ID, "left-tabs-navigation-tab-event_key_5").click()
