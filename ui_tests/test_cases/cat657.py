import sys
import os

sys.path.insert(1, os.path.join(sys.path[0], ".."))

import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from common.pages.login import Login
from common.web_driver_util import init_web_driver, settings


class CAT657(unittest.TestCase):
    """https://cfp-psc.atlassian.net/browse/CAT-657"""

    def setUp(self):
        self.driver = init_web_driver()
        self.driver.maximize_window()
        self.user = settings["candidate"]

    def test_notepad_stays_clear(self):
        login = Login(self.driver)
        homepage = login.login_valid_user(self.user["email"], self.user["password"])
        sample_tests = homepage.sample_tests_button()
        test = sample_tests.select_test("Sample e-MIB")
        test.proceed_to_test()
        test.start_test()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
