import sys
import os

sys.path.insert(1, os.path.join(sys.path[0], ".."))

import unittest
from time import sleep  # REQUIRED FOR THIS TEST
from selenium import webdriver
from selenium.webdriver.common.by import By
from common.pages.login import Login
from common.web_driver_util import init_web_driver, settings


class CAT656(unittest.TestCase):
    """https://cfp-psc.atlassian.net/browse/CAT-656"""

    def setUp(self):
        self.driver = init_web_driver()
        self.driver.maximize_window()
        self.user = settings["candidate"]

    def test_sample_test_and_timer(self):
        # navigate to Sample e-MIB
        login = Login(self.driver)
        homepage = login.login_valid_user(self.user["email"], self.user["password"])
        sample_tests = homepage.sample_tests_button()
        sample_emib = sample_tests.select_test("Sample e-MIB")

        # enter and start test
        sample_emib.proceed_to_test()
        sample_emib.start_test()

        # verify we are on Background Information tab
        background_tab_class_list = self.driver.find_element(
            By.ID, "emib-tabs-tab-background"
        ).get_attribute("class")
        self.assertIn("active", background_tab_class_list)

        # verify the other two tabs exist
        self.assertTrue(self.driver.find_element(By.ID, "emib-tabs-tab-instructions"))
        self.assertTrue(self.driver.find_element(By.ID, "emib-tabs-tab-inbox"))

        # sleep for x seconds then verify time has passed on the timer
        timer = self.driver.find_element(
            By.CSS_SELECTOR, "#unit-test-timer span:nth-child(2)"
        )
        initial_time = int(timer.text[-2:])
        sleep(3)
        later_time = int(timer.text[-2:])

        # assert timer shows time has passed
        self.assertGreater(later_time, initial_time)

        # we use icon beside timer button to tell if it is open or closed
        timer_toggle_icon = self.driver.find_element(
            By.CSS_SELECTOR, "#unit-test-toggle-timer svg"
        )

        # hide timer and verify it is hidden
        sample_emib.toggle_timer()
        self.assertIn("fa-plus-circle", timer_toggle_icon.get_attribute("class"))

        # show timer and verify it is visible
        sample_emib.toggle_timer()
        self.assertIn("fa-minus-circle", timer_toggle_icon.get_attribute("class"))

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
