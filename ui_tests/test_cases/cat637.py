import sys
import os

sys.path.insert(1, os.path.join(sys.path[0], ".."))

import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from common.pages.login import Login
from common.web_driver_util import (
    init_web_driver,
    save_created_account,
    generate_random_account_info,
)


class CAT637(unittest.TestCase):
    """https://cfp-psc.atlassian.net/browse/CAT-637"""

    def setUp(self):
        self.driver = init_web_driver()
        self.driver.maximize_window()

    def test_valid_account_creation(self):
        login = Login(self.driver)

        # enter account info and submit form
        login.open_create_account_tab()
        account_info = generate_random_account_info()
        login.create_account(account_info)
        login.submit_create_account_tab()

        # save account info if creation was successful
        fullname = account_info["fname"] + " " + account_info["lname"]
        WebDriverWait(self.driver, 10).until(
            EC.text_to_be_present_in_element((By.ID, "user-welcome-message"), fullname)
        )
        save_created_account(account_info)

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
