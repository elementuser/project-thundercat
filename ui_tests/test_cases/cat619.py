import sys
import os

sys.path.insert(1, os.path.join(sys.path[0], ".."))

import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from common.pages.login import Login
from common.web_driver_util import init_web_driver


class CAT619(unittest.TestCase):
    """https://cfp-psc.atlassian.net/browse/CAT-619"""

    def setUp(self):
        self.driver = init_web_driver()
        self.driver.maximize_window()

    def test_invalid_account_creation_errors_show(self):
        login = Login(self.driver)

        # submit an empty create account form
        login.open_create_account_tab()
        login.submit_create_account_tab()

        # verify all errors show for all empty fields
        self.assertTrue(self.driver.find_element(By.ID, "first-name-error"))
        self.assertTrue(self.driver.find_element(By.ID, "last-name-error"))
        self.assertTrue(self.driver.find_element(By.ID, "date-error"))
        self.assertTrue(self.driver.find_element(By.ID, "email-address-error"))
        self.assertTrue(self.driver.find_element(By.ID, "password-errors"))
        self.assertTrue(self.driver.find_element(By.ID, "privacy-notice-error"))

    def test_almost_valid_account_creation(self):
        login = Login(self.driver)

        # fill out part of the form and submit
        login.open_create_account_tab()
        login.create_account(
            {
                "fname": "",
                "lname": "tester",
                "dob": "31/12/1900",
                "email": "testemail@canada.ca",
                "pri": "",
                "pswd": "Random1!",
                "confirmPswd": "Random1!",
                "privNotice": False,
            }
        )
        login.submit_create_account_tab()

        # verify first name and privacy notice errors show
        self.assertTrue(self.driver.find_element(By.ID, "first-name-error"))
        self.assertTrue(self.driver.find_element(By.ID, "privacy-notice-error"))

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
