import sys
import os

sys.path.insert(1, os.path.join(sys.path[0], ".."))

import unittest
from common.pages.login import Login
from common.web_driver_util import init_web_driver, settings


class TestLogin(unittest.TestCase):
    def setUp(self):
        self.driver = init_web_driver()
        self.driver.maximize_window()
        self.user = settings["candidate"]

    def test_valid_login(self):
        login = Login(self.driver)
        login.login_valid_user(self.user["email"], self.user["password"])

    def test_valid_login_thru_header(self):
        login = Login(self.driver)
        login.login_valid_user_thru_header(self.user["email"], self.user["password"])

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
