import sys
import os

sys.path.insert(1, os.path.join(sys.path[0], ".."))

import unittest
from common.pages.login import Login
from common.web_driver_util import init_web_driver, settings


class TestProfile(unittest.TestCase):
    def setUp(self):
        self.driver = init_web_driver()
        self.driver.maximize_window()
        self.user = settings["candidate"]

    def test_profile_functions(self):
        login = Login(self.driver)
        homepage = login.login_valid_user(self.user["email"], self.user["password"])
        homepage.toggle_language()
        profile = homepage.open_menu_item("My Profile")
        profile.pri_info()
        profile.date_birth_info()
        profile.secondary_email_info()
        profile.personal_info_form("3", "12", "1980", self.user["email"], "123456789")
        profile.preferences_tab()
        profile.notification_primary()
        profile.notification_secondary()
        profile.display_profile_picture()
        profile.display_tooltips_icon()
        profile.password_tab()
        profile.current_password("Random1!")
        profile.new_password("Random1!")
        profile.new_confirm_password("Random1!")
        profile.save_password()
        profile.reveal_current_password()
        profile.reveal_new_password()
        profile.reveal_confirm_password()
        profile.new_password_info()
        profile.permissions_tab()
        profile.permissions_check_test()
        profile.permissions_check_ppc()
        profile.permissions_check_system()
        profile.permissions_tool_tip()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
