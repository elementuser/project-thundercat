import sys
import os

sys.path.insert(1, os.path.join(sys.path[0], ".."))

import unittest
from common.pages.login import Login
from common.web_driver_util import init_web_driver, settings


class TestSystemAdministration(unittest.TestCase):
    def setUp(self):
        self.driver = init_web_driver()
        self.driver.maximize_window()
        self.admin = settings["admin"]

    def test_visit_all_nav_sections(self):
        login = Login(self.driver)
        system_admin = login.login_valid_user(
            self.admin["email"], self.admin["password"], user_is_admin=True
        )

        system_admin.open_nav_item("Permissions")

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
