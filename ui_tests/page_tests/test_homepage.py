import sys
import os

sys.path.insert(1, os.path.join(sys.path[0], ".."))

import unittest
from common.pages.login import Login
from common.web_driver_util import init_web_driver, settings


class TestHomepage(unittest.TestCase):
    def setUp(self):
        self.driver = init_web_driver()
        self.driver.maximize_window()
        self.user = settings["candidate"]
        self.admin = settings["admin"]

    def test_valid_check_in(self):
        login = Login(self.driver)
        homepage = login.login_valid_user(self.user["email"], self.user["password"])
        homepage.open_check_in()
        homepage.enter_access_code("boop")
        homepage.submit_check_in()

    def test_valid_cancel(self):
        login = Login(self.driver)
        homepage = login.login_valid_user(self.user["email"], self.user["password"])
        homepage.open_check_in()
        homepage.cancel_check_in()

    def test_admin_login(self):
        login = Login(self.driver)
        login.login_valid_user(
            self.admin["email"], self.admin["password"], user_is_admin=True
        )

    def tearDown(self):
        self.driver.close()


if __name__ == "__main__":
    unittest.main()
