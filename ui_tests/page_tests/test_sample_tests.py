import sys
import os

sys.path.insert(1, os.path.join(sys.path[0], ".."))

import unittest
from common.pages.login import Login
from common.web_driver_util import init_web_driver, settings
from selenium.webdriver.common.by import By


class TestSampleTests(unittest.TestCase):
    def setUp(self):
        self.driver = init_web_driver()
        self.driver.maximize_window()
        self.user = settings["candidate"]

    def test_open_emib(self):
        login = Login(self.driver)
        homepage = login.login_valid_user(self.user["email"], self.user["password"])
        sample_tests = homepage.sample_tests_button()
        sample_tests.select_test("Sample e-MIB")

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
