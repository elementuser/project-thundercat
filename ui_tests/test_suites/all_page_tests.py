import unittest

"""Update test_settings.json to modify test settings"""

# load all page tests in /page_tests/
LOADER = unittest.TestLoader()
TEST_DIRECTORY = "./page_tests/"
SUITE = LOADER.discover(TEST_DIRECTORY, pattern="test_*.py")

# run the tests with maximum verbosity
RUNNER = unittest.TextTestRunner(verbosity=2)
RUNNER.run(SUITE)
