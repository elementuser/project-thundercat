import unittest

"""Update test_settings.json to modify test settings"""

# load all cat tests in /test_cases/
LOADER = unittest.TestLoader()
TEST_DIRECTORY = "./test_cases/"
SUITE = LOADER.discover(TEST_DIRECTORY, pattern="cat*.py")

# run the tests with maximum verbosity
RUNNER = unittest.TextTestRunner(verbosity=2)
RUNNER.run(SUITE)
