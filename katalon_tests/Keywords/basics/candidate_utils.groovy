package basics

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class candidate_utils {
	/**
	 * Candidate check-in functionality
	 */
	@Keyword
	def void candidateCheckIn(String username, String testAccessCode){

		// login (using candidate credentials)
		(new basics.utils()).login(username, GlobalVariable.USERS_ENCRYPTED_PASSWORD)
		//click check-in
		WebUI.click(findTestObject('Object Repository/Page_CAT - Home/candidate_dashboard_button_Check-in'))
		//click cancel
		WebUI.click(findTestObject('Object Repository/Page_CAT - Home/check_in_popup_button_Cancel'))
		//click check-in
		WebUI.click(findTestObject('Object Repository/Page_CAT - Home/candidate_dashboard_button_Check-in'))
		//Enter test-access-code
		WebUI.setText(findTestObject('Object Repository/Page_CAT - Home/check_in_popup_input_Test Access Code_test-access-code-field'), testAccessCode)
		//click check-in
		WebUI.click(findTestObject('Object Repository/Page_CAT - Home/check_in_popup_button_Check-in'))
		//get the name of the assigned test
		String assignedTest = (new database.DataGetting()).getFirstTestAccessDefinition("en_name")
		//Verify the candidate test
		WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Home/div_Unsupervised Test of Reading Comprehension (assesses French as second official language)'), 5)
		WebUI.verifyEqual(assignedTest, WebUI.getText(findTestObject('Object Repository/Page_CAT - Home/div_Unsupervised Test of Reading Comprehension (assesses French as second official language)')))
		// adding a 2 seconds delay
		WebUI.delay(2)

	}

	/**
	 * Error validation for Candidate check-in functionality
	 */
	@Keyword
	def void runCandidateCheckInErrorValidation(String username, String testAccessCode){
		// candidate check-in with existing test access code
		(new basics.candidate_utils()).candidateCheckIn(username, testAccessCode)
		//verify the error message - you have already used this test access code
		WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - TA/label_You have already used this test access code'), 2)
		//click cancel
		WebUI.click(findTestObject('Object Repository/Page_CAT - Home/check_in_popup_button_Cancel'))
		// logout
		(new basics.utils()).logout()
		String invalidTestAccessCode = testAccessCode + "XXXX"
		//candidate check-in with invalid test access code
		(new basics.candidate_utils()).candidateCheckIn(username, invalidTestAccessCode)
		//verify the error message - Must be a valid access code
		WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - TA/label_Must be a valid Test Access Code'), 2)
		//click cancel
		WebUI.click(findTestObject('Object Repository/Page_CAT - Home/check_in_popup_button_Cancel'))
		// logout
		(new basics.utils()).logout()
		//Generate new Test access code for the same test
		(new basics.ta_utils()).generateTestAccessCode()
		String testAccessCodeNew = WebUI.getText(findTestObject('Object Repository/Page_CAT - TA/label_test_access_code_2'))
		// logout
		(new basics.utils()).logout()
		//candidate check in with the new test access code
		(new basics.candidate_utils()).candidateCheckIn(username, testAccessCodeNew)
		//verify the error message - you have already checked in for this test
		WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - TA/label_You havealready checked in for this test'), 2)
		//click cancel
		WebUI.click(findTestObject('Object Repository/Page_CAT - Home/check_in_popup_button_Cancel'))
		// logout
		(new basics.utils()).logout()
		// login (using TA credentials)
		(new basics.utils()).login(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)
		WebUI.delay(2)
		//Click Delete on new test access code
		WebUI.click(findTestObject('Object Repository/Page_CAT - TA/span_Delete_test_access_code_2'))
		//Click Delete on pop up
		WebUI.click(findTestObject('Object Repository/Page_CAT - TA/span_Delete'))
		// logout
		(new basics.utils()).logout()
	}

	@Keyword
	def void runCompleteProfile(String username){
		// login (using candidate credentials)
		(new basics.utils()).login(username, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

		// Complete Profile
		WebUI.click(findTestObject('Page_CAT - Home/button_MENU'))

		WebUI.click(findTestObject('Page_CAT - Home/button_My Profile'))

		WebUI.click(findTestObject('Page_CAT - Profile/input_No_aboriginal-radio-buttons'))

		WebUI.click(findTestObject('Page_CAT - Profile/input_No_disability-radio-buttons'))

		WebUI.click(findTestObject('Page_CAT - Profile/input_No_identify-as-woman-radio-buttons'))

		WebUI.click(findTestObject('Page_CAT - Profile/input_No_visible-minority-radio-buttons'))

		WebUI.click(findTestObject('Page_CAT - Profile/button_Save_Personal_info'))

		WebUI.click(findTestObject('Page_CAT - Profile/button_OK'))

		// Go back to Check-In page
		WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_MENU'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/span_Check-in'))

		// logout
		(new basics.utils()).logout()
	}
}
