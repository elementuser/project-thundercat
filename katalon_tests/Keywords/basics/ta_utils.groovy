package basics

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.Keys

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class ta_utils {

	/**
	 * Generate Test access code
	 */
	@Keyword
	def void generateTestAccessCode(){
		// login (using TA credentials)
		(new basics.utils()).login(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)
		WebUI.delay(2)

		//Click Generate Test Access Code
		WebUI.click(findTestObject('Object Repository/Page_CAT - TA/span_Generate Test Access Code'))

		//Click Cancel
		WebUI.click(findTestObject('Object Repository/Page_CAT - TA/span_Cancel'))

		//Click Generate Test Access Code
		WebUI.click(findTestObject('Object Repository/Page_CAT - TA/span_Generate Test Access Code'))

		//Select Test Order Number
		WebUI.click(findTestObject('Object Repository/Page_CAT - TA/svg_Select_css-tj5bde-Svg'))
		WebUI.delay(2)
		WebUI.click(findTestObject('Page_CAT - Test Administrator/test_order_number_dropdown_input'))

		//Select Test
		WebUI.delay(2)
		WebUI.click(findTestObject('Object Repository/Page_CAT - TA/svg_Select_css-tj5bde-Svg_1'))
		WebUI.sendKeys(findTestObject('Page_CAT - Test Administrator/test_dropdown_input'), Keys.chord(Keys.ENTER))

		//Select Test Session Language
		WebUI.delay(2)
		WebUI.click(findTestObject('Object Repository/Page_CAT - TA/svg_Select_css-tj5bde-Svg_1_2'))
		WebUI.sendKeys(findTestObject('Page_CAT - Test Administrator/test_session_language_dropdown_input'), Keys.chord(Keys.ENTER))

		//Click Generate
		WebUI.delay(2)
		WebUI.click(findTestObject('Page_CAT - TA/span_Generate'))
	}
}
