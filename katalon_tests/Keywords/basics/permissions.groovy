package basics

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import basics.DateUtil
import java.util.Calendar
import java.text.DecimalFormat

import org.openqa.selenium.Keys

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class permissions {
	/**
	 * send permission request functionality (need to be logged in to call this keyword)
	 * this keyword should be called for users without any permission, otherwise the requested permission might not be the right one
	 * note that if you call this keyword with a user that has pending or active permissions, they will be deleted at the beginning of the execution 
	 */
	@Keyword
	def void sendPermissionRequest(String username, String requestedPermission, boolean runErrorValidation){
		// make sure that there is no pending or active permissions associated to this user
		// deleting user permissions
		new database.DataDeletion().deleteUserPermissions(username)
		// deleting user pending permissions
		new database.DataDeletion().deleteAllPendingUserPermissions(username)
		// navigating to main URL
		WebUI.navigateToUrl(GlobalVariable.MAIN_URL)
		// click MENU
		WebUI.click(findTestObject('Page_CAT - Home/button_MENU'))
		// click My Profile
		WebUI.click(findTestObject('Page_CAT - Home/button_My Profile'))
		// select Rights and Permissions side navigation item
		WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Rights and Permissions'))
		// click Obtain Rights and Permissions button
		WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Obtain Rights and Permissions'))
		// if runErrorValidation is true
		if (runErrorValidation) {
			// click Cancel
			WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/rights_and_permission_popup_button_Cancel'))
			// click Obtain Rights and Permissions button
			WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Obtain Rights and Permissions'))
			// fill out form with invalid entries
			WebUI.setText(findTestObject('Object Repository/Page_CAT - Profile/input_,You must enter your Government of Ca_34c402'),
					'invalid.email.com')
			WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/div_PRI or Service Number'))
			WebUI.setText(findTestObject('Object Repository/Page_CAT - Profile/input_PRI or Service Number_pri-or-military-nbr'), '123')
			WebUI.setText(findTestObject('Object Repository/Page_CAT - Profile/input_Supervisor Name_supervisor'), 'Invalid Name 1')
			WebUI.setText(findTestObject('Object Repository/Page_CAT - Profile/input_Supervisor Email Address_supervisor-email'), 'invalid.supervisor.email.com')
			// click Submit
			WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Submit'))
			// make sure that all respective error messages are displayed
			WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Profile/label_Rights_and_permissions_Must be a valid Email Address'), 2)
			WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Profile/label_Rights_and_permissions_Must be a valid PRI or Service Number'), 2)
			WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Profile/label_Rights_and_permissions_The name contains one or more invalid characters'), 2)
			WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Profile/label_Rights_and_permissions_Must be a valid Email Address'), 2)
			WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Profile/label_Rights_and_permissions_Must provide a reason for this request'), 2)
			WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Profile/label_Rights_and_permissions_Must select at least one user role'), 2)
		}
		// fill out the form
		WebUI.setText(findTestObject('Object Repository/Page_CAT - Profile/input_,You must enter your Government of Ca_34c402'),
				username)
		WebUI.setText(findTestObject('Object Repository/Page_CAT - Profile/input_PRI or Service Number_pri-or-military-nbr'), '12345679')
		WebUI.setText(findTestObject('Object Repository/Page_CAT - Profile/input_Supervisor Name_supervisor'), 'Katalon Supervisor')
		WebUI.setText(findTestObject('Object Repository/Page_CAT - Profile/input_Supervisor Email Address_supervisor-email'), 'katalon.supervisor@email.ca')
		WebUI.setText(findTestObject('Object Repository/Page_CAT - Profile/textarea_Katalon Tests'), 'Katalon Tests')
		// permission requested logic
		// ETTA
		if (requestedPermission == GlobalVariable.ETTA_PERMISSION) {
			// select ETTA permission
			WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/input_etta_permission_to_request'))
		} else if (requestedPermission == GlobalVariable.PPC_PERMISSION) {
			// select PPC permission
			WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/input_ppc_permission_to_request'))
		}
		else if (requestedPermission == GlobalVariable.TA_PERMISSION) {
			// select PPC permission
			WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/input_ta_permission_to_request'))
		}
		else if (requestedPermission == GlobalVariable.ADAPTATIONS_PERMISSION) {
			// select PPC permission
			WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/input_adaptations_permission_to_request'))
		}
		// click Submit
		WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Submit'))
	}

	/**
	 * approve permission request functionality
	 * you need access to the super user account (if you want to approve an ETTA permission request) and/or to the ETTA account (if you want to approve any other permissions) 
	 */
	@Keyword
	def void approvePermissionRequest(String username, String firstName, String lastName, boolean approveEttaPermission){
		// navigating to main URL
		WebUI.navigateToUrl(GlobalVariable.MAIN_URL)
		//		// if we need to approve ETTA permission
		//		if (approveEttaPermission) {
		//			// login (as a super user)
		//			(new basics.utils()).login(GlobalVariable.SUPER_USER_USERNAME, GlobalVariable.SUPER_USER_ENCRYPTED_PASSWORD)
		//		} else {
		//			// login (as a super user)
		//			(new basics.utils()).login(GlobalVariable.ETTA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)
		//		}

		// NEW - New roles have been created, we will have to refactor so we use the right permission user to approve another
		// For now, we will use our SuperUser to approve everything
		(new basics.utils()).login(GlobalVariable.SUPER_USER_USERNAME, GlobalVariable.SUPER_USER_ENCRYPTED_PASSWORD)

		// wait for loading to be done
		WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/pending_permissions_first_row_user_role'), 25)
		// adding delay
		WebUI.delay(2)
		// search for user related to permission to approve
		WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_permission-requests-search-bar'),
				username)
		// simulate "Enter" key press
		WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_permission-requests-search-bar'), Keys.chord(Keys.ENTER))
		// wait for loading to be done
		WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/pending_permissions_first_row_user_role'), 25)
		// adding delay
		WebUI.delay(2)
		// click View (located in the table)
		WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_View'))
		// make sure that the right active permission has been selected
		if (!(WebUI.getText(findTestObject('Object Repository/Page_CAT - Business Operations/pending_permissions_request_popup_description_paragraph')).contains(
		firstName))) {
			KeywordUtil.markFailedAndStop('Wrong permission request has been selected')
		}
		if (!(WebUI.getText(findTestObject('Object Repository/Page_CAT - Business Operations/pending_permissions_request_popup_description_paragraph')).contains(
		lastName))) {
			KeywordUtil.markFailedAndStop('Wrong permission request has been selected')
		}
		// click Approve
		WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_Approve'))
		// select Active Rights and Permissions tab
		WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/a_Active Rights and Permissions'))
		// wait for loading to be done
		WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/active_permissions_first_row_user_name_and_contact_info'), 25)
		// adding delay
		WebUI.delay(2)
		// search for user related to permission to approve
		WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_active-permissions-search-bar'),
				username)
		// simulate "Enter" key press
		WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_active-permissions-search-bar'), Keys.chord(Keys.ENTER))
		// click Modify
		WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_Modify'))
		// click Close popup
		WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_Close'))
	}

	/**
	 * deny permission request functionality
	 * you need access to the ETTA account to call that keyword
	 * no more than one permission request should exist for the specified user 
	 */
	@Keyword
	def void denyPermissionRequest(String username, boolean runErrorValidation){
		// navigating to main URL
		WebUI.navigateToUrl(GlobalVariable.MAIN_URL)
		// wait for loading to be done
		WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/pending_permissions_first_row_user_role'), 25)
		// adding delay
		WebUI.delay(2)
		// search for user related to permission to approve
		WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_permission-requests-search-bar'),
				username)
		// simulate "Enter" key press
		WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_permission-requests-search-bar'), Keys.chord(Keys.ENTER))
		// wait for loading to be done
		WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/pending_permissions_first_row_user_role'), 25)
		// adding delay
		WebUI.delay(2)
		// click View (located in the table)
		WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_View'))
		if (runErrorValidation) {
			// click Deny
			WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_Deny'))
			// make sure that the reason for denial of request error message is displayed
			WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/label_You must enter a reason to deny the permission'), 2)
		}
		// add reason message
		WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/textarea_Katalon Test Deny Permission Request Test'),
				'Katalon Test Deny Permission Request Test')
		// click Deny again
		WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_Deny'))
		// make sure that the table is now empty (still searching for specified username)
		WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/label_There are no rights or permissions requests at the moment'), 2)
	}

	/**
	 * assign test access functionality
	 * you need access to the ETTA account to call this keyword
	 * set "testAccessAlreadyAssigned" to true if the TA already has the test permission, so it will test the test permission already assigned error and assign a second one with a different test order number
	 */
	@Keyword
	def void assignTestAccess(boolean runErrorValidation, boolean testAccessAlreadyAssigned, String firstname, String lastname){
		// login
		new basics.utils().login(GlobalVariable.ETTA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)
		// adding delay
		WebUI.delay(5)
		// select Test Access Management side navigation item
		WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_Test Access Management'))
		// Click Manual Entry button
		WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_Manual Entry'))
		if (runErrorValidation) {
			// Click Save
			WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_Save'))
			// make sure that all error messages are displayed
			WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/test_access_test_order_number_error_message_label'),
					2)
			WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/test_access_assessment_process_number_error_message_label'),
					2)
			WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/test_access_organization_code_error_message_label'), 2)
			WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/test_access_fis_organization_code_error_message_label'), 2)
			WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/test_access_fis_reference_code_error_message_label'), 2)
			WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/test_access_billing_contact_name_error_message_label'), 2)
			WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/test_access_billing_contact_info_error_message_label'), 2)
			WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/test_access_test_administrator_error_message_label'), 2)
			WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/test_access_invalid_date_error_message_label'), 2)
			WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/test_access_test_error_message_label'), 2)
			// fill out form
			WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Test Order Number_test-order-number'),
					GlobalVariable.TEST_ACCESS_TEST_ORDER_NUMBER)
			WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Assessment Process Number_staffing-pr_2da128'),
					GlobalVariable.TEST_ACCESS_STAFFING_PROCESS_NUMBER)
			WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Organization Code_department-ministry-code'),
					GlobalVariable.TEST_ACCESS_ORGANIZATION_CODE)
			WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_FIS Organization Code_is-org'), 'Org Code')
			WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_FIS Reference Code_is-ref'), 'Ref Code')
			WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Billing Contact Name_billing-contact'),
					'Katalon Contact Name')
			WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Billing Contact Information_billing-c_55fc8e'),
					'katalon.contact.info@email.ca')
			WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/div_Test Administrator(s)_users-dropdown-react-select-dropdown'))
			WebUI.setText(findTestObject('Page_CAT - Business Operations/input_Test Administrator(s)_react-select-18-input'),
					lastname + ', ' + firstname)
			WebUI.sendKeys(findTestObject('Page_CAT - Business Operations/input_Test Administrator(s)_react-select-18-input'),  Keys.chord(Keys.ENTER))
			
			// putting a passed date
			WebUI.click(findTestObject('Page_CAT - Business Operations/expiry_date_day_field_Select'))

			DateUtil dateUtil = new DateUtil();

			Calendar dateBeforeToday = dateUtil.getDateBeforeToday()

			String yearNow = new DecimalFormat("00").format(dateBeforeToday.get(Calendar.YEAR))
			String monthNow = new DecimalFormat("00").format(dateBeforeToday.get(Calendar.MONTH)+1)
			String dayNow = new DecimalFormat("00").format(dateBeforeToday.get(Calendar.DAY_OF_MONTH))
			
			// DAY
			WebUI.setText(findTestObject('Object Repository/Page_CAT - Operations/input_Select_react-select-21-input'), "${dayNow}")
			
			WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Operations/input_Select_react-select-21-input'), Keys.chord(
					Keys.ENTER))

			// MONTH
			WebUI.setText(findTestObject('Object Repository/Page_CAT - Operations/input_Select_react-select-22-input'), "${monthNow}")
			
			WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Operations/input_Select_react-select-22-input'), Keys.chord(
					Keys.ENTER))
			
			// YEAR
			WebUI.setText(findTestObject('Object Repository/Page_CAT - Operations/input_Select_react-select-23-input'), "${yearNow}")
			
			WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Operations/input_Select_react-select-23-input'), Keys.chord(
					Keys.ENTER))
			
			WebUI.click(findTestObject('Page_CAT - Business Operations/test_accesses_first_collapsing_container_button'))
			WebUI.click(findTestObject('Page_CAT - Business Operations/test_accesses_first_test_access_checkbox'))
			// click Save
			WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_Save'))
			
			// make sure that the future date error is displayed
			WebUI.verifyElementPresent(findTestObject('Page_CAT - Business Operations/test_access_future_date_error_message_label'), 2)
			
			// update year field
			Calendar dateAfterToday = dateUtil.getDateAfterToday()
			
			yearNow = new DecimalFormat("00").format(dateAfterToday.get(Calendar.YEAR))
			monthNow = new DecimalFormat("00").format(dateAfterToday.get(Calendar.MONTH)+1)
			dayNow = new DecimalFormat("00").format(dateAfterToday.get(Calendar.DAY_OF_MONTH))
			
			// DAY
			WebUI.setText(findTestObject('Object Repository/Page_CAT - Operations/input_Select_react-select-21-input'), "${dayNow}")
			
			WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Operations/input_Select_react-select-21-input'), Keys.chord(
					Keys.ENTER))

			// MONTH
			WebUI.setText(findTestObject('Object Repository/Page_CAT - Operations/input_Select_react-select-22-input'), "${monthNow}")
			
			WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Operations/input_Select_react-select-22-input'), Keys.chord(
					Keys.ENTER))
			
			// YEAR
			WebUI.setText(findTestObject('Object Repository/Page_CAT - Operations/input_Select_react-select-23-input'), "${yearNow}")
			
			WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Operations/input_Select_react-select-23-input'), Keys.chord(
					Keys.ENTER))

		} else {
			WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Test Order Number_test-order-number'),
					GlobalVariable.TEST_ACCESS_TEST_ORDER_NUMBER)
			WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Assessment Process Number_staffing-pr_2da128'),
					GlobalVariable.TEST_ACCESS_STAFFING_PROCESS_NUMBER)
			WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Organization Code_department-ministry-code'),
					GlobalVariable.TEST_ACCESS_ORGANIZATION_CODE)
			WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_FIS Organization Code_is-org'), 'Org Code')
			WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_FIS Reference Code_is-ref'), 'Ref Code')
			WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Billing Contact Name_billing-contact'),
					'Katalon Contact Name')
			WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Billing Contact Information_billing-c_55fc8e'),
					'katalon.contact.info@email.ca')
			WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/div_Test Administrator(s)_users-dropdown-react-select-dropdown'))
			WebUI.setText(findTestObject('Page_CAT - Business Operations/input_Test Administrator(s)_react-select-18-input'),
					lastname + ", " + firstname)
			WebUI.sendKeys(findTestObject('Page_CAT - Business Operations/input_Test Administrator(s)_react-select-18-input'),  Keys.chord(Keys.ENTER))
			// putting an invalid date
			WebUI.click(findTestObject('Page_CAT - Business Operations/expiry_date_day_field_Select'))
			WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/div_01'))
			WebUI.delay(1)
			WebUI.click(findTestObject('Page_CAT - Business Operations/expiry_date_month_field_Select'))
			WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/div_02'))
			WebUI.delay(1)
			WebUI.click(findTestObject('Page_CAT - Business Operations/expiry_date_year_field_Select'))
			WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/div_2026'))
			WebUI.delay(1)
			WebUI.click(findTestObject('Page_CAT - Business Operations/test_accesses_first_collapsing_container_button'))
			WebUI.click(findTestObject('Page_CAT - Business Operations/test_accesses_first_test_access_checkbox'))
		}
		// click Save
		WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_Save'))
		// click Cancel
		WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/test_access_confirmation_button_cancel'))
		// click Save
		WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_Save'))

		// making sure that the right data has been input
		if (!(WebUI.getText(findTestObject('Object Repository/Page_CAT - Business Operations/test_accesses_confirmation_names_and_contact_details')).contains(
		GlobalVariable.TA_USER_EMAIL_ADDRESS)) && !(WebUI.getText(findTestObject('Object Repository/Page_CAT - Business Operations/test_accesses_confirmation_names_and_contact_details')).contains(
		GlobalVariable.TA_USER_TWO_EMAIL_ADDRESS))) {
			KeywordUtil.markFailedAndStop('One of The following text has not been found: ' + GlobalVariable.TA_USER_EMAIL_ADDRESS + ', ' + GlobalVariable.TA_USER_TWO_EMAIL_ADDRESS)
		}
		// get test name
		//		String testName = new database.DataGetting().getFirstTestAccessDefinition("en_name")
		//		if (!(WebUI.getText(findTestObject('Object Repository/Page_CAT - Business Operations/test_accesses_confirmation_tests')).contains(
		//		testName))) {
		//			KeywordUtil.markFailedAndStop('The following text has not been found: ' + testName)
		//		}
		if (!(WebUI.getText(findTestObject('Object Repository/Page_CAT - Business Operations/test_accesses_confirmation_test_order_info')).contains(
		GlobalVariable.TEST_ACCESS_TEST_ORDER_NUMBER))) {
			KeywordUtil.markFailedAndStop('The following text has not been found: ' + GlobalVariable.TEST_ACCESS_TEST_ORDER_NUMBER)
		}
		if (!(WebUI.getText(findTestObject('Object Repository/Page_CAT - Business Operations/test_accesses_confirmation_test_order_info')).contains(
		GlobalVariable.TEST_ACCESS_STAFFING_PROCESS_NUMBER))) {
			KeywordUtil.markFailedAndStop('The following text has not been found: ' + GlobalVariable.TEST_ACCESS_STAFFING_PROCESS_NUMBER)
		}
		if (!(WebUI.getText(findTestObject('Object Repository/Page_CAT - Business Operations/test_accesses_confirmation_test_order_info')).contains(
		GlobalVariable.TEST_ACCESS_ORGANIZATION_CODE))) {
			KeywordUtil.markFailedAndStop('The following text has not been found: ' + GlobalVariable.TEST_ACCESS_ORGANIZATION_CODE)
		}
		// click Save
		WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/test_access_confirmation_button_Save'))
		// if testAccessAlreadyAssigned is set to true
		if (testAccessAlreadyAssigned) {
			//make sure that the expected error message is displayed
			WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/assign_test_accesses_permission_already_granted_error'), 2)
			// update Test Order Number
			WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Test Order Number_test-order-number'),
					GlobalVariable.TEST_ACCESS_TEST_ORDER_NUMBER_2)
			// click Save
			WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_Save'))
			// click Save
			WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/test_access_confirmation_button_Save'))
		}
		// click OK
		WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/button_OK'), 5)
		WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_OK'))



		// --------------------------
		// MODIFY ACTIVE TEST ACCESS
		// --------------------------


		// select Active Test Accesses tab
		WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/a_Active Test Accesses'), 10)
		WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/a_Active Test Accesses'))
		// wait for loading to be done
		WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/active_test_accesses_first_row_test_administrator'), 25)
		// adding delay
		WebUI.delay(5)
		// if testAccessAlreadyAssigned is set to true
		if (testAccessAlreadyAssigned) {
			// search for KATALON2 test order number
			WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_active-test-accesses-search-bar'),
					GlobalVariable.TEST_ACCESS_TEST_ORDER_NUMBER_2)
		} else {
			// search for katalon ta user
			WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_active-test-accesses-search-bar'),
					lastname + ', ' + firstname)
		}
		// simulate "Enter" key press
		WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_active-test-accesses-search-bar'),
				Keys.chord(Keys.ENTER))
		// wait for loading to be done
		WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/active_test_accesses_first_row_test_administrator'), 25)
		// adding delay
		WebUI.delay(4)
		// click Modify
		WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/active_test_accesses_button_Modify'))
		// close popup
		WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/active_test_accesses_popup_button_Close'))
	}
}
