package basics

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.Keys as Keys

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class utils {

	/**
	 * open browser and navigate to home page
	 */
	@Keyword
	def void NavigateToHomePage(){
		// opening browser
		WebUI.openBrowser('')
		// maximizing window
		WebUI.maximizeWindow()
		// navigating to main URL
		WebUI.navigateToUrl(GlobalVariable.MAIN_URL)
		// selecting English language (splash page)
		WebUI.click(findTestObject('Object Repository/Page_OCCAT - CFPPSC/button_English'))
	}


	/**
	 * login functionality
	 */
	@Keyword
	def void login(String username, String encryptedPassword){
		// fill out login form (using wrong credentials)
		WebUI.setText(findTestObject('Object Repository/Page_CAT - Home/input_Email Address_username'), username)
		WebUI.setEncryptedText(findTestObject('Object Repository/Page_CAT - Home/input_Password_password'), encryptedPassword)
		// click Login
		WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Login'))
		// making sure that the user has logged in successfully
		// check if the Login button is not displayed in the top navigation bar (expected_
		boolean confirmLoginSuccessful = WebUI.verifyElementNotPresent(findTestObject('Object Repository/Page_CAT - Home/button_Login_in_top_nav_bar'), 2)
		// checking if the user is logged in
		if (!confirmLoginSuccessful) {
			// if not successfully logged in, marking test as failed and stop it
			KeywordUtil.markFailedAndStop("Unable to login with " + username + " account")
		} // adding a 2 seconds delay
		WebUI.delay(2)
	}
	/**
	 * logout functionality
	 */
	@Keyword
	def void logout(){
		// click MENU
		WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_MENU'))
		// wait for logout link to be visible
		WebUI.waitForElementVisible(findTestObject('Object Repository/Page_CAT - Home/button_Logout'), 5)
		// click Logout
		WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Logout'))
		// add delay to make sure that the logout action is called
		WebUI.delay(2)
		// navigating to main URL
		WebUI.navigateToUrl(GlobalVariable.MAIN_URL)
		// selecting English language (splash page)
		WebUI.click(findTestObject('Object Repository/Page_OCCAT - CFPPSC/button_English'))
	}

	/**
	 * logout and close functionality
	 */
	@Keyword
	def void logoutAndClose(){
		// click MENU
		WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_MENU'))
		// wait for logout link to be visible
		WebUI.waitForElementVisible(findTestObject('Object Repository/Page_CAT - Home/button_Logout'), 5)
		// click Logout
		WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Logout'))
		// add delay to make sure that the logout action is called
		WebUI.delay(2)
		// closing browser
		WebUI.closeBrowser()
	}

	/**
	 * create account functionality (cannot be logged in when calling this function)
	 */
	@Keyword
	def void createNewAccount(String firstName, String lastName, String emailAddress, String encryptedPassword, boolean runErrorValidation){
		// checking if this account already exists
		boolean accountAlreadyExists = (new database.DataValidation()).accountAlreadyExists(emailAddress)
		// if the account already exists
		if (accountAlreadyExists) {
			// deleting all account related data
			(new database.DataDeletion()).deleteUser(emailAddress)
		}
		// navigating to main URL
		WebUI.navigateToUrl(GlobalVariable.MAIN_URL)
		// click Create an account tab
		WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Create an account'))
		// if runErrorValidation is true
		if (runErrorValidation) {
			// click Create account button
			WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Create account'))

			// make sure that respective error messages are displayed
			WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Error CAT - Home/label_Must be a valid First Name'), 2)
			WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Error CAT - Home/label_Must be a valid Last Name'), 2)
			WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Error CAT - Home/label_Must be a valid Date'), 2)
			WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Error CAT - Home/label_Must be a valid Email Address'), 2)
			String passwordRequirements = WebUI.getText(findTestObject('Object Repository/Page_Error CAT - Home/label_Password requirements'))
			if (!passwordRequirements.contains(WebUI.getText(findTestObject('Object Repository/Page_Error CAT - Home/p_Your password must contain the following')))) {
				KeywordUtil.markFailedAndStop("Password requirements does not contains the specified text")
			}
			if (!passwordRequirements.contains(WebUI.getText(findTestObject('Object Repository/Page_Error CAT - Home/li_At least one uppercase letter')))) {
				KeywordUtil.markFailedAndStop("Password requirements does not contains the specified text")
			}
			if (!passwordRequirements.contains(WebUI.getText(findTestObject('Object Repository/Page_Error CAT - Home/li_At least one lowercase letter_first')))) {
				KeywordUtil.markFailedAndStop("Password requirements does not contains the specified text")
			}
			if (!passwordRequirements.contains(WebUI.getText(findTestObject('Object Repository/Page_Error CAT - Home/li_At least one number_first')))) {
				KeywordUtil.markFailedAndStop("Password requirements does not contains the specified text")
			}
			if (!passwordRequirements.contains(WebUI.getText(findTestObject('Object Repository/Page_Error CAT - Home/li_At least one special character_first')))) {
				KeywordUtil.markFailedAndStop("Password requirements does not contains the specified text")
			}
			if (!passwordRequirements.contains(WebUI.getText(findTestObject('Object Repository/Page_Error CAT - Home/li_Minimum and maximum of characters_first')))) {
				KeywordUtil.markFailedAndStop("Password requirements does not contains the specified text")
			}
			// fill out form
			WebUI.setText(findTestObject('Object Repository/Page_Error CAT - Home/input_First Name_first-name-field'), firstName)
			WebUI.setText(findTestObject('Object Repository/Page_Error CAT - Home/input_Last Name_last-name-field'), lastName)

			// Day
			WebUI.setText(findTestObject('Object Repository/Page_CAT - Home/input_Select_react-select-2-input'), '31')
			WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Home/input_Select_react-select-2-input'), Keys.chord(Keys.ENTER))

			// Month
			WebUI.setText(findTestObject('Object Repository/Page_CAT - Home/input_Select_react-select-3-input'), '02')
			WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Home/input_Select_react-select-3-input'), Keys.chord(Keys.ENTER))

			// Year
			WebUI.setText(findTestObject('Object Repository/Page_CAT - Home/input_Select_react-select-4-input'), '2000')
			WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Home/input_Select_react-select-4-input'), Keys.chord(Keys.ENTER))

			WebUI.setText(findTestObject('Object Repository/Page_Error CAT - Home/input_Valid_psrs-applicant-id'), 'A123')
			WebUI.setText(findTestObject('Object Repository/Page_Error CAT - Home/input_Valid_pri-or-military-nbr-field'), '123456')
			WebUI.setText(findTestObject('Object Repository/Page_Error CAT - Home/input_Email Address_email-address-field'), 'invalid.email.ca')
			WebUI.setEncryptedText(findTestObject('Object Repository/Page_Error CAT - Home/input_Minimum of 8 characters and maximum o_aa717e'),
					'lXbL6rWvZZo=')
			WebUI.click(findTestObject('Object Repository/Page_Error CAT - Home/input_Valid_privacy-notice-checkbox'))
			// click create account button
			WebUI.click(findTestObject('Object Repository/Page_Error CAT - Home/button_Create account'))
			// make sure that respective error messages are displayed
			WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Error CAT - Home/label_Must be a valid Date'), 2)
			WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Error CAT - Home/label_Must be a valid PSRS Applicant ID'), 2)
			WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Error CAT - Home/label_Must be a valid PRI or Service Number'), 2)
			WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Error CAT - Home/label_Must be a valid Email Address'), 2)
			passwordRequirements = WebUI.getText(findTestObject('Object Repository/Page_Error CAT - Home/label_Password requirements'))
			if (!passwordRequirements.contains(WebUI.getText(findTestObject('Object Repository/Page_Error CAT - Home/p_Your password must contain the following')))) {
				KeywordUtil.markFailedAndStop("Password requirements does not contains the specified text")
			}
			if (!passwordRequirements.contains(WebUI.getText(findTestObject('Object Repository/Page_Error CAT - Home/li_At least one lowercase letter_second')))) {
				KeywordUtil.markFailedAndStop("Password requirements does not contains the specified text")
			}
			if (!passwordRequirements.contains(WebUI.getText(findTestObject('Object Repository/Page_Error CAT - Home/li_At least one number_second')))) {
				KeywordUtil.markFailedAndStop("Password requirements does not contains the specified text")
			}
			if (!passwordRequirements.contains(WebUI.getText(findTestObject('Object Repository/Page_Error CAT - Home/li_At least one special character_second')))) {
				KeywordUtil.markFailedAndStop("Password requirements does not contains the specified text")
			}
			if (!passwordRequirements.contains(WebUI.getText(findTestObject('Object Repository/Page_Error CAT - Home/li_Minimum and maximum of characters_second')))) {
				KeywordUtil.markFailedAndStop("Password requirements does not contains the specified text")
			}
			WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Error CAT - Home/label_Your password confirmation must match your New Password'), 2)

			// update form

			// Day
			WebUI.setText(findTestObject('Object Repository/Page_CAT - Home/input_Select_react-select-2-input'), '20')
			WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Home/input_Select_react-select-2-input'), Keys.chord(Keys.ENTER))

			WebUI.setText(findTestObject('Object Repository/Page_Error CAT - Home/input_Valid_psrs-applicant-id'), 'A123456')
			WebUI.setText(findTestObject('Object Repository/Page_Error CAT - Home/input_Valid_pri-or-military-nbr-field'), '12345679')
			WebUI.setText(findTestObject('Object Repository/Page_Error CAT - Home/input_Email Address_email-address-field'), emailAddress)
			WebUI.setEncryptedText(findTestObject('Object Repository/Page_Error CAT - Home/input_Confirm Password_password-confirmation-field'),
					'omPcHkjY2Xs=')
			WebUI.setEncryptedText(findTestObject('Object Repository/Page_Error CAT - Home/input_Minimum of 8 characters and maximum o_aa717e'),
					'omPcHkjY2Xs=')
			// click create account button
			WebUI.click(findTestObject('Object Repository/Page_Error CAT - Home/button_Create account'))
			// make sure that respective error messages are displayed
			passwordRequirements = WebUI.getText(findTestObject('Object Repository/Page_Error CAT - Home/label_Password requirements'))
			if (!passwordRequirements.contains(WebUI.getText(findTestObject('Object Repository/Page_Error CAT - Home/p_Your password must contain the following')))) {
				KeywordUtil.markFailedAndStop("Password requirements does not contains the specified text")
			}
			if (!passwordRequirements.contains(WebUI.getText(findTestObject('Object Repository/Page_Error CAT - Home/li_At least one number_third')))) {
				KeywordUtil.markFailedAndStop("Password requirements does not contains the specified text")
			}
			if (!passwordRequirements.contains(WebUI.getText(findTestObject('Object Repository/Page_Error CAT - Home/li_At least one special character_third')))) {
				KeywordUtil.markFailedAndStop("Password requirements does not contains the specified text")
			}
			if (!passwordRequirements.contains(WebUI.getText(findTestObject('Object Repository/Page_Error CAT - Home/li_Minimum and maximum of characters_third')))) {
				KeywordUtil.markFailedAndStop("Password requirements does not contains the specified text")
			}
			// update form
			WebUI.setEncryptedText(findTestObject('Object Repository/Page_Error CAT - Home/input_Minimum of 8 characters and maximum o_aa717e'),
					'rU9mizVyH4o=')
			WebUI.setEncryptedText(findTestObject('Object Repository/Page_Error CAT - Home/input_Confirm Password_password-confirmation-field'),
					'rU9mizVyH4o=')
			// click create account button
			WebUI.click(findTestObject('Object Repository/Page_Error CAT - Home/button_Create account'))
			// make sure that respective error messages are displayed
			passwordRequirements = WebUI.getText(findTestObject('Object Repository/Page_Error CAT - Home/label_Password requirements'))
			if (!passwordRequirements.contains(WebUI.getText(findTestObject('Object Repository/Page_Error CAT - Home/p_Your password must contain the following')))) {
				KeywordUtil.markFailedAndStop("Password requirements does not contains the specified text")
			}
			if (!passwordRequirements.contains(WebUI.getText(findTestObject('Object Repository/Page_Error CAT - Home/li_At least one special character_fourth')))) {
				KeywordUtil.markFailedAndStop("Password requirements does not contains the specified text")
			}
			if (!passwordRequirements.contains(WebUI.getText(findTestObject('Object Repository/Page_Error CAT - Home/li_Minimum and maximum of characters_fourth')))) {
				KeywordUtil.markFailedAndStop("Password requirements does not contains the specified text")
			}
			// update form
			WebUI.setEncryptedText(findTestObject('Object Repository/Page_Error CAT - Home/input_Minimum of 8 characters and maximum o_aa717e'),
					'dqcbUR4IQEw=')
			WebUI.setEncryptedText(findTestObject('Object Repository/Page_Error CAT - Home/input_Confirm Password_password-confirmation-field'),
					'dqcbUR4IQEw=')
			// click create account button
			WebUI.click(findTestObject('Object Repository/Page_Error CAT - Home/button_Create account'))
			// make sure that respective error messages are displayed
			passwordRequirements = WebUI.getText(findTestObject('Object Repository/Page_Error CAT - Home/label_Password requirements'))
			if (!passwordRequirements.contains(WebUI.getText(findTestObject('Object Repository/Page_Error CAT - Home/p_Your password must contain the following')))) {
				KeywordUtil.markFailedAndStop("Password requirements does not contains the specified text")
			}
			if (!passwordRequirements.contains(WebUI.getText(findTestObject('Object Repository/Page_Error CAT - Home/li_Minimum and maximum of characters_fifth')))) {
				KeywordUtil.markFailedAndStop("Password requirements does not contains the specified text")
			}
			// testing the display field functionality for password fields
			// fill out all password fields
			WebUI.setEncryptedText(findTestObject('Object Repository/Page_Error CAT - Home/input_Minimum of 8 characters and maximum o_aa717e'),
					'Rj/cwQdeR3hJ4O0dMLksCTTbX9A2/Cri')
			WebUI.setEncryptedText(findTestObject('Object Repository/Page_Error CAT - Home/input_Confirm Password_password-confirmation-field'),
					'Rj/cwQdeR3hJ4O0dMLksCTTbX9A2/Cri')

			// click on the view password button (for all password fields)
			WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_password_visibility'))
			WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_confirm_password_visibility'))

			// make sure that all fields are now visible
			WebUI.verifyEqual(WebUI.getAttribute(findTestObject('Object Repository/Page_Error CAT - Home/input_Minimum of 8 characters and maximum o_aa717e'),
					'value'), 'ShowPasswordTest')
			WebUI.verifyEqual(WebUI.getAttribute(findTestObject('Object Repository/Page_Error CAT - Home/input_Confirm Password_password-confirmation-field'),
					'value'), 'ShowPasswordTest')

			// click on the view password button (for all password fields) in order to hide back the characters
			WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_password_visibility'))
			WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_confirm_password_visibility'))

			// clear all password fields
			WebUI.sendKeys(findTestObject('Object Repository/Page_Error CAT - Home/input_Minimum of 8 characters and maximum o_aa717e'), Keys.chord(
					Keys.CONTROL, 'a'))

			WebUI.sendKeys(findTestObject('Object Repository/Page_Error CAT - Home/input_Minimum of 8 characters and maximum o_aa717e'), Keys.chord(
					Keys.BACK_SPACE))

			WebUI.sendKeys(findTestObject('Object Repository/Page_Error CAT - Home/input_Confirm Password_password-confirmation-field'),
					Keys.chord(Keys.CONTROL, 'a'))

			WebUI.sendKeys(findTestObject('Object Repository/Page_Error CAT - Home/input_Confirm Password_password-confirmation-field'),
					Keys.chord(Keys.BACK_SPACE))

			// update form
			WebUI.setEncryptedText(findTestObject('Object Repository/Page_Error CAT - Home/input_Minimum of 8 characters and maximum o_aa717e'),
					encryptedPassword)
			WebUI.setEncryptedText(findTestObject('Object Repository/Page_Error CAT - Home/input_Confirm Password_password-confirmation-field'),
					encryptedPassword)
			// runErrorValidation is false
		} else {
			// fill out form with the right data
			WebUI.setText(findTestObject('Object Repository/Page_Error CAT - Home/input_First Name_first-name-field'), firstName)
			WebUI.setText(findTestObject('Object Repository/Page_Error CAT - Home/input_Last Name_last-name-field'), lastName)

			// Day
			WebUI.setText(findTestObject('Object Repository/Page_CAT - Home/input_Select_react-select-2-input'), '20')
			WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Home/input_Select_react-select-2-input'), Keys.chord(Keys.ENTER))

			// Month
			WebUI.setText(findTestObject('Object Repository/Page_CAT - Home/input_Select_react-select-3-input'), '02')
			WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Home/input_Select_react-select-3-input'), Keys.chord(Keys.ENTER))

			// Year
			WebUI.setText(findTestObject('Object Repository/Page_CAT - Home/input_Select_react-select-4-input'), '2000')
			WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Home/input_Select_react-select-4-input'), Keys.chord(Keys.ENTER))


			WebUI.setText(findTestObject('Object Repository/Page_Error CAT - Home/input_Valid_psrs-applicant-id'), 'A123456')
			WebUI.setText(findTestObject('Object Repository/Page_Error CAT - Home/input_Valid_pri-or-military-nbr-field'), '12345679')
			WebUI.setText(findTestObject('Object Repository/Page_Error CAT - Home/input_Email Address_email-address-field'), emailAddress)
			WebUI.setEncryptedText(findTestObject('Object Repository/Page_Error CAT - Home/input_Minimum of 8 characters and maximum o_aa717e'),
					encryptedPassword)
			WebUI.setEncryptedText(findTestObject('Object Repository/Page_Error CAT - Home/input_Confirm Password_password-confirmation-field'),
					encryptedPassword)
			WebUI.click(findTestObject('Object Repository/Page_Error CAT - Home/input_Valid_privacy-notice-checkbox'))
		}

		// click create account button
		WebUI.click(findTestObject('Object Repository/Page_Error CAT - Home/button_Create account'))
		// make sure that you get the welcome message (proof of log in successful)
		WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Home/h1_Welcome_message_on_login'), 2)
	}

}