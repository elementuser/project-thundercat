package database

import java.sql.*

import com.kms.katalon.core.annotation.Keyword

public class DataGetting {
	/**
	 * getting the amount of active sample tests
	 */
	@Keyword
	def Integer amountOfSampleTests(){
		// open database connection
		Connection connection = (new database.DBConnection()).connection()
		// get amount of active sample tests
		ResultSet resultSet = (new database.DBConnection()).executeQuery(connection, "SELECT COUNT(*) FROM cms_models_testdefinition WHERE is_public = 1 AND active = 1")
		resultSet.next()
		Integer amountOfSampleTests = resultSet.getString(1) as Integer
		// close database connection
		(new database.DBConnection()).closeConnection(connection)
		return amountOfSampleTests
	}

	/**
	 * getting the first test access definition (getting the first active non public test based on parent code ascending order)
	 */
	@Keyword
	def Object getFirstTestAccessDefinition(String columnName){
		// open database connection
		Connection connection = (new database.DBConnection()).connection()
		// get first active non public test definition
		ResultSet resultSet = (new database.DBConnection()).executeQuery(connection, "SELECT TOP 1 * FROM cms_models_testdefinition WHERE is_public = 0 AND active = 1 AND archived = 0 ORDER BY parent_code, id ASC")
		resultSet.next()
		// getting index of specified column
		Integer columnIndex = resultSet.findColumn(columnName) as Integer
		// getting value of found index
		String columnValue = resultSet.getString(columnIndex) as String
		// close database connection
		(new database.DBConnection()).closeConnection(connection)
		return columnValue
	}

	/**
	 * getting the user permission definition based on provided permission's codename
	 */
	@Keyword
	def Object getUserPermissionDefinitionData(String codename, String columnName){
		// open database connection
		Connection connection = (new database.DBConnection()).connection()
		// get permission based on provided codename
		ResultSet resultSet = (new database.DBConnection()).executeQuery(connection, "SELECT * FROM user_management_models_custompermissions WHERE codename = '${codename}'")
		resultSet.next()
		// getting index of specified column
		Integer columnIndex = resultSet.findColumn(columnName) as Integer
		// getting value of found index
		String columnValue = resultSet.getString(columnIndex) as String
		// close database connection
		(new database.DBConnection()).closeConnection(connection)
		return columnValue
	}
	/**
	 * getting specified user password reset token
	 */
	@Keyword
	def Object getUserPasswordResetToken(String username){
		// open database connection
		Connection connection = (new database.DBConnection()).connection()
		// get password reset token
		ResultSet resultSet = (new database.DBConnection()).executeQuery(connection, "SELECT [key] FROM django_rest_passwordreset_resetpasswordtoken WHERE user_id = (SELECT id FROM user_management_models_user WHERE username = '${username}')")
		resultSet.next()
		String passwordResetToken = resultSet.getString(1) as String
		// close database connection
		(new database.DBConnection()).closeConnection(connection)
		return passwordResetToken
	}
}
