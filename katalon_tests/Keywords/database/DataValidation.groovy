package database

import java.sql.*

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.util.KeywordUtil

public class DataValidation {
	/**
	 * checking if a specified account already exists
	 */
	@Keyword
	def boolean accountAlreadyExists(String username){
		// initialize alreadyExists boolean
		boolean alreadyExists = false
		// open database connection
		Connection connection = (new database.DBConnection()).connection()
		// get specified user
		ResultSet resultSet = (new database.DBConnection()).executeQuery(connection, "SELECT COUNT(*) FROM user_management_models_user WHERE username = '${username}'")
		resultSet.next()
		Integer count = resultSet.getString(1) as Integer

		// if count is greater than 0
		if (count > 0) {
			// set alreadyExists to true
			alreadyExists = true
		}

		(new database.DBConnection()).closeConnection(connection)
		// returning boolean value
		return alreadyExists
	}

	/**
	 * checking if a user has a specified permission requested or associated to his account
	 */
	@Keyword
	def boolean userPermissionAlreadyAssociated(String username, String permission){
		// initialize alreadyAssociated boolean
		boolean alreadyAssociated = false
		// open database connection
		Connection connection = (new database.DBConnection()).connection()
		// getting permission ID
		Integer permissionId = null
		try {
			// get permission ID
			ResultSet resultSet1 = (new database.DBConnection()).executeQuery(connection, "SELECT permission_id FROM user_management_models_custompermissions WHERE codename = '${permission}'")
			resultSet1.next()
			permissionId = resultSet1.getString(1) as Integer
		} catch (Exception e) {
			KeywordUtil.markFailedAndStop("The specified permission (" + permission + ") does not exist")
		}
		// get user permission
		ResultSet resultSet2 = (new database.DBConnection()).executeQuery(connection, "SELECT COUNT(*) FROM user_management_models_customuserpermissions WHERE user_id = '${username}' AND permission_id = ${permissionId}")
		resultSet2.next()
		Integer count1 = resultSet2.getString(1) as Integer

		// get user pending permission
		ResultSet resultSet3 = (new database.DBConnection()).executeQuery(connection, "SELECT COUNT(*) FROM user_management_models_permissionrequest WHERE username_id = '${username}' AND permission_requested_id = ${permissionId}")
		resultSet3.next()
		Integer count2 = resultSet3.getString(1) as Integer

		// if count1 or count2 is greater than 0
		if (count1 > 0 || count2 > 0) {
			// set alreadyAssociated to true
			alreadyAssociated = true
		}

		(new database.DBConnection()).closeConnection(connection)
		// returning boolean value
		return alreadyAssociated
	}

	/**
	 * checking if a user has a specified active permission associated to his account
	 */
	@Keyword
	def boolean isUserActivePermissionExist(String username, String permission){
		// initialize activePermissionAlreadyExists boolean
		boolean activePermissionAlreadyExists = false
		// open database connection
		Connection connection = (new database.DBConnection()).connection()
		// getting permission ID
		Integer permissionId = null
		try {
			// get permission ID
			ResultSet resultSet1 = (new database.DBConnection()).executeQuery(connection, "SELECT permission_id FROM user_management_models_custompermissions WHERE codename = '${permission}'")
			resultSet1.next()
			permissionId = resultSet1.getString(1) as Integer
		} catch (Exception e) {
			KeywordUtil.markFailedAndStop("The specified permission (" + permission + ") does not exist")
		}
		// get user permission
		ResultSet resultSet2 = (new database.DBConnection()).executeQuery(connection, "SELECT COUNT(*) FROM user_management_models_customuserpermissions WHERE user_id = '${username}' AND permission_id = ${permissionId}")
		resultSet2.next()
		Integer count = resultSet2.getString(1) as Integer

		// if count1 or count2 is greater than 0
		if (count > 0) {
			// set alreadyAssociated to true
			activePermissionAlreadyExists = true
		}

		(new database.DBConnection()).closeConnection(connection)
		// returning boolean value
		return activePermissionAlreadyExists
	}

	/**
	 * checking if the TA already has a test access (test permission)
	 */
	@Keyword
	def boolean taAlreadyHasTestAccess(String username){
		// initialize alreadyHasTestAccess boolean
		boolean alreadyHasTestAccess = false
		// open database connection
		Connection connection = (new database.DBConnection()).connection()

		// get test access
		ResultSet resultSet2 = (new database.DBConnection()).executeQuery(connection, "SELECT COUNT(*) FROM cms_models_testpermissions WHERE username_id = '${username}'")
		resultSet2.next()
		Integer count = resultSet2.getString(1) as Integer

		// if count1 or count2 is greater than 0
		if (count > 0) {
			// set alreadyAssociated to true
			alreadyHasTestAccess = true
		}

		(new database.DBConnection()).closeConnection(connection)
		// returning boolean value
		return alreadyHasTestAccess
	}
}
