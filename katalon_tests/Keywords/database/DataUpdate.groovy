package database

import java.sql.*

import com.kms.katalon.core.annotation.Keyword

import internal.GlobalVariable


public class DataUpdate {
	/**
	 * resetting specified user locked account data
	 * concerned tables:
	 * 		- user_management_models_user
	 */
	@Keyword
	def boolean resetLockedAccountData(String username){
		// open database connection
		Connection connection = (new database.DBConnection()).connection()
		// update locked account data
		(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "UPDATE user_management_models_user SET locked=0, locked_until=NULL, login_attempts=0 WHERE username = '${username}'")
		// close connection
		(new database.DBConnection()).closeConnection(connection)
	}

	/**
	 * updating test access expiry date
	 * concerned tables:
	 * 		- cms_models_testpermissions
	 */
	@Keyword
	def boolean updateTestPermissionExpiryDate(String testOrderNumber, String newExpiryDate){
		// open database connection
		Connection connection = (new database.DBConnection()).connection()
		// update locked account data
		(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "UPDATE cms_models_testpermissions SET expiry_date = '${newExpiryDate}' WHERE username_id = '${GlobalVariable.TA_USER_EMAIL_ADDRESS}' AND test_order_number = '${testOrderNumber}'")
		// close connection
		(new database.DBConnection()).closeConnection(connection)
	}
}
