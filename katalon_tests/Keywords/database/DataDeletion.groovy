package database

import java.sql.*

import com.kms.katalon.core.annotation.Keyword

import internal.GlobalVariable

public class DataDeletion {
	/**
	 * deleting specified user (including all user related data)
	 * concerned tables:
	 *		- user_management_models_user
	 *		- user_management_models_historicaluser
	 *		- authtoken_token
	 *		- user_management_models_accommodations
	 *		- user_management_models_historicalaccommodations
	 *		- user_management_models_userextendedprofile
	 *		- user_management_models_historicaluserextendedprofile
	 *		- user_management_models_userextendedprofileminority
	 *		- user_management_models_historicaluserextendedprofileminority
	 *		- user_management_models_permissionrequest
	 *		- user_management_models_historicalpermissionrequest
	 *		- user_management_models_customuserpermissions
	 *		- user_management_models_historicalcustomuserpermissions
	 *		- cms_models_testpermissions
	 *		- cms_models_historicaltestpermissions
	 *		- custom_models_channelspresence
	 *		- custom_models_historicalchannelspresence
	 *		- custom_models_assignedtest
	 *		- custom_models_historicalassignedtest
	 *		- custom_models_taactions
	 *		- custom_models_historicaltaactions
	 *		- custom_models_pausetestactions
	 *		- custom_models_historicalpausetestactions
	 *		- custom_models_locktestactions
	 *		- custom_models_historicallocktestactions
	 *		- custom_models_assignedtestsection
	 *		- custom_models_historicalassignedtestsection
	 *		- custom_models_assignedtestsectionaccesstimes
	 *		- custom_models_historicalassignedtestsectionaccesstimes
	 *		- custom_models_testaccesscode
	 *		- custom_models_historicaltestaccesscode
	 *		- django_rest_passwordreset_resetpasswordtoken
	 *		- user_management_models_userpasswordresettracking
	 *		- user_management_models_historicaluserpasswordresettracking
	 */
	@Keyword
	def void deleteUser(String username){
		// open database connection
		Connection connection = (new database.DBConnection()).connection()
		// initializing userId
		Integer userId = 0
		// get specified user
		ResultSet resultSet = (new database.DBConnection()).executeQuery(connection, "SELECT id FROM user_management_models_user WHERE username = '${username}'")
		try {
			resultSet.next()
			userId = resultSet.getString(1) as Integer
		} catch (Exception e) { System.out.println(username + " has not been found in the database") }

		if (userId > 0) {
			// delete user extended profile data
			(new database.DataDeletion().deleteUserExtendedProfileData(username))
			// delete user accommodations history table
			(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM user_management_models_historicalaccommodations WHERE user_id = ${userId}")
			// delete user accommodations
			(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM user_management_models_accommodations WHERE user_id = ${userId}")
			// delete channels related data
			(new database.DataDeletion().deleteCandidateChannelPresence(username))
			// delete test access codes related data in historical table
			(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM custom_models_historicaltestaccesscode WHERE ta_username_id = '${username}'")
			// delete test access codes related data
			(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM custom_models_testaccesscode WHERE ta_username_id = '${username}'")
			// delete assigned tests related data
			(new database.DataDeletion().deleteCandidateAssignedTestsData(username))
			// delete user test permissions
			(new database.DataDeletion().deleteUserTestPermission(username, GlobalVariable.TEST_ACCESS_TEST_ORDER_NUMBER))
			(new database.DataDeletion().deleteUserTestPermission(username, GlobalVariable.TEST_ACCESS_TEST_ORDER_NUMBER_2))
			// delete permission requests history table
			(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM user_management_models_historicalpermissionrequest WHERE username_id = '${username}'")
			// delete permission requests
			(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM user_management_models_permissionrequest WHERE username_id = '${username}'")
			// delete user permissions
			(new database.DataDeletion().deleteUserPermissions(username))
			// delete user auth token
			(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM authtoken_token WHERE user_id = ${userId}")
			// delete user password reset related data
			(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM django_rest_passwordreset_resetpasswordtoken WHERE user_id = ${userId}")
			// delete user password reset tracking related data in historical table
			(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM user_management_models_historicaluserpasswordresettracking WHERE user_id = ${userId}")
			// delete user password reset tracking related data
			(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM user_management_models_userpasswordresettracking WHERE user_id = ${userId}")
			// delete user
			(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM user_management_models_historicaluser WHERE id = ${userId} OR history_user_id = ${userId}")
			// delete user - historicalunsupervisedtestaccesscode
			(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM dbo.custom_models_historicalunsupervisedtestaccesscode WHERE id = ${userId} OR history_user_id = ${userId}")
			// delete user - historicaltestpermissions
			(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM dbo.cms_models_historicaltestpermissions WHERE history_user_id = ${userId}")
			// delete user - historicaltestaccesscode
			(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM dbo.custom_models_historicaltestaccesscode WHERE history_user_id = ${userId}")
			// delete user - historicalaccommodationrequest
			(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM dbo.custom_models_historicalaccommodationrequest WHERE history_user_id = ${userId}")
			// delete user - historicaladditionaltime
			(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM dbo.custom_models_historicaladditionaltime WHERE history_user_id = ${userId}")
			// delete user
			(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM user_management_models_user WHERE id = ${userId}")
		}

		// close database connection
		(new database.DBConnection()).closeConnection(connection)
	}

	/**
	 * deleting specified user extended profile data 
	 * concerned tables:
	 * 		- user_management_models_userextendedprofile
	 * 		- user_management_models_userextendedprofileminority
	 */
	@Keyword
	def void deleteUserExtendedProfileData(String username){
		// open database connection
		Connection connection = (new database.DBConnection()).connection()
		// initializing userId
		Integer userId = 0
		// get specified user
		ResultSet resultSet = (new database.DBConnection()).executeQuery(connection, "SELECT id FROM user_management_models_user WHERE username = '${username}'")
		try {
			resultSet.next()
			userId = resultSet.getString(1) as Integer
		} catch (Exception e) { System.out.println("No user found based on specified username/email address") }

		if (userId > 0) {
			// get user extended profile ID
			ResultSet resultSet2 = (new database.DBConnection()).executeQuery(connection, "SELECT id FROM user_management_models_userextendedprofile WHERE user_id = ${userId}")
			// initializing userExtendedProfileId
			Integer userExtendedProfileId = 0
			try {
				resultSet2.next()
				userExtendedProfileId = resultSet2.getString(1) as Integer
			} catch (Exception e) { System.out.println("No user extended profile ID found related to " + username) }


			if (userExtendedProfileId != 0) {
				// delete user extended profile related in historical table
				(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM user_management_models_historicaluserextendedprofile WHERE user_id = ${userId}")
				// delete user extended profile related
				(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM user_management_models_userextendedprofile WHERE user_id = ${userId}")
			}
		}

		// close database connection
		(new database.DBConnection()).closeConnection(connection)
	}

	/**
	 * deleting specified user permissions (including historical data)
	 * concerned tables:
	 * 		- user_management_models_customuserpermissions
	 * 		- user_management_models_historicalcustomuserpermissions
	 * 		- user_management_models_user
	 */
	@Keyword
	def void deleteUserPermissions(String username){
		// open database connection
		Connection connection = (new database.DBConnection()).connection()
		// updated user verified state
		(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "UPDATE user_management_models_user SET verified = 0 WHERE username = '${username}'")
		// delete user permissions historical data
		(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM user_management_models_historicalcustomuserpermissions WHERE user_id = '${username}'")
		// delete user permissions
		(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM user_management_models_customuserpermissions WHERE user_id = '${username}'")
		// close database connection
		(new database.DBConnection()).closeConnection(connection)
	}

	/**
	 * deleting specified pending user permissions based on provided permission type
	 * concerned tables:
	 * 		- user_management_models_customuserpermissions
	 * 		- user_management_models_permissionrequest
	 */
	@Keyword
	deletePendingUserPermissions(String username, String permissionType){
		// open database connection
		Connection connection = (new database.DBConnection()).connection()

		// initializing permission type id
		Integer permissionTypeId = 0
		// retrieve id of permission type
		ResultSet resultSet = (new database.DBConnection()).executeQuery(connection, "SELECT permission_id FROM user_management_models_custompermissions WHERE codename = '${permissionType}'")
		try {
			resultSet.next()
			permissionTypeId = resultSet.getString(1) as Integer
		} catch (Exception e) { System.out.println("No permission type found based on specified permission type") }
		// updated user verified state
		(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "UPDATE user_management_models_user SET verified = 0 WHERE username = '${username}'")
		// delete pending user permission
		(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM user_management_models_permissionrequest WHERE username_id = '${username}' AND permission_requested_id = '${permissionTypeId}'")
		// close database connection
		(new database.DBConnection()).closeConnection(connection)
	}

	/**
	 * deleting specified pending user permissions based on provided permission type
	 * concerned tables:
	 * 		- user_management_models_customuserpermissions
	 * 		- user_management_models_permissionrequest
	 */
	@Keyword
	deleteAllPendingUserPermissions(String username){
		// open database connection
		Connection connection = (new database.DBConnection()).connection()
		// updated user verified state
		(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "UPDATE user_management_models_user SET verified = 0 WHERE username = '${username}'")
		// delete pending user permission
		(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM user_management_models_permissionrequest WHERE username_id = '${username}'")
		// close database connection
		(new database.DBConnection()).closeConnection(connection)
	}

	/**
	 * deleting specified user test permission based on provided test order number
	 * concerned tables:
	 * 		- cms_models_testpermissions
	 * 		- cms_models_historicaltestpermissions
	 */
	@Keyword
	deleteUserTestPermission(String username, String testOrderNumber){
		// open database connection
		Connection connection = (new database.DBConnection()).connection()
		// delete specified test permission historical data
		(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM cms_models_historicaltestpermissions WHERE username_id = '${username}' and test_order_number = '${testOrderNumber}'")
		// delete specified test permission
		(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM cms_models_testpermissions WHERE username_id = '${username}' and test_order_number = '${testOrderNumber}'")
		// close database connection
		(new database.DBConnection()).closeConnection(connection)
	}

	/**
	 * deleting specified user channel presence
	 * concerned tables:
	 * 		- custom_models_channelspresence
	 */
	@Keyword
	def void deleteCandidateChannelPresence(String username){
		// open database connection
		Connection connection = (new database.DBConnection()).connection()
		// initializing userId
		Integer userId = 0
		// get specified user
		ResultSet resultSet = (new database.DBConnection()).executeQuery(connection, "SELECT id FROM user_management_models_user WHERE username = '${username}'")
		try {
			resultSet.next()
			userId = resultSet.getString(1) as Integer
		} catch (Exception e) { System.out.println("No user found based on specified username/email address") }
		(new database.DBConnection()).closeConnection(connection)
	}

	/**
	 * deleting specified user assigned tests data related
	 * concerned tables:
	 * 		- custom_models_assignedtest
	 * 		- custom_models_taactions
	 * 		- custom_models_pausetestactions
	 * 		- custom_models_locktestactions
	 * 		- custom_models_assignedtestsection
	 * 		- custom_models_assignedtestsectionaccesstimes
	 */
	@Keyword
	deleteCandidateAssignedTestsData(String username){
		// open database connection
		Connection connection = (new database.DBConnection()).connection()
		// delete assigned test section access times data related in historical table
		(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM custom_models_historicalassignedtestsectionaccesstimes WHERE assigned_test_section_id in (SELECT id FROM custom_models_assignedtestsection WHERE assigned_test_id in (SELECT id FROM custom_models_assignedtest WHERE username_id = '${username}'))")
		// delete assigned test section access times data related
		(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM custom_models_assignedtestsectionaccesstimes WHERE assigned_test_section_id in (SELECT id FROM custom_models_assignedtestsection WHERE assigned_test_id in (SELECT id FROM custom_models_assignedtest WHERE username_id = '${username}'))")
		// delete assigned test section data related in historical table
		(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM custom_models_historicalassignedtestsection WHERE assigned_test_id in (SELECT id FROM custom_models_assignedtest WHERE username_id = '${username}') OR history_user_id = (SELECT id FROM user_management_models_user WHERE username = '${username}')")
		// delete assigned test section data related
		(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM custom_models_assignedtestsection WHERE assigned_test_id in (SELECT id FROM custom_models_assignedtest WHERE username_id = '${username}')")
		// delete lock test action data related in historical table
		(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM custom_models_historicallocktestactions WHERE ta_action_id in (SELECT id FROM custom_models_taactions WHERE assigned_test_id in (SELECT id FROM custom_models_assignedtest WHERE username_id = '${username}' OR ta_id = '${username}')) OR history_user_id = (SELECT id FROM user_management_models_user WHERE username = '${username}')")
		// delete lock test action data related
		(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM custom_models_locktestactions WHERE ta_action_id in (SELECT id FROM custom_models_taactions WHERE assigned_test_id in (SELECT id FROM custom_models_assignedtest WHERE username_id = '${username}' OR ta_id = '${username}'))")
		// delete pause test action data related in historical table
		(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM custom_models_historicalpausetestactions WHERE ta_action_id in (SELECT id FROM custom_models_taactions WHERE assigned_test_id in (SELECT id FROM custom_models_assignedtest WHERE username_id = '${username}' OR ta_id = '${username}')) OR history_user_id = (SELECT id FROM user_management_models_user WHERE username = '${username}')")
		// delete pause test action data related
		(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM custom_models_pausetestactions WHERE ta_action_id in (SELECT id FROM custom_models_taactions WHERE assigned_test_id in (SELECT id FROM custom_models_assignedtest WHERE username_id = '${username}' OR ta_id = '${username}'))")
		// delete TA actions data related in historical table
		(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM custom_models_historicaltaactions WHERE assigned_test_id in (SELECT id FROM custom_models_assignedtest WHERE username_id = '${username}' OR ta_id = '${username}') OR history_user_id = (SELECT id FROM user_management_models_user WHERE username = '${username}')")
		// delete TA actions data related
		(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM custom_models_taactions WHERE assigned_test_id in (SELECT id FROM custom_models_assignedtest WHERE username_id = '${username}' OR ta_id = '${username}')")
		// delete ETTA actions data related in historical table
		(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM custom_models_historicalettaactions WHERE assigned_test_id in (SELECT id FROM custom_models_assignedtest WHERE username_id = '${username}' OR ta_id = '${username}')")
		// delete ETTA actions data related
		(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM custom_models_ettaactions WHERE assigned_test_id in (SELECT id FROM custom_models_assignedtest WHERE username_id = '${username}' OR ta_id = '${username}')")
		// delete candidate answers related data in historical candidate multiple choice answers table
		(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM custom_models_historicalcandidatemultiplechoiceanswers WHERE candidate_answers_id in (SELECT id from custom_models_candidateanswers where assigned_test_id in (SELECT id FROM custom_models_assignedtest WHERE username_id = '${username}'))")
		// delete candidate answers related data in candidate multiple choice answers table
		(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM custom_models_candidatemultiplechoiceanswers WHERE candidate_answers_id in (SELECT id from custom_models_candidateanswers where assigned_test_id in (SELECT id FROM custom_models_assignedtest WHERE username_id = '${username}'))")
		// delete candidate answers related data in historical candidate answers table
		(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM custom_models_historicalcandidateanswers WHERE assigned_test_id in (SELECT id FROM custom_models_assignedtest WHERE username_id = '${username}')")
		// delete candidate answers related data in candidate answers table
		(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM custom_models_candidateanswers WHERE assigned_test_id in (SELECT id FROM custom_models_assignedtest WHERE username_id = '${username}')")
		// delete assigned tests data related in historical table
		(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM custom_models_historicalassignedtest WHERE username_id = '${username}' OR ta_id = '${username}'")
		// delete assigned tests data related
		(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM custom_models_assignedtest WHERE username_id = '${username}' OR ta_id = '${username}'")
		// close database connection
		(new database.DBConnection()).closeConnection(connection)
	}

	/**
	 * deleting specified TA test access codes
	 * concerned tables:
	 * 		- custom_models_testaccesscode
	 */
	@Keyword
	deleteTaTestAccessCodes(String username){
		// open database connection
		Connection connection = (new database.DBConnection()).connection()
		// delete test access codes related data
		(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM custom_models_testaccesscode WHERE ta_username_id = '${username}'")
		// close database connection
		(new database.DBConnection()).closeConnection(connection)
	}

	/**
	 * deleting specified user accommodations
	 * concerned tables:
	 * 		- user_management_models_accommodations
	 */
	@Keyword
	deleteUserAccommodations(String username){
		// open database connection
		Connection connection = (new database.DBConnection()).connection()
		// initializing userId
		Integer userId = 0
		// get specified user
		ResultSet resultSet = (new database.DBConnection()).executeQuery(connection, "SELECT id FROM user_management_models_user WHERE username = '${username}'")
		try {
			resultSet.next()
			userId = resultSet.getString(1) as Integer
		} catch (Exception e) { System.out.println("No user found based on specified username/email address") }
		if (userId > 0) {
			// delete test access codes related data
			(new database.DBConnection()).executeDeleteOrUpdateQuery(connection, "DELETE FROM user_management_models_accommodations WHERE user_id = '${userId}'")
		}
		// close database connection
		(new database.DBConnection()).closeConnection(connection)
	}
}
