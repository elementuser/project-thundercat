<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Select_react-select-3-input</name>
   <tag></tag>
   <elementGuidId>96430ac8-93ce-4bf4-8635-a76fec7c2b30</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#react-select-3-input</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='date-picker-month-field-react-select-dropdown']/div/div/div/div/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>73f0681b-3604-4b20-aa37-47833286af56</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocapitalize</name>
      <type>Main</type>
      <value>none</value>
      <webElementGuid>a00ffc32-989e-45fd-a4d4-54d9e64f5302</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>4a3927ba-f848-40cb-9b2c-d6a4e0d68667</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocorrect</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>69648a26-aae5-4472-a58b-d6b15d000d56</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>react-select-3-input</value>
      <webElementGuid>98bc0e61-e9da-4614-a30e-d10b75efd850</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>spellcheck</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>3eabf8cc-d04e-4d1d-9658-55d49a672656</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>724cc389-7048-4fe7-b076-a2e2a14f661f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>80ac1a27-b097-430e-92cb-13d67ee8a6c1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-autocomplete</name>
      <type>Main</type>
      <value>list</value>
      <webElementGuid>3bc2b5b8-571b-4aa1-bd33-4caff51187a4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-labelledby</name>
      <type>Main</type>
      <value>dob-title month-field-selected current-month-value-intro date-picker-month-field-value-label</value>
      <webElementGuid>590d080d-9db5-42cd-987f-a3388850fcd6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;react-select-3-input&quot;)</value>
      <webElementGuid>55ef8fd3-d0f0-46eb-9f59-b701c73ce0fb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='react-select-3-input']</value>
      <webElementGuid>907a13f2-65dd-4ff8-b55d-ea90cbc01147</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='date-picker-month-field-react-select-dropdown']/div/div/div[2]/div/input</value>
      <webElementGuid>8c0de568-225a-42bf-8cf9-d6e95eedac1a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div/div/div/div[2]/div/input</value>
      <webElementGuid>8951b521-fe43-4964-92c3-3e5887bf3a39</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'react-select-3-input' and @type = 'text']</value>
      <webElementGuid>0247809d-a9bf-4fc0-b61e-a560ed590504</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
