<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Select is focused ,type to refine lis_34ea30</name>
   <tag></tag>
   <elementGuidId>9134196a-c795-4c5f-8c6f-d639cc5c8a76</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#react-select-27-input</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//div[@id='date-picker-year-field-react-select-dropdown']/div/div/div[2]/div/input)[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>1aa67658-ef67-4829-beee-c8cfdfd9bef8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocapitalize</name>
      <type>Main</type>
      <value>none</value>
      <webElementGuid>97347fc2-5bac-4ffe-a720-56c8341d66d6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>09ccdcdf-a279-42da-b67f-1ed1eaf74c75</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocorrect</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>eacc1aa6-12a5-4a23-8bd5-0cc02f3406a9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>react-select-27-input</value>
      <webElementGuid>f4274507-821f-4554-894a-21a1e3019c2b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>spellcheck</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>50fd9401-3106-484f-acb9-e4b1f959a106</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>8e9ea41c-bd71-4ebb-85bf-313ad5234e14</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>dd5669ec-6759-4ef6-8a75-0464afc70aa6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-autocomplete</name>
      <type>Main</type>
      <value>list</value>
      <webElementGuid>8b3de498-cb78-4f43-849c-c6abf8bf568f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-labelledby</name>
      <type>Main</type>
      <value>expiry-date-label year-field-selected current-year-value-intro date-picker-year-field-value-label</value>
      <webElementGuid>fe0f0382-fa12-43de-8ff8-f69ac2b3b481</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;react-select-27-input&quot;)</value>
      <webElementGuid>73b2701c-fff3-4c6f-8d91-bfef868f8112</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='react-select-27-input']</value>
      <webElementGuid>5c942552-1829-46ea-a135-0e0dc94e697d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>(//div[@id='date-picker-year-field-react-select-dropdown']/div/div/div[2]/div/input)[2]</value>
      <webElementGuid>dd521ce9-83b8-4db4-bff1-2f8db83d10c2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div[3]/div/div/div/div/div/div/div[2]/div/input</value>
      <webElementGuid>d236df0e-17d0-437a-a2e5-80558b1296ea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'react-select-27-input' and @type = 'text']</value>
      <webElementGuid>1416c309-076c-4798-9a2d-cd300c0b9086</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
