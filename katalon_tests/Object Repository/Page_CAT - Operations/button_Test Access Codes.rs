<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Test Access Codes</name>
   <tag></tag>
   <elementGuidId>6b61f997-6140-41db-87fb-76d71651e85a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//button[@id='4'])[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#navigation-items-section > #4</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>2890a27e-ce95-4d4d-b24b-33997f2951ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>4</value>
      <webElementGuid>fe3ee598-8459-4856-9be8-220023111112</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>b2b8bcc8-263f-4660-8bfa-9bbd977c9dfa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-selected</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>b5860597-2c3c-48eb-a313-27ac948d8d1d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>tab</value>
      <webElementGuid>0f33fa58-0320-4a63-8747-442de86b24a8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-xl-12 col-lg-12 col-md-12 col-sm-12</value>
      <webElementGuid>e2f6dd85-b340-4ea5-b3e6-24bf94af6b9a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-controls</name>
      <type>Main</type>
      <value>id-k0jt9f-12</value>
      <webElementGuid>56bf3e89-8070-4d53-8c70-1747639371fd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Test Access Codes</value>
      <webElementGuid>6064f9ac-71f0-43a2-ac4c-274f190def3f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navigation-items-section&quot;)/button[@id=&quot;4&quot;]</value>
      <webElementGuid>5f34ec0a-4285-4ad4-8610-626f16f043fb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@id='4'])[2]</value>
      <webElementGuid>b5c02c66-104d-4f94-948a-be2e769aaa49</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navigation-items-section']/button[4]</value>
      <webElementGuid>3321deca-9891-4235-afee-c3db5047660e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test Access Management'])[1]/following::button[1]</value>
      <webElementGuid>80c1311b-0943-4a9a-b14c-d6f9dcc0ef8e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Item Bank Administration'])[1]/following::button[2]</value>
      <webElementGuid>6fd0576a-65eb-444d-ad7d-87ab339b7728</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Active Tests'])[1]/preceding::button[1]</value>
      <webElementGuid>49e7c1e6-8045-4f87-97d1-e0faf8c0b67a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='User Look-Up'])[1]/preceding::button[2]</value>
      <webElementGuid>69af4a32-1e7b-4ded-8685-805e1d13473d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Test Access Codes']/parent::*</value>
      <webElementGuid>92d0ae3c-7b25-44fd-8f61-e13b87d2e9aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button[4]</value>
      <webElementGuid>8c7eccef-5fac-424b-a343-7fba85466c13</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = '4' and (text() = 'Test Access Codes' or . = 'Test Access Codes')]</value>
      <webElementGuid>c80441c4-a162-48ff-b8e3-e1faf0bdf0e1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
