<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Primary Email Address_primary-email-a_194327</name>
   <tag></tag>
   <elementGuidId>bd05b791-a5b8-4d27-97c3-1c619e3bba8d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='primary-email-address-input']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#primary-email-address-input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>9495f9f8-58b3-4b4e-9a31-9b1964239421</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>primary-email-address-input</value>
      <webElementGuid>91406c1e-4996-4fcd-839c-ee45fbbc9ad8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>valid-field</value>
      <webElementGuid>a7936d07-f6e5-487b-9e2b-be1457153a58</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-required</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>8cd7789b-c49c-46da-b75e-40e4c345b0b0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>e741ad62-eacf-47e3-924c-5f0a72f0a24f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>katalon.tatwo@email.ca</value>
      <webElementGuid>8523b21b-9d02-4623-8c3d-a9a26f028742</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;primary-email-address-input&quot;)</value>
      <webElementGuid>5a550f46-07ca-4489-9ca3-b674af6f4f81</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='primary-email-address-input']</value>
      <webElementGuid>83bfd4cf-f296-47b1-9e18-91ce5872e8bc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='personal-info-primary-email-address']/div[2]/input</value>
      <webElementGuid>85cf4112-f9c5-4633-8cc2-b49373a93983</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div[2]/input</value>
      <webElementGuid>5a758fd8-160f-4dce-ac5f-612cfde30269</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'primary-email-address-input' and @type = 'text']</value>
      <webElementGuid>7b8059c0-5090-4d2f-927e-4abd7488d93b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
