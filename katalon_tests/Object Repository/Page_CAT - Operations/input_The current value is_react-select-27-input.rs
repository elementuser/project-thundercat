<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_The current value is_react-select-27-input</name>
   <tag></tag>
   <elementGuidId>18ca3430-93d0-4d20-8b67-e0770351880d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='react-select-27-input']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#react-select-27-input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>ba57bf80-bc7a-4d7b-8d5c-79c92ad31e73</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocapitalize</name>
      <type>Main</type>
      <value>none</value>
      <webElementGuid>84265573-c050-4e7f-98da-fd0f8ccce3bb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>9670501b-b43e-46db-b222-7c5c1ef4fb05</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocorrect</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>d33ea743-7f88-4f9d-8b29-0d9c28b33949</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>react-select-27-input</value>
      <webElementGuid>963ba510-fa06-4acc-abde-e370c0e4ff16</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>spellcheck</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>a54cafea-dcd6-4454-a430-785fe829070b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>7641fe8d-89e5-49c5-8d1f-beaad094f5ad</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>7c2e74ad-c4e1-4bc1-96e9-c1fa3e00a023</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-autocomplete</name>
      <type>Main</type>
      <value>list</value>
      <webElementGuid>b5eba0e5-653b-4fcf-af68-ca8254c48487</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-labelledby</name>
      <type>Main</type>
      <value>expiry-date-label year-field-selected current-year-value-intro date-picker-year-field-value-label</value>
      <webElementGuid>e444aaea-d843-4e46-8804-b651182bd94c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;react-select-27-input&quot;)</value>
      <webElementGuid>0ed4d42b-a44f-4add-a7b6-8feff751d7f2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='react-select-27-input']</value>
      <webElementGuid>4d8f71b4-d84a-4c71-8119-a6486e0dae1d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>(//div[@id='date-picker-year-field-react-select-dropdown']/div/div/div[2]/div/input)[2]</value>
      <webElementGuid>ae97c74b-4413-41b3-9dfa-ae63ae3b05a6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div[3]/div/div/div/div/div/div/div[2]/div/input</value>
      <webElementGuid>8923ac4a-4050-48da-b055-aefa61fb1dfe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'react-select-27-input' and @type = 'text']</value>
      <webElementGuid>5a3caf03-d2a7-4f02-8e3c-99b520fd4f1e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
