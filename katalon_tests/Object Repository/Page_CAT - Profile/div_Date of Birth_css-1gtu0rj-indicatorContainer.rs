<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Date of Birth_css-1gtu0rj-indicatorContainer</name>
   <tag></tag>
   <elementGuidId>4a601efc-a9b5-4e85-9bbb-124e662b1671</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='date-picker-day-field-react-select-dropdown']/div/div[2]/div</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;date-picker-day-field-react-select-dropdown&quot;)/div[@class=&quot;css-o1r4wa-control&quot;]/div[@class=&quot;css-1hb7zxy-IndicatorsContainer&quot;]/div[@class=&quot;css-1gtu0rj-indicatorContainer&quot;]</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>427477a9-0606-4c88-afca-5aed0120bfbd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value> css-1gtu0rj-indicatorContainer</value>
      <webElementGuid>c678c0ef-1d89-4609-8565-46d0e8b4117a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-hidden</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>4f0b74ed-566f-415c-af64-849d21bdd934</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;date-picker-day-field-react-select-dropdown&quot;)/div[@class=&quot;css-o1r4wa-control&quot;]/div[@class=&quot;css-1hb7zxy-IndicatorsContainer&quot;]/div[@class=&quot;css-1gtu0rj-indicatorContainer&quot;]</value>
      <webElementGuid>a8f01ceb-fb82-44b5-9c37-9a98505354a5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='date-picker-day-field-react-select-dropdown']/div/div[2]/div</value>
      <webElementGuid>071992ea-214f-4c1a-9c8a-a81b86eb342b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Date of Birth:'])[1]/following::div[17]</value>
      <webElementGuid>8ab0d210-d586-4fc8-9982-0799a40bb7e3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Secondary Email Address (optional):'])[1]/following::div[20]</value>
      <webElementGuid>ebb07ab7-786c-4793-be8f-f4bd64d61664</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Day'])[1]/preceding::div[34]</value>
      <webElementGuid>796eae76-57ac-4da9-905e-ac16e03b0d85</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Day field selected'])[1]/preceding::div[34]</value>
      <webElementGuid>49ee58fe-bf9d-4eb7-b1b5-34d94660b03b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div[2]/div/div/div/div/div/div/div[2]/div</value>
      <webElementGuid>0e919f69-2c4b-4cdc-9e6f-c70ad30fffca</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
