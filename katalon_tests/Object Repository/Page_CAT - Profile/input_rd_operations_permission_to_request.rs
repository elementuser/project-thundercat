<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_rd_operations_permission_to_request</name>
   <tag></tag>
   <elementGuidId>0fc796a2-e490-4cce-a4c2-ae74d60bc124</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#permission-checkbox-0</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*/text()[normalize-space(.)='R&amp;D Operations']/parent::*</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>a8691a33-2f28-4962-85de-65df5df15d30</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>permission-checkbox-0</value>
      <webElementGuid>d4ced85e-2cb1-4a72-b61b-635f6132accb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>permissions-title permission-name-0 permission-description-0</value>
      <webElementGuid>d7c511b6-ec3d-4afb-9fab-6e7b8b80a3db</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-invalid</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>a98ad6f4-ed46-42d9-a563-635907d6706d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>c4847e34-c349-442a-81bd-824ab7e0da85</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;permission-checkbox-0&quot;)</value>
      <webElementGuid>72d0d68f-4af9-42c4-b483-cc526ed21811</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='permission-checkbox-0']</value>
      <webElementGuid>ca6f8bb1-dd4e-4705-9521-cbc45ad497a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='modal-description']/div/div/div/div[2]/div/div/div/div/input</value>
      <webElementGuid>1f8edc30-0a3c-47a1-ab11-d0baf7f1c54a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div/input</value>
      <webElementGuid>e09f2d32-10e2-466f-9e74-8a9a32ac5965</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
