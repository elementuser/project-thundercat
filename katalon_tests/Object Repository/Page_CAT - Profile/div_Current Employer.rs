<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Current Employer</name>
   <tag></tag>
   <elementGuidId>cbefca6b-ab73-460d-a63e-7b663b383b24</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='personal-info-current-employer']/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#personal-info-current-employer > div.col-xl-6.col-lg-6.col-md-6.col-sm-12.col-12</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>ff559935-29de-4f9c-884f-7bd11dc0b324</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12</value>
      <webElementGuid>1858f14b-a3d4-433c-8d2a-6fae14a133eb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Current Employer:</value>
      <webElementGuid>261fd8e5-845b-41fd-9a1f-5e8b0e631f60</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;personal-info-current-employer&quot;)/div[@class=&quot;col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12&quot;]</value>
      <webElementGuid>f7227d43-f549-4d09-bef2-8562460df680</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='personal-info-current-employer']/div</value>
      <webElementGuid>1deb6d02-414b-49c4-b922-c6c95a28b529</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Additional Information (Optional)'])[1]/following::div[3]</value>
      <webElementGuid>590d5230-f315-454d-9743-e95262189b77</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Prefer not to say'])[4]/following::div[5]</value>
      <webElementGuid>fa138835-ad3e-410e-a743-1b072b7e50af</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Please select'])[1]/preceding::div[1]</value>
      <webElementGuid>b8930848-ed71-4bb6-865f-fef06e1230f3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div[2]/div/div</value>
      <webElementGuid>a4e22fe0-ff2f-4b6a-ae32-d53a727b45ae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Current Employer:' or . = 'Current Employer:')]</value>
      <webElementGuid>cac1dce7-8036-4100-b379-f1e0a6243522</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
