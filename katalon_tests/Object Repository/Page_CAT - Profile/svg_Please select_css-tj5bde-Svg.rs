<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>svg_Please select_css-tj5bde-Svg</name>
   <tag></tag>
   <elementGuidId>e6c66f37-89d1-4060-8149-133a7073a718</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Please select'])[6]/following::*[name()='svg'][1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>svg</value>
      <webElementGuid>81a8abec-6d0d-4b16-bf6a-f2cb7fc5cff0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>height</name>
      <type>Main</type>
      <value>20</value>
      <webElementGuid>53db5129-d981-42c6-82bd-9e22d6672f8c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>width</name>
      <type>Main</type>
      <value>20</value>
      <webElementGuid>681895b3-3d2a-47d0-a863-3a0c9d067918</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>viewBox</name>
      <type>Main</type>
      <value>0 0 20 20</value>
      <webElementGuid>aeb0898e-120e-439c-80a1-fdaae3d7b519</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-hidden</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>bc54acae-77bd-4041-80ea-037edcae9392</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>focusable</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>6d2b974a-cdfa-4cc2-bc5c-9dfcbe150f73</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>css-tj5bde-Svg</value>
      <webElementGuid>fd4578d7-b3ec-4bfe-8394-42aed5a50e67</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;personal-info-residence-dropdown-react-select-dropdown&quot;)/div[@class=&quot;css-afx8a7-control&quot;]/div[@class=&quot;css-1hb7zxy-IndicatorsContainer&quot;]/div[@class=&quot;css-1gtu0rj-indicatorContainer&quot;]/svg[@class=&quot;css-tj5bde-Svg&quot;]</value>
      <webElementGuid>ea50c01a-8485-49f3-aeb8-de0f44cf1caf</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Please select'])[6]/following::*[name()='svg'][1]</value>
      <webElementGuid>bbe92c13-dbb6-48f0-856a-099482346f71</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Please select'])[5]/following::*[name()='svg'][1]</value>
      <webElementGuid>3a679e21-64d8-406a-9708-7c165a7bf9e1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Education:'])[1]/preceding::*[name()='svg'][1]</value>
      <webElementGuid>27645b86-f5b7-4803-a1e8-ff8c4b5720cf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Please select'])[7]/preceding::*[name()='svg'][1]</value>
      <webElementGuid>7b7ac159-cf43-4258-8a18-e50f43252bec</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
