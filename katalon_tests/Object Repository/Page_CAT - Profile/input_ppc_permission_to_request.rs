<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_ppc_permission_to_request</name>
   <tag></tag>
   <elementGuidId>63e46dac-4812-442c-85e7-d01546a5b40a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#permission-checkbox-2</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*/text()[normalize-space(.)='PPC R&amp;D Level 1']/parent::*</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>77f5302b-b7cf-4e55-8dfb-aa8f7185d370</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>permission-checkbox-2</value>
      <webElementGuid>93198071-785e-4713-a475-0855e8658f2b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>permissions-title permission-name-2 permission-description-2</value>
      <webElementGuid>732b62b2-6a3e-4817-9f5f-8c66d4f2b537</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-invalid</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>2c9817f9-ebe1-4b09-9ce3-626d031f6e4a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>7641e718-2a9c-43af-992d-345f0b0218cf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;permission-checkbox-2&quot;)</value>
      <webElementGuid>8c686dd7-c403-4d67-a54d-b325b63842b5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='permission-checkbox-2']</value>
      <webElementGuid>1d491f59-3e8b-4dd0-8ad0-0565fe96147c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='modal-description']/div/div/div/div[2]/div/div[3]/div/div/input</value>
      <webElementGuid>827eab92-5792-447c-9ed2-779e9c3e1787</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/input</value>
      <webElementGuid>c3d0fb36-3349-45ca-b49a-713636f342f4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
