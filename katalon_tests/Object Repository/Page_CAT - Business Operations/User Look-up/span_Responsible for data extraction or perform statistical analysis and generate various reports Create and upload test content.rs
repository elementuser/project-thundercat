<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Responsible for data extraction or perform statistical analysis and generate various reports Create and upload test content</name>
   <tag></tag>
   <elementGuidId>5c26596e-7c90-4c6b-8480-536eeabc9f34</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//td[@id='user-rights-permissions-column-2-label']/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#permission-description-0 > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>e8ad7e34-1cd7-442c-a86b-eeedd1f81417</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Responsible for data extraction or perform statistical analysis and generate various reports. Create and upload test content.</value>
      <webElementGuid>bede8e8a-913a-4866-96b4-3efac53c44f5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;permission-description-0&quot;)/span[1]</value>
      <webElementGuid>4d0dd493-928a-4533-b9e7-4af43a24ba42</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='permission-description-0']/span</value>
      <webElementGuid>ac62abf9-5ddb-445d-87b3-03e360cee896</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Description:'])[1]/following::span[1]</value>
      <webElementGuid>40f348df-b81e-479d-8846-79e28bd0a5e7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PPC R&amp;D Level 1'])[1]/following::span[1]</value>
      <webElementGuid>3bb83e3e-3a2f-4070-b898-02aa19cddea4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Status pending:'])[1]/preceding::span[1]</value>
      <webElementGuid>5b45ee5c-3e1a-4d00-8267-d4722f1eddd9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pending'])[1]/preceding::span[1]</value>
      <webElementGuid>e09b555b-8921-4c89-83a5-fbc6682234fa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Responsible for data extraction or perform statistical analysis and generate various reports. Create and upload test content.']/parent::*</value>
      <webElementGuid>a6cb4fcf-4a38-4b17-8bab-38bba4dd242b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/span</value>
      <webElementGuid>e6e1973f-1a6f-4935-a50d-62dcb2df1cc0</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
