<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Search</name>
   <tag></tag>
   <elementGuidId>f657b777-b031-4268-9a01-b856026f98fe</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='permissions-tabs-tabpane-permission-requests']/div/div/div/div/div[2]/div/div/button</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.secondary</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>45bbf744-aa55-4870-8738-9bd17663da68</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>secondary</value>
      <webElementGuid>594719db-decf-43fb-83d0-b0b2101709df</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>Search:</value>
      <webElementGuid>3bddfba3-a4c3-4cab-844f-7ad12d4f07e0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-disabled</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>470edd98-d402-4613-91d3-c9e2d7a006ae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Search:</value>
      <webElementGuid>6f370319-687e-48a0-9dd9-48b104178057</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;permissions-tabs-tabpane-permission-requests&quot;)/div[1]/div[1]/div[1]/div[2]/button[@class=&quot;secondary&quot;]</value>
      <webElementGuid>15bf532d-f74b-4507-99cf-7bfe0e9520d0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='permissions-tabs-tabpane-permission-requests']/div/div/div/div[2]/button</value>
      <webElementGuid>47146348-2231-402d-aef7-b825bdf5c984</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Clear Search'])[1]/following::button[1]</value>
      <webElementGuid>b4eaaddf-3478-4a5b-821f-4dc98cc9769d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search:'])[1]/following::button[2]</value>
      <webElementGuid>f529d39e-5543-4593-9499-6c6b645e9cc0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Display:'])[1]/preceding::button[1]</value>
      <webElementGuid>24af0099-ba28-42eb-828b-f622c7038367</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/div/div/div/div[2]/button</value>
      <webElementGuid>7b3245ee-8d0d-4e36-be75-1b1456c7e79b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
