<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Grant Test Access</name>
   <tag></tag>
   <elementGuidId>b5a6afb1-282e-4d30-8454-b25ef8b64081</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;application/x-www-form-urlencoded&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;billing_contact&quot;,
      &quot;value&quot;: &quot;${billing_contact}&quot;
    },
    {
      &quot;name&quot;: &quot;billing_contact_info&quot;,
      &quot;value&quot;: &quot;${billing_contact_info}&quot;
    },
    {
      &quot;name&quot;: &quot;department_ministry_code&quot;,
      &quot;value&quot;: &quot;${department_ministry_code}&quot;
    },
    {
      &quot;name&quot;: &quot;expiry_date&quot;,
      &quot;value&quot;: &quot;${expiry_date}&quot;
    },
    {
      &quot;name&quot;: &quot;is_org&quot;,
      &quot;value&quot;: &quot;${is_org}&quot;
    },
    {
      &quot;name&quot;: &quot;is_ref&quot;,
      &quot;value&quot;: &quot;${is_ref}&quot;
    },
    {
      &quot;name&quot;: &quot;staffing_process_number&quot;,
      &quot;value&quot;: &quot;${staffing_process_number}&quot;
    },
    {
      &quot;name&quot;: &quot;test_order_number&quot;,
      &quot;value&quot;: &quot;${test_order_number}&quot;
    },
    {
      &quot;name&quot;: &quot;test_ids&quot;,
      &quot;value&quot;: &quot;${test_ids}&quot;
    },
    {
      &quot;name&quot;: &quot;usernames&quot;,
      &quot;value&quot;: &quot;${usernames}&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>x-www-form-urlencoded</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/x-www-form-urlencoded</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>authorization</name>
      <type>Main</type>
      <value>JWT ${authorization}</value>
   </httpHeaderProperties>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${THUNDERCAT_URL}/api/grant-test-permission/</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>'http://localhost:81/oec-cat'</defaultValue>
      <description></description>
      <id>2ef33c7b-4a39-4780-bac4-225f193412e5</id>
      <masked>false</masked>
      <name>THUNDERCAT_URL</name>
   </variables>
   <variables>
      <defaultValue>'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjI4ODg0NTAxLCJqdGkiOiI2ZThiMjQ2NDA1NjE0ZjIxYTRmZGM0ZjM5MjUyYTg3YiIsInVzZXJfaWQiOjQ2LCJ1c2VybmFtZSI6ImthdGFsb24uZXR0YUBlbWFpbC5jYSJ9.FaS88lirsWEGh_g7s3v_TZKR2_4VrHnV12yBM6rkPtQ'</defaultValue>
      <description></description>
      <id>574862ff-cbe0-4e48-aacf-be590003db15</id>
      <masked>false</masked>
      <name>authorization</name>
   </variables>
   <variables>
      <defaultValue>'Katalon Contact Name'</defaultValue>
      <description></description>
      <id>9a77e30b-691f-4d17-94cc-fc932d554c87</id>
      <masked>false</masked>
      <name>billing_contact</name>
   </variables>
   <variables>
      <defaultValue>'katalon.contact.info@email.ca'</defaultValue>
      <description></description>
      <id>da14ba6c-fd56-42c1-a737-4e8296c7056c</id>
      <masked>false</masked>
      <name>billing_contact_info</name>
   </variables>
   <variables>
      <defaultValue>'Org Code'</defaultValue>
      <description></description>
      <id>2e7addb5-6312-453b-9c53-8a64d7accd7b</id>
      <masked>false</masked>
      <name>department_ministry_code</name>
   </variables>
   <variables>
      <defaultValue>'2026-05-13'</defaultValue>
      <description></description>
      <id>f721aa68-d7cb-4fc1-8941-5c9472a1103c</id>
      <masked>false</masked>
      <name>expiry_date</name>
   </variables>
   <variables>
      <defaultValue>'\'Org Code\''</defaultValue>
      <description></description>
      <id>1221fc0b-fe3b-42ab-8968-0dfa7e83ec32</id>
      <masked>false</masked>
      <name>is_org</name>
   </variables>
   <variables>
      <defaultValue>'\'Ref Code\''</defaultValue>
      <description></description>
      <id>af6ee10a-7174-458a-9eab-05fb10a8c3ab</id>
      <masked>false</masked>
      <name>is_ref</name>
   </variables>
   <variables>
      <defaultValue>'2021-01-01123'</defaultValue>
      <description></description>
      <id>5ebb0dc8-7c81-455a-8b06-15af90c1543d</id>
      <masked>false</masked>
      <name>staffing_process_number</name>
   </variables>
   <variables>
      <defaultValue>'KATALON2'</defaultValue>
      <description></description>
      <id>1db72cdd-1ad1-47af-9c17-2f04109a756e</id>
      <masked>false</masked>
      <name>test_order_number</name>
   </variables>
   <variables>
      <defaultValue>'123'</defaultValue>
      <description></description>
      <id>06d70654-b51c-48ff-b1f6-377b2d940f28</id>
      <masked>false</masked>
      <name>test_ids</name>
   </variables>
   <variables>
      <defaultValue>'katalon.ta@email.ca'</defaultValue>
      <description></description>
      <id>c55d7c9e-a060-470a-a08e-6b148dbf41d9</id>
      <masked>false</masked>
      <name>usernames</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
