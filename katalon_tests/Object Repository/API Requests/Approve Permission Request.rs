<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Approve Permission Request</name>
   <tag></tag>
   <elementGuidId>b0f6b18c-d7d0-4f24-843e-3a4a0e391bd4</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;application/x-www-form-urlencoded&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;username&quot;,
      &quot;value&quot;: &quot;${username}&quot;
    },
    {
      &quot;name&quot;: &quot;permission_id&quot;,
      &quot;value&quot;: &quot;${permission_id}&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>x-www-form-urlencoded</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/x-www-form-urlencoded</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>authorization</name>
      <type>Main</type>
      <value>JWT ${authorization}</value>
   </httpHeaderProperties>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${THUNDERCAT_URL}/api/grant-permission/</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>'http://localhost:81/oec-cat'</defaultValue>
      <description></description>
      <id>865e475b-1966-4625-8288-3c56c7ebb18a</id>
      <masked>false</masked>
      <name>THUNDERCAT_URL</name>
   </variables>
   <variables>
      <defaultValue>'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjI4ODY1NTAwLCJqdGkiOiJjNWZiZjEwMDY3Zjg0NDQwODQ2MjQyMDIyMmFjYTNlNSIsInVzZXJfaWQiOjQ2LCJ1c2VybmFtZSI6ImthdGFsb24uZXR0YUBlbWFpbC5jYSJ9.yVX8PO_0LklnIWsEeZfpIOElLDoS6J3RlvYQ8UoXuDc'</defaultValue>
      <description></description>
      <id>b88df379-c10c-47a3-9881-2ee01ca7c300</id>
      <masked>false</masked>
      <name>authorization</name>
   </variables>
   <variables>
      <defaultValue>'katalon.userone@email.ca'</defaultValue>
      <description></description>
      <id>0bfdfc81-aef9-4cdf-aee5-875967681a12</id>
      <masked>false</masked>
      <name>username</name>
   </variables>
   <variables>
      <defaultValue>3</defaultValue>
      <description></description>
      <id>9879a01b-8b24-4f58-8b0a-b9fde5e84fa0</id>
      <masked>false</masked>
      <name>permission_id</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
