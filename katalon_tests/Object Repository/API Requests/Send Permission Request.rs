<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Send Permission Request</name>
   <tag></tag>
   <elementGuidId>b3b8fc05-b144-4a2a-8169-170775502c15</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;application/x-www-form-urlencoded&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;goc_email&quot;,
      &quot;value&quot;: &quot;${goc_email}&quot;
    },
    {
      &quot;name&quot;: &quot;permission_requested&quot;,
      &quot;value&quot;: &quot;${permission_requested}&quot;
    },
    {
      &quot;name&quot;: &quot;pri_or_military_nbr&quot;,
      &quot;value&quot;: &quot;${pri_or_military_nbr}&quot;
    },
    {
      &quot;name&quot;: &quot;rationale&quot;,
      &quot;value&quot;: &quot;${rationale}&quot;
    },
    {
      &quot;name&quot;: &quot;supervisor&quot;,
      &quot;value&quot;: &quot;${supervisor}&quot;
    },
    {
      &quot;name&quot;: &quot;supervisor_email&quot;,
      &quot;value&quot;: &quot;${supervisor_email}&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>x-www-form-urlencoded</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/x-www-form-urlencoded</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>JWT ${authorization}</value>
   </httpHeaderProperties>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${THUNDERCAT_URL}/api/send-permission-request/</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>'http://localhost:81/oec-cat'</defaultValue>
      <description></description>
      <id>85fde1e1-86b5-473a-8dd8-fc79796918f9</id>
      <masked>false</masked>
      <name>THUNDERCAT_URL</name>
   </variables>
   <variables>
      <defaultValue>'candidate@email.ca'</defaultValue>
      <description></description>
      <id>24cd9852-d16b-4ee9-bb61-f797091d7b97</id>
      <masked>false</masked>
      <name>goc_email</name>
   </variables>
   <variables>
      <defaultValue>1</defaultValue>
      <description></description>
      <id>bb1d5119-1ac3-43d1-b0df-7c51d7ab6567</id>
      <masked>false</masked>
      <name>permission_requested</name>
   </variables>
   <variables>
      <defaultValue>'12345679'</defaultValue>
      <description></description>
      <id>5c38b8a6-ea68-44fb-a8b3-7d8edc63a438</id>
      <masked>false</masked>
      <name>pri_or_military_nbr</name>
   </variables>
   <variables>
      <defaultValue>'Katalon Tests'</defaultValue>
      <description></description>
      <id>e827d24b-cfd4-4734-a7b6-0dbaf85f53e5</id>
      <masked>false</masked>
      <name>rationale</name>
   </variables>
   <variables>
      <defaultValue>'Katalon Supervisor'</defaultValue>
      <description></description>
      <id>e2711290-71db-42ff-a5c0-1356464aa95c</id>
      <masked>false</masked>
      <name>supervisor</name>
   </variables>
   <variables>
      <defaultValue>'Katalon.supervisor@email.ca'</defaultValue>
      <description></description>
      <id>26f3a174-35ff-4ae2-a31d-f7963a2ec647</id>
      <masked>false</masked>
      <name>supervisor_email</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>de45656e-dbe9-40e1-99b4-a3e49e0ae51e</id>
      <masked>false</masked>
      <name>authorization</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
