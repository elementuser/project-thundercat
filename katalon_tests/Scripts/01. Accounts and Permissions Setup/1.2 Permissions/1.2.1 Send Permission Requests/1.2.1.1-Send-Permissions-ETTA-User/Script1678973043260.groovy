import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// ========== ETTA PERMISSION REQUEST ==========
// check if the permission is already requested or associated to this account
boolean ettaPermissionAlreadyAssociated = CustomKeywords.'database.DataValidation.userPermissionAlreadyAssociated'(GlobalVariable.ETTA_USER_EMAIL_ADDRESS, 
    GlobalVariable.ETTA_PERMISSION)

// if ettaPermissionAlreadyAssociated is false
if (!(ettaPermissionAlreadyAssociated)) {
    // login
    CustomKeywords.'basics.utils.login'(GlobalVariable.ETTA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

    // send ETTA permission request
    CustomKeywords.'basics.permissions.sendPermissionRequest'(GlobalVariable.ETTA_USER_EMAIL_ADDRESS, GlobalVariable.ETTA_PERMISSION, 
        false)

    // logout
    CustomKeywords.'basics.utils.logout'()
}

// close browser
WebUI.closeBrowser()

