import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// ========== TA PERMISSION REQUEST ==========
// check if the permission is already requested or associated to this account
boolean taPermissionAlreadyAssociated = CustomKeywords.'database.DataValidation.userPermissionAlreadyAssociated'(GlobalVariable.TA_USER_EMAIL_ADDRESS, 
    GlobalVariable.TA_PERMISSION)

// if taPermissionAlreadyAssociated is false
if (!(taPermissionAlreadyAssociated)) {
    // login
    CustomKeywords.'basics.utils.login'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

    // send TA permission request
    CustomKeywords.'basics.permissions.sendPermissionRequest'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.TA_PERMISSION, 
        false)

    // logout
    CustomKeywords.'basics.utils.logout'()
}

// close browser
WebUI.closeBrowser()

