import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// ========== PPC PERMISSION REQUEST ==========
// check if the permission is already requested or associated to this account
boolean ppcPermissionAlreadyAssociated = CustomKeywords.'database.DataValidation.userPermissionAlreadyAssociated'(GlobalVariable.PPC_USER_EMAIL_ADDRESS, 
    GlobalVariable.PPC_PERMISSION)

// if ppcPermissionAlreadyAssociated is false
if (!(ppcPermissionAlreadyAssociated)) {
    // login
    CustomKeywords.'basics.utils.login'(GlobalVariable.PPC_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

    // send PPC permission request
    CustomKeywords.'basics.permissions.sendPermissionRequest'(GlobalVariable.PPC_USER_EMAIL_ADDRESS, GlobalVariable.PPC_PERMISSION, 
        false)

    // logout
    CustomKeywords.'basics.utils.logout'()
}

// close browser
WebUI.closeBrowser()

