import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// ========== APPROVE ETTA PERMISSION ==========
// check if the user already has the permission
alreadyHasEttaPermission = CustomKeywords.'database.DataValidation.isUserActivePermissionExist'(GlobalVariable.ETTA_USER_EMAIL_ADDRESS, 
    GlobalVariable.ETTA_PERMISSION)

// if user doesn't have the permission
if (!(alreadyHasEttaPermission)) {
    // approve etta permission
    CustomKeywords.'basics.permissions.approvePermissionRequest'(GlobalVariable.ETTA_USER_EMAIL_ADDRESS, GlobalVariable.ETTA_USER_FIRST_NAME, 
        GlobalVariable.ETTA_USER_LAST_NAME, true)

    // logout
    CustomKeywords.'basics.utils.logout'()
}

// close browser
WebUI.closeBrowser()

