import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// ========== APPROVE TA 1 PERMISSION ==========
// check if the user already has the permission
alreadyHasTaPermission = CustomKeywords.'database.DataValidation.isUserActivePermissionExist'(GlobalVariable.TA_USER_EMAIL_ADDRESS, 
    GlobalVariable.TA_PERMISSION)

// if user doesn't have the permission
if (!(alreadyHasTaPermission)) {
    // approve ta permission
    CustomKeywords.'basics.permissions.approvePermissionRequest'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.TA_USER_FIRST_NAME, 
        GlobalVariable.TA_USER_LAST_NAME, false)

    // logout
    CustomKeywords.'basics.utils.logout'()
}

// close browser
WebUI.closeBrowser()

