import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// ========== APPROVE TA 2 PERMISSION ==========

// check if the user two already has the permission
alreadyHasTaTwoPermission = CustomKeywords.'database.DataValidation.isUserActivePermissionExist'(GlobalVariable.TA_USER_TWO_EMAIL_ADDRESS, 
    GlobalVariable.TA_PERMISSION)

// if user doesn't have the permission
if (!(alreadyHasTaTwoPermission)) {
    // approve ta permission
    CustomKeywords.'basics.permissions.approvePermissionRequest'(GlobalVariable.TA_USER_TWO_EMAIL_ADDRESS, GlobalVariable.TA_USER_TWO_FIRST_NAME, 
        GlobalVariable.TA_USER_TWO_LAST_NAME, false)

    // logout
    CustomKeywords.'basics.utils.logout'()
}

// ========== APPROVE TA PERMISSION (END) ==========
// close browser
WebUI.closeBrowser()

