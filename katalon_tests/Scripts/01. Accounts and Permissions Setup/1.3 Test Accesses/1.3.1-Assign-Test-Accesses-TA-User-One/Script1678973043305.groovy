import internal.GlobalVariable as GlobalVariable

// check if the specified TA already has a test access
boolean taAlreadyHasTestAccess = CustomKeywords.'database.DataValidation.taAlreadyHasTestAccess'(GlobalVariable.TA_USER_EMAIL_ADDRESS)

// if the TA does already have the test access
if (!(taAlreadyHasTestAccess)) {
    // open browser and navigate to login page
    CustomKeywords.'basics.utils.NavigateToHomePage'()

    // assign test access
    // TODO before  February 1st 2026: Update the expiry date of the assigned test 
    CustomKeywords.'basics.permissions.assignTestAccess'(false, false, 'Katalon', 'TA')

    // logout and close the browser
    CustomKeywords.'basics.utils.logoutAndClose'()
}