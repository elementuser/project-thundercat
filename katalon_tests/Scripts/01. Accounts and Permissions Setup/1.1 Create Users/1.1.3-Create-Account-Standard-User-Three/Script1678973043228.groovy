import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()


// ========== STANDARD USER THREE ==========
// check if Standard User three exists
boolean standardUserThreeAlreadyExists = CustomKeywords.'database.DataValidation.accountAlreadyExists'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

// if the user does not exist
if (!(standardUserThreeAlreadyExists)) {
    // create new account
    CustomKeywords.'basics.utils.createNewAccount'(GlobalVariable.STANDARD_USER_THREE_FIRST_NAME, GlobalVariable.STANDARD_USER_THREE_LAST_NAME, 
        GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD, false)

    // logout
    CustomKeywords.'basics.utils.logout'()
}

// ========== STANDARD USER THREE (END) ==========

// closing browser
WebUI.closeBrowser()

