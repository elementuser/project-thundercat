import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// ========== PPC USER ==========
// check if PPC user exists
boolean ppcUserAlreadyExists = CustomKeywords.'database.DataValidation.accountAlreadyExists'(GlobalVariable.PPC_USER_EMAIL_ADDRESS)

// if the user does not exist
if (!(ppcUserAlreadyExists)) {
    // create new account
    CustomKeywords.'basics.utils.createNewAccount'(GlobalVariable.PPC_USER_FIRST_NAME, GlobalVariable.PPC_USER_LAST_NAME, 
        GlobalVariable.PPC_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD, false)

    // logout
    CustomKeywords.'basics.utils.logout'()
}

// ========== PPC USER (END) ==========

// closing browser
WebUI.closeBrowser()

