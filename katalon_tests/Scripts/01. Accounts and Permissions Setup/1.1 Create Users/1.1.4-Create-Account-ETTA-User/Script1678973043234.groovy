import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// ========== ETTA USER ==========
// check if ETTA user exists
boolean ettaUserAlreadyExists = CustomKeywords.'database.DataValidation.accountAlreadyExists'(GlobalVariable.ETTA_USER_EMAIL_ADDRESS)

// if the user does not exist
if (!(ettaUserAlreadyExists)) {
    // create new account
    CustomKeywords.'basics.utils.createNewAccount'(GlobalVariable.ETTA_USER_FIRST_NAME, GlobalVariable.ETTA_USER_LAST_NAME, 
        GlobalVariable.ETTA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD, false)

    // logout
    CustomKeywords.'basics.utils.logout'()
}

// ========== ETTA USER (END) ==========

// closing browser
WebUI.closeBrowser()

