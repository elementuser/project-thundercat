import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// ========== TA USER 2 ==========
// check if TA Two user exists
boolean taTwoUserAlreadyExists = CustomKeywords.'database.DataValidation.accountAlreadyExists'(GlobalVariable.TA_USER_TWO_EMAIL_ADDRESS)

// if the user does not exist
if (!(taTwoUserAlreadyExists)) {
    // create new account
    CustomKeywords.'basics.utils.createNewAccount'(GlobalVariable.TA_USER_TWO_FIRST_NAME, GlobalVariable.TA_USER_TWO_LAST_NAME, 
        GlobalVariable.TA_USER_TWO_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD, false)

    // logout
    CustomKeywords.'basics.utils.logout'()
}

// closing browser
WebUI.closeBrowser()

