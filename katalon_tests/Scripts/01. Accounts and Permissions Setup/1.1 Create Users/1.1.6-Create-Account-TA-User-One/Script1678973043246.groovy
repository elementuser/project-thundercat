import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// ========== TA USER 1 ==========
// check if TA user exists
boolean taUserAlreadyExists = CustomKeywords.'database.DataValidation.accountAlreadyExists'(GlobalVariable.TA_USER_EMAIL_ADDRESS)

// if the user does not exist
if (!(taUserAlreadyExists)) {
    // create new account
    CustomKeywords.'basics.utils.createNewAccount'(GlobalVariable.TA_USER_FIRST_NAME, GlobalVariable.TA_USER_LAST_NAME, 
        GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD, false)

    // logout
    CustomKeywords.'basics.utils.logout'()
}

// closing browser
WebUI.closeBrowser()

