import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// ========== STANDARD USER TWO ==========
// check if Standard User two exists
boolean standardUserTwoAlreadyExists = CustomKeywords.'database.DataValidation.accountAlreadyExists'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS)

// if the user does not exist
if (!(standardUserTwoAlreadyExists)) {
    // create new account
    CustomKeywords.'basics.utils.createNewAccount'(GlobalVariable.STANDARD_USER_TWO_FIRST_NAME, GlobalVariable.STANDARD_USER_TWO_LAST_NAME, 
        GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD, false)

    // logout
    CustomKeywords.'basics.utils.logout'()
}

// ========== STANDARD USER TWO (END) ==========

// closing browser
WebUI.closeBrowser()

