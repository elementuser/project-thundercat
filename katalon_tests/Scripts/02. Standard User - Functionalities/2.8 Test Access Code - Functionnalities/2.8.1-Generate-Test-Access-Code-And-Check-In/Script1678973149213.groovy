import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// make sure that there is no current assigned tests data
CustomKeywords.'database.DataDeletion.deleteCandidateAssignedTestsData'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS)

CustomKeywords.'database.DataDeletion.deleteCandidateAssignedTestsData'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS)

CustomKeywords.'database.DataDeletion.deleteCandidateAssignedTestsData'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

// make sure that there is no channel presence related to this user
CustomKeywords.'database.DataDeletion.deleteCandidateChannelPresence'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS)

// make sure that there is no active tests access code related to the TA user
CustomKeywords.'database.DataDeletion.deleteTaTestAccessCodes'(GlobalVariable.TA_USER_EMAIL_ADDRESS)

// login as candidate
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

// click MENU
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_MENU (2)'))

// select My Tests
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_My Tests'))

// make sure that there are no tests in the table
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Home/my_tests_page_No tests have been scored yet'), 
    2)

// logout
CustomKeywords.'basics.utils.logout'()

// generate test access code
CustomKeywords.'basics.ta_utils.generateTestAccessCode'()

String testAccessCode = WebUI.getText(findTestObject('Object Repository/Page_CAT - TA/label_TI5PTVNLLX'))

// logout
CustomKeywords.'basics.utils.logout'()

// candidate check in
CustomKeywords.'basics.candidate_utils.candidateCheckIn'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS, testAccessCode)

// logout
CustomKeywords.'basics.utils.logout'()

// login as TA
CustomKeywords.'basics.utils.login'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

// adding a 2 seconds delay
WebUI.delay(2)

//Delete the generated test-access-code
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/div_Delete'))

//Click Delete on popup
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/span_Delete'))

// click Active candidates
WebUI.click(findTestObject('Page_CAT - TA/button_Active Candidates'))

// click Approve
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/svg_Checked-in_svg-inline--fa fa-thumbs-up fa-w-16_3'))

// adding a 2 seconds delay
WebUI.delay(2)

// logout
CustomKeywords.'basics.utils.logout'()

// login as candidate
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

// make sure that the candidate can now start the test
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Start'))

// adding this condition, since this test was often faling in here because of the websocket connection error
// if connection error page is displayed
if (WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Websocket Connection Error/connection_error_button_Logout'), 
    5, FailureHandling.OPTIONAL)) {
    // close the browser
    WebUI.closeBrowser()

    // open browser and navigate to login page
    CustomKeywords.'basics.utils.NavigateToHomePage'()

    // delete any channel presence related to this user
    CustomKeywords.'database.DataDeletion.deleteCandidateChannelPresence'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS)

    // adding 2 seconds delay
    WebUI.delay(2)

    //login as candidate
    CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)
}

// click on Quit Test
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Welcome/button_Quit Test'))

// select the check boxes
WebUI.click(findTestObject('Object Repository/Page_CAT - Quit Test/input_Quit Test_checkbox-condition-0'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Quit Test/input_I choose to quit this test_checkbox-condition-1'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Quit Test/input_I understand that my test will not be scored_checkbox-condition-2'))

// click on quit test
WebUI.click(findTestObject('Object Repository/Page_CAT - Quit Test/button_Quit Test'))

// click on return to home page
WebUI.click(findTestObject('Object Repository/Page_CAT - Quit Test/button_Return to Home Page'))

// click MENU
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_MENU (2)'))

// select My Tests
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_My Tests'))

// delete assigned tests data related
CustomKeywords.'database.DataDeletion.deleteCandidateAssignedTestsData'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS)

// logout and close browser
CustomKeywords.'basics.utils.logoutAndClose'()

