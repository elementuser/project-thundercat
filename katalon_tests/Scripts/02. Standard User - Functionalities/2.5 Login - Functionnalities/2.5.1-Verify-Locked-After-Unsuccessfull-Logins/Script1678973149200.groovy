import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// fill out login form (using wrong credentials)
WebUI.setText(findTestObject('Object Repository/Page_CAT - Home/input_Email Address_username'), GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS)

WebUI.setEncryptedText(findTestObject('Object Repository/Page_CAT - Home/input_Password_password'), 'F0nEkE6iWDL/XN5iFe+X3iFuKw+gdAPk')

// looping five times
for (int i = 0; i < 5; i++) {
    // click Login
    WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Login'))

    // add delay to make sure that the button is clicked five times
    WebUI.delay(1)
}

// make sure that the "account locked" error is displayed
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Error CAT - Home/label_User account has been locked for 4 hours'), 
    2)

// make sure that the URL is still "/login"
String currentUrl = WebUI.getUrl()

String expectedUrl = GlobalVariable.MAIN_URL + GlobalVariable.LOGIN_URL

WebUI.verifyEqual(currentUrl, expectedUrl)

// resetting locked account data
CustomKeywords.'database.DataUpdate.resetLockedAccountData'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS)

// closing the browser
WebUI.closeBrowser()