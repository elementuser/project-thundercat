import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// click Sample Test link
WebUI.click(findTestObject('Page_CAT - Home/a_Sample Tests'))

// make sure that the sample tests page is displayed
String currentUrl = WebUI.getUrl()

String expectedUrl = GlobalVariable.MAIN_URL + GlobalVariable.SAMPLE_TESTS_URL

WebUI.verifyEqual(currentUrl, expectedUrl)

// note here that if you update the Sample Test <h1> wording, this test will fail (you would need to update the text attribute to fix it)
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Sample Tests/h1_Sample Tests'), 2)

// click Login button
WebUI.click(findTestObject('Object Repository/Page_CAT - Sample Tests/button_Login'))

// login
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

// click Sample Test link
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/a_Sample Tests when logged in'))

// make sure that the sample tests page is displayed
WebUI.verifyEqual(currentUrl, expectedUrl)

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Sample Tests/h1_Sample Tests'), 2)

// start every single sample test
// initialize web driver
WebDriver driver = DriverFactory.getWebDriver()

// get amount of active sample tests
Integer amountOfSampleTests = CustomKeywords.'database.DataGetting.amountOfSampleTests'()

// create a loop based on the amount of active sample tests found in the DB
for (int i = 1; i <= amountOfSampleTests; i++) {
    // get the sample tests table object with rows and columns
    WebElement table = driver.findElement(By.xpath('//div[@id=\'AppRoot\']/div/div/div/div[2]/div/div[2]/div/table'))

    List<WebElement> rows = table.findElements(By.tagName('tr'))

    List<WebElement> cols = rows.get(i).findElements(By.tagName('td'))

    // find start test button and click it
    cols.get(1).findElement(By.tagName('button')).click()

    // make sure that the sample tests page is displayed
    WebUI.verifyEqual(currentUrl, expectedUrl)

    // add small delay
    WebUI.delay(1)

    // click Sample Test link
    WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/a_Sample Tests when logged in'))

    // wait for sample tests table to be loaded
    WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Sample Tests/sample_tests_table_first_row_test_title'), 
        25)

    // add small delay
    WebUI.delay(1)
}

// logout and close browser
CustomKeywords.'basics.utils.logoutAndClose'()

