import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

//delete user accommodations if exists
CustomKeywords.'database.DataDeletion.deleteUserAccommodations'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS)

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// click Display button
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Display'))

// update font size
WebUI.click(findTestObject('Page_CAT - Home/display_settings_font_size_dropdown'))

WebUI.click(findTestObject('Page_CAT - Home/display_settings_font_size_22px'))

// update font family
WebUI.click(findTestObject('Page_CAT - Home/display_settings_font_family_dropdown'))

WebUI.click(findTestObject('Page_CAT - Home/display_settings_font_family_Calibri'))

// enable text spacing
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/div_Text Spacing_react-switch-handle'))

// close popup
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Close'))

// click Display button
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Display'))

// make sure that the font size and the font family have been updated properly
WebUI.verifyEqual(WebUI.getText(findTestObject('Page_CAT - Home/display_settings_font_size_dropdown')), '22px')

WebUI.verifyEqual(WebUI.getText(findTestObject('Page_CAT - Home/display_settings_font_family_dropdown')), 'Calibri')

// put back default font size
WebUI.click(findTestObject('Page_CAT - Home/display_settings_font_size_dropdown'))

WebUI.click(findTestObject('Page_CAT - Home/display_settings_font_size_16px'))

// put back default font family
WebUI.click(findTestObject('Page_CAT - Home/display_settings_font_family_dropdown'))

WebUI.click(findTestObject('Page_CAT - Home/display_settings_font_family_Nunito Sans'))

// disable text spacing
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/div_Text Spacing_react-switch-handle'))

// make sure that the font size and the font family have been updated properly
WebUI.verifyEqual(WebUI.getText(findTestObject('Page_CAT - Home/display_settings_font_size_dropdown')), '16px')

WebUI.verifyEqual(WebUI.getText(findTestObject('Page_CAT - Home/display_settings_font_family_dropdown')), 'Nunito Sans')

// close popup
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Close'))

// login
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

// click Display button
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Display'))

// update font size
WebUI.click(findTestObject('Page_CAT - Home/display_settings_font_size_dropdown'))

WebUI.click(findTestObject('Page_CAT - Home/display_settings_logged_in_font_size_22px'))

// update font family
WebUI.click(findTestObject('Page_CAT - Home/display_settings_font_family_dropdown'))

WebUI.click(findTestObject('Page_CAT - Home/display_settings_logged_in_font_family_Calibri'))

// enable text spacing
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/div_Text Spacing_react-switch-handle'))

// close popup
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Close'))

// logout
CustomKeywords.'basics.utils.logout'()

// login
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

// make sure that the previous settings have been updated in the profile' preferences
// click MENU
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_MENU (2)'))

// select My Profile
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_My Profile (1)'))

// select Preferences
WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Preferences'))

// make sure that the font size and the font family have been updated properly
WebUI.verifyEqual(WebUI.getText(findTestObject('Object Repository/Page_CAT - Profile/preferences_font_size_dropdown')), 
    '22px')

WebUI.verifyEqual(WebUI.getText(findTestObject('Object Repository/Page_CAT - Profile/preferences_font_family_dropdown')), 
    'Calibri')

// navigating to main URL
WebUI.navigateToUrl(GlobalVariable.MAIN_URL)

// click Display button
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Display'))

// put back default font size
WebUI.click(findTestObject('Page_CAT - Home/display_settings_font_size_dropdown'))

WebUI.click(findTestObject('Page_CAT - Home/display_settings_font_size_16px'))

// put back default font family
WebUI.click(findTestObject('Page_CAT - Home/display_settings_font_family_dropdown'))

WebUI.click(findTestObject('Page_CAT - Home/display_settings_font_family_Nunito Sans'))

// disable text spacing
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/div_Text Spacing_react-switch-handle'))

// make sure that the font size and the font family have been updated properly
WebUI.verifyEqual(WebUI.getText(findTestObject('Page_CAT - Home/display_settings_font_size_dropdown')), '16px')

WebUI.verifyEqual(WebUI.getText(findTestObject('Page_CAT - Home/display_settings_font_family_dropdown')), 'Nunito Sans')

// close popup
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Close'))

// logout and close browser
CustomKeywords.'basics.utils.logoutAndClose'()