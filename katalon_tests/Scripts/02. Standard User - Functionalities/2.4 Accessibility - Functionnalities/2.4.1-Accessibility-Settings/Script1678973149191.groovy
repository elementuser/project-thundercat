import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// login
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

// click MENU
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_MENU (2)'))

// select My Profile
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_My Profile (1)'))

// select Preferences
WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Preferences'))

// update the font size
WebUI.click(findTestObject('Page_CAT - Profile/preferences_font_size_dropdown'))

WebUI.click(findTestObject('Page_CAT - Profile/preferences_font_size_30px'))

// update the font family
WebUI.click(findTestObject('Page_CAT - Profile/preferences_font_family_dropdown'))

WebUI.click(findTestObject('Page_CAT - Profile/preferences_font_family_Arial Black'))

// enable the text spacing
WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/div_Text Spacing_react-switch-handle'))

// logout
CustomKeywords.'basics.utils.logout'()

// login back in
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

// click MENU
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_MENU (2)'))

// select My Profile
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_My Profile (1)'))

// select Preferences
WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Preferences'))

// make sure that the font size and the font family have been updated properly
WebUI.verifyEqual(WebUI.getText(findTestObject('Object Repository/Page_CAT - Profile/preferences_font_size_dropdown')), 
    '30px')

WebUI.verifyEqual(WebUI.getText(findTestObject('Object Repository/Page_CAT - Profile/preferences_font_family_dropdown')), 
    'Arial Black')

// put back the default preferences
// update the font size
WebUI.click(findTestObject('Page_CAT - Profile/preferences_font_size_dropdown'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/preferences_font_size_16px'))

// update the font family
WebUI.click(findTestObject('Page_CAT - Profile/preferences_font_family_dropdown'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/preferences_font_family_Nunito Sans'))

// make sure that the font size and the font family have been updated again properly
WebUI.verifyEqual(WebUI.getText(findTestObject('Object Repository/Page_CAT - Profile/preferences_font_size_dropdown')), 
    '16px')

WebUI.verifyEqual(WebUI.getText(findTestObject('Object Repository/Page_CAT - Profile/preferences_font_family_dropdown')), 
    'Nunito Sans')

// disable the text spacing
WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/div_Text Spacing_react-switch-handle'))

// logout and close browser
CustomKeywords.'basics.utils.logoutAndClose'()