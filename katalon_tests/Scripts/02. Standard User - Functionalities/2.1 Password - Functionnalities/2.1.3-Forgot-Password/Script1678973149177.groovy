import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import groovy.time.TimeCategory as TimeCategory
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration

String currentProfile = RunConfiguration.getExecutionProfile()

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// click Forgot Password?
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Forgot Password'))

// click Cancel
WebUI.click(findTestObject('Page_CAT - Home/forgot_password_popup_button_Cancel'))

// click Forgot Password?
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Forgot Password'))

// fill out the form (with invalid email)
WebUI.setText(findTestObject('Page_CAT - Home/forgot_password_popup_input_Email Address_reset-password-email-address'), 
    'invalid.email.com')

// click Email me a password reset link
WebUI.click(findTestObject('Page_CAT - Home/forgot_password_popup_button_Email me a password reset link'))

// getting the respective error message
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Home/forgot_password_popup_Must be a valid Email Address'), 
    2)

// fill out the form (with non-existing user)
WebUI.setText(findTestObject('Page_CAT - Home/forgot_password_popup_input_Email Address_reset-password-email-address'), 
    'non-existing-user@email.ca')

// click Email me a password reset link
WebUI.click(findTestObject('Page_CAT - Home/forgot_password_popup_button_Email me a password reset link'))

// getting the respective error message
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Home/forgot_password_popup_Email provided does not match an active account'), 
    2)

// current profile is local
if (currentProfile != 'local') {

	// fill out the form (with the right data)
	WebUI.setText(findTestObject('Page_CAT - Home/forgot_password_popup_input_Email Address_reset-password-email-address'), 
	    GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS)
	
	// click Email me a password reset link
	WebUI.click(findTestObject('Page_CAT - Home/forgot_password_popup_button_Email me a password reset link'))
	
	// make sure that the confirmation label is displayed
	WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Home/forgot_password_popup_An email has been sent to this address'), 
	    2)
	
	// click Resend the email
	WebUI.click(findTestObject('Object Repository/Page_CAT - Home/forgot_password_popup_button_Resend the email'))
	
	// adding small delay
	WebUI.delay(1)
	
	// click Resend the email again
	WebUI.click(findTestObject('Object Repository/Page_CAT - Home/forgot_password_popup_button_Resend the email'))
	
	// adding small delay
	WebUI.delay(1)
	
	// click Resend the email again
	WebUI.click(findTestObject('Object Repository/Page_CAT - Home/forgot_password_popup_button_Resend the email'))
	
	// make sure that the sent email (maximum amount) error message is displayed
	WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Home/forgot_password_popup_label_You can no longer send email related to this account'), 
	    2)
	
	// click OK
	WebUI.click(findTestObject('Object Repository/Page_CAT - Home/forgot_password_popup_button_OK'))
	
	// getting password reset token directly from DB (since we cannot get it from the emails)
	String passwordResetToken = CustomKeywords.'database.DataGetting.getUserPasswordResetToken'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS)
	
	// building password reset URL
	String passwordResetUrl = (GlobalVariable.MAIN_URL + GlobalVariable.PASSWORD_RESET_URL) + passwordResetToken
	
	// navigate to password reset URL
	WebUI.navigateToUrl(passwordResetUrl)
	
	// fill out new password field
	WebUI.setEncryptedText(findTestObject('Object Repository/Page_Reset Password/password_reset_form_new_password_input'), 'p4y+y39Ir5M2dyN03GLHaGxQ6WA8WZwGqHNPkXSVlcU=')
	
	// fill out confirm password field
	WebUI.setEncryptedText(findTestObject('Object Repository/Page_Reset Password/password_reset_form_confirm_password_input'), 
	    'p4y+y39Ir5M2dyN03GLHaGxQ6WA8WZwGqHNPkXSVlcU=')
	
	// make the new password field visible
	WebUI.click(findTestObject('Object Repository/Page_Reset Password/password_reset_form_new_password_visibility_icon'))
	
	// make the confirm password field visible
	WebUI.click(findTestObject('Object Repository/Page_Reset Password/password_reset_form_confirm_password_visibility_icon'))
	
	// make sure that the text is now visible
	WebUI.verifyEqual(WebUI.getAttribute(findTestObject('Object Repository/Page_Reset Password/password_reset_form_new_password_input'), 
	        'value'), 'PasswordResetVisibilityTest')
	
	WebUI.verifyEqual(WebUI.getAttribute(findTestObject('Object Repository/Page_Reset Password/password_reset_form_confirm_password_input'), 
	        'value'), 'PasswordResetVisibilityTest')
	
	// make the new password field non-visible
	WebUI.click(findTestObject('Object Repository/Page_Reset Password/password_reset_form_new_password_visibility_icon'))
	
	// make the confirm password field non-visible
	WebUI.click(findTestObject('Object Repository/Page_Reset Password/password_reset_form_confirm_password_visibility_icon'))
	
	// clear all password fields
	WebUI.sendKeys(findTestObject('Object Repository/Page_Reset Password/password_reset_form_new_password_input'), Keys.chord(
	        Keys.CONTROL, 'a'))
	
	WebUI.sendKeys(findTestObject('Object Repository/Page_Reset Password/password_reset_form_new_password_input'), Keys.chord(
	        Keys.BACK_SPACE))
	
	WebUI.sendKeys(findTestObject('Object Repository/Page_Reset Password/password_reset_form_confirm_password_input'), Keys.chord(
	        Keys.CONTROL, 'a'))
	
	WebUI.sendKeys(findTestObject('Object Repository/Page_Reset Password/password_reset_form_confirm_password_input'), Keys.chord(
	        Keys.BACK_SPACE))
	
	// fill out confirm password field (simply adding 'a' as an input to trigger the errors)
	WebUI.setEncryptedText(findTestObject('Object Repository/Page_Reset Password/password_reset_form_confirm_password_input'), 
	    'J7R4ejOak4Y=')
	
	// click Reset Password
	WebUI.click(findTestObject('Object Repository/Page_Reset Password/password_reset_form_button_Reset Password'))
	
	// make sure that the respective error messages are displayed
	WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Reset Password/password_reset_form_password_requirements'), 
	    2)
	
	WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Reset Password/password_reset_form_password_confirmation_must_match'), 
	    2)
	
	// fill out new password field (password too common)
	WebUI.setEncryptedText(findTestObject('Object Repository/Page_Reset Password/password_reset_form_new_password_input'), 'p4y+y39Ir5PEPmX20UxFKw==')
	
	// fill out confirm password field
	WebUI.setEncryptedText(findTestObject('Object Repository/Page_Reset Password/password_reset_form_confirm_password_input'), 
	    'p4y+y39Ir5PEPmX20UxFKw==')
	
	// click Reset Password
	WebUI.click(findTestObject('Object Repository/Page_Reset Password/password_reset_form_button_Reset Password'))
	
	// make sure that the respective error messages are displayed
	WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Reset Password/password_reset_form_label_This password is too common'), 
	    2)
	
	// fill out new password field (real new password)
	WebUI.setEncryptedText(findTestObject('Object Repository/Page_Reset Password/password_reset_form_new_password_input'), GlobalVariable.USERS_NEW_ENCRYPTED_PASSWORD)
	
	// fill out confirm password field
	WebUI.setEncryptedText(findTestObject('Object Repository/Page_Reset Password/password_reset_form_confirm_password_input'), 
	    GlobalVariable.USERS_NEW_ENCRYPTED_PASSWORD)
	
	// click Reset Password
	WebUI.click(findTestObject('Object Repository/Page_Reset Password/password_reset_form_button_Reset Password'))
	
	// click OK
	WebUI.click(findTestObject('Object Repository/Page_Reset Password/reset_password_confirmation_popup_button_OK'))
	
	// login with the new credentials
	CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.USERS_NEW_ENCRYPTED_PASSWORD)
	
	// click MENU
	WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_MENU (2)'))
	
	// select My Profile
	WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_My Profile (1)'))
	
	// select Password
	WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Password'))
	
	// validate last password update date
	// initializing today date
	Date today = null
	
	// getting today's date
	use(TimeCategory, { 
	        today = new Date()
	    })
	
	// initializing passwordLastUpdatedDate
	String passwordLastUpdatedDate = WebUI.getText(findTestObject('Object Repository/Page_CAT - Profile/p_Your password was last updated on')).split(
	    ': ')[1]
	
	// make sure that the last password updated date is today
	WebUI.verifyEqual(today.format('yyyy-MM-dd'), passwordLastUpdatedDate)
	
	// put back the old password
	WebUI.setEncryptedText(findTestObject('Object Repository/Page_CAT - Profile/input_Current Password_current-password-input'), 
	    GlobalVariable.USERS_NEW_ENCRYPTED_PASSWORD)
	
	WebUI.setEncryptedText(findTestObject('Object Repository/Page_CAT - Profile/input_Minimum of 8 characters and maximum o_9b0816'), 
	    GlobalVariable.USERS_ENCRYPTED_PASSWORD)
	
	WebUI.setEncryptedText(findTestObject('Object Repository/Page_CAT - Profile/input_Confirm Password_confirm-password-input'), 
	    GlobalVariable.USERS_ENCRYPTED_PASSWORD)
	
	// click Save
	WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Save_My_Password'))
	
	// click OK
	WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_OK'))
	
	// logout
	CustomKeywords.'basics.utils.logout'()
	
	// make sure that the password reset link is no longer available (expected to be redirected to login page)
	// navigate to password reset URL
	WebUI.navigateToUrl(passwordResetUrl)
	
	// adding small delay
	WebUI.delay(2)
	
	String currentUrl = WebUI.getUrl()
	
	String expectedUrl = GlobalVariable.MAIN_URL + GlobalVariable.LOGIN_URL
	
	WebUI.verifyEqual(currentUrl, expectedUrl)

}

// close the browser
WebUI.closeBrowser()
