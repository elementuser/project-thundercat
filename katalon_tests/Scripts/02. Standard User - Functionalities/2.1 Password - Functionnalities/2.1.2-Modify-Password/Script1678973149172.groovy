import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import groovy.time.TimeCategory as TimeCategory
import internal.GlobalVariable as GlobalVariable

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// login
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

// click MENU
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_MENU (2)'))

// select My Profile
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_My Profile (1)'))

// select Password
WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Password'))

// testing the display field functionality for each password field
// fill out all password fields
WebUI.setEncryptedText(findTestObject('Object Repository/Page_CAT - Profile/input_Current Password_current-password-input'), 
    'Rj/cwQdeR3hJ4O0dMLksCTTbX9A2/Cri')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_CAT - Profile/input_Minimum of 8 characters and maximum o_9b0816'), 
    'Rj/cwQdeR3hJ4O0dMLksCTTbX9A2/Cri')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_CAT - Profile/input_Confirm Password_confirm-password-input'), 
    'Rj/cwQdeR3hJ4O0dMLksCTTbX9A2/Cri')

// click on the view password button (for all password fields)
WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/span_Current Password_current-password-visility-icon'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/span_Minimum of 8 characters and maximum of 15_new-password-visility-icon'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/span_Confirm Password_confirm-new-password-visility-icon'))

// make sure that all fields are now visible
WebUI.verifyEqual(WebUI.getAttribute(findTestObject('Object Repository/Page_CAT - Profile/input_Current Password_current-password-input'), 
        'value'), 'ShowPasswordTest')

WebUI.verifyEqual(WebUI.getAttribute(findTestObject('Object Repository/Page_CAT - Profile/input_Minimum of 8 characters and maximum o_9b0816'), 
        'value'), 'ShowPasswordTest')

WebUI.verifyEqual(WebUI.getAttribute(findTestObject('Object Repository/Page_CAT - Profile/input_Confirm Password_confirm-password-input'), 
        'value'), 'ShowPasswordTest')

// click on the view password button (for all password fields) in order to hide back the characters
WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/span_Current Password_current-password-visility-icon'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/span_Minimum of 8 characters and maximum of 15_new-password-visility-icon'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/span_Confirm Password_confirm-new-password-visility-icon'))

// clear all password fields
WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Profile/input_Current Password_current-password-input'), Keys.chord(
        Keys.CONTROL, 'a'))

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Profile/input_Current Password_current-password-input'), Keys.chord(
        Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Profile/input_Minimum of 8 characters and maximum o_9b0816'), 
    Keys.chord(Keys.CONTROL, 'a'))

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Profile/input_Minimum of 8 characters and maximum o_9b0816'), 
    Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Profile/input_Confirm Password_confirm-password-input'), Keys.chord(
        Keys.CONTROL, 'a'))

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Profile/input_Confirm Password_confirm-password-input'), Keys.chord(
        Keys.BACK_SPACE))

// fill out current password field only (with a wrong password)
WebUI.setEncryptedText(findTestObject('Object Repository/Page_CAT - Profile/input_Current Password_current-password-input'), 
    'RyzK/uu8Q1+7/zuOsKmZ+g==')

// click Save
WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Save_My_Password'))

// make sure that respective error messages are displayed
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Profile/div_password_requirements_in_my_password_page'), 
    2)

// fill out new password and confirm password fields (non matching password)
WebUI.setEncryptedText(findTestObject('Object Repository/Page_CAT - Profile/input_Minimum of 8 characters and maximum o_9b0816'), 
    GlobalVariable.USERS_NEW_ENCRYPTED_PASSWORD)

WebUI.setEncryptedText(findTestObject('Object Repository/Page_CAT - Profile/input_Confirm Password_confirm-password-input'), 
    '+ySD09iij20=')

// click Save
WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Save_My_Password'))

// make sure that respective error messages are displayed
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Profile/label_confirm_password_error_in_my_password_page'), 
    2)

// update confirm password field
WebUI.setEncryptedText(findTestObject('Object Repository/Page_CAT - Profile/input_Confirm Password_confirm-password-input'), 
    GlobalVariable.USERS_NEW_ENCRYPTED_PASSWORD)

// click Save
WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Save_My_Password'))

// make sure that respective error messages are displayed
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Profile/label_Invalid_password_error_in_my_password_page'), 
    2)

// update current password field (with the right credentials this time)
WebUI.setEncryptedText(findTestObject('Object Repository/Page_CAT - Profile/input_Current Password_current-password-input'), 
    GlobalVariable.USERS_ENCRYPTED_PASSWORD)

// click Save
WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Save_My_Password'))

// click OK
WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_OK'))

// validate last password update date
// initializing today date
Date today = null

// getting today's date
use(TimeCategory, { 
        today = new Date()
    })

// initializing passwordLastUpdatedDate
String passwordLastUpdatedDate = WebUI.getText(findTestObject('Object Repository/Page_CAT - Profile/p_Your password was last updated on')).split(
    ': ')[1]

// make sure that the last password updated date is today
WebUI.verifyEqual(today.format('yyyy-MM-dd'), passwordLastUpdatedDate)

// logout
CustomKeywords.'basics.utils.logout'()

// login with the new credentials
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.USERS_NEW_ENCRYPTED_PASSWORD)

// put back the old password
// click MENU
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_MENU (2)'))

// select My Profile
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_My Profile (1)'))

// select Password
WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Password'))

// fill out the my password form
WebUI.setEncryptedText(findTestObject('Object Repository/Page_CAT - Profile/input_Current Password_current-password-input'), 
    GlobalVariable.USERS_NEW_ENCRYPTED_PASSWORD)

WebUI.setEncryptedText(findTestObject('Object Repository/Page_CAT - Profile/input_Minimum of 8 characters and maximum o_9b0816'), 
    GlobalVariable.USERS_ENCRYPTED_PASSWORD)

WebUI.setEncryptedText(findTestObject('Object Repository/Page_CAT - Profile/input_Confirm Password_confirm-password-input'), 
    GlobalVariable.USERS_ENCRYPTED_PASSWORD)

// click Save
WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Save_My_Password'))

// click OK
WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_OK'))

// logout and close browser
CustomKeywords.'basics.utils.logoutAndClose'()