import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// not calling the basics.utils.login keyword here since we need to test the errors
// fill out loging form (using wrong credentials in order to test the password visibility functionality)
WebUI.setText(findTestObject('Object Repository/Page_CAT - Home/input_Email Address_username'), GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS)

WebUI.setEncryptedText(findTestObject('Object Repository/Page_CAT - Home/input_Password_password'), 'Rj/cwQdeR3hJ4O0dMLksCTTbX9A2/Cri')

// click on the view password button
WebUI.click(findTestObject('Page_CAT - Home/login_form_password_visibility_button'))

// make sure that the password field is now visible
WebUI.verifyEqual(WebUI.getAttribute(findTestObject('Object Repository/Page_CAT - Home/input_Password_password'), 'value'), 
    'ShowPasswordTest')

// click on the view password button
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/login_form_password_visibility_button'))

// clear the password field
WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Home/input_Password_password'), Keys.chord(Keys.CONTROL, 'a'))

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Home/input_Password_password'), Keys.chord(Keys.BACK_SPACE))

// fill out login form (using wrong credentials)
WebUI.setEncryptedText(findTestObject('Object Repository/Page_CAT - Home/input_Password_password'), 'VXL+DIHsB+A90UY4Rz9dJx48j40oFyMY')

// click Login
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Login'))

// make sure that respective error messages are displayed
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Error CAT - Home/label_Email address andor password is invalid'), 
    2)

// login (using right credentials)
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

// logout and close browser
CustomKeywords.'basics.utils.logoutAndClose'()

