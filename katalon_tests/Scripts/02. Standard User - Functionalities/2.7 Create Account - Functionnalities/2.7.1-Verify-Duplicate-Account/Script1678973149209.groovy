import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// click Create an account tab
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Create an account'))

// fill out form with the right data
WebUI.setText(findTestObject('Object Repository/Page_Error CAT - Home/input_First Name_first-name-field'), 'TemporaryText')

WebUI.setText(findTestObject('Object Repository/Page_Error CAT - Home/input_Last Name_last-name-field'), 'TemporaryText')

WebUI.click(findTestObject('Object Repository/Page_Error CAT - Home/div_option 31, selected._css-1gtu0rj-indica_9a18c0'))

WebUI.click(findTestObject('Object Repository/Page_Error CAT - Home/div_20'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Home/div_Select_css-1gtu0rj-indicatorContainer_1'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Home/div_02'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Home/div_Select_css-1gtu0rj-indicatorContainer_1_2'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Home/div_2000'))

WebUI.setText(findTestObject('Object Repository/Page_Error CAT - Home/input_Valid_psrs-applicant-id'), 'A123456')

WebUI.setText(findTestObject('Object Repository/Page_Error CAT - Home/input_Valid_pri-or-military-nbr-field'), '12345679')

WebUI.setText(findTestObject('Object Repository/Page_Error CAT - Home/input_Email Address_email-address-field'), GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS)

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Error CAT - Home/input_Minimum of 8 characters and maximum o_aa717e'), 
    GlobalVariable.USERS_ENCRYPTED_PASSWORD)

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Error CAT - Home/input_Confirm Password_password-confirmation-field'), 
    GlobalVariable.USERS_ENCRYPTED_PASSWORD)

WebUI.click(findTestObject('Object Repository/Page_Error CAT - Home/input_Valid_privacy-notice-checkbox'))

// click create account button
WebUI.click(findTestObject('Object Repository/Page_Error CAT - Home/button_Create account'))

// make sure that the already associated email error is displayed
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Error CAT - Home/label_An account is already associated to this email address'), 
    2)

// make sure that the URL is still "/login"
String currentUrl = WebUI.getUrl()

String expectedUrl = GlobalVariable.MAIN_URL + GlobalVariable.LOGIN_URL

WebUI.verifyEqual(currentUrl, expectedUrl)

// closing the browser
WebUI.closeBrowser()