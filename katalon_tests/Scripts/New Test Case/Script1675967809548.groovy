import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys


WebUI.click(findTestObject('Object Repository/Page_CAT - Operations/button_Test Access Codes'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Operations/button_Test Access Management'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Operations/svg_Search_svg-inline--fa fa-edit'))

WebUI.setText(findTestObject('Object Repository/Page_CAT - Operations/input_Test Order Number_test-order-number'), 'fdsa')

WebUI.setText(findTestObject('Object Repository/Page_CAT - Operations/input_Assessment Process Number_staffing-pr_2da128'), 
    'fdsa')

WebUI.click(findTestObject('Object Repository/Page_CAT - Operations/div_Select'))

WebUI.setText(findTestObject('Object Repository/Page_CAT - Operations/input_Select_react-select-21-input'), '01')

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Operations/input_Select_react-select-21-input'), Keys.chord(
        Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_CAT - Operations/div_Select'))

WebUI.setText(findTestObject('Object Repository/Page_CAT - Operations/input_Select_react-select-22-input'), '02')

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Operations/input_Select_react-select-22-input'), Keys.chord(
        Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_CAT - Operations/div_Select'))

WebUI.setText(findTestObject('Object Repository/Page_CAT - Operations/input_Select_react-select-23-input'), '2023')

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Operations/input_Select_react-select-23-input'), Keys.chord(
        Keys.ENTER))

WebUI.closeBrowser()

