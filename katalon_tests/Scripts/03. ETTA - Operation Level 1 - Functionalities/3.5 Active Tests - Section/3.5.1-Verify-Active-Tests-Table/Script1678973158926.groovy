import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

//delete test access codes of TA user if exists
CustomKeywords.'database.DataDeletion.deleteTaTestAccessCodes'(GlobalVariable.TA_USER_EMAIL_ADDRESS)

//delete test access codes of TA Two user if exists
CustomKeywords.'database.DataDeletion.deleteTaTestAccessCodes'(GlobalVariable.TA_USER_TWO_EMAIL_ADDRESS)

// make sure that there is no current assigned tests data
CustomKeywords.'database.DataDeletion.deleteCandidateAssignedTestsData'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS)

// make sure that there is no channel presence related to user
CustomKeywords.'database.DataDeletion.deleteCandidateChannelPresence'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS)

// make sure that there is no current assigned tests data
CustomKeywords.'database.DataDeletion.deleteCandidateAssignedTestsData'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS)

// make sure that there is no channel presence related to user
CustomKeywords.'database.DataDeletion.deleteCandidateChannelPresence'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS)

// make sure that there is no current assigned tests data
CustomKeywords.'database.DataDeletion.deleteCandidateAssignedTestsData'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

// make sure that there is no channel presence related to user
CustomKeywords.'database.DataDeletion.deleteCandidateChannelPresence'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

// make sure that there is no active test access codes
CustomKeywords.'database.DataDeletion.deleteTaTestAccessCodes'(GlobalVariable.TA_USER_EMAIL_ADDRESS)

//Generate Test access codes using TA
//Generating  test access codes using API (Last argument number represents to generate number of test access codes here it generates 1)
String[] testAccessCode1 = CustomKeywords.'api.api_requests.generateTestAccessCodeUsingAPI'(GlobalVariable.TA_USER_EMAIL_ADDRESS, 
    GlobalVariable.USERS_ENCRYPTED_PASSWORD, 1)

//Generate Test access codes using TA Two
//Generating  test access codes using API (Last argument number represents to generate number of test access codes here it generates 1)
String[] testAccessCode2 = CustomKeywords.'api.api_requests.generateTestAccessCodeUsingAPI'(GlobalVariable.TA_USER_TWO_EMAIL_ADDRESS, 
    GlobalVariable.USERS_ENCRYPTED_PASSWORD, 1)

//candidate check in
CustomKeywords.'api.api_requests.candidateCheckInUsingAPI'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD, 
    testAccessCode1[0])

//candidate check in
CustomKeywords.'api.api_requests.candidateCheckInUsingAPI'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD, 
    testAccessCode1[0])

//candidate check in
CustomKeywords.'api.api_requests.candidateCheckInUsingAPI'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD, 
    testAccessCode2[0])

// Navigate to home page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// login (as a ETTA user)
CustomKeywords.'basics.utils.login'(GlobalVariable.ETTA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

//Adding delay
WebUI.delay(1)

// Click on Active Tests
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/ActiveTests/button_Active Tests'))

//Adding delay for the page to load
WebUI.delay(2)

//Verify Pagination (Select 2)
WebUI.click(findTestObject('Object Repository/Page_CAT - Operations/svg_Number of results per page_css-tj5bde-Svg_1'))

// Delay for 1 second for the dropdown
//WebUI.delay(1)

// Wait for dropdown (50) element
WebUI.setText(findTestObject('Object Repository/Page_CAT - Operations/input_Number of results per page_react-sele_99cc3c'), 
    '50')

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Operations/input_Number of results per page_react-sele_99cc3c'), 
    Keys.chord(Keys.ENTER))

// Delay for 2 seconds
WebUI.delay(2)

//Click on previous and next buttons
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/ActiveTests/svg_Next page_svg-inline--fa fa-caret-right fa-w-6'))

// Delay for 2 seconds
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/ActiveTests/svg_Previous page_svg-inline--fa fa-caret-left fa-w-6'))

//Verify Pagination (Select 25)
WebUI.click(findTestObject('Object Repository/Page_CAT - Operations/svg_Number of results per page_css-tj5bde-Svg_1'))

// Delay for 4 seconds for the dropdown
WebUI.delay(4)

// Wait for dropdown (2) element
WebUI.setText(findTestObject('Object Repository/Page_CAT - Operations/input_Number of results per page_react-sele_99cc3c'), 
    '25')

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Operations/input_Number of results per page_react-sele_99cc3c'), 
    Keys.chord(Keys.ENTER))

// Delay for 3 second
WebUI.delay(3)

// Search by candidate name
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/ActiveTests/input_Search_all-active-candidates-search-bar'), 
    'UserTwo')

WebUI.delay(2)

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Business Operations/ActiveTests/input_Search_all-active-candidates-search-bar'), 
    Keys.chord(Keys.ENTER))

// Add delay of 2 seconds for the results to show up
WebUI.delay(4)

WebUI.verifyTextPresent('1 result(s)', false)

// clear search
WebUI.click(findTestObject('Page_CAT - Business Operations/button_Clear Search'))

// Search by TA name
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/ActiveTests/input_Search_all-active-candidates-search-bar'), 
    'TATwo, Katalon')

WebUI.delay(2)

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Business Operations/ActiveTests/input_Search_all-active-candidates-search-bar'), 
    Keys.chord(Keys.ENTER))

// Add delay of 4 seconds for the results to show up
WebUI.delay(4)

WebUI.verifyTextPresent('1 result(s)', false)

// clear search
WebUI.click(findTestObject('Page_CAT - Business Operations/button_Clear Search'))

// Search by Test Order number
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/ActiveTests/input_Search_all-active-candidates-search-bar'), 
    'KATALON')

WebUI.delay(2)

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Business Operations/ActiveTests/input_Search_all-active-candidates-search-bar'), 
    Keys.chord(Keys.ENTER))

// Add delay of 4 seconds for the results to show up
WebUI.delay(4)

WebUI.verifyTextPresent('3 result(s)', false)

// clear search
WebUI.click(findTestObject('Page_CAT - Business Operations/button_Clear Search'))

// Search for no data
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/ActiveTests/input_Search_all-active-candidates-search-bar'), 
    'CheckDataDoesNotExist')

// Add small delay
WebUI.delay(2)

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Business Operations/ActiveTests/input_Search_all-active-candidates-search-bar'), 
    Keys.chord(Keys.ENTER))

// Add small delay
WebUI.delay(4)

WebUI.verifyTextPresent('0 result(s)', false)

// clear search
WebUI.click(findTestObject('Page_CAT - Business Operations/button_Clear Search'))

// Add small delay
WebUI.delay(2)

// Search by candidate name and invalidate
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/ActiveTests/input_Search_all-active-candidates-search-bar'), 
    'UserOne')

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Business Operations/ActiveTests/input_Search_all-active-candidates-search-bar'), 
    Keys.chord(Keys.ENTER))

// Add delay of 2 seconds for the results to show up
WebUI.delay(2)

//click on invalidate
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/ActiveTests/button_Invalidate'))

// click cancel
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/ActiveTests/button_Cancel'))

//click on invalidate
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/ActiveTests/button_Invalidate'))

//set reason for invalidating the test
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/ActiveTests/textarea_Reason for Invalidating the test_reason-for-invalidating'), 
    'Invalidating test for katalon testing')

//Click on Invalidate Access
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/ActiveTests/button_Invalidate Access'))

WebUI.delay(1)

// logout
CustomKeywords.'basics.utils.logout'()

// login (as a TA user) and verify candidate row is removed
CustomKeywords.'basics.utils.login'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

WebUI.delay(1)

//Click Active candidates
WebUI.click(findTestObject('Page_CAT - TA/button_Active Candidates'))

// delay to load active candidates page
WebUI.delay(1)

//to verify that the candidate row is deleted from TA Active candidates table
WebUI.verifyTextNotPresent('katalon.userone@email.ca', false)

// logout
CustomKeywords.'basics.utils.logout'()

// login (as a standard user one) and verify there is no access to the test
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

WebUI.delay(1)

WebUI.verifyTextPresent('You have no assigned tests', false)

// logout
CustomKeywords.'basics.utils.logout'()

// login (as a TA Two user) and approve the candidate
CustomKeywords.'basics.utils.login'(GlobalVariable.TA_USER_TWO_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

WebUI.delay(1)

//Click Active candidates
WebUI.click(findTestObject('Page_CAT - TA/button_Active Candidates'))

// wait for the table to be loaded
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Test Administrator/ta_active_candidates_first_row_candidate_name'), 
    25)

// adding a 2 seconds delay
WebUI.delay(2)

//Click Approve
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/button_Checked-in_secondary_approve'))

// adding a 2 seconds delay
//WebUI.delay(2)

// resync candidates (to avoid any websocket connection errors)
//click resync
//WebUI.click(findTestObject('Object Repository/Page_CAT - TA/button_Testing_secondary_Resynchronize'))

//click resync on pop up
//WebUI.click(findTestObject('Object Repository/Page_CAT - TA/button_Resynchronize_popup'))

// wait for the table to be loaded
//WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - TA/button_Testing_secondary_Resynchronize'), 25)

// adding a 2 seconds delay
WebUI.delay(2)

// logout
CustomKeywords.'basics.utils.logout'()

//login as candidate and start the test
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

// adding this condition, since this test was often faling in here because of the websocket connection error
// if connection error page is displayed
if (WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Websocket Connection Error/connection_error_button_Logout'), 
    5, FailureHandling.OPTIONAL)) {
    // close the browser
    WebUI.closeBrowser()

    // open browser and navigate to login page
    CustomKeywords.'basics.utils.NavigateToHomePage'()

    // delete any channel presence related to this user
    CustomKeywords.'database.DataDeletion.deleteCandidateChannelPresence'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

    // adding 2 seconds delay
    WebUI.delay(2)

    //login as candidate
    CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)
}

//Click on Start
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Start'))

// adding a 2 seconds delay
WebUI.delay(2)

WebUI.closeBrowser()

// Navigate to home page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// login (as a ETTA user) and invalidate the test
CustomKeywords.'basics.utils.login'(GlobalVariable.ETTA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

//Adding delay
WebUI.delay(1)

// Click on Active Tests
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/ActiveTests/button_Active Tests'))

//Adding delay for the page to load
WebUI.delay(2)

// Search by candidate name
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/ActiveTests/input_Search_all-active-candidates-search-bar'), 
    'UserThree')

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Business Operations/ActiveTests/input_Search_all-active-candidates-search-bar'), 
    Keys.chord(Keys.ENTER))

// Add delay of 2 seconds for the results to show up
WebUI.delay(2)

//click on invalidate
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/ActiveTests/button_Invalidate'))

//set reason for invalidating the test
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/ActiveTests/textarea_Reason for Invalidating the test_reason-for-invalidating'), 
    'Invalidating test for katalon testing')

//Click on Invalidate Access
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/ActiveTests/button_Invalidate Access'))

WebUI.delay(5)

// logout
CustomKeywords.'basics.utils.logout'()

// login (as a TA Two user) and verify that the candidate row is deleted
CustomKeywords.'basics.utils.login'(GlobalVariable.TA_USER_TWO_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

WebUI.delay(1)

//Click Active candidates
WebUI.click(findTestObject('Page_CAT - TA/button_Active Candidates'))

WebUI.delay(2)

//to verify that the candidate row is deleted from TA Active candidates table
WebUI.verifyTextNotPresent('katalon.userthree@email.ca', false)

// logout
CustomKeywords.'basics.utils.logout'()

//login as candidate and verify that there is no access to the test
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

WebUI.delay(2)

WebUI.verifyTextPresent('You have no assigned tests', false)

// logout
CustomKeywords.'basics.utils.logout'()

// login (as a TA user) and approve the candidate
CustomKeywords.'basics.utils.login'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

WebUI.delay(1)

//Click Active candidates
WebUI.click(findTestObject('Page_CAT - TA/button_Active Candidates'))

// wait for the table to be loaded
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Test Administrator/ta_active_candidates_first_row_candidate_name'), 
    25)

// adding a 2 seconds delay
WebUI.delay(2)

//Click Approve
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/button_Checked-in_secondary_approve'))

// adding a 2 seconds delay
WebUI.delay(2)

// resync candidates (to avoid any websocket connection errors)
//click resync
//WebUI.click(findTestObject('Object Repository/Page_CAT - TA/button_Testing_secondary_Resynchronize'))

//click resync on pop up
//WebUI.click(findTestObject('Object Repository/Page_CAT - TA/button_Resynchronize_popup'))

// wait for the table to be loaded
//WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - TA/button_Testing_secondary_Resynchronize'), 25)

// adding a 2 seconds delay
WebUI.delay(2)

// logout
CustomKeywords.'basics.utils.logout'()

// login (as a ETTA user) and invalidate the test
CustomKeywords.'basics.utils.login'(GlobalVariable.ETTA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

//Adding delay
WebUI.delay(1)

// Click on Active Tests
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/ActiveTests/button_Active Tests'))

//Adding delay for the page to load
WebUI.delay(2)

// Search by candidate name
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/ActiveTests/input_Search_all-active-candidates-search-bar'), 
    'UserTwo')

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Business Operations/ActiveTests/input_Search_all-active-candidates-search-bar'), 
    Keys.chord(Keys.ENTER))

// Add delay of 2 seconds for the results to show up
WebUI.delay(2)

//click on invalidate
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/ActiveTests/button_Invalidate'))

//set reason for invalidating the test
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/ActiveTests/textarea_Reason for Invalidating the test_reason-for-invalidating'), 
    'Invalidating test for katalon testing')

//Click on Invalidate Access
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/ActiveTests/button_Invalidate Access'))

WebUI.delay(5)

// logout
CustomKeywords.'basics.utils.logout'()

// login (as a TA user) and verify that the candidate row does not exists
CustomKeywords.'basics.utils.login'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

WebUI.delay(1)

//Click Active candidates
WebUI.click(findTestObject('Page_CAT - TA/button_Active Candidates'))

WebUI.delay(2)

//to verify that the candidate row is deleted from TA Active candidates table
WebUI.verifyTextNotPresent('katalon.usertwo@email.ca', false)

// logout
CustomKeywords.'basics.utils.logout'()

//login as candidate
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

WebUI.delay(2)

WebUI.verifyTextPresent('You have no assigned tests', false)

// logout and close browser
CustomKeywords.'basics.utils.logoutAndClose'()

//delete test access codes of TA user if exists
CustomKeywords.'database.DataDeletion.deleteTaTestAccessCodes'(GlobalVariable.TA_USER_EMAIL_ADDRESS)

//delete test access codes of TA Two user if exists
CustomKeywords.'database.DataDeletion.deleteTaTestAccessCodes'(GlobalVariable.TA_USER_TWO_EMAIL_ADDRESS)