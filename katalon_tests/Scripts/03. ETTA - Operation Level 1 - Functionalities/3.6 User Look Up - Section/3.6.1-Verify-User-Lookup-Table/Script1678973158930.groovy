import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration

String currentProfile = RunConfiguration.getExecutionProfile()

//delete test access codes of TA user if exists
CustomKeywords.'database.DataDeletion.deleteTaTestAccessCodes'(GlobalVariable.TA_USER_EMAIL_ADDRESS)

// make sure that there is no current assigned tests data
CustomKeywords.'database.DataDeletion.deleteCandidateAssignedTestsData'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS)

// make sure that there is no channel presence related to user
CustomKeywords.'database.DataDeletion.deleteCandidateChannelPresence'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS)

// making sure that the user does not have any permissions assigned to his account
CustomKeywords.'database.DataDeletion.deleteUserPermissions'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS)

// making sure that the user does not have any pending permissions assigned to his account
CustomKeywords.'database.DataDeletion.deletePendingUserPermissions'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.ETTA_PERMISSION)

CustomKeywords.'database.DataDeletion.deletePendingUserPermissions'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.PPC_PERMISSION)

CustomKeywords.'database.DataDeletion.deletePendingUserPermissions'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.TA_PERMISSION)

CustomKeywords.'database.DataDeletion.deletePendingUserPermissions'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.SCORER_PERMISSION)

CustomKeywords.'database.DataDeletion.deletePendingUserPermissions'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.TD_LEVEL_1_PERMISSION)

//Generate Test access codes using TA
//Generating  test access codes using API (Last argument number represents to generate number of test access codes here it generates 1)
String[] testAccessCode1 = CustomKeywords.'api.api_requests.generateTestAccessCodeUsingAPI'(GlobalVariable.TA_USER_EMAIL_ADDRESS, 
    GlobalVariable.USERS_ENCRYPTED_PASSWORD, 1)

//candidate check in
CustomKeywords.'api.api_requests.candidateCheckInUsingAPI'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD, 
    testAccessCode1[0])

// send PPC Permission request for user one
CustomKeywords.'api.api_requests.sendPermissionRequestUsingAPI'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD, 
    GlobalVariable.PPC_PERMISSION)

// Navigate to home page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// login (as a ETTA user)
CustomKeywords.'basics.utils.login'(GlobalVariable.ETTA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

//Adding delay
WebUI.delay(1)

// Click on User Look-up
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/User Look-up/button_User Look-up'))

//Adding delay
WebUI.delay(4)

WebUI.click(findTestObject('Object Repository/Page_CAT - Operations/div_Number of results per page_css-1gtu0rj-_72ae82_2'))

WebUI.setText(findTestObject('Object Repository/Page_CAT - Operations/input_Number of results per page_react-sele_f780b1_2'), 
    '50')

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Operations/input_Number of results per page_react-sele_f780b1_2'), 
    Keys.chord(Keys.ENTER))

//Adding delay
WebUI.delay(1)

//click on next page
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/User Look-up/svg_Next page_svg-inline--fa fa-caret-right fa-w-6'))

//Adding delay
WebUI.delay(1)

//click on previous page
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/User Look-up/svg_Previous page_svg-inline--fa fa-caret-l_a1e700'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Operations/div_Number of results per page_css-1gtu0rj-_72ae82_2'))

WebUI.setText(findTestObject('Object Repository/Page_CAT - Operations/input_Number of results per page_react-sele_f780b1_2'), 
    '25')

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Operations/input_Number of results per page_react-sele_f780b1_2'), 
    Keys.chord(Keys.ENTER))

//click on next page
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/User Look-up/svg_Next page_svg-inline--fa fa-caret-right fa-w-6'))

//Adding delay
WebUI.delay(1)

//click on previous page
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/User Look-up/svg_Previous page_svg-inline--fa fa-caret-l_a1e700'))

// Delay for 1 second for the dropdown
WebUI.delay(1)

// Search by last name
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/User Look-up/input_Search_user-look-up-search-bar'), 
    GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS)

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Business Operations/User Look-up/input_Search_user-look-up-search-bar'), 
    Keys.chord(Keys.ENTER))

// Add delay of 2 seconds for the results to show up
WebUI.delay(2)

WebUI.verifyTextPresent('1 result(s)', false)

// clear search
WebUI.click(findTestObject('Page_CAT - Business Operations/button_Clear Search'))

// Add delay of 2 seconds for the results to show up
WebUI.delay(2)

// Search by user DOB
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/User Look-up/input_Search_user-look-up-search-bar'), 
    '2000-02-20')

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Business Operations/User Look-up/input_Search_user-look-up-search-bar'), 
    Keys.chord(Keys.ENTER))

// Add delay of 2 seconds for the results to show up
WebUI.delay(2)

// current profile is local
if (currentProfile == 'local') {
	WebUI.verifyTextPresent('5 result(s)', false)
}
else {
	WebUI.verifyTextPresent('6 result(s)', false)
}

// Search by user email
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/User Look-up/input_Search_user-look-up-search-bar'), 
    GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS)

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Business Operations/User Look-up/input_Search_user-look-up-search-bar'), 
    Keys.chord(Keys.ENTER))

// Add delay of 2 seconds for the results to show up
WebUI.delay(2)

WebUI.verifyTextPresent('1 result(s)', false)

// click on view button
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/User Look-up/button_View'))

// delay 
WebUI.delay(2)

// click on back to user look up results button
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/User Look-up/span_Back to User Look-Up results'))

WebUI.delay(4)

// click on view
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/User Look-up/button_View'))

WebUI.delay(2)

// verify personal Information
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/User Look-up/button_Personal Information'))

WebUI.delay(2)

//WebUI.verifyTextPresent(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, false)

WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_CAT - Operations/input_Primary Email Address_primary-email-a_194327'), 'value', GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, 5)

//WebUI.verifyTextPresent('secondary.email@email.ca', false)

WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/User Look-up/button_Tests'))

WebUI.delay(2)

//click on view test details
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/User Look-up/svg_Checked-in_svg-inline--fa fa-binoculars fa-w-16'))

WebUI.delay(2)

//click close
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_Close_popup'))

WebUI.delay(2)

//click on invalidate access
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/User Look-up/svg_Checked-in_svg-inline--fa fa-times-circle fa-w-16'))

//click cancel
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/User Look-up/button_Cancel'))

WebUI.delay(2)

//click on invalidate access
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/User Look-up/svg_Checked-in_svg-inline--fa fa-times-circle fa-w-16'))

//set reason for invalidating the test
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/User Look-up/textarea_Reason for invalidating test access_reason-for-invalidating'), 
    'Katalon Testing')

//click invalidate button
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/User Look-up/button_Invalidate Access'))

WebUI.delay(2)

//click on view test details
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/User Look-up/svg_Checked-in_svg-inline--fa fa-binoculars fa-w-16'))

WebUI.delay(2)

//verify invalidate test reason
WebUI.verifyTextPresent('Katalon Testing', false)

//click close
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_Close_popup'))

WebUI.delay(2)

//verify status
WebUI.verifyTextPresent('Unassigned', false)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/User Look-up/button_Rights and Permissions'))

WebUI.delay(2)

// make sure that the user' permissions table contains the new requested permission
String permissionTitle = CustomKeywords.'database.DataGetting.getUserPermissionDefinitionData'(GlobalVariable.PPC_PERMISSION, 
    'en_name')

String permissionDescription = CustomKeywords.'database.DataGetting.getUserPermissionDefinitionData'(GlobalVariable.PPC_PERMISSION, 
    'en_description')

// validate permission title
WebUI.verifyEqual(WebUI.getText(findTestObject('Object Repository/Page_CAT - Business Operations/User Look-up/span_PPC RD Level 1')), 
    permissionTitle)

// validate permisiion description
WebUI.verifyEqual(WebUI.getText(findTestObject('Object Repository/Page_CAT - Business Operations/User Look-up/span_Responsible for data extraction or perform statistical analysis and generate various reports Create and upload test content')), 
    permissionDescription)

// validate permission status
WebUI.verifyEqual(WebUI.getText(findTestObject('Object Repository/Page_CAT - Business Operations/User Look-up/span_Pending')), 
    GlobalVariable.PENDING_PERMISSION_STATUS)

WebUI.verifyTextPresent('The user does not have any test access permissions.', false)

//delete test access codes of TA user if exists
CustomKeywords.'database.DataDeletion.deleteTaTestAccessCodes'(GlobalVariable.TA_USER_EMAIL_ADDRESS)

// delete pending user permissions for standard user three
CustomKeywords.'database.DataDeletion.deletePendingUserPermissions'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.PPC_PERMISSION)

// delete assigned tests data
CustomKeywords.'database.DataDeletion.deleteCandidateAssignedTestsData'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS)

// Logout and close
CustomKeywords.'basics.utils.logoutAndClose'()