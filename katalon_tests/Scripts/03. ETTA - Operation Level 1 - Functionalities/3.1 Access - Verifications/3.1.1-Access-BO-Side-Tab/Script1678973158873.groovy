import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// login (using right credentials)
CustomKeywords.'basics.utils.login'(GlobalVariable.ETTA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

//verify user lands on Rights and Permissions page
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/button_Rights and Permissions'), 
    2)

//Click on Menu
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_MENU'))

//Click Business Operations page
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/span_Business Operations'))

//verify user lands on Rights and Permissions page
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/button_Rights and Permissions'), 
    2)

// logout and close browser
CustomKeywords.'basics.utils.logoutAndClose'()