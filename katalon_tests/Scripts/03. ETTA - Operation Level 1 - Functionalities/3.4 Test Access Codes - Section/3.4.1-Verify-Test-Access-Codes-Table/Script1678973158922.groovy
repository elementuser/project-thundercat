import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

//delete test access codes of TA user if exists
CustomKeywords.'database.DataDeletion.deleteTaTestAccessCodes'(GlobalVariable.TA_USER_EMAIL_ADDRESS)

//delete test access codes of TA Two user if exists
CustomKeywords.'database.DataDeletion.deleteTaTestAccessCodes'(GlobalVariable.TA_USER_TWO_EMAIL_ADDRESS)

//Generate Test access codes using TA
//Generating  test access codes using API (Last argument number represents to generate number of test access codes here it generates 2)
String[] testAccessCodes = CustomKeywords.'api.api_requests.generateTestAccessCodeUsingAPI'(GlobalVariable.TA_USER_EMAIL_ADDRESS, 
    GlobalVariable.USERS_ENCRYPTED_PASSWORD, 2)

//Generate Test access codes using TA Two
//Generating  test access codes using API (Last argument number represents to generate number of test access codes here it generates 2)
String[] testAccessCodes2 = CustomKeywords.'api.api_requests.generateTestAccessCodeUsingAPI'(GlobalVariable.TA_USER_TWO_EMAIL_ADDRESS, 
    GlobalVariable.USERS_ENCRYPTED_PASSWORD, 2)

CustomKeywords.'basics.utils.NavigateToHomePage'()

// login (as a ETTA user)
CustomKeywords.'basics.utils.login'(GlobalVariable.ETTA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

// Click on Test Access Codes
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_Test Access Codes'))

// select 100 (display option)
WebUI.click(findTestObject('Page_CAT - Business Operations/svg_Number of results per page_css-tj5bde-Svg'))

WebUI.setText(findTestObject('Object Repository/Page_CAT - Operations/input_Number of results per page_react-sele_f780b1'),
	'100')

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Operations/input_Number of results per page_react-sele_f780b1'),
	Keys.chord(Keys.ENTER))

// Delay for 1 second
WebUI.delay(1)

// select 50 (display option)
WebUI.click(findTestObject('Page_CAT - Business Operations/svg_Number of results per page_css-tj5bde-Svg'))

WebUI.setText(findTestObject('Object Repository/Page_CAT - Operations/input_Number of results per page_react-sele_f780b1'),
	'50')

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Operations/input_Number of results per page_react-sele_f780b1'),
	Keys.chord(Keys.ENTER))

// Delay for 1 second
WebUI.delay(1)

// select 25 (display option)
WebUI.click(findTestObject('Page_CAT - Business Operations/svg_Number of results per page_css-tj5bde-Svg'))

WebUI.setText(findTestObject('Object Repository/Page_CAT - Operations/input_Number of results per page_react-sele_f780b1'),
	'25')

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Operations/input_Number of results per page_react-sele_f780b1'),
	Keys.chord(Keys.ENTER))

// Delay for 4 second
WebUI.delay(4)

// Search by Test Order number
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_test-access-codes-search-bar'), 
    'KATALON')

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_test-access-codes-search-bar'), 
    Keys.chord(Keys.ENTER))

// Add small delay
WebUI.delay(4)

// Wait for the load to be completed
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/active_test_access_codes_first_row_test_access_code_info'),
	25)

// Add delay of 2 seconds for the results to show up
WebUI.delay(2)

// clear search
WebUI.click(findTestObject('Page_CAT - Business Operations/button_Clear Search'))

// Add small delay
WebUI.delay(4)

// Search by TA name
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_test-access-codes-search-bar'), 
    (GlobalVariable.TA_USER_LAST_NAME + ', ') + GlobalVariable.TA_USER_FIRST_NAME)

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_test-access-codes-search-bar'), 
    Keys.chord(Keys.ENTER))

// Add small delay
WebUI.delay(4)

// Wait for the load to be completed
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/active_test_access_codes_first_row_test_access_code_info'),
	25)

// Add delay of 2 seconds for the results to show up
WebUI.delay(2)

//Clear search
WebUI.click(findTestObject('Page_CAT - Business Operations/button_Clear Search'))

// Add small delay
WebUI.delay(4)

// Search by second TA name
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_test-access-codes-search-bar'), 
    (GlobalVariable.TA_USER_TWO_LAST_NAME + ', ') + GlobalVariable.TA_USER_TWO_FIRST_NAME)

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_test-access-codes-search-bar'), 
    Keys.chord(Keys.ENTER))

// Add small delay
WebUI.delay(4)

// Wait for the load to be completed
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/active_test_access_codes_first_row_test_access_code_info'),
	25)

// Add delay of 2 seconds for the results to show up
WebUI.delay(2)

//Clear search
WebUI.click(findTestObject('Page_CAT - Business Operations/button_Clear Search'))

// Add small delay
WebUI.delay(4)

//search by test access Code
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_test-access-codes-search-bar'), 
    testAccessCodes[0])

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_test-access-codes-search-bar'), 
    Keys.chord(Keys.ENTER))

// Add small delay
WebUI.delay(4)

// Wait for the load to be completed
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/active_test_access_codes_first_row_test_access_code_info'),
	25)

// Add delay of 2 seconds for the results to show up
WebUI.delay(2)

//Clear search
WebUI.click(findTestObject('Page_CAT - Business Operations/button_Clear Search'))

// Add small delay
WebUI.delay(4)

// Search for no data
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_test-access-codes-search-bar'), 
    'CheckDataDoesNotExist')

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_test-access-codes-search-bar'), 
    Keys.chord(Keys.ENTER))

// Add small delay
WebUI.delay(4)

// Wait for the load to be completed
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/active_test_access_codes_no_data_found'),
	25)

// Add delay of 2 seconds for the results to show up
WebUI.delay(2)

WebUI.click(findTestObject('Page_CAT - Business Operations/button_Clear Search'))

// Add small delay
WebUI.delay(4)

// Delete all 4 test access codes
//search by test access Code
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_test-access-codes-search-bar'),
	testAccessCodes[0])

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_test-access-codes-search-bar'),
	Keys.chord(Keys.ENTER))

// Wait for the load to be completed
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/active_test_access_codes_first_row_test_access_code_info'),
	25)

// Add small delay
WebUI.delay(4)

//Click Delete
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/div_Delete'))

//Click cancel (to verify cancel button)
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_Cancel'))

//Click Delete
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/div_Delete'))

//Click Delete on popup
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/span_Delete'))

// Add small delay
WebUI.delay(4)

//search by test access Code
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_test-access-codes-search-bar'),
	testAccessCodes[1])

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_test-access-codes-search-bar'),
	Keys.chord(Keys.ENTER))

// Wait for the load to be completed
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/active_test_access_codes_first_row_test_access_code_info'),
	25)

// Add small delay
WebUI.delay(4)

//Click Delete
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/div_Delete'))

//Click Delete on popup
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/span_Delete'))

// Add small delay
WebUI.delay(4)

//search by test access Code
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_test-access-codes-search-bar'),
	testAccessCodes2[0])

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_test-access-codes-search-bar'),
	Keys.chord(Keys.ENTER))

// Wait for the load to be completed
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/active_test_access_codes_first_row_test_access_code_info'),
	25)

// Add small delay
WebUI.delay(4)

//Click Delete
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/div_Delete'))

//Click Delete on popup
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/span_Delete'))

// Add small delay
WebUI.delay(4)

//search by test access Code
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_test-access-codes-search-bar'),
	testAccessCodes2[1])

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_test-access-codes-search-bar'),
	Keys.chord(Keys.ENTER))

// Wait for the load to be completed
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/active_test_access_codes_first_row_test_access_code_info'),
	25)

// Add small delay
WebUI.delay(4)

//Click Delete
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/div_Delete'))

//Click Delete on popup
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/span_Delete'))

// Add small delay
WebUI.delay(4)

// logout
CustomKeywords.'basics.utils.logout'()

// login (as a TAuser)
CustomKeywords.'basics.utils.login'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

WebUI.delay(2)

WebUI.verifyTextNotPresent(testAccessCodes[0], true)

WebUI.verifyTextNotPresent(testAccessCodes[1], true)

// logout 
CustomKeywords.'basics.utils.logout'()

// login (as a TATwo user)
CustomKeywords.'basics.utils.login'(GlobalVariable.TA_USER_TWO_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

WebUI.delay(2)

WebUI.verifyTextNotPresent(testAccessCodes2[0], true)

WebUI.verifyTextNotPresent(testAccessCodes2[1], true)

// logout and close browser
CustomKeywords.'basics.utils.logoutAndClose'()