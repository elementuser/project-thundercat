import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// make sure that the ta user does not already have the test permission related to test order number: GlobalVariable.TEST_ACCESS_TEST_ORDER_NUMBER_2
CustomKeywords.'database.DataDeletion.deleteUserTestPermission'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.TEST_ACCESS_TEST_ORDER_NUMBER_2)

// Assign test access with test order name Katalon 2
CustomKeywords.'api.api_requests.assignTestAccessUsingAPI'(GlobalVariable.TA_USER_EMAIL_ADDRESS)

// login as ETTA user
CustomKeywords.'basics.utils.login'(GlobalVariable.ETTA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

WebUI.delay(2)

// select Test Access Management side navigation item
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_Test Access Management'))

// select Active Test Accesses tab
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/a_Active Test Accesses'))

WebUI.delay(4)

// search for KATALON2 test order number
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_active-test-accesses-search-bar'), 
    GlobalVariable.TEST_ACCESS_TEST_ORDER_NUMBER_2)

// simulate "Enter" key press
WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_active-test-accesses-search-bar'), 
    Keys.chord(Keys.ENTER))

// Wait till element is present
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/active_test_accesses_first_row_test_administrator'), 
    25)

// Verify test order 2 is found
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/div_KATALON2'), 2)

// Clear search
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/active_test_accesses_button_Clear Search'))

// adding a small delay
WebUI.delay(4)

// Search for KATALON2 test order number using name first name
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_active-test-accesses-search-bar'), 
    GlobalVariable.TA_USER_FIRST_NAME)

// simulate "Enter" key press
WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_active-test-accesses-search-bar'), 
    Keys.chord(Keys.ENTER))

// Wait till element is present
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/active_test_accesses_first_row_test_administrator'), 
    25)

// Verify test order 2 is found
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/div_KATALON2'), 2)

// Clear search
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/active_test_accesses_button_Clear Search'))

// adding a small delay
WebUI.delay(4)

// Search for KATALON2 test order number using name TA first name
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_active-test-accesses-search-bar'), 
    GlobalVariable.TA_USER_FIRST_NAME)

// simulate "Enter" key press
WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_active-test-accesses-search-bar'), 
    Keys.chord(Keys.ENTER))

// Wait till element is present
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/active_test_accesses_first_row_test_administrator'), 
    25)

// Verify test order 2 is found
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/div_KATALON2'), 2)

// Clear search
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/active_test_accesses_button_Clear Search'))

// adding a small delay
WebUI.delay(4)

// Search for KATALON2 test order number using name TA last name
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_active-test-accesses-search-bar'), 
    GlobalVariable.TA_USER_LAST_NAME)

// simulate "Enter" key press
WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_active-test-accesses-search-bar'), 
    Keys.chord(Keys.ENTER))

// Wait till element is present
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/active_test_accesses_first_row_test_administrator'), 
    25)

// Verify test order 2 is found
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/div_KATALON2'), 2)

// Clear search
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/active_test_accesses_button_Clear Search'))

// adding a small delay
WebUI.delay(4)

// Search for KATALON2 test order number using expiry date
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_active-test-accesses-search-bar'), 
    GlobalVariable.TEST_ACCESS_TEST_ORDER_EXPIRY_DATE)

// simulate "Enter" key press
WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_active-test-accesses-search-bar'), 
    Keys.chord(Keys.ENTER))

// Wait till element is present
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/active_test_accesses_first_row_test_administrator'), 
    25)

// Verify test order 2 is found
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/div_KATALON2'), 2)

// Clear search
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/active_test_accesses_button_Clear Search'))

// adding a small delay
WebUI.delay(4)

// Partial search for KATALON2 using test order number
String partialTestOrderName = GlobalVariable.TEST_ACCESS_TEST_ORDER_NUMBER_2

WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_active-test-accesses-search-bar'), 
    partialTestOrderName.substring(0, partialTestOrderName.length() - 3))

// simulate "Enter" key press
WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_active-test-accesses-search-bar'), 
    Keys.chord(Keys.ENTER))

// Wait till element is present
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/active_test_accesses_first_row_test_administrator'), 
    25)

// Verify test order 2 is found
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/div_KATALON2'), 2)

// Clear search
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/active_test_accesses_button_Clear Search'))

// adding a small delay
WebUI.delay(4)

// Partial search for KATALON2 using TA Name
String partialTAName = GlobalVariable.TEST_ACCESS_TEST_ORDER_NUMBER_2

WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_active-test-accesses-search-bar'), 
    partialTAName.substring(0, partialTAName.length() - 1))

// simulate "Enter" key press
WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_active-test-accesses-search-bar'), 
    Keys.chord(Keys.ENTER))

// Wait till element is present
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/active_test_accesses_first_row_test_administrator'), 
    25)

// Verify test order 2 is found
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/div_KATALON2'), 2)

// delete test access permissions
CustomKeywords.'database.DataDeletion.deleteUserTestPermission'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.TEST_ACCESS_TEST_ORDER_NUMBER_2)

// adding small delay
WebUI.delay(2)

// Logout and close
CustomKeywords.'basics.utils.logoutAndClose'()