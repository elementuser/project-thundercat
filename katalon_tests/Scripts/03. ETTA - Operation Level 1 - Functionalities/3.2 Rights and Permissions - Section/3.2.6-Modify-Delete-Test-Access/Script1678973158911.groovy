import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import basics.DateUtil
import java.util.Calendar
import java.text.DecimalFormat
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// make sure that there is no existing assigned test accesses related to GlobalVariable.TEST_ACCESS_TEST_ORDER_NUMBER_2
CustomKeywords.'database.DataDeletion.deleteUserTestPermission'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.TEST_ACCESS_TEST_ORDER_NUMBER_2)

// assign test access (parameters are set to "true", in order to test the error validation and since the TA already has an active test permission)
CustomKeywords.'basics.permissions.assignTestAccess'(true, true, GlobalVariable.TA_USER_FIRST_NAME, GlobalVariable.TA_USER_LAST_NAME)

// click Modify
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/active_test_accesses_button_Modify'))

// make sure that the right test permission has been selected
if (!(WebUI.getText(findTestObject('Object Repository/Page_CAT - Business Operations/modify_test_access_popup_description_paragraph')).contains(
    GlobalVariable.TA_USER_FIRST_NAME))) {
    KeywordUtil.markFailedAndStop('The following text has not been found: ' + GlobalVariable.TA_USER_FIRST_NAME)
}

if (!(WebUI.getText(findTestObject('Object Repository/Page_CAT - Business Operations/modify_test_access_popup_description_paragraph')).contains(
    GlobalVariable.TA_USER_LAST_NAME))) {
    KeywordUtil.markFailedAndStop('The following text has not been found: ' + GlobalVariable.TA_USER_LAST_NAME)
}

// update date (invalid date)

// Day
WebUI.click(findTestObject('Object Repository/Page_CAT - Operations/svg_Expiry Date_css-tj5bde-Svg'))

WebUI.setText(findTestObject('Object Repository/Page_CAT - Operations/div_31'),
	'31')

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Operations/div_31'), Keys.chord(Keys.ENTER))

// Month
WebUI.click(findTestObject('Object Repository/Page_CAT - Operations/svg_The current value is_css-tj5bde-Svg'))

WebUI.setText(findTestObject('Object Repository/Page_CAT - Operations/div_02'), 
    '02')

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Operations/div_02'), Keys.chord(Keys.ENTER))


// Year
DateUtil dateUtil = new DateUtil()

WebUI.click(findTestObject('Object Repository/Page_CAT - Operations/svg_The current value is_css-tj5bde-Svg_1'))

WebUI.setText(findTestObject('Object Repository/Page_CAT - Operations/div_2023'),
	"${dateUtil.getCurrentDateYear()}")

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Operations/div_2023'), Keys.chord(Keys.ENTER))

// click Modify
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/modify_test_access_popup_Modify_button'))

// make sure that respective error messages are displayed
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/modify_test_access_popup_Must be a valid Date'), 
    2)

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/modify_test_access_popup_You must provide a reason for modifying for this request'), 
    2)

// update date (passed date)

Calendar dateBeforeToday = dateUtil.getDateBeforeToday()

String yearNow = new DecimalFormat("00").format(dateBeforeToday.get(Calendar.YEAR))
String monthNow = new DecimalFormat("00").format(dateBeforeToday.get(Calendar.MONTH)+1)
String dayNow = new DecimalFormat("00").format(dateBeforeToday.get(Calendar.DAY_OF_MONTH))

// Day
WebUI.click(findTestObject('Object Repository/Page_CAT - Operations/svg_Expiry Date_css-tj5bde-Svg'))

WebUI.setText(findTestObject('Object Repository/Page_CAT - Operations/div_31'),
	"${dayNow}")

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Operations/div_31'), Keys.chord(Keys.ENTER))

// Month
WebUI.click(findTestObject('Object Repository/Page_CAT - Operations/svg_The current value is_css-tj5bde-Svg'))

WebUI.setText(findTestObject('Object Repository/Page_CAT - Operations/div_02'), 
    "${monthNow}")

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Operations/div_02'), Keys.chord(Keys.ENTER))


// Year
WebUI.click(findTestObject('Object Repository/Page_CAT - Operations/svg_The current value is_css-tj5bde-Svg_1'))

WebUI.setText(findTestObject('Object Repository/Page_CAT - Operations/div_2023'),
	"${yearNow}")

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Operations/div_2023'), Keys.chord(Keys.ENTER))

// add reason for modification
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/modify_test_access_popup_reason_for_modification'), 
    'Katalon Test Reason For Modification or Deletion')

// click Modify
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/modify_test_access_popup_Modify_button'))

// make sure that respective error messages are displayed
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/label_Cannot be a passed date'), 
    2)

// update date (valid date)

WebUI.click(findTestObject('Object Repository/Page_CAT - Operations/svg_The current value is_css-tj5bde-Svg_1'))

WebUI.setText(findTestObject('Object Repository/Page_CAT - Operations/div_2023'),
	"${dateUtil.getCurrentDateYear()+3}")

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Operations/div_2023'), Keys.chord(Keys.ENTER))

// adding reason for modification or deletion
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/modify_test_access_popup_reason_for_modification_or_deletion'), 
    'Katalon Test Reason For Modification or Deletion')

// click Modify
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/modify_test_access_popup_Modify_button'))

// adding 1 second delay
WebUI.delay(1)

// click OK
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/modify_test_access_confirmation_popup_button_OK'))

// Add delay of 4 seconds for the results to show up
WebUI.delay(4)

// search for KATALON2 test order number
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_active-test-accesses-search-bar'), 
    GlobalVariable.TEST_ACCESS_TEST_ORDER_NUMBER_2)

// Add delay of 4 seconds for the results to show up
WebUI.delay(4)

// simulate "Enter" key press
WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_active-test-accesses-search-bar'), 
    Keys.chord(Keys.ENTER))

// Wait for the load to be completed
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/active_permissions_first_row_user_name_and_contact_info'), 
    25)

// Add delay of 4 seconds for the results to show up
WebUI.delay(4)

// click Modify
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/active_test_accesses_button_Modify'))

// make sure that the right test permission has been selected
if (!(WebUI.getText(findTestObject('Object Repository/Page_CAT - Business Operations/modify_test_access_popup_description_paragraph')).contains(
    GlobalVariable.TA_USER_FIRST_NAME))) {
    KeywordUtil.markFailedAndStop('The following text has not been found: ' + GlobalVariable.TA_USER_FIRST_NAME)
}

if (!(WebUI.getText(findTestObject('Object Repository/Page_CAT - Business Operations/modify_test_access_popup_description_paragraph')).contains(
    GlobalVariable.TA_USER_LAST_NAME))) {
    KeywordUtil.markFailedAndStop('The following text has not been found: ' + GlobalVariable.TA_USER_LAST_NAME)
}

// make sure that we get the updated data (updated date)
WebUI.verifyEqual(WebUI.getText(findTestObject('Object Repository/Page_CAT - Business Operations/modify_test_access_popup_date_day_field')), 
    "${dayNow}")

WebUI.verifyEqual(WebUI.getText(findTestObject('Object Repository/Page_CAT - Business Operations/modify_test_access_popup_date_month_field')), 
    "${monthNow}")

WebUI.verifyEqual(WebUI.getText(findTestObject('Object Repository/Page_CAT - Business Operations/modify_test_access_popup_date_year_field')), 
    "${dateUtil.getCurrentDateYear()+3}")

// adding reason for modification or deletion
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/modify_test_access_popup_reason_for_modification_or_deletion'), 
    'Katalon Test Reason For Modification or Deletion')

// click Delete
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/modify_test_access_popup_Delete_button'))

// click Cancel
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/modify_test_access_delete_confirmation_popup_Cancel_button'))

// click Delete
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/modify_test_access_popup_Delete_button'))

// click Delete
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/modify_test_access_delete_confirmation_popup_Delete_button'))

// adding small delay
WebUI.delay(4)

// search for KATALON2 test order number
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_active-test-accesses-search-bar'), 
    GlobalVariable.TEST_ACCESS_TEST_ORDER_NUMBER_2)

// simulate "Enter" key press
WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_active-test-accesses-search-bar'), 
    Keys.chord(Keys.ENTER))

// adding small delay
WebUI.delay(2)

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/active_test_accesses_table_no_results_found_label'), 
    2)

// delete needed data
CustomKeywords.'database.DataDeletion.deleteUserTestPermission'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.TEST_ACCESS_TEST_ORDER_NUMBER_2)

// adding small delay
WebUI.delay(2)

// logout and close browser
CustomKeywords.'basics.utils.logoutAndClose'()