import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// send PPC Permission request for user one
CustomKeywords.'api.api_requests.sendPermissionRequestUsingAPI'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD, 
    GlobalVariable.PPC_PERMISSION)

// Approve permission request for standard user one
CustomKeywords.'api.api_requests.approvePermissionRequestUsingAPI'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.PPC_PERMISSION, 
    false)

// login as ETTA user
CustomKeywords.'basics.utils.login'(GlobalVariable.SUPER_USER_USERNAME, GlobalVariable.SUPER_USER_ENCRYPTED_PASSWORD)

WebUI.delay(2)

WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/a_Active Rights and Permissions'), 
    25)

// Select Active Rights and Permissions Tab
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/a_Active Rights and Permissions'))

// Wait for loading to be done
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/active_permissions_first_row_user_name_and_contact_info'), 
    25)

// Adding delay
WebUI.delay(2)

// search for user related to permission to approve
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_active-permissions-search-bar'), 
    GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS)

// simulate "Enter" key press
WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_active-permissions-search-bar'), 
    Keys.chord(Keys.ENTER))

// wait for loading to be done
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/pending_permissions_first_row_user_role'), 
    25)

// adding delay
WebUI.delay(2)

// click Modify
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_Modify'))

// make sure that the right active permission has been selected
if (!(WebUI.getText(findTestObject('Object Repository/Page_CAT - Business Operations/modify_active_permission_popup_description_paragraph')).contains(
    GlobalVariable.STANDARD_USER_ONE_FIRST_NAME))) {
    KeywordUtil.markFailedAndStop('Wrong active permission has been selected')
}

if (!(WebUI.getText(findTestObject('Object Repository/Page_CAT - Business Operations/modify_active_permission_popup_description_paragraph')).contains(
    GlobalVariable.STANDARD_USER_ONE_LAST_NAME))) {
    KeywordUtil.markFailedAndStop('Wrong active permission has been selected')
}

// fill out form (with invalid entries)
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Government of Canada Email Address_goc-email'), 
    'invalid.email.ca')

WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Supervisor Name_supervisor'), 'Katalon Supervisor1')

WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Supervisor Email Address_supervisor-email'), 
    'invalid.email.ca')

// click Modify
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/modify_active_permission_popup_button_Modify'))

// make sure that all respective error messages are displayed
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/modify_active_permission_popup_goc_error_message'), 
    2)

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/modify_active_permission_popup_supervisor_name_error_message'), 
    2)

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/modify_active_permission_popup_supervisor_email_error_message'), 
    2)

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/modify_active_permission_popup_reason_error_message'), 
    2)

// fill out form (with valid entries)
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Government of Canada Email Address_goc-email'), 
    GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS + 'updated')

WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Supervisor Name_supervisor'), 'Katalon Supervisor Updated')

WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Supervisor Email Address_supervisor-email'), 
    'katalon.supervisor@email.caupdated')

WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/modify_active_permission_popup_reason_for_modification'), 
    'Reason for modification Katalon Test')

// click Modify
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/modify_active_permission_popup_button_Modify'))

// click OK
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/modify_active_permission_confirmation_popup_button_OK'), 
    2)

WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/modify_active_permission_confirmation_popup_button_OK'))

// search for updated active permission
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_active-permissions-search-bar'), 
    GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS)

// simulate "Enter" key press
WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_active-permissions-search-bar'), 
    Keys.chord(Keys.ENTER))

// Wait for the load to be completed
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/active_permissions_first_row_user_name_and_contact_info'), 
    25)

// Add delay of 2 seconds for the results to show up
WebUI.delay(2)

// click Modify
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_Modify'))

// make sure that the right active permission has been selected
if (!(WebUI.getText(findTestObject('Object Repository/Page_CAT - Business Operations/modify_active_permission_popup_description_paragraph')).contains(
    GlobalVariable.STANDARD_USER_ONE_FIRST_NAME))) {
    KeywordUtil.markFailedAndStop('Wrong active permission has been selected')
}

if (!(WebUI.getText(findTestObject('Object Repository/Page_CAT - Business Operations/modify_active_permission_popup_description_paragraph')).contains(
    GlobalVariable.STANDARD_USER_ONE_LAST_NAME))) {
    KeywordUtil.markFailedAndStop('Wrong active permission has been selected')
}

// make sure that the data has been updated
gocEmail = WebUI.getAttribute(findTestObject('Object Repository/Page_CAT - Business Operations/input_Government of Canada Email Address_goc-email'), 
    'value')

supervisorName = WebUI.getAttribute(findTestObject('Object Repository/Page_CAT - Business Operations/input_Supervisor Name_supervisor'), 
    'value')

supervisorEmail = WebUI.getAttribute(findTestObject('Object Repository/Page_CAT - Business Operations/input_Supervisor Email Address_supervisor-email'), 
    'value')

WebUI.verifyEqual(gocEmail, GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS + 'updated')

WebUI.verifyEqual(supervisorName, 'Katalon Supervisor Updated')

WebUI.verifyEqual(supervisorEmail, 'katalon.supervisor@email.caupdated')

// add reason for deletion
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/textarea_Deleting Katalon User'), 'Deleting Katalon User')

// click Delete
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/modify_active_permission_popup_button_Delete'))

//click Cancel
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/modify_active_permission_popup_delete_confirmation_cancel_button'))

// click Delete
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/modify_active_permission_popup_button_Delete'))

// click Delete (confirmation)
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/modify_active_permission_popup_delete_confirmation_delete_button'))

// search for updated active permission
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_active-permissions-search-bar'), 
    GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS)

// simulate "Enter" key press
WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_active-permissions-search-bar'), 
    Keys.chord(Keys.ENTER))

// make sure that there are no results found
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/active_permissions_table_no_results_found_label'), 
    2)

// delete user permissions
CustomKeywords.'database.DataDeletion.deleteUserPermissions'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS)

// logout and close browser
CustomKeywords.'basics.utils.logoutAndClose'()

