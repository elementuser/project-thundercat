import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// ========== CREATE PENDING REQUESTS ==============
// send PPC Permission request for user one
CustomKeywords.'api.api_requests.sendPermissionRequestUsingAPI'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD, 
    GlobalVariable.SCORER_PERMISSION)

// send PPC Permission request for user two
CustomKeywords.'api.api_requests.sendPermissionRequestUsingAPI'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD, 
    GlobalVariable.SCORER_PERMISSION)

// send PPC Permission request for user three
CustomKeywords.'api.api_requests.sendPermissionRequestUsingAPI'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD, 
    GlobalVariable.SCORER_PERMISSION)

// ========== CREATE PENDING REQUESTS (END) ==============
// login as ETTA user
CustomKeywords.'basics.utils.login'(GlobalVariable.ETTA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

// Wait for the load to be completed
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/pending_permissions_first_row_user_role'), 
    25)

// Add delay of 2 seconds for the results to show up
WebUI.delay(2)

//========= Search Permissions ============
// Search by last name
WebUI.setText(findTestObject('Page_CAT - Business Operations/input_Search_permission-requests-search-bar'), GlobalVariable.STANDARD_USER_ONE_LAST_NAME)

// Click the search button 
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_Search'))

// Verify user one is found
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/div_UserOne Katalon (katalonuseroneemailca)'), 
    2)

// Clear search
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_Clear Search'))

// adding a small delay
WebUI.delay(1)

// Wait for the load to be completed
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/pending_permissions_first_row_user_role'), 
    25)

// Add delay of 2 seconds for the results to show up
WebUI.delay(2)

// Search by last name, first name
WebUI.setText(findTestObject('Page_CAT - Business Operations/input_Search_permission-requests-search-bar'), (GlobalVariable.STANDARD_USER_ONE_LAST_NAME + 
    ', ') + GlobalVariable.STANDARD_USER_ONE_FIRST_NAME)

// Press enter key
WebUI.sendKeys(findTestObject('Page_CAT - Business Operations/input_Search_permission-requests-search-bar'), Keys.chord(
        Keys.ENTER))

// Wait for the load to be completed
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/pending_permissions_first_row_user_role'), 
    25)

// Add delay of 2 seconds for the results to show up
WebUI.delay(2)

// Verify user found
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/div_UserOne Katalon (katalonuseroneemailca)'), 
    2)

// Clear search
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_Clear Search'))

// adding a small delay
WebUI.delay(1)

// Search by first name, last name
WebUI.setText(findTestObject('Page_CAT - Business Operations/input_Search_permission-requests-search-bar'), (GlobalVariable.STANDARD_USER_ONE_FIRST_NAME + 
    ', ') + GlobalVariable.STANDARD_USER_ONE_LAST_NAME)

// Press enter key
WebUI.sendKeys(findTestObject('Page_CAT - Business Operations/input_Search_permission-requests-search-bar'), Keys.chord(
        Keys.ENTER))

// Wait for the load to be completed
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/pending_permissions_first_row_user_role'), 
    25)

// Add delay of 2 seconds for the results to show up
WebUI.delay(2)

// Verify user found
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/div_UserOne Katalon (katalonuseroneemailca)'), 
    2)

// Clear search
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_Clear Search'))

// adding a small delay
WebUI.delay(1)

String lastName = GlobalVariable.STANDARD_USER_ONE_LAST_NAME

// Search by partial name 
WebUI.setText(findTestObject('Page_CAT - Business Operations/input_Search_permission-requests-search-bar'), lastName.substring(
        0, lastName.length() - 1))

// Press enter key
WebUI.sendKeys(findTestObject('Page_CAT - Business Operations/input_Search_permission-requests-search-bar'), Keys.chord(
        Keys.ENTER))

// Wait for the load to be completed
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/pending_permissions_first_row_user_role'), 
    25)

// Add delay of 2 seconds for the results to show up
WebUI.delay(2)

// Verify user found
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/div_UserOne Katalon (katalonuseroneemailca)'), 
    2)

// Clear search
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_Clear Search'))

// adding a small delay
WebUI.delay(1)

// Search by user role
WebUI.setText(findTestObject('Page_CAT - Business Operations/input_Search_permission-requests-search-bar'), 'SCORER')

// Press enter key
WebUI.sendKeys(findTestObject('Page_CAT - Business Operations/input_Search_permission-requests-search-bar'), Keys.chord(
        Keys.ENTER))

// Wait for the load to be completed
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/pending_permissions_first_row_user_role'), 
    25)

// Add delay of 2 seconds for the results to show up
WebUI.delay(2)

// Verify user found
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/div_UserOne Katalon (katalonuseroneemailca)'), 
    2)

// Clear search
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_Clear Search'))

// adding a small delay
WebUI.delay(1)

// Get todays year
Date todayYear = new Date()

String todayYearStr = todayYear.format('yyyy')

// Search using todays year
WebUI.setText(findTestObject('Page_CAT - Business Operations/input_Search_permission-requests-search-bar'), todayYearStr)

// Press enter key
WebUI.sendKeys(findTestObject('Page_CAT - Business Operations/input_Search_permission-requests-search-bar'), Keys.chord(
        Keys.ENTER))

// Wait for the load to be completed
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/pending_permissions_first_row_user_role'), 
    25)

// Add delay of 2 seconds for the results to show up
WebUI.delay(2)

// Verify user one is found
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/div_UserOne Katalon (katalonuseroneemailca)'), 
    2)

// Clear search
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_Clear Search'))

// adding a small delay
WebUI.delay(1)

// Search for data that does not exist
WebUI.setText(findTestObject('Page_CAT - Business Operations/input_Search_permission-requests-search-bar'), 'CheckDataDoesNotExist')

// Press enter key
WebUI.sendKeys(findTestObject('Page_CAT - Business Operations/input_Search_permission-requests-search-bar'), Keys.chord(
        Keys.ENTER))

// Wait for the load to be completed
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/pending_permissions_no_results_found'), 
    25)

// Add delay of 2 seconds for the results to show up
WebUI.delay(2)

// Verify no results found
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/label_0 result(s)'), 2)

// Clear search
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_Clear Search'))

// adding a small delay
WebUI.delay(1)

// Search for multiple users by name 'Katalon'
WebUI.setText(findTestObject('Page_CAT - Business Operations/input_Search_permission-requests-search-bar'), GlobalVariable.STANDARD_USER_ONE_FIRST_NAME)

// Press enter key
WebUI.sendKeys(findTestObject('Page_CAT - Business Operations/input_Search_permission-requests-search-bar'), Keys.chord(
        Keys.ENTER))

// Wait for the load to be completed
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/pending_permissions_first_row_user_role'), 
    25)

// Add delay of 2 seconds for the results to show up
WebUI.delay(2)

// Click on display number list
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/svg_Number of results per page_css-tj5bde-Svg_permission'))

// Delay for 1 second for the dropdown
WebUI.delay(1)

// Wait for dropdown (50) element
WebUI.waitForElementPresent(findTestObject('Page_CAT - Business Operations/div_50'), 25)

// Set display number to 3
WebUI.click(findTestObject('Page_CAT - Business Operations/div_50'))

// Check that results returned is 3
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/label_3 result(s)'), 2)

// Verify user one is found
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/div_UserOne Katalon (katalonuseroneemailca)'), 
    2)

// Verify user two is found
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/div_UserTwo Katalon (katalonusertwoemailca)'), 
    2)

// Verify user three is found
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/div_UserThree Katalon (katalonuserthreeemailca)'), 
    2)

// Click on display number list
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/svg_Number of results per page_css-tj5bde-Svg_permission'))

// Delay for 1 second for the dropdown
WebUI.delay(1)

// Set display number to 100
WebUI.click(findTestObject('Page_CAT - Business Operations/div_100'))

// Wait for the load to be completed
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/pending_permissions_first_row_user_role'), 
    25)

// Add delay of 2 seconds for the results to show up
WebUI.delay(2)

// Verify user one is found
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/div_UserOne Katalon (katalonuseroneemailca)'), 
    2)

// Verify user two is found
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/div_UserTwo Katalon (katalonusertwoemailca)'), 
    2)

// Verify user three is found
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/div_UserThree Katalon (katalonuserthreeemailca)'), 
    2)

// Clear search
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_Clear Search'))

// adding a small delay
WebUI.delay(1)

// delete pending user permissions for standard user one
CustomKeywords.'database.DataDeletion.deletePendingUserPermissions'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.PPC_PERMISSION)

// delete pending user permissions for standard user two
CustomKeywords.'database.DataDeletion.deletePendingUserPermissions'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS, GlobalVariable.PPC_PERMISSION)

// delete pending user permissions for standard user three
CustomKeywords.'database.DataDeletion.deletePendingUserPermissions'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.PPC_PERMISSION)

// Logout and close
CustomKeywords.'basics.utils.logoutAndClose'()

