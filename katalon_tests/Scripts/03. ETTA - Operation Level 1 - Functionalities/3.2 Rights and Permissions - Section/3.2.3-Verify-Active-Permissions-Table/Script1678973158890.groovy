import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// ========== CREATE PENDING REQUESTS ==============
// send PPC Permission request for user one
CustomKeywords.'api.api_requests.sendPermissionRequestUsingAPI'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD, 
    GlobalVariable.SCORER_PERMISSION)

// send PPC Permission request for user two
CustomKeywords.'api.api_requests.sendPermissionRequestUsingAPI'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD, 
    GlobalVariable.SCORER_PERMISSION)

// send PPC Permission request for user three
CustomKeywords.'api.api_requests.sendPermissionRequestUsingAPI'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD, 
    GlobalVariable.SCORER_PERMISSION)

// ========== CREATE PENDING REQUESTS (END) ==============
// ========== APPROVE PENDING REQUESTS ===================
// Approve permission request for standard user one
CustomKeywords.'api.api_requests.approvePermissionRequestUsingAPI'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.SCORER_PERMISSION, 
    false)

// Approve permission request for standard user two
CustomKeywords.'api.api_requests.approvePermissionRequestUsingAPI'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS, GlobalVariable.SCORER_PERMISSION, 
    false)

// Approve permission request for standard user three
CustomKeywords.'api.api_requests.approvePermissionRequestUsingAPI'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.SCORER_PERMISSION, 
    false)

// ========== APPROVE PENDING REQUESTS (END) ==============
// login as ETTA user
CustomKeywords.'basics.utils.login'(GlobalVariable.ETTA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

WebUI.delay(2)

WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/a_Active Rights and Permissions'), 
    25)

// Select Active Rights and Permissions Tab
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/a_Active Rights and Permissions'))

// Wait for loading to be done
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/active_permissions_first_row_user_name_and_contact_info'), 
    25)

// Adding delay
WebUI.delay(2)

// Search by last name
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_active-permissions-search-bar'), 
    GlobalVariable.STANDARD_USER_ONE_LAST_NAME)

// Press enter key
WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_active-permissions-search-bar'), 
    Keys.chord(Keys.ENTER))

// Wait for the load to be completed
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/active_permissions_first_row_user_name_and_contact_info'), 
    25)

// Add delay of 2 seconds for the results to show up
WebUI.delay(2)

// Verify user one is found
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/td_UserOne Katalon (katalonuseroneemailca)'), 
    2)

// Clear search
WebUI.click(findTestObject('Page_CAT - Business Operations/button_Clear Search-active'))

// adding a small delay
WebUI.delay(2)

// Wait for the load to be completed
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/active_permissions_first_row_user_name_and_contact_info'), 
    25)

// Add delay of 2 seconds for the results to show up
WebUI.delay(2)

// Search by last name, first name
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_active-permissions-search-bar'), 
    (GlobalVariable.STANDARD_USER_ONE_LAST_NAME + ', ') + GlobalVariable.STANDARD_USER_ONE_FIRST_NAME)

// Press enter key
WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_active-permissions-search-bar'), 
    Keys.chord(Keys.ENTER))

// Wait for the load to be completed
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/active_permissions_first_row_user_name_and_contact_info'), 
    25)

// Add delay of 2 seconds for the results to show up
WebUI.delay(2)

// Verify user found
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/td_UserOne Katalon (katalonuseroneemailca)'), 
    2)

// Clear search
WebUI.click(findTestObject('Page_CAT - Business Operations/button_Clear Search-active'))

// adding a small delay
WebUI.delay(2)

// Search by first name, last name
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_active-permissions-search-bar'), 
    (GlobalVariable.STANDARD_USER_ONE_FIRST_NAME + ', ') + GlobalVariable.STANDARD_USER_ONE_LAST_NAME)

// Press enter key
WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_active-permissions-search-bar'), 
    Keys.chord(Keys.ENTER))

// Wait for the load to be completed
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/active_permissions_first_row_user_name_and_contact_info'), 
    25)

// Add delay of 2 seconds for the results to show up
WebUI.delay(2)

// Verify user found
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/div_UserOne Katalon (katalonuseroneemailca)'), 
    2)

// Clear search
WebUI.click(findTestObject('Page_CAT - Business Operations/button_Clear Search-active'))

// adding a small delay
WebUI.delay(2)

String lastName = GlobalVariable.STANDARD_USER_ONE_LAST_NAME

// Search by partial name
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_active-permissions-search-bar'), 
    lastName.substring(0, lastName.length() - 1))

// Press enter key
WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_active-permissions-search-bar'), 
    Keys.chord(Keys.ENTER))

// Wait for the load to be completed
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/active_permissions_first_row_user_name_and_contact_info'), 
    25)

// Add delay of 2 seconds for the results to show up
WebUI.delay(2)

// Verify user found
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/td_UserOne Katalon (katalonuseroneemailca)'), 
    2)

// Clear search
WebUI.click(findTestObject('Page_CAT - Business Operations/button_Clear Search-active'))

// adding a small delay
WebUI.delay(2)

// Search by user role
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_active-permissions-search-bar'), 
    'SCORER')

// Press enter key
WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_active-permissions-search-bar'), 
    Keys.chord(Keys.ENTER))

// Wait for the load to be completed
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/active_permissions_first_row_user_name_and_contact_info'), 
    25)

// Add delay of 2 seconds for the results to show up
WebUI.delay(2)

// Verify user found
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/td_UserOne Katalon (katalonuseroneemailca)'), 
    2)

// Clear search
WebUI.click(findTestObject('Page_CAT - Business Operations/button_Clear Search-active'))

// Wait for the load to be completed
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/active_permissions_first_row_user_name_and_contact_info'), 
    25)

// adding a small delay
WebUI.delay(2)

// Search for data that does not exist
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_active-permissions-search-bar'), 
    'CheckDataDoesNotExist')

// Press enter key
WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_active-permissions-search-bar'), 
    Keys.chord(Keys.ENTER))

// adding small delay
WebUI.delay(2)

// Wait for the load to be completed
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/active_permissions_table_no_results_found_label'), 
    25)

// Add delay of 2 seconds for the results to show up
WebUI.delay(2)

// Verify no results found
WebUI.verifyElementPresent(findTestObject('Page_CAT - Business Operations/label_0 result(s)-active'), 2)

// Clear search
WebUI.click(findTestObject('Page_CAT - Business Operations/button_Clear Search-active'))

// adding a small delay
WebUI.delay(2)

// Search for multiple users by name 'Katalon'
String partialLastName = GlobalVariable.STANDARD_USER_ONE_LAST_NAME

WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_active-permissions-search-bar'), 
    (GlobalVariable.STANDARD_USER_ONE_FIRST_NAME + ', ') + partialLastName.substring(0, 4))

// Press enter key
WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_active-permissions-search-bar'), 
    Keys.chord(Keys.ENTER))

// Wait for the load to be completed
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/active_permissions_first_row_user_name_and_contact_info'), 
    25)

// Add delay of 2 seconds for the results to show up
WebUI.delay(2)

// Click on display number list
WebUI.click(findTestObject('Page_CAT - Business Operations/svg_Number of results per page_css-tj5bde-Svg-active'))

// Delay for 1 second for the dropdown
WebUI.delay(2)

// Check that results returned is 3
WebUI.verifyElementPresent(findTestObject('Page_CAT - Business Operations/label_3 result(s)-active'), 2)

// Verify user one is found
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/td_UserOne Katalon (katalonuseroneemailca)'), 
    2)

// Verify user two is found
WebUI.verifyElementPresent(findTestObject('Page_CAT - Business Operations/td_UserTwo Katalon (katalonusertwoemailca)-active-first-page'), 
    2)

// Verify user three is found
WebUI.verifyElementPresent(findTestObject('Page_CAT - Business Operations/td_UserThree Katalon (katalonuserthreeemailca)-active-first-page'), 
    2)

// delete user permissions for standard user one
CustomKeywords.'database.DataDeletion.deleteUserPermissions'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS)

// delete user permissions for standard user two
CustomKeywords.'database.DataDeletion.deleteUserPermissions'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS)

// delete user permissions for standard user three
CustomKeywords.'database.DataDeletion.deleteUserPermissions'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

// Logout and close
CustomKeywords.'basics.utils.logoutAndClose'()

