import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// make sure that there is no current assigned tests data
CustomKeywords.'database.DataDeletion.deleteCandidateAssignedTestsData'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

// make sure that there is no channel presence related to user
CustomKeywords.'database.DataDeletion.deleteCandidateChannelPresence'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

//Generate Test access codes using TA
//Generating  test access codes using API (Last argument number represents to generate number of test access codes here it generates 1)
String[] testAccessCode = CustomKeywords.'api.api_requests.generateTestAccessCodeUsingAPI'(GlobalVariable.TA_USER_EMAIL_ADDRESS, 
    GlobalVariable.USERS_ENCRYPTED_PASSWORD, 1)

//candidate check in
CustomKeywords.'api.api_requests.candidateCheckInUsingAPI'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD, 
    testAccessCode[0])

//login as TA
CustomKeywords.'basics.utils.login'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

//Click Active candidates
WebUI.click(findTestObject('Page_CAT - TA/button_Active Candidates'))

// wait for the table to be loaded
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Test Administrator/ta_active_candidates_first_row_candidate_name'), 
    25)

// adding a 2 seconds delay
WebUI.delay(2)

//Click Approve
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/button_Checked-in_secondary_approve'))

// adding a 2 seconds delay
WebUI.delay(2)

// logout
CustomKeywords.'basics.utils.logout'()

//login as candidate
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

//Click on Start
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Start'))

WebUI.delay(2)

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// login (using TA credentials)
CustomKeywords.'basics.utils.login'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

//Click Active candidates
WebUI.click(findTestObject('Page_CAT - TA/button_Active Candidates'))

// wait for the table to be loaded
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Test Administrator/ta_active_candidates_first_row_candidate_name'), 
    25)

// adding a 2 seconds delay
WebUI.delay(2)

//Click on the lock button
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/btn-lock-icon'))

WebUI.click(findTestObject('Object Repository/Page_CAT - TA/button_Lock the Test'))

WebUI.delay(1)

// logout
CustomKeywords.'basics.utils.logout'()

// adding 2 seconds delay
WebUI.delay(2)

// make sure that there is no channel presence related this user (this part was often failing)
CustomKeywords.'database.DataDeletion.deleteCandidateChannelPresence'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

//login as candidate
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

//verify that the test is locked
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT-Test locked/label_Test Locked'), 10)

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT-Test locked/p_Please contact your test administrator for further instructions'), 
    2)

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT-Test locked/p_Your test has been locked by the test administrator'), 
    2)

WebUI.closeBrowser()

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// login (using TA credentials)
CustomKeywords.'basics.utils.login'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

WebUI.delay(2)

//Delete the generated test-access-code
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/div_Delete'))

//Click Delete on popup
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/span_Delete'))

//Click Active candidates
WebUI.click(findTestObject('Page_CAT - TA/button_Active Candidates'))

// wait for the table to be loaded
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Test Administrator/ta_active_candidates_first_row_candidate_name'), 
    25)

// adding a 2 seconds delay
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Page_CAT - TA/btn-lock-icon'))

WebUI.click(findTestObject('Object Repository/Page_CAT - TA/button_Cancel'))

//Unlock the test
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/btn-lock-icon'))

WebUI.click(findTestObject('Object Repository/Page_CAT - TA/button_Unlock Test'))

WebUI.delay(1)

// logout
CustomKeywords.'basics.utils.logout'()

//login as candidate
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

//Click on Quit Test
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Test Welcome/button_Quit Test'), 10)

WebUI.click(findTestObject('Object Repository/Page_CAT - Test Welcome/button_Quit Test'))

//Select the check boxes
WebUI.click(findTestObject('Object Repository/Page_CAT - Quit Test/input_Quit Test_checkbox-condition-0'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Quit Test/input_I choose to quit this test_checkbox-condition-1'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Quit Test/input_I understand that my test will not be scored_checkbox-condition-2'))

//click on quit test
WebUI.click(findTestObject('Object Repository/Page_CAT - Quit Test/button_Quit Test'))

//click on return to home page
WebUI.click(findTestObject('Object Repository/Page_CAT - Quit Test/button_Return to Home Page'))

// delete assigned tests data related
CustomKeywords.'database.DataDeletion.deleteCandidateAssignedTestsData'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

// logout
CustomKeywords.'basics.utils.logoutAndClose'()