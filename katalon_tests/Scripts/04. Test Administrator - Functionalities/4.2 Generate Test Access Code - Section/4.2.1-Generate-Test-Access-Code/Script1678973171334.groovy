import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

CustomKeywords.'basics.ta_utils.generateTestAccessCode'()

//Click Delete
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/div_Delete'))

//Click Cancel
WebUI.click(findTestObject('Page_CAT - TA/span_Cancel'))

//Click Delete
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/div_Delete'))

//Click Delete on popup
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/span_Delete'))

// logout and close browser
CustomKeywords.'basics.utils.logoutAndClose'()