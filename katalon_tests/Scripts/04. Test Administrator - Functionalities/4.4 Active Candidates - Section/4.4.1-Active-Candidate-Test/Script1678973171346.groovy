import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// make sure that there is no current assigned tests data (even other users in case other tests failed)
CustomKeywords.'database.DataDeletion.deleteCandidateAssignedTestsData'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS)

CustomKeywords.'database.DataDeletion.deleteCandidateAssignedTestsData'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS)

CustomKeywords.'database.DataDeletion.deleteCandidateAssignedTestsData'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

// make sure that there is no channel presence related to user (even other users in case other tests failed)
CustomKeywords.'database.DataDeletion.deleteCandidateChannelPresence'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS)

CustomKeywords.'database.DataDeletion.deleteCandidateChannelPresence'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS)

CustomKeywords.'database.DataDeletion.deleteCandidateChannelPresence'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

// make sure that there is no active test access codes
CustomKeywords.'database.DataDeletion.deleteTaTestAccessCodes'(GlobalVariable.TA_USER_EMAIL_ADDRESS)

// ========== REMOVING TEST ACCESS BEFORE STARTING THE TEST ==========
//Generate Test access codes using TA
//Generating  test access codes using API (Last argument number represents to generate number of test access codes here it generates 1)
String[] testAccessCode = CustomKeywords.'api.api_requests.generateTestAccessCodeUsingAPI'(GlobalVariable.TA_USER_EMAIL_ADDRESS, 
    GlobalVariable.USERS_ENCRYPTED_PASSWORD, 1)

//candidate check in
CustomKeywords.'api.api_requests.candidateCheckInUsingAPI'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD, 
    testAccessCode[0])

//login as TA
CustomKeywords.'basics.utils.login'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

//Click Active candidates
WebUI.click(findTestObject('Page_CAT - TA/button_Active Candidates'))

// wait for the table to be loaded
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Test Administrator/ta_active_candidates_first_row_candidate_name'), 
    25)

// adding a 2 seconds delay
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Page_CAT - TA/button_Checked-in_secondary_trash'))

//Click Cancel
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/span_Cancel'))

WebUI.click(findTestObject('Object Repository/Page_CAT - TA/button_Checked-in_secondary_trash'))

//Remove candidate access to the test code
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/span_Remove Access'))

WebUI.click(findTestObject('Object Repository/Page_CAT - TA/button_Test Access Codes'))

//Delete the generated test-access-code
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/div_Delete'))

//Click Delete on popup
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/span_Delete'))

// logout
CustomKeywords.'basics.utils.logout'()

// login as candidate
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - TA/label_You have no assigned tests'), 2)

// logout
CustomKeywords.'basics.utils.logout'()

// ========== REMOVING TEST ACCESS BEFORE STARTING THE TEST (END) ==========
// ========== REMOVING TEST ACCESS AFTER STARTING THE TEST ==========
//Generate Test access codes using TA
//Generating  test access codes using API (Last argument number represents to generate number of test access codes here it generates 1)
String[] testAccessCode2 = CustomKeywords.'api.api_requests.generateTestAccessCodeUsingAPI'(GlobalVariable.TA_USER_EMAIL_ADDRESS, 
    GlobalVariable.USERS_ENCRYPTED_PASSWORD, 1)

//candidate check in
CustomKeywords.'api.api_requests.candidateCheckInUsingAPI'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD, 
    testAccessCode2[0])

//login as TA
CustomKeywords.'basics.utils.login'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

//Delete the generated test-access-code
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/div_Delete'))

//Click Delete on popup
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/span_Delete'))

//Click Active candidates
WebUI.click(findTestObject('Page_CAT - TA/button_Active Candidates'))

// wait for the table to be loaded
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Test Administrator/ta_active_candidates_first_row_candidate_name'), 
    25)

// adding a 2 seconds delay
WebUI.delay(2)

//Click Approve
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/button_Checked-in_secondary_approve'))

// logout
CustomKeywords.'basics.utils.logout'()

//login as Candidate
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

// adding small delay
WebUI.delay(2)

// click Start (starting test)
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/candidate_dashboard_start_test_button'))

// adding small delay
WebUI.delay(2)

// close the browser
WebUI.closeBrowser()

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

//login as TA
CustomKeywords.'basics.utils.login'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

//Click Active candidates
WebUI.click(findTestObject('Page_CAT - TA/button_Active Candidates'))

// wait for the table to be loaded
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Test Administrator/ta_active_candidates_first_row_candidate_name'), 
    25)

// adding a 2 seconds delay
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Page_CAT - TA/button_Checked-in_secondary_trash'))

//Remove candidate access to the test code
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/span_Remove Access'))

// adding small delay
WebUI.delay(2)

// make sure that the active candidates table is empty
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Test Administrator/active_candidates_table_There are no active candidates'), 
    2)

// logout
CustomKeywords.'basics.utils.logout'()

// login as candidate
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - TA/label_You have no assigned tests'), 2)

// ========== REMOVING TEST ACCESS AFTER STARTING THE TEST (END) ==========
// delete assigned tests data related
CustomKeywords.'database.DataDeletion.deleteCandidateAssignedTestsData'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

// logout and close browser
CustomKeywords.'basics.utils.logoutAndClose'()