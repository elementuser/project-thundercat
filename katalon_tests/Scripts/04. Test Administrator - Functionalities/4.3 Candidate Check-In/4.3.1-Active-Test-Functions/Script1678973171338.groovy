import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable


// Making sure that the TA has the Test Adaptations Permission
// Verify if TA as the TA Permissions
if(!new database.DataValidation().userPermissionAlreadyAssociated(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.TA_PERMISSION)) {
	// send TA permission for our TA
	CustomKeywords.'api.api_requests.sendPermissionRequestUsingAPI'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD,
		GlobalVariable.TA_PERMISSION)
	
	// Approve permission request for our TA
	CustomKeywords.'api.api_requests.approvePermissionRequestUsingAPI'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.TA_PERMISSION,
		false)
}
// Verify if TA as the Adaptations Permissions
if(!new database.DataValidation().userPermissionAlreadyAssociated(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.ADAPTATIONS_PERMISSION)) {
	// send Adaptations permission for our TA
	CustomKeywords.'api.api_requests.sendPermissionRequestUsingAPI'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD,
		GlobalVariable.ADAPTATIONS_PERMISSION)
	
	// Approve permission request for our TA
	CustomKeywords.'api.api_requests.approvePermissionRequestUsingAPI'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.ADAPTATIONS_PERMISSION,
		false)
}

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// adding a 2 seconds delay
WebUI.delay(2)

// make sure that there is no current assigned tests data
CustomKeywords.'database.DataDeletion.deleteCandidateAssignedTestsData'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

// make sure that there is no channel presence related to user
CustomKeywords.'database.DataDeletion.deleteCandidateChannelPresence'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

//Generate Test access code
CustomKeywords.'basics.ta_utils.generateTestAccessCode'()

String testAccessCode = WebUI.getText(findTestObject('Object Repository/Page_CAT - TA/label_TI5PTVNLLX'))

// logout
CustomKeywords.'basics.utils.logout'()

// complete profile
CustomKeywords.'basics.candidate_utils.runCompleteProfile'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

//candidate check in
CustomKeywords.'basics.candidate_utils.candidateCheckIn'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, testAccessCode)

// logout
CustomKeywords.'basics.utils.logout'()

//candidate check in Error validation
CustomKeywords.'basics.candidate_utils.runCandidateCheckInErrorValidation'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS,
	testAccessCode)

//login as TA
CustomKeywords.'basics.utils.login'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

//Click Active candidates
WebUI.click(findTestObject('Page_CAT - TA/button_Active Candidates'))

// wait for the table to be loaded
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Test Administrator/ta_active_candidates_first_row_candidate_name'), 
    25)

// adding a 2 seconds delay
WebUI.delay(2)

//Click edit time limit
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/button_Checked-in_secondary-time-limit'))

//update time limit
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/svg__svg-inline--fa fa-sort-up fa-w-10'))

WebUI.click(findTestObject('Object Repository/Page_CAT - TA/svg__svg-inline--fa fa-sort-up fa-w-10'))

//Click cancel
WebUI.click(findTestObject('Page_CAT - TA/span_Cancel'))

//click edit time limit
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/button_Checked-in_secondary-time-limit'))

//update time limit
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/svg__svg-inline--fa fa-sort-up fa-w-10'))

WebUI.click(findTestObject('Object Repository/Page_CAT - TA/svg__svg-inline--fa fa-sort-up fa-w-10'))

//Click set timer
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/span_Set Timer'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Page_CAT - TA/button_Checked-in_secondary_trash'))

//Remove candidate access to the test code
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/span_Remove Access'))

WebUI.click(findTestObject('Object Repository/Page_CAT - TA/button_Test Access Codes'))

//Delete the generated test-access-code
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/div_Delete'))

//Click Delete on popup
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/span_Delete'))

// delete assigned tests data related
CustomKeywords.'database.DataDeletion.deleteCandidateAssignedTestsData'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

// logout and close browser
CustomKeywords.'basics.utils.logoutAndClose'()