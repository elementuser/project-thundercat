import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// make sure that there is no current assigned tests data
CustomKeywords.'database.DataDeletion.deleteCandidateAssignedTestsData'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS)

CustomKeywords.'database.DataDeletion.deleteCandidateAssignedTestsData'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

// make sure that there is no channel presence related to first and second candidates that we'll use in this use case
CustomKeywords.'database.DataDeletion.deleteCandidateChannelPresence'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS)

CustomKeywords.'database.DataDeletion.deleteCandidateChannelPresence'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

//Generate Test access codes using TA
//Generating  test access codes using API (Last argument number represents to generate number of test access codes here it generates 1)
String[] testAccessCode = CustomKeywords.'api.api_requests.generateTestAccessCodeUsingAPI'(GlobalVariable.TA_USER_EMAIL_ADDRESS, 
    GlobalVariable.USERS_ENCRYPTED_PASSWORD, 1)

//first candidate check in
CustomKeywords.'api.api_requests.candidateCheckInUsingAPI'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD, 
    testAccessCode[0])

//second candidate check in
CustomKeywords.'api.api_requests.candidateCheckInUsingAPI'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD, 
    testAccessCode[0])

// login as TA
CustomKeywords.'basics.utils.login'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

// click Active candidates
WebUI.click(findTestObject('Page_CAT - TA/button_Active Candidates'))

// wait for the table to be loaded
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Test Administrator/ta_active_candidates_first_row_candidate_name'), 
    25)

// adding a 2 seconds delay
WebUI.delay(2)

// click Approve (first candidate)
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/svg_Checked-in_svg-inline--fa fa-thumbs-up fa-w-16'))

// adding a 2 seconds delay
WebUI.delay(2)

// click Approve (second candidate)
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/svg_Checked-in_svg-inline--fa fa-thumbs-up fa-w-16_2'))

// click Lock All
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Lock All'))

// click Cancel 
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Cancel'))

// click Lock All
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Lock All'))

// click Lock Tests
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Lock Tests'))

// wait for the table to be loaded
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Test Administrator/ta_active_candidates_first_row_candidate_name'), 
    25)

// logout
CustomKeywords.'basics.utils.logout'()

// login as first candidate
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

// adding this condition, since this test was often faling in here because of the websocket connection error
// if connection error page is displayed
if (WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Websocket Connection Error/connection_error_button_Logout'), 
    5, FailureHandling.OPTIONAL)) {
    // close the browser
    WebUI.closeBrowser()

    // open browser and navigate to login page
    CustomKeywords.'basics.utils.NavigateToHomePage'()

    // delete any channel presence related to this user
    CustomKeywords.'database.DataDeletion.deleteCandidateChannelPresence'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS)

    // adding 2 seconds delay
    WebUI.delay(2)

    //login as candidate
    CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)
}

// make sure that the test is locked
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Test Welcome/label_Test Locked'), 10)

// close the browser
WebUI.closeBrowser()

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// login as second candidate
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

// adding this condition, since this test was often faling in here because of the websocket connection error
// if connection error page is displayed
if (WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Websocket Connection Error/connection_error_button_Logout'), 
    5, FailureHandling.OPTIONAL)) {
    // close the browser
    WebUI.closeBrowser()

    // open browser and navigate to login page
    CustomKeywords.'basics.utils.NavigateToHomePage'()

    // delete any channel presence related to this user
    CustomKeywords.'database.DataDeletion.deleteCandidateChannelPresence'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

    // adding 2 seconds delay
    WebUI.delay(2)

    //login as candidate
    CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)
}

// make sure that the test is locked
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Test Welcome/label_Test Locked'), 10)

// close the browser
WebUI.closeBrowser()

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// login as TA
CustomKeywords.'basics.utils.login'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

// adding a 2 seconds delay
WebUI.delay(2)

//Delete the generated test-access-code
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/div_Delete'))

//Click Delete on popup
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/span_Delete'))

// click Active candidates
WebUI.click(findTestObject('Page_CAT - TA/button_Active Candidates'))

// wait for the table to be loaded
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Test Administrator/ta_active_candidates_first_row_candidate_name'), 
    25)

// adding a 2 seconds delay
WebUI.delay(2)

// click Unlock All
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Unlock All'))

// click Cancel
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Cancel'))

// click Unlock All
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Unlock All'))

// click Unlock All Tests
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Unlock All Tests'))

// wait for the table to be loaded
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Test Administrator/ta_active_candidates_first_row_candidate_name'), 
    25)

// logout
CustomKeywords.'basics.utils.logout'()

// make sure that there is no channel presence related this user (this was failing often in headless mode)
CustomKeywords.'database.DataDeletion.deleteCandidateChannelPresence'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS)

// login as first candidate
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

// make sure that the candidate can now start the test
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Start'))

// adding this condition, since this test was often faling in here because of the websocket connection error
// if connection error page is displayed
if (WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Websocket Connection Error/connection_error_button_Logout'), 
    5, FailureHandling.OPTIONAL)) {
    // close the browser
    WebUI.closeBrowser()

    // open browser and navigate to login page
    CustomKeywords.'basics.utils.NavigateToHomePage'()

    // delete any channel presence related to this user
    CustomKeywords.'database.DataDeletion.deleteCandidateChannelPresence'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS)

    // adding 2 seconds delay
    WebUI.delay(2)

    //login as candidate
    CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)
}

// click on Quit Test
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Test Welcome/button_Quit Test'), 10)

WebUI.click(findTestObject('Object Repository/Page_CAT - Test Welcome/button_Quit Test'))

// select the check boxes
WebUI.click(findTestObject('Object Repository/Page_CAT - Quit Test/input_Quit Test_checkbox-condition-0'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Quit Test/input_I choose to quit this test_checkbox-condition-1'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Quit Test/input_I understand that my test will not be scored_checkbox-condition-2'))

// click on quit test
WebUI.click(findTestObject('Object Repository/Page_CAT - Quit Test/button_Quit Test'))

// click on return to home page
WebUI.click(findTestObject('Object Repository/Page_CAT - Quit Test/button_Return to Home Page'))

// logout
CustomKeywords.'basics.utils.logout'()

// make sure that there is no channel presence related this user (this was failing often in headless mode)
CustomKeywords.'database.DataDeletion.deleteCandidateChannelPresence'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

// adding 2 seconds delay
WebUI.delay(2)

// login as second candidate
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

// make sure that the candidate can now start the test
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Start'))

// adding this condition, since this test was often faling in here because of the websocket connection error
// if connection error page is displayed
if (WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Websocket Connection Error/connection_error_button_Logout'), 
    5, FailureHandling.OPTIONAL)) {
    // close the browser
    WebUI.closeBrowser()

    // open browser and navigate to login page
    CustomKeywords.'basics.utils.NavigateToHomePage'()

    // delete any channel presence related to this user
    CustomKeywords.'database.DataDeletion.deleteCandidateChannelPresence'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

    // adding 2 seconds delay
    WebUI.delay(2)

    //login as candidate
    CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)
}

// click on Quit Test
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Test Welcome/button_Quit Test'), 10)

WebUI.click(findTestObject('Object Repository/Page_CAT - Test Welcome/button_Quit Test'))

// select the check boxes
WebUI.click(findTestObject('Object Repository/Page_CAT - Quit Test/input_Quit Test_checkbox-condition-0'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Quit Test/input_I choose to quit this test_checkbox-condition-1'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Quit Test/input_I understand that my test will not be scored_checkbox-condition-2'))

// click on quit test
WebUI.click(findTestObject('Object Repository/Page_CAT - Quit Test/button_Quit Test'))

// click on return to home page
WebUI.click(findTestObject('Object Repository/Page_CAT - Quit Test/button_Return to Home Page'))

// logout
CustomKeywords.'basics.utils.logout'()

// login as TA
CustomKeywords.'basics.utils.login'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

// adding a 2 seconds delay
WebUI.delay(2)

// click Active candidates
WebUI.click(findTestObject('Page_CAT - TA/button_Active Candidates'))

// make sure that there are no more active candidate in TA' active candidates table
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Test Administrator/active_candidates_table_There are no active candidates'), 
    2)

// delete assigned tests data related
// first candidate
CustomKeywords.'database.DataDeletion.deleteCandidateAssignedTestsData'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS)

// second candidate
CustomKeywords.'database.DataDeletion.deleteCandidateAssignedTestsData'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

// logout and close browser
CustomKeywords.'basics.utils.logoutAndClose'()