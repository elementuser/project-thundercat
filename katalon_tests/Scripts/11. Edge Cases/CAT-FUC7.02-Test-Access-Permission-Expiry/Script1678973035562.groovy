import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration

String currentProfile = RunConfiguration.getExecutionProfile()

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// make sure that the TA user does not already have a second test access (GlobalVariable.TEST_ACCESS_TEST_ORDER_NUMBER_2)
CustomKeywords.'database.DataDeletion.deleteUserTestPermission'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.TEST_ACCESS_TEST_ORDER_NUMBER_2)

// make sure that there are no active test access codes
CustomKeywords.'database.DataDeletion.deleteTaTestAccessCodes'(GlobalVariable.TA_USER_EMAIL_ADDRESS)

// assigning new test access to TA user
CustomKeywords.'basics.permissions.assignTestAccess'(false, true, GlobalVariable.TA_USER_FIRST_NAME, GlobalVariable.TA_USER_LAST_NAME)

// logout
CustomKeywords.'basics.utils.logout'()

// login as a TA
CustomKeywords.'basics.utils.login'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

//Click Generate Test Access Code
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/span_Generate Test Access Code'))

//Select Test Order Number
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/svg_Select_css-tj5bde-Svg'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/div_KATALON2-option-1'))

//Select Test
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/svg_Select_css-tj5bde-Svg_1'))

WebUI.sendKeys(findTestObject('Page_CAT - Test Administrator/test_dropdown_input_2'), Keys.chord(Keys.ENTER))

//Select Test Session Language
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/svg_Select_css-tj5bde-Svg_1_2'))

WebUI.sendKeys(findTestObject('Page_CAT - Test Administrator/test_session_language_dropdown_input_2'), Keys.chord(Keys.ENTER))

//Click Generate
WebUI.click(findTestObject('Page_CAT - TA/span_Generate'))

// adding delay
WebUI.delay(2)

// click MENU
WebUI.click(findTestObject('Page_CAT - Home/button_MENU'))

// click My Profile
WebUI.click(findTestObject('Page_CAT - Home/button_My Profile'))

// select Rights and Permissions side navigation item
WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Rights and Permissions'))

// adding 2 seconds delay
WebUI.delay(2)

// scroll to bottom of the page
WebUI.scrollToPosition(9999999, 9999999)

// make sure that both test permissions are displayed in the table
// first row (Test Order Number = GlobalVariable.TEST_ACCESS_TEST_ORDER_NUMBER)
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Profile/profile_page_test_access_permissions_table_test_form_1'), 
    2)

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Profile/profile_page_test_access_permissions_table_test_order_number_1'), 
    2)

WebUI.verifyEqual(WebUI.getText(findTestObject('Object Repository/Page_CAT - Profile/profile_page_test_access_permissions_table_test_order_number_1')), 
    GlobalVariable.TEST_ACCESS_TEST_ORDER_NUMBER)

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Profile/profile_page_test_access_permissions_table_assessment_process_number_1'), 
    2)

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Profile/profile_page_test_access_permissions_table_expiry_date_1'), 
    2)

// second row (Test Order Number = GlobalVariable.TEST_ACCESS_TEST_ORDER_NUMBER_2)
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Profile/profile_page_test_access_permissions_table_test_form_2'), 
    2)

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Profile/profile_page_test_access_permissions_table_test_order_number_2'), 
    2)

WebUI.verifyEqual(WebUI.getText(findTestObject('Object Repository/Page_CAT - Profile/profile_page_test_access_permissions_table_test_order_number_2')), 
    GlobalVariable.TEST_ACCESS_TEST_ORDER_NUMBER_2)

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Profile/profile_page_test_access_permissions_table_assessment_process_number_2'), 
    2)

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Profile/profile_page_test_access_permissions_table_expiry_date_2'), 
    2)

// make sure that all sort icons are clickable
WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/svg_Test Order Number_svg-inline--fa fa-sor_71f366'))

WebUI.delay(2)

WebUI.scrollToPosition(9999999, 9999999)

WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/svg_Assessment Process Number_svg-inline--f_e778de'))

WebUI.delay(2)

WebUI.scrollToPosition(9999999, 9999999)

WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/svg_Expiry Date_svg-inline--fa fa-sort fa-w-10'))

WebUI.delay(2)

WebUI.scrollToPosition(9999999, 9999999)

WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/svg_Test Form_svg-inline--fa fa-sort fa-w-10'))

// get yesterday's date
Date yesterday = new Date() - 1

String yesterdayStr = yesterday.format('yyyy-MM-dd')

// ONLY TEST IF WE ARE ON LOCAL ENVIRONEMENT
if (currentProfile.equals('local')) {

	// update expiry date of second test access (where test order number = GlobalVariable.TEST_ACCESS_TEST_ORDER_NUMBER_2)
	CustomKeywords.'database.DataUpdate.updateTestPermissionExpiryDate'(GlobalVariable.TEST_ACCESS_TEST_ORDER_NUMBER_2, yesterdayStr)

	// logout
	CustomKeywords.'basics.utils.logout'()


	// wait a minute, so the celery task is executed
	WebUI.delay(60)
	
	// log back in
	CustomKeywords.'basics.utils.login'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)
	
	// make sure that there is no active test access code
	WebUI.verifyElementNotPresent(findTestObject('Object Repository/Page_CAT - Test Administrator/ta_test_access_codes_table_first_row_test_access_code'), 
	    2, FailureHandling.STOP_ON_FAILURE)
	
	// click MENU
	WebUI.click(findTestObject('Page_CAT - Home/button_MENU'))
	
	// click My Profile
	WebUI.click(findTestObject('Page_CAT - Home/button_My Profile'))
	
	// select Rights and Permissions side navigation item
	WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Rights and Permissions'))
	
	// adding 2 seconds delay
	WebUI.delay(2)
	
	// scroll to bottom of the page
	WebUI.scrollToPosition(9999999, 9999999)
	
	// make sure that only the first test permission is displayed in the table
	// first row (Test Order Number = GlobalVariable.TEST_ACCESS_TEST_ORDER_NUMBER)
	WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Profile/profile_page_test_access_permissions_table_test_form_1'), 
	    2)
	
	WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Profile/profile_page_test_access_permissions_table_test_order_number_1'), 
	    2)
	
	WebUI.verifyEqual(WebUI.getText(findTestObject('Object Repository/Page_CAT - Profile/profile_page_test_access_permissions_table_test_order_number_1')), 
	    GlobalVariable.TEST_ACCESS_TEST_ORDER_NUMBER)
	
	WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Profile/profile_page_test_access_permissions_table_assessment_process_number_1'), 
	    2)
	
	WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Profile/profile_page_test_access_permissions_table_expiry_date_1'), 
	    2)
	
	// second row should not be displayed (Test Order Number = GlobalVariable.TEST_ACCESS_TEST_ORDER_NUMBER_2)
	WebUI.verifyElementNotPresent(findTestObject('Object Repository/Page_CAT - Profile/profile_page_test_access_permissions_table_test_form_2'), 
	    2)
	
	WebUI.verifyElementNotPresent(findTestObject('Object Repository/Page_CAT - Profile/profile_page_test_access_permissions_table_test_order_number_2'), 
	    2)
	
	WebUI.verifyElementNotPresent(findTestObject('Object Repository/Page_CAT - Profile/profile_page_test_access_permissions_table_assessment_process_number_2'), 
	    2)
	
	WebUI.verifyElementNotPresent(findTestObject('Object Repository/Page_CAT - Profile/profile_page_test_access_permissions_table_expiry_date_2'), 
	    2)
	
	// Delete TA's second test access
	CustomKeywords.'database.DataDeletion.deleteUserTestPermission'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.TEST_ACCESS_TEST_ORDER_NUMBER_2)
} else {
	// make sure that there are no active test access codes
	CustomKeywords.'database.DataDeletion.deleteTaTestAccessCodes'(GlobalVariable.TA_USER_EMAIL_ADDRESS)
}

// logout and close the browser
CustomKeywords.'basics.utils.logoutAndClose'()