import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

// =========== Request Permissions ===========
// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// making sure that the user does not have any permissions assigned to his account
CustomKeywords.'database.DataDeletion.deleteUserPermissions'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS)

// making sure that the user does not have any pending permissions assigned to his account
CustomKeywords.'database.DataDeletion.deletePendingUserPermissions'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.TA_PERMISSION)

CustomKeywords.'database.DataDeletion.deletePendingUserPermissions'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.ETTA_PERMISSION)

CustomKeywords.'database.DataDeletion.deletePendingUserPermissions'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.PPC_PERMISSION)

CustomKeywords.'database.DataDeletion.deletePendingUserPermissions'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.SCORER_PERMISSION)

CustomKeywords.'database.DataDeletion.deletePendingUserPermissions'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.TEST_BUILDER_PERMISSION)

CustomKeywords.'database.DataDeletion.deletePendingUserPermissions'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.ADAPTATIONS_PERMISSION)

CustomKeywords.'database.DataDeletion.deletePendingUserPermissions'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.RD_OPERATIONS_PERMISSION)

CustomKeywords.'database.DataDeletion.deletePendingUserPermissions'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.TD_LEVEL_1_PERMISSION)

// login
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

// click MENU
WebUI.click(findTestObject('Page_CAT - Home/button_MENU'))

// click My Profile
WebUI.click(findTestObject('Page_CAT - Home/button_My Profile'))

// select Rights and Permissions side navigation item
WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Rights and Permissions'))

// click Obtain Rights and Permissions button
WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Obtain Rights and Permissions'))

// fill out the form
WebUI.setText(findTestObject('Object Repository/Page_CAT - Profile/input_,You must enter your Government of Ca_34c402'), 
    GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS)

WebUI.setText(findTestObject('Object Repository/Page_CAT - Profile/input_PRI or Service Number_pri-or-military-nbr'), '12345679')

WebUI.setText(findTestObject('Object Repository/Page_CAT - Profile/input_Supervisor Name_supervisor'), 'Katalon Supervisor')

WebUI.setText(findTestObject('Object Repository/Page_CAT - Profile/input_Supervisor Email Address_supervisor-email'), 'katalon.supervisor@email.ca')

WebUI.setText(findTestObject('Object Repository/Page_CAT - Profile/textarea_Katalon Tests'), 'Katalon Tests')

// select TA permission
WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/input_ta_permission_to_request'))

// select ETTA permission (Operations Level 1)
WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/input_etta_permission_to_request'))

// select PPC R&D Level 1 permission
WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/input_ppc_permission_to_request'))

// select Test Scorer permission
WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/input_scorer_permission_to_request'))

// select Test Builder permission
WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/input_test_builder_permission_to_request'))

// select Test Adaptations permission
WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/input_adaptations_permission_to_request'))

// select R&D Operations permission
WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/input_rd_operations_permission_to_request'))

// select Test Developer permission
WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/input_test_developer_level_1_permission_to_request'))

//click Submit
WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Submit'))

// adding delay
WebUI.delay(2)

// logout
CustomKeywords.'basics.utils.logout'()

// =========== Request Permissions (END) ===========
// =========== Approve Requests ===========
// login
CustomKeywords.'basics.utils.login'(GlobalVariable.SUPER_USER_USERNAME, GlobalVariable.SUPER_USER_ENCRYPTED_PASSWORD)

// wait for loading to be done
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/pending_permissions_loading_icon'), 
    25)

// adding delay
WebUI.delay(10)

// search for user related to permission to approve
WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_permission-requests-search-bar'), 
    GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS)

// simulate "Enter" key press
WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Business Operations/input_Search_permission-requests-search-bar'), 
    Keys.chord(Keys.ENTER))

// wait for loading to be done
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/pending_permissions_loading_icon'), 
    25)

// adding delay
WebUI.delay(4)

// click View (located in the table)
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_View'))

// make sure that the right active permission has been selected
if (!(WebUI.getText(findTestObject('Object Repository/Page_CAT - Business Operations/pending_permissions_request_popup_description_paragraph')).contains(
    GlobalVariable.STANDARD_USER_ONE_FIRST_NAME))) {
    KeywordUtil.markFailedAndStop('Wrong permission request has been selected')
}

if (!(WebUI.getText(findTestObject('Object Repository/Page_CAT - Business Operations/pending_permissions_request_popup_description_paragraph')).contains(
    GlobalVariable.STANDARD_USER_ONE_LAST_NAME))) {
    KeywordUtil.markFailedAndStop('Wrong permission request has been selected')
}

// click Approve
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_Approve'))

// wait for loading to be done
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/pending_permissions_loading_icon'), 
    25)

// adding delay
WebUI.delay(4)

// click View (located in the table)
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_View'))

// make sure that the right active permission has been selected
if (!(WebUI.getText(findTestObject('Object Repository/Page_CAT - Business Operations/pending_permissions_request_popup_description_paragraph')).contains(
    GlobalVariable.STANDARD_USER_ONE_FIRST_NAME))) {
    KeywordUtil.markFailedAndStop('Wrong permission request has been selected')
}

if (!(WebUI.getText(findTestObject('Object Repository/Page_CAT - Business Operations/pending_permissions_request_popup_description_paragraph')).contains(
    GlobalVariable.STANDARD_USER_ONE_LAST_NAME))) {
    KeywordUtil.markFailedAndStop('Wrong permission request has been selected')
}

// click Approve
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_Approve'))

// wait for loading to be done
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/pending_permissions_loading_icon'), 
    25)

// adding delay
WebUI.delay(4)

// click View (located in the table)
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_View'))

// make sure that the right active permission has been selected
if (!(WebUI.getText(findTestObject('Object Repository/Page_CAT - Business Operations/pending_permissions_request_popup_description_paragraph')).contains(
    GlobalVariable.STANDARD_USER_ONE_FIRST_NAME))) {
    KeywordUtil.markFailedAndStop('Wrong permission request has been selected')
}

if (!(WebUI.getText(findTestObject('Object Repository/Page_CAT - Business Operations/pending_permissions_request_popup_description_paragraph')).contains(
    GlobalVariable.STANDARD_USER_ONE_LAST_NAME))) {
    KeywordUtil.markFailedAndStop('Wrong permission request has been selected')
}

// click Approve
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_Approve'))

// wait for loading to be done
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/pending_permissions_loading_icon'), 
    25)

// adding delay
WebUI.delay(4)

// click View (located in the table)
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_View'))

// make sure that the right active permission has been selected
if (!(WebUI.getText(findTestObject('Object Repository/Page_CAT - Business Operations/pending_permissions_request_popup_description_paragraph')).contains(
    GlobalVariable.STANDARD_USER_ONE_FIRST_NAME))) {
    KeywordUtil.markFailedAndStop('Wrong permission request has been selected')
}

if (!(WebUI.getText(findTestObject('Object Repository/Page_CAT - Business Operations/pending_permissions_request_popup_description_paragraph')).contains(
    GlobalVariable.STANDARD_USER_ONE_LAST_NAME))) {
    KeywordUtil.markFailedAndStop('Wrong permission request has been selected')
}

// click Approve
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_Approve'))

// wait for loading to be done
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/pending_permissions_loading_icon'), 
    25)

// adding delay
WebUI.delay(4)

// click View (located in the table)
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_View'))

// make sure that the right active permission has been selected
if (!(WebUI.getText(findTestObject('Object Repository/Page_CAT - Business Operations/pending_permissions_request_popup_description_paragraph')).contains(
    GlobalVariable.STANDARD_USER_ONE_FIRST_NAME))) {
    KeywordUtil.markFailedAndStop('Wrong permission request has been selected')
}

if (!(WebUI.getText(findTestObject('Object Repository/Page_CAT - Business Operations/pending_permissions_request_popup_description_paragraph')).contains(
    GlobalVariable.STANDARD_USER_ONE_LAST_NAME))) {
    KeywordUtil.markFailedAndStop('Wrong permission request has been selected')
}

// click Approve
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_Approve'))

// wait for loading to be done
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/pending_permissions_loading_icon'),
	25)

// adding delay
WebUI.delay(4)

// click View (located in the table)
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_View'))

// make sure that the right active permission has been selected
if (!(WebUI.getText(findTestObject('Object Repository/Page_CAT - Business Operations/pending_permissions_request_popup_description_paragraph')).contains(
	GlobalVariable.STANDARD_USER_ONE_FIRST_NAME))) {
	KeywordUtil.markFailedAndStop('Wrong permission request has been selected')
}

if (!(WebUI.getText(findTestObject('Object Repository/Page_CAT - Business Operations/pending_permissions_request_popup_description_paragraph')).contains(
	GlobalVariable.STANDARD_USER_ONE_LAST_NAME))) {
	KeywordUtil.markFailedAndStop('Wrong permission request has been selected')
}

// click Approve
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_Approve'))

// wait for loading to be done
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/pending_permissions_loading_icon'),
	25)

// adding delay
WebUI.delay(4)

// click View (located in the table)
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_View'))

// make sure that the right active permission has been selected
if (!(WebUI.getText(findTestObject('Object Repository/Page_CAT - Business Operations/pending_permissions_request_popup_description_paragraph')).contains(
	GlobalVariable.STANDARD_USER_ONE_FIRST_NAME))) {
	KeywordUtil.markFailedAndStop('Wrong permission request has been selected')
}

if (!(WebUI.getText(findTestObject('Object Repository/Page_CAT - Business Operations/pending_permissions_request_popup_description_paragraph')).contains(
	GlobalVariable.STANDARD_USER_ONE_LAST_NAME))) {
	KeywordUtil.markFailedAndStop('Wrong permission request has been selected')
}

// click Approve
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_Approve'))

// wait for loading to be done
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/pending_permissions_loading_icon'),
	25)

// adding delay
WebUI.delay(4)

// click View (located in the table)
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_View'))

// make sure that the right active permission has been selected
if (!(WebUI.getText(findTestObject('Object Repository/Page_CAT - Business Operations/pending_permissions_request_popup_description_paragraph')).contains(
	GlobalVariable.STANDARD_USER_ONE_FIRST_NAME))) {
	KeywordUtil.markFailedAndStop('Wrong permission request has been selected')
}

if (!(WebUI.getText(findTestObject('Object Repository/Page_CAT - Business Operations/pending_permissions_request_popup_description_paragraph')).contains(
	GlobalVariable.STANDARD_USER_ONE_LAST_NAME))) {
	KeywordUtil.markFailedAndStop('Wrong permission request has been selected')
}

// click Approve
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_Approve'))


// wait for loading to be done
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/pending_permissions_no_results_found'),
	25)

// adding delay
WebUI.delay(4)

// logout
CustomKeywords.'basics.utils.logout'()

// =========== Approve Requests (END) ===========
// ========== TESTS ========== 
// VERIFY USER CANT REQUEST MORE ACCESS
// login
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

// click MENU
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_MENU (2)'))

// click My Profile
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_My Profile (1)'))

// click permissions
WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Rights and Permissions'))

// Adding small delay
WebUI.delay(2)

// verify rights button is not present
WebUI.verifyElementNotPresent(findTestObject('Object Repository/Page_CAT - Profile/button_Obtain Rights and Permissions'), 
    2)

// VERIFY USER CANT REQUEST MORE ACCESS (END)

// VERIFY THE USER HAS ACCESS TO ALL OF THE SECTIONS
// open business opperations
// click MENU
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_MENU (2)'))

// click business opperations
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Business Operations (1)'))

// adding small delay
WebUI.delay(2)

// open r&d dashboard
// click MENU
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_MENU (2)'))

// click r&d dashboard
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_RD Dashboard'))

// open test scorer
// click MENU
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_MENU (2)'))

// click test scorer
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Test Scorer'))

// adding small delay
WebUI.delay(2)

// open test administration
// click MENU
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_MENU (2)'))

// click test admin
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Test Administrator'))

// adding small delay
WebUI.delay(2)

// open test developer homepage
// click MENU
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_MENU (2)'))

// click test developer
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Test Developer'))

// adding small delay
WebUI.delay(2)

// VERIFY THE USER HAS ACCESS TO ALL OF THE SECTIONS (END)
//// ========== TESTS (END) ========== 
// TEARDOWN
// Delete users permissions
CustomKeywords.'database.DataDeletion.deleteUserPermissions'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS)

// logout and close the browser
CustomKeywords.'basics.utils.logoutAndClose'()