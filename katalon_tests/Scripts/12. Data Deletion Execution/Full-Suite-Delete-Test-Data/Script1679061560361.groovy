import internal.GlobalVariable as GlobalVariable

// delete Standard User One
CustomKeywords.'database.DataDeletion.deleteUser'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS)

// delete Standard User Two
CustomKeywords.'database.DataDeletion.deleteUser'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS)

// delete Standard User Three
CustomKeywords.'database.DataDeletion.deleteUser'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

// delete PPC User
CustomKeywords.'database.DataDeletion.deleteUser'(GlobalVariable.PPC_USER_EMAIL_ADDRESS)

// delete TA User
CustomKeywords.'database.DataDeletion.deleteUser'(GlobalVariable.TA_USER_EMAIL_ADDRESS)

// delete ETTA User
CustomKeywords.'database.DataDeletion.deleteUser'(GlobalVariable.ETTA_USER_EMAIL_ADDRESS)