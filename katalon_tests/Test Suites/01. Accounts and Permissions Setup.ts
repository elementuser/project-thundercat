<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>01. Accounts and Permissions Setup</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>true</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>75d7a8af-102c-4ba2-ba16-fe356ae9f1b0</testSuiteGuid>
   <testCaseLink>
      <guid>cc84d6e9-c568-43e6-923d-e663429370f2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/01. Accounts and Permissions Setup/1.1 Create Users/1.1.1-Create-Account-Standard-User-One</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>4a1d55ba-446a-45bd-bbcb-02b28c99974c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/01. Accounts and Permissions Setup/1.1 Create Users/1.1.2-Create-Account-Standard-User-Two</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>a9453778-3feb-4395-b9c4-622fbdd2a8ac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/01. Accounts and Permissions Setup/1.1 Create Users/1.1.3-Create-Account-Standard-User-Three</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>09937574-80a8-41b5-8f7a-71ecc52c4c34</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/01. Accounts and Permissions Setup/1.1 Create Users/1.1.4-Create-Account-ETTA-User</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>27799db7-d647-44cc-b4c1-48bcc2797ce4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/01. Accounts and Permissions Setup/1.1 Create Users/1.1.5-Create-Account-PPC-User</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>3df68806-2171-4558-a120-829949b71530</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/01. Accounts and Permissions Setup/1.1 Create Users/1.1.6-Create-Account-TA-User-One</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>8bbca84b-cd3f-47aa-a8ba-dd4e08921400</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/01. Accounts and Permissions Setup/1.1 Create Users/1.1.7-Create-Account-TA-User-Two</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>d5581a42-f5d5-4972-805d-660830d10319</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/01. Accounts and Permissions Setup/1.2 Permissions/1.2.1 Send Permission Requests/1.2.1.1-Send-Permissions-ETTA-User</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>e52bac99-16d3-43a9-a1c0-2403ad1df7ad</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/01. Accounts and Permissions Setup/1.2 Permissions/1.2.1 Send Permission Requests/1.2.1.2-Send-Permissions-PPC-User</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>7d32984f-a0ed-44fc-84f0-8a46e5852355</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/01. Accounts and Permissions Setup/1.2 Permissions/1.2.1 Send Permission Requests/1.2.1.3-Send-Permissions-TA-User-One</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>57580e70-a181-473d-aab5-cd55b52224b1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/01. Accounts and Permissions Setup/1.2 Permissions/1.2.1 Send Permission Requests/1.2.1.4-Send-Permissions-TA-User-Two</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>cec73d4e-b84f-424d-aad1-47892551304f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/01. Accounts and Permissions Setup/1.2 Permissions/1.2.2 Approve Permission Requests/1.2.2.1-Approve-Permissions-ETTA-User</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>ea72bc65-e958-41fb-946a-13cabd2d59fb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/01. Accounts and Permissions Setup/1.2 Permissions/1.2.2 Approve Permission Requests/1.2.2.2-Approve-Permissions-PPC-User</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>82abdbb6-2b1f-497c-a769-c8c592038211</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/01. Accounts and Permissions Setup/1.2 Permissions/1.2.2 Approve Permission Requests/1.2.2.3-Approve-Permissions-TA-User-One</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>f7dc9c5a-15e6-4c05-ae67-2f33bb4c4baa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/01. Accounts and Permissions Setup/1.2 Permissions/1.2.2 Approve Permission Requests/1.2.2.4-Approve-Permissions-TA-User-Two</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>2e68260f-1e46-4bc9-9db7-3882d4d208aa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/01. Accounts and Permissions Setup/1.3 Test Accesses/1.3.1-Assign-Test-Accesses-TA-User-One</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>90b9883f-8b23-49ec-af21-1fd4d01ab8fd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/01. Accounts and Permissions Setup/1.3 Test Accesses/1.3.2-Assign-Test-Accesses-TA-User-Two</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
