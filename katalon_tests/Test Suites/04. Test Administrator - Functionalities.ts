<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>04. Test Administrator - Functionalities</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>true</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>0f0cacc3-7144-4a28-8ca0-b586cb3158a5</testSuiteGuid>
   <testCaseLink>
      <guid>1f667b1f-360b-4447-b7a9-b6fe8ac400b1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/04. Test Administrator - Functionalities/4.1 Language - Functionnality/4.1.1-Page-Language-Changes</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>bb102a8a-1839-4013-91f9-fd32c4395f4e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/04. Test Administrator - Functionalities/4.2 Generate Test Access Code - Section/4.2.1-Generate-Test-Access-Code</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>35644775-5b59-43bb-ae11-96a12ccc11e8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/04. Test Administrator - Functionalities/4.3 Candidate Check-In/4.3.1-Active-Test-Functions</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>47b050f6-16dd-4a92-9b7c-acaf62c86346</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/04. Test Administrator - Functionalities/4.3 Candidate Check-In/4.3.2-Multiple-Candidates-In-Test</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>f97dbd3a-9511-49c3-a401-293a6ec92711</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/04. Test Administrator - Functionalities/4.4 Active Candidates - Section/4.4.1-Active-Candidate-Test</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>b6c3807e-a371-430b-8b1e-cf0f269d0bd4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/04. Test Administrator - Functionalities/4.5 In-Test Functionnalities/4.5.1-Quit-Test</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>70c714cc-71e4-42b4-9815-926533542b0b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/04. Test Administrator - Functionalities/4.5 In-Test Functionnalities/4.5.2-Lock-Test-By-TA</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>4d7ee885-729b-4339-8313-cc0c5f7ca140</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/04. Test Administrator - Functionalities/4.5 In-Test Functionnalities/4.5.3-Lock-Test-By-Candidate-REFACTOR NEEDED</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
