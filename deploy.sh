#!/bin/bash

path="/CAT"


if [ -d $path/project-thundercat ]; then
    cd $path/project-thundercat
    git pull
    docker-compose -f docker-compose-cicd.yml down
    docker-compose -f docker-compose-cicd.yml build
    docker-compose -f docker-compose-cicd.yml up -d
else
    git clone https://gitlab.com/thundercat-transition/project-thundercat.git $path/project-thundercat
    cd $path/project-thundercat
    docker-compose -f docker-compose-cicd.yml down
    docker-compose -f docker-compose-cicd.yml build
    docker-compose -f docker-compose-cicd.yml up -d
fi
