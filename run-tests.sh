#!/bin/bash
# Constants to add colour to text
BLUE='\033[0;34m'
CYAN='\033[0;36m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color

# getting latest version of MSSQL image
printf "${CYAN}Getting latest version of MSSQL image${NC}\n"
docker pull projectthundercat/cat-mssql
printf "${CYAN}Latest version of MSSQL image pulled successfully${NC}\n"

# making sure that your local environment is down
printf "${CYAN}Closing your local environment${NC}\n"
docker compose down
printf "${CYAN}Local environment has been closed successfully${NC}\n"

#==================== BACKEND ====================
printf "**** ${BLUE}Begin backend tests${NC} ****\n"
#faking all migrations in case this script is executed while using the MSSQL image (structure of DB with empty tables and static data only)
printf "${CYAN}Faking all migrations${NC}\n"
docker compose -f docker-compose-cicd-frontend-backend.yml run --rm backend ./manage.py migrate --fake
printf "${CYAN}Migrations have been faked successfully${NC}\n"
#executing  backend tests
printf "${CYAN}Executing backend tests${NC}\n"
docker compose -f docker-compose-cicd-frontend-backend.yml run --rm backend ./manage.py test tests
printf "**** ${GREEN}Complete backend tests${NC} ****\n"
# ==================== BACKEND (END) ====================

# ==================== FRONTEND ====================
printf "**** ${BLUE}Begin frontend tests${NC} ****\n"
# executing frontend tests
printf "${CYAN}Executing Frontend tests${NC}\n"
docker compose -f docker-compose-cicd-frontend-backend.yml run -e CI=true --rm frontend npm test src/tests/ -- --coverage
printf "**** ${GREEN}Complete frontend tests${NC} ****\n"
# ==================== FRONTEND (END) ====================

# ==================== API ====================
printf "**** ${BLUE}Begin API tests${NC} ****\n"
# building the apps
printf "${CYAN}Building the app${NC}\n"
docker compose -f docker-compose-cicd-api-local.yml up -d --build
printf "${CYAN}App has been built successfully${NC}\n"
# creating a superuser
printf "${CYAN}Creating super user${NC}\n"
# password needs to be set as a variable, since it contains a special character (will fail if set directly in the docker exec command while using Windows PoweShell to run this script)
password="Admin1234!"
docker exec -i project-thundercat-backend-1 python manage.py shell -c "from django.contrib.auth import get_user_model; import datetime; import pytz; User = get_user_model(); User.objects.create_superuser(username='admin', first_name='Admin', last_name='Account', birth_date='1975-01-01', pri='', military_service_nbr='', email='admin@email.ca', secondary_email='', last_password_change='2021-01-01', is_staff=True, psrs_applicant_id='', last_login=datetime.datetime.now(pytz.utc), password='$password');"
printf "${CYAN}Super user has been created${NC}\n"
# adding small delay to allow the database setup to complete
printf "${CYAN}Sleep time of 30 seconds to allow database setup to complete${NC}\n"
docker exec -i project-thundercat-backend-1 python manage.py shell -c "import time; time.sleep(30);"
# going to the right directory in order to be able to run newman
printf "${CYAN}Changing directory (./node_modules/newman/bin)${NC}\n"
cd ./node_modules/newman/bin
# executing API tests with newman
printf "${CYAN}Executing newman command${NC}\n"
node newman run ../../../backend/tests/postman/ThunderCAT-new.postman_collection.json -e ../../../backend/tests/postman/environments/local/ThunderCAT.local.postman_environment.json
printf "**** ${GREEN}Complete API tests${NC} ****\n"
# ==================== API (END) ====================

# closing all running containers
printf "${CYAN}Closing all running containers${NC}\n"
docker compose down
printf "${CYAN}All running containers have been closed successfully${NC}\n"
